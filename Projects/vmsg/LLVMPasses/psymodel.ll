; ModuleID = 'psymodel.c'
source_filename = "psymodel.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type { i32, i32, i32, double, [2 x [1600 x double]], [2 x [1776 x double]], [2 x [2 x [576 x double]]], [2 x [2 x [576 x double]]], [2 x double], [2 x double], [4 x [1024 x double]], [2 x [4 x [1024 x double]]], [2 x [4 x double]], [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]], [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]], [4 x double], [2 x [4 x double]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x [3 x i32]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [2 x i32], i32, i32, i32 }
%struct.hip_global_struct = type opaque
%struct.III_psy_ratio = type { %struct.III_psy_xmin, %struct.III_psy_xmin }
%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon.2, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon.2 = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.anon.3 = type { i32, i32, i32 }

@psymodel_init.sk = internal constant [11 x float] [float 0xC01D9999A0000000, float 0xC01D9999A0000000, float 0xC01D9999A0000000, float -9.500000e+00, float 0xC01D9999A0000000, float 0xC018666660000000, float -5.500000e+00, float 0xC012CCCCC0000000, float 0xC012CCCCC0000000, float 0xC012CCCCC0000000, float 0xC012CCCCC0000000], align 16
@vbrpsy_attack_detection.fircoef = internal constant [10 x float] [float 0xBC73F30560000000, float 0xBF9170C360000000, float 0xBC6F1E3500000000, float 0x3FA567C0E0000000, float 0xBC9367EA20000000, float 0xBFB66F13C0000000, float 0xBC81C67160000000, float 0x3FC7DA3D00000000, float 0xBC9FD53600000000, float 0xBFE4159C40000000], align 16
@tab = internal constant [9 x float] [float 1.000000e+00, float 0x3FE96B26C0000000, float 0x3FE430D300000000, float 0x3FE430D300000000, float 0x3FE430D300000000, float 0x3FE430D300000000, float 0x3FE430D300000000, float 0x3FD0137F40000000, float 0x3FBE13D320000000], align 16
@tab_mask_add_delta = internal constant [9 x i32] [i32 2, i32 2, i32 2, i32 1, i32 1, i32 1, i32 0, i32 0, i32 -1], align 16
@vbrpsy_mask_add.table2 = internal constant [10 x float] [float 0x3FFC73D120000000, float 0x3FFD8A7CA0000000, float 0x3FFEABD4E0000000, float 0x3FFF229360000000, float 0x3FFF9B21C0000000, float 0x3FFD51EF60000000, float 0x3FFB32FB40000000, float 0x3FF7F09C40000000, float 0x3FF5122F80000000, float 1.000000e+00], align 16
@pecalc_s.regcoef_s = internal constant [12 x float] [float 0x40279999A0000000, float 0x402B333340000000, float 0x4031333340000000, float 3.200000e+01, float 4.650000e+01, float 0x4049A66660000000, float 5.750000e+01, float 0x4050C66660000000, float 7.150000e+01, float 0x4055266660000000, float 0x4058666660000000, float 1.300000e+02], align 16
@pecalc_l.regcoef_l = internal constant [21 x float] [float 0x401B333340000000, float 0x4017333340000000, float 0x4017333340000000, float 0x40199999A0000000, float 6.500000e+00, float 0x4023CCCCC0000000, float 0x4028333340000000, float 0x402CCCCCC0000000, float 1.500000e+01, float 0x4032E66660000000, float 0x40359999A0000000, float 0x403AE66660000000, float 0x40411999A0000000, float 0x40441999A0000000, float 0x4047666660000000, float 5.650000e+01, float 0x404E5999A0000000, float 0x40527999A0000000, float 0x40556CCCC0000000, float 0x40575999A0000000, float 0x405F866660000000], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @L3psycho_anal_vbr(%struct.lame_internal_flags* %gfc, float** %buffer, i32 %gr_out, [2 x %struct.III_psy_ratio]* %masking_ratio, [2 x %struct.III_psy_ratio]* %masking_MS_ratio, float* %percep_entropy, float* %percep_MS_entropy, float* %energy, i32* %blocktype_d) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca float**, align 4
  %gr_out.addr = alloca i32, align 4
  %masking_ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %masking_MS_ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %percep_entropy.addr = alloca float*, align 4
  %percep_MS_entropy.addr = alloca float*, align 4
  %energy.addr = alloca float*, align 4
  %blocktype_d.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gdl = alloca %struct.PsyConst_CB2SB_t*, align 4
  %gds = alloca %struct.PsyConst_CB2SB_t*, align 4
  %plt = alloca %struct.plotting_data*, align 4
  %last_thm = alloca [4 x %struct.III_psy_xmin], align 16
  %wsamp_l = alloca [1024 x float]*, align 4
  %wsamp_s = alloca [3 x [256 x float]]*, align 4
  %fftenergy = alloca [513 x float], align 16
  %fftenergy_s = alloca [3 x [129 x float]], align 16
  %wsamp_L = alloca [2 x [1024 x float]], align 16
  %wsamp_S = alloca [2 x [3 x [256 x float]]], align 16
  %eb = alloca [4 x [64 x float]], align 16
  %thr = alloca [4 x [64 x float]], align 16
  %sub_short_factor = alloca [4 x [3 x float]], align 16
  %thmm = alloca float, align 4
  %pcfact = alloca float, align 4
  %ath_factor = alloca float, align 4
  %const_eb = alloca [64 x float]*, align 4
  %const_fftenergy_s = alloca [129 x float]*, align 4
  %ns_attacks = alloca [4 x [4 x i32]], align 16
  %uselongblock = alloca [2 x i32], align 4
  %chn = alloca i32, align 4
  %sb = alloca i32, align 4
  %sblock = alloca i32, align 4
  %n_chn_psy = alloca i32, align 4
  %ch01 = alloca i32, align 4
  %force_short_block_calc = alloca i32, align 4
  %ch0161 = alloca i32, align 4
  %ch0198 = alloca i32, align 4
  %new_thmm = alloca [3 x float], align 4
  %prev_thm = alloca float, align 4
  %t1 = alloca float, align 4
  %t2 = alloca float, align 4
  %ppe = alloca float*, align 4
  %type = alloca i32, align 4
  %mr = alloca %struct.III_psy_ratio*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 %gr_out, i32* %gr_out.addr, align 4
  store [2 x %struct.III_psy_ratio]* %masking_ratio, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %masking_MS_ratio, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  store float* %percep_entropy, float** %percep_entropy.addr, align 4
  store float* %percep_MS_entropy, float** %percep_MS_entropy.addr, align 4
  store float* %energy, float** %energy.addr, align 4
  store i32* %blocktype_d, i32** %blocktype_d.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 22
  %3 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %l = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %3, i32 0, i32 2
  store %struct.PsyConst_CB2SB_t* %l, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 22
  %5 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy2, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %5, i32 0, i32 3
  store %struct.PsyConst_CB2SB_t* %s, %struct.PsyConst_CB2SB_t** %gds, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 31
  %7 = load i32, i32* %analysis, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 23
  %9 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.plotting_data* [ %9, %cond.true ], [ null, %cond.false ]
  store %struct.plotting_data* %cond, %struct.plotting_data** %plt, align 4
  store float 0x3FE3333340000000, float* %pcfact, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 44
  %11 = load float, float* %msfix, align 4
  %cmp = fcmp ogt float %11, 0.000000e+00
  br i1 %cmp, label %cond.true3, label %cond.false4

cond.true3:                                       ; preds = %cond.end
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATH_offset_factor = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 46
  %13 = load float, float* %ATH_offset_factor, align 4
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 21
  %15 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %adjust_factor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %15, i32 0, i32 2
  %16 = load float, float* %adjust_factor, align 4
  %mul = fmul float %13, %16
  br label %cond.end5

cond.false4:                                      ; preds = %cond.end
  br label %cond.end5

cond.end5:                                        ; preds = %cond.false4, %cond.true3
  %cond6 = phi float [ %mul, %cond.true3 ], [ 1.000000e+00, %cond.false4 ]
  store float %cond6, float* %ath_factor, align 4
  %arraydecay = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 0
  store [64 x float]* %arraydecay, [64 x float]** %const_eb, align 4
  %arraydecay7 = getelementptr inbounds [3 x [129 x float]], [3 x [129 x float]]* %fftenergy_s, i32 0, i32 0
  store [129 x float]* %arraydecay7, [129 x float]** %const_fftenergy_s, align 4
  %17 = bitcast [4 x [4 x i32]]* %ns_attacks to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %17, i8 0, i32 64, i1 false)
  %18 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %18, i32 0, i32 41
  %19 = load i32, i32* %mode, align 4
  %cmp8 = icmp eq i32 %19, 1
  br i1 %cmp8, label %cond.true9, label %cond.false10

cond.true9:                                       ; preds = %cond.end5
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end5
  %20 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %20, i32 0, i32 14
  %21 = load i32, i32* %channels_out, align 4
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true9
  %cond12 = phi i32 [ 4, %cond.true9 ], [ %21, %cond.false10 ]
  store i32 %cond12, i32* %n_chn_psy, align 4
  %arrayidx = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %last_thm, i32 0, i32 0
  %22 = bitcast %struct.III_psy_xmin* %arrayidx to i8*
  %23 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %23, i32 0, i32 4
  %arrayidx13 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm, i32 0, i32 0
  %24 = bitcast %struct.III_psy_xmin* %arrayidx13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %22, i8* align 4 %24, i32 976, i1 false)
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %26 = load float**, float*** %buffer.addr, align 4
  %27 = load i32, i32* %gr_out.addr, align 4
  %28 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  %29 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  %30 = load float*, float** %energy.addr, align 4
  %arraydecay14 = getelementptr inbounds [4 x [3 x float]], [4 x [3 x float]]* %sub_short_factor, i32 0, i32 0
  %arraydecay15 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 0
  %arraydecay16 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 0
  call void @vbrpsy_attack_detection(%struct.lame_internal_flags* %25, float** %26, i32 %27, [2 x %struct.III_psy_ratio]* %28, [2 x %struct.III_psy_ratio]* %29, float* %30, [3 x float]* %arraydecay14, [4 x i32]* %arraydecay15, i32* %arraydecay16)
  %31 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %arraydecay17 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 0
  call void @vbrpsy_compute_block_type(%struct.SessionConfig_t* %31, i32* %arraydecay17)
  store i32 0, i32* %chn, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end11
  %32 = load i32, i32* %chn, align 4
  %33 = load i32, i32* %n_chn_psy, align 4
  %cmp18 = icmp slt i32 %32, %33
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %34 = load i32, i32* %chn, align 4
  %and = and i32 %34, 1
  store i32 %and, i32* %ch01, align 4
  %arraydecay19 = getelementptr inbounds [2 x [1024 x float]], [2 x [1024 x float]]* %wsamp_L, i32 0, i32 0
  %35 = load i32, i32* %ch01, align 4
  %add.ptr = getelementptr inbounds [1024 x float], [1024 x float]* %arraydecay19, i32 %35
  store [1024 x float]* %add.ptr, [1024 x float]** %wsamp_l, align 4
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %37 = load float**, float*** %buffer.addr, align 4
  %38 = load i32, i32* %chn, align 4
  %39 = load i32, i32* %gr_out.addr, align 4
  %arraydecay20 = getelementptr inbounds [513 x float], [513 x float]* %fftenergy, i32 0, i32 0
  %40 = load [1024 x float]*, [1024 x float]** %wsamp_l, align 4
  call void @vbrpsy_compute_fft_l(%struct.lame_internal_flags* %36, float** %37, i32 %38, i32 %39, float* %arraydecay20, [1024 x float]* %40)
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %42 = load i32, i32* %gr_out.addr, align 4
  %43 = load i32, i32* %chn, align 4
  %arraydecay21 = getelementptr inbounds [513 x float], [513 x float]* %fftenergy, i32 0, i32 0
  call void @vbrpsy_compute_loudness_approximation_l(%struct.lame_internal_flags* %41, i32 %42, i32 %43, float* %arraydecay21)
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay22 = getelementptr inbounds [513 x float], [513 x float]* %fftenergy, i32 0, i32 0
  %45 = load i32, i32* %chn, align 4
  %arrayidx23 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 %45
  %arraydecay24 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx23, i32 0, i32 0
  %46 = load i32, i32* %chn, align 4
  %arrayidx25 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 %46
  %arraydecay26 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx25, i32 0, i32 0
  %47 = load i32, i32* %chn, align 4
  call void @vbrpsy_compute_masking_l(%struct.lame_internal_flags* %44, float* %arraydecay22, float* %arraydecay24, float* %arraydecay26, i32 %47)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %chn, align 4
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %chn, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %49 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode27 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %49, i32 0, i32 41
  %50 = load i32, i32* %mode27, align 4
  %cmp28 = icmp eq i32 %50, 1
  br i1 %cmp28, label %if.then, label %if.end38

if.then:                                          ; preds = %for.end
  %arrayidx29 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 0
  %51 = load i32, i32* %arrayidx29, align 4
  %arrayidx30 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 1
  %52 = load i32, i32* %arrayidx30, align 4
  %add = add nsw i32 %51, %52
  %cmp31 = icmp eq i32 %add, 2
  br i1 %cmp31, label %if.then32, label %if.end

if.then32:                                        ; preds = %if.then
  %53 = load [64 x float]*, [64 x float]** %const_eb, align 4
  %arraydecay33 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 0
  %54 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %mld_cb = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %54, i32 0, i32 3
  %arraydecay34 = getelementptr inbounds [64 x float], [64 x float]* %mld_cb, i32 0, i32 0
  %55 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %55, i32 0, i32 21
  %56 = load %struct.ATH_t*, %struct.ATH_t** %ATH35, align 8
  %cb_l = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %56, i32 0, i32 10
  %arraydecay36 = getelementptr inbounds [64 x float], [64 x float]* %cb_l, i32 0, i32 0
  %57 = load float, float* %ath_factor, align 4
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix37 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 44
  %59 = load float, float* %msfix37, align 4
  %60 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %60, i32 0, i32 11
  %61 = load i32, i32* %npart, align 4
  call void @vbrpsy_compute_MS_thresholds([64 x float]* %53, [64 x float]* %arraydecay33, float* %arraydecay34, float* %arraydecay36, float %57, float %59, i32 %61)
  br label %if.end

if.end:                                           ; preds = %if.then32, %if.then
  br label %if.end38

if.end38:                                         ; preds = %if.end, %for.end
  store i32 0, i32* %chn, align 4
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc50, %if.end38
  %62 = load i32, i32* %chn, align 4
  %63 = load i32, i32* %n_chn_psy, align 4
  %cmp40 = icmp slt i32 %62, %63
  br i1 %cmp40, label %for.body41, label %for.end52

for.body41:                                       ; preds = %for.cond39
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %65 = load i32, i32* %chn, align 4
  %arrayidx42 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 %65
  %arraydecay43 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx42, i32 0, i32 0
  %66 = load i32, i32* %chn, align 4
  %arrayidx44 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 %66
  %arraydecay45 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx44, i32 0, i32 0
  %67 = load i32, i32* %chn, align 4
  call void @convert_partition2scalefac_l(%struct.lame_internal_flags* %64, float* %arraydecay43, float* %arraydecay45, i32 %67)
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %69 = load i32, i32* %chn, align 4
  %arrayidx46 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 %69
  %arraydecay47 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx46, i32 0, i32 0
  %70 = load i32, i32* %chn, align 4
  %arrayidx48 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 %70
  %arraydecay49 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx48, i32 0, i32 0
  %71 = load i32, i32* %chn, align 4
  call void @convert_partition2scalefac_l_to_s(%struct.lame_internal_flags* %68, float* %arraydecay47, float* %arraydecay49, i32 %71)
  br label %for.inc50

for.inc50:                                        ; preds = %for.body41
  %72 = load i32, i32* %chn, align 4
  %inc51 = add nsw i32 %72, 1
  store i32 %inc51, i32* %chn, align 4
  br label %for.cond39

for.end52:                                        ; preds = %for.cond39
  %73 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy53 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %73, i32 0, i32 22
  %74 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy53, align 4
  %force_short_block_calc54 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %74, i32 0, i32 7
  %75 = load i32, i32* %force_short_block_calc54, align 4
  store i32 %75, i32* %force_short_block_calc, align 4
  store i32 0, i32* %sblock, align 4
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc112, %for.end52
  %76 = load i32, i32* %sblock, align 4
  %cmp56 = icmp slt i32 %76, 3
  br i1 %cmp56, label %for.body57, label %for.end114

for.body57:                                       ; preds = %for.cond55
  store i32 0, i32* %chn, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc75, %for.body57
  %77 = load i32, i32* %chn, align 4
  %78 = load i32, i32* %n_chn_psy, align 4
  %cmp59 = icmp slt i32 %77, %78
  br i1 %cmp59, label %for.body60, label %for.end77

for.body60:                                       ; preds = %for.cond58
  %79 = load i32, i32* %chn, align 4
  %and62 = and i32 %79, 1
  store i32 %and62, i32* %ch0161, align 4
  %80 = load i32, i32* %ch0161, align 4
  %arrayidx63 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 %80
  %81 = load i32, i32* %arrayidx63, align 4
  %tobool64 = icmp ne i32 %81, 0
  br i1 %tobool64, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body60
  %82 = load i32, i32* %force_short_block_calc, align 4
  %tobool65 = icmp ne i32 %82, 0
  br i1 %tobool65, label %if.else, label %if.then66

if.then66:                                        ; preds = %land.lhs.true
  %83 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %84 = load i32, i32* %chn, align 4
  %85 = load i32, i32* %sblock, align 4
  call void @vbrpsy_skip_masking_s(%struct.lame_internal_flags* %83, i32 %84, i32 %85)
  br label %if.end74

if.else:                                          ; preds = %land.lhs.true, %for.body60
  %arraydecay67 = getelementptr inbounds [2 x [3 x [256 x float]]], [2 x [3 x [256 x float]]]* %wsamp_S, i32 0, i32 0
  %86 = load i32, i32* %ch0161, align 4
  %add.ptr68 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %arraydecay67, i32 %86
  store [3 x [256 x float]]* %add.ptr68, [3 x [256 x float]]** %wsamp_s, align 4
  %87 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %88 = load float**, float*** %buffer.addr, align 4
  %89 = load i32, i32* %chn, align 4
  %90 = load i32, i32* %sblock, align 4
  %arraydecay69 = getelementptr inbounds [3 x [129 x float]], [3 x [129 x float]]* %fftenergy_s, i32 0, i32 0
  %91 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s, align 4
  call void @vbrpsy_compute_fft_s(%struct.lame_internal_flags* %87, float** %88, i32 %89, i32 %90, [129 x float]* %arraydecay69, [3 x [256 x float]]* %91)
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %93 = load [129 x float]*, [129 x float]** %const_fftenergy_s, align 4
  %94 = load i32, i32* %chn, align 4
  %arrayidx70 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 %94
  %arraydecay71 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx70, i32 0, i32 0
  %95 = load i32, i32* %chn, align 4
  %arrayidx72 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 %95
  %arraydecay73 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx72, i32 0, i32 0
  %96 = load i32, i32* %chn, align 4
  %97 = load i32, i32* %sblock, align 4
  call void @vbrpsy_compute_masking_s(%struct.lame_internal_flags* %92, [129 x float]* %93, float* %arraydecay71, float* %arraydecay73, i32 %96, i32 %97)
  br label %if.end74

if.end74:                                         ; preds = %if.else, %if.then66
  br label %for.inc75

for.inc75:                                        ; preds = %if.end74
  %98 = load i32, i32* %chn, align 4
  %inc76 = add nsw i32 %98, 1
  store i32 %inc76, i32* %chn, align 4
  br label %for.cond58

for.end77:                                        ; preds = %for.cond58
  %99 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode78 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %99, i32 0, i32 41
  %100 = load i32, i32* %mode78, align 4
  %cmp79 = icmp eq i32 %100, 1
  br i1 %cmp79, label %if.then80, label %if.end94

if.then80:                                        ; preds = %for.end77
  %arrayidx81 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 0
  %101 = load i32, i32* %arrayidx81, align 4
  %arrayidx82 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 1
  %102 = load i32, i32* %arrayidx82, align 4
  %add83 = add nsw i32 %101, %102
  %cmp84 = icmp eq i32 %add83, 0
  br i1 %cmp84, label %if.then85, label %if.end93

if.then85:                                        ; preds = %if.then80
  %103 = load [64 x float]*, [64 x float]** %const_eb, align 4
  %arraydecay86 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 0
  %104 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %mld_cb87 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %104, i32 0, i32 3
  %arraydecay88 = getelementptr inbounds [64 x float], [64 x float]* %mld_cb87, i32 0, i32 0
  %105 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH89 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %105, i32 0, i32 21
  %106 = load %struct.ATH_t*, %struct.ATH_t** %ATH89, align 8
  %cb_s = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %106, i32 0, i32 11
  %arraydecay90 = getelementptr inbounds [64 x float], [64 x float]* %cb_s, i32 0, i32 0
  %107 = load float, float* %ath_factor, align 4
  %108 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix91 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %108, i32 0, i32 44
  %109 = load float, float* %msfix91, align 4
  %110 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %npart92 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %110, i32 0, i32 11
  %111 = load i32, i32* %npart92, align 4
  call void @vbrpsy_compute_MS_thresholds([64 x float]* %103, [64 x float]* %arraydecay86, float* %arraydecay88, float* %arraydecay90, float %107, float %109, i32 %111)
  br label %if.end93

if.end93:                                         ; preds = %if.then85, %if.then80
  br label %if.end94

if.end94:                                         ; preds = %if.end93, %for.end77
  store i32 0, i32* %chn, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc109, %if.end94
  %112 = load i32, i32* %chn, align 4
  %113 = load i32, i32* %n_chn_psy, align 4
  %cmp96 = icmp slt i32 %112, %113
  br i1 %cmp96, label %for.body97, label %for.end111

for.body97:                                       ; preds = %for.cond95
  %114 = load i32, i32* %chn, align 4
  %and99 = and i32 %114, 1
  store i32 %and99, i32* %ch0198, align 4
  %115 = load i32, i32* %ch0198, align 4
  %arrayidx100 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 %115
  %116 = load i32, i32* %arrayidx100, align 4
  %tobool101 = icmp ne i32 %116, 0
  br i1 %tobool101, label %lor.lhs.false, label %if.then103

lor.lhs.false:                                    ; preds = %for.body97
  %117 = load i32, i32* %force_short_block_calc, align 4
  %tobool102 = icmp ne i32 %117, 0
  br i1 %tobool102, label %if.then103, label %if.end108

if.then103:                                       ; preds = %lor.lhs.false, %for.body97
  %118 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %119 = load i32, i32* %chn, align 4
  %arrayidx104 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %eb, i32 0, i32 %119
  %arraydecay105 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx104, i32 0, i32 0
  %120 = load i32, i32* %chn, align 4
  %arrayidx106 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %thr, i32 0, i32 %120
  %arraydecay107 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx106, i32 0, i32 0
  %121 = load i32, i32* %chn, align 4
  %122 = load i32, i32* %sblock, align 4
  call void @convert_partition2scalefac_s(%struct.lame_internal_flags* %118, float* %arraydecay105, float* %arraydecay107, i32 %121, i32 %122)
  br label %if.end108

if.end108:                                        ; preds = %if.then103, %lor.lhs.false
  br label %for.inc109

for.inc109:                                       ; preds = %if.end108
  %123 = load i32, i32* %chn, align 4
  %inc110 = add nsw i32 %123, 1
  store i32 %inc110, i32* %chn, align 4
  br label %for.cond95

for.end111:                                       ; preds = %for.cond95
  br label %for.inc112

for.inc112:                                       ; preds = %for.end111
  %124 = load i32, i32* %sblock, align 4
  %inc113 = add nsw i32 %124, 1
  store i32 %inc113, i32* %sblock, align 4
  br label %for.cond55

for.end114:                                       ; preds = %for.cond55
  store i32 0, i32* %chn, align 4
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc231, %for.end114
  %125 = load i32, i32* %chn, align 4
  %126 = load i32, i32* %n_chn_psy, align 4
  %cmp116 = icmp slt i32 %125, %126
  br i1 %cmp116, label %for.body117, label %for.end233

for.body117:                                      ; preds = %for.cond115
  store i32 0, i32* %sb, align 4
  br label %for.cond118

for.cond118:                                      ; preds = %for.inc228, %for.body117
  %127 = load i32, i32* %sb, align 4
  %cmp119 = icmp slt i32 %127, 13
  br i1 %cmp119, label %for.body120, label %for.end230

for.body120:                                      ; preds = %for.cond118
  store i32 0, i32* %sblock, align 4
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc212, %for.body120
  %128 = load i32, i32* %sblock, align 4
  %cmp122 = icmp slt i32 %128, 3
  br i1 %cmp122, label %for.body123, label %for.end214

for.body123:                                      ; preds = %for.cond121
  %129 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm124 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %129, i32 0, i32 4
  %130 = load i32, i32* %chn, align 4
  %arrayidx125 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm124, i32 0, i32 %130
  %s126 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx125, i32 0, i32 1
  %131 = load i32, i32* %sb, align 4
  %arrayidx127 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s126, i32 0, i32 %131
  %132 = load i32, i32* %sblock, align 4
  %arrayidx128 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx127, i32 0, i32 %132
  %133 = load float, float* %arrayidx128, align 4
  store float %133, float* %thmm, align 4
  %134 = load float, float* %thmm, align 4
  %conv = fpext float %134 to double
  %mul129 = fmul double %conv, 8.000000e-01
  %conv130 = fptrunc double %mul129 to float
  store float %conv130, float* %thmm, align 4
  %135 = load float, float* %thmm, align 4
  store float %135, float* %t2, align 4
  store float %135, float* %t1, align 4
  %136 = load i32, i32* %sblock, align 4
  %cmp131 = icmp sgt i32 %136, 0
  br i1 %cmp131, label %if.then133, label %if.else135

if.then133:                                       ; preds = %for.body123
  %137 = load i32, i32* %sblock, align 4
  %sub = sub nsw i32 %137, 1
  %arrayidx134 = getelementptr inbounds [3 x float], [3 x float]* %new_thmm, i32 0, i32 %sub
  %138 = load float, float* %arrayidx134, align 4
  store float %138, float* %prev_thm, align 4
  br label %if.end140

if.else135:                                       ; preds = %for.body123
  %139 = load i32, i32* %chn, align 4
  %arrayidx136 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %last_thm, i32 0, i32 %139
  %s137 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx136, i32 0, i32 1
  %140 = load i32, i32* %sb, align 4
  %arrayidx138 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s137, i32 0, i32 %140
  %arrayidx139 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx138, i32 0, i32 2
  %141 = load float, float* %arrayidx139, align 4
  store float %141, float* %prev_thm, align 4
  br label %if.end140

if.end140:                                        ; preds = %if.else135, %if.then133
  %142 = load i32, i32* %chn, align 4
  %arrayidx141 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 %142
  %143 = load i32, i32* %sblock, align 4
  %arrayidx142 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx141, i32 0, i32 %143
  %144 = load i32, i32* %arrayidx142, align 4
  %cmp143 = icmp sge i32 %144, 2
  br i1 %cmp143, label %if.then151, label %lor.lhs.false145

lor.lhs.false145:                                 ; preds = %if.end140
  %145 = load i32, i32* %chn, align 4
  %arrayidx146 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 %145
  %146 = load i32, i32* %sblock, align 4
  %add147 = add nsw i32 %146, 1
  %arrayidx148 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx146, i32 0, i32 %add147
  %147 = load i32, i32* %arrayidx148, align 4
  %cmp149 = icmp eq i32 %147, 1
  br i1 %cmp149, label %if.then151, label %if.end152

if.then151:                                       ; preds = %lor.lhs.false145, %if.end140
  %148 = load float, float* %prev_thm, align 4
  %149 = load float, float* %thmm, align 4
  %call = call float @NS_INTERP(float %148, float %149, float 0x3FD70A3D80000000)
  store float %call, float* %t1, align 4
  br label %if.end152

if.end152:                                        ; preds = %if.then151, %lor.lhs.false145
  %150 = load float, float* %t1, align 4
  %151 = load float, float* %thmm, align 4
  %cmp153 = fcmp olt float %150, %151
  br i1 %cmp153, label %cond.true155, label %cond.false156

cond.true155:                                     ; preds = %if.end152
  %152 = load float, float* %t1, align 4
  br label %cond.end157

cond.false156:                                    ; preds = %if.end152
  %153 = load float, float* %thmm, align 4
  br label %cond.end157

cond.end157:                                      ; preds = %cond.false156, %cond.true155
  %cond158 = phi float [ %152, %cond.true155 ], [ %153, %cond.false156 ]
  store float %cond158, float* %thmm, align 4
  %154 = load i32, i32* %chn, align 4
  %arrayidx159 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 %154
  %155 = load i32, i32* %sblock, align 4
  %arrayidx160 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx159, i32 0, i32 %155
  %156 = load i32, i32* %arrayidx160, align 4
  %cmp161 = icmp eq i32 %156, 1
  br i1 %cmp161, label %if.then163, label %if.else165

if.then163:                                       ; preds = %cond.end157
  %157 = load float, float* %prev_thm, align 4
  %158 = load float, float* %thmm, align 4
  %call164 = call float @NS_INTERP(float %157, float %158, float 0x3FC70A3D80000000)
  store float %call164, float* %t2, align 4
  br label %if.end195

if.else165:                                       ; preds = %cond.end157
  %159 = load i32, i32* %sblock, align 4
  %cmp166 = icmp eq i32 %159, 0
  br i1 %cmp166, label %land.lhs.true168, label %lor.lhs.false172

land.lhs.true168:                                 ; preds = %if.else165
  %160 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_attacks = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %160, i32 0, i32 9
  %161 = load i32, i32* %chn, align 4
  %arrayidx169 = getelementptr inbounds [4 x i32], [4 x i32]* %last_attacks, i32 0, i32 %161
  %162 = load i32, i32* %arrayidx169, align 4
  %cmp170 = icmp eq i32 %162, 3
  br i1 %cmp170, label %if.then181, label %lor.lhs.false172

lor.lhs.false172:                                 ; preds = %land.lhs.true168, %if.else165
  %163 = load i32, i32* %sblock, align 4
  %cmp173 = icmp sgt i32 %163, 0
  br i1 %cmp173, label %land.lhs.true175, label %if.end194

land.lhs.true175:                                 ; preds = %lor.lhs.false172
  %164 = load i32, i32* %chn, align 4
  %arrayidx176 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 %164
  %165 = load i32, i32* %sblock, align 4
  %sub177 = sub nsw i32 %165, 1
  %arrayidx178 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx176, i32 0, i32 %sub177
  %166 = load i32, i32* %arrayidx178, align 4
  %cmp179 = icmp eq i32 %166, 3
  br i1 %cmp179, label %if.then181, label %if.end194

if.then181:                                       ; preds = %land.lhs.true175, %land.lhs.true168
  %167 = load i32, i32* %sblock, align 4
  switch i32 %167, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb186
    i32 2, label %sw.bb191
  ]

sw.bb:                                            ; preds = %if.then181
  %168 = load i32, i32* %chn, align 4
  %arrayidx182 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %last_thm, i32 0, i32 %168
  %s183 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx182, i32 0, i32 1
  %169 = load i32, i32* %sb, align 4
  %arrayidx184 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s183, i32 0, i32 %169
  %arrayidx185 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx184, i32 0, i32 1
  %170 = load float, float* %arrayidx185, align 4
  store float %170, float* %prev_thm, align 4
  br label %sw.epilog

sw.bb186:                                         ; preds = %if.then181
  %171 = load i32, i32* %chn, align 4
  %arrayidx187 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %last_thm, i32 0, i32 %171
  %s188 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx187, i32 0, i32 1
  %172 = load i32, i32* %sb, align 4
  %arrayidx189 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s188, i32 0, i32 %172
  %arrayidx190 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx189, i32 0, i32 2
  %173 = load float, float* %arrayidx190, align 4
  store float %173, float* %prev_thm, align 4
  br label %sw.epilog

sw.bb191:                                         ; preds = %if.then181
  %arrayidx192 = getelementptr inbounds [3 x float], [3 x float]* %new_thmm, i32 0, i32 0
  %174 = load float, float* %arrayidx192, align 4
  store float %174, float* %prev_thm, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.then181, %sw.bb191, %sw.bb186, %sw.bb
  %175 = load float, float* %prev_thm, align 4
  %176 = load float, float* %thmm, align 4
  %call193 = call float @NS_INTERP(float %175, float %176, float 0x3FC70A3D80000000)
  store float %call193, float* %t2, align 4
  br label %if.end194

if.end194:                                        ; preds = %sw.epilog, %land.lhs.true175, %lor.lhs.false172
  br label %if.end195

if.end195:                                        ; preds = %if.end194, %if.then163
  %177 = load float, float* %t1, align 4
  %178 = load float, float* %thmm, align 4
  %cmp196 = fcmp olt float %177, %178
  br i1 %cmp196, label %cond.true198, label %cond.false199

cond.true198:                                     ; preds = %if.end195
  %179 = load float, float* %t1, align 4
  br label %cond.end200

cond.false199:                                    ; preds = %if.end195
  %180 = load float, float* %thmm, align 4
  br label %cond.end200

cond.end200:                                      ; preds = %cond.false199, %cond.true198
  %cond201 = phi float [ %179, %cond.true198 ], [ %180, %cond.false199 ]
  store float %cond201, float* %thmm, align 4
  %181 = load float, float* %t2, align 4
  %182 = load float, float* %thmm, align 4
  %cmp202 = fcmp olt float %181, %182
  br i1 %cmp202, label %cond.true204, label %cond.false205

cond.true204:                                     ; preds = %cond.end200
  %183 = load float, float* %t2, align 4
  br label %cond.end206

cond.false205:                                    ; preds = %cond.end200
  %184 = load float, float* %thmm, align 4
  br label %cond.end206

cond.end206:                                      ; preds = %cond.false205, %cond.true204
  %cond207 = phi float [ %183, %cond.true204 ], [ %184, %cond.false205 ]
  store float %cond207, float* %thmm, align 4
  %185 = load i32, i32* %chn, align 4
  %arrayidx208 = getelementptr inbounds [4 x [3 x float]], [4 x [3 x float]]* %sub_short_factor, i32 0, i32 %185
  %186 = load i32, i32* %sblock, align 4
  %arrayidx209 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx208, i32 0, i32 %186
  %187 = load float, float* %arrayidx209, align 4
  %188 = load float, float* %thmm, align 4
  %mul210 = fmul float %188, %187
  store float %mul210, float* %thmm, align 4
  %189 = load float, float* %thmm, align 4
  %190 = load i32, i32* %sblock, align 4
  %arrayidx211 = getelementptr inbounds [3 x float], [3 x float]* %new_thmm, i32 0, i32 %190
  store float %189, float* %arrayidx211, align 4
  br label %for.inc212

for.inc212:                                       ; preds = %cond.end206
  %191 = load i32, i32* %sblock, align 4
  %inc213 = add nsw i32 %191, 1
  store i32 %inc213, i32* %sblock, align 4
  br label %for.cond121

for.end214:                                       ; preds = %for.cond121
  store i32 0, i32* %sblock, align 4
  br label %for.cond215

for.cond215:                                      ; preds = %for.inc225, %for.end214
  %192 = load i32, i32* %sblock, align 4
  %cmp216 = icmp slt i32 %192, 3
  br i1 %cmp216, label %for.body218, label %for.end227

for.body218:                                      ; preds = %for.cond215
  %193 = load i32, i32* %sblock, align 4
  %arrayidx219 = getelementptr inbounds [3 x float], [3 x float]* %new_thmm, i32 0, i32 %193
  %194 = load float, float* %arrayidx219, align 4
  %195 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm220 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %195, i32 0, i32 4
  %196 = load i32, i32* %chn, align 4
  %arrayidx221 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm220, i32 0, i32 %196
  %s222 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx221, i32 0, i32 1
  %197 = load i32, i32* %sb, align 4
  %arrayidx223 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s222, i32 0, i32 %197
  %198 = load i32, i32* %sblock, align 4
  %arrayidx224 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx223, i32 0, i32 %198
  store float %194, float* %arrayidx224, align 4
  br label %for.inc225

for.inc225:                                       ; preds = %for.body218
  %199 = load i32, i32* %sblock, align 4
  %inc226 = add nsw i32 %199, 1
  store i32 %inc226, i32* %sblock, align 4
  br label %for.cond215

for.end227:                                       ; preds = %for.cond215
  br label %for.inc228

for.inc228:                                       ; preds = %for.end227
  %200 = load i32, i32* %sb, align 4
  %inc229 = add nsw i32 %200, 1
  store i32 %inc229, i32* %sb, align 4
  br label %for.cond118

for.end230:                                       ; preds = %for.cond118
  br label %for.inc231

for.inc231:                                       ; preds = %for.end230
  %201 = load i32, i32* %chn, align 4
  %inc232 = add nsw i32 %201, 1
  store i32 %inc232, i32* %chn, align 4
  br label %for.cond115

for.end233:                                       ; preds = %for.cond115
  store i32 0, i32* %chn, align 4
  br label %for.cond234

for.cond234:                                      ; preds = %for.inc242, %for.end233
  %202 = load i32, i32* %chn, align 4
  %203 = load i32, i32* %n_chn_psy, align 4
  %cmp235 = icmp slt i32 %202, %203
  br i1 %cmp235, label %for.body237, label %for.end244

for.body237:                                      ; preds = %for.cond234
  %204 = load i32, i32* %chn, align 4
  %arrayidx238 = getelementptr inbounds [4 x [4 x i32]], [4 x [4 x i32]]* %ns_attacks, i32 0, i32 %204
  %arrayidx239 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx238, i32 0, i32 2
  %205 = load i32, i32* %arrayidx239, align 8
  %206 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_attacks240 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %206, i32 0, i32 9
  %207 = load i32, i32* %chn, align 4
  %arrayidx241 = getelementptr inbounds [4 x i32], [4 x i32]* %last_attacks240, i32 0, i32 %207
  store i32 %205, i32* %arrayidx241, align 4
  br label %for.inc242

for.inc242:                                       ; preds = %for.body237
  %208 = load i32, i32* %chn, align 4
  %inc243 = add nsw i32 %208, 1
  store i32 %inc243, i32* %chn, align 4
  br label %for.cond234

for.end244:                                       ; preds = %for.cond234
  %209 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %210 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out245 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %210, i32 0, i32 14
  %211 = load i32, i32* %channels_out245, align 4
  %arraydecay246 = getelementptr inbounds [2 x i32], [2 x i32]* %uselongblock, i32 0, i32 0
  %212 = load i32*, i32** %blocktype_d.addr, align 4
  call void @vbrpsy_apply_block_type(%struct.PsyStateVar_t* %209, i32 %211, i32* %arraydecay246, i32* %212)
  store i32 0, i32* %chn, align 4
  br label %for.cond247

for.cond247:                                      ; preds = %for.inc290, %for.end244
  %213 = load i32, i32* %chn, align 4
  %214 = load i32, i32* %n_chn_psy, align 4
  %cmp248 = icmp slt i32 %213, %214
  br i1 %cmp248, label %for.body250, label %for.end292

for.body250:                                      ; preds = %for.cond247
  %215 = load i32, i32* %chn, align 4
  %cmp251 = icmp sgt i32 %215, 1
  br i1 %cmp251, label %if.then253, label %if.else267

if.then253:                                       ; preds = %for.body250
  %216 = load float*, float** %percep_MS_entropy.addr, align 4
  %add.ptr254 = getelementptr inbounds float, float* %216, i32 -2
  store float* %add.ptr254, float** %ppe, align 4
  store i32 0, i32* %type, align 4
  %217 = load i32*, i32** %blocktype_d.addr, align 4
  %arrayidx255 = getelementptr inbounds i32, i32* %217, i32 0
  %218 = load i32, i32* %arrayidx255, align 4
  %cmp256 = icmp eq i32 %218, 2
  br i1 %cmp256, label %if.then262, label %lor.lhs.false258

lor.lhs.false258:                                 ; preds = %if.then253
  %219 = load i32*, i32** %blocktype_d.addr, align 4
  %arrayidx259 = getelementptr inbounds i32, i32* %219, i32 1
  %220 = load i32, i32* %arrayidx259, align 4
  %cmp260 = icmp eq i32 %220, 2
  br i1 %cmp260, label %if.then262, label %if.end263

if.then262:                                       ; preds = %lor.lhs.false258, %if.then253
  store i32 2, i32* %type, align 4
  br label %if.end263

if.end263:                                        ; preds = %if.then262, %lor.lhs.false258
  %221 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  %222 = load i32, i32* %gr_out.addr, align 4
  %arrayidx264 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %221, i32 %222
  %223 = load i32, i32* %chn, align 4
  %sub265 = sub nsw i32 %223, 2
  %arrayidx266 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx264, i32 0, i32 %sub265
  store %struct.III_psy_ratio* %arrayidx266, %struct.III_psy_ratio** %mr, align 4
  br label %if.end271

if.else267:                                       ; preds = %for.body250
  %224 = load float*, float** %percep_entropy.addr, align 4
  store float* %224, float** %ppe, align 4
  %225 = load i32*, i32** %blocktype_d.addr, align 4
  %226 = load i32, i32* %chn, align 4
  %arrayidx268 = getelementptr inbounds i32, i32* %225, i32 %226
  %227 = load i32, i32* %arrayidx268, align 4
  store i32 %227, i32* %type, align 4
  %228 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  %229 = load i32, i32* %gr_out.addr, align 4
  %arrayidx269 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %228, i32 %229
  %230 = load i32, i32* %chn, align 4
  %arrayidx270 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx269, i32 0, i32 %230
  store %struct.III_psy_ratio* %arrayidx270, %struct.III_psy_ratio** %mr, align 4
  br label %if.end271

if.end271:                                        ; preds = %if.else267, %if.end263
  %231 = load i32, i32* %type, align 4
  %cmp272 = icmp eq i32 %231, 2
  br i1 %cmp272, label %if.then274, label %if.else277

if.then274:                                       ; preds = %if.end271
  %232 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr, align 4
  %233 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %233, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 2
  %234 = load float, float* %masking_lower, align 4
  %call275 = call float @pecalc_s(%struct.III_psy_ratio* %232, float %234)
  %235 = load float*, float** %ppe, align 4
  %236 = load i32, i32* %chn, align 4
  %arrayidx276 = getelementptr inbounds float, float* %235, i32 %236
  store float %call275, float* %arrayidx276, align 4
  br label %if.end282

if.else277:                                       ; preds = %if.end271
  %237 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr, align 4
  %238 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt278 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %238, i32 0, i32 13
  %masking_lower279 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt278, i32 0, i32 2
  %239 = load float, float* %masking_lower279, align 4
  %call280 = call float @pecalc_l(%struct.III_psy_ratio* %237, float %239)
  %240 = load float*, float** %ppe, align 4
  %241 = load i32, i32* %chn, align 4
  %arrayidx281 = getelementptr inbounds float, float* %240, i32 %241
  store float %call280, float* %arrayidx281, align 4
  br label %if.end282

if.end282:                                        ; preds = %if.else277, %if.then274
  %242 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %tobool283 = icmp ne %struct.plotting_data* %242, null
  br i1 %tobool283, label %if.then284, label %if.end289

if.then284:                                       ; preds = %if.end282
  %243 = load float*, float** %ppe, align 4
  %244 = load i32, i32* %chn, align 4
  %arrayidx285 = getelementptr inbounds float, float* %243, i32 %244
  %245 = load float, float* %arrayidx285, align 4
  %conv286 = fpext float %245 to double
  %246 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %pe = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %246, i32 0, i32 12
  %247 = load i32, i32* %gr_out.addr, align 4
  %arrayidx287 = getelementptr inbounds [2 x [4 x double]], [2 x [4 x double]]* %pe, i32 0, i32 %247
  %248 = load i32, i32* %chn, align 4
  %arrayidx288 = getelementptr inbounds [4 x double], [4 x double]* %arrayidx287, i32 0, i32 %248
  store double %conv286, double* %arrayidx288, align 8
  br label %if.end289

if.end289:                                        ; preds = %if.then284, %if.end282
  br label %for.inc290

for.inc290:                                       ; preds = %if.end289
  %249 = load i32, i32* %chn, align 4
  %inc291 = add nsw i32 %249, 1
  store i32 %inc291, i32* %chn, align 4
  br label %for.cond247

for.end292:                                       ; preds = %for.cond247
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_attack_detection(%struct.lame_internal_flags* %gfc, float** %buffer, i32 %gr_out, [2 x %struct.III_psy_ratio]* %masking_ratio, [2 x %struct.III_psy_ratio]* %masking_MS_ratio, float* %energy, [3 x float]* %sub_short_factor, [4 x i32]* %ns_attacks, i32* %uselongblock) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca float**, align 4
  %gr_out.addr = alloca i32, align 4
  %masking_ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %masking_MS_ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %energy.addr = alloca float*, align 4
  %sub_short_factor.addr = alloca [3 x float]*, align 4
  %ns_attacks.addr = alloca [4 x i32]*, align 4
  %uselongblock.addr = alloca i32*, align 4
  %ns_hpfsmpl = alloca [2 x [576 x float]], align 16
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %plt = alloca %struct.plotting_data*, align 4
  %n_chn_out = alloca i32, align 4
  %n_chn_psy = alloca i32, align 4
  %chn = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %firbuf = alloca float*, align 4
  %sum1 = alloca float, align 4
  %sum2 = alloca float, align 4
  %attack_intensity = alloca [12 x float], align 16
  %en_subshort = alloca [12 x float], align 16
  %en_short = alloca [4 x float], align 16
  %pf = alloca float*, align 4
  %ns_uselongblock = alloca i32, align 4
  %l = alloca float, align 4
  %r = alloca float, align 4
  %pfe = alloca float*, align 4
  %p = alloca float, align 4
  %enn = alloca float, align 4
  %factor = alloca float, align 4
  %x = alloca float, align 4
  %x223 = alloca float, align 4
  %u = alloca float, align 4
  %v = alloca float, align 4
  %m = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 %gr_out, i32* %gr_out.addr, align 4
  store [2 x %struct.III_psy_ratio]* %masking_ratio, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %masking_MS_ratio, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  store float* %energy, float** %energy.addr, align 4
  store [3 x float]* %sub_short_factor, [3 x float]** %sub_short_factor.addr, align 4
  store [4 x i32]* %ns_attacks, [4 x i32]** %ns_attacks.addr, align 4
  store i32* %uselongblock, i32** %uselongblock.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 31
  %3 = load i32, i32* %analysis, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 23
  %5 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.plotting_data* [ %5, %cond.true ], [ null, %cond.false ]
  store %struct.plotting_data* %cond, %struct.plotting_data** %plt, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 14
  %7 = load i32, i32* %channels_out, align 4
  store i32 %7, i32* %n_chn_out, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 41
  %9 = load i32, i32* %mode, align 4
  %cmp = icmp eq i32 %9, 1
  br i1 %cmp, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  br label %cond.end4

cond.false3:                                      ; preds = %cond.end
  %10 = load i32, i32* %n_chn_out, align 4
  br label %cond.end4

cond.end4:                                        ; preds = %cond.false3, %cond.true2
  %cond5 = phi i32 [ 4, %cond.true2 ], [ %10, %cond.false3 ]
  store i32 %cond5, i32* %n_chn_psy, align 4
  %arrayidx = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx, i32 0, i32 0
  %11 = bitcast float* %arrayidx6 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %11, i8 0, i32 4608, i1 false)
  store i32 0, i32* %chn, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc63, %cond.end4
  %12 = load i32, i32* %chn, align 4
  %13 = load i32, i32* %n_chn_out, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body, label %for.end65

for.body:                                         ; preds = %for.cond
  %14 = load float**, float*** %buffer.addr, align 4
  %15 = load i32, i32* %chn, align 4
  %arrayidx8 = getelementptr inbounds float*, float** %14, i32 %15
  %16 = load float*, float** %arrayidx8, align 4
  %arrayidx9 = getelementptr inbounds float, float* %16, i32 397
  store float* %arrayidx9, float** %firbuf, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc40, %for.body
  %17 = load i32, i32* %i, align 4
  %cmp11 = icmp slt i32 %17, 576
  br i1 %cmp11, label %for.body12, label %for.end41

for.body12:                                       ; preds = %for.cond10
  %18 = load float*, float** %firbuf, align 4
  %19 = load i32, i32* %i, align 4
  %add = add nsw i32 %19, 10
  %arrayidx13 = getelementptr inbounds float, float* %18, i32 %add
  %20 = load float, float* %arrayidx13, align 4
  store float %20, float* %sum1, align 4
  store float 0.000000e+00, float* %sum2, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body12
  %21 = load i32, i32* %j, align 4
  %cmp15 = icmp slt i32 %21, 9
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %22 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds [10 x float], [10 x float]* @vbrpsy_attack_detection.fircoef, i32 0, i32 %22
  %23 = load float, float* %arrayidx17, align 4
  %24 = load float*, float** %firbuf, align 4
  %25 = load i32, i32* %i, align 4
  %26 = load i32, i32* %j, align 4
  %add18 = add nsw i32 %25, %26
  %arrayidx19 = getelementptr inbounds float, float* %24, i32 %add18
  %27 = load float, float* %arrayidx19, align 4
  %28 = load float*, float** %firbuf, align 4
  %29 = load i32, i32* %i, align 4
  %add20 = add nsw i32 %29, 21
  %30 = load i32, i32* %j, align 4
  %sub = sub nsw i32 %add20, %30
  %arrayidx21 = getelementptr inbounds float, float* %28, i32 %sub
  %31 = load float, float* %arrayidx21, align 4
  %add22 = fadd float %27, %31
  %mul = fmul float %23, %add22
  %32 = load float, float* %sum1, align 4
  %add23 = fadd float %32, %mul
  store float %add23, float* %sum1, align 4
  %33 = load i32, i32* %j, align 4
  %add24 = add nsw i32 %33, 1
  %arrayidx25 = getelementptr inbounds [10 x float], [10 x float]* @vbrpsy_attack_detection.fircoef, i32 0, i32 %add24
  %34 = load float, float* %arrayidx25, align 4
  %35 = load float*, float** %firbuf, align 4
  %36 = load i32, i32* %i, align 4
  %37 = load i32, i32* %j, align 4
  %add26 = add nsw i32 %36, %37
  %add27 = add nsw i32 %add26, 1
  %arrayidx28 = getelementptr inbounds float, float* %35, i32 %add27
  %38 = load float, float* %arrayidx28, align 4
  %39 = load float*, float** %firbuf, align 4
  %40 = load i32, i32* %i, align 4
  %add29 = add nsw i32 %40, 21
  %41 = load i32, i32* %j, align 4
  %sub30 = sub nsw i32 %add29, %41
  %sub31 = sub nsw i32 %sub30, 1
  %arrayidx32 = getelementptr inbounds float, float* %39, i32 %sub31
  %42 = load float, float* %arrayidx32, align 4
  %add33 = fadd float %38, %42
  %mul34 = fmul float %34, %add33
  %43 = load float, float* %sum2, align 4
  %add35 = fadd float %43, %mul34
  store float %add35, float* %sum2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body16
  %44 = load i32, i32* %j, align 4
  %add36 = add nsw i32 %44, 2
  store i32 %add36, i32* %j, align 4
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  %45 = load float, float* %sum1, align 4
  %46 = load float, float* %sum2, align 4
  %add37 = fadd float %45, %46
  %47 = load i32, i32* %chn, align 4
  %arrayidx38 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 %47
  %48 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx38, i32 0, i32 %48
  store float %add37, float* %arrayidx39, align 4
  br label %for.inc40

for.inc40:                                        ; preds = %for.end
  %49 = load i32, i32* %i, align 4
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond10

for.end41:                                        ; preds = %for.cond10
  %50 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  %51 = load i32, i32* %gr_out.addr, align 4
  %arrayidx42 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %50, i32 %51
  %52 = load i32, i32* %chn, align 4
  %arrayidx43 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx42, i32 0, i32 %52
  %en = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %arrayidx43, i32 0, i32 1
  %53 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en44 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %53, i32 0, i32 5
  %54 = load i32, i32* %chn, align 4
  %arrayidx45 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en44, i32 0, i32 %54
  %55 = bitcast %struct.III_psy_xmin* %en to i8*
  %56 = bitcast %struct.III_psy_xmin* %arrayidx45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 244, i1 false)
  %57 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_ratio.addr, align 4
  %58 = load i32, i32* %gr_out.addr, align 4
  %arrayidx46 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %57, i32 %58
  %59 = load i32, i32* %chn, align 4
  %arrayidx47 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx46, i32 0, i32 %59
  %thm = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %arrayidx47, i32 0, i32 0
  %60 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm48 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %60, i32 0, i32 4
  %61 = load i32, i32* %chn, align 4
  %arrayidx49 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm48, i32 0, i32 %61
  %62 = bitcast %struct.III_psy_xmin* %thm to i8*
  %63 = bitcast %struct.III_psy_xmin* %arrayidx49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 244, i1 false)
  %64 = load i32, i32* %n_chn_psy, align 4
  %cmp50 = icmp sgt i32 %64, 2
  br i1 %cmp50, label %if.then, label %if.end

if.then:                                          ; preds = %for.end41
  %65 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  %66 = load i32, i32* %gr_out.addr, align 4
  %arrayidx51 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %65, i32 %66
  %67 = load i32, i32* %chn, align 4
  %arrayidx52 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx51, i32 0, i32 %67
  %en53 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %arrayidx52, i32 0, i32 1
  %68 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en54 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %68, i32 0, i32 5
  %69 = load i32, i32* %chn, align 4
  %add55 = add nsw i32 %69, 2
  %arrayidx56 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en54, i32 0, i32 %add55
  %70 = bitcast %struct.III_psy_xmin* %en53 to i8*
  %71 = bitcast %struct.III_psy_xmin* %arrayidx56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 244, i1 false)
  %72 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking_MS_ratio.addr, align 4
  %73 = load i32, i32* %gr_out.addr, align 4
  %arrayidx57 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %72, i32 %73
  %74 = load i32, i32* %chn, align 4
  %arrayidx58 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx57, i32 0, i32 %74
  %thm59 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %arrayidx58, i32 0, i32 0
  %75 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm60 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %75, i32 0, i32 4
  %76 = load i32, i32* %chn, align 4
  %add61 = add nsw i32 %76, 2
  %arrayidx62 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm60, i32 0, i32 %add61
  %77 = bitcast %struct.III_psy_xmin* %thm59 to i8*
  %78 = bitcast %struct.III_psy_xmin* %arrayidx62 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %77, i8* align 4 %78, i32 244, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end41
  br label %for.inc63

for.inc63:                                        ; preds = %if.end
  %79 = load i32, i32* %chn, align 4
  %inc64 = add nsw i32 %79, 1
  store i32 %inc64, i32* %chn, align 4
  br label %for.cond

for.end65:                                        ; preds = %for.cond
  store i32 0, i32* %chn, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc365, %for.end65
  %80 = load i32, i32* %chn, align 4
  %81 = load i32, i32* %n_chn_psy, align 4
  %cmp67 = icmp slt i32 %80, %81
  br i1 %cmp67, label %for.body68, label %for.end367

for.body68:                                       ; preds = %for.cond66
  %82 = bitcast [4 x float]* %en_short to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %82, i8 0, i32 16, i1 false)
  %83 = load i32, i32* %chn, align 4
  %and = and i32 %83, 1
  %arrayidx69 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 %and
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %arrayidx69, i32 0, i32 0
  store float* %arraydecay, float** %pf, align 4
  store i32 1, i32* %ns_uselongblock, align 4
  %84 = load i32, i32* %chn, align 4
  %cmp70 = icmp eq i32 %84, 2
  br i1 %cmp70, label %if.then71, label %if.end88

if.then71:                                        ; preds = %for.body68
  store i32 0, i32* %i, align 4
  store i32 576, i32* %j, align 4
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc85, %if.then71
  %85 = load i32, i32* %j, align 4
  %cmp73 = icmp sgt i32 %85, 0
  br i1 %cmp73, label %for.body74, label %for.end87

for.body74:                                       ; preds = %for.cond72
  %arrayidx75 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 0
  %86 = load i32, i32* %i, align 4
  %arrayidx76 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx75, i32 0, i32 %86
  %87 = load float, float* %arrayidx76, align 4
  store float %87, float* %l, align 4
  %arrayidx77 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 1
  %88 = load i32, i32* %i, align 4
  %arrayidx78 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx77, i32 0, i32 %88
  %89 = load float, float* %arrayidx78, align 4
  store float %89, float* %r, align 4
  %90 = load float, float* %l, align 4
  %91 = load float, float* %r, align 4
  %add79 = fadd float %90, %91
  %arrayidx80 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 0
  %92 = load i32, i32* %i, align 4
  %arrayidx81 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx80, i32 0, i32 %92
  store float %add79, float* %arrayidx81, align 4
  %93 = load float, float* %l, align 4
  %94 = load float, float* %r, align 4
  %sub82 = fsub float %93, %94
  %arrayidx83 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %ns_hpfsmpl, i32 0, i32 1
  %95 = load i32, i32* %i, align 4
  %arrayidx84 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx83, i32 0, i32 %95
  store float %sub82, float* %arrayidx84, align 4
  br label %for.inc85

for.inc85:                                        ; preds = %for.body74
  %96 = load i32, i32* %i, align 4
  %inc86 = add nsw i32 %96, 1
  store i32 %inc86, i32* %i, align 4
  %97 = load i32, i32* %j, align 4
  %dec = add nsw i32 %97, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond72

for.end87:                                        ; preds = %for.cond72
  br label %if.end88

if.end88:                                         ; preds = %for.end87, %for.body68
  store i32 0, i32* %i, align 4
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc105, %if.end88
  %98 = load i32, i32* %i, align 4
  %cmp90 = icmp slt i32 %98, 3
  br i1 %cmp90, label %for.body91, label %for.end107

for.body91:                                       ; preds = %for.cond89
  %99 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_en_subshort = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %99, i32 0, i32 8
  %100 = load i32, i32* %chn, align 4
  %arrayidx92 = getelementptr inbounds [4 x [9 x float]], [4 x [9 x float]]* %last_en_subshort, i32 0, i32 %100
  %101 = load i32, i32* %i, align 4
  %add93 = add nsw i32 %101, 6
  %arrayidx94 = getelementptr inbounds [9 x float], [9 x float]* %arrayidx92, i32 0, i32 %add93
  %102 = load float, float* %arrayidx94, align 4
  %103 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %103
  store float %102, float* %arrayidx95, align 4
  %104 = load i32, i32* %i, align 4
  %arrayidx96 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %104
  %105 = load float, float* %arrayidx96, align 4
  %106 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_en_subshort97 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %106, i32 0, i32 8
  %107 = load i32, i32* %chn, align 4
  %arrayidx98 = getelementptr inbounds [4 x [9 x float]], [4 x [9 x float]]* %last_en_subshort97, i32 0, i32 %107
  %108 = load i32, i32* %i, align 4
  %add99 = add nsw i32 %108, 4
  %arrayidx100 = getelementptr inbounds [9 x float], [9 x float]* %arrayidx98, i32 0, i32 %add99
  %109 = load float, float* %arrayidx100, align 4
  %div = fdiv float %105, %109
  %110 = load i32, i32* %i, align 4
  %arrayidx101 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 %110
  store float %div, float* %arrayidx101, align 4
  %111 = load i32, i32* %i, align 4
  %arrayidx102 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %111
  %112 = load float, float* %arrayidx102, align 4
  %arrayidx103 = getelementptr inbounds [4 x float], [4 x float]* %en_short, i32 0, i32 0
  %113 = load float, float* %arrayidx103, align 16
  %add104 = fadd float %113, %112
  store float %add104, float* %arrayidx103, align 16
  br label %for.inc105

for.inc105:                                       ; preds = %for.body91
  %114 = load i32, i32* %i, align 4
  %inc106 = add nsw i32 %114, 1
  store i32 %inc106, i32* %i, align 4
  br label %for.cond89

for.end107:                                       ; preds = %for.cond89
  store i32 0, i32* %i, align 4
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc159, %for.end107
  %115 = load i32, i32* %i, align 4
  %cmp109 = icmp slt i32 %115, 9
  br i1 %cmp109, label %for.body110, label %for.end161

for.body110:                                      ; preds = %for.cond108
  %116 = load float*, float** %pf, align 4
  %add.ptr = getelementptr inbounds float, float* %116, i32 64
  store float* %add.ptr, float** %pfe, align 4
  store float 1.000000e+00, float* %p, align 4
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc121, %for.body110
  %117 = load float*, float** %pf, align 4
  %118 = load float*, float** %pfe, align 4
  %cmp112 = icmp ult float* %117, %118
  br i1 %cmp112, label %for.body113, label %for.end122

for.body113:                                      ; preds = %for.cond111
  %119 = load float, float* %p, align 4
  %conv = fpext float %119 to double
  %120 = load float*, float** %pf, align 4
  %121 = load float, float* %120, align 4
  %conv114 = fpext float %121 to double
  %122 = call double @llvm.fabs.f64(double %conv114)
  %cmp115 = fcmp olt double %conv, %122
  br i1 %cmp115, label %if.then117, label %if.end120

if.then117:                                       ; preds = %for.body113
  %123 = load float*, float** %pf, align 4
  %124 = load float, float* %123, align 4
  %conv118 = fpext float %124 to double
  %125 = call double @llvm.fabs.f64(double %conv118)
  %conv119 = fptrunc double %125 to float
  store float %conv119, float* %p, align 4
  br label %if.end120

if.end120:                                        ; preds = %if.then117, %for.body113
  br label %for.inc121

for.inc121:                                       ; preds = %if.end120
  %126 = load float*, float** %pf, align 4
  %incdec.ptr = getelementptr inbounds float, float* %126, i32 1
  store float* %incdec.ptr, float** %pf, align 4
  br label %for.cond111

for.end122:                                       ; preds = %for.cond111
  %127 = load float, float* %p, align 4
  %128 = load i32, i32* %i, align 4
  %add123 = add nsw i32 %128, 3
  %arrayidx124 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add123
  store float %127, float* %arrayidx124, align 4
  %129 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_en_subshort125 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %129, i32 0, i32 8
  %130 = load i32, i32* %chn, align 4
  %arrayidx126 = getelementptr inbounds [4 x [9 x float]], [4 x [9 x float]]* %last_en_subshort125, i32 0, i32 %130
  %131 = load i32, i32* %i, align 4
  %arrayidx127 = getelementptr inbounds [9 x float], [9 x float]* %arrayidx126, i32 0, i32 %131
  store float %127, float* %arrayidx127, align 4
  %132 = load float, float* %p, align 4
  %133 = load i32, i32* %i, align 4
  %div128 = sdiv i32 %133, 3
  %add129 = add nsw i32 1, %div128
  %arrayidx130 = getelementptr inbounds [4 x float], [4 x float]* %en_short, i32 0, i32 %add129
  %134 = load float, float* %arrayidx130, align 4
  %add131 = fadd float %134, %132
  store float %add131, float* %arrayidx130, align 4
  %135 = load float, float* %p, align 4
  %136 = load i32, i32* %i, align 4
  %add132 = add nsw i32 %136, 3
  %sub133 = sub nsw i32 %add132, 2
  %arrayidx134 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %sub133
  %137 = load float, float* %arrayidx134, align 4
  %cmp135 = fcmp ogt float %135, %137
  br i1 %cmp135, label %if.then137, label %if.else

if.then137:                                       ; preds = %for.end122
  %138 = load float, float* %p, align 4
  %139 = load i32, i32* %i, align 4
  %add138 = add nsw i32 %139, 3
  %sub139 = sub nsw i32 %add138, 2
  %arrayidx140 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %sub139
  %140 = load float, float* %arrayidx140, align 4
  %div141 = fdiv float %138, %140
  store float %div141, float* %p, align 4
  br label %if.end156

if.else:                                          ; preds = %for.end122
  %141 = load i32, i32* %i, align 4
  %add142 = add nsw i32 %141, 3
  %sub143 = sub nsw i32 %add142, 2
  %arrayidx144 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %sub143
  %142 = load float, float* %arrayidx144, align 4
  %143 = load float, float* %p, align 4
  %mul145 = fmul float %143, 1.000000e+01
  %cmp146 = fcmp ogt float %142, %mul145
  br i1 %cmp146, label %if.then148, label %if.else154

if.then148:                                       ; preds = %if.else
  %144 = load i32, i32* %i, align 4
  %add149 = add nsw i32 %144, 3
  %sub150 = sub nsw i32 %add149, 2
  %arrayidx151 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %sub150
  %145 = load float, float* %arrayidx151, align 4
  %146 = load float, float* %p, align 4
  %mul152 = fmul float %146, 1.000000e+01
  %div153 = fdiv float %145, %mul152
  store float %div153, float* %p, align 4
  br label %if.end155

if.else154:                                       ; preds = %if.else
  store float 0.000000e+00, float* %p, align 4
  br label %if.end155

if.end155:                                        ; preds = %if.else154, %if.then148
  br label %if.end156

if.end156:                                        ; preds = %if.end155, %if.then137
  %147 = load float, float* %p, align 4
  %148 = load i32, i32* %i, align 4
  %add157 = add nsw i32 %148, 3
  %arrayidx158 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 %add157
  store float %147, float* %arrayidx158, align 4
  br label %for.inc159

for.inc159:                                       ; preds = %if.end156
  %149 = load i32, i32* %i, align 4
  %inc160 = add nsw i32 %149, 1
  store i32 %inc160, i32* %i, align 4
  br label %for.cond108

for.end161:                                       ; preds = %for.cond108
  store i32 0, i32* %i, align 4
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc197, %for.end161
  %150 = load i32, i32* %i, align 4
  %cmp163 = icmp slt i32 %150, 3
  br i1 %cmp163, label %for.body165, label %for.end199

for.body165:                                      ; preds = %for.cond162
  %151 = load i32, i32* %i, align 4
  %mul166 = mul nsw i32 %151, 3
  %add167 = add nsw i32 %mul166, 3
  %arrayidx168 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add167
  %152 = load float, float* %arrayidx168, align 4
  %153 = load i32, i32* %i, align 4
  %mul169 = mul nsw i32 %153, 3
  %add170 = add nsw i32 %mul169, 4
  %arrayidx171 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add170
  %154 = load float, float* %arrayidx171, align 4
  %add172 = fadd float %152, %154
  %155 = load i32, i32* %i, align 4
  %mul173 = mul nsw i32 %155, 3
  %add174 = add nsw i32 %mul173, 5
  %arrayidx175 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add174
  %156 = load float, float* %arrayidx175, align 4
  %add176 = fadd float %add172, %156
  store float %add176, float* %enn, align 4
  store float 1.000000e+00, float* %factor, align 4
  %157 = load i32, i32* %i, align 4
  %mul177 = mul nsw i32 %157, 3
  %add178 = add nsw i32 %mul177, 5
  %arrayidx179 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add178
  %158 = load float, float* %arrayidx179, align 4
  %mul180 = fmul float %158, 6.000000e+00
  %159 = load float, float* %enn, align 4
  %cmp181 = fcmp olt float %mul180, %159
  br i1 %cmp181, label %if.then183, label %if.end194

if.then183:                                       ; preds = %for.body165
  %160 = load float, float* %factor, align 4
  %mul184 = fmul float %160, 5.000000e-01
  store float %mul184, float* %factor, align 4
  %161 = load i32, i32* %i, align 4
  %mul185 = mul nsw i32 %161, 3
  %add186 = add nsw i32 %mul185, 4
  %arrayidx187 = getelementptr inbounds [12 x float], [12 x float]* %en_subshort, i32 0, i32 %add186
  %162 = load float, float* %arrayidx187, align 4
  %mul188 = fmul float %162, 6.000000e+00
  %163 = load float, float* %enn, align 4
  %cmp189 = fcmp olt float %mul188, %163
  br i1 %cmp189, label %if.then191, label %if.end193

if.then191:                                       ; preds = %if.then183
  %164 = load float, float* %factor, align 4
  %mul192 = fmul float %164, 5.000000e-01
  store float %mul192, float* %factor, align 4
  br label %if.end193

if.end193:                                        ; preds = %if.then191, %if.then183
  br label %if.end194

if.end194:                                        ; preds = %if.end193, %for.body165
  %165 = load float, float* %factor, align 4
  %166 = load [3 x float]*, [3 x float]** %sub_short_factor.addr, align 4
  %167 = load i32, i32* %chn, align 4
  %arrayidx195 = getelementptr inbounds [3 x float], [3 x float]* %166, i32 %167
  %168 = load i32, i32* %i, align 4
  %arrayidx196 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx195, i32 0, i32 %168
  store float %165, float* %arrayidx196, align 4
  br label %for.inc197

for.inc197:                                       ; preds = %if.end194
  %169 = load i32, i32* %i, align 4
  %inc198 = add nsw i32 %169, 1
  store i32 %inc198, i32* %i, align 4
  br label %for.cond162

for.end199:                                       ; preds = %for.cond162
  %170 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %tobool200 = icmp ne %struct.plotting_data* %170, null
  br i1 %tobool200, label %if.then201, label %if.end222

if.then201:                                       ; preds = %for.end199
  %arrayidx202 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 0
  %171 = load float, float* %arrayidx202, align 16
  store float %171, float* %x, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond203

for.cond203:                                      ; preds = %for.inc213, %if.then201
  %172 = load i32, i32* %i, align 4
  %cmp204 = icmp slt i32 %172, 12
  br i1 %cmp204, label %for.body206, label %for.end215

for.body206:                                      ; preds = %for.cond203
  %173 = load float, float* %x, align 4
  %174 = load i32, i32* %i, align 4
  %arrayidx207 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 %174
  %175 = load float, float* %arrayidx207, align 4
  %cmp208 = fcmp olt float %173, %175
  br i1 %cmp208, label %if.then210, label %if.end212

if.then210:                                       ; preds = %for.body206
  %176 = load i32, i32* %i, align 4
  %arrayidx211 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 %176
  %177 = load float, float* %arrayidx211, align 4
  store float %177, float* %x, align 4
  br label %if.end212

if.end212:                                        ; preds = %if.then210, %for.body206
  br label %for.inc213

for.inc213:                                       ; preds = %if.end212
  %178 = load i32, i32* %i, align 4
  %inc214 = add nsw i32 %178, 1
  store i32 %inc214, i32* %i, align 4
  br label %for.cond203

for.end215:                                       ; preds = %for.cond203
  %179 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %ers_save = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %179, i32 0, i32 17
  %180 = load i32, i32* %chn, align 4
  %arrayidx216 = getelementptr inbounds [4 x double], [4 x double]* %ers_save, i32 0, i32 %180
  %181 = load double, double* %arrayidx216, align 8
  %182 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %ers = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %182, i32 0, i32 18
  %183 = load i32, i32* %gr_out.addr, align 4
  %arrayidx217 = getelementptr inbounds [2 x [4 x double]], [2 x [4 x double]]* %ers, i32 0, i32 %183
  %184 = load i32, i32* %chn, align 4
  %arrayidx218 = getelementptr inbounds [4 x double], [4 x double]* %arrayidx217, i32 0, i32 %184
  store double %181, double* %arrayidx218, align 8
  %185 = load float, float* %x, align 4
  %conv219 = fpext float %185 to double
  %186 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %ers_save220 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %186, i32 0, i32 17
  %187 = load i32, i32* %chn, align 4
  %arrayidx221 = getelementptr inbounds [4 x double], [4 x double]* %ers_save220, i32 0, i32 %187
  store double %conv219, double* %arrayidx221, align 8
  br label %if.end222

if.end222:                                        ; preds = %for.end215, %for.end199
  %188 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %188, i32 0, i32 22
  %189 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %attack_threshold = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %189, i32 0, i32 5
  %190 = load i32, i32* %chn, align 4
  %arrayidx224 = getelementptr inbounds [4 x float], [4 x float]* %attack_threshold, i32 0, i32 %190
  %191 = load float, float* %arrayidx224, align 4
  store float %191, float* %x223, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond225

for.cond225:                                      ; preds = %for.inc245, %if.end222
  %192 = load i32, i32* %i, align 4
  %cmp226 = icmp slt i32 %192, 12
  br i1 %cmp226, label %for.body228, label %for.end247

for.body228:                                      ; preds = %for.cond225
  %193 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %194 = load i32, i32* %chn, align 4
  %arrayidx229 = getelementptr inbounds [4 x i32], [4 x i32]* %193, i32 %194
  %195 = load i32, i32* %i, align 4
  %div230 = sdiv i32 %195, 3
  %arrayidx231 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx229, i32 0, i32 %div230
  %196 = load i32, i32* %arrayidx231, align 4
  %cmp232 = icmp eq i32 %196, 0
  br i1 %cmp232, label %if.then234, label %if.end244

if.then234:                                       ; preds = %for.body228
  %197 = load i32, i32* %i, align 4
  %arrayidx235 = getelementptr inbounds [12 x float], [12 x float]* %attack_intensity, i32 0, i32 %197
  %198 = load float, float* %arrayidx235, align 4
  %199 = load float, float* %x223, align 4
  %cmp236 = fcmp ogt float %198, %199
  br i1 %cmp236, label %if.then238, label %if.end243

if.then238:                                       ; preds = %if.then234
  %200 = load i32, i32* %i, align 4
  %rem = srem i32 %200, 3
  %add239 = add nsw i32 %rem, 1
  %201 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %202 = load i32, i32* %chn, align 4
  %arrayidx240 = getelementptr inbounds [4 x i32], [4 x i32]* %201, i32 %202
  %203 = load i32, i32* %i, align 4
  %div241 = sdiv i32 %203, 3
  %arrayidx242 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx240, i32 0, i32 %div241
  store i32 %add239, i32* %arrayidx242, align 4
  br label %if.end243

if.end243:                                        ; preds = %if.then238, %if.then234
  br label %if.end244

if.end244:                                        ; preds = %if.end243, %for.body228
  br label %for.inc245

for.inc245:                                       ; preds = %if.end244
  %204 = load i32, i32* %i, align 4
  %inc246 = add nsw i32 %204, 1
  store i32 %inc246, i32* %i, align 4
  br label %for.cond225

for.end247:                                       ; preds = %for.cond225
  store i32 1, i32* %i, align 4
  br label %for.cond248

for.cond248:                                      ; preds = %for.inc288, %for.end247
  %205 = load i32, i32* %i, align 4
  %cmp249 = icmp slt i32 %205, 4
  br i1 %cmp249, label %for.body251, label %for.end290

for.body251:                                      ; preds = %for.cond248
  %206 = load i32, i32* %i, align 4
  %sub252 = sub nsw i32 %206, 1
  %arrayidx253 = getelementptr inbounds [4 x float], [4 x float]* %en_short, i32 0, i32 %sub252
  %207 = load float, float* %arrayidx253, align 4
  store float %207, float* %u, align 4
  %208 = load i32, i32* %i, align 4
  %arrayidx254 = getelementptr inbounds [4 x float], [4 x float]* %en_short, i32 0, i32 %208
  %209 = load float, float* %arrayidx254, align 4
  store float %209, float* %v, align 4
  %210 = load float, float* %u, align 4
  %211 = load float, float* %v, align 4
  %cmp255 = fcmp ogt float %210, %211
  br i1 %cmp255, label %cond.true257, label %cond.false258

cond.true257:                                     ; preds = %for.body251
  %212 = load float, float* %u, align 4
  br label %cond.end259

cond.false258:                                    ; preds = %for.body251
  %213 = load float, float* %v, align 4
  br label %cond.end259

cond.end259:                                      ; preds = %cond.false258, %cond.true257
  %cond260 = phi float [ %212, %cond.true257 ], [ %213, %cond.false258 ]
  store float %cond260, float* %m, align 4
  %214 = load float, float* %m, align 4
  %cmp261 = fcmp olt float %214, 4.000000e+04
  br i1 %cmp261, label %if.then263, label %if.end287

if.then263:                                       ; preds = %cond.end259
  %215 = load float, float* %u, align 4
  %216 = load float, float* %v, align 4
  %mul264 = fmul float 0x3FFB333340000000, %216
  %cmp265 = fcmp olt float %215, %mul264
  br i1 %cmp265, label %land.lhs.true, label %if.end286

land.lhs.true:                                    ; preds = %if.then263
  %217 = load float, float* %v, align 4
  %218 = load float, float* %u, align 4
  %mul267 = fmul float 0x3FFB333340000000, %218
  %cmp268 = fcmp olt float %217, %mul267
  br i1 %cmp268, label %if.then270, label %if.end286

if.then270:                                       ; preds = %land.lhs.true
  %219 = load i32, i32* %i, align 4
  %cmp271 = icmp eq i32 %219, 1
  br i1 %cmp271, label %land.lhs.true273, label %if.end283

land.lhs.true273:                                 ; preds = %if.then270
  %220 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %221 = load i32, i32* %chn, align 4
  %arrayidx274 = getelementptr inbounds [4 x i32], [4 x i32]* %220, i32 %221
  %arrayidx275 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx274, i32 0, i32 0
  %222 = load i32, i32* %arrayidx275, align 4
  %223 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %224 = load i32, i32* %chn, align 4
  %arrayidx276 = getelementptr inbounds [4 x i32], [4 x i32]* %223, i32 %224
  %225 = load i32, i32* %i, align 4
  %arrayidx277 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx276, i32 0, i32 %225
  %226 = load i32, i32* %arrayidx277, align 4
  %cmp278 = icmp sle i32 %222, %226
  br i1 %cmp278, label %if.then280, label %if.end283

if.then280:                                       ; preds = %land.lhs.true273
  %227 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %228 = load i32, i32* %chn, align 4
  %arrayidx281 = getelementptr inbounds [4 x i32], [4 x i32]* %227, i32 %228
  %arrayidx282 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx281, i32 0, i32 0
  store i32 0, i32* %arrayidx282, align 4
  br label %if.end283

if.end283:                                        ; preds = %if.then280, %land.lhs.true273, %if.then270
  %229 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %230 = load i32, i32* %chn, align 4
  %arrayidx284 = getelementptr inbounds [4 x i32], [4 x i32]* %229, i32 %230
  %231 = load i32, i32* %i, align 4
  %arrayidx285 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx284, i32 0, i32 %231
  store i32 0, i32* %arrayidx285, align 4
  br label %if.end286

if.end286:                                        ; preds = %if.end283, %land.lhs.true, %if.then263
  br label %if.end287

if.end287:                                        ; preds = %if.end286, %cond.end259
  br label %for.inc288

for.inc288:                                       ; preds = %if.end287
  %232 = load i32, i32* %i, align 4
  %inc289 = add nsw i32 %232, 1
  store i32 %inc289, i32* %i, align 4
  br label %for.cond248

for.end290:                                       ; preds = %for.cond248
  %233 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %234 = load i32, i32* %chn, align 4
  %arrayidx291 = getelementptr inbounds [4 x i32], [4 x i32]* %233, i32 %234
  %arrayidx292 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx291, i32 0, i32 0
  %235 = load i32, i32* %arrayidx292, align 4
  %236 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_attacks = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %236, i32 0, i32 9
  %237 = load i32, i32* %chn, align 4
  %arrayidx293 = getelementptr inbounds [4 x i32], [4 x i32]* %last_attacks, i32 0, i32 %237
  %238 = load i32, i32* %arrayidx293, align 4
  %cmp294 = icmp sle i32 %235, %238
  br i1 %cmp294, label %if.then296, label %if.end299

if.then296:                                       ; preds = %for.end290
  %239 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %240 = load i32, i32* %chn, align 4
  %arrayidx297 = getelementptr inbounds [4 x i32], [4 x i32]* %239, i32 %240
  %arrayidx298 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx297, i32 0, i32 0
  store i32 0, i32* %arrayidx298, align 4
  br label %if.end299

if.end299:                                        ; preds = %if.then296, %for.end290
  %241 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_attacks300 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %241, i32 0, i32 9
  %242 = load i32, i32* %chn, align 4
  %arrayidx301 = getelementptr inbounds [4 x i32], [4 x i32]* %last_attacks300, i32 0, i32 %242
  %243 = load i32, i32* %arrayidx301, align 4
  %cmp302 = icmp eq i32 %243, 3
  br i1 %cmp302, label %if.then316, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end299
  %244 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %245 = load i32, i32* %chn, align 4
  %arrayidx304 = getelementptr inbounds [4 x i32], [4 x i32]* %244, i32 %245
  %arrayidx305 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx304, i32 0, i32 0
  %246 = load i32, i32* %arrayidx305, align 4
  %247 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %248 = load i32, i32* %chn, align 4
  %arrayidx306 = getelementptr inbounds [4 x i32], [4 x i32]* %247, i32 %248
  %arrayidx307 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx306, i32 0, i32 1
  %249 = load i32, i32* %arrayidx307, align 4
  %add308 = add nsw i32 %246, %249
  %250 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %251 = load i32, i32* %chn, align 4
  %arrayidx309 = getelementptr inbounds [4 x i32], [4 x i32]* %250, i32 %251
  %arrayidx310 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx309, i32 0, i32 2
  %252 = load i32, i32* %arrayidx310, align 4
  %add311 = add nsw i32 %add308, %252
  %253 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %254 = load i32, i32* %chn, align 4
  %arrayidx312 = getelementptr inbounds [4 x i32], [4 x i32]* %253, i32 %254
  %arrayidx313 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx312, i32 0, i32 3
  %255 = load i32, i32* %arrayidx313, align 4
  %add314 = add nsw i32 %add311, %255
  %tobool315 = icmp ne i32 %add314, 0
  br i1 %tobool315, label %if.then316, label %if.end350

if.then316:                                       ; preds = %lor.lhs.false, %if.end299
  store i32 0, i32* %ns_uselongblock, align 4
  %256 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %257 = load i32, i32* %chn, align 4
  %arrayidx317 = getelementptr inbounds [4 x i32], [4 x i32]* %256, i32 %257
  %arrayidx318 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx317, i32 0, i32 1
  %258 = load i32, i32* %arrayidx318, align 4
  %tobool319 = icmp ne i32 %258, 0
  br i1 %tobool319, label %land.lhs.true320, label %if.end327

land.lhs.true320:                                 ; preds = %if.then316
  %259 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %260 = load i32, i32* %chn, align 4
  %arrayidx321 = getelementptr inbounds [4 x i32], [4 x i32]* %259, i32 %260
  %arrayidx322 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx321, i32 0, i32 0
  %261 = load i32, i32* %arrayidx322, align 4
  %tobool323 = icmp ne i32 %261, 0
  br i1 %tobool323, label %if.then324, label %if.end327

if.then324:                                       ; preds = %land.lhs.true320
  %262 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %263 = load i32, i32* %chn, align 4
  %arrayidx325 = getelementptr inbounds [4 x i32], [4 x i32]* %262, i32 %263
  %arrayidx326 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx325, i32 0, i32 1
  store i32 0, i32* %arrayidx326, align 4
  br label %if.end327

if.end327:                                        ; preds = %if.then324, %land.lhs.true320, %if.then316
  %264 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %265 = load i32, i32* %chn, align 4
  %arrayidx328 = getelementptr inbounds [4 x i32], [4 x i32]* %264, i32 %265
  %arrayidx329 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx328, i32 0, i32 2
  %266 = load i32, i32* %arrayidx329, align 4
  %tobool330 = icmp ne i32 %266, 0
  br i1 %tobool330, label %land.lhs.true331, label %if.end338

land.lhs.true331:                                 ; preds = %if.end327
  %267 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %268 = load i32, i32* %chn, align 4
  %arrayidx332 = getelementptr inbounds [4 x i32], [4 x i32]* %267, i32 %268
  %arrayidx333 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx332, i32 0, i32 1
  %269 = load i32, i32* %arrayidx333, align 4
  %tobool334 = icmp ne i32 %269, 0
  br i1 %tobool334, label %if.then335, label %if.end338

if.then335:                                       ; preds = %land.lhs.true331
  %270 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %271 = load i32, i32* %chn, align 4
  %arrayidx336 = getelementptr inbounds [4 x i32], [4 x i32]* %270, i32 %271
  %arrayidx337 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx336, i32 0, i32 2
  store i32 0, i32* %arrayidx337, align 4
  br label %if.end338

if.end338:                                        ; preds = %if.then335, %land.lhs.true331, %if.end327
  %272 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %273 = load i32, i32* %chn, align 4
  %arrayidx339 = getelementptr inbounds [4 x i32], [4 x i32]* %272, i32 %273
  %arrayidx340 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx339, i32 0, i32 3
  %274 = load i32, i32* %arrayidx340, align 4
  %tobool341 = icmp ne i32 %274, 0
  br i1 %tobool341, label %land.lhs.true342, label %if.end349

land.lhs.true342:                                 ; preds = %if.end338
  %275 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %276 = load i32, i32* %chn, align 4
  %arrayidx343 = getelementptr inbounds [4 x i32], [4 x i32]* %275, i32 %276
  %arrayidx344 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx343, i32 0, i32 2
  %277 = load i32, i32* %arrayidx344, align 4
  %tobool345 = icmp ne i32 %277, 0
  br i1 %tobool345, label %if.then346, label %if.end349

if.then346:                                       ; preds = %land.lhs.true342
  %278 = load [4 x i32]*, [4 x i32]** %ns_attacks.addr, align 4
  %279 = load i32, i32* %chn, align 4
  %arrayidx347 = getelementptr inbounds [4 x i32], [4 x i32]* %278, i32 %279
  %arrayidx348 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx347, i32 0, i32 3
  store i32 0, i32* %arrayidx348, align 4
  br label %if.end349

if.end349:                                        ; preds = %if.then346, %land.lhs.true342, %if.end338
  br label %if.end350

if.end350:                                        ; preds = %if.end349, %lor.lhs.false
  %280 = load i32, i32* %chn, align 4
  %cmp351 = icmp slt i32 %280, 2
  br i1 %cmp351, label %if.then353, label %if.else355

if.then353:                                       ; preds = %if.end350
  %281 = load i32, i32* %ns_uselongblock, align 4
  %282 = load i32*, i32** %uselongblock.addr, align 4
  %283 = load i32, i32* %chn, align 4
  %arrayidx354 = getelementptr inbounds i32, i32* %282, i32 %283
  store i32 %281, i32* %arrayidx354, align 4
  br label %if.end362

if.else355:                                       ; preds = %if.end350
  %284 = load i32, i32* %ns_uselongblock, align 4
  %cmp356 = icmp eq i32 %284, 0
  br i1 %cmp356, label %if.then358, label %if.end361

if.then358:                                       ; preds = %if.else355
  %285 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx359 = getelementptr inbounds i32, i32* %285, i32 1
  store i32 0, i32* %arrayidx359, align 4
  %286 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx360 = getelementptr inbounds i32, i32* %286, i32 0
  store i32 0, i32* %arrayidx360, align 4
  br label %if.end361

if.end361:                                        ; preds = %if.then358, %if.else355
  br label %if.end362

if.end362:                                        ; preds = %if.end361, %if.then353
  %287 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %tot_ener = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %287, i32 0, i32 7
  %288 = load i32, i32* %chn, align 4
  %arrayidx363 = getelementptr inbounds [4 x float], [4 x float]* %tot_ener, i32 0, i32 %288
  %289 = load float, float* %arrayidx363, align 4
  %290 = load float*, float** %energy.addr, align 4
  %291 = load i32, i32* %chn, align 4
  %arrayidx364 = getelementptr inbounds float, float* %290, i32 %291
  store float %289, float* %arrayidx364, align 4
  br label %for.inc365

for.inc365:                                       ; preds = %if.end362
  %292 = load i32, i32* %chn, align 4
  %inc366 = add nsw i32 %292, 1
  store i32 %inc366, i32* %chn, align 4
  br label %for.cond66

for.end367:                                       ; preds = %for.cond66
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_block_type(%struct.SessionConfig_t* %cfg, i32* %uselongblock) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %uselongblock.addr = alloca i32*, align 4
  %chn = alloca i32, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  store i32* %uselongblock, i32** %uselongblock.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %short_blocks = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 42
  %1 = load i32, i32* %short_blocks, align 4
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 0
  %3 = load i32, i32* %arrayidx, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %land.lhs.true1, label %if.then

land.lhs.true1:                                   ; preds = %land.lhs.true
  %4 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %4, i32 1
  %5 = load i32, i32* %arrayidx2, align 4
  %tobool3 = icmp ne i32 %5, 0
  br i1 %tobool3, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true1, %land.lhs.true
  %6 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %6, i32 1
  store i32 0, i32* %arrayidx4, align 4
  %7 = load i32*, i32** %uselongblock.addr, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %7, i32 0
  store i32 0, i32* %arrayidx5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true1, %entry
  store i32 0, i32* %chn, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %chn, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 14
  %10 = load i32, i32* %channels_out, align 4
  %cmp6 = icmp slt i32 %8, %10
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %short_blocks7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 42
  %12 = load i32, i32* %short_blocks7, align 4
  %cmp8 = icmp eq i32 %12, 2
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %for.body
  %13 = load i32*, i32** %uselongblock.addr, align 4
  %14 = load i32, i32* %chn, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %13, i32 %14
  store i32 1, i32* %arrayidx10, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %for.body
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %short_blocks12 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 42
  %16 = load i32, i32* %short_blocks12, align 4
  %cmp13 = icmp eq i32 %16, 3
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end11
  %17 = load i32*, i32** %uselongblock.addr, align 4
  %18 = load i32, i32* %chn, align 4
  %arrayidx15 = getelementptr inbounds i32, i32* %17, i32 %18
  store i32 0, i32* %arrayidx15, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end11
  br label %for.inc

for.inc:                                          ; preds = %if.end16
  %19 = load i32, i32* %chn, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %chn, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_fft_l(%struct.lame_internal_flags* %gfc, float** %buffer, i32 %chn, i32 %gr_out, float* %fftenergy, [1024 x float]* %wsamp_l) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca float**, align 4
  %chn.addr = alloca i32, align 4
  %gr_out.addr = alloca i32, align 4
  %fftenergy.addr = alloca float*, align 4
  %wsamp_l.addr = alloca [1024 x float]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %plt = alloca %struct.plotting_data*, align 4
  %j = alloca i32, align 4
  %sqrt2_half = alloca float, align 4
  %l = alloca float, align 4
  %r = alloca float, align 4
  %re = alloca float, align 4
  %im = alloca float, align 4
  %totalenergy = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store i32 %gr_out, i32* %gr_out.addr, align 4
  store float* %fftenergy, float** %fftenergy.addr, align 4
  store [1024 x float]* %wsamp_l, [1024 x float]** %wsamp_l.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 31
  %3 = load i32, i32* %analysis, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 23
  %5 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.plotting_data* [ %5, %cond.true ], [ null, %cond.false ]
  store %struct.plotting_data* %cond, %struct.plotting_data** %plt, align 4
  %6 = load i32, i32* %chn.addr, align 4
  %cmp = icmp slt i32 %6, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %8 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arraydecay = getelementptr inbounds [1024 x float], [1024 x float]* %8, i32 0, i32 0
  %9 = load i32, i32* %chn.addr, align 4
  %10 = load float**, float*** %buffer.addr, align 4
  call void @fft_long(%struct.lame_internal_flags* %7, float* %arraydecay, i32 %9, float** %10)
  br label %if.end13

if.else:                                          ; preds = %cond.end
  %11 = load i32, i32* %chn.addr, align 4
  %cmp2 = icmp eq i32 %11, 2
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  store float 0x3FE6A09E60000000, float* %sqrt2_half, align 4
  store i32 1023, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %12 = load i32, i32* %j, align 4
  %cmp4 = icmp sge i32 %12, 0
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arrayidx = getelementptr inbounds [1024 x float], [1024 x float]* %13, i32 0
  %14 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds [1024 x float], [1024 x float]* %arrayidx, i32 0, i32 %14
  %15 = load float, float* %arrayidx5, align 4
  store float %15, float* %l, align 4
  %16 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arrayidx6 = getelementptr inbounds [1024 x float], [1024 x float]* %16, i32 1
  %17 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds [1024 x float], [1024 x float]* %arrayidx6, i32 0, i32 %17
  %18 = load float, float* %arrayidx7, align 4
  store float %18, float* %r, align 4
  %19 = load float, float* %l, align 4
  %20 = load float, float* %r, align 4
  %add = fadd float %19, %20
  %mul = fmul float %add, 0x3FE6A09E60000000
  %21 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arrayidx8 = getelementptr inbounds [1024 x float], [1024 x float]* %21, i32 0
  %22 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds [1024 x float], [1024 x float]* %arrayidx8, i32 0, i32 %22
  store float %mul, float* %arrayidx9, align 4
  %23 = load float, float* %l, align 4
  %24 = load float, float* %r, align 4
  %sub = fsub float %23, %24
  %mul10 = fmul float %sub, 0x3FE6A09E60000000
  %25 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arrayidx11 = getelementptr inbounds [1024 x float], [1024 x float]* %25, i32 1
  %26 = load i32, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds [1024 x float], [1024 x float]* %arrayidx11, i32 0, i32 %26
  store float %mul10, float* %arrayidx12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %27 = load i32, i32* %j, align 4
  %dec = add nsw i32 %27, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %if.else
  br label %if.end13

if.end13:                                         ; preds = %if.end, %if.then
  %28 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %arrayidx14 = getelementptr inbounds [1024 x float], [1024 x float]* %28, i32 0
  %arrayidx15 = getelementptr inbounds [1024 x float], [1024 x float]* %arrayidx14, i32 0, i32 0
  %29 = load float, float* %arrayidx15, align 4
  %30 = load float*, float** %fftenergy.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %30, i32 0
  store float %29, float* %arrayidx16, align 4
  %31 = load float*, float** %fftenergy.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %31, i32 0
  %32 = load float, float* %arrayidx17, align 4
  %33 = load float*, float** %fftenergy.addr, align 4
  %arrayidx18 = getelementptr inbounds float, float* %33, i32 0
  %34 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %34, %32
  store float %mul19, float* %arrayidx18, align 4
  store i32 511, i32* %j, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc33, %if.end13
  %35 = load i32, i32* %j, align 4
  %cmp21 = icmp sge i32 %35, 0
  br i1 %cmp21, label %for.body22, label %for.end35

for.body22:                                       ; preds = %for.cond20
  %36 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %37 = load i32, i32* %j, align 4
  %sub23 = sub nsw i32 512, %37
  %arrayidx24 = getelementptr inbounds [1024 x float], [1024 x float]* %36, i32 0, i32 %sub23
  %38 = load float, float* %arrayidx24, align 4
  store float %38, float* %re, align 4
  %39 = load [1024 x float]*, [1024 x float]** %wsamp_l.addr, align 4
  %40 = load i32, i32* %j, align 4
  %add25 = add nsw i32 512, %40
  %arrayidx26 = getelementptr inbounds [1024 x float], [1024 x float]* %39, i32 0, i32 %add25
  %41 = load float, float* %arrayidx26, align 4
  store float %41, float* %im, align 4
  %42 = load float, float* %re, align 4
  %43 = load float, float* %re, align 4
  %mul27 = fmul float %42, %43
  %44 = load float, float* %im, align 4
  %45 = load float, float* %im, align 4
  %mul28 = fmul float %44, %45
  %add29 = fadd float %mul27, %mul28
  %mul30 = fmul float %add29, 5.000000e-01
  %46 = load float*, float** %fftenergy.addr, align 4
  %47 = load i32, i32* %j, align 4
  %sub31 = sub nsw i32 512, %47
  %arrayidx32 = getelementptr inbounds float, float* %46, i32 %sub31
  store float %mul30, float* %arrayidx32, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body22
  %48 = load i32, i32* %j, align 4
  %dec34 = add nsw i32 %48, -1
  store i32 %dec34, i32* %j, align 4
  br label %for.cond20

for.end35:                                        ; preds = %for.cond20
  store float 0.000000e+00, float* %totalenergy, align 4
  store i32 11, i32* %j, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc41, %for.end35
  %49 = load i32, i32* %j, align 4
  %cmp37 = icmp slt i32 %49, 513
  br i1 %cmp37, label %for.body38, label %for.end42

for.body38:                                       ; preds = %for.cond36
  %50 = load float*, float** %fftenergy.addr, align 4
  %51 = load i32, i32* %j, align 4
  %arrayidx39 = getelementptr inbounds float, float* %50, i32 %51
  %52 = load float, float* %arrayidx39, align 4
  %53 = load float, float* %totalenergy, align 4
  %add40 = fadd float %53, %52
  store float %add40, float* %totalenergy, align 4
  br label %for.inc41

for.inc41:                                        ; preds = %for.body38
  %54 = load i32, i32* %j, align 4
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond36

for.end42:                                        ; preds = %for.cond36
  %55 = load float, float* %totalenergy, align 4
  %56 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %tot_ener = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %56, i32 0, i32 7
  %57 = load i32, i32* %chn.addr, align 4
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %tot_ener, i32 0, i32 %57
  store float %55, float* %arrayidx43, align 4
  %58 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %tobool44 = icmp ne %struct.plotting_data* %58, null
  br i1 %tobool44, label %if.then45, label %if.end61

if.then45:                                        ; preds = %for.end42
  store i32 0, i32* %j, align 4
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc58, %if.then45
  %59 = load i32, i32* %j, align 4
  %cmp47 = icmp slt i32 %59, 513
  br i1 %cmp47, label %for.body48, label %for.end60

for.body48:                                       ; preds = %for.cond46
  %60 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %energy_save = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %60, i32 0, i32 10
  %61 = load i32, i32* %chn.addr, align 4
  %arrayidx49 = getelementptr inbounds [4 x [1024 x double]], [4 x [1024 x double]]* %energy_save, i32 0, i32 %61
  %62 = load i32, i32* %j, align 4
  %arrayidx50 = getelementptr inbounds [1024 x double], [1024 x double]* %arrayidx49, i32 0, i32 %62
  %63 = load double, double* %arrayidx50, align 8
  %64 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %energy = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %64, i32 0, i32 11
  %65 = load i32, i32* %gr_out.addr, align 4
  %arrayidx51 = getelementptr inbounds [2 x [4 x [1024 x double]]], [2 x [4 x [1024 x double]]]* %energy, i32 0, i32 %65
  %66 = load i32, i32* %chn.addr, align 4
  %arrayidx52 = getelementptr inbounds [4 x [1024 x double]], [4 x [1024 x double]]* %arrayidx51, i32 0, i32 %66
  %67 = load i32, i32* %j, align 4
  %arrayidx53 = getelementptr inbounds [1024 x double], [1024 x double]* %arrayidx52, i32 0, i32 %67
  store double %63, double* %arrayidx53, align 8
  %68 = load float*, float** %fftenergy.addr, align 4
  %69 = load i32, i32* %j, align 4
  %arrayidx54 = getelementptr inbounds float, float* %68, i32 %69
  %70 = load float, float* %arrayidx54, align 4
  %conv = fpext float %70 to double
  %71 = load %struct.plotting_data*, %struct.plotting_data** %plt, align 4
  %energy_save55 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %71, i32 0, i32 10
  %72 = load i32, i32* %chn.addr, align 4
  %arrayidx56 = getelementptr inbounds [4 x [1024 x double]], [4 x [1024 x double]]* %energy_save55, i32 0, i32 %72
  %73 = load i32, i32* %j, align 4
  %arrayidx57 = getelementptr inbounds [1024 x double], [1024 x double]* %arrayidx56, i32 0, i32 %73
  store double %conv, double* %arrayidx57, align 8
  br label %for.inc58

for.inc58:                                        ; preds = %for.body48
  %74 = load i32, i32* %j, align 4
  %inc59 = add nsw i32 %74, 1
  store i32 %inc59, i32* %j, align 4
  br label %for.cond46

for.end60:                                        ; preds = %for.cond46
  br label %if.end61

if.end61:                                         ; preds = %for.end60, %for.end42
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_loudness_approximation_l(%struct.lame_internal_flags* %gfc, i32 %gr_out, i32 %chn, float* %fftenergy) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gr_out.addr = alloca i32, align 4
  %chn.addr = alloca i32, align 4
  %fftenergy.addr = alloca float*, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %gr_out, i32* %gr_out.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store float* %fftenergy, float** %fftenergy.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load i32, i32* %chn.addr, align 4
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %loudness_sq_save = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %2, i32 0, i32 6
  %3 = load i32, i32* %chn.addr, align 4
  %arrayidx = getelementptr inbounds [2 x float], [2 x float]* %loudness_sq_save, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 10
  %loudness_sq = getelementptr inbounds %struct.PsyResult_t, %struct.PsyResult_t* %ov_psy, i32 0, i32 0
  %6 = load i32, i32* %gr_out.addr, align 4
  %arrayidx1 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %loudness_sq, i32 0, i32 %6
  %7 = load i32, i32* %chn.addr, align 4
  %arrayidx2 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1, i32 0, i32 %7
  store float %4, float* %arrayidx2, align 4
  %8 = load float*, float** %fftenergy.addr, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 21
  %10 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %eql_w = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %10, i32 0, i32 12
  %arraydecay = getelementptr inbounds [512 x float], [512 x float]* %eql_w, i32 0, i32 0
  %call = call float @psycho_loudness_approx(float* %8, float* %arraydecay)
  %11 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %loudness_sq_save3 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %11, i32 0, i32 6
  %12 = load i32, i32* %chn.addr, align 4
  %arrayidx4 = getelementptr inbounds [2 x float], [2 x float]* %loudness_sq_save3, i32 0, i32 %12
  store float %call, float* %arrayidx4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_masking_l(%struct.lame_internal_flags* %gfc, float* %fftenergy, float* %eb_l, float* %thr, i32 %chn) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %fftenergy.addr = alloca float*, align 4
  %eb_l.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gdl = alloca %struct.PsyConst_CB2SB_t*, align 4
  %max = alloca [64 x float], align 16
  %avg = alloca [64 x float], align 16
  %mask_idx_l = alloca [66 x i8], align 16
  %k = alloca i32, align 4
  %b = alloca i32, align 4
  %x = alloca float, align 4
  %ecb = alloca float, align 4
  %avg_mask = alloca float, align 4
  %t = alloca float, align 4
  %masking_lower = alloca float, align 4
  %kk = alloca i32, align 4
  %last = alloca i32, align 4
  %delta = alloca i32, align 4
  %dd = alloca i32, align 4
  %dd_n = alloca i32, align 4
  %ecb_limit = alloca float, align 4
  %ecb_limit_2 = alloca float, align 4
  %ecb_limit_1 = alloca float, align 4
  %ecb_limit81 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %fftenergy, float** %fftenergy.addr, align 4
  store float* %eb_l, float** %eb_l.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %l = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %2, i32 0, i32 2
  store %struct.PsyConst_CB2SB_t* %l, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %3 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %4 = load float*, float** %fftenergy.addr, align 4
  %5 = load float*, float** %eb_l.addr, align 4
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [64 x float], [64 x float]* %avg, i32 0, i32 0
  call void @calc_energy(%struct.PsyConst_CB2SB_t* %3, float* %4, float* %5, float* %arraydecay, float* %arraydecay1)
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay2 = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [64 x float], [64 x float]* %avg, i32 0, i32 0
  %arraydecay4 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 0
  call void @calc_mask_index_l(%struct.lame_internal_flags* %6, float* %arraydecay2, float* %arraydecay3, i8* %arraydecay4)
  store i32 0, i32* %k, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %b, align 4
  %8 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %8, i32 0, i32 11
  %9 = load i32, i32* %npart, align 4
  %cmp = icmp slt i32 %7, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %masking_lower5 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %10, i32 0, i32 0
  %11 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds [64 x float], [64 x float]* %masking_lower5, i32 0, i32 %11
  %12 = load float, float* %arrayidx, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 13
  %masking_lower6 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 2
  %14 = load float, float* %masking_lower6, align 4
  %mul = fmul float %12, %14
  store float %mul, float* %masking_lower, align 4
  %15 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %s3ind = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %15, i32 0, i32 7
  %16 = load i32, i32* %b, align 4
  %arrayidx7 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind, i32 0, i32 %16
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx7, i32 0, i32 0
  %17 = load i32, i32* %arrayidx8, align 4
  store i32 %17, i32* %kk, align 4
  %18 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %s3ind9 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %18, i32 0, i32 7
  %19 = load i32, i32* %b, align 4
  %arrayidx10 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind9, i32 0, i32 %19
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx10, i32 0, i32 1
  %20 = load i32, i32* %arrayidx11, align 4
  store i32 %20, i32* %last, align 4
  %21 = load i32, i32* %b, align 4
  %arrayidx12 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 %21
  %22 = load i8, i8* %arrayidx12, align 1
  %conv = zext i8 %22 to i32
  %call = call i32 @mask_add_delta(i32 %conv)
  store i32 %call, i32* %delta, align 4
  store i32 0, i32* %dd, align 4
  store i32 0, i32* %dd_n, align 4
  %23 = load i32, i32* %kk, align 4
  %arrayidx13 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 %23
  %24 = load i8, i8* %arrayidx13, align 1
  %conv14 = zext i8 %24 to i32
  store i32 %conv14, i32* %dd, align 4
  %25 = load i32, i32* %dd_n, align 4
  %add = add nsw i32 %25, 1
  store i32 %add, i32* %dd_n, align 4
  %26 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %s3 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %26, i32 0, i32 13
  %27 = load float*, float** %s3, align 4
  %28 = load i32, i32* %k, align 4
  %arrayidx15 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx15, align 4
  %30 = load float*, float** %eb_l.addr, align 4
  %31 = load i32, i32* %kk, align 4
  %arrayidx16 = getelementptr inbounds float, float* %30, i32 %31
  %32 = load float, float* %arrayidx16, align 4
  %mul17 = fmul float %29, %32
  %33 = load i32, i32* %kk, align 4
  %arrayidx18 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 %33
  %34 = load i8, i8* %arrayidx18, align 1
  %idxprom = zext i8 %34 to i32
  %arrayidx19 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %idxprom
  %35 = load float, float* %arrayidx19, align 4
  %mul20 = fmul float %mul17, %35
  store float %mul20, float* %ecb, align 4
  %36 = load i32, i32* %k, align 4
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %k, align 4
  %37 = load i32, i32* %kk, align 4
  %inc21 = add nsw i32 %37, 1
  store i32 %inc21, i32* %kk, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body
  %38 = load i32, i32* %kk, align 4
  %39 = load i32, i32* %last, align 4
  %cmp22 = icmp sle i32 %38, %39
  br i1 %cmp22, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %40 = load i32, i32* %kk, align 4
  %arrayidx24 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 %40
  %41 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %41 to i32
  %42 = load i32, i32* %dd, align 4
  %add26 = add nsw i32 %42, %conv25
  store i32 %add26, i32* %dd, align 4
  %43 = load i32, i32* %dd_n, align 4
  %add27 = add nsw i32 %43, 1
  store i32 %add27, i32* %dd_n, align 4
  %44 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %s328 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %44, i32 0, i32 13
  %45 = load float*, float** %s328, align 4
  %46 = load i32, i32* %k, align 4
  %arrayidx29 = getelementptr inbounds float, float* %45, i32 %46
  %47 = load float, float* %arrayidx29, align 4
  %48 = load float*, float** %eb_l.addr, align 4
  %49 = load i32, i32* %kk, align 4
  %arrayidx30 = getelementptr inbounds float, float* %48, i32 %49
  %50 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %47, %50
  %51 = load i32, i32* %kk, align 4
  %arrayidx32 = getelementptr inbounds [66 x i8], [66 x i8]* %mask_idx_l, i32 0, i32 %51
  %52 = load i8, i8* %arrayidx32, align 1
  %idxprom33 = zext i8 %52 to i32
  %arrayidx34 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %idxprom33
  %53 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %mul31, %53
  store float %mul35, float* %x, align 4
  %54 = load float, float* %ecb, align 4
  %55 = load float, float* %x, align 4
  %56 = load i32, i32* %kk, align 4
  %57 = load i32, i32* %b, align 4
  %sub = sub nsw i32 %56, %57
  %58 = load i32, i32* %delta, align 4
  %call36 = call float @vbrpsy_mask_add(float %54, float %55, i32 %sub, i32 %58)
  store float %call36, float* %t, align 4
  %59 = load float, float* %t, align 4
  store float %59, float* %ecb, align 4
  %60 = load i32, i32* %k, align 4
  %inc37 = add nsw i32 %60, 1
  store i32 %inc37, i32* %k, align 4
  %61 = load i32, i32* %kk, align 4
  %inc38 = add nsw i32 %61, 1
  store i32 %inc38, i32* %kk, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %62 = load i32, i32* %dd, align 4
  %mul39 = mul nsw i32 2, %62
  %add40 = add nsw i32 1, %mul39
  %63 = load i32, i32* %dd_n, align 4
  %mul41 = mul nsw i32 2, %63
  %div = sdiv i32 %add40, %mul41
  store i32 %div, i32* %dd, align 4
  %64 = load i32, i32* %dd, align 4
  %arrayidx42 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %64
  %65 = load float, float* %arrayidx42, align 4
  %mul43 = fmul float %65, 5.000000e-01
  store float %mul43, float* %avg_mask, align 4
  %66 = load float, float* %avg_mask, align 4
  %67 = load float, float* %ecb, align 4
  %mul44 = fmul float %67, %66
  store float %mul44, float* %ecb, align 4
  %68 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %blocktype_old = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %68, i32 0, i32 10
  %69 = load i32, i32* %chn.addr, align 4
  %and = and i32 %69, 1
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old, i32 0, i32 %and
  %70 = load i32, i32* %arrayidx45, align 4
  %cmp46 = icmp eq i32 %70, 2
  br i1 %cmp46, label %if.then, label %if.else73

if.then:                                          ; preds = %while.end
  %71 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l1 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %71, i32 0, i32 0
  %72 = load i32, i32* %chn.addr, align 4
  %arrayidx48 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l1, i32 0, i32 %72
  %73 = load i32, i32* %b, align 4
  %arrayidx49 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx48, i32 0, i32 %73
  %74 = load float, float* %arrayidx49, align 4
  %mul50 = fmul float 2.000000e+00, %74
  store float %mul50, float* %ecb_limit, align 4
  %75 = load float, float* %ecb_limit, align 4
  %cmp51 = fcmp ogt float %75, 0.000000e+00
  br i1 %cmp51, label %if.then53, label %if.else

if.then53:                                        ; preds = %if.then
  %76 = load float, float* %ecb, align 4
  %77 = load float, float* %ecb_limit, align 4
  %cmp54 = fcmp olt float %76, %77
  br i1 %cmp54, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then53
  %78 = load float, float* %ecb, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then53
  %79 = load float, float* %ecb_limit, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %78, %cond.true ], [ %79, %cond.false ]
  %80 = load float*, float** %thr.addr, align 4
  %81 = load i32, i32* %b, align 4
  %arrayidx56 = getelementptr inbounds float, float* %80, i32 %81
  store float %cond, float* %arrayidx56, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %82 = load float, float* %ecb, align 4
  %conv57 = fpext float %82 to double
  %83 = load float*, float** %eb_l.addr, align 4
  %84 = load i32, i32* %b, align 4
  %arrayidx58 = getelementptr inbounds float, float* %83, i32 %84
  %85 = load float, float* %arrayidx58, align 4
  %conv59 = fpext float %85 to double
  %mul60 = fmul double %conv59, 3.000000e-01
  %cmp61 = fcmp olt double %conv57, %mul60
  br i1 %cmp61, label %cond.true63, label %cond.false65

cond.true63:                                      ; preds = %if.else
  %86 = load float, float* %ecb, align 4
  %conv64 = fpext float %86 to double
  br label %cond.end69

cond.false65:                                     ; preds = %if.else
  %87 = load float*, float** %eb_l.addr, align 4
  %88 = load i32, i32* %b, align 4
  %arrayidx66 = getelementptr inbounds float, float* %87, i32 %88
  %89 = load float, float* %arrayidx66, align 4
  %conv67 = fpext float %89 to double
  %mul68 = fmul double %conv67, 3.000000e-01
  br label %cond.end69

cond.end69:                                       ; preds = %cond.false65, %cond.true63
  %cond70 = phi double [ %conv64, %cond.true63 ], [ %mul68, %cond.false65 ]
  %conv71 = fptrunc double %cond70 to float
  %90 = load float*, float** %thr.addr, align 4
  %91 = load i32, i32* %b, align 4
  %arrayidx72 = getelementptr inbounds float, float* %90, i32 %91
  store float %conv71, float* %arrayidx72, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end69, %cond.end
  br label %if.end111

if.else73:                                        ; preds = %while.end
  %92 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l2 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %92, i32 0, i32 1
  %93 = load i32, i32* %chn.addr, align 4
  %arrayidx74 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l2, i32 0, i32 %93
  %94 = load i32, i32* %b, align 4
  %arrayidx75 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx74, i32 0, i32 %94
  %95 = load float, float* %arrayidx75, align 4
  %mul76 = fmul float 1.600000e+01, %95
  store float %mul76, float* %ecb_limit_2, align 4
  %96 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l177 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %96, i32 0, i32 0
  %97 = load i32, i32* %chn.addr, align 4
  %arrayidx78 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l177, i32 0, i32 %97
  %98 = load i32, i32* %b, align 4
  %arrayidx79 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx78, i32 0, i32 %98
  %99 = load float, float* %arrayidx79, align 4
  %mul80 = fmul float 2.000000e+00, %99
  store float %mul80, float* %ecb_limit_1, align 4
  %100 = load float, float* %ecb_limit_2, align 4
  %cmp82 = fcmp ole float %100, 0.000000e+00
  br i1 %cmp82, label %if.then84, label %if.end85

if.then84:                                        ; preds = %if.else73
  %101 = load float, float* %ecb, align 4
  store float %101, float* %ecb_limit_2, align 4
  br label %if.end85

if.end85:                                         ; preds = %if.then84, %if.else73
  %102 = load float, float* %ecb_limit_1, align 4
  %cmp86 = fcmp ole float %102, 0.000000e+00
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %if.end85
  %103 = load float, float* %ecb, align 4
  store float %103, float* %ecb_limit_1, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then88, %if.end85
  %104 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %blocktype_old90 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %104, i32 0, i32 10
  %105 = load i32, i32* %chn.addr, align 4
  %and91 = and i32 %105, 1
  %arrayidx92 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old90, i32 0, i32 %and91
  %106 = load i32, i32* %arrayidx92, align 4
  %cmp93 = icmp eq i32 %106, 0
  br i1 %cmp93, label %if.then95, label %if.else102

if.then95:                                        ; preds = %if.end89
  %107 = load float, float* %ecb_limit_1, align 4
  %108 = load float, float* %ecb_limit_2, align 4
  %cmp96 = fcmp olt float %107, %108
  br i1 %cmp96, label %cond.true98, label %cond.false99

cond.true98:                                      ; preds = %if.then95
  %109 = load float, float* %ecb_limit_1, align 4
  br label %cond.end100

cond.false99:                                     ; preds = %if.then95
  %110 = load float, float* %ecb_limit_2, align 4
  br label %cond.end100

cond.end100:                                      ; preds = %cond.false99, %cond.true98
  %cond101 = phi float [ %109, %cond.true98 ], [ %110, %cond.false99 ]
  store float %cond101, float* %ecb_limit81, align 4
  br label %if.end103

if.else102:                                       ; preds = %if.end89
  %111 = load float, float* %ecb_limit_1, align 4
  store float %111, float* %ecb_limit81, align 4
  br label %if.end103

if.end103:                                        ; preds = %if.else102, %cond.end100
  %112 = load float, float* %ecb, align 4
  %113 = load float, float* %ecb_limit81, align 4
  %cmp104 = fcmp olt float %112, %113
  br i1 %cmp104, label %cond.true106, label %cond.false107

cond.true106:                                     ; preds = %if.end103
  %114 = load float, float* %ecb, align 4
  br label %cond.end108

cond.false107:                                    ; preds = %if.end103
  %115 = load float, float* %ecb_limit81, align 4
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false107, %cond.true106
  %cond109 = phi float [ %114, %cond.true106 ], [ %115, %cond.false107 ]
  %116 = load float*, float** %thr.addr, align 4
  %117 = load i32, i32* %b, align 4
  %arrayidx110 = getelementptr inbounds float, float* %116, i32 %117
  store float %cond109, float* %arrayidx110, align 4
  br label %if.end111

if.end111:                                        ; preds = %cond.end108, %if.end
  %118 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l1112 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %118, i32 0, i32 0
  %119 = load i32, i32* %chn.addr, align 4
  %arrayidx113 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l1112, i32 0, i32 %119
  %120 = load i32, i32* %b, align 4
  %arrayidx114 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx113, i32 0, i32 %120
  %121 = load float, float* %arrayidx114, align 4
  %122 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l2115 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %122, i32 0, i32 1
  %123 = load i32, i32* %chn.addr, align 4
  %arrayidx116 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l2115, i32 0, i32 %123
  %124 = load i32, i32* %b, align 4
  %arrayidx117 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx116, i32 0, i32 %124
  store float %121, float* %arrayidx117, align 4
  %125 = load float, float* %ecb, align 4
  %126 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l1118 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %126, i32 0, i32 0
  %127 = load i32, i32* %chn.addr, align 4
  %arrayidx119 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l1118, i32 0, i32 %127
  %128 = load i32, i32* %b, align 4
  %arrayidx120 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx119, i32 0, i32 %128
  store float %125, float* %arrayidx120, align 4
  %129 = load i32, i32* %b, align 4
  %arrayidx121 = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 %129
  %130 = load float, float* %arrayidx121, align 4
  store float %130, float* %x, align 4
  %131 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %minval = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %131, i32 0, i32 1
  %132 = load i32, i32* %b, align 4
  %arrayidx122 = getelementptr inbounds [64 x float], [64 x float]* %minval, i32 0, i32 %132
  %133 = load float, float* %arrayidx122, align 4
  %134 = load float, float* %x, align 4
  %mul123 = fmul float %134, %133
  store float %mul123, float* %x, align 4
  %135 = load float, float* %avg_mask, align 4
  %136 = load float, float* %x, align 4
  %mul124 = fmul float %136, %135
  store float %mul124, float* %x, align 4
  %137 = load float*, float** %thr.addr, align 4
  %138 = load i32, i32* %b, align 4
  %arrayidx125 = getelementptr inbounds float, float* %137, i32 %138
  %139 = load float, float* %arrayidx125, align 4
  %140 = load float, float* %x, align 4
  %cmp126 = fcmp ogt float %139, %140
  br i1 %cmp126, label %if.then128, label %if.end130

if.then128:                                       ; preds = %if.end111
  %141 = load float, float* %x, align 4
  %142 = load float*, float** %thr.addr, align 4
  %143 = load i32, i32* %b, align 4
  %arrayidx129 = getelementptr inbounds float, float* %142, i32 %143
  store float %141, float* %arrayidx129, align 4
  br label %if.end130

if.end130:                                        ; preds = %if.then128, %if.end111
  %144 = load float, float* %masking_lower, align 4
  %cmp131 = fcmp ogt float %144, 1.000000e+00
  br i1 %cmp131, label %if.then133, label %if.end136

if.then133:                                       ; preds = %if.end130
  %145 = load float, float* %masking_lower, align 4
  %146 = load float*, float** %thr.addr, align 4
  %147 = load i32, i32* %b, align 4
  %arrayidx134 = getelementptr inbounds float, float* %146, i32 %147
  %148 = load float, float* %arrayidx134, align 4
  %mul135 = fmul float %148, %145
  store float %mul135, float* %arrayidx134, align 4
  br label %if.end136

if.end136:                                        ; preds = %if.then133, %if.end130
  %149 = load float*, float** %thr.addr, align 4
  %150 = load i32, i32* %b, align 4
  %arrayidx137 = getelementptr inbounds float, float* %149, i32 %150
  %151 = load float, float* %arrayidx137, align 4
  %152 = load float*, float** %eb_l.addr, align 4
  %153 = load i32, i32* %b, align 4
  %arrayidx138 = getelementptr inbounds float, float* %152, i32 %153
  %154 = load float, float* %arrayidx138, align 4
  %cmp139 = fcmp ogt float %151, %154
  br i1 %cmp139, label %if.then141, label %if.end144

if.then141:                                       ; preds = %if.end136
  %155 = load float*, float** %eb_l.addr, align 4
  %156 = load i32, i32* %b, align 4
  %arrayidx142 = getelementptr inbounds float, float* %155, i32 %156
  %157 = load float, float* %arrayidx142, align 4
  %158 = load float*, float** %thr.addr, align 4
  %159 = load i32, i32* %b, align 4
  %arrayidx143 = getelementptr inbounds float, float* %158, i32 %159
  store float %157, float* %arrayidx143, align 4
  br label %if.end144

if.end144:                                        ; preds = %if.then141, %if.end136
  %160 = load float, float* %masking_lower, align 4
  %cmp145 = fcmp olt float %160, 1.000000e+00
  br i1 %cmp145, label %if.then147, label %if.end150

if.then147:                                       ; preds = %if.end144
  %161 = load float, float* %masking_lower, align 4
  %162 = load float*, float** %thr.addr, align 4
  %163 = load i32, i32* %b, align 4
  %arrayidx148 = getelementptr inbounds float, float* %162, i32 %163
  %164 = load float, float* %arrayidx148, align 4
  %mul149 = fmul float %164, %161
  store float %mul149, float* %arrayidx148, align 4
  br label %if.end150

if.end150:                                        ; preds = %if.then147, %if.end144
  br label %for.inc

for.inc:                                          ; preds = %if.end150
  %165 = load i32, i32* %b, align 4
  %inc151 = add nsw i32 %165, 1
  store i32 %inc151, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %for.cond152

for.cond152:                                      ; preds = %for.inc158, %for.end
  %166 = load i32, i32* %b, align 4
  %cmp153 = icmp slt i32 %166, 64
  br i1 %cmp153, label %for.body155, label %for.end160

for.body155:                                      ; preds = %for.cond152
  %167 = load float*, float** %eb_l.addr, align 4
  %168 = load i32, i32* %b, align 4
  %arrayidx156 = getelementptr inbounds float, float* %167, i32 %168
  store float 0.000000e+00, float* %arrayidx156, align 4
  %169 = load float*, float** %thr.addr, align 4
  %170 = load i32, i32* %b, align 4
  %arrayidx157 = getelementptr inbounds float, float* %169, i32 %170
  store float 0.000000e+00, float* %arrayidx157, align 4
  br label %for.inc158

for.inc158:                                       ; preds = %for.body155
  %171 = load i32, i32* %b, align 4
  %inc159 = add nsw i32 %171, 1
  store i32 %inc159, i32* %b, align 4
  br label %for.cond152

for.end160:                                       ; preds = %for.cond152
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_MS_thresholds([64 x float]* %eb, [64 x float]* %thr, float* %cb_mld, float* %ath_cb, float %athlower, float %msfix, i32 %n) #0 {
entry:
  %eb.addr = alloca [64 x float]*, align 4
  %thr.addr = alloca [64 x float]*, align 4
  %cb_mld.addr = alloca float*, align 4
  %ath_cb.addr = alloca float*, align 4
  %athlower.addr = alloca float, align 4
  %msfix.addr = alloca float, align 4
  %n.addr = alloca i32, align 4
  %msfix2 = alloca float, align 4
  %rside = alloca float, align 4
  %rmid = alloca float, align 4
  %b = alloca i32, align 4
  %ebM = alloca float, align 4
  %ebS = alloca float, align 4
  %thmL = alloca float, align 4
  %thmR = alloca float, align 4
  %thmM = alloca float, align 4
  %thmS = alloca float, align 4
  %mld_m = alloca float, align 4
  %mld_s = alloca float, align 4
  %tmp_m = alloca float, align 4
  %tmp_s = alloca float, align 4
  %thmLR = alloca float, align 4
  %thmMS = alloca float, align 4
  %ath = alloca float, align 4
  %tmp_l = alloca float, align 4
  %tmp_r = alloca float, align 4
  %f = alloca float, align 4
  store [64 x float]* %eb, [64 x float]** %eb.addr, align 4
  store [64 x float]* %thr, [64 x float]** %thr.addr, align 4
  store float* %cb_mld, float** %cb_mld.addr, align 4
  store float* %ath_cb, float** %ath_cb.addr, align 4
  store float %athlower, float* %athlower.addr, align 4
  store float %msfix, float* %msfix.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load float, float* %msfix.addr, align 4
  %mul = fmul float %0, 2.000000e+00
  store float %mul, float* %msfix2, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %b, align 4
  %2 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load [64 x float]*, [64 x float]** %eb.addr, align 4
  %arrayidx = getelementptr inbounds [64 x float], [64 x float]* %3, i32 2
  %4 = load i32, i32* %b, align 4
  %arrayidx1 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx, i32 0, i32 %4
  %5 = load float, float* %arrayidx1, align 4
  store float %5, float* %ebM, align 4
  %6 = load [64 x float]*, [64 x float]** %eb.addr, align 4
  %arrayidx2 = getelementptr inbounds [64 x float], [64 x float]* %6, i32 3
  %7 = load i32, i32* %b, align 4
  %arrayidx3 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx2, i32 0, i32 %7
  %8 = load float, float* %arrayidx3, align 4
  store float %8, float* %ebS, align 4
  %9 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx4 = getelementptr inbounds [64 x float], [64 x float]* %9, i32 0
  %10 = load i32, i32* %b, align 4
  %arrayidx5 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx4, i32 0, i32 %10
  %11 = load float, float* %arrayidx5, align 4
  store float %11, float* %thmL, align 4
  %12 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx6 = getelementptr inbounds [64 x float], [64 x float]* %12, i32 1
  %13 = load i32, i32* %b, align 4
  %arrayidx7 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx6, i32 0, i32 %13
  %14 = load float, float* %arrayidx7, align 4
  store float %14, float* %thmR, align 4
  %15 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx8 = getelementptr inbounds [64 x float], [64 x float]* %15, i32 2
  %16 = load i32, i32* %b, align 4
  %arrayidx9 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx8, i32 0, i32 %16
  %17 = load float, float* %arrayidx9, align 4
  store float %17, float* %thmM, align 4
  %18 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx10 = getelementptr inbounds [64 x float], [64 x float]* %18, i32 3
  %19 = load i32, i32* %b, align 4
  %arrayidx11 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx10, i32 0, i32 %19
  %20 = load float, float* %arrayidx11, align 4
  store float %20, float* %thmS, align 4
  %21 = load float, float* %thmL, align 4
  %22 = load float, float* %thmR, align 4
  %mul12 = fmul float 0x3FF947AE20000000, %22
  %cmp13 = fcmp ole float %21, %mul12
  br i1 %cmp13, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %23 = load float, float* %thmR, align 4
  %24 = load float, float* %thmL, align 4
  %mul14 = fmul float 0x3FF947AE20000000, %24
  %cmp15 = fcmp ole float %23, %mul14
  br i1 %cmp15, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %25 = load float*, float** %cb_mld.addr, align 4
  %26 = load i32, i32* %b, align 4
  %arrayidx16 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx16, align 4
  %28 = load float, float* %ebS, align 4
  %mul17 = fmul float %27, %28
  store float %mul17, float* %mld_m, align 4
  %29 = load float*, float** %cb_mld.addr, align 4
  %30 = load i32, i32* %b, align 4
  %arrayidx18 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx18, align 4
  %32 = load float, float* %ebM, align 4
  %mul19 = fmul float %31, %32
  store float %mul19, float* %mld_s, align 4
  %33 = load float, float* %thmS, align 4
  %34 = load float, float* %mld_m, align 4
  %cmp20 = fcmp olt float %33, %34
  br i1 %cmp20, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %35 = load float, float* %thmS, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %36 = load float, float* %mld_m, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %35, %cond.true ], [ %36, %cond.false ]
  store float %cond, float* %tmp_m, align 4
  %37 = load float, float* %thmM, align 4
  %38 = load float, float* %mld_s, align 4
  %cmp21 = fcmp olt float %37, %38
  br i1 %cmp21, label %cond.true22, label %cond.false23

cond.true22:                                      ; preds = %cond.end
  %39 = load float, float* %thmM, align 4
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end
  %40 = load float, float* %mld_s, align 4
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true22
  %cond25 = phi float [ %39, %cond.true22 ], [ %40, %cond.false23 ]
  store float %cond25, float* %tmp_s, align 4
  %41 = load float, float* %thmM, align 4
  %42 = load float, float* %tmp_m, align 4
  %cmp26 = fcmp ogt float %41, %42
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %cond.end24
  %43 = load float, float* %thmM, align 4
  br label %cond.end29

cond.false28:                                     ; preds = %cond.end24
  %44 = load float, float* %tmp_m, align 4
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false28, %cond.true27
  %cond30 = phi float [ %43, %cond.true27 ], [ %44, %cond.false28 ]
  store float %cond30, float* %rmid, align 4
  %45 = load float, float* %thmS, align 4
  %46 = load float, float* %tmp_s, align 4
  %cmp31 = fcmp ogt float %45, %46
  br i1 %cmp31, label %cond.true32, label %cond.false33

cond.true32:                                      ; preds = %cond.end29
  %47 = load float, float* %thmS, align 4
  br label %cond.end34

cond.false33:                                     ; preds = %cond.end29
  %48 = load float, float* %tmp_s, align 4
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false33, %cond.true32
  %cond35 = phi float [ %47, %cond.true32 ], [ %48, %cond.false33 ]
  store float %cond35, float* %rside, align 4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %for.body
  %49 = load float, float* %thmM, align 4
  store float %49, float* %rmid, align 4
  %50 = load float, float* %thmS, align 4
  store float %50, float* %rside, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %cond.end34
  %51 = load float, float* %msfix.addr, align 4
  %cmp36 = fcmp ogt float %51, 0.000000e+00
  br i1 %cmp36, label %if.then37, label %if.end84

if.then37:                                        ; preds = %if.end
  %52 = load float*, float** %ath_cb.addr, align 4
  %53 = load i32, i32* %b, align 4
  %arrayidx38 = getelementptr inbounds float, float* %52, i32 %53
  %54 = load float, float* %arrayidx38, align 4
  %55 = load float, float* %athlower.addr, align 4
  %mul39 = fmul float %54, %55
  store float %mul39, float* %ath, align 4
  %56 = load float, float* %thmL, align 4
  %57 = load float, float* %ath, align 4
  %cmp40 = fcmp ogt float %56, %57
  br i1 %cmp40, label %cond.true41, label %cond.false42

cond.true41:                                      ; preds = %if.then37
  %58 = load float, float* %thmL, align 4
  br label %cond.end43

cond.false42:                                     ; preds = %if.then37
  %59 = load float, float* %ath, align 4
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false42, %cond.true41
  %cond44 = phi float [ %58, %cond.true41 ], [ %59, %cond.false42 ]
  store float %cond44, float* %tmp_l, align 4
  %60 = load float, float* %thmR, align 4
  %61 = load float, float* %ath, align 4
  %cmp45 = fcmp ogt float %60, %61
  br i1 %cmp45, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %cond.end43
  %62 = load float, float* %thmR, align 4
  br label %cond.end48

cond.false47:                                     ; preds = %cond.end43
  %63 = load float, float* %ath, align 4
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false47, %cond.true46
  %cond49 = phi float [ %62, %cond.true46 ], [ %63, %cond.false47 ]
  store float %cond49, float* %tmp_r, align 4
  %64 = load float, float* %tmp_l, align 4
  %65 = load float, float* %tmp_r, align 4
  %cmp50 = fcmp olt float %64, %65
  br i1 %cmp50, label %cond.true51, label %cond.false52

cond.true51:                                      ; preds = %cond.end48
  %66 = load float, float* %tmp_l, align 4
  br label %cond.end53

cond.false52:                                     ; preds = %cond.end48
  %67 = load float, float* %tmp_r, align 4
  br label %cond.end53

cond.end53:                                       ; preds = %cond.false52, %cond.true51
  %cond54 = phi float [ %66, %cond.true51 ], [ %67, %cond.false52 ]
  store float %cond54, float* %thmLR, align 4
  %68 = load float, float* %rmid, align 4
  %69 = load float, float* %ath, align 4
  %cmp55 = fcmp ogt float %68, %69
  br i1 %cmp55, label %cond.true56, label %cond.false57

cond.true56:                                      ; preds = %cond.end53
  %70 = load float, float* %rmid, align 4
  br label %cond.end58

cond.false57:                                     ; preds = %cond.end53
  %71 = load float, float* %ath, align 4
  br label %cond.end58

cond.end58:                                       ; preds = %cond.false57, %cond.true56
  %cond59 = phi float [ %70, %cond.true56 ], [ %71, %cond.false57 ]
  store float %cond59, float* %thmM, align 4
  %72 = load float, float* %rside, align 4
  %73 = load float, float* %ath, align 4
  %cmp60 = fcmp ogt float %72, %73
  br i1 %cmp60, label %cond.true61, label %cond.false62

cond.true61:                                      ; preds = %cond.end58
  %74 = load float, float* %rside, align 4
  br label %cond.end63

cond.false62:                                     ; preds = %cond.end58
  %75 = load float, float* %ath, align 4
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true61
  %cond64 = phi float [ %74, %cond.true61 ], [ %75, %cond.false62 ]
  store float %cond64, float* %thmS, align 4
  %76 = load float, float* %thmM, align 4
  %77 = load float, float* %thmS, align 4
  %add = fadd float %76, %77
  store float %add, float* %thmMS, align 4
  %78 = load float, float* %thmMS, align 4
  %cmp65 = fcmp ogt float %78, 0.000000e+00
  br i1 %cmp65, label %land.lhs.true66, label %if.end73

land.lhs.true66:                                  ; preds = %cond.end63
  %79 = load float, float* %thmLR, align 4
  %80 = load float, float* %msfix2, align 4
  %mul67 = fmul float %79, %80
  %81 = load float, float* %thmMS, align 4
  %cmp68 = fcmp olt float %mul67, %81
  br i1 %cmp68, label %if.then69, label %if.end73

if.then69:                                        ; preds = %land.lhs.true66
  %82 = load float, float* %thmLR, align 4
  %83 = load float, float* %msfix2, align 4
  %mul70 = fmul float %82, %83
  %84 = load float, float* %thmMS, align 4
  %div = fdiv float %mul70, %84
  store float %div, float* %f, align 4
  %85 = load float, float* %f, align 4
  %86 = load float, float* %thmM, align 4
  %mul71 = fmul float %86, %85
  store float %mul71, float* %thmM, align 4
  %87 = load float, float* %f, align 4
  %88 = load float, float* %thmS, align 4
  %mul72 = fmul float %88, %87
  store float %mul72, float* %thmS, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.then69, %land.lhs.true66, %cond.end63
  %89 = load float, float* %thmM, align 4
  %90 = load float, float* %rmid, align 4
  %cmp74 = fcmp olt float %89, %90
  br i1 %cmp74, label %cond.true75, label %cond.false76

cond.true75:                                      ; preds = %if.end73
  %91 = load float, float* %thmM, align 4
  br label %cond.end77

cond.false76:                                     ; preds = %if.end73
  %92 = load float, float* %rmid, align 4
  br label %cond.end77

cond.end77:                                       ; preds = %cond.false76, %cond.true75
  %cond78 = phi float [ %91, %cond.true75 ], [ %92, %cond.false76 ]
  store float %cond78, float* %rmid, align 4
  %93 = load float, float* %thmS, align 4
  %94 = load float, float* %rside, align 4
  %cmp79 = fcmp olt float %93, %94
  br i1 %cmp79, label %cond.true80, label %cond.false81

cond.true80:                                      ; preds = %cond.end77
  %95 = load float, float* %thmS, align 4
  br label %cond.end82

cond.false81:                                     ; preds = %cond.end77
  %96 = load float, float* %rside, align 4
  br label %cond.end82

cond.end82:                                       ; preds = %cond.false81, %cond.true80
  %cond83 = phi float [ %95, %cond.true80 ], [ %96, %cond.false81 ]
  store float %cond83, float* %rside, align 4
  br label %if.end84

if.end84:                                         ; preds = %cond.end82, %if.end
  %97 = load float, float* %rmid, align 4
  %98 = load float, float* %ebM, align 4
  %cmp85 = fcmp ogt float %97, %98
  br i1 %cmp85, label %if.then86, label %if.end87

if.then86:                                        ; preds = %if.end84
  %99 = load float, float* %ebM, align 4
  store float %99, float* %rmid, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.then86, %if.end84
  %100 = load float, float* %rside, align 4
  %101 = load float, float* %ebS, align 4
  %cmp88 = fcmp ogt float %100, %101
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.end87
  %102 = load float, float* %ebS, align 4
  store float %102, float* %rside, align 4
  br label %if.end90

if.end90:                                         ; preds = %if.then89, %if.end87
  %103 = load float, float* %rmid, align 4
  %104 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx91 = getelementptr inbounds [64 x float], [64 x float]* %104, i32 2
  %105 = load i32, i32* %b, align 4
  %arrayidx92 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx91, i32 0, i32 %105
  store float %103, float* %arrayidx92, align 4
  %106 = load float, float* %rside, align 4
  %107 = load [64 x float]*, [64 x float]** %thr.addr, align 4
  %arrayidx93 = getelementptr inbounds [64 x float], [64 x float]* %107, i32 3
  %108 = load i32, i32* %b, align 4
  %arrayidx94 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx93, i32 0, i32 %108
  store float %106, float* %arrayidx94, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end90
  %109 = load i32, i32* %b, align 4
  %inc = add nsw i32 %109, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @convert_partition2scalefac_l(%struct.lame_internal_flags* %gfc, float* %eb, float* %thr, i32 %chn) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %eb.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gdl = alloca %struct.PsyConst_CB2SB_t*, align 4
  %enn = alloca float*, align 4
  %thm = alloca float*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %l = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %2, i32 0, i32 2
  store %struct.PsyConst_CB2SB_t* %l, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %3 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %3, i32 0, i32 5
  %4 = load i32, i32* %chn.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en, i32 0, i32 %4
  %l1 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [22 x float], [22 x float]* %l1, i32 0, i32 0
  store float* %arrayidx2, float** %enn, align 4
  %5 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm3 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %5, i32 0, i32 4
  %6 = load i32, i32* %chn.addr, align 4
  %arrayidx4 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm3, i32 0, i32 %6
  %l5 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx4, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [22 x float], [22 x float]* %l5, i32 0, i32 0
  store float* %arrayidx6, float** %thm, align 4
  %7 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %8 = load float*, float** %eb.addr, align 4
  %9 = load float*, float** %thr.addr, align 4
  %10 = load float*, float** %enn, align 4
  %11 = load float*, float** %thm, align 4
  call void @convert_partition2scalefac(%struct.PsyConst_CB2SB_t* %7, float* %8, float* %9, float* %10, float* %11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @convert_partition2scalefac_l_to_s(%struct.lame_internal_flags* %gfc, float* %eb, float* %thr, i32 %chn) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %eb.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gds = alloca %struct.PsyConst_CB2SB_t*, align 4
  %enn = alloca [13 x float], align 16
  %thm = alloca [13 x float], align 16
  %sb = alloca i32, align 4
  %sblock = alloca i32, align 4
  %scale = alloca float, align 4
  %tmp_enn = alloca float, align 4
  %tmp_thm = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %l_to_s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %2, i32 0, i32 4
  store %struct.PsyConst_CB2SB_t* %l_to_s, %struct.PsyConst_CB2SB_t** %gds, align 4
  %3 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %4 = load float*, float** %eb.addr, align 4
  %5 = load float*, float** %thr.addr, align 4
  %arraydecay = getelementptr inbounds [13 x float], [13 x float]* %enn, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [13 x float], [13 x float]* %thm, i32 0, i32 0
  call void @convert_partition2scalefac(%struct.PsyConst_CB2SB_t* %3, float* %4, float* %5, float* %arraydecay, float* %arraydecay1)
  store i32 0, i32* %sb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %6 = load i32, i32* %sb, align 4
  %cmp = icmp slt i32 %6, 13
  br i1 %cmp, label %for.body, label %for.end16

for.body:                                         ; preds = %for.cond
  store float 1.562500e-02, float* %scale, align 4
  %7 = load i32, i32* %sb, align 4
  %arrayidx = getelementptr inbounds [13 x float], [13 x float]* %enn, i32 0, i32 %7
  %8 = load float, float* %arrayidx, align 4
  store float %8, float* %tmp_enn, align 4
  %9 = load i32, i32* %sb, align 4
  %arrayidx2 = getelementptr inbounds [13 x float], [13 x float]* %thm, i32 0, i32 %9
  %10 = load float, float* %arrayidx2, align 4
  %mul = fmul float %10, 1.562500e-02
  store float %mul, float* %tmp_thm, align 4
  store i32 0, i32* %sblock, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %sblock, align 4
  %cmp4 = icmp slt i32 %11, 3
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond3
  %12 = load float, float* %tmp_enn, align 4
  %13 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %13, i32 0, i32 5
  %14 = load i32, i32* %chn.addr, align 4
  %arrayidx6 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en, i32 0, i32 %14
  %s = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx6, i32 0, i32 1
  %15 = load i32, i32* %sb, align 4
  %arrayidx7 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s, i32 0, i32 %15
  %16 = load i32, i32* %sblock, align 4
  %arrayidx8 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx7, i32 0, i32 %16
  store float %12, float* %arrayidx8, align 4
  %17 = load float, float* %tmp_thm, align 4
  %18 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm9 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %18, i32 0, i32 4
  %19 = load i32, i32* %chn.addr, align 4
  %arrayidx10 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm9, i32 0, i32 %19
  %s11 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx10, i32 0, i32 1
  %20 = load i32, i32* %sb, align 4
  %arrayidx12 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s11, i32 0, i32 %20
  %21 = load i32, i32* %sblock, align 4
  %arrayidx13 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx12, i32 0, i32 %21
  store float %17, float* %arrayidx13, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %22 = load i32, i32* %sblock, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %sblock, align 4
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc14

for.inc14:                                        ; preds = %for.end
  %23 = load i32, i32* %sb, align 4
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %sb, align 4
  br label %for.cond

for.end16:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_skip_masking_s(%struct.lame_internal_flags* %gfc, i32 %chn, i32 %sblock) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %chn.addr = alloca i32, align 4
  %sblock.addr = alloca i32, align 4
  %nbs2 = alloca float*, align 4
  %nbs1 = alloca float*, align 4
  %n = alloca i32, align 4
  %b = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store i32 %sblock, i32* %sblock.addr, align 4
  %0 = load i32, i32* %sblock.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 9
  %nb_s2 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %sv_psy, i32 0, i32 3
  %2 = load i32, i32* %chn.addr, align 4
  %arrayidx = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s2, i32 0, i32 %2
  %arrayidx1 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx, i32 0, i32 0
  store float* %arrayidx1, float** %nbs2, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 9
  %nb_s1 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %sv_psy2, i32 0, i32 2
  %4 = load i32, i32* %chn.addr, align 4
  %arrayidx3 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s1, i32 0, i32 %4
  %arrayidx4 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx3, i32 0, i32 0
  store float* %arrayidx4, float** %nbs1, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 22
  %6 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %6, i32 0, i32 3
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s, i32 0, i32 11
  %7 = load i32, i32* %npart, align 4
  store i32 %7, i32* %n, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %b, align 4
  %9 = load i32, i32* %n, align 4
  %cmp5 = icmp slt i32 %8, %9
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load float*, float** %nbs1, align 4
  %11 = load i32, i32* %b, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 %11
  %12 = load float, float* %arrayidx6, align 4
  %13 = load float*, float** %nbs2, align 4
  %14 = load i32, i32* %b, align 4
  %arrayidx7 = getelementptr inbounds float, float* %13, i32 %14
  store float %12, float* %arrayidx7, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %b, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_fft_s(%struct.lame_internal_flags* %gfc, float** %buffer, i32 %chn, i32 %sblock, [129 x float]* %fftenergy_s, [3 x [256 x float]]* %wsamp_s) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca float**, align 4
  %chn.addr = alloca i32, align 4
  %sblock.addr = alloca i32, align 4
  %fftenergy_s.addr = alloca [129 x float]*, align 4
  %wsamp_s.addr = alloca [3 x [256 x float]]*, align 4
  %j = alloca i32, align 4
  %sqrt2_half = alloca float, align 4
  %l = alloca float, align 4
  %r = alloca float, align 4
  %re = alloca float, align 4
  %im = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store i32 %sblock, i32* %sblock.addr, align 4
  store [129 x float]* %fftenergy_s, [129 x float]** %fftenergy_s.addr, align 4
  store [3 x [256 x float]]* %wsamp_s, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %0 = load i32, i32* %sblock.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %chn.addr, align 4
  %cmp1 = icmp slt i32 %1, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %3 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %arraydecay = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %3, i32 0, i32 0
  %4 = load i32, i32* %chn.addr, align 4
  %5 = load float**, float*** %buffer.addr, align 4
  call void @fft_short(%struct.lame_internal_flags* %2, [256 x float]* %arraydecay, i32 %4, float** %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %6 = load i32, i32* %chn.addr, align 4
  %cmp2 = icmp eq i32 %6, 2
  br i1 %cmp2, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.end
  store float 0x3FE6A09E60000000, float* %sqrt2_half, align 4
  store i32 255, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %7 = load i32, i32* %j, align 4
  %cmp4 = icmp sge i32 %7, 0
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %arrayidx = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %8, i32 0
  %9 = load i32, i32* %sblock.addr, align 4
  %arrayidx5 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %arrayidx, i32 0, i32 %9
  %10 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx5, i32 0, i32 %10
  %11 = load float, float* %arrayidx6, align 4
  store float %11, float* %l, align 4
  %12 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %arrayidx7 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %12, i32 1
  %13 = load i32, i32* %sblock.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %arrayidx7, i32 0, i32 %13
  %14 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx8, i32 0, i32 %14
  %15 = load float, float* %arrayidx9, align 4
  store float %15, float* %r, align 4
  %16 = load float, float* %l, align 4
  %17 = load float, float* %r, align 4
  %add = fadd float %16, %17
  %mul = fmul float %add, 0x3FE6A09E60000000
  %18 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %arrayidx10 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %18, i32 0
  %19 = load i32, i32* %sblock.addr, align 4
  %arrayidx11 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %arrayidx10, i32 0, i32 %19
  %20 = load i32, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx11, i32 0, i32 %20
  store float %mul, float* %arrayidx12, align 4
  %21 = load float, float* %l, align 4
  %22 = load float, float* %r, align 4
  %sub = fsub float %21, %22
  %mul13 = fmul float %sub, 0x3FE6A09E60000000
  %23 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %arrayidx14 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %23, i32 1
  %24 = load i32, i32* %sblock.addr, align 4
  %arrayidx15 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %arrayidx14, i32 0, i32 %24
  %25 = load i32, i32* %j, align 4
  %arrayidx16 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx15, i32 0, i32 %25
  store float %mul13, float* %arrayidx16, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %j, align 4
  %dec = add nsw i32 %26, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end17

if.end17:                                         ; preds = %for.end, %if.end
  %27 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %28 = load i32, i32* %sblock.addr, align 4
  %arrayidx18 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %27, i32 0, i32 %28
  %arrayidx19 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx18, i32 0, i32 0
  %29 = load float, float* %arrayidx19, align 4
  %30 = load [129 x float]*, [129 x float]** %fftenergy_s.addr, align 4
  %31 = load i32, i32* %sblock.addr, align 4
  %arrayidx20 = getelementptr inbounds [129 x float], [129 x float]* %30, i32 %31
  %arrayidx21 = getelementptr inbounds [129 x float], [129 x float]* %arrayidx20, i32 0, i32 0
  store float %29, float* %arrayidx21, align 4
  %32 = load [129 x float]*, [129 x float]** %fftenergy_s.addr, align 4
  %33 = load i32, i32* %sblock.addr, align 4
  %arrayidx22 = getelementptr inbounds [129 x float], [129 x float]* %32, i32 %33
  %arrayidx23 = getelementptr inbounds [129 x float], [129 x float]* %arrayidx22, i32 0, i32 0
  %34 = load float, float* %arrayidx23, align 4
  %35 = load [129 x float]*, [129 x float]** %fftenergy_s.addr, align 4
  %36 = load i32, i32* %sblock.addr, align 4
  %arrayidx24 = getelementptr inbounds [129 x float], [129 x float]* %35, i32 %36
  %arrayidx25 = getelementptr inbounds [129 x float], [129 x float]* %arrayidx24, i32 0, i32 0
  %37 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %37, %34
  store float %mul26, float* %arrayidx25, align 4
  store i32 127, i32* %j, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc43, %if.end17
  %38 = load i32, i32* %j, align 4
  %cmp28 = icmp sge i32 %38, 0
  br i1 %cmp28, label %for.body29, label %for.end45

for.body29:                                       ; preds = %for.cond27
  %39 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %40 = load i32, i32* %sblock.addr, align 4
  %arrayidx30 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %39, i32 0, i32 %40
  %41 = load i32, i32* %j, align 4
  %sub31 = sub nsw i32 128, %41
  %arrayidx32 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx30, i32 0, i32 %sub31
  %42 = load float, float* %arrayidx32, align 4
  store float %42, float* %re, align 4
  %43 = load [3 x [256 x float]]*, [3 x [256 x float]]** %wsamp_s.addr, align 4
  %44 = load i32, i32* %sblock.addr, align 4
  %arrayidx33 = getelementptr inbounds [3 x [256 x float]], [3 x [256 x float]]* %43, i32 0, i32 %44
  %45 = load i32, i32* %j, align 4
  %add34 = add nsw i32 128, %45
  %arrayidx35 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx33, i32 0, i32 %add34
  %46 = load float, float* %arrayidx35, align 4
  store float %46, float* %im, align 4
  %47 = load float, float* %re, align 4
  %48 = load float, float* %re, align 4
  %mul36 = fmul float %47, %48
  %49 = load float, float* %im, align 4
  %50 = load float, float* %im, align 4
  %mul37 = fmul float %49, %50
  %add38 = fadd float %mul36, %mul37
  %mul39 = fmul float %add38, 5.000000e-01
  %51 = load [129 x float]*, [129 x float]** %fftenergy_s.addr, align 4
  %52 = load i32, i32* %sblock.addr, align 4
  %arrayidx40 = getelementptr inbounds [129 x float], [129 x float]* %51, i32 %52
  %53 = load i32, i32* %j, align 4
  %sub41 = sub nsw i32 128, %53
  %arrayidx42 = getelementptr inbounds [129 x float], [129 x float]* %arrayidx40, i32 0, i32 %sub41
  store float %mul39, float* %arrayidx42, align 4
  br label %for.inc43

for.inc43:                                        ; preds = %for.body29
  %54 = load i32, i32* %j, align 4
  %dec44 = add nsw i32 %54, -1
  store i32 %dec44, i32* %j, align 4
  br label %for.cond27

for.end45:                                        ; preds = %for.cond27
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_compute_masking_s(%struct.lame_internal_flags* %gfc, [129 x float]* %fftenergy_s, float* %eb, float* %thr, i32 %chn, i32 %sblock) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %fftenergy_s.addr = alloca [129 x float]*, align 4
  %eb.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %sblock.addr = alloca i32, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gds = alloca %struct.PsyConst_CB2SB_t*, align 4
  %max = alloca [64 x float], align 16
  %avg = alloca [64 x float], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %b = alloca i32, align 4
  %mask_idx_s = alloca [64 x i8], align 16
  %ebb = alloca float, align 4
  %m = alloca float, align 4
  %n = alloca i32, align 4
  %el = alloca float, align 4
  %kk = alloca i32, align 4
  %last = alloca i32, align 4
  %delta = alloca i32, align 4
  %dd = alloca i32, align 4
  %dd_n = alloca i32, align 4
  %x = alloca float, align 4
  %ecb = alloca float, align 4
  %avg_mask = alloca float, align 4
  %masking_lower = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [129 x float]* %fftenergy_s, [129 x float]** %fftenergy_s.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store i32 %sblock, i32* %sblock.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %2, i32 0, i32 3
  store %struct.PsyConst_CB2SB_t* %s, %struct.PsyConst_CB2SB_t** %gds, align 4
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 0
  %3 = bitcast float* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 256, i1 false)
  %arraydecay1 = getelementptr inbounds [64 x float], [64 x float]* %avg, i32 0, i32 0
  %4 = bitcast float* %arraydecay1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 256, i1 false)
  store i32 0, i32* %j, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %entry
  %5 = load i32, i32* %b, align 4
  %6 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %6, i32 0, i32 11
  %7 = load i32, i32* %npart, align 4
  %cmp = icmp slt i32 %5, %7
  br i1 %cmp, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %ebb, align 4
  store float 0.000000e+00, float* %m, align 4
  %8 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %8, i32 0, i32 8
  %9 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx, align 4
  store i32 %10, i32* %n, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %n, align 4
  %cmp3 = icmp slt i32 %11, %12
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %13 = load [129 x float]*, [129 x float]** %fftenergy_s.addr, align 4
  %14 = load i32, i32* %sblock.addr, align 4
  %arrayidx5 = getelementptr inbounds [129 x float], [129 x float]* %13, i32 %14
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds [129 x float], [129 x float]* %arrayidx5, i32 0, i32 %15
  %16 = load float, float* %arrayidx6, align 4
  store float %16, float* %el, align 4
  %17 = load float, float* %el, align 4
  %18 = load float, float* %ebb, align 4
  %add = fadd float %18, %17
  store float %add, float* %ebb, align 4
  %19 = load float, float* %m, align 4
  %20 = load float, float* %el, align 4
  %cmp7 = fcmp olt float %19, %20
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %21 = load float, float* %el, align 4
  store float %21, float* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %inc8 = add nsw i32 %23, 1
  store i32 %inc8, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %24 = load float, float* %ebb, align 4
  %25 = load float*, float** %eb.addr, align 4
  %26 = load i32, i32* %b, align 4
  %arrayidx9 = getelementptr inbounds float, float* %25, i32 %26
  store float %24, float* %arrayidx9, align 4
  %27 = load float, float* %m, align 4
  %28 = load i32, i32* %b, align 4
  %arrayidx10 = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 %28
  store float %27, float* %arrayidx10, align 4
  %29 = load float, float* %ebb, align 4
  %30 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %rnumlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %30, i32 0, i32 2
  %31 = load i32, i32* %b, align 4
  %arrayidx11 = getelementptr inbounds [64 x float], [64 x float]* %rnumlines, i32 0, i32 %31
  %32 = load float, float* %arrayidx11, align 4
  %mul = fmul float %29, %32
  %33 = load i32, i32* %b, align 4
  %arrayidx12 = getelementptr inbounds [64 x float], [64 x float]* %avg, i32 0, i32 %33
  store float %mul, float* %arrayidx12, align 4
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %34 = load i32, i32* %b, align 4
  %inc14 = add nsw i32 %34, 1
  store i32 %inc14, i32* %b, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay16 = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 0
  %arraydecay17 = getelementptr inbounds [64 x float], [64 x float]* %avg, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 0
  call void @vbrpsy_calc_mask_index_s(%struct.lame_internal_flags* %35, float* %arraydecay16, float* %arraydecay17, i8* %arraydecay18)
  store i32 0, i32* %b, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc104, %for.end15
  %36 = load i32, i32* %b, align 4
  %37 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %npart20 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %37, i32 0, i32 11
  %38 = load i32, i32* %npart20, align 4
  %cmp21 = icmp slt i32 %36, %38
  br i1 %cmp21, label %for.body22, label %for.end106

for.body22:                                       ; preds = %for.cond19
  %39 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %s3ind = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %39, i32 0, i32 7
  %40 = load i32, i32* %b, align 4
  %arrayidx23 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind, i32 0, i32 %40
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx23, i32 0, i32 0
  %41 = load i32, i32* %arrayidx24, align 4
  store i32 %41, i32* %kk, align 4
  %42 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %s3ind25 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %42, i32 0, i32 7
  %43 = load i32, i32* %b, align 4
  %arrayidx26 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind25, i32 0, i32 %43
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx26, i32 0, i32 1
  %44 = load i32, i32* %arrayidx27, align 4
  store i32 %44, i32* %last, align 4
  %45 = load i32, i32* %b, align 4
  %arrayidx28 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx28, align 1
  %conv = zext i8 %46 to i32
  %call = call i32 @mask_add_delta(i32 %conv)
  store i32 %call, i32* %delta, align 4
  %47 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %masking_lower29 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %47, i32 0, i32 0
  %48 = load i32, i32* %b, align 4
  %arrayidx30 = getelementptr inbounds [64 x float], [64 x float]* %masking_lower29, i32 0, i32 %48
  %49 = load float, float* %arrayidx30, align 4
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %50, i32 0, i32 13
  %masking_lower31 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 2
  %51 = load float, float* %masking_lower31, align 4
  %mul32 = fmul float %49, %51
  store float %mul32, float* %masking_lower, align 4
  %52 = load i32, i32* %kk, align 4
  %arrayidx33 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 %52
  %53 = load i8, i8* %arrayidx33, align 1
  %conv34 = zext i8 %53 to i32
  store i32 %conv34, i32* %dd, align 4
  store i32 1, i32* %dd_n, align 4
  %54 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %s3 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %54, i32 0, i32 13
  %55 = load float*, float** %s3, align 4
  %56 = load i32, i32* %j, align 4
  %arrayidx35 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx35, align 4
  %58 = load float*, float** %eb.addr, align 4
  %59 = load i32, i32* %kk, align 4
  %arrayidx36 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %57, %60
  %61 = load i32, i32* %kk, align 4
  %arrayidx38 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 %61
  %62 = load i8, i8* %arrayidx38, align 1
  %idxprom = zext i8 %62 to i32
  %arrayidx39 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %idxprom
  %63 = load float, float* %arrayidx39, align 4
  %mul40 = fmul float %mul37, %63
  store float %mul40, float* %ecb, align 4
  %64 = load i32, i32* %j, align 4
  %inc41 = add nsw i32 %64, 1
  store i32 %inc41, i32* %j, align 4
  %65 = load i32, i32* %kk, align 4
  %inc42 = add nsw i32 %65, 1
  store i32 %inc42, i32* %kk, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body22
  %66 = load i32, i32* %kk, align 4
  %67 = load i32, i32* %last, align 4
  %cmp43 = icmp sle i32 %66, %67
  br i1 %cmp43, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %68 = load i32, i32* %kk, align 4
  %arrayidx45 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 %68
  %69 = load i8, i8* %arrayidx45, align 1
  %conv46 = zext i8 %69 to i32
  %70 = load i32, i32* %dd, align 4
  %add47 = add nsw i32 %70, %conv46
  store i32 %add47, i32* %dd, align 4
  %71 = load i32, i32* %dd_n, align 4
  %add48 = add nsw i32 %71, 1
  store i32 %add48, i32* %dd_n, align 4
  %72 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %s349 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %72, i32 0, i32 13
  %73 = load float*, float** %s349, align 4
  %74 = load i32, i32* %j, align 4
  %arrayidx50 = getelementptr inbounds float, float* %73, i32 %74
  %75 = load float, float* %arrayidx50, align 4
  %76 = load float*, float** %eb.addr, align 4
  %77 = load i32, i32* %kk, align 4
  %arrayidx51 = getelementptr inbounds float, float* %76, i32 %77
  %78 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %75, %78
  %79 = load i32, i32* %kk, align 4
  %arrayidx53 = getelementptr inbounds [64 x i8], [64 x i8]* %mask_idx_s, i32 0, i32 %79
  %80 = load i8, i8* %arrayidx53, align 1
  %idxprom54 = zext i8 %80 to i32
  %arrayidx55 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %idxprom54
  %81 = load float, float* %arrayidx55, align 4
  %mul56 = fmul float %mul52, %81
  store float %mul56, float* %x, align 4
  %82 = load float, float* %ecb, align 4
  %83 = load float, float* %x, align 4
  %84 = load i32, i32* %kk, align 4
  %85 = load i32, i32* %b, align 4
  %sub = sub nsw i32 %84, %85
  %86 = load i32, i32* %delta, align 4
  %call57 = call float @vbrpsy_mask_add(float %82, float %83, i32 %sub, i32 %86)
  store float %call57, float* %ecb, align 4
  %87 = load i32, i32* %j, align 4
  %inc58 = add nsw i32 %87, 1
  store i32 %inc58, i32* %j, align 4
  %88 = load i32, i32* %kk, align 4
  %inc59 = add nsw i32 %88, 1
  store i32 %inc59, i32* %kk, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %89 = load i32, i32* %dd, align 4
  %mul60 = mul nsw i32 2, %89
  %add61 = add nsw i32 1, %mul60
  %90 = load i32, i32* %dd_n, align 4
  %mul62 = mul nsw i32 2, %90
  %div = sdiv i32 %add61, %mul62
  store i32 %div, i32* %dd, align 4
  %91 = load i32, i32* %dd, align 4
  %arrayidx63 = getelementptr inbounds [9 x float], [9 x float]* @tab, i32 0, i32 %91
  %92 = load float, float* %arrayidx63, align 4
  %mul64 = fmul float %92, 5.000000e-01
  store float %mul64, float* %avg_mask, align 4
  %93 = load float, float* %avg_mask, align 4
  %94 = load float, float* %ecb, align 4
  %mul65 = fmul float %94, %93
  store float %mul65, float* %ecb, align 4
  %95 = load float, float* %ecb, align 4
  %96 = load float*, float** %thr.addr, align 4
  %97 = load i32, i32* %b, align 4
  %arrayidx66 = getelementptr inbounds float, float* %96, i32 %97
  store float %95, float* %arrayidx66, align 4
  %98 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_s1 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %98, i32 0, i32 2
  %99 = load i32, i32* %chn.addr, align 4
  %arrayidx67 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s1, i32 0, i32 %99
  %100 = load i32, i32* %b, align 4
  %arrayidx68 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx67, i32 0, i32 %100
  %101 = load float, float* %arrayidx68, align 4
  %102 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_s2 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %102, i32 0, i32 3
  %103 = load i32, i32* %chn.addr, align 4
  %arrayidx69 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s2, i32 0, i32 %103
  %104 = load i32, i32* %b, align 4
  %arrayidx70 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx69, i32 0, i32 %104
  store float %101, float* %arrayidx70, align 4
  %105 = load float, float* %ecb, align 4
  %106 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_s171 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %106, i32 0, i32 2
  %107 = load i32, i32* %chn.addr, align 4
  %arrayidx72 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s171, i32 0, i32 %107
  %108 = load i32, i32* %b, align 4
  %arrayidx73 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx72, i32 0, i32 %108
  store float %105, float* %arrayidx73, align 4
  %109 = load i32, i32* %b, align 4
  %arrayidx74 = getelementptr inbounds [64 x float], [64 x float]* %max, i32 0, i32 %109
  %110 = load float, float* %arrayidx74, align 4
  store float %110, float* %x, align 4
  %111 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %minval = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %111, i32 0, i32 1
  %112 = load i32, i32* %b, align 4
  %arrayidx75 = getelementptr inbounds [64 x float], [64 x float]* %minval, i32 0, i32 %112
  %113 = load float, float* %arrayidx75, align 4
  %114 = load float, float* %x, align 4
  %mul76 = fmul float %114, %113
  store float %mul76, float* %x, align 4
  %115 = load float, float* %avg_mask, align 4
  %116 = load float, float* %x, align 4
  %mul77 = fmul float %116, %115
  store float %mul77, float* %x, align 4
  %117 = load float*, float** %thr.addr, align 4
  %118 = load i32, i32* %b, align 4
  %arrayidx78 = getelementptr inbounds float, float* %117, i32 %118
  %119 = load float, float* %arrayidx78, align 4
  %120 = load float, float* %x, align 4
  %cmp79 = fcmp ogt float %119, %120
  br i1 %cmp79, label %if.then81, label %if.end83

if.then81:                                        ; preds = %while.end
  %121 = load float, float* %x, align 4
  %122 = load float*, float** %thr.addr, align 4
  %123 = load i32, i32* %b, align 4
  %arrayidx82 = getelementptr inbounds float, float* %122, i32 %123
  store float %121, float* %arrayidx82, align 4
  br label %if.end83

if.end83:                                         ; preds = %if.then81, %while.end
  %124 = load float, float* %masking_lower, align 4
  %cmp84 = fcmp ogt float %124, 1.000000e+00
  br i1 %cmp84, label %if.then86, label %if.end89

if.then86:                                        ; preds = %if.end83
  %125 = load float, float* %masking_lower, align 4
  %126 = load float*, float** %thr.addr, align 4
  %127 = load i32, i32* %b, align 4
  %arrayidx87 = getelementptr inbounds float, float* %126, i32 %127
  %128 = load float, float* %arrayidx87, align 4
  %mul88 = fmul float %128, %125
  store float %mul88, float* %arrayidx87, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then86, %if.end83
  %129 = load float*, float** %thr.addr, align 4
  %130 = load i32, i32* %b, align 4
  %arrayidx90 = getelementptr inbounds float, float* %129, i32 %130
  %131 = load float, float* %arrayidx90, align 4
  %132 = load float*, float** %eb.addr, align 4
  %133 = load i32, i32* %b, align 4
  %arrayidx91 = getelementptr inbounds float, float* %132, i32 %133
  %134 = load float, float* %arrayidx91, align 4
  %cmp92 = fcmp ogt float %131, %134
  br i1 %cmp92, label %if.then94, label %if.end97

if.then94:                                        ; preds = %if.end89
  %135 = load float*, float** %eb.addr, align 4
  %136 = load i32, i32* %b, align 4
  %arrayidx95 = getelementptr inbounds float, float* %135, i32 %136
  %137 = load float, float* %arrayidx95, align 4
  %138 = load float*, float** %thr.addr, align 4
  %139 = load i32, i32* %b, align 4
  %arrayidx96 = getelementptr inbounds float, float* %138, i32 %139
  store float %137, float* %arrayidx96, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then94, %if.end89
  %140 = load float, float* %masking_lower, align 4
  %cmp98 = fcmp olt float %140, 1.000000e+00
  br i1 %cmp98, label %if.then100, label %if.end103

if.then100:                                       ; preds = %if.end97
  %141 = load float, float* %masking_lower, align 4
  %142 = load float*, float** %thr.addr, align 4
  %143 = load i32, i32* %b, align 4
  %arrayidx101 = getelementptr inbounds float, float* %142, i32 %143
  %144 = load float, float* %arrayidx101, align 4
  %mul102 = fmul float %144, %141
  store float %mul102, float* %arrayidx101, align 4
  br label %if.end103

if.end103:                                        ; preds = %if.then100, %if.end97
  br label %for.inc104

for.inc104:                                       ; preds = %if.end103
  %145 = load i32, i32* %b, align 4
  %inc105 = add nsw i32 %145, 1
  store i32 %inc105, i32* %b, align 4
  br label %for.cond19

for.end106:                                       ; preds = %for.cond19
  br label %for.cond107

for.cond107:                                      ; preds = %for.inc113, %for.end106
  %146 = load i32, i32* %b, align 4
  %cmp108 = icmp slt i32 %146, 64
  br i1 %cmp108, label %for.body110, label %for.end115

for.body110:                                      ; preds = %for.cond107
  %147 = load float*, float** %eb.addr, align 4
  %148 = load i32, i32* %b, align 4
  %arrayidx111 = getelementptr inbounds float, float* %147, i32 %148
  store float 0.000000e+00, float* %arrayidx111, align 4
  %149 = load float*, float** %thr.addr, align 4
  %150 = load i32, i32* %b, align 4
  %arrayidx112 = getelementptr inbounds float, float* %149, i32 %150
  store float 0.000000e+00, float* %arrayidx112, align 4
  br label %for.inc113

for.inc113:                                       ; preds = %for.body110
  %151 = load i32, i32* %b, align 4
  %inc114 = add nsw i32 %151, 1
  store i32 %inc114, i32* %b, align 4
  br label %for.cond107

for.end115:                                       ; preds = %for.cond107
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @convert_partition2scalefac_s(%struct.lame_internal_flags* %gfc, float* %eb, float* %thr, i32 %chn, i32 %sblock) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %eb.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %sblock.addr = alloca i32, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gds = alloca %struct.PsyConst_CB2SB_t*, align 4
  %enn = alloca [13 x float], align 16
  %thm = alloca [13 x float], align 16
  %sb = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store i32 %sblock, i32* %sblock.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %2, i32 0, i32 3
  store %struct.PsyConst_CB2SB_t* %s, %struct.PsyConst_CB2SB_t** %gds, align 4
  %3 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %4 = load float*, float** %eb.addr, align 4
  %5 = load float*, float** %thr.addr, align 4
  %arraydecay = getelementptr inbounds [13 x float], [13 x float]* %enn, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [13 x float], [13 x float]* %thm, i32 0, i32 0
  call void @convert_partition2scalefac(%struct.PsyConst_CB2SB_t* %3, float* %4, float* %5, float* %arraydecay, float* %arraydecay1)
  store i32 0, i32* %sb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %sb, align 4
  %cmp = icmp slt i32 %6, 13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32, i32* %sb, align 4
  %arrayidx = getelementptr inbounds [13 x float], [13 x float]* %enn, i32 0, i32 %7
  %8 = load float, float* %arrayidx, align 4
  %9 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %9, i32 0, i32 5
  %10 = load i32, i32* %chn.addr, align 4
  %arrayidx2 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en, i32 0, i32 %10
  %s3 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx2, i32 0, i32 1
  %11 = load i32, i32* %sb, align 4
  %arrayidx4 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s3, i32 0, i32 %11
  %12 = load i32, i32* %sblock.addr, align 4
  %arrayidx5 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx4, i32 0, i32 %12
  store float %8, float* %arrayidx5, align 4
  %13 = load i32, i32* %sb, align 4
  %arrayidx6 = getelementptr inbounds [13 x float], [13 x float]* %thm, i32 0, i32 %13
  %14 = load float, float* %arrayidx6, align 4
  %15 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm7 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %15, i32 0, i32 4
  %16 = load i32, i32* %chn.addr, align 4
  %arrayidx8 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm7, i32 0, i32 %16
  %s9 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx8, i32 0, i32 1
  %17 = load i32, i32* %sb, align 4
  %arrayidx10 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s9, i32 0, i32 %17
  %18 = load i32, i32* %sblock.addr, align 4
  %arrayidx11 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx10, i32 0, i32 %18
  store float %14, float* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %sb, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %sb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal float @NS_INTERP(float %x, float %y, float %r) #0 {
entry:
  %retval = alloca float, align 4
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %r.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %r, float* %r.addr, align 4
  %0 = load float, float* %r.addr, align 4
  %cmp = fcmp oge float %0, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %x.addr, align 4
  store float %1, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load float, float* %r.addr, align 4
  %cmp1 = fcmp ole float %2, 0.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load float, float* %y.addr, align 4
  store float %3, float* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load float, float* %y.addr, align 4
  %cmp4 = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  %5 = load float, float* %x.addr, align 4
  %6 = load float, float* %y.addr, align 4
  %div = fdiv float %5, %6
  %7 = load float, float* %r.addr, align 4
  %8 = call float @llvm.pow.f32(float %div, float %7)
  %9 = load float, float* %y.addr, align 4
  %mul = fmul float %8, %9
  store float %mul, float* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then2, %if.then
  %10 = load float, float* %retval, align 4
  ret float %10
}

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_apply_block_type(%struct.PsyStateVar_t* %psv, i32 %nch, i32* %uselongblock, i32* %blocktype_d) #0 {
entry:
  %psv.addr = alloca %struct.PsyStateVar_t*, align 4
  %nch.addr = alloca i32, align 4
  %uselongblock.addr = alloca i32*, align 4
  %blocktype_d.addr = alloca i32*, align 4
  %chn = alloca i32, align 4
  %blocktype = alloca i32, align 4
  store %struct.PsyStateVar_t* %psv, %struct.PsyStateVar_t** %psv.addr, align 4
  store i32 %nch, i32* %nch.addr, align 4
  store i32* %uselongblock, i32** %uselongblock.addr, align 4
  store i32* %blocktype_d, i32** %blocktype_d.addr, align 4
  store i32 0, i32* %chn, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %chn, align 4
  %1 = load i32, i32* %nch.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %blocktype, align 4
  %2 = load i32*, i32** %uselongblock.addr, align 4
  %3 = load i32, i32* %chn, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %5 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %5, i32 0, i32 10
  %6 = load i32, i32* %chn, align 4
  %arrayidx1 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx1, align 4
  %cmp2 = icmp eq i32 %7, 2
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  store i32 3, i32* %blocktype, align 4
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end18

if.else:                                          ; preds = %for.body
  store i32 2, i32* %blocktype, align 4
  %8 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old4 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %8, i32 0, i32 10
  %9 = load i32, i32* %chn, align 4
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old4, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx5, align 4
  %cmp6 = icmp eq i32 %10, 0
  br i1 %cmp6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.else
  %11 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old8 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %11, i32 0, i32 10
  %12 = load i32, i32* %chn, align 4
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old8, i32 0, i32 %12
  store i32 1, i32* %arrayidx9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.else
  %13 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old11 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %13, i32 0, i32 10
  %14 = load i32, i32* %chn, align 4
  %arrayidx12 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old11, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx12, align 4
  %cmp13 = icmp eq i32 %15, 3
  br i1 %cmp13, label %if.then14, label %if.end17

if.then14:                                        ; preds = %if.end10
  %16 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old15 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %16, i32 0, i32 10
  %17 = load i32, i32* %chn, align 4
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old15, i32 0, i32 %17
  store i32 2, i32* %arrayidx16, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then14, %if.end10
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end
  %18 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old19 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %18, i32 0, i32 10
  %19 = load i32, i32* %chn, align 4
  %arrayidx20 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old19, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx20, align 4
  %21 = load i32*, i32** %blocktype_d.addr, align 4
  %22 = load i32, i32* %chn, align 4
  %arrayidx21 = getelementptr inbounds i32, i32* %21, i32 %22
  store i32 %20, i32* %arrayidx21, align 4
  %23 = load i32, i32* %blocktype, align 4
  %24 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv.addr, align 4
  %blocktype_old22 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %24, i32 0, i32 10
  %25 = load i32, i32* %chn, align 4
  %arrayidx23 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old22, i32 0, i32 %25
  store i32 %23, i32* %arrayidx23, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end18
  %26 = load i32, i32* %chn, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %chn, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal float @pecalc_s(%struct.III_psy_ratio* %mr, float %masking_lower) #0 {
entry:
  %mr.addr = alloca %struct.III_psy_ratio*, align 4
  %masking_lower.addr = alloca float, align 4
  %pe_s = alloca float, align 4
  %sb = alloca i32, align 4
  %sblock = alloca i32, align 4
  %thm = alloca float, align 4
  %x = alloca float, align 4
  %en = alloca float, align 4
  store %struct.III_psy_ratio* %mr, %struct.III_psy_ratio** %mr.addr, align 4
  store float %masking_lower, float* %masking_lower.addr, align 4
  store float 0x4073511EC0000000, float* %pe_s, align 4
  store i32 0, i32* %sb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc30, %entry
  %0 = load i32, i32* %sb, align 4
  %cmp = icmp ult i32 %0, 12
  br i1 %cmp, label %for.body, label %for.end32

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %sblock, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %sblock, align 4
  %cmp2 = icmp ult i32 %1, 3
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %2 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr.addr, align 4
  %thm4 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %2, i32 0, i32 0
  %s = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm4, i32 0, i32 1
  %3 = load i32, i32* %sb, align 4
  %arrayidx = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s, i32 0, i32 %3
  %4 = load i32, i32* %sblock, align 4
  %arrayidx5 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx, i32 0, i32 %4
  %5 = load float, float* %arrayidx5, align 4
  store float %5, float* %thm, align 4
  %6 = load float, float* %thm, align 4
  %cmp6 = fcmp ogt float %6, 0.000000e+00
  br i1 %cmp6, label %if.then, label %if.end29

if.then:                                          ; preds = %for.body3
  %7 = load float, float* %thm, align 4
  %8 = load float, float* %masking_lower.addr, align 4
  %mul = fmul float %7, %8
  store float %mul, float* %x, align 4
  %9 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr.addr, align 4
  %en7 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %9, i32 0, i32 1
  %s8 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en7, i32 0, i32 1
  %10 = load i32, i32* %sb, align 4
  %arrayidx9 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s8, i32 0, i32 %10
  %11 = load i32, i32* %sblock, align 4
  %arrayidx10 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx9, i32 0, i32 %11
  %12 = load float, float* %arrayidx10, align 4
  store float %12, float* %en, align 4
  %13 = load float, float* %en, align 4
  %14 = load float, float* %x, align 4
  %cmp11 = fcmp ogt float %13, %14
  br i1 %cmp11, label %if.then12, label %if.end28

if.then12:                                        ; preds = %if.then
  %15 = load float, float* %en, align 4
  %16 = load float, float* %x, align 4
  %mul13 = fmul float %16, 1.000000e+10
  %cmp14 = fcmp ogt float %15, %mul13
  br i1 %cmp14, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.then12
  %17 = load i32, i32* %sb, align 4
  %arrayidx16 = getelementptr inbounds [12 x float], [12 x float]* @pecalc_s.regcoef_s, i32 0, i32 %17
  %18 = load float, float* %arrayidx16, align 4
  %conv = fpext float %18 to double
  %mul17 = fmul double %conv, 0x4037069E2AA2AA5C
  %19 = load float, float* %pe_s, align 4
  %conv18 = fpext float %19 to double
  %add = fadd double %conv18, %mul17
  %conv19 = fptrunc double %add to float
  store float %conv19, float* %pe_s, align 4
  br label %if.end

if.else:                                          ; preds = %if.then12
  %20 = load i32, i32* %sb, align 4
  %arrayidx20 = getelementptr inbounds [12 x float], [12 x float]* @pecalc_s.regcoef_s, i32 0, i32 %20
  %21 = load float, float* %arrayidx20, align 4
  %conv21 = fpext float %21 to double
  %22 = load float, float* %en, align 4
  %23 = load float, float* %x, align 4
  %div = fdiv float %22, %23
  %call = call float @fast_log2(float %div)
  %conv22 = fpext float %call to double
  %mul23 = fmul double %conv22, 0x3FD34413509F79FE
  %mul24 = fmul double %conv21, %mul23
  %24 = load float, float* %pe_s, align 4
  %conv25 = fpext float %24 to double
  %add26 = fadd double %conv25, %mul24
  %conv27 = fptrunc double %add26 to float
  store float %conv27, float* %pe_s, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then15
  br label %if.end28

if.end28:                                         ; preds = %if.end, %if.then
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %for.body3
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %25 = load i32, i32* %sblock, align 4
  %inc = add i32 %25, 1
  store i32 %inc, i32* %sblock, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc30

for.inc30:                                        ; preds = %for.end
  %26 = load i32, i32* %sb, align 4
  %inc31 = add i32 %26, 1
  store i32 %inc31, i32* %sb, align 4
  br label %for.cond

for.end32:                                        ; preds = %for.cond
  %27 = load float, float* %pe_s, align 4
  ret float %27
}

; Function Attrs: noinline nounwind optnone
define internal float @pecalc_l(%struct.III_psy_ratio* %mr, float %masking_lower) #0 {
entry:
  %mr.addr = alloca %struct.III_psy_ratio*, align 4
  %masking_lower.addr = alloca float, align 4
  %pe_l = alloca float, align 4
  %sb = alloca i32, align 4
  %thm = alloca float, align 4
  %x = alloca float, align 4
  %en = alloca float, align 4
  store %struct.III_psy_ratio* %mr, %struct.III_psy_ratio** %mr.addr, align 4
  store float %masking_lower, float* %masking_lower.addr, align 4
  store float 0x407190EB80000000, float* %pe_l, align 4
  store i32 0, i32* %sb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %sb, align 4
  %cmp = icmp ult i32 %0, 21
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr.addr, align 4
  %thm1 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %1, i32 0, i32 0
  %l = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm1, i32 0, i32 0
  %2 = load i32, i32* %sb, align 4
  %arrayidx = getelementptr inbounds [22 x float], [22 x float]* %l, i32 0, i32 %2
  %3 = load float, float* %arrayidx, align 4
  store float %3, float* %thm, align 4
  %4 = load float, float* %thm, align 4
  %cmp2 = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end24

if.then:                                          ; preds = %for.body
  %5 = load float, float* %thm, align 4
  %6 = load float, float* %masking_lower.addr, align 4
  %mul = fmul float %5, %6
  store float %mul, float* %x, align 4
  %7 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %mr.addr, align 4
  %en3 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %7, i32 0, i32 1
  %l4 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en3, i32 0, i32 0
  %8 = load i32, i32* %sb, align 4
  %arrayidx5 = getelementptr inbounds [22 x float], [22 x float]* %l4, i32 0, i32 %8
  %9 = load float, float* %arrayidx5, align 4
  store float %9, float* %en, align 4
  %10 = load float, float* %en, align 4
  %11 = load float, float* %x, align 4
  %cmp6 = fcmp ogt float %10, %11
  br i1 %cmp6, label %if.then7, label %if.end23

if.then7:                                         ; preds = %if.then
  %12 = load float, float* %en, align 4
  %13 = load float, float* %x, align 4
  %mul8 = fmul float %13, 1.000000e+10
  %cmp9 = fcmp ogt float %12, %mul8
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then7
  %14 = load i32, i32* %sb, align 4
  %arrayidx11 = getelementptr inbounds [21 x float], [21 x float]* @pecalc_l.regcoef_l, i32 0, i32 %14
  %15 = load float, float* %arrayidx11, align 4
  %conv = fpext float %15 to double
  %mul12 = fmul double %conv, 0x4037069E2AA2AA5C
  %16 = load float, float* %pe_l, align 4
  %conv13 = fpext float %16 to double
  %add = fadd double %conv13, %mul12
  %conv14 = fptrunc double %add to float
  store float %conv14, float* %pe_l, align 4
  br label %if.end

if.else:                                          ; preds = %if.then7
  %17 = load i32, i32* %sb, align 4
  %arrayidx15 = getelementptr inbounds [21 x float], [21 x float]* @pecalc_l.regcoef_l, i32 0, i32 %17
  %18 = load float, float* %arrayidx15, align 4
  %conv16 = fpext float %18 to double
  %19 = load float, float* %en, align 4
  %20 = load float, float* %x, align 4
  %div = fdiv float %19, %20
  %call = call float @fast_log2(float %div)
  %conv17 = fpext float %call to double
  %mul18 = fmul double %conv17, 0x3FD34413509F79FE
  %mul19 = fmul double %conv16, %mul18
  %21 = load float, float* %pe_l, align 4
  %conv20 = fpext float %21 to double
  %add21 = fadd double %conv20, %mul19
  %conv22 = fptrunc double %add21 to float
  store float %conv22, float* %pe_l, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then10
  br label %if.end23

if.end23:                                         ; preds = %if.end, %if.then
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end24
  %22 = load i32, i32* %sb, align 4
  %inc = add i32 %22, 1
  store i32 %inc, i32* %sb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load float, float* %pe_l, align 4
  ret float %23
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @psymodel_init(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %psv = alloca %struct.PsyStateVar_t*, align 4
  %gd = alloca %struct.PsyConst_t*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %b = alloca i32, align 4
  %sb = alloca i32, align 4
  %k = alloca i32, align 4
  %bvl_a = alloca float, align 4
  %bvl_b = alloca float, align 4
  %snr_l_a = alloca float, align 4
  %snr_l_b = alloca float, align 4
  %snr_s_a = alloca float, align 4
  %snr_s_b = alloca float, align 4
  %bval = alloca [64 x float], align 16
  %bval_width = alloca [64 x float], align 16
  %norm = alloca [64 x float], align 16
  %sfreq = alloca float, align 4
  %xav = alloca float, align 4
  %xbv = alloca float, align 4
  %minval_low = alloca float, align 4
  %snr = alloca double, align 8
  %x = alloca double, align 8
  %freq = alloca float, align 4
  %level = alloca float, align 4
  %x201 = alloca double, align 8
  %snr202 = alloca double, align 8
  %freq231 = alloca float, align 4
  %level237 = alloca float, align 4
  %msfix = alloca float, align 4
  %freq384 = alloca float, align 4
  %freq_inc = alloca float, align 4
  %eql_balance = alloca float, align 4
  %x460 = alloca float, align 4
  %y = alloca float, align 4
  %sk_s = alloca float, align 4
  %sk_l = alloca float, align 4
  %m = alloca float, align 4
  %m527 = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 9
  store %struct.PsyStateVar_t* %sv_psy, %struct.PsyStateVar_t** %psv, align 4
  store float 1.300000e+01, float* %bvl_a, align 4
  store float 2.400000e+01, float* %bvl_b, align 4
  store float 0.000000e+00, float* %snr_l_a, align 4
  store float 0.000000e+00, float* %snr_l_b, align 4
  store float -8.250000e+00, float* %snr_s_a, align 4
  store float -4.500000e+00, float* %snr_s_b, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 12
  %5 = load i32, i32* %samplerate_out, align 4
  %conv = sitofp i32 %5 to float
  store float %conv, float* %sfreq, align 4
  store float 1.000000e+01, float* %xav, align 4
  store float 1.200000e+01, float* %xbv, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %minval = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 63
  %7 = load float, float* %minval, align 4
  %sub = fsub float 0.000000e+00, %7
  store float %sub, float* %minval_low, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 22
  %9 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %cmp = icmp ne %struct.PsyConst_t* %9, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %norm, i32 0, i32 0
  %10 = bitcast float* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %10, i8 0, i32 256, i1 false)
  %call = call i8* @calloc(i32 1, i32 11112)
  %11 = bitcast i8* %call to %struct.PsyConst_t*
  store %struct.PsyConst_t* %11, %struct.PsyConst_t** %gd, align 4
  %12 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cd_psy3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 22
  store %struct.PsyConst_t* %12, %struct.PsyConst_t** %cd_psy3, align 4
  %14 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalZ = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %14, i32 0, i32 36
  %15 = load i32, i32* %experimentalZ, align 4
  %16 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %force_short_block_calc = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %16, i32 0, i32 7
  store i32 %15, i32* %force_short_block_calc, align 4
  %17 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %blocktype_old = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %17, i32 0, i32 10
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old, i32 0, i32 1
  store i32 0, i32* %arrayidx, align 4
  %18 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %blocktype_old4 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %18, i32 0, i32 10
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype_old4, i32 0, i32 0
  store i32 0, i32* %arrayidx5, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc65, %if.end
  %19 = load i32, i32* %i, align 4
  %cmp6 = icmp slt i32 %19, 4
  br i1 %cmp6, label %for.body, label %for.end67

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %20 = load i32, i32* %j, align 4
  %cmp9 = icmp slt i32 %20, 64
  br i1 %cmp9, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond8
  %21 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l1 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %21, i32 0, i32 0
  %22 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l1, i32 0, i32 %22
  %23 = load i32, i32* %j, align 4
  %arrayidx13 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx12, i32 0, i32 %23
  store float 0x4415AF1D80000000, float* %arrayidx13, align 4
  %24 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_l2 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %24, i32 0, i32 1
  %25 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_l2, i32 0, i32 %25
  %26 = load i32, i32* %j, align 4
  %arrayidx15 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx14, i32 0, i32 %26
  store float 0x4415AF1D80000000, float* %arrayidx15, align 4
  %27 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_s2 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %27, i32 0, i32 3
  %28 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s2, i32 0, i32 %28
  %29 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx16, i32 0, i32 %29
  store float 1.000000e+00, float* %arrayidx17, align 4
  %30 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %nb_s1 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %30, i32 0, i32 2
  %31 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [4 x [64 x float]], [4 x [64 x float]]* %nb_s1, i32 0, i32 %31
  %32 = load i32, i32* %j, align 4
  %arrayidx19 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx18, i32 0, i32 %32
  store float 1.000000e+00, float* %arrayidx19, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %33 = load i32, i32* %j, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  store i32 0, i32* %sb, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc29, %for.end
  %34 = load i32, i32* %sb, align 4
  %cmp21 = icmp slt i32 %34, 22
  br i1 %cmp21, label %for.body23, label %for.end31

for.body23:                                       ; preds = %for.cond20
  %35 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %35, i32 0, i32 5
  %36 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en, i32 0, i32 %36
  %l = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx24, i32 0, i32 0
  %37 = load i32, i32* %sb, align 4
  %arrayidx25 = getelementptr inbounds [22 x float], [22 x float]* %l, i32 0, i32 %37
  store float 0x4415AF1D80000000, float* %arrayidx25, align 4
  %38 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %38, i32 0, i32 4
  %39 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm, i32 0, i32 %39
  %l27 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx26, i32 0, i32 0
  %40 = load i32, i32* %sb, align 4
  %arrayidx28 = getelementptr inbounds [22 x float], [22 x float]* %l27, i32 0, i32 %40
  store float 0x4415AF1D80000000, float* %arrayidx28, align 4
  br label %for.inc29

for.inc29:                                        ; preds = %for.body23
  %41 = load i32, i32* %sb, align 4
  %inc30 = add nsw i32 %41, 1
  store i32 %inc30, i32* %sb, align 4
  br label %for.cond20

for.end31:                                        ; preds = %for.cond20
  store i32 0, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc53, %for.end31
  %42 = load i32, i32* %j, align 4
  %cmp33 = icmp slt i32 %42, 3
  br i1 %cmp33, label %for.body35, label %for.end55

for.body35:                                       ; preds = %for.cond32
  store i32 0, i32* %sb, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc49, %for.body35
  %43 = load i32, i32* %sb, align 4
  %cmp37 = icmp slt i32 %43, 13
  br i1 %cmp37, label %for.body39, label %for.end51

for.body39:                                       ; preds = %for.cond36
  %44 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %en40 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %44, i32 0, i32 5
  %45 = load i32, i32* %i, align 4
  %arrayidx41 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %en40, i32 0, i32 %45
  %s = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx41, i32 0, i32 1
  %46 = load i32, i32* %sb, align 4
  %arrayidx42 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s, i32 0, i32 %46
  %47 = load i32, i32* %j, align 4
  %arrayidx43 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx42, i32 0, i32 %47
  store float 0x4415AF1D80000000, float* %arrayidx43, align 4
  %48 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %thm44 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %48, i32 0, i32 4
  %49 = load i32, i32* %i, align 4
  %arrayidx45 = getelementptr inbounds [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin]* %thm44, i32 0, i32 %49
  %s46 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %arrayidx45, i32 0, i32 1
  %50 = load i32, i32* %sb, align 4
  %arrayidx47 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s46, i32 0, i32 %50
  %51 = load i32, i32* %j, align 4
  %arrayidx48 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx47, i32 0, i32 %51
  store float 0x4415AF1D80000000, float* %arrayidx48, align 4
  br label %for.inc49

for.inc49:                                        ; preds = %for.body39
  %52 = load i32, i32* %sb, align 4
  %inc50 = add nsw i32 %52, 1
  store i32 %inc50, i32* %sb, align 4
  br label %for.cond36

for.end51:                                        ; preds = %for.cond36
  %53 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_attacks = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %53, i32 0, i32 9
  %54 = load i32, i32* %i, align 4
  %arrayidx52 = getelementptr inbounds [4 x i32], [4 x i32]* %last_attacks, i32 0, i32 %54
  store i32 0, i32* %arrayidx52, align 4
  br label %for.inc53

for.inc53:                                        ; preds = %for.end51
  %55 = load i32, i32* %j, align 4
  %inc54 = add nsw i32 %55, 1
  store i32 %inc54, i32* %j, align 4
  br label %for.cond32

for.end55:                                        ; preds = %for.cond32
  store i32 0, i32* %j, align 4
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc62, %for.end55
  %56 = load i32, i32* %j, align 4
  %cmp57 = icmp slt i32 %56, 9
  br i1 %cmp57, label %for.body59, label %for.end64

for.body59:                                       ; preds = %for.cond56
  %57 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %last_en_subshort = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %57, i32 0, i32 8
  %58 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds [4 x [9 x float]], [4 x [9 x float]]* %last_en_subshort, i32 0, i32 %58
  %59 = load i32, i32* %j, align 4
  %arrayidx61 = getelementptr inbounds [9 x float], [9 x float]* %arrayidx60, i32 0, i32 %59
  store float 1.000000e+01, float* %arrayidx61, align 4
  br label %for.inc62

for.inc62:                                        ; preds = %for.body59
  %60 = load i32, i32* %j, align 4
  %inc63 = add nsw i32 %60, 1
  store i32 %inc63, i32* %j, align 4
  br label %for.cond56

for.end64:                                        ; preds = %for.cond56
  br label %for.inc65

for.inc65:                                        ; preds = %for.end64
  %61 = load i32, i32* %i, align 4
  %inc66 = add nsw i32 %61, 1
  store i32 %inc66, i32* %i, align 4
  br label %for.cond

for.end67:                                        ; preds = %for.cond
  %62 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %loudness_sq_save = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %62, i32 0, i32 6
  %arrayidx68 = getelementptr inbounds [2 x float], [2 x float]* %loudness_sq_save, i32 0, i32 1
  store float 0.000000e+00, float* %arrayidx68, align 4
  %63 = load %struct.PsyStateVar_t*, %struct.PsyStateVar_t** %psv, align 4
  %loudness_sq_save69 = getelementptr inbounds %struct.PsyStateVar_t, %struct.PsyStateVar_t* %63, i32 0, i32 6
  %arrayidx70 = getelementptr inbounds [2 x float], [2 x float]* %loudness_sq_save69, i32 0, i32 0
  store float 0.000000e+00, float* %arrayidx70, align 4
  %64 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l71 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %64, i32 0, i32 2
  %65 = load float, float* %sfreq, align 4
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 8
  %l72 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %arraydecay73 = getelementptr inbounds [23 x i32], [23 x i32]* %l72, i32 0, i32 0
  call void @init_numline(%struct.PsyConst_CB2SB_t* %l71, float %65, i32 1024, i32 576, i32 22, i32* %arraydecay73)
  %67 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l74 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %67, i32 0, i32 2
  %68 = load float, float* %sfreq, align 4
  %arraydecay75 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 0
  %arraydecay76 = getelementptr inbounds [64 x float], [64 x float]* %bval_width, i32 0, i32 0
  call void @compute_bark_values(%struct.PsyConst_CB2SB_t* %l74, float %68, i32 1024, float* %arraydecay75, float* %arraydecay76)
  store i32 0, i32* %i, align 4
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc100, %for.end67
  %69 = load i32, i32* %i, align 4
  %70 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l78 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %70, i32 0, i32 2
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l78, i32 0, i32 11
  %71 = load i32, i32* %npart, align 4
  %cmp79 = icmp slt i32 %69, %71
  br i1 %cmp79, label %for.body81, label %for.end102

for.body81:                                       ; preds = %for.cond77
  %72 = load float, float* %snr_l_a, align 4
  %conv82 = fpext float %72 to double
  store double %conv82, double* %snr, align 8
  %73 = load i32, i32* %i, align 4
  %arrayidx83 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %73
  %74 = load float, float* %arrayidx83, align 4
  %75 = load float, float* %bvl_a, align 4
  %cmp84 = fcmp oge float %74, %75
  br i1 %cmp84, label %if.then86, label %if.end96

if.then86:                                        ; preds = %for.body81
  %76 = load float, float* %snr_l_b, align 4
  %77 = load i32, i32* %i, align 4
  %arrayidx87 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %77
  %78 = load float, float* %arrayidx87, align 4
  %79 = load float, float* %bvl_a, align 4
  %sub88 = fsub float %78, %79
  %mul = fmul float %76, %sub88
  %80 = load float, float* %bvl_b, align 4
  %81 = load float, float* %bvl_a, align 4
  %sub89 = fsub float %80, %81
  %div = fdiv float %mul, %sub89
  %82 = load float, float* %snr_l_a, align 4
  %83 = load float, float* %bvl_b, align 4
  %84 = load i32, i32* %i, align 4
  %arrayidx90 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %84
  %85 = load float, float* %arrayidx90, align 4
  %sub91 = fsub float %83, %85
  %mul92 = fmul float %82, %sub91
  %86 = load float, float* %bvl_b, align 4
  %87 = load float, float* %bvl_a, align 4
  %sub93 = fsub float %86, %87
  %div94 = fdiv float %mul92, %sub93
  %add = fadd float %div, %div94
  %conv95 = fpext float %add to double
  store double %conv95, double* %snr, align 8
  br label %if.end96

if.end96:                                         ; preds = %if.then86, %for.body81
  %88 = load double, double* %snr, align 8
  %div97 = fdiv double %88, 1.000000e+01
  %89 = call double @llvm.pow.f64(double 1.000000e+01, double %div97)
  %conv98 = fptrunc double %89 to float
  %90 = load i32, i32* %i, align 4
  %arrayidx99 = getelementptr inbounds [64 x float], [64 x float]* %norm, i32 0, i32 %90
  store float %conv98, float* %arrayidx99, align 4
  br label %for.inc100

for.inc100:                                       ; preds = %if.end96
  %91 = load i32, i32* %i, align 4
  %inc101 = add nsw i32 %91, 1
  store i32 %inc101, i32* %i, align 4
  br label %for.cond77

for.end102:                                       ; preds = %for.cond77
  %92 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l103 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %92, i32 0, i32 2
  %s3 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l103, i32 0, i32 13
  %93 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l104 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %93, i32 0, i32 2
  %s3ind = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l104, i32 0, i32 7
  %arraydecay105 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind, i32 0, i32 0
  %94 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l106 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %94, i32 0, i32 2
  %npart107 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l106, i32 0, i32 11
  %95 = load i32, i32* %npart107, align 4
  %arraydecay108 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 0
  %arraydecay109 = getelementptr inbounds [64 x float], [64 x float]* %bval_width, i32 0, i32 0
  %arraydecay110 = getelementptr inbounds [64 x float], [64 x float]* %norm, i32 0, i32 0
  %call111 = call i32 @init_s3_values(float** %s3, [2 x i32]* %arraydecay105, i32 %95, float* %arraydecay108, float* %arraydecay109, float* %arraydecay110)
  store i32 %call111, i32* %i, align 4
  %96 = load i32, i32* %i, align 4
  %tobool = icmp ne i32 %96, 0
  br i1 %tobool, label %if.then112, label %if.end113

if.then112:                                       ; preds = %for.end102
  %97 = load i32, i32* %i, align 4
  store i32 %97, i32* %retval, align 4
  br label %return

if.end113:                                        ; preds = %for.end102
  store i32 0, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc185, %if.end113
  %98 = load i32, i32* %i, align 4
  %99 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l115 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %99, i32 0, i32 2
  %npart116 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l115, i32 0, i32 11
  %100 = load i32, i32* %npart116, align 4
  %cmp117 = icmp slt i32 %98, %100
  br i1 %cmp117, label %for.body119, label %for.end187

for.body119:                                      ; preds = %for.cond114
  store double 0x47EFFFFFE0000000, double* %x, align 8
  store i32 0, i32* %k, align 4
  br label %for.cond120

for.cond120:                                      ; preds = %for.inc148, %for.body119
  %101 = load i32, i32* %k, align 4
  %102 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l121 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %102, i32 0, i32 2
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l121, i32 0, i32 8
  %103 = load i32, i32* %i, align 4
  %arrayidx122 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %103
  %104 = load i32, i32* %arrayidx122, align 4
  %cmp123 = icmp slt i32 %101, %104
  br i1 %cmp123, label %for.body125, label %for.end151

for.body125:                                      ; preds = %for.cond120
  %105 = load float, float* %sfreq, align 4
  %106 = load i32, i32* %j, align 4
  %conv126 = sitofp i32 %106 to float
  %mul127 = fmul float %105, %conv126
  %conv128 = fpext float %mul127 to double
  %div129 = fdiv double %conv128, 1.024000e+06
  %conv130 = fptrunc double %div129 to float
  store float %conv130, float* %freq, align 4
  %107 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %108 = load float, float* %freq, align 4
  %mul131 = fmul float %108, 1.000000e+03
  %call132 = call float @ATHformula(%struct.SessionConfig_t* %107, float %mul131)
  %sub133 = fsub float %call132, 2.000000e+01
  store float %sub133, float* %level, align 4
  %109 = load float, float* %level, align 4
  %conv134 = fpext float %109 to double
  %mul135 = fmul double 1.000000e-01, %conv134
  %110 = call double @llvm.pow.f64(double 1.000000e+01, double %mul135)
  %conv136 = fptrunc double %110 to float
  store float %conv136, float* %level, align 4
  %111 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l137 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %111, i32 0, i32 2
  %numlines138 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l137, i32 0, i32 8
  %112 = load i32, i32* %i, align 4
  %arrayidx139 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines138, i32 0, i32 %112
  %113 = load i32, i32* %arrayidx139, align 4
  %conv140 = sitofp i32 %113 to float
  %114 = load float, float* %level, align 4
  %mul141 = fmul float %114, %conv140
  store float %mul141, float* %level, align 4
  %115 = load double, double* %x, align 8
  %116 = load float, float* %level, align 4
  %conv142 = fpext float %116 to double
  %cmp143 = fcmp ogt double %115, %conv142
  br i1 %cmp143, label %if.then145, label %if.end147

if.then145:                                       ; preds = %for.body125
  %117 = load float, float* %level, align 4
  %conv146 = fpext float %117 to double
  store double %conv146, double* %x, align 8
  br label %if.end147

if.end147:                                        ; preds = %if.then145, %for.body125
  br label %for.inc148

for.inc148:                                       ; preds = %if.end147
  %118 = load i32, i32* %k, align 4
  %inc149 = add nsw i32 %118, 1
  store i32 %inc149, i32* %k, align 4
  %119 = load i32, i32* %j, align 4
  %inc150 = add nsw i32 %119, 1
  store i32 %inc150, i32* %j, align 4
  br label %for.cond120

for.end151:                                       ; preds = %for.cond120
  %120 = load double, double* %x, align 8
  %conv152 = fptrunc double %120 to float
  %121 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %121, i32 0, i32 21
  %122 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %cb_l = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %122, i32 0, i32 10
  %123 = load i32, i32* %i, align 4
  %arrayidx153 = getelementptr inbounds [64 x float], [64 x float]* %cb_l, i32 0, i32 %123
  store float %conv152, float* %arrayidx153, align 4
  %124 = load i32, i32* %i, align 4
  %arrayidx154 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %124
  %125 = load float, float* %arrayidx154, align 4
  %126 = load float, float* %xav, align 4
  %div155 = fdiv float %125, %126
  %conv156 = fpext float %div155 to double
  %sub157 = fsub double %conv156, 1.000000e+00
  %mul158 = fmul double 2.000000e+01, %sub157
  store double %mul158, double* %x, align 8
  %127 = load double, double* %x, align 8
  %cmp159 = fcmp ogt double %127, 6.000000e+00
  br i1 %cmp159, label %if.then161, label %if.end162

if.then161:                                       ; preds = %for.end151
  store double 3.000000e+01, double* %x, align 8
  br label %if.end162

if.end162:                                        ; preds = %if.then161, %for.end151
  %128 = load double, double* %x, align 8
  %129 = load float, float* %minval_low, align 4
  %conv163 = fpext float %129 to double
  %cmp164 = fcmp olt double %128, %conv163
  br i1 %cmp164, label %if.then166, label %if.end168

if.then166:                                       ; preds = %if.end162
  %130 = load float, float* %minval_low, align 4
  %conv167 = fpext float %130 to double
  store double %conv167, double* %x, align 8
  br label %if.end168

if.end168:                                        ; preds = %if.then166, %if.end162
  %131 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out169 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %131, i32 0, i32 12
  %132 = load i32, i32* %samplerate_out169, align 4
  %cmp170 = icmp slt i32 %132, 44000
  br i1 %cmp170, label %if.then172, label %if.end173

if.then172:                                       ; preds = %if.end168
  store double 3.000000e+01, double* %x, align 8
  br label %if.end173

if.end173:                                        ; preds = %if.then172, %if.end168
  %133 = load double, double* %x, align 8
  %sub174 = fsub double %133, 8.000000e+00
  store double %sub174, double* %x, align 8
  %134 = load double, double* %x, align 8
  %div175 = fdiv double %134, 1.000000e+01
  %135 = call double @llvm.pow.f64(double 1.000000e+01, double %div175)
  %136 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l176 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %136, i32 0, i32 2
  %numlines177 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l176, i32 0, i32 8
  %137 = load i32, i32* %i, align 4
  %arrayidx178 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines177, i32 0, i32 %137
  %138 = load i32, i32* %arrayidx178, align 4
  %conv179 = sitofp i32 %138 to double
  %mul180 = fmul double %135, %conv179
  %conv181 = fptrunc double %mul180 to float
  %139 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l182 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %139, i32 0, i32 2
  %minval183 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l182, i32 0, i32 1
  %140 = load i32, i32* %i, align 4
  %arrayidx184 = getelementptr inbounds [64 x float], [64 x float]* %minval183, i32 0, i32 %140
  store float %conv181, float* %arrayidx184, align 4
  br label %for.inc185

for.inc185:                                       ; preds = %if.end173
  %141 = load i32, i32* %i, align 4
  %inc186 = add nsw i32 %141, 1
  store i32 %inc186, i32* %i, align 4
  br label %for.cond114

for.end187:                                       ; preds = %for.cond114
  %142 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s188 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %142, i32 0, i32 3
  %143 = load float, float* %sfreq, align 4
  %144 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band189 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %144, i32 0, i32 8
  %s190 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band189, i32 0, i32 1
  %arraydecay191 = getelementptr inbounds [14 x i32], [14 x i32]* %s190, i32 0, i32 0
  call void @init_numline(%struct.PsyConst_CB2SB_t* %s188, float %143, i32 256, i32 192, i32 13, i32* %arraydecay191)
  %145 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s192 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %145, i32 0, i32 3
  %146 = load float, float* %sfreq, align 4
  %arraydecay193 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 0
  %arraydecay194 = getelementptr inbounds [64 x float], [64 x float]* %bval_width, i32 0, i32 0
  call void @compute_bark_values(%struct.PsyConst_CB2SB_t* %s192, float %146, i32 256, float* %arraydecay193, float* %arraydecay194)
  store i32 0, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond195

for.cond195:                                      ; preds = %for.inc311, %for.end187
  %147 = load i32, i32* %i, align 4
  %148 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s196 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %148, i32 0, i32 3
  %npart197 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s196, i32 0, i32 11
  %149 = load i32, i32* %npart197, align 4
  %cmp198 = icmp slt i32 %147, %149
  br i1 %cmp198, label %for.body200, label %for.end313

for.body200:                                      ; preds = %for.cond195
  %150 = load float, float* %snr_s_a, align 4
  %conv203 = fpext float %150 to double
  store double %conv203, double* %snr202, align 8
  %151 = load i32, i32* %i, align 4
  %arrayidx204 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %151
  %152 = load float, float* %arrayidx204, align 4
  %153 = load float, float* %bvl_a, align 4
  %cmp205 = fcmp oge float %152, %153
  br i1 %cmp205, label %if.then207, label %if.end220

if.then207:                                       ; preds = %for.body200
  %154 = load float, float* %snr_s_b, align 4
  %155 = load i32, i32* %i, align 4
  %arrayidx208 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %155
  %156 = load float, float* %arrayidx208, align 4
  %157 = load float, float* %bvl_a, align 4
  %sub209 = fsub float %156, %157
  %mul210 = fmul float %154, %sub209
  %158 = load float, float* %bvl_b, align 4
  %159 = load float, float* %bvl_a, align 4
  %sub211 = fsub float %158, %159
  %div212 = fdiv float %mul210, %sub211
  %160 = load float, float* %snr_s_a, align 4
  %161 = load float, float* %bvl_b, align 4
  %162 = load i32, i32* %i, align 4
  %arrayidx213 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %162
  %163 = load float, float* %arrayidx213, align 4
  %sub214 = fsub float %161, %163
  %mul215 = fmul float %160, %sub214
  %164 = load float, float* %bvl_b, align 4
  %165 = load float, float* %bvl_a, align 4
  %sub216 = fsub float %164, %165
  %div217 = fdiv float %mul215, %sub216
  %add218 = fadd float %div212, %div217
  %conv219 = fpext float %add218 to double
  store double %conv219, double* %snr202, align 8
  br label %if.end220

if.end220:                                        ; preds = %if.then207, %for.body200
  %166 = load double, double* %snr202, align 8
  %div221 = fdiv double %166, 1.000000e+01
  %167 = call double @llvm.pow.f64(double 1.000000e+01, double %div221)
  %conv222 = fptrunc double %167 to float
  %168 = load i32, i32* %i, align 4
  %arrayidx223 = getelementptr inbounds [64 x float], [64 x float]* %norm, i32 0, i32 %168
  store float %conv222, float* %arrayidx223, align 4
  store double 0x47EFFFFFE0000000, double* %x201, align 8
  store i32 0, i32* %k, align 4
  br label %for.cond224

for.cond224:                                      ; preds = %for.inc255, %if.end220
  %169 = load i32, i32* %k, align 4
  %170 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s225 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %170, i32 0, i32 3
  %numlines226 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s225, i32 0, i32 8
  %171 = load i32, i32* %i, align 4
  %arrayidx227 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines226, i32 0, i32 %171
  %172 = load i32, i32* %arrayidx227, align 4
  %cmp228 = icmp slt i32 %169, %172
  br i1 %cmp228, label %for.body230, label %for.end258

for.body230:                                      ; preds = %for.cond224
  %173 = load float, float* %sfreq, align 4
  %174 = load i32, i32* %j, align 4
  %conv232 = sitofp i32 %174 to float
  %mul233 = fmul float %173, %conv232
  %conv234 = fpext float %mul233 to double
  %div235 = fdiv double %conv234, 2.560000e+05
  %conv236 = fptrunc double %div235 to float
  store float %conv236, float* %freq231, align 4
  %175 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %176 = load float, float* %freq231, align 4
  %mul238 = fmul float %176, 1.000000e+03
  %call239 = call float @ATHformula(%struct.SessionConfig_t* %175, float %mul238)
  %sub240 = fsub float %call239, 2.000000e+01
  store float %sub240, float* %level237, align 4
  %177 = load float, float* %level237, align 4
  %conv241 = fpext float %177 to double
  %mul242 = fmul double 1.000000e-01, %conv241
  %178 = call double @llvm.pow.f64(double 1.000000e+01, double %mul242)
  %conv243 = fptrunc double %178 to float
  store float %conv243, float* %level237, align 4
  %179 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s244 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %179, i32 0, i32 3
  %numlines245 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s244, i32 0, i32 8
  %180 = load i32, i32* %i, align 4
  %arrayidx246 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines245, i32 0, i32 %180
  %181 = load i32, i32* %arrayidx246, align 4
  %conv247 = sitofp i32 %181 to float
  %182 = load float, float* %level237, align 4
  %mul248 = fmul float %182, %conv247
  store float %mul248, float* %level237, align 4
  %183 = load double, double* %x201, align 8
  %184 = load float, float* %level237, align 4
  %conv249 = fpext float %184 to double
  %cmp250 = fcmp ogt double %183, %conv249
  br i1 %cmp250, label %if.then252, label %if.end254

if.then252:                                       ; preds = %for.body230
  %185 = load float, float* %level237, align 4
  %conv253 = fpext float %185 to double
  store double %conv253, double* %x201, align 8
  br label %if.end254

if.end254:                                        ; preds = %if.then252, %for.body230
  br label %for.inc255

for.inc255:                                       ; preds = %if.end254
  %186 = load i32, i32* %k, align 4
  %inc256 = add nsw i32 %186, 1
  store i32 %inc256, i32* %k, align 4
  %187 = load i32, i32* %j, align 4
  %inc257 = add nsw i32 %187, 1
  store i32 %inc257, i32* %j, align 4
  br label %for.cond224

for.end258:                                       ; preds = %for.cond224
  %188 = load double, double* %x201, align 8
  %conv259 = fptrunc double %188 to float
  %189 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH260 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %189, i32 0, i32 21
  %190 = load %struct.ATH_t*, %struct.ATH_t** %ATH260, align 8
  %cb_s = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %190, i32 0, i32 11
  %191 = load i32, i32* %i, align 4
  %arrayidx261 = getelementptr inbounds [64 x float], [64 x float]* %cb_s, i32 0, i32 %191
  store float %conv259, float* %arrayidx261, align 4
  %192 = load i32, i32* %i, align 4
  %arrayidx262 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %192
  %193 = load float, float* %arrayidx262, align 4
  %194 = load float, float* %xbv, align 4
  %div263 = fdiv float %193, %194
  %conv264 = fpext float %div263 to double
  %sub265 = fsub double %conv264, 1.000000e+00
  %mul266 = fmul double 7.000000e+00, %sub265
  store double %mul266, double* %x201, align 8
  %195 = load i32, i32* %i, align 4
  %arrayidx267 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %195
  %196 = load float, float* %arrayidx267, align 4
  %197 = load float, float* %xbv, align 4
  %cmp268 = fcmp ogt float %196, %197
  br i1 %cmp268, label %if.then270, label %if.end275

if.then270:                                       ; preds = %for.end258
  %198 = load double, double* %x201, align 8
  %add271 = fadd double 1.000000e+00, %198
  %199 = call double @llvm.log.f64(double %add271)
  %mul272 = fmul double %199, 3.100000e+00
  %add273 = fadd double 1.000000e+00, %mul272
  %200 = load double, double* %x201, align 8
  %mul274 = fmul double %200, %add273
  store double %mul274, double* %x201, align 8
  br label %if.end275

if.end275:                                        ; preds = %if.then270, %for.end258
  %201 = load i32, i32* %i, align 4
  %arrayidx276 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 %201
  %202 = load float, float* %arrayidx276, align 4
  %203 = load float, float* %xbv, align 4
  %cmp277 = fcmp olt float %202, %203
  br i1 %cmp277, label %if.then279, label %if.end284

if.then279:                                       ; preds = %if.end275
  %204 = load double, double* %x201, align 8
  %sub280 = fsub double 1.000000e+00, %204
  %205 = call double @llvm.log.f64(double %sub280)
  %mul281 = fmul double %205, 2.300000e+00
  %add282 = fadd double 1.000000e+00, %mul281
  %206 = load double, double* %x201, align 8
  %mul283 = fmul double %206, %add282
  store double %mul283, double* %x201, align 8
  br label %if.end284

if.end284:                                        ; preds = %if.then279, %if.end275
  %207 = load double, double* %x201, align 8
  %cmp285 = fcmp ogt double %207, 6.000000e+00
  br i1 %cmp285, label %if.then287, label %if.end288

if.then287:                                       ; preds = %if.end284
  store double 3.000000e+01, double* %x201, align 8
  br label %if.end288

if.end288:                                        ; preds = %if.then287, %if.end284
  %208 = load double, double* %x201, align 8
  %209 = load float, float* %minval_low, align 4
  %conv289 = fpext float %209 to double
  %cmp290 = fcmp olt double %208, %conv289
  br i1 %cmp290, label %if.then292, label %if.end294

if.then292:                                       ; preds = %if.end288
  %210 = load float, float* %minval_low, align 4
  %conv293 = fpext float %210 to double
  store double %conv293, double* %x201, align 8
  br label %if.end294

if.end294:                                        ; preds = %if.then292, %if.end288
  %211 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out295 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %211, i32 0, i32 12
  %212 = load i32, i32* %samplerate_out295, align 4
  %cmp296 = icmp slt i32 %212, 44000
  br i1 %cmp296, label %if.then298, label %if.end299

if.then298:                                       ; preds = %if.end294
  store double 3.000000e+01, double* %x201, align 8
  br label %if.end299

if.end299:                                        ; preds = %if.then298, %if.end294
  %213 = load double, double* %x201, align 8
  %sub300 = fsub double %213, 8.000000e+00
  store double %sub300, double* %x201, align 8
  %214 = load double, double* %x201, align 8
  %div301 = fdiv double %214, 1.000000e+01
  %215 = call double @llvm.pow.f64(double 1.000000e+01, double %div301)
  %216 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s302 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %216, i32 0, i32 3
  %numlines303 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s302, i32 0, i32 8
  %217 = load i32, i32* %i, align 4
  %arrayidx304 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines303, i32 0, i32 %217
  %218 = load i32, i32* %arrayidx304, align 4
  %conv305 = sitofp i32 %218 to double
  %mul306 = fmul double %215, %conv305
  %conv307 = fptrunc double %mul306 to float
  %219 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s308 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %219, i32 0, i32 3
  %minval309 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s308, i32 0, i32 1
  %220 = load i32, i32* %i, align 4
  %arrayidx310 = getelementptr inbounds [64 x float], [64 x float]* %minval309, i32 0, i32 %220
  store float %conv307, float* %arrayidx310, align 4
  br label %for.inc311

for.inc311:                                       ; preds = %if.end299
  %221 = load i32, i32* %i, align 4
  %inc312 = add nsw i32 %221, 1
  store i32 %inc312, i32* %i, align 4
  br label %for.cond195

for.end313:                                       ; preds = %for.cond195
  %222 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s314 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %222, i32 0, i32 3
  %s3315 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s314, i32 0, i32 13
  %223 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s316 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %223, i32 0, i32 3
  %s3ind317 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s316, i32 0, i32 7
  %arraydecay318 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind317, i32 0, i32 0
  %224 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s319 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %224, i32 0, i32 3
  %npart320 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s319, i32 0, i32 11
  %225 = load i32, i32* %npart320, align 4
  %arraydecay321 = getelementptr inbounds [64 x float], [64 x float]* %bval, i32 0, i32 0
  %arraydecay322 = getelementptr inbounds [64 x float], [64 x float]* %bval_width, i32 0, i32 0
  %arraydecay323 = getelementptr inbounds [64 x float], [64 x float]* %norm, i32 0, i32 0
  %call324 = call i32 @init_s3_values(float** %s3315, [2 x i32]* %arraydecay318, i32 %225, float* %arraydecay321, float* %arraydecay322, float* %arraydecay323)
  store i32 %call324, i32* %i, align 4
  %226 = load i32, i32* %i, align 4
  %tobool325 = icmp ne i32 %226, 0
  br i1 %tobool325, label %if.then326, label %if.end327

if.then326:                                       ; preds = %for.end313
  %227 = load i32, i32* %i, align 4
  store i32 %227, i32* %retval, align 4
  br label %return

if.end327:                                        ; preds = %for.end313
  call void @init_mask_add_max_values()
  %228 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @init_fft(%struct.lame_internal_flags* %228)
  %229 = load float, float* %sfreq, align 4
  %conv328 = fpext float %229 to double
  %mul329 = fmul double 1.000000e-02, %conv328
  %div330 = fdiv double %mul329, 1.920000e+02
  %div331 = fdiv double 0xC0026BB1BBB55516, %div330
  %230 = call double @llvm.exp.f64(double %div331)
  %conv332 = fptrunc double %230 to float
  %231 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %decay = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %231, i32 0, i32 6
  store float %conv332, float* %decay, align 4
  store float 3.500000e+00, float* %msfix, align 4
  %232 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_safe_joint_stereo = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %232, i32 0, i32 20
  %233 = load i32, i32* %use_safe_joint_stereo, align 4
  %tobool333 = icmp ne i32 %233, 0
  br i1 %tobool333, label %if.then334, label %if.end335

if.then334:                                       ; preds = %if.end327
  store float 1.000000e+00, float* %msfix, align 4
  br label %if.end335

if.end335:                                        ; preds = %if.then334, %if.end327
  %234 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix336 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %234, i32 0, i32 44
  %235 = load float, float* %msfix336, align 4
  %conv337 = fpext float %235 to double
  %236 = call double @llvm.fabs.f64(double %conv337)
  %cmp338 = fcmp ogt double %236, 0.000000e+00
  br i1 %cmp338, label %if.then340, label %if.end342

if.then340:                                       ; preds = %if.end335
  %237 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix341 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %237, i32 0, i32 44
  %238 = load float, float* %msfix341, align 4
  store float %238, float* %msfix, align 4
  br label %if.end342

if.end342:                                        ; preds = %if.then340, %if.end335
  %239 = load float, float* %msfix, align 4
  %240 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix343 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %240, i32 0, i32 44
  store float %239, float* %msfix343, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond344

for.cond344:                                      ; preds = %for.inc368, %if.end342
  %241 = load i32, i32* %b, align 4
  %242 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l345 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %242, i32 0, i32 2
  %npart346 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l345, i32 0, i32 11
  %243 = load i32, i32* %npart346, align 4
  %cmp347 = icmp slt i32 %241, %243
  br i1 %cmp347, label %for.body349, label %for.end370

for.body349:                                      ; preds = %for.cond344
  %244 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l350 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %244, i32 0, i32 2
  %s3ind351 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l350, i32 0, i32 7
  %245 = load i32, i32* %b, align 4
  %arrayidx352 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind351, i32 0, i32 %245
  %arrayidx353 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx352, i32 0, i32 1
  %246 = load i32, i32* %arrayidx353, align 4
  %247 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l354 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %247, i32 0, i32 2
  %npart355 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l354, i32 0, i32 11
  %248 = load i32, i32* %npart355, align 4
  %sub356 = sub nsw i32 %248, 1
  %cmp357 = icmp sgt i32 %246, %sub356
  br i1 %cmp357, label %if.then359, label %if.end367

if.then359:                                       ; preds = %for.body349
  %249 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l360 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %249, i32 0, i32 2
  %npart361 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l360, i32 0, i32 11
  %250 = load i32, i32* %npart361, align 4
  %sub362 = sub nsw i32 %250, 1
  %251 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l363 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %251, i32 0, i32 2
  %s3ind364 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l363, i32 0, i32 7
  %252 = load i32, i32* %b, align 4
  %arrayidx365 = getelementptr inbounds [64 x [2 x i32]], [64 x [2 x i32]]* %s3ind364, i32 0, i32 %252
  %arrayidx366 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx365, i32 0, i32 1
  store i32 %sub362, i32* %arrayidx366, align 4
  br label %if.end367

if.end367:                                        ; preds = %if.then359, %for.body349
  br label %for.inc368

for.inc368:                                       ; preds = %if.end367
  %253 = load i32, i32* %b, align 4
  %inc369 = add nsw i32 %253, 1
  store i32 %inc369, i32* %b, align 4
  br label %for.cond344

for.end370:                                       ; preds = %for.cond344
  %254 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %254, i32 0, i32 15
  %255 = load i32, i32* %mode_gr, align 4
  %conv371 = sitofp i32 %255 to double
  %mul372 = fmul double 5.760000e+02, %conv371
  %256 = load float, float* %sfreq, align 4
  %conv373 = fpext float %256 to double
  %div374 = fdiv double %mul372, %conv373
  %mul375 = fmul double -1.200000e+00, %div374
  %257 = call double @llvm.pow.f64(double 1.000000e+01, double %mul375)
  %conv376 = fptrunc double %257 to float
  %258 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH377 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %258, i32 0, i32 21
  %259 = load %struct.ATH_t*, %struct.ATH_t** %ATH377, align 8
  %decay378 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %259, i32 0, i32 4
  store float %conv376, float* %decay378, align 4
  %260 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH379 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %260, i32 0, i32 21
  %261 = load %struct.ATH_t*, %struct.ATH_t** %ATH379, align 8
  %adjust_factor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %261, i32 0, i32 2
  store float 0x3F847AE140000000, float* %adjust_factor, align 4
  %262 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH380 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %262, i32 0, i32 21
  %263 = load %struct.ATH_t*, %struct.ATH_t** %ATH380, align 8
  %adjust_limit = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %263, i32 0, i32 3
  store float 1.000000e+00, float* %adjust_limit, align 4
  %264 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHtype = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %264, i32 0, i32 48
  %265 = load i32, i32* %ATHtype, align 4
  %cmp381 = icmp ne i32 %265, -1
  br i1 %cmp381, label %if.then383, label %if.end419

if.then383:                                       ; preds = %for.end370
  %266 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out385 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %266, i32 0, i32 12
  %267 = load i32, i32* %samplerate_out385, align 4
  %conv386 = sitofp i32 %267 to float
  %div387 = fdiv float %conv386, 1.024000e+03
  store float %div387, float* %freq_inc, align 4
  store float 0.000000e+00, float* %eql_balance, align 4
  store float 0.000000e+00, float* %freq384, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond388

for.cond388:                                      ; preds = %for.inc404, %if.then383
  %268 = load i32, i32* %i, align 4
  %cmp389 = icmp slt i32 %268, 512
  br i1 %cmp389, label %for.body391, label %for.end406

for.body391:                                      ; preds = %for.cond388
  %269 = load float, float* %freq_inc, align 4
  %270 = load float, float* %freq384, align 4
  %add392 = fadd float %270, %269
  store float %add392, float* %freq384, align 4
  %271 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %272 = load float, float* %freq384, align 4
  %call393 = call float @ATHformula(%struct.SessionConfig_t* %271, float %272)
  %div394 = fdiv float %call393, 1.000000e+01
  %conv395 = fpext float %div394 to double
  %273 = call double @llvm.pow.f64(double 1.000000e+01, double %conv395)
  %div396 = fdiv double 1.000000e+00, %273
  %conv397 = fptrunc double %div396 to float
  %274 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH398 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %274, i32 0, i32 21
  %275 = load %struct.ATH_t*, %struct.ATH_t** %ATH398, align 8
  %eql_w = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %275, i32 0, i32 12
  %276 = load i32, i32* %i, align 4
  %arrayidx399 = getelementptr inbounds [512 x float], [512 x float]* %eql_w, i32 0, i32 %276
  store float %conv397, float* %arrayidx399, align 4
  %277 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH400 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %277, i32 0, i32 21
  %278 = load %struct.ATH_t*, %struct.ATH_t** %ATH400, align 8
  %eql_w401 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %278, i32 0, i32 12
  %279 = load i32, i32* %i, align 4
  %arrayidx402 = getelementptr inbounds [512 x float], [512 x float]* %eql_w401, i32 0, i32 %279
  %280 = load float, float* %arrayidx402, align 4
  %281 = load float, float* %eql_balance, align 4
  %add403 = fadd float %281, %280
  store float %add403, float* %eql_balance, align 4
  br label %for.inc404

for.inc404:                                       ; preds = %for.body391
  %282 = load i32, i32* %i, align 4
  %inc405 = add nsw i32 %282, 1
  store i32 %inc405, i32* %i, align 4
  br label %for.cond388

for.end406:                                       ; preds = %for.cond388
  %283 = load float, float* %eql_balance, align 4
  %conv407 = fpext float %283 to double
  %div408 = fdiv double 1.000000e+00, %conv407
  %conv409 = fptrunc double %div408 to float
  store float %conv409, float* %eql_balance, align 4
  store i32 512, i32* %i, align 4
  br label %for.cond410

for.cond410:                                      ; preds = %for.body413, %for.end406
  %284 = load i32, i32* %i, align 4
  %dec = add nsw i32 %284, -1
  store i32 %dec, i32* %i, align 4
  %cmp411 = icmp sge i32 %dec, 0
  br i1 %cmp411, label %for.body413, label %for.end418

for.body413:                                      ; preds = %for.cond410
  %285 = load float, float* %eql_balance, align 4
  %286 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH414 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %286, i32 0, i32 21
  %287 = load %struct.ATH_t*, %struct.ATH_t** %ATH414, align 8
  %eql_w415 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %287, i32 0, i32 12
  %288 = load i32, i32* %i, align 4
  %arrayidx416 = getelementptr inbounds [512 x float], [512 x float]* %eql_w415, i32 0, i32 %288
  %289 = load float, float* %arrayidx416, align 4
  %mul417 = fmul float %289, %285
  store float %mul417, float* %arrayidx416, align 4
  br label %for.cond410

for.end418:                                       ; preds = %for.cond410
  br label %if.end419

if.end419:                                        ; preds = %for.end418, %for.end370
  store i32 0, i32* %j, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond420

for.cond420:                                      ; preds = %for.inc437, %if.end419
  %290 = load i32, i32* %b, align 4
  %291 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s421 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %291, i32 0, i32 3
  %npart422 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s421, i32 0, i32 11
  %292 = load i32, i32* %npart422, align 4
  %cmp423 = icmp slt i32 %290, %292
  br i1 %cmp423, label %for.body425, label %for.end439

for.body425:                                      ; preds = %for.cond420
  store i32 0, i32* %i, align 4
  br label %for.cond426

for.cond426:                                      ; preds = %for.inc434, %for.body425
  %293 = load i32, i32* %i, align 4
  %294 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s427 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %294, i32 0, i32 3
  %numlines428 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s427, i32 0, i32 8
  %295 = load i32, i32* %b, align 4
  %arrayidx429 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines428, i32 0, i32 %295
  %296 = load i32, i32* %arrayidx429, align 4
  %cmp430 = icmp slt i32 %293, %296
  br i1 %cmp430, label %for.body432, label %for.end436

for.body432:                                      ; preds = %for.cond426
  %297 = load i32, i32* %j, align 4
  %inc433 = add nsw i32 %297, 1
  store i32 %inc433, i32* %j, align 4
  br label %for.inc434

for.inc434:                                       ; preds = %for.body432
  %298 = load i32, i32* %i, align 4
  %inc435 = add nsw i32 %298, 1
  store i32 %inc435, i32* %i, align 4
  br label %for.cond426

for.end436:                                       ; preds = %for.cond426
  br label %for.inc437

for.inc437:                                       ; preds = %for.end436
  %299 = load i32, i32* %b, align 4
  %inc438 = add nsw i32 %299, 1
  store i32 %inc438, i32* %b, align 4
  br label %for.cond420

for.end439:                                       ; preds = %for.cond420
  store i32 0, i32* %j, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond440

for.cond440:                                      ; preds = %for.inc457, %for.end439
  %300 = load i32, i32* %b, align 4
  %301 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l441 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %301, i32 0, i32 2
  %npart442 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l441, i32 0, i32 11
  %302 = load i32, i32* %npart442, align 4
  %cmp443 = icmp slt i32 %300, %302
  br i1 %cmp443, label %for.body445, label %for.end459

for.body445:                                      ; preds = %for.cond440
  store i32 0, i32* %i, align 4
  br label %for.cond446

for.cond446:                                      ; preds = %for.inc454, %for.body445
  %303 = load i32, i32* %i, align 4
  %304 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l447 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %304, i32 0, i32 2
  %numlines448 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l447, i32 0, i32 8
  %305 = load i32, i32* %b, align 4
  %arrayidx449 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines448, i32 0, i32 %305
  %306 = load i32, i32* %arrayidx449, align 4
  %cmp450 = icmp slt i32 %303, %306
  br i1 %cmp450, label %for.body452, label %for.end456

for.body452:                                      ; preds = %for.cond446
  %307 = load i32, i32* %j, align 4
  %inc453 = add nsw i32 %307, 1
  store i32 %inc453, i32* %j, align 4
  br label %for.inc454

for.inc454:                                       ; preds = %for.body452
  %308 = load i32, i32* %i, align 4
  %inc455 = add nsw i32 %308, 1
  store i32 %inc455, i32* %i, align 4
  br label %for.cond446

for.end456:                                       ; preds = %for.cond446
  br label %for.inc457

for.inc457:                                       ; preds = %for.end456
  %309 = load i32, i32* %b, align 4
  %inc458 = add nsw i32 %309, 1
  store i32 %inc458, i32* %b, align 4
  br label %for.cond440

for.end459:                                       ; preds = %for.cond440
  %310 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %310, i32 0, i32 66
  %311 = load float, float* %attackthre, align 4
  store float %311, float* %x460, align 4
  %312 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre_s = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %312, i32 0, i32 67
  %313 = load float, float* %attackthre_s, align 4
  store float %313, float* %y, align 4
  %314 = load float, float* %x460, align 4
  %cmp461 = fcmp olt float %314, 0.000000e+00
  br i1 %cmp461, label %if.then463, label %if.end464

if.then463:                                       ; preds = %for.end459
  store float 0x40119999A0000000, float* %x460, align 4
  br label %if.end464

if.end464:                                        ; preds = %if.then463, %for.end459
  %315 = load float, float* %y, align 4
  %cmp465 = fcmp olt float %315, 0.000000e+00
  br i1 %cmp465, label %if.then467, label %if.end468

if.then467:                                       ; preds = %if.end464
  store float 2.500000e+01, float* %y, align 4
  br label %if.end468

if.end468:                                        ; preds = %if.then467, %if.end464
  %316 = load float, float* %x460, align 4
  %317 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %attack_threshold = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %317, i32 0, i32 5
  %arrayidx469 = getelementptr inbounds [4 x float], [4 x float]* %attack_threshold, i32 0, i32 2
  store float %316, float* %arrayidx469, align 4
  %318 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %attack_threshold470 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %318, i32 0, i32 5
  %arrayidx471 = getelementptr inbounds [4 x float], [4 x float]* %attack_threshold470, i32 0, i32 1
  store float %316, float* %arrayidx471, align 4
  %319 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %attack_threshold472 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %319, i32 0, i32 5
  %arrayidx473 = getelementptr inbounds [4 x float], [4 x float]* %attack_threshold472, i32 0, i32 0
  store float %316, float* %arrayidx473, align 4
  %320 = load float, float* %y, align 4
  %321 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %attack_threshold474 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %321, i32 0, i32 5
  %arrayidx475 = getelementptr inbounds [4 x float], [4 x float]* %attack_threshold474, i32 0, i32 3
  store float %320, float* %arrayidx475, align 4
  store float -1.000000e+01, float* %sk_s, align 4
  store float 0xC012CCCCC0000000, float* %sk_l, align 4
  %322 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %322, i32 0, i32 41
  %323 = load i32, i32* %VBR_q, align 4
  %cmp476 = icmp slt i32 %323, 4
  br i1 %cmp476, label %if.then478, label %if.else

if.then478:                                       ; preds = %if.end468
  %324 = load float, float* getelementptr inbounds ([11 x float], [11 x float]* @psymodel_init.sk, i32 0, i32 0), align 16
  store float %324, float* %sk_s, align 4
  store float %324, float* %sk_l, align 4
  br label %if.end489

if.else:                                          ; preds = %if.end468
  %325 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q479 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %325, i32 0, i32 41
  %326 = load i32, i32* %VBR_q479, align 4
  %arrayidx480 = getelementptr inbounds [11 x float], [11 x float]* @psymodel_init.sk, i32 0, i32 %326
  %327 = load float, float* %arrayidx480, align 4
  %328 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %328, i32 0, i32 40
  %329 = load float, float* %VBR_q_frac, align 4
  %330 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q481 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %330, i32 0, i32 41
  %331 = load i32, i32* %VBR_q481, align 4
  %arrayidx482 = getelementptr inbounds [11 x float], [11 x float]* @psymodel_init.sk, i32 0, i32 %331
  %332 = load float, float* %arrayidx482, align 4
  %333 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q483 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %333, i32 0, i32 41
  %334 = load i32, i32* %VBR_q483, align 4
  %add484 = add nsw i32 %334, 1
  %arrayidx485 = getelementptr inbounds [11 x float], [11 x float]* @psymodel_init.sk, i32 0, i32 %add484
  %335 = load float, float* %arrayidx485, align 4
  %sub486 = fsub float %332, %335
  %mul487 = fmul float %329, %sub486
  %add488 = fadd float %327, %mul487
  store float %add488, float* %sk_s, align 4
  store float %add488, float* %sk_l, align 4
  br label %if.end489

if.end489:                                        ; preds = %if.else, %if.then478
  store i32 0, i32* %b, align 4
  br label %for.cond490

for.cond490:                                      ; preds = %for.inc508, %if.end489
  %336 = load i32, i32* %b, align 4
  %337 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s491 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %337, i32 0, i32 3
  %npart492 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s491, i32 0, i32 11
  %338 = load i32, i32* %npart492, align 4
  %cmp493 = icmp slt i32 %336, %338
  br i1 %cmp493, label %for.body495, label %for.end510

for.body495:                                      ; preds = %for.cond490
  %339 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s496 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %339, i32 0, i32 3
  %npart497 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s496, i32 0, i32 11
  %340 = load i32, i32* %npart497, align 4
  %341 = load i32, i32* %b, align 4
  %sub498 = sub nsw i32 %340, %341
  %conv499 = sitofp i32 %sub498 to float
  %342 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s500 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %342, i32 0, i32 3
  %npart501 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s500, i32 0, i32 11
  %343 = load i32, i32* %npart501, align 4
  %conv502 = sitofp i32 %343 to float
  %div503 = fdiv float %conv499, %conv502
  store float %div503, float* %m, align 4
  %344 = load float, float* %sk_s, align 4
  %345 = load float, float* %m, align 4
  %mul504 = fmul float %344, %345
  %mul505 = fmul float %mul504, 0x3FB99999A0000000
  %346 = call float @llvm.pow.f32(float 1.000000e+01, float %mul505)
  %347 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s506 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %347, i32 0, i32 3
  %masking_lower = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s506, i32 0, i32 0
  %348 = load i32, i32* %b, align 4
  %arrayidx507 = getelementptr inbounds [64 x float], [64 x float]* %masking_lower, i32 0, i32 %348
  store float %346, float* %arrayidx507, align 4
  br label %for.inc508

for.inc508:                                       ; preds = %for.body495
  %349 = load i32, i32* %b, align 4
  %inc509 = add nsw i32 %349, 1
  store i32 %inc509, i32* %b, align 4
  br label %for.cond490

for.end510:                                       ; preds = %for.cond490
  br label %for.cond511

for.cond511:                                      ; preds = %for.inc518, %for.end510
  %350 = load i32, i32* %b, align 4
  %cmp512 = icmp slt i32 %350, 64
  br i1 %cmp512, label %for.body514, label %for.end520

for.body514:                                      ; preds = %for.cond511
  %351 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %s515 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %351, i32 0, i32 3
  %masking_lower516 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s515, i32 0, i32 0
  %352 = load i32, i32* %b, align 4
  %arrayidx517 = getelementptr inbounds [64 x float], [64 x float]* %masking_lower516, i32 0, i32 %352
  store float 1.000000e+00, float* %arrayidx517, align 4
  br label %for.inc518

for.inc518:                                       ; preds = %for.body514
  %353 = load i32, i32* %b, align 4
  %inc519 = add nsw i32 %353, 1
  store i32 %inc519, i32* %b, align 4
  br label %for.cond511

for.end520:                                       ; preds = %for.cond511
  store i32 0, i32* %b, align 4
  br label %for.cond521

for.cond521:                                      ; preds = %for.inc541, %for.end520
  %354 = load i32, i32* %b, align 4
  %355 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l522 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %355, i32 0, i32 2
  %npart523 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l522, i32 0, i32 11
  %356 = load i32, i32* %npart523, align 4
  %cmp524 = icmp slt i32 %354, %356
  br i1 %cmp524, label %for.body526, label %for.end543

for.body526:                                      ; preds = %for.cond521
  %357 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l528 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %357, i32 0, i32 2
  %npart529 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l528, i32 0, i32 11
  %358 = load i32, i32* %npart529, align 4
  %359 = load i32, i32* %b, align 4
  %sub530 = sub nsw i32 %358, %359
  %conv531 = sitofp i32 %sub530 to float
  %360 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l532 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %360, i32 0, i32 2
  %npart533 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l532, i32 0, i32 11
  %361 = load i32, i32* %npart533, align 4
  %conv534 = sitofp i32 %361 to float
  %div535 = fdiv float %conv531, %conv534
  store float %div535, float* %m527, align 4
  %362 = load float, float* %sk_l, align 4
  %363 = load float, float* %m527, align 4
  %mul536 = fmul float %362, %363
  %mul537 = fmul float %mul536, 0x3FB99999A0000000
  %364 = call float @llvm.pow.f32(float 1.000000e+01, float %mul537)
  %365 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l538 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %365, i32 0, i32 2
  %masking_lower539 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l538, i32 0, i32 0
  %366 = load i32, i32* %b, align 4
  %arrayidx540 = getelementptr inbounds [64 x float], [64 x float]* %masking_lower539, i32 0, i32 %366
  store float %364, float* %arrayidx540, align 4
  br label %for.inc541

for.inc541:                                       ; preds = %for.body526
  %367 = load i32, i32* %b, align 4
  %inc542 = add nsw i32 %367, 1
  store i32 %inc542, i32* %b, align 4
  br label %for.cond521

for.end543:                                       ; preds = %for.cond521
  br label %for.cond544

for.cond544:                                      ; preds = %for.inc551, %for.end543
  %368 = load i32, i32* %b, align 4
  %cmp545 = icmp slt i32 %368, 64
  br i1 %cmp545, label %for.body547, label %for.end553

for.body547:                                      ; preds = %for.cond544
  %369 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l548 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %369, i32 0, i32 2
  %masking_lower549 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l548, i32 0, i32 0
  %370 = load i32, i32* %b, align 4
  %arrayidx550 = getelementptr inbounds [64 x float], [64 x float]* %masking_lower549, i32 0, i32 %370
  store float 1.000000e+00, float* %arrayidx550, align 4
  br label %for.inc551

for.inc551:                                       ; preds = %for.body547
  %371 = load i32, i32* %b, align 4
  %inc552 = add nsw i32 %371, 1
  store i32 %inc552, i32* %b, align 4
  br label %for.cond544

for.end553:                                       ; preds = %for.cond544
  %372 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l_to_s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %372, i32 0, i32 4
  %373 = bitcast %struct.PsyConst_CB2SB_t* %l_to_s to i8*
  %374 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l554 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %374, i32 0, i32 2
  %375 = bitcast %struct.PsyConst_CB2SB_t* %l554 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %373, i8* align 4 %375, i32 2160, i1 false)
  %376 = load %struct.PsyConst_t*, %struct.PsyConst_t** %gd, align 4
  %l_to_s555 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %376, i32 0, i32 4
  %377 = load float, float* %sfreq, align 4
  %378 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band556 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %378, i32 0, i32 8
  %s557 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band556, i32 0, i32 1
  %arraydecay558 = getelementptr inbounds [14 x i32], [14 x i32]* %s557, i32 0, i32 0
  call void @init_numline(%struct.PsyConst_CB2SB_t* %l_to_s555, float %377, i32 1024, i32 192, i32 13, i32* %arraydecay558)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end553, %if.then326, %if.then112, %if.then
  %379 = load i32, i32* %retval, align 4
  ret i32 %379
}

declare i8* @calloc(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define internal void @init_numline(%struct.PsyConst_CB2SB_t* %gd, float %sfreq, i32 %fft_size, i32 %mdct_size, i32 %sbmax, i32* %scalepos) #0 {
entry:
  %gd.addr = alloca %struct.PsyConst_CB2SB_t*, align 4
  %sfreq.addr = alloca float, align 4
  %fft_size.addr = alloca i32, align 4
  %mdct_size.addr = alloca i32, align 4
  %sbmax.addr = alloca i32, align 4
  %scalepos.addr = alloca i32*, align 4
  %b_frq = alloca [65 x float], align 16
  %mdct_freq_frac = alloca float, align 4
  %deltafreq = alloca float, align 4
  %partition = alloca [513 x i32], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ni = alloca i32, align 4
  %sfb = alloca i32, align 4
  %bark1 = alloca float, align 4
  %j2 = alloca i32, align 4
  %nl = alloca i32, align 4
  %nl50 = alloca i32, align 4
  %freq = alloca float, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %bo = alloca i32, align 4
  %start = alloca i32, align 4
  %end = alloca i32, align 4
  %f_tmp = alloca float, align 4
  %bo_w = alloca float, align 4
  store %struct.PsyConst_CB2SB_t* %gd, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  store float %sfreq, float* %sfreq.addr, align 4
  store i32 %fft_size, i32* %fft_size.addr, align 4
  store i32 %mdct_size, i32* %mdct_size.addr, align 4
  store i32 %sbmax, i32* %sbmax.addr, align 4
  store i32* %scalepos, i32** %scalepos.addr, align 4
  %0 = load float, float* %sfreq.addr, align 4
  %1 = load i32, i32* %mdct_size.addr, align 4
  %conv = sitofp i32 %1 to float
  %mul = fmul float 2.000000e+00, %conv
  %div = fdiv float %0, %mul
  store float %div, float* %mdct_freq_frac, align 4
  %2 = load i32, i32* %fft_size.addr, align 4
  %conv1 = sitofp i32 %2 to float
  %3 = load i32, i32* %mdct_size.addr, align 4
  %conv2 = sitofp i32 %3 to float
  %mul3 = fmul float 2.000000e+00, %conv2
  %div4 = fdiv float %conv1, %mul3
  store float %div4, float* %deltafreq, align 4
  %4 = bitcast [513 x i32]* %partition to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 2052, i1 false)
  %5 = load i32, i32* %fft_size.addr, align 4
  %conv5 = sitofp i32 %5 to float
  %6 = load float, float* %sfreq.addr, align 4
  %div6 = fdiv float %6, %conv5
  store float %div6, float* %sfreq.addr, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %ni, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %entry
  %7 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %7, 64
  br i1 %cmp, label %for.body, label %for.end41

for.body:                                         ; preds = %for.cond
  %8 = load float, float* %sfreq.addr, align 4
  %9 = load i32, i32* %j, align 4
  %conv8 = sitofp i32 %9 to float
  %mul9 = fmul float %8, %conv8
  %call = call float @freq2bark(float %mul9)
  store float %call, float* %bark1, align 4
  %10 = load float, float* %sfreq.addr, align 4
  %11 = load i32, i32* %j, align 4
  %conv10 = sitofp i32 %11 to float
  %mul11 = fmul float %10, %conv10
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [65 x float], [65 x float]* %b_frq, i32 0, i32 %12
  store float %mul11, float* %arrayidx, align 4
  %13 = load i32, i32* %j, align 4
  store i32 %13, i32* %j2, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %14 = load float, float* %sfreq.addr, align 4
  %15 = load i32, i32* %j2, align 4
  %conv13 = sitofp i32 %15 to float
  %mul14 = fmul float %14, %conv13
  %call15 = call float @freq2bark(float %mul14)
  %16 = load float, float* %bark1, align 4
  %sub = fsub float %call15, %16
  %conv16 = fpext float %sub to double
  %cmp17 = fcmp olt double %conv16, 3.400000e-01
  br i1 %cmp17, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond12
  %17 = load i32, i32* %j2, align 4
  %18 = load i32, i32* %fft_size.addr, align 4
  %div19 = sdiv i32 %18, 2
  %cmp20 = icmp sle i32 %17, %div19
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond12
  %19 = phi i1 [ false, %for.cond12 ], [ %cmp20, %land.rhs ]
  br i1 %19, label %for.body22, label %for.end

for.body22:                                       ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body22
  %20 = load i32, i32* %j2, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j2, align 4
  br label %for.cond12

for.end:                                          ; preds = %land.end
  %21 = load i32, i32* %j2, align 4
  %22 = load i32, i32* %j, align 4
  %sub23 = sub nsw i32 %21, %22
  store i32 %sub23, i32* %nl, align 4
  %23 = load i32, i32* %nl, align 4
  %24 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %24, i32 0, i32 8
  %25 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %25
  store i32 %23, i32* %arrayidx24, align 4
  %26 = load i32, i32* %nl, align 4
  %cmp25 = icmp sgt i32 %26, 0
  br i1 %cmp25, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %27 = load i32, i32* %nl, align 4
  %conv27 = sitofp i32 %27 to float
  %div28 = fdiv float 1.000000e+00, %conv27
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div28, %cond.true ], [ 0.000000e+00, %cond.false ]
  %28 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %rnumlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %28, i32 0, i32 2
  %29 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds [64 x float], [64 x float]* %rnumlines, i32 0, i32 %29
  store float %cond, float* %arrayidx29, align 4
  %30 = load i32, i32* %i, align 4
  %add = add nsw i32 %30, 1
  store i32 %add, i32* %ni, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %31 = load i32, i32* %j, align 4
  %32 = load i32, i32* %j2, align 4
  %cmp30 = icmp slt i32 %31, %32
  br i1 %cmp30, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load i32, i32* %i, align 4
  %34 = load i32, i32* %j, align 4
  %inc32 = add nsw i32 %34, 1
  store i32 %inc32, i32* %j, align 4
  %arrayidx33 = getelementptr inbounds [513 x i32], [513 x i32]* %partition, i32 0, i32 %34
  store i32 %33, i32* %arrayidx33, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %35 = load i32, i32* %j, align 4
  %36 = load i32, i32* %fft_size.addr, align 4
  %div34 = sdiv i32 %36, 2
  %cmp35 = icmp sgt i32 %35, %div34
  br i1 %cmp35, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %37 = load i32, i32* %fft_size.addr, align 4
  %div37 = sdiv i32 %37, 2
  store i32 %div37, i32* %j, align 4
  %38 = load i32, i32* %i, align 4
  %inc38 = add nsw i32 %38, 1
  store i32 %inc38, i32* %i, align 4
  br label %for.end41

if.end:                                           ; preds = %while.end
  br label %for.inc39

for.inc39:                                        ; preds = %if.end
  %39 = load i32, i32* %i, align 4
  %inc40 = add nsw i32 %39, 1
  store i32 %inc40, i32* %i, align 4
  br label %for.cond

for.end41:                                        ; preds = %if.then, %for.cond
  %40 = load float, float* %sfreq.addr, align 4
  %41 = load i32, i32* %j, align 4
  %conv42 = sitofp i32 %41 to float
  %mul43 = fmul float %40, %conv42
  %42 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [65 x float], [65 x float]* %b_frq, i32 0, i32 %42
  store float %mul43, float* %arrayidx44, align 4
  %43 = load i32, i32* %sbmax.addr, align 4
  %44 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %n_sb = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %44, i32 0, i32 12
  store i32 %43, i32* %n_sb, align 4
  %45 = load i32, i32* %ni, align 4
  %46 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %46, i32 0, i32 11
  store i32 %45, i32* %npart, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc61, %for.end41
  %47 = load i32, i32* %i, align 4
  %48 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %npart46 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %48, i32 0, i32 11
  %49 = load i32, i32* %npart46, align 4
  %cmp47 = icmp slt i32 %47, %49
  br i1 %cmp47, label %for.body49, label %for.end63

for.body49:                                       ; preds = %for.cond45
  %50 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %numlines51 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %50, i32 0, i32 8
  %51 = load i32, i32* %i, align 4
  %arrayidx52 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines51, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx52, align 4
  store i32 %52, i32* %nl50, align 4
  %53 = load float, float* %sfreq.addr, align 4
  %54 = load i32, i32* %j, align 4
  %55 = load i32, i32* %nl50, align 4
  %div53 = sdiv i32 %55, 2
  %add54 = add nsw i32 %54, %div53
  %conv55 = sitofp i32 %add54 to float
  %mul56 = fmul float %53, %conv55
  store float %mul56, float* %freq, align 4
  %56 = load float, float* %freq, align 4
  %conv57 = fpext float %56 to double
  %call58 = call float @stereo_demask(double %conv57)
  %57 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %mld_cb = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %57, i32 0, i32 3
  %58 = load i32, i32* %i, align 4
  %arrayidx59 = getelementptr inbounds [64 x float], [64 x float]* %mld_cb, i32 0, i32 %58
  store float %call58, float* %arrayidx59, align 4
  %59 = load i32, i32* %nl50, align 4
  %60 = load i32, i32* %j, align 4
  %add60 = add nsw i32 %60, %59
  store i32 %add60, i32* %j, align 4
  br label %for.inc61

for.inc61:                                        ; preds = %for.body49
  %61 = load i32, i32* %i, align 4
  %inc62 = add nsw i32 %61, 1
  store i32 %inc62, i32* %i, align 4
  br label %for.cond45

for.end63:                                        ; preds = %for.cond45
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc70, %for.end63
  %62 = load i32, i32* %i, align 4
  %cmp65 = icmp slt i32 %62, 64
  br i1 %cmp65, label %for.body67, label %for.end72

for.body67:                                       ; preds = %for.cond64
  %63 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %mld_cb68 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %63, i32 0, i32 3
  %64 = load i32, i32* %i, align 4
  %arrayidx69 = getelementptr inbounds [64 x float], [64 x float]* %mld_cb68, i32 0, i32 %64
  store float 1.000000e+00, float* %arrayidx69, align 4
  br label %for.inc70

for.inc70:                                        ; preds = %for.body67
  %65 = load i32, i32* %i, align 4
  %inc71 = add nsw i32 %65, 1
  store i32 %inc71, i32* %i, align 4
  br label %for.cond64

for.end72:                                        ; preds = %for.cond64
  store i32 0, i32* %sfb, align 4
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc133, %for.end72
  %66 = load i32, i32* %sfb, align 4
  %67 = load i32, i32* %sbmax.addr, align 4
  %cmp74 = icmp slt i32 %66, %67
  br i1 %cmp74, label %for.body76, label %for.end135

for.body76:                                       ; preds = %for.cond73
  %68 = load i32*, i32** %scalepos.addr, align 4
  %69 = load i32, i32* %sfb, align 4
  %arrayidx77 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx77, align 4
  store i32 %70, i32* %start, align 4
  %71 = load i32*, i32** %scalepos.addr, align 4
  %72 = load i32, i32* %sfb, align 4
  %add78 = add nsw i32 %72, 1
  %arrayidx79 = getelementptr inbounds i32, i32* %71, i32 %add78
  %73 = load i32, i32* %arrayidx79, align 4
  store i32 %73, i32* %end, align 4
  %74 = load float, float* %deltafreq, align 4
  %conv80 = fpext float %74 to double
  %75 = load i32, i32* %start, align 4
  %conv81 = sitofp i32 %75 to double
  %sub82 = fsub double %conv81, 5.000000e-01
  %mul83 = fmul double %conv80, %sub82
  %add84 = fadd double 5.000000e-01, %mul83
  %76 = call double @llvm.floor.f64(double %add84)
  %conv85 = fptosi double %76 to i32
  store i32 %conv85, i32* %i1, align 4
  %77 = load i32, i32* %i1, align 4
  %cmp86 = icmp slt i32 %77, 0
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %for.body76
  store i32 0, i32* %i1, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then88, %for.body76
  %78 = load float, float* %deltafreq, align 4
  %conv90 = fpext float %78 to double
  %79 = load i32, i32* %end, align 4
  %conv91 = sitofp i32 %79 to double
  %sub92 = fsub double %conv91, 5.000000e-01
  %mul93 = fmul double %conv90, %sub92
  %add94 = fadd double 5.000000e-01, %mul93
  %80 = call double @llvm.floor.f64(double %add94)
  %conv95 = fptosi double %80 to i32
  store i32 %conv95, i32* %i2, align 4
  %81 = load i32, i32* %i2, align 4
  %82 = load i32, i32* %fft_size.addr, align 4
  %div96 = sdiv i32 %82, 2
  %cmp97 = icmp sgt i32 %81, %div96
  br i1 %cmp97, label %if.then99, label %if.end101

if.then99:                                        ; preds = %if.end89
  %83 = load i32, i32* %fft_size.addr, align 4
  %div100 = sdiv i32 %83, 2
  store i32 %div100, i32* %i2, align 4
  br label %if.end101

if.end101:                                        ; preds = %if.then99, %if.end89
  %84 = load i32, i32* %i2, align 4
  %arrayidx102 = getelementptr inbounds [513 x i32], [513 x i32]* %partition, i32 0, i32 %84
  %85 = load i32, i32* %arrayidx102, align 4
  store i32 %85, i32* %bo, align 4
  %86 = load i32, i32* %i1, align 4
  %arrayidx103 = getelementptr inbounds [513 x i32], [513 x i32]* %partition, i32 0, i32 %86
  %87 = load i32, i32* %arrayidx103, align 4
  %88 = load i32, i32* %i2, align 4
  %arrayidx104 = getelementptr inbounds [513 x i32], [513 x i32]* %partition, i32 0, i32 %88
  %89 = load i32, i32* %arrayidx104, align 4
  %add105 = add nsw i32 %87, %89
  %div106 = sdiv i32 %add105, 2
  %90 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %bm = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %90, i32 0, i32 9
  %91 = load i32, i32* %sfb, align 4
  %arrayidx107 = getelementptr inbounds [22 x i32], [22 x i32]* %bm, i32 0, i32 %91
  store i32 %div106, i32* %arrayidx107, align 4
  %92 = load i32, i32* %bo, align 4
  %93 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %bo108 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %93, i32 0, i32 10
  %94 = load i32, i32* %sfb, align 4
  %arrayidx109 = getelementptr inbounds [22 x i32], [22 x i32]* %bo108, i32 0, i32 %94
  store i32 %92, i32* %arrayidx109, align 4
  %95 = load float, float* %mdct_freq_frac, align 4
  %96 = load i32, i32* %end, align 4
  %conv110 = sitofp i32 %96 to float
  %mul111 = fmul float %95, %conv110
  store float %mul111, float* %f_tmp, align 4
  %97 = load float, float* %f_tmp, align 4
  %98 = load i32, i32* %bo, align 4
  %arrayidx112 = getelementptr inbounds [65 x float], [65 x float]* %b_frq, i32 0, i32 %98
  %99 = load float, float* %arrayidx112, align 4
  %sub113 = fsub float %97, %99
  %100 = load i32, i32* %bo, align 4
  %add114 = add nsw i32 %100, 1
  %arrayidx115 = getelementptr inbounds [65 x float], [65 x float]* %b_frq, i32 0, i32 %add114
  %101 = load float, float* %arrayidx115, align 4
  %102 = load i32, i32* %bo, align 4
  %arrayidx116 = getelementptr inbounds [65 x float], [65 x float]* %b_frq, i32 0, i32 %102
  %103 = load float, float* %arrayidx116, align 4
  %sub117 = fsub float %101, %103
  %div118 = fdiv float %sub113, %sub117
  store float %div118, float* %bo_w, align 4
  %104 = load float, float* %bo_w, align 4
  %cmp119 = fcmp olt float %104, 0.000000e+00
  br i1 %cmp119, label %if.then121, label %if.else

if.then121:                                       ; preds = %if.end101
  store float 0.000000e+00, float* %bo_w, align 4
  br label %if.end126

if.else:                                          ; preds = %if.end101
  %105 = load float, float* %bo_w, align 4
  %cmp122 = fcmp ogt float %105, 1.000000e+00
  br i1 %cmp122, label %if.then124, label %if.end125

if.then124:                                       ; preds = %if.else
  store float 1.000000e+00, float* %bo_w, align 4
  br label %if.end125

if.end125:                                        ; preds = %if.then124, %if.else
  br label %if.end126

if.end126:                                        ; preds = %if.end125, %if.then121
  %106 = load float, float* %bo_w, align 4
  %107 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %bo_weight = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %107, i32 0, i32 5
  %108 = load i32, i32* %sfb, align 4
  %arrayidx127 = getelementptr inbounds [22 x float], [22 x float]* %bo_weight, i32 0, i32 %108
  store float %106, float* %arrayidx127, align 4
  %109 = load float, float* %mdct_freq_frac, align 4
  %110 = load i32, i32* %start, align 4
  %conv128 = sitofp i32 %110 to float
  %mul129 = fmul float %109, %conv128
  %conv130 = fpext float %mul129 to double
  %call131 = call float @stereo_demask(double %conv130)
  %111 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %mld = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %111, i32 0, i32 4
  %112 = load i32, i32* %sfb, align 4
  %arrayidx132 = getelementptr inbounds [22 x float], [22 x float]* %mld, i32 0, i32 %112
  store float %call131, float* %arrayidx132, align 4
  br label %for.inc133

for.inc133:                                       ; preds = %if.end126
  %113 = load i32, i32* %sfb, align 4
  %inc134 = add nsw i32 %113, 1
  store i32 %inc134, i32* %sfb, align 4
  br label %for.cond73

for.end135:                                       ; preds = %for.cond73
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @compute_bark_values(%struct.PsyConst_CB2SB_t* %gd, float %sfreq, i32 %fft_size, float* %bval, float* %bval_width) #0 {
entry:
  %gd.addr = alloca %struct.PsyConst_CB2SB_t*, align 4
  %sfreq.addr = alloca float, align 4
  %fft_size.addr = alloca i32, align 4
  %bval.addr = alloca float*, align 4
  %bval_width.addr = alloca float*, align 4
  %k = alloca i32, align 4
  %j = alloca i32, align 4
  %ni = alloca i32, align 4
  %w = alloca i32, align 4
  %bark1 = alloca float, align 4
  %bark2 = alloca float, align 4
  store %struct.PsyConst_CB2SB_t* %gd, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  store float %sfreq, float* %sfreq.addr, align 4
  store i32 %fft_size, i32* %fft_size.addr, align 4
  store float* %bval, float** %bval.addr, align 4
  store float* %bval_width, float** %bval_width.addr, align 4
  store i32 0, i32* %j, align 4
  %0 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %0, i32 0, i32 11
  %1 = load i32, i32* %npart, align 4
  store i32 %1, i32* %ni, align 4
  %2 = load i32, i32* %fft_size.addr, align 4
  %conv = sitofp i32 %2 to float
  %3 = load float, float* %sfreq.addr, align 4
  %div = fdiv float %3, %conv
  store float %div, float* %sfreq.addr, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %k, align 4
  %5 = load i32, i32* %ni, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %6, i32 0, i32 8
  %7 = load i32, i32* %k, align 4
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx, align 4
  store i32 %8, i32* %w, align 4
  %9 = load float, float* %sfreq.addr, align 4
  %10 = load i32, i32* %j, align 4
  %conv2 = sitofp i32 %10 to float
  %mul = fmul float %9, %conv2
  %call = call float @freq2bark(float %mul)
  store float %call, float* %bark1, align 4
  %11 = load float, float* %sfreq.addr, align 4
  %12 = load i32, i32* %j, align 4
  %13 = load i32, i32* %w, align 4
  %add = add nsw i32 %12, %13
  %sub = sub nsw i32 %add, 1
  %conv3 = sitofp i32 %sub to float
  %mul4 = fmul float %11, %conv3
  %call5 = call float @freq2bark(float %mul4)
  store float %call5, float* %bark2, align 4
  %14 = load float, float* %bark1, align 4
  %15 = load float, float* %bark2, align 4
  %add6 = fadd float %14, %15
  %conv7 = fpext float %add6 to double
  %mul8 = fmul double 5.000000e-01, %conv7
  %conv9 = fptrunc double %mul8 to float
  %16 = load float*, float** %bval.addr, align 4
  %17 = load i32, i32* %k, align 4
  %arrayidx10 = getelementptr inbounds float, float* %16, i32 %17
  store float %conv9, float* %arrayidx10, align 4
  %18 = load float, float* %sfreq.addr, align 4
  %conv11 = fpext float %18 to double
  %19 = load i32, i32* %j, align 4
  %conv12 = sitofp i32 %19 to double
  %sub13 = fsub double %conv12, 5.000000e-01
  %mul14 = fmul double %conv11, %sub13
  %conv15 = fptrunc double %mul14 to float
  %call16 = call float @freq2bark(float %conv15)
  store float %call16, float* %bark1, align 4
  %20 = load float, float* %sfreq.addr, align 4
  %conv17 = fpext float %20 to double
  %21 = load i32, i32* %j, align 4
  %22 = load i32, i32* %w, align 4
  %add18 = add nsw i32 %21, %22
  %conv19 = sitofp i32 %add18 to double
  %sub20 = fsub double %conv19, 5.000000e-01
  %mul21 = fmul double %conv17, %sub20
  %conv22 = fptrunc double %mul21 to float
  %call23 = call float @freq2bark(float %conv22)
  store float %call23, float* %bark2, align 4
  %23 = load float, float* %bark2, align 4
  %24 = load float, float* %bark1, align 4
  %sub24 = fsub float %23, %24
  %25 = load float*, float** %bval_width.addr, align 4
  %26 = load i32, i32* %k, align 4
  %arrayidx25 = getelementptr inbounds float, float* %25, i32 %26
  store float %sub24, float* %arrayidx25, align 4
  %27 = load i32, i32* %w, align 4
  %28 = load i32, i32* %j, align 4
  %add26 = add nsw i32 %28, %27
  store i32 %add26, i32* %j, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %k, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #4

; Function Attrs: noinline nounwind optnone
define internal i32 @init_s3_values(float** %p, [2 x i32]* %s3ind, i32 %npart, float* %bval, float* %bval_width, float* %norm) #0 {
entry:
  %retval = alloca i32, align 4
  %p.addr = alloca float**, align 4
  %s3ind.addr = alloca [2 x i32]*, align 4
  %npart.addr = alloca i32, align 4
  %bval.addr = alloca float*, align 4
  %bval_width.addr = alloca float*, align 4
  %norm.addr = alloca float*, align 4
  %s3 = alloca [64 x [64 x float]], align 16
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %numberOfNoneZero = alloca i32, align 4
  %v = alloca float, align 4
  store float** %p, float*** %p.addr, align 4
  store [2 x i32]* %s3ind, [2 x i32]** %s3ind.addr, align 4
  store i32 %npart, i32* %npart.addr, align 4
  store float* %bval, float** %bval.addr, align 4
  store float* %bval_width, float** %bval_width.addr, align 4
  store float* %norm, float** %norm.addr, align 4
  store i32 0, i32* %numberOfNoneZero, align 4
  %arrayidx = getelementptr inbounds [64 x [64 x float]], [64 x [64 x float]]* %s3, i32 0, i32 0
  %arrayidx1 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx, i32 0, i32 0
  %0 = bitcast float* %arrayidx1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %0, i8 0, i32 16384, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %npart.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end14

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %j, align 4
  %4 = load i32, i32* %npart.addr, align 4
  %cmp3 = icmp slt i32 %3, %4
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %5 = load float*, float** %bval.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds float, float* %5, i32 %6
  %7 = load float, float* %arrayidx5, align 4
  %8 = load float*, float** %bval.addr, align 4
  %9 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx6, align 4
  %sub = fsub float %7, %10
  %call = call float @s3_func(float %sub)
  %11 = load float*, float** %bval_width.addr, align 4
  %12 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds float, float* %11, i32 %12
  %13 = load float, float* %arrayidx7, align 4
  %mul = fmul float %call, %13
  store float %mul, float* %v, align 4
  %14 = load float, float* %v, align 4
  %15 = load float*, float** %norm.addr, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %14, %17
  %18 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [64 x [64 x float]], [64 x [64 x float]]* %s3, i32 0, i32 %18
  %19 = load i32, i32* %j, align 4
  %arrayidx11 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx10, i32 0, i32 %19
  store float %mul9, float* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %20 = load i32, i32* %j, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i, align 4
  br label %for.cond

for.end14:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc48, %for.end14
  %22 = load i32, i32* %i, align 4
  %23 = load i32, i32* %npart.addr, align 4
  %cmp16 = icmp slt i32 %22, %23
  br i1 %cmp16, label %for.body17, label %for.end50

for.body17:                                       ; preds = %for.cond15
  store i32 0, i32* %j, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc24, %for.body17
  %24 = load i32, i32* %j, align 4
  %25 = load i32, i32* %npart.addr, align 4
  %cmp19 = icmp slt i32 %24, %25
  br i1 %cmp19, label %for.body20, label %for.end26

for.body20:                                       ; preds = %for.cond18
  %26 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds [64 x [64 x float]], [64 x [64 x float]]* %s3, i32 0, i32 %26
  %27 = load i32, i32* %j, align 4
  %arrayidx22 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx21, i32 0, i32 %27
  %28 = load float, float* %arrayidx22, align 4
  %cmp23 = fcmp ogt float %28, 0.000000e+00
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.body20
  br label %for.end26

if.end:                                           ; preds = %for.body20
  br label %for.inc24

for.inc24:                                        ; preds = %if.end
  %29 = load i32, i32* %j, align 4
  %inc25 = add nsw i32 %29, 1
  store i32 %inc25, i32* %j, align 4
  br label %for.cond18

for.end26:                                        ; preds = %if.then, %for.cond18
  %30 = load i32, i32* %j, align 4
  %31 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %32 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %31, i32 %32
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx27, i32 0, i32 0
  store i32 %30, i32* %arrayidx28, align 4
  %33 = load i32, i32* %npart.addr, align 4
  %sub29 = sub nsw i32 %33, 1
  store i32 %sub29, i32* %j, align 4
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc38, %for.end26
  %34 = load i32, i32* %j, align 4
  %cmp31 = icmp sgt i32 %34, 0
  br i1 %cmp31, label %for.body32, label %for.end39

for.body32:                                       ; preds = %for.cond30
  %35 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds [64 x [64 x float]], [64 x [64 x float]]* %s3, i32 0, i32 %35
  %36 = load i32, i32* %j, align 4
  %arrayidx34 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx33, i32 0, i32 %36
  %37 = load float, float* %arrayidx34, align 4
  %cmp35 = fcmp ogt float %37, 0.000000e+00
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %for.body32
  br label %for.end39

if.end37:                                         ; preds = %for.body32
  br label %for.inc38

for.inc38:                                        ; preds = %if.end37
  %38 = load i32, i32* %j, align 4
  %dec = add nsw i32 %38, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond30

for.end39:                                        ; preds = %if.then36, %for.cond30
  %39 = load i32, i32* %j, align 4
  %40 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %41 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr inbounds [2 x i32], [2 x i32]* %40, i32 %41
  %arrayidx41 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx40, i32 0, i32 1
  store i32 %39, i32* %arrayidx41, align 4
  %42 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %43 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [2 x i32], [2 x i32]* %42, i32 %43
  %arrayidx43 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx42, i32 0, i32 1
  %44 = load i32, i32* %arrayidx43, align 4
  %45 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %46 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [2 x i32], [2 x i32]* %45, i32 %46
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx44, i32 0, i32 0
  %47 = load i32, i32* %arrayidx45, align 4
  %sub46 = sub nsw i32 %44, %47
  %add = add nsw i32 %sub46, 1
  %48 = load i32, i32* %numberOfNoneZero, align 4
  %add47 = add nsw i32 %48, %add
  store i32 %add47, i32* %numberOfNoneZero, align 4
  br label %for.inc48

for.inc48:                                        ; preds = %for.end39
  %49 = load i32, i32* %i, align 4
  %inc49 = add nsw i32 %49, 1
  store i32 %inc49, i32* %i, align 4
  br label %for.cond15

for.end50:                                        ; preds = %for.cond15
  %50 = load i32, i32* %numberOfNoneZero, align 4
  %call51 = call i8* @calloc(i32 %50, i32 4)
  %51 = bitcast i8* %call51 to float*
  %52 = load float**, float*** %p.addr, align 4
  store float* %51, float** %52, align 4
  %53 = load float**, float*** %p.addr, align 4
  %54 = load float*, float** %53, align 4
  %tobool = icmp ne float* %54, null
  br i1 %tobool, label %if.end53, label %if.then52

if.then52:                                        ; preds = %for.end50
  store i32 -1, i32* %retval, align 4
  br label %return

if.end53:                                         ; preds = %for.end50
  store i32 0, i32* %k, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc71, %if.end53
  %55 = load i32, i32* %i, align 4
  %56 = load i32, i32* %npart.addr, align 4
  %cmp55 = icmp slt i32 %55, %56
  br i1 %cmp55, label %for.body56, label %for.end73

for.body56:                                       ; preds = %for.cond54
  %57 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %58 = load i32, i32* %i, align 4
  %arrayidx57 = getelementptr inbounds [2 x i32], [2 x i32]* %57, i32 %58
  %arrayidx58 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx57, i32 0, i32 0
  %59 = load i32, i32* %arrayidx58, align 4
  store i32 %59, i32* %j, align 4
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc68, %for.body56
  %60 = load i32, i32* %j, align 4
  %61 = load [2 x i32]*, [2 x i32]** %s3ind.addr, align 4
  %62 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %61, i32 %62
  %arrayidx61 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx60, i32 0, i32 1
  %63 = load i32, i32* %arrayidx61, align 4
  %cmp62 = icmp sle i32 %60, %63
  br i1 %cmp62, label %for.body63, label %for.end70

for.body63:                                       ; preds = %for.cond59
  %64 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr inbounds [64 x [64 x float]], [64 x [64 x float]]* %s3, i32 0, i32 %64
  %65 = load i32, i32* %j, align 4
  %arrayidx65 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx64, i32 0, i32 %65
  %66 = load float, float* %arrayidx65, align 4
  %67 = load float**, float*** %p.addr, align 4
  %68 = load float*, float** %67, align 4
  %69 = load i32, i32* %k, align 4
  %inc66 = add nsw i32 %69, 1
  store i32 %inc66, i32* %k, align 4
  %arrayidx67 = getelementptr inbounds float, float* %68, i32 %69
  store float %66, float* %arrayidx67, align 4
  br label %for.inc68

for.inc68:                                        ; preds = %for.body63
  %70 = load i32, i32* %j, align 4
  %inc69 = add nsw i32 %70, 1
  store i32 %inc69, i32* %j, align 4
  br label %for.cond59

for.end70:                                        ; preds = %for.cond59
  br label %for.inc71

for.inc71:                                        ; preds = %for.end70
  %71 = load i32, i32* %i, align 4
  %inc72 = add nsw i32 %71, 1
  store i32 %inc72, i32* %i, align 4
  br label %for.cond54

for.end73:                                        ; preds = %for.cond54
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end73, %if.then52
  %72 = load i32, i32* %retval, align 4
  ret i32 %72
}

declare float @ATHformula(%struct.SessionConfig_t*, float) #3

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log.f64(double) #4

; Function Attrs: noinline nounwind optnone
define internal void @init_mask_add_max_values() #0 {
entry:
  ret void
}

declare void @init_fft(%struct.lame_internal_flags*) #3

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.exp.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #4

declare void @fft_long(%struct.lame_internal_flags*, float*, i32, float**) #3

; Function Attrs: noinline nounwind optnone
define internal float @psycho_loudness_approx(float* %energy, float* %eql_w) #0 {
entry:
  %energy.addr = alloca float*, align 4
  %eql_w.addr = alloca float*, align 4
  %i = alloca i32, align 4
  %loudness_power = alloca float, align 4
  store float* %energy, float** %energy.addr, align 4
  store float* %eql_w, float** %eql_w.addr, align 4
  store float 0.000000e+00, float* %loudness_power, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 512
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load float*, float** %energy.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %4 = load float*, float** %eql_w.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx1, align 4
  %mul = fmul float %3, %6
  %7 = load float, float* %loudness_power, align 4
  %add = fadd float %7, %mul
  store float %add, float* %loudness_power, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load float, float* %loudness_power, align 4
  %conv = fpext float %9 to double
  %mul2 = fmul double %conv, 0x3DA3BC67458A22D1
  %conv3 = fptrunc double %mul2 to float
  store float %conv3, float* %loudness_power, align 4
  %10 = load float, float* %loudness_power, align 4
  ret float %10
}

; Function Attrs: noinline nounwind optnone
define internal void @calc_energy(%struct.PsyConst_CB2SB_t* %l, float* %fftenergy, float* %eb, float* %max, float* %avg) #0 {
entry:
  %l.addr = alloca %struct.PsyConst_CB2SB_t*, align 4
  %fftenergy.addr = alloca float*, align 4
  %eb.addr = alloca float*, align 4
  %max.addr = alloca float*, align 4
  %avg.addr = alloca float*, align 4
  %b = alloca i32, align 4
  %j = alloca i32, align 4
  %ebb = alloca float, align 4
  %m = alloca float, align 4
  %i = alloca i32, align 4
  %el = alloca float, align 4
  store %struct.PsyConst_CB2SB_t* %l, %struct.PsyConst_CB2SB_t** %l.addr, align 4
  store float* %fftenergy, float** %fftenergy.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %max, float** %max.addr, align 4
  store float* %avg, float** %avg.addr, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %entry
  %0 = load i32, i32* %b, align 4
  %1 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %l.addr, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %1, i32 0, i32 11
  %2 = load i32, i32* %npart, align 4
  %cmp = icmp slt i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %ebb, align 4
  store float 0.000000e+00, float* %m, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %i, align 4
  %4 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %l.addr, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %4, i32 0, i32 8
  %5 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  %cmp2 = icmp slt i32 %3, %6
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %7 = load float*, float** %fftenergy.addr, align 4
  %8 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx4, align 4
  store float %9, float* %el, align 4
  %10 = load float, float* %el, align 4
  %11 = load float, float* %ebb, align 4
  %add = fadd float %11, %10
  store float %add, float* %ebb, align 4
  %12 = load float, float* %m, align 4
  %13 = load float, float* %el, align 4
  %cmp5 = fcmp olt float %12, %13
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body3
  %14 = load float, float* %el, align 4
  store float %14, float* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body3
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  %inc6 = add nsw i32 %16, 1
  store i32 %inc6, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %17 = load float, float* %ebb, align 4
  %18 = load float*, float** %eb.addr, align 4
  %19 = load i32, i32* %b, align 4
  %arrayidx7 = getelementptr inbounds float, float* %18, i32 %19
  store float %17, float* %arrayidx7, align 4
  %20 = load float, float* %m, align 4
  %21 = load float*, float** %max.addr, align 4
  %22 = load i32, i32* %b, align 4
  %arrayidx8 = getelementptr inbounds float, float* %21, i32 %22
  store float %20, float* %arrayidx8, align 4
  %23 = load float, float* %ebb, align 4
  %24 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %l.addr, align 4
  %rnumlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %24, i32 0, i32 2
  %25 = load i32, i32* %b, align 4
  %arrayidx9 = getelementptr inbounds [64 x float], [64 x float]* %rnumlines, i32 0, i32 %25
  %26 = load float, float* %arrayidx9, align 4
  %mul = fmul float %23, %26
  %27 = load float*, float** %avg.addr, align 4
  %28 = load i32, i32* %b, align 4
  %arrayidx10 = getelementptr inbounds float, float* %27, i32 %28
  store float %mul, float* %arrayidx10, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %29 = load i32, i32* %b, align 4
  %inc12 = add nsw i32 %29, 1
  store i32 %inc12, i32* %b, align 4
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @calc_mask_index_l(%struct.lame_internal_flags* %gfc, float* %max, float* %avg, i8* %mask_idx) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %max.addr = alloca float*, align 4
  %avg.addr = alloca float*, align 4
  %mask_idx.addr = alloca i8*, align 4
  %gdl = alloca %struct.PsyConst_CB2SB_t*, align 4
  %m = alloca float, align 4
  %a = alloca float, align 4
  %b = alloca i32, align 4
  %k = alloca i32, align 4
  %last_tab_entry = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %max, float** %max.addr, align 4
  store float* %avg, float** %avg.addr, align 4
  store i8* %mask_idx, i8** %mask_idx.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 22
  %1 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %l = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %1, i32 0, i32 2
  store %struct.PsyConst_CB2SB_t* %l, %struct.PsyConst_CB2SB_t** %gdl, align 4
  store i32 8, i32* %last_tab_entry, align 4
  store i32 0, i32* %b, align 4
  %2 = load float*, float** %avg.addr, align 4
  %3 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float*, float** %avg.addr, align 4
  %6 = load i32, i32* %b, align 4
  %add = add nsw i32 %6, 1
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 %add
  %7 = load float, float* %arrayidx1, align 4
  %add2 = fadd float %4, %7
  store float %add2, float* %a, align 4
  %8 = load float, float* %a, align 4
  %cmp = fcmp ogt float %8, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %9 = load float*, float** %max.addr, align 4
  %10 = load i32, i32* %b, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx3, align 4
  store float %11, float* %m, align 4
  %12 = load float, float* %m, align 4
  %13 = load float*, float** %max.addr, align 4
  %14 = load i32, i32* %b, align 4
  %add4 = add nsw i32 %14, 1
  %arrayidx5 = getelementptr inbounds float, float* %13, i32 %add4
  %15 = load float, float* %arrayidx5, align 4
  %cmp6 = fcmp olt float %12, %15
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %16 = load float*, float** %max.addr, align 4
  %17 = load i32, i32* %b, align 4
  %add8 = add nsw i32 %17, 1
  %arrayidx9 = getelementptr inbounds float, float* %16, i32 %add8
  %18 = load float, float* %arrayidx9, align 4
  store float %18, float* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %19 = load float, float* %m, align 4
  %mul = fmul float %19, 2.000000e+00
  %20 = load float, float* %a, align 4
  %sub = fsub float %mul, %20
  %mul10 = fmul float 2.000000e+01, %sub
  %21 = load float, float* %a, align 4
  %22 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %22, i32 0, i32 8
  %23 = load i32, i32* %b, align 4
  %arrayidx11 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx11, align 4
  %25 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines12 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %25, i32 0, i32 8
  %26 = load i32, i32* %b, align 4
  %add13 = add nsw i32 %26, 1
  %arrayidx14 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines12, i32 0, i32 %add13
  %27 = load i32, i32* %arrayidx14, align 4
  %add15 = add nsw i32 %24, %27
  %sub16 = sub nsw i32 %add15, 1
  %conv = sitofp i32 %sub16 to float
  %mul17 = fmul float %21, %conv
  %div = fdiv float %mul10, %mul17
  store float %div, float* %a, align 4
  %28 = load float, float* %a, align 4
  %conv18 = fptosi float %28 to i32
  store i32 %conv18, i32* %k, align 4
  %29 = load i32, i32* %k, align 4
  %cmp19 = icmp sgt i32 %29, 8
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end
  store i32 8, i32* %k, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %if.end
  %30 = load i32, i32* %k, align 4
  %conv23 = trunc i32 %30 to i8
  %31 = load i8*, i8** %mask_idx.addr, align 4
  %32 = load i32, i32* %b, align 4
  %arrayidx24 = getelementptr inbounds i8, i8* %31, i32 %32
  store i8 %conv23, i8* %arrayidx24, align 1
  br label %if.end26

if.else:                                          ; preds = %entry
  %33 = load i8*, i8** %mask_idx.addr, align 4
  %34 = load i32, i32* %b, align 4
  %arrayidx25 = getelementptr inbounds i8, i8* %33, i32 %34
  store i8 0, i8* %arrayidx25, align 1
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.end22
  store i32 1, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end26
  %35 = load i32, i32* %b, align 4
  %36 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %36, i32 0, i32 11
  %37 = load i32, i32* %npart, align 4
  %sub27 = sub nsw i32 %37, 1
  %cmp28 = icmp slt i32 %35, %sub27
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %38 = load float*, float** %avg.addr, align 4
  %39 = load i32, i32* %b, align 4
  %sub30 = sub nsw i32 %39, 1
  %arrayidx31 = getelementptr inbounds float, float* %38, i32 %sub30
  %40 = load float, float* %arrayidx31, align 4
  %41 = load float*, float** %avg.addr, align 4
  %42 = load i32, i32* %b, align 4
  %arrayidx32 = getelementptr inbounds float, float* %41, i32 %42
  %43 = load float, float* %arrayidx32, align 4
  %add33 = fadd float %40, %43
  %44 = load float*, float** %avg.addr, align 4
  %45 = load i32, i32* %b, align 4
  %add34 = add nsw i32 %45, 1
  %arrayidx35 = getelementptr inbounds float, float* %44, i32 %add34
  %46 = load float, float* %arrayidx35, align 4
  %add36 = fadd float %add33, %46
  store float %add36, float* %a, align 4
  %47 = load float, float* %a, align 4
  %cmp37 = fcmp ogt float %47, 0.000000e+00
  br i1 %cmp37, label %if.then39, label %if.else80

if.then39:                                        ; preds = %for.body
  %48 = load float*, float** %max.addr, align 4
  %49 = load i32, i32* %b, align 4
  %sub40 = sub nsw i32 %49, 1
  %arrayidx41 = getelementptr inbounds float, float* %48, i32 %sub40
  %50 = load float, float* %arrayidx41, align 4
  store float %50, float* %m, align 4
  %51 = load float, float* %m, align 4
  %52 = load float*, float** %max.addr, align 4
  %53 = load i32, i32* %b, align 4
  %arrayidx42 = getelementptr inbounds float, float* %52, i32 %53
  %54 = load float, float* %arrayidx42, align 4
  %cmp43 = fcmp olt float %51, %54
  br i1 %cmp43, label %if.then45, label %if.end47

if.then45:                                        ; preds = %if.then39
  %55 = load float*, float** %max.addr, align 4
  %56 = load i32, i32* %b, align 4
  %arrayidx46 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx46, align 4
  store float %57, float* %m, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %if.then39
  %58 = load float, float* %m, align 4
  %59 = load float*, float** %max.addr, align 4
  %60 = load i32, i32* %b, align 4
  %add48 = add nsw i32 %60, 1
  %arrayidx49 = getelementptr inbounds float, float* %59, i32 %add48
  %61 = load float, float* %arrayidx49, align 4
  %cmp50 = fcmp olt float %58, %61
  br i1 %cmp50, label %if.then52, label %if.end55

if.then52:                                        ; preds = %if.end47
  %62 = load float*, float** %max.addr, align 4
  %63 = load i32, i32* %b, align 4
  %add53 = add nsw i32 %63, 1
  %arrayidx54 = getelementptr inbounds float, float* %62, i32 %add53
  %64 = load float, float* %arrayidx54, align 4
  store float %64, float* %m, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then52, %if.end47
  %65 = load float, float* %m, align 4
  %mul56 = fmul float %65, 3.000000e+00
  %66 = load float, float* %a, align 4
  %sub57 = fsub float %mul56, %66
  %mul58 = fmul float 2.000000e+01, %sub57
  %67 = load float, float* %a, align 4
  %68 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines59 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %68, i32 0, i32 8
  %69 = load i32, i32* %b, align 4
  %sub60 = sub nsw i32 %69, 1
  %arrayidx61 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines59, i32 0, i32 %sub60
  %70 = load i32, i32* %arrayidx61, align 4
  %71 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines62 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %71, i32 0, i32 8
  %72 = load i32, i32* %b, align 4
  %arrayidx63 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines62, i32 0, i32 %72
  %73 = load i32, i32* %arrayidx63, align 4
  %add64 = add nsw i32 %70, %73
  %74 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines65 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %74, i32 0, i32 8
  %75 = load i32, i32* %b, align 4
  %add66 = add nsw i32 %75, 1
  %arrayidx67 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines65, i32 0, i32 %add66
  %76 = load i32, i32* %arrayidx67, align 4
  %add68 = add nsw i32 %add64, %76
  %sub69 = sub nsw i32 %add68, 1
  %conv70 = sitofp i32 %sub69 to float
  %mul71 = fmul float %67, %conv70
  %div72 = fdiv float %mul58, %mul71
  store float %div72, float* %a, align 4
  %77 = load float, float* %a, align 4
  %conv73 = fptosi float %77 to i32
  store i32 %conv73, i32* %k, align 4
  %78 = load i32, i32* %k, align 4
  %cmp74 = icmp sgt i32 %78, 8
  br i1 %cmp74, label %if.then76, label %if.end77

if.then76:                                        ; preds = %if.end55
  store i32 8, i32* %k, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then76, %if.end55
  %79 = load i32, i32* %k, align 4
  %conv78 = trunc i32 %79 to i8
  %80 = load i8*, i8** %mask_idx.addr, align 4
  %81 = load i32, i32* %b, align 4
  %arrayidx79 = getelementptr inbounds i8, i8* %80, i32 %81
  store i8 %conv78, i8* %arrayidx79, align 1
  br label %if.end82

if.else80:                                        ; preds = %for.body
  %82 = load i8*, i8** %mask_idx.addr, align 4
  %83 = load i32, i32* %b, align 4
  %arrayidx81 = getelementptr inbounds i8, i8* %82, i32 %83
  store i8 0, i8* %arrayidx81, align 1
  br label %if.end82

if.end82:                                         ; preds = %if.else80, %if.end77
  br label %for.inc

for.inc:                                          ; preds = %if.end82
  %84 = load i32, i32* %b, align 4
  %inc = add nsw i32 %84, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %85 = load float*, float** %avg.addr, align 4
  %86 = load i32, i32* %b, align 4
  %sub83 = sub nsw i32 %86, 1
  %arrayidx84 = getelementptr inbounds float, float* %85, i32 %sub83
  %87 = load float, float* %arrayidx84, align 4
  %88 = load float*, float** %avg.addr, align 4
  %89 = load i32, i32* %b, align 4
  %arrayidx85 = getelementptr inbounds float, float* %88, i32 %89
  %90 = load float, float* %arrayidx85, align 4
  %add86 = fadd float %87, %90
  store float %add86, float* %a, align 4
  %91 = load float, float* %a, align 4
  %cmp87 = fcmp ogt float %91, 0.000000e+00
  br i1 %cmp87, label %if.then89, label %if.else118

if.then89:                                        ; preds = %for.end
  %92 = load float*, float** %max.addr, align 4
  %93 = load i32, i32* %b, align 4
  %sub90 = sub nsw i32 %93, 1
  %arrayidx91 = getelementptr inbounds float, float* %92, i32 %sub90
  %94 = load float, float* %arrayidx91, align 4
  store float %94, float* %m, align 4
  %95 = load float, float* %m, align 4
  %96 = load float*, float** %max.addr, align 4
  %97 = load i32, i32* %b, align 4
  %arrayidx92 = getelementptr inbounds float, float* %96, i32 %97
  %98 = load float, float* %arrayidx92, align 4
  %cmp93 = fcmp olt float %95, %98
  br i1 %cmp93, label %if.then95, label %if.end97

if.then95:                                        ; preds = %if.then89
  %99 = load float*, float** %max.addr, align 4
  %100 = load i32, i32* %b, align 4
  %arrayidx96 = getelementptr inbounds float, float* %99, i32 %100
  %101 = load float, float* %arrayidx96, align 4
  store float %101, float* %m, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then95, %if.then89
  %102 = load float, float* %m, align 4
  %mul98 = fmul float %102, 2.000000e+00
  %103 = load float, float* %a, align 4
  %sub99 = fsub float %mul98, %103
  %mul100 = fmul float 2.000000e+01, %sub99
  %104 = load float, float* %a, align 4
  %105 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines101 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %105, i32 0, i32 8
  %106 = load i32, i32* %b, align 4
  %sub102 = sub nsw i32 %106, 1
  %arrayidx103 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines101, i32 0, i32 %sub102
  %107 = load i32, i32* %arrayidx103, align 4
  %108 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gdl, align 4
  %numlines104 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %108, i32 0, i32 8
  %109 = load i32, i32* %b, align 4
  %arrayidx105 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines104, i32 0, i32 %109
  %110 = load i32, i32* %arrayidx105, align 4
  %add106 = add nsw i32 %107, %110
  %sub107 = sub nsw i32 %add106, 1
  %conv108 = sitofp i32 %sub107 to float
  %mul109 = fmul float %104, %conv108
  %div110 = fdiv float %mul100, %mul109
  store float %div110, float* %a, align 4
  %111 = load float, float* %a, align 4
  %conv111 = fptosi float %111 to i32
  store i32 %conv111, i32* %k, align 4
  %112 = load i32, i32* %k, align 4
  %cmp112 = icmp sgt i32 %112, 8
  br i1 %cmp112, label %if.then114, label %if.end115

if.then114:                                       ; preds = %if.end97
  store i32 8, i32* %k, align 4
  br label %if.end115

if.end115:                                        ; preds = %if.then114, %if.end97
  %113 = load i32, i32* %k, align 4
  %conv116 = trunc i32 %113 to i8
  %114 = load i8*, i8** %mask_idx.addr, align 4
  %115 = load i32, i32* %b, align 4
  %arrayidx117 = getelementptr inbounds i8, i8* %114, i32 %115
  store i8 %conv116, i8* %arrayidx117, align 1
  br label %if.end120

if.else118:                                       ; preds = %for.end
  %116 = load i8*, i8** %mask_idx.addr, align 4
  %117 = load i32, i32* %b, align 4
  %arrayidx119 = getelementptr inbounds i8, i8* %116, i32 %117
  store i8 0, i8* %arrayidx119, align 1
  br label %if.end120

if.end120:                                        ; preds = %if.else118, %if.end115
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @mask_add_delta(i32 %i) #0 {
entry:
  %i.addr = alloca i32, align 4
  store i32 %i, i32* %i.addr, align 4
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [9 x i32], [9 x i32]* @tab_mask_add_delta, i32 0, i32 %0
  %1 = load i32, i32* %arrayidx, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define internal float @vbrpsy_mask_add(float %m1, float %m2, i32 %b, i32 %delta) #0 {
entry:
  %retval = alloca float, align 4
  %m1.addr = alloca float, align 4
  %m2.addr = alloca float, align 4
  %b.addr = alloca i32, align 4
  %delta.addr = alloca i32, align 4
  %ratio = alloca float, align 4
  %i = alloca i32, align 4
  store float %m1, float* %m1.addr, align 4
  store float %m2, float* %m2.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  store i32 %delta, i32* %delta.addr, align 4
  %0 = load float, float* %m1.addr, align 4
  %cmp = fcmp olt float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %m1.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %m2.addr, align 4
  %cmp1 = fcmp olt float %1, 0.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 0.000000e+00, float* %m2.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %m1.addr, align 4
  %cmp4 = fcmp ole float %2, 0.000000e+00
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  %3 = load float, float* %m2.addr, align 4
  store float %3, float* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %4 = load float, float* %m2.addr, align 4
  %cmp7 = fcmp ole float %4, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  %5 = load float, float* %m1.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %6 = load float, float* %m2.addr, align 4
  %7 = load float, float* %m1.addr, align 4
  %cmp10 = fcmp ogt float %6, %7
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end9
  %8 = load float, float* %m2.addr, align 4
  %9 = load float, float* %m1.addr, align 4
  %div = fdiv float %8, %9
  store float %div, float* %ratio, align 4
  br label %if.end13

if.else:                                          ; preds = %if.end9
  %10 = load float, float* %m1.addr, align 4
  %11 = load float, float* %m2.addr, align 4
  %div12 = fdiv float %10, %11
  store float %div12, float* %ratio, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then11
  %12 = load i32, i32* %b.addr, align 4
  %call = call i32 @abs(i32 %12) #6
  %13 = load i32, i32* %delta.addr, align 4
  %cmp14 = icmp sle i32 %call, %13
  br i1 %cmp14, label %if.then15, label %if.end23

if.then15:                                        ; preds = %if.end13
  %14 = load float, float* %ratio, align 4
  %cmp16 = fcmp oge float %14, 0x400D36C420000000
  br i1 %cmp16, label %if.then17, label %if.else18

if.then17:                                        ; preds = %if.then15
  %15 = load float, float* %m1.addr, align 4
  %16 = load float, float* %m2.addr, align 4
  %add = fadd float %15, %16
  store float %add, float* %retval, align 4
  br label %return

if.else18:                                        ; preds = %if.then15
  %17 = load float, float* %ratio, align 4
  %call19 = call float @fast_log2(float %17)
  %conv = fpext float %call19 to double
  %mul = fmul double %conv, 0x40134413509F79FE
  %conv20 = fptosi double %mul to i32
  store i32 %conv20, i32* %i, align 4
  %18 = load float, float* %m1.addr, align 4
  %19 = load float, float* %m2.addr, align 4
  %add21 = fadd float %18, %19
  %20 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [10 x float], [10 x float]* @vbrpsy_mask_add.table2, i32 0, i32 %20
  %21 = load float, float* %arrayidx, align 4
  %mul22 = fmul float %add21, %21
  store float %mul22, float* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.end13
  %22 = load float, float* %ratio, align 4
  %cmp24 = fcmp olt float %22, 0x403F9F6E40000000
  br i1 %cmp24, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.end23
  %23 = load float, float* %m1.addr, align 4
  %24 = load float, float* %m2.addr, align 4
  %add27 = fadd float %23, %24
  store float %add27, float* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.end23
  %25 = load float, float* %m1.addr, align 4
  %26 = load float, float* %m2.addr, align 4
  %cmp29 = fcmp olt float %25, %26
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end28
  %27 = load float, float* %m2.addr, align 4
  store float %27, float* %m1.addr, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end28
  %28 = load float, float* %m1.addr, align 4
  store float %28, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end32, %if.then26, %if.else18, %if.then17, %if.then8, %if.then5
  %29 = load float, float* %retval, align 4
  ret float %29
}

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #5

declare float @fast_log2(float) #3

; Function Attrs: noinline nounwind optnone
define internal void @convert_partition2scalefac(%struct.PsyConst_CB2SB_t* %gd, float* %eb, float* %thr, float* %enn_out, float* %thm_out) #0 {
entry:
  %gd.addr = alloca %struct.PsyConst_CB2SB_t*, align 4
  %eb.addr = alloca float*, align 4
  %thr.addr = alloca float*, align 4
  %enn_out.addr = alloca float*, align 4
  %thm_out.addr = alloca float*, align 4
  %enn = alloca float, align 4
  %thmm = alloca float, align 4
  %sb = alloca i32, align 4
  %b = alloca i32, align 4
  %n = alloca i32, align 4
  %bo_sb = alloca i32, align 4
  %npart = alloca i32, align 4
  %b_lim = alloca i32, align 4
  %w_curr = alloca float, align 4
  %w_next = alloca float, align 4
  store %struct.PsyConst_CB2SB_t* %gd, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  store float* %eb, float** %eb.addr, align 4
  store float* %thr, float** %thr.addr, align 4
  store float* %enn_out, float** %enn_out.addr, align 4
  store float* %thm_out, float** %thm_out.addr, align 4
  %0 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %n_sb = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %0, i32 0, i32 12
  %1 = load i32, i32* %n_sb, align 4
  store i32 %1, i32* %n, align 4
  store float 0.000000e+00, float* %thmm, align 4
  store float 0.000000e+00, float* %enn, align 4
  store i32 0, i32* %b, align 4
  store i32 0, i32* %sb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %sb, align 4
  %3 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %bo = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %4, i32 0, i32 10
  %5 = load i32, i32* %sb, align 4
  %arrayidx = getelementptr inbounds [22 x i32], [22 x i32]* %bo, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  store i32 %6, i32* %bo_sb, align 4
  %7 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %npart1 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %7, i32 0, i32 11
  %8 = load i32, i32* %npart1, align 4
  store i32 %8, i32* %npart, align 4
  %9 = load i32, i32* %bo_sb, align 4
  %10 = load i32, i32* %npart, align 4
  %cmp2 = icmp slt i32 %9, %10
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %bo_sb, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %12 = load i32, i32* %npart, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %11, %cond.true ], [ %12, %cond.false ]
  store i32 %cond, i32* %b_lim, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %13 = load i32, i32* %b, align 4
  %14 = load i32, i32* %b_lim, align 4
  %cmp3 = icmp slt i32 %13, %14
  br i1 %cmp3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %15 = load float*, float** %eb.addr, align 4
  %16 = load i32, i32* %b, align 4
  %arrayidx4 = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx4, align 4
  %18 = load float, float* %enn, align 4
  %add = fadd float %18, %17
  store float %add, float* %enn, align 4
  %19 = load float*, float** %thr.addr, align 4
  %20 = load i32, i32* %b, align 4
  %arrayidx5 = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx5, align 4
  %22 = load float, float* %thmm, align 4
  %add6 = fadd float %22, %21
  store float %add6, float* %thmm, align 4
  %23 = load i32, i32* %b, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %b, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = load i32, i32* %b, align 4
  %25 = load i32, i32* %npart, align 4
  %cmp7 = icmp sge i32 %24, %25
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %26 = load float, float* %enn, align 4
  %27 = load float*, float** %enn_out.addr, align 4
  %28 = load i32, i32* %sb, align 4
  %arrayidx8 = getelementptr inbounds float, float* %27, i32 %28
  store float %26, float* %arrayidx8, align 4
  %29 = load float, float* %thmm, align 4
  %30 = load float*, float** %thm_out.addr, align 4
  %31 = load i32, i32* %sb, align 4
  %arrayidx9 = getelementptr inbounds float, float* %30, i32 %31
  store float %29, float* %arrayidx9, align 4
  %32 = load i32, i32* %sb, align 4
  %inc10 = add nsw i32 %32, 1
  store i32 %inc10, i32* %sb, align 4
  br label %for.end

if.end:                                           ; preds = %while.end
  %33 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gd.addr, align 4
  %bo_weight = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %33, i32 0, i32 5
  %34 = load i32, i32* %sb, align 4
  %arrayidx11 = getelementptr inbounds [22 x float], [22 x float]* %bo_weight, i32 0, i32 %34
  %35 = load float, float* %arrayidx11, align 4
  store float %35, float* %w_curr, align 4
  %36 = load float, float* %w_curr, align 4
  %sub = fsub float 1.000000e+00, %36
  store float %sub, float* %w_next, align 4
  %37 = load float, float* %w_curr, align 4
  %38 = load float*, float** %eb.addr, align 4
  %39 = load i32, i32* %b, align 4
  %arrayidx12 = getelementptr inbounds float, float* %38, i32 %39
  %40 = load float, float* %arrayidx12, align 4
  %mul = fmul float %37, %40
  %41 = load float, float* %enn, align 4
  %add13 = fadd float %41, %mul
  store float %add13, float* %enn, align 4
  %42 = load float, float* %w_curr, align 4
  %43 = load float*, float** %thr.addr, align 4
  %44 = load i32, i32* %b, align 4
  %arrayidx14 = getelementptr inbounds float, float* %43, i32 %44
  %45 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %42, %45
  %46 = load float, float* %thmm, align 4
  %add16 = fadd float %46, %mul15
  store float %add16, float* %thmm, align 4
  %47 = load float, float* %enn, align 4
  %48 = load float*, float** %enn_out.addr, align 4
  %49 = load i32, i32* %sb, align 4
  %arrayidx17 = getelementptr inbounds float, float* %48, i32 %49
  store float %47, float* %arrayidx17, align 4
  %50 = load float, float* %thmm, align 4
  %51 = load float*, float** %thm_out.addr, align 4
  %52 = load i32, i32* %sb, align 4
  %arrayidx18 = getelementptr inbounds float, float* %51, i32 %52
  store float %50, float* %arrayidx18, align 4
  %53 = load float, float* %w_next, align 4
  %54 = load float*, float** %eb.addr, align 4
  %55 = load i32, i32* %b, align 4
  %arrayidx19 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx19, align 4
  %mul20 = fmul float %53, %56
  store float %mul20, float* %enn, align 4
  %57 = load float, float* %w_next, align 4
  %58 = load float*, float** %thr.addr, align 4
  %59 = load i32, i32* %b, align 4
  %arrayidx21 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx21, align 4
  %mul22 = fmul float %57, %60
  store float %mul22, float* %thmm, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %61 = load i32, i32* %b, align 4
  %inc23 = add nsw i32 %61, 1
  store i32 %inc23, i32* %b, align 4
  %62 = load i32, i32* %sb, align 4
  %inc24 = add nsw i32 %62, 1
  store i32 %inc24, i32* %sb, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc30, %for.end
  %63 = load i32, i32* %sb, align 4
  %64 = load i32, i32* %n, align 4
  %cmp26 = icmp slt i32 %63, %64
  br i1 %cmp26, label %for.body27, label %for.end32

for.body27:                                       ; preds = %for.cond25
  %65 = load float*, float** %enn_out.addr, align 4
  %66 = load i32, i32* %sb, align 4
  %arrayidx28 = getelementptr inbounds float, float* %65, i32 %66
  store float 0.000000e+00, float* %arrayidx28, align 4
  %67 = load float*, float** %thm_out.addr, align 4
  %68 = load i32, i32* %sb, align 4
  %arrayidx29 = getelementptr inbounds float, float* %67, i32 %68
  store float 0.000000e+00, float* %arrayidx29, align 4
  br label %for.inc30

for.inc30:                                        ; preds = %for.body27
  %69 = load i32, i32* %sb, align 4
  %inc31 = add nsw i32 %69, 1
  store i32 %inc31, i32* %sb, align 4
  br label %for.cond25

for.end32:                                        ; preds = %for.cond25
  ret void
}

declare void @fft_short(%struct.lame_internal_flags*, [256 x float]*, i32, float**) #3

; Function Attrs: noinline nounwind optnone
define internal void @vbrpsy_calc_mask_index_s(%struct.lame_internal_flags* %gfc, float* %max, float* %avg, i8* %mask_idx) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %max.addr = alloca float*, align 4
  %avg.addr = alloca float*, align 4
  %mask_idx.addr = alloca i8*, align 4
  %gds = alloca %struct.PsyConst_CB2SB_t*, align 4
  %m = alloca float, align 4
  %a = alloca float, align 4
  %b = alloca i32, align 4
  %k = alloca i32, align 4
  %last_tab_entry = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %max, float** %max.addr, align 4
  store float* %avg, float** %avg.addr, align 4
  store i8* %mask_idx, i8** %mask_idx.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 22
  %1 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %1, i32 0, i32 3
  store %struct.PsyConst_CB2SB_t* %s, %struct.PsyConst_CB2SB_t** %gds, align 4
  store i32 8, i32* %last_tab_entry, align 4
  store i32 0, i32* %b, align 4
  %2 = load float*, float** %avg.addr, align 4
  %3 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float*, float** %avg.addr, align 4
  %6 = load i32, i32* %b, align 4
  %add = add nsw i32 %6, 1
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 %add
  %7 = load float, float* %arrayidx1, align 4
  %add2 = fadd float %4, %7
  store float %add2, float* %a, align 4
  %8 = load float, float* %a, align 4
  %cmp = fcmp ogt float %8, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %9 = load float*, float** %max.addr, align 4
  %10 = load i32, i32* %b, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx3, align 4
  store float %11, float* %m, align 4
  %12 = load float, float* %m, align 4
  %13 = load float*, float** %max.addr, align 4
  %14 = load i32, i32* %b, align 4
  %add4 = add nsw i32 %14, 1
  %arrayidx5 = getelementptr inbounds float, float* %13, i32 %add4
  %15 = load float, float* %arrayidx5, align 4
  %cmp6 = fcmp olt float %12, %15
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %16 = load float*, float** %max.addr, align 4
  %17 = load i32, i32* %b, align 4
  %add8 = add nsw i32 %17, 1
  %arrayidx9 = getelementptr inbounds float, float* %16, i32 %add8
  %18 = load float, float* %arrayidx9, align 4
  store float %18, float* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %19 = load float, float* %m, align 4
  %mul = fmul float %19, 2.000000e+00
  %20 = load float, float* %a, align 4
  %sub = fsub float %mul, %20
  %mul10 = fmul float 2.000000e+01, %sub
  %21 = load float, float* %a, align 4
  %22 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %22, i32 0, i32 8
  %23 = load i32, i32* %b, align 4
  %arrayidx11 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx11, align 4
  %25 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines12 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %25, i32 0, i32 8
  %26 = load i32, i32* %b, align 4
  %add13 = add nsw i32 %26, 1
  %arrayidx14 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines12, i32 0, i32 %add13
  %27 = load i32, i32* %arrayidx14, align 4
  %add15 = add nsw i32 %24, %27
  %sub16 = sub nsw i32 %add15, 1
  %conv = sitofp i32 %sub16 to float
  %mul17 = fmul float %21, %conv
  %div = fdiv float %mul10, %mul17
  store float %div, float* %a, align 4
  %28 = load float, float* %a, align 4
  %conv18 = fptosi float %28 to i32
  store i32 %conv18, i32* %k, align 4
  %29 = load i32, i32* %k, align 4
  %cmp19 = icmp sgt i32 %29, 8
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end
  store i32 8, i32* %k, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %if.end
  %30 = load i32, i32* %k, align 4
  %conv23 = trunc i32 %30 to i8
  %31 = load i8*, i8** %mask_idx.addr, align 4
  %32 = load i32, i32* %b, align 4
  %arrayidx24 = getelementptr inbounds i8, i8* %31, i32 %32
  store i8 %conv23, i8* %arrayidx24, align 1
  br label %if.end26

if.else:                                          ; preds = %entry
  %33 = load i8*, i8** %mask_idx.addr, align 4
  %34 = load i32, i32* %b, align 4
  %arrayidx25 = getelementptr inbounds i8, i8* %33, i32 %34
  store i8 0, i8* %arrayidx25, align 1
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.end22
  store i32 1, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end26
  %35 = load i32, i32* %b, align 4
  %36 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %npart = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %36, i32 0, i32 11
  %37 = load i32, i32* %npart, align 4
  %sub27 = sub nsw i32 %37, 1
  %cmp28 = icmp slt i32 %35, %sub27
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %38 = load float*, float** %avg.addr, align 4
  %39 = load i32, i32* %b, align 4
  %sub30 = sub nsw i32 %39, 1
  %arrayidx31 = getelementptr inbounds float, float* %38, i32 %sub30
  %40 = load float, float* %arrayidx31, align 4
  %41 = load float*, float** %avg.addr, align 4
  %42 = load i32, i32* %b, align 4
  %arrayidx32 = getelementptr inbounds float, float* %41, i32 %42
  %43 = load float, float* %arrayidx32, align 4
  %add33 = fadd float %40, %43
  %44 = load float*, float** %avg.addr, align 4
  %45 = load i32, i32* %b, align 4
  %add34 = add nsw i32 %45, 1
  %arrayidx35 = getelementptr inbounds float, float* %44, i32 %add34
  %46 = load float, float* %arrayidx35, align 4
  %add36 = fadd float %add33, %46
  store float %add36, float* %a, align 4
  %47 = load float, float* %a, align 4
  %conv37 = fpext float %47 to double
  %cmp38 = fcmp ogt double %conv37, 0.000000e+00
  br i1 %cmp38, label %if.then40, label %if.else81

if.then40:                                        ; preds = %for.body
  %48 = load float*, float** %max.addr, align 4
  %49 = load i32, i32* %b, align 4
  %sub41 = sub nsw i32 %49, 1
  %arrayidx42 = getelementptr inbounds float, float* %48, i32 %sub41
  %50 = load float, float* %arrayidx42, align 4
  store float %50, float* %m, align 4
  %51 = load float, float* %m, align 4
  %52 = load float*, float** %max.addr, align 4
  %53 = load i32, i32* %b, align 4
  %arrayidx43 = getelementptr inbounds float, float* %52, i32 %53
  %54 = load float, float* %arrayidx43, align 4
  %cmp44 = fcmp olt float %51, %54
  br i1 %cmp44, label %if.then46, label %if.end48

if.then46:                                        ; preds = %if.then40
  %55 = load float*, float** %max.addr, align 4
  %56 = load i32, i32* %b, align 4
  %arrayidx47 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx47, align 4
  store float %57, float* %m, align 4
  br label %if.end48

if.end48:                                         ; preds = %if.then46, %if.then40
  %58 = load float, float* %m, align 4
  %59 = load float*, float** %max.addr, align 4
  %60 = load i32, i32* %b, align 4
  %add49 = add nsw i32 %60, 1
  %arrayidx50 = getelementptr inbounds float, float* %59, i32 %add49
  %61 = load float, float* %arrayidx50, align 4
  %cmp51 = fcmp olt float %58, %61
  br i1 %cmp51, label %if.then53, label %if.end56

if.then53:                                        ; preds = %if.end48
  %62 = load float*, float** %max.addr, align 4
  %63 = load i32, i32* %b, align 4
  %add54 = add nsw i32 %63, 1
  %arrayidx55 = getelementptr inbounds float, float* %62, i32 %add54
  %64 = load float, float* %arrayidx55, align 4
  store float %64, float* %m, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.then53, %if.end48
  %65 = load float, float* %m, align 4
  %mul57 = fmul float %65, 3.000000e+00
  %66 = load float, float* %a, align 4
  %sub58 = fsub float %mul57, %66
  %mul59 = fmul float 2.000000e+01, %sub58
  %67 = load float, float* %a, align 4
  %68 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines60 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %68, i32 0, i32 8
  %69 = load i32, i32* %b, align 4
  %sub61 = sub nsw i32 %69, 1
  %arrayidx62 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines60, i32 0, i32 %sub61
  %70 = load i32, i32* %arrayidx62, align 4
  %71 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines63 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %71, i32 0, i32 8
  %72 = load i32, i32* %b, align 4
  %arrayidx64 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines63, i32 0, i32 %72
  %73 = load i32, i32* %arrayidx64, align 4
  %add65 = add nsw i32 %70, %73
  %74 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines66 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %74, i32 0, i32 8
  %75 = load i32, i32* %b, align 4
  %add67 = add nsw i32 %75, 1
  %arrayidx68 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines66, i32 0, i32 %add67
  %76 = load i32, i32* %arrayidx68, align 4
  %add69 = add nsw i32 %add65, %76
  %sub70 = sub nsw i32 %add69, 1
  %conv71 = sitofp i32 %sub70 to float
  %mul72 = fmul float %67, %conv71
  %div73 = fdiv float %mul59, %mul72
  store float %div73, float* %a, align 4
  %77 = load float, float* %a, align 4
  %conv74 = fptosi float %77 to i32
  store i32 %conv74, i32* %k, align 4
  %78 = load i32, i32* %k, align 4
  %cmp75 = icmp sgt i32 %78, 8
  br i1 %cmp75, label %if.then77, label %if.end78

if.then77:                                        ; preds = %if.end56
  store i32 8, i32* %k, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.then77, %if.end56
  %79 = load i32, i32* %k, align 4
  %conv79 = trunc i32 %79 to i8
  %80 = load i8*, i8** %mask_idx.addr, align 4
  %81 = load i32, i32* %b, align 4
  %arrayidx80 = getelementptr inbounds i8, i8* %80, i32 %81
  store i8 %conv79, i8* %arrayidx80, align 1
  br label %if.end83

if.else81:                                        ; preds = %for.body
  %82 = load i8*, i8** %mask_idx.addr, align 4
  %83 = load i32, i32* %b, align 4
  %arrayidx82 = getelementptr inbounds i8, i8* %82, i32 %83
  store i8 0, i8* %arrayidx82, align 1
  br label %if.end83

if.end83:                                         ; preds = %if.else81, %if.end78
  br label %for.inc

for.inc:                                          ; preds = %if.end83
  %84 = load i32, i32* %b, align 4
  %inc = add nsw i32 %84, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %85 = load float*, float** %avg.addr, align 4
  %86 = load i32, i32* %b, align 4
  %sub84 = sub nsw i32 %86, 1
  %arrayidx85 = getelementptr inbounds float, float* %85, i32 %sub84
  %87 = load float, float* %arrayidx85, align 4
  %88 = load float*, float** %avg.addr, align 4
  %89 = load i32, i32* %b, align 4
  %arrayidx86 = getelementptr inbounds float, float* %88, i32 %89
  %90 = load float, float* %arrayidx86, align 4
  %add87 = fadd float %87, %90
  store float %add87, float* %a, align 4
  %91 = load float, float* %a, align 4
  %cmp88 = fcmp ogt float %91, 0.000000e+00
  br i1 %cmp88, label %if.then90, label %if.else119

if.then90:                                        ; preds = %for.end
  %92 = load float*, float** %max.addr, align 4
  %93 = load i32, i32* %b, align 4
  %sub91 = sub nsw i32 %93, 1
  %arrayidx92 = getelementptr inbounds float, float* %92, i32 %sub91
  %94 = load float, float* %arrayidx92, align 4
  store float %94, float* %m, align 4
  %95 = load float, float* %m, align 4
  %96 = load float*, float** %max.addr, align 4
  %97 = load i32, i32* %b, align 4
  %arrayidx93 = getelementptr inbounds float, float* %96, i32 %97
  %98 = load float, float* %arrayidx93, align 4
  %cmp94 = fcmp olt float %95, %98
  br i1 %cmp94, label %if.then96, label %if.end98

if.then96:                                        ; preds = %if.then90
  %99 = load float*, float** %max.addr, align 4
  %100 = load i32, i32* %b, align 4
  %arrayidx97 = getelementptr inbounds float, float* %99, i32 %100
  %101 = load float, float* %arrayidx97, align 4
  store float %101, float* %m, align 4
  br label %if.end98

if.end98:                                         ; preds = %if.then96, %if.then90
  %102 = load float, float* %m, align 4
  %mul99 = fmul float %102, 2.000000e+00
  %103 = load float, float* %a, align 4
  %sub100 = fsub float %mul99, %103
  %mul101 = fmul float 2.000000e+01, %sub100
  %104 = load float, float* %a, align 4
  %105 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines102 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %105, i32 0, i32 8
  %106 = load i32, i32* %b, align 4
  %sub103 = sub nsw i32 %106, 1
  %arrayidx104 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines102, i32 0, i32 %sub103
  %107 = load i32, i32* %arrayidx104, align 4
  %108 = load %struct.PsyConst_CB2SB_t*, %struct.PsyConst_CB2SB_t** %gds, align 4
  %numlines105 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %108, i32 0, i32 8
  %109 = load i32, i32* %b, align 4
  %arrayidx106 = getelementptr inbounds [64 x i32], [64 x i32]* %numlines105, i32 0, i32 %109
  %110 = load i32, i32* %arrayidx106, align 4
  %add107 = add nsw i32 %107, %110
  %sub108 = sub nsw i32 %add107, 1
  %conv109 = sitofp i32 %sub108 to float
  %mul110 = fmul float %104, %conv109
  %div111 = fdiv float %mul101, %mul110
  store float %div111, float* %a, align 4
  %111 = load float, float* %a, align 4
  %conv112 = fptosi float %111 to i32
  store i32 %conv112, i32* %k, align 4
  %112 = load i32, i32* %k, align 4
  %cmp113 = icmp sgt i32 %112, 8
  br i1 %cmp113, label %if.then115, label %if.end116

if.then115:                                       ; preds = %if.end98
  store i32 8, i32* %k, align 4
  br label %if.end116

if.end116:                                        ; preds = %if.then115, %if.end98
  %113 = load i32, i32* %k, align 4
  %conv117 = trunc i32 %113 to i8
  %114 = load i8*, i8** %mask_idx.addr, align 4
  %115 = load i32, i32* %b, align 4
  %arrayidx118 = getelementptr inbounds i8, i8* %114, i32 %115
  store i8 %conv117, i8* %arrayidx118, align 1
  br label %if.end121

if.else119:                                       ; preds = %for.end
  %116 = load i8*, i8** %mask_idx.addr, align 4
  %117 = load i32, i32* %b, align 4
  %arrayidx120 = getelementptr inbounds i8, i8* %116, i32 %117
  store i8 0, i8* %arrayidx120, align 1
  br label %if.end121

if.end121:                                        ; preds = %if.else119, %if.end116
  ret void
}

declare float @freq2bark(float) #3

; Function Attrs: noinline nounwind optnone
define internal float @stereo_demask(double %f) #0 {
entry:
  %f.addr = alloca double, align 8
  %arg = alloca double, align 8
  store double %f, double* %f.addr, align 8
  %0 = load double, double* %f.addr, align 8
  %conv = fptrunc double %0 to float
  %call = call float @freq2bark(float %conv)
  %conv1 = fpext float %call to double
  store double %conv1, double* %arg, align 8
  %1 = load double, double* %arg, align 8
  %cmp = fcmp olt double %1, 1.550000e+01
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load double, double* %arg, align 8
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %2, %cond.true ], [ 1.550000e+01, %cond.false ]
  %div = fdiv double %cond, 1.550000e+01
  store double %div, double* %arg, align 8
  %3 = load double, double* %arg, align 8
  %mul = fmul double 0x400921FB54442D18, %3
  %4 = call double @llvm.cos.f64(double %mul)
  %sub = fsub double 1.000000e+00, %4
  %mul3 = fmul double 1.250000e+00, %sub
  %sub4 = fsub double %mul3, 2.500000e+00
  %5 = call double @llvm.pow.f64(double 1.000000e+01, double %sub4)
  %conv5 = fptrunc double %5 to float
  ret float %conv5
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #4

; Function Attrs: noinline nounwind optnone
define internal float @s3_func(float %bark) #0 {
entry:
  %retval = alloca float, align 4
  %bark.addr = alloca float, align 4
  %tempx = alloca float, align 4
  %x = alloca float, align 4
  %tempy = alloca float, align 4
  %temp = alloca float, align 4
  store float %bark, float* %bark.addr, align 4
  %0 = load float, float* %bark.addr, align 4
  store float %0, float* %tempx, align 4
  %1 = load float, float* %tempx, align 4
  %cmp = fcmp oge float %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %tempx, align 4
  %mul = fmul float %2, 3.000000e+00
  store float %mul, float* %tempx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %tempx, align 4
  %conv = fpext float %3 to double
  %mul1 = fmul double %conv, 1.500000e+00
  %conv2 = fptrunc double %mul1 to float
  store float %conv2, float* %tempx, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %4 = load float, float* %tempx, align 4
  %conv3 = fpext float %4 to double
  %cmp4 = fcmp oge double %conv3, 5.000000e-01
  br i1 %cmp4, label %land.lhs.true, label %if.else19

land.lhs.true:                                    ; preds = %if.end
  %5 = load float, float* %tempx, align 4
  %conv6 = fpext float %5 to double
  %cmp7 = fcmp ole double %conv6, 2.500000e+00
  br i1 %cmp7, label %if.then9, label %if.else19

if.then9:                                         ; preds = %land.lhs.true
  %6 = load float, float* %tempx, align 4
  %conv10 = fpext float %6 to double
  %sub = fsub double %conv10, 5.000000e-01
  %conv11 = fptrunc double %sub to float
  store float %conv11, float* %temp, align 4
  %7 = load float, float* %temp, align 4
  %8 = load float, float* %temp, align 4
  %mul12 = fmul float %7, %8
  %conv13 = fpext float %mul12 to double
  %9 = load float, float* %temp, align 4
  %conv14 = fpext float %9 to double
  %mul15 = fmul double 2.000000e+00, %conv14
  %sub16 = fsub double %conv13, %mul15
  %mul17 = fmul double 8.000000e+00, %sub16
  %conv18 = fptrunc double %mul17 to float
  store float %conv18, float* %x, align 4
  br label %if.end20

if.else19:                                        ; preds = %land.lhs.true, %if.end
  store float 0.000000e+00, float* %x, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.else19, %if.then9
  %10 = load float, float* %tempx, align 4
  %conv21 = fpext float %10 to double
  %add = fadd double %conv21, 4.740000e-01
  %conv22 = fptrunc double %add to float
  store float %conv22, float* %tempx, align 4
  %11 = load float, float* %tempx, align 4
  %conv23 = fpext float %11 to double
  %mul24 = fmul double 7.500000e+00, %conv23
  %add25 = fadd double 0x402F9F6E6106AB15, %mul24
  %12 = load float, float* %tempx, align 4
  %13 = load float, float* %tempx, align 4
  %mul26 = fmul float %12, %13
  %conv27 = fpext float %mul26 to double
  %add28 = fadd double 1.000000e+00, %conv27
  %14 = call double @llvm.sqrt.f64(double %add28)
  %mul29 = fmul double 1.750000e+01, %14
  %sub30 = fsub double %add25, %mul29
  %conv31 = fptrunc double %sub30 to float
  store float %conv31, float* %tempy, align 4
  %15 = load float, float* %tempy, align 4
  %conv32 = fpext float %15 to double
  %cmp33 = fcmp ole double %conv32, -6.000000e+01
  br i1 %cmp33, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end20
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end36:                                         ; preds = %if.end20
  %16 = load float, float* %x, align 4
  %17 = load float, float* %tempy, align 4
  %add37 = fadd float %16, %17
  %conv38 = fpext float %add37 to double
  %mul39 = fmul double %conv38, 0x3FCD791C5F888823
  %18 = call double @llvm.exp.f64(double %mul39)
  %conv40 = fptrunc double %18 to float
  store float %conv40, float* %tempx, align 4
  %19 = load float, float* %tempx, align 4
  %conv41 = fpext float %19 to double
  %div = fdiv double %conv41, 0x3FE526403B597262
  %conv42 = fptrunc double %div to float
  store float %conv42, float* %tempx, align 4
  %20 = load float, float* %tempx, align 4
  store float %20, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end36, %if.then35
  %21 = load float, float* %retval, align 4
  ret float %21
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #4

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
