; ModuleID = 'takehiro.c'
source_filename = "takehiro.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.anon.2 = type { i32, i32 }
%struct.huffcodetab = type { i32, i32, i16*, i8* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.calc_noise_data_t = type { i32, i32, [39 x i32], [39 x float], [39 x float] }
%union.fi_union = type { float }

@t32l = external constant [0 x i8], align 1
@t33l = external constant [0 x i8], align 1
@ipow20 = external global [257 x float], align 16
@slen1_tab = hidden constant [16 x i32] [i32 0, i32 0, i32 0, i32 0, i32 3, i32 1, i32 1, i32 1, i32 2, i32 2, i32 2, i32 3, i32 3, i32 3, i32 4, i32 4], align 16
@slen2_tab = hidden constant [16 x i32] [i32 0, i32 1, i32 2, i32 3, i32 0, i32 1, i32 2, i32 3, i32 1, i32 2, i32 3, i32 1, i32 2, i32 3, i32 2, i32 3], align 16
@pretab = external constant [22 x i32], align 16
@subdv_table = internal constant [23 x %struct.anon.2] [%struct.anon.2 zeroinitializer, %struct.anon.2 zeroinitializer, %struct.anon.2 zeroinitializer, %struct.anon.2 zeroinitializer, %struct.anon.2 zeroinitializer, %struct.anon.2 { i32 0, i32 1 }, %struct.anon.2 { i32 1, i32 1 }, %struct.anon.2 { i32 1, i32 1 }, %struct.anon.2 { i32 1, i32 2 }, %struct.anon.2 { i32 2, i32 2 }, %struct.anon.2 { i32 2, i32 3 }, %struct.anon.2 { i32 2, i32 3 }, %struct.anon.2 { i32 3, i32 4 }, %struct.anon.2 { i32 3, i32 4 }, %struct.anon.2 { i32 3, i32 4 }, %struct.anon.2 { i32 4, i32 5 }, %struct.anon.2 { i32 4, i32 5 }, %struct.anon.2 { i32 4, i32 6 }, %struct.anon.2 { i32 5, i32 6 }, %struct.anon.2 { i32 5, i32 6 }, %struct.anon.2 { i32 5, i32 7 }, %struct.anon.2 { i32 6, i32 7 }, %struct.anon.2 { i32 6, i32 7 }], align 16
@adj43asm = external global [8208 x float], align 16
@scfsi_band = external constant [5 x i32], align 16
@slen1_n = internal constant [16 x i32] [i32 1, i32 1, i32 1, i32 1, i32 8, i32 2, i32 2, i32 2, i32 4, i32 4, i32 4, i32 8, i32 8, i32 8, i32 16, i32 16], align 16
@slen2_n = internal constant [16 x i32] [i32 1, i32 2, i32 4, i32 8, i32 1, i32 2, i32 4, i32 8, i32 2, i32 4, i32 8, i32 2, i32 4, i32 8, i32 4, i32 8], align 16
@scale_short = internal constant [16 x i32] [i32 0, i32 18, i32 36, i32 54, i32 54, i32 36, i32 54, i32 72, i32 54, i32 72, i32 90, i32 72, i32 90, i32 108, i32 108, i32 126], align 16
@scale_mixed = internal constant [16 x i32] [i32 0, i32 18, i32 36, i32 54, i32 51, i32 35, i32 53, i32 71, i32 52, i32 70, i32 88, i32 69, i32 87, i32 105, i32 104, i32 122], align 16
@scale_long = internal constant [16 x i32] [i32 0, i32 10, i32 20, i32 30, i32 33, i32 21, i32 31, i32 41, i32 32, i32 42, i32 52, i32 43, i32 53, i32 63, i32 64, i32 74], align 16
@nr_of_sfb_block = external constant [6 x [3 x [4 x i32]]], align 16
@max_range_sfac_tab = internal constant [6 x [4 x i32]] [[4 x i32] [i32 15, i32 15, i32 7, i32 7], [4 x i32] [i32 15, i32 15, i32 7, i32 0], [4 x i32] [i32 7, i32 3, i32 0, i32 0], [4 x i32] [i32 15, i32 31, i32 31, i32 0], [4 x i32] [i32 7, i32 7, i32 7, i32 0], [4 x i32] [i32 3, i32 3, i32 0, i32 0]], align 16
@mpeg2_scale_bitcount.log2tab = internal constant [16 x i32] [i32 0, i32 1, i32 2, i32 2, i32 3, i32 3, i32 3, i32 3, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4], align 16
@.str = private unnamed_addr constant [38 x i8] c"intensity stereo not implemented yet\0A\00", align 1
@count_fncs = internal constant [16 x i32 (i32*, i32*, i32, i32*)*] [i32 (i32*, i32*, i32, i32*)* @count_bit_null, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from2, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from2, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3, i32 (i32*, i32*, i32, i32*)* @count_bit_noESC_from3], align 16
@ht = external constant [34 x %struct.huffcodetab], align 16
@huf_tbl_noESC = internal constant [15 x i32] [i32 1, i32 2, i32 5, i32 7, i32 7, i32 10, i32 10, i32 13, i32 13, i32 13, i32 13, i32 13, i32 13, i32 13, i32 13], align 16
@table23 = external constant [9 x i32], align 16
@table56 = external constant [16 x i32], align 16
@largetbl = external constant [256 x i32], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @noquant_count_bits(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi, %struct.calc_noise_data_t* %prev_noise) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %prev_noise.addr = alloca %struct.calc_noise_data_t*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %bits = alloca i32, align 4
  %i = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %ix = alloca i32*, align 4
  %x4 = alloca i32, align 4
  %x3 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %p = alloca i32, align 4
  %sfb = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store %struct.calc_noise_data_t* %prev_noise, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %bits, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  store i32* %arraydecay, i32** %ix, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 30
  %3 = load i32, i32* %max_nonzero_coeff, align 4
  %add = add nsw i32 %3, 2
  %shr = ashr i32 %add, 1
  %shl = shl i32 %shr, 1
  %cmp = icmp slt i32 576, %shl
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %max_nonzero_coeff2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 30
  %5 = load i32, i32* %max_nonzero_coeff2, align 4
  %add3 = add nsw i32 %5, 2
  %shr4 = ashr i32 %add3, 1
  %shl5 = shl i32 %shr4, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 576, %cond.true ], [ %shl5, %cond.false ]
  store i32 %cond, i32* %i, align 4
  %6 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool = icmp ne %struct.calc_noise_data_t* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %7 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %sfb_count1 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %7, i32 0, i32 1
  store i32 0, i32* %sfb_count1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4
  %cmp6 = icmp sgt i32 %8, 1
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i32*, i32** %ix, align 4
  %10 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %10, 1
  %arrayidx = getelementptr inbounds i32, i32* %9, i32 %sub
  %11 = load i32, i32* %arrayidx, align 4
  %12 = load i32*, i32** %ix, align 4
  %13 = load i32, i32* %i, align 4
  %sub7 = sub nsw i32 %13, 2
  %arrayidx8 = getelementptr inbounds i32, i32* %12, i32 %sub7
  %14 = load i32, i32* %arrayidx8, align 4
  %or = or i32 %11, %14
  %tobool9 = icmp ne i32 %or, 0
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.body
  br label %for.end

if.end11:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %15 = load i32, i32* %i, align 4
  %sub12 = sub nsw i32 %15, 2
  store i32 %sub12, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then10, %for.cond
  %16 = load i32, i32* %i, align 4
  %17 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 6
  store i32 %16, i32* %count1, align 4
  store i32 0, i32* %a2, align 4
  store i32 0, i32* %a1, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc40, %for.end
  %18 = load i32, i32* %i, align 4
  %cmp14 = icmp sgt i32 %18, 3
  br i1 %cmp14, label %for.body15, label %for.end42

for.body15:                                       ; preds = %for.cond13
  %19 = load i32*, i32** %ix, align 4
  %20 = load i32, i32* %i, align 4
  %sub16 = sub nsw i32 %20, 4
  %arrayidx17 = getelementptr inbounds i32, i32* %19, i32 %sub16
  %21 = load i32, i32* %arrayidx17, align 4
  store i32 %21, i32* %x4, align 4
  %22 = load i32*, i32** %ix, align 4
  %23 = load i32, i32* %i, align 4
  %sub18 = sub nsw i32 %23, 3
  %arrayidx19 = getelementptr inbounds i32, i32* %22, i32 %sub18
  %24 = load i32, i32* %arrayidx19, align 4
  store i32 %24, i32* %x3, align 4
  %25 = load i32*, i32** %ix, align 4
  %26 = load i32, i32* %i, align 4
  %sub20 = sub nsw i32 %26, 2
  %arrayidx21 = getelementptr inbounds i32, i32* %25, i32 %sub20
  %27 = load i32, i32* %arrayidx21, align 4
  store i32 %27, i32* %x2, align 4
  %28 = load i32*, i32** %ix, align 4
  %29 = load i32, i32* %i, align 4
  %sub22 = sub nsw i32 %29, 1
  %arrayidx23 = getelementptr inbounds i32, i32* %28, i32 %sub22
  %30 = load i32, i32* %arrayidx23, align 4
  store i32 %30, i32* %x1, align 4
  %31 = load i32, i32* %x4, align 4
  %32 = load i32, i32* %x3, align 4
  %or24 = or i32 %31, %32
  %33 = load i32, i32* %x2, align 4
  %or25 = or i32 %or24, %33
  %34 = load i32, i32* %x1, align 4
  %or26 = or i32 %or25, %34
  %cmp27 = icmp ugt i32 %or26, 1
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.body15
  br label %for.end42

if.end29:                                         ; preds = %for.body15
  %35 = load i32, i32* %x4, align 4
  %mul = mul nsw i32 %35, 2
  %36 = load i32, i32* %x3, align 4
  %add30 = add nsw i32 %mul, %36
  %mul31 = mul nsw i32 %add30, 2
  %37 = load i32, i32* %x2, align 4
  %add32 = add nsw i32 %mul31, %37
  %mul33 = mul nsw i32 %add32, 2
  %38 = load i32, i32* %x1, align 4
  %add34 = add nsw i32 %mul33, %38
  store i32 %add34, i32* %p, align 4
  %39 = load i32, i32* %p, align 4
  %arrayidx35 = getelementptr inbounds [0 x i8], [0 x i8]* @t32l, i32 0, i32 %39
  %40 = load i8, i8* %arrayidx35, align 1
  %conv = zext i8 %40 to i32
  %41 = load i32, i32* %a1, align 4
  %add36 = add nsw i32 %41, %conv
  store i32 %add36, i32* %a1, align 4
  %42 = load i32, i32* %p, align 4
  %arrayidx37 = getelementptr inbounds [0 x i8], [0 x i8]* @t33l, i32 0, i32 %42
  %43 = load i8, i8* %arrayidx37, align 1
  %conv38 = zext i8 %43 to i32
  %44 = load i32, i32* %a2, align 4
  %add39 = add nsw i32 %44, %conv38
  store i32 %add39, i32* %a2, align 4
  br label %for.inc40

for.inc40:                                        ; preds = %if.end29
  %45 = load i32, i32* %i, align 4
  %sub41 = sub nsw i32 %45, 4
  store i32 %sub41, i32* %i, align 4
  br label %for.cond13

for.end42:                                        ; preds = %if.then28, %for.cond13
  %46 = load i32, i32* %a1, align 4
  store i32 %46, i32* %bits, align 4
  %47 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 17
  store i32 0, i32* %count1table_select, align 4
  %48 = load i32, i32* %a1, align 4
  %49 = load i32, i32* %a2, align 4
  %cmp43 = icmp sgt i32 %48, %49
  br i1 %cmp43, label %if.then45, label %if.end47

if.then45:                                        ; preds = %for.end42
  %50 = load i32, i32* %a2, align 4
  store i32 %50, i32* %bits, align 4
  %51 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1table_select46 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %51, i32 0, i32 17
  store i32 1, i32* %count1table_select46, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %for.end42
  %52 = load i32, i32* %bits, align 4
  %53 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1bits = getelementptr inbounds %struct.gr_info, %struct.gr_info* %53, i32 0, i32 27
  store i32 %52, i32* %count1bits, align 4
  %54 = load i32, i32* %i, align 4
  %55 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %55, i32 0, i32 5
  store i32 %54, i32* %big_values, align 4
  %56 = load i32, i32* %i, align 4
  %cmp48 = icmp eq i32 %56, 0
  br i1 %cmp48, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end47
  %57 = load i32, i32* %bits, align 4
  store i32 %57, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.end47
  %58 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %58, i32 0, i32 9
  %59 = load i32, i32* %block_type, align 4
  %cmp52 = icmp eq i32 %59, 2
  br i1 %cmp52, label %if.then54, label %if.else

if.then54:                                        ; preds = %if.end51
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %60, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 1
  %arrayidx55 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 3
  %61 = load i32, i32* %arrayidx55, align 4
  %mul56 = mul nsw i32 3, %61
  store i32 %mul56, i32* %a1, align 4
  %62 = load i32, i32* %a1, align 4
  %63 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values57 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %63, i32 0, i32 5
  %64 = load i32, i32* %big_values57, align 4
  %cmp58 = icmp sgt i32 %62, %64
  br i1 %cmp58, label %if.then60, label %if.end62

if.then60:                                        ; preds = %if.then54
  %65 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values61 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %65, i32 0, i32 5
  %66 = load i32, i32* %big_values61, align 4
  store i32 %66, i32* %a1, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %if.then54
  %67 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values63 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %67, i32 0, i32 5
  %68 = load i32, i32* %big_values63, align 4
  store i32 %68, i32* %a2, align 4
  br label %if.end101

if.else:                                          ; preds = %if.end51
  %69 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type64 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %69, i32 0, i32 9
  %70 = load i32, i32* %block_type64, align 4
  %cmp65 = icmp eq i32 %70, 0
  br i1 %cmp65, label %if.then67, label %if.else90

if.then67:                                        ; preds = %if.else
  %71 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %71, i32 0, i32 13
  %bv_scf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 10
  %72 = load i32, i32* %i, align 4
  %sub68 = sub nsw i32 %72, 2
  %arrayidx69 = getelementptr inbounds [576 x i8], [576 x i8]* %bv_scf, i32 0, i32 %sub68
  %73 = load i8, i8* %arrayidx69, align 1
  %conv70 = sext i8 %73 to i32
  %74 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region0_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %74, i32 0, i32 13
  store i32 %conv70, i32* %region0_count, align 4
  store i32 %conv70, i32* %a1, align 4
  %75 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt71 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %75, i32 0, i32 13
  %bv_scf72 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt71, i32 0, i32 10
  %76 = load i32, i32* %i, align 4
  %sub73 = sub nsw i32 %76, 1
  %arrayidx74 = getelementptr inbounds [576 x i8], [576 x i8]* %bv_scf72, i32 0, i32 %sub73
  %77 = load i8, i8* %arrayidx74, align 1
  %conv75 = sext i8 %77 to i32
  %78 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region1_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %78, i32 0, i32 14
  store i32 %conv75, i32* %region1_count, align 4
  store i32 %conv75, i32* %a2, align 4
  %79 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band76 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %79, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band76, i32 0, i32 0
  %80 = load i32, i32* %a1, align 4
  %81 = load i32, i32* %a2, align 4
  %add77 = add nsw i32 %80, %81
  %add78 = add nsw i32 %add77, 2
  %arrayidx79 = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %add78
  %82 = load i32, i32* %arrayidx79, align 4
  store i32 %82, i32* %a2, align 4
  %83 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band80 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %83, i32 0, i32 8
  %l81 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band80, i32 0, i32 0
  %84 = load i32, i32* %a1, align 4
  %add82 = add nsw i32 %84, 1
  %arrayidx83 = getelementptr inbounds [23 x i32], [23 x i32]* %l81, i32 0, i32 %add82
  %85 = load i32, i32* %arrayidx83, align 4
  store i32 %85, i32* %a1, align 4
  %86 = load i32, i32* %a2, align 4
  %87 = load i32, i32* %i, align 4
  %cmp84 = icmp slt i32 %86, %87
  br i1 %cmp84, label %if.then86, label %if.end89

if.then86:                                        ; preds = %if.then67
  %88 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %88, i32 0, i32 25
  %89 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table, align 8
  %90 = load i32*, i32** %ix, align 4
  %91 = load i32, i32* %a2, align 4
  %add.ptr = getelementptr inbounds i32, i32* %90, i32 %91
  %92 = load i32*, i32** %ix, align 4
  %93 = load i32, i32* %i, align 4
  %add.ptr87 = getelementptr inbounds i32, i32* %92, i32 %93
  %call = call i32 %89(i32* %add.ptr, i32* %add.ptr87, i32* %bits)
  %94 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %94, i32 0, i32 11
  %arrayidx88 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 2
  store i32 %call, i32* %arrayidx88, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then86, %if.then67
  br label %if.end100

if.else90:                                        ; preds = %if.else
  %95 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region0_count91 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %95, i32 0, i32 13
  store i32 7, i32* %region0_count91, align 4
  %96 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region1_count92 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %96, i32 0, i32 14
  store i32 13, i32* %region1_count92, align 4
  %97 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band93 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %97, i32 0, i32 8
  %l94 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band93, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [23 x i32], [23 x i32]* %l94, i32 0, i32 8
  %98 = load i32, i32* %arrayidx95, align 4
  store i32 %98, i32* %a1, align 4
  %99 = load i32, i32* %i, align 4
  store i32 %99, i32* %a2, align 4
  %100 = load i32, i32* %a1, align 4
  %101 = load i32, i32* %a2, align 4
  %cmp96 = icmp sgt i32 %100, %101
  br i1 %cmp96, label %if.then98, label %if.end99

if.then98:                                        ; preds = %if.else90
  %102 = load i32, i32* %a2, align 4
  store i32 %102, i32* %a1, align 4
  br label %if.end99

if.end99:                                         ; preds = %if.then98, %if.else90
  br label %if.end100

if.end100:                                        ; preds = %if.end99, %if.end89
  br label %if.end101

if.end101:                                        ; preds = %if.end100, %if.end62
  %103 = load i32, i32* %a1, align 4
  %104 = load i32, i32* %i, align 4
  %cmp102 = icmp slt i32 %103, %104
  br i1 %cmp102, label %cond.true104, label %cond.false105

cond.true104:                                     ; preds = %if.end101
  %105 = load i32, i32* %a1, align 4
  br label %cond.end106

cond.false105:                                    ; preds = %if.end101
  %106 = load i32, i32* %i, align 4
  br label %cond.end106

cond.end106:                                      ; preds = %cond.false105, %cond.true104
  %cond107 = phi i32 [ %105, %cond.true104 ], [ %106, %cond.false105 ]
  store i32 %cond107, i32* %a1, align 4
  %107 = load i32, i32* %a2, align 4
  %108 = load i32, i32* %i, align 4
  %cmp108 = icmp slt i32 %107, %108
  br i1 %cmp108, label %cond.true110, label %cond.false111

cond.true110:                                     ; preds = %cond.end106
  %109 = load i32, i32* %a2, align 4
  br label %cond.end112

cond.false111:                                    ; preds = %cond.end106
  %110 = load i32, i32* %i, align 4
  br label %cond.end112

cond.end112:                                      ; preds = %cond.false111, %cond.true110
  %cond113 = phi i32 [ %109, %cond.true110 ], [ %110, %cond.false111 ]
  store i32 %cond113, i32* %a2, align 4
  %111 = load i32, i32* %a1, align 4
  %cmp114 = icmp slt i32 0, %111
  br i1 %cmp114, label %if.then116, label %if.end122

if.then116:                                       ; preds = %cond.end112
  %112 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table117 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %112, i32 0, i32 25
  %113 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table117, align 8
  %114 = load i32*, i32** %ix, align 4
  %115 = load i32*, i32** %ix, align 4
  %116 = load i32, i32* %a1, align 4
  %add.ptr118 = getelementptr inbounds i32, i32* %115, i32 %116
  %call119 = call i32 %113(i32* %114, i32* %add.ptr118, i32* %bits)
  %117 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select120 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %117, i32 0, i32 11
  %arrayidx121 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select120, i32 0, i32 0
  store i32 %call119, i32* %arrayidx121, align 4
  br label %if.end122

if.end122:                                        ; preds = %if.then116, %cond.end112
  %118 = load i32, i32* %a1, align 4
  %119 = load i32, i32* %a2, align 4
  %cmp123 = icmp slt i32 %118, %119
  br i1 %cmp123, label %if.then125, label %if.end132

if.then125:                                       ; preds = %if.end122
  %120 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table126 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %120, i32 0, i32 25
  %121 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table126, align 8
  %122 = load i32*, i32** %ix, align 4
  %123 = load i32, i32* %a1, align 4
  %add.ptr127 = getelementptr inbounds i32, i32* %122, i32 %123
  %124 = load i32*, i32** %ix, align 4
  %125 = load i32, i32* %a2, align 4
  %add.ptr128 = getelementptr inbounds i32, i32* %124, i32 %125
  %call129 = call i32 %121(i32* %add.ptr127, i32* %add.ptr128, i32* %bits)
  %126 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select130 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %126, i32 0, i32 11
  %arrayidx131 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select130, i32 0, i32 1
  store i32 %call129, i32* %arrayidx131, align 4
  br label %if.end132

if.end132:                                        ; preds = %if.then125, %if.end122
  %127 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %127, i32 0, i32 5
  %128 = load i32, i32* %use_best_huffman, align 4
  %cmp133 = icmp eq i32 %128, 2
  br i1 %cmp133, label %if.then135, label %if.end137

if.then135:                                       ; preds = %if.end132
  %129 = load i32, i32* %bits, align 4
  %130 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %130, i32 0, i32 4
  store i32 %129, i32* %part2_3_length, align 4
  %131 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %132 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  call void @best_huffman_divide(%struct.lame_internal_flags* %131, %struct.gr_info* %132)
  %133 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length136 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %133, i32 0, i32 4
  %134 = load i32, i32* %part2_3_length136, align 4
  store i32 %134, i32* %bits, align 4
  br label %if.end137

if.end137:                                        ; preds = %if.then135, %if.end132
  %135 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool138 = icmp ne %struct.calc_noise_data_t* %135, null
  br i1 %tobool138, label %if.then139, label %if.end152

if.then139:                                       ; preds = %if.end137
  %136 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type140 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %136, i32 0, i32 9
  %137 = load i32, i32* %block_type140, align 4
  %cmp141 = icmp eq i32 %137, 0
  br i1 %cmp141, label %if.then143, label %if.end151

if.then143:                                       ; preds = %if.then139
  store i32 0, i32* %sfb, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then143
  %138 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band144 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %138, i32 0, i32 8
  %l145 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band144, i32 0, i32 0
  %139 = load i32, i32* %sfb, align 4
  %arrayidx146 = getelementptr inbounds [23 x i32], [23 x i32]* %l145, i32 0, i32 %139
  %140 = load i32, i32* %arrayidx146, align 4
  %141 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values147 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %141, i32 0, i32 5
  %142 = load i32, i32* %big_values147, align 4
  %cmp148 = icmp slt i32 %140, %142
  br i1 %cmp148, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %143 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %143, 1
  store i32 %inc, i32* %sfb, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %144 = load i32, i32* %sfb, align 4
  %145 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %sfb_count1150 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %145, i32 0, i32 1
  store i32 %144, i32* %sfb_count1150, align 4
  br label %if.end151

if.end151:                                        ; preds = %while.end, %if.then139
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.end137
  %146 = load i32, i32* %bits, align 4
  store i32 %146, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end152, %if.then50
  %147 = load i32, i32* %retval, align 4
  ret i32 %147
}

; Function Attrs: noinline nounwind optnone
define hidden void @best_huffman_divide(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %i = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %cod_info2 = alloca %struct.gr_info, align 4
  %ix = alloca i32*, align 4
  %r01_bits = alloca [23 x i32], align 16
  %r01_div = alloca [23 x i32], align 16
  %r0_tbl = alloca [23 x i32], align 16
  %r1_tbl = alloca [23 x i32], align 16
  %p = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  store i32* %arraydecay, i32** %ix, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 9
  %3 = load i32, i32* %block_type, align 4
  %cmp = icmp eq i32 %3, 2
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 15
  %5 = load i32, i32* %mode_gr, align 4
  %cmp2 = icmp eq i32 %5, 1
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %if.end88

if.end:                                           ; preds = %land.lhs.true, %entry
  %6 = bitcast %struct.gr_info* %cod_info2 to i8*
  %7 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %8 = bitcast %struct.gr_info* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %8, i32 5252, i1 false)
  %9 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 9
  %10 = load i32, i32* %block_type3, align 4
  %cmp4 = icmp eq i32 %10, 0
  br i1 %cmp4, label %if.then5, label %if.end14

if.then5:                                         ; preds = %if.end
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %12 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %13 = load i32*, i32** %ix, align 4
  %arraydecay6 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_bits, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_div, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [23 x i32], [23 x i32]* %r0_tbl, i32 0, i32 0
  %arraydecay9 = getelementptr inbounds [23 x i32], [23 x i32]* %r1_tbl, i32 0, i32 0
  call void @recalc_divide_init(%struct.lame_internal_flags* %11, %struct.gr_info* %12, i32* %13, i32* %arraydecay6, i32* %arraydecay7, i32* %arraydecay8, i32* %arraydecay9)
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %15 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %16 = load i32*, i32** %ix, align 4
  %arraydecay10 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_bits, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_div, i32 0, i32 0
  %arraydecay12 = getelementptr inbounds [23 x i32], [23 x i32]* %r0_tbl, i32 0, i32 0
  %arraydecay13 = getelementptr inbounds [23 x i32], [23 x i32]* %r1_tbl, i32 0, i32 0
  call void @recalc_divide_sub(%struct.lame_internal_flags* %14, %struct.gr_info* %cod_info2, %struct.gr_info* %15, i32* %16, i32* %arraydecay10, i32* %arraydecay11, i32* %arraydecay12, i32* %arraydecay13)
  br label %if.end14

if.end14:                                         ; preds = %if.then5, %if.end
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 5
  %17 = load i32, i32* %big_values, align 4
  store i32 %17, i32* %i, align 4
  %18 = load i32, i32* %i, align 4
  %cmp15 = icmp eq i32 %18, 0
  br i1 %cmp15, label %if.then19, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end14
  %19 = load i32*, i32** %ix, align 4
  %20 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %20, 2
  %arrayidx = getelementptr inbounds i32, i32* %19, i32 %sub
  %21 = load i32, i32* %arrayidx, align 4
  %22 = load i32*, i32** %ix, align 4
  %23 = load i32, i32* %i, align 4
  %sub16 = sub nsw i32 %23, 1
  %arrayidx17 = getelementptr inbounds i32, i32* %22, i32 %sub16
  %24 = load i32, i32* %arrayidx17, align 4
  %or = or i32 %21, %24
  %cmp18 = icmp ugt i32 %or, 1
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %lor.lhs.false, %if.end14
  br label %if.end88

if.end20:                                         ; preds = %lor.lhs.false
  %25 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 6
  %26 = load i32, i32* %count1, align 4
  %add = add nsw i32 %26, 2
  store i32 %add, i32* %i, align 4
  %27 = load i32, i32* %i, align 4
  %cmp21 = icmp sgt i32 %27, 576
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end20
  br label %if.end88

if.end23:                                         ; preds = %if.end20
  %28 = bitcast %struct.gr_info* %cod_info2 to i8*
  %29 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %30 = bitcast %struct.gr_info* %29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %30, i32 5252, i1 false)
  %31 = load i32, i32* %i, align 4
  %count124 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 6
  store i32 %31, i32* %count124, align 4
  store i32 0, i32* %a2, align 4
  store i32 0, i32* %a1, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end23
  %32 = load i32, i32* %i, align 4
  %big_values25 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 5
  %33 = load i32, i32* %big_values25, align 4
  %cmp26 = icmp sgt i32 %32, %33
  br i1 %cmp26, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %34 = load i32*, i32** %ix, align 4
  %35 = load i32, i32* %i, align 4
  %sub27 = sub nsw i32 %35, 4
  %arrayidx28 = getelementptr inbounds i32, i32* %34, i32 %sub27
  %36 = load i32, i32* %arrayidx28, align 4
  %mul = mul nsw i32 %36, 2
  %37 = load i32*, i32** %ix, align 4
  %38 = load i32, i32* %i, align 4
  %sub29 = sub nsw i32 %38, 3
  %arrayidx30 = getelementptr inbounds i32, i32* %37, i32 %sub29
  %39 = load i32, i32* %arrayidx30, align 4
  %add31 = add nsw i32 %mul, %39
  %mul32 = mul nsw i32 %add31, 2
  %40 = load i32*, i32** %ix, align 4
  %41 = load i32, i32* %i, align 4
  %sub33 = sub nsw i32 %41, 2
  %arrayidx34 = getelementptr inbounds i32, i32* %40, i32 %sub33
  %42 = load i32, i32* %arrayidx34, align 4
  %add35 = add nsw i32 %mul32, %42
  %mul36 = mul nsw i32 %add35, 2
  %43 = load i32*, i32** %ix, align 4
  %44 = load i32, i32* %i, align 4
  %sub37 = sub nsw i32 %44, 1
  %arrayidx38 = getelementptr inbounds i32, i32* %43, i32 %sub37
  %45 = load i32, i32* %arrayidx38, align 4
  %add39 = add nsw i32 %mul36, %45
  store i32 %add39, i32* %p, align 4
  %46 = load i32, i32* %p, align 4
  %arrayidx40 = getelementptr inbounds [0 x i8], [0 x i8]* @t32l, i32 0, i32 %46
  %47 = load i8, i8* %arrayidx40, align 1
  %conv = zext i8 %47 to i32
  %48 = load i32, i32* %a1, align 4
  %add41 = add nsw i32 %48, %conv
  store i32 %add41, i32* %a1, align 4
  %49 = load i32, i32* %p, align 4
  %arrayidx42 = getelementptr inbounds [0 x i8], [0 x i8]* @t33l, i32 0, i32 %49
  %50 = load i8, i8* %arrayidx42, align 1
  %conv43 = zext i8 %50 to i32
  %51 = load i32, i32* %a2, align 4
  %add44 = add nsw i32 %51, %conv43
  store i32 %add44, i32* %a2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %i, align 4
  %sub45 = sub nsw i32 %52, 4
  store i32 %sub45, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = load i32, i32* %i, align 4
  %big_values46 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 5
  store i32 %53, i32* %big_values46, align 4
  %count1table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 17
  store i32 0, i32* %count1table_select, align 4
  %54 = load i32, i32* %a1, align 4
  %55 = load i32, i32* %a2, align 4
  %cmp47 = icmp sgt i32 %54, %55
  br i1 %cmp47, label %if.then49, label %if.end51

if.then49:                                        ; preds = %for.end
  %56 = load i32, i32* %a2, align 4
  store i32 %56, i32* %a1, align 4
  %count1table_select50 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 17
  store i32 1, i32* %count1table_select50, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.then49, %for.end
  %57 = load i32, i32* %a1, align 4
  %count1bits = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 27
  store i32 %57, i32* %count1bits, align 4
  %block_type52 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 9
  %58 = load i32, i32* %block_type52, align 4
  %cmp53 = icmp eq i32 %58, 0
  br i1 %cmp53, label %if.then55, label %if.else

if.then55:                                        ; preds = %if.end51
  %59 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %60 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %61 = load i32*, i32** %ix, align 4
  %arraydecay56 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_bits, i32 0, i32 0
  %arraydecay57 = getelementptr inbounds [23 x i32], [23 x i32]* %r01_div, i32 0, i32 0
  %arraydecay58 = getelementptr inbounds [23 x i32], [23 x i32]* %r0_tbl, i32 0, i32 0
  %arraydecay59 = getelementptr inbounds [23 x i32], [23 x i32]* %r1_tbl, i32 0, i32 0
  call void @recalc_divide_sub(%struct.lame_internal_flags* %59, %struct.gr_info* %cod_info2, %struct.gr_info* %60, i32* %61, i32* %arraydecay56, i32* %arraydecay57, i32* %arraydecay58, i32* %arraydecay59)
  br label %if.end88

if.else:                                          ; preds = %if.end51
  %62 = load i32, i32* %a1, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 4
  store i32 %62, i32* %part2_3_length, align 4
  %63 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %63, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 8
  %64 = load i32, i32* %arrayidx60, align 4
  store i32 %64, i32* %a1, align 4
  %65 = load i32, i32* %a1, align 4
  %66 = load i32, i32* %i, align 4
  %cmp61 = icmp sgt i32 %65, %66
  br i1 %cmp61, label %if.then63, label %if.end64

if.then63:                                        ; preds = %if.else
  %67 = load i32, i32* %i, align 4
  store i32 %67, i32* %a1, align 4
  br label %if.end64

if.end64:                                         ; preds = %if.then63, %if.else
  %68 = load i32, i32* %a1, align 4
  %cmp65 = icmp sgt i32 %68, 0
  br i1 %cmp65, label %if.then67, label %if.end70

if.then67:                                        ; preds = %if.end64
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 25
  %70 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table, align 8
  %71 = load i32*, i32** %ix, align 4
  %72 = load i32*, i32** %ix, align 4
  %73 = load i32, i32* %a1, align 4
  %add.ptr = getelementptr inbounds i32, i32* %72, i32 %73
  %part2_3_length68 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 4
  %call = call i32 %70(i32* %71, i32* %add.ptr, i32* %part2_3_length68)
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 11
  %arrayidx69 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  store i32 %call, i32* %arrayidx69, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then67, %if.end64
  %74 = load i32, i32* %i, align 4
  %75 = load i32, i32* %a1, align 4
  %cmp71 = icmp sgt i32 %74, %75
  br i1 %cmp71, label %if.then73, label %if.end81

if.then73:                                        ; preds = %if.end70
  %76 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table74 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %76, i32 0, i32 25
  %77 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table74, align 8
  %78 = load i32*, i32** %ix, align 4
  %79 = load i32, i32* %a1, align 4
  %add.ptr75 = getelementptr inbounds i32, i32* %78, i32 %79
  %80 = load i32*, i32** %ix, align 4
  %81 = load i32, i32* %i, align 4
  %add.ptr76 = getelementptr inbounds i32, i32* %80, i32 %81
  %part2_3_length77 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 4
  %call78 = call i32 %77(i32* %add.ptr75, i32* %add.ptr76, i32* %part2_3_length77)
  %table_select79 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 11
  %arrayidx80 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select79, i32 0, i32 1
  store i32 %call78, i32* %arrayidx80, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then73, %if.end70
  %82 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length82 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %82, i32 0, i32 4
  %83 = load i32, i32* %part2_3_length82, align 4
  %part2_3_length83 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info2, i32 0, i32 4
  %84 = load i32, i32* %part2_3_length83, align 4
  %cmp84 = icmp sgt i32 %83, %84
  br i1 %cmp84, label %if.then86, label %if.end87

if.then86:                                        ; preds = %if.end81
  %85 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %86 = bitcast %struct.gr_info* %85 to i8*
  %87 = bitcast %struct.gr_info* %cod_info2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %86, i8* align 4 %87, i32 5252, i1 false)
  br label %if.end87

if.end87:                                         ; preds = %if.then86, %if.end81
  br label %if.end88

if.end88:                                         ; preds = %if.then, %if.then19, %if.then22, %if.end87, %if.then55
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @count_bits(%struct.lame_internal_flags* %gfc, float* %xr, %struct.gr_info* %gi, %struct.calc_noise_data_t* %prev_noise) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %xr.addr = alloca float*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %prev_noise.addr = alloca %struct.calc_noise_data_t*, align 4
  %ix = alloca i32*, align 4
  %w = alloca float, align 4
  %sfb = alloca i32, align 4
  %j = alloca i32, align 4
  %gain = alloca i32, align 4
  %roundfac = alloca float, align 4
  %width = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %xr, float** %xr.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store %struct.calc_noise_data_t* %prev_noise, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  store i32* %arraydecay, i32** %ix, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 7
  %2 = load i32, i32* %global_gain, align 4
  %arrayidx = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %div = fdiv float 8.206000e+03, %3
  store float %div, float* %w, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 3
  %5 = load float, float* %xrpow_max, align 4
  %6 = load float, float* %w, align 4
  %cmp = fcmp ogt float %5, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 100000, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %7 = load float*, float** %xr.addr, align 4
  %8 = load i32*, i32** %ix, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %global_gain1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 7
  %10 = load i32, i32* %global_gain1, align 4
  %arrayidx2 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %10
  %11 = load float, float* %arrayidx2, align 4
  %12 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %13 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  call void @quantize_xrpow(float* %7, i32* %8, float %11, %struct.gr_info* %12, %struct.calc_noise_data_t* %13)
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %15 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %15, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then3, label %if.end31

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %j, align 4
  %16 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %global_gain4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 7
  %17 = load i32, i32* %global_gain4, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %18, i32 0, i32 16
  %19 = load i32, i32* %scalefac_scale, align 4
  %add = add nsw i32 %17, %19
  store i32 %add, i32* %gain, align 4
  %20 = load i32, i32* %gain, align 4
  %arrayidx5 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %20
  %21 = load float, float* %arrayidx5, align 4
  %conv = fpext float %21 to double
  %div6 = fdiv double 0x3FE44E006A3AB199, %conv
  %conv7 = fptrunc double %div6 to float
  store float %conv7, float* %roundfac, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %if.then3
  %22 = load i32, i32* %sfb, align 4
  %23 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %23, i32 0, i32 22
  %24 = load i32, i32* %sfbmax, align 4
  %cmp8 = icmp slt i32 %22, %24
  br i1 %cmp8, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %25 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %width10 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 25
  %26 = load i32, i32* %sfb, align 4
  %arrayidx11 = getelementptr inbounds [39 x i32], [39 x i32]* %width10, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx11, align 4
  store i32 %27, i32* %width, align 4
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %28, i32 0, i32 13
  %pseudohalf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt12, i32 0, i32 7
  %29 = load i32, i32* %sfb, align 4
  %arrayidx13 = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf, i32 0, i32 %29
  %30 = load i32, i32* %arrayidx13, align 4
  %tobool14 = icmp ne i32 %30, 0
  br i1 %tobool14, label %if.else, label %if.then15

if.then15:                                        ; preds = %for.body
  %31 = load i32, i32* %width, align 4
  %32 = load i32, i32* %j, align 4
  %add16 = add nsw i32 %32, %31
  store i32 %add16, i32* %j, align 4
  br label %if.end27

if.else:                                          ; preds = %for.body
  %33 = load i32, i32* %j, align 4
  store i32 %33, i32* %k, align 4
  %34 = load i32, i32* %width, align 4
  %35 = load i32, i32* %j, align 4
  %add17 = add nsw i32 %35, %34
  store i32 %add17, i32* %j, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %if.else
  %36 = load i32, i32* %k, align 4
  %37 = load i32, i32* %j, align 4
  %cmp19 = icmp slt i32 %36, %37
  br i1 %cmp19, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond18
  %38 = load float*, float** %xr.addr, align 4
  %39 = load i32, i32* %k, align 4
  %arrayidx22 = getelementptr inbounds float, float* %38, i32 %39
  %40 = load float, float* %arrayidx22, align 4
  %41 = load float, float* %roundfac, align 4
  %cmp23 = fcmp oge float %40, %41
  br i1 %cmp23, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body21
  %42 = load i32*, i32** %ix, align 4
  %43 = load i32, i32* %k, align 4
  %arrayidx25 = getelementptr inbounds i32, i32* %42, i32 %43
  %44 = load i32, i32* %arrayidx25, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body21
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %44, %cond.true ], [ 0, %cond.false ]
  %45 = load i32*, i32** %ix, align 4
  %46 = load i32, i32* %k, align 4
  %arrayidx26 = getelementptr inbounds i32, i32* %45, i32 %46
  store i32 %cond, i32* %arrayidx26, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %47 = load i32, i32* %k, align 4
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  br label %if.end27

if.end27:                                         ; preds = %for.end, %if.then15
  br label %for.inc28

for.inc28:                                        ; preds = %if.end27
  %48 = load i32, i32* %sfb, align 4
  %inc29 = add nsw i32 %48, 1
  store i32 %inc29, i32* %sfb, align 4
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  br label %if.end31

if.end31:                                         ; preds = %for.end30, %if.end
  %49 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %50 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %51 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %call = call i32 @noquant_count_bits(%struct.lame_internal_flags* %49, %struct.gr_info* %50, %struct.calc_noise_data_t* %51)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end31, %if.then
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

; Function Attrs: noinline nounwind optnone
define internal void @quantize_xrpow(float* %xp, i32* %pi, float %istep, %struct.gr_info* %cod_info, %struct.calc_noise_data_t* %prev_noise) #0 {
entry:
  %xp.addr = alloca float*, align 4
  %pi.addr = alloca i32*, align 4
  %istep.addr = alloca float, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %prev_noise.addr = alloca %struct.calc_noise_data_t*, align 4
  %sfb = alloca i32, align 4
  %sfbmax = alloca i32, align 4
  %j = alloca i32, align 4
  %prev_data_use = alloca i32, align 4
  %iData = alloca i32*, align 4
  %accumulate = alloca i32, align 4
  %accumulate01 = alloca i32, align 4
  %acc_iData = alloca i32*, align 4
  %acc_xp = alloca float*, align 4
  %step = alloca i32, align 4
  %l = alloca i32, align 4
  %usefullsize = alloca i32, align 4
  store float* %xp, float** %xp.addr, align 4
  store i32* %pi, i32** %pi.addr, align 4
  store float %istep, float* %istep.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store %struct.calc_noise_data_t* %prev_noise, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %accumulate, align 4
  store i32 0, i32* %accumulate01, align 4
  %0 = load i32*, i32** %pi.addr, align 4
  store i32* %0, i32** %iData, align 4
  %1 = load float*, float** %xp.addr, align 4
  store float* %1, float** %acc_xp, align 4
  %2 = load i32*, i32** %iData, align 4
  store i32* %2, i32** %acc_iData, align 4
  %3 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool = icmp ne %struct.calc_noise_data_t* %3, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 7
  %5 = load i32, i32* %global_gain, align 4
  %6 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %global_gain1 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %6, i32 0, i32 0
  %7 = load i32, i32* %global_gain1, align 4
  %cmp = icmp eq i32 %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp, %land.rhs ]
  %land.ext = zext i1 %8 to i32
  store i32 %land.ext, i32* %prev_data_use, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 9
  %10 = load i32, i32* %block_type, align 4
  %cmp2 = icmp eq i32 %10, 2
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.end
  store i32 38, i32* %sfbmax, align 4
  br label %if.end

if.else:                                          ; preds = %land.end
  store i32 21, i32* %sfbmax, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %11 = load i32, i32* %sfb, align 4
  %12 = load i32, i32* %sfbmax, align 4
  %cmp3 = icmp sle i32 %11, %12
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 -1, i32* %step, align 4
  %13 = load i32, i32* %prev_data_use, align 4
  %tobool4 = icmp ne i32 %13, 0
  br i1 %tobool4, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %14 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type5 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 9
  %15 = load i32, i32* %block_type5, align 4
  %cmp6 = icmp eq i32 %15, 0
  br i1 %cmp6, label %if.then7, label %if.end15

if.then7:                                         ; preds = %lor.lhs.false, %for.body
  %16 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain8 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 7
  %17 = load i32, i32* %global_gain8, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %18, i32 0, i32 2
  %19 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx, align 4
  %21 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %21, i32 0, i32 15
  %22 = load i32, i32* %preflag, align 4
  %tobool9 = icmp ne i32 %22, 0
  br i1 %tobool9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then7
  %23 = load i32, i32* %sfb, align 4
  %arrayidx10 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx10, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %24, %cond.true ], [ 0, %cond.false ]
  %add = add nsw i32 %20, %cond
  %25 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 16
  %26 = load i32, i32* %scalefac_scale, align 4
  %add11 = add nsw i32 %26, 1
  %shl = shl i32 %add, %add11
  %sub = sub nsw i32 %17, %shl
  %27 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %27, i32 0, i32 12
  %28 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window = getelementptr inbounds %struct.gr_info, %struct.gr_info* %28, i32 0, i32 26
  %29 = load i32, i32* %sfb, align 4
  %arrayidx12 = getelementptr inbounds [39 x i32], [39 x i32]* %window, i32 0, i32 %29
  %30 = load i32, i32* %arrayidx12, align 4
  %arrayidx13 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %30
  %31 = load i32, i32* %arrayidx13, align 4
  %mul = mul nsw i32 %31, 8
  %sub14 = sub nsw i32 %sub, %mul
  store i32 %sub14, i32* %step, align 4
  br label %if.end15

if.end15:                                         ; preds = %cond.end, %lor.lhs.false
  %32 = load i32, i32* %prev_data_use, align 4
  %tobool16 = icmp ne i32 %32, 0
  br i1 %tobool16, label %land.lhs.true, label %if.else27

land.lhs.true:                                    ; preds = %if.end15
  %33 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %step17 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %33, i32 0, i32 2
  %34 = load i32, i32* %sfb, align 4
  %arrayidx18 = getelementptr inbounds [39 x i32], [39 x i32]* %step17, i32 0, i32 %34
  %35 = load i32, i32* %arrayidx18, align 4
  %36 = load i32, i32* %step, align 4
  %cmp19 = icmp eq i32 %35, %36
  br i1 %cmp19, label %if.then20, label %if.else27

if.then20:                                        ; preds = %land.lhs.true
  %37 = load i32, i32* %accumulate, align 4
  %tobool21 = icmp ne i32 %37, 0
  br i1 %tobool21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then20
  %38 = load i32, i32* %accumulate, align 4
  %39 = load float, float* %istep.addr, align 4
  %40 = load float*, float** %acc_xp, align 4
  %41 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow(i32 %38, float %39, float* %40, i32* %41)
  store i32 0, i32* %accumulate, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %if.then20
  %42 = load i32, i32* %accumulate01, align 4
  %tobool24 = icmp ne i32 %42, 0
  br i1 %tobool24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end23
  %43 = load i32, i32* %accumulate01, align 4
  %44 = load float, float* %istep.addr, align 4
  %45 = load float*, float** %acc_xp, align 4
  %46 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow_01(i32 %43, float %44, float* %45, i32* %46)
  store i32 0, i32* %accumulate01, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end23
  br label %if.end86

if.else27:                                        ; preds = %land.lhs.true, %if.end15
  %47 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 25
  %48 = load i32, i32* %sfb, align 4
  %arrayidx28 = getelementptr inbounds [39 x i32], [39 x i32]* %width, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx28, align 4
  store i32 %49, i32* %l, align 4
  %50 = load i32, i32* %j, align 4
  %51 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width29 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %51, i32 0, i32 25
  %52 = load i32, i32* %sfb, align 4
  %arrayidx30 = getelementptr inbounds [39 x i32], [39 x i32]* %width29, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx30, align 4
  %add31 = add nsw i32 %50, %53
  %54 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %54, i32 0, i32 30
  %55 = load i32, i32* %max_nonzero_coeff, align 4
  %cmp32 = icmp sgt i32 %add31, %55
  br i1 %cmp32, label %if.then33, label %if.end46

if.then33:                                        ; preds = %if.else27
  %56 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff34 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %56, i32 0, i32 30
  %57 = load i32, i32* %max_nonzero_coeff34, align 4
  %58 = load i32, i32* %j, align 4
  %sub35 = sub nsw i32 %57, %58
  %add36 = add nsw i32 %sub35, 1
  store i32 %add36, i32* %usefullsize, align 4
  %59 = load i32*, i32** %pi.addr, align 4
  %60 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff37 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %60, i32 0, i32 30
  %61 = load i32, i32* %max_nonzero_coeff37, align 4
  %arrayidx38 = getelementptr inbounds i32, i32* %59, i32 %61
  %62 = bitcast i32* %arrayidx38 to i8*
  %63 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff39 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %63, i32 0, i32 30
  %64 = load i32, i32* %max_nonzero_coeff39, align 4
  %sub40 = sub nsw i32 576, %64
  %mul41 = mul i32 4, %sub40
  call void @llvm.memset.p0i8.i32(i8* align 4 %62, i8 0, i32 %mul41, i1 false)
  %65 = load i32, i32* %usefullsize, align 4
  store i32 %65, i32* %l, align 4
  %66 = load i32, i32* %l, align 4
  %cmp42 = icmp slt i32 %66, 0
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.then33
  store i32 0, i32* %l, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then43, %if.then33
  %67 = load i32, i32* %sfbmax, align 4
  %add45 = add nsw i32 %67, 1
  store i32 %add45, i32* %sfb, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.end44, %if.else27
  %68 = load i32, i32* %accumulate, align 4
  %tobool47 = icmp ne i32 %68, 0
  br i1 %tobool47, label %if.end51, label %land.lhs.true48

land.lhs.true48:                                  ; preds = %if.end46
  %69 = load i32, i32* %accumulate01, align 4
  %tobool49 = icmp ne i32 %69, 0
  br i1 %tobool49, label %if.end51, label %if.then50

if.then50:                                        ; preds = %land.lhs.true48
  %70 = load i32*, i32** %iData, align 4
  store i32* %70, i32** %acc_iData, align 4
  %71 = load float*, float** %xp.addr, align 4
  store float* %71, float** %acc_xp, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %land.lhs.true48, %if.end46
  %72 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool52 = icmp ne %struct.calc_noise_data_t* %72, null
  br i1 %tobool52, label %land.lhs.true53, label %if.else71

land.lhs.true53:                                  ; preds = %if.end51
  %73 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %sfb_count1 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %73, i32 0, i32 1
  %74 = load i32, i32* %sfb_count1, align 4
  %cmp54 = icmp sgt i32 %74, 0
  br i1 %cmp54, label %land.lhs.true55, label %if.else71

land.lhs.true55:                                  ; preds = %land.lhs.true53
  %75 = load i32, i32* %sfb, align 4
  %76 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %sfb_count156 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %76, i32 0, i32 1
  %77 = load i32, i32* %sfb_count156, align 4
  %cmp57 = icmp sge i32 %75, %77
  br i1 %cmp57, label %land.lhs.true58, label %if.else71

land.lhs.true58:                                  ; preds = %land.lhs.true55
  %78 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %step59 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %78, i32 0, i32 2
  %79 = load i32, i32* %sfb, align 4
  %arrayidx60 = getelementptr inbounds [39 x i32], [39 x i32]* %step59, i32 0, i32 %79
  %80 = load i32, i32* %arrayidx60, align 4
  %cmp61 = icmp sgt i32 %80, 0
  br i1 %cmp61, label %land.lhs.true62, label %if.else71

land.lhs.true62:                                  ; preds = %land.lhs.true58
  %81 = load i32, i32* %step, align 4
  %82 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %step63 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %82, i32 0, i32 2
  %83 = load i32, i32* %sfb, align 4
  %arrayidx64 = getelementptr inbounds [39 x i32], [39 x i32]* %step63, i32 0, i32 %83
  %84 = load i32, i32* %arrayidx64, align 4
  %cmp65 = icmp sge i32 %81, %84
  br i1 %cmp65, label %if.then66, label %if.else71

if.then66:                                        ; preds = %land.lhs.true62
  %85 = load i32, i32* %accumulate, align 4
  %tobool67 = icmp ne i32 %85, 0
  br i1 %tobool67, label %if.then68, label %if.end69

if.then68:                                        ; preds = %if.then66
  %86 = load i32, i32* %accumulate, align 4
  %87 = load float, float* %istep.addr, align 4
  %88 = load float*, float** %acc_xp, align 4
  %89 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow(i32 %86, float %87, float* %88, i32* %89)
  store i32 0, i32* %accumulate, align 4
  %90 = load i32*, i32** %iData, align 4
  store i32* %90, i32** %acc_iData, align 4
  %91 = load float*, float** %xp.addr, align 4
  store float* %91, float** %acc_xp, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.then68, %if.then66
  %92 = load i32, i32* %l, align 4
  %93 = load i32, i32* %accumulate01, align 4
  %add70 = add nsw i32 %93, %92
  store i32 %add70, i32* %accumulate01, align 4
  br label %if.end76

if.else71:                                        ; preds = %land.lhs.true62, %land.lhs.true58, %land.lhs.true55, %land.lhs.true53, %if.end51
  %94 = load i32, i32* %accumulate01, align 4
  %tobool72 = icmp ne i32 %94, 0
  br i1 %tobool72, label %if.then73, label %if.end74

if.then73:                                        ; preds = %if.else71
  %95 = load i32, i32* %accumulate01, align 4
  %96 = load float, float* %istep.addr, align 4
  %97 = load float*, float** %acc_xp, align 4
  %98 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow_01(i32 %95, float %96, float* %97, i32* %98)
  store i32 0, i32* %accumulate01, align 4
  %99 = load i32*, i32** %iData, align 4
  store i32* %99, i32** %acc_iData, align 4
  %100 = load float*, float** %xp.addr, align 4
  store float* %100, float** %acc_xp, align 4
  br label %if.end74

if.end74:                                         ; preds = %if.then73, %if.else71
  %101 = load i32, i32* %l, align 4
  %102 = load i32, i32* %accumulate, align 4
  %add75 = add nsw i32 %102, %101
  store i32 %add75, i32* %accumulate, align 4
  br label %if.end76

if.end76:                                         ; preds = %if.end74, %if.end69
  %103 = load i32, i32* %l, align 4
  %cmp77 = icmp sle i32 %103, 0
  br i1 %cmp77, label %if.then78, label %if.end85

if.then78:                                        ; preds = %if.end76
  %104 = load i32, i32* %accumulate01, align 4
  %tobool79 = icmp ne i32 %104, 0
  br i1 %tobool79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %if.then78
  %105 = load i32, i32* %accumulate01, align 4
  %106 = load float, float* %istep.addr, align 4
  %107 = load float*, float** %acc_xp, align 4
  %108 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow_01(i32 %105, float %106, float* %107, i32* %108)
  store i32 0, i32* %accumulate01, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %if.then78
  %109 = load i32, i32* %accumulate, align 4
  %tobool82 = icmp ne i32 %109, 0
  br i1 %tobool82, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end81
  %110 = load i32, i32* %accumulate, align 4
  %111 = load float, float* %istep.addr, align 4
  %112 = load float*, float** %acc_xp, align 4
  %113 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow(i32 %110, float %111, float* %112, i32* %113)
  store i32 0, i32* %accumulate, align 4
  br label %if.end84

if.end84:                                         ; preds = %if.then83, %if.end81
  br label %for.end

if.end85:                                         ; preds = %if.end76
  br label %if.end86

if.end86:                                         ; preds = %if.end85, %if.end26
  %114 = load i32, i32* %sfb, align 4
  %115 = load i32, i32* %sfbmax, align 4
  %cmp87 = icmp sle i32 %114, %115
  br i1 %cmp87, label %if.then88, label %if.end97

if.then88:                                        ; preds = %if.end86
  %116 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width89 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %116, i32 0, i32 25
  %117 = load i32, i32* %sfb, align 4
  %arrayidx90 = getelementptr inbounds [39 x i32], [39 x i32]* %width89, i32 0, i32 %117
  %118 = load i32, i32* %arrayidx90, align 4
  %119 = load i32*, i32** %iData, align 4
  %add.ptr = getelementptr inbounds i32, i32* %119, i32 %118
  store i32* %add.ptr, i32** %iData, align 4
  %120 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width91 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %120, i32 0, i32 25
  %121 = load i32, i32* %sfb, align 4
  %arrayidx92 = getelementptr inbounds [39 x i32], [39 x i32]* %width91, i32 0, i32 %121
  %122 = load i32, i32* %arrayidx92, align 4
  %123 = load float*, float** %xp.addr, align 4
  %add.ptr93 = getelementptr inbounds float, float* %123, i32 %122
  store float* %add.ptr93, float** %xp.addr, align 4
  %124 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width94 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %124, i32 0, i32 25
  %125 = load i32, i32* %sfb, align 4
  %arrayidx95 = getelementptr inbounds [39 x i32], [39 x i32]* %width94, i32 0, i32 %125
  %126 = load i32, i32* %arrayidx95, align 4
  %127 = load i32, i32* %j, align 4
  %add96 = add nsw i32 %127, %126
  store i32 %add96, i32* %j, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then88, %if.end86
  br label %for.inc

for.inc:                                          ; preds = %if.end97
  %128 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %128, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %if.end84, %for.cond
  %129 = load i32, i32* %accumulate, align 4
  %tobool98 = icmp ne i32 %129, 0
  br i1 %tobool98, label %if.then99, label %if.end100

if.then99:                                        ; preds = %for.end
  %130 = load i32, i32* %accumulate, align 4
  %131 = load float, float* %istep.addr, align 4
  %132 = load float*, float** %acc_xp, align 4
  %133 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow(i32 %130, float %131, float* %132, i32* %133)
  store i32 0, i32* %accumulate, align 4
  br label %if.end100

if.end100:                                        ; preds = %if.then99, %for.end
  %134 = load i32, i32* %accumulate01, align 4
  %tobool101 = icmp ne i32 %134, 0
  br i1 %tobool101, label %if.then102, label %if.end103

if.then102:                                       ; preds = %if.end100
  %135 = load i32, i32* %accumulate01, align 4
  %136 = load float, float* %istep.addr, align 4
  %137 = load float*, float** %acc_xp, align 4
  %138 = load i32*, i32** %acc_iData, align 4
  call void @quantize_lines_xrpow_01(i32 %135, float %136, float* %137, i32* %138)
  store i32 0, i32* %accumulate01, align 4
  br label %if.end103

if.end103:                                        ; preds = %if.then102, %if.end100
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @recalc_divide_init(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, i32* %ix, i32* %r01_bits, i32* %r01_div, i32* %r0_tbl, i32* %r1_tbl) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %ix.addr = alloca i32*, align 4
  %r01_bits.addr = alloca i32*, align 4
  %r01_div.addr = alloca i32*, align 4
  %r0_tbl.addr = alloca i32*, align 4
  %r1_tbl.addr = alloca i32*, align 4
  %r0 = alloca i32, align 4
  %r1 = alloca i32, align 4
  %bigv = alloca i32, align 4
  %r0t = alloca i32, align 4
  %r1t = alloca i32, align 4
  %bits = alloca i32, align 4
  %a1 = alloca i32, align 4
  %r0bits = alloca i32, align 4
  %a2 = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %r01_bits, i32** %r01_bits.addr, align 4
  store i32* %r01_div, i32** %r01_div.addr, align 4
  store i32* %r0_tbl, i32** %r0_tbl.addr, align 4
  store i32* %r1_tbl, i32** %r1_tbl.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 5
  %1 = load i32, i32* %big_values, align 4
  store i32 %1, i32* %bigv, align 4
  store i32 0, i32* %r0, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %r0, align 4
  %cmp = icmp sle i32 %2, 22
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %r01_bits.addr, align 4
  %4 = load i32, i32* %r0, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  store i32 100000, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %r0, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %r0, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %r0, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc37, %for.end
  %6 = load i32, i32* %r0, align 4
  %cmp2 = icmp slt i32 %6, 16
  br i1 %cmp2, label %for.body3, label %for.end39

for.body3:                                        ; preds = %for.cond1
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %8 = load i32, i32* %r0, align 4
  %add = add nsw i32 %8, 1
  %arrayidx4 = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %add
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %a1, align 4
  %10 = load i32, i32* %a1, align 4
  %11 = load i32, i32* %bigv, align 4
  %cmp5 = icmp sge i32 %10, %11
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body3
  br label %for.end39

if.end:                                           ; preds = %for.body3
  store i32 0, i32* %r0bits, align 4
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 25
  %13 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table, align 8
  %14 = load i32*, i32** %ix.addr, align 4
  %15 = load i32*, i32** %ix.addr, align 4
  %16 = load i32, i32* %a1, align 4
  %add.ptr = getelementptr inbounds i32, i32* %15, i32 %16
  %call = call i32 %13(i32* %14, i32* %add.ptr, i32* %r0bits)
  store i32 %call, i32* %r0t, align 4
  store i32 0, i32* %r1, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc34, %if.end
  %17 = load i32, i32* %r1, align 4
  %cmp7 = icmp slt i32 %17, 8
  br i1 %cmp7, label %for.body8, label %for.end36

for.body8:                                        ; preds = %for.cond6
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band9 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 8
  %l10 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band9, i32 0, i32 0
  %19 = load i32, i32* %r0, align 4
  %20 = load i32, i32* %r1, align 4
  %add11 = add nsw i32 %19, %20
  %add12 = add nsw i32 %add11, 2
  %arrayidx13 = getelementptr inbounds [23 x i32], [23 x i32]* %l10, i32 0, i32 %add12
  %21 = load i32, i32* %arrayidx13, align 4
  store i32 %21, i32* %a2, align 4
  %22 = load i32, i32* %a2, align 4
  %23 = load i32, i32* %bigv, align 4
  %cmp14 = icmp sge i32 %22, %23
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %for.body8
  br label %for.end36

if.end16:                                         ; preds = %for.body8
  %24 = load i32, i32* %r0bits, align 4
  store i32 %24, i32* %bits, align 4
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 25
  %26 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table17, align 8
  %27 = load i32*, i32** %ix.addr, align 4
  %28 = load i32, i32* %a1, align 4
  %add.ptr18 = getelementptr inbounds i32, i32* %27, i32 %28
  %29 = load i32*, i32** %ix.addr, align 4
  %30 = load i32, i32* %a2, align 4
  %add.ptr19 = getelementptr inbounds i32, i32* %29, i32 %30
  %call20 = call i32 %26(i32* %add.ptr18, i32* %add.ptr19, i32* %bits)
  store i32 %call20, i32* %r1t, align 4
  %31 = load i32*, i32** %r01_bits.addr, align 4
  %32 = load i32, i32* %r0, align 4
  %33 = load i32, i32* %r1, align 4
  %add21 = add nsw i32 %32, %33
  %arrayidx22 = getelementptr inbounds i32, i32* %31, i32 %add21
  %34 = load i32, i32* %arrayidx22, align 4
  %35 = load i32, i32* %bits, align 4
  %cmp23 = icmp sgt i32 %34, %35
  br i1 %cmp23, label %if.then24, label %if.end33

if.then24:                                        ; preds = %if.end16
  %36 = load i32, i32* %bits, align 4
  %37 = load i32*, i32** %r01_bits.addr, align 4
  %38 = load i32, i32* %r0, align 4
  %39 = load i32, i32* %r1, align 4
  %add25 = add nsw i32 %38, %39
  %arrayidx26 = getelementptr inbounds i32, i32* %37, i32 %add25
  store i32 %36, i32* %arrayidx26, align 4
  %40 = load i32, i32* %r0, align 4
  %41 = load i32*, i32** %r01_div.addr, align 4
  %42 = load i32, i32* %r0, align 4
  %43 = load i32, i32* %r1, align 4
  %add27 = add nsw i32 %42, %43
  %arrayidx28 = getelementptr inbounds i32, i32* %41, i32 %add27
  store i32 %40, i32* %arrayidx28, align 4
  %44 = load i32, i32* %r0t, align 4
  %45 = load i32*, i32** %r0_tbl.addr, align 4
  %46 = load i32, i32* %r0, align 4
  %47 = load i32, i32* %r1, align 4
  %add29 = add nsw i32 %46, %47
  %arrayidx30 = getelementptr inbounds i32, i32* %45, i32 %add29
  store i32 %44, i32* %arrayidx30, align 4
  %48 = load i32, i32* %r1t, align 4
  %49 = load i32*, i32** %r1_tbl.addr, align 4
  %50 = load i32, i32* %r0, align 4
  %51 = load i32, i32* %r1, align 4
  %add31 = add nsw i32 %50, %51
  %arrayidx32 = getelementptr inbounds i32, i32* %49, i32 %add31
  store i32 %48, i32* %arrayidx32, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.then24, %if.end16
  br label %for.inc34

for.inc34:                                        ; preds = %if.end33
  %52 = load i32, i32* %r1, align 4
  %inc35 = add nsw i32 %52, 1
  store i32 %inc35, i32* %r1, align 4
  br label %for.cond6

for.end36:                                        ; preds = %if.then15, %for.cond6
  br label %for.inc37

for.inc37:                                        ; preds = %for.end36
  %53 = load i32, i32* %r0, align 4
  %inc38 = add nsw i32 %53, 1
  store i32 %inc38, i32* %r0, align 4
  br label %for.cond1

for.end39:                                        ; preds = %if.then, %for.cond1
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @recalc_divide_sub(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info2, %struct.gr_info* %gi, i32* %ix, i32* %r01_bits, i32* %r01_div, i32* %r0_tbl, i32* %r1_tbl) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info2.addr = alloca %struct.gr_info*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %ix.addr = alloca i32*, align 4
  %r01_bits.addr = alloca i32*, align 4
  %r01_div.addr = alloca i32*, align 4
  %r0_tbl.addr = alloca i32*, align 4
  %r1_tbl.addr = alloca i32*, align 4
  %bits = alloca i32, align 4
  %r2 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %bigv = alloca i32, align 4
  %r2t = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info2, %struct.gr_info** %cod_info2.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %r01_bits, i32** %r01_bits.addr, align 4
  store i32* %r01_div, i32** %r01_div.addr, align 4
  store i32* %r0_tbl, i32** %r0_tbl.addr, align 4
  store i32* %r1_tbl, i32** %r1_tbl.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info2.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 5
  %1 = load i32, i32* %big_values, align 4
  store i32 %1, i32* %bigv, align 4
  store i32 2, i32* %r2, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %r2, align 4
  %cmp = icmp slt i32 %2, 23
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %4 = load i32, i32* %r2, align 4
  %arrayidx = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  store i32 %5, i32* %a2, align 4
  %6 = load i32, i32* %a2, align 4
  %7 = load i32, i32* %bigv, align 4
  %cmp1 = icmp sge i32 %6, %7
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  %8 = load i32*, i32** %r01_bits.addr, align 4
  %9 = load i32, i32* %r2, align 4
  %sub = sub nsw i32 %9, 2
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %sub
  %10 = load i32, i32* %arrayidx2, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info2.addr, align 4
  %count1bits = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 27
  %12 = load i32, i32* %count1bits, align 4
  %add = add nsw i32 %10, %12
  store i32 %add, i32* %bits, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 4
  %14 = load i32, i32* %part2_3_length, align 4
  %15 = load i32, i32* %bits, align 4
  %cmp3 = icmp sle i32 %14, %15
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  br label %for.end

if.end5:                                          ; preds = %if.end
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 25
  %17 = load i32 (i32*, i32*, i32*)*, i32 (i32*, i32*, i32*)** %choose_table, align 8
  %18 = load i32*, i32** %ix.addr, align 4
  %19 = load i32, i32* %a2, align 4
  %add.ptr = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = load i32*, i32** %ix.addr, align 4
  %21 = load i32, i32* %bigv, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %20, i32 %21
  %call = call i32 %17(i32* %add.ptr, i32* %add.ptr6, i32* %bits)
  store i32 %call, i32* %r2t, align 4
  %22 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length7 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %22, i32 0, i32 4
  %23 = load i32, i32* %part2_3_length7, align 4
  %24 = load i32, i32* %bits, align 4
  %cmp8 = icmp sle i32 %23, %24
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end5
  br label %for.inc

if.end10:                                         ; preds = %if.end5
  %25 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %26 = bitcast %struct.gr_info* %25 to i8*
  %27 = load %struct.gr_info*, %struct.gr_info** %cod_info2.addr, align 4
  %28 = bitcast %struct.gr_info* %27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %28, i32 5252, i1 false)
  %29 = load i32, i32* %bits, align 4
  %30 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length11 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 4
  store i32 %29, i32* %part2_3_length11, align 4
  %31 = load i32*, i32** %r01_div.addr, align 4
  %32 = load i32, i32* %r2, align 4
  %sub12 = sub nsw i32 %32, 2
  %arrayidx13 = getelementptr inbounds i32, i32* %31, i32 %sub12
  %33 = load i32, i32* %arrayidx13, align 4
  %34 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region0_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %34, i32 0, i32 13
  store i32 %33, i32* %region0_count, align 4
  %35 = load i32, i32* %r2, align 4
  %sub14 = sub nsw i32 %35, 2
  %36 = load i32*, i32** %r01_div.addr, align 4
  %37 = load i32, i32* %r2, align 4
  %sub15 = sub nsw i32 %37, 2
  %arrayidx16 = getelementptr inbounds i32, i32* %36, i32 %sub15
  %38 = load i32, i32* %arrayidx16, align 4
  %sub17 = sub nsw i32 %sub14, %38
  %39 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region1_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %39, i32 0, i32 14
  store i32 %sub17, i32* %region1_count, align 4
  %40 = load i32*, i32** %r0_tbl.addr, align 4
  %41 = load i32, i32* %r2, align 4
  %sub18 = sub nsw i32 %41, 2
  %arrayidx19 = getelementptr inbounds i32, i32* %40, i32 %sub18
  %42 = load i32, i32* %arrayidx19, align 4
  %43 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %43, i32 0, i32 11
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  store i32 %42, i32* %arrayidx20, align 4
  %44 = load i32*, i32** %r1_tbl.addr, align 4
  %45 = load i32, i32* %r2, align 4
  %sub21 = sub nsw i32 %45, 2
  %arrayidx22 = getelementptr inbounds i32, i32* %44, i32 %sub21
  %46 = load i32, i32* %arrayidx22, align 4
  %47 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select23 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 11
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select23, i32 0, i32 1
  store i32 %46, i32* %arrayidx24, align 4
  %48 = load i32, i32* %r2t, align 4
  %49 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select25 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %49, i32 0, i32 11
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select25, i32 0, i32 2
  store i32 %48, i32* %arrayidx26, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end10, %if.then9
  %50 = load i32, i32* %r2, align 4
  %inc = add nsw i32 %50, 1
  store i32 %inc, i32* %r2, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then4, %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @best_scalefac_store(%struct.lame_internal_flags* %gfc, i32 %gr, i32 %ch, %struct.III_side_info_t* %l3_side) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gr.addr = alloca i32, align 4
  %ch.addr = alloca i32, align 4
  %l3_side.addr = alloca %struct.III_side_info_t*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %gi = alloca %struct.gr_info*, align 4
  %sfb = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %l = alloca i32, align 4
  %recalc = alloca i32, align 4
  %width = alloca i32, align 4
  %s = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  store %struct.III_side_info_t* %l3_side, %struct.III_side_info_t** %l3_side.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %1, i32 0, i32 0
  %2 = load i32, i32* %gr.addr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %2
  %3 = load i32, i32* %ch.addr, align 4
  %arrayidx2 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %3
  store %struct.gr_info* %arrayidx2, %struct.gr_info** %gi, align 4
  store i32 0, i32* %recalc, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc14, %entry
  %4 = load i32, i32* %sfb, align 4
  %5 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 22
  %6 = load i32, i32* %sfbmax, align 4
  %cmp = icmp slt i32 %4, %6
  br i1 %cmp, label %for.body, label %for.end16

for.body:                                         ; preds = %for.cond
  %7 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %width3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 25
  %8 = load i32, i32* %sfb, align 4
  %arrayidx4 = getelementptr inbounds [39 x i32], [39 x i32]* %width3, i32 0, i32 %8
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %width, align 4
  %10 = load i32, i32* %j, align 4
  store i32 %10, i32* %l, align 4
  %11 = load i32, i32* %width, align 4
  %12 = load i32, i32* %j, align 4
  %add = add nsw i32 %12, %11
  store i32 %add, i32* %j, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %l, align 4
  %14 = load i32, i32* %j, align 4
  %cmp6 = icmp slt i32 %13, %14
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond5
  %15 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 1
  %16 = load i32, i32* %l, align 4
  %arrayidx8 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx8, align 4
  %cmp9 = icmp ne i32 %17, 0
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body7
  br label %for.end

if.end:                                           ; preds = %for.body7
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %l, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond5

for.end:                                          ; preds = %if.then, %for.cond5
  %19 = load i32, i32* %l, align 4
  %20 = load i32, i32* %j, align 4
  %cmp10 = icmp eq i32 %19, %20
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %for.end
  store i32 -2, i32* %recalc, align 4
  %21 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %21, i32 0, i32 2
  %22 = load i32, i32* %sfb, align 4
  %arrayidx12 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %22
  store i32 -2, i32* %arrayidx12, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %for.end
  br label %for.inc14

for.inc14:                                        ; preds = %if.end13
  %23 = load i32, i32* %sfb, align 4
  %inc15 = add nsw i32 %23, 1
  store i32 %inc15, i32* %sfb, align 4
  br label %for.cond

for.end16:                                        ; preds = %for.cond
  %24 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 16
  %25 = load i32, i32* %scalefac_scale, align 4
  %tobool = icmp ne i32 %25, 0
  br i1 %tobool, label %if.end53, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.end16
  %26 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %26, i32 0, i32 15
  %27 = load i32, i32* %preflag, align 4
  %tobool17 = icmp ne i32 %27, 0
  br i1 %tobool17, label %if.end53, label %if.then18

if.then18:                                        ; preds = %land.lhs.true
  store i32 0, i32* %s, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc30, %if.then18
  %28 = load i32, i32* %sfb, align 4
  %29 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbmax20 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %29, i32 0, i32 22
  %30 = load i32, i32* %sfbmax20, align 4
  %cmp21 = icmp slt i32 %28, %30
  br i1 %cmp21, label %for.body22, label %for.end32

for.body22:                                       ; preds = %for.cond19
  %31 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac23 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %31, i32 0, i32 2
  %32 = load i32, i32* %sfb, align 4
  %arrayidx24 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac23, i32 0, i32 %32
  %33 = load i32, i32* %arrayidx24, align 4
  %cmp25 = icmp sgt i32 %33, 0
  br i1 %cmp25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %for.body22
  %34 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %34, i32 0, i32 2
  %35 = load i32, i32* %sfb, align 4
  %arrayidx28 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac27, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx28, align 4
  %37 = load i32, i32* %s, align 4
  %or = or i32 %37, %36
  store i32 %or, i32* %s, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.then26, %for.body22
  br label %for.inc30

for.inc30:                                        ; preds = %if.end29
  %38 = load i32, i32* %sfb, align 4
  %inc31 = add nsw i32 %38, 1
  store i32 %inc31, i32* %sfb, align 4
  br label %for.cond19

for.end32:                                        ; preds = %for.cond19
  %39 = load i32, i32* %s, align 4
  %and = and i32 %39, 1
  %tobool33 = icmp ne i32 %and, 0
  br i1 %tobool33, label %if.end52, label %land.lhs.true34

land.lhs.true34:                                  ; preds = %for.end32
  %40 = load i32, i32* %s, align 4
  %cmp35 = icmp ne i32 %40, 0
  br i1 %cmp35, label %if.then36, label %if.end52

if.then36:                                        ; preds = %land.lhs.true34
  store i32 0, i32* %sfb, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc48, %if.then36
  %41 = load i32, i32* %sfb, align 4
  %42 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbmax38 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %42, i32 0, i32 22
  %43 = load i32, i32* %sfbmax38, align 4
  %cmp39 = icmp slt i32 %41, %43
  br i1 %cmp39, label %for.body40, label %for.end50

for.body40:                                       ; preds = %for.cond37
  %44 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac41 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %44, i32 0, i32 2
  %45 = load i32, i32* %sfb, align 4
  %arrayidx42 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac41, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx42, align 4
  %cmp43 = icmp sgt i32 %46, 0
  br i1 %cmp43, label %if.then44, label %if.end47

if.then44:                                        ; preds = %for.body40
  %47 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac45 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 2
  %48 = load i32, i32* %sfb, align 4
  %arrayidx46 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac45, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx46, align 4
  %shr = ashr i32 %49, 1
  store i32 %shr, i32* %arrayidx46, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then44, %for.body40
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47
  %50 = load i32, i32* %sfb, align 4
  %inc49 = add nsw i32 %50, 1
  store i32 %inc49, i32* %sfb, align 4
  br label %for.cond37

for.end50:                                        ; preds = %for.cond37
  store i32 1, i32* %recalc, align 4
  %51 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_scale51 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %51, i32 0, i32 16
  store i32 1, i32* %scalefac_scale51, align 4
  br label %if.end52

if.end52:                                         ; preds = %for.end50, %land.lhs.true34, %for.end32
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %land.lhs.true, %for.end16
  %52 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %preflag54 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %52, i32 0, i32 15
  %53 = load i32, i32* %preflag54, align 4
  %tobool55 = icmp ne i32 %53, 0
  br i1 %tobool55, label %if.end95, label %land.lhs.true56

land.lhs.true56:                                  ; preds = %if.end53
  %54 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %54, i32 0, i32 9
  %55 = load i32, i32* %block_type, align 4
  %cmp57 = icmp ne i32 %55, 2
  br i1 %cmp57, label %land.lhs.true58, label %if.end95

land.lhs.true58:                                  ; preds = %land.lhs.true56
  %56 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %56, i32 0, i32 15
  %57 = load i32, i32* %mode_gr, align 4
  %cmp59 = icmp eq i32 %57, 2
  br i1 %cmp59, label %if.then60, label %if.end95

if.then60:                                        ; preds = %land.lhs.true58
  store i32 11, i32* %sfb, align 4
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc74, %if.then60
  %58 = load i32, i32* %sfb, align 4
  %cmp62 = icmp slt i32 %58, 21
  br i1 %cmp62, label %for.body63, label %for.end76

for.body63:                                       ; preds = %for.cond61
  %59 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac64 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %59, i32 0, i32 2
  %60 = load i32, i32* %sfb, align 4
  %arrayidx65 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac64, i32 0, i32 %60
  %61 = load i32, i32* %arrayidx65, align 4
  %62 = load i32, i32* %sfb, align 4
  %arrayidx66 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %62
  %63 = load i32, i32* %arrayidx66, align 4
  %cmp67 = icmp slt i32 %61, %63
  br i1 %cmp67, label %land.lhs.true68, label %if.end73

land.lhs.true68:                                  ; preds = %for.body63
  %64 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac69 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %64, i32 0, i32 2
  %65 = load i32, i32* %sfb, align 4
  %arrayidx70 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac69, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx70, align 4
  %cmp71 = icmp ne i32 %66, -2
  br i1 %cmp71, label %if.then72, label %if.end73

if.then72:                                        ; preds = %land.lhs.true68
  br label %for.end76

if.end73:                                         ; preds = %land.lhs.true68, %for.body63
  br label %for.inc74

for.inc74:                                        ; preds = %if.end73
  %67 = load i32, i32* %sfb, align 4
  %inc75 = add nsw i32 %67, 1
  store i32 %inc75, i32* %sfb, align 4
  br label %for.cond61

for.end76:                                        ; preds = %if.then72, %for.cond61
  %68 = load i32, i32* %sfb, align 4
  %cmp77 = icmp eq i32 %68, 21
  br i1 %cmp77, label %if.then78, label %if.end94

if.then78:                                        ; preds = %for.end76
  store i32 11, i32* %sfb, align 4
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc90, %if.then78
  %69 = load i32, i32* %sfb, align 4
  %cmp80 = icmp slt i32 %69, 21
  br i1 %cmp80, label %for.body81, label %for.end92

for.body81:                                       ; preds = %for.cond79
  %70 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac82 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %70, i32 0, i32 2
  %71 = load i32, i32* %sfb, align 4
  %arrayidx83 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac82, i32 0, i32 %71
  %72 = load i32, i32* %arrayidx83, align 4
  %cmp84 = icmp sgt i32 %72, 0
  br i1 %cmp84, label %if.then85, label %if.end89

if.then85:                                        ; preds = %for.body81
  %73 = load i32, i32* %sfb, align 4
  %arrayidx86 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %73
  %74 = load i32, i32* %arrayidx86, align 4
  %75 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac87 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %75, i32 0, i32 2
  %76 = load i32, i32* %sfb, align 4
  %arrayidx88 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac87, i32 0, i32 %76
  %77 = load i32, i32* %arrayidx88, align 4
  %sub = sub nsw i32 %77, %74
  store i32 %sub, i32* %arrayidx88, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then85, %for.body81
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89
  %78 = load i32, i32* %sfb, align 4
  %inc91 = add nsw i32 %78, 1
  store i32 %inc91, i32* %sfb, align 4
  br label %for.cond79

for.end92:                                        ; preds = %for.cond79
  store i32 1, i32* %recalc, align 4
  %79 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %preflag93 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %79, i32 0, i32 15
  store i32 1, i32* %preflag93, align 4
  br label %if.end94

if.end94:                                         ; preds = %for.end92, %for.end76
  br label %if.end95

if.end95:                                         ; preds = %if.end94, %land.lhs.true58, %land.lhs.true56, %if.end53
  store i32 0, i32* %i, align 4
  br label %for.cond96

for.cond96:                                       ; preds = %for.inc101, %if.end95
  %80 = load i32, i32* %i, align 4
  %cmp97 = icmp slt i32 %80, 4
  br i1 %cmp97, label %for.body98, label %for.end103

for.body98:                                       ; preds = %for.cond96
  %81 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %scfsi = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %81, i32 0, i32 5
  %82 = load i32, i32* %ch.addr, align 4
  %arrayidx99 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi, i32 0, i32 %82
  %83 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx99, i32 0, i32 %83
  store i32 0, i32* %arrayidx100, align 4
  br label %for.inc101

for.inc101:                                       ; preds = %for.body98
  %84 = load i32, i32* %i, align 4
  %inc102 = add nsw i32 %84, 1
  store i32 %inc102, i32* %i, align 4
  br label %for.cond96

for.end103:                                       ; preds = %for.cond96
  %85 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr104 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %85, i32 0, i32 15
  %86 = load i32, i32* %mode_gr104, align 4
  %cmp105 = icmp eq i32 %86, 2
  br i1 %cmp105, label %land.lhs.true106, label %if.end121

land.lhs.true106:                                 ; preds = %for.end103
  %87 = load i32, i32* %gr.addr, align 4
  %cmp107 = icmp eq i32 %87, 1
  br i1 %cmp107, label %land.lhs.true108, label %if.end121

land.lhs.true108:                                 ; preds = %land.lhs.true106
  %88 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt109 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %88, i32 0, i32 0
  %arrayidx110 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt109, i32 0, i32 0
  %89 = load i32, i32* %ch.addr, align 4
  %arrayidx111 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx110, i32 0, i32 %89
  %block_type112 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx111, i32 0, i32 9
  %90 = load i32, i32* %block_type112, align 4
  %cmp113 = icmp ne i32 %90, 2
  br i1 %cmp113, label %land.lhs.true114, label %if.end121

land.lhs.true114:                                 ; preds = %land.lhs.true108
  %91 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt115 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %91, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt115, i32 0, i32 1
  %92 = load i32, i32* %ch.addr, align 4
  %arrayidx117 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx116, i32 0, i32 %92
  %block_type118 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx117, i32 0, i32 9
  %93 = load i32, i32* %block_type118, align 4
  %cmp119 = icmp ne i32 %93, 2
  br i1 %cmp119, label %if.then120, label %if.end121

if.then120:                                       ; preds = %land.lhs.true114
  %94 = load i32, i32* %ch.addr, align 4
  %95 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  call void @scfsi_calc(i32 %94, %struct.III_side_info_t* %95)
  store i32 0, i32* %recalc, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then120, %land.lhs.true114, %land.lhs.true108, %land.lhs.true106, %for.end103
  store i32 0, i32* %sfb, align 4
  br label %for.cond122

for.cond122:                                      ; preds = %for.inc133, %if.end121
  %96 = load i32, i32* %sfb, align 4
  %97 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbmax123 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %97, i32 0, i32 22
  %98 = load i32, i32* %sfbmax123, align 4
  %cmp124 = icmp slt i32 %96, %98
  br i1 %cmp124, label %for.body125, label %for.end135

for.body125:                                      ; preds = %for.cond122
  %99 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac126 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %99, i32 0, i32 2
  %100 = load i32, i32* %sfb, align 4
  %arrayidx127 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac126, i32 0, i32 %100
  %101 = load i32, i32* %arrayidx127, align 4
  %cmp128 = icmp eq i32 %101, -2
  br i1 %cmp128, label %if.then129, label %if.end132

if.then129:                                       ; preds = %for.body125
  %102 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac130 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %102, i32 0, i32 2
  %103 = load i32, i32* %sfb, align 4
  %arrayidx131 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac130, i32 0, i32 %103
  store i32 0, i32* %arrayidx131, align 4
  br label %if.end132

if.end132:                                        ; preds = %if.then129, %for.body125
  br label %for.inc133

for.inc133:                                       ; preds = %if.end132
  %104 = load i32, i32* %sfb, align 4
  %inc134 = add nsw i32 %104, 1
  store i32 %inc134, i32* %sfb, align 4
  br label %for.cond122

for.end135:                                       ; preds = %for.cond122
  %105 = load i32, i32* %recalc, align 4
  %tobool136 = icmp ne i32 %105, 0
  br i1 %tobool136, label %if.then137, label %if.end138

if.then137:                                       ; preds = %for.end135
  %106 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %107 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %call = call i32 @scale_bitcount(%struct.lame_internal_flags* %106, %struct.gr_info* %107)
  br label %if.end138

if.end138:                                        ; preds = %if.then137, %for.end135
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @scfsi_calc(i32 %ch, %struct.III_side_info_t* %l3_side) #0 {
entry:
  %ch.addr = alloca i32, align 4
  %l3_side.addr = alloca %struct.III_side_info_t*, align 4
  %i = alloca i32, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %c2 = alloca i32, align 4
  %sfb = alloca i32, align 4
  %gi = alloca %struct.gr_info*, align 4
  %g0 = alloca %struct.gr_info*, align 4
  %c = alloca i32, align 4
  store i32 %ch, i32* %ch.addr, align 4
  store %struct.III_side_info_t* %l3_side, %struct.III_side_info_t** %l3_side.addr, align 4
  %0 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 1
  %1 = load i32, i32* %ch.addr, align 4
  %arrayidx1 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %1
  store %struct.gr_info* %arrayidx1, %struct.gr_info** %gi, align 4
  %2 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt2 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt2, i32 0, i32 0
  %3 = load i32, i32* %ch.addr, align 4
  %arrayidx4 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx3, i32 0, i32 %3
  store %struct.gr_info* %arrayidx4, %struct.gr_info** %g0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %4 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %4, 4
  br i1 %cmp, label %for.body, label %for.end37

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [5 x i32], [5 x i32]* @scfsi_band, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx5, align 4
  store i32 %6, i32* %sfb, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %sfb, align 4
  %8 = load i32, i32* %i, align 4
  %add = add i32 %8, 1
  %arrayidx7 = getelementptr inbounds [5 x i32], [5 x i32]* @scfsi_band, i32 0, i32 %add
  %9 = load i32, i32* %arrayidx7, align 4
  %cmp8 = icmp slt i32 %7, %9
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond6
  %10 = load %struct.gr_info*, %struct.gr_info** %g0, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %10, i32 0, i32 2
  %11 = load i32, i32* %sfb, align 4
  %arrayidx10 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx10, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac11 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 2
  %14 = load i32, i32* %sfb, align 4
  %arrayidx12 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac11, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx12, align 4
  %cmp13 = icmp ne i32 %12, %15
  br i1 %cmp13, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body9
  %16 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac14 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 2
  %17 = load i32, i32* %sfb, align 4
  %arrayidx15 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac14, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx15, align 4
  %cmp16 = icmp sge i32 %18, 0
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %for.end

if.end:                                           ; preds = %land.lhs.true, %for.body9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond6

for.end:                                          ; preds = %if.then, %for.cond6
  %20 = load i32, i32* %sfb, align 4
  %21 = load i32, i32* %i, align 4
  %add17 = add i32 %21, 1
  %arrayidx18 = getelementptr inbounds [5 x i32], [5 x i32]* @scfsi_band, i32 0, i32 %add17
  %22 = load i32, i32* %arrayidx18, align 4
  %cmp19 = icmp eq i32 %20, %22
  br i1 %cmp19, label %if.then20, label %if.end34

if.then20:                                        ; preds = %for.end
  %23 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds [5 x i32], [5 x i32]* @scfsi_band, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx21, align 4
  store i32 %24, i32* %sfb, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc29, %if.then20
  %25 = load i32, i32* %sfb, align 4
  %26 = load i32, i32* %i, align 4
  %add23 = add i32 %26, 1
  %arrayidx24 = getelementptr inbounds [5 x i32], [5 x i32]* @scfsi_band, i32 0, i32 %add23
  %27 = load i32, i32* %arrayidx24, align 4
  %cmp25 = icmp slt i32 %25, %27
  br i1 %cmp25, label %for.body26, label %for.end31

for.body26:                                       ; preds = %for.cond22
  %28 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %28, i32 0, i32 2
  %29 = load i32, i32* %sfb, align 4
  %arrayidx28 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac27, i32 0, i32 %29
  store i32 -1, i32* %arrayidx28, align 4
  br label %for.inc29

for.inc29:                                        ; preds = %for.body26
  %30 = load i32, i32* %sfb, align 4
  %inc30 = add nsw i32 %30, 1
  store i32 %inc30, i32* %sfb, align 4
  br label %for.cond22

for.end31:                                        ; preds = %for.cond22
  %31 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %scfsi = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %31, i32 0, i32 5
  %32 = load i32, i32* %ch.addr, align 4
  %arrayidx32 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi, i32 0, i32 %32
  %33 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx32, i32 0, i32 %33
  store i32 1, i32* %arrayidx33, align 4
  br label %if.end34

if.end34:                                         ; preds = %for.end31, %for.end
  br label %for.inc35

for.inc35:                                        ; preds = %if.end34
  %34 = load i32, i32* %i, align 4
  %inc36 = add i32 %34, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond

for.end37:                                        ; preds = %for.cond
  store i32 0, i32* %c1, align 4
  store i32 0, i32* %s1, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc54, %for.end37
  %35 = load i32, i32* %sfb, align 4
  %cmp39 = icmp slt i32 %35, 11
  br i1 %cmp39, label %for.body40, label %for.end56

for.body40:                                       ; preds = %for.cond38
  %36 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac41 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %36, i32 0, i32 2
  %37 = load i32, i32* %sfb, align 4
  %arrayidx42 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac41, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx42, align 4
  %cmp43 = icmp eq i32 %38, -1
  br i1 %cmp43, label %if.then44, label %if.end45

if.then44:                                        ; preds = %for.body40
  br label %for.inc54

if.end45:                                         ; preds = %for.body40
  %39 = load i32, i32* %c1, align 4
  %inc46 = add nsw i32 %39, 1
  store i32 %inc46, i32* %c1, align 4
  %40 = load i32, i32* %s1, align 4
  %41 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac47 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %41, i32 0, i32 2
  %42 = load i32, i32* %sfb, align 4
  %arrayidx48 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac47, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx48, align 4
  %cmp49 = icmp slt i32 %40, %43
  br i1 %cmp49, label %if.then50, label %if.end53

if.then50:                                        ; preds = %if.end45
  %44 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac51 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %44, i32 0, i32 2
  %45 = load i32, i32* %sfb, align 4
  %arrayidx52 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac51, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx52, align 4
  store i32 %46, i32* %s1, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.then50, %if.end45
  br label %for.inc54

for.inc54:                                        ; preds = %if.end53, %if.then44
  %47 = load i32, i32* %sfb, align 4
  %inc55 = add nsw i32 %47, 1
  store i32 %inc55, i32* %sfb, align 4
  br label %for.cond38

for.end56:                                        ; preds = %for.cond38
  store i32 0, i32* %c2, align 4
  store i32 0, i32* %s2, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc73, %for.end56
  %48 = load i32, i32* %sfb, align 4
  %cmp58 = icmp slt i32 %48, 21
  br i1 %cmp58, label %for.body59, label %for.end75

for.body59:                                       ; preds = %for.cond57
  %49 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac60 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %49, i32 0, i32 2
  %50 = load i32, i32* %sfb, align 4
  %arrayidx61 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac60, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx61, align 4
  %cmp62 = icmp eq i32 %51, -1
  br i1 %cmp62, label %if.then63, label %if.end64

if.then63:                                        ; preds = %for.body59
  br label %for.inc73

if.end64:                                         ; preds = %for.body59
  %52 = load i32, i32* %c2, align 4
  %inc65 = add nsw i32 %52, 1
  store i32 %inc65, i32* %c2, align 4
  %53 = load i32, i32* %s2, align 4
  %54 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac66 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %54, i32 0, i32 2
  %55 = load i32, i32* %sfb, align 4
  %arrayidx67 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac66, i32 0, i32 %55
  %56 = load i32, i32* %arrayidx67, align 4
  %cmp68 = icmp slt i32 %53, %56
  br i1 %cmp68, label %if.then69, label %if.end72

if.then69:                                        ; preds = %if.end64
  %57 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac70 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %57, i32 0, i32 2
  %58 = load i32, i32* %sfb, align 4
  %arrayidx71 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac70, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx71, align 4
  store i32 %59, i32* %s2, align 4
  br label %if.end72

if.end72:                                         ; preds = %if.then69, %if.end64
  br label %for.inc73

for.inc73:                                        ; preds = %if.end72, %if.then63
  %60 = load i32, i32* %sfb, align 4
  %inc74 = add nsw i32 %60, 1
  store i32 %inc74, i32* %sfb, align 4
  br label %for.cond57

for.end75:                                        ; preds = %for.cond57
  store i32 0, i32* %i, align 4
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc94, %for.end75
  %61 = load i32, i32* %i, align 4
  %cmp77 = icmp ult i32 %61, 16
  br i1 %cmp77, label %for.body78, label %for.end96

for.body78:                                       ; preds = %for.cond76
  %62 = load i32, i32* %s1, align 4
  %63 = load i32, i32* %i, align 4
  %arrayidx79 = getelementptr inbounds [16 x i32], [16 x i32]* @slen1_n, i32 0, i32 %63
  %64 = load i32, i32* %arrayidx79, align 4
  %cmp80 = icmp slt i32 %62, %64
  br i1 %cmp80, label %land.lhs.true81, label %if.end93

land.lhs.true81:                                  ; preds = %for.body78
  %65 = load i32, i32* %s2, align 4
  %66 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [16 x i32], [16 x i32]* @slen2_n, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx82, align 4
  %cmp83 = icmp slt i32 %65, %67
  br i1 %cmp83, label %if.then84, label %if.end93

if.then84:                                        ; preds = %land.lhs.true81
  %68 = load i32, i32* %i, align 4
  %arrayidx85 = getelementptr inbounds [16 x i32], [16 x i32]* @slen1_tab, i32 0, i32 %68
  %69 = load i32, i32* %arrayidx85, align 4
  %70 = load i32, i32* %c1, align 4
  %mul = mul nsw i32 %69, %70
  %71 = load i32, i32* %i, align 4
  %arrayidx86 = getelementptr inbounds [16 x i32], [16 x i32]* @slen2_tab, i32 0, i32 %71
  %72 = load i32, i32* %arrayidx86, align 4
  %73 = load i32, i32* %c2, align 4
  %mul87 = mul nsw i32 %72, %73
  %add88 = add nsw i32 %mul, %mul87
  store i32 %add88, i32* %c, align 4
  %74 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %74, i32 0, i32 18
  %75 = load i32, i32* %part2_length, align 4
  %76 = load i32, i32* %c, align 4
  %cmp89 = icmp sgt i32 %75, %76
  br i1 %cmp89, label %if.then90, label %if.end92

if.then90:                                        ; preds = %if.then84
  %77 = load i32, i32* %c, align 4
  %78 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %part2_length91 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %78, i32 0, i32 18
  store i32 %77, i32* %part2_length91, align 4
  %79 = load i32, i32* %i, align 4
  %80 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %80, i32 0, i32 8
  store i32 %79, i32* %scalefac_compress, align 4
  br label %if.end92

if.end92:                                         ; preds = %if.then90, %if.then84
  br label %if.end93

if.end93:                                         ; preds = %if.end92, %land.lhs.true81, %for.body78
  br label %for.inc94

for.inc94:                                        ; preds = %if.end93
  %81 = load i32, i32* %i, align 4
  %inc95 = add i32 %81, 1
  store i32 %inc95, i32* %i, align 4
  br label %for.cond76

for.end96:                                        ; preds = %for.cond76
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @scale_bitcount(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 15
  %1 = load i32, i32* %mode_gr, align 4
  %cmp = icmp eq i32 %1, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call = call i32 @mpeg1_scale_bitcount(%struct.lame_internal_flags* %2, %struct.gr_info* %3)
  store i32 %call, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %5 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call1 = call i32 @mpeg2_scale_bitcount(%struct.lame_internal_flags* %4, %struct.gr_info* %5)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @mpeg1_scale_bitcount(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %k = alloca i32, align 4
  %sfb = alloca i32, align 4
  %max_slen1 = alloca i32, align 4
  %max_slen2 = alloca i32, align 4
  %tab = alloca i32*, align 4
  %scalefac = alloca i32*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32 0, i32* %max_slen1, align 4
  store i32 0, i32* %max_slen2, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac1, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 9
  %3 = load i32, i32* %block_type, align 4
  %cmp = icmp eq i32 %3, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32* getelementptr inbounds ([16 x i32], [16 x i32]* @scale_short, i32 0, i32 0), i32** %tab, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 10
  %5 = load i32, i32* %mixed_block_flag, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  store i32* getelementptr inbounds ([16 x i32], [16 x i32]* @scale_mixed, i32 0, i32 0), i32** %tab, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end23

if.else:                                          ; preds = %entry
  store i32* getelementptr inbounds ([16 x i32], [16 x i32]* @scale_long, i32 0, i32 0), i32** %tab, align 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 15
  %7 = load i32, i32* %preflag, align 4
  %tobool3 = icmp ne i32 %7, 0
  br i1 %tobool3, label %if.end22, label %if.then4

if.then4:                                         ; preds = %if.else
  store i32 11, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then4
  %8 = load i32, i32* %sfb, align 4
  %cmp5 = icmp slt i32 %8, 21
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i32*, i32** %scalefac, align 4
  %10 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx, align 4
  %12 = load i32, i32* %sfb, align 4
  %arrayidx6 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx6, align 4
  %cmp7 = icmp slt i32 %11, %13
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %for.body
  br label %for.end

if.end9:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %14 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  %15 = load i32, i32* %sfb, align 4
  %cmp10 = icmp eq i32 %15, 21
  br i1 %cmp10, label %if.then11, label %if.end21

if.then11:                                        ; preds = %for.end
  %16 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag12 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 15
  store i32 1, i32* %preflag12, align 4
  store i32 11, i32* %sfb, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc18, %if.then11
  %17 = load i32, i32* %sfb, align 4
  %cmp14 = icmp slt i32 %17, 21
  br i1 %cmp14, label %for.body15, label %for.end20

for.body15:                                       ; preds = %for.cond13
  %18 = load i32, i32* %sfb, align 4
  %arrayidx16 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx16, align 4
  %20 = load i32*, i32** %scalefac, align 4
  %21 = load i32, i32* %sfb, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %20, i32 %21
  %22 = load i32, i32* %arrayidx17, align 4
  %sub = sub nsw i32 %22, %19
  store i32 %sub, i32* %arrayidx17, align 4
  br label %for.inc18

for.inc18:                                        ; preds = %for.body15
  %23 = load i32, i32* %sfb, align 4
  %inc19 = add nsw i32 %23, 1
  store i32 %inc19, i32* %sfb, align 4
  br label %for.cond13

for.end20:                                        ; preds = %for.cond13
  br label %if.end21

if.end21:                                         ; preds = %for.end20, %for.end
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.else
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end
  store i32 0, i32* %sfb, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc32, %if.end23
  %24 = load i32, i32* %sfb, align 4
  %25 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbdivide = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 24
  %26 = load i32, i32* %sfbdivide, align 4
  %cmp25 = icmp slt i32 %24, %26
  br i1 %cmp25, label %for.body26, label %for.end34

for.body26:                                       ; preds = %for.cond24
  %27 = load i32, i32* %max_slen1, align 4
  %28 = load i32*, i32** %scalefac, align 4
  %29 = load i32, i32* %sfb, align 4
  %arrayidx27 = getelementptr inbounds i32, i32* %28, i32 %29
  %30 = load i32, i32* %arrayidx27, align 4
  %cmp28 = icmp slt i32 %27, %30
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.body26
  %31 = load i32*, i32** %scalefac, align 4
  %32 = load i32, i32* %sfb, align 4
  %arrayidx30 = getelementptr inbounds i32, i32* %31, i32 %32
  %33 = load i32, i32* %arrayidx30, align 4
  store i32 %33, i32* %max_slen1, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %for.body26
  br label %for.inc32

for.inc32:                                        ; preds = %if.end31
  %34 = load i32, i32* %sfb, align 4
  %inc33 = add nsw i32 %34, 1
  store i32 %inc33, i32* %sfb, align 4
  br label %for.cond24

for.end34:                                        ; preds = %for.cond24
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc43, %for.end34
  %35 = load i32, i32* %sfb, align 4
  %36 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %36, i32 0, i32 22
  %37 = load i32, i32* %sfbmax, align 4
  %cmp36 = icmp slt i32 %35, %37
  br i1 %cmp36, label %for.body37, label %for.end45

for.body37:                                       ; preds = %for.cond35
  %38 = load i32, i32* %max_slen2, align 4
  %39 = load i32*, i32** %scalefac, align 4
  %40 = load i32, i32* %sfb, align 4
  %arrayidx38 = getelementptr inbounds i32, i32* %39, i32 %40
  %41 = load i32, i32* %arrayidx38, align 4
  %cmp39 = icmp slt i32 %38, %41
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %for.body37
  %42 = load i32*, i32** %scalefac, align 4
  %43 = load i32, i32* %sfb, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %42, i32 %43
  %44 = load i32, i32* %arrayidx41, align 4
  store i32 %44, i32* %max_slen2, align 4
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %for.body37
  br label %for.inc43

for.inc43:                                        ; preds = %if.end42
  %45 = load i32, i32* %sfb, align 4
  %inc44 = add nsw i32 %45, 1
  store i32 %inc44, i32* %sfb, align 4
  br label %for.cond35

for.end45:                                        ; preds = %for.cond35
  %46 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %46, i32 0, i32 18
  store i32 100000, i32* %part2_length, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc61, %for.end45
  %47 = load i32, i32* %k, align 4
  %cmp47 = icmp slt i32 %47, 16
  br i1 %cmp47, label %for.body48, label %for.end63

for.body48:                                       ; preds = %for.cond46
  %48 = load i32, i32* %max_slen1, align 4
  %49 = load i32, i32* %k, align 4
  %arrayidx49 = getelementptr inbounds [16 x i32], [16 x i32]* @slen1_n, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx49, align 4
  %cmp50 = icmp slt i32 %48, %50
  br i1 %cmp50, label %land.lhs.true, label %if.end60

land.lhs.true:                                    ; preds = %for.body48
  %51 = load i32, i32* %max_slen2, align 4
  %52 = load i32, i32* %k, align 4
  %arrayidx51 = getelementptr inbounds [16 x i32], [16 x i32]* @slen2_n, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx51, align 4
  %cmp52 = icmp slt i32 %51, %53
  br i1 %cmp52, label %land.lhs.true53, label %if.end60

land.lhs.true53:                                  ; preds = %land.lhs.true
  %54 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length54 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %54, i32 0, i32 18
  %55 = load i32, i32* %part2_length54, align 4
  %56 = load i32*, i32** %tab, align 4
  %57 = load i32, i32* %k, align 4
  %arrayidx55 = getelementptr inbounds i32, i32* %56, i32 %57
  %58 = load i32, i32* %arrayidx55, align 4
  %cmp56 = icmp sgt i32 %55, %58
  br i1 %cmp56, label %if.then57, label %if.end60

if.then57:                                        ; preds = %land.lhs.true53
  %59 = load i32*, i32** %tab, align 4
  %60 = load i32, i32* %k, align 4
  %arrayidx58 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx58, align 4
  %62 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length59 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %62, i32 0, i32 18
  store i32 %61, i32* %part2_length59, align 4
  %63 = load i32, i32* %k, align 4
  %64 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %64, i32 0, i32 8
  store i32 %63, i32* %scalefac_compress, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then57, %land.lhs.true53, %land.lhs.true, %for.body48
  br label %for.inc61

for.inc61:                                        ; preds = %if.end60
  %65 = load i32, i32* %k, align 4
  %inc62 = add nsw i32 %65, 1
  store i32 %inc62, i32* %k, align 4
  br label %for.cond46

for.end63:                                        ; preds = %for.cond46
  %66 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length64 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %66, i32 0, i32 18
  %67 = load i32, i32* %part2_length64, align 4
  %cmp65 = icmp eq i32 %67, 100000
  %conv = zext i1 %cmp65 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define internal i32 @mpeg2_scale_bitcount(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %table_number = alloca i32, align 4
  %row_in_table = alloca i32, align 4
  %partition = alloca i32, align 4
  %nr_sfb = alloca i32, align 4
  %window = alloca i32, align 4
  %over = alloca i32, align 4
  %i = alloca i32, align 4
  %sfb = alloca i32, align 4
  %max_sfac = alloca [4 x i32], align 16
  %partition_table = alloca i32*, align 4
  %scalefac = alloca i32*, align 4
  %slen1 = alloca i32, align 4
  %slen2 = alloca i32, align 4
  %slen3 = alloca i32, align 4
  %slen4 = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac1, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 15
  %2 = load i32, i32* %preflag, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 2, i32* %table_number, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %table_number, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %3, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %4
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 9
  %7 = load i32, i32* %block_type, align 4
  %cmp2 = icmp eq i32 %7, 2
  br i1 %cmp2, label %if.then3, label %if.else36

if.then3:                                         ; preds = %for.end
  store i32 1, i32* %row_in_table, align 4
  %8 = load i32, i32* %table_number, align 4
  %arrayidx4 = getelementptr inbounds [6 x [3 x [4 x i32]]], [6 x [3 x [4 x i32]]]* @nr_of_sfb_block, i32 0, i32 %8
  %9 = load i32, i32* %row_in_table, align 4
  %arrayidx5 = getelementptr inbounds [3 x [4 x i32]], [3 x [4 x i32]]* %arrayidx4, i32 0, i32 %9
  %arrayidx6 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx5, i32 0, i32 0
  store i32* %arrayidx6, i32** %partition_table, align 4
  store i32 0, i32* %sfb, align 4
  store i32 0, i32* %partition, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc33, %if.then3
  %10 = load i32, i32* %partition, align 4
  %cmp8 = icmp slt i32 %10, 4
  br i1 %cmp8, label %for.body9, label %for.end35

for.body9:                                        ; preds = %for.cond7
  %11 = load i32*, i32** %partition_table, align 4
  %12 = load i32, i32* %partition, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %11, i32 %12
  %13 = load i32, i32* %arrayidx10, align 4
  %div = sdiv i32 %13, 3
  store i32 %div, i32* %nr_sfb, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc29, %for.body9
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %nr_sfb, align 4
  %cmp12 = icmp slt i32 %14, %15
  br i1 %cmp12, label %for.body13, label %for.end32

for.body13:                                       ; preds = %for.cond11
  store i32 0, i32* %window, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc26, %for.body13
  %16 = load i32, i32* %window, align 4
  %cmp15 = icmp slt i32 %16, 3
  br i1 %cmp15, label %for.body16, label %for.end28

for.body16:                                       ; preds = %for.cond14
  %17 = load i32*, i32** %scalefac, align 4
  %18 = load i32, i32* %sfb, align 4
  %mul = mul nsw i32 %18, 3
  %19 = load i32, i32* %window, align 4
  %add = add nsw i32 %mul, %19
  %arrayidx17 = getelementptr inbounds i32, i32* %17, i32 %add
  %20 = load i32, i32* %arrayidx17, align 4
  %21 = load i32, i32* %partition, align 4
  %arrayidx18 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx18, align 4
  %cmp19 = icmp sgt i32 %20, %22
  br i1 %cmp19, label %if.then20, label %if.end25

if.then20:                                        ; preds = %for.body16
  %23 = load i32*, i32** %scalefac, align 4
  %24 = load i32, i32* %sfb, align 4
  %mul21 = mul nsw i32 %24, 3
  %25 = load i32, i32* %window, align 4
  %add22 = add nsw i32 %mul21, %25
  %arrayidx23 = getelementptr inbounds i32, i32* %23, i32 %add22
  %26 = load i32, i32* %arrayidx23, align 4
  %27 = load i32, i32* %partition, align 4
  %arrayidx24 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %27
  store i32 %26, i32* %arrayidx24, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then20, %for.body16
  br label %for.inc26

for.inc26:                                        ; preds = %if.end25
  %28 = load i32, i32* %window, align 4
  %inc27 = add nsw i32 %28, 1
  store i32 %inc27, i32* %window, align 4
  br label %for.cond14

for.end28:                                        ; preds = %for.cond14
  br label %for.inc29

for.inc29:                                        ; preds = %for.end28
  %29 = load i32, i32* %i, align 4
  %inc30 = add nsw i32 %29, 1
  store i32 %inc30, i32* %i, align 4
  %30 = load i32, i32* %sfb, align 4
  %inc31 = add nsw i32 %30, 1
  store i32 %inc31, i32* %sfb, align 4
  br label %for.cond11

for.end32:                                        ; preds = %for.cond11
  br label %for.inc33

for.inc33:                                        ; preds = %for.end32
  %31 = load i32, i32* %partition, align 4
  %inc34 = add nsw i32 %31, 1
  store i32 %inc34, i32* %partition, align 4
  br label %for.cond7

for.end35:                                        ; preds = %for.cond7
  br label %if.end61

if.else36:                                        ; preds = %for.end
  store i32 0, i32* %row_in_table, align 4
  %32 = load i32, i32* %table_number, align 4
  %arrayidx37 = getelementptr inbounds [6 x [3 x [4 x i32]]], [6 x [3 x [4 x i32]]]* @nr_of_sfb_block, i32 0, i32 %32
  %33 = load i32, i32* %row_in_table, align 4
  %arrayidx38 = getelementptr inbounds [3 x [4 x i32]], [3 x [4 x i32]]* %arrayidx37, i32 0, i32 %33
  %arrayidx39 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx38, i32 0, i32 0
  store i32* %arrayidx39, i32** %partition_table, align 4
  store i32 0, i32* %sfb, align 4
  store i32 0, i32* %partition, align 4
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc58, %if.else36
  %34 = load i32, i32* %partition, align 4
  %cmp41 = icmp slt i32 %34, 4
  br i1 %cmp41, label %for.body42, label %for.end60

for.body42:                                       ; preds = %for.cond40
  %35 = load i32*, i32** %partition_table, align 4
  %36 = load i32, i32* %partition, align 4
  %arrayidx43 = getelementptr inbounds i32, i32* %35, i32 %36
  %37 = load i32, i32* %arrayidx43, align 4
  store i32 %37, i32* %nr_sfb, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc54, %for.body42
  %38 = load i32, i32* %i, align 4
  %39 = load i32, i32* %nr_sfb, align 4
  %cmp45 = icmp slt i32 %38, %39
  br i1 %cmp45, label %for.body46, label %for.end57

for.body46:                                       ; preds = %for.cond44
  %40 = load i32*, i32** %scalefac, align 4
  %41 = load i32, i32* %sfb, align 4
  %arrayidx47 = getelementptr inbounds i32, i32* %40, i32 %41
  %42 = load i32, i32* %arrayidx47, align 4
  %43 = load i32, i32* %partition, align 4
  %arrayidx48 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx48, align 4
  %cmp49 = icmp sgt i32 %42, %44
  br i1 %cmp49, label %if.then50, label %if.end53

if.then50:                                        ; preds = %for.body46
  %45 = load i32*, i32** %scalefac, align 4
  %46 = load i32, i32* %sfb, align 4
  %arrayidx51 = getelementptr inbounds i32, i32* %45, i32 %46
  %47 = load i32, i32* %arrayidx51, align 4
  %48 = load i32, i32* %partition, align 4
  %arrayidx52 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %48
  store i32 %47, i32* %arrayidx52, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.then50, %for.body46
  br label %for.inc54

for.inc54:                                        ; preds = %if.end53
  %49 = load i32, i32* %i, align 4
  %inc55 = add nsw i32 %49, 1
  store i32 %inc55, i32* %i, align 4
  %50 = load i32, i32* %sfb, align 4
  %inc56 = add nsw i32 %50, 1
  store i32 %inc56, i32* %sfb, align 4
  br label %for.cond44

for.end57:                                        ; preds = %for.cond44
  br label %for.inc58

for.inc58:                                        ; preds = %for.end57
  %51 = load i32, i32* %partition, align 4
  %inc59 = add nsw i32 %51, 1
  store i32 %inc59, i32* %partition, align 4
  br label %for.cond40

for.end60:                                        ; preds = %for.cond40
  br label %if.end61

if.end61:                                         ; preds = %for.end60, %for.end35
  store i32 0, i32* %over, align 4
  store i32 0, i32* %partition, align 4
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc72, %if.end61
  %52 = load i32, i32* %partition, align 4
  %cmp63 = icmp slt i32 %52, 4
  br i1 %cmp63, label %for.body64, label %for.end74

for.body64:                                       ; preds = %for.cond62
  %53 = load i32, i32* %partition, align 4
  %arrayidx65 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx65, align 4
  %55 = load i32, i32* %table_number, align 4
  %arrayidx66 = getelementptr inbounds [6 x [4 x i32]], [6 x [4 x i32]]* @max_range_sfac_tab, i32 0, i32 %55
  %56 = load i32, i32* %partition, align 4
  %arrayidx67 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx66, i32 0, i32 %56
  %57 = load i32, i32* %arrayidx67, align 4
  %cmp68 = icmp sgt i32 %54, %57
  br i1 %cmp68, label %if.then69, label %if.end71

if.then69:                                        ; preds = %for.body64
  %58 = load i32, i32* %over, align 4
  %inc70 = add nsw i32 %58, 1
  store i32 %inc70, i32* %over, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.then69, %for.body64
  br label %for.inc72

for.inc72:                                        ; preds = %if.end71
  %59 = load i32, i32* %partition, align 4
  %inc73 = add nsw i32 %59, 1
  store i32 %inc73, i32* %partition, align 4
  br label %for.cond62

for.end74:                                        ; preds = %for.cond62
  %60 = load i32, i32* %over, align 4
  %tobool75 = icmp ne i32 %60, 0
  br i1 %tobool75, label %if.end114, label %if.then76

if.then76:                                        ; preds = %for.end74
  %61 = load i32, i32* %table_number, align 4
  %arrayidx77 = getelementptr inbounds [6 x [3 x [4 x i32]]], [6 x [3 x [4 x i32]]]* @nr_of_sfb_block, i32 0, i32 %61
  %62 = load i32, i32* %row_in_table, align 4
  %arrayidx78 = getelementptr inbounds [3 x [4 x i32]], [3 x [4 x i32]]* %arrayidx77, i32 0, i32 %62
  %arraydecay79 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx78, i32 0, i32 0
  %63 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_partition_table = getelementptr inbounds %struct.gr_info, %struct.gr_info* %63, i32 0, i32 28
  store i32* %arraydecay79, i32** %sfb_partition_table, align 4
  store i32 0, i32* %partition, align 4
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc86, %if.then76
  %64 = load i32, i32* %partition, align 4
  %cmp81 = icmp slt i32 %64, 4
  br i1 %cmp81, label %for.body82, label %for.end88

for.body82:                                       ; preds = %for.cond80
  %65 = load i32, i32* %partition, align 4
  %arrayidx83 = getelementptr inbounds [4 x i32], [4 x i32]* %max_sfac, i32 0, i32 %65
  %66 = load i32, i32* %arrayidx83, align 4
  %arrayidx84 = getelementptr inbounds [16 x i32], [16 x i32]* @mpeg2_scale_bitcount.log2tab, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx84, align 4
  %68 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen = getelementptr inbounds %struct.gr_info, %struct.gr_info* %68, i32 0, i32 29
  %69 = load i32, i32* %partition, align 4
  %arrayidx85 = getelementptr inbounds [4 x i32], [4 x i32]* %slen, i32 0, i32 %69
  store i32 %67, i32* %arrayidx85, align 4
  br label %for.inc86

for.inc86:                                        ; preds = %for.body82
  %70 = load i32, i32* %partition, align 4
  %inc87 = add nsw i32 %70, 1
  store i32 %inc87, i32* %partition, align 4
  br label %for.cond80

for.end88:                                        ; preds = %for.cond80
  %71 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen89 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %71, i32 0, i32 29
  %arrayidx90 = getelementptr inbounds [4 x i32], [4 x i32]* %slen89, i32 0, i32 0
  %72 = load i32, i32* %arrayidx90, align 4
  store i32 %72, i32* %slen1, align 4
  %73 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen91 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %73, i32 0, i32 29
  %arrayidx92 = getelementptr inbounds [4 x i32], [4 x i32]* %slen91, i32 0, i32 1
  %74 = load i32, i32* %arrayidx92, align 4
  store i32 %74, i32* %slen2, align 4
  %75 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen93 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %75, i32 0, i32 29
  %arrayidx94 = getelementptr inbounds [4 x i32], [4 x i32]* %slen93, i32 0, i32 2
  %76 = load i32, i32* %arrayidx94, align 4
  store i32 %76, i32* %slen3, align 4
  %77 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen95 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %77, i32 0, i32 29
  %arrayidx96 = getelementptr inbounds [4 x i32], [4 x i32]* %slen95, i32 0, i32 3
  %78 = load i32, i32* %arrayidx96, align 4
  store i32 %78, i32* %slen4, align 4
  %79 = load i32, i32* %table_number, align 4
  switch i32 %79, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb102
    i32 2, label %sw.bb109
  ]

sw.bb:                                            ; preds = %for.end88
  %80 = load i32, i32* %slen1, align 4
  %mul97 = mul nsw i32 %80, 5
  %81 = load i32, i32* %slen2, align 4
  %add98 = add nsw i32 %mul97, %81
  %shl = shl i32 %add98, 4
  %82 = load i32, i32* %slen3, align 4
  %shl99 = shl i32 %82, 2
  %add100 = add nsw i32 %shl, %shl99
  %83 = load i32, i32* %slen4, align 4
  %add101 = add nsw i32 %add100, %83
  %84 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %84, i32 0, i32 8
  store i32 %add101, i32* %scalefac_compress, align 4
  br label %sw.epilog

sw.bb102:                                         ; preds = %for.end88
  %85 = load i32, i32* %slen1, align 4
  %mul103 = mul nsw i32 %85, 5
  %86 = load i32, i32* %slen2, align 4
  %add104 = add nsw i32 %mul103, %86
  %shl105 = shl i32 %add104, 2
  %add106 = add nsw i32 400, %shl105
  %87 = load i32, i32* %slen3, align 4
  %add107 = add nsw i32 %add106, %87
  %88 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_compress108 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %88, i32 0, i32 8
  store i32 %add107, i32* %scalefac_compress108, align 4
  br label %sw.epilog

sw.bb109:                                         ; preds = %for.end88
  %89 = load i32, i32* %slen1, align 4
  %mul110 = mul nsw i32 %89, 3
  %add111 = add nsw i32 500, %mul110
  %90 = load i32, i32* %slen2, align 4
  %add112 = add nsw i32 %add111, %90
  %91 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_compress113 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %91, i32 0, i32 8
  store i32 %add112, i32* %scalefac_compress113, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %for.end88
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %92, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str, i32 0, i32 0))
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb109, %sw.bb102, %sw.bb
  br label %if.end114

if.end114:                                        ; preds = %sw.epilog, %for.end74
  %93 = load i32, i32* %over, align 4
  %tobool115 = icmp ne i32 %93, 0
  br i1 %tobool115, label %if.end130, label %if.then116

if.then116:                                       ; preds = %if.end114
  %94 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %94, i32 0, i32 18
  store i32 0, i32* %part2_length, align 4
  store i32 0, i32* %partition, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc127, %if.then116
  %95 = load i32, i32* %partition, align 4
  %cmp118 = icmp slt i32 %95, 4
  br i1 %cmp118, label %for.body119, label %for.end129

for.body119:                                      ; preds = %for.cond117
  %96 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen120 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %96, i32 0, i32 29
  %97 = load i32, i32* %partition, align 4
  %arrayidx121 = getelementptr inbounds [4 x i32], [4 x i32]* %slen120, i32 0, i32 %97
  %98 = load i32, i32* %arrayidx121, align 4
  %99 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_partition_table122 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %99, i32 0, i32 28
  %100 = load i32*, i32** %sfb_partition_table122, align 4
  %101 = load i32, i32* %partition, align 4
  %arrayidx123 = getelementptr inbounds i32, i32* %100, i32 %101
  %102 = load i32, i32* %arrayidx123, align 4
  %mul124 = mul nsw i32 %98, %102
  %103 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length125 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %103, i32 0, i32 18
  %104 = load i32, i32* %part2_length125, align 4
  %add126 = add nsw i32 %104, %mul124
  store i32 %add126, i32* %part2_length125, align 4
  br label %for.inc127

for.inc127:                                       ; preds = %for.body119
  %105 = load i32, i32* %partition, align 4
  %inc128 = add nsw i32 %105, 1
  store i32 %inc128, i32* %partition, align 4
  br label %for.cond117

for.end129:                                       ; preds = %for.cond117
  br label %if.end130

if.end130:                                        ; preds = %for.end129, %if.end114
  %106 = load i32, i32* %over, align 4
  ret i32 %106
}

; Function Attrs: noinline nounwind optnone
define hidden void @huffman_init(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %i = alloca i32, align 4
  %scfb_anz = alloca i32, align 4
  %bv_index = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %choose_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 25
  store i32 (i32*, i32*, i32*)* @choose_table_nonMMX, i32 (i32*, i32*, i32*)** %choose_table, align 8
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp sle i32 %1, 576
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %scfb_anz, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %3 = load i32, i32* %scfb_anz, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %scfb_anz, align 4
  %arrayidx = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %inc
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load i32, i32* %i, align 4
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %6 = load i32, i32* %scfb_anz, align 4
  %arrayidx2 = getelementptr inbounds [23 x %struct.anon.2], [23 x %struct.anon.2]* @subdv_table, i32 0, i32 %6
  %region0_count = getelementptr inbounds %struct.anon.2, %struct.anon.2* %arrayidx2, i32 0, i32 0
  %7 = load i32, i32* %region0_count, align 8
  store i32 %7, i32* %bv_index, align 4
  br label %while.cond3

while.cond3:                                      ; preds = %while.body8, %while.end
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 8
  %l5 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band4, i32 0, i32 0
  %9 = load i32, i32* %bv_index, align 4
  %add = add nsw i32 %9, 1
  %arrayidx6 = getelementptr inbounds [23 x i32], [23 x i32]* %l5, i32 0, i32 %add
  %10 = load i32, i32* %arrayidx6, align 4
  %11 = load i32, i32* %i, align 4
  %cmp7 = icmp sgt i32 %10, %11
  br i1 %cmp7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond3
  %12 = load i32, i32* %bv_index, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %bv_index, align 4
  br label %while.cond3

while.end9:                                       ; preds = %while.cond3
  %13 = load i32, i32* %bv_index, align 4
  %cmp10 = icmp slt i32 %13, 0
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %14 = load i32, i32* %scfb_anz, align 4
  %arrayidx11 = getelementptr inbounds [23 x %struct.anon.2], [23 x %struct.anon.2]* @subdv_table, i32 0, i32 %14
  %region0_count12 = getelementptr inbounds %struct.anon.2, %struct.anon.2* %arrayidx11, i32 0, i32 0
  %15 = load i32, i32* %region0_count12, align 8
  store i32 %15, i32* %bv_index, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  %16 = load i32, i32* %bv_index, align 4
  %conv = trunc i32 %16 to i8
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 13
  %bv_scf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 10
  %18 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %18, 2
  %arrayidx13 = getelementptr inbounds [576 x i8], [576 x i8]* %bv_scf, i32 0, i32 %sub
  store i8 %conv, i8* %arrayidx13, align 1
  %19 = load i32, i32* %scfb_anz, align 4
  %arrayidx14 = getelementptr inbounds [23 x %struct.anon.2], [23 x %struct.anon.2]* @subdv_table, i32 0, i32 %19
  %region1_count = getelementptr inbounds %struct.anon.2, %struct.anon.2* %arrayidx14, i32 0, i32 1
  %20 = load i32, i32* %region1_count, align 4
  store i32 %20, i32* %bv_index, align 4
  br label %while.cond15

while.cond15:                                     ; preds = %while.body28, %if.end
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 8
  %l17 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band16, i32 0, i32 0
  %22 = load i32, i32* %bv_index, align 4
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt18 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 13
  %bv_scf19 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt18, i32 0, i32 10
  %24 = load i32, i32* %i, align 4
  %sub20 = sub nsw i32 %24, 2
  %arrayidx21 = getelementptr inbounds [576 x i8], [576 x i8]* %bv_scf19, i32 0, i32 %sub20
  %25 = load i8, i8* %arrayidx21, align 1
  %conv22 = sext i8 %25 to i32
  %add23 = add nsw i32 %22, %conv22
  %add24 = add nsw i32 %add23, 2
  %arrayidx25 = getelementptr inbounds [23 x i32], [23 x i32]* %l17, i32 0, i32 %add24
  %26 = load i32, i32* %arrayidx25, align 4
  %27 = load i32, i32* %i, align 4
  %cmp26 = icmp sgt i32 %26, %27
  br i1 %cmp26, label %while.body28, label %while.end30

while.body28:                                     ; preds = %while.cond15
  %28 = load i32, i32* %bv_index, align 4
  %dec29 = add nsw i32 %28, -1
  store i32 %dec29, i32* %bv_index, align 4
  br label %while.cond15

while.end30:                                      ; preds = %while.cond15
  %29 = load i32, i32* %bv_index, align 4
  %cmp31 = icmp slt i32 %29, 0
  br i1 %cmp31, label %if.then33, label %if.end36

if.then33:                                        ; preds = %while.end30
  %30 = load i32, i32* %scfb_anz, align 4
  %arrayidx34 = getelementptr inbounds [23 x %struct.anon.2], [23 x %struct.anon.2]* @subdv_table, i32 0, i32 %30
  %region1_count35 = getelementptr inbounds %struct.anon.2, %struct.anon.2* %arrayidx34, i32 0, i32 1
  %31 = load i32, i32* %region1_count35, align 4
  store i32 %31, i32* %bv_index, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then33, %while.end30
  %32 = load i32, i32* %bv_index, align 4
  %conv37 = trunc i32 %32 to i8
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt38 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 13
  %bv_scf39 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt38, i32 0, i32 10
  %34 = load i32, i32* %i, align 4
  %sub40 = sub nsw i32 %34, 1
  %arrayidx41 = getelementptr inbounds [576 x i8], [576 x i8]* %bv_scf39, i32 0, i32 %sub40
  store i8 %conv37, i8* %arrayidx41, align 1
  br label %for.inc

for.inc:                                          ; preds = %if.end36
  %35 = load i32, i32* %i, align 4
  %add42 = add nsw i32 %35, 2
  store i32 %add42, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @choose_table_nonMMX(i32* %ix, i32* %end, i32* %_s) #0 {
entry:
  %retval = alloca i32, align 4
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %_s.addr = alloca i32*, align 4
  %s = alloca i32*, align 4
  %max = alloca i32, align 4
  %choice = alloca i32, align 4
  %choice2 = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32* %_s, i32** %_s.addr, align 4
  %0 = load i32*, i32** %_s.addr, align 4
  store i32* %0, i32** %s, align 4
  %1 = load i32*, i32** %ix.addr, align 4
  %2 = load i32*, i32** %end.addr, align 4
  %call = call i32 @ix_max(i32* %1, i32* %2)
  store i32 %call, i32* %max, align 4
  %3 = load i32, i32* %max, align 4
  %cmp = icmp ule i32 %3, 15
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %max, align 4
  %arrayidx = getelementptr inbounds [16 x i32 (i32*, i32*, i32, i32*)*], [16 x i32 (i32*, i32*, i32, i32*)*]* @count_fncs, i32 0, i32 %4
  %5 = load i32 (i32*, i32*, i32, i32*)*, i32 (i32*, i32*, i32, i32*)** %arrayidx, align 4
  %6 = load i32*, i32** %ix.addr, align 4
  %7 = load i32*, i32** %end.addr, align 4
  %8 = load i32, i32* %max, align 4
  %9 = load i32*, i32** %s, align 4
  %call1 = call i32 %5(i32* %6, i32* %7, i32 %8, i32* %9)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %10 = load i32, i32* %max, align 4
  %cmp2 = icmp ugt i32 %10, 8206
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %11 = load i32*, i32** %s, align 4
  store i32 100000, i32* %11, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %12 = load i32, i32* %max, align 4
  %sub = sub i32 %12, 15
  store i32 %sub, i32* %max, align 4
  store i32 24, i32* %choice2, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end4
  %13 = load i32, i32* %choice2, align 4
  %cmp5 = icmp slt i32 %13, 32
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %choice2, align 4
  %arrayidx6 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %14
  %linmax = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx6, i32 0, i32 1
  %15 = load i32, i32* %linmax, align 4
  %16 = load i32, i32* %max, align 4
  %cmp7 = icmp uge i32 %15, %16
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %for.body
  br label %for.end

if.end9:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %17 = load i32, i32* %choice2, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %choice2, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  %18 = load i32, i32* %choice2, align 4
  %sub10 = sub nsw i32 %18, 8
  store i32 %sub10, i32* %choice, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc19, %for.end
  %19 = load i32, i32* %choice, align 4
  %cmp12 = icmp slt i32 %19, 24
  br i1 %cmp12, label %for.body13, label %for.end21

for.body13:                                       ; preds = %for.cond11
  %20 = load i32, i32* %choice, align 4
  %arrayidx14 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %20
  %linmax15 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx14, i32 0, i32 1
  %21 = load i32, i32* %linmax15, align 4
  %22 = load i32, i32* %max, align 4
  %cmp16 = icmp uge i32 %21, %22
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.body13
  br label %for.end21

if.end18:                                         ; preds = %for.body13
  br label %for.inc19

for.inc19:                                        ; preds = %if.end18
  %23 = load i32, i32* %choice, align 4
  %inc20 = add nsw i32 %23, 1
  store i32 %inc20, i32* %choice, align 4
  br label %for.cond11

for.end21:                                        ; preds = %if.then17, %for.cond11
  %24 = load i32*, i32** %ix.addr, align 4
  %25 = load i32*, i32** %end.addr, align 4
  %26 = load i32, i32* %choice, align 4
  %27 = load i32, i32* %choice2, align 4
  %28 = load i32*, i32** %s, align 4
  %call22 = call i32 @count_bit_ESC(i32* %24, i32* %25, i32 %26, i32 %27, i32* %28)
  store i32 %call22, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end21, %if.then3, %if.then
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

; Function Attrs: noinline nounwind optnone
define internal void @quantize_lines_xrpow(i32 %l, float %istep, float* %xp, i32* %pi) #0 {
entry:
  %l.addr = alloca i32, align 4
  %istep.addr = alloca float, align 4
  %xp.addr = alloca float*, align 4
  %pi.addr = alloca i32*, align 4
  %fi = alloca %union.fi_union*, align 4
  %remaining = alloca i32, align 4
  %x0 = alloca double, align 8
  %x1 = alloca double, align 8
  %x2 = alloca double, align 8
  %x3 = alloca double, align 8
  %x073 = alloca double, align 8
  %x177 = alloca double, align 8
  store i32 %l, i32* %l.addr, align 4
  store float %istep, float* %istep.addr, align 4
  store float* %xp, float** %xp.addr, align 4
  store i32* %pi, i32** %pi.addr, align 4
  %0 = load i32*, i32** %pi.addr, align 4
  %1 = bitcast i32* %0 to %union.fi_union*
  store %union.fi_union* %1, %union.fi_union** %fi, align 4
  %2 = load i32, i32* %l.addr, align 4
  %shr = lshr i32 %2, 1
  store i32 %shr, i32* %l.addr, align 4
  %3 = load i32, i32* %l.addr, align 4
  %rem = urem i32 %3, 2
  store i32 %rem, i32* %remaining, align 4
  %4 = load i32, i32* %l.addr, align 4
  %shr1 = lshr i32 %4, 1
  store i32 %shr1, i32* %l.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %5 = load i32, i32* %l.addr, align 4
  %dec = add i32 %5, -1
  store i32 %dec, i32* %l.addr, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load float, float* %istep.addr, align 4
  %7 = load float*, float** %xp.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %7, i32 0
  %8 = load float, float* %arrayidx, align 4
  %mul = fmul float %6, %8
  %conv = fpext float %mul to double
  store double %conv, double* %x0, align 8
  %9 = load float, float* %istep.addr, align 4
  %10 = load float*, float** %xp.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %10, i32 1
  %11 = load float, float* %arrayidx2, align 4
  %mul3 = fmul float %9, %11
  %conv4 = fpext float %mul3 to double
  store double %conv4, double* %x1, align 8
  %12 = load float, float* %istep.addr, align 4
  %13 = load float*, float** %xp.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %13, i32 2
  %14 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %12, %14
  %conv7 = fpext float %mul6 to double
  store double %conv7, double* %x2, align 8
  %15 = load float, float* %istep.addr, align 4
  %16 = load float*, float** %xp.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %16, i32 3
  %17 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %15, %17
  %conv10 = fpext float %mul9 to double
  store double %conv10, double* %x3, align 8
  %18 = load double, double* %x0, align 8
  %add = fadd double %18, 0x4160000000000000
  store double %add, double* %x0, align 8
  %19 = load double, double* %x0, align 8
  %conv11 = fptrunc double %19 to float
  %20 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx12 = getelementptr inbounds %union.fi_union, %union.fi_union* %20, i32 0
  %f = bitcast %union.fi_union* %arrayidx12 to float*
  store float %conv11, float* %f, align 4
  %21 = load double, double* %x1, align 8
  %add13 = fadd double %21, 0x4160000000000000
  store double %add13, double* %x1, align 8
  %22 = load double, double* %x1, align 8
  %conv14 = fptrunc double %22 to float
  %23 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx15 = getelementptr inbounds %union.fi_union, %union.fi_union* %23, i32 1
  %f16 = bitcast %union.fi_union* %arrayidx15 to float*
  store float %conv14, float* %f16, align 4
  %24 = load double, double* %x2, align 8
  %add17 = fadd double %24, 0x4160000000000000
  store double %add17, double* %x2, align 8
  %25 = load double, double* %x2, align 8
  %conv18 = fptrunc double %25 to float
  %26 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx19 = getelementptr inbounds %union.fi_union, %union.fi_union* %26, i32 2
  %f20 = bitcast %union.fi_union* %arrayidx19 to float*
  store float %conv18, float* %f20, align 4
  %27 = load double, double* %x3, align 8
  %add21 = fadd double %27, 0x4160000000000000
  store double %add21, double* %x3, align 8
  %28 = load double, double* %x3, align 8
  %conv22 = fptrunc double %28 to float
  %29 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx23 = getelementptr inbounds %union.fi_union, %union.fi_union* %29, i32 3
  %f24 = bitcast %union.fi_union* %arrayidx23 to float*
  store float %conv22, float* %f24, align 4
  %30 = load double, double* %x0, align 8
  %31 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx25 = getelementptr inbounds %union.fi_union, %union.fi_union* %31, i32 0
  %i = bitcast %union.fi_union* %arrayidx25 to i32*
  %32 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %32, 1258291200
  %arrayidx26 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub
  %33 = load float, float* %arrayidx26, align 4
  %conv27 = fpext float %33 to double
  %add28 = fadd double %30, %conv27
  %conv29 = fptrunc double %add28 to float
  %34 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx30 = getelementptr inbounds %union.fi_union, %union.fi_union* %34, i32 0
  %f31 = bitcast %union.fi_union* %arrayidx30 to float*
  store float %conv29, float* %f31, align 4
  %35 = load double, double* %x1, align 8
  %36 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx32 = getelementptr inbounds %union.fi_union, %union.fi_union* %36, i32 1
  %i33 = bitcast %union.fi_union* %arrayidx32 to i32*
  %37 = load i32, i32* %i33, align 4
  %sub34 = sub nsw i32 %37, 1258291200
  %arrayidx35 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub34
  %38 = load float, float* %arrayidx35, align 4
  %conv36 = fpext float %38 to double
  %add37 = fadd double %35, %conv36
  %conv38 = fptrunc double %add37 to float
  %39 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx39 = getelementptr inbounds %union.fi_union, %union.fi_union* %39, i32 1
  %f40 = bitcast %union.fi_union* %arrayidx39 to float*
  store float %conv38, float* %f40, align 4
  %40 = load double, double* %x2, align 8
  %41 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx41 = getelementptr inbounds %union.fi_union, %union.fi_union* %41, i32 2
  %i42 = bitcast %union.fi_union* %arrayidx41 to i32*
  %42 = load i32, i32* %i42, align 4
  %sub43 = sub nsw i32 %42, 1258291200
  %arrayidx44 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub43
  %43 = load float, float* %arrayidx44, align 4
  %conv45 = fpext float %43 to double
  %add46 = fadd double %40, %conv45
  %conv47 = fptrunc double %add46 to float
  %44 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx48 = getelementptr inbounds %union.fi_union, %union.fi_union* %44, i32 2
  %f49 = bitcast %union.fi_union* %arrayidx48 to float*
  store float %conv47, float* %f49, align 4
  %45 = load double, double* %x3, align 8
  %46 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx50 = getelementptr inbounds %union.fi_union, %union.fi_union* %46, i32 3
  %i51 = bitcast %union.fi_union* %arrayidx50 to i32*
  %47 = load i32, i32* %i51, align 4
  %sub52 = sub nsw i32 %47, 1258291200
  %arrayidx53 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub52
  %48 = load float, float* %arrayidx53, align 4
  %conv54 = fpext float %48 to double
  %add55 = fadd double %45, %conv54
  %conv56 = fptrunc double %add55 to float
  %49 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx57 = getelementptr inbounds %union.fi_union, %union.fi_union* %49, i32 3
  %f58 = bitcast %union.fi_union* %arrayidx57 to float*
  store float %conv56, float* %f58, align 4
  %50 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx59 = getelementptr inbounds %union.fi_union, %union.fi_union* %50, i32 0
  %i60 = bitcast %union.fi_union* %arrayidx59 to i32*
  %51 = load i32, i32* %i60, align 4
  %sub61 = sub nsw i32 %51, 1258291200
  store i32 %sub61, i32* %i60, align 4
  %52 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx62 = getelementptr inbounds %union.fi_union, %union.fi_union* %52, i32 1
  %i63 = bitcast %union.fi_union* %arrayidx62 to i32*
  %53 = load i32, i32* %i63, align 4
  %sub64 = sub nsw i32 %53, 1258291200
  store i32 %sub64, i32* %i63, align 4
  %54 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx65 = getelementptr inbounds %union.fi_union, %union.fi_union* %54, i32 2
  %i66 = bitcast %union.fi_union* %arrayidx65 to i32*
  %55 = load i32, i32* %i66, align 4
  %sub67 = sub nsw i32 %55, 1258291200
  store i32 %sub67, i32* %i66, align 4
  %56 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx68 = getelementptr inbounds %union.fi_union, %union.fi_union* %56, i32 3
  %i69 = bitcast %union.fi_union* %arrayidx68 to i32*
  %57 = load i32, i32* %i69, align 4
  %sub70 = sub nsw i32 %57, 1258291200
  store i32 %sub70, i32* %i69, align 4
  %58 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %add.ptr = getelementptr inbounds %union.fi_union, %union.fi_union* %58, i32 4
  store %union.fi_union* %add.ptr, %union.fi_union** %fi, align 4
  %59 = load float*, float** %xp.addr, align 4
  %add.ptr71 = getelementptr inbounds float, float* %59, i32 4
  store float* %add.ptr71, float** %xp.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %60 = load i32, i32* %remaining, align 4
  %tobool72 = icmp ne i32 %60, 0
  br i1 %tobool72, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %61 = load float, float* %istep.addr, align 4
  %62 = load float*, float** %xp.addr, align 4
  %arrayidx74 = getelementptr inbounds float, float* %62, i32 0
  %63 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %61, %63
  %conv76 = fpext float %mul75 to double
  store double %conv76, double* %x073, align 8
  %64 = load float, float* %istep.addr, align 4
  %65 = load float*, float** %xp.addr, align 4
  %arrayidx78 = getelementptr inbounds float, float* %65, i32 1
  %66 = load float, float* %arrayidx78, align 4
  %mul79 = fmul float %64, %66
  %conv80 = fpext float %mul79 to double
  store double %conv80, double* %x177, align 8
  %67 = load double, double* %x073, align 8
  %add81 = fadd double %67, 0x4160000000000000
  store double %add81, double* %x073, align 8
  %68 = load double, double* %x073, align 8
  %conv82 = fptrunc double %68 to float
  %69 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx83 = getelementptr inbounds %union.fi_union, %union.fi_union* %69, i32 0
  %f84 = bitcast %union.fi_union* %arrayidx83 to float*
  store float %conv82, float* %f84, align 4
  %70 = load double, double* %x177, align 8
  %add85 = fadd double %70, 0x4160000000000000
  store double %add85, double* %x177, align 8
  %71 = load double, double* %x177, align 8
  %conv86 = fptrunc double %71 to float
  %72 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx87 = getelementptr inbounds %union.fi_union, %union.fi_union* %72, i32 1
  %f88 = bitcast %union.fi_union* %arrayidx87 to float*
  store float %conv86, float* %f88, align 4
  %73 = load double, double* %x073, align 8
  %74 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx89 = getelementptr inbounds %union.fi_union, %union.fi_union* %74, i32 0
  %i90 = bitcast %union.fi_union* %arrayidx89 to i32*
  %75 = load i32, i32* %i90, align 4
  %sub91 = sub nsw i32 %75, 1258291200
  %arrayidx92 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub91
  %76 = load float, float* %arrayidx92, align 4
  %conv93 = fpext float %76 to double
  %add94 = fadd double %73, %conv93
  %conv95 = fptrunc double %add94 to float
  %77 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx96 = getelementptr inbounds %union.fi_union, %union.fi_union* %77, i32 0
  %f97 = bitcast %union.fi_union* %arrayidx96 to float*
  store float %conv95, float* %f97, align 4
  %78 = load double, double* %x177, align 8
  %79 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx98 = getelementptr inbounds %union.fi_union, %union.fi_union* %79, i32 1
  %i99 = bitcast %union.fi_union* %arrayidx98 to i32*
  %80 = load i32, i32* %i99, align 4
  %sub100 = sub nsw i32 %80, 1258291200
  %arrayidx101 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub100
  %81 = load float, float* %arrayidx101, align 4
  %conv102 = fpext float %81 to double
  %add103 = fadd double %78, %conv102
  %conv104 = fptrunc double %add103 to float
  %82 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx105 = getelementptr inbounds %union.fi_union, %union.fi_union* %82, i32 1
  %f106 = bitcast %union.fi_union* %arrayidx105 to float*
  store float %conv104, float* %f106, align 4
  %83 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx107 = getelementptr inbounds %union.fi_union, %union.fi_union* %83, i32 0
  %i108 = bitcast %union.fi_union* %arrayidx107 to i32*
  %84 = load i32, i32* %i108, align 4
  %sub109 = sub nsw i32 %84, 1258291200
  store i32 %sub109, i32* %i108, align 4
  %85 = load %union.fi_union*, %union.fi_union** %fi, align 4
  %arrayidx110 = getelementptr inbounds %union.fi_union, %union.fi_union* %85, i32 1
  %i111 = bitcast %union.fi_union* %arrayidx110 to i32*
  %86 = load i32, i32* %i111, align 4
  %sub112 = sub nsw i32 %86, 1258291200
  store i32 %sub112, i32* %i111, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @quantize_lines_xrpow_01(i32 %l, float %istep, float* %xr, i32* %ix) #0 {
entry:
  %l.addr = alloca i32, align 4
  %istep.addr = alloca float, align 4
  %xr.addr = alloca float*, align 4
  %ix.addr = alloca i32*, align 4
  %compareval0 = alloca float, align 4
  %i = alloca i32, align 4
  %xr_0 = alloca float, align 4
  %xr_1 = alloca float, align 4
  %ix_0 = alloca i32, align 4
  %ix_1 = alloca i32, align 4
  store i32 %l, i32* %l.addr, align 4
  store float %istep, float* %istep.addr, align 4
  store float* %xr, float** %xr.addr, align 4
  store i32* %ix, i32** %ix.addr, align 4
  %0 = load float, float* %istep.addr, align 4
  %div = fdiv float 0x3FE306F680000000, %0
  store float %div, float* %compareval0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %l.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %xr.addr, align 4
  %4 = load i32, i32* %i, align 4
  %add = add i32 %4, 0
  %arrayidx = getelementptr inbounds float, float* %3, i32 %add
  %5 = load float, float* %arrayidx, align 4
  store float %5, float* %xr_0, align 4
  %6 = load float*, float** %xr.addr, align 4
  %7 = load i32, i32* %i, align 4
  %add1 = add i32 %7, 1
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 %add1
  %8 = load float, float* %arrayidx2, align 4
  store float %8, float* %xr_1, align 4
  %9 = load float, float* %compareval0, align 4
  %10 = load float, float* %xr_0, align 4
  %cmp3 = fcmp ogt float %9, %10
  %11 = zext i1 %cmp3 to i64
  %cond = select i1 %cmp3, i32 0, i32 1
  store i32 %cond, i32* %ix_0, align 4
  %12 = load float, float* %compareval0, align 4
  %13 = load float, float* %xr_1, align 4
  %cmp4 = fcmp ogt float %12, %13
  %14 = zext i1 %cmp4 to i64
  %cond5 = select i1 %cmp4, i32 0, i32 1
  store i32 %cond5, i32* %ix_1, align 4
  %15 = load i32, i32* %ix_0, align 4
  %16 = load i32*, i32** %ix.addr, align 4
  %17 = load i32, i32* %i, align 4
  %add6 = add i32 %17, 0
  %arrayidx7 = getelementptr inbounds i32, i32* %16, i32 %add6
  store i32 %15, i32* %arrayidx7, align 4
  %18 = load i32, i32* %ix_1, align 4
  %19 = load i32*, i32** %ix.addr, align 4
  %20 = load i32, i32* %i, align 4
  %add8 = add i32 %20, 1
  %arrayidx9 = getelementptr inbounds i32, i32* %19, i32 %add8
  store i32 %18, i32* %arrayidx9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %add10 = add i32 %21, 2
  store i32 %add10, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #3

; Function Attrs: noinline nounwind optnone
define internal i32 @ix_max(i32* %ix, i32* %end) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %max1 = alloca i32, align 4
  %max2 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 0, i32* %max1, align 4
  store i32 0, i32* %max2, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %0 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %0, i32 1
  store i32* %incdec.ptr, i32** %ix.addr, align 4
  %1 = load i32, i32* %0, align 4
  store i32 %1, i32* %x1, align 4
  %2 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i32, i32* %2, i32 1
  store i32* %incdec.ptr1, i32** %ix.addr, align 4
  %3 = load i32, i32* %2, align 4
  store i32 %3, i32* %x2, align 4
  %4 = load i32, i32* %max1, align 4
  %5 = load i32, i32* %x1, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  %6 = load i32, i32* %x1, align 4
  store i32 %6, i32* %max1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  %7 = load i32, i32* %max2, align 4
  %8 = load i32, i32* %x2, align 4
  %cmp2 = icmp slt i32 %7, %8
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %9 = load i32, i32* %x2, align 4
  store i32 %9, i32* %max2, align 4
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  br label %do.cond

do.cond:                                          ; preds = %if.end4
  %10 = load i32*, i32** %ix.addr, align 4
  %11 = load i32*, i32** %end.addr, align 4
  %cmp5 = icmp ult i32* %10, %11
  br i1 %cmp5, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %12 = load i32, i32* %max1, align 4
  %13 = load i32, i32* %max2, align 4
  %cmp6 = icmp slt i32 %12, %13
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %do.end
  %14 = load i32, i32* %max2, align 4
  store i32 %14, i32* %max1, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %do.end
  %15 = load i32, i32* %max1, align 4
  ret i32 %15
}

; Function Attrs: noinline nounwind optnone
define internal i32 @count_bit_ESC(i32* %ix, i32* %end, i32 %t1, i32 %t2, i32* %s) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %t1.addr = alloca i32, align 4
  %t2.addr = alloca i32, align 4
  %s.addr = alloca i32*, align 4
  %linbits = alloca i32, align 4
  %sum = alloca i32, align 4
  %sum2 = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 %t1, i32* %t1.addr, align 4
  store i32 %t2, i32* %t2.addr, align 4
  store i32* %s, i32** %s.addr, align 4
  %0 = load i32, i32* %t1.addr, align 4
  %arrayidx = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %0
  %xlen = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx, i32 0, i32 0
  %1 = load i32, i32* %xlen, align 16
  %mul = mul i32 %1, 65536
  %2 = load i32, i32* %t2.addr, align 4
  %arrayidx1 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %2
  %xlen2 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx1, i32 0, i32 0
  %3 = load i32, i32* %xlen2, align 16
  %add = add i32 %mul, %3
  store i32 %add, i32* %linbits, align 4
  store i32 0, i32* %sum, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %4 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %4, i32 1
  store i32* %incdec.ptr, i32** %ix.addr, align 4
  %5 = load i32, i32* %4, align 4
  store i32 %5, i32* %x, align 4
  %6 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i32, i32* %6, i32 1
  store i32* %incdec.ptr3, i32** %ix.addr, align 4
  %7 = load i32, i32* %6, align 4
  store i32 %7, i32* %y, align 4
  %8 = load i32, i32* %x, align 4
  %cmp = icmp uge i32 %8, 15
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  store i32 15, i32* %x, align 4
  %9 = load i32, i32* %linbits, align 4
  %10 = load i32, i32* %sum, align 4
  %add4 = add i32 %10, %9
  store i32 %add4, i32* %sum, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %do.body
  %11 = load i32, i32* %y, align 4
  %cmp5 = icmp uge i32 %11, 15
  br i1 %cmp5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  store i32 15, i32* %y, align 4
  %12 = load i32, i32* %linbits, align 4
  %13 = load i32, i32* %sum, align 4
  %add7 = add i32 %13, %12
  store i32 %add7, i32* %sum, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %14 = load i32, i32* %x, align 4
  %shl = shl i32 %14, 4
  store i32 %shl, i32* %x, align 4
  %15 = load i32, i32* %y, align 4
  %16 = load i32, i32* %x, align 4
  %add9 = add i32 %16, %15
  store i32 %add9, i32* %x, align 4
  %17 = load i32, i32* %x, align 4
  %arrayidx10 = getelementptr inbounds [256 x i32], [256 x i32]* @largetbl, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx10, align 4
  %19 = load i32, i32* %sum, align 4
  %add11 = add i32 %19, %18
  store i32 %add11, i32* %sum, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end8
  %20 = load i32*, i32** %ix.addr, align 4
  %21 = load i32*, i32** %end.addr, align 4
  %cmp12 = icmp ult i32* %20, %21
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %22 = load i32, i32* %sum, align 4
  %and = and i32 %22, 65535
  store i32 %and, i32* %sum2, align 4
  %23 = load i32, i32* %sum, align 4
  %shr = lshr i32 %23, 16
  store i32 %shr, i32* %sum, align 4
  %24 = load i32, i32* %sum, align 4
  %25 = load i32, i32* %sum2, align 4
  %cmp13 = icmp ugt i32 %24, %25
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %26 = load i32, i32* %sum2, align 4
  store i32 %26, i32* %sum, align 4
  %27 = load i32, i32* %t2.addr, align 4
  store i32 %27, i32* %t1.addr, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %28 = load i32, i32* %sum, align 4
  %29 = load i32*, i32** %s.addr, align 4
  %30 = load i32, i32* %29, align 4
  %add16 = add i32 %30, %28
  store i32 %add16, i32* %29, align 4
  %31 = load i32, i32* %t1.addr, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define internal i32 @count_bit_null(i32* %ix, i32* %end, i32 %max, i32* %s) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %max.addr = alloca i32, align 4
  %s.addr = alloca i32*, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 %max, i32* %max.addr, align 4
  store i32* %s, i32** %s.addr, align 4
  %0 = load i32*, i32** %ix.addr, align 4
  %1 = load i32*, i32** %end.addr, align 4
  %2 = load i32, i32* %max.addr, align 4
  %3 = load i32*, i32** %s.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @count_bit_noESC(i32* %ix, i32* %end, i32 %mx, i32* %s) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %mx.addr = alloca i32, align 4
  %s.addr = alloca i32*, align 4
  %sum1 = alloca i32, align 4
  %hlen1 = alloca i8*, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 %mx, i32* %mx.addr, align 4
  store i32* %s, i32** %s.addr, align 4
  store i32 0, i32* %sum1, align 4
  %0 = load i8*, i8** getelementptr inbounds ([34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 1, i32 3), align 4
  store i8* %0, i8** %hlen1, align 4
  %1 = load i32, i32* %mx.addr, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %2 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %2, i32 1
  store i32* %incdec.ptr, i32** %ix.addr, align 4
  %3 = load i32, i32* %2, align 4
  store i32 %3, i32* %x0, align 4
  %4 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i32, i32* %4, i32 1
  store i32* %incdec.ptr1, i32** %ix.addr, align 4
  %5 = load i32, i32* %4, align 4
  store i32 %5, i32* %x1, align 4
  %6 = load i8*, i8** %hlen1, align 4
  %7 = load i32, i32* %x0, align 4
  %8 = load i32, i32* %x0, align 4
  %add = add i32 %7, %8
  %9 = load i32, i32* %x1, align 4
  %add2 = add i32 %add, %9
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %add2
  %10 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %10 to i32
  %11 = load i32, i32* %sum1, align 4
  %add3 = add i32 %11, %conv
  store i32 %add3, i32* %sum1, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %12 = load i32*, i32** %ix.addr, align 4
  %13 = load i32*, i32** %end.addr, align 4
  %cmp = icmp ult i32* %12, %13
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %14 = load i32, i32* %sum1, align 4
  %15 = load i32*, i32** %s.addr, align 4
  %16 = load i32, i32* %15, align 4
  %add5 = add i32 %16, %14
  store i32 %add5, i32* %15, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define internal i32 @count_bit_noESC_from2(i32* %ix, i32* %end, i32 %max, i32* %s) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %max.addr = alloca i32, align 4
  %s.addr = alloca i32*, align 4
  %t1 = alloca i32, align 4
  %xlen = alloca i32, align 4
  %table = alloca i32*, align 4
  %sum = alloca i32, align 4
  %sum2 = alloca i32, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 %max, i32* %max.addr, align 4
  store i32* %s, i32** %s.addr, align 4
  %0 = load i32, i32* %max.addr, align 4
  %sub = sub nsw i32 %0, 1
  %arrayidx = getelementptr inbounds [15 x i32], [15 x i32]* @huf_tbl_noESC, i32 0, i32 %sub
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %t1, align 4
  %2 = load i32, i32* %t1, align 4
  %arrayidx1 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %2
  %xlen2 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx1, i32 0, i32 0
  %3 = load i32, i32* %xlen2, align 16
  store i32 %3, i32* %xlen, align 4
  %4 = load i32, i32* %t1, align 4
  %cmp = icmp eq i32 %4, 2
  %5 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32* getelementptr inbounds ([9 x i32], [9 x i32]* @table23, i32 0, i32 0), i32* getelementptr inbounds ([16 x i32], [16 x i32]* @table56, i32 0, i32 0)
  store i32* %cond, i32** %table, align 4
  store i32 0, i32* %sum, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %6 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %6, i32 1
  store i32* %incdec.ptr, i32** %ix.addr, align 4
  %7 = load i32, i32* %6, align 4
  store i32 %7, i32* %x0, align 4
  %8 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i32, i32* %8, i32 1
  store i32* %incdec.ptr3, i32** %ix.addr, align 4
  %9 = load i32, i32* %8, align 4
  store i32 %9, i32* %x1, align 4
  %10 = load i32*, i32** %table, align 4
  %11 = load i32, i32* %x0, align 4
  %12 = load i32, i32* %xlen, align 4
  %mul = mul i32 %11, %12
  %13 = load i32, i32* %x1, align 4
  %add = add i32 %mul, %13
  %arrayidx4 = getelementptr inbounds i32, i32* %10, i32 %add
  %14 = load i32, i32* %arrayidx4, align 4
  %15 = load i32, i32* %sum, align 4
  %add5 = add i32 %15, %14
  store i32 %add5, i32* %sum, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %16 = load i32*, i32** %ix.addr, align 4
  %17 = load i32*, i32** %end.addr, align 4
  %cmp6 = icmp ult i32* %16, %17
  br i1 %cmp6, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %18 = load i32, i32* %sum, align 4
  %and = and i32 %18, 65535
  store i32 %and, i32* %sum2, align 4
  %19 = load i32, i32* %sum, align 4
  %shr = lshr i32 %19, 16
  store i32 %shr, i32* %sum, align 4
  %20 = load i32, i32* %sum, align 4
  %21 = load i32, i32* %sum2, align 4
  %cmp7 = icmp ugt i32 %20, %21
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %do.end
  %22 = load i32, i32* %sum2, align 4
  store i32 %22, i32* %sum, align 4
  %23 = load i32, i32* %t1, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %t1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %do.end
  %24 = load i32, i32* %sum, align 4
  %25 = load i32*, i32** %s.addr, align 4
  %26 = load i32, i32* %25, align 4
  %add8 = add i32 %26, %24
  store i32 %add8, i32* %25, align 4
  %27 = load i32, i32* %t1, align 4
  ret i32 %27
}

; Function Attrs: noinline nounwind optnone
define internal i32 @count_bit_noESC_from3(i32* %ix, i32* %end, i32 %max, i32* %s) #0 {
entry:
  %ix.addr = alloca i32*, align 4
  %end.addr = alloca i32*, align 4
  %max.addr = alloca i32, align 4
  %s.addr = alloca i32*, align 4
  %t1 = alloca i32, align 4
  %sum1 = alloca i32, align 4
  %sum2 = alloca i32, align 4
  %sum3 = alloca i32, align 4
  %xlen = alloca i32, align 4
  %hlen1 = alloca i8*, align 4
  %hlen2 = alloca i8*, align 4
  %hlen3 = alloca i8*, align 4
  %t = alloca i32, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x = alloca i32, align 4
  store i32* %ix, i32** %ix.addr, align 4
  store i32* %end, i32** %end.addr, align 4
  store i32 %max, i32* %max.addr, align 4
  store i32* %s, i32** %s.addr, align 4
  %0 = load i32, i32* %max.addr, align 4
  %sub = sub nsw i32 %0, 1
  %arrayidx = getelementptr inbounds [15 x i32], [15 x i32]* @huf_tbl_noESC, i32 0, i32 %sub
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %t1, align 4
  store i32 0, i32* %sum1, align 4
  store i32 0, i32* %sum2, align 4
  store i32 0, i32* %sum3, align 4
  %2 = load i32, i32* %t1, align 4
  %arrayidx1 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %2
  %xlen2 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx1, i32 0, i32 0
  %3 = load i32, i32* %xlen2, align 16
  store i32 %3, i32* %xlen, align 4
  %4 = load i32, i32* %t1, align 4
  %arrayidx3 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %4
  %hlen = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx3, i32 0, i32 3
  %5 = load i8*, i8** %hlen, align 4
  store i8* %5, i8** %hlen1, align 4
  %6 = load i32, i32* %t1, align 4
  %add = add nsw i32 %6, 1
  %arrayidx4 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %add
  %hlen5 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx4, i32 0, i32 3
  %7 = load i8*, i8** %hlen5, align 4
  store i8* %7, i8** %hlen2, align 4
  %8 = load i32, i32* %t1, align 4
  %add6 = add nsw i32 %8, 2
  %arrayidx7 = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %add6
  %hlen8 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %arrayidx7, i32 0, i32 3
  %9 = load i8*, i8** %hlen8, align 4
  store i8* %9, i8** %hlen3, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %10 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %10, i32 1
  store i32* %incdec.ptr, i32** %ix.addr, align 4
  %11 = load i32, i32* %10, align 4
  store i32 %11, i32* %x0, align 4
  %12 = load i32*, i32** %ix.addr, align 4
  %incdec.ptr9 = getelementptr inbounds i32, i32* %12, i32 1
  store i32* %incdec.ptr9, i32** %ix.addr, align 4
  %13 = load i32, i32* %12, align 4
  store i32 %13, i32* %x1, align 4
  %14 = load i32, i32* %x0, align 4
  %15 = load i32, i32* %xlen, align 4
  %mul = mul i32 %14, %15
  %16 = load i32, i32* %x1, align 4
  %add10 = add i32 %mul, %16
  store i32 %add10, i32* %x, align 4
  %17 = load i8*, i8** %hlen1, align 4
  %18 = load i32, i32* %x, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load i8, i8* %arrayidx11, align 1
  %conv = zext i8 %19 to i32
  %20 = load i32, i32* %sum1, align 4
  %add12 = add i32 %20, %conv
  store i32 %add12, i32* %sum1, align 4
  %21 = load i8*, i8** %hlen2, align 4
  %22 = load i32, i32* %x, align 4
  %arrayidx13 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx13, align 1
  %conv14 = zext i8 %23 to i32
  %24 = load i32, i32* %sum2, align 4
  %add15 = add i32 %24, %conv14
  store i32 %add15, i32* %sum2, align 4
  %25 = load i8*, i8** %hlen3, align 4
  %26 = load i32, i32* %x, align 4
  %arrayidx16 = getelementptr inbounds i8, i8* %25, i32 %26
  %27 = load i8, i8* %arrayidx16, align 1
  %conv17 = zext i8 %27 to i32
  %28 = load i32, i32* %sum3, align 4
  %add18 = add i32 %28, %conv17
  store i32 %add18, i32* %sum3, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %29 = load i32*, i32** %ix.addr, align 4
  %30 = load i32*, i32** %end.addr, align 4
  %cmp = icmp ult i32* %29, %30
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %31 = load i32, i32* %t1, align 4
  store i32 %31, i32* %t, align 4
  %32 = load i32, i32* %sum1, align 4
  %33 = load i32, i32* %sum2, align 4
  %cmp20 = icmp ugt i32 %32, %33
  br i1 %cmp20, label %if.then, label %if.end

if.then:                                          ; preds = %do.end
  %34 = load i32, i32* %sum2, align 4
  store i32 %34, i32* %sum1, align 4
  %35 = load i32, i32* %t, align 4
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %t, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %do.end
  %36 = load i32, i32* %sum1, align 4
  %37 = load i32, i32* %sum3, align 4
  %cmp22 = icmp ugt i32 %36, %37
  br i1 %cmp22, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end
  %38 = load i32, i32* %sum3, align 4
  store i32 %38, i32* %sum1, align 4
  %39 = load i32, i32* %t1, align 4
  %add25 = add nsw i32 %39, 2
  store i32 %add25, i32* %t, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end
  %40 = load i32, i32* %sum1, align 4
  %41 = load i32*, i32** %s.addr, align 4
  %42 = load i32, i32* %41, align 4
  %add27 = add i32 %42, %40
  store i32 %add27, i32* %41, align 4
  %43 = load i32, i32* %t, align 4
  ret i32 %43
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
