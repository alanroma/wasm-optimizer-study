; ModuleID = 'layer2.c'
source_filename = "layer2.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.al_table2 = type { i16, i16 }
%struct.mpstr_tag = type { %struct.buf*, %struct.buf*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.frame, %struct.III_sideinfo, [2 x [3904 x i8]], [2 x [2 x [576 x float]]], [2 x i32], i32, i32, [2 x [2 x [272 x float]]], i32, i32, i32, i8*, %struct.plotting_data*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.buf = type { i8*, i32, i32, %struct.buf*, %struct.buf* }
%struct.frame = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.al_table2*, i32, i32 }
%struct.III_sideinfo = type { i32, i32, [2 x %struct.anon] }
%struct.anon = type { [2 x %struct.gr_info_s] }
%struct.gr_info_s = type { i32, i32, i32, i32, i32, i32, [3 x i32], [3 x i32], [3 x i32], i32, i32, i32, i32, i32, i32, i32, [3 x float*], float* }
%struct.plotting_data = type opaque
%struct.sideinfo_layer_II_struct = type { [32 x [2 x i8]], [32 x [2 x [3 x i8]]] }

@alloc_0 = hidden constant [288 x %struct.al_table2] [%struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }], align 16
@alloc_1 = hidden constant [300 x %struct.al_table2] [%struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 16, i16 -32767 }], align 16
@alloc_2 = hidden constant [80 x %struct.al_table2] [%struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }], align 16
@alloc_3 = hidden constant [112 x %struct.al_table2] [%struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 15, i16 -16383 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }], align 16
@alloc_4 = hidden constant [196 x %struct.al_table2] [%struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 4, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 3, i16 -3 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 8, i16 -127 }, %struct.al_table2 { i16 9, i16 -255 }, %struct.al_table2 { i16 10, i16 -511 }, %struct.al_table2 { i16 11, i16 -1023 }, %struct.al_table2 { i16 12, i16 -2047 }, %struct.al_table2 { i16 13, i16 -4095 }, %struct.al_table2 { i16 14, i16 -8191 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 3, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 4, i16 -7 }, %struct.al_table2 { i16 5, i16 -15 }, %struct.al_table2 { i16 6, i16 -31 }, %struct.al_table2 { i16 7, i16 -63 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }, %struct.al_table2 { i16 2, i16 0 }, %struct.al_table2 { i16 5, i16 3 }, %struct.al_table2 { i16 7, i16 5 }, %struct.al_table2 { i16 10, i16 9 }], align 16
@hip_init_tables_layer2.mulmul = internal constant [27 x double] [double 0.000000e+00, double 0xBFE5555555555555, double 0x3FE5555555555555, double 0x3FD2492492492492, double 0x3FC1111111111111, double 0x3FB0842108421084, double 0x3FA0410410410410, double 0x3F90204081020408, double 0x3F80101010101010, double 0x3F70080402010080, double 0x3F60040100401004, double 0x3F50020040080100, double 0x3F40010010010010, double 0x3F30008004002001, double 0x3F20004001000400, double 0x3F10002000400080, double 0x3F00001000100010, double -8.000000e-01, double -4.000000e-01, double 4.000000e-01, double 8.000000e-01, double 0xBFEC71C71C71C71C, double 0xBFDC71C71C71C71C, double 0xBFCC71C71C71C71C, double 0x3FCC71C71C71C71C, double 0x3FDC71C71C71C71C, double 0x3FEC71C71C71C71C], align 16
@hip_init_tables_layer2.base = internal constant [3 x [9 x i8]] [[9 x i8] c"\01\00\02\00\00\00\00\00\00", [9 x i8] c"\11\12\00\13\14\00\00\00\00", [9 x i8] c"\15\01\16\17\00\18\19\02\1A"], align 16
@hip_init_tables_layer2.tablen = internal constant [3 x i32] [i32 3, i32 5, i32 9], align 4
@hip_init_tables_layer2.itable = internal global i8* null, align 4
@hip_init_tables_layer2.tables = internal global [3 x i8*] [i8* getelementptr inbounds ([96 x i8], [96 x i8]* @grp_3tab, i32 0, i32 0), i8* getelementptr inbounds ([384 x i8], [384 x i8]* @grp_5tab, i32 0, i32 0), i8* getelementptr inbounds ([3072 x i8], [3072 x i8]* @grp_9tab, i32 0, i32 0)], align 4
@grp_3tab = internal global [96 x i8] zeroinitializer, align 16
@grp_5tab = internal global [384 x i8] zeroinitializer, align 16
@grp_9tab = internal global [3072 x i8] zeroinitializer, align 16
@gd_are_hip_tables_layer2_initialized = internal global i32 0, align 4
@muls = external global [27 x [64 x float]], align 16
@II_select_table.translate = internal constant <{ [2 x [16 x i32]], <{ <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, <{ i32, i32, i32, [13 x i32] }> }>, [2 x [16 x i32]] }> <{ [2 x [16 x i32]] [[16 x i32] [i32 0, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0], [16 x i32] [i32 0, i32 2, i32 2, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0]], <{ <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, <{ i32, i32, i32, [13 x i32] }> }> <{ <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }> <{ i32 0, i32 2, i32 2, i32 2, i32 2, i32 2, i32 2, [9 x i32] zeroinitializer }>, <{ i32, i32, i32, [13 x i32] }> <{ i32 0, i32 2, i32 2, [13 x i32] zeroinitializer }> }>, [2 x [16 x i32]] [[16 x i32] [i32 0, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0], [16 x i32] [i32 0, i32 3, i32 3, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 1, i32 0]] }>, align 16
@II_select_table.tables = internal global [5 x %struct.al_table2*] [%struct.al_table2* getelementptr inbounds ([288 x %struct.al_table2], [288 x %struct.al_table2]* @alloc_0, i32 0, i32 0), %struct.al_table2* getelementptr inbounds ([300 x %struct.al_table2], [300 x %struct.al_table2]* @alloc_1, i32 0, i32 0), %struct.al_table2* getelementptr inbounds ([80 x %struct.al_table2], [80 x %struct.al_table2]* @alloc_2, i32 0, i32 0), %struct.al_table2* getelementptr inbounds ([112 x %struct.al_table2], [112 x %struct.al_table2]* @alloc_3, i32 0, i32 0), %struct.al_table2* getelementptr inbounds ([196 x %struct.al_table2], [196 x %struct.al_table2]* @alloc_4, i32 0, i32 0)], align 16
@II_select_table.sblims = internal constant [5 x i32] [i32 27, i32 30, i32 8, i32 12, i32 30], align 16
@grp_table_select.dummy_table = internal global [3 x i8] zeroinitializer, align 1

; Function Attrs: noinline nounwind optnone
define hidden void @hip_init_tables_layer2() #0 {
entry:
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %l = alloca i32, align 4
  %len = alloca i32, align 4
  %table = alloca float*, align 4
  %m = alloca double, align 8
  %0 = load i32, i32* @gd_are_hip_tables_layer2_initialized, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end44

if.end:                                           ; preds = %entry
  store i32 1, i32* @gd_are_hip_tables_layer2_initialized, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* @hip_init_tables_layer2.tables, i32 0, i32 %2
  %3 = load i8*, i8** %arrayidx, align 4
  store i8* %3, i8** @hip_init_tables_layer2.itable, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [3 x i32], [3 x i32]* @hip_init_tables_layer2.tablen, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx1, align 4
  store i32 %5, i32* %len, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc22, %for.body
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %len, align 4
  %cmp3 = icmp slt i32 %6, %7
  br i1 %cmp3, label %for.body4, label %for.end24

for.body4:                                        ; preds = %for.cond2
  store i32 0, i32* %k, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc19, %for.body4
  %8 = load i32, i32* %k, align 4
  %9 = load i32, i32* %len, align 4
  %cmp6 = icmp slt i32 %8, %9
  br i1 %cmp6, label %for.body7, label %for.end21

for.body7:                                        ; preds = %for.cond5
  store i32 0, i32* %l, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body7
  %10 = load i32, i32* %l, align 4
  %11 = load i32, i32* %len, align 4
  %cmp9 = icmp slt i32 %10, %11
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %12 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [3 x [9 x i8]], [3 x [9 x i8]]* @hip_init_tables_layer2.base, i32 0, i32 %12
  %13 = load i32, i32* %l, align 4
  %arrayidx12 = getelementptr inbounds [9 x i8], [9 x i8]* %arrayidx11, i32 0, i32 %13
  %14 = load i8, i8* %arrayidx12, align 1
  %15 = load i8*, i8** @hip_init_tables_layer2.itable, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %15, i32 1
  store i8* %incdec.ptr, i8** @hip_init_tables_layer2.itable, align 4
  store i8 %14, i8* %15, align 1
  %16 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds [3 x [9 x i8]], [3 x [9 x i8]]* @hip_init_tables_layer2.base, i32 0, i32 %16
  %17 = load i32, i32* %k, align 4
  %arrayidx14 = getelementptr inbounds [9 x i8], [9 x i8]* %arrayidx13, i32 0, i32 %17
  %18 = load i8, i8* %arrayidx14, align 1
  %19 = load i8*, i8** @hip_init_tables_layer2.itable, align 4
  %incdec.ptr15 = getelementptr inbounds i8, i8* %19, i32 1
  store i8* %incdec.ptr15, i8** @hip_init_tables_layer2.itable, align 4
  store i8 %18, i8* %19, align 1
  %20 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [3 x [9 x i8]], [3 x [9 x i8]]* @hip_init_tables_layer2.base, i32 0, i32 %20
  %21 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds [9 x i8], [9 x i8]* %arrayidx16, i32 0, i32 %21
  %22 = load i8, i8* %arrayidx17, align 1
  %23 = load i8*, i8** @hip_init_tables_layer2.itable, align 4
  %incdec.ptr18 = getelementptr inbounds i8, i8* %23, i32 1
  store i8* %incdec.ptr18, i8** @hip_init_tables_layer2.itable, align 4
  store i8 %22, i8* %23, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %24 = load i32, i32* %l, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %for.inc19

for.inc19:                                        ; preds = %for.end
  %25 = load i32, i32* %k, align 4
  %inc20 = add nsw i32 %25, 1
  store i32 %inc20, i32* %k, align 4
  br label %for.cond5

for.end21:                                        ; preds = %for.cond5
  br label %for.inc22

for.inc22:                                        ; preds = %for.end21
  %26 = load i32, i32* %j, align 4
  %inc23 = add nsw i32 %26, 1
  store i32 %inc23, i32* %j, align 4
  br label %for.cond2

for.end24:                                        ; preds = %for.cond2
  br label %for.inc25

for.inc25:                                        ; preds = %for.end24
  %27 = load i32, i32* %i, align 4
  %inc26 = add nsw i32 %27, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  store i32 0, i32* %k, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc42, %for.end27
  %28 = load i32, i32* %k, align 4
  %cmp29 = icmp slt i32 %28, 27
  br i1 %cmp29, label %for.body30, label %for.end44

for.body30:                                       ; preds = %for.cond28
  %29 = load i32, i32* %k, align 4
  %arrayidx31 = getelementptr inbounds [27 x double], [27 x double]* @hip_init_tables_layer2.mulmul, i32 0, i32 %29
  %30 = load double, double* %arrayidx31, align 8
  store double %30, double* %m, align 8
  %31 = load i32, i32* %k, align 4
  %arrayidx32 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %31
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %arrayidx32, i32 0, i32 0
  store float* %arraydecay, float** %table, align 4
  store i32 3, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc38, %for.body30
  %32 = load i32, i32* %i, align 4
  %cmp34 = icmp slt i32 %32, 63
  br i1 %cmp34, label %for.body35, label %for.end40

for.body35:                                       ; preds = %for.cond33
  %33 = load double, double* %m, align 8
  %34 = load i32, i32* %j, align 4
  %conv = sitofp i32 %34 to double
  %div = fdiv double %conv, 3.000000e+00
  %35 = call double @llvm.pow.f64(double 2.000000e+00, double %div)
  %mul = fmul double %33, %35
  %conv36 = fptrunc double %mul to float
  %36 = load float*, float** %table, align 4
  %incdec.ptr37 = getelementptr inbounds float, float* %36, i32 1
  store float* %incdec.ptr37, float** %table, align 4
  store float %conv36, float* %36, align 4
  br label %for.inc38

for.inc38:                                        ; preds = %for.body35
  %37 = load i32, i32* %i, align 4
  %inc39 = add nsw i32 %37, 1
  store i32 %inc39, i32* %i, align 4
  %38 = load i32, i32* %j, align 4
  %dec = add nsw i32 %38, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond33

for.end40:                                        ; preds = %for.cond33
  %39 = load float*, float** %table, align 4
  %incdec.ptr41 = getelementptr inbounds float, float* %39, i32 1
  store float* %incdec.ptr41, float** %table, align 4
  store float 0.000000e+00, float* %39, align 4
  br label %for.inc42

for.inc42:                                        ; preds = %for.end40
  %40 = load i32, i32* %k, align 4
  %inc43 = add nsw i32 %40, 1
  store i32 %inc43, i32* %k, align 4
  br label %for.cond28

for.end44:                                        ; preds = %if.then, %for.cond28
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @decode_layer2_sideinfo(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @decode_layer2_frame(%struct.mpstr_tag* %mp, i8* %pcm_sample, i32* %pcm_point) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %pcm_sample.addr = alloca i8*, align 4
  %pcm_point.addr = alloca i32*, align 4
  %fraction = alloca [2 x [4 x [32 x float]]], align 16
  %si = alloca %struct.sideinfo_layer_II_struct, align 1
  %fr = alloca %struct.frame*, align 4
  %single = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %clip = alloca i32, align 4
  %p1 = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %pcm_sample, i8** %pcm_sample.addr, align 4
  store i32* %pcm_point, i32** %pcm_point.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 17
  store %struct.frame* %fr1, %struct.frame** %fr, align 4
  %1 = load %struct.frame*, %struct.frame** %fr, align 4
  %single2 = getelementptr inbounds %struct.frame, %struct.frame* %1, i32 0, i32 1
  %2 = load i32, i32* %single2, align 4
  store i32 %2, i32* %single, align 4
  store i32 0, i32* %clip, align 4
  %3 = load %struct.frame*, %struct.frame** %fr, align 4
  call void @II_select_table(%struct.frame* %3)
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %5 = load %struct.frame*, %struct.frame** %fr, align 4
  call void @II_step_one(%struct.mpstr_tag* %4, %struct.sideinfo_layer_II_struct* %si, %struct.frame* %5)
  %6 = load %struct.frame*, %struct.frame** %fr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 0
  %7 = load i32, i32* %stereo, align 4
  %cmp = icmp eq i32 %7, 1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load i32, i32* %single, align 4
  %cmp3 = icmp eq i32 %8, 3
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %single, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %9 = load i32, i32* %single, align 4
  %cmp4 = icmp sge i32 %9, 0
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc12, %if.then5
  %10 = load i32, i32* %i, align 4
  %cmp6 = icmp slt i32 %10, 12
  br i1 %cmp6, label %for.body, label %for.end14

for.body:                                         ; preds = %for.cond
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %12 = load %struct.frame*, %struct.frame** %fr, align 4
  %13 = load i32, i32* %i, align 4
  %shr = ashr i32 %13, 2
  %arraydecay = getelementptr inbounds [2 x [4 x [32 x float]]], [2 x [4 x [32 x float]]]* %fraction, i32 0, i32 0
  call void @II_step_two(%struct.mpstr_tag* %11, %struct.sideinfo_layer_II_struct* %si, %struct.frame* %12, i32 %shr, [4 x [32 x float]]* %arraydecay)
  store i32 0, i32* %j, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4
  %cmp8 = icmp slt i32 %14, 3
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %15 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %16 = load i32, i32* %single, align 4
  %arrayidx = getelementptr inbounds [2 x [4 x [32 x float]]], [2 x [4 x [32 x float]]]* %fraction, i32 0, i32 %16
  %17 = load i32, i32* %j, align 4
  %arrayidx10 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx, i32 0, i32 %17
  %arraydecay11 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx10, i32 0, i32 0
  %18 = load i8*, i8** %pcm_sample.addr, align 4
  %19 = load i32*, i32** %pcm_point.addr, align 4
  %call = call i32 @synth_1to1_mono(%struct.mpstr_tag* %15, float* %arraydecay11, i8* %18, i32* %19)
  %20 = load i32, i32* %clip, align 4
  %add = add nsw i32 %20, %call
  store i32 %add, i32* %clip, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %21 = load i32, i32* %j, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc12

for.inc12:                                        ; preds = %for.end
  %22 = load i32, i32* %i, align 4
  %inc13 = add nsw i32 %22, 1
  store i32 %inc13, i32* %i, align 4
  br label %for.cond

for.end14:                                        ; preds = %for.cond
  br label %if.end39

if.else:                                          ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc36, %if.else
  %23 = load i32, i32* %i, align 4
  %cmp16 = icmp slt i32 %23, 12
  br i1 %cmp16, label %for.body17, label %for.end38

for.body17:                                       ; preds = %for.cond15
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %25 = load %struct.frame*, %struct.frame** %fr, align 4
  %26 = load i32, i32* %i, align 4
  %shr18 = ashr i32 %26, 2
  %arraydecay19 = getelementptr inbounds [2 x [4 x [32 x float]]], [2 x [4 x [32 x float]]]* %fraction, i32 0, i32 0
  call void @II_step_two(%struct.mpstr_tag* %24, %struct.sideinfo_layer_II_struct* %si, %struct.frame* %25, i32 %shr18, [4 x [32 x float]]* %arraydecay19)
  store i32 0, i32* %j, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc33, %for.body17
  %27 = load i32, i32* %j, align 4
  %cmp21 = icmp slt i32 %27, 3
  br i1 %cmp21, label %for.body22, label %for.end35

for.body22:                                       ; preds = %for.cond20
  %28 = load i32*, i32** %pcm_point.addr, align 4
  %29 = load i32, i32* %28, align 4
  store i32 %29, i32* %p1, align 4
  %30 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arrayidx23 = getelementptr inbounds [2 x [4 x [32 x float]]], [2 x [4 x [32 x float]]]* %fraction, i32 0, i32 0
  %31 = load i32, i32* %j, align 4
  %arrayidx24 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx23, i32 0, i32 %31
  %arraydecay25 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx24, i32 0, i32 0
  %32 = load i8*, i8** %pcm_sample.addr, align 4
  %call26 = call i32 @synth_1to1(%struct.mpstr_tag* %30, float* %arraydecay25, i32 0, i8* %32, i32* %p1)
  %33 = load i32, i32* %clip, align 4
  %add27 = add nsw i32 %33, %call26
  store i32 %add27, i32* %clip, align 4
  %34 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arrayidx28 = getelementptr inbounds [2 x [4 x [32 x float]]], [2 x [4 x [32 x float]]]* %fraction, i32 0, i32 1
  %35 = load i32, i32* %j, align 4
  %arrayidx29 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx28, i32 0, i32 %35
  %arraydecay30 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx29, i32 0, i32 0
  %36 = load i8*, i8** %pcm_sample.addr, align 4
  %37 = load i32*, i32** %pcm_point.addr, align 4
  %call31 = call i32 @synth_1to1(%struct.mpstr_tag* %34, float* %arraydecay30, i32 1, i8* %36, i32* %37)
  %38 = load i32, i32* %clip, align 4
  %add32 = add nsw i32 %38, %call31
  store i32 %add32, i32* %clip, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body22
  %39 = load i32, i32* %j, align 4
  %inc34 = add nsw i32 %39, 1
  store i32 %inc34, i32* %j, align 4
  br label %for.cond20

for.end35:                                        ; preds = %for.cond20
  br label %for.inc36

for.inc36:                                        ; preds = %for.end35
  %40 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %40, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond15

for.end38:                                        ; preds = %for.cond15
  br label %if.end39

if.end39:                                         ; preds = %for.end38, %for.end14
  %41 = load i32, i32* %clip, align 4
  ret i32 %41
}

; Function Attrs: noinline nounwind optnone
define internal void @II_select_table(%struct.frame* %fr) #0 {
entry:
  %fr.addr = alloca %struct.frame*, align 4
  %table = alloca i32, align 4
  %sblim = alloca i32, align 4
  store %struct.frame* %fr, %struct.frame** %fr.addr, align 4
  %0 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf = getelementptr inbounds %struct.frame, %struct.frame* %0, i32 0, i32 2
  %1 = load i32, i32* %lsf, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 4, i32* %table, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %2 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency = getelementptr inbounds %struct.frame, %struct.frame* %2, i32 0, i32 8
  %3 = load i32, i32* %sampling_frequency, align 4
  %arrayidx = getelementptr inbounds [3 x [2 x [16 x i32]]], [3 x [2 x [16 x i32]]]* bitcast (<{ [2 x [16 x i32]], <{ <{ i32, i32, i32, i32, i32, i32, i32, [9 x i32] }>, <{ i32, i32, i32, [13 x i32] }> }>, [2 x [16 x i32]] }>* @II_select_table.translate to [3 x [2 x [16 x i32]]]*), i32 0, i32 %3
  %4 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %4, i32 0, i32 0
  %5 = load i32, i32* %stereo, align 4
  %sub = sub nsw i32 2, %5
  %arrayidx1 = getelementptr inbounds [2 x [16 x i32]], [2 x [16 x i32]]* %arrayidx, i32 0, i32 %sub
  %6 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 7
  %7 = load i32, i32* %bitrate_index, align 4
  %arrayidx2 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx1, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx2, align 4
  store i32 %8, i32* %table, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %9 = load i32, i32* %table, align 4
  %arrayidx3 = getelementptr inbounds [5 x i32], [5 x i32]* @II_select_table.sblims, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx3, align 4
  store i32 %10, i32* %sblim, align 4
  %11 = load i32, i32* %table, align 4
  %arrayidx4 = getelementptr inbounds [5 x %struct.al_table2*], [5 x %struct.al_table2*]* @II_select_table.tables, i32 0, i32 %11
  %12 = load %struct.al_table2*, %struct.al_table2** %arrayidx4, align 4
  %13 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %alloc = getelementptr inbounds %struct.frame, %struct.frame* %13, i32 0, i32 18
  store %struct.al_table2* %12, %struct.al_table2** %alloc, align 4
  %14 = load i32, i32* %sblim, align 4
  %15 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %II_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %15, i32 0, i32 17
  store i32 %14, i32* %II_sblimit, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @II_step_one(%struct.mpstr_tag* %mp, %struct.sideinfo_layer_II_struct* %si, %struct.frame* %fr) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %si.addr = alloca %struct.sideinfo_layer_II_struct*, align 4
  %fr.addr = alloca %struct.frame*, align 4
  %nch = alloca i32, align 4
  %sblimit = alloca i32, align 4
  %jsbound = alloca i32, align 4
  %alloc1 = alloca %struct.al_table2*, align 4
  %scfsi = alloca [32 x [2 x i8]], align 16
  %i = alloca i32, align 4
  %ch = alloca i32, align 4
  %step = alloca i16, align 2
  %b0 = alloca i8, align 1
  %b1 = alloca i8, align 1
  %step18 = alloca i16, align 2
  %b020 = alloca i8, align 1
  %n0 = alloca i8, align 1
  %n1 = alloca i8, align 1
  %b045 = alloca i8, align 1
  %b154 = alloca i8, align 1
  %step75 = alloca i16, align 2
  %b077 = alloca i8, align 1
  %n093 = alloca i8, align 1
  %b097 = alloca i8, align 1
  %s0 = alloca i8, align 1
  %s1 = alloca i8, align 1
  %s2 = alloca i8, align 1
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store %struct.sideinfo_layer_II_struct* %si, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  store %struct.frame* %fr, %struct.frame** %fr.addr, align 4
  %0 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %0, i32 0, i32 0
  %1 = load i32, i32* %stereo, align 4
  store i32 %1, i32* %nch, align 4
  %2 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %II_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %2, i32 0, i32 17
  %3 = load i32, i32* %II_sblimit, align 4
  store i32 %3, i32* %sblimit, align 4
  %4 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode = getelementptr inbounds %struct.frame, %struct.frame* %4, i32 0, i32 11
  %5 = load i32, i32* %mode, align 4
  %cmp = icmp eq i32 %5, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode_ext = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 12
  %7 = load i32, i32* %mode_ext, align 4
  %shl = shl i32 %7, 2
  %add = add nsw i32 %shl, 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %II_sblimit1 = getelementptr inbounds %struct.frame, %struct.frame* %8, i32 0, i32 17
  %9 = load i32, i32* %II_sblimit1, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ %9, %cond.false ]
  store i32 %cond, i32* %jsbound, align 4
  %10 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %alloc = getelementptr inbounds %struct.frame, %struct.frame* %10, i32 0, i32 18
  %11 = load %struct.al_table2*, %struct.al_table2** %alloc, align 4
  store %struct.al_table2* %11, %struct.al_table2** %alloc1, align 4
  %12 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %13 = bitcast %struct.sideinfo_layer_II_struct* %12 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %13, i8 0, i32 256, i1 false)
  %14 = load i32, i32* %jsbound, align 4
  %15 = load i32, i32* %sblimit, align 4
  %cmp2 = icmp sgt i32 %14, %15
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %16 = load i32, i32* %sblimit, align 4
  store i32 %16, i32* %jsbound, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %17 = load i32, i32* %nch, align 4
  %cmp3 = icmp eq i32 %17, 2
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %jsbound, align 4
  %cmp5 = icmp slt i32 %18, %19
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %bits = getelementptr inbounds %struct.al_table2, %struct.al_table2* %20, i32 0, i32 0
  %21 = load i16, i16* %bits, align 2
  store i16 %21, i16* %step, align 2
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %23 = load i16, i16* %step, align 2
  %conv = sext i16 %23 to i32
  %call = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %22, i32 %conv)
  store i8 %call, i8* %b0, align 1
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %25 = load i16, i16* %step, align 2
  %conv6 = sext i16 %25 to i32
  %call7 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %24, i32 %conv6)
  store i8 %call7, i8* %b1, align 1
  %26 = load i16, i16* %step, align 2
  %conv8 = sext i16 %26 to i32
  %shl9 = shl i32 1, %conv8
  %27 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %add.ptr = getelementptr inbounds %struct.al_table2, %struct.al_table2* %27, i32 %shl9
  store %struct.al_table2* %add.ptr, %struct.al_table2** %alloc1, align 4
  %28 = load i8, i8* %b0, align 1
  %29 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %29, i32 0, i32 0
  %30 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation, i32 0, i32 %30
  %arrayidx10 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx, i32 0, i32 0
  store i8 %28, i8* %arrayidx10, align 1
  %31 = load i8, i8* %b1, align 1
  %32 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation11 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %32, i32 0, i32 0
  %33 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation11, i32 0, i32 %33
  %arrayidx13 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx12, i32 0, i32 1
  store i8 %31, i8* %arrayidx13, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %i, align 4
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load i32, i32* %jsbound, align 4
  store i32 %35, i32* %i, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc32, %for.end
  %36 = load i32, i32* %i, align 4
  %37 = load i32, i32* %sblimit, align 4
  %cmp15 = icmp slt i32 %36, %37
  br i1 %cmp15, label %for.body17, label %for.end34

for.body17:                                       ; preds = %for.cond14
  %38 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %bits19 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %38, i32 0, i32 0
  %39 = load i16, i16* %bits19, align 2
  store i16 %39, i16* %step18, align 2
  %40 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %41 = load i16, i16* %step18, align 2
  %conv21 = sext i16 %41 to i32
  %call22 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %40, i32 %conv21)
  store i8 %call22, i8* %b020, align 1
  %42 = load i16, i16* %step18, align 2
  %conv23 = sext i16 %42 to i32
  %shl24 = shl i32 1, %conv23
  %43 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %add.ptr25 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %43, i32 %shl24
  store %struct.al_table2* %add.ptr25, %struct.al_table2** %alloc1, align 4
  %44 = load i8, i8* %b020, align 1
  %45 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation26 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %45, i32 0, i32 0
  %46 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation26, i32 0, i32 %46
  %arrayidx28 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx27, i32 0, i32 0
  store i8 %44, i8* %arrayidx28, align 1
  %47 = load i8, i8* %b020, align 1
  %48 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation29 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %48, i32 0, i32 0
  %49 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation29, i32 0, i32 %49
  %arrayidx31 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx30, i32 0, i32 1
  store i8 %47, i8* %arrayidx31, align 1
  br label %for.inc32

for.inc32:                                        ; preds = %for.body17
  %50 = load i32, i32* %i, align 4
  %inc33 = add nsw i32 %50, 1
  store i32 %inc33, i32* %i, align 4
  br label %for.cond14

for.end34:                                        ; preds = %for.cond14
  store i32 0, i32* %i, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc68, %for.end34
  %51 = load i32, i32* %i, align 4
  %52 = load i32, i32* %sblimit, align 4
  %cmp36 = icmp slt i32 %51, %52
  br i1 %cmp36, label %for.body38, label %for.end70

for.body38:                                       ; preds = %for.cond35
  %53 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation39 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %53, i32 0, i32 0
  %54 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation39, i32 0, i32 %54
  %arrayidx41 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx40, i32 0, i32 0
  %55 = load i8, i8* %arrayidx41, align 1
  store i8 %55, i8* %n0, align 1
  %56 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation42 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %56, i32 0, i32 0
  %57 = load i32, i32* %i, align 4
  %arrayidx43 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation42, i32 0, i32 %57
  %arrayidx44 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx43, i32 0, i32 1
  %58 = load i8, i8* %arrayidx44, align 1
  store i8 %58, i8* %n1, align 1
  %59 = load i8, i8* %n0, align 1
  %conv46 = zext i8 %59 to i32
  %tobool = icmp ne i32 %conv46, 0
  br i1 %tobool, label %cond.true47, label %cond.false50

cond.true47:                                      ; preds = %for.body38
  %60 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call48 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %60, i32 2)
  %conv49 = zext i8 %call48 to i32
  br label %cond.end51

cond.false50:                                     ; preds = %for.body38
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false50, %cond.true47
  %cond52 = phi i32 [ %conv49, %cond.true47 ], [ 0, %cond.false50 ]
  %conv53 = trunc i32 %cond52 to i8
  store i8 %conv53, i8* %b045, align 1
  %61 = load i8, i8* %n1, align 1
  %conv55 = zext i8 %61 to i32
  %tobool56 = icmp ne i32 %conv55, 0
  br i1 %tobool56, label %cond.true57, label %cond.false60

cond.true57:                                      ; preds = %cond.end51
  %62 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call58 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %62, i32 2)
  %conv59 = zext i8 %call58 to i32
  br label %cond.end61

cond.false60:                                     ; preds = %cond.end51
  br label %cond.end61

cond.end61:                                       ; preds = %cond.false60, %cond.true57
  %cond62 = phi i32 [ %conv59, %cond.true57 ], [ 0, %cond.false60 ]
  %conv63 = trunc i32 %cond62 to i8
  store i8 %conv63, i8* %b154, align 1
  %63 = load i8, i8* %b045, align 1
  %64 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scfsi, i32 0, i32 %64
  %arrayidx65 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx64, i32 0, i32 0
  store i8 %63, i8* %arrayidx65, align 2
  %65 = load i8, i8* %b154, align 1
  %66 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scfsi, i32 0, i32 %66
  %arrayidx67 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx66, i32 0, i32 1
  store i8 %65, i8* %arrayidx67, align 1
  br label %for.inc68

for.inc68:                                        ; preds = %cond.end61
  %67 = load i32, i32* %i, align 4
  %inc69 = add nsw i32 %67, 1
  store i32 %inc69, i32* %i, align 4
  br label %for.cond35

for.end70:                                        ; preds = %for.cond35
  br label %if.end112

if.else:                                          ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc86, %if.else
  %68 = load i32, i32* %i, align 4
  %69 = load i32, i32* %sblimit, align 4
  %cmp72 = icmp slt i32 %68, %69
  br i1 %cmp72, label %for.body74, label %for.end88

for.body74:                                       ; preds = %for.cond71
  %70 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %bits76 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %70, i32 0, i32 0
  %71 = load i16, i16* %bits76, align 2
  store i16 %71, i16* %step75, align 2
  %72 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %73 = load i16, i16* %step75, align 2
  %conv78 = sext i16 %73 to i32
  %call79 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %72, i32 %conv78)
  store i8 %call79, i8* %b077, align 1
  %74 = load i16, i16* %step75, align 2
  %conv80 = sext i16 %74 to i32
  %shl81 = shl i32 1, %conv80
  %75 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %add.ptr82 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %75, i32 %shl81
  store %struct.al_table2* %add.ptr82, %struct.al_table2** %alloc1, align 4
  %76 = load i8, i8* %b077, align 1
  %77 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation83 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %77, i32 0, i32 0
  %78 = load i32, i32* %i, align 4
  %arrayidx84 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation83, i32 0, i32 %78
  %arrayidx85 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx84, i32 0, i32 0
  store i8 %76, i8* %arrayidx85, align 1
  br label %for.inc86

for.inc86:                                        ; preds = %for.body74
  %79 = load i32, i32* %i, align 4
  %inc87 = add nsw i32 %79, 1
  store i32 %inc87, i32* %i, align 4
  br label %for.cond71

for.end88:                                        ; preds = %for.cond71
  store i32 0, i32* %i, align 4
  br label %for.cond89

for.cond89:                                       ; preds = %for.inc109, %for.end88
  %80 = load i32, i32* %i, align 4
  %81 = load i32, i32* %sblimit, align 4
  %cmp90 = icmp slt i32 %80, %81
  br i1 %cmp90, label %for.body92, label %for.end111

for.body92:                                       ; preds = %for.cond89
  %82 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation94 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %82, i32 0, i32 0
  %83 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation94, i32 0, i32 %83
  %arrayidx96 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx95, i32 0, i32 0
  %84 = load i8, i8* %arrayidx96, align 1
  store i8 %84, i8* %n093, align 1
  %85 = load i8, i8* %n093, align 1
  %conv98 = zext i8 %85 to i32
  %tobool99 = icmp ne i32 %conv98, 0
  br i1 %tobool99, label %cond.true100, label %cond.false103

cond.true100:                                     ; preds = %for.body92
  %86 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call101 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %86, i32 2)
  %conv102 = zext i8 %call101 to i32
  br label %cond.end104

cond.false103:                                    ; preds = %for.body92
  br label %cond.end104

cond.end104:                                      ; preds = %cond.false103, %cond.true100
  %cond105 = phi i32 [ %conv102, %cond.true100 ], [ 0, %cond.false103 ]
  %conv106 = trunc i32 %cond105 to i8
  store i8 %conv106, i8* %b097, align 1
  %87 = load i8, i8* %b097, align 1
  %88 = load i32, i32* %i, align 4
  %arrayidx107 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scfsi, i32 0, i32 %88
  %arrayidx108 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx107, i32 0, i32 0
  store i8 %87, i8* %arrayidx108, align 2
  br label %for.inc109

for.inc109:                                       ; preds = %cond.end104
  %89 = load i32, i32* %i, align 4
  %inc110 = add nsw i32 %89, 1
  store i32 %inc110, i32* %i, align 4
  br label %for.cond89

for.end111:                                       ; preds = %for.cond89
  br label %if.end112

if.end112:                                        ; preds = %for.end111, %for.end70
  store i32 0, i32* %i, align 4
  br label %for.cond113

for.cond113:                                      ; preds = %for.inc155, %if.end112
  %90 = load i32, i32* %i, align 4
  %91 = load i32, i32* %sblimit, align 4
  %cmp114 = icmp slt i32 %90, %91
  br i1 %cmp114, label %for.body116, label %for.end157

for.body116:                                      ; preds = %for.cond113
  store i32 0, i32* %ch, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc152, %for.body116
  %92 = load i32, i32* %ch, align 4
  %93 = load i32, i32* %nch, align 4
  %cmp118 = icmp slt i32 %92, %93
  br i1 %cmp118, label %for.body120, label %for.end154

for.body120:                                      ; preds = %for.cond117
  store i8 0, i8* %s0, align 1
  store i8 0, i8* %s1, align 1
  store i8 0, i8* %s2, align 1
  %94 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation121 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %94, i32 0, i32 0
  %95 = load i32, i32* %i, align 4
  %arrayidx122 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation121, i32 0, i32 %95
  %96 = load i32, i32* %ch, align 4
  %arrayidx123 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx122, i32 0, i32 %96
  %97 = load i8, i8* %arrayidx123, align 1
  %tobool124 = icmp ne i8 %97, 0
  br i1 %tobool124, label %if.then125, label %if.end140

if.then125:                                       ; preds = %for.body120
  %98 = load i32, i32* %i, align 4
  %arrayidx126 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scfsi, i32 0, i32 %98
  %99 = load i32, i32* %ch, align 4
  %arrayidx127 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx126, i32 0, i32 %99
  %100 = load i8, i8* %arrayidx127, align 1
  %conv128 = zext i8 %100 to i32
  switch i32 %conv128, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb132
    i32 2, label %sw.bb135
    i32 3, label %sw.bb137
  ]

sw.bb:                                            ; preds = %if.then125
  %101 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call129 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %101, i32 6)
  store i8 %call129, i8* %s0, align 1
  %102 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call130 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %102, i32 6)
  store i8 %call130, i8* %s1, align 1
  %103 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call131 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %103, i32 6)
  store i8 %call131, i8* %s2, align 1
  br label %sw.epilog

sw.bb132:                                         ; preds = %if.then125
  %104 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call133 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %104, i32 6)
  store i8 %call133, i8* %s0, align 1
  %105 = load i8, i8* %s0, align 1
  store i8 %105, i8* %s1, align 1
  %106 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call134 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %106, i32 6)
  store i8 %call134, i8* %s2, align 1
  br label %sw.epilog

sw.bb135:                                         ; preds = %if.then125
  %107 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call136 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %107, i32 6)
  store i8 %call136, i8* %s0, align 1
  %108 = load i8, i8* %s0, align 1
  store i8 %108, i8* %s1, align 1
  %109 = load i8, i8* %s0, align 1
  store i8 %109, i8* %s2, align 1
  br label %sw.epilog

sw.bb137:                                         ; preds = %if.then125
  %110 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call138 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %110, i32 6)
  store i8 %call138, i8* %s0, align 1
  %111 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call139 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %111, i32 6)
  store i8 %call139, i8* %s1, align 1
  %112 = load i8, i8* %s1, align 1
  store i8 %112, i8* %s2, align 1
  br label %sw.epilog

sw.default:                                       ; preds = %if.then125
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb137, %sw.bb135, %sw.bb132, %sw.bb
  br label %if.end140

if.end140:                                        ; preds = %sw.epilog, %for.body120
  %113 = load i8, i8* %s0, align 1
  %114 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %114, i32 0, i32 1
  %115 = load i32, i32* %i, align 4
  %arrayidx141 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor, i32 0, i32 %115
  %116 = load i32, i32* %ch, align 4
  %arrayidx142 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx141, i32 0, i32 %116
  %arrayidx143 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx142, i32 0, i32 0
  store i8 %113, i8* %arrayidx143, align 1
  %117 = load i8, i8* %s1, align 1
  %118 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor144 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %118, i32 0, i32 1
  %119 = load i32, i32* %i, align 4
  %arrayidx145 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor144, i32 0, i32 %119
  %120 = load i32, i32* %ch, align 4
  %arrayidx146 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx145, i32 0, i32 %120
  %arrayidx147 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx146, i32 0, i32 1
  store i8 %117, i8* %arrayidx147, align 1
  %121 = load i8, i8* %s2, align 1
  %122 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor148 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %122, i32 0, i32 1
  %123 = load i32, i32* %i, align 4
  %arrayidx149 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor148, i32 0, i32 %123
  %124 = load i32, i32* %ch, align 4
  %arrayidx150 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx149, i32 0, i32 %124
  %arrayidx151 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx150, i32 0, i32 2
  store i8 %121, i8* %arrayidx151, align 1
  br label %for.inc152

for.inc152:                                       ; preds = %if.end140
  %125 = load i32, i32* %ch, align 4
  %inc153 = add nsw i32 %125, 1
  store i32 %inc153, i32* %ch, align 4
  br label %for.cond117

for.end154:                                       ; preds = %for.cond117
  br label %for.inc155

for.inc155:                                       ; preds = %for.end154
  %126 = load i32, i32* %i, align 4
  %inc156 = add nsw i32 %126, 1
  store i32 %inc156, i32* %i, align 4
  br label %for.cond113

for.end157:                                       ; preds = %for.cond113
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @II_step_two(%struct.mpstr_tag* %mp, %struct.sideinfo_layer_II_struct* %si, %struct.frame* %fr, i32 %gr, [4 x [32 x float]]* %fraction) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %si.addr = alloca %struct.sideinfo_layer_II_struct*, align 4
  %fr.addr = alloca %struct.frame*, align 4
  %gr.addr = alloca i32, align 4
  %fraction.addr = alloca [4 x [32 x float]]*, align 4
  %alloc1 = alloca %struct.al_table2*, align 4
  %sblimit = alloca i32, align 4
  %jsbound = alloca i32, align 4
  %i = alloca i32, align 4
  %ch = alloca i32, align 4
  %nch = alloca i32, align 4
  %cm = alloca double, align 8
  %r0 = alloca double, align 8
  %r1 = alloca double, align 8
  %r2 = alloca double, align 8
  %step = alloca i16, align 2
  %ba = alloca i8, align 1
  %x1 = alloca i8, align 1
  %alloc2 = alloca %struct.al_table2*, align 4
  %k = alloca i16, align 2
  %d1 = alloca i16, align 2
  %v0 = alloca i32, align 4
  %v1 = alloca i32, align 4
  %v2 = alloca i32, align 4
  %idx = alloca i32, align 4
  %tab = alloca i8*, align 4
  %k0 = alloca i8, align 1
  %k1 = alloca i8, align 1
  %k2 = alloca i8, align 1
  %step110 = alloca i16, align 2
  %ba112 = alloca i8, align 1
  %alloc2118 = alloca %struct.al_table2*, align 4
  %k121 = alloca i16, align 2
  %d1123 = alloca i16, align 2
  %v0138 = alloca i32, align 4
  %v1141 = alloca i32, align 4
  %v2144 = alloca i32, align 4
  %x1151 = alloca i8, align 1
  %idx198 = alloca i32, align 4
  %tab201 = alloca i8*, align 4
  %k0203 = alloca i8, align 1
  %k1205 = alloca i8, align 1
  %k2207 = alloca i8, align 1
  %x1213 = alloca i8, align 1
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store %struct.sideinfo_layer_II_struct* %si, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  store %struct.frame* %fr, %struct.frame** %fr.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store [4 x [32 x float]]* %fraction, [4 x [32 x float]]** %fraction.addr, align 4
  %0 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %alloc = getelementptr inbounds %struct.frame, %struct.frame* %0, i32 0, i32 18
  %1 = load %struct.al_table2*, %struct.al_table2** %alloc, align 4
  store %struct.al_table2* %1, %struct.al_table2** %alloc1, align 4
  %2 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %II_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %2, i32 0, i32 17
  %3 = load i32, i32* %II_sblimit, align 4
  store i32 %3, i32* %sblimit, align 4
  %4 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode = getelementptr inbounds %struct.frame, %struct.frame* %4, i32 0, i32 11
  %5 = load i32, i32* %mode, align 4
  %cmp = icmp eq i32 %5, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode_ext = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 12
  %7 = load i32, i32* %mode_ext, align 4
  %shl = shl i32 %7, 2
  %add = add nsw i32 %shl, 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %II_sblimit1 = getelementptr inbounds %struct.frame, %struct.frame* %8, i32 0, i32 17
  %9 = load i32, i32* %II_sblimit1, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ %9, %cond.false ]
  store i32 %cond, i32* %jsbound, align 4
  %10 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %10, i32 0, i32 0
  %11 = load i32, i32* %stereo, align 4
  store i32 %11, i32* %nch, align 4
  %12 = load i32, i32* %jsbound, align 4
  %13 = load i32, i32* %sblimit, align 4
  %cmp2 = icmp sgt i32 %12, %13
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %14 = load i32, i32* %sblimit, align 4
  store i32 %14, i32* %jsbound, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc103, %if.end
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %jsbound, align 4
  %cmp3 = icmp slt i32 %15, %16
  br i1 %cmp3, label %for.body, label %for.end105

for.body:                                         ; preds = %for.cond
  %17 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %bits = getelementptr inbounds %struct.al_table2, %struct.al_table2* %17, i32 0, i32 0
  %18 = load i16, i16* %bits, align 2
  store i16 %18, i16* %step, align 2
  store i32 0, i32* %ch, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %19 = load i32, i32* %ch, align 4
  %20 = load i32, i32* %nch, align 4
  %cmp5 = icmp slt i32 %19, %20
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %21 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %21, i32 0, i32 0
  %22 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation, i32 0, i32 %22
  %23 = load i32, i32* %ch, align 4
  %arrayidx7 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx, i32 0, i32 %23
  %24 = load i8, i8* %arrayidx7, align 1
  store i8 %24, i8* %ba, align 1
  %25 = load i8, i8* %ba, align 1
  %tobool = icmp ne i8 %25, 0
  br i1 %tobool, label %if.then8, label %if.else89

if.then8:                                         ; preds = %for.body6
  %26 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %26, i32 0, i32 1
  %27 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor, i32 0, i32 %27
  %28 = load i32, i32* %ch, align 4
  %arrayidx10 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx9, i32 0, i32 %28
  %29 = load i32, i32* %gr.addr, align 4
  %arrayidx11 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx10, i32 0, i32 %29
  %30 = load i8, i8* %arrayidx11, align 1
  store i8 %30, i8* %x1, align 1
  %31 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %32 = load i8, i8* %ba, align 1
  %conv = zext i8 %32 to i32
  %add.ptr = getelementptr inbounds %struct.al_table2, %struct.al_table2* %31, i32 %conv
  store %struct.al_table2* %add.ptr, %struct.al_table2** %alloc2, align 4
  %33 = load %struct.al_table2*, %struct.al_table2** %alloc2, align 4
  %bits12 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %33, i32 0, i32 0
  %34 = load i16, i16* %bits12, align 2
  store i16 %34, i16* %k, align 2
  %35 = load %struct.al_table2*, %struct.al_table2** %alloc2, align 4
  %d = getelementptr inbounds %struct.al_table2, %struct.al_table2* %35, i32 0, i32 1
  %36 = load i16, i16* %d, align 2
  store i16 %36, i16* %d1, align 2
  %37 = load i16, i16* %k, align 2
  %conv13 = sext i16 %37 to i32
  %cmp14 = icmp sle i32 %conv13, 16
  br i1 %cmp14, label %cond.true16, label %cond.false18

cond.true16:                                      ; preds = %if.then8
  %38 = load i16, i16* %k, align 2
  %conv17 = sext i16 %38 to i32
  br label %cond.end19

cond.false18:                                     ; preds = %if.then8
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false18, %cond.true16
  %cond20 = phi i32 [ %conv17, %cond.true16 ], [ 16, %cond.false18 ]
  %conv21 = trunc i32 %cond20 to i16
  store i16 %conv21, i16* %k, align 2
  %39 = load i8, i8* %x1, align 1
  %conv22 = zext i8 %39 to i32
  %cmp23 = icmp slt i32 %conv22, 64
  br i1 %cmp23, label %cond.true25, label %cond.false27

cond.true25:                                      ; preds = %cond.end19
  %40 = load i8, i8* %x1, align 1
  %conv26 = zext i8 %40 to i32
  br label %cond.end28

cond.false27:                                     ; preds = %cond.end19
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false27, %cond.true25
  %cond29 = phi i32 [ %conv26, %cond.true25 ], [ 63, %cond.false27 ]
  %conv30 = trunc i32 %cond29 to i8
  store i8 %conv30, i8* %x1, align 1
  %41 = load i16, i16* %d1, align 2
  %conv31 = sext i16 %41 to i32
  %cmp32 = icmp slt i32 %conv31, 0
  br i1 %cmp32, label %if.then34, label %if.else

if.then34:                                        ; preds = %cond.end28
  %42 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %43 = load i16, i16* %k, align 2
  %conv35 = sext i16 %43 to i32
  %call = call i32 @getbits(%struct.mpstr_tag* %42, i32 %conv35)
  store i32 %call, i32* %v0, align 4
  %44 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %45 = load i16, i16* %k, align 2
  %conv36 = sext i16 %45 to i32
  %call37 = call i32 @getbits(%struct.mpstr_tag* %44, i32 %conv36)
  store i32 %call37, i32* %v1, align 4
  %46 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %47 = load i16, i16* %k, align 2
  %conv38 = sext i16 %47 to i32
  %call39 = call i32 @getbits(%struct.mpstr_tag* %46, i32 %conv38)
  store i32 %call39, i32* %v2, align 4
  %48 = load i16, i16* %k, align 2
  %idxprom = sext i16 %48 to i32
  %arrayidx40 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom
  %49 = load i8, i8* %x1, align 1
  %idxprom41 = zext i8 %49 to i32
  %arrayidx42 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx40, i32 0, i32 %idxprom41
  %50 = load float, float* %arrayidx42, align 4
  %conv43 = fpext float %50 to double
  store double %conv43, double* %cm, align 8
  %51 = load i32, i32* %v0, align 4
  %52 = load i16, i16* %d1, align 2
  %conv44 = sext i16 %52 to i32
  %add45 = add nsw i32 %51, %conv44
  %conv46 = sitofp i32 %add45 to double
  %53 = load double, double* %cm, align 8
  %mul = fmul double %conv46, %53
  store double %mul, double* %r0, align 8
  %54 = load i32, i32* %v1, align 4
  %55 = load i16, i16* %d1, align 2
  %conv47 = sext i16 %55 to i32
  %add48 = add nsw i32 %54, %conv47
  %conv49 = sitofp i32 %add48 to double
  %56 = load double, double* %cm, align 8
  %mul50 = fmul double %conv49, %56
  store double %mul50, double* %r1, align 8
  %57 = load i32, i32* %v2, align 4
  %58 = load i16, i16* %d1, align 2
  %conv51 = sext i16 %58 to i32
  %add52 = add nsw i32 %57, %conv51
  %conv53 = sitofp i32 %add52 to double
  %59 = load double, double* %cm, align 8
  %mul54 = fmul double %conv53, %59
  store double %mul54, double* %r2, align 8
  br label %if.end76

if.else:                                          ; preds = %cond.end28
  %60 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %61 = load i16, i16* %k, align 2
  %conv55 = sext i16 %61 to i32
  %call56 = call i32 @getbits(%struct.mpstr_tag* %60, i32 %conv55)
  store i32 %call56, i32* %idx, align 4
  %62 = load i16, i16* %d1, align 2
  %63 = load i32, i32* %idx, align 4
  %call57 = call i8* @grp_table_select(i16 signext %62, i32 %63)
  store i8* %call57, i8** %tab, align 4
  %64 = load i8*, i8** %tab, align 4
  %arrayidx58 = getelementptr inbounds i8, i8* %64, i32 0
  %65 = load i8, i8* %arrayidx58, align 1
  store i8 %65, i8* %k0, align 1
  %66 = load i8*, i8** %tab, align 4
  %arrayidx59 = getelementptr inbounds i8, i8* %66, i32 1
  %67 = load i8, i8* %arrayidx59, align 1
  store i8 %67, i8* %k1, align 1
  %68 = load i8*, i8** %tab, align 4
  %arrayidx60 = getelementptr inbounds i8, i8* %68, i32 2
  %69 = load i8, i8* %arrayidx60, align 1
  store i8 %69, i8* %k2, align 1
  %70 = load i8, i8* %k0, align 1
  %idxprom61 = zext i8 %70 to i32
  %arrayidx62 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom61
  %71 = load i8, i8* %x1, align 1
  %idxprom63 = zext i8 %71 to i32
  %arrayidx64 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx62, i32 0, i32 %idxprom63
  %72 = load float, float* %arrayidx64, align 4
  %conv65 = fpext float %72 to double
  store double %conv65, double* %r0, align 8
  %73 = load i8, i8* %k1, align 1
  %idxprom66 = zext i8 %73 to i32
  %arrayidx67 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom66
  %74 = load i8, i8* %x1, align 1
  %idxprom68 = zext i8 %74 to i32
  %arrayidx69 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx67, i32 0, i32 %idxprom68
  %75 = load float, float* %arrayidx69, align 4
  %conv70 = fpext float %75 to double
  store double %conv70, double* %r1, align 8
  %76 = load i8, i8* %k2, align 1
  %idxprom71 = zext i8 %76 to i32
  %arrayidx72 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom71
  %77 = load i8, i8* %x1, align 1
  %idxprom73 = zext i8 %77 to i32
  %arrayidx74 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx72, i32 0, i32 %idxprom73
  %78 = load float, float* %arrayidx74, align 4
  %conv75 = fpext float %78 to double
  store double %conv75, double* %r2, align 8
  br label %if.end76

if.end76:                                         ; preds = %if.else, %if.then34
  %79 = load double, double* %r0, align 8
  %conv77 = fptrunc double %79 to float
  %80 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %81 = load i32, i32* %ch, align 4
  %arrayidx78 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %80, i32 %81
  %arrayidx79 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx78, i32 0, i32 0
  %82 = load i32, i32* %i, align 4
  %arrayidx80 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx79, i32 0, i32 %82
  store float %conv77, float* %arrayidx80, align 4
  %83 = load double, double* %r1, align 8
  %conv81 = fptrunc double %83 to float
  %84 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %85 = load i32, i32* %ch, align 4
  %arrayidx82 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %84, i32 %85
  %arrayidx83 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx82, i32 0, i32 1
  %86 = load i32, i32* %i, align 4
  %arrayidx84 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx83, i32 0, i32 %86
  store float %conv81, float* %arrayidx84, align 4
  %87 = load double, double* %r2, align 8
  %conv85 = fptrunc double %87 to float
  %88 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %89 = load i32, i32* %ch, align 4
  %arrayidx86 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %88, i32 %89
  %arrayidx87 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx86, i32 0, i32 2
  %90 = load i32, i32* %i, align 4
  %arrayidx88 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx87, i32 0, i32 %90
  store float %conv85, float* %arrayidx88, align 4
  br label %if.end99

if.else89:                                        ; preds = %for.body6
  %91 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %92 = load i32, i32* %ch, align 4
  %arrayidx90 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %91, i32 %92
  %arrayidx91 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx90, i32 0, i32 2
  %93 = load i32, i32* %i, align 4
  %arrayidx92 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx91, i32 0, i32 %93
  store float 0.000000e+00, float* %arrayidx92, align 4
  %94 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %95 = load i32, i32* %ch, align 4
  %arrayidx93 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %94, i32 %95
  %arrayidx94 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx93, i32 0, i32 1
  %96 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx94, i32 0, i32 %96
  store float 0.000000e+00, float* %arrayidx95, align 4
  %97 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %98 = load i32, i32* %ch, align 4
  %arrayidx96 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %97, i32 %98
  %arrayidx97 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx96, i32 0, i32 0
  %99 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx97, i32 0, i32 %99
  store float 0.000000e+00, float* %arrayidx98, align 4
  br label %if.end99

if.end99:                                         ; preds = %if.else89, %if.end76
  br label %for.inc

for.inc:                                          ; preds = %if.end99
  %100 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %100, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %101 = load i16, i16* %step, align 2
  %conv100 = sext i16 %101 to i32
  %shl101 = shl i32 1, %conv100
  %102 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %add.ptr102 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %102, i32 %shl101
  store %struct.al_table2* %add.ptr102, %struct.al_table2** %alloc1, align 4
  br label %for.inc103

for.inc103:                                       ; preds = %for.end
  %103 = load i32, i32* %i, align 4
  %inc104 = add nsw i32 %103, 1
  store i32 %inc104, i32* %i, align 4
  br label %for.cond

for.end105:                                       ; preds = %for.cond
  %104 = load i32, i32* %jsbound, align 4
  store i32 %104, i32* %i, align 4
  br label %for.cond106

for.cond106:                                      ; preds = %for.inc281, %for.end105
  %105 = load i32, i32* %i, align 4
  %106 = load i32, i32* %sblimit, align 4
  %cmp107 = icmp slt i32 %105, %106
  br i1 %cmp107, label %for.body109, label %for.end283

for.body109:                                      ; preds = %for.cond106
  %107 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %bits111 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %107, i32 0, i32 0
  %108 = load i16, i16* %bits111, align 2
  store i16 %108, i16* %step110, align 2
  %109 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %allocation113 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %109, i32 0, i32 0
  %110 = load i32, i32* %i, align 4
  %arrayidx114 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation113, i32 0, i32 %110
  %arrayidx115 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx114, i32 0, i32 0
  %111 = load i8, i8* %arrayidx115, align 1
  store i8 %111, i8* %ba112, align 1
  %112 = load i8, i8* %ba112, align 1
  %tobool116 = icmp ne i8 %112, 0
  br i1 %tobool116, label %if.then117, label %if.else258

if.then117:                                       ; preds = %for.body109
  %113 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %114 = load i8, i8* %ba112, align 1
  %conv119 = zext i8 %114 to i32
  %add.ptr120 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %113, i32 %conv119
  store %struct.al_table2* %add.ptr120, %struct.al_table2** %alloc2118, align 4
  %115 = load %struct.al_table2*, %struct.al_table2** %alloc2118, align 4
  %bits122 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %115, i32 0, i32 0
  %116 = load i16, i16* %bits122, align 2
  store i16 %116, i16* %k121, align 2
  %117 = load %struct.al_table2*, %struct.al_table2** %alloc2118, align 4
  %d124 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %117, i32 0, i32 1
  %118 = load i16, i16* %d124, align 2
  store i16 %118, i16* %d1123, align 2
  %119 = load i16, i16* %k121, align 2
  %conv125 = sext i16 %119 to i32
  %cmp126 = icmp sle i32 %conv125, 16
  br i1 %cmp126, label %cond.true128, label %cond.false130

cond.true128:                                     ; preds = %if.then117
  %120 = load i16, i16* %k121, align 2
  %conv129 = sext i16 %120 to i32
  br label %cond.end131

cond.false130:                                    ; preds = %if.then117
  br label %cond.end131

cond.end131:                                      ; preds = %cond.false130, %cond.true128
  %cond132 = phi i32 [ %conv129, %cond.true128 ], [ 16, %cond.false130 ]
  %conv133 = trunc i32 %cond132 to i16
  store i16 %conv133, i16* %k121, align 2
  %121 = load i16, i16* %d1123, align 2
  %conv134 = sext i16 %121 to i32
  %cmp135 = icmp slt i32 %conv134, 0
  br i1 %cmp135, label %if.then137, label %if.else197

if.then137:                                       ; preds = %cond.end131
  %122 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %123 = load i16, i16* %k121, align 2
  %conv139 = sext i16 %123 to i32
  %call140 = call i32 @getbits(%struct.mpstr_tag* %122, i32 %conv139)
  store i32 %call140, i32* %v0138, align 4
  %124 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %125 = load i16, i16* %k121, align 2
  %conv142 = sext i16 %125 to i32
  %call143 = call i32 @getbits(%struct.mpstr_tag* %124, i32 %conv142)
  store i32 %call143, i32* %v1141, align 4
  %126 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %127 = load i16, i16* %k121, align 2
  %conv145 = sext i16 %127 to i32
  %call146 = call i32 @getbits(%struct.mpstr_tag* %126, i32 %conv145)
  store i32 %call146, i32* %v2144, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc194, %if.then137
  %128 = load i32, i32* %ch, align 4
  %129 = load i32, i32* %nch, align 4
  %cmp148 = icmp slt i32 %128, %129
  br i1 %cmp148, label %for.body150, label %for.end196

for.body150:                                      ; preds = %for.cond147
  %130 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor152 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %130, i32 0, i32 1
  %131 = load i32, i32* %i, align 4
  %arrayidx153 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor152, i32 0, i32 %131
  %132 = load i32, i32* %ch, align 4
  %arrayidx154 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx153, i32 0, i32 %132
  %133 = load i32, i32* %gr.addr, align 4
  %arrayidx155 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx154, i32 0, i32 %133
  %134 = load i8, i8* %arrayidx155, align 1
  store i8 %134, i8* %x1151, align 1
  %135 = load i8, i8* %x1151, align 1
  %conv156 = zext i8 %135 to i32
  %cmp157 = icmp slt i32 %conv156, 64
  br i1 %cmp157, label %cond.true159, label %cond.false161

cond.true159:                                     ; preds = %for.body150
  %136 = load i8, i8* %x1151, align 1
  %conv160 = zext i8 %136 to i32
  br label %cond.end162

cond.false161:                                    ; preds = %for.body150
  br label %cond.end162

cond.end162:                                      ; preds = %cond.false161, %cond.true159
  %cond163 = phi i32 [ %conv160, %cond.true159 ], [ 63, %cond.false161 ]
  %conv164 = trunc i32 %cond163 to i8
  store i8 %conv164, i8* %x1151, align 1
  %137 = load i16, i16* %k121, align 2
  %idxprom165 = sext i16 %137 to i32
  %arrayidx166 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom165
  %138 = load i8, i8* %x1151, align 1
  %idxprom167 = zext i8 %138 to i32
  %arrayidx168 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx166, i32 0, i32 %idxprom167
  %139 = load float, float* %arrayidx168, align 4
  %conv169 = fpext float %139 to double
  store double %conv169, double* %cm, align 8
  %140 = load i32, i32* %v0138, align 4
  %141 = load i16, i16* %d1123, align 2
  %conv170 = sext i16 %141 to i32
  %add171 = add nsw i32 %140, %conv170
  %conv172 = sitofp i32 %add171 to double
  %142 = load double, double* %cm, align 8
  %mul173 = fmul double %conv172, %142
  store double %mul173, double* %r0, align 8
  %143 = load i32, i32* %v1141, align 4
  %144 = load i16, i16* %d1123, align 2
  %conv174 = sext i16 %144 to i32
  %add175 = add nsw i32 %143, %conv174
  %conv176 = sitofp i32 %add175 to double
  %145 = load double, double* %cm, align 8
  %mul177 = fmul double %conv176, %145
  store double %mul177, double* %r1, align 8
  %146 = load i32, i32* %v2144, align 4
  %147 = load i16, i16* %d1123, align 2
  %conv178 = sext i16 %147 to i32
  %add179 = add nsw i32 %146, %conv178
  %conv180 = sitofp i32 %add179 to double
  %148 = load double, double* %cm, align 8
  %mul181 = fmul double %conv180, %148
  store double %mul181, double* %r2, align 8
  %149 = load double, double* %r0, align 8
  %conv182 = fptrunc double %149 to float
  %150 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %151 = load i32, i32* %ch, align 4
  %arrayidx183 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %150, i32 %151
  %arrayidx184 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx183, i32 0, i32 0
  %152 = load i32, i32* %i, align 4
  %arrayidx185 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx184, i32 0, i32 %152
  store float %conv182, float* %arrayidx185, align 4
  %153 = load double, double* %r1, align 8
  %conv186 = fptrunc double %153 to float
  %154 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %155 = load i32, i32* %ch, align 4
  %arrayidx187 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %154, i32 %155
  %arrayidx188 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx187, i32 0, i32 1
  %156 = load i32, i32* %i, align 4
  %arrayidx189 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx188, i32 0, i32 %156
  store float %conv186, float* %arrayidx189, align 4
  %157 = load double, double* %r2, align 8
  %conv190 = fptrunc double %157 to float
  %158 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %159 = load i32, i32* %ch, align 4
  %arrayidx191 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %158, i32 %159
  %arrayidx192 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx191, i32 0, i32 2
  %160 = load i32, i32* %i, align 4
  %arrayidx193 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx192, i32 0, i32 %160
  store float %conv190, float* %arrayidx193, align 4
  br label %for.inc194

for.inc194:                                       ; preds = %cond.end162
  %161 = load i32, i32* %ch, align 4
  %inc195 = add nsw i32 %161, 1
  store i32 %inc195, i32* %ch, align 4
  br label %for.cond147

for.end196:                                       ; preds = %for.cond147
  br label %if.end257

if.else197:                                       ; preds = %cond.end131
  %162 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %163 = load i16, i16* %k121, align 2
  %conv199 = sext i16 %163 to i32
  %call200 = call i32 @getbits(%struct.mpstr_tag* %162, i32 %conv199)
  store i32 %call200, i32* %idx198, align 4
  %164 = load i16, i16* %d1123, align 2
  %165 = load i32, i32* %idx198, align 4
  %call202 = call i8* @grp_table_select(i16 signext %164, i32 %165)
  store i8* %call202, i8** %tab201, align 4
  %166 = load i8*, i8** %tab201, align 4
  %arrayidx204 = getelementptr inbounds i8, i8* %166, i32 0
  %167 = load i8, i8* %arrayidx204, align 1
  store i8 %167, i8* %k0203, align 1
  %168 = load i8*, i8** %tab201, align 4
  %arrayidx206 = getelementptr inbounds i8, i8* %168, i32 1
  %169 = load i8, i8* %arrayidx206, align 1
  store i8 %169, i8* %k1205, align 1
  %170 = load i8*, i8** %tab201, align 4
  %arrayidx208 = getelementptr inbounds i8, i8* %170, i32 2
  %171 = load i8, i8* %arrayidx208, align 1
  store i8 %171, i8* %k2207, align 1
  store i32 0, i32* %ch, align 4
  br label %for.cond209

for.cond209:                                      ; preds = %for.inc254, %if.else197
  %172 = load i32, i32* %ch, align 4
  %173 = load i32, i32* %nch, align 4
  %cmp210 = icmp slt i32 %172, %173
  br i1 %cmp210, label %for.body212, label %for.end256

for.body212:                                      ; preds = %for.cond209
  %174 = load %struct.sideinfo_layer_II_struct*, %struct.sideinfo_layer_II_struct** %si.addr, align 4
  %scalefactor214 = getelementptr inbounds %struct.sideinfo_layer_II_struct, %struct.sideinfo_layer_II_struct* %174, i32 0, i32 1
  %175 = load i32, i32* %i, align 4
  %arrayidx215 = getelementptr inbounds [32 x [2 x [3 x i8]]], [32 x [2 x [3 x i8]]]* %scalefactor214, i32 0, i32 %175
  %176 = load i32, i32* %ch, align 4
  %arrayidx216 = getelementptr inbounds [2 x [3 x i8]], [2 x [3 x i8]]* %arrayidx215, i32 0, i32 %176
  %177 = load i32, i32* %gr.addr, align 4
  %arrayidx217 = getelementptr inbounds [3 x i8], [3 x i8]* %arrayidx216, i32 0, i32 %177
  %178 = load i8, i8* %arrayidx217, align 1
  store i8 %178, i8* %x1213, align 1
  %179 = load i8, i8* %x1213, align 1
  %conv218 = zext i8 %179 to i32
  %cmp219 = icmp slt i32 %conv218, 64
  br i1 %cmp219, label %cond.true221, label %cond.false223

cond.true221:                                     ; preds = %for.body212
  %180 = load i8, i8* %x1213, align 1
  %conv222 = zext i8 %180 to i32
  br label %cond.end224

cond.false223:                                    ; preds = %for.body212
  br label %cond.end224

cond.end224:                                      ; preds = %cond.false223, %cond.true221
  %cond225 = phi i32 [ %conv222, %cond.true221 ], [ 63, %cond.false223 ]
  %conv226 = trunc i32 %cond225 to i8
  store i8 %conv226, i8* %x1213, align 1
  %181 = load i8, i8* %k0203, align 1
  %idxprom227 = zext i8 %181 to i32
  %arrayidx228 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom227
  %182 = load i8, i8* %x1213, align 1
  %idxprom229 = zext i8 %182 to i32
  %arrayidx230 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx228, i32 0, i32 %idxprom229
  %183 = load float, float* %arrayidx230, align 4
  %conv231 = fpext float %183 to double
  store double %conv231, double* %r0, align 8
  %184 = load i8, i8* %k1205, align 1
  %idxprom232 = zext i8 %184 to i32
  %arrayidx233 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom232
  %185 = load i8, i8* %x1213, align 1
  %idxprom234 = zext i8 %185 to i32
  %arrayidx235 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx233, i32 0, i32 %idxprom234
  %186 = load float, float* %arrayidx235, align 4
  %conv236 = fpext float %186 to double
  store double %conv236, double* %r1, align 8
  %187 = load i8, i8* %k2207, align 1
  %idxprom237 = zext i8 %187 to i32
  %arrayidx238 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %idxprom237
  %188 = load i8, i8* %x1213, align 1
  %idxprom239 = zext i8 %188 to i32
  %arrayidx240 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx238, i32 0, i32 %idxprom239
  %189 = load float, float* %arrayidx240, align 4
  %conv241 = fpext float %189 to double
  store double %conv241, double* %r2, align 8
  %190 = load double, double* %r0, align 8
  %conv242 = fptrunc double %190 to float
  %191 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %192 = load i32, i32* %ch, align 4
  %arrayidx243 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %191, i32 %192
  %arrayidx244 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx243, i32 0, i32 0
  %193 = load i32, i32* %i, align 4
  %arrayidx245 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx244, i32 0, i32 %193
  store float %conv242, float* %arrayidx245, align 4
  %194 = load double, double* %r1, align 8
  %conv246 = fptrunc double %194 to float
  %195 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %196 = load i32, i32* %ch, align 4
  %arrayidx247 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %195, i32 %196
  %arrayidx248 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx247, i32 0, i32 1
  %197 = load i32, i32* %i, align 4
  %arrayidx249 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx248, i32 0, i32 %197
  store float %conv246, float* %arrayidx249, align 4
  %198 = load double, double* %r2, align 8
  %conv250 = fptrunc double %198 to float
  %199 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %200 = load i32, i32* %ch, align 4
  %arrayidx251 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %199, i32 %200
  %arrayidx252 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx251, i32 0, i32 2
  %201 = load i32, i32* %i, align 4
  %arrayidx253 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx252, i32 0, i32 %201
  store float %conv250, float* %arrayidx253, align 4
  br label %for.inc254

for.inc254:                                       ; preds = %cond.end224
  %202 = load i32, i32* %ch, align 4
  %inc255 = add nsw i32 %202, 1
  store i32 %inc255, i32* %ch, align 4
  br label %for.cond209

for.end256:                                       ; preds = %for.cond209
  br label %if.end257

if.end257:                                        ; preds = %for.end256, %for.end196
  br label %if.end277

if.else258:                                       ; preds = %for.body109
  %203 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx259 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %203, i32 0
  %arrayidx260 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx259, i32 0, i32 2
  %204 = load i32, i32* %i, align 4
  %arrayidx261 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx260, i32 0, i32 %204
  store float 0.000000e+00, float* %arrayidx261, align 4
  %205 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx262 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %205, i32 0
  %arrayidx263 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx262, i32 0, i32 1
  %206 = load i32, i32* %i, align 4
  %arrayidx264 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx263, i32 0, i32 %206
  store float 0.000000e+00, float* %arrayidx264, align 4
  %207 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx265 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %207, i32 0
  %arrayidx266 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx265, i32 0, i32 0
  %208 = load i32, i32* %i, align 4
  %arrayidx267 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx266, i32 0, i32 %208
  store float 0.000000e+00, float* %arrayidx267, align 4
  %209 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx268 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %209, i32 1
  %arrayidx269 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx268, i32 0, i32 2
  %210 = load i32, i32* %i, align 4
  %arrayidx270 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx269, i32 0, i32 %210
  store float 0.000000e+00, float* %arrayidx270, align 4
  %211 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx271 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %211, i32 1
  %arrayidx272 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx271, i32 0, i32 1
  %212 = load i32, i32* %i, align 4
  %arrayidx273 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx272, i32 0, i32 %212
  store float 0.000000e+00, float* %arrayidx273, align 4
  %213 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %arrayidx274 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %213, i32 1
  %arrayidx275 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx274, i32 0, i32 0
  %214 = load i32, i32* %i, align 4
  %arrayidx276 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx275, i32 0, i32 %214
  store float 0.000000e+00, float* %arrayidx276, align 4
  br label %if.end277

if.end277:                                        ; preds = %if.else258, %if.end257
  %215 = load i16, i16* %step110, align 2
  %conv278 = sext i16 %215 to i32
  %shl279 = shl i32 1, %conv278
  %216 = load %struct.al_table2*, %struct.al_table2** %alloc1, align 4
  %add.ptr280 = getelementptr inbounds %struct.al_table2, %struct.al_table2* %216, i32 %shl279
  store %struct.al_table2* %add.ptr280, %struct.al_table2** %alloc1, align 4
  br label %for.inc281

for.inc281:                                       ; preds = %if.end277
  %217 = load i32, i32* %i, align 4
  %inc282 = add nsw i32 %217, 1
  store i32 %inc282, i32* %i, align 4
  br label %for.cond106

for.end283:                                       ; preds = %for.cond106
  %218 = load i32, i32* %sblimit, align 4
  %219 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %219, i32 0, i32 19
  %220 = load i32, i32* %down_sample_sblimit, align 4
  %cmp284 = icmp sgt i32 %218, %220
  br i1 %cmp284, label %if.then286, label %if.end288

if.then286:                                       ; preds = %for.end283
  %221 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample_sblimit287 = getelementptr inbounds %struct.frame, %struct.frame* %221, i32 0, i32 19
  %222 = load i32, i32* %down_sample_sblimit287, align 4
  store i32 %222, i32* %sblimit, align 4
  br label %if.end288

if.end288:                                        ; preds = %if.then286, %for.end283
  store i32 0, i32* %ch, align 4
  br label %for.cond289

for.cond289:                                      ; preds = %for.inc309, %if.end288
  %223 = load i32, i32* %ch, align 4
  %224 = load i32, i32* %nch, align 4
  %cmp290 = icmp slt i32 %223, %224
  br i1 %cmp290, label %for.body292, label %for.end311

for.body292:                                      ; preds = %for.cond289
  %225 = load i32, i32* %sblimit, align 4
  store i32 %225, i32* %i, align 4
  br label %for.cond293

for.cond293:                                      ; preds = %for.inc306, %for.body292
  %226 = load i32, i32* %i, align 4
  %cmp294 = icmp slt i32 %226, 32
  br i1 %cmp294, label %for.body296, label %for.end308

for.body296:                                      ; preds = %for.cond293
  %227 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %228 = load i32, i32* %ch, align 4
  %arrayidx297 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %227, i32 %228
  %arrayidx298 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx297, i32 0, i32 2
  %229 = load i32, i32* %i, align 4
  %arrayidx299 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx298, i32 0, i32 %229
  store float 0.000000e+00, float* %arrayidx299, align 4
  %230 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %231 = load i32, i32* %ch, align 4
  %arrayidx300 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %230, i32 %231
  %arrayidx301 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx300, i32 0, i32 1
  %232 = load i32, i32* %i, align 4
  %arrayidx302 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx301, i32 0, i32 %232
  store float 0.000000e+00, float* %arrayidx302, align 4
  %233 = load [4 x [32 x float]]*, [4 x [32 x float]]** %fraction.addr, align 4
  %234 = load i32, i32* %ch, align 4
  %arrayidx303 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %233, i32 %234
  %arrayidx304 = getelementptr inbounds [4 x [32 x float]], [4 x [32 x float]]* %arrayidx303, i32 0, i32 0
  %235 = load i32, i32* %i, align 4
  %arrayidx305 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx304, i32 0, i32 %235
  store float 0.000000e+00, float* %arrayidx305, align 4
  br label %for.inc306

for.inc306:                                       ; preds = %for.body296
  %236 = load i32, i32* %i, align 4
  %inc307 = add nsw i32 %236, 1
  store i32 %inc307, i32* %i, align 4
  br label %for.cond293

for.end308:                                       ; preds = %for.cond293
  br label %for.inc309

for.inc309:                                       ; preds = %for.end308
  %237 = load i32, i32* %ch, align 4
  %inc310 = add nsw i32 %237, 1
  store i32 %inc310, i32* %ch, align 4
  br label %for.cond289

for.end311:                                       ; preds = %for.cond289
  ret void
}

declare i32 @synth_1to1_mono(%struct.mpstr_tag*, float*, i8*, i32*) #2

declare i32 @synth_1to1(%struct.mpstr_tag*, float*, i32, i8*, i32*) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare zeroext i8 @get_leq_8_bits(%struct.mpstr_tag*, i32) #2

declare i32 @getbits(%struct.mpstr_tag*, i32) #2

; Function Attrs: noinline nounwind optnone
define internal i8* @grp_table_select(i16 signext %d1, i32 %idx) #0 {
entry:
  %retval = alloca i8*, align 4
  %d1.addr = alloca i16, align 2
  %idx.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store i16 %d1, i16* %d1.addr, align 2
  store i32 %idx, i32* %idx.addr, align 4
  %0 = load i16, i16* %d1.addr, align 2
  %conv = sext i16 %0 to i32
  switch i32 %conv, label %sw.default [
    i32 3, label %sw.bb
    i32 5, label %sw.bb2
    i32 9, label %sw.bb11
  ]

sw.bb:                                            ; preds = %entry
  store i32 27, i32* %x, align 4
  %1 = load i32, i32* %idx.addr, align 4
  %2 = load i32, i32* %x, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.bb
  %3 = load i32, i32* %idx.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %sw.bb
  %4 = load i32, i32* %x, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %3, %cond.true ], [ %4, %cond.false ]
  store i32 %cond, i32* %idx.addr, align 4
  %5 = load i32, i32* %idx.addr, align 4
  %mul = mul i32 3, %5
  %arrayidx = getelementptr inbounds [96 x i8], [96 x i8]* @grp_3tab, i32 0, i32 %mul
  store i8* %arrayidx, i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i32 125, i32* %x, align 4
  %6 = load i32, i32* %idx.addr, align 4
  %7 = load i32, i32* %x, align 4
  %cmp3 = icmp ult i32 %6, %7
  br i1 %cmp3, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %sw.bb2
  %8 = load i32, i32* %idx.addr, align 4
  br label %cond.end7

cond.false6:                                      ; preds = %sw.bb2
  %9 = load i32, i32* %x, align 4
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true5
  %cond8 = phi i32 [ %8, %cond.true5 ], [ %9, %cond.false6 ]
  store i32 %cond8, i32* %idx.addr, align 4
  %10 = load i32, i32* %idx.addr, align 4
  %mul9 = mul i32 3, %10
  %arrayidx10 = getelementptr inbounds [384 x i8], [384 x i8]* @grp_5tab, i32 0, i32 %mul9
  store i8* %arrayidx10, i8** %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry
  store i32 729, i32* %x, align 4
  %11 = load i32, i32* %idx.addr, align 4
  %12 = load i32, i32* %x, align 4
  %cmp12 = icmp ult i32 %11, %12
  br i1 %cmp12, label %cond.true14, label %cond.false15

cond.true14:                                      ; preds = %sw.bb11
  %13 = load i32, i32* %idx.addr, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %sw.bb11
  %14 = load i32, i32* %x, align 4
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true14
  %cond17 = phi i32 [ %13, %cond.true14 ], [ %14, %cond.false15 ]
  store i32 %cond17, i32* %idx.addr, align 4
  %15 = load i32, i32* %idx.addr, align 4
  %mul18 = mul i32 3, %15
  %arrayidx19 = getelementptr inbounds [3072 x i8], [3072 x i8]* @grp_9tab, i32 0, i32 %mul18
  store i8* %arrayidx19, i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @grp_table_select.dummy_table, i32 0, i32 0), i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %cond.end16, %cond.end7, %cond.end
  %16 = load i8*, i8** %retval, align 4
  ret i8* %16
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
