; ModuleID = 'interface.c'
source_filename = "interface.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mpstr_tag = type { %struct.buf*, %struct.buf*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.frame, %struct.III_sideinfo, [2 x [3904 x i8]], [2 x [2 x [576 x float]]], [2 x i32], i32, i32, [2 x [2 x [272 x float]]], i32, i32, i32, i8*, %struct.plotting_data*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.buf = type { i8*, i32, i32, %struct.buf*, %struct.buf* }
%struct.frame = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.al_table2*, i32, i32 }
%struct.al_table2 = type { i16, i16 }
%struct.III_sideinfo = type { i32, i32, [2 x %struct.anon] }
%struct.anon = type { [2 x %struct.gr_info_s] }
%struct.gr_info_s = type { i32, i32, i32, i32, i32, i32, [3 x i32], [3 x i32], [3 x i32], i32, i32, i32, i32, i32, i32, i32, [3 x float*], float* }
%struct.plotting_data = type opaque
%struct.VBRTAGDATA = type { i32, i32, i32, i32, i32, i32, [100 x i8], i32, i32, i32 }

@.str = private unnamed_addr constant [49 x i8] c"hip: Insufficient memory for decoding buffer %d\0A\00", align 1
@.str.1 = private unnamed_addr constant [45 x i8] c"hip: out space too small for unclipped mode\0A\00", align 1
@.str.2 = private unnamed_addr constant [56 x i8] c"hip: bitstream problem, resyncing skipping %d bytes...\0A\00", align 1
@.str.3 = private unnamed_addr constant [52 x i8] c"hip: wordpointer trashed.  size=%i (%i)  bytes=%i \0A\00", align 1
@.str.4 = private unnamed_addr constant [53 x i8] c"hip: error audio data exceeds framesize by %d bytes\0A\00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"hip: invalid layer %d\0A\00", align 1
@.str.6 = private unnamed_addr constant [51 x i8] c"hip: fatal error.  MAXFRAMESIZE not large enough.\0A\00", align 1
@.str.7 = private unnamed_addr constant [30 x i8] c"hip: addbuf() Out of memory!\0A\00", align 1
@.str.8 = private unnamed_addr constant [48 x i8] c"hip: Fatal error! tried to read past mp buffer\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @InitMP3(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  call void @hip_init_tables_layer1()
  call void @hip_init_tables_layer2()
  call void @hip_init_tables_layer3()
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tobool = icmp ne %struct.mpstr_tag* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %2 = bitcast %struct.mpstr_tag* %1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %2, i8 0, i32 22000, i1 false)
  %3 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %3, i32 0, i32 12
  store i32 0, i32* %framesize, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %num_frames = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 3
  store i32 0, i32* %num_frames, align 4
  %5 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %enc_delay = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %5, i32 0, i32 4
  store i32 -1, i32* %enc_delay, align 4
  %6 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %enc_padding = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %6, i32 0, i32 5
  store i32 -1, i32* %enc_padding, align 4
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %vbr_header = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 2
  store i32 0, i32* %vbr_header, align 4
  %8 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %8, i32 0, i32 6
  store i32 0, i32* %header_parsed, align 4
  %9 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %side_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %9, i32 0, i32 7
  store i32 0, i32* %side_parsed, align 4
  %10 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %data_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %10, i32 0, i32 8
  store i32 0, i32* %data_parsed, align 4
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %free_format = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %11, i32 0, i32 9
  store i32 0, i32* %free_format, align 4
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %old_free_format = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %12, i32 0, i32 10
  store i32 0, i32* %old_free_format, align 4
  %13 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %13, i32 0, i32 13
  store i32 0, i32* %ssize, align 4
  %14 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %14, i32 0, i32 14
  store i32 0, i32* %dsize, align 4
  %15 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %15, i32 0, i32 15
  store i32 -1, i32* %fsizeold, align 4
  %16 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %16, i32 0, i32 11
  store i32 0, i32* %bsize, align 4
  %17 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %17, i32 0, i32 1
  store %struct.buf* null, %struct.buf** %tail, align 4
  %18 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %head = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %18, i32 0, i32 0
  store %struct.buf* null, %struct.buf** %head, align 4
  %19 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %19, i32 0, i32 17
  %single = getelementptr inbounds %struct.frame, %struct.frame* %fr, i32 0, i32 1
  store i32 -1, i32* %single, align 4
  %20 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %20, i32 0, i32 23
  store i32 0, i32* %bsnum, align 4
  %21 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %21, i32 0, i32 19
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %22, i32 0, i32 23
  %23 = load i32, i32* %bsnum1, align 4
  %arrayidx = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace, i32 0, i32 %23
  %arraydecay = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 512
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %24, i32 0, i32 28
  store i8* %add.ptr, i8** %wordpointer, align 4
  %25 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %25, i32 0, i32 27
  store i32 0, i32* %bitindex, align 4
  %26 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_bo = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %26, i32 0, i32 25
  store i32 1, i32* %synth_bo, align 4
  %27 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %sync_bitstream = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %27, i32 0, i32 26
  store i32 1, i32* %sync_bitstream, align 4
  %28 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_dbg = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %28, i32 0, i32 31
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %report_dbg, align 4
  %29 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %29, i32 0, i32 32
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %report_err, align 4
  %30 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_msg = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %30, i32 0, i32 30
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %report_msg, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  call void @make_decode_tables(i32 32767)
  ret i32 1
}

declare void @hip_init_tables_layer1() #1

declare void @hip_init_tables_layer2() #1

declare void @hip_init_tables_layer3() #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare void @lame_report_def(i8*, i8*) #1

declare void @make_decode_tables(i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @ExitMP3(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %b = alloca %struct.buf*, align 4
  %bn = alloca %struct.buf*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tobool = icmp ne %struct.mpstr_tag* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %1, i32 0, i32 1
  %2 = load %struct.buf*, %struct.buf** %tail, align 4
  store %struct.buf* %2, %struct.buf** %b, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %3 = load %struct.buf*, %struct.buf** %b, align 4
  %tobool1 = icmp ne %struct.buf* %3, null
  br i1 %tobool1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load %struct.buf*, %struct.buf** %b, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %4, i32 0, i32 0
  %5 = load i8*, i8** %pnt, align 4
  call void @free(i8* %5)
  %6 = load %struct.buf*, %struct.buf** %b, align 4
  %next = getelementptr inbounds %struct.buf, %struct.buf* %6, i32 0, i32 3
  %7 = load %struct.buf*, %struct.buf** %next, align 4
  store %struct.buf* %7, %struct.buf** %bn, align 4
  %8 = load %struct.buf*, %struct.buf** %b, align 4
  %9 = bitcast %struct.buf* %8 to i8*
  call void @free(i8* %9)
  %10 = load %struct.buf*, %struct.buf** %bn, align 4
  store %struct.buf* %10, %struct.buf** %b, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.end:                                           ; preds = %while.end, %entry
  ret void
}

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @remove_buf(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %buf = alloca %struct.buf*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 1
  %1 = load %struct.buf*, %struct.buf** %tail, align 4
  store %struct.buf* %1, %struct.buf** %buf, align 4
  %2 = load %struct.buf*, %struct.buf** %buf, align 4
  %next = getelementptr inbounds %struct.buf, %struct.buf* %2, i32 0, i32 3
  %3 = load %struct.buf*, %struct.buf** %next, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 1
  store %struct.buf* %3, %struct.buf** %tail1, align 4
  %5 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail2 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %5, i32 0, i32 1
  %6 = load %struct.buf*, %struct.buf** %tail2, align 4
  %tobool = icmp ne %struct.buf* %6, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail3 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 1
  %8 = load %struct.buf*, %struct.buf** %tail3, align 4
  %prev = getelementptr inbounds %struct.buf, %struct.buf* %8, i32 0, i32 4
  store %struct.buf* null, %struct.buf** %prev, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %head = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %9, i32 0, i32 0
  store %struct.buf* null, %struct.buf** %head, align 4
  %10 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail4 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %10, i32 0, i32 1
  store %struct.buf* null, %struct.buf** %tail4, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = load %struct.buf*, %struct.buf** %buf, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %11, i32 0, i32 0
  %12 = load i8*, i8** %pnt, align 4
  call void @free(i8* %12)
  %13 = load %struct.buf*, %struct.buf** %buf, align 4
  %14 = bitcast %struct.buf* %13 to i8*
  call void @free(i8* %14)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @decode_reset(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call = call i32 @InitMP3(%struct.mpstr_tag* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @audiodata_precedesframes(%struct.mpstr_tag* %mp) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 17
  %lay = getelementptr inbounds %struct.frame, %struct.frame* %fr, i32 0, i32 5
  %1 = load i32, i32* %lay, align 4
  %cmp = icmp eq i32 %1, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call = call i32 @layer3_audiodata_precedesframes(%struct.mpstr_tag* %2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare i32 @layer3_audiodata_precedesframes(%struct.mpstr_tag*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @decodeMP3(%struct.mpstr_tag* %mp, i8* %in, i32 %isize, i8* %out, i32 %osize, i32* %done) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %in.addr = alloca i8*, align 4
  %isize.addr = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %osize.addr = alloca i32, align 4
  %done.addr = alloca i32*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %isize, i32* %isize.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %osize, i32* %osize.addr, align 4
  store i32* %done, i32** %done.addr, align 4
  %0 = load i32, i32* %osize.addr, align 4
  %cmp = icmp slt i32 %0, 4608
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %1, i32 0, i32 32
  %2 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  %3 = load i32, i32* %osize.addr, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %2, i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str, i32 0, i32 0), i32 %3)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %5 = load i8*, i8** %in.addr, align 4
  %6 = load i32, i32* %isize.addr, align 4
  %7 = load i8*, i8** %out.addr, align 4
  %8 = load i32*, i32** %done.addr, align 4
  %call = call i32 @decodeMP3_clipchoice(%struct.mpstr_tag* %4, i8* %5, i32 %6, i8* %7, i32* %8, i32 (%struct.mpstr_tag*, float*, i8*, i32*)* @synth_1to1_mono, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)* @synth_1to1)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

declare void @lame_report_fnc(void (i8*, i8*)*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @decodeMP3_clipchoice(%struct.mpstr_tag* %mp, i8* %in, i32 %isize, i8* %out, i32* %done, i32 (%struct.mpstr_tag*, float*, i8*, i32*)* %synth_1to1_mono_ptr, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)* %synth_1to1_ptr) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %in.addr = alloca i8*, align 4
  %isize.addr = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %done.addr = alloca i32*, align 4
  %synth_1to1_mono_ptr.addr = alloca i32 (%struct.mpstr_tag*, float*, i8*, i32*)*, align 4
  %synth_1to1_ptr.addr = alloca i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)*, align 4
  %i = alloca i32, align 4
  %iret = alloca i32, align 4
  %bits = alloca i32, align 4
  %bytes = alloca i32, align 4
  %vbrbytes = alloca i32, align 4
  %size = alloca i32, align 4
  %framesize133 = alloca i32, align 4
  %size251 = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %isize, i32* %isize.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32* %done, i32** %done.addr, align 4
  store i32 (%struct.mpstr_tag*, float*, i8*, i32*)* %synth_1to1_mono_ptr, i32 (%struct.mpstr_tag*, float*, i8*, i32*)** %synth_1to1_mono_ptr.addr, align 4
  store i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)* %synth_1to1_ptr, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)** %synth_1to1_ptr.addr, align 4
  %0 = load i8*, i8** %in.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %isize.addr, align 4
  %tobool1 = icmp ne i32 %1, 0
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %2 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i32, i32* %isize.addr, align 4
  %call = call %struct.buf* @addbuf(%struct.mpstr_tag* %2, i8* %3, i32 %4)
  %cmp = icmp eq %struct.buf* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true2, %land.lhs.true, %entry
  %5 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %5, i32 0, i32 6
  %6 = load i32, i32* %header_parsed, align 4
  %tobool3 = icmp ne i32 %6, 0
  br i1 %tobool3, label %if.end103, label %if.then4

if.then4:                                         ; preds = %if.end
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 15
  %8 = load i32, i32* %fsizeold, align 4
  %cmp5 = icmp eq i32 %8, -1
  br i1 %cmp5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then4
  %9 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %sync_bitstream = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %9, i32 0, i32 26
  %10 = load i32, i32* %sync_bitstream, align 4
  %tobool6 = icmp ne i32 %10, 0
  br i1 %tobool6, label %if.then7, label %if.else25

if.then7:                                         ; preds = %lor.lhs.false, %if.then4
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %sync_bitstream8 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %11, i32 0, i32 26
  store i32 0, i32* %sync_bitstream8, align 4
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call9 = call i32 @sync_buffer(%struct.mpstr_tag* %12, i32 0)
  store i32 %call9, i32* %bytes, align 4
  %13 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %13, i32 0, i32 11
  %14 = load i32, i32* %bsize, align 4
  %15 = load i32, i32* %bytes, align 4
  %add = add nsw i32 %15, 194
  %cmp10 = icmp sge i32 %14, %add
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.then7
  %16 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %17 = load i32, i32* %bytes, align 4
  %call12 = call i32 @check_vbr_header(%struct.mpstr_tag* %16, i32 %17)
  store i32 %call12, i32* %vbrbytes, align 4
  br label %if.end13

if.else:                                          ; preds = %if.then7
  store i32 1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.then11
  %18 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %vbr_header = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %18, i32 0, i32 2
  %19 = load i32, i32* %vbr_header, align 4
  %tobool14 = icmp ne i32 %19, 0
  br i1 %tobool14, label %if.then15, label %if.end24

if.then15:                                        ; preds = %if.end13
  %20 = load i32, i32* %bytes, align 4
  %21 = load i32, i32* %vbrbytes, align 4
  %add16 = add nsw i32 %20, %21
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize17 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %22, i32 0, i32 11
  %23 = load i32, i32* %bsize17, align 4
  %cmp18 = icmp sgt i32 %add16, %23
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.then15
  store i32 1, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.then15
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end20
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %vbrbytes, align 4
  %26 = load i32, i32* %bytes, align 4
  %add21 = add nsw i32 %25, %26
  %cmp22 = icmp slt i32 %24, %add21
  br i1 %cmp22, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %27 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call23 = call i32 @read_buf_byte(%struct.mpstr_tag* %27)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %28 = load i32, i32* %i, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.end13
  br label %if.end27

if.else25:                                        ; preds = %lor.lhs.false
  %29 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call26 = call i32 @sync_buffer(%struct.mpstr_tag* %29, i32 1)
  store i32 %call26, i32* %bytes, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.else25, %if.end24
  %30 = load i32, i32* %bytes, align 4
  %cmp28 = icmp slt i32 %30, 0
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end27
  store i32 1, i32* %retval, align 4
  br label %return

if.end30:                                         ; preds = %if.end27
  %31 = load i32, i32* %bytes, align 4
  %cmp31 = icmp sgt i32 %31, 0
  br i1 %cmp31, label %if.then32, label %if.end59

if.then32:                                        ; preds = %if.end30
  %32 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold33 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %32, i32 0, i32 15
  %33 = load i32, i32* %fsizeold33, align 4
  %cmp34 = icmp ne i32 %33, -1
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.then32
  %34 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %34, i32 0, i32 32
  %35 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  %36 = load i32, i32* %bytes, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %35, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.2, i32 0, i32 0), i32 %36)
  br label %if.end36

if.end36:                                         ; preds = %if.then35, %if.then32
  %37 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %old_free_format = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %37, i32 0, i32 10
  store i32 0, i32* %old_free_format, align 4
  %38 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %sync_bitstream37 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %38, i32 0, i32 26
  store i32 1, i32* %sync_bitstream37, align 4
  %39 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %39, i32 0, i32 28
  %40 = load i8*, i8** %wordpointer, align 4
  %41 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %41, i32 0, i32 19
  %42 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %42, i32 0, i32 23
  %43 = load i32, i32* %bsnum, align 4
  %arrayidx = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace, i32 0, i32 %43
  %arraydecay = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 512
  %sub.ptr.lhs.cast = ptrtoint i8* %40 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %add.ptr to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %size, align 4
  %44 = load i32, i32* %size, align 4
  %cmp38 = icmp sgt i32 %44, 2880
  br i1 %cmp38, label %if.then39, label %if.end47

if.then39:                                        ; preds = %if.end36
  %45 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err40 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %45, i32 0, i32 32
  %46 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err40, align 4
  %47 = load i32, i32* %size, align 4
  %48 = load i32, i32* %bytes, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %46, i8* getelementptr inbounds ([52 x i8], [52 x i8]* @.str.3, i32 0, i32 0), i32 %47, i32 2880, i32 %48)
  store i32 0, i32* %size, align 4
  %49 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace41 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %49, i32 0, i32 19
  %50 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum42 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %50, i32 0, i32 23
  %51 = load i32, i32* %bsnum42, align 4
  %arrayidx43 = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace41, i32 0, i32 %51
  %arraydecay44 = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx43, i32 0, i32 0
  %add.ptr45 = getelementptr inbounds i8, i8* %arraydecay44, i32 512
  %52 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer46 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %52, i32 0, i32 28
  store i8* %add.ptr45, i8** %wordpointer46, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then39, %if.end36
  %53 = load i32, i32* %size, align 4
  %54 = load i32, i32* %bytes, align 4
  %add48 = add nsw i32 %53, %54
  %sub = sub nsw i32 %add48, 2880
  store i32 %sub, i32* %i, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc53, %if.end47
  %55 = load i32, i32* %i, align 4
  %cmp50 = icmp sgt i32 %55, 0
  br i1 %cmp50, label %for.body51, label %for.end55

for.body51:                                       ; preds = %for.cond49
  %56 = load i32, i32* %bytes, align 4
  %dec = add nsw i32 %56, -1
  store i32 %dec, i32* %bytes, align 4
  %57 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call52 = call i32 @read_buf_byte(%struct.mpstr_tag* %57)
  br label %for.inc53

for.inc53:                                        ; preds = %for.body51
  %58 = load i32, i32* %i, align 4
  %dec54 = add nsw i32 %58, -1
  store i32 %dec54, i32* %i, align 4
  br label %for.cond49

for.end55:                                        ; preds = %for.cond49
  %59 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %60 = load i32, i32* %bytes, align 4
  %61 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer56 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %61, i32 0, i32 28
  %62 = load i8*, i8** %wordpointer56, align 4
  call void @copy_mp(%struct.mpstr_tag* %59, i32 %60, i8* %62)
  %63 = load i32, i32* %bytes, align 4
  %64 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold57 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %64, i32 0, i32 15
  %65 = load i32, i32* %fsizeold57, align 4
  %add58 = add nsw i32 %65, %63
  store i32 %add58, i32* %fsizeold57, align 4
  br label %if.end59

if.end59:                                         ; preds = %for.end55, %if.end30
  %66 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  call void @read_head(%struct.mpstr_tag* %66)
  %67 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %68 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %68, i32 0, i32 17
  %69 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %69, i32 0, i32 22
  %70 = load i32, i32* %header, align 4
  %call60 = call i32 @decode_header(%struct.mpstr_tag* %67, %struct.frame* %fr, i32 %70)
  %tobool61 = icmp ne i32 %call60, 0
  br i1 %tobool61, label %if.end63, label %if.then62

if.then62:                                        ; preds = %if.end59
  store i32 -1, i32* %retval, align 4
  br label %return

if.end63:                                         ; preds = %if.end59
  %71 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header_parsed64 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %71, i32 0, i32 6
  store i32 1, i32* %header_parsed64, align 4
  %72 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr65 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %72, i32 0, i32 17
  %framesize = getelementptr inbounds %struct.frame, %struct.frame* %fr65, i32 0, i32 16
  %73 = load i32, i32* %framesize, align 4
  %74 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize66 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %74, i32 0, i32 12
  store i32 %73, i32* %framesize66, align 4
  %75 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize67 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %75, i32 0, i32 12
  %76 = load i32, i32* %framesize67, align 4
  %cmp68 = icmp eq i32 %76, 0
  %conv = zext i1 %cmp68 to i32
  %77 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %free_format = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %77, i32 0, i32 9
  store i32 %conv, i32* %free_format, align 4
  %78 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr69 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %78, i32 0, i32 17
  %lsf = getelementptr inbounds %struct.frame, %struct.frame* %fr69, i32 0, i32 2
  %79 = load i32, i32* %lsf, align 4
  %tobool70 = icmp ne i32 %79, 0
  br i1 %tobool70, label %if.then71, label %if.else75

if.then71:                                        ; preds = %if.end63
  %80 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr72 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %80, i32 0, i32 17
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %fr72, i32 0, i32 0
  %81 = load i32, i32* %stereo, align 4
  %cmp73 = icmp eq i32 %81, 1
  %82 = zext i1 %cmp73 to i64
  %cond = select i1 %cmp73, i32 9, i32 17
  %83 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %83, i32 0, i32 13
  store i32 %cond, i32* %ssize, align 4
  br label %if.end82

if.else75:                                        ; preds = %if.end63
  %84 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr76 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %84, i32 0, i32 17
  %stereo77 = getelementptr inbounds %struct.frame, %struct.frame* %fr76, i32 0, i32 0
  %85 = load i32, i32* %stereo77, align 4
  %cmp78 = icmp eq i32 %85, 1
  %86 = zext i1 %cmp78 to i64
  %cond80 = select i1 %cmp78, i32 17, i32 32
  %87 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize81 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %87, i32 0, i32 13
  store i32 %cond80, i32* %ssize81, align 4
  br label %if.end82

if.end82:                                         ; preds = %if.else75, %if.then71
  %88 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr83 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %88, i32 0, i32 17
  %error_protection = getelementptr inbounds %struct.frame, %struct.frame* %fr83, i32 0, i32 6
  %89 = load i32, i32* %error_protection, align 4
  %tobool84 = icmp ne i32 %89, 0
  br i1 %tobool84, label %if.then85, label %if.end88

if.then85:                                        ; preds = %if.end82
  %90 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize86 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %90, i32 0, i32 13
  %91 = load i32, i32* %ssize86, align 4
  %add87 = add nsw i32 %91, 2
  store i32 %add87, i32* %ssize86, align 4
  br label %if.end88

if.end88:                                         ; preds = %if.then85, %if.end82
  %92 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum89 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %92, i32 0, i32 23
  %93 = load i32, i32* %bsnum89, align 4
  %sub90 = sub nsw i32 1, %93
  %94 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum91 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %94, i32 0, i32 23
  store i32 %sub90, i32* %bsnum91, align 4
  %95 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace92 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %95, i32 0, i32 19
  %96 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum93 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %96, i32 0, i32 23
  %97 = load i32, i32* %bsnum93, align 4
  %arrayidx94 = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace92, i32 0, i32 %97
  %arraydecay95 = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx94, i32 0, i32 0
  %add.ptr96 = getelementptr inbounds i8, i8* %arraydecay95, i32 512
  %98 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer97 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %98, i32 0, i32 28
  store i8* %add.ptr96, i8** %wordpointer97, align 4
  %99 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %99, i32 0, i32 27
  store i32 0, i32* %bitindex, align 4
  %100 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold98 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %100, i32 0, i32 15
  %101 = load i32, i32* %fsizeold98, align 4
  %cmp99 = icmp eq i32 %101, -1
  br i1 %cmp99, label %if.then101, label %if.end102

if.then101:                                       ; preds = %if.end88
  store i32 1, i32* %retval, align 4
  br label %return

if.end102:                                        ; preds = %if.end88
  br label %if.end103

if.end103:                                        ; preds = %if.end102, %if.end
  %102 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %side_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %102, i32 0, i32 7
  %103 = load i32, i32* %side_parsed, align 4
  %tobool104 = icmp ne i32 %103, 0
  br i1 %tobool104, label %if.end162, label %if.then105

if.then105:                                       ; preds = %if.end103
  %104 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr106 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %104, i32 0, i32 17
  %lay = getelementptr inbounds %struct.frame, %struct.frame* %fr106, i32 0, i32 5
  %105 = load i32, i32* %lay, align 4
  %cmp107 = icmp eq i32 %105, 3
  br i1 %cmp107, label %if.then109, label %if.else148

if.then109:                                       ; preds = %if.then105
  %106 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize110 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %106, i32 0, i32 11
  %107 = load i32, i32* %bsize110, align 4
  %108 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize111 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %108, i32 0, i32 13
  %109 = load i32, i32* %ssize111, align 4
  %cmp112 = icmp slt i32 %107, %109
  br i1 %cmp112, label %if.then114, label %if.end115

if.then114:                                       ; preds = %if.then109
  store i32 1, i32* %retval, align 4
  br label %return

if.end115:                                        ; preds = %if.then109
  %110 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %111 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize116 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %111, i32 0, i32 13
  %112 = load i32, i32* %ssize116, align 4
  %113 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer117 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %113, i32 0, i32 28
  %114 = load i8*, i8** %wordpointer117, align 4
  call void @copy_mp(%struct.mpstr_tag* %110, i32 %112, i8* %114)
  %115 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr118 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %115, i32 0, i32 17
  %error_protection119 = getelementptr inbounds %struct.frame, %struct.frame* %fr118, i32 0, i32 6
  %116 = load i32, i32* %error_protection119, align 4
  %tobool120 = icmp ne i32 %116, 0
  br i1 %tobool120, label %if.then121, label %if.end123

if.then121:                                       ; preds = %if.end115
  %117 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call122 = call i32 @getbits(%struct.mpstr_tag* %117, i32 16)
  br label %if.end123

if.end123:                                        ; preds = %if.then121, %if.end115
  %118 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call124 = call i32 @decode_layer3_sideinfo(%struct.mpstr_tag* %118)
  store i32 %call124, i32* %bits, align 4
  %119 = load i32, i32* %bits, align 4
  %cmp125 = icmp slt i32 %119, 0
  br i1 %cmp125, label %if.then127, label %if.end128

if.then127:                                       ; preds = %if.end123
  store i32 0, i32* %bits, align 4
  br label %if.end128

if.end128:                                        ; preds = %if.then127, %if.end123
  %120 = load i32, i32* %bits, align 4
  %add129 = add nsw i32 %120, 7
  %div = sdiv i32 %add129, 8
  %121 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %121, i32 0, i32 14
  store i32 %div, i32* %dsize, align 4
  %122 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %free_format130 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %122, i32 0, i32 9
  %123 = load i32, i32* %free_format130, align 4
  %tobool131 = icmp ne i32 %123, 0
  br i1 %tobool131, label %if.end147, label %if.then132

if.then132:                                       ; preds = %if.end128
  %124 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr134 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %124, i32 0, i32 17
  %framesize135 = getelementptr inbounds %struct.frame, %struct.frame* %fr134, i32 0, i32 16
  %125 = load i32, i32* %framesize135, align 4
  %126 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize136 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %126, i32 0, i32 13
  %127 = load i32, i32* %ssize136, align 4
  %sub137 = sub nsw i32 %125, %127
  store i32 %sub137, i32* %framesize133, align 4
  %128 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize138 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %128, i32 0, i32 14
  %129 = load i32, i32* %dsize138, align 4
  %130 = load i32, i32* %framesize133, align 4
  %cmp139 = icmp sgt i32 %129, %130
  br i1 %cmp139, label %if.then141, label %if.end146

if.then141:                                       ; preds = %if.then132
  %131 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err142 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %131, i32 0, i32 32
  %132 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err142, align 4
  %133 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize143 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %133, i32 0, i32 14
  %134 = load i32, i32* %dsize143, align 4
  %135 = load i32, i32* %framesize133, align 4
  %sub144 = sub nsw i32 %134, %135
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %132, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.4, i32 0, i32 0), i32 %sub144)
  %136 = load i32, i32* %framesize133, align 4
  %137 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize145 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %137, i32 0, i32 14
  store i32 %136, i32* %dsize145, align 4
  br label %if.end146

if.end146:                                        ; preds = %if.then141, %if.then132
  br label %if.end147

if.end147:                                        ; preds = %if.end146, %if.end128
  br label %if.end160

if.else148:                                       ; preds = %if.then105
  %138 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr149 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %138, i32 0, i32 17
  %framesize150 = getelementptr inbounds %struct.frame, %struct.frame* %fr149, i32 0, i32 16
  %139 = load i32, i32* %framesize150, align 4
  %140 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize151 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %140, i32 0, i32 11
  %141 = load i32, i32* %bsize151, align 4
  %cmp152 = icmp sgt i32 %139, %141
  br i1 %cmp152, label %if.then154, label %if.end155

if.then154:                                       ; preds = %if.else148
  store i32 1, i32* %retval, align 4
  br label %return

if.end155:                                        ; preds = %if.else148
  %142 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr156 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %142, i32 0, i32 17
  %framesize157 = getelementptr inbounds %struct.frame, %struct.frame* %fr156, i32 0, i32 16
  %143 = load i32, i32* %framesize157, align 4
  %144 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize158 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %144, i32 0, i32 14
  store i32 %143, i32* %dsize158, align 4
  %145 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize159 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %145, i32 0, i32 13
  store i32 0, i32* %ssize159, align 4
  br label %if.end160

if.end160:                                        ; preds = %if.end155, %if.end147
  %146 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %side_parsed161 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %146, i32 0, i32 7
  store i32 1, i32* %side_parsed161, align 4
  br label %if.end162

if.end162:                                        ; preds = %if.end160, %if.end103
  store i32 1, i32* %iret, align 4
  %147 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %data_parsed = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %147, i32 0, i32 8
  %148 = load i32, i32* %data_parsed, align 4
  %tobool163 = icmp ne i32 %148, 0
  br i1 %tobool163, label %if.end210, label %if.then164

if.then164:                                       ; preds = %if.end162
  %149 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize165 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %149, i32 0, i32 14
  %150 = load i32, i32* %dsize165, align 4
  %151 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize166 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %151, i32 0, i32 11
  %152 = load i32, i32* %bsize166, align 4
  %cmp167 = icmp sgt i32 %150, %152
  br i1 %cmp167, label %if.then169, label %if.end170

if.then169:                                       ; preds = %if.then164
  store i32 1, i32* %retval, align 4
  br label %return

if.end170:                                        ; preds = %if.then164
  %153 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %154 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize171 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %154, i32 0, i32 14
  %155 = load i32, i32* %dsize171, align 4
  %156 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer172 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %156, i32 0, i32 28
  %157 = load i8*, i8** %wordpointer172, align 4
  call void @copy_mp(%struct.mpstr_tag* %153, i32 %155, i8* %157)
  %158 = load i32*, i32** %done.addr, align 4
  store i32 0, i32* %158, align 4
  %159 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr173 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %159, i32 0, i32 17
  %lay174 = getelementptr inbounds %struct.frame, %struct.frame* %fr173, i32 0, i32 5
  %160 = load i32, i32* %lay174, align 4
  switch i32 %160, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb186
    i32 3, label %sw.bb194
  ]

sw.bb:                                            ; preds = %if.end170
  %161 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr175 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %161, i32 0, i32 17
  %error_protection176 = getelementptr inbounds %struct.frame, %struct.frame* %fr175, i32 0, i32 6
  %162 = load i32, i32* %error_protection176, align 4
  %tobool177 = icmp ne i32 %162, 0
  br i1 %tobool177, label %if.then178, label %if.end180

if.then178:                                       ; preds = %sw.bb
  %163 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call179 = call i32 @getbits(%struct.mpstr_tag* %163, i32 16)
  br label %if.end180

if.end180:                                        ; preds = %if.then178, %sw.bb
  %164 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %165 = load i8*, i8** %out.addr, align 4
  %166 = load i32*, i32** %done.addr, align 4
  %call181 = call i32 @decode_layer1_frame(%struct.mpstr_tag* %164, i8* %165, i32* %166)
  %cmp182 = icmp slt i32 %call181, 0
  br i1 %cmp182, label %if.then184, label %if.end185

if.then184:                                       ; preds = %if.end180
  store i32 -1, i32* %retval, align 4
  br label %return

if.end185:                                        ; preds = %if.end180
  br label %sw.epilog

sw.bb186:                                         ; preds = %if.end170
  %167 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr187 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %167, i32 0, i32 17
  %error_protection188 = getelementptr inbounds %struct.frame, %struct.frame* %fr187, i32 0, i32 6
  %168 = load i32, i32* %error_protection188, align 4
  %tobool189 = icmp ne i32 %168, 0
  br i1 %tobool189, label %if.then190, label %if.end192

if.then190:                                       ; preds = %sw.bb186
  %169 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call191 = call i32 @getbits(%struct.mpstr_tag* %169, i32 16)
  br label %if.end192

if.end192:                                        ; preds = %if.then190, %sw.bb186
  %170 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %171 = load i8*, i8** %out.addr, align 4
  %172 = load i32*, i32** %done.addr, align 4
  %call193 = call i32 @decode_layer2_frame(%struct.mpstr_tag* %170, i8* %171, i32* %172)
  br label %sw.epilog

sw.bb194:                                         ; preds = %if.end170
  %173 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %174 = load i8*, i8** %out.addr, align 4
  %175 = load i32*, i32** %done.addr, align 4
  %176 = load i32 (%struct.mpstr_tag*, float*, i8*, i32*)*, i32 (%struct.mpstr_tag*, float*, i8*, i32*)** %synth_1to1_mono_ptr.addr, align 4
  %177 = load i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)*, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)** %synth_1to1_ptr.addr, align 4
  %call195 = call i32 @decode_layer3_frame(%struct.mpstr_tag* %173, i8* %174, i32* %175, i32 (%struct.mpstr_tag*, float*, i8*, i32*)* %176, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)* %177)
  br label %sw.epilog

sw.default:                                       ; preds = %if.end170
  %178 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err196 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %178, i32 0, i32 32
  %179 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err196, align 4
  %180 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr197 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %180, i32 0, i32 17
  %lay198 = getelementptr inbounds %struct.frame, %struct.frame* %fr197, i32 0, i32 5
  %181 = load i32, i32* %lay198, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %179, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0), i32 %181)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb194, %if.end192, %if.end185
  %182 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace199 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %182, i32 0, i32 19
  %183 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum200 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %183, i32 0, i32 23
  %184 = load i32, i32* %bsnum200, align 4
  %arrayidx201 = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace199, i32 0, i32 %184
  %arraydecay202 = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx201, i32 0, i32 0
  %add.ptr203 = getelementptr inbounds i8, i8* %arraydecay202, i32 512
  %185 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize204 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %185, i32 0, i32 13
  %186 = load i32, i32* %ssize204, align 4
  %add.ptr205 = getelementptr inbounds i8, i8* %add.ptr203, i32 %186
  %187 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize206 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %187, i32 0, i32 14
  %188 = load i32, i32* %dsize206, align 4
  %add.ptr207 = getelementptr inbounds i8, i8* %add.ptr205, i32 %188
  %189 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer208 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %189, i32 0, i32 28
  store i8* %add.ptr207, i8** %wordpointer208, align 4
  %190 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %data_parsed209 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %190, i32 0, i32 8
  store i32 1, i32* %data_parsed209, align 4
  store i32 0, i32* %iret, align 4
  br label %if.end210

if.end210:                                        ; preds = %sw.epilog, %if.end162
  %191 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %free_format211 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %191, i32 0, i32 9
  %192 = load i32, i32* %free_format211, align 4
  %tobool212 = icmp ne i32 %192, 0
  br i1 %tobool212, label %if.then213, label %if.end237

if.then213:                                       ; preds = %if.end210
  %193 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %old_free_format214 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %193, i32 0, i32 10
  %194 = load i32, i32* %old_free_format214, align 4
  %tobool215 = icmp ne i32 %194, 0
  br i1 %tobool215, label %if.then216, label %if.else220

if.then216:                                       ; preds = %if.then213
  %195 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold_nopadding = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %195, i32 0, i32 16
  %196 = load i32, i32* %fsizeold_nopadding, align 4
  %197 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr217 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %197, i32 0, i32 17
  %padding = getelementptr inbounds %struct.frame, %struct.frame* %fr217, i32 0, i32 9
  %198 = load i32, i32* %padding, align 4
  %add218 = add nsw i32 %196, %198
  %199 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize219 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %199, i32 0, i32 12
  store i32 %add218, i32* %framesize219, align 4
  br label %if.end236

if.else220:                                       ; preds = %if.then213
  %200 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call221 = call i32 @sync_buffer(%struct.mpstr_tag* %200, i32 1)
  store i32 %call221, i32* %bytes, align 4
  %201 = load i32, i32* %bytes, align 4
  %cmp222 = icmp slt i32 %201, 0
  br i1 %cmp222, label %if.then224, label %if.end225

if.then224:                                       ; preds = %if.else220
  %202 = load i32, i32* %iret, align 4
  store i32 %202, i32* %retval, align 4
  br label %return

if.end225:                                        ; preds = %if.else220
  %203 = load i32, i32* %bytes, align 4
  %204 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize226 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %204, i32 0, i32 13
  %205 = load i32, i32* %ssize226, align 4
  %add227 = add nsw i32 %203, %205
  %206 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize228 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %206, i32 0, i32 14
  %207 = load i32, i32* %dsize228, align 4
  %add229 = add nsw i32 %add227, %207
  %208 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize230 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %208, i32 0, i32 12
  store i32 %add229, i32* %framesize230, align 4
  %209 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize231 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %209, i32 0, i32 12
  %210 = load i32, i32* %framesize231, align 4
  %211 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr232 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %211, i32 0, i32 17
  %padding233 = getelementptr inbounds %struct.frame, %struct.frame* %fr232, i32 0, i32 9
  %212 = load i32, i32* %padding233, align 4
  %sub234 = sub nsw i32 %210, %212
  %213 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold_nopadding235 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %213, i32 0, i32 16
  store i32 %sub234, i32* %fsizeold_nopadding235, align 4
  br label %if.end236

if.end236:                                        ; preds = %if.end225, %if.then216
  br label %if.end237

if.end237:                                        ; preds = %if.end236, %if.end210
  %214 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize238 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %214, i32 0, i32 12
  %215 = load i32, i32* %framesize238, align 4
  %216 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %ssize239 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %216, i32 0, i32 13
  %217 = load i32, i32* %ssize239, align 4
  %218 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %dsize240 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %218, i32 0, i32 14
  %219 = load i32, i32* %dsize240, align 4
  %add241 = add nsw i32 %217, %219
  %sub242 = sub nsw i32 %215, %add241
  store i32 %sub242, i32* %bytes, align 4
  %220 = load i32, i32* %bytes, align 4
  %221 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize243 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %221, i32 0, i32 11
  %222 = load i32, i32* %bsize243, align 4
  %cmp244 = icmp sgt i32 %220, %222
  br i1 %cmp244, label %if.then246, label %if.end247

if.then246:                                       ; preds = %if.end237
  %223 = load i32, i32* %iret, align 4
  store i32 %223, i32* %retval, align 4
  br label %return

if.end247:                                        ; preds = %if.end237
  %224 = load i32, i32* %bytes, align 4
  %cmp248 = icmp sgt i32 %224, 0
  br i1 %cmp248, label %if.then250, label %if.end275

if.then250:                                       ; preds = %if.end247
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then250
  %225 = load i32, i32* %bytes, align 4
  %cmp252 = icmp sgt i32 %225, 512
  br i1 %cmp252, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %226 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call254 = call i32 @read_buf_byte(%struct.mpstr_tag* %226)
  %227 = load i32, i32* %bytes, align 4
  %dec255 = add nsw i32 %227, -1
  store i32 %dec255, i32* %bytes, align 4
  %228 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize256 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %228, i32 0, i32 12
  %229 = load i32, i32* %framesize256, align 4
  %dec257 = add nsw i32 %229, -1
  store i32 %dec257, i32* %framesize256, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %230 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %231 = load i32, i32* %bytes, align 4
  %232 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer258 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %232, i32 0, i32 28
  %233 = load i8*, i8** %wordpointer258, align 4
  call void @copy_mp(%struct.mpstr_tag* %230, i32 %231, i8* %233)
  %234 = load i32, i32* %bytes, align 4
  %235 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer259 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %235, i32 0, i32 28
  %236 = load i8*, i8** %wordpointer259, align 4
  %add.ptr260 = getelementptr inbounds i8, i8* %236, i32 %234
  store i8* %add.ptr260, i8** %wordpointer259, align 4
  %237 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer261 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %237, i32 0, i32 28
  %238 = load i8*, i8** %wordpointer261, align 4
  %239 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace262 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %239, i32 0, i32 19
  %240 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum263 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %240, i32 0, i32 23
  %241 = load i32, i32* %bsnum263, align 4
  %arrayidx264 = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace262, i32 0, i32 %241
  %arraydecay265 = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx264, i32 0, i32 0
  %add.ptr266 = getelementptr inbounds i8, i8* %arraydecay265, i32 512
  %sub.ptr.lhs.cast267 = ptrtoint i8* %238 to i32
  %sub.ptr.rhs.cast268 = ptrtoint i8* %add.ptr266 to i32
  %sub.ptr.sub269 = sub i32 %sub.ptr.lhs.cast267, %sub.ptr.rhs.cast268
  store i32 %sub.ptr.sub269, i32* %size251, align 4
  %242 = load i32, i32* %size251, align 4
  %cmp270 = icmp sgt i32 %242, 2880
  br i1 %cmp270, label %if.then272, label %if.end274

if.then272:                                       ; preds = %while.end
  %243 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err273 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %243, i32 0, i32 32
  %244 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err273, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %244, i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.6, i32 0, i32 0))
  br label %if.end274

if.end274:                                        ; preds = %if.then272, %while.end
  br label %if.end275

if.end275:                                        ; preds = %if.end274, %if.end247
  %245 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize276 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %245, i32 0, i32 12
  %246 = load i32, i32* %framesize276, align 4
  %247 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold277 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %247, i32 0, i32 15
  store i32 %246, i32* %fsizeold277, align 4
  %248 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %free_format278 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %248, i32 0, i32 9
  %249 = load i32, i32* %free_format278, align 4
  %250 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %old_free_format279 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %250, i32 0, i32 10
  store i32 %249, i32* %old_free_format279, align 4
  %251 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %framesize280 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %251, i32 0, i32 12
  store i32 0, i32* %framesize280, align 4
  %252 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header_parsed281 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %252, i32 0, i32 6
  store i32 0, i32* %header_parsed281, align 4
  %253 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %side_parsed282 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %253, i32 0, i32 7
  store i32 0, i32* %side_parsed282, align 4
  %254 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %data_parsed283 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %254, i32 0, i32 8
  store i32 0, i32* %data_parsed283, align 4
  %255 = load i32, i32* %iret, align 4
  store i32 %255, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end275, %if.then246, %if.then224, %if.then184, %if.then169, %if.then154, %if.then114, %if.then101, %if.then62, %if.then29, %for.end, %if.then19, %if.else, %if.then
  %256 = load i32, i32* %retval, align 4
  ret i32 %256
}

declare i32 @synth_1to1_mono(%struct.mpstr_tag*, float*, i8*, i32*) #1

declare i32 @synth_1to1(%struct.mpstr_tag*, float*, i32, i8*, i32*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @decodeMP3_unclipped(%struct.mpstr_tag* %mp, i8* %in, i32 %isize, i8* %out, i32 %osize, i32* %done) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %in.addr = alloca i8*, align 4
  %isize.addr = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %osize.addr = alloca i32, align 4
  %done.addr = alloca i32*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %isize, i32* %isize.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %osize, i32* %osize.addr, align 4
  store i32* %done, i32** %done.addr, align 4
  %0 = load i32, i32* %osize.addr, align 4
  %cmp = icmp slt i32 %0, 9216
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %1, i32 0, i32 32
  %2 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %2, i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0))
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %4 = load i8*, i8** %in.addr, align 4
  %5 = load i32, i32* %isize.addr, align 4
  %6 = load i8*, i8** %out.addr, align 4
  %7 = load i32*, i32** %done.addr, align 4
  %call = call i32 @decodeMP3_clipchoice(%struct.mpstr_tag* %3, i8* %4, i32 %5, i8* %6, i32* %7, i32 (%struct.mpstr_tag*, float*, i8*, i32*)* @synth_1to1_mono_unclipped, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)* @synth_1to1_unclipped)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @synth_1to1_mono_unclipped(%struct.mpstr_tag*, float*, i8*, i32*) #1

declare i32 @synth_1to1_unclipped(%struct.mpstr_tag*, float*, i32, i8*, i32*) #1

; Function Attrs: noinline nounwind optnone
define internal %struct.buf* @addbuf(%struct.mpstr_tag* %mp, i8* %buf, i32 %size) #0 {
entry:
  %retval = alloca %struct.buf*, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %buf.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %nbuf = alloca %struct.buf*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %call = call i8* @malloc(i32 20)
  %0 = bitcast i8* %call to %struct.buf*
  store %struct.buf* %0, %struct.buf** %nbuf, align 4
  %1 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %tobool = icmp ne %struct.buf* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %2 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %2, i32 0, i32 32
  %3 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %3, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.7, i32 0, i32 0))
  store %struct.buf* null, %struct.buf** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %size.addr, align 4
  %call1 = call i8* @malloc(i32 %4)
  %5 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %5, i32 0, i32 0
  store i8* %call1, i8** %pnt, align 4
  %6 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %pnt2 = getelementptr inbounds %struct.buf, %struct.buf* %6, i32 0, i32 0
  %7 = load i8*, i8** %pnt2, align 4
  %tobool3 = icmp ne i8* %7, null
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end
  %8 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %9 = bitcast %struct.buf* %8 to i8*
  call void @free(i8* %9)
  store %struct.buf* null, %struct.buf** %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %10 = load i32, i32* %size.addr, align 4
  %11 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %size6 = getelementptr inbounds %struct.buf, %struct.buf* %11, i32 0, i32 1
  store i32 %10, i32* %size6, align 4
  %12 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %pnt7 = getelementptr inbounds %struct.buf, %struct.buf* %12, i32 0, i32 0
  %13 = load i8*, i8** %pnt7, align 4
  %14 = load i8*, i8** %buf.addr, align 4
  %15 = load i32, i32* %size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %13, i8* align 1 %14, i32 %15, i1 false)
  %16 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %next = getelementptr inbounds %struct.buf, %struct.buf* %16, i32 0, i32 3
  store %struct.buf* null, %struct.buf** %next, align 4
  %17 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %head = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %17, i32 0, i32 0
  %18 = load %struct.buf*, %struct.buf** %head, align 4
  %19 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %prev = getelementptr inbounds %struct.buf, %struct.buf* %19, i32 0, i32 4
  store %struct.buf* %18, %struct.buf** %prev, align 4
  %20 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %pos = getelementptr inbounds %struct.buf, %struct.buf* %20, i32 0, i32 2
  store i32 0, i32* %pos, align 4
  %21 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %21, i32 0, i32 1
  %22 = load %struct.buf*, %struct.buf** %tail, align 4
  %tobool8 = icmp ne %struct.buf* %22, null
  br i1 %tobool8, label %if.else, label %if.then9

if.then9:                                         ; preds = %if.end5
  %23 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail10 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %24, i32 0, i32 1
  store %struct.buf* %23, %struct.buf** %tail10, align 4
  br label %if.end13

if.else:                                          ; preds = %if.end5
  %25 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %26 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %head11 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %26, i32 0, i32 0
  %27 = load %struct.buf*, %struct.buf** %head11, align 4
  %next12 = getelementptr inbounds %struct.buf, %struct.buf* %27, i32 0, i32 3
  store %struct.buf* %25, %struct.buf** %next12, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then9
  %28 = load %struct.buf*, %struct.buf** %nbuf, align 4
  %29 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %head14 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %29, i32 0, i32 0
  store %struct.buf* %28, %struct.buf** %head14, align 4
  %30 = load i32, i32* %size.addr, align 4
  %31 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %31, i32 0, i32 11
  %32 = load i32, i32* %bsize, align 4
  %add = add nsw i32 %32, %30
  store i32 %add, i32* %bsize, align 4
  %33 = load %struct.buf*, %struct.buf** %nbuf, align 4
  store %struct.buf* %33, %struct.buf** %retval, align 4
  br label %return

return:                                           ; preds = %if.end13, %if.then4, %if.then
  %34 = load %struct.buf*, %struct.buf** %retval, align 4
  ret %struct.buf* %34
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sync_buffer(%struct.mpstr_tag* %mp, i32 %free_match) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %free_match.addr = alloca i32, align 4
  %b = alloca [4 x i32], align 16
  %i = alloca i32, align 4
  %h = alloca i32, align 4
  %pos = alloca i32, align 4
  %buf = alloca %struct.buf*, align 4
  %fr = alloca %struct.frame*, align 4
  %head = alloca i32, align 4
  %mode = alloca i32, align 4
  %stereo = alloca i32, align 4
  %sampling_frequency = alloca i32, align 4
  %mpeg25 = alloca i32, align 4
  %lsf = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %free_match, i32* %free_match.addr, align 4
  %0 = bitcast [4 x i32]* %b to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %0, i8 0, i32 16, i1 false)
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %1, i32 0, i32 1
  %2 = load %struct.buf*, %struct.buf** %tail, align 4
  store %struct.buf* %2, %struct.buf** %buf, align 4
  %3 = load %struct.buf*, %struct.buf** %buf, align 4
  %tobool = icmp ne %struct.buf* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %struct.buf*, %struct.buf** %buf, align 4
  %pos1 = getelementptr inbounds %struct.buf, %struct.buf* %4, i32 0, i32 2
  %5 = load i32, i32* %pos1, align 4
  store i32 %5, i32* %pos, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %6 = load i32, i32* %i, align 4
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 11
  %8 = load i32, i32* %bsize, align 4
  %cmp = icmp slt i32 %6, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 1
  %9 = load i32, i32* %arrayidx, align 4
  %arrayidx2 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 0
  store i32 %9, i32* %arrayidx2, align 16
  %arrayidx3 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 2
  %10 = load i32, i32* %arrayidx3, align 8
  %arrayidx4 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 1
  store i32 %10, i32* %arrayidx4, align 4
  %arrayidx5 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 3
  %11 = load i32, i32* %arrayidx5, align 4
  %arrayidx6 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 2
  store i32 %11, i32* %arrayidx6, align 8
  br label %while.cond

while.cond:                                       ; preds = %if.end10, %for.body
  %12 = load i32, i32* %pos, align 4
  %13 = load %struct.buf*, %struct.buf** %buf, align 4
  %size = getelementptr inbounds %struct.buf, %struct.buf* %13, i32 0, i32 1
  %14 = load i32, i32* %size, align 4
  %cmp7 = icmp sge i32 %12, %14
  br i1 %cmp7, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %15 = load %struct.buf*, %struct.buf** %buf, align 4
  %next = getelementptr inbounds %struct.buf, %struct.buf* %15, i32 0, i32 3
  %16 = load %struct.buf*, %struct.buf** %next, align 4
  store %struct.buf* %16, %struct.buf** %buf, align 4
  %17 = load %struct.buf*, %struct.buf** %buf, align 4
  %tobool8 = icmp ne %struct.buf* %17, null
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %while.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %while.body
  %18 = load %struct.buf*, %struct.buf** %buf, align 4
  %pos11 = getelementptr inbounds %struct.buf, %struct.buf* %18, i32 0, i32 2
  %19 = load i32, i32* %pos11, align 4
  store i32 %19, i32* %pos, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %20 = load %struct.buf*, %struct.buf** %buf, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %20, i32 0, i32 0
  %21 = load i8*, i8** %pnt, align 4
  %22 = load i32, i32* %pos, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx12, align 1
  %conv = zext i8 %23 to i32
  %arrayidx13 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 3
  store i32 %conv, i32* %arrayidx13, align 4
  %24 = load i32, i32* %pos, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %pos, align 4
  %25 = load i32, i32* %i, align 4
  %cmp14 = icmp sge i32 %25, 3
  br i1 %cmp14, label %if.then16, label %if.end65

if.then16:                                        ; preds = %while.end
  %26 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr17 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %26, i32 0, i32 17
  store %struct.frame* %fr17, %struct.frame** %fr, align 4
  %arrayidx18 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 0
  %27 = load i32, i32* %arrayidx18, align 16
  store i32 %27, i32* %head, align 4
  %28 = load i32, i32* %head, align 4
  %shl = shl i32 %28, 8
  store i32 %shl, i32* %head, align 4
  %arrayidx19 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 1
  %29 = load i32, i32* %arrayidx19, align 4
  %30 = load i32, i32* %head, align 4
  %or = or i32 %30, %29
  store i32 %or, i32* %head, align 4
  %31 = load i32, i32* %head, align 4
  %shl20 = shl i32 %31, 8
  store i32 %shl20, i32* %head, align 4
  %arrayidx21 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 2
  %32 = load i32, i32* %arrayidx21, align 8
  %33 = load i32, i32* %head, align 4
  %or22 = or i32 %33, %32
  store i32 %or22, i32* %head, align 4
  %34 = load i32, i32* %head, align 4
  %shl23 = shl i32 %34, 8
  store i32 %shl23, i32* %head, align 4
  %arrayidx24 = getelementptr inbounds [4 x i32], [4 x i32]* %b, i32 0, i32 3
  %35 = load i32, i32* %arrayidx24, align 4
  %36 = load i32, i32* %head, align 4
  %or25 = or i32 %36, %35
  store i32 %or25, i32* %head, align 4
  %37 = load i32, i32* %head, align 4
  %38 = load %struct.frame*, %struct.frame** %fr, align 4
  %lay = getelementptr inbounds %struct.frame, %struct.frame* %38, i32 0, i32 5
  %39 = load i32, i32* %lay, align 4
  %call = call i32 @head_check(i32 %37, i32 %39)
  store i32 %call, i32* %h, align 4
  %40 = load i32, i32* %h, align 4
  %tobool26 = icmp ne i32 %40, 0
  br i1 %tobool26, label %land.lhs.true, label %if.end61

land.lhs.true:                                    ; preds = %if.then16
  %41 = load i32, i32* %free_match.addr, align 4
  %tobool27 = icmp ne i32 %41, 0
  br i1 %tobool27, label %if.then28, label %if.end61

if.then28:                                        ; preds = %land.lhs.true
  %42 = load i32, i32* %head, align 4
  %and = and i32 %42, 1048576
  %tobool29 = icmp ne i32 %and, 0
  br i1 %tobool29, label %if.then30, label %if.else

if.then30:                                        ; preds = %if.then28
  %43 = load i32, i32* %head, align 4
  %and31 = and i32 %43, 524288
  %tobool32 = icmp ne i32 %and31, 0
  %44 = zext i1 %tobool32 to i64
  %cond = select i1 %tobool32, i32 0, i32 1
  store i32 %cond, i32* %lsf, align 4
  store i32 0, i32* %mpeg25, align 4
  br label %if.end33

if.else:                                          ; preds = %if.then28
  store i32 1, i32* %lsf, align 4
  store i32 1, i32* %mpeg25, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.else, %if.then30
  %45 = load i32, i32* %head, align 4
  %shr = lshr i32 %45, 6
  %and34 = and i32 %shr, 3
  store i32 %and34, i32* %mode, align 4
  %46 = load i32, i32* %mode, align 4
  %cmp35 = icmp eq i32 %46, 3
  %47 = zext i1 %cmp35 to i64
  %cond37 = select i1 %cmp35, i32 1, i32 2
  store i32 %cond37, i32* %stereo, align 4
  %48 = load i32, i32* %mpeg25, align 4
  %tobool38 = icmp ne i32 %48, 0
  br i1 %tobool38, label %if.then39, label %if.else42

if.then39:                                        ; preds = %if.end33
  %49 = load i32, i32* %head, align 4
  %shr40 = lshr i32 %49, 10
  %and41 = and i32 %shr40, 3
  %add = add i32 6, %and41
  store i32 %add, i32* %sampling_frequency, align 4
  br label %if.end46

if.else42:                                        ; preds = %if.end33
  %50 = load i32, i32* %head, align 4
  %shr43 = lshr i32 %50, 10
  %and44 = and i32 %shr43, 3
  %51 = load i32, i32* %lsf, align 4
  %mul = mul nsw i32 %51, 3
  %add45 = add i32 %and44, %mul
  store i32 %add45, i32* %sampling_frequency, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.else42, %if.then39
  %52 = load i32, i32* %stereo, align 4
  %53 = load %struct.frame*, %struct.frame** %fr, align 4
  %stereo47 = getelementptr inbounds %struct.frame, %struct.frame* %53, i32 0, i32 0
  %54 = load i32, i32* %stereo47, align 4
  %cmp48 = icmp eq i32 %52, %54
  br i1 %cmp48, label %land.lhs.true50, label %land.end

land.lhs.true50:                                  ; preds = %if.end46
  %55 = load i32, i32* %lsf, align 4
  %56 = load %struct.frame*, %struct.frame** %fr, align 4
  %lsf51 = getelementptr inbounds %struct.frame, %struct.frame* %56, i32 0, i32 2
  %57 = load i32, i32* %lsf51, align 4
  %cmp52 = icmp eq i32 %55, %57
  br i1 %cmp52, label %land.lhs.true54, label %land.end

land.lhs.true54:                                  ; preds = %land.lhs.true50
  %58 = load i32, i32* %mpeg25, align 4
  %59 = load %struct.frame*, %struct.frame** %fr, align 4
  %mpeg2555 = getelementptr inbounds %struct.frame, %struct.frame* %59, i32 0, i32 3
  %60 = load i32, i32* %mpeg2555, align 4
  %cmp56 = icmp eq i32 %58, %60
  br i1 %cmp56, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true54
  %61 = load i32, i32* %sampling_frequency, align 4
  %62 = load %struct.frame*, %struct.frame** %fr, align 4
  %sampling_frequency58 = getelementptr inbounds %struct.frame, %struct.frame* %62, i32 0, i32 8
  %63 = load i32, i32* %sampling_frequency58, align 4
  %cmp59 = icmp eq i32 %61, %63
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true54, %land.lhs.true50, %if.end46
  %64 = phi i1 [ false, %land.lhs.true54 ], [ false, %land.lhs.true50 ], [ false, %if.end46 ], [ %cmp59, %land.rhs ]
  %land.ext = zext i1 %64 to i32
  store i32 %land.ext, i32* %h, align 4
  br label %if.end61

if.end61:                                         ; preds = %land.end, %land.lhs.true, %if.then16
  %65 = load i32, i32* %h, align 4
  %tobool62 = icmp ne i32 %65, 0
  br i1 %tobool62, label %if.then63, label %if.end64

if.then63:                                        ; preds = %if.end61
  %66 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %66, 3
  store i32 %sub, i32* %retval, align 4
  br label %return

if.end64:                                         ; preds = %if.end61
  br label %if.end65

if.end65:                                         ; preds = %if.end64, %while.end
  br label %for.inc

for.inc:                                          ; preds = %if.end65
  %67 = load i32, i32* %i, align 4
  %inc66 = add nsw i32 %67, 1
  store i32 %inc66, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then63, %if.then9, %if.then
  %68 = load i32, i32* %retval, align 4
  ret i32 %68
}

; Function Attrs: noinline nounwind optnone
define internal i32 @check_vbr_header(%struct.mpstr_tag* %mp, i32 %bytes) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %bytes.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %pos = alloca i32, align 4
  %buf = alloca %struct.buf*, align 4
  %xing = alloca [194 x i8], align 16
  %pTagData = alloca %struct.VBRTAGDATA, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %bytes, i32* %bytes.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 1
  %1 = load %struct.buf*, %struct.buf** %tail, align 4
  store %struct.buf* %1, %struct.buf** %buf, align 4
  %2 = load %struct.buf*, %struct.buf** %buf, align 4
  %pos1 = getelementptr inbounds %struct.buf, %struct.buf* %2, i32 0, i32 2
  %3 = load i32, i32* %pos1, align 4
  store i32 %3, i32* %pos, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %bytes.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %while.cond

while.cond:                                       ; preds = %if.end, %for.body
  %6 = load i32, i32* %pos, align 4
  %7 = load %struct.buf*, %struct.buf** %buf, align 4
  %size = getelementptr inbounds %struct.buf, %struct.buf* %7, i32 0, i32 1
  %8 = load i32, i32* %size, align 4
  %cmp2 = icmp sge i32 %6, %8
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load %struct.buf*, %struct.buf** %buf, align 4
  %next = getelementptr inbounds %struct.buf, %struct.buf* %9, i32 0, i32 3
  %10 = load %struct.buf*, %struct.buf** %next, align 4
  store %struct.buf* %10, %struct.buf** %buf, align 4
  %11 = load %struct.buf*, %struct.buf** %buf, align 4
  %tobool = icmp ne %struct.buf* %11, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %while.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %12 = load %struct.buf*, %struct.buf** %buf, align 4
  %pos3 = getelementptr inbounds %struct.buf, %struct.buf* %12, i32 0, i32 2
  %13 = load i32, i32* %pos3, align 4
  store i32 %13, i32* %pos, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %14 = load i32, i32* %pos, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %pos, align 4
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %15 = load i32, i32* %i, align 4
  %inc4 = add nsw i32 %15, 1
  store i32 %inc4, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc20, %for.end
  %16 = load i32, i32* %i, align 4
  %cmp6 = icmp slt i32 %16, 194
  br i1 %cmp6, label %for.body7, label %for.end22

for.body7:                                        ; preds = %for.cond5
  br label %while.cond8

while.cond8:                                      ; preds = %if.end15, %for.body7
  %17 = load i32, i32* %pos, align 4
  %18 = load %struct.buf*, %struct.buf** %buf, align 4
  %size9 = getelementptr inbounds %struct.buf, %struct.buf* %18, i32 0, i32 1
  %19 = load i32, i32* %size9, align 4
  %cmp10 = icmp sge i32 %17, %19
  br i1 %cmp10, label %while.body11, label %while.end17

while.body11:                                     ; preds = %while.cond8
  %20 = load %struct.buf*, %struct.buf** %buf, align 4
  %next12 = getelementptr inbounds %struct.buf, %struct.buf* %20, i32 0, i32 3
  %21 = load %struct.buf*, %struct.buf** %next12, align 4
  store %struct.buf* %21, %struct.buf** %buf, align 4
  %22 = load %struct.buf*, %struct.buf** %buf, align 4
  %tobool13 = icmp ne %struct.buf* %22, null
  br i1 %tobool13, label %if.end15, label %if.then14

if.then14:                                        ; preds = %while.body11
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %while.body11
  %23 = load %struct.buf*, %struct.buf** %buf, align 4
  %pos16 = getelementptr inbounds %struct.buf, %struct.buf* %23, i32 0, i32 2
  %24 = load i32, i32* %pos16, align 4
  store i32 %24, i32* %pos, align 4
  br label %while.cond8

while.end17:                                      ; preds = %while.cond8
  %25 = load %struct.buf*, %struct.buf** %buf, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %25, i32 0, i32 0
  %26 = load i8*, i8** %pnt, align 4
  %27 = load i32, i32* %pos, align 4
  %arrayidx = getelementptr inbounds i8, i8* %26, i32 %27
  %28 = load i8, i8* %arrayidx, align 1
  %29 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [194 x i8], [194 x i8]* %xing, i32 0, i32 %29
  store i8 %28, i8* %arrayidx18, align 1
  %30 = load i32, i32* %pos, align 4
  %inc19 = add nsw i32 %30, 1
  store i32 %inc19, i32* %pos, align 4
  br label %for.inc20

for.inc20:                                        ; preds = %while.end17
  %31 = load i32, i32* %i, align 4
  %inc21 = add nsw i32 %31, 1
  store i32 %inc21, i32* %i, align 4
  br label %for.cond5

for.end22:                                        ; preds = %for.cond5
  %arraydecay = getelementptr inbounds [194 x i8], [194 x i8]* %xing, i32 0, i32 0
  %call = call i32 @GetVbrTag(%struct.VBRTAGDATA* %pTagData, i8* %arraydecay)
  %32 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %vbr_header = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %32, i32 0, i32 2
  store i32 %call, i32* %vbr_header, align 4
  %33 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %vbr_header23 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %33, i32 0, i32 2
  %34 = load i32, i32* %vbr_header23, align 4
  %tobool24 = icmp ne i32 %34, 0
  br i1 %tobool24, label %if.then25, label %if.end32

if.then25:                                        ; preds = %for.end22
  %frames = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %pTagData, i32 0, i32 3
  %35 = load i32, i32* %frames, align 4
  %36 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %num_frames = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %36, i32 0, i32 3
  store i32 %35, i32* %num_frames, align 4
  %enc_delay = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %pTagData, i32 0, i32 8
  %37 = load i32, i32* %enc_delay, align 4
  %38 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %enc_delay26 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %38, i32 0, i32 4
  store i32 %37, i32* %enc_delay26, align 4
  %enc_padding = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %pTagData, i32 0, i32 9
  %39 = load i32, i32* %enc_padding, align 4
  %40 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %enc_padding27 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %40, i32 0, i32 5
  store i32 %39, i32* %enc_padding27, align 4
  %headersize = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %pTagData, i32 0, i32 7
  %41 = load i32, i32* %headersize, align 4
  %cmp28 = icmp slt i32 %41, 1
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.then25
  store i32 1, i32* %retval, align 4
  br label %return

if.end30:                                         ; preds = %if.then25
  %headersize31 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %pTagData, i32 0, i32 7
  %42 = load i32, i32* %headersize31, align 4
  store i32 %42, i32* %retval, align 4
  br label %return

if.end32:                                         ; preds = %for.end22
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end32, %if.end30, %if.then29, %if.then14, %if.then
  %43 = load i32, i32* %retval, align 4
  ret i32 %43
}

; Function Attrs: noinline nounwind optnone
define internal i32 @read_buf_byte(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %b = alloca i32, align 4
  %pos = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 1
  %1 = load %struct.buf*, %struct.buf** %tail, align 4
  %pos1 = getelementptr inbounds %struct.buf, %struct.buf* %1, i32 0, i32 2
  %2 = load i32, i32* %pos1, align 4
  store i32 %2, i32* %pos, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %3 = load i32, i32* %pos, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail2 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 1
  %5 = load %struct.buf*, %struct.buf** %tail2, align 4
  %size = getelementptr inbounds %struct.buf, %struct.buf* %5, i32 0, i32 1
  %6 = load i32, i32* %size, align 4
  %cmp = icmp sge i32 %3, %6
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  call void @remove_buf(%struct.mpstr_tag* %7)
  %8 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail3 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %8, i32 0, i32 1
  %9 = load %struct.buf*, %struct.buf** %tail3, align 4
  %tobool = icmp ne %struct.buf* %9, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %while.body
  %10 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %10, i32 0, i32 32
  %11 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %11, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.8, i32 0, i32 0))
  call void @exit(i32 1) #5
  unreachable

if.end:                                           ; preds = %while.body
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail4 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %12, i32 0, i32 1
  %13 = load %struct.buf*, %struct.buf** %tail4, align 4
  %pos5 = getelementptr inbounds %struct.buf, %struct.buf* %13, i32 0, i32 2
  %14 = load i32, i32* %pos5, align 4
  store i32 %14, i32* %pos, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail6 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %15, i32 0, i32 1
  %16 = load %struct.buf*, %struct.buf** %tail6, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %16, i32 0, i32 0
  %17 = load i8*, i8** %pnt, align 4
  %18 = load i32, i32* %pos, align 4
  %arrayidx = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %19 to i32
  store i32 %conv, i32* %b, align 4
  %20 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %20, i32 0, i32 11
  %21 = load i32, i32* %bsize, align 4
  %dec = add nsw i32 %21, -1
  store i32 %dec, i32* %bsize, align 4
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail7 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %22, i32 0, i32 1
  %23 = load %struct.buf*, %struct.buf** %tail7, align 4
  %pos8 = getelementptr inbounds %struct.buf, %struct.buf* %23, i32 0, i32 2
  %24 = load i32, i32* %pos8, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %pos8, align 4
  %25 = load i32, i32* %b, align 4
  ret i32 %25
}

; Function Attrs: noinline nounwind optnone
define internal void @copy_mp(%struct.mpstr_tag* %mp, i32 %size, i8* %ptr) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %size.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  %len = alloca i32, align 4
  %nlen = alloca i32, align 4
  %blen = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 0, i32* %len, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end21, %entry
  %0 = load i32, i32* %len, align 4
  %1 = load i32, i32* %size.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %2, i32 0, i32 1
  %3 = load %struct.buf*, %struct.buf** %tail, align 4
  %tobool = icmp ne %struct.buf* %3, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %4 = phi i1 [ false, %while.cond ], [ %tobool, %land.rhs ]
  br i1 %4, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %5 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %5, i32 0, i32 1
  %6 = load %struct.buf*, %struct.buf** %tail1, align 4
  %size2 = getelementptr inbounds %struct.buf, %struct.buf* %6, i32 0, i32 1
  %7 = load i32, i32* %size2, align 4
  %8 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail3 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %8, i32 0, i32 1
  %9 = load %struct.buf*, %struct.buf** %tail3, align 4
  %pos = getelementptr inbounds %struct.buf, %struct.buf* %9, i32 0, i32 2
  %10 = load i32, i32* %pos, align 4
  %sub = sub nsw i32 %7, %10
  store i32 %sub, i32* %blen, align 4
  %11 = load i32, i32* %size.addr, align 4
  %12 = load i32, i32* %len, align 4
  %sub4 = sub nsw i32 %11, %12
  %13 = load i32, i32* %blen, align 4
  %cmp5 = icmp sle i32 %sub4, %13
  br i1 %cmp5, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %14 = load i32, i32* %size.addr, align 4
  %15 = load i32, i32* %len, align 4
  %sub6 = sub nsw i32 %14, %15
  store i32 %sub6, i32* %nlen, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %16 = load i32, i32* %blen, align 4
  store i32 %16, i32* %nlen, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load i8*, i8** %ptr.addr, align 4
  %18 = load i32, i32* %len, align 4
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail7 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %19, i32 0, i32 1
  %20 = load %struct.buf*, %struct.buf** %tail7, align 4
  %pnt = getelementptr inbounds %struct.buf, %struct.buf* %20, i32 0, i32 0
  %21 = load i8*, i8** %pnt, align 4
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail8 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %22, i32 0, i32 1
  %23 = load %struct.buf*, %struct.buf** %tail8, align 4
  %pos9 = getelementptr inbounds %struct.buf, %struct.buf* %23, i32 0, i32 2
  %24 = load i32, i32* %pos9, align 4
  %add.ptr10 = getelementptr inbounds i8, i8* %21, i32 %24
  %25 = load i32, i32* %nlen, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %add.ptr10, i32 %25, i1 false)
  %26 = load i32, i32* %nlen, align 4
  %27 = load i32, i32* %len, align 4
  %add = add nsw i32 %27, %26
  store i32 %add, i32* %len, align 4
  %28 = load i32, i32* %nlen, align 4
  %29 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail11 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %29, i32 0, i32 1
  %30 = load %struct.buf*, %struct.buf** %tail11, align 4
  %pos12 = getelementptr inbounds %struct.buf, %struct.buf* %30, i32 0, i32 2
  %31 = load i32, i32* %pos12, align 4
  %add13 = add nsw i32 %31, %28
  store i32 %add13, i32* %pos12, align 4
  %32 = load i32, i32* %nlen, align 4
  %33 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsize = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %33, i32 0, i32 11
  %34 = load i32, i32* %bsize, align 4
  %sub14 = sub nsw i32 %34, %32
  store i32 %sub14, i32* %bsize, align 4
  %35 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail15 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %35, i32 0, i32 1
  %36 = load %struct.buf*, %struct.buf** %tail15, align 4
  %pos16 = getelementptr inbounds %struct.buf, %struct.buf* %36, i32 0, i32 2
  %37 = load i32, i32* %pos16, align 4
  %38 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %tail17 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %38, i32 0, i32 1
  %39 = load %struct.buf*, %struct.buf** %tail17, align 4
  %size18 = getelementptr inbounds %struct.buf, %struct.buf* %39, i32 0, i32 1
  %40 = load i32, i32* %size18, align 4
  %cmp19 = icmp eq i32 %37, %40
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end
  %41 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  call void @remove_buf(%struct.mpstr_tag* %41)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %if.end
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @read_head(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %head = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call = call i32 @read_buf_byte(%struct.mpstr_tag* %0)
  store i32 %call, i32* %head, align 4
  %1 = load i32, i32* %head, align 4
  %shl = shl i32 %1, 8
  store i32 %shl, i32* %head, align 4
  %2 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call1 = call i32 @read_buf_byte(%struct.mpstr_tag* %2)
  %3 = load i32, i32* %head, align 4
  %or = or i32 %3, %call1
  store i32 %or, i32* %head, align 4
  %4 = load i32, i32* %head, align 4
  %shl2 = shl i32 %4, 8
  store i32 %shl2, i32* %head, align 4
  %5 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call3 = call i32 @read_buf_byte(%struct.mpstr_tag* %5)
  %6 = load i32, i32* %head, align 4
  %or4 = or i32 %6, %call3
  store i32 %or4, i32* %head, align 4
  %7 = load i32, i32* %head, align 4
  %shl5 = shl i32 %7, 8
  store i32 %shl5, i32* %head, align 4
  %8 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call6 = call i32 @read_buf_byte(%struct.mpstr_tag* %8)
  %9 = load i32, i32* %head, align 4
  %or7 = or i32 %9, %call6
  store i32 %or7, i32* %head, align 4
  %10 = load i32, i32* %head, align 4
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %header = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %11, i32 0, i32 22
  store i32 %10, i32* %header, align 4
  ret void
}

declare i32 @decode_header(%struct.mpstr_tag*, %struct.frame*, i32) #1

declare i32 @getbits(%struct.mpstr_tag*, i32) #1

declare i32 @decode_layer3_sideinfo(%struct.mpstr_tag*) #1

declare i32 @decode_layer1_frame(%struct.mpstr_tag*, i8*, i32*) #1

declare i32 @decode_layer2_frame(%struct.mpstr_tag*, i8*, i32*) #1

declare i32 @decode_layer3_frame(%struct.mpstr_tag*, i8*, i32*, i32 (%struct.mpstr_tag*, float*, i8*, i32*)*, i32 (%struct.mpstr_tag*, float*, i32, i8*, i32*)*) #1

declare i8* @malloc(i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare i32 @head_check(i32, i32) #1

declare i32 @GetVbrTag(%struct.VBRTAGDATA*, i8*) #1

; Function Attrs: noreturn
declare void @exit(i32) #4

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
