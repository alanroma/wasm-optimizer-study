; ModuleID = 'version.c'
source_filename = "version.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_version_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i8* }

@get_lame_version.str = internal constant i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), align 4
@.str = private unnamed_addr constant [6 x i8] c"3.100\00", align 1
@get_lame_short_version.str = internal constant i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), align 4
@get_lame_very_short_version.str = internal constant i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0), align 4
@.str.1 = private unnamed_addr constant [11 x i8] c"LAME3.100 \00", align 1
@get_lame_tag_encoder_short_version.str = internal constant i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0), align 4
@get_psy_version.str = internal constant i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), align 4
@.str.2 = private unnamed_addr constant [4 x i8] c"1.0\00", align 1
@get_lame_url.str = internal constant i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0), align 4
@.str.3 = private unnamed_addr constant [19 x i8] c"http://lame.sf.net\00", align 1
@get_lame_version_numerical.features = internal constant i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.4, i32 0, i32 0), align 4
@.str.4 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@get_lame_os_bitness.strXX = internal constant i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.4, i32 0, i32 0), align 4
@get_lame_os_bitness.str32 = internal constant i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0), align 4
@.str.5 = private unnamed_addr constant [7 x i8] c"32bits\00", align 1
@get_lame_os_bitness.str64 = internal constant i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.6, i32 0, i32 0), align 4
@.str.6 = private unnamed_addr constant [7 x i8] c"64bits\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_version() #0 {
entry:
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_short_version() #0 {
entry:
  ret i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_very_short_version() #0 {
entry:
  ret i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_tag_encoder_short_version() #0 {
entry:
  ret i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_psy_version() #0 {
entry:
  ret i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_url() #0 {
entry:
  ret i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define hidden void @get_lame_version_numerical(%struct.lame_version_t* %lvp) #0 {
entry:
  %lvp.addr = alloca %struct.lame_version_t*, align 4
  store %struct.lame_version_t* %lvp, %struct.lame_version_t** %lvp.addr, align 4
  %0 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %major = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %0, i32 0, i32 0
  store i32 3, i32* %major, align 4
  %1 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %minor = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %1, i32 0, i32 1
  store i32 100, i32* %minor, align 4
  %2 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %alpha = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %2, i32 0, i32 2
  store i32 0, i32* %alpha, align 4
  %3 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %beta = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %3, i32 0, i32 3
  store i32 0, i32* %beta, align 4
  %4 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %psy_major = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %4, i32 0, i32 4
  store i32 1, i32* %psy_major, align 4
  %5 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %psy_minor = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %5, i32 0, i32 5
  store i32 0, i32* %psy_minor, align 4
  %6 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %psy_alpha = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %6, i32 0, i32 6
  store i32 0, i32* %psy_alpha, align 4
  %7 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %psy_beta = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %7, i32 0, i32 7
  store i32 0, i32* %psy_beta, align 4
  %8 = load %struct.lame_version_t*, %struct.lame_version_t** %lvp.addr, align 4
  %features = getelementptr inbounds %struct.lame_version_t, %struct.lame_version_t* %8, i32 0, i32 8
  store i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.4, i32 0, i32 0), i8** %features, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @get_lame_os_bitness() #0 {
entry:
  ret i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0)
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
