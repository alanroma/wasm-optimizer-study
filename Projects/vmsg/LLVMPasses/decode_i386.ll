; ModuleID = 'decode_i386.c'
source_filename = "decode_i386.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mpstr_tag = type { %struct.buf*, %struct.buf*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.frame, %struct.III_sideinfo, [2 x [3904 x i8]], [2 x [2 x [576 x float]]], [2 x i32], i32, i32, [2 x [2 x [272 x float]]], i32, i32, i32, i8*, %struct.plotting_data*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.buf = type { i8*, i32, i32, %struct.buf*, %struct.buf* }
%struct.frame = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.al_table2*, i32, i32 }
%struct.al_table2 = type opaque
%struct.III_sideinfo = type { i32, i32, [2 x %struct.anon] }
%struct.anon = type { [2 x %struct.gr_info_s] }
%struct.gr_info_s = type { i32, i32, i32, i32, i32, i32, [3 x i32], [3 x i32], [3 x i32], i32, i32, i32, i32, i32, i32, i32, [3 x float*], float* }
%struct.plotting_data = type opaque

@synth_1to1.step = internal constant i32 2, align 4
@decwin = external global [544 x float], align 16
@synth_1to1_unclipped.step = internal constant i32 2, align 4

; Function Attrs: noinline nounwind optnone
define hidden i32 @synth_1to1_mono(%struct.mpstr_tag* %mp, float* %bandPtr, i8* %out, i32* %pnt) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %bandPtr.addr = alloca float*, align 4
  %out.addr = alloca i8*, align 4
  %pnt.addr = alloca i32*, align 4
  %samples_tmp = alloca [64 x i16], align 16
  %tmp1 = alloca i16*, align 4
  %i = alloca i32, align 4
  %ret = alloca i32, align 4
  %pnt1 = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store float* %bandPtr, float** %bandPtr.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32* %pnt, i32** %pnt.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i16], [64 x i16]* %samples_tmp, i32 0, i32 0
  store i16* %arraydecay, i16** %tmp1, align 4
  store i32 0, i32* %pnt1, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %1 = load float*, float** %bandPtr.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i16], [64 x i16]* %samples_tmp, i32 0, i32 0
  %2 = bitcast i16* %arraydecay1 to i8*
  %call = call i32 @synth_1to1(%struct.mpstr_tag* %0, float* %1, i32 0, i8* %2, i32* %pnt1)
  store i32 %call, i32* %ret, align 4
  %3 = load i32*, i32** %pnt.addr, align 4
  %4 = load i32, i32* %3, align 4
  %5 = load i8*, i8** %out.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %4
  store i8* %add.ptr, i8** %out.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i16*, i16** %tmp1, align 4
  %8 = load i16, i16* %7, align 2
  %9 = load i8*, i8** %out.addr, align 4
  %10 = bitcast i8* %9 to i16*
  store i16 %8, i16* %10, align 2
  %11 = load i8*, i8** %out.addr, align 4
  %add.ptr2 = getelementptr inbounds i8, i8* %11, i32 2
  store i8* %add.ptr2, i8** %out.addr, align 4
  %12 = load i16*, i16** %tmp1, align 4
  %add.ptr3 = getelementptr inbounds i16, i16* %12, i32 2
  store i16* %add.ptr3, i16** %tmp1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load i32*, i32** %pnt.addr, align 4
  %15 = load i32, i32* %14, align 4
  %add = add i32 %15, 64
  store i32 %add, i32* %14, align 4
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @synth_1to1(%struct.mpstr_tag* %mp, float* %bandPtr, i32 %channel, i8* %out, i32* %pnt) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %bandPtr.addr = alloca float*, align 4
  %channel.addr = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %pnt.addr = alloca i32*, align 4
  %bo = alloca i32, align 4
  %samples = alloca i16*, align 4
  %b0 = alloca float*, align 4
  %buf = alloca [272 x float]*, align 4
  %clip = alloca i32, align 4
  %bo1 = alloca i32, align 4
  %j = alloca i32, align 4
  %window = alloca float*, align 4
  %sum = alloca float, align 4
  %sum114 = alloca float, align 4
  %sum178 = alloca float, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store float* %bandPtr, float** %bandPtr.addr, align 4
  store i32 %channel, i32* %channel.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32* %pnt, i32** %pnt.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i32*, i32** %pnt.addr, align 4
  %2 = load i32, i32* %1, align 4
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %2
  %3 = bitcast i8* %add.ptr to i16*
  store i16* %3, i16** %samples, align 4
  store i32 0, i32* %clip, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_bo = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 25
  %5 = load i32, i32* %synth_bo, align 4
  store i32 %5, i32* %bo, align 4
  %6 = load i32, i32* %channel.addr, align 4
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %bo, align 4
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bo, align 4
  %8 = load i32, i32* %bo, align 4
  %and = and i32 %8, 15
  store i32 %and, i32* %bo, align 4
  %9 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_buffs = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %9, i32 0, i32 24
  %arrayidx = getelementptr inbounds [2 x [2 x [272 x float]]], [2 x [2 x [272 x float]]]* %synth_buffs, i32 0, i32 0
  %arraydecay = getelementptr inbounds [2 x [272 x float]], [2 x [272 x float]]* %arrayidx, i32 0, i32 0
  store [272 x float]* %arraydecay, [272 x float]** %buf, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load i16*, i16** %samples, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %10, i32 1
  store i16* %incdec.ptr, i16** %samples, align 4
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_buffs1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %11, i32 0, i32 24
  %arrayidx2 = getelementptr inbounds [2 x [2 x [272 x float]]], [2 x [2 x [272 x float]]]* %synth_buffs1, i32 0, i32 1
  %arraydecay3 = getelementptr inbounds [2 x [272 x float]], [2 x [272 x float]]* %arrayidx2, i32 0, i32 0
  store [272 x float]* %arraydecay3, [272 x float]** %buf, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load i32, i32* %bo, align 4
  %and4 = and i32 %12, 1
  %tobool5 = icmp ne i32 %and4, 0
  br i1 %tobool5, label %if.then6, label %if.else16

if.then6:                                         ; preds = %if.end
  %13 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx7 = getelementptr inbounds [272 x float], [272 x float]* %13, i32 0
  %arraydecay8 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx7, i32 0, i32 0
  store float* %arraydecay8, float** %b0, align 4
  %14 = load i32, i32* %bo, align 4
  store i32 %14, i32* %bo1, align 4
  %15 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx9 = getelementptr inbounds [272 x float], [272 x float]* %15, i32 1
  %arraydecay10 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx9, i32 0, i32 0
  %16 = load i32, i32* %bo, align 4
  %add = add nsw i32 %16, 1
  %and11 = and i32 %add, 15
  %add.ptr12 = getelementptr inbounds float, float* %arraydecay10, i32 %and11
  %17 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx13 = getelementptr inbounds [272 x float], [272 x float]* %17, i32 0
  %arraydecay14 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx13, i32 0, i32 0
  %18 = load i32, i32* %bo, align 4
  %add.ptr15 = getelementptr inbounds float, float* %arraydecay14, i32 %18
  %19 = load float*, float** %bandPtr.addr, align 4
  call void @dct64(float* %add.ptr12, float* %add.ptr15, float* %19)
  br label %if.end27

if.else16:                                        ; preds = %if.end
  %20 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx17 = getelementptr inbounds [272 x float], [272 x float]* %20, i32 1
  %arraydecay18 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx17, i32 0, i32 0
  store float* %arraydecay18, float** %b0, align 4
  %21 = load i32, i32* %bo, align 4
  %add19 = add nsw i32 %21, 1
  store i32 %add19, i32* %bo1, align 4
  %22 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx20 = getelementptr inbounds [272 x float], [272 x float]* %22, i32 0
  %arraydecay21 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx20, i32 0, i32 0
  %23 = load i32, i32* %bo, align 4
  %add.ptr22 = getelementptr inbounds float, float* %arraydecay21, i32 %23
  %24 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx23 = getelementptr inbounds [272 x float], [272 x float]* %24, i32 1
  %arraydecay24 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx23, i32 0, i32 0
  %25 = load i32, i32* %bo, align 4
  %add.ptr25 = getelementptr inbounds float, float* %arraydecay24, i32 %25
  %add.ptr26 = getelementptr inbounds float, float* %add.ptr25, i32 1
  %26 = load float*, float** %bandPtr.addr, align 4
  call void @dct64(float* %add.ptr22, float* %add.ptr26, float* %26)
  br label %if.end27

if.end27:                                         ; preds = %if.else16, %if.then6
  %27 = load i32, i32* %bo, align 4
  %28 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_bo28 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %28, i32 0, i32 25
  store i32 %27, i32* %synth_bo28, align 4
  %29 = load i32, i32* %bo1, align 4
  %idx.neg = sub i32 0, %29
  %add.ptr29 = getelementptr inbounds float, float* getelementptr inbounds ([544 x float], [544 x float]* @decwin, i32 0, i32 16), i32 %idx.neg
  store float* %add.ptr29, float** %window, align 4
  store i32 16, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end27
  %30 = load i32, i32* %j, align 4
  %tobool30 = icmp ne i32 %30, 0
  br i1 %tobool30, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load float*, float** %window, align 4
  %arrayidx31 = getelementptr inbounds float, float* %31, i32 0
  %32 = load float, float* %arrayidx31, align 4
  %33 = load float*, float** %b0, align 4
  %arrayidx32 = getelementptr inbounds float, float* %33, i32 0
  %34 = load float, float* %arrayidx32, align 4
  %mul = fmul float %32, %34
  store float %mul, float* %sum, align 4
  %35 = load float*, float** %window, align 4
  %arrayidx33 = getelementptr inbounds float, float* %35, i32 1
  %36 = load float, float* %arrayidx33, align 4
  %37 = load float*, float** %b0, align 4
  %arrayidx34 = getelementptr inbounds float, float* %37, i32 1
  %38 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %36, %38
  %39 = load float, float* %sum, align 4
  %sub = fsub float %39, %mul35
  store float %sub, float* %sum, align 4
  %40 = load float*, float** %window, align 4
  %arrayidx36 = getelementptr inbounds float, float* %40, i32 2
  %41 = load float, float* %arrayidx36, align 4
  %42 = load float*, float** %b0, align 4
  %arrayidx37 = getelementptr inbounds float, float* %42, i32 2
  %43 = load float, float* %arrayidx37, align 4
  %mul38 = fmul float %41, %43
  %44 = load float, float* %sum, align 4
  %add39 = fadd float %44, %mul38
  store float %add39, float* %sum, align 4
  %45 = load float*, float** %window, align 4
  %arrayidx40 = getelementptr inbounds float, float* %45, i32 3
  %46 = load float, float* %arrayidx40, align 4
  %47 = load float*, float** %b0, align 4
  %arrayidx41 = getelementptr inbounds float, float* %47, i32 3
  %48 = load float, float* %arrayidx41, align 4
  %mul42 = fmul float %46, %48
  %49 = load float, float* %sum, align 4
  %sub43 = fsub float %49, %mul42
  store float %sub43, float* %sum, align 4
  %50 = load float*, float** %window, align 4
  %arrayidx44 = getelementptr inbounds float, float* %50, i32 4
  %51 = load float, float* %arrayidx44, align 4
  %52 = load float*, float** %b0, align 4
  %arrayidx45 = getelementptr inbounds float, float* %52, i32 4
  %53 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %51, %53
  %54 = load float, float* %sum, align 4
  %add47 = fadd float %54, %mul46
  store float %add47, float* %sum, align 4
  %55 = load float*, float** %window, align 4
  %arrayidx48 = getelementptr inbounds float, float* %55, i32 5
  %56 = load float, float* %arrayidx48, align 4
  %57 = load float*, float** %b0, align 4
  %arrayidx49 = getelementptr inbounds float, float* %57, i32 5
  %58 = load float, float* %arrayidx49, align 4
  %mul50 = fmul float %56, %58
  %59 = load float, float* %sum, align 4
  %sub51 = fsub float %59, %mul50
  store float %sub51, float* %sum, align 4
  %60 = load float*, float** %window, align 4
  %arrayidx52 = getelementptr inbounds float, float* %60, i32 6
  %61 = load float, float* %arrayidx52, align 4
  %62 = load float*, float** %b0, align 4
  %arrayidx53 = getelementptr inbounds float, float* %62, i32 6
  %63 = load float, float* %arrayidx53, align 4
  %mul54 = fmul float %61, %63
  %64 = load float, float* %sum, align 4
  %add55 = fadd float %64, %mul54
  store float %add55, float* %sum, align 4
  %65 = load float*, float** %window, align 4
  %arrayidx56 = getelementptr inbounds float, float* %65, i32 7
  %66 = load float, float* %arrayidx56, align 4
  %67 = load float*, float** %b0, align 4
  %arrayidx57 = getelementptr inbounds float, float* %67, i32 7
  %68 = load float, float* %arrayidx57, align 4
  %mul58 = fmul float %66, %68
  %69 = load float, float* %sum, align 4
  %sub59 = fsub float %69, %mul58
  store float %sub59, float* %sum, align 4
  %70 = load float*, float** %window, align 4
  %arrayidx60 = getelementptr inbounds float, float* %70, i32 8
  %71 = load float, float* %arrayidx60, align 4
  %72 = load float*, float** %b0, align 4
  %arrayidx61 = getelementptr inbounds float, float* %72, i32 8
  %73 = load float, float* %arrayidx61, align 4
  %mul62 = fmul float %71, %73
  %74 = load float, float* %sum, align 4
  %add63 = fadd float %74, %mul62
  store float %add63, float* %sum, align 4
  %75 = load float*, float** %window, align 4
  %arrayidx64 = getelementptr inbounds float, float* %75, i32 9
  %76 = load float, float* %arrayidx64, align 4
  %77 = load float*, float** %b0, align 4
  %arrayidx65 = getelementptr inbounds float, float* %77, i32 9
  %78 = load float, float* %arrayidx65, align 4
  %mul66 = fmul float %76, %78
  %79 = load float, float* %sum, align 4
  %sub67 = fsub float %79, %mul66
  store float %sub67, float* %sum, align 4
  %80 = load float*, float** %window, align 4
  %arrayidx68 = getelementptr inbounds float, float* %80, i32 10
  %81 = load float, float* %arrayidx68, align 4
  %82 = load float*, float** %b0, align 4
  %arrayidx69 = getelementptr inbounds float, float* %82, i32 10
  %83 = load float, float* %arrayidx69, align 4
  %mul70 = fmul float %81, %83
  %84 = load float, float* %sum, align 4
  %add71 = fadd float %84, %mul70
  store float %add71, float* %sum, align 4
  %85 = load float*, float** %window, align 4
  %arrayidx72 = getelementptr inbounds float, float* %85, i32 11
  %86 = load float, float* %arrayidx72, align 4
  %87 = load float*, float** %b0, align 4
  %arrayidx73 = getelementptr inbounds float, float* %87, i32 11
  %88 = load float, float* %arrayidx73, align 4
  %mul74 = fmul float %86, %88
  %89 = load float, float* %sum, align 4
  %sub75 = fsub float %89, %mul74
  store float %sub75, float* %sum, align 4
  %90 = load float*, float** %window, align 4
  %arrayidx76 = getelementptr inbounds float, float* %90, i32 12
  %91 = load float, float* %arrayidx76, align 4
  %92 = load float*, float** %b0, align 4
  %arrayidx77 = getelementptr inbounds float, float* %92, i32 12
  %93 = load float, float* %arrayidx77, align 4
  %mul78 = fmul float %91, %93
  %94 = load float, float* %sum, align 4
  %add79 = fadd float %94, %mul78
  store float %add79, float* %sum, align 4
  %95 = load float*, float** %window, align 4
  %arrayidx80 = getelementptr inbounds float, float* %95, i32 13
  %96 = load float, float* %arrayidx80, align 4
  %97 = load float*, float** %b0, align 4
  %arrayidx81 = getelementptr inbounds float, float* %97, i32 13
  %98 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %96, %98
  %99 = load float, float* %sum, align 4
  %sub83 = fsub float %99, %mul82
  store float %sub83, float* %sum, align 4
  %100 = load float*, float** %window, align 4
  %arrayidx84 = getelementptr inbounds float, float* %100, i32 14
  %101 = load float, float* %arrayidx84, align 4
  %102 = load float*, float** %b0, align 4
  %arrayidx85 = getelementptr inbounds float, float* %102, i32 14
  %103 = load float, float* %arrayidx85, align 4
  %mul86 = fmul float %101, %103
  %104 = load float, float* %sum, align 4
  %add87 = fadd float %104, %mul86
  store float %add87, float* %sum, align 4
  %105 = load float*, float** %window, align 4
  %arrayidx88 = getelementptr inbounds float, float* %105, i32 15
  %106 = load float, float* %arrayidx88, align 4
  %107 = load float*, float** %b0, align 4
  %arrayidx89 = getelementptr inbounds float, float* %107, i32 15
  %108 = load float, float* %arrayidx89, align 4
  %mul90 = fmul float %106, %108
  %109 = load float, float* %sum, align 4
  %sub91 = fsub float %109, %mul90
  store float %sub91, float* %sum, align 4
  %110 = load float, float* %sum, align 4
  %conv = fpext float %110 to double
  %cmp = fcmp ogt double %conv, 3.276700e+04
  br i1 %cmp, label %if.then93, label %if.else94

if.then93:                                        ; preds = %for.body
  %111 = load i16*, i16** %samples, align 4
  store i16 32767, i16* %111, align 2
  %112 = load i32, i32* %clip, align 4
  %inc = add nsw i32 %112, 1
  store i32 %inc, i32* %clip, align 4
  br label %if.end109

if.else94:                                        ; preds = %for.body
  %113 = load float, float* %sum, align 4
  %conv95 = fpext float %113 to double
  %cmp96 = fcmp olt double %conv95, -3.276800e+04
  br i1 %cmp96, label %if.then98, label %if.else100

if.then98:                                        ; preds = %if.else94
  %114 = load i16*, i16** %samples, align 4
  store i16 -32768, i16* %114, align 2
  %115 = load i32, i32* %clip, align 4
  %inc99 = add nsw i32 %115, 1
  store i32 %inc99, i32* %clip, align 4
  br label %if.end108

if.else100:                                       ; preds = %if.else94
  %116 = load float, float* %sum, align 4
  %cmp101 = fcmp ogt float %116, 0.000000e+00
  br i1 %cmp101, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else100
  %117 = load float, float* %sum, align 4
  %conv103 = fpext float %117 to double
  %add104 = fadd double %conv103, 5.000000e-01
  br label %cond.end

cond.false:                                       ; preds = %if.else100
  %118 = load float, float* %sum, align 4
  %conv105 = fpext float %118 to double
  %sub106 = fsub double %conv105, 5.000000e-01
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %add104, %cond.true ], [ %sub106, %cond.false ]
  %conv107 = fptosi double %cond to i16
  %119 = load i16*, i16** %samples, align 4
  store i16 %conv107, i16* %119, align 2
  br label %if.end108

if.end108:                                        ; preds = %cond.end, %if.then98
  br label %if.end109

if.end109:                                        ; preds = %if.end108, %if.then93
  br label %for.inc

for.inc:                                          ; preds = %if.end109
  %120 = load i32, i32* %j, align 4
  %dec110 = add nsw i32 %120, -1
  store i32 %dec110, i32* %j, align 4
  %121 = load float*, float** %b0, align 4
  %add.ptr111 = getelementptr inbounds float, float* %121, i32 16
  store float* %add.ptr111, float** %b0, align 4
  %122 = load float*, float** %window, align 4
  %add.ptr112 = getelementptr inbounds float, float* %122, i32 32
  store float* %add.ptr112, float** %window, align 4
  %123 = load i16*, i16** %samples, align 4
  %add.ptr113 = getelementptr inbounds i16, i16* %123, i32 2
  store i16* %add.ptr113, i16** %samples, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %124 = load float*, float** %window, align 4
  %arrayidx115 = getelementptr inbounds float, float* %124, i32 0
  %125 = load float, float* %arrayidx115, align 4
  %126 = load float*, float** %b0, align 4
  %arrayidx116 = getelementptr inbounds float, float* %126, i32 0
  %127 = load float, float* %arrayidx116, align 4
  %mul117 = fmul float %125, %127
  store float %mul117, float* %sum114, align 4
  %128 = load float*, float** %window, align 4
  %arrayidx118 = getelementptr inbounds float, float* %128, i32 2
  %129 = load float, float* %arrayidx118, align 4
  %130 = load float*, float** %b0, align 4
  %arrayidx119 = getelementptr inbounds float, float* %130, i32 2
  %131 = load float, float* %arrayidx119, align 4
  %mul120 = fmul float %129, %131
  %132 = load float, float* %sum114, align 4
  %add121 = fadd float %132, %mul120
  store float %add121, float* %sum114, align 4
  %133 = load float*, float** %window, align 4
  %arrayidx122 = getelementptr inbounds float, float* %133, i32 4
  %134 = load float, float* %arrayidx122, align 4
  %135 = load float*, float** %b0, align 4
  %arrayidx123 = getelementptr inbounds float, float* %135, i32 4
  %136 = load float, float* %arrayidx123, align 4
  %mul124 = fmul float %134, %136
  %137 = load float, float* %sum114, align 4
  %add125 = fadd float %137, %mul124
  store float %add125, float* %sum114, align 4
  %138 = load float*, float** %window, align 4
  %arrayidx126 = getelementptr inbounds float, float* %138, i32 6
  %139 = load float, float* %arrayidx126, align 4
  %140 = load float*, float** %b0, align 4
  %arrayidx127 = getelementptr inbounds float, float* %140, i32 6
  %141 = load float, float* %arrayidx127, align 4
  %mul128 = fmul float %139, %141
  %142 = load float, float* %sum114, align 4
  %add129 = fadd float %142, %mul128
  store float %add129, float* %sum114, align 4
  %143 = load float*, float** %window, align 4
  %arrayidx130 = getelementptr inbounds float, float* %143, i32 8
  %144 = load float, float* %arrayidx130, align 4
  %145 = load float*, float** %b0, align 4
  %arrayidx131 = getelementptr inbounds float, float* %145, i32 8
  %146 = load float, float* %arrayidx131, align 4
  %mul132 = fmul float %144, %146
  %147 = load float, float* %sum114, align 4
  %add133 = fadd float %147, %mul132
  store float %add133, float* %sum114, align 4
  %148 = load float*, float** %window, align 4
  %arrayidx134 = getelementptr inbounds float, float* %148, i32 10
  %149 = load float, float* %arrayidx134, align 4
  %150 = load float*, float** %b0, align 4
  %arrayidx135 = getelementptr inbounds float, float* %150, i32 10
  %151 = load float, float* %arrayidx135, align 4
  %mul136 = fmul float %149, %151
  %152 = load float, float* %sum114, align 4
  %add137 = fadd float %152, %mul136
  store float %add137, float* %sum114, align 4
  %153 = load float*, float** %window, align 4
  %arrayidx138 = getelementptr inbounds float, float* %153, i32 12
  %154 = load float, float* %arrayidx138, align 4
  %155 = load float*, float** %b0, align 4
  %arrayidx139 = getelementptr inbounds float, float* %155, i32 12
  %156 = load float, float* %arrayidx139, align 4
  %mul140 = fmul float %154, %156
  %157 = load float, float* %sum114, align 4
  %add141 = fadd float %157, %mul140
  store float %add141, float* %sum114, align 4
  %158 = load float*, float** %window, align 4
  %arrayidx142 = getelementptr inbounds float, float* %158, i32 14
  %159 = load float, float* %arrayidx142, align 4
  %160 = load float*, float** %b0, align 4
  %arrayidx143 = getelementptr inbounds float, float* %160, i32 14
  %161 = load float, float* %arrayidx143, align 4
  %mul144 = fmul float %159, %161
  %162 = load float, float* %sum114, align 4
  %add145 = fadd float %162, %mul144
  store float %add145, float* %sum114, align 4
  %163 = load float, float* %sum114, align 4
  %conv146 = fpext float %163 to double
  %cmp147 = fcmp ogt double %conv146, 3.276700e+04
  br i1 %cmp147, label %if.then149, label %if.else151

if.then149:                                       ; preds = %for.end
  %164 = load i16*, i16** %samples, align 4
  store i16 32767, i16* %164, align 2
  %165 = load i32, i32* %clip, align 4
  %inc150 = add nsw i32 %165, 1
  store i32 %inc150, i32* %clip, align 4
  br label %if.end170

if.else151:                                       ; preds = %for.end
  %166 = load float, float* %sum114, align 4
  %conv152 = fpext float %166 to double
  %cmp153 = fcmp olt double %conv152, -3.276800e+04
  br i1 %cmp153, label %if.then155, label %if.else157

if.then155:                                       ; preds = %if.else151
  %167 = load i16*, i16** %samples, align 4
  store i16 -32768, i16* %167, align 2
  %168 = load i32, i32* %clip, align 4
  %inc156 = add nsw i32 %168, 1
  store i32 %inc156, i32* %clip, align 4
  br label %if.end169

if.else157:                                       ; preds = %if.else151
  %169 = load float, float* %sum114, align 4
  %cmp158 = fcmp ogt float %169, 0.000000e+00
  br i1 %cmp158, label %cond.true160, label %cond.false163

cond.true160:                                     ; preds = %if.else157
  %170 = load float, float* %sum114, align 4
  %conv161 = fpext float %170 to double
  %add162 = fadd double %conv161, 5.000000e-01
  br label %cond.end166

cond.false163:                                    ; preds = %if.else157
  %171 = load float, float* %sum114, align 4
  %conv164 = fpext float %171 to double
  %sub165 = fsub double %conv164, 5.000000e-01
  br label %cond.end166

cond.end166:                                      ; preds = %cond.false163, %cond.true160
  %cond167 = phi double [ %add162, %cond.true160 ], [ %sub165, %cond.false163 ]
  %conv168 = fptosi double %cond167 to i16
  %172 = load i16*, i16** %samples, align 4
  store i16 %conv168, i16* %172, align 2
  br label %if.end169

if.end169:                                        ; preds = %cond.end166, %if.then155
  br label %if.end170

if.end170:                                        ; preds = %if.end169, %if.then149
  %173 = load float*, float** %b0, align 4
  %add.ptr171 = getelementptr inbounds float, float* %173, i32 -16
  store float* %add.ptr171, float** %b0, align 4
  %174 = load float*, float** %window, align 4
  %add.ptr172 = getelementptr inbounds float, float* %174, i32 -32
  store float* %add.ptr172, float** %window, align 4
  %175 = load i16*, i16** %samples, align 4
  %add.ptr173 = getelementptr inbounds i16, i16* %175, i32 2
  store i16* %add.ptr173, i16** %samples, align 4
  %176 = load i32, i32* %bo1, align 4
  %shl = shl i32 %176, 1
  %177 = load float*, float** %window, align 4
  %add.ptr174 = getelementptr inbounds float, float* %177, i32 %shl
  store float* %add.ptr174, float** %window, align 4
  store i32 15, i32* %j, align 4
  br label %for.cond175

for.cond175:                                      ; preds = %for.inc267, %if.end170
  %178 = load i32, i32* %j, align 4
  %tobool176 = icmp ne i32 %178, 0
  br i1 %tobool176, label %for.body177, label %for.end272

for.body177:                                      ; preds = %for.cond175
  %179 = load float*, float** %window, align 4
  %arrayidx179 = getelementptr inbounds float, float* %179, i32 -1
  %180 = load float, float* %arrayidx179, align 4
  %fneg = fneg float %180
  %181 = load float*, float** %b0, align 4
  %arrayidx180 = getelementptr inbounds float, float* %181, i32 0
  %182 = load float, float* %arrayidx180, align 4
  %mul181 = fmul float %fneg, %182
  store float %mul181, float* %sum178, align 4
  %183 = load float*, float** %window, align 4
  %arrayidx182 = getelementptr inbounds float, float* %183, i32 -2
  %184 = load float, float* %arrayidx182, align 4
  %185 = load float*, float** %b0, align 4
  %arrayidx183 = getelementptr inbounds float, float* %185, i32 1
  %186 = load float, float* %arrayidx183, align 4
  %mul184 = fmul float %184, %186
  %187 = load float, float* %sum178, align 4
  %sub185 = fsub float %187, %mul184
  store float %sub185, float* %sum178, align 4
  %188 = load float*, float** %window, align 4
  %arrayidx186 = getelementptr inbounds float, float* %188, i32 -3
  %189 = load float, float* %arrayidx186, align 4
  %190 = load float*, float** %b0, align 4
  %arrayidx187 = getelementptr inbounds float, float* %190, i32 2
  %191 = load float, float* %arrayidx187, align 4
  %mul188 = fmul float %189, %191
  %192 = load float, float* %sum178, align 4
  %sub189 = fsub float %192, %mul188
  store float %sub189, float* %sum178, align 4
  %193 = load float*, float** %window, align 4
  %arrayidx190 = getelementptr inbounds float, float* %193, i32 -4
  %194 = load float, float* %arrayidx190, align 4
  %195 = load float*, float** %b0, align 4
  %arrayidx191 = getelementptr inbounds float, float* %195, i32 3
  %196 = load float, float* %arrayidx191, align 4
  %mul192 = fmul float %194, %196
  %197 = load float, float* %sum178, align 4
  %sub193 = fsub float %197, %mul192
  store float %sub193, float* %sum178, align 4
  %198 = load float*, float** %window, align 4
  %arrayidx194 = getelementptr inbounds float, float* %198, i32 -5
  %199 = load float, float* %arrayidx194, align 4
  %200 = load float*, float** %b0, align 4
  %arrayidx195 = getelementptr inbounds float, float* %200, i32 4
  %201 = load float, float* %arrayidx195, align 4
  %mul196 = fmul float %199, %201
  %202 = load float, float* %sum178, align 4
  %sub197 = fsub float %202, %mul196
  store float %sub197, float* %sum178, align 4
  %203 = load float*, float** %window, align 4
  %arrayidx198 = getelementptr inbounds float, float* %203, i32 -6
  %204 = load float, float* %arrayidx198, align 4
  %205 = load float*, float** %b0, align 4
  %arrayidx199 = getelementptr inbounds float, float* %205, i32 5
  %206 = load float, float* %arrayidx199, align 4
  %mul200 = fmul float %204, %206
  %207 = load float, float* %sum178, align 4
  %sub201 = fsub float %207, %mul200
  store float %sub201, float* %sum178, align 4
  %208 = load float*, float** %window, align 4
  %arrayidx202 = getelementptr inbounds float, float* %208, i32 -7
  %209 = load float, float* %arrayidx202, align 4
  %210 = load float*, float** %b0, align 4
  %arrayidx203 = getelementptr inbounds float, float* %210, i32 6
  %211 = load float, float* %arrayidx203, align 4
  %mul204 = fmul float %209, %211
  %212 = load float, float* %sum178, align 4
  %sub205 = fsub float %212, %mul204
  store float %sub205, float* %sum178, align 4
  %213 = load float*, float** %window, align 4
  %arrayidx206 = getelementptr inbounds float, float* %213, i32 -8
  %214 = load float, float* %arrayidx206, align 4
  %215 = load float*, float** %b0, align 4
  %arrayidx207 = getelementptr inbounds float, float* %215, i32 7
  %216 = load float, float* %arrayidx207, align 4
  %mul208 = fmul float %214, %216
  %217 = load float, float* %sum178, align 4
  %sub209 = fsub float %217, %mul208
  store float %sub209, float* %sum178, align 4
  %218 = load float*, float** %window, align 4
  %arrayidx210 = getelementptr inbounds float, float* %218, i32 -9
  %219 = load float, float* %arrayidx210, align 4
  %220 = load float*, float** %b0, align 4
  %arrayidx211 = getelementptr inbounds float, float* %220, i32 8
  %221 = load float, float* %arrayidx211, align 4
  %mul212 = fmul float %219, %221
  %222 = load float, float* %sum178, align 4
  %sub213 = fsub float %222, %mul212
  store float %sub213, float* %sum178, align 4
  %223 = load float*, float** %window, align 4
  %arrayidx214 = getelementptr inbounds float, float* %223, i32 -10
  %224 = load float, float* %arrayidx214, align 4
  %225 = load float*, float** %b0, align 4
  %arrayidx215 = getelementptr inbounds float, float* %225, i32 9
  %226 = load float, float* %arrayidx215, align 4
  %mul216 = fmul float %224, %226
  %227 = load float, float* %sum178, align 4
  %sub217 = fsub float %227, %mul216
  store float %sub217, float* %sum178, align 4
  %228 = load float*, float** %window, align 4
  %arrayidx218 = getelementptr inbounds float, float* %228, i32 -11
  %229 = load float, float* %arrayidx218, align 4
  %230 = load float*, float** %b0, align 4
  %arrayidx219 = getelementptr inbounds float, float* %230, i32 10
  %231 = load float, float* %arrayidx219, align 4
  %mul220 = fmul float %229, %231
  %232 = load float, float* %sum178, align 4
  %sub221 = fsub float %232, %mul220
  store float %sub221, float* %sum178, align 4
  %233 = load float*, float** %window, align 4
  %arrayidx222 = getelementptr inbounds float, float* %233, i32 -12
  %234 = load float, float* %arrayidx222, align 4
  %235 = load float*, float** %b0, align 4
  %arrayidx223 = getelementptr inbounds float, float* %235, i32 11
  %236 = load float, float* %arrayidx223, align 4
  %mul224 = fmul float %234, %236
  %237 = load float, float* %sum178, align 4
  %sub225 = fsub float %237, %mul224
  store float %sub225, float* %sum178, align 4
  %238 = load float*, float** %window, align 4
  %arrayidx226 = getelementptr inbounds float, float* %238, i32 -13
  %239 = load float, float* %arrayidx226, align 4
  %240 = load float*, float** %b0, align 4
  %arrayidx227 = getelementptr inbounds float, float* %240, i32 12
  %241 = load float, float* %arrayidx227, align 4
  %mul228 = fmul float %239, %241
  %242 = load float, float* %sum178, align 4
  %sub229 = fsub float %242, %mul228
  store float %sub229, float* %sum178, align 4
  %243 = load float*, float** %window, align 4
  %arrayidx230 = getelementptr inbounds float, float* %243, i32 -14
  %244 = load float, float* %arrayidx230, align 4
  %245 = load float*, float** %b0, align 4
  %arrayidx231 = getelementptr inbounds float, float* %245, i32 13
  %246 = load float, float* %arrayidx231, align 4
  %mul232 = fmul float %244, %246
  %247 = load float, float* %sum178, align 4
  %sub233 = fsub float %247, %mul232
  store float %sub233, float* %sum178, align 4
  %248 = load float*, float** %window, align 4
  %arrayidx234 = getelementptr inbounds float, float* %248, i32 -15
  %249 = load float, float* %arrayidx234, align 4
  %250 = load float*, float** %b0, align 4
  %arrayidx235 = getelementptr inbounds float, float* %250, i32 14
  %251 = load float, float* %arrayidx235, align 4
  %mul236 = fmul float %249, %251
  %252 = load float, float* %sum178, align 4
  %sub237 = fsub float %252, %mul236
  store float %sub237, float* %sum178, align 4
  %253 = load float*, float** %window, align 4
  %arrayidx238 = getelementptr inbounds float, float* %253, i32 0
  %254 = load float, float* %arrayidx238, align 4
  %255 = load float*, float** %b0, align 4
  %arrayidx239 = getelementptr inbounds float, float* %255, i32 15
  %256 = load float, float* %arrayidx239, align 4
  %mul240 = fmul float %254, %256
  %257 = load float, float* %sum178, align 4
  %sub241 = fsub float %257, %mul240
  store float %sub241, float* %sum178, align 4
  %258 = load float, float* %sum178, align 4
  %conv242 = fpext float %258 to double
  %cmp243 = fcmp ogt double %conv242, 3.276700e+04
  br i1 %cmp243, label %if.then245, label %if.else247

if.then245:                                       ; preds = %for.body177
  %259 = load i16*, i16** %samples, align 4
  store i16 32767, i16* %259, align 2
  %260 = load i32, i32* %clip, align 4
  %inc246 = add nsw i32 %260, 1
  store i32 %inc246, i32* %clip, align 4
  br label %if.end266

if.else247:                                       ; preds = %for.body177
  %261 = load float, float* %sum178, align 4
  %conv248 = fpext float %261 to double
  %cmp249 = fcmp olt double %conv248, -3.276800e+04
  br i1 %cmp249, label %if.then251, label %if.else253

if.then251:                                       ; preds = %if.else247
  %262 = load i16*, i16** %samples, align 4
  store i16 -32768, i16* %262, align 2
  %263 = load i32, i32* %clip, align 4
  %inc252 = add nsw i32 %263, 1
  store i32 %inc252, i32* %clip, align 4
  br label %if.end265

if.else253:                                       ; preds = %if.else247
  %264 = load float, float* %sum178, align 4
  %cmp254 = fcmp ogt float %264, 0.000000e+00
  br i1 %cmp254, label %cond.true256, label %cond.false259

cond.true256:                                     ; preds = %if.else253
  %265 = load float, float* %sum178, align 4
  %conv257 = fpext float %265 to double
  %add258 = fadd double %conv257, 5.000000e-01
  br label %cond.end262

cond.false259:                                    ; preds = %if.else253
  %266 = load float, float* %sum178, align 4
  %conv260 = fpext float %266 to double
  %sub261 = fsub double %conv260, 5.000000e-01
  br label %cond.end262

cond.end262:                                      ; preds = %cond.false259, %cond.true256
  %cond263 = phi double [ %add258, %cond.true256 ], [ %sub261, %cond.false259 ]
  %conv264 = fptosi double %cond263 to i16
  %267 = load i16*, i16** %samples, align 4
  store i16 %conv264, i16* %267, align 2
  br label %if.end265

if.end265:                                        ; preds = %cond.end262, %if.then251
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %if.then245
  br label %for.inc267

for.inc267:                                       ; preds = %if.end266
  %268 = load i32, i32* %j, align 4
  %dec268 = add nsw i32 %268, -1
  store i32 %dec268, i32* %j, align 4
  %269 = load float*, float** %b0, align 4
  %add.ptr269 = getelementptr inbounds float, float* %269, i32 -16
  store float* %add.ptr269, float** %b0, align 4
  %270 = load float*, float** %window, align 4
  %add.ptr270 = getelementptr inbounds float, float* %270, i32 -32
  store float* %add.ptr270, float** %window, align 4
  %271 = load i16*, i16** %samples, align 4
  %add.ptr271 = getelementptr inbounds i16, i16* %271, i32 2
  store i16* %add.ptr271, i16** %samples, align 4
  br label %for.cond175

for.end272:                                       ; preds = %for.cond175
  %272 = load i32*, i32** %pnt.addr, align 4
  %273 = load i32, i32* %272, align 4
  %add273 = add i32 %273, 128
  store i32 %add273, i32* %272, align 4
  %274 = load i32, i32* %clip, align 4
  ret i32 %274
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @synth_1to1_mono_unclipped(%struct.mpstr_tag* %mp, float* %bandPtr, i8* %out, i32* %pnt) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %bandPtr.addr = alloca float*, align 4
  %out.addr = alloca i8*, align 4
  %pnt.addr = alloca i32*, align 4
  %samples_tmp = alloca [64 x float], align 16
  %tmp1 = alloca float*, align 4
  %i = alloca i32, align 4
  %ret = alloca i32, align 4
  %pnt1 = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store float* %bandPtr, float** %bandPtr.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32* %pnt, i32** %pnt.addr, align 4
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %samples_tmp, i32 0, i32 0
  store float* %arraydecay, float** %tmp1, align 4
  store i32 0, i32* %pnt1, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %1 = load float*, float** %bandPtr.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x float], [64 x float]* %samples_tmp, i32 0, i32 0
  %2 = bitcast float* %arraydecay1 to i8*
  %call = call i32 @synth_1to1_unclipped(%struct.mpstr_tag* %0, float* %1, i32 0, i8* %2, i32* %pnt1)
  store i32 %call, i32* %ret, align 4
  %3 = load i32*, i32** %pnt.addr, align 4
  %4 = load i32, i32* %3, align 4
  %5 = load i8*, i8** %out.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %4
  store i8* %add.ptr, i8** %out.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load float*, float** %tmp1, align 4
  %8 = load float, float* %7, align 4
  %9 = load i8*, i8** %out.addr, align 4
  %10 = bitcast i8* %9 to float*
  store float %8, float* %10, align 4
  %11 = load i8*, i8** %out.addr, align 4
  %add.ptr2 = getelementptr inbounds i8, i8* %11, i32 4
  store i8* %add.ptr2, i8** %out.addr, align 4
  %12 = load float*, float** %tmp1, align 4
  %add.ptr3 = getelementptr inbounds float, float* %12, i32 2
  store float* %add.ptr3, float** %tmp1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load i32*, i32** %pnt.addr, align 4
  %15 = load i32, i32* %14, align 4
  %add = add i32 %15, 128
  store i32 %add, i32* %14, align 4
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @synth_1to1_unclipped(%struct.mpstr_tag* %mp, float* %bandPtr, i32 %channel, i8* %out, i32* %pnt) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %bandPtr.addr = alloca float*, align 4
  %channel.addr = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %pnt.addr = alloca i32*, align 4
  %bo = alloca i32, align 4
  %samples = alloca float*, align 4
  %b0 = alloca float*, align 4
  %buf = alloca [272 x float]*, align 4
  %clip = alloca i32, align 4
  %bo1 = alloca i32, align 4
  %j = alloca i32, align 4
  %window = alloca float*, align 4
  %sum = alloca float, align 4
  %sum96 = alloca float, align 4
  %sum135 = alloca float, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store float* %bandPtr, float** %bandPtr.addr, align 4
  store i32 %channel, i32* %channel.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32* %pnt, i32** %pnt.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i32*, i32** %pnt.addr, align 4
  %2 = load i32, i32* %1, align 4
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %2
  %3 = bitcast i8* %add.ptr to float*
  store float* %3, float** %samples, align 4
  store i32 0, i32* %clip, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_bo = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 25
  %5 = load i32, i32* %synth_bo, align 4
  store i32 %5, i32* %bo, align 4
  %6 = load i32, i32* %channel.addr, align 4
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %bo, align 4
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %bo, align 4
  %8 = load i32, i32* %bo, align 4
  %and = and i32 %8, 15
  store i32 %and, i32* %bo, align 4
  %9 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_buffs = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %9, i32 0, i32 24
  %arrayidx = getelementptr inbounds [2 x [2 x [272 x float]]], [2 x [2 x [272 x float]]]* %synth_buffs, i32 0, i32 0
  %arraydecay = getelementptr inbounds [2 x [272 x float]], [2 x [272 x float]]* %arrayidx, i32 0, i32 0
  store [272 x float]* %arraydecay, [272 x float]** %buf, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load float*, float** %samples, align 4
  %incdec.ptr = getelementptr inbounds float, float* %10, i32 1
  store float* %incdec.ptr, float** %samples, align 4
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_buffs1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %11, i32 0, i32 24
  %arrayidx2 = getelementptr inbounds [2 x [2 x [272 x float]]], [2 x [2 x [272 x float]]]* %synth_buffs1, i32 0, i32 1
  %arraydecay3 = getelementptr inbounds [2 x [272 x float]], [2 x [272 x float]]* %arrayidx2, i32 0, i32 0
  store [272 x float]* %arraydecay3, [272 x float]** %buf, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load i32, i32* %bo, align 4
  %and4 = and i32 %12, 1
  %tobool5 = icmp ne i32 %and4, 0
  br i1 %tobool5, label %if.then6, label %if.else16

if.then6:                                         ; preds = %if.end
  %13 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx7 = getelementptr inbounds [272 x float], [272 x float]* %13, i32 0
  %arraydecay8 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx7, i32 0, i32 0
  store float* %arraydecay8, float** %b0, align 4
  %14 = load i32, i32* %bo, align 4
  store i32 %14, i32* %bo1, align 4
  %15 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx9 = getelementptr inbounds [272 x float], [272 x float]* %15, i32 1
  %arraydecay10 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx9, i32 0, i32 0
  %16 = load i32, i32* %bo, align 4
  %add = add nsw i32 %16, 1
  %and11 = and i32 %add, 15
  %add.ptr12 = getelementptr inbounds float, float* %arraydecay10, i32 %and11
  %17 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx13 = getelementptr inbounds [272 x float], [272 x float]* %17, i32 0
  %arraydecay14 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx13, i32 0, i32 0
  %18 = load i32, i32* %bo, align 4
  %add.ptr15 = getelementptr inbounds float, float* %arraydecay14, i32 %18
  %19 = load float*, float** %bandPtr.addr, align 4
  call void @dct64(float* %add.ptr12, float* %add.ptr15, float* %19)
  br label %if.end27

if.else16:                                        ; preds = %if.end
  %20 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx17 = getelementptr inbounds [272 x float], [272 x float]* %20, i32 1
  %arraydecay18 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx17, i32 0, i32 0
  store float* %arraydecay18, float** %b0, align 4
  %21 = load i32, i32* %bo, align 4
  %add19 = add nsw i32 %21, 1
  store i32 %add19, i32* %bo1, align 4
  %22 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx20 = getelementptr inbounds [272 x float], [272 x float]* %22, i32 0
  %arraydecay21 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx20, i32 0, i32 0
  %23 = load i32, i32* %bo, align 4
  %add.ptr22 = getelementptr inbounds float, float* %arraydecay21, i32 %23
  %24 = load [272 x float]*, [272 x float]** %buf, align 4
  %arrayidx23 = getelementptr inbounds [272 x float], [272 x float]* %24, i32 1
  %arraydecay24 = getelementptr inbounds [272 x float], [272 x float]* %arrayidx23, i32 0, i32 0
  %25 = load i32, i32* %bo, align 4
  %add.ptr25 = getelementptr inbounds float, float* %arraydecay24, i32 %25
  %add.ptr26 = getelementptr inbounds float, float* %add.ptr25, i32 1
  %26 = load float*, float** %bandPtr.addr, align 4
  call void @dct64(float* %add.ptr22, float* %add.ptr26, float* %26)
  br label %if.end27

if.end27:                                         ; preds = %if.else16, %if.then6
  %27 = load i32, i32* %bo, align 4
  %28 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %synth_bo28 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %28, i32 0, i32 25
  store i32 %27, i32* %synth_bo28, align 4
  %29 = load i32, i32* %bo1, align 4
  %idx.neg = sub i32 0, %29
  %add.ptr29 = getelementptr inbounds float, float* getelementptr inbounds ([544 x float], [544 x float]* @decwin, i32 0, i32 16), i32 %idx.neg
  store float* %add.ptr29, float** %window, align 4
  store i32 16, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end27
  %30 = load i32, i32* %j, align 4
  %tobool30 = icmp ne i32 %30, 0
  br i1 %tobool30, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load float*, float** %window, align 4
  %arrayidx31 = getelementptr inbounds float, float* %31, i32 0
  %32 = load float, float* %arrayidx31, align 4
  %33 = load float*, float** %b0, align 4
  %arrayidx32 = getelementptr inbounds float, float* %33, i32 0
  %34 = load float, float* %arrayidx32, align 4
  %mul = fmul float %32, %34
  store float %mul, float* %sum, align 4
  %35 = load float*, float** %window, align 4
  %arrayidx33 = getelementptr inbounds float, float* %35, i32 1
  %36 = load float, float* %arrayidx33, align 4
  %37 = load float*, float** %b0, align 4
  %arrayidx34 = getelementptr inbounds float, float* %37, i32 1
  %38 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %36, %38
  %39 = load float, float* %sum, align 4
  %sub = fsub float %39, %mul35
  store float %sub, float* %sum, align 4
  %40 = load float*, float** %window, align 4
  %arrayidx36 = getelementptr inbounds float, float* %40, i32 2
  %41 = load float, float* %arrayidx36, align 4
  %42 = load float*, float** %b0, align 4
  %arrayidx37 = getelementptr inbounds float, float* %42, i32 2
  %43 = load float, float* %arrayidx37, align 4
  %mul38 = fmul float %41, %43
  %44 = load float, float* %sum, align 4
  %add39 = fadd float %44, %mul38
  store float %add39, float* %sum, align 4
  %45 = load float*, float** %window, align 4
  %arrayidx40 = getelementptr inbounds float, float* %45, i32 3
  %46 = load float, float* %arrayidx40, align 4
  %47 = load float*, float** %b0, align 4
  %arrayidx41 = getelementptr inbounds float, float* %47, i32 3
  %48 = load float, float* %arrayidx41, align 4
  %mul42 = fmul float %46, %48
  %49 = load float, float* %sum, align 4
  %sub43 = fsub float %49, %mul42
  store float %sub43, float* %sum, align 4
  %50 = load float*, float** %window, align 4
  %arrayidx44 = getelementptr inbounds float, float* %50, i32 4
  %51 = load float, float* %arrayidx44, align 4
  %52 = load float*, float** %b0, align 4
  %arrayidx45 = getelementptr inbounds float, float* %52, i32 4
  %53 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %51, %53
  %54 = load float, float* %sum, align 4
  %add47 = fadd float %54, %mul46
  store float %add47, float* %sum, align 4
  %55 = load float*, float** %window, align 4
  %arrayidx48 = getelementptr inbounds float, float* %55, i32 5
  %56 = load float, float* %arrayidx48, align 4
  %57 = load float*, float** %b0, align 4
  %arrayidx49 = getelementptr inbounds float, float* %57, i32 5
  %58 = load float, float* %arrayidx49, align 4
  %mul50 = fmul float %56, %58
  %59 = load float, float* %sum, align 4
  %sub51 = fsub float %59, %mul50
  store float %sub51, float* %sum, align 4
  %60 = load float*, float** %window, align 4
  %arrayidx52 = getelementptr inbounds float, float* %60, i32 6
  %61 = load float, float* %arrayidx52, align 4
  %62 = load float*, float** %b0, align 4
  %arrayidx53 = getelementptr inbounds float, float* %62, i32 6
  %63 = load float, float* %arrayidx53, align 4
  %mul54 = fmul float %61, %63
  %64 = load float, float* %sum, align 4
  %add55 = fadd float %64, %mul54
  store float %add55, float* %sum, align 4
  %65 = load float*, float** %window, align 4
  %arrayidx56 = getelementptr inbounds float, float* %65, i32 7
  %66 = load float, float* %arrayidx56, align 4
  %67 = load float*, float** %b0, align 4
  %arrayidx57 = getelementptr inbounds float, float* %67, i32 7
  %68 = load float, float* %arrayidx57, align 4
  %mul58 = fmul float %66, %68
  %69 = load float, float* %sum, align 4
  %sub59 = fsub float %69, %mul58
  store float %sub59, float* %sum, align 4
  %70 = load float*, float** %window, align 4
  %arrayidx60 = getelementptr inbounds float, float* %70, i32 8
  %71 = load float, float* %arrayidx60, align 4
  %72 = load float*, float** %b0, align 4
  %arrayidx61 = getelementptr inbounds float, float* %72, i32 8
  %73 = load float, float* %arrayidx61, align 4
  %mul62 = fmul float %71, %73
  %74 = load float, float* %sum, align 4
  %add63 = fadd float %74, %mul62
  store float %add63, float* %sum, align 4
  %75 = load float*, float** %window, align 4
  %arrayidx64 = getelementptr inbounds float, float* %75, i32 9
  %76 = load float, float* %arrayidx64, align 4
  %77 = load float*, float** %b0, align 4
  %arrayidx65 = getelementptr inbounds float, float* %77, i32 9
  %78 = load float, float* %arrayidx65, align 4
  %mul66 = fmul float %76, %78
  %79 = load float, float* %sum, align 4
  %sub67 = fsub float %79, %mul66
  store float %sub67, float* %sum, align 4
  %80 = load float*, float** %window, align 4
  %arrayidx68 = getelementptr inbounds float, float* %80, i32 10
  %81 = load float, float* %arrayidx68, align 4
  %82 = load float*, float** %b0, align 4
  %arrayidx69 = getelementptr inbounds float, float* %82, i32 10
  %83 = load float, float* %arrayidx69, align 4
  %mul70 = fmul float %81, %83
  %84 = load float, float* %sum, align 4
  %add71 = fadd float %84, %mul70
  store float %add71, float* %sum, align 4
  %85 = load float*, float** %window, align 4
  %arrayidx72 = getelementptr inbounds float, float* %85, i32 11
  %86 = load float, float* %arrayidx72, align 4
  %87 = load float*, float** %b0, align 4
  %arrayidx73 = getelementptr inbounds float, float* %87, i32 11
  %88 = load float, float* %arrayidx73, align 4
  %mul74 = fmul float %86, %88
  %89 = load float, float* %sum, align 4
  %sub75 = fsub float %89, %mul74
  store float %sub75, float* %sum, align 4
  %90 = load float*, float** %window, align 4
  %arrayidx76 = getelementptr inbounds float, float* %90, i32 12
  %91 = load float, float* %arrayidx76, align 4
  %92 = load float*, float** %b0, align 4
  %arrayidx77 = getelementptr inbounds float, float* %92, i32 12
  %93 = load float, float* %arrayidx77, align 4
  %mul78 = fmul float %91, %93
  %94 = load float, float* %sum, align 4
  %add79 = fadd float %94, %mul78
  store float %add79, float* %sum, align 4
  %95 = load float*, float** %window, align 4
  %arrayidx80 = getelementptr inbounds float, float* %95, i32 13
  %96 = load float, float* %arrayidx80, align 4
  %97 = load float*, float** %b0, align 4
  %arrayidx81 = getelementptr inbounds float, float* %97, i32 13
  %98 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %96, %98
  %99 = load float, float* %sum, align 4
  %sub83 = fsub float %99, %mul82
  store float %sub83, float* %sum, align 4
  %100 = load float*, float** %window, align 4
  %arrayidx84 = getelementptr inbounds float, float* %100, i32 14
  %101 = load float, float* %arrayidx84, align 4
  %102 = load float*, float** %b0, align 4
  %arrayidx85 = getelementptr inbounds float, float* %102, i32 14
  %103 = load float, float* %arrayidx85, align 4
  %mul86 = fmul float %101, %103
  %104 = load float, float* %sum, align 4
  %add87 = fadd float %104, %mul86
  store float %add87, float* %sum, align 4
  %105 = load float*, float** %window, align 4
  %arrayidx88 = getelementptr inbounds float, float* %105, i32 15
  %106 = load float, float* %arrayidx88, align 4
  %107 = load float*, float** %b0, align 4
  %arrayidx89 = getelementptr inbounds float, float* %107, i32 15
  %108 = load float, float* %arrayidx89, align 4
  %mul90 = fmul float %106, %108
  %109 = load float, float* %sum, align 4
  %sub91 = fsub float %109, %mul90
  store float %sub91, float* %sum, align 4
  %110 = load float, float* %sum, align 4
  %111 = load float*, float** %samples, align 4
  store float %110, float* %111, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %112 = load i32, i32* %j, align 4
  %dec92 = add nsw i32 %112, -1
  store i32 %dec92, i32* %j, align 4
  %113 = load float*, float** %b0, align 4
  %add.ptr93 = getelementptr inbounds float, float* %113, i32 16
  store float* %add.ptr93, float** %b0, align 4
  %114 = load float*, float** %window, align 4
  %add.ptr94 = getelementptr inbounds float, float* %114, i32 32
  store float* %add.ptr94, float** %window, align 4
  %115 = load float*, float** %samples, align 4
  %add.ptr95 = getelementptr inbounds float, float* %115, i32 2
  store float* %add.ptr95, float** %samples, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %116 = load float*, float** %window, align 4
  %arrayidx97 = getelementptr inbounds float, float* %116, i32 0
  %117 = load float, float* %arrayidx97, align 4
  %118 = load float*, float** %b0, align 4
  %arrayidx98 = getelementptr inbounds float, float* %118, i32 0
  %119 = load float, float* %arrayidx98, align 4
  %mul99 = fmul float %117, %119
  store float %mul99, float* %sum96, align 4
  %120 = load float*, float** %window, align 4
  %arrayidx100 = getelementptr inbounds float, float* %120, i32 2
  %121 = load float, float* %arrayidx100, align 4
  %122 = load float*, float** %b0, align 4
  %arrayidx101 = getelementptr inbounds float, float* %122, i32 2
  %123 = load float, float* %arrayidx101, align 4
  %mul102 = fmul float %121, %123
  %124 = load float, float* %sum96, align 4
  %add103 = fadd float %124, %mul102
  store float %add103, float* %sum96, align 4
  %125 = load float*, float** %window, align 4
  %arrayidx104 = getelementptr inbounds float, float* %125, i32 4
  %126 = load float, float* %arrayidx104, align 4
  %127 = load float*, float** %b0, align 4
  %arrayidx105 = getelementptr inbounds float, float* %127, i32 4
  %128 = load float, float* %arrayidx105, align 4
  %mul106 = fmul float %126, %128
  %129 = load float, float* %sum96, align 4
  %add107 = fadd float %129, %mul106
  store float %add107, float* %sum96, align 4
  %130 = load float*, float** %window, align 4
  %arrayidx108 = getelementptr inbounds float, float* %130, i32 6
  %131 = load float, float* %arrayidx108, align 4
  %132 = load float*, float** %b0, align 4
  %arrayidx109 = getelementptr inbounds float, float* %132, i32 6
  %133 = load float, float* %arrayidx109, align 4
  %mul110 = fmul float %131, %133
  %134 = load float, float* %sum96, align 4
  %add111 = fadd float %134, %mul110
  store float %add111, float* %sum96, align 4
  %135 = load float*, float** %window, align 4
  %arrayidx112 = getelementptr inbounds float, float* %135, i32 8
  %136 = load float, float* %arrayidx112, align 4
  %137 = load float*, float** %b0, align 4
  %arrayidx113 = getelementptr inbounds float, float* %137, i32 8
  %138 = load float, float* %arrayidx113, align 4
  %mul114 = fmul float %136, %138
  %139 = load float, float* %sum96, align 4
  %add115 = fadd float %139, %mul114
  store float %add115, float* %sum96, align 4
  %140 = load float*, float** %window, align 4
  %arrayidx116 = getelementptr inbounds float, float* %140, i32 10
  %141 = load float, float* %arrayidx116, align 4
  %142 = load float*, float** %b0, align 4
  %arrayidx117 = getelementptr inbounds float, float* %142, i32 10
  %143 = load float, float* %arrayidx117, align 4
  %mul118 = fmul float %141, %143
  %144 = load float, float* %sum96, align 4
  %add119 = fadd float %144, %mul118
  store float %add119, float* %sum96, align 4
  %145 = load float*, float** %window, align 4
  %arrayidx120 = getelementptr inbounds float, float* %145, i32 12
  %146 = load float, float* %arrayidx120, align 4
  %147 = load float*, float** %b0, align 4
  %arrayidx121 = getelementptr inbounds float, float* %147, i32 12
  %148 = load float, float* %arrayidx121, align 4
  %mul122 = fmul float %146, %148
  %149 = load float, float* %sum96, align 4
  %add123 = fadd float %149, %mul122
  store float %add123, float* %sum96, align 4
  %150 = load float*, float** %window, align 4
  %arrayidx124 = getelementptr inbounds float, float* %150, i32 14
  %151 = load float, float* %arrayidx124, align 4
  %152 = load float*, float** %b0, align 4
  %arrayidx125 = getelementptr inbounds float, float* %152, i32 14
  %153 = load float, float* %arrayidx125, align 4
  %mul126 = fmul float %151, %153
  %154 = load float, float* %sum96, align 4
  %add127 = fadd float %154, %mul126
  store float %add127, float* %sum96, align 4
  %155 = load float, float* %sum96, align 4
  %156 = load float*, float** %samples, align 4
  store float %155, float* %156, align 4
  %157 = load float*, float** %b0, align 4
  %add.ptr128 = getelementptr inbounds float, float* %157, i32 -16
  store float* %add.ptr128, float** %b0, align 4
  %158 = load float*, float** %window, align 4
  %add.ptr129 = getelementptr inbounds float, float* %158, i32 -32
  store float* %add.ptr129, float** %window, align 4
  %159 = load float*, float** %samples, align 4
  %add.ptr130 = getelementptr inbounds float, float* %159, i32 2
  store float* %add.ptr130, float** %samples, align 4
  %160 = load i32, i32* %bo1, align 4
  %shl = shl i32 %160, 1
  %161 = load float*, float** %window, align 4
  %add.ptr131 = getelementptr inbounds float, float* %161, i32 %shl
  store float* %add.ptr131, float** %window, align 4
  store i32 15, i32* %j, align 4
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc199, %for.end
  %162 = load i32, i32* %j, align 4
  %tobool133 = icmp ne i32 %162, 0
  br i1 %tobool133, label %for.body134, label %for.end204

for.body134:                                      ; preds = %for.cond132
  %163 = load float*, float** %window, align 4
  %arrayidx136 = getelementptr inbounds float, float* %163, i32 -1
  %164 = load float, float* %arrayidx136, align 4
  %fneg = fneg float %164
  %165 = load float*, float** %b0, align 4
  %arrayidx137 = getelementptr inbounds float, float* %165, i32 0
  %166 = load float, float* %arrayidx137, align 4
  %mul138 = fmul float %fneg, %166
  store float %mul138, float* %sum135, align 4
  %167 = load float*, float** %window, align 4
  %arrayidx139 = getelementptr inbounds float, float* %167, i32 -2
  %168 = load float, float* %arrayidx139, align 4
  %169 = load float*, float** %b0, align 4
  %arrayidx140 = getelementptr inbounds float, float* %169, i32 1
  %170 = load float, float* %arrayidx140, align 4
  %mul141 = fmul float %168, %170
  %171 = load float, float* %sum135, align 4
  %sub142 = fsub float %171, %mul141
  store float %sub142, float* %sum135, align 4
  %172 = load float*, float** %window, align 4
  %arrayidx143 = getelementptr inbounds float, float* %172, i32 -3
  %173 = load float, float* %arrayidx143, align 4
  %174 = load float*, float** %b0, align 4
  %arrayidx144 = getelementptr inbounds float, float* %174, i32 2
  %175 = load float, float* %arrayidx144, align 4
  %mul145 = fmul float %173, %175
  %176 = load float, float* %sum135, align 4
  %sub146 = fsub float %176, %mul145
  store float %sub146, float* %sum135, align 4
  %177 = load float*, float** %window, align 4
  %arrayidx147 = getelementptr inbounds float, float* %177, i32 -4
  %178 = load float, float* %arrayidx147, align 4
  %179 = load float*, float** %b0, align 4
  %arrayidx148 = getelementptr inbounds float, float* %179, i32 3
  %180 = load float, float* %arrayidx148, align 4
  %mul149 = fmul float %178, %180
  %181 = load float, float* %sum135, align 4
  %sub150 = fsub float %181, %mul149
  store float %sub150, float* %sum135, align 4
  %182 = load float*, float** %window, align 4
  %arrayidx151 = getelementptr inbounds float, float* %182, i32 -5
  %183 = load float, float* %arrayidx151, align 4
  %184 = load float*, float** %b0, align 4
  %arrayidx152 = getelementptr inbounds float, float* %184, i32 4
  %185 = load float, float* %arrayidx152, align 4
  %mul153 = fmul float %183, %185
  %186 = load float, float* %sum135, align 4
  %sub154 = fsub float %186, %mul153
  store float %sub154, float* %sum135, align 4
  %187 = load float*, float** %window, align 4
  %arrayidx155 = getelementptr inbounds float, float* %187, i32 -6
  %188 = load float, float* %arrayidx155, align 4
  %189 = load float*, float** %b0, align 4
  %arrayidx156 = getelementptr inbounds float, float* %189, i32 5
  %190 = load float, float* %arrayidx156, align 4
  %mul157 = fmul float %188, %190
  %191 = load float, float* %sum135, align 4
  %sub158 = fsub float %191, %mul157
  store float %sub158, float* %sum135, align 4
  %192 = load float*, float** %window, align 4
  %arrayidx159 = getelementptr inbounds float, float* %192, i32 -7
  %193 = load float, float* %arrayidx159, align 4
  %194 = load float*, float** %b0, align 4
  %arrayidx160 = getelementptr inbounds float, float* %194, i32 6
  %195 = load float, float* %arrayidx160, align 4
  %mul161 = fmul float %193, %195
  %196 = load float, float* %sum135, align 4
  %sub162 = fsub float %196, %mul161
  store float %sub162, float* %sum135, align 4
  %197 = load float*, float** %window, align 4
  %arrayidx163 = getelementptr inbounds float, float* %197, i32 -8
  %198 = load float, float* %arrayidx163, align 4
  %199 = load float*, float** %b0, align 4
  %arrayidx164 = getelementptr inbounds float, float* %199, i32 7
  %200 = load float, float* %arrayidx164, align 4
  %mul165 = fmul float %198, %200
  %201 = load float, float* %sum135, align 4
  %sub166 = fsub float %201, %mul165
  store float %sub166, float* %sum135, align 4
  %202 = load float*, float** %window, align 4
  %arrayidx167 = getelementptr inbounds float, float* %202, i32 -9
  %203 = load float, float* %arrayidx167, align 4
  %204 = load float*, float** %b0, align 4
  %arrayidx168 = getelementptr inbounds float, float* %204, i32 8
  %205 = load float, float* %arrayidx168, align 4
  %mul169 = fmul float %203, %205
  %206 = load float, float* %sum135, align 4
  %sub170 = fsub float %206, %mul169
  store float %sub170, float* %sum135, align 4
  %207 = load float*, float** %window, align 4
  %arrayidx171 = getelementptr inbounds float, float* %207, i32 -10
  %208 = load float, float* %arrayidx171, align 4
  %209 = load float*, float** %b0, align 4
  %arrayidx172 = getelementptr inbounds float, float* %209, i32 9
  %210 = load float, float* %arrayidx172, align 4
  %mul173 = fmul float %208, %210
  %211 = load float, float* %sum135, align 4
  %sub174 = fsub float %211, %mul173
  store float %sub174, float* %sum135, align 4
  %212 = load float*, float** %window, align 4
  %arrayidx175 = getelementptr inbounds float, float* %212, i32 -11
  %213 = load float, float* %arrayidx175, align 4
  %214 = load float*, float** %b0, align 4
  %arrayidx176 = getelementptr inbounds float, float* %214, i32 10
  %215 = load float, float* %arrayidx176, align 4
  %mul177 = fmul float %213, %215
  %216 = load float, float* %sum135, align 4
  %sub178 = fsub float %216, %mul177
  store float %sub178, float* %sum135, align 4
  %217 = load float*, float** %window, align 4
  %arrayidx179 = getelementptr inbounds float, float* %217, i32 -12
  %218 = load float, float* %arrayidx179, align 4
  %219 = load float*, float** %b0, align 4
  %arrayidx180 = getelementptr inbounds float, float* %219, i32 11
  %220 = load float, float* %arrayidx180, align 4
  %mul181 = fmul float %218, %220
  %221 = load float, float* %sum135, align 4
  %sub182 = fsub float %221, %mul181
  store float %sub182, float* %sum135, align 4
  %222 = load float*, float** %window, align 4
  %arrayidx183 = getelementptr inbounds float, float* %222, i32 -13
  %223 = load float, float* %arrayidx183, align 4
  %224 = load float*, float** %b0, align 4
  %arrayidx184 = getelementptr inbounds float, float* %224, i32 12
  %225 = load float, float* %arrayidx184, align 4
  %mul185 = fmul float %223, %225
  %226 = load float, float* %sum135, align 4
  %sub186 = fsub float %226, %mul185
  store float %sub186, float* %sum135, align 4
  %227 = load float*, float** %window, align 4
  %arrayidx187 = getelementptr inbounds float, float* %227, i32 -14
  %228 = load float, float* %arrayidx187, align 4
  %229 = load float*, float** %b0, align 4
  %arrayidx188 = getelementptr inbounds float, float* %229, i32 13
  %230 = load float, float* %arrayidx188, align 4
  %mul189 = fmul float %228, %230
  %231 = load float, float* %sum135, align 4
  %sub190 = fsub float %231, %mul189
  store float %sub190, float* %sum135, align 4
  %232 = load float*, float** %window, align 4
  %arrayidx191 = getelementptr inbounds float, float* %232, i32 -15
  %233 = load float, float* %arrayidx191, align 4
  %234 = load float*, float** %b0, align 4
  %arrayidx192 = getelementptr inbounds float, float* %234, i32 14
  %235 = load float, float* %arrayidx192, align 4
  %mul193 = fmul float %233, %235
  %236 = load float, float* %sum135, align 4
  %sub194 = fsub float %236, %mul193
  store float %sub194, float* %sum135, align 4
  %237 = load float*, float** %window, align 4
  %arrayidx195 = getelementptr inbounds float, float* %237, i32 0
  %238 = load float, float* %arrayidx195, align 4
  %239 = load float*, float** %b0, align 4
  %arrayidx196 = getelementptr inbounds float, float* %239, i32 15
  %240 = load float, float* %arrayidx196, align 4
  %mul197 = fmul float %238, %240
  %241 = load float, float* %sum135, align 4
  %sub198 = fsub float %241, %mul197
  store float %sub198, float* %sum135, align 4
  %242 = load float, float* %sum135, align 4
  %243 = load float*, float** %samples, align 4
  store float %242, float* %243, align 4
  br label %for.inc199

for.inc199:                                       ; preds = %for.body134
  %244 = load i32, i32* %j, align 4
  %dec200 = add nsw i32 %244, -1
  store i32 %dec200, i32* %j, align 4
  %245 = load float*, float** %b0, align 4
  %add.ptr201 = getelementptr inbounds float, float* %245, i32 -16
  store float* %add.ptr201, float** %b0, align 4
  %246 = load float*, float** %window, align 4
  %add.ptr202 = getelementptr inbounds float, float* %246, i32 -32
  store float* %add.ptr202, float** %window, align 4
  %247 = load float*, float** %samples, align 4
  %add.ptr203 = getelementptr inbounds float, float* %247, i32 2
  store float* %add.ptr203, float** %samples, align 4
  br label %for.cond132

for.end204:                                       ; preds = %for.cond132
  %248 = load i32*, i32** %pnt.addr, align 4
  %249 = load i32, i32* %248, align 4
  %add205 = add i32 %249, 256
  store i32 %add205, i32* %248, align 4
  %250 = load i32, i32* %clip, align 4
  ret i32 %250
}

declare void @dct64(float*, float*, float*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
