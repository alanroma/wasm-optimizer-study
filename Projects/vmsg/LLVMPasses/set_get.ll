; ModuleID = 'set_get.c'
source_filename = "set_get.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.2, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon.0], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon.0 = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.1, %struct.anon.1 }
%struct.anon.1 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.2 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.anon.3 = type { i32, i32, i32 }

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_num_samples(%struct.lame_global_struct* %gfp, i32 %num_samples) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %num_samples.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %num_samples, i32* %num_samples.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %num_samples.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 1
  store i32 %1, i32* %num_samples1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare i32 @is_lame_global_flags_valid(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_num_samples(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 1
  %2 = load i32, i32* %num_samples, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_in_samplerate(%struct.lame_global_struct* %gfp, i32 %in_samplerate) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %in_samplerate.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %in_samplerate, i32* %in_samplerate.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end2

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %in_samplerate.addr, align 4
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %2 = load i32, i32* %in_samplerate.addr, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 3
  store i32 %2, i32* %samplerate_in, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end2:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end2, %if.end, %if.then1
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_in_samplerate(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 3
  %2 = load i32, i32* %samplerate_in, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_num_channels(%struct.lame_global_struct* %gfp, i32 %num_channels) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %num_channels.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %num_channels, i32* %num_channels.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %num_channels.addr, align 4
  %cmp = icmp slt i32 2, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %num_channels.addr, align 4
  %cmp1 = icmp sge i32 0, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %num_channels.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 2
  store i32 %3, i32* %num_channels3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_num_channels(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 2
  %2 = load i32, i32* %num_channels, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_scale(%struct.lame_global_struct* %gfp, float %scale) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %scale.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %scale, float* %scale.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %scale.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 5
  store float %1, float* %scale1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_scale(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 5
  %2 = load float, float* %scale, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_scale_left(%struct.lame_global_struct* %gfp, float %scale) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %scale.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %scale, float* %scale.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %scale.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 6
  store float %1, float* %scale_left, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_scale_left(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 6
  %2 = load float, float* %scale_left, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_scale_right(%struct.lame_global_struct* %gfp, float %scale) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %scale.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %scale, float* %scale.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %scale.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 7
  store float %1, float* %scale_right, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_scale_right(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 7
  %2 = load float, float* %scale_right, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_out_samplerate(%struct.lame_global_struct* %gfp, i32 %out_samplerate) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %out_samplerate.addr = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %out_samplerate, i32* %out_samplerate.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %out_samplerate.addr, align 4
  %cmp = icmp ne i32 %1, 0
  br i1 %cmp, label %if.then1, label %if.end5

if.then1:                                         ; preds = %if.then
  store i32 0, i32* %v, align 4
  %2 = load i32, i32* %out_samplerate.addr, align 4
  %call2 = call i32 @SmpFrqIndex(i32 %2, i32* %v)
  %cmp3 = icmp slt i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then1
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then1
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %3 = load i32, i32* %out_samplerate.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 4
  store i32 %3, i32* %samplerate_out, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.end5, %if.then4
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

declare i32 @SmpFrqIndex(i32, i32*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_out_samplerate(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 4
  %2 = load i32, i32* %samplerate_out, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_analysis(%struct.lame_global_struct* %gfp, i32 %analysis) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %analysis.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %analysis, i32* %analysis.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %analysis.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %analysis.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %analysis.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %analysis3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 8
  store i32 %3, i32* %analysis3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_analysis(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %analysis = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 8
  %2 = load i32, i32* %analysis, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_bWriteVbrTag(%struct.lame_global_struct* %gfp, i32 %bWriteVbrTag) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %bWriteVbrTag.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %bWriteVbrTag, i32* %bWriteVbrTag.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %bWriteVbrTag.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %bWriteVbrTag.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %bWriteVbrTag.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 9
  store i32 %3, i32* %write_lame_tag, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_bWriteVbrTag(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 9
  %2 = load i32, i32* %write_lame_tag, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_decode_only(%struct.lame_global_struct* %gfp, i32 %decode_only) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %decode_only.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %decode_only, i32* %decode_only.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %decode_only.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %decode_only.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %decode_only.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %decode_only3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 10
  store i32 %3, i32* %decode_only3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_decode_only(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %decode_only = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 10
  %2 = load i32, i32* %decode_only, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ogg(%struct.lame_global_struct* %gfp, i32 %ogg) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ogg.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %ogg, i32* %ogg.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %ogg.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ogg(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_quality(%struct.lame_global_struct* %gfp, i32 %quality) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %quality.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %quality, i32* %quality.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %quality.addr, align 4
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality2 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 11
  store i32 0, i32* %quality2, align 4
  br label %if.end8

if.else:                                          ; preds = %if.then
  %3 = load i32, i32* %quality.addr, align 4
  %cmp3 = icmp sgt i32 %3, 9
  br i1 %cmp3, label %if.then4, label %if.else6

if.then4:                                         ; preds = %if.else
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality5 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 11
  store i32 9, i32* %quality5, align 4
  br label %if.end

if.else6:                                         ; preds = %if.else
  %5 = load i32, i32* %quality.addr, align 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality7 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %6, i32 0, i32 11
  store i32 %5, i32* %quality7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else6, %if.then4
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then1
  store i32 0, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.end8
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_quality(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 11
  %2 = load i32, i32* %quality, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_mode(%struct.lame_global_struct* %gfp, i32 %mode) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %mode.addr = alloca i32, align 4
  %mpg_mode = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %mode, i32* %mode.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %mode.addr, align 4
  store i32 %1, i32* %mpg_mode, align 4
  %2 = load i32, i32* %mpg_mode, align 4
  %cmp = icmp slt i32 %2, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %3 = load i32, i32* %mpg_mode, align 4
  %cmp1 = icmp sle i32 5, %3
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i32, i32* %mode.addr, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 12
  store i32 %4, i32* %mode3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_mode(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 12
  %2 = load i32, i32* %mode, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 4, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_mode_automs(%struct.lame_global_struct* %gfp, i32 %mode_automs) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %mode_automs.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %mode_automs, i32* %mode_automs.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %mode_automs.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %mode_automs.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call3 = call i32 @lame_set_mode(%struct.lame_global_struct* %3, i32 1)
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_mode_automs(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_force_ms(%struct.lame_global_struct* %gfp, i32 %force_ms) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %force_ms.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %force_ms, i32* %force_ms.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %force_ms.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %force_ms.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %force_ms.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %force_ms3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 13
  store i32 %3, i32* %force_ms3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_force_ms(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %force_ms = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 13
  %2 = load i32, i32* %force_ms, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_free_format(%struct.lame_global_struct* %gfp, i32 %free_format) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %free_format.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %free_format, i32* %free_format.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %free_format.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %free_format.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %free_format.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %free_format3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 14
  store i32 %3, i32* %free_format3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_free_format(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %free_format = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 14
  %2 = load i32, i32* %free_format, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_findReplayGain(%struct.lame_global_struct* %gfp, i32 %findReplayGain) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %findReplayGain.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %findReplayGain, i32* %findReplayGain.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %findReplayGain.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %findReplayGain.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %findReplayGain.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %findReplayGain3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 15
  store i32 %3, i32* %findReplayGain3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_findReplayGain(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %findReplayGain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 15
  %2 = load i32, i32* %findReplayGain, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_decode_on_the_fly(%struct.lame_global_struct* %gfp, i32 %decode_on_the_fly) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %decode_on_the_fly.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %decode_on_the_fly, i32* %decode_on_the_fly.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_decode_on_the_fly(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %decode_on_the_fly = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 16
  %2 = load i32, i32* %decode_on_the_fly, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_findPeakSample(%struct.lame_global_struct* %gfp, i32 %arg) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %arg.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %arg, i32* %arg.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %arg.addr, align 4
  %call = call i32 @lame_set_decode_on_the_fly(%struct.lame_global_struct* %0, i32 %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_findPeakSample(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_get_decode_on_the_fly(%struct.lame_global_struct* %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ReplayGain_input(%struct.lame_global_struct* %gfp, i32 %arg) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %arg.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %arg, i32* %arg.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %arg.addr, align 4
  %call = call i32 @lame_set_findReplayGain(%struct.lame_global_struct* %0, i32 %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ReplayGain_input(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_get_findReplayGain(%struct.lame_global_struct* %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ReplayGain_decode(%struct.lame_global_struct* %gfp, i32 %arg) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %arg.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %arg, i32* %arg.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %arg.addr, align 4
  %call = call i32 @lame_set_decode_on_the_fly(%struct.lame_global_struct* %0, i32 %1)
  %cmp = icmp slt i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %3 = load i32, i32* %arg.addr, align 4
  %call1 = call i32 @lame_set_findReplayGain(%struct.lame_global_struct* %2, i32 %3)
  %cmp2 = icmp slt i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %lor.lhs.false
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ReplayGain_decode(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_get_decode_on_the_fly(%struct.lame_global_struct* %0)
  %cmp = icmp sgt i32 %call, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1 = call i32 @lame_get_findReplayGain(%struct.lame_global_struct* %1)
  %cmp2 = icmp sgt i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %land.lhs.true, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %2 = load i32, i32* %retval, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_nogap_total(%struct.lame_global_struct* %gfp, i32 %the_nogap_total) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %the_nogap_total.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %the_nogap_total, i32* %the_nogap_total.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %the_nogap_total.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_total = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 18
  store i32 %1, i32* %nogap_total, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_nogap_total(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_total = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 18
  %2 = load i32, i32* %nogap_total, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_nogap_currentindex(%struct.lame_global_struct* %gfp, i32 %the_nogap_index) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %the_nogap_index.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %the_nogap_index, i32* %the_nogap_index.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %the_nogap_index.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_current = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 19
  store i32 %1, i32* %nogap_current, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_nogap_currentindex(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_current = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 19
  %2 = load i32, i32* %nogap_current, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_errorf(%struct.lame_global_struct* %gfp, void (i8*, i8*)* %func) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %func.addr = alloca void (i8*, i8*)*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store void (i8*, i8*)* %func, void (i8*, i8*)** %func.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load void (i8*, i8*)*, void (i8*, i8*)** %func.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 68
  %errorf = getelementptr inbounds %struct.anon, %struct.anon* %report, i32 0, i32 2
  store void (i8*, i8*)* %1, void (i8*, i8*)** %errorf, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_debugf(%struct.lame_global_struct* %gfp, void (i8*, i8*)* %func) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %func.addr = alloca void (i8*, i8*)*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store void (i8*, i8*)* %func, void (i8*, i8*)** %func.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load void (i8*, i8*)*, void (i8*, i8*)** %func.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 68
  %debugf = getelementptr inbounds %struct.anon, %struct.anon* %report, i32 0, i32 1
  store void (i8*, i8*)* %1, void (i8*, i8*)** %debugf, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_msgf(%struct.lame_global_struct* %gfp, void (i8*, i8*)* %func) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %func.addr = alloca void (i8*, i8*)*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store void (i8*, i8*)* %func, void (i8*, i8*)** %func.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load void (i8*, i8*)*, void (i8*, i8*)** %func.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 68
  %msgf = getelementptr inbounds %struct.anon, %struct.anon* %report, i32 0, i32 0
  store void (i8*, i8*)* %1, void (i8*, i8*)** %msgf, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_brate(%struct.lame_global_struct* %gfp, i32 %brate) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %brate.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %brate, i32* %brate.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %brate.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 24
  store i32 %1, i32* %brate1, align 4
  %3 = load i32, i32* %brate.addr, align 4
  %cmp = icmp sgt i32 %3, 320
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %disable_reservoir = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 32
  store i32 1, i32* %disable_reservoir, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.end
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_brate(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 24
  %2 = load i32, i32* %brate, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_compression_ratio(%struct.lame_global_struct* %gfp, float %compression_ratio) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %compression_ratio.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %compression_ratio, float* %compression_ratio.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %compression_ratio.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 25
  store float %1, float* %compression_ratio1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_compression_ratio(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 25
  %2 = load float, float* %compression_ratio, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_copyright(%struct.lame_global_struct* %gfp, i32 %copyright) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %copyright.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %copyright, i32* %copyright.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %copyright.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %copyright.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %copyright.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %copyright3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 26
  store i32 %3, i32* %copyright3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_copyright(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %copyright = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 26
  %2 = load i32, i32* %copyright, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_original(%struct.lame_global_struct* %gfp, i32 %original) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %original.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %original, i32* %original.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %original.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %original.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %original.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %original3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 27
  store i32 %3, i32* %original3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_original(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %original = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 27
  %2 = load i32, i32* %original, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_error_protection(%struct.lame_global_struct* %gfp, i32 %error_protection) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %error_protection.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %error_protection, i32* %error_protection.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %error_protection.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %error_protection.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %error_protection.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %error_protection3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 30
  store i32 %3, i32* %error_protection3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_error_protection(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %error_protection = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 30
  %2 = load i32, i32* %error_protection, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_padding_type(%struct.lame_global_struct* %gfp, i32 %padding_type) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %padding_type.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %padding_type, i32* %padding_type.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %padding_type.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_padding_type(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_extension(%struct.lame_global_struct* %gfp, i32 %extension) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %extension.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %extension, i32* %extension.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %extension.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %extension.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %extension.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %extension3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 28
  store i32 %3, i32* %extension3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_extension(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %extension = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 28
  %2 = load i32, i32* %extension, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_strict_ISO(%struct.lame_global_struct* %gfp, i32 %val) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %val.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %val.addr, align 4
  %cmp1 = icmp slt i32 2, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %val.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 31
  store i32 %3, i32* %strict_ISO, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_strict_ISO(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 31
  %2 = load i32, i32* %strict_ISO, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_disable_reservoir(%struct.lame_global_struct* %gfp, i32 %disable_reservoir) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %disable_reservoir.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %disable_reservoir, i32* %disable_reservoir.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %disable_reservoir.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %disable_reservoir.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %disable_reservoir.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %disable_reservoir3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 32
  store i32 %3, i32* %disable_reservoir3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_disable_reservoir(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %disable_reservoir = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 32
  %2 = load i32, i32* %disable_reservoir, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_experimentalX(%struct.lame_global_struct* %gfp, i32 %experimentalX) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %experimentalX.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %experimentalX, i32* %experimentalX.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i32, i32* %experimentalX.addr, align 4
  %call1 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %1, i32 %2)
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %4 = load i32, i32* %experimentalX.addr, align 4
  %call2 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %3, i32 %4)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_quant_comp(%struct.lame_global_struct* %gfp, i32 %quant_type) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %quant_type.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %quant_type, i32* %quant_type.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %quant_type.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 33
  store i32 %1, i32* %quant_comp, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %gfp, i32 %quant_type) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %quant_type.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %quant_type, i32* %quant_type.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %quant_type.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 34
  store i32 %1, i32* %quant_comp_short, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_experimentalX(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_get_quant_comp(%struct.lame_global_struct* %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_quant_comp(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 33
  %2 = load i32, i32* %quant_comp, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_quant_comp_short(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 34
  %2 = load i32, i32* %quant_comp_short, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_experimentalY(%struct.lame_global_struct* %gfp, i32 %experimentalY) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %experimentalY.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %experimentalY, i32* %experimentalY.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %experimentalY.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalY1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 35
  store i32 %1, i32* %experimentalY1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_experimentalY(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalY = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 35
  %2 = load i32, i32* %experimentalY, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_experimentalZ(%struct.lame_global_struct* %gfp, i32 %experimentalZ) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %experimentalZ.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %experimentalZ, i32* %experimentalZ.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %experimentalZ.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalZ1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 36
  store i32 %1, i32* %experimentalZ1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_experimentalZ(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalZ = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 36
  %2 = load i32, i32* %experimentalZ, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_exp_nspsytune(%struct.lame_global_struct* %gfp, i32 %exp_nspsytune) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %exp_nspsytune.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %exp_nspsytune, i32* %exp_nspsytune.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %exp_nspsytune.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 37
  store i32 %1, i32* %exp_nspsytune1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_exp_nspsytune(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 37
  %2 = load i32, i32* %exp_nspsytune, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR(%struct.lame_global_struct* %gfp, i32 %VBR) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR.addr = alloca i32, align 4
  %vbr_q = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR, i32* %VBR.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %VBR.addr, align 4
  store i32 %1, i32* %vbr_q, align 4
  %2 = load i32, i32* %vbr_q, align 4
  %cmp = icmp sgt i32 0, %2
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %3 = load i32, i32* %vbr_q, align 4
  %cmp1 = icmp sle i32 5, %3
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i32, i32* %VBR.addr, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 39
  store i32 %4, i32* %VBR3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 39
  %2 = load i32, i32* %VBR, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_q(%struct.lame_global_struct* %gfp, i32 %VBR_q) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_q.addr = alloca i32, align 4
  %ret = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR_q, i32* %VBR_q.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  store i32 0, i32* %ret, align 4
  %1 = load i32, i32* %VBR_q.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 -1, i32* %ret, align 4
  store i32 0, i32* %VBR_q.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %2 = load i32, i32* %VBR_q.addr, align 4
  %cmp2 = icmp slt i32 9, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %ret, align 4
  store i32 9, i32* %VBR_q.addr, align 4
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %3 = load i32, i32* %VBR_q.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q5 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 41
  store i32 %3, i32* %VBR_q5, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 40
  store float 0.000000e+00, float* %VBR_q_frac, align 4
  %6 = load i32, i32* %ret, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.end4
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR_q(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 41
  %2 = load i32, i32* %VBR_q, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_quality(%struct.lame_global_struct* %gfp, float %VBR_q) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_q.addr = alloca float, align 4
  %ret = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %VBR_q, float* %VBR_q.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end10

if.then:                                          ; preds = %entry
  store i32 0, i32* %ret, align 4
  %1 = load float, float* %VBR_q.addr, align 4
  %cmp = fcmp ogt float 0.000000e+00, %1
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 -1, i32* %ret, align 4
  store float 0.000000e+00, float* %VBR_q.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %2 = load float, float* %VBR_q.addr, align 4
  %conv = fpext float %2 to double
  %cmp2 = fcmp olt double 9.999000e+00, %conv
  br i1 %cmp2, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 -1, i32* %ret, align 4
  store float 0x4023FF7CE0000000, float* %VBR_q.addr, align 4
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.end
  %3 = load float, float* %VBR_q.addr, align 4
  %conv6 = fptosi float %3 to i32
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q7 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 41
  store i32 %conv6, i32* %VBR_q7, align 4
  %5 = load float, float* %VBR_q.addr, align 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q8 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %6, i32 0, i32 41
  %7 = load i32, i32* %VBR_q8, align 4
  %conv9 = sitofp i32 %7 to float
  %sub = fsub float %5, %conv9
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %8, i32 0, i32 40
  store float %sub, float* %VBR_q_frac, align 4
  %9 = load i32, i32* %ret, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end10, %if.end5
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_VBR_quality(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 41
  %2 = load i32, i32* %VBR_q, align 4
  %conv = sitofp i32 %2 to float
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 40
  %4 = load float, float* %VBR_q_frac, align 4
  %add = fadd float %conv, %4
  store float %add, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load float, float* %retval, align 4
  ret float %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %gfp, i32 %VBR_mean_bitrate_kbps) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_mean_bitrate_kbps.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR_mean_bitrate_kbps, i32* %VBR_mean_bitrate_kbps.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %VBR_mean_bitrate_kbps.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 42
  store i32 %1, i32* %VBR_mean_bitrate_kbps1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 42
  %2 = load i32, i32* %VBR_mean_bitrate_kbps, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_min_bitrate_kbps(%struct.lame_global_struct* %gfp, i32 %VBR_min_bitrate_kbps) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_min_bitrate_kbps.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR_min_bitrate_kbps, i32* %VBR_min_bitrate_kbps.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %VBR_min_bitrate_kbps.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 43
  store i32 %1, i32* %VBR_min_bitrate_kbps1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR_min_bitrate_kbps(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 43
  %2 = load i32, i32* %VBR_min_bitrate_kbps, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_max_bitrate_kbps(%struct.lame_global_struct* %gfp, i32 %VBR_max_bitrate_kbps) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_max_bitrate_kbps.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR_max_bitrate_kbps, i32* %VBR_max_bitrate_kbps.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %VBR_max_bitrate_kbps.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 44
  store i32 %1, i32* %VBR_max_bitrate_kbps1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR_max_bitrate_kbps(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 44
  %2 = load i32, i32* %VBR_max_bitrate_kbps, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_VBR_hard_min(%struct.lame_global_struct* %gfp, i32 %VBR_hard_min) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %VBR_hard_min.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %VBR_hard_min, i32* %VBR_hard_min.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %VBR_hard_min.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %VBR_hard_min.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %VBR_hard_min.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_hard_min3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 45
  store i32 %3, i32* %VBR_hard_min3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.end, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_VBR_hard_min(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_hard_min = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 45
  %2 = load i32, i32* %VBR_hard_min, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_lowpassfreq(%struct.lame_global_struct* %gfp, i32 %lowpassfreq) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lowpassfreq.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %lowpassfreq, i32* %lowpassfreq.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %lowpassfreq.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 46
  store i32 %1, i32* %lowpassfreq1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_lowpassfreq(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 46
  %2 = load i32, i32* %lowpassfreq, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_lowpasswidth(%struct.lame_global_struct* %gfp, i32 %lowpasswidth) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lowpasswidth.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %lowpasswidth, i32* %lowpasswidth.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %lowpasswidth.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpasswidth1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 48
  store i32 %1, i32* %lowpasswidth1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_lowpasswidth(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 48
  %2 = load i32, i32* %lowpasswidth, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_highpassfreq(%struct.lame_global_struct* %gfp, i32 %highpassfreq) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %highpassfreq.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %highpassfreq, i32* %highpassfreq.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %highpassfreq.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpassfreq1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 47
  store i32 %1, i32* %highpassfreq1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_highpassfreq(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 47
  %2 = load i32, i32* %highpassfreq, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_highpasswidth(%struct.lame_global_struct* %gfp, i32 %highpasswidth) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %highpasswidth.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %highpasswidth, i32* %highpasswidth.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %highpasswidth.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpasswidth1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 49
  store i32 %1, i32* %highpasswidth1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_highpasswidth(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 49
  %2 = load i32, i32* %highpasswidth, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_maskingadjust(%struct.lame_global_struct* %gfp, float %adjust) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %adjust.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %adjust, float* %adjust.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %adjust.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 50
  store float %1, float* %maskingadjust, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_maskingadjust(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 50
  %2 = load float, float* %maskingadjust, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %gfp, float %adjust) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %adjust.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %adjust, float* %adjust.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %adjust.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 51
  store float %1, float* %maskingadjust_short, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_maskingadjust_short(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 51
  %2 = load float, float* %maskingadjust_short, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ATHonly(%struct.lame_global_struct* %gfp, i32 %ATHonly) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ATHonly.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %ATHonly, i32* %ATHonly.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %ATHonly.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHonly1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 52
  store i32 %1, i32* %ATHonly1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ATHonly(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHonly = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 52
  %2 = load i32, i32* %ATHonly, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ATHshort(%struct.lame_global_struct* %gfp, i32 %ATHshort) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ATHshort.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %ATHshort, i32* %ATHshort.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %ATHshort.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHshort1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 53
  store i32 %1, i32* %ATHshort1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ATHshort(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHshort = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 53
  %2 = load i32, i32* %ATHshort, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_noATH(%struct.lame_global_struct* %gfp, i32 %noATH) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %noATH.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %noATH, i32* %noATH.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %noATH.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noATH1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 54
  store i32 %1, i32* %noATH1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_noATH(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noATH = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 54
  %2 = load i32, i32* %noATH, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ATHtype(%struct.lame_global_struct* %gfp, i32 %ATHtype) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ATHtype.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %ATHtype, i32* %ATHtype.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %ATHtype.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 55
  store i32 %1, i32* %ATHtype1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_ATHtype(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 55
  %2 = load i32, i32* %ATHtype, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ATHcurve(%struct.lame_global_struct* %gfp, float %ATHcurve) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ATHcurve.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %ATHcurve, float* %ATHcurve.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %ATHcurve.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 56
  store float %1, float* %ATHcurve1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_ATHcurve(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 56
  %2 = load float, float* %ATHcurve, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_ATHlower(%struct.lame_global_struct* %gfp, float %ATHlower) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ATHlower.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %ATHlower, float* %ATHlower.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %ATHlower.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATH_lower_db = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 57
  store float %1, float* %ATH_lower_db, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_ATHlower(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATH_lower_db = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 57
  %2 = load float, float* %ATH_lower_db, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_athaa_type(%struct.lame_global_struct* %gfp, i32 %athaa_type) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %athaa_type.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %athaa_type, i32* %athaa_type.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %athaa_type.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_type1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 58
  store i32 %1, i32* %athaa_type1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_athaa_type(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_type = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 58
  %2 = load i32, i32* %athaa_type, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_athaa_loudapprox(%struct.lame_global_struct* %gfp, i32 %athaa_loudapprox) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %athaa_loudapprox.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %athaa_loudapprox, i32* %athaa_loudapprox.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %athaa_loudapprox.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_athaa_loudapprox(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_athaa_sensitivity(%struct.lame_global_struct* %gfp, float %athaa_sensitivity) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %athaa_sensitivity.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %athaa_sensitivity, float* %athaa_sensitivity.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %athaa_sensitivity.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_sensitivity1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 59
  store float %1, float* %athaa_sensitivity1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_athaa_sensitivity(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_sensitivity = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 59
  %2 = load float, float* %athaa_sensitivity, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_cwlimit(%struct.lame_global_struct* %gfp, i32 %cwlimit) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %cwlimit.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %cwlimit, i32* %cwlimit.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %cwlimit.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_cwlimit(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_allow_diff_short(%struct.lame_global_struct* %gfp, i32 %allow_diff_short) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %allow_diff_short.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %allow_diff_short, i32* %allow_diff_short.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %allow_diff_short.addr, align 4
  %tobool1 = icmp ne i32 %1, 0
  %2 = zext i1 %tobool1 to i64
  %cond = select i1 %tobool1, i32 0, i32 1
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 60
  store i32 %cond, i32* %short_blocks, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_allow_diff_short(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 60
  %2 = load i32, i32* %short_blocks, align 4
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  store i32 1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then1
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_useTemporal(%struct.lame_global_struct* %gfp, i32 %useTemporal) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %useTemporal.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %useTemporal, i32* %useTemporal.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %useTemporal.addr, align 4
  %cmp = icmp sle i32 0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %useTemporal.addr, align 4
  %cmp1 = icmp sle i32 %2, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %land.lhs.true
  %3 = load i32, i32* %useTemporal.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 61
  store i32 %3, i32* %useTemporal3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_useTemporal(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 61
  %2 = load i32, i32* %useTemporal, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_interChRatio(%struct.lame_global_struct* %gfp, float %ratio) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ratio.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %ratio, float* %ratio.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load float, float* %ratio.addr, align 4
  %cmp = fcmp ole float 0.000000e+00, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load float, float* %ratio.addr, align 4
  %conv = fpext float %2 to double
  %cmp1 = fcmp ole double %conv, 1.000000e+00
  br i1 %cmp1, label %if.then3, label %if.end

if.then3:                                         ; preds = %land.lhs.true
  %3 = load float, float* %ratio.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 62
  store float %3, float* %interChRatio, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_interChRatio(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 62
  %2 = load float, float* %interChRatio, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_substep(%struct.lame_global_struct* %gfp, i32 %method) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %method.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %method, i32* %method.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %method.addr, align 4
  %cmp = icmp sle i32 0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %method.addr, align 4
  %cmp1 = icmp sle i32 %2, 7
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %land.lhs.true
  %3 = load i32, i32* %method.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %substep_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 20
  store i32 %3, i32* %substep_shaping, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end3

if.end3:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_substep(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %substep_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 20
  %2 = load i32, i32* %substep_shaping, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_sfscale(%struct.lame_global_struct* %gfp, i32 %val) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %val.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %val.addr, align 4
  %cmp = icmp ne i32 %1, 0
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 2, i32 1
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noise_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 21
  store i32 %cond, i32* %noise_shaping, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_sfscale(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noise_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 21
  %2 = load i32, i32* %noise_shaping, align 4
  %cmp = icmp eq i32 %2, 2
  %3 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  store i32 %cond, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_subblock_gain(%struct.lame_global_struct* %gfp, i32 %sbgain) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %sbgain.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %sbgain, i32* %sbgain.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %sbgain.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 22
  store i32 %1, i32* %subblock_gain, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_subblock_gain(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 22
  %2 = load i32, i32* %subblock_gain, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_no_short_blocks(%struct.lame_global_struct* %gfp, i32 %no_short_blocks) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %no_short_blocks.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %no_short_blocks, i32* %no_short_blocks.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %no_short_blocks.addr, align 4
  %cmp = icmp sle i32 0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %no_short_blocks.addr, align 4
  %cmp1 = icmp sle i32 %2, 1
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %land.lhs.true
  %3 = load i32, i32* %no_short_blocks.addr, align 4
  %tobool3 = icmp ne i32 %3, 0
  %4 = zext i1 %tobool3 to i64
  %cond = select i1 %tobool3, i32 2, i32 0
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 60
  store i32 %cond, i32* %short_blocks, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then2
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_no_short_blocks(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 60
  %2 = load i32, i32* %short_blocks, align 4
  switch i32 %2, label %sw.default [
    i32 -1, label %sw.bb
    i32 2, label %sw.bb1
    i32 0, label %sw.bb2
    i32 1, label %sw.bb2
    i32 3, label %sw.bb2
  ]

sw.default:                                       ; preds = %if.then
  br label %sw.bb

sw.bb:                                            ; preds = %if.then, %sw.default
  store i32 -1, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %if.then
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %if.then, %if.then, %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %sw.bb2, %sw.bb1, %sw.bb
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_force_short_blocks(%struct.lame_global_struct* %gfp, i32 %short_blocks) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %short_blocks.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %short_blocks, i32* %short_blocks.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end12

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %short_blocks.addr, align 4
  %cmp = icmp sgt i32 0, %1
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %2 = load i32, i32* %short_blocks.addr, align 4
  %cmp1 = icmp slt i32 1, %2
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %short_blocks.addr, align 4
  %cmp3 = icmp eq i32 %3, 1
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks5 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 60
  store i32 3, i32* %short_blocks5, align 4
  br label %if.end11

if.else:                                          ; preds = %if.end
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks6 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 60
  %6 = load i32, i32* %short_blocks6, align 4
  %cmp7 = icmp eq i32 %6, 3
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.else
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks9 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %7, i32 0, i32 60
  store i32 0, i32* %short_blocks9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.then4
  store i32 0, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.end11, %if.then2
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_force_short_blocks(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 60
  %2 = load i32, i32* %short_blocks, align 4
  switch i32 %2, label %sw.default [
    i32 -1, label %sw.bb
    i32 2, label %sw.bb1
    i32 0, label %sw.bb1
    i32 1, label %sw.bb1
    i32 3, label %sw.bb2
  ]

sw.default:                                       ; preds = %if.then
  br label %sw.bb

sw.bb:                                            ; preds = %if.then, %sw.default
  store i32 -1, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %if.then, %if.then, %if.then
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %if.then
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %sw.bb2, %sw.bb1, %sw.bb
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %gfp, float %lrm) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lrm.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %lrm, float* %lrm.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %lrm.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 66
  store float %1, float* %attackthre, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_short_threshold_lrm(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 66
  %2 = load float, float* %attackthre, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %gfp, float %s) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %s.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %s, float* %s.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %s.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre_s = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 67
  store float %1, float* %attackthre_s, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_short_threshold_s(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre_s = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 67
  %2 = load float, float* %attackthre_s, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_short_threshold(%struct.lame_global_struct* %gfp, float %lrm, float %s) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lrm.addr = alloca float, align 4
  %s.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %lrm, float* %lrm.addr, align 4
  store float %s, float* %s.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load float, float* %lrm.addr, align 4
  %call1 = call i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %1, float %2)
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %4 = load float, float* %s.addr, align 4
  %call2 = call i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %3, float %4)
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_emphasis(%struct.lame_global_struct* %gfp, i32 %emphasis) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %emphasis.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %emphasis, i32* %emphasis.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %emphasis.addr, align 4
  %cmp = icmp sle i32 0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %emphasis.addr, align 4
  %cmp1 = icmp slt i32 %2, 4
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %land.lhs.true
  %3 = load i32, i32* %emphasis.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %emphasis3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 29
  store i32 %3, i32* %emphasis3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then2
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_emphasis(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %emphasis = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 29
  %2 = load i32, i32* %emphasis, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_version(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 0
  %5 = load i32, i32* %version, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

declare i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_encoder_delay(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  %encoder_delay = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 6
  %5 = load i32, i32* %encoder_delay, align 8
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_encoder_padding(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  %encoder_padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 7
  %5 = load i32, i32* %encoder_padding, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_framesize(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 15
  %6 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %6
  store i32 %mul, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then3
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_frameNum(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  %frame_number = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 3
  %5 = load i32, i32* %frame_number, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_mf_samples_to_encode(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 11
  %mf_samples_to_encode = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 18
  %5 = load i32, i32* %mf_samples_to_encode, align 8
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_size_mp3buffer(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %size = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call4 = call i32 @compute_flushbits(%struct.lame_internal_flags* %4, i32* %size)
  %5 = load i32, i32* %size, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

declare i32 @compute_flushbits(%struct.lame_internal_flags*, i32*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_RadioGain(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 15
  %RadioGain = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 2
  %5 = load i32, i32* %RadioGain, align 8
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_AudiophileGain(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_PeakSample(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 15
  %PeakSample = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 1
  %5 = load float, float* %PeakSample, align 4
  store float %5, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_noclipGainChange(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 15
  %noclipGainChange = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 3
  %5 = load i32, i32* %noclipGainChange, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_noclipScale(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 15
  %noclipScale = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 0
  %5 = load float, float* %noclipScale, align 8
  store float %5, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_totalframes(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %pcm_samples_per_frame = alloca i32, align 4
  %pcm_samples_to_encode = alloca i32, align 4
  %end_padding = alloca i32, align 4
  %frames = alloca i32, align 4
  %resampled_samples_to_encode = alloca double, align 8
  %frames_f = alloca double, align 8
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end45

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end44

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 15
  %6 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %6
  store i32 %mul, i32* %pcm_samples_per_frame, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %7, i32 0, i32 1
  %8 = load i32, i32* %num_samples, align 4
  store i32 %8, i32* %pcm_samples_to_encode, align 4
  store i32 0, i32* %end_padding, align 4
  store i32 0, i32* %frames, align 4
  %9 = load i32, i32* %pcm_samples_to_encode, align 4
  %cmp = icmp eq i32 %9, -1
  br i1 %cmp, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then3
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then3
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 11
  %11 = load i32, i32* %samplerate_in, align 4
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 12
  %13 = load i32, i32* %samplerate_out, align 4
  %cmp6 = icmp ne i32 %11, %13
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  store double 0.000000e+00, double* %resampled_samples_to_encode, align 8
  store double 0.000000e+00, double* %frames_f, align 8
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in8 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 11
  %15 = load i32, i32* %samplerate_in8, align 4
  %cmp9 = icmp sgt i32 %15, 0
  br i1 %cmp9, label %if.then10, label %if.end16

if.then10:                                        ; preds = %if.then7
  %16 = load i32, i32* %pcm_samples_to_encode, align 4
  %conv = uitofp i32 %16 to double
  store double %conv, double* %resampled_samples_to_encode, align 8
  %17 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out11 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %17, i32 0, i32 12
  %18 = load i32, i32* %samplerate_out11, align 4
  %conv12 = sitofp i32 %18 to double
  %19 = load double, double* %resampled_samples_to_encode, align 8
  %mul13 = fmul double %19, %conv12
  store double %mul13, double* %resampled_samples_to_encode, align 8
  %20 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in14 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %20, i32 0, i32 11
  %21 = load i32, i32* %samplerate_in14, align 4
  %conv15 = sitofp i32 %21 to double
  %22 = load double, double* %resampled_samples_to_encode, align 8
  %div = fdiv double %22, %conv15
  store double %div, double* %resampled_samples_to_encode, align 8
  br label %if.end16

if.end16:                                         ; preds = %if.then10, %if.then7
  %23 = load double, double* %resampled_samples_to_encode, align 8
  %cmp17 = fcmp ole double %23, 0.000000e+00
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  store i32 0, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.end16
  %24 = load double, double* %resampled_samples_to_encode, align 8
  %25 = load i32, i32* %pcm_samples_per_frame, align 4
  %conv21 = uitofp i32 %25 to double
  %div22 = fdiv double %24, %conv21
  %26 = call double @llvm.floor.f64(double %div22)
  store double %26, double* %frames_f, align 8
  %27 = load double, double* %frames_f, align 8
  %cmp23 = fcmp oge double %27, 0x41DFFFFFFF400000
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end20
  store i32 0, i32* %retval, align 4
  br label %return

if.end26:                                         ; preds = %if.end20
  %28 = load double, double* %frames_f, align 8
  %conv27 = fptosi double %28 to i32
  store i32 %conv27, i32* %frames, align 4
  %29 = load i32, i32* %frames, align 4
  %30 = load i32, i32* %pcm_samples_per_frame, align 4
  %mul28 = mul i32 %29, %30
  %conv29 = uitofp i32 %mul28 to double
  %31 = load double, double* %resampled_samples_to_encode, align 8
  %sub = fsub double %31, %conv29
  store double %sub, double* %resampled_samples_to_encode, align 8
  %32 = load double, double* %resampled_samples_to_encode, align 8
  %33 = call double @llvm.ceil.f64(double %32)
  %conv30 = fptoui double %33 to i32
  store i32 %conv30, i32* %pcm_samples_to_encode, align 4
  br label %if.end34

if.else:                                          ; preds = %if.end
  %34 = load i32, i32* %pcm_samples_to_encode, align 4
  %35 = load i32, i32* %pcm_samples_per_frame, align 4
  %div31 = udiv i32 %34, %35
  store i32 %div31, i32* %frames, align 4
  %36 = load i32, i32* %frames, align 4
  %37 = load i32, i32* %pcm_samples_per_frame, align 4
  %mul32 = mul i32 %36, %37
  %38 = load i32, i32* %pcm_samples_to_encode, align 4
  %sub33 = sub i32 %38, %mul32
  store i32 %sub33, i32* %pcm_samples_to_encode, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end26
  %39 = load i32, i32* %pcm_samples_to_encode, align 4
  %add = add i32 %39, 576
  store i32 %add, i32* %pcm_samples_to_encode, align 4
  %40 = load i32, i32* %pcm_samples_per_frame, align 4
  %41 = load i32, i32* %pcm_samples_to_encode, align 4
  %42 = load i32, i32* %pcm_samples_per_frame, align 4
  %rem = urem i32 %41, %42
  %sub35 = sub i32 %40, %rem
  store i32 %sub35, i32* %end_padding, align 4
  %43 = load i32, i32* %end_padding, align 4
  %cmp36 = icmp ult i32 %43, 576
  br i1 %cmp36, label %if.then38, label %if.end40

if.then38:                                        ; preds = %if.end34
  %44 = load i32, i32* %pcm_samples_per_frame, align 4
  %45 = load i32, i32* %end_padding, align 4
  %add39 = add i32 %45, %44
  store i32 %add39, i32* %end_padding, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then38, %if.end34
  %46 = load i32, i32* %end_padding, align 4
  %47 = load i32, i32* %pcm_samples_to_encode, align 4
  %add41 = add i32 %47, %46
  store i32 %add41, i32* %pcm_samples_to_encode, align 4
  %48 = load i32, i32* %pcm_samples_to_encode, align 4
  %49 = load i32, i32* %pcm_samples_per_frame, align 4
  %div42 = udiv i32 %48, %49
  %50 = load i32, i32* %frames, align 4
  %add43 = add i32 %50, %div42
  store i32 %add43, i32* %frames, align 4
  %51 = load i32, i32* %frames, align 4
  store i32 %51, i32* %retval, align 4
  br label %return

if.end44:                                         ; preds = %if.then
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end45, %if.end40, %if.then25, %if.then19, %if.then5
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #2

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.ceil.f64(double) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_preset(%struct.lame_global_struct* %gfp, i32 %preset) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %preset.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %preset, i32* %preset.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %preset.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 38
  store i32 %1, i32* %preset1, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %4 = load i32, i32* %preset.addr, align 4
  %call2 = call i32 @apply_preset(%struct.lame_global_struct* %3, i32 %4, i32 1)
  store i32 %call2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

declare i32 @apply_preset(%struct.lame_global_struct*, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_asm_optimizations(%struct.lame_global_struct* %gfp, i32 %optim, i32 %mode) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %optim.addr = alloca i32, align 4
  %mode.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %optim, i32* %optim.addr, align 4
  store i32 %mode, i32* %mode.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %mode.addr, align 4
  %cmp = icmp eq i32 %1, 1
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  store i32 %cond, i32* %mode.addr, align 4
  %3 = load i32, i32* %optim.addr, align 4
  switch i32 %3, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb3
  ]

sw.bb:                                            ; preds = %if.then
  %4 = load i32, i32* %mode.addr, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 71
  %mmx = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations, i32 0, i32 0
  store i32 %4, i32* %mmx, align 4
  %6 = load i32, i32* %optim.addr, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %if.then
  %7 = load i32, i32* %mode.addr, align 4
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations2 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %8, i32 0, i32 71
  %amd3dnow = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations2, i32 0, i32 1
  store i32 %7, i32* %amd3dnow, align 4
  %9 = load i32, i32* %optim.addr, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %if.then
  %10 = load i32, i32* %mode.addr, align 4
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations4 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %11, i32 0, i32 71
  %sse = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations4, i32 0, i32 2
  store i32 %10, i32* %sse, align 4
  %12 = load i32, i32* %optim.addr, align 4
  store i32 %12, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %if.then
  %13 = load i32, i32* %optim.addr, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %sw.default, %sw.bb3, %sw.bb1, %sw.bb
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_set_write_id3tag_automatic(%struct.lame_global_struct* %gfp, i32 %v) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %v.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %v.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_id3tag_automatic = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 17
  store i32 %1, i32* %write_id3tag_automatic, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_write_id3tag_automatic(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_id3tag_automatic = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 17
  %2 = load i32, i32* %write_id3tag_automatic, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_set_tune(%struct.lame_global_struct* %gfp, float %val) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %val.addr = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float %val, float* %val.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %val.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tune_value_a = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 65
  store float %1, float* %tune_value_a, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tune = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 64
  store i32 1, i32* %tune, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_set_msfix(%struct.lame_global_struct* %gfp, double %msfix) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %msfix.addr = alloca double, align 8
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store double %msfix, double* %msfix.addr, align 8
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load double, double* %msfix.addr, align 8
  %conv = fptrunc double %1 to float
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %msfix1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 63
  store float %conv, float* %msfix1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @lame_get_msfix(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca float, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %msfix = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 63
  %2 = load float, float* %msfix, align 4
  store float %2, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load float, float* %retval, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_preset_expopts(%struct.lame_global_struct* %gfp, i32 %preset_expopts) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %preset_expopts.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %preset_expopts, i32* %preset_expopts.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %preset_expopts.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_set_preset_notune(%struct.lame_global_struct* %gfp, i32 %preset_notune) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %preset_notune.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %preset_notune, i32* %preset_notune.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32, i32* %preset_notune.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_maximum_number_of_samples(%struct.lame_global_struct* %gfp, i32 %buffer_size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer_size.addr = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %buffer_size, i32* %buffer_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %5 = load i32, i32* %buffer_size.addr, align 4
  %call4 = call i32 @calc_maximum_input_samples_for_buffer_size(%struct.lame_internal_flags* %4, i32 %5)
  store i32 %call4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then3
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @calc_maximum_input_samples_for_buffer_size(%struct.lame_internal_flags* %gfc, i32 %buffer_size) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer_size.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %pcm_samples_per_frame = alloca i32, align 4
  %frames_per_buffer = alloca i32, align 4
  %input_samples_per_buffer = alloca i32, align 4
  %kbps = alloca i32, align 4
  %pad = alloca i32, align 4
  %bpf = alloca i32, align 4
  %ratio = alloca double, align 8
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %buffer_size, i32* %buffer_size.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %1, i32 0, i32 15
  %2 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %2
  store i32 %mul, i32* %pcm_samples_per_frame, align 4
  store i32 0, i32* %frames_per_buffer, align 4
  store i32 0, i32* %input_samples_per_buffer, align 4
  store i32 320, i32* %kbps, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 12
  %4 = load i32, i32* %samplerate_out, align 4
  %cmp = icmp slt i32 %4, 16000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 64, i32* %kbps, align 4
  br label %if.end6

if.else:                                          ; preds = %entry
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 12
  %6 = load i32, i32* %samplerate_out2, align 4
  %cmp3 = icmp slt i32 %6, 32000
  br i1 %cmp3, label %if.then4, label %if.else5

if.then4:                                         ; preds = %if.else
  store i32 160, i32* %kbps, align 4
  br label %if.end

if.else5:                                         ; preds = %if.else
  store i32 320, i32* %kbps, align 4
  br label %if.end

if.end:                                           ; preds = %if.else5, %if.then4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 34
  %8 = load i32, i32* %free_format, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.then7, label %if.else8

if.then7:                                         ; preds = %if.end6
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 26
  %10 = load i32, i32* %avg_bitrate, align 4
  store i32 %10, i32* %kbps, align 4
  br label %if.end13

if.else8:                                         ; preds = %if.end6
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 22
  %12 = load i32, i32* %vbr, align 4
  %cmp9 = icmp eq i32 %12, 0
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.else8
  %13 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate11 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %13, i32 0, i32 26
  %14 = load i32, i32* %avg_bitrate11, align 4
  store i32 %14, i32* %kbps, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.else8
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.then7
  store i32 1, i32* %pad, align 4
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 0
  %16 = load i32, i32* %version, align 4
  %add = add nsw i32 %16, 1
  %mul14 = mul nsw i32 %add, 72000
  %17 = load i32, i32* %kbps, align 4
  %mul15 = mul nsw i32 %mul14, %17
  %18 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out16 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %18, i32 0, i32 12
  %19 = load i32, i32* %samplerate_out16, align 4
  %div = sdiv i32 %mul15, %19
  %add17 = add nsw i32 %div, 1
  store i32 %add17, i32* %bpf, align 4
  %20 = load i32, i32* %buffer_size.addr, align 4
  %21 = load i32, i32* %bpf, align 4
  %div18 = udiv i32 %20, %21
  store i32 %div18, i32* %frames_per_buffer, align 4
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %22, i32 0, i32 11
  %23 = load i32, i32* %samplerate_in, align 4
  %conv = sitofp i32 %23 to double
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out19 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 12
  %25 = load i32, i32* %samplerate_out19, align 4
  %conv20 = sitofp i32 %25 to double
  %div21 = fdiv double %conv, %conv20
  store double %div21, double* %ratio, align 8
  %26 = load i32, i32* %pcm_samples_per_frame, align 4
  %27 = load i32, i32* %frames_per_buffer, align 4
  %mul22 = mul nsw i32 %26, %27
  %conv23 = sitofp i32 %mul22 to double
  %28 = load double, double* %ratio, align 8
  %mul24 = fmul double %conv23, %28
  %conv25 = fptosi double %mul24 to i32
  store i32 %conv25, i32* %input_samples_per_buffer, align 4
  %29 = load i32, i32* %input_samples_per_buffer, align 4
  ret i32 %29
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
