; ModuleID = 'presets.c'
source_filename = "presets.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.abr_presets_t = type { i32, i32, i32, i32, float, float, float, float, float, float, float, float, i32 }
%struct.vbr_presets_t = type { i32, i32, i32, i32, float, float, float, float, float, float, float, float, i32, i32, float, float, float }
%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.2, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon.0], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon.0 = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.1, %struct.anon.1 }
%struct.anon.1 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.2 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.anon.3 = type { i32, i32, i32 }

@__const.apply_abr_preset.abr_switch_map = private unnamed_addr constant [17 x %struct.abr_presets_t] [%struct.abr_presets_t { i32 8, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -3.000000e+01, float 1.100000e+01, float 0x3F53A92A40000000, i32 1 }, %struct.abr_presets_t { i32 16, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -2.500000e+01, float 1.100000e+01, float 0x3F50624DE0000000, i32 1 }, %struct.abr_presets_t { i32 24, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -2.000000e+01, float 1.100000e+01, float 0x3F50624DE0000000, i32 1 }, %struct.abr_presets_t { i32 32, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -1.500000e+01, float 1.100000e+01, float 0x3F50624DE0000000, i32 1 }, %struct.abr_presets_t { i32 40, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -1.000000e+01, float 1.100000e+01, float 0x3F4D7DBF40000000, i32 1 }, %struct.abr_presets_t { i32 48, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -1.000000e+01, float 1.100000e+01, float 0x3F4D7DBF40000000, i32 1 }, %struct.abr_presets_t { i32 56, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -6.000000e+00, float 1.100000e+01, float 0x3F4A36E2E0000000, i32 1 }, %struct.abr_presets_t { i32 64, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float -2.000000e+00, float 1.100000e+01, float 0x3F4A36E2E0000000, i32 1 }, %struct.abr_presets_t { i32 80, i32 9, i32 9, i32 0, float 0.000000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float 0.000000e+00, float 8.000000e+00, float 0x3F46F00680000000, i32 1 }, %struct.abr_presets_t { i32 96, i32 9, i32 9, i32 0, float 2.500000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float 1.000000e+00, float 5.500000e+00, float 0x3F43A92A40000000, i32 1 }, %struct.abr_presets_t { i32 112, i32 9, i32 9, i32 0, float 2.250000e+00, float 0x401A666660000000, float 1.450000e+02, float 0x3FEE666660000000, float 0.000000e+00, float 2.000000e+00, float 4.500000e+00, float 0x3F40624DE0000000, i32 1 }, %struct.abr_presets_t { i32 128, i32 9, i32 9, i32 0, float 0x3FFF333340000000, float 0x40199999A0000000, float 1.400000e+02, float 0x3FEE666660000000, float 0.000000e+00, float 3.000000e+00, float 4.000000e+00, float 0x3F2A36E2E0000000, i32 1 }, %struct.abr_presets_t { i32 160, i32 9, i32 9, i32 1, float 0x3FFCA3D700000000, float 6.000000e+00, float 1.350000e+02, float 0x3FEE666660000000, float -2.000000e+00, float 5.000000e+00, float 3.500000e+00, float 0.000000e+00, i32 1 }, %struct.abr_presets_t { i32 192, i32 9, i32 9, i32 1, float 0x3FF7D70A40000000, float 0x4016666660000000, float 1.250000e+02, float 0x3FEF0A3D80000000, float -4.000000e+00, float 7.000000e+00, float 3.000000e+00, float 0.000000e+00, i32 0 }, %struct.abr_presets_t { i32 224, i32 9, i32 9, i32 1, float 1.250000e+00, float 0x4014CCCCC0000000, float 1.250000e+02, float 0x3FEF5C2900000000, float -6.000000e+00, float 9.000000e+00, float 2.000000e+00, float 0.000000e+00, i32 0 }, %struct.abr_presets_t { i32 256, i32 9, i32 9, i32 1, float 0x3FEF0A3D80000000, float 0x4014CCCCC0000000, float 1.250000e+02, float 1.000000e+00, float -8.000000e+00, float 1.000000e+01, float 1.000000e+00, float 0.000000e+00, i32 0 }, %struct.abr_presets_t { i32 320, i32 9, i32 9, i32 1, float 0x3FECCCCCC0000000, float 0x4014CCCCC0000000, float 1.250000e+02, float 1.000000e+00, float -1.000000e+01, float 1.200000e+01, float 0.000000e+00, float 0.000000e+00, i32 0 }], align 16
@vbr_mt_psy_switch_map = internal constant [11 x %struct.vbr_presets_t] [%struct.vbr_presets_t { i32 0, i32 9, i32 9, i32 0, float 0x4010CCCCC0000000, float 2.500000e+01, float 0xC01B333340000000, float 0xC01B333340000000, float 0x401C666660000000, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, i32 2, i32 31, float 1.000000e+00, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 1, i32 9, i32 9, i32 0, float 0x4010CCCCC0000000, float 2.500000e+01, float 0xC013333340000000, float 0xC013333340000000, float 0x40159999A0000000, float 0x3FF6666660000000, float -1.000000e+00, float 0.000000e+00, i32 2, i32 27, float 0x3FF1F3B640000000, float 5.000000e+00, float 9.800000e+01 }, %struct.vbr_presets_t { i32 2, i32 9, i32 9, i32 0, float 0x4010CCCCC0000000, float 2.500000e+01, float 0xC004CCCCC0000000, float 0xC004CCCCC0000000, float 0x400D9999A0000000, float 2.000000e+00, float -3.000000e+00, float 0.000000e+00, i32 2, i32 23, float 0x3FF49BA5E0000000, float 5.000000e+00, float 9.700000e+01 }, %struct.vbr_presets_t { i32 3, i32 9, i32 9, i32 1, float 0x4010CCCCC0000000, float 2.500000e+01, float 0xBFF99999A0000000, float 0xBFF99999A0000000, float 2.000000e+00, float 2.000000e+00, float -5.000000e+00, float 0.000000e+00, i32 2, i32 18, float 0x3FF7A9FBE0000000, float 5.000000e+00, float 9.600000e+01 }, %struct.vbr_presets_t { i32 4, i32 9, i32 9, i32 1, float 0x4010CCCCC0000000, float 2.500000e+01, float -0.000000e+00, float -0.000000e+00, float 0.000000e+00, float 2.000000e+00, float -8.000000e+00, float 0.000000e+00, i32 2, i32 12, float 0x3FFB2B0200000000, float 5.000000e+00, float 9.500000e+01 }, %struct.vbr_presets_t { i32 5, i32 9, i32 9, i32 1, float 0x4010CCCCC0000000, float 2.500000e+01, float 0x3FF4CCCCC0000000, float 0x3FF4CCCCC0000000, float -6.000000e+00, float 3.500000e+00, float -1.100000e+01, float 0.000000e+00, i32 2, i32 8, float 0x3FFF333340000000, float 5.000000e+00, float 0x40578CCCC0000000 }, %struct.vbr_presets_t { i32 6, i32 9, i32 9, i32 1, float 4.500000e+00, float 1.000000e+02, float 0x40019999A0000000, float 0x4002666660000000, float -1.200000e+01, float 6.000000e+00, float -1.400000e+01, float 0.000000e+00, i32 2, i32 4, float 0x4001E978E0000000, float 3.000000e+00, float 0x40577999A0000000 }, %struct.vbr_presets_t { i32 7, i32 9, i32 9, i32 1, float 0x4013333340000000, float 2.000000e+02, float 0x40059999A0000000, float 0x40059999A0000000, float -1.800000e+01, float 9.000000e+00, float -1.700000e+01, float 0.000000e+00, i32 2, i32 0, float 0x40048F5C20000000, float 1.000000e+00, float 0x4057666660000000 }, %struct.vbr_presets_t { i32 8, i32 9, i32 9, i32 1, float 0x4015333340000000, float 3.000000e+02, float 0x4006666660000000, float 0x4006666660000000, float -2.100000e+01, float 1.000000e+01, float -2.300000e+01, float 0x3F2A36E2E0000000, i32 0, i32 0, float 0x40079BA5E0000000, float 0.000000e+00, float 0x4057533340000000 }, %struct.vbr_presets_t { i32 9, i32 9, i32 9, i32 1, float 0x401A666660000000, float 3.000000e+02, float 0x4006666660000000, float 0x4006666660000000, float -2.300000e+01, float 1.100000e+01, float -2.500000e+01, float 0x3F43A92A40000000, i32 0, i32 0, float 0x400B1A9FC0000000, float 0.000000e+00, float 0x4057533340000000 }, %struct.vbr_presets_t { i32 10, i32 9, i32 9, i32 1, float 2.500000e+01, float 3.000000e+02, float 0x4006666660000000, float 0x4006666660000000, float -2.500000e+01, float 1.200000e+01, float -2.700000e+01, float 0x3F647AE140000000, i32 0, i32 0, float 3.500000e+00, float 0.000000e+00, float 0x4057533340000000 }], align 16
@vbr_old_switch_map = internal constant [11 x %struct.vbr_presets_t] [%struct.vbr_presets_t { i32 0, i32 9, i32 9, i32 0, float 0x4014CCCCC0000000, float 1.250000e+02, float 0xC010CCCCC0000000, float 0xC019333340000000, float 0x4013333340000000, float 1.000000e+00, float 0.000000e+00, float 0.000000e+00, i32 2, i32 21, float 0x3FEF0A3D80000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 1, i32 9, i32 9, i32 0, float 0x4015333340000000, float 1.250000e+02, float 0xC00CCCCCC0000000, float 0xC016666660000000, float 4.500000e+00, float 1.500000e+00, float 0.000000e+00, float 0.000000e+00, i32 2, i32 21, float 0x3FF59999A0000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 2, i32 9, i32 9, i32 0, float 0x4016666660000000, float 1.250000e+02, float 0xC0019999A0000000, float -3.500000e+00, float 0x4006666660000000, float 2.000000e+00, float 0.000000e+00, float 0.000000e+00, i32 2, i32 21, float 0x3FF7D70A40000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 3, i32 9, i32 9, i32 1, float 0x4017333340000000, float 1.300000e+02, float 0xBFFCCCCCC0000000, float 0xC006666660000000, float 0x4004CCCCC0000000, float 3.000000e+00, float -4.000000e+00, float 0.000000e+00, i32 2, i32 20, float 0x3FFA3D70A0000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 4, i32 9, i32 9, i32 1, float 6.000000e+00, float 1.350000e+02, float 0xBFE6666660000000, float 0xBFF19999A0000000, float 0x3FF19999A0000000, float 3.500000e+00, float -8.000000e+00, float 0.000000e+00, i32 2, i32 0, float 0x3FFCA3D700000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 5, i32 9, i32 9, i32 1, float 0x40199999A0000000, float 1.400000e+02, float 5.000000e-01, float 0x3FD99999A0000000, float -7.500000e+00, float 4.000000e+00, float -1.200000e+01, float 0x3F2A36E2E0000000, i32 0, i32 0, float 0x3FFF333340000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 6, i32 9, i32 9, i32 1, float 0x401A666660000000, float 1.450000e+02, float 0x3FE570A3E0000000, float 0x3FE4CCCCC0000000, float 0xC02D666660000000, float 6.500000e+00, float -1.900000e+01, float 0x3F3A36E2E0000000, i32 0, i32 0, float 0x4002666660000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 7, i32 9, i32 9, i32 1, float 0x401A666660000000, float 1.450000e+02, float 0x3FE99999A0000000, float 7.500000e-01, float 0xC033B33340000000, float 8.000000e+00, float -2.200000e+01, float 0x3F43A92A40000000, i32 0, i32 0, float 0x40059999A0000000, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 8, i32 9, i32 9, i32 1, float 0x401A666660000000, float 1.450000e+02, float 0x3FF3333340000000, float 0x3FF2666660000000, float -2.750000e+01, float 1.000000e+01, float -2.300000e+01, float 0x3F46F00680000000, i32 0, i32 0, float 0.000000e+00, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 9, i32 9, i32 9, i32 1, float 0x401A666660000000, float 1.450000e+02, float 0x3FF99999A0000000, float 0x3FF99999A0000000, float -3.600000e+01, float 1.100000e+01, float -2.500000e+01, float 0x3F4A36E2E0000000, i32 0, i32 0, float 0.000000e+00, float 5.000000e+00, float 1.000000e+02 }, %struct.vbr_presets_t { i32 10, i32 9, i32 9, i32 1, float 0x401A666660000000, float 1.450000e+02, float 2.000000e+00, float 2.000000e+00, float -3.600000e+01, float 1.200000e+01, float -2.500000e+01, float 0x3F4A36E2E0000000, i32 0, i32 0, float 0.000000e+00, float 5.000000e+00, float 1.000000e+02 }], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @apply_preset(%struct.lame_global_struct* %gfp, i32 %preset, i32 %enforce) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %preset.addr = alloca i32, align 4
  %enforce.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %preset, i32* %preset.addr, align 4
  store i32 %enforce, i32* %enforce.addr, align 4
  %0 = load i32, i32* %preset.addr, align 4
  switch i32 %0, label %sw.epilog [
    i32 1000, label %sw.bb
    i32 1006, label %sw.bb1
    i32 1007, label %sw.bb1
    i32 1001, label %sw.bb3
    i32 1004, label %sw.bb3
    i32 1002, label %sw.bb5
    i32 1005, label %sw.bb5
    i32 1003, label %sw.bb7
  ]

sw.bb:                                            ; preds = %entry
  store i32 470, i32* %preset.addr, align 4
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_set_VBR(%struct.lame_global_struct* %1, i32 4)
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry, %entry
  store i32 460, i32* %preset.addr, align 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call2 = call i32 @lame_set_VBR(%struct.lame_global_struct* %2, i32 4)
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry, %entry
  store i32 480, i32* %preset.addr, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call4 = call i32 @lame_set_VBR(%struct.lame_global_struct* %3, i32 4)
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry, %entry
  store i32 500, i32* %preset.addr, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call6 = call i32 @lame_set_VBR(%struct.lame_global_struct* %4, i32 4)
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  store i32 320, i32* %preset.addr, align 4
  %5 = load i32, i32* %preset.addr, align 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset8 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %6, i32 0, i32 38
  store i32 %5, i32* %preset8, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %8 = load i32, i32* %preset.addr, align 4
  %9 = load i32, i32* %enforce.addr, align 4
  %call9 = call i32 @apply_abr_preset(%struct.lame_global_struct* %7, i32 %8, i32 %9)
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call10 = call i32 @lame_set_VBR(%struct.lame_global_struct* %10, i32 0)
  %11 = load i32, i32* %preset.addr, align 4
  store i32 %11, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry, %sw.bb5, %sw.bb3, %sw.bb1, %sw.bb
  %12 = load i32, i32* %preset.addr, align 4
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset11 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %13, i32 0, i32 38
  store i32 %12, i32* %preset11, align 4
  %14 = load i32, i32* %preset.addr, align 4
  switch i32 %14, label %sw.default [
    i32 410, label %sw.bb12
    i32 420, label %sw.bb13
    i32 430, label %sw.bb14
    i32 440, label %sw.bb15
    i32 450, label %sw.bb16
    i32 460, label %sw.bb17
    i32 470, label %sw.bb18
    i32 480, label %sw.bb19
    i32 490, label %sw.bb20
    i32 500, label %sw.bb21
  ]

sw.bb12:                                          ; preds = %sw.epilog
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %16 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %15, i32 9, i32 %16)
  %17 = load i32, i32* %preset.addr, align 4
  store i32 %17, i32* %retval, align 4
  br label %return

sw.bb13:                                          ; preds = %sw.epilog
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %19 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %18, i32 8, i32 %19)
  %20 = load i32, i32* %preset.addr, align 4
  store i32 %20, i32* %retval, align 4
  br label %return

sw.bb14:                                          ; preds = %sw.epilog
  %21 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %22 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %21, i32 7, i32 %22)
  %23 = load i32, i32* %preset.addr, align 4
  store i32 %23, i32* %retval, align 4
  br label %return

sw.bb15:                                          ; preds = %sw.epilog
  %24 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %25 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %24, i32 6, i32 %25)
  %26 = load i32, i32* %preset.addr, align 4
  store i32 %26, i32* %retval, align 4
  br label %return

sw.bb16:                                          ; preds = %sw.epilog
  %27 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %28 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %27, i32 5, i32 %28)
  %29 = load i32, i32* %preset.addr, align 4
  store i32 %29, i32* %retval, align 4
  br label %return

sw.bb17:                                          ; preds = %sw.epilog
  %30 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %31 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %30, i32 4, i32 %31)
  %32 = load i32, i32* %preset.addr, align 4
  store i32 %32, i32* %retval, align 4
  br label %return

sw.bb18:                                          ; preds = %sw.epilog
  %33 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %34 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %33, i32 3, i32 %34)
  %35 = load i32, i32* %preset.addr, align 4
  store i32 %35, i32* %retval, align 4
  br label %return

sw.bb19:                                          ; preds = %sw.epilog
  %36 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %37 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %36, i32 2, i32 %37)
  %38 = load i32, i32* %preset.addr, align 4
  store i32 %38, i32* %retval, align 4
  br label %return

sw.bb20:                                          ; preds = %sw.epilog
  %39 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %40 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %39, i32 1, i32 %40)
  %41 = load i32, i32* %preset.addr, align 4
  store i32 %41, i32* %retval, align 4
  br label %return

sw.bb21:                                          ; preds = %sw.epilog
  %42 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %43 = load i32, i32* %enforce.addr, align 4
  call void @apply_vbr_preset(%struct.lame_global_struct* %42, i32 0, i32 %43)
  %44 = load i32, i32* %preset.addr, align 4
  store i32 %44, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %sw.epilog
  br label %sw.epilog22

sw.epilog22:                                      ; preds = %sw.default
  %45 = load i32, i32* %preset.addr, align 4
  %cmp = icmp sle i32 8, %45
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %sw.epilog22
  %46 = load i32, i32* %preset.addr, align 4
  %cmp23 = icmp sle i32 %46, 320
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %47 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %48 = load i32, i32* %preset.addr, align 4
  %49 = load i32, i32* %enforce.addr, align 4
  %call24 = call i32 @apply_abr_preset(%struct.lame_global_struct* %47, i32 %48, i32 %49)
  store i32 %call24, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %sw.epilog22
  %50 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset25 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %50, i32 0, i32 38
  store i32 0, i32* %preset25, align 4
  %51 = load i32, i32* %preset.addr, align 4
  store i32 %51, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then, %sw.bb21, %sw.bb20, %sw.bb19, %sw.bb18, %sw.bb17, %sw.bb16, %sw.bb15, %sw.bb14, %sw.bb13, %sw.bb12, %sw.bb7
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

declare i32 @lame_set_VBR(%struct.lame_global_struct*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @apply_abr_preset(%struct.lame_global_struct* %gfp, i32 %preset, i32 %enforce) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %preset.addr = alloca i32, align 4
  %enforce.addr = alloca i32, align 4
  %abr_switch_map = alloca [17 x %struct.abr_presets_t], align 16
  %r = alloca i32, align 4
  %actual_bitrate = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %preset, i32* %preset.addr, align 4
  store i32 %enforce, i32* %enforce.addr, align 4
  %0 = bitcast [17 x %struct.abr_presets_t]* %abr_switch_map to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %0, i8* align 16 bitcast ([17 x %struct.abr_presets_t]* @__const.apply_abr_preset.abr_switch_map to i8*), i32 884, i1 false)
  %1 = load i32, i32* %preset.addr, align 4
  store i32 %1, i32* %actual_bitrate, align 4
  %2 = load i32, i32* %preset.addr, align 4
  %conv = trunc i32 %2 to i16
  %call = call i32 @nearestBitrateFullIndex(i16 zeroext %conv)
  store i32 %call, i32* %r, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1 = call i32 @lame_set_VBR(%struct.lame_global_struct* %3, i32 3)
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %5 = load i32, i32* %actual_bitrate, align 4
  %call2 = call i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %4, i32 %5)
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call3 = call i32 @lame_get_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %7)
  %call4 = call i32 @min_int(i32 %call3, i32 320)
  %call5 = call i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %6, i32 %call4)
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call6 = call i32 @lame_get_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %9)
  %call7 = call i32 @max_int(i32 %call6, i32 8)
  %call8 = call i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %8, i32 %call7)
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call9 = call i32 @lame_get_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %11)
  %call10 = call i32 @lame_set_brate(%struct.lame_global_struct* %10, i32 %call9)
  %12 = load i32, i32* %r, align 4
  %arrayidx = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %12
  %safejoint = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx, i32 0, i32 3
  %13 = load i32, i32* %safejoint, align 4
  %cmp = icmp sgt i32 %13, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %14 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call12 = call i32 @lame_get_exp_nspsytune(%struct.lame_global_struct* %15)
  %or = or i32 %call12, 2
  %call13 = call i32 @lame_set_exp_nspsytune(%struct.lame_global_struct* %14, i32 %or)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load i32, i32* %r, align 4
  %arrayidx14 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %16
  %sfscale = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx14, i32 0, i32 12
  %17 = load i32, i32* %sfscale, align 4
  %cmp15 = icmp sgt i32 %17, 0
  br i1 %cmp15, label %if.then17, label %if.end19

if.then17:                                        ; preds = %if.end
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call18 = call i32 @lame_set_sfscale(%struct.lame_global_struct* %18, i32 1)
  br label %if.end19

if.end19:                                         ; preds = %if.then17, %if.end
  %19 = load i32, i32* %enforce.addr, align 4
  %tobool = icmp ne i32 %19, 0
  br i1 %tobool, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.end19
  %20 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %21 = load i32, i32* %r, align 4
  %arrayidx21 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %21
  %quant_comp = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx21, i32 0, i32 1
  %22 = load i32, i32* %quant_comp, align 4
  %call22 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %20, i32 %22)
  br label %if.end32

if.else:                                          ; preds = %if.end19
  %23 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call23 = call i32 @lame_get_quant_comp(%struct.lame_global_struct* %23)
  %sub = sub nsw i32 %call23, -1
  %conv24 = sitofp i32 %sub to double
  %24 = call double @llvm.fabs.f64(double %conv24)
  %cmp25 = fcmp ogt double %24, 0.000000e+00
  br i1 %cmp25, label %if.end31, label %if.then27

if.then27:                                        ; preds = %if.else
  %25 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %26 = load i32, i32* %r, align 4
  %arrayidx28 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %26
  %quant_comp29 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx28, i32 0, i32 1
  %27 = load i32, i32* %quant_comp29, align 4
  %call30 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %25, i32 %27)
  br label %if.end31

if.end31:                                         ; preds = %if.then27, %if.else
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then20
  %28 = load i32, i32* %enforce.addr, align 4
  %tobool33 = icmp ne i32 %28, 0
  br i1 %tobool33, label %if.then34, label %if.else37

if.then34:                                        ; preds = %if.end32
  %29 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %30 = load i32, i32* %r, align 4
  %arrayidx35 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %30
  %quant_comp_s = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx35, i32 0, i32 2
  %31 = load i32, i32* %quant_comp_s, align 4
  %call36 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %29, i32 %31)
  br label %if.end48

if.else37:                                        ; preds = %if.end32
  %32 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call38 = call i32 @lame_get_quant_comp_short(%struct.lame_global_struct* %32)
  %sub39 = sub nsw i32 %call38, -1
  %conv40 = sitofp i32 %sub39 to double
  %33 = call double @llvm.fabs.f64(double %conv40)
  %cmp41 = fcmp ogt double %33, 0.000000e+00
  br i1 %cmp41, label %if.end47, label %if.then43

if.then43:                                        ; preds = %if.else37
  %34 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %35 = load i32, i32* %r, align 4
  %arrayidx44 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %35
  %quant_comp_s45 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx44, i32 0, i32 2
  %36 = load i32, i32* %quant_comp_s45, align 4
  %call46 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %34, i32 %36)
  br label %if.end47

if.end47:                                         ; preds = %if.then43, %if.else37
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then34
  %37 = load i32, i32* %enforce.addr, align 4
  %tobool49 = icmp ne i32 %37, 0
  br i1 %tobool49, label %if.then50, label %if.else53

if.then50:                                        ; preds = %if.end48
  %38 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %39 = load i32, i32* %r, align 4
  %arrayidx51 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %39
  %nsmsfix = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx51, i32 0, i32 4
  %40 = load float, float* %nsmsfix, align 4
  %conv52 = fpext float %40 to double
  call void @lame_set_msfix(%struct.lame_global_struct* %38, double %conv52)
  br label %if.end64

if.else53:                                        ; preds = %if.end48
  %41 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call54 = call float @lame_get_msfix(%struct.lame_global_struct* %41)
  %sub55 = fsub float %call54, -1.000000e+00
  %conv56 = fpext float %sub55 to double
  %42 = call double @llvm.fabs.f64(double %conv56)
  %cmp57 = fcmp ogt double %42, 0.000000e+00
  br i1 %cmp57, label %if.end63, label %if.then59

if.then59:                                        ; preds = %if.else53
  %43 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %44 = load i32, i32* %r, align 4
  %arrayidx60 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %44
  %nsmsfix61 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx60, i32 0, i32 4
  %45 = load float, float* %nsmsfix61, align 4
  %conv62 = fpext float %45 to double
  call void @lame_set_msfix(%struct.lame_global_struct* %43, double %conv62)
  br label %if.end63

if.end63:                                         ; preds = %if.then59, %if.else53
  br label %if.end64

if.end64:                                         ; preds = %if.end63, %if.then50
  %46 = load i32, i32* %enforce.addr, align 4
  %tobool65 = icmp ne i32 %46, 0
  br i1 %tobool65, label %if.then66, label %if.else69

if.then66:                                        ; preds = %if.end64
  %47 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %48 = load i32, i32* %r, align 4
  %arrayidx67 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %48
  %st_lrm = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx67, i32 0, i32 5
  %49 = load float, float* %st_lrm, align 4
  %call68 = call i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %47, float %49)
  br label %if.end80

if.else69:                                        ; preds = %if.end64
  %50 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call70 = call float @lame_get_short_threshold_lrm(%struct.lame_global_struct* %50)
  %sub71 = fsub float %call70, -1.000000e+00
  %conv72 = fpext float %sub71 to double
  %51 = call double @llvm.fabs.f64(double %conv72)
  %cmp73 = fcmp ogt double %51, 0.000000e+00
  br i1 %cmp73, label %if.end79, label %if.then75

if.then75:                                        ; preds = %if.else69
  %52 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %53 = load i32, i32* %r, align 4
  %arrayidx76 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %53
  %st_lrm77 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx76, i32 0, i32 5
  %54 = load float, float* %st_lrm77, align 4
  %call78 = call i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %52, float %54)
  br label %if.end79

if.end79:                                         ; preds = %if.then75, %if.else69
  br label %if.end80

if.end80:                                         ; preds = %if.end79, %if.then66
  %55 = load i32, i32* %enforce.addr, align 4
  %tobool81 = icmp ne i32 %55, 0
  br i1 %tobool81, label %if.then82, label %if.else85

if.then82:                                        ; preds = %if.end80
  %56 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %57 = load i32, i32* %r, align 4
  %arrayidx83 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %57
  %st_s = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx83, i32 0, i32 6
  %58 = load float, float* %st_s, align 4
  %call84 = call i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %56, float %58)
  br label %if.end96

if.else85:                                        ; preds = %if.end80
  %59 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call86 = call float @lame_get_short_threshold_s(%struct.lame_global_struct* %59)
  %sub87 = fsub float %call86, -1.000000e+00
  %conv88 = fpext float %sub87 to double
  %60 = call double @llvm.fabs.f64(double %conv88)
  %cmp89 = fcmp ogt double %60, 0.000000e+00
  br i1 %cmp89, label %if.end95, label %if.then91

if.then91:                                        ; preds = %if.else85
  %61 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %62 = load i32, i32* %r, align 4
  %arrayidx92 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %62
  %st_s93 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx92, i32 0, i32 6
  %63 = load float, float* %st_s93, align 4
  %call94 = call i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %61, float %63)
  br label %if.end95

if.end95:                                         ; preds = %if.then91, %if.else85
  br label %if.end96

if.end96:                                         ; preds = %if.end95, %if.then82
  %64 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %65 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call97 = call float @lame_get_scale(%struct.lame_global_struct* %65)
  %66 = load i32, i32* %r, align 4
  %arrayidx98 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %66
  %scale = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx98, i32 0, i32 7
  %67 = load float, float* %scale, align 4
  %mul = fmul float %call97, %67
  %call99 = call i32 @lame_set_scale(%struct.lame_global_struct* %64, float %mul)
  %68 = load i32, i32* %enforce.addr, align 4
  %tobool100 = icmp ne i32 %68, 0
  br i1 %tobool100, label %if.then101, label %if.else104

if.then101:                                       ; preds = %if.end96
  %69 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %70 = load i32, i32* %r, align 4
  %arrayidx102 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %70
  %masking_adj = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx102, i32 0, i32 8
  %71 = load float, float* %masking_adj, align 4
  %call103 = call i32 @lame_set_maskingadjust(%struct.lame_global_struct* %69, float %71)
  br label %if.end115

if.else104:                                       ; preds = %if.end96
  %72 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call105 = call float @lame_get_maskingadjust(%struct.lame_global_struct* %72)
  %sub106 = fsub float %call105, 0.000000e+00
  %conv107 = fpext float %sub106 to double
  %73 = call double @llvm.fabs.f64(double %conv107)
  %cmp108 = fcmp ogt double %73, 0.000000e+00
  br i1 %cmp108, label %if.end114, label %if.then110

if.then110:                                       ; preds = %if.else104
  %74 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %75 = load i32, i32* %r, align 4
  %arrayidx111 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %75
  %masking_adj112 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx111, i32 0, i32 8
  %76 = load float, float* %masking_adj112, align 4
  %call113 = call i32 @lame_set_maskingadjust(%struct.lame_global_struct* %74, float %76)
  br label %if.end114

if.end114:                                        ; preds = %if.then110, %if.else104
  br label %if.end115

if.end115:                                        ; preds = %if.end114, %if.then101
  %77 = load i32, i32* %r, align 4
  %arrayidx116 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %77
  %masking_adj117 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx116, i32 0, i32 8
  %78 = load float, float* %masking_adj117, align 4
  %cmp118 = fcmp ogt float %78, 0.000000e+00
  br i1 %cmp118, label %if.then120, label %if.else144

if.then120:                                       ; preds = %if.end115
  %79 = load i32, i32* %enforce.addr, align 4
  %tobool121 = icmp ne i32 %79, 0
  br i1 %tobool121, label %if.then122, label %if.else129

if.then122:                                       ; preds = %if.then120
  %80 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %81 = load i32, i32* %r, align 4
  %arrayidx123 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %81
  %masking_adj124 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx123, i32 0, i32 8
  %82 = load float, float* %masking_adj124, align 4
  %conv125 = fpext float %82 to double
  %mul126 = fmul double %conv125, 9.000000e-01
  %conv127 = fptrunc double %mul126 to float
  %call128 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %80, float %conv127)
  br label %if.end143

if.else129:                                       ; preds = %if.then120
  %83 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call130 = call float @lame_get_maskingadjust_short(%struct.lame_global_struct* %83)
  %sub131 = fsub float %call130, 0.000000e+00
  %conv132 = fpext float %sub131 to double
  %84 = call double @llvm.fabs.f64(double %conv132)
  %cmp133 = fcmp ogt double %84, 0.000000e+00
  br i1 %cmp133, label %if.end142, label %if.then135

if.then135:                                       ; preds = %if.else129
  %85 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %86 = load i32, i32* %r, align 4
  %arrayidx136 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %86
  %masking_adj137 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx136, i32 0, i32 8
  %87 = load float, float* %masking_adj137, align 4
  %conv138 = fpext float %87 to double
  %mul139 = fmul double %conv138, 9.000000e-01
  %conv140 = fptrunc double %mul139 to float
  %call141 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %85, float %conv140)
  br label %if.end142

if.end142:                                        ; preds = %if.then135, %if.else129
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %if.then122
  br label %if.end168

if.else144:                                       ; preds = %if.end115
  %88 = load i32, i32* %enforce.addr, align 4
  %tobool145 = icmp ne i32 %88, 0
  br i1 %tobool145, label %if.then146, label %if.else153

if.then146:                                       ; preds = %if.else144
  %89 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %90 = load i32, i32* %r, align 4
  %arrayidx147 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %90
  %masking_adj148 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx147, i32 0, i32 8
  %91 = load float, float* %masking_adj148, align 4
  %conv149 = fpext float %91 to double
  %mul150 = fmul double %conv149, 1.100000e+00
  %conv151 = fptrunc double %mul150 to float
  %call152 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %89, float %conv151)
  br label %if.end167

if.else153:                                       ; preds = %if.else144
  %92 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call154 = call float @lame_get_maskingadjust_short(%struct.lame_global_struct* %92)
  %sub155 = fsub float %call154, 0.000000e+00
  %conv156 = fpext float %sub155 to double
  %93 = call double @llvm.fabs.f64(double %conv156)
  %cmp157 = fcmp ogt double %93, 0.000000e+00
  br i1 %cmp157, label %if.end166, label %if.then159

if.then159:                                       ; preds = %if.else153
  %94 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %95 = load i32, i32* %r, align 4
  %arrayidx160 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %95
  %masking_adj161 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx160, i32 0, i32 8
  %96 = load float, float* %masking_adj161, align 4
  %conv162 = fpext float %96 to double
  %mul163 = fmul double %conv162, 1.100000e+00
  %conv164 = fptrunc double %mul163 to float
  %call165 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %94, float %conv164)
  br label %if.end166

if.end166:                                        ; preds = %if.then159, %if.else153
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.then146
  br label %if.end168

if.end168:                                        ; preds = %if.end167, %if.end143
  %97 = load i32, i32* %enforce.addr, align 4
  %tobool169 = icmp ne i32 %97, 0
  br i1 %tobool169, label %if.then170, label %if.else173

if.then170:                                       ; preds = %if.end168
  %98 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %99 = load i32, i32* %r, align 4
  %arrayidx171 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %99
  %ath_lower = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx171, i32 0, i32 9
  %100 = load float, float* %ath_lower, align 4
  %call172 = call i32 @lame_set_ATHlower(%struct.lame_global_struct* %98, float %100)
  br label %if.end184

if.else173:                                       ; preds = %if.end168
  %101 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call174 = call float @lame_get_ATHlower(%struct.lame_global_struct* %101)
  %sub175 = fsub float %call174, 0.000000e+00
  %conv176 = fpext float %sub175 to double
  %102 = call double @llvm.fabs.f64(double %conv176)
  %cmp177 = fcmp ogt double %102, 0.000000e+00
  br i1 %cmp177, label %if.end183, label %if.then179

if.then179:                                       ; preds = %if.else173
  %103 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %104 = load i32, i32* %r, align 4
  %arrayidx180 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %104
  %ath_lower181 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx180, i32 0, i32 9
  %105 = load float, float* %ath_lower181, align 4
  %call182 = call i32 @lame_set_ATHlower(%struct.lame_global_struct* %103, float %105)
  br label %if.end183

if.end183:                                        ; preds = %if.then179, %if.else173
  br label %if.end184

if.end184:                                        ; preds = %if.end183, %if.then170
  %106 = load i32, i32* %enforce.addr, align 4
  %tobool185 = icmp ne i32 %106, 0
  br i1 %tobool185, label %if.then186, label %if.else189

if.then186:                                       ; preds = %if.end184
  %107 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %108 = load i32, i32* %r, align 4
  %arrayidx187 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %108
  %ath_curve = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx187, i32 0, i32 10
  %109 = load float, float* %ath_curve, align 4
  %call188 = call i32 @lame_set_ATHcurve(%struct.lame_global_struct* %107, float %109)
  br label %if.end200

if.else189:                                       ; preds = %if.end184
  %110 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call190 = call float @lame_get_ATHcurve(%struct.lame_global_struct* %110)
  %sub191 = fsub float %call190, -1.000000e+00
  %conv192 = fpext float %sub191 to double
  %111 = call double @llvm.fabs.f64(double %conv192)
  %cmp193 = fcmp ogt double %111, 0.000000e+00
  br i1 %cmp193, label %if.end199, label %if.then195

if.then195:                                       ; preds = %if.else189
  %112 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %113 = load i32, i32* %r, align 4
  %arrayidx196 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %113
  %ath_curve197 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx196, i32 0, i32 10
  %114 = load float, float* %ath_curve197, align 4
  %call198 = call i32 @lame_set_ATHcurve(%struct.lame_global_struct* %112, float %114)
  br label %if.end199

if.end199:                                        ; preds = %if.then195, %if.else189
  br label %if.end200

if.end200:                                        ; preds = %if.end199, %if.then186
  %115 = load i32, i32* %enforce.addr, align 4
  %tobool201 = icmp ne i32 %115, 0
  br i1 %tobool201, label %if.then202, label %if.else205

if.then202:                                       ; preds = %if.end200
  %116 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %117 = load i32, i32* %r, align 4
  %arrayidx203 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %117
  %interch = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx203, i32 0, i32 11
  %118 = load float, float* %interch, align 4
  %call204 = call i32 @lame_set_interChRatio(%struct.lame_global_struct* %116, float %118)
  br label %if.end216

if.else205:                                       ; preds = %if.end200
  %119 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call206 = call float @lame_get_interChRatio(%struct.lame_global_struct* %119)
  %sub207 = fsub float %call206, -1.000000e+00
  %conv208 = fpext float %sub207 to double
  %120 = call double @llvm.fabs.f64(double %conv208)
  %cmp209 = fcmp ogt double %120, 0.000000e+00
  br i1 %cmp209, label %if.end215, label %if.then211

if.then211:                                       ; preds = %if.else205
  %121 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %122 = load i32, i32* %r, align 4
  %arrayidx212 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %122
  %interch213 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx212, i32 0, i32 11
  %123 = load float, float* %interch213, align 4
  %call214 = call i32 @lame_set_interChRatio(%struct.lame_global_struct* %121, float %123)
  br label %if.end215

if.end215:                                        ; preds = %if.then211, %if.else205
  br label %if.end216

if.end216:                                        ; preds = %if.end215, %if.then202
  %124 = load i32, i32* %r, align 4
  %arrayidx217 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %124
  %abr_kbps = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx217, i32 0, i32 0
  %125 = load i32, i32* %abr_kbps, align 4
  %126 = load i32, i32* %r, align 4
  %arrayidx218 = getelementptr inbounds [17 x %struct.abr_presets_t], [17 x %struct.abr_presets_t]* %abr_switch_map, i32 0, i32 %126
  %abr_kbps219 = getelementptr inbounds %struct.abr_presets_t, %struct.abr_presets_t* %arrayidx218, i32 0, i32 0
  %127 = load i32, i32* %abr_kbps219, align 4
  %conv220 = sitofp i32 %127 to double
  %div = fdiv double %conv220, 3.200000e+02
  %mul221 = fmul double 5.000000e+00, %div
  %conv222 = fptrunc double %mul221 to float
  %128 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %128, i32 0, i32 70
  %129 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %129, i32 0, i32 5
  %minval = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 63
  store float %conv222, float* %minval, align 4
  %130 = load i32, i32* %preset.addr, align 4
  ret i32 %130
}

; Function Attrs: noinline nounwind optnone
define internal void @apply_vbr_preset(%struct.lame_global_struct* %gfp, i32 %a, i32 %enforce) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %a.addr = alloca i32, align 4
  %enforce.addr = alloca i32, align 4
  %vbr_preset = alloca %struct.vbr_presets_t*, align 4
  %x = alloca float, align 4
  %p = alloca %struct.vbr_presets_t, align 4
  %q = alloca %struct.vbr_presets_t, align 4
  %set = alloca %struct.vbr_presets_t*, align 4
  %nsp = alloca i32, align 4
  %val = alloca i32, align 4
  %sf21mod = alloca i32, align 4
  %x284 = alloca double, align 8
  %y = alloca double, align 8
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %enforce, i32* %enforce.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @lame_get_VBR(%struct.lame_global_struct* %0)
  %call1 = call %struct.vbr_presets_t* @get_vbr_preset(i32 %call)
  store %struct.vbr_presets_t* %call1, %struct.vbr_presets_t** %vbr_preset, align 4
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 40
  %2 = load float, float* %VBR_q_frac, align 4
  store float %2, float* %x, align 4
  %3 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %vbr_preset, align 4
  %4 = load i32, i32* %a.addr, align 4
  %arrayidx = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %3, i32 %4
  %5 = bitcast %struct.vbr_presets_t* %p to i8*
  %6 = bitcast %struct.vbr_presets_t* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 68, i1 false)
  %7 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %vbr_preset, align 4
  %8 = load i32, i32* %a.addr, align 4
  %add = add nsw i32 %8, 1
  %arrayidx2 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %7, i32 %add
  %9 = bitcast %struct.vbr_presets_t* %q to i8*
  %10 = bitcast %struct.vbr_presets_t* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 68, i1 false)
  store %struct.vbr_presets_t* %p, %struct.vbr_presets_t** %set, align 4
  %vbr_q = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 0
  %11 = load i32, i32* %vbr_q, align 4
  %quant_comp = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 1
  %12 = load i32, i32* %quant_comp, align 4
  %quant_comp_s = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 2
  %13 = load i32, i32* %quant_comp_s, align 4
  %expY = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 3
  %14 = load i32, i32* %expY, align 4
  %st_lrm = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 4
  %15 = load float, float* %st_lrm, align 4
  %16 = load float, float* %x, align 4
  %st_lrm3 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 4
  %17 = load float, float* %st_lrm3, align 4
  %st_lrm4 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 4
  %18 = load float, float* %st_lrm4, align 4
  %sub = fsub float %17, %18
  %mul = fmul float %16, %sub
  %add5 = fadd float %15, %mul
  %st_lrm6 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 4
  store float %add5, float* %st_lrm6, align 4
  %st_s = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 5
  %19 = load float, float* %st_s, align 4
  %20 = load float, float* %x, align 4
  %st_s7 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 5
  %21 = load float, float* %st_s7, align 4
  %st_s8 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 5
  %22 = load float, float* %st_s8, align 4
  %sub9 = fsub float %21, %22
  %mul10 = fmul float %20, %sub9
  %add11 = fadd float %19, %mul10
  %st_s12 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 5
  store float %add11, float* %st_s12, align 4
  %masking_adj = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 6
  %23 = load float, float* %masking_adj, align 4
  %24 = load float, float* %x, align 4
  %masking_adj13 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 6
  %25 = load float, float* %masking_adj13, align 4
  %masking_adj14 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 6
  %26 = load float, float* %masking_adj14, align 4
  %sub15 = fsub float %25, %26
  %mul16 = fmul float %24, %sub15
  %add17 = fadd float %23, %mul16
  %masking_adj18 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 6
  store float %add17, float* %masking_adj18, align 4
  %masking_adj_short = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 7
  %27 = load float, float* %masking_adj_short, align 4
  %28 = load float, float* %x, align 4
  %masking_adj_short19 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 7
  %29 = load float, float* %masking_adj_short19, align 4
  %masking_adj_short20 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 7
  %30 = load float, float* %masking_adj_short20, align 4
  %sub21 = fsub float %29, %30
  %mul22 = fmul float %28, %sub21
  %add23 = fadd float %27, %mul22
  %masking_adj_short24 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 7
  store float %add23, float* %masking_adj_short24, align 4
  %ath_lower = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 8
  %31 = load float, float* %ath_lower, align 4
  %32 = load float, float* %x, align 4
  %ath_lower25 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 8
  %33 = load float, float* %ath_lower25, align 4
  %ath_lower26 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 8
  %34 = load float, float* %ath_lower26, align 4
  %sub27 = fsub float %33, %34
  %mul28 = fmul float %32, %sub27
  %add29 = fadd float %31, %mul28
  %ath_lower30 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 8
  store float %add29, float* %ath_lower30, align 4
  %ath_curve = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 9
  %35 = load float, float* %ath_curve, align 4
  %36 = load float, float* %x, align 4
  %ath_curve31 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 9
  %37 = load float, float* %ath_curve31, align 4
  %ath_curve32 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 9
  %38 = load float, float* %ath_curve32, align 4
  %sub33 = fsub float %37, %38
  %mul34 = fmul float %36, %sub33
  %add35 = fadd float %35, %mul34
  %ath_curve36 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 9
  store float %add35, float* %ath_curve36, align 4
  %ath_sensitivity = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 10
  %39 = load float, float* %ath_sensitivity, align 4
  %40 = load float, float* %x, align 4
  %ath_sensitivity37 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 10
  %41 = load float, float* %ath_sensitivity37, align 4
  %ath_sensitivity38 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 10
  %42 = load float, float* %ath_sensitivity38, align 4
  %sub39 = fsub float %41, %42
  %mul40 = fmul float %40, %sub39
  %add41 = fadd float %39, %mul40
  %ath_sensitivity42 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 10
  store float %add41, float* %ath_sensitivity42, align 4
  %interch = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 11
  %43 = load float, float* %interch, align 4
  %44 = load float, float* %x, align 4
  %interch43 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 11
  %45 = load float, float* %interch43, align 4
  %interch44 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 11
  %46 = load float, float* %interch44, align 4
  %sub45 = fsub float %45, %46
  %mul46 = fmul float %44, %sub45
  %add47 = fadd float %43, %mul46
  %interch48 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 11
  store float %add47, float* %interch48, align 4
  %safejoint = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 12
  %47 = load i32, i32* %safejoint, align 4
  %sfb21mod = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 13
  %48 = load i32, i32* %sfb21mod, align 4
  %conv = sitofp i32 %48 to float
  %49 = load float, float* %x, align 4
  %sfb21mod49 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 13
  %50 = load i32, i32* %sfb21mod49, align 4
  %sfb21mod50 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 13
  %51 = load i32, i32* %sfb21mod50, align 4
  %sub51 = sub nsw i32 %50, %51
  %conv52 = sitofp i32 %sub51 to float
  %mul53 = fmul float %49, %conv52
  %add54 = fadd float %conv, %mul53
  %conv55 = fptosi float %add54 to i32
  %sfb21mod56 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 13
  store i32 %conv55, i32* %sfb21mod56, align 4
  %msfix = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 14
  %52 = load float, float* %msfix, align 4
  %53 = load float, float* %x, align 4
  %msfix57 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 14
  %54 = load float, float* %msfix57, align 4
  %msfix58 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 14
  %55 = load float, float* %msfix58, align 4
  %sub59 = fsub float %54, %55
  %mul60 = fmul float %53, %sub59
  %add61 = fadd float %52, %mul60
  %msfix62 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 14
  store float %add61, float* %msfix62, align 4
  %minval = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 15
  %56 = load float, float* %minval, align 4
  %57 = load float, float* %x, align 4
  %minval63 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 15
  %58 = load float, float* %minval63, align 4
  %minval64 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 15
  %59 = load float, float* %minval64, align 4
  %sub65 = fsub float %58, %59
  %mul66 = fmul float %57, %sub65
  %add67 = fadd float %56, %mul66
  %minval68 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 15
  store float %add67, float* %minval68, align 4
  %ath_fixpoint = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 16
  %60 = load float, float* %ath_fixpoint, align 4
  %61 = load float, float* %x, align 4
  %ath_fixpoint69 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %q, i32 0, i32 16
  %62 = load float, float* %ath_fixpoint69, align 4
  %ath_fixpoint70 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 16
  %63 = load float, float* %ath_fixpoint70, align 4
  %sub71 = fsub float %62, %63
  %mul72 = fmul float %61, %sub71
  %add73 = fadd float %60, %mul72
  %ath_fixpoint74 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %p, i32 0, i32 16
  store float %add73, float* %ath_fixpoint74, align 4
  %64 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %65 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %vbr_q75 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %65, i32 0, i32 0
  %66 = load i32, i32* %vbr_q75, align 4
  %call76 = call i32 @lame_set_VBR_q(%struct.lame_global_struct* %64, i32 %66)
  %67 = load i32, i32* %enforce.addr, align 4
  %tobool = icmp ne i32 %67, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %68 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %69 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %quant_comp77 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %69, i32 0, i32 1
  %70 = load i32, i32* %quant_comp77, align 4
  %call78 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %68, i32 %70)
  br label %if.end86

if.else:                                          ; preds = %entry
  %71 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call79 = call i32 @lame_get_quant_comp(%struct.lame_global_struct* %71)
  %sub80 = sub nsw i32 %call79, -1
  %conv81 = sitofp i32 %sub80 to double
  %72 = call double @llvm.fabs.f64(double %conv81)
  %cmp = fcmp ogt double %72, 0.000000e+00
  br i1 %cmp, label %if.end, label %if.then83

if.then83:                                        ; preds = %if.else
  %73 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %74 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %quant_comp84 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %74, i32 0, i32 1
  %75 = load i32, i32* %quant_comp84, align 4
  %call85 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %73, i32 %75)
  br label %if.end

if.end:                                           ; preds = %if.then83, %if.else
  br label %if.end86

if.end86:                                         ; preds = %if.end, %if.then
  %76 = load i32, i32* %enforce.addr, align 4
  %tobool87 = icmp ne i32 %76, 0
  br i1 %tobool87, label %if.then88, label %if.else91

if.then88:                                        ; preds = %if.end86
  %77 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %78 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %quant_comp_s89 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %78, i32 0, i32 2
  %79 = load i32, i32* %quant_comp_s89, align 4
  %call90 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %77, i32 %79)
  br label %if.end101

if.else91:                                        ; preds = %if.end86
  %80 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call92 = call i32 @lame_get_quant_comp_short(%struct.lame_global_struct* %80)
  %sub93 = sub nsw i32 %call92, -1
  %conv94 = sitofp i32 %sub93 to double
  %81 = call double @llvm.fabs.f64(double %conv94)
  %cmp95 = fcmp ogt double %81, 0.000000e+00
  br i1 %cmp95, label %if.end100, label %if.then97

if.then97:                                        ; preds = %if.else91
  %82 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %83 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %quant_comp_s98 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %83, i32 0, i32 2
  %84 = load i32, i32* %quant_comp_s98, align 4
  %call99 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %82, i32 %84)
  br label %if.end100

if.end100:                                        ; preds = %if.then97, %if.else91
  br label %if.end101

if.end101:                                        ; preds = %if.end100, %if.then88
  %85 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %expY102 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %85, i32 0, i32 3
  %86 = load i32, i32* %expY102, align 4
  %tobool103 = icmp ne i32 %86, 0
  br i1 %tobool103, label %if.then104, label %if.end107

if.then104:                                       ; preds = %if.end101
  %87 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %88 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %expY105 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %88, i32 0, i32 3
  %89 = load i32, i32* %expY105, align 4
  %call106 = call i32 @lame_set_experimentalY(%struct.lame_global_struct* %87, i32 %89)
  br label %if.end107

if.end107:                                        ; preds = %if.then104, %if.end101
  %90 = load i32, i32* %enforce.addr, align 4
  %tobool108 = icmp ne i32 %90, 0
  br i1 %tobool108, label %if.then109, label %if.else112

if.then109:                                       ; preds = %if.end107
  %91 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %92 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %st_lrm110 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %92, i32 0, i32 4
  %93 = load float, float* %st_lrm110, align 4
  %call111 = call i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %91, float %93)
  br label %if.end122

if.else112:                                       ; preds = %if.end107
  %94 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call113 = call float @lame_get_short_threshold_lrm(%struct.lame_global_struct* %94)
  %sub114 = fsub float %call113, -1.000000e+00
  %conv115 = fpext float %sub114 to double
  %95 = call double @llvm.fabs.f64(double %conv115)
  %cmp116 = fcmp ogt double %95, 0.000000e+00
  br i1 %cmp116, label %if.end121, label %if.then118

if.then118:                                       ; preds = %if.else112
  %96 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %97 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %st_lrm119 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %97, i32 0, i32 4
  %98 = load float, float* %st_lrm119, align 4
  %call120 = call i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct* %96, float %98)
  br label %if.end121

if.end121:                                        ; preds = %if.then118, %if.else112
  br label %if.end122

if.end122:                                        ; preds = %if.end121, %if.then109
  %99 = load i32, i32* %enforce.addr, align 4
  %tobool123 = icmp ne i32 %99, 0
  br i1 %tobool123, label %if.then124, label %if.else127

if.then124:                                       ; preds = %if.end122
  %100 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %101 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %st_s125 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %101, i32 0, i32 5
  %102 = load float, float* %st_s125, align 4
  %call126 = call i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %100, float %102)
  br label %if.end137

if.else127:                                       ; preds = %if.end122
  %103 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call128 = call float @lame_get_short_threshold_s(%struct.lame_global_struct* %103)
  %sub129 = fsub float %call128, -1.000000e+00
  %conv130 = fpext float %sub129 to double
  %104 = call double @llvm.fabs.f64(double %conv130)
  %cmp131 = fcmp ogt double %104, 0.000000e+00
  br i1 %cmp131, label %if.end136, label %if.then133

if.then133:                                       ; preds = %if.else127
  %105 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %106 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %st_s134 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %106, i32 0, i32 5
  %107 = load float, float* %st_s134, align 4
  %call135 = call i32 @lame_set_short_threshold_s(%struct.lame_global_struct* %105, float %107)
  br label %if.end136

if.end136:                                        ; preds = %if.then133, %if.else127
  br label %if.end137

if.end137:                                        ; preds = %if.end136, %if.then124
  %108 = load i32, i32* %enforce.addr, align 4
  %tobool138 = icmp ne i32 %108, 0
  br i1 %tobool138, label %if.then139, label %if.else142

if.then139:                                       ; preds = %if.end137
  %109 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %110 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %masking_adj140 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %110, i32 0, i32 6
  %111 = load float, float* %masking_adj140, align 4
  %call141 = call i32 @lame_set_maskingadjust(%struct.lame_global_struct* %109, float %111)
  br label %if.end152

if.else142:                                       ; preds = %if.end137
  %112 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call143 = call float @lame_get_maskingadjust(%struct.lame_global_struct* %112)
  %sub144 = fsub float %call143, 0.000000e+00
  %conv145 = fpext float %sub144 to double
  %113 = call double @llvm.fabs.f64(double %conv145)
  %cmp146 = fcmp ogt double %113, 0.000000e+00
  br i1 %cmp146, label %if.end151, label %if.then148

if.then148:                                       ; preds = %if.else142
  %114 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %115 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %masking_adj149 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %115, i32 0, i32 6
  %116 = load float, float* %masking_adj149, align 4
  %call150 = call i32 @lame_set_maskingadjust(%struct.lame_global_struct* %114, float %116)
  br label %if.end151

if.end151:                                        ; preds = %if.then148, %if.else142
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.then139
  %117 = load i32, i32* %enforce.addr, align 4
  %tobool153 = icmp ne i32 %117, 0
  br i1 %tobool153, label %if.then154, label %if.else157

if.then154:                                       ; preds = %if.end152
  %118 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %119 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %masking_adj_short155 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %119, i32 0, i32 7
  %120 = load float, float* %masking_adj_short155, align 4
  %call156 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %118, float %120)
  br label %if.end167

if.else157:                                       ; preds = %if.end152
  %121 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call158 = call float @lame_get_maskingadjust_short(%struct.lame_global_struct* %121)
  %sub159 = fsub float %call158, 0.000000e+00
  %conv160 = fpext float %sub159 to double
  %122 = call double @llvm.fabs.f64(double %conv160)
  %cmp161 = fcmp ogt double %122, 0.000000e+00
  br i1 %cmp161, label %if.end166, label %if.then163

if.then163:                                       ; preds = %if.else157
  %123 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %124 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %masking_adj_short164 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %124, i32 0, i32 7
  %125 = load float, float* %masking_adj_short164, align 4
  %call165 = call i32 @lame_set_maskingadjust_short(%struct.lame_global_struct* %123, float %125)
  br label %if.end166

if.end166:                                        ; preds = %if.then163, %if.else157
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.then154
  %126 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call168 = call i32 @lame_get_VBR(%struct.lame_global_struct* %126)
  %cmp169 = icmp eq i32 %call168, 1
  br i1 %cmp169, label %if.then174, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end167
  %127 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call171 = call i32 @lame_get_VBR(%struct.lame_global_struct* %127)
  %cmp172 = icmp eq i32 %call171, 4
  br i1 %cmp172, label %if.then174, label %if.end176

if.then174:                                       ; preds = %lor.lhs.false, %if.end167
  %128 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call175 = call i32 @lame_set_ATHtype(%struct.lame_global_struct* %128, i32 5)
  br label %if.end176

if.end176:                                        ; preds = %if.then174, %lor.lhs.false
  %129 = load i32, i32* %enforce.addr, align 4
  %tobool177 = icmp ne i32 %129, 0
  br i1 %tobool177, label %if.then178, label %if.else181

if.then178:                                       ; preds = %if.end176
  %130 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %131 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_lower179 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %131, i32 0, i32 8
  %132 = load float, float* %ath_lower179, align 4
  %call180 = call i32 @lame_set_ATHlower(%struct.lame_global_struct* %130, float %132)
  br label %if.end191

if.else181:                                       ; preds = %if.end176
  %133 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call182 = call float @lame_get_ATHlower(%struct.lame_global_struct* %133)
  %sub183 = fsub float %call182, 0.000000e+00
  %conv184 = fpext float %sub183 to double
  %134 = call double @llvm.fabs.f64(double %conv184)
  %cmp185 = fcmp ogt double %134, 0.000000e+00
  br i1 %cmp185, label %if.end190, label %if.then187

if.then187:                                       ; preds = %if.else181
  %135 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %136 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_lower188 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %136, i32 0, i32 8
  %137 = load float, float* %ath_lower188, align 4
  %call189 = call i32 @lame_set_ATHlower(%struct.lame_global_struct* %135, float %137)
  br label %if.end190

if.end190:                                        ; preds = %if.then187, %if.else181
  br label %if.end191

if.end191:                                        ; preds = %if.end190, %if.then178
  %138 = load i32, i32* %enforce.addr, align 4
  %tobool192 = icmp ne i32 %138, 0
  br i1 %tobool192, label %if.then193, label %if.else196

if.then193:                                       ; preds = %if.end191
  %139 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %140 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_curve194 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %140, i32 0, i32 9
  %141 = load float, float* %ath_curve194, align 4
  %call195 = call i32 @lame_set_ATHcurve(%struct.lame_global_struct* %139, float %141)
  br label %if.end206

if.else196:                                       ; preds = %if.end191
  %142 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call197 = call float @lame_get_ATHcurve(%struct.lame_global_struct* %142)
  %sub198 = fsub float %call197, -1.000000e+00
  %conv199 = fpext float %sub198 to double
  %143 = call double @llvm.fabs.f64(double %conv199)
  %cmp200 = fcmp ogt double %143, 0.000000e+00
  br i1 %cmp200, label %if.end205, label %if.then202

if.then202:                                       ; preds = %if.else196
  %144 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %145 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_curve203 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %145, i32 0, i32 9
  %146 = load float, float* %ath_curve203, align 4
  %call204 = call i32 @lame_set_ATHcurve(%struct.lame_global_struct* %144, float %146)
  br label %if.end205

if.end205:                                        ; preds = %if.then202, %if.else196
  br label %if.end206

if.end206:                                        ; preds = %if.end205, %if.then193
  %147 = load i32, i32* %enforce.addr, align 4
  %tobool207 = icmp ne i32 %147, 0
  br i1 %tobool207, label %if.then208, label %if.else211

if.then208:                                       ; preds = %if.end206
  %148 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %149 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_sensitivity209 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %149, i32 0, i32 10
  %150 = load float, float* %ath_sensitivity209, align 4
  %call210 = call i32 @lame_set_athaa_sensitivity(%struct.lame_global_struct* %148, float %150)
  br label %if.end221

if.else211:                                       ; preds = %if.end206
  %151 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call212 = call float @lame_get_athaa_sensitivity(%struct.lame_global_struct* %151)
  %sub213 = fsub float %call212, 0.000000e+00
  %conv214 = fpext float %sub213 to double
  %152 = call double @llvm.fabs.f64(double %conv214)
  %cmp215 = fcmp ogt double %152, 0.000000e+00
  br i1 %cmp215, label %if.end220, label %if.then217

if.then217:                                       ; preds = %if.else211
  %153 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %154 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_sensitivity218 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %154, i32 0, i32 10
  %155 = load float, float* %ath_sensitivity218, align 4
  %call219 = call i32 @lame_set_athaa_sensitivity(%struct.lame_global_struct* %153, float %155)
  br label %if.end220

if.end220:                                        ; preds = %if.then217, %if.else211
  br label %if.end221

if.end221:                                        ; preds = %if.end220, %if.then208
  %156 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %interch222 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %156, i32 0, i32 11
  %157 = load float, float* %interch222, align 4
  %cmp223 = fcmp ogt float %157, 0.000000e+00
  br i1 %cmp223, label %if.then225, label %if.end241

if.then225:                                       ; preds = %if.end221
  %158 = load i32, i32* %enforce.addr, align 4
  %tobool226 = icmp ne i32 %158, 0
  br i1 %tobool226, label %if.then227, label %if.else230

if.then227:                                       ; preds = %if.then225
  %159 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %160 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %interch228 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %160, i32 0, i32 11
  %161 = load float, float* %interch228, align 4
  %call229 = call i32 @lame_set_interChRatio(%struct.lame_global_struct* %159, float %161)
  br label %if.end240

if.else230:                                       ; preds = %if.then225
  %162 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call231 = call float @lame_get_interChRatio(%struct.lame_global_struct* %162)
  %sub232 = fsub float %call231, -1.000000e+00
  %conv233 = fpext float %sub232 to double
  %163 = call double @llvm.fabs.f64(double %conv233)
  %cmp234 = fcmp ogt double %163, 0.000000e+00
  br i1 %cmp234, label %if.end239, label %if.then236

if.then236:                                       ; preds = %if.else230
  %164 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %165 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %interch237 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %165, i32 0, i32 11
  %166 = load float, float* %interch237, align 4
  %call238 = call i32 @lame_set_interChRatio(%struct.lame_global_struct* %164, float %166)
  br label %if.end239

if.end239:                                        ; preds = %if.then236, %if.else230
  br label %if.end240

if.end240:                                        ; preds = %if.end239, %if.then227
  br label %if.end241

if.end241:                                        ; preds = %if.end240, %if.end221
  %167 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %safejoint242 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %167, i32 0, i32 12
  %168 = load i32, i32* %safejoint242, align 4
  %cmp243 = icmp sgt i32 %168, 0
  br i1 %cmp243, label %if.then245, label %if.end248

if.then245:                                       ; preds = %if.end241
  %169 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %170 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call246 = call i32 @lame_get_exp_nspsytune(%struct.lame_global_struct* %170)
  %or = or i32 %call246, 2
  %call247 = call i32 @lame_set_exp_nspsytune(%struct.lame_global_struct* %169, i32 %or)
  br label %if.end248

if.end248:                                        ; preds = %if.then245, %if.end241
  %171 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %sfb21mod249 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %171, i32 0, i32 13
  %172 = load i32, i32* %sfb21mod249, align 4
  %cmp250 = icmp sgt i32 %172, 0
  br i1 %cmp250, label %if.then252, label %if.end261

if.then252:                                       ; preds = %if.end248
  %173 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call253 = call i32 @lame_get_exp_nspsytune(%struct.lame_global_struct* %173)
  store i32 %call253, i32* %nsp, align 4
  %174 = load i32, i32* %nsp, align 4
  %shr = ashr i32 %174, 20
  %and = and i32 %shr, 63
  store i32 %and, i32* %val, align 4
  %175 = load i32, i32* %val, align 4
  %cmp254 = icmp eq i32 %175, 0
  br i1 %cmp254, label %if.then256, label %if.end260

if.then256:                                       ; preds = %if.then252
  %176 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %sfb21mod257 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %176, i32 0, i32 13
  %177 = load i32, i32* %sfb21mod257, align 4
  %shl = shl i32 %177, 20
  %178 = load i32, i32* %nsp, align 4
  %or258 = or i32 %shl, %178
  store i32 %or258, i32* %sf21mod, align 4
  %179 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %180 = load i32, i32* %sf21mod, align 4
  %call259 = call i32 @lame_set_exp_nspsytune(%struct.lame_global_struct* %179, i32 %180)
  br label %if.end260

if.end260:                                        ; preds = %if.then256, %if.then252
  br label %if.end261

if.end261:                                        ; preds = %if.end260, %if.end248
  %181 = load i32, i32* %enforce.addr, align 4
  %tobool262 = icmp ne i32 %181, 0
  br i1 %tobool262, label %if.then263, label %if.else266

if.then263:                                       ; preds = %if.end261
  %182 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %183 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %msfix264 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %183, i32 0, i32 14
  %184 = load float, float* %msfix264, align 4
  %conv265 = fpext float %184 to double
  call void @lame_set_msfix(%struct.lame_global_struct* %182, double %conv265)
  br label %if.end276

if.else266:                                       ; preds = %if.end261
  %185 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call267 = call float @lame_get_msfix(%struct.lame_global_struct* %185)
  %sub268 = fsub float %call267, -1.000000e+00
  %conv269 = fpext float %sub268 to double
  %186 = call double @llvm.fabs.f64(double %conv269)
  %cmp270 = fcmp ogt double %186, 0.000000e+00
  br i1 %cmp270, label %if.end275, label %if.then272

if.then272:                                       ; preds = %if.else266
  %187 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %188 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %msfix273 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %188, i32 0, i32 14
  %189 = load float, float* %msfix273, align 4
  %conv274 = fpext float %189 to double
  call void @lame_set_msfix(%struct.lame_global_struct* %187, double %conv274)
  br label %if.end275

if.end275:                                        ; preds = %if.then272, %if.else266
  br label %if.end276

if.end276:                                        ; preds = %if.end275, %if.then263
  %190 = load i32, i32* %enforce.addr, align 4
  %cmp277 = icmp eq i32 %190, 0
  br i1 %cmp277, label %if.then279, label %if.end281

if.then279:                                       ; preds = %if.end276
  %191 = load i32, i32* %a.addr, align 4
  %192 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %192, i32 0, i32 41
  store i32 %191, i32* %VBR_q, align 4
  %193 = load float, float* %x, align 4
  %194 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac280 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %194, i32 0, i32 40
  store float %193, float* %VBR_q_frac280, align 4
  br label %if.end281

if.end281:                                        ; preds = %if.then279, %if.end276
  %195 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %minval282 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %195, i32 0, i32 15
  %196 = load float, float* %minval282, align 4
  %197 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %197, i32 0, i32 70
  %198 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %198, i32 0, i32 5
  %minval283 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 63
  store float %196, float* %minval283, align 4
  %199 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %199, i32 0, i32 5
  %200 = load float, float* %scale, align 4
  %conv285 = fpext float %200 to double
  %201 = call double @llvm.fabs.f64(double %conv285)
  store double %201, double* %x284, align 8
  %202 = load double, double* %x284, align 8
  %cmp286 = fcmp ogt double %202, 0.000000e+00
  br i1 %cmp286, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end281
  %203 = load double, double* %x284, align 8
  %204 = call double @llvm.log10.f64(double %203)
  %mul288 = fmul double 1.000000e+01, %204
  br label %cond.end

cond.false:                                       ; preds = %if.end281
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %mul288, %cond.true ], [ 0.000000e+00, %cond.false ]
  store double %cond, double* %y, align 8
  %205 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %set, align 4
  %ath_fixpoint289 = getelementptr inbounds %struct.vbr_presets_t, %struct.vbr_presets_t* %205, i32 0, i32 16
  %206 = load float, float* %ath_fixpoint289, align 4
  %conv290 = fpext float %206 to double
  %207 = load double, double* %y, align 8
  %sub291 = fsub double %conv290, %207
  %conv292 = fptrunc double %sub291 to float
  %208 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags293 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %208, i32 0, i32 70
  %209 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags293, align 4
  %cfg294 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %209, i32 0, i32 5
  %ATHfixpoint = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg294, i32 0, i32 52
  store float %conv292, float* %ATHfixpoint, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare i32 @nearestBitrateFullIndex(i16 zeroext) #1

declare i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @min_int(i32 %a, i32 %b) #0 {
entry:
  %retval = alloca i32, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %a.addr, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %b.addr, align 4
  store i32 %3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

declare i32 @lame_get_VBR_mean_bitrate_kbps(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @max_int(i32 %a, i32 %b) #0 {
entry:
  %retval = alloca i32, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %cmp = icmp sgt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %a.addr, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %b.addr, align 4
  store i32 %3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

declare i32 @lame_set_brate(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_exp_nspsytune(%struct.lame_global_struct*, i32) #1

declare i32 @lame_get_exp_nspsytune(%struct.lame_global_struct*) #1

declare i32 @lame_set_sfscale(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_quant_comp(%struct.lame_global_struct*, i32) #1

declare i32 @lame_get_quant_comp(%struct.lame_global_struct*) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #3

declare i32 @lame_set_quant_comp_short(%struct.lame_global_struct*, i32) #1

declare i32 @lame_get_quant_comp_short(%struct.lame_global_struct*) #1

declare void @lame_set_msfix(%struct.lame_global_struct*, double) #1

declare float @lame_get_msfix(%struct.lame_global_struct*) #1

declare i32 @lame_set_short_threshold_lrm(%struct.lame_global_struct*, float) #1

declare float @lame_get_short_threshold_lrm(%struct.lame_global_struct*) #1

declare i32 @lame_set_short_threshold_s(%struct.lame_global_struct*, float) #1

declare float @lame_get_short_threshold_s(%struct.lame_global_struct*) #1

declare i32 @lame_set_scale(%struct.lame_global_struct*, float) #1

declare float @lame_get_scale(%struct.lame_global_struct*) #1

declare i32 @lame_set_maskingadjust(%struct.lame_global_struct*, float) #1

declare float @lame_get_maskingadjust(%struct.lame_global_struct*) #1

declare i32 @lame_set_maskingadjust_short(%struct.lame_global_struct*, float) #1

declare float @lame_get_maskingadjust_short(%struct.lame_global_struct*) #1

declare i32 @lame_set_ATHlower(%struct.lame_global_struct*, float) #1

declare float @lame_get_ATHlower(%struct.lame_global_struct*) #1

declare i32 @lame_set_ATHcurve(%struct.lame_global_struct*, float) #1

declare float @lame_get_ATHcurve(%struct.lame_global_struct*) #1

declare i32 @lame_set_interChRatio(%struct.lame_global_struct*, float) #1

declare float @lame_get_interChRatio(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define internal %struct.vbr_presets_t* @get_vbr_preset(i32 %v) #0 {
entry:
  %retval = alloca %struct.vbr_presets_t*, align 4
  %v.addr = alloca i32, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load i32, i32* %v.addr, align 4
  switch i32 %0, label %sw.default [
    i32 4, label %sw.bb
    i32 1, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry
  store %struct.vbr_presets_t* getelementptr inbounds ([11 x %struct.vbr_presets_t], [11 x %struct.vbr_presets_t]* @vbr_mt_psy_switch_map, i32 0, i32 0), %struct.vbr_presets_t** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store %struct.vbr_presets_t* getelementptr inbounds ([11 x %struct.vbr_presets_t], [11 x %struct.vbr_presets_t]* @vbr_old_switch_map, i32 0, i32 0), %struct.vbr_presets_t** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb
  %1 = load %struct.vbr_presets_t*, %struct.vbr_presets_t** %retval, align 4
  ret %struct.vbr_presets_t* %1
}

declare i32 @lame_get_VBR(%struct.lame_global_struct*) #1

declare i32 @lame_set_VBR_q(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_experimentalY(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_ATHtype(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_athaa_sensitivity(%struct.lame_global_struct*, float) #1

declare float @lame_get_athaa_sensitivity(%struct.lame_global_struct*) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log10.f64(double) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
