; ModuleID = 'tabinit.c'
source_filename = "tabinit.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@cos64 = internal global [16 x float] zeroinitializer, align 16
@cos32 = internal global [8 x float] zeroinitializer, align 16
@cos16 = internal global [4 x float] zeroinitializer, align 16
@cos8 = internal global [2 x float] zeroinitializer, align 4
@cos4 = internal global [1 x float] zeroinitializer, align 4
@pnts = hidden global [5 x float*] [float* getelementptr inbounds ([16 x float], [16 x float]* @cos64, i32 0, i32 0), float* getelementptr inbounds ([8 x float], [8 x float]* @cos32, i32 0, i32 0), float* getelementptr inbounds ([4 x float], [4 x float]* @cos16, i32 0, i32 0), float* getelementptr inbounds ([2 x float], [2 x float]* @cos8, i32 0, i32 0), float* getelementptr inbounds ([1 x float], [1 x float]* @cos4, i32 0, i32 0)], align 16
@table_init_called = internal global i32 0, align 4
@decwin = hidden global [544 x float] zeroinitializer, align 16
@dewin = internal constant <{ [257 x double], [255 x double] }> <{ [257 x double] [double 0.000000e+00, double -1.525900e-05, double -1.525900e-05, double -1.525900e-05, double -1.525900e-05, double -1.525900e-05, double -1.525900e-05, double -3.051800e-05, double -3.051800e-05, double -3.051800e-05, double -3.051800e-05, double -4.577600e-05, double -4.577600e-05, double -6.103500e-05, double -6.103500e-05, double 0xBF140000F084A6B2, double 0xBF140000F084A6B2, double 0xBF180004903B29AA, double -1.068120e-04, double -1.068120e-04, double -1.220700e-04, double -1.373290e-04, double -1.525880e-04, double -1.678470e-04, double -1.983640e-04, double -2.136230e-04, double -2.441410e-04, double -2.593990e-04, double -2.899170e-04, double -3.204350e-04, double -3.662110e-04, double -3.967290e-04, double -4.425050e-04, double -4.730220e-04, double -5.340580e-04, double -5.798340e-04, double -6.256100e-04, double -6.866460e-04, double -7.476810e-04, double -8.087160e-04, double 0xBF4D000080D9594D, double 0xBF4F80009EE9EE23, double 0xBF5100005E7D417D, double 0xBF5240006D858BE8, double 0xBF53BFFFA3A88000, double 0xBF553FFFECAC329B, double -1.388550e-03, double 0xBF5840007EB397D0, double 0xBF59FFFFEED1F418, double 0xBF5BC00071D10EE2, double 0xBF5D3FFFA7F402FA, double 0xBF5F400064EE85F4, double -2.014160e-03, double 0xBF6160002C05FE83, double 0xBF6260000112E0BF, double 0xBF63400042926E24, double 0xBF641FFFFAA19C47, double 0xBF651FFFCFAE7E83, double 0xBF660000112E0BE8, double 0xBF66DFFFC93D3A0C, double -2.899170e-03, double 0xBF6880002F3EA0BE, double 0xBF693FFFCA501ACB, double 0xBF69FFFFEED1F418, double 0xBF6A9FFFF656194D, double 0xBF6B3FFFFDDA3E83, double 0xBF6BBFFFE860AFA1, double 0xBF6C20003F59CBE8, double 0xBF6C5FFFEFE4D4D6, double 0xBF6C80000CE288EE, double 0xBF6C80000CE288EE, double 0xBF6C5FFFEFE4D4D6, double 0xBF6C0000225C17D0, double 0xBF6B9FFFCB62FB89, double -3.280640e-03, double 0xBF69FFFFEED1F418, double 0xBF69000019C511DC, double 0xBF679FFFEDBF1359, double 0xBF6620002E2BC000, double 0xBF646000349D0477, double 0xBF623FFFE4152CA7, double 0xBF5FBFFFC60497D0, double 0xBF5A800062C8C477, double 0xBF54BFFF78B5623C, double -8.697510e-04, double -4.425050e-04, double 3.051800e-05, double 5.493160e-04, double 0x3F520000338A23B8, double 0x3F5BC00071D10EE2, double 0x3F6320002594BA0C, double 0x3F689FFFC2CBF595, double 0x3F6E8000406CACA7, double 0x3F7260000112E0BF, double 5.294800e-03, double 0x3F790FFFE38BBC47, double 0x3F7CAFFFF3A6E771, double 0x3F803800105FE359, double 0x3F8227FFF0B2FD65, double 0x3F84280001E1094D, double 0x3F8637FFFF31D771, double 0x3F8858000B017FA1, double 0x3F8A7FFFFBB47D06, double 0x3F8CB7FFFAE65477, double 0x3F8EF800015798EE, double 0x3F90A00007842536, double 0x3F91C3FFFD2E720C, double 0x3F92E8000406CACA, double 0x3F940BFFF9B117A1, double 0x3F9530000089705F, double 0x3F96500003C2129B, double 0x3F9767FFFFBB47D0, double 0x3F987BFFF814C683, double 0x3F9987FFFA5CE418, double 0x3F9A880002F3EA0C, double 0x3F9B7C0000ABCC77, double 0x3F9C640004B29741, double 0x3F9D380007C8DD65, double 0x3F9DFBFFFC60497D, double 0x3F9EA7FFFD958653, double 0x3F9F44000179F506, double 0x3F9FBFFFF98EBB89, double 0x3FA011FFFF20A965, double 0x3FA035FFFD61FC30, double 0x3FA049FFFE5280D6, double 0x3FA04E0001F23759, double 0x3FA03FFFFDDA3E83, double 0x3FA01E000168C6FA, double 0x3F9FD3FFFA7F4030, double 0x3F9F3FFFFDDA3E83, double 0x3F9E7FFFFBB47D06, double 0x3F9D9400053C07A1, double 0x3F9C780005A31BE8, double 0x3F9B2BFFFCE9B9DC, double 0x3F99AFFFFC3DED65, double 0x3F97FBFFFC60497D, double 0x3F96180004903B2A, double 0x3F94000000000000, double 0x3F91ABFFF89E36E2, double 0x3F8E4FFFF29406B3, double 0x3F88D00010A49B89, double 0x3F82E7FFF2D8BEE2, double 0x3F791FFFF20A9653, double 0x3F6720000338A23C, double -6.866460e-04, double 0xBF71FFFFEED1F418, double -8.316040e-03, double 0xBF896FFFF5CCA8EE, double 0xBF911BFFFF98EBB9, double 0xBF95AFFFFC3DED65, double 0xBF9A6FFFFE63AEE2, double 0xBF9F5800026A79AD, double 0xBFA23400042926E2, double 0xBFA4CDFFFDA6B45F, double 0xBFA777FFFD0C15F4, double 0xBFAA2DFFFEB9951E, double 0xBFACF1FFFBE8072A, double 0xBFAFBE000055E63C, double 0xBFB1490001B61630, double 0xBFB2B3FFFFDDA3E8, double 0xBFB420FFFFD50CE2, double 0xBFB58CFFFEE4883C, double 0xBFB6F700006FAB4D, double 0xBFB85CFFFE5B17DC, double 0xBFB9BCFFFF6DF89B, double 0xBFBB16FFFF5CCA8F, double 0xBFBC66FFFED35A30, double 0xBFBDAD00021D2A77, double 0xBFBEE5FFFDEB6C8F, double 0xBFC0087FFF1C5DE2, double 0xBFC095800026A79B, double 0xBFC1197FFF7ADB24, double 0xBFC1930000089706, double 0xBFC2017FFF3622F4, double 0xBFC2640000671447, double 0xBFC2B87FFFA5CE41, double 0xBFC2FF7FFF8C0930, double 0xBFC33700006FAB4D, double 0xBFC35DFFFF43057D, double 0xBFC3737FFF69AD18, double 0xBFC37700006FAB4D, double 0xBFC3677FFF475100, double 0xBFC343800105FE36, double 0xBFC30AFFFF3A6E77, double 0xBFC2BC0000ABCC77, double 0xBFC25680010E953C, double 0xBFC1D97FFF7ADB24, double 0xBFC143FFFF543389, double 0xBFC09600009A9E6B, double 0xBFBF9BFFFF98EBB9, double 0xBFBDD7FFFE1EF6B3, double 0xBFBBDDFFFF43057D, double 0xBFB9ADFFFEB9951E, double 0xBFB746FFFFE63AEE, double 0xBFB4A80001E1094D, double 0xBFB1D100005E7D41, double 0xBFAD7FFFFBB47D06, double 0xBFA6EE0000DF569B, double 0xBF9FD3FFFA7F4030, double 0xBF90E8000406CACA, double 0xBF517FFFBF935359, double 0x3F8F2FFFEF5B6477, double 0x3FA093FFFCA501AD, double 0x3FA9C80000CE288F, double 0x3FB1B50000C59189, double 0x3FB6BA000101B2B3, double 0x3FBBF20000338A24, double 0x3FC0AE0000DF569B, double 0x3FC37B800037D5A7, double 0x3FC65F80009EE9EE, double 0x3FC959FFFFEED1F4, double 0x3FCC69FFFF656195, double 0x3FCF8D7FFF587F0C, double 0x3FD161C00082FF1B, double 0x3FD3057FFF9D373C, double 0x3FD4B13FFF8597EB, double 0x3FD6643FFF8E2EF1, double 0x3FD81D7FFFE1EF6B, double 0x3FD9DC80000CE289, double 0x3FDBA0400039FB68, double 0x3FDD67FFFFBB47D0, double 0x3FDF32BFFFCE9B9E, double 0x3FE07FDFFFE3024C, double 0x3FE166FFFFE63AEE, double 0x3FE24E1FFFE97390, double 0x3FE33500003C212A, double 0x3FE41AFFFFC3DED6, double 0x3FE4FFBFFFC60498, double 0x3FE5E2A0003B0E49, double 0x3FE6C320002594BA, double 0x3FE7A0C0002481D9, double 0x3FE87B1FFFE0DC8A, double 0x3FE9519FFFDC9107, double 0x3FEA23C0002D18DF, double 0x3FEAF140000F084A, double 0x3FEBB93FFFCA501B, double 0x3FEC7B9FFFCB62FC, double 0x3FED37C0000ABCC7, double 0x3FEDED20001466AE, double 0x3FEE9B800037D5A7, double 0x3FEF4260000112E1, double 0x3FEFE140000F084A, double 0x3FF03BE000055E64, double 0x3FF082D00021D2A7, double 0x3FF0C53FFFECAC33, double 0x3FF102EFFFFA182C, double 0x3FF13BE000055E64, double 0x3FF16FC0000ABCC7, double 0x3FF19E90000A3357, double 0x3FF1C82FFFE6C45F, double 0x3FF1EC6FFFFE63AF, double 0x3FF20B3FFFFDDA3F, double 0x3FF22490001B6163, double 0x3FF23860001240ED, double 0x3FF2467FFFFBB47D, double 0x3FF24EFFFFE63AEE, double 0x3FF251E000168C70], [255 x double] zeroinitializer }>, align 16

; Function Attrs: noinline nounwind optnone
define hidden void @make_decode_tables(i32 %scaleval) #0 {
entry:
  %scaleval.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %kr = alloca i32, align 4
  %divv = alloca i32, align 4
  %table = alloca float*, align 4
  %costab = alloca float*, align 4
  store i32 %scaleval, i32* %scaleval.addr, align 4
  %0 = load i32, i32* @table_init_called, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end72

if.end:                                           ; preds = %entry
  store i32 1, i32* @table_init_called, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 5
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %shr = ashr i32 16, %2
  store i32 %shr, i32* %kr, align 4
  %3 = load i32, i32* %i, align 4
  %shr1 = ashr i32 64, %3
  store i32 %shr1, i32* %divv, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [5 x float*], [5 x float*]* @pnts, i32 0, i32 %4
  %5 = load float*, float** %arrayidx, align 4
  store float* %5, float** %costab, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %k, align 4
  %7 = load i32, i32* %kr, align 4
  %cmp3 = icmp slt i32 %6, %7
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %8 = load i32, i32* %k, align 4
  %conv = sitofp i32 %8 to double
  %mul = fmul double %conv, 2.000000e+00
  %add = fadd double %mul, 1.000000e+00
  %mul5 = fmul double 0x400921FB54442D18, %add
  %9 = load i32, i32* %divv, align 4
  %conv6 = sitofp i32 %9 to double
  %div = fdiv double %mul5, %conv6
  %10 = call double @llvm.cos.f64(double %div)
  %mul7 = fmul double 2.000000e+00, %10
  %div8 = fdiv double 1.000000e+00, %mul7
  %conv9 = fptrunc double %div8 to float
  %11 = load float*, float** %costab, align 4
  %12 = load i32, i32* %k, align 4
  %arrayidx10 = getelementptr inbounds float, float* %11, i32 %12
  store float %conv9, float* %arrayidx10, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %13 = load i32, i32* %k, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %14 = load i32, i32* %i, align 4
  %inc12 = add nsw i32 %14, 1
  store i32 %inc12, i32* %i, align 4
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  store float* getelementptr inbounds ([544 x float], [544 x float]* @decwin, i32 0, i32 0), float** %table, align 4
  %15 = load i32, i32* %scaleval.addr, align 4
  %sub = sub nsw i32 0, %15
  store i32 %sub, i32* %scaleval.addr, align 4
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc38, %for.end13
  %16 = load i32, i32* %i, align 4
  %cmp15 = icmp slt i32 %16, 256
  br i1 %cmp15, label %for.body17, label %for.end42

for.body17:                                       ; preds = %for.cond14
  %17 = load float*, float** %table, align 4
  %cmp18 = icmp ult float* %17, getelementptr inbounds ([544 x float], [544 x float]* @decwin, i32 0, i32 528)
  br i1 %cmp18, label %if.then20, label %if.end27

if.then20:                                        ; preds = %for.body17
  %18 = load i32, i32* %j, align 4
  %arrayidx21 = getelementptr inbounds [512 x double], [512 x double]* bitcast (<{ [257 x double], [255 x double] }>* @dewin to [512 x double]*), i32 0, i32 %18
  %19 = load double, double* %arrayidx21, align 8
  %20 = load i32, i32* %scaleval.addr, align 4
  %conv22 = sitofp i32 %20 to double
  %mul23 = fmul double %19, %conv22
  %conv24 = fptrunc double %mul23 to float
  %21 = load float*, float** %table, align 4
  %arrayidx25 = getelementptr inbounds float, float* %21, i32 0
  store float %conv24, float* %arrayidx25, align 4
  %22 = load float*, float** %table, align 4
  %arrayidx26 = getelementptr inbounds float, float* %22, i32 16
  store float %conv24, float* %arrayidx26, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then20, %for.body17
  %23 = load i32, i32* %i, align 4
  %rem = srem i32 %23, 32
  %cmp28 = icmp eq i32 %rem, 31
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end27
  %24 = load float*, float** %table, align 4
  %add.ptr = getelementptr inbounds float, float* %24, i32 -1023
  store float* %add.ptr, float** %table, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.end27
  %25 = load i32, i32* %i, align 4
  %rem32 = srem i32 %25, 64
  %cmp33 = icmp eq i32 %rem32, 63
  br i1 %cmp33, label %if.then35, label %if.end37

if.then35:                                        ; preds = %if.end31
  %26 = load i32, i32* %scaleval.addr, align 4
  %sub36 = sub nsw i32 0, %26
  store i32 %sub36, i32* %scaleval.addr, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.then35, %if.end31
  br label %for.inc38

for.inc38:                                        ; preds = %if.end37
  %27 = load i32, i32* %i, align 4
  %inc39 = add nsw i32 %27, 1
  store i32 %inc39, i32* %i, align 4
  %28 = load i32, i32* %j, align 4
  %inc40 = add nsw i32 %28, 1
  store i32 %inc40, i32* %j, align 4
  %29 = load float*, float** %table, align 4
  %add.ptr41 = getelementptr inbounds float, float* %29, i32 32
  store float* %add.ptr41, float** %table, align 4
  br label %for.cond14

for.end42:                                        ; preds = %for.cond14
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc69, %for.end42
  %30 = load i32, i32* %i, align 4
  %cmp44 = icmp slt i32 %30, 512
  br i1 %cmp44, label %for.body46, label %for.end72

for.body46:                                       ; preds = %for.cond43
  %31 = load float*, float** %table, align 4
  %cmp47 = icmp ult float* %31, getelementptr inbounds ([544 x float], [544 x float]* @decwin, i32 0, i32 528)
  br i1 %cmp47, label %if.then49, label %if.end56

if.then49:                                        ; preds = %for.body46
  %32 = load i32, i32* %j, align 4
  %arrayidx50 = getelementptr inbounds [512 x double], [512 x double]* bitcast (<{ [257 x double], [255 x double] }>* @dewin to [512 x double]*), i32 0, i32 %32
  %33 = load double, double* %arrayidx50, align 8
  %34 = load i32, i32* %scaleval.addr, align 4
  %conv51 = sitofp i32 %34 to double
  %mul52 = fmul double %33, %conv51
  %conv53 = fptrunc double %mul52 to float
  %35 = load float*, float** %table, align 4
  %arrayidx54 = getelementptr inbounds float, float* %35, i32 0
  store float %conv53, float* %arrayidx54, align 4
  %36 = load float*, float** %table, align 4
  %arrayidx55 = getelementptr inbounds float, float* %36, i32 16
  store float %conv53, float* %arrayidx55, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.then49, %for.body46
  %37 = load i32, i32* %i, align 4
  %rem57 = srem i32 %37, 32
  %cmp58 = icmp eq i32 %rem57, 31
  br i1 %cmp58, label %if.then60, label %if.end62

if.then60:                                        ; preds = %if.end56
  %38 = load float*, float** %table, align 4
  %add.ptr61 = getelementptr inbounds float, float* %38, i32 -1023
  store float* %add.ptr61, float** %table, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %if.end56
  %39 = load i32, i32* %i, align 4
  %rem63 = srem i32 %39, 64
  %cmp64 = icmp eq i32 %rem63, 63
  br i1 %cmp64, label %if.then66, label %if.end68

if.then66:                                        ; preds = %if.end62
  %40 = load i32, i32* %scaleval.addr, align 4
  %sub67 = sub nsw i32 0, %40
  store i32 %sub67, i32* %scaleval.addr, align 4
  br label %if.end68

if.end68:                                         ; preds = %if.then66, %if.end62
  br label %for.inc69

for.inc69:                                        ; preds = %if.end68
  %41 = load i32, i32* %i, align 4
  %inc70 = add nsw i32 %41, 1
  store i32 %inc70, i32* %i, align 4
  %42 = load i32, i32* %j, align 4
  %dec = add nsw i32 %42, -1
  store i32 %dec, i32* %j, align 4
  %43 = load float*, float** %table, align 4
  %add.ptr71 = getelementptr inbounds float, float* %43, i32 32
  store float* %add.ptr71, float** %table, align 4
  br label %for.cond43

for.end72:                                        ; preds = %if.then, %for.cond43
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
