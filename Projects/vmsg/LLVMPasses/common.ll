; ModuleID = 'common.c'
source_filename = "common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mpstr_tag = type { %struct.buf*, %struct.buf*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.frame, %struct.III_sideinfo, [2 x [3904 x i8]], [2 x [2 x [576 x float]]], [2 x i32], i32, i32, [2 x [2 x [272 x float]]], i32, i32, i32, i8*, %struct.plotting_data*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.buf = type { i8*, i32, i32, %struct.buf*, %struct.buf* }
%struct.frame = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.al_table2*, i32, i32 }
%struct.al_table2 = type opaque
%struct.III_sideinfo = type { i32, i32, [2 x %struct.anon] }
%struct.anon = type { [2 x %struct.gr_info_s] }
%struct.gr_info_s = type { i32, i32, i32, i32, i32, i32, [3 x i32], [3 x i32], [3 x i32], i32, i32, i32, i32, i32, i32, i32, [3 x float*], float* }
%struct.plotting_data = type opaque

@tabsel_123 = hidden constant [2 x [3 x [16 x i32]]] [[3 x [16 x i32]] [[16 x i32] [i32 0, i32 32, i32 64, i32 96, i32 128, i32 160, i32 192, i32 224, i32 256, i32 288, i32 320, i32 352, i32 384, i32 416, i32 448, i32 0], [16 x i32] [i32 0, i32 32, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 160, i32 192, i32 224, i32 256, i32 320, i32 384, i32 0], [16 x i32] [i32 0, i32 32, i32 40, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 160, i32 192, i32 224, i32 256, i32 320, i32 0]], [3 x [16 x i32]] [[16 x i32] [i32 0, i32 32, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 144, i32 160, i32 176, i32 192, i32 224, i32 256, i32 0], [16 x i32] [i32 0, i32 8, i32 16, i32 24, i32 32, i32 40, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 144, i32 160, i32 0], [16 x i32] [i32 0, i32 8, i32 16, i32 24, i32 32, i32 40, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 144, i32 160, i32 0]]], align 16
@freqs = hidden constant [9 x i32] [i32 44100, i32 48000, i32 32000, i32 22050, i32 24000, i32 16000, i32 11025, i32 12000, i32 8000], align 16
@.str = private unnamed_addr constant [38 x i8] c"MPEG-2.5 is supported by Layer3 only\0A\00", align 1
@.str.1 = private unnamed_addr constant [14 x i8] c"Stream error\0A\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"Frame size too big.\0A\00", align 1
@.str.3 = private unnamed_addr constant [31 x i8] c"Sorry, layer %d not supported\0A\00", align 1
@.str.4 = private unnamed_addr constant [33 x i8] c"hip: Can't step back %ld bytes!\0A\00", align 1
@muls = hidden global [27 x [64 x float]] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @head_check(i32 %head, i32 %check_layer) #0 {
entry:
  %retval = alloca i32, align 4
  %head.addr = alloca i32, align 4
  %check_layer.addr = alloca i32, align 4
  %nLayer = alloca i32, align 4
  store i32 %head, i32* %head.addr, align 4
  store i32 %check_layer, i32* %check_layer.addr, align 4
  %0 = load i32, i32* %head.addr, align 4
  %shr = lshr i32 %0, 17
  %and = and i32 %shr, 3
  %sub = sub i32 4, %and
  store i32 %sub, i32* %nLayer, align 4
  %1 = load i32, i32* %head.addr, align 4
  %and1 = and i32 %1, -2097152
  %cmp = icmp ne i32 %and1, -2097152
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %nLayer, align 4
  %cmp2 = icmp eq i32 %2, 4
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %3 = load i32, i32* %check_layer.addr, align 4
  %cmp5 = icmp sgt i32 %3, 0
  br i1 %cmp5, label %land.lhs.true, label %if.end8

land.lhs.true:                                    ; preds = %if.end4
  %4 = load i32, i32* %nLayer, align 4
  %5 = load i32, i32* %check_layer.addr, align 4
  %cmp6 = icmp ne i32 %4, %5
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %land.lhs.true, %if.end4
  %6 = load i32, i32* %head.addr, align 4
  %shr9 = lshr i32 %6, 12
  %and10 = and i32 %shr9, 15
  %cmp11 = icmp eq i32 %and10, 15
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end8
  store i32 0, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end8
  %7 = load i32, i32* %head.addr, align 4
  %shr14 = lshr i32 %7, 10
  %and15 = and i32 %shr14, 3
  %cmp16 = icmp eq i32 %and15, 3
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i32 0, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end13
  %8 = load i32, i32* %head.addr, align 4
  %and19 = and i32 %8, 3
  %cmp20 = icmp eq i32 %and19, 2
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end18
  store i32 0, i32* %retval, align 4
  br label %return

if.end22:                                         ; preds = %if.end18
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then21, %if.then17, %if.then12, %if.then7, %if.then3, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @decode_header(%struct.mpstr_tag* %mp, %struct.frame* %fr, i32 %newhead) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %fr.addr = alloca %struct.frame*, align 4
  %newhead.addr = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store %struct.frame* %fr, %struct.frame** %fr.addr, align 4
  store i32 %newhead, i32* %newhead.addr, align 4
  %0 = load i32, i32* %newhead.addr, align 4
  %and = and i32 %0, 1048576
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newhead.addr, align 4
  %and1 = and i32 %1, 524288
  %tobool2 = icmp ne i32 %and1, 0
  %2 = zext i1 %tobool2 to i64
  %cond = select i1 %tobool2, i32 0, i32 1
  %3 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf = getelementptr inbounds %struct.frame, %struct.frame* %3, i32 0, i32 2
  store i32 %cond, i32* %lsf, align 4
  %4 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mpeg25 = getelementptr inbounds %struct.frame, %struct.frame* %4, i32 0, i32 3
  store i32 0, i32* %mpeg25, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf3 = getelementptr inbounds %struct.frame, %struct.frame* %5, i32 0, i32 2
  store i32 1, i32* %lsf3, align 4
  %6 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mpeg254 = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 3
  store i32 1, i32* %mpeg254, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %7 = load i32, i32* %newhead.addr, align 4
  %shr = lshr i32 %7, 17
  %and5 = and i32 %shr, 3
  %sub = sub i32 4, %and5
  %8 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lay = getelementptr inbounds %struct.frame, %struct.frame* %8, i32 0, i32 5
  store i32 %sub, i32* %lay, align 4
  %9 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lay6 = getelementptr inbounds %struct.frame, %struct.frame* %9, i32 0, i32 5
  %10 = load i32, i32* %lay6, align 4
  %cmp = icmp ne i32 %10, 3
  br i1 %cmp, label %land.lhs.true, label %if.end10

land.lhs.true:                                    ; preds = %if.end
  %11 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mpeg257 = getelementptr inbounds %struct.frame, %struct.frame* %11, i32 0, i32 3
  %12 = load i32, i32* %mpeg257, align 4
  %tobool8 = icmp ne i32 %12, 0
  br i1 %tobool8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %land.lhs.true
  %13 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %13, i32 0, i32 32
  %14 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %14, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %land.lhs.true, %if.end
  %15 = load i32, i32* %newhead.addr, align 4
  %shr11 = lshr i32 %15, 10
  %and12 = and i32 %shr11, 3
  %cmp13 = icmp eq i32 %and12, 3
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end10
  %16 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err15 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %16, i32 0, i32 32
  %17 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err15, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %17, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.1, i32 0, i32 0))
  store i32 0, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.end10
  %18 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mpeg2517 = getelementptr inbounds %struct.frame, %struct.frame* %18, i32 0, i32 3
  %19 = load i32, i32* %mpeg2517, align 4
  %tobool18 = icmp ne i32 %19, 0
  br i1 %tobool18, label %if.then19, label %if.else22

if.then19:                                        ; preds = %if.end16
  %20 = load i32, i32* %newhead.addr, align 4
  %shr20 = lshr i32 %20, 10
  %and21 = and i32 %shr20, 3
  %add = add i32 6, %and21
  %21 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency = getelementptr inbounds %struct.frame, %struct.frame* %21, i32 0, i32 8
  store i32 %add, i32* %sampling_frequency, align 4
  br label %if.end28

if.else22:                                        ; preds = %if.end16
  %22 = load i32, i32* %newhead.addr, align 4
  %shr23 = lshr i32 %22, 10
  %and24 = and i32 %shr23, 3
  %23 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf25 = getelementptr inbounds %struct.frame, %struct.frame* %23, i32 0, i32 2
  %24 = load i32, i32* %lsf25, align 4
  %mul = mul nsw i32 %24, 3
  %add26 = add i32 %and24, %mul
  %25 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency27 = getelementptr inbounds %struct.frame, %struct.frame* %25, i32 0, i32 8
  store i32 %add26, i32* %sampling_frequency27, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.else22, %if.then19
  %26 = load i32, i32* %newhead.addr, align 4
  %shr29 = lshr i32 %26, 16
  %and30 = and i32 %shr29, 1
  %xor = xor i32 %and30, 1
  %27 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %error_protection = getelementptr inbounds %struct.frame, %struct.frame* %27, i32 0, i32 6
  store i32 %xor, i32* %error_protection, align 4
  %28 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mpeg2531 = getelementptr inbounds %struct.frame, %struct.frame* %28, i32 0, i32 3
  %29 = load i32, i32* %mpeg2531, align 4
  %tobool32 = icmp ne i32 %29, 0
  br i1 %tobool32, label %if.then33, label %if.end36

if.then33:                                        ; preds = %if.end28
  %30 = load i32, i32* %newhead.addr, align 4
  %shr34 = lshr i32 %30, 12
  %and35 = and i32 %shr34, 15
  %31 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index = getelementptr inbounds %struct.frame, %struct.frame* %31, i32 0, i32 7
  store i32 %and35, i32* %bitrate_index, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then33, %if.end28
  %32 = load i32, i32* %newhead.addr, align 4
  %shr37 = lshr i32 %32, 12
  %and38 = and i32 %shr37, 15
  %33 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index39 = getelementptr inbounds %struct.frame, %struct.frame* %33, i32 0, i32 7
  store i32 %and38, i32* %bitrate_index39, align 4
  %34 = load i32, i32* %newhead.addr, align 4
  %shr40 = lshr i32 %34, 9
  %and41 = and i32 %shr40, 1
  %35 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %padding = getelementptr inbounds %struct.frame, %struct.frame* %35, i32 0, i32 9
  store i32 %and41, i32* %padding, align 4
  %36 = load i32, i32* %newhead.addr, align 4
  %shr42 = lshr i32 %36, 8
  %and43 = and i32 %shr42, 1
  %37 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %extension = getelementptr inbounds %struct.frame, %struct.frame* %37, i32 0, i32 10
  store i32 %and43, i32* %extension, align 4
  %38 = load i32, i32* %newhead.addr, align 4
  %shr44 = lshr i32 %38, 6
  %and45 = and i32 %shr44, 3
  %39 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode = getelementptr inbounds %struct.frame, %struct.frame* %39, i32 0, i32 11
  store i32 %and45, i32* %mode, align 4
  %40 = load i32, i32* %newhead.addr, align 4
  %shr46 = lshr i32 %40, 4
  %and47 = and i32 %shr46, 3
  %41 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode_ext = getelementptr inbounds %struct.frame, %struct.frame* %41, i32 0, i32 12
  store i32 %and47, i32* %mode_ext, align 4
  %42 = load i32, i32* %newhead.addr, align 4
  %shr48 = lshr i32 %42, 3
  %and49 = and i32 %shr48, 1
  %43 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %copyright = getelementptr inbounds %struct.frame, %struct.frame* %43, i32 0, i32 13
  store i32 %and49, i32* %copyright, align 4
  %44 = load i32, i32* %newhead.addr, align 4
  %shr50 = lshr i32 %44, 2
  %and51 = and i32 %shr50, 1
  %45 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %original = getelementptr inbounds %struct.frame, %struct.frame* %45, i32 0, i32 14
  store i32 %and51, i32* %original, align 4
  %46 = load i32, i32* %newhead.addr, align 4
  %and52 = and i32 %46, 3
  %47 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %emphasis = getelementptr inbounds %struct.frame, %struct.frame* %47, i32 0, i32 15
  store i32 %and52, i32* %emphasis, align 4
  %48 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %mode53 = getelementptr inbounds %struct.frame, %struct.frame* %48, i32 0, i32 11
  %49 = load i32, i32* %mode53, align 4
  %cmp54 = icmp eq i32 %49, 3
  %50 = zext i1 %cmp54 to i64
  %cond55 = select i1 %cmp54, i32 1, i32 2
  %51 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %51, i32 0, i32 0
  store i32 %cond55, i32* %stereo, align 4
  %52 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lay56 = getelementptr inbounds %struct.frame, %struct.frame* %52, i32 0, i32 5
  %53 = load i32, i32* %lay56, align 4
  switch i32 %53, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb72
    i32 3, label %sw.bb92
  ]

sw.bb:                                            ; preds = %if.end36
  %54 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf57 = getelementptr inbounds %struct.frame, %struct.frame* %54, i32 0, i32 2
  %55 = load i32, i32* %lsf57, align 4
  %arrayidx = getelementptr inbounds [2 x [3 x [16 x i32]]], [2 x [3 x [16 x i32]]]* @tabsel_123, i32 0, i32 %55
  %arrayidx58 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* %arrayidx, i32 0, i32 0
  %56 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index59 = getelementptr inbounds %struct.frame, %struct.frame* %56, i32 0, i32 7
  %57 = load i32, i32* %bitrate_index59, align 4
  %arrayidx60 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx58, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx60, align 4
  %mul61 = mul nsw i32 %58, 12000
  %59 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize = getelementptr inbounds %struct.frame, %struct.frame* %59, i32 0, i32 16
  store i32 %mul61, i32* %framesize, align 4
  %60 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency62 = getelementptr inbounds %struct.frame, %struct.frame* %60, i32 0, i32 8
  %61 = load i32, i32* %sampling_frequency62, align 4
  %arrayidx63 = getelementptr inbounds [9 x i32], [9 x i32]* @freqs, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx63, align 4
  %63 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize64 = getelementptr inbounds %struct.frame, %struct.frame* %63, i32 0, i32 16
  %64 = load i32, i32* %framesize64, align 4
  %div = sdiv i32 %64, %62
  store i32 %div, i32* %framesize64, align 4
  %65 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize65 = getelementptr inbounds %struct.frame, %struct.frame* %65, i32 0, i32 16
  %66 = load i32, i32* %framesize65, align 4
  %67 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %padding66 = getelementptr inbounds %struct.frame, %struct.frame* %67, i32 0, i32 9
  %68 = load i32, i32* %padding66, align 4
  %add67 = add nsw i32 %66, %68
  %shl = shl i32 %add67, 2
  %sub68 = sub nsw i32 %shl, 4
  %69 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize69 = getelementptr inbounds %struct.frame, %struct.frame* %69, i32 0, i32 16
  store i32 %sub68, i32* %framesize69, align 4
  %70 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample = getelementptr inbounds %struct.frame, %struct.frame* %70, i32 0, i32 20
  store i32 0, i32* %down_sample, align 4
  %71 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample70 = getelementptr inbounds %struct.frame, %struct.frame* %71, i32 0, i32 20
  %72 = load i32, i32* %down_sample70, align 4
  %shr71 = ashr i32 32, %72
  %73 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %73, i32 0, i32 19
  store i32 %shr71, i32* %down_sample_sblimit, align 4
  br label %sw.epilog

sw.bb72:                                          ; preds = %if.end36
  %74 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf73 = getelementptr inbounds %struct.frame, %struct.frame* %74, i32 0, i32 2
  %75 = load i32, i32* %lsf73, align 4
  %arrayidx74 = getelementptr inbounds [2 x [3 x [16 x i32]]], [2 x [3 x [16 x i32]]]* @tabsel_123, i32 0, i32 %75
  %arrayidx75 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* %arrayidx74, i32 0, i32 1
  %76 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index76 = getelementptr inbounds %struct.frame, %struct.frame* %76, i32 0, i32 7
  %77 = load i32, i32* %bitrate_index76, align 4
  %arrayidx77 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx75, i32 0, i32 %77
  %78 = load i32, i32* %arrayidx77, align 4
  %mul78 = mul nsw i32 %78, 144000
  %79 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize79 = getelementptr inbounds %struct.frame, %struct.frame* %79, i32 0, i32 16
  store i32 %mul78, i32* %framesize79, align 4
  %80 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency80 = getelementptr inbounds %struct.frame, %struct.frame* %80, i32 0, i32 8
  %81 = load i32, i32* %sampling_frequency80, align 4
  %arrayidx81 = getelementptr inbounds [9 x i32], [9 x i32]* @freqs, i32 0, i32 %81
  %82 = load i32, i32* %arrayidx81, align 4
  %83 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize82 = getelementptr inbounds %struct.frame, %struct.frame* %83, i32 0, i32 16
  %84 = load i32, i32* %framesize82, align 4
  %div83 = sdiv i32 %84, %82
  store i32 %div83, i32* %framesize82, align 4
  %85 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %padding84 = getelementptr inbounds %struct.frame, %struct.frame* %85, i32 0, i32 9
  %86 = load i32, i32* %padding84, align 4
  %sub85 = sub nsw i32 %86, 4
  %87 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize86 = getelementptr inbounds %struct.frame, %struct.frame* %87, i32 0, i32 16
  %88 = load i32, i32* %framesize86, align 4
  %add87 = add nsw i32 %88, %sub85
  store i32 %add87, i32* %framesize86, align 4
  %89 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample88 = getelementptr inbounds %struct.frame, %struct.frame* %89, i32 0, i32 20
  store i32 0, i32* %down_sample88, align 4
  %90 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample89 = getelementptr inbounds %struct.frame, %struct.frame* %90, i32 0, i32 20
  %91 = load i32, i32* %down_sample89, align 4
  %shr90 = ashr i32 32, %91
  %92 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %down_sample_sblimit91 = getelementptr inbounds %struct.frame, %struct.frame* %92, i32 0, i32 19
  store i32 %shr90, i32* %down_sample_sblimit91, align 4
  br label %sw.epilog

sw.bb92:                                          ; preds = %if.end36
  %93 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize93 = getelementptr inbounds %struct.frame, %struct.frame* %93, i32 0, i32 16
  %94 = load i32, i32* %framesize93, align 4
  %cmp94 = icmp sgt i32 %94, 4096
  br i1 %cmp94, label %if.then95, label %if.end98

if.then95:                                        ; preds = %sw.bb92
  %95 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err96 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %95, i32 0, i32 32
  %96 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err96, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %96, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0))
  %97 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize97 = getelementptr inbounds %struct.frame, %struct.frame* %97, i32 0, i32 16
  store i32 4096, i32* %framesize97, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end98:                                         ; preds = %sw.bb92
  %98 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index99 = getelementptr inbounds %struct.frame, %struct.frame* %98, i32 0, i32 7
  %99 = load i32, i32* %bitrate_index99, align 4
  %cmp100 = icmp eq i32 %99, 0
  br i1 %cmp100, label %if.then101, label %if.else103

if.then101:                                       ; preds = %if.end98
  %100 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize102 = getelementptr inbounds %struct.frame, %struct.frame* %100, i32 0, i32 16
  store i32 0, i32* %framesize102, align 4
  br label %if.end122

if.else103:                                       ; preds = %if.end98
  %101 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf104 = getelementptr inbounds %struct.frame, %struct.frame* %101, i32 0, i32 2
  %102 = load i32, i32* %lsf104, align 4
  %arrayidx105 = getelementptr inbounds [2 x [3 x [16 x i32]]], [2 x [3 x [16 x i32]]]* @tabsel_123, i32 0, i32 %102
  %arrayidx106 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* %arrayidx105, i32 0, i32 2
  %103 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %bitrate_index107 = getelementptr inbounds %struct.frame, %struct.frame* %103, i32 0, i32 7
  %104 = load i32, i32* %bitrate_index107, align 4
  %arrayidx108 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx106, i32 0, i32 %104
  %105 = load i32, i32* %arrayidx108, align 4
  %mul109 = mul nsw i32 %105, 144000
  %106 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize110 = getelementptr inbounds %struct.frame, %struct.frame* %106, i32 0, i32 16
  store i32 %mul109, i32* %framesize110, align 4
  %107 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %sampling_frequency111 = getelementptr inbounds %struct.frame, %struct.frame* %107, i32 0, i32 8
  %108 = load i32, i32* %sampling_frequency111, align 4
  %arrayidx112 = getelementptr inbounds [9 x i32], [9 x i32]* @freqs, i32 0, i32 %108
  %109 = load i32, i32* %arrayidx112, align 4
  %110 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lsf113 = getelementptr inbounds %struct.frame, %struct.frame* %110, i32 0, i32 2
  %111 = load i32, i32* %lsf113, align 4
  %shl114 = shl i32 %109, %111
  %112 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize115 = getelementptr inbounds %struct.frame, %struct.frame* %112, i32 0, i32 16
  %113 = load i32, i32* %framesize115, align 4
  %div116 = sdiv i32 %113, %shl114
  store i32 %div116, i32* %framesize115, align 4
  %114 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize117 = getelementptr inbounds %struct.frame, %struct.frame* %114, i32 0, i32 16
  %115 = load i32, i32* %framesize117, align 4
  %116 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %padding118 = getelementptr inbounds %struct.frame, %struct.frame* %116, i32 0, i32 9
  %117 = load i32, i32* %padding118, align 4
  %add119 = add nsw i32 %115, %117
  %sub120 = sub nsw i32 %add119, 4
  %118 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %framesize121 = getelementptr inbounds %struct.frame, %struct.frame* %118, i32 0, i32 16
  store i32 %sub120, i32* %framesize121, align 4
  br label %if.end122

if.end122:                                        ; preds = %if.else103, %if.then101
  br label %sw.epilog

sw.default:                                       ; preds = %if.end36
  %119 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err123 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %119, i32 0, i32 32
  %120 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err123, align 4
  %121 = load %struct.frame*, %struct.frame** %fr.addr, align 4
  %lay124 = getelementptr inbounds %struct.frame, %struct.frame* %121, i32 0, i32 5
  %122 = load i32, i32* %lay124, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %120, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.3, i32 0, i32 0), i32 %122)
  store i32 0, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %if.end122, %sw.bb72, %sw.bb
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.default, %if.then95, %if.then14, %if.then9
  %123 = load i32, i32* %retval, align 4
  ret i32 %123
}

declare void @lame_report_fnc(void (i8*, i8*)*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @getbits(%struct.mpstr_tag* %mp, i32 %number_of_bits) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %number_of_bits.addr = alloca i32, align 4
  %rval = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %number_of_bits, i32* %number_of_bits.addr, align 4
  %0 = load i32, i32* %number_of_bits.addr, align 4
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %1, i32 0, i32 28
  %2 = load i8*, i8** %wordpointer, align 4
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %3, i32 0, i32 28
  %4 = load i8*, i8** %wordpointer1, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 0
  %5 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %5 to i32
  store i32 %conv, i32* %rval, align 4
  %6 = load i32, i32* %rval, align 4
  %shl = shl i32 %6, 8
  store i32 %shl, i32* %rval, align 4
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer2 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 28
  %8 = load i8*, i8** %wordpointer2, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %8, i32 1
  %9 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %9 to i32
  %10 = load i32, i32* %rval, align 4
  %or = or i32 %10, %conv4
  store i32 %or, i32* %rval, align 4
  %11 = load i32, i32* %rval, align 4
  %shl5 = shl i32 %11, 8
  store i32 %shl5, i32* %rval, align 4
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer6 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %12, i32 0, i32 28
  %13 = load i8*, i8** %wordpointer6, align 4
  %arrayidx7 = getelementptr inbounds i8, i8* %13, i32 2
  %14 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %14 to i32
  %15 = load i32, i32* %rval, align 4
  %or9 = or i32 %15, %conv8
  store i32 %or9, i32* %rval, align 4
  %16 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %16, i32 0, i32 27
  %17 = load i32, i32* %bitindex, align 4
  %18 = load i32, i32* %rval, align 4
  %shl10 = shl i32 %18, %17
  store i32 %shl10, i32* %rval, align 4
  %19 = load i32, i32* %rval, align 4
  %and = and i32 %19, 16777215
  store i32 %and, i32* %rval, align 4
  %20 = load i32, i32* %number_of_bits.addr, align 4
  %21 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex11 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %21, i32 0, i32 27
  %22 = load i32, i32* %bitindex11, align 4
  %add = add nsw i32 %22, %20
  store i32 %add, i32* %bitindex11, align 4
  %23 = load i32, i32* %number_of_bits.addr, align 4
  %sub = sub nsw i32 24, %23
  %24 = load i32, i32* %rval, align 4
  %shr = lshr i32 %24, %sub
  store i32 %shr, i32* %rval, align 4
  %25 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex12 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %25, i32 0, i32 27
  %26 = load i32, i32* %bitindex12, align 4
  %shr13 = ashr i32 %26, 3
  %27 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer14 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %27, i32 0, i32 28
  %28 = load i8*, i8** %wordpointer14, align 4
  %add.ptr = getelementptr inbounds i8, i8* %28, i32 %shr13
  store i8* %add.ptr, i8** %wordpointer14, align 4
  %29 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex15 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %29, i32 0, i32 27
  %30 = load i32, i32* %bitindex15, align 4
  %and16 = and i32 %30, 7
  store i32 %and16, i32* %bitindex15, align 4
  %31 = load i32, i32* %rval, align 4
  store i32 %31, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @getbits_fast(%struct.mpstr_tag* %mp, i32 %number_of_bits) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %number_of_bits.addr = alloca i32, align 4
  %rval = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %number_of_bits, i32* %number_of_bits.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 28
  %1 = load i8*, i8** %wordpointer, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %rval, align 4
  %3 = load i32, i32* %rval, align 4
  %shl = shl i32 %3, 8
  store i32 %shl, i32* %rval, align 4
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 28
  %5 = load i8*, i8** %wordpointer1, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %6 to i32
  %7 = load i32, i32* %rval, align 4
  %or = or i32 %7, %conv3
  store i32 %or, i32* %rval, align 4
  %8 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %8, i32 0, i32 27
  %9 = load i32, i32* %bitindex, align 4
  %10 = load i32, i32* %rval, align 4
  %shl4 = shl i32 %10, %9
  store i32 %shl4, i32* %rval, align 4
  %11 = load i32, i32* %rval, align 4
  %and = and i32 %11, 65535
  store i32 %and, i32* %rval, align 4
  %12 = load i32, i32* %number_of_bits.addr, align 4
  %13 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex5 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %13, i32 0, i32 27
  %14 = load i32, i32* %bitindex5, align 4
  %add = add nsw i32 %14, %12
  store i32 %add, i32* %bitindex5, align 4
  %15 = load i32, i32* %number_of_bits.addr, align 4
  %sub = sub nsw i32 16, %15
  %16 = load i32, i32* %rval, align 4
  %shr = lshr i32 %16, %sub
  store i32 %shr, i32* %rval, align 4
  %17 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex6 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %17, i32 0, i32 27
  %18 = load i32, i32* %bitindex6, align 4
  %shr7 = ashr i32 %18, 3
  %19 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer8 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %19, i32 0, i32 28
  %20 = load i8*, i8** %wordpointer8, align 4
  %add.ptr = getelementptr inbounds i8, i8* %20, i32 %shr7
  store i8* %add.ptr, i8** %wordpointer8, align 4
  %21 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex9 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %21, i32 0, i32 27
  %22 = load i32, i32* %bitindex9, align 4
  %and10 = and i32 %22, 7
  store i32 %and10, i32* %bitindex9, align 4
  %23 = load i32, i32* %rval, align 4
  ret i32 %23
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %mp, i32 %number_of_bits) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %number_of_bits.addr = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %number_of_bits, i32* %number_of_bits.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %1 = load i32, i32* %number_of_bits.addr, align 4
  %call = call i32 @getbits_fast(%struct.mpstr_tag* %0, i32 %1)
  %conv = trunc i32 %call to i8
  ret i8 %conv
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i16 @get_leq_16_bits(%struct.mpstr_tag* %mp, i32 %number_of_bits) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %number_of_bits.addr = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %number_of_bits, i32* %number_of_bits.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %1 = load i32, i32* %number_of_bits.addr, align 4
  %call = call i32 @getbits_fast(%struct.mpstr_tag* %0, i32 %1)
  %conv = trunc i32 %call to i16
  ret i16 %conv
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @set_pointer(%struct.mpstr_tag* %mp, i32 %backstep) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %backstep.addr = alloca i32, align 4
  %bsbufold = alloca i8*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i32 %backstep, i32* %backstep.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 15
  %1 = load i32, i32* %fsizeold, align 4
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load i32, i32* %backstep.addr, align 4
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %3, i32 0, i32 32
  %4 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  %5 = load i32, i32* %backstep.addr, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %4, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.4, i32 0, i32 0), i32 %5)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %6 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsspace = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %6, i32 0, i32 19
  %7 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bsnum = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %7, i32 0, i32 23
  %8 = load i32, i32* %bsnum, align 4
  %sub = sub nsw i32 1, %8
  %arrayidx = getelementptr inbounds [2 x [3904 x i8]], [2 x [3904 x i8]]* %bsspace, i32 0, i32 %sub
  %arraydecay = getelementptr inbounds [3904 x i8], [3904 x i8]* %arrayidx, i32 0, i32 0
  %add.ptr = getelementptr inbounds i8, i8* %arraydecay, i32 512
  store i8* %add.ptr, i8** %bsbufold, align 4
  %9 = load i32, i32* %backstep.addr, align 4
  %10 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %10, i32 0, i32 28
  %11 = load i8*, i8** %wordpointer, align 4
  %idx.neg = sub i32 0, %9
  %add.ptr2 = getelementptr inbounds i8, i8* %11, i32 %idx.neg
  store i8* %add.ptr2, i8** %wordpointer, align 4
  %12 = load i32, i32* %backstep.addr, align 4
  %tobool = icmp ne i32 %12, 0
  br i1 %tobool, label %if.then3, label %if.end9

if.then3:                                         ; preds = %if.end
  %13 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %wordpointer4 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %13, i32 0, i32 28
  %14 = load i8*, i8** %wordpointer4, align 4
  %15 = load i8*, i8** %bsbufold, align 4
  %16 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fsizeold5 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %16, i32 0, i32 15
  %17 = load i32, i32* %fsizeold5, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %15, i32 %17
  %18 = load i32, i32* %backstep.addr, align 4
  %idx.neg7 = sub i32 0, %18
  %add.ptr8 = getelementptr inbounds i8, i8* %add.ptr6, i32 %idx.neg7
  %19 = load i32, i32* %backstep.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %14, i8* align 1 %add.ptr8, i32 %19, i1 false)
  br label %if.end9

if.end9:                                          ; preds = %if.then3, %if.end
  %20 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %bitindex = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %20, i32 0, i32 27
  store i32 0, i32* %bitindex, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
