; ModuleID = 'util.c'
source_filename = "util.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.aligned_pointer_t = type { i8*, i8* }
%union.anon.2 = type { float }

@bitrate_table = external constant [3 x [16 x i32]], align 16
@__const.nearestBitrateFullIndex.full_bitrate_table = private unnamed_addr constant [17 x i32] [i32 8, i32 16, i32 24, i32 32, i32 40, i32 48, i32 56, i32 64, i32 80, i32 96, i32 112, i32 128, i32 160, i32 192, i32 224, i32 256, i32 320], align 16
@init_log_table.init = internal global i32 0, align 4
@log_table = internal global [513 x float] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone
define hidden void @free_id3tag(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %node = alloca %struct.FrameDataNode*, align 4
  %p = alloca i8*, align 4
  %q = alloca i8*, align 4
  %r = alloca i8*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 16
  %language = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 12
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %language, i32 0, i32 0
  store i8 0, i8* %arrayidx, align 8
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 16
  %title = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 2
  %2 = load i8*, i8** %title, align 8
  %cmp = icmp ne i8* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %title3 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec2, i32 0, i32 2
  %4 = load i8*, i8** %title3, align 8
  call void @free(i8* %4)
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %title5 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec4, i32 0, i32 2
  store i8* null, i8** %title5, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 16
  %artist = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec6, i32 0, i32 3
  %7 = load i8*, i8** %artist, align 4
  %cmp7 = icmp ne i8* %7, null
  br i1 %cmp7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %if.end
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec9 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 16
  %artist10 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec9, i32 0, i32 3
  %9 = load i8*, i8** %artist10, align 4
  call void @free(i8* %9)
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec11 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 16
  %artist12 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec11, i32 0, i32 3
  store i8* null, i8** %artist12, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.then8, %if.end
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 16
  %album = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec14, i32 0, i32 4
  %12 = load i8*, i8** %album, align 8
  %cmp15 = icmp ne i8* %12, null
  br i1 %cmp15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %if.end13
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 16
  %album18 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec17, i32 0, i32 4
  %14 = load i8*, i8** %album18, align 8
  call void @free(i8* %14)
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec19 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 16
  %album20 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec19, i32 0, i32 4
  store i8* null, i8** %album20, align 8
  br label %if.end21

if.end21:                                         ; preds = %if.then16, %if.end13
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 16
  %comment = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec22, i32 0, i32 5
  %17 = load i8*, i8** %comment, align 4
  %cmp23 = icmp ne i8* %17, null
  br i1 %cmp23, label %if.then24, label %if.end29

if.then24:                                        ; preds = %if.end21
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec25 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 16
  %comment26 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec25, i32 0, i32 5
  %19 = load i8*, i8** %comment26, align 4
  call void @free(i8* %19)
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec27 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 16
  %comment28 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec27, i32 0, i32 5
  store i8* null, i8** %comment28, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.then24, %if.end21
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec30 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 16
  %albumart = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec30, i32 0, i32 8
  %22 = load i8*, i8** %albumart, align 8
  %cmp31 = icmp ne i8* %22, null
  br i1 %cmp31, label %if.then32, label %if.end39

if.then32:                                        ; preds = %if.end29
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec33 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 16
  %albumart34 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec33, i32 0, i32 8
  %24 = load i8*, i8** %albumart34, align 8
  call void @free(i8* %24)
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 16
  %albumart36 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec35, i32 0, i32 8
  store i8* null, i8** %albumart36, align 8
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec37 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 16
  %albumart_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec37, i32 0, i32 9
  store i32 0, i32* %albumart_size, align 4
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec38 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 16
  %albumart_mimetype = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec38, i32 0, i32 11
  store i32 0, i32* %albumart_mimetype, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.then32, %if.end29
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec40 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %28, i32 0, i32 16
  %v2_head = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec40, i32 0, i32 13
  %29 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head, align 4
  %cmp41 = icmp ne %struct.FrameDataNode* %29, null
  br i1 %cmp41, label %if.then42, label %if.end51

if.then42:                                        ; preds = %if.end39
  %30 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec43 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %30, i32 0, i32 16
  %v2_head44 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec43, i32 0, i32 13
  %31 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head44, align 4
  store %struct.FrameDataNode* %31, %struct.FrameDataNode** %node, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then42
  %32 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %32, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.0, %struct.anon.0* %dsc, i32 0, i32 0
  %b = bitcast %union.anon* %ptr to i8**
  %33 = load i8*, i8** %b, align 4
  store i8* %33, i8** %p, align 4
  %34 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %34, i32 0, i32 4
  %ptr45 = getelementptr inbounds %struct.anon.0, %struct.anon.0* %txt, i32 0, i32 0
  %b46 = bitcast %union.anon* %ptr45 to i8**
  %35 = load i8*, i8** %b46, align 4
  store i8* %35, i8** %q, align 4
  %36 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %37 = bitcast %struct.FrameDataNode* %36 to i8*
  store i8* %37, i8** %r, align 4
  %38 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %nxt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %38, i32 0, i32 0
  %39 = load %struct.FrameDataNode*, %struct.FrameDataNode** %nxt, align 4
  store %struct.FrameDataNode* %39, %struct.FrameDataNode** %node, align 4
  %40 = load i8*, i8** %p, align 4
  call void @free(i8* %40)
  %41 = load i8*, i8** %q, align 4
  call void @free(i8* %41)
  %42 = load i8*, i8** %r, align 4
  call void @free(i8* %42)
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %43 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp47 = icmp ne %struct.FrameDataNode* %43, null
  br i1 %cmp47, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec48 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %44, i32 0, i32 16
  %v2_head49 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec48, i32 0, i32 13
  store %struct.FrameDataNode* null, %struct.FrameDataNode** %v2_head49, align 4
  %45 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec50 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %45, i32 0, i32 16
  %v2_tail = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec50, i32 0, i32 14
  store %struct.FrameDataNode* null, %struct.FrameDataNode** %v2_tail, align 8
  br label %if.end51

if.end51:                                         ; preds = %do.end, %if.end39
  ret void
}

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @freegfc(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cmp = icmp eq %struct.lame_internal_flags* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp1 = icmp sle i32 %1, 640
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 11
  %blackfilt = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt, i32 0, i32 %3
  %4 = load float*, float** %arrayidx, align 4
  %cmp2 = icmp ne float* %4, null
  br i1 %cmp2, label %if.then3, label %if.end10

if.then3:                                         ; preds = %for.body
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 11
  %blackfilt5 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc4, i32 0, i32 4
  %6 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt5, i32 0, i32 %6
  %7 = load float*, float** %arrayidx6, align 4
  %8 = bitcast float* %7 to i8*
  call void @free(i8* %8)
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc7 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 11
  %blackfilt8 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc7, i32 0, i32 4
  %10 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt8, i32 0, i32 %10
  store float* null, float** %arrayidx9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then3, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc11 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 11
  %inbuf_old = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc11, i32 0, i32 3
  %arrayidx12 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old, i32 0, i32 0
  %13 = load float*, float** %arrayidx12, align 8
  %tobool = icmp ne float* %13, null
  br i1 %tobool, label %if.then13, label %if.end20

if.then13:                                        ; preds = %for.end
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 11
  %inbuf_old15 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc14, i32 0, i32 3
  %arrayidx16 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old15, i32 0, i32 0
  %15 = load float*, float** %arrayidx16, align 8
  %16 = bitcast float* %15 to i8*
  call void @free(i8* %16)
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 11
  %inbuf_old18 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc17, i32 0, i32 3
  %arrayidx19 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old18, i32 0, i32 0
  store float* null, float** %arrayidx19, align 8
  br label %if.end20

if.end20:                                         ; preds = %if.then13, %for.end
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 11
  %inbuf_old22 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc21, i32 0, i32 3
  %arrayidx23 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old22, i32 0, i32 1
  %19 = load float*, float** %arrayidx23, align 4
  %tobool24 = icmp ne float* %19, null
  br i1 %tobool24, label %if.then25, label %if.end32

if.then25:                                        ; preds = %if.end20
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc26 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 11
  %inbuf_old27 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc26, i32 0, i32 3
  %arrayidx28 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old27, i32 0, i32 1
  %21 = load float*, float** %arrayidx28, align 4
  %22 = bitcast float* %21 to i8*
  call void @free(i8* %22)
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc29 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 11
  %inbuf_old30 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc29, i32 0, i32 3
  %arrayidx31 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old30, i32 0, i32 1
  store float* null, float** %arrayidx31, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then25, %if.end20
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %24, i32 0, i32 6
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs, i32 0, i32 0
  %25 = load i8*, i8** %buf, align 8
  %cmp33 = icmp ne i8* %25, null
  br i1 %cmp33, label %if.then34, label %if.end39

if.then34:                                        ; preds = %if.end32
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 6
  %buf36 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs35, i32 0, i32 0
  %27 = load i8*, i8** %buf36, align 8
  call void @free(i8* %27)
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs37 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %28, i32 0, i32 6
  %buf38 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs37, i32 0, i32 0
  store i8* null, i8** %buf38, align 8
  br label %if.end39

if.end39:                                         ; preds = %if.then34, %if.end32
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 20
  %bag = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table, i32 0, i32 5
  %30 = load i32*, i32** %bag, align 4
  %tobool40 = icmp ne i32* %30, null
  br i1 %tobool40, label %if.then41, label %if.end47

if.then41:                                        ; preds = %if.end39
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table42 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %31, i32 0, i32 20
  %bag43 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table42, i32 0, i32 5
  %32 = load i32*, i32** %bag43, align 4
  %33 = bitcast i32* %32 to i8*
  call void @free(i8* %33)
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 20
  %bag45 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table44, i32 0, i32 5
  store i32* null, i32** %bag45, align 4
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table46 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %35, i32 0, i32 20
  %size = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table46, i32 0, i32 4
  store i32 0, i32* %size, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then41, %if.end39
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 21
  %37 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %tobool48 = icmp ne %struct.ATH_t* %37, null
  br i1 %tobool48, label %if.then49, label %if.end51

if.then49:                                        ; preds = %if.end47
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH50 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %38, i32 0, i32 21
  %39 = load %struct.ATH_t*, %struct.ATH_t** %ATH50, align 8
  %40 = bitcast %struct.ATH_t* %39 to i8*
  call void @free(i8* %40)
  br label %if.end51

if.end51:                                         ; preds = %if.then49, %if.end47
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 14
  %rgdata = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg, i32 0, i32 0
  %42 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata, align 4
  %tobool52 = icmp ne %struct.replaygain_data* %42, null
  br i1 %tobool52, label %if.then53, label %if.end56

if.then53:                                        ; preds = %if.end51
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg54 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %43, i32 0, i32 14
  %rgdata55 = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg54, i32 0, i32 0
  %44 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata55, align 4
  %45 = bitcast %struct.replaygain_data* %44 to i8*
  call void @free(i8* %45)
  br label %if.end56

if.end56:                                         ; preds = %if.then53, %if.end51
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc57 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %46, i32 0, i32 11
  %in_buffer_0 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc57, i32 0, i32 15
  %47 = load float*, float** %in_buffer_0, align 8
  %tobool58 = icmp ne float* %47, null
  br i1 %tobool58, label %if.then59, label %if.end62

if.then59:                                        ; preds = %if.end56
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc60 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %48, i32 0, i32 11
  %in_buffer_061 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc60, i32 0, i32 15
  %49 = load float*, float** %in_buffer_061, align 8
  %50 = bitcast float* %49 to i8*
  call void @free(i8* %50)
  br label %if.end62

if.end62:                                         ; preds = %if.then59, %if.end56
  %51 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc63 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %51, i32 0, i32 11
  %in_buffer_1 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc63, i32 0, i32 16
  %52 = load float*, float** %in_buffer_1, align 4
  %tobool64 = icmp ne float* %52, null
  br i1 %tobool64, label %if.then65, label %if.end68

if.then65:                                        ; preds = %if.end62
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc66 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %53, i32 0, i32 11
  %in_buffer_167 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc66, i32 0, i32 16
  %54 = load float*, float** %in_buffer_167, align 4
  %55 = bitcast float* %54 to i8*
  call void @free(i8* %55)
  br label %if.end68

if.end68:                                         ; preds = %if.then65, %if.end62
  %56 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @free_id3tag(%struct.lame_internal_flags* %56)
  %57 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @free_global_data(%struct.lame_internal_flags* %57)
  %58 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %59 = bitcast %struct.lame_internal_flags* %58 to i8*
  call void @free(i8* %59)
  br label %return

return:                                           ; preds = %if.end68, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @free_global_data(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end18

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 22
  %2 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %tobool1 = icmp ne %struct.PsyConst_t* %2, null
  br i1 %tobool1, label %if.then, label %if.end18

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 22
  %4 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy2, align 4
  %l = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %4, i32 0, i32 2
  %s3 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l, i32 0, i32 13
  %5 = load float*, float** %s3, align 4
  %tobool3 = icmp ne float* %5, null
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 22
  %7 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy5, align 4
  %l6 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %7, i32 0, i32 2
  %s37 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %l6, i32 0, i32 13
  %8 = load float*, float** %s37, align 4
  %9 = bitcast float* %8 to i8*
  call void @free(i8* %9)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy8 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 22
  %11 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy8, align 4
  %s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %11, i32 0, i32 3
  %s39 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s, i32 0, i32 13
  %12 = load float*, float** %s39, align 4
  %tobool10 = icmp ne float* %12, null
  br i1 %tobool10, label %if.then11, label %if.end15

if.then11:                                        ; preds = %if.end
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 22
  %14 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy12, align 4
  %s13 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %14, i32 0, i32 3
  %s314 = getelementptr inbounds %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t* %s13, i32 0, i32 13
  %15 = load float*, float** %s314, align 4
  %16 = bitcast float* %15 to i8*
  call void @free(i8* %16)
  br label %if.end15

if.end15:                                         ; preds = %if.then11, %if.end
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 22
  %18 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy16, align 4
  %19 = bitcast %struct.PsyConst_t* %18 to i8*
  call void @free(i8* %19)
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 22
  store %struct.PsyConst_t* null, %struct.PsyConst_t** %cd_psy17, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.end15, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @calloc_aligned(%struct.aligned_pointer_t* %ptr, i32 %size, i32 %bytes) #0 {
entry:
  %ptr.addr = alloca %struct.aligned_pointer_t*, align 4
  %size.addr = alloca i32, align 4
  %bytes.addr = alloca i32, align 4
  store %struct.aligned_pointer_t* %ptr, %struct.aligned_pointer_t** %ptr.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %bytes, i32* %bytes.addr, align 4
  %0 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %tobool = icmp ne %struct.aligned_pointer_t* %0, null
  br i1 %tobool, label %if.then, label %if.end18

if.then:                                          ; preds = %entry
  %1 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %1, i32 0, i32 1
  %2 = load i8*, i8** %pointer, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %if.end17, label %if.then2

if.then2:                                         ; preds = %if.then
  %3 = load i32, i32* %size.addr, align 4
  %4 = load i32, i32* %bytes.addr, align 4
  %add = add i32 %3, %4
  %call = call i8* @malloc(i32 %add)
  %5 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer3 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %5, i32 0, i32 1
  store i8* %call, i8** %pointer3, align 4
  %6 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer4 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %6, i32 0, i32 1
  %7 = load i8*, i8** %pointer4, align 4
  %cmp = icmp ne i8* %7, null
  br i1 %cmp, label %if.then5, label %if.else14

if.then5:                                         ; preds = %if.then2
  %8 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer6 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %8, i32 0, i32 1
  %9 = load i8*, i8** %pointer6, align 4
  %10 = load i32, i32* %size.addr, align 4
  %11 = load i32, i32* %bytes.addr, align 4
  %add7 = add i32 %10, %11
  call void @llvm.memset.p0i8.i32(i8* align 1 %9, i8 0, i32 %add7, i1 false)
  %12 = load i32, i32* %bytes.addr, align 4
  %cmp8 = icmp ugt i32 %12, 0
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.then5
  %13 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer10 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %13, i32 0, i32 1
  %14 = load i8*, i8** %pointer10, align 4
  %15 = ptrtoint i8* %14 to i32
  %16 = load i32, i32* %bytes.addr, align 4
  %add11 = add i32 %15, %16
  %sub = sub i32 %add11, 1
  %17 = load i32, i32* %bytes.addr, align 4
  %div = udiv i32 %sub, %17
  %18 = load i32, i32* %bytes.addr, align 4
  %mul = mul i32 %div, %18
  %19 = inttoptr i32 %mul to i8*
  %20 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %aligned = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %20, i32 0, i32 0
  store i8* %19, i8** %aligned, align 4
  br label %if.end

if.else:                                          ; preds = %if.then5
  %21 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer12 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %21, i32 0, i32 1
  %22 = load i8*, i8** %pointer12, align 4
  %23 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %aligned13 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %23, i32 0, i32 0
  store i8* %22, i8** %aligned13, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then9
  br label %if.end16

if.else14:                                        ; preds = %if.then2
  %24 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %aligned15 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %24, i32 0, i32 0
  store i8* null, i8** %aligned15, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.else14, %if.end
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %entry
  ret void
}

declare i8* @malloc(i32) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden void @free_aligned(%struct.aligned_pointer_t* %ptr) #0 {
entry:
  %ptr.addr = alloca %struct.aligned_pointer_t*, align 4
  store %struct.aligned_pointer_t* %ptr, %struct.aligned_pointer_t** %ptr.addr, align 4
  %0 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %tobool = icmp ne %struct.aligned_pointer_t* %0, null
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %1, i32 0, i32 1
  %2 = load i8*, i8** %pointer, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %3 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer3 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %3, i32 0, i32 1
  %4 = load i8*, i8** %pointer3, align 4
  call void @free(i8* %4)
  %5 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %pointer4 = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %5, i32 0, i32 1
  store i8* null, i8** %pointer4, align 4
  %6 = load %struct.aligned_pointer_t*, %struct.aligned_pointer_t** %ptr.addr, align 4
  %aligned = getelementptr inbounds %struct.aligned_pointer_t, %struct.aligned_pointer_t* %6, i32 0, i32 0
  store i8* null, i8** %aligned, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @ATHformula(%struct.SessionConfig_t* %cfg, float %f) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %f.addr = alloca float, align 4
  %ath = alloca float, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  store float %f, float* %f.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATHtype = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 48
  %1 = load i32, i32* %ATHtype, align 4
  switch i32 %1, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb3
    i32 3, label %sw.bb5
    i32 4, label %sw.bb7
    i32 5, label %sw.bb9
  ]

sw.bb:                                            ; preds = %entry
  %2 = load float, float* %f.addr, align 4
  %call = call float @ATHformula_GB(float %2, float 9.000000e+00, float 0x3FB99999A0000000, float 2.400000e+01)
  store float %call, float* %ath, align 4
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  %3 = load float, float* %f.addr, align 4
  %call2 = call float @ATHformula_GB(float %3, float -1.000000e+00, float 0x3FB99999A0000000, float 2.400000e+01)
  store float %call2, float* %ath, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %entry
  %4 = load float, float* %f.addr, align 4
  %call4 = call float @ATHformula_GB(float %4, float 0.000000e+00, float 0x3FB99999A0000000, float 2.400000e+01)
  store float %call4, float* %ath, align 4
  br label %sw.epilog

sw.bb5:                                           ; preds = %entry
  %5 = load float, float* %f.addr, align 4
  %call6 = call float @ATHformula_GB(float %5, float 1.000000e+00, float 0x3FB99999A0000000, float 2.400000e+01)
  %add = fadd float %call6, 6.000000e+00
  store float %add, float* %ath, align 4
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %6 = load float, float* %f.addr, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATHcurve = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 47
  %8 = load float, float* %ATHcurve, align 4
  %call8 = call float @ATHformula_GB(float %6, float %8, float 0x3FB99999A0000000, float 2.400000e+01)
  store float %call8, float* %ath, align 4
  br label %sw.epilog

sw.bb9:                                           ; preds = %entry
  %9 = load float, float* %f.addr, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATHcurve10 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 47
  %11 = load float, float* %ATHcurve10, align 4
  %call11 = call float @ATHformula_GB(float %9, float %11, float 0x400B47AE20000000, float 0x40301999A0000000)
  store float %call11, float* %ath, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %12 = load float, float* %f.addr, align 4
  %call12 = call float @ATHformula_GB(float %12, float 0.000000e+00, float 0x3FB99999A0000000, float 2.400000e+01)
  store float %call12, float* %ath, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb9, %sw.bb7, %sw.bb5, %sw.bb3, %sw.bb1, %sw.bb
  %13 = load float, float* %ath, align 4
  ret float %13
}

; Function Attrs: noinline nounwind optnone
define internal float @ATHformula_GB(float %f, float %value, float %f_min, float %f_max) #0 {
entry:
  %f.addr = alloca float, align 4
  %value.addr = alloca float, align 4
  %f_min.addr = alloca float, align 4
  %f_max.addr = alloca float, align 4
  %ath = alloca float, align 4
  store float %f, float* %f.addr, align 4
  store float %value, float* %value.addr, align 4
  store float %f_min, float* %f_min.addr, align 4
  store float %f_max, float* %f_max.addr, align 4
  %0 = load float, float* %f.addr, align 4
  %conv = fpext float %0 to double
  %cmp = fcmp olt double %conv, -3.000000e-01
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 3.410000e+03, float* %f.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %f.addr, align 4
  %div = fdiv float %1, 1.000000e+03
  store float %div, float* %f.addr, align 4
  %2 = load float, float* %f_min.addr, align 4
  %3 = load float, float* %f.addr, align 4
  %cmp2 = fcmp ogt float %2, %3
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %4 = load float, float* %f_min.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %5 = load float, float* %f.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %4, %cond.true ], [ %5, %cond.false ]
  store float %cond, float* %f.addr, align 4
  %6 = load float, float* %f_max.addr, align 4
  %7 = load float, float* %f.addr, align 4
  %cmp4 = fcmp olt float %6, %7
  br i1 %cmp4, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %8 = load float, float* %f_max.addr, align 4
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %9 = load float, float* %f.addr, align 4
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %cond9 = phi float [ %8, %cond.true6 ], [ %9, %cond.false7 ]
  store float %cond9, float* %f.addr, align 4
  %10 = load float, float* %f.addr, align 4
  %conv10 = fpext float %10 to double
  %11 = call double @llvm.pow.f64(double %conv10, double -8.000000e-01)
  %mul = fmul double 3.640000e+00, %11
  %12 = load float, float* %f.addr, align 4
  %conv11 = fpext float %12 to double
  %sub = fsub double %conv11, 3.400000e+00
  %13 = call double @llvm.pow.f64(double %sub, double 2.000000e+00)
  %mul12 = fmul double -6.000000e-01, %13
  %14 = call double @llvm.exp.f64(double %mul12)
  %mul13 = fmul double 6.800000e+00, %14
  %sub14 = fsub double %mul, %mul13
  %15 = load float, float* %f.addr, align 4
  %conv15 = fpext float %15 to double
  %sub16 = fsub double %conv15, 0x4021666666666666
  %16 = call double @llvm.pow.f64(double %sub16, double 2.000000e+00)
  %mul17 = fmul double -1.500000e-01, %16
  %17 = call double @llvm.exp.f64(double %mul17)
  %mul18 = fmul double 6.000000e+00, %17
  %add = fadd double %sub14, %mul18
  %18 = load float, float* %value.addr, align 4
  %conv19 = fpext float %18 to double
  %mul20 = fmul double 4.000000e-02, %conv19
  %add21 = fadd double 6.000000e-01, %mul20
  %mul22 = fmul double %add21, 1.000000e-03
  %19 = load float, float* %f.addr, align 4
  %conv23 = fpext float %19 to double
  %20 = call double @llvm.pow.f64(double %conv23, double 4.000000e+00)
  %mul24 = fmul double %mul22, %20
  %add25 = fadd double %add, %mul24
  %conv26 = fptrunc double %add25 to float
  store float %conv26, float* %ath, align 4
  %21 = load float, float* %ath, align 4
  ret float %21
}

; Function Attrs: noinline nounwind optnone
define hidden float @freq2bark(float %freq) #0 {
entry:
  %freq.addr = alloca float, align 4
  store float %freq, float* %freq.addr, align 4
  %0 = load float, float* %freq.addr, align 4
  %cmp = fcmp olt float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %freq.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %freq.addr, align 4
  %conv = fpext float %1 to double
  %mul = fmul double %conv, 1.000000e-03
  %conv1 = fptrunc double %mul to float
  store float %conv1, float* %freq.addr, align 4
  %2 = load float, float* %freq.addr, align 4
  %conv2 = fpext float %2 to double
  %mul3 = fmul double 7.600000e-01, %conv2
  %call = call double @atan(double %mul3) #7
  %mul4 = fmul double 1.300000e+01, %call
  %3 = load float, float* %freq.addr, align 4
  %4 = load float, float* %freq.addr, align 4
  %mul5 = fmul float %3, %4
  %conv6 = fpext float %mul5 to double
  %div = fdiv double %conv6, 5.625000e+01
  %call7 = call double @atan(double %div) #7
  %mul8 = fmul double 3.500000e+00, %call7
  %add = fadd double %mul4, %mul8
  %conv9 = fptrunc double %add to float
  ret float %conv9
}

; Function Attrs: nounwind readnone
declare double @atan(double) #3

; Function Attrs: noinline nounwind optnone
define hidden i32 @FindNearestBitrate(i32 %bRate, i32 %version, i32 %samplerate) #0 {
entry:
  %bRate.addr = alloca i32, align 4
  %version.addr = alloca i32, align 4
  %samplerate.addr = alloca i32, align 4
  %bitrate = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %bRate, i32* %bRate.addr, align 4
  store i32 %version, i32* %version.addr, align 4
  store i32 %samplerate, i32* %samplerate.addr, align 4
  %0 = load i32, i32* %samplerate.addr, align 4
  %cmp = icmp slt i32 %0, 16000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 2, i32* %version.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i32, i32* %version.addr, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %1
  %arrayidx1 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 1
  %2 = load i32, i32* %arrayidx1, align 4
  store i32 %2, i32* %bitrate, align 4
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %cmp2 = icmp sle i32 %3, 14
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %version.addr, align 4
  %arrayidx3 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %4
  %5 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx3, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx4, align 4
  %cmp5 = icmp sgt i32 %6, 0
  br i1 %cmp5, label %if.then6, label %if.end31

if.then6:                                         ; preds = %for.body
  %7 = load i32, i32* %version.addr, align 4
  %arrayidx7 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %7
  %8 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx7, i32 0, i32 %8
  %9 = load i32, i32* %arrayidx8, align 4
  %10 = load i32, i32* %bRate.addr, align 4
  %sub = sub nsw i32 %9, %10
  %cmp9 = icmp sgt i32 %sub, 0
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then6
  %11 = load i32, i32* %version.addr, align 4
  %arrayidx10 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %11
  %12 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx10, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx11, align 4
  %14 = load i32, i32* %bRate.addr, align 4
  %sub12 = sub nsw i32 %13, %14
  br label %cond.end

cond.false:                                       ; preds = %if.then6
  %15 = load i32, i32* %version.addr, align 4
  %arrayidx13 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %15
  %16 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx13, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx14, align 4
  %18 = load i32, i32* %bRate.addr, align 4
  %sub15 = sub nsw i32 %17, %18
  %sub16 = sub nsw i32 0, %sub15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub12, %cond.true ], [ %sub16, %cond.false ]
  %19 = load i32, i32* %bitrate, align 4
  %20 = load i32, i32* %bRate.addr, align 4
  %sub17 = sub nsw i32 %19, %20
  %cmp18 = icmp sgt i32 %sub17, 0
  br i1 %cmp18, label %cond.true19, label %cond.false21

cond.true19:                                      ; preds = %cond.end
  %21 = load i32, i32* %bitrate, align 4
  %22 = load i32, i32* %bRate.addr, align 4
  %sub20 = sub nsw i32 %21, %22
  br label %cond.end24

cond.false21:                                     ; preds = %cond.end
  %23 = load i32, i32* %bitrate, align 4
  %24 = load i32, i32* %bRate.addr, align 4
  %sub22 = sub nsw i32 %23, %24
  %sub23 = sub nsw i32 0, %sub22
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false21, %cond.true19
  %cond25 = phi i32 [ %sub20, %cond.true19 ], [ %sub23, %cond.false21 ]
  %cmp26 = icmp slt i32 %cond, %cond25
  br i1 %cmp26, label %if.then27, label %if.end30

if.then27:                                        ; preds = %cond.end24
  %25 = load i32, i32* %version.addr, align 4
  %arrayidx28 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %25
  %26 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx28, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx29, align 4
  store i32 %27, i32* %bitrate, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then27, %cond.end24
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %28 = load i32, i32* %i, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %29 = load i32, i32* %bitrate, align 4
  ret i32 %29
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @nearestBitrateFullIndex(i16 zeroext %bitrate) #0 {
entry:
  %retval = alloca i32, align 4
  %bitrate.addr = alloca i16, align 2
  %full_bitrate_table = alloca [17 x i32], align 16
  %lower_range = alloca i32, align 4
  %lower_range_kbps = alloca i32, align 4
  %upper_range = alloca i32, align 4
  %upper_range_kbps = alloca i32, align 4
  %b = alloca i32, align 4
  store i16 %bitrate, i16* %bitrate.addr, align 2
  %0 = bitcast [17 x i32]* %full_bitrate_table to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %0, i8* align 16 bitcast ([17 x i32]* @__const.nearestBitrateFullIndex.full_bitrate_table to i8*), i32 68, i1 false)
  store i32 0, i32* %lower_range, align 4
  store i32 0, i32* %lower_range_kbps, align 4
  store i32 0, i32* %upper_range, align 4
  store i32 0, i32* %upper_range_kbps, align 4
  %arrayidx = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 16
  %1 = load i32, i32* %arrayidx, align 16
  store i32 %1, i32* %upper_range_kbps, align 4
  store i32 16, i32* %upper_range, align 4
  %arrayidx1 = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 16
  %2 = load i32, i32* %arrayidx1, align 16
  store i32 %2, i32* %lower_range_kbps, align 4
  store i32 16, i32* %lower_range, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %b, align 4
  %cmp = icmp slt i32 %3, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i16, i16* %bitrate.addr, align 2
  %conv = zext i16 %4 to i32
  %5 = load i32, i32* %b, align 4
  %add = add nsw i32 %5, 1
  %arrayidx2 = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 %add
  %6 = load i32, i32* %arrayidx2, align 4
  %cmp3 = icmp sgt i32 %conv, %6
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %7 = load i16, i16* %bitrate.addr, align 2
  %conv5 = zext i16 %7 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %8 = load i32, i32* %b, align 4
  %add6 = add nsw i32 %8, 1
  %arrayidx7 = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 %add6
  %9 = load i32, i32* %arrayidx7, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv5, %cond.true ], [ %9, %cond.false ]
  %10 = load i16, i16* %bitrate.addr, align 2
  %conv8 = zext i16 %10 to i32
  %cmp9 = icmp ne i32 %cond, %conv8
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %11 = load i32, i32* %b, align 4
  %add11 = add nsw i32 %11, 1
  %arrayidx12 = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 %add11
  %12 = load i32, i32* %arrayidx12, align 4
  store i32 %12, i32* %upper_range_kbps, align 4
  %13 = load i32, i32* %b, align 4
  %add13 = add nsw i32 %13, 1
  store i32 %add13, i32* %upper_range, align 4
  %14 = load i32, i32* %b, align 4
  %arrayidx14 = getelementptr inbounds [17 x i32], [17 x i32]* %full_bitrate_table, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx14, align 4
  store i32 %15, i32* %lower_range_kbps, align 4
  %16 = load i32, i32* %b, align 4
  store i32 %16, i32* %lower_range, align 4
  br label %for.end

if.end:                                           ; preds = %cond.end
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %b, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %18 = load i32, i32* %upper_range_kbps, align 4
  %19 = load i16, i16* %bitrate.addr, align 2
  %conv15 = zext i16 %19 to i32
  %sub = sub nsw i32 %18, %conv15
  %20 = load i16, i16* %bitrate.addr, align 2
  %conv16 = zext i16 %20 to i32
  %21 = load i32, i32* %lower_range_kbps, align 4
  %sub17 = sub nsw i32 %conv16, %21
  %cmp18 = icmp sgt i32 %sub, %sub17
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %for.end
  %22 = load i32, i32* %lower_range, align 4
  store i32 %22, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %for.end
  %23 = load i32, i32* %upper_range, align 4
  store i32 %23, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define hidden i32 @map2MP3Frequency(i32 %freq) #0 {
entry:
  %retval = alloca i32, align 4
  %freq.addr = alloca i32, align 4
  store i32 %freq, i32* %freq.addr, align 4
  %0 = load i32, i32* %freq.addr, align 4
  %cmp = icmp sle i32 %0, 8000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 8000, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %freq.addr, align 4
  %cmp1 = icmp sle i32 %1, 11025
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 11025, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load i32, i32* %freq.addr, align 4
  %cmp4 = icmp sle i32 %2, 12000
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 12000, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %3 = load i32, i32* %freq.addr, align 4
  %cmp7 = icmp sle i32 %3, 16000
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 16000, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %4 = load i32, i32* %freq.addr, align 4
  %cmp10 = icmp sle i32 %4, 22050
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store i32 22050, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end9
  %5 = load i32, i32* %freq.addr, align 4
  %cmp13 = icmp sle i32 %5, 24000
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 24000, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end12
  %6 = load i32, i32* %freq.addr, align 4
  %cmp16 = icmp sle i32 %6, 32000
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  store i32 32000, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end15
  %7 = load i32, i32* %freq.addr, align 4
  %cmp19 = icmp sle i32 %7, 44100
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end18
  store i32 44100, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end18
  store i32 48000, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20, %if.then17, %if.then14, %if.then11, %if.then8, %if.then5, %if.then2, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @BitrateIndex(i32 %bRate, i32 %version, i32 %samplerate) #0 {
entry:
  %retval = alloca i32, align 4
  %bRate.addr = alloca i32, align 4
  %version.addr = alloca i32, align 4
  %samplerate.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %bRate, i32* %bRate.addr, align 4
  store i32 %version, i32* %version.addr, align 4
  store i32 %samplerate, i32* %samplerate.addr, align 4
  %0 = load i32, i32* %samplerate.addr, align 4
  %cmp = icmp slt i32 %0, 16000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 2, i32* %version.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp1 = icmp sle i32 %1, 14
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %version.addr, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %2
  %3 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx2, align 4
  %cmp3 = icmp sgt i32 %4, 0
  br i1 %cmp3, label %if.then4, label %if.end10

if.then4:                                         ; preds = %for.body
  %5 = load i32, i32* %version.addr, align 4
  %arrayidx5 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %5
  %6 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx5, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx6, align 4
  %8 = load i32, i32* %bRate.addr, align 4
  %cmp7 = icmp eq i32 %7, %8
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then4
  %9 = load i32, i32* %i, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.then4
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then8
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @SmpFrqIndex(i32 %sample_freq, i32* %version) #0 {
entry:
  %retval = alloca i32, align 4
  %sample_freq.addr = alloca i32, align 4
  %version.addr = alloca i32*, align 4
  store i32 %sample_freq, i32* %sample_freq.addr, align 4
  store i32* %version, i32** %version.addr, align 4
  %0 = load i32, i32* %sample_freq.addr, align 4
  switch i32 %0, label %sw.default [
    i32 44100, label %sw.bb
    i32 48000, label %sw.bb1
    i32 32000, label %sw.bb2
    i32 22050, label %sw.bb3
    i32 24000, label %sw.bb4
    i32 16000, label %sw.bb5
    i32 11025, label %sw.bb6
    i32 12000, label %sw.bb7
    i32 8000, label %sw.bb8
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32*, i32** %version.addr, align 4
  store i32 1, i32* %1, align 4
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load i32*, i32** %version.addr, align 4
  store i32 1, i32* %2, align 4
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  %3 = load i32*, i32** %version.addr, align 4
  store i32 1, i32* %3, align 4
  store i32 2, i32* %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  %4 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %4, align 4
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  %5 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %5, align 4
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry
  %6 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %6, align 4
  store i32 2, i32* %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  %7 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %7, align 4
  store i32 0, i32* %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry
  %8 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %8, align 4
  store i32 1, i32* %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  %9 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %9, align 4
  store i32 2, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %10 = load i32*, i32** %version.addr, align 4
  store i32 0, i32* %10, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @isResamplingNecessary(%struct.SessionConfig_t* %cfg) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %l = alloca i32, align 4
  %h = alloca i32, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 12
  %1 = load i32, i32* %samplerate_out, align 4
  %conv = sitofp i32 %1 to float
  %mul = fmul float %conv, 0x3FEFFBE760000000
  %conv1 = fptosi float %mul to i32
  store i32 %conv1, i32* %l, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_out2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 12
  %3 = load i32, i32* %samplerate_out2, align 4
  %conv3 = sitofp i32 %3 to float
  %mul4 = fmul float %conv3, 0x3FF0020C40000000
  %conv5 = fptosi float %mul4 to i32
  store i32 %conv5, i32* %h, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 11
  %5 = load i32, i32* %samplerate_in, align 4
  %6 = load i32, i32* %l, align 4
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %7 = load i32, i32* %h, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_in7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 11
  %9 = load i32, i32* %samplerate_in7, align 4
  %cmp8 = icmp slt i32 %7, %9
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %10 = phi i1 [ true, %entry ], [ %cmp8, %lor.rhs ]
  %11 = zext i1 %10 to i64
  %cond = select i1 %10, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define hidden void @fill_buffer(%struct.lame_internal_flags* %gfc, float** %mfbuf, float** %in_buffer, i32 %nsamples, i32* %n_in, i32* %n_out) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %mfbuf.addr = alloca float**, align 4
  %in_buffer.addr = alloca float**, align 4
  %nsamples.addr = alloca i32, align 4
  %n_in.addr = alloca i32*, align 4
  %n_out.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %mf_size = alloca i32, align 4
  %framesize = alloca i32, align 4
  %nout = alloca i32, align 4
  %ch = alloca i32, align 4
  %nch = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %mfbuf, float*** %mfbuf.addr, align 4
  store float** %in_buffer, float*** %in_buffer.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i32* %n_in, i32** %n_in.addr, align 4
  store i32* %n_out, i32** %n_out.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  %mf_size2 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 19
  %2 = load i32, i32* %mf_size2, align 4
  store i32 %2, i32* %mf_size, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 15
  %4 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %4
  store i32 %mul, i32* %framesize, align 4
  store i32 0, i32* %ch, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 14
  %6 = load i32, i32* %channels_out, align 4
  store i32 %6, i32* %nch, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call = call i32 @isResamplingNecessary(%struct.SessionConfig_t* %7)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %9 = load float**, float*** %mfbuf.addr, align 4
  %10 = load i32, i32* %ch, align 4
  %arrayidx = getelementptr inbounds float*, float** %9, i32 %10
  %11 = load float*, float** %arrayidx, align 4
  %12 = load i32, i32* %mf_size, align 4
  %arrayidx3 = getelementptr inbounds float, float* %11, i32 %12
  %13 = load i32, i32* %framesize, align 4
  %14 = load float**, float*** %in_buffer.addr, align 4
  %15 = load i32, i32* %ch, align 4
  %arrayidx4 = getelementptr inbounds float*, float** %14, i32 %15
  %16 = load float*, float** %arrayidx4, align 4
  %17 = load i32, i32* %nsamples.addr, align 4
  %18 = load i32*, i32** %n_in.addr, align 4
  %19 = load i32, i32* %ch, align 4
  %call5 = call i32 @fill_buffer_resample(%struct.lame_internal_flags* %8, float* %arrayidx3, i32 %13, float* %16, i32 %17, i32* %18, i32 %19)
  store i32 %call5, i32* %nout, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %20 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %ch, align 4
  %21 = load i32, i32* %nch, align 4
  %cmp = icmp slt i32 %inc, %21
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %22 = load i32, i32* %nout, align 4
  %23 = load i32*, i32** %n_out.addr, align 4
  store i32 %22, i32* %23, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = load i32, i32* %framesize, align 4
  %25 = load i32, i32* %nsamples.addr, align 4
  %cmp6 = icmp slt i32 %24, %25
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %26 = load i32, i32* %framesize, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %27 = load i32, i32* %nsamples.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %26, %cond.true ], [ %27, %cond.false ]
  store i32 %cond, i32* %nout, align 4
  br label %do.body7

do.body7:                                         ; preds = %do.cond13, %cond.end
  %28 = load float**, float*** %mfbuf.addr, align 4
  %29 = load i32, i32* %ch, align 4
  %arrayidx8 = getelementptr inbounds float*, float** %28, i32 %29
  %30 = load float*, float** %arrayidx8, align 4
  %31 = load i32, i32* %mf_size, align 4
  %arrayidx9 = getelementptr inbounds float, float* %30, i32 %31
  %32 = bitcast float* %arrayidx9 to i8*
  %33 = load float**, float*** %in_buffer.addr, align 4
  %34 = load i32, i32* %ch, align 4
  %arrayidx10 = getelementptr inbounds float*, float** %33, i32 %34
  %35 = load float*, float** %arrayidx10, align 4
  %arrayidx11 = getelementptr inbounds float, float* %35, i32 0
  %36 = bitcast float* %arrayidx11 to i8*
  %37 = load i32, i32* %nout, align 4
  %mul12 = mul i32 %37, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %36, i32 %mul12, i1 false)
  br label %do.cond13

do.cond13:                                        ; preds = %do.body7
  %38 = load i32, i32* %ch, align 4
  %inc14 = add nsw i32 %38, 1
  store i32 %inc14, i32* %ch, align 4
  %39 = load i32, i32* %nch, align 4
  %cmp15 = icmp slt i32 %inc14, %39
  br i1 %cmp15, label %do.body7, label %do.end16

do.end16:                                         ; preds = %do.cond13
  %40 = load i32, i32* %nout, align 4
  %41 = load i32*, i32** %n_out.addr, align 4
  store i32 %40, i32* %41, align 4
  %42 = load i32, i32* %nout, align 4
  %43 = load i32*, i32** %n_in.addr, align 4
  store i32 %42, i32* %43, align 4
  br label %if.end

if.end:                                           ; preds = %do.end16, %do.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @fill_buffer_resample(%struct.lame_internal_flags* %gfc, float* %outbuf, i32 %desired_len, float* %inbuf, i32 %len, i32* %num_used, i32 %ch) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %outbuf.addr = alloca float*, align 4
  %desired_len.addr = alloca i32, align 4
  %inbuf.addr = alloca float*, align 4
  %len.addr = alloca i32, align 4
  %num_used.addr = alloca i32*, align 4
  %ch.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %resample_ratio = alloca double, align 8
  %BLACKSIZE = alloca i32, align 4
  %offset = alloca float, align 4
  %xvalue = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %filter_l = alloca i32, align 4
  %fcn = alloca float, align 4
  %intratio = alloca float, align 4
  %inbuf_old = alloca float*, align 4
  %bpc = alloca i32, align 4
  %sum = alloca float, align 4
  %time0 = alloca double, align 8
  %joff = alloca i32, align 4
  %j2 = alloca i32, align 4
  %y = alloca float, align 4
  %n_shift = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %outbuf, float** %outbuf.addr, align 4
  store i32 %desired_len, i32* %desired_len.addr, align 4
  store float* %inbuf, float** %inbuf.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32* %num_used, i32** %num_used.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 11
  %3 = load i32, i32* %samplerate_in, align 4
  %conv = sitofp i32 %3 to double
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 12
  %5 = load i32, i32* %samplerate_out, align 4
  %conv2 = sitofp i32 %5 to double
  %div = fdiv double %conv, %conv2
  store double %div, double* %resample_ratio, align 8
  store i32 0, i32* %j, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out3 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 12
  %7 = load i32, i32* %samplerate_out3, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out4 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 12
  %9 = load i32, i32* %samplerate_out4, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 11
  %11 = load i32, i32* %samplerate_in5, align 4
  %call = call i32 @gcd(i32 %9, i32 %11)
  %div6 = sdiv i32 %7, %call
  store i32 %div6, i32* %bpc, align 4
  %12 = load i32, i32* %bpc, align 4
  %cmp = icmp sgt i32 %12, 320
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 320, i32* %bpc, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load double, double* %resample_ratio, align 8
  %14 = load double, double* %resample_ratio, align 8
  %add = fadd double 5.000000e-01, %14
  %15 = call double @llvm.floor.f64(double %add)
  %sub = fsub double %13, %15
  %16 = call double @llvm.fabs.f64(double %sub)
  %cmp8 = fcmp olt double %16, 0x3E80000000000000
  %conv9 = zext i1 %cmp8 to i32
  %conv10 = sitofp i32 %conv9 to float
  store float %conv10, float* %intratio, align 4
  %17 = load double, double* %resample_ratio, align 8
  %div11 = fdiv double 1.000000e+00, %17
  %conv12 = fptrunc double %div11 to float
  store float %conv12, float* %fcn, align 4
  %18 = load float, float* %fcn, align 4
  %conv13 = fpext float %18 to double
  %cmp14 = fcmp ogt double %conv13, 1.000000e+00
  br i1 %cmp14, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end
  store float 1.000000e+00, float* %fcn, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end
  store i32 31, i32* %filter_l, align 4
  %19 = load float, float* %intratio, align 4
  %20 = load i32, i32* %filter_l, align 4
  %conv18 = sitofp i32 %20 to float
  %add19 = fadd float %conv18, %19
  %conv20 = fptosi float %add19 to i32
  store i32 %conv20, i32* %filter_l, align 4
  %21 = load i32, i32* %filter_l, align 4
  %add21 = add nsw i32 %21, 1
  store i32 %add21, i32* %BLACKSIZE, align 4
  %22 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %fill_buffer_resample_init = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %22, i32 0, i32 4
  %23 = load i32, i32* %fill_buffer_resample_init, align 8
  %cmp22 = icmp eq i32 %23, 0
  br i1 %cmp22, label %if.then24, label %if.end77

if.then24:                                        ; preds = %if.end17
  %24 = load i32, i32* %BLACKSIZE, align 4
  %call25 = call i8* @calloc(i32 %24, i32 4)
  %25 = bitcast i8* %call25 to float*
  %26 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %inbuf_old26 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %26, i32 0, i32 3
  %arrayidx = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old26, i32 0, i32 0
  store float* %25, float** %arrayidx, align 8
  %27 = load i32, i32* %BLACKSIZE, align 4
  %call27 = call i8* @calloc(i32 %27, i32 4)
  %28 = bitcast i8* %call27 to float*
  %29 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %inbuf_old28 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %29, i32 0, i32 3
  %arrayidx29 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old28, i32 0, i32 1
  store float* %28, float** %arrayidx29, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then24
  %30 = load i32, i32* %i, align 4
  %31 = load i32, i32* %bpc, align 4
  %mul = mul nsw i32 2, %31
  %cmp30 = icmp sle i32 %30, %mul
  br i1 %cmp30, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %32 = load i32, i32* %BLACKSIZE, align 4
  %call32 = call i8* @calloc(i32 %32, i32 4)
  %33 = bitcast i8* %call32 to float*
  %34 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %blackfilt = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %34, i32 0, i32 4
  %35 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt, i32 0, i32 %35
  store float* %33, float** %arrayidx33, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %36 = load i32, i32* %i, align 4
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %itime = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %37, i32 0, i32 2
  %arrayidx34 = getelementptr inbounds [2 x double], [2 x double]* %itime, i32 0, i32 0
  store double 0.000000e+00, double* %arrayidx34, align 8
  %38 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %itime35 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %38, i32 0, i32 2
  %arrayidx36 = getelementptr inbounds [2 x double], [2 x double]* %itime35, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx36, align 8
  store i32 0, i32* %j, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc73, %for.end
  %39 = load i32, i32* %j, align 4
  %40 = load i32, i32* %bpc, align 4
  %mul38 = mul nsw i32 2, %40
  %cmp39 = icmp sle i32 %39, %mul38
  br i1 %cmp39, label %for.body41, label %for.end75

for.body41:                                       ; preds = %for.cond37
  store float 0.000000e+00, float* %sum, align 4
  %41 = load i32, i32* %j, align 4
  %42 = load i32, i32* %bpc, align 4
  %sub42 = sub nsw i32 %41, %42
  %conv43 = sitofp i32 %sub42 to double
  %43 = load i32, i32* %bpc, align 4
  %conv44 = sitofp i32 %43 to double
  %mul45 = fmul double 2.000000e+00, %conv44
  %div46 = fdiv double %conv43, %mul45
  %conv47 = fptrunc double %div46 to float
  store float %conv47, float* %offset, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc59, %for.body41
  %44 = load i32, i32* %i, align 4
  %45 = load i32, i32* %filter_l, align 4
  %cmp49 = icmp sle i32 %44, %45
  br i1 %cmp49, label %for.body51, label %for.end61

for.body51:                                       ; preds = %for.cond48
  %46 = load i32, i32* %i, align 4
  %conv52 = sitofp i32 %46 to float
  %47 = load float, float* %offset, align 4
  %sub53 = fsub float %conv52, %47
  %48 = load float, float* %fcn, align 4
  %49 = load i32, i32* %filter_l, align 4
  %call54 = call float @blackman(float %sub53, float %48, i32 %49)
  %50 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %blackfilt55 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %50, i32 0, i32 4
  %51 = load i32, i32* %j, align 4
  %arrayidx56 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt55, i32 0, i32 %51
  %52 = load float*, float** %arrayidx56, align 4
  %53 = load i32, i32* %i, align 4
  %arrayidx57 = getelementptr inbounds float, float* %52, i32 %53
  store float %call54, float* %arrayidx57, align 4
  %54 = load float, float* %sum, align 4
  %add58 = fadd float %54, %call54
  store float %add58, float* %sum, align 4
  br label %for.inc59

for.inc59:                                        ; preds = %for.body51
  %55 = load i32, i32* %i, align 4
  %inc60 = add nsw i32 %55, 1
  store i32 %inc60, i32* %i, align 4
  br label %for.cond48

for.end61:                                        ; preds = %for.cond48
  store i32 0, i32* %i, align 4
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc70, %for.end61
  %56 = load i32, i32* %i, align 4
  %57 = load i32, i32* %filter_l, align 4
  %cmp63 = icmp sle i32 %56, %57
  br i1 %cmp63, label %for.body65, label %for.end72

for.body65:                                       ; preds = %for.cond62
  %58 = load float, float* %sum, align 4
  %59 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %blackfilt66 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %59, i32 0, i32 4
  %60 = load i32, i32* %j, align 4
  %arrayidx67 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt66, i32 0, i32 %60
  %61 = load float*, float** %arrayidx67, align 4
  %62 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %61, i32 %62
  %63 = load float, float* %arrayidx68, align 4
  %div69 = fdiv float %63, %58
  store float %div69, float* %arrayidx68, align 4
  br label %for.inc70

for.inc70:                                        ; preds = %for.body65
  %64 = load i32, i32* %i, align 4
  %inc71 = add nsw i32 %64, 1
  store i32 %inc71, i32* %i, align 4
  br label %for.cond62

for.end72:                                        ; preds = %for.cond62
  br label %for.inc73

for.inc73:                                        ; preds = %for.end72
  %65 = load i32, i32* %j, align 4
  %inc74 = add nsw i32 %65, 1
  store i32 %inc74, i32* %j, align 4
  br label %for.cond37

for.end75:                                        ; preds = %for.cond37
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %fill_buffer_resample_init76 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 4
  store i32 1, i32* %fill_buffer_resample_init76, align 8
  br label %if.end77

if.end77:                                         ; preds = %for.end75, %if.end17
  %67 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %inbuf_old78 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %67, i32 0, i32 3
  %68 = load i32, i32* %ch.addr, align 4
  %arrayidx79 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf_old78, i32 0, i32 %68
  %69 = load float*, float** %arrayidx79, align 4
  store float* %69, float** %inbuf_old, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc135, %if.end77
  %70 = load i32, i32* %k, align 4
  %71 = load i32, i32* %desired_len.addr, align 4
  %cmp81 = icmp slt i32 %70, %71
  br i1 %cmp81, label %for.body83, label %for.end137

for.body83:                                       ; preds = %for.cond80
  %72 = load i32, i32* %k, align 4
  %conv84 = sitofp i32 %72 to double
  %73 = load double, double* %resample_ratio, align 8
  %mul85 = fmul double %conv84, %73
  store double %mul85, double* %time0, align 8
  %74 = load double, double* %time0, align 8
  %75 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %itime86 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %75, i32 0, i32 2
  %76 = load i32, i32* %ch.addr, align 4
  %arrayidx87 = getelementptr inbounds [2 x double], [2 x double]* %itime86, i32 0, i32 %76
  %77 = load double, double* %arrayidx87, align 8
  %sub88 = fsub double %74, %77
  %78 = call double @llvm.floor.f64(double %sub88)
  %conv89 = fptosi double %78 to i32
  store i32 %conv89, i32* %j, align 4
  %79 = load i32, i32* %filter_l, align 4
  %80 = load i32, i32* %j, align 4
  %add90 = add nsw i32 %79, %80
  %81 = load i32, i32* %filter_l, align 4
  %div91 = sdiv i32 %81, 2
  %sub92 = sub nsw i32 %add90, %div91
  %82 = load i32, i32* %len.addr, align 4
  %cmp93 = icmp sge i32 %sub92, %82
  br i1 %cmp93, label %if.then95, label %if.end96

if.then95:                                        ; preds = %for.body83
  br label %for.end137

if.end96:                                         ; preds = %for.body83
  %83 = load double, double* %time0, align 8
  %84 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %itime97 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %84, i32 0, i32 2
  %85 = load i32, i32* %ch.addr, align 4
  %arrayidx98 = getelementptr inbounds [2 x double], [2 x double]* %itime97, i32 0, i32 %85
  %86 = load double, double* %arrayidx98, align 8
  %sub99 = fsub double %83, %86
  %87 = load i32, i32* %j, align 4
  %conv100 = sitofp i32 %87 to double
  %88 = load i32, i32* %filter_l, align 4
  %rem = srem i32 %88, 2
  %conv101 = sitofp i32 %rem to double
  %mul102 = fmul double 5.000000e-01, %conv101
  %add103 = fadd double %conv100, %mul102
  %sub104 = fsub double %sub99, %add103
  %conv105 = fptrunc double %sub104 to float
  store float %conv105, float* %offset, align 4
  %89 = load float, float* %offset, align 4
  %mul106 = fmul float %89, 2.000000e+00
  %90 = load i32, i32* %bpc, align 4
  %conv107 = sitofp i32 %90 to float
  %mul108 = fmul float %mul106, %conv107
  %91 = load i32, i32* %bpc, align 4
  %conv109 = sitofp i32 %91 to float
  %add110 = fadd float %mul108, %conv109
  %conv111 = fpext float %add110 to double
  %add112 = fadd double %conv111, 5.000000e-01
  %92 = call double @llvm.floor.f64(double %add112)
  %conv113 = fptosi double %92 to i32
  store i32 %conv113, i32* %joff, align 4
  store float 0.000000e+00, float* %xvalue, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc131, %if.end96
  %93 = load i32, i32* %i, align 4
  %94 = load i32, i32* %filter_l, align 4
  %cmp115 = icmp sle i32 %93, %94
  br i1 %cmp115, label %for.body117, label %for.end133

for.body117:                                      ; preds = %for.cond114
  %95 = load i32, i32* %i, align 4
  %96 = load i32, i32* %j, align 4
  %add118 = add nsw i32 %95, %96
  %97 = load i32, i32* %filter_l, align 4
  %div119 = sdiv i32 %97, 2
  %sub120 = sub nsw i32 %add118, %div119
  store i32 %sub120, i32* %j2, align 4
  %98 = load i32, i32* %j2, align 4
  %cmp121 = icmp slt i32 %98, 0
  br i1 %cmp121, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body117
  %99 = load float*, float** %inbuf_old, align 4
  %100 = load i32, i32* %BLACKSIZE, align 4
  %101 = load i32, i32* %j2, align 4
  %add123 = add nsw i32 %100, %101
  %arrayidx124 = getelementptr inbounds float, float* %99, i32 %add123
  %102 = load float, float* %arrayidx124, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body117
  %103 = load float*, float** %inbuf.addr, align 4
  %104 = load i32, i32* %j2, align 4
  %arrayidx125 = getelementptr inbounds float, float* %103, i32 %104
  %105 = load float, float* %arrayidx125, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %102, %cond.true ], [ %105, %cond.false ]
  store float %cond, float* %y, align 4
  %106 = load float, float* %y, align 4
  %107 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %blackfilt126 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %107, i32 0, i32 4
  %108 = load i32, i32* %joff, align 4
  %arrayidx127 = getelementptr inbounds [641 x float*], [641 x float*]* %blackfilt126, i32 0, i32 %108
  %109 = load float*, float** %arrayidx127, align 4
  %110 = load i32, i32* %i, align 4
  %arrayidx128 = getelementptr inbounds float, float* %109, i32 %110
  %111 = load float, float* %arrayidx128, align 4
  %mul129 = fmul float %106, %111
  %112 = load float, float* %xvalue, align 4
  %add130 = fadd float %112, %mul129
  store float %add130, float* %xvalue, align 4
  br label %for.inc131

for.inc131:                                       ; preds = %cond.end
  %113 = load i32, i32* %i, align 4
  %inc132 = add nsw i32 %113, 1
  store i32 %inc132, i32* %i, align 4
  br label %for.cond114

for.end133:                                       ; preds = %for.cond114
  %114 = load float, float* %xvalue, align 4
  %115 = load float*, float** %outbuf.addr, align 4
  %116 = load i32, i32* %k, align 4
  %arrayidx134 = getelementptr inbounds float, float* %115, i32 %116
  store float %114, float* %arrayidx134, align 4
  br label %for.inc135

for.inc135:                                       ; preds = %for.end133
  %117 = load i32, i32* %k, align 4
  %inc136 = add nsw i32 %117, 1
  store i32 %inc136, i32* %k, align 4
  br label %for.cond80

for.end137:                                       ; preds = %if.then95, %for.cond80
  %118 = load i32, i32* %len.addr, align 4
  %119 = load i32, i32* %filter_l, align 4
  %120 = load i32, i32* %j, align 4
  %add138 = add nsw i32 %119, %120
  %121 = load i32, i32* %filter_l, align 4
  %div139 = sdiv i32 %121, 2
  %sub140 = sub nsw i32 %add138, %div139
  %cmp141 = icmp slt i32 %118, %sub140
  br i1 %cmp141, label %cond.true143, label %cond.false144

cond.true143:                                     ; preds = %for.end137
  %122 = load i32, i32* %len.addr, align 4
  br label %cond.end148

cond.false144:                                    ; preds = %for.end137
  %123 = load i32, i32* %filter_l, align 4
  %124 = load i32, i32* %j, align 4
  %add145 = add nsw i32 %123, %124
  %125 = load i32, i32* %filter_l, align 4
  %div146 = sdiv i32 %125, 2
  %sub147 = sub nsw i32 %add145, %div146
  br label %cond.end148

cond.end148:                                      ; preds = %cond.false144, %cond.true143
  %cond149 = phi i32 [ %122, %cond.true143 ], [ %sub147, %cond.false144 ]
  %126 = load i32*, i32** %num_used.addr, align 4
  store i32 %cond149, i32* %126, align 4
  %127 = load i32*, i32** %num_used.addr, align 4
  %128 = load i32, i32* %127, align 4
  %conv150 = sitofp i32 %128 to double
  %129 = load i32, i32* %k, align 4
  %conv151 = sitofp i32 %129 to double
  %130 = load double, double* %resample_ratio, align 8
  %mul152 = fmul double %conv151, %130
  %sub153 = fsub double %conv150, %mul152
  %131 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %itime154 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %131, i32 0, i32 2
  %132 = load i32, i32* %ch.addr, align 4
  %arrayidx155 = getelementptr inbounds [2 x double], [2 x double]* %itime154, i32 0, i32 %132
  %133 = load double, double* %arrayidx155, align 8
  %add156 = fadd double %133, %sub153
  store double %add156, double* %arrayidx155, align 8
  %134 = load i32*, i32** %num_used.addr, align 4
  %135 = load i32, i32* %134, align 4
  %136 = load i32, i32* %BLACKSIZE, align 4
  %cmp157 = icmp sge i32 %135, %136
  br i1 %cmp157, label %if.then159, label %if.else

if.then159:                                       ; preds = %cond.end148
  store i32 0, i32* %i, align 4
  br label %for.cond160

for.cond160:                                      ; preds = %for.inc168, %if.then159
  %137 = load i32, i32* %i, align 4
  %138 = load i32, i32* %BLACKSIZE, align 4
  %cmp161 = icmp slt i32 %137, %138
  br i1 %cmp161, label %for.body163, label %for.end170

for.body163:                                      ; preds = %for.cond160
  %139 = load float*, float** %inbuf.addr, align 4
  %140 = load i32*, i32** %num_used.addr, align 4
  %141 = load i32, i32* %140, align 4
  %142 = load i32, i32* %i, align 4
  %add164 = add nsw i32 %141, %142
  %143 = load i32, i32* %BLACKSIZE, align 4
  %sub165 = sub nsw i32 %add164, %143
  %arrayidx166 = getelementptr inbounds float, float* %139, i32 %sub165
  %144 = load float, float* %arrayidx166, align 4
  %145 = load float*, float** %inbuf_old, align 4
  %146 = load i32, i32* %i, align 4
  %arrayidx167 = getelementptr inbounds float, float* %145, i32 %146
  store float %144, float* %arrayidx167, align 4
  br label %for.inc168

for.inc168:                                       ; preds = %for.body163
  %147 = load i32, i32* %i, align 4
  %inc169 = add nsw i32 %147, 1
  store i32 %inc169, i32* %i, align 4
  br label %for.cond160

for.end170:                                       ; preds = %for.cond160
  br label %if.end192

if.else:                                          ; preds = %cond.end148
  %148 = load i32, i32* %BLACKSIZE, align 4
  %149 = load i32*, i32** %num_used.addr, align 4
  %150 = load i32, i32* %149, align 4
  %sub171 = sub nsw i32 %148, %150
  store i32 %sub171, i32* %n_shift, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond172

for.cond172:                                      ; preds = %for.inc179, %if.else
  %151 = load i32, i32* %i, align 4
  %152 = load i32, i32* %n_shift, align 4
  %cmp173 = icmp slt i32 %151, %152
  br i1 %cmp173, label %for.body175, label %for.end181

for.body175:                                      ; preds = %for.cond172
  %153 = load float*, float** %inbuf_old, align 4
  %154 = load i32, i32* %i, align 4
  %155 = load i32*, i32** %num_used.addr, align 4
  %156 = load i32, i32* %155, align 4
  %add176 = add nsw i32 %154, %156
  %arrayidx177 = getelementptr inbounds float, float* %153, i32 %add176
  %157 = load float, float* %arrayidx177, align 4
  %158 = load float*, float** %inbuf_old, align 4
  %159 = load i32, i32* %i, align 4
  %arrayidx178 = getelementptr inbounds float, float* %158, i32 %159
  store float %157, float* %arrayidx178, align 4
  br label %for.inc179

for.inc179:                                       ; preds = %for.body175
  %160 = load i32, i32* %i, align 4
  %inc180 = add nsw i32 %160, 1
  store i32 %inc180, i32* %i, align 4
  br label %for.cond172

for.end181:                                       ; preds = %for.cond172
  store i32 0, i32* %j, align 4
  br label %for.cond182

for.cond182:                                      ; preds = %for.inc188, %for.end181
  %161 = load i32, i32* %i, align 4
  %162 = load i32, i32* %BLACKSIZE, align 4
  %cmp183 = icmp slt i32 %161, %162
  br i1 %cmp183, label %for.body185, label %for.end191

for.body185:                                      ; preds = %for.cond182
  %163 = load float*, float** %inbuf.addr, align 4
  %164 = load i32, i32* %j, align 4
  %arrayidx186 = getelementptr inbounds float, float* %163, i32 %164
  %165 = load float, float* %arrayidx186, align 4
  %166 = load float*, float** %inbuf_old, align 4
  %167 = load i32, i32* %i, align 4
  %arrayidx187 = getelementptr inbounds float, float* %166, i32 %167
  store float %165, float* %arrayidx187, align 4
  br label %for.inc188

for.inc188:                                       ; preds = %for.body185
  %168 = load i32, i32* %i, align 4
  %inc189 = add nsw i32 %168, 1
  store i32 %inc189, i32* %i, align 4
  %169 = load i32, i32* %j, align 4
  %inc190 = add nsw i32 %169, 1
  store i32 %inc190, i32* %j, align 4
  br label %for.cond182

for.end191:                                       ; preds = %for.cond182
  br label %if.end192

if.end192:                                        ; preds = %for.end191, %for.end170
  %170 = load i32, i32* %k, align 4
  ret i32 %170
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_report_def(i8* %format, i8* %args) #0 {
entry:
  %format.addr = alloca i8*, align 4
  %args.addr = alloca i8*, align 4
  store i8* %format, i8** %format.addr, align 4
  store i8* %args, i8** %args.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_report_fnc(void (i8*, i8*)* %print_f, i8* %format, ...) #0 {
entry:
  %print_f.addr = alloca void (i8*, i8*)*, align 4
  %format.addr = alloca i8*, align 4
  %args = alloca i8*, align 4
  store void (i8*, i8*)* %print_f, void (i8*, i8*)** %print_f.addr, align 4
  store i8* %format, i8** %format.addr, align 4
  %0 = load void (i8*, i8*)*, void (i8*, i8*)** %print_f.addr, align 4
  %tobool = icmp ne void (i8*, i8*)* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %args1 = bitcast i8** %args to i8*
  call void @llvm.va_start(i8* %args1)
  %1 = load void (i8*, i8*)*, void (i8*, i8*)** %print_f.addr, align 4
  %2 = load i8*, i8** %format.addr, align 4
  %3 = load i8*, i8** %args, align 4
  call void %1(i8* %2, i8* %3)
  %args2 = bitcast i8** %args to i8*
  call void @llvm.va_end(i8* %args2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
declare void @llvm.va_start(i8*) #5

; Function Attrs: nounwind
declare void @llvm.va_end(i8*) #5

; Function Attrs: noinline nounwind optnone
define hidden void @lame_debugf(%struct.lame_internal_flags* %gfc, i8* %format, ...) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %format.addr = alloca i8*, align 4
  %args = alloca i8*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %format, i8** %format.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_dbg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 29
  %2 = load void (i8*, i8*)*, void (i8*, i8*)** %report_dbg, align 8
  %tobool1 = icmp ne void (i8*, i8*)* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %args2 = bitcast i8** %args to i8*
  call void @llvm.va_start(i8* %args2)
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_dbg3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 29
  %4 = load void (i8*, i8*)*, void (i8*, i8*)** %report_dbg3, align 8
  %5 = load i8*, i8** %format.addr, align 4
  %6 = load i8*, i8** %args, align 4
  call void %4(i8* %5, i8* %6)
  %args4 = bitcast i8** %args to i8*
  call void @llvm.va_end(i8* %args4)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_msgf(%struct.lame_internal_flags* %gfc, i8* %format, ...) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %format.addr = alloca i8*, align 4
  %args = alloca i8*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %format, i8** %format.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_msg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 28
  %2 = load void (i8*, i8*)*, void (i8*, i8*)** %report_msg, align 4
  %tobool1 = icmp ne void (i8*, i8*)* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %args2 = bitcast i8** %args to i8*
  call void @llvm.va_start(i8* %args2)
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_msg3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 28
  %4 = load void (i8*, i8*)*, void (i8*, i8*)** %report_msg3, align 4
  %5 = load i8*, i8** %format.addr, align 4
  %6 = load i8*, i8** %args, align 4
  call void %4(i8* %5, i8* %6)
  %args4 = bitcast i8** %args to i8*
  call void @llvm.va_end(i8* %args4)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_errorf(%struct.lame_internal_flags* %gfc, i8* %format, ...) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %format.addr = alloca i8*, align 4
  %args = alloca i8*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %format, i8** %format.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_err = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 30
  %2 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  %tobool1 = icmp ne void (i8*, i8*)* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %args2 = bitcast i8** %args to i8*
  call void @llvm.va_start(i8* %args2)
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %report_err3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 30
  %4 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err3, align 4
  %5 = load i8*, i8** %format.addr, align 4
  %6 = load i8*, i8** %args, align 4
  call void %4(i8* %5, i8* %6)
  %args4 = bitcast i8** %args to i8*
  call void @llvm.va_end(i8* %args4)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @has_MMX() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @has_3DNow() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @has_SSE() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @has_SSE2() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden void @disable_FPE() #0 {
entry:
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @init_log_table() #0 {
entry:
  %j = alloca i32, align 4
  %0 = load i32, i32* @init_log_table.init, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %1 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %1, 513
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %j, align 4
  %conv = sitofp i32 %2 to float
  %div = fdiv float %conv, 5.120000e+02
  %add = fadd float 1.000000e+00, %div
  %conv1 = fpext float %add to double
  %3 = call double @llvm.log.f64(double %conv1)
  %4 = call double @llvm.log.f64(double 2.000000e+00)
  %div2 = fdiv double %3, %4
  %conv3 = fptrunc double %div2 to float
  %5 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [513 x float], [513 x float]* @log_table, i32 0, i32 %5
  store float %conv3, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %j, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  store i32 1, i32* @init_log_table.init, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log.f64(double) #6

; Function Attrs: noinline nounwind optnone
define hidden float @fast_log2(float %x) #0 {
entry:
  %x.addr = alloca float, align 4
  %log2val = alloca float, align 4
  %partial = alloca float, align 4
  %fi = alloca %union.anon.2, align 4
  %mantisse = alloca i32, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %f = bitcast %union.anon.2* %fi to float*
  store float %0, float* %f, align 4
  %i = bitcast %union.anon.2* %fi to i32*
  %1 = load i32, i32* %i, align 4
  %and = and i32 %1, 8388607
  store i32 %and, i32* %mantisse, align 4
  %i1 = bitcast %union.anon.2* %fi to i32*
  %2 = load i32, i32* %i1, align 4
  %shr = ashr i32 %2, 23
  %and2 = and i32 %shr, 255
  %sub = sub nsw i32 %and2, 127
  %conv = sitofp i32 %sub to float
  store float %conv, float* %log2val, align 4
  %3 = load i32, i32* %mantisse, align 4
  %and3 = and i32 %3, 16383
  %conv4 = sitofp i32 %and3 to float
  store float %conv4, float* %partial, align 4
  %4 = load float, float* %partial, align 4
  %mul = fmul float %4, 0x3F10000000000000
  store float %mul, float* %partial, align 4
  %5 = load i32, i32* %mantisse, align 4
  %shr5 = ashr i32 %5, 14
  store i32 %shr5, i32* %mantisse, align 4
  %6 = load i32, i32* %mantisse, align 4
  %arrayidx = getelementptr inbounds [513 x float], [513 x float]* @log_table, i32 0, i32 %6
  %7 = load float, float* %arrayidx, align 4
  %8 = load float, float* %partial, align 4
  %sub6 = fsub float 1.000000e+00, %8
  %mul7 = fmul float %7, %sub6
  %9 = load i32, i32* %mantisse, align 4
  %add = add nsw i32 %9, 1
  %arrayidx8 = getelementptr inbounds [513 x float], [513 x float]* @log_table, i32 0, i32 %add
  %10 = load float, float* %arrayidx8, align 4
  %11 = load float, float* %partial, align 4
  %mul9 = fmul float %10, %11
  %add10 = fadd float %mul7, %mul9
  %12 = load float, float* %log2val, align 4
  %add11 = fadd float %12, %add10
  store float %add11, float* %log2val, align 4
  %13 = load float, float* %log2val, align 4
  ret float %13
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.exp.f64(double) #6

; Function Attrs: noinline nounwind optnone
define internal i32 @gcd(i32 %i, i32 %j) #0 {
entry:
  %i.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %j, i32* %j.addr, align 4
  %0 = load i32, i32* %j.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %j.addr, align 4
  %2 = load i32, i32* %i.addr, align 4
  %3 = load i32, i32* %j.addr, align 4
  %rem = srem i32 %2, %3
  %call = call i32 @gcd(i32 %1, i32 %rem)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load i32, i32* %i.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call, %cond.true ], [ %4, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #6

declare i8* @calloc(i32, i32) #1

; Function Attrs: noinline nounwind optnone
define internal float @blackman(float %x, float %fcn, i32 %l) #0 {
entry:
  %retval = alloca float, align 4
  %x.addr = alloca float, align 4
  %fcn.addr = alloca float, align 4
  %l.addr = alloca i32, align 4
  %bkwn = alloca float, align 4
  %x2 = alloca float, align 4
  %wcn = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %fcn, float* %fcn.addr, align 4
  store i32 %l, i32* %l.addr, align 4
  %0 = load float, float* %fcn.addr, align 4
  %conv = fpext float %0 to double
  %mul = fmul double 0x400921FB54442D18, %conv
  %conv1 = fptrunc double %mul to float
  store float %conv1, float* %wcn, align 4
  %1 = load i32, i32* %l.addr, align 4
  %conv2 = sitofp i32 %1 to float
  %2 = load float, float* %x.addr, align 4
  %div = fdiv float %2, %conv2
  store float %div, float* %x.addr, align 4
  %3 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load float, float* %x.addr, align 4
  %cmp4 = fcmp ogt float %4, 1.000000e+00
  br i1 %cmp4, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end
  %5 = load float, float* %x.addr, align 4
  %conv8 = fpext float %5 to double
  %sub = fsub double %conv8, 5.000000e-01
  %conv9 = fptrunc double %sub to float
  store float %conv9, float* %x2, align 4
  %6 = load float, float* %x.addr, align 4
  %mul10 = fmul float 2.000000e+00, %6
  %conv11 = fpext float %mul10 to double
  %mul12 = fmul double %conv11, 0x400921FB54442D18
  %7 = call double @llvm.cos.f64(double %mul12)
  %mul13 = fmul double 5.000000e-01, %7
  %sub14 = fsub double 4.200000e-01, %mul13
  %8 = load float, float* %x.addr, align 4
  %mul15 = fmul float 4.000000e+00, %8
  %conv16 = fpext float %mul15 to double
  %mul17 = fmul double %conv16, 0x400921FB54442D18
  %9 = call double @llvm.cos.f64(double %mul17)
  %mul18 = fmul double 8.000000e-02, %9
  %add = fadd double %sub14, %mul18
  %conv19 = fptrunc double %add to float
  store float %conv19, float* %bkwn, align 4
  %10 = load float, float* %x2, align 4
  %conv20 = fpext float %10 to double
  %11 = call double @llvm.fabs.f64(double %conv20)
  %cmp21 = fcmp olt double %11, 1.000000e-09
  br i1 %cmp21, label %if.then23, label %if.else

if.then23:                                        ; preds = %if.end7
  %12 = load float, float* %wcn, align 4
  %conv24 = fpext float %12 to double
  %div25 = fdiv double %conv24, 0x400921FB54442D18
  %conv26 = fptrunc double %div25 to float
  store float %conv26, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end7
  %13 = load float, float* %bkwn, align 4
  %conv27 = fpext float %13 to double
  %14 = load i32, i32* %l.addr, align 4
  %conv28 = sitofp i32 %14 to float
  %15 = load float, float* %wcn, align 4
  %mul29 = fmul float %conv28, %15
  %16 = load float, float* %x2, align 4
  %mul30 = fmul float %mul29, %16
  %conv31 = fpext float %mul30 to double
  %17 = call double @llvm.sin.f64(double %conv31)
  %mul32 = fmul double %conv27, %17
  %18 = load i32, i32* %l.addr, align 4
  %conv33 = sitofp i32 %18 to double
  %mul34 = fmul double 0x400921FB54442D18, %conv33
  %19 = load float, float* %x2, align 4
  %conv35 = fpext float %19 to double
  %mul36 = fmul double %mul34, %conv35
  %div37 = fdiv double %mul32, %mul36
  %conv38 = fptrunc double %div37 to float
  store float %conv38, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then23
  %20 = load float, float* %retval, align 4
  ret float %20
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sin.f64(double) #6

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
