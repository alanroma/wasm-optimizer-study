; ModuleID = 'pong.c'
source_filename = "pong.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.RGB = type { i32, i32, i32 }
%struct.Rect = type { i32, i32, i32, i32 }
%struct.Player = type { %struct.Rect, i32 }
%struct.Ball = type { %struct.Rect, %struct.Vec2, float }
%struct.Vec2 = type { float, float }

@white = hidden global %struct.RGB { i32 255, i32 255, i32 255 }, align 4
@black = hidden global %struct.RGB zeroinitializer, align 4
@walls = hidden global [2 x %struct.Rect] [%struct.Rect { i32 0, i32 0, i32 800, i32 16 }, %struct.Rect { i32 0, i32 584, i32 800, i32 16 }], align 16
@line = hidden global %struct.Rect { i32 400, i32 0, i32 400, i32 600 }, align 4
@player1 = hidden global %struct.Player { %struct.Rect { i32 0, i32 268, i32 16, i32 64 }, i32 0 }, align 4
@player2 = hidden global %struct.Player { %struct.Rect { i32 784, i32 268, i32 16, i32 64 }, i32 0 }, align 4
@ball = hidden global %struct.Ball { %struct.Rect { i32 392, i32 16, i32 16, i32 16 }, %struct.Vec2 { float 1.000000e+00, float 1.000000e+00 }, float 1.500000e+02 }, align 4

; Function Attrs: noinline nounwind optnone
define hidden void @setFill(%struct.RGB* byval(%struct.RGB) align 4 %color) #0 {
entry:
  %r = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 0
  %0 = load i32, i32* %r, align 4
  %g = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 1
  %1 = load i32, i32* %g, align 4
  %b = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 2
  %2 = load i32, i32* %b, align 4
  call void @setFillStyle(i32 %0, i32 %1, i32 %2)
  ret void
}

declare void @setFillStyle(i32, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @setStroke(%struct.RGB* byval(%struct.RGB) align 4 %color) #0 {
entry:
  %r = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 0
  %0 = load i32, i32* %r, align 4
  %g = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 1
  %1 = load i32, i32* %g, align 4
  %b = getelementptr inbounds %struct.RGB, %struct.RGB* %color, i32 0, i32 2
  %2 = load i32, i32* %b, align 4
  call void @setStrokeStyle(i32 %0, i32 %1, i32 %2)
  ret void
}

declare void @setStrokeStyle(i32, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @clamp(i32 %value, i32 %valueOffset, i32 %min, i32 %max) #0 {
entry:
  %retval = alloca i32, align 4
  %value.addr = alloca i32, align 4
  %valueOffset.addr = alloca i32, align 4
  %min.addr = alloca i32, align 4
  %max.addr = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4
  store i32 %valueOffset, i32* %valueOffset.addr, align 4
  store i32 %min, i32* %min.addr, align 4
  store i32 %max, i32* %max.addr, align 4
  %0 = load i32, i32* %value.addr, align 4
  %1 = load i32, i32* %min.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %min.addr, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %value.addr, align 4
  %4 = load i32, i32* %valueOffset.addr, align 4
  %add = add nsw i32 %3, %4
  %5 = load i32, i32* %max.addr, align 4
  %cmp1 = icmp sgt i32 %add, %5
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %6 = load i32, i32* %max.addr, align 4
  %7 = load i32, i32* %valueOffset.addr, align 4
  %sub = sub nsw i32 %6, %7
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %8 = load i32, i32* %value.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ %8, %cond.false ]
  store i32 %cond, i32* %retval, align 4
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @isInBounds(%struct.Vec2* byval(%struct.Vec2) align 4 %point, %struct.Rect* byval(%struct.Rect) align 4 %box) #0 {
entry:
  %x = getelementptr inbounds %struct.Vec2, %struct.Vec2* %point, i32 0, i32 0
  %0 = load float, float* %x, align 4
  %x1 = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 0
  %1 = load i32, i32* %x1, align 4
  %conv = sitofp i32 %1 to float
  %cmp = fcmp ogt float %0, %conv
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %x3 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %point, i32 0, i32 0
  %2 = load float, float* %x3, align 4
  %x4 = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 0
  %3 = load i32, i32* %x4, align 4
  %width = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 2
  %4 = load i32, i32* %width, align 4
  %add = add nsw i32 %3, %4
  %conv5 = sitofp i32 %add to float
  %cmp6 = fcmp olt float %2, %conv5
  br i1 %cmp6, label %land.lhs.true8, label %land.end

land.lhs.true8:                                   ; preds = %land.lhs.true
  %y = getelementptr inbounds %struct.Vec2, %struct.Vec2* %point, i32 0, i32 1
  %5 = load float, float* %y, align 4
  %y9 = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 1
  %6 = load i32, i32* %y9, align 4
  %conv10 = sitofp i32 %6 to float
  %cmp11 = fcmp ogt float %5, %conv10
  br i1 %cmp11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true8
  %y13 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %point, i32 0, i32 1
  %7 = load float, float* %y13, align 4
  %y14 = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 1
  %8 = load i32, i32* %y14, align 4
  %height = getelementptr inbounds %struct.Rect, %struct.Rect* %box, i32 0, i32 3
  %9 = load i32, i32* %height, align 4
  %add15 = add nsw i32 %8, %9
  %conv16 = sitofp i32 %add15 to float
  %cmp17 = fcmp olt float %7, %conv16
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true8, %land.lhs.true, %entry
  %10 = phi i1 [ false, %land.lhs.true8 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp17, %land.rhs ]
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @doesCollide(%struct.Rect* byval(%struct.Rect) align 4 %a, %struct.Rect* byval(%struct.Rect) align 4 %b) #0 {
entry:
  %retval = alloca i1, align 1
  %corners = alloca [4 x %struct.Vec2], align 16
  %i = alloca i32, align 4
  %arrayinit.begin = getelementptr inbounds [4 x %struct.Vec2], [4 x %struct.Vec2]* %corners, i32 0, i32 0
  %x = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.begin, i32 0, i32 0
  %x1 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 0
  %0 = load i32, i32* %x1, align 4
  %conv = sitofp i32 %0 to float
  store float %conv, float* %x, align 8
  %y = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.begin, i32 0, i32 1
  %y2 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 1
  %1 = load i32, i32* %y2, align 4
  %conv3 = sitofp i32 %1 to float
  store float %conv3, float* %y, align 4
  %arrayinit.element = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.begin, i32 1
  %x4 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element, i32 0, i32 0
  %x5 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 0
  %2 = load i32, i32* %x5, align 4
  %width = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 2
  %3 = load i32, i32* %width, align 4
  %add = add nsw i32 %2, %3
  %conv6 = sitofp i32 %add to float
  store float %conv6, float* %x4, align 8
  %y7 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element, i32 0, i32 1
  %y8 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 1
  %4 = load i32, i32* %y8, align 4
  %conv9 = sitofp i32 %4 to float
  store float %conv9, float* %y7, align 4
  %arrayinit.element10 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element, i32 1
  %x11 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element10, i32 0, i32 0
  %x12 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 0
  %5 = load i32, i32* %x12, align 4
  %width13 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 2
  %6 = load i32, i32* %width13, align 4
  %add14 = add nsw i32 %5, %6
  %conv15 = sitofp i32 %add14 to float
  store float %conv15, float* %x11, align 8
  %y16 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element10, i32 0, i32 1
  %y17 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 1
  %7 = load i32, i32* %y17, align 4
  %height = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 3
  %8 = load i32, i32* %height, align 4
  %add18 = add nsw i32 %7, %8
  %conv19 = sitofp i32 %add18 to float
  store float %conv19, float* %y16, align 4
  %arrayinit.element20 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element10, i32 1
  %x21 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element20, i32 0, i32 0
  %x22 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 0
  %9 = load i32, i32* %x22, align 4
  %conv23 = sitofp i32 %9 to float
  store float %conv23, float* %x21, align 8
  %y24 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %arrayinit.element20, i32 0, i32 1
  %y25 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 1
  %10 = load i32, i32* %y25, align 4
  %height26 = getelementptr inbounds %struct.Rect, %struct.Rect* %a, i32 0, i32 3
  %11 = load i32, i32* %height26, align 4
  %add27 = add nsw i32 %10, %11
  %conv28 = sitofp i32 %add27 to float
  store float %conv28, float* %y24, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %12, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %struct.Vec2], [4 x %struct.Vec2]* %corners, i32 0, i32 %13
  %call = call zeroext i1 @isInBounds(%struct.Vec2* byval(%struct.Vec2) align 4 %arrayidx, %struct.Rect* byval(%struct.Rect) align 4 %b)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: noinline nounwind optnone
define hidden void @movePlayer(%struct.Player* %player, i32 %direction, float %delta) #0 {
entry:
  %player.addr = alloca %struct.Player*, align 4
  %direction.addr = alloca i32, align 4
  %delta.addr = alloca float, align 4
  %curY = alloca i32, align 4
  %deltaY = alloca float, align 4
  %newY = alloca i32, align 4
  store %struct.Player* %player, %struct.Player** %player.addr, align 4
  store i32 %direction, i32* %direction.addr, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load %struct.Player*, %struct.Player** %player.addr, align 4
  %paddle = getelementptr inbounds %struct.Player, %struct.Player* %0, i32 0, i32 0
  %y = getelementptr inbounds %struct.Rect, %struct.Rect* %paddle, i32 0, i32 1
  %1 = load i32, i32* %y, align 4
  store i32 %1, i32* %curY, align 4
  %2 = load float, float* %delta.addr, align 4
  %div = fdiv float %2, 1.000000e+03
  %conv = fpext float %div to double
  %mul = fmul double %conv, 4.000000e+02
  %conv1 = fptrunc double %mul to float
  store float %conv1, float* %deltaY, align 4
  %3 = load i32, i32* %curY, align 4
  %4 = load float, float* %deltaY, align 4
  %conv2 = fptosi float %4 to i32
  %5 = load i32, i32* %direction.addr, align 4
  %mul3 = mul nsw i32 %conv2, %5
  %add = add nsw i32 %3, %mul3
  store i32 %add, i32* %newY, align 4
  %6 = load i32, i32* %newY, align 4
  %7 = load %struct.Player*, %struct.Player** %player.addr, align 4
  %paddle4 = getelementptr inbounds %struct.Player, %struct.Player* %7, i32 0, i32 0
  %height = getelementptr inbounds %struct.Rect, %struct.Rect* %paddle4, i32 0, i32 3
  %8 = load i32, i32* %height, align 4
  %call = call i32 @clamp(i32 %6, i32 %8, i32 16, i32 584)
  %9 = load %struct.Player*, %struct.Player** %player.addr, align 4
  %paddle5 = getelementptr inbounds %struct.Player, %struct.Player* %9, i32 0, i32 0
  %y6 = getelementptr inbounds %struct.Rect, %struct.Rect* %paddle5, i32 0, i32 1
  store i32 %call, i32* %y6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @moveBall(%struct.Ball* %ball, float %delta) #0 {
entry:
  %ball.addr = alloca %struct.Ball*, align 4
  %delta.addr = alloca float, align 4
  %curX = alloca i32, align 4
  %curY = alloca i32, align 4
  %deltaPos = alloca float, align 4
  %newX = alloca i32, align 4
  %newY = alloca i32, align 4
  store %struct.Ball* %ball, %struct.Ball** %ball.addr, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere = getelementptr inbounds %struct.Ball, %struct.Ball* %0, i32 0, i32 0
  %x = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere, i32 0, i32 0
  %1 = load i32, i32* %x, align 4
  store i32 %1, i32* %curX, align 4
  %2 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere1 = getelementptr inbounds %struct.Ball, %struct.Ball* %2, i32 0, i32 0
  %y = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere1, i32 0, i32 1
  %3 = load i32, i32* %y, align 4
  store i32 %3, i32* %curY, align 4
  %4 = load float, float* %delta.addr, align 4
  %div = fdiv float %4, 1.000000e+03
  %5 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %velocity = getelementptr inbounds %struct.Ball, %struct.Ball* %5, i32 0, i32 2
  %6 = load float, float* %velocity, align 4
  %mul = fmul float %div, %6
  store float %mul, float* %deltaPos, align 4
  %7 = load i32, i32* %curX, align 4
  %conv = sitofp i32 %7 to float
  %8 = load float, float* %deltaPos, align 4
  %conv2 = fptosi float %8 to i32
  %conv3 = sitofp i32 %conv2 to float
  %9 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir = getelementptr inbounds %struct.Ball, %struct.Ball* %9, i32 0, i32 1
  %x4 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir, i32 0, i32 0
  %10 = load float, float* %x4, align 4
  %mul5 = fmul float %conv3, %10
  %add = fadd float %conv, %mul5
  %conv6 = fptosi float %add to i32
  store i32 %conv6, i32* %newX, align 4
  %11 = load i32, i32* %curY, align 4
  %conv7 = sitofp i32 %11 to float
  %12 = load float, float* %deltaPos, align 4
  %conv8 = fptosi float %12 to i32
  %conv9 = sitofp i32 %conv8 to float
  %13 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir10 = getelementptr inbounds %struct.Ball, %struct.Ball* %13, i32 0, i32 1
  %y11 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir10, i32 0, i32 1
  %14 = load float, float* %y11, align 4
  %mul12 = fmul float %conv9, %14
  %add13 = fadd float %conv7, %mul12
  %conv14 = fptosi float %add13 to i32
  store i32 %conv14, i32* %newY, align 4
  %15 = load i32, i32* %newX, align 4
  %16 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere15 = getelementptr inbounds %struct.Ball, %struct.Ball* %16, i32 0, i32 0
  %x16 = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere15, i32 0, i32 0
  store i32 %15, i32* %x16, align 4
  %17 = load i32, i32* %newY, align 4
  %18 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere17 = getelementptr inbounds %struct.Ball, %struct.Ball* %18, i32 0, i32 0
  %y18 = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere17, i32 0, i32 1
  store i32 %17, i32* %y18, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @drawObject(%struct.Rect* byval(%struct.Rect) align 4 %o) #0 {
entry:
  call void @setFill(%struct.RGB* byval(%struct.RGB) align 4 @white)
  %x = getelementptr inbounds %struct.Rect, %struct.Rect* %o, i32 0, i32 0
  %0 = load i32, i32* %x, align 4
  %y = getelementptr inbounds %struct.Rect, %struct.Rect* %o, i32 0, i32 1
  %1 = load i32, i32* %y, align 4
  %width = getelementptr inbounds %struct.Rect, %struct.Rect* %o, i32 0, i32 2
  %2 = load i32, i32* %width, align 4
  %height = getelementptr inbounds %struct.Rect, %struct.Rect* %o, i32 0, i32 3
  %3 = load i32, i32* %height, align 4
  call void @fillRect(i32 %0, i32 %1, i32 %2, i32 %3)
  ret void
}

declare void @fillRect(i32, i32, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @drawScores() #0 {
entry:
  call void @setTextSize(i32 64)
  %0 = load i32, i32* getelementptr inbounds (%struct.Player, %struct.Player* @player1, i32 0, i32 1), align 4
  call void @fillText(i32 %0, i32 304, i32 80)
  %1 = load i32, i32* getelementptr inbounds (%struct.Player, %struct.Player* @player2, i32 0, i32 1), align 4
  call void @fillText(i32 %1, i32 464, i32 80)
  ret void
}

declare void @setTextSize(i32) #1

declare void @fillText(i32, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @resetGame(%struct.Player* %winner, %struct.Ball* %ball, i32 %xDir) #0 {
entry:
  %winner.addr = alloca %struct.Player*, align 4
  %ball.addr = alloca %struct.Ball*, align 4
  %xDir.addr = alloca i32, align 4
  store %struct.Player* %winner, %struct.Player** %winner.addr, align 4
  store %struct.Ball* %ball, %struct.Ball** %ball.addr, align 4
  store i32 %xDir, i32* %xDir.addr, align 4
  %0 = load %struct.Player*, %struct.Player** %winner.addr, align 4
  %score = getelementptr inbounds %struct.Player, %struct.Player* %0, i32 0, i32 1
  %1 = load i32, i32* %score, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %score, align 4
  %2 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere = getelementptr inbounds %struct.Ball, %struct.Ball* %2, i32 0, i32 0
  %x = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere, i32 0, i32 0
  store i32 392, i32* %x, align 4
  %3 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere1 = getelementptr inbounds %struct.Ball, %struct.Ball* %3, i32 0, i32 0
  %y = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere1, i32 0, i32 1
  store i32 16, i32* %y, align 4
  %4 = load i32, i32* %xDir.addr, align 4
  %conv = sitofp i32 %4 to float
  %5 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir = getelementptr inbounds %struct.Ball, %struct.Ball* %5, i32 0, i32 1
  %x2 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir, i32 0, i32 0
  store float %conv, float* %x2, align 4
  %6 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir3 = getelementptr inbounds %struct.Ball, %struct.Ball* %6, i32 0, i32 1
  %y4 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir3, i32 0, i32 1
  store float 1.000000e+00, float* %y4, align 4
  %7 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %velocity = getelementptr inbounds %struct.Ball, %struct.Ball* %7, i32 0, i32 2
  store float 1.500000e+02, float* %velocity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @drawCourt() #0 {
entry:
  %length = alloca i32, align 4
  %i = alloca i32, align 4
  call void @setFill(%struct.RGB* byval(%struct.RGB) align 4 @black)
  call void @fillRect(i32 0, i32 0, i32 800, i32 600)
  call void @setFill(%struct.RGB* byval(%struct.RGB) align 4 @white)
  store i32 2, i32* %length, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %length, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2 x %struct.Rect], [2 x %struct.Rect]* @walls, i32 0, i32 %2
  call void @drawObject(%struct.Rect* byval(%struct.Rect) align 4 %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @setStroke(%struct.RGB* byval(%struct.RGB) align 4 @white)
  call void @setLineDash(i32 8, i32 8)
  call void @setLineWidth(i32 8)
  %4 = load i32, i32* getelementptr inbounds (%struct.Rect, %struct.Rect* @line, i32 0, i32 0), align 4
  %5 = load i32, i32* getelementptr inbounds (%struct.Rect, %struct.Rect* @line, i32 0, i32 1), align 4
  %6 = load i32, i32* getelementptr inbounds (%struct.Rect, %struct.Rect* @line, i32 0, i32 2), align 4
  %7 = load i32, i32* getelementptr inbounds (%struct.Rect, %struct.Rect* @line, i32 0, i32 3), align 4
  call void @drawLine(i32 %4, i32 %5, i32 %6, i32 %7)
  ret void
}

declare void @setLineDash(i32, i32) #1

declare void @setLineWidth(i32) #1

declare void @drawLine(i32, i32, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @checkCollisions(%struct.Ball* %ball, i32 %player1dir, i32 %player2dir) #0 {
entry:
  %ball.addr = alloca %struct.Ball*, align 4
  %player1dir.addr = alloca i32, align 4
  %player2dir.addr = alloca i32, align 4
  %length = alloca i32, align 4
  %i = alloca i32, align 4
  %collideWithPlayer1 = alloca i8, align 1
  %collideWithPlayer2 = alloca i8, align 1
  store %struct.Ball* %ball, %struct.Ball** %ball.addr, align 4
  store i32 %player1dir, i32* %player1dir.addr, align 4
  store i32 %player2dir, i32* %player2dir.addr, align 4
  store i32 2, i32* %length, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %length, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere = getelementptr inbounds %struct.Ball, %struct.Ball* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2 x %struct.Rect], [2 x %struct.Rect]* @walls, i32 0, i32 %3
  %call = call zeroext i1 @doesCollide(%struct.Rect* byval(%struct.Rect) align 4 %sphere, %struct.Rect* byval(%struct.Rect) align 4 %arrayidx)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir = getelementptr inbounds %struct.Ball, %struct.Ball* %4, i32 0, i32 1
  %y = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir, i32 0, i32 1
  %5 = load float, float* %y, align 4
  %mul = fmul float %5, -1.000000e+00
  %6 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir1 = getelementptr inbounds %struct.Ball, %struct.Ball* %6, i32 0, i32 1
  %y2 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir1, i32 0, i32 1
  store float %mul, float* %y2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere3 = getelementptr inbounds %struct.Ball, %struct.Ball* %8, i32 0, i32 0
  %call4 = call zeroext i1 @doesCollide(%struct.Rect* byval(%struct.Rect) align 4 %sphere3, %struct.Rect* byval(%struct.Rect) align 4 getelementptr inbounds (%struct.Player, %struct.Player* @player1, i32 0, i32 0))
  %frombool = zext i1 %call4 to i8
  store i8 %frombool, i8* %collideWithPlayer1, align 1
  %9 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere5 = getelementptr inbounds %struct.Ball, %struct.Ball* %9, i32 0, i32 0
  %call6 = call zeroext i1 @doesCollide(%struct.Rect* byval(%struct.Rect) align 4 %sphere5, %struct.Rect* byval(%struct.Rect) align 4 getelementptr inbounds (%struct.Player, %struct.Player* @player2, i32 0, i32 0))
  %frombool7 = zext i1 %call6 to i8
  store i8 %frombool7, i8* %collideWithPlayer2, align 1
  %10 = load i8, i8* %collideWithPlayer1, align 1
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %11 = load i8, i8* %collideWithPlayer2, align 1
  %tobool8 = trunc i8 %11 to i1
  br i1 %tobool8, label %if.then9, label %if.end41

if.then9:                                         ; preds = %lor.lhs.false, %for.end
  %12 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir10 = getelementptr inbounds %struct.Ball, %struct.Ball* %12, i32 0, i32 1
  %x = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir10, i32 0, i32 0
  %13 = load float, float* %x, align 4
  %mul11 = fmul float %13, -1.000000e+00
  %14 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir12 = getelementptr inbounds %struct.Ball, %struct.Ball* %14, i32 0, i32 1
  %x13 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir12, i32 0, i32 0
  store float %mul11, float* %x13, align 4
  %15 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %velocity = getelementptr inbounds %struct.Ball, %struct.Ball* %15, i32 0, i32 2
  %16 = load float, float* %velocity, align 4
  %conv = fpext float %16 to double
  %add = fadd double %conv, 2.500000e+01
  %conv14 = fptrunc double %add to float
  %17 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %velocity15 = getelementptr inbounds %struct.Ball, %struct.Ball* %17, i32 0, i32 2
  store float %conv14, float* %velocity15, align 4
  %18 = load i8, i8* %collideWithPlayer1, align 1
  %tobool16 = trunc i8 %18 to i1
  br i1 %tobool16, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.then9
  %19 = load i32, i32* %player1dir.addr, align 4
  %cmp18 = icmp ne i32 %19, 0
  br i1 %cmp18, label %if.then20, label %if.end27

if.then20:                                        ; preds = %land.lhs.true
  %20 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir21 = getelementptr inbounds %struct.Ball, %struct.Ball* %20, i32 0, i32 1
  %y22 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir21, i32 0, i32 1
  %21 = load float, float* %y22, align 4
  %22 = load i32, i32* %player1dir.addr, align 4
  %conv23 = sitofp i32 %22 to float
  %mul24 = fmul float %21, %conv23
  %23 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir25 = getelementptr inbounds %struct.Ball, %struct.Ball* %23, i32 0, i32 1
  %y26 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir25, i32 0, i32 1
  store float %mul24, float* %y26, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then20, %land.lhs.true, %if.then9
  %24 = load i8, i8* %collideWithPlayer2, align 1
  %tobool28 = trunc i8 %24 to i1
  br i1 %tobool28, label %land.lhs.true30, label %if.end40

land.lhs.true30:                                  ; preds = %if.end27
  %25 = load i32, i32* %player2dir.addr, align 4
  %cmp31 = icmp ne i32 %25, 0
  br i1 %cmp31, label %if.then33, label %if.end40

if.then33:                                        ; preds = %land.lhs.true30
  %26 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir34 = getelementptr inbounds %struct.Ball, %struct.Ball* %26, i32 0, i32 1
  %y35 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir34, i32 0, i32 1
  %27 = load float, float* %y35, align 4
  %28 = load i32, i32* %player2dir.addr, align 4
  %conv36 = sitofp i32 %28 to float
  %mul37 = fmul float %27, %conv36
  %29 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %dir38 = getelementptr inbounds %struct.Ball, %struct.Ball* %29, i32 0, i32 1
  %y39 = getelementptr inbounds %struct.Vec2, %struct.Vec2* %dir38, i32 0, i32 1
  store float %mul37, float* %y39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then33, %land.lhs.true30, %if.end27
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %lor.lhs.false
  %30 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere42 = getelementptr inbounds %struct.Ball, %struct.Ball* %30, i32 0, i32 0
  %x43 = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere42, i32 0, i32 0
  %31 = load i32, i32* %x43, align 4
  %cmp44 = icmp slt i32 %31, 0
  br i1 %cmp44, label %if.then46, label %if.else

if.then46:                                        ; preds = %if.end41
  %32 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  call void @resetGame(%struct.Player* @player2, %struct.Ball* %32, i32 1)
  br label %if.end55

if.else:                                          ; preds = %if.end41
  %33 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere47 = getelementptr inbounds %struct.Ball, %struct.Ball* %33, i32 0, i32 0
  %x48 = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere47, i32 0, i32 0
  %34 = load i32, i32* %x48, align 4
  %35 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  %sphere49 = getelementptr inbounds %struct.Ball, %struct.Ball* %35, i32 0, i32 0
  %width = getelementptr inbounds %struct.Rect, %struct.Rect* %sphere49, i32 0, i32 2
  %36 = load i32, i32* %width, align 4
  %add50 = add nsw i32 %34, %36
  %cmp51 = icmp sgt i32 %add50, 800
  br i1 %cmp51, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.else
  %37 = load %struct.Ball*, %struct.Ball** %ball.addr, align 4
  call void @resetGame(%struct.Player* @player1, %struct.Ball* %37, i32 -1)
  br label %if.end54

if.end54:                                         ; preds = %if.then53, %if.else
  br label %if.end55

if.end55:                                         ; preds = %if.end54, %if.then46
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @tick(float %delta, i32 %player1dir, i32 %player2dir) #0 {
entry:
  %delta.addr = alloca float, align 4
  %player1dir.addr = alloca i32, align 4
  %player2dir.addr = alloca i32, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 %player1dir, i32* %player1dir.addr, align 4
  store i32 %player2dir, i32* %player2dir.addr, align 4
  call void @drawCourt()
  call void @drawScores()
  %0 = load i32, i32* %player1dir.addr, align 4
  %1 = load i32, i32* %player2dir.addr, align 4
  call void @checkCollisions(%struct.Ball* @ball, i32 %0, i32 %1)
  %2 = load i32, i32* %player1dir.addr, align 4
  %3 = load float, float* %delta.addr, align 4
  call void @movePlayer(%struct.Player* @player1, i32 %2, float %3)
  %4 = load i32, i32* %player2dir.addr, align 4
  %5 = load float, float* %delta.addr, align 4
  call void @movePlayer(%struct.Player* @player2, i32 %4, float %5)
  %6 = load float, float* %delta.addr, align 4
  call void @moveBall(%struct.Ball* @ball, float %6)
  call void @drawObject(%struct.Rect* byval(%struct.Rect) align 4 getelementptr inbounds (%struct.Ball, %struct.Ball* @ball, i32 0, i32 0))
  call void @drawObject(%struct.Rect* byval(%struct.Rect) align 4 getelementptr inbounds (%struct.Player, %struct.Player* @player1, i32 0, i32 0))
  call void @drawObject(%struct.Rect* byval(%struct.Rect) align 4 getelementptr inbounds (%struct.Player, %struct.Player* @player2, i32 0, i32 0))
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
