(module
  (type (;0;) (func (result i32)))
  (type (;1;) (func))
  (type (;2;) (func (param i32)))
  (type (;3;) (func (param i32 i32 i32)))
  (type (;4;) (func (param i32 i32 i32 i32)))
  (type (;5;) (func (param i32 i32) (result i32)))
  (type (;6;) (func (param i32 i32)))
  (type (;7;) (func (param i32 i32 f32)))
  (type (;8;) (func (param i32 f32)))
  (type (;9;) (func (param f32 i32 i32)))
  (type (;10;) (func (param i32 i32 i32 i32) (result i32)))
  (import "env" "setFillStyle" (func (;0;) (type 3)))
  (import "env" "setStrokeStyle" (func (;1;) (type 3)))
  (import "env" "fillRect" (func (;2;) (type 4)))
  (import "env" "setTextSize" (func (;3;) (type 2)))
  (import "env" "fillText" (func (;4;) (type 3)))
  (import "env" "setLineDash" (func (;5;) (type 6)))
  (import "env" "setLineWidth" (func (;6;) (type 2)))
  (import "env" "drawLine" (func (;7;) (type 4)))
  (import "env" "stackSave" (func (;8;) (type 0)))
  (import "env" "stackRestore" (func (;9;) (type 2)))
  (import "env" "g$white" (func (;10;) (type 0)))
  (import "env" "g$player1" (func (;11;) (type 0)))
  (import "env" "g$player2" (func (;12;) (type 0)))
  (import "env" "g$black" (func (;13;) (type 0)))
  (import "env" "g$walls" (func (;14;) (type 0)))
  (import "env" "g$line" (func (;15;) (type 0)))
  (import "env" "g$ball" (func (;16;) (type 0)))
  (import "env" "__memory_base" (global (;0;) i32))
  (import "env" "__table_base" (global (;1;) i32))
  (import "env" "memory" (memory (;0;) 0))
  (import "env" "table" (table (;0;) 0 funcref))
  (func (;17;) (type 1)
    call 18)
  (func (;18;) (type 1)
    nop)
  (func (;19;) (type 2) (param i32)
    (local i32 i32 i32)
    local.get 0
    i32.load
    local.set 1
    local.get 0
    i32.load offset=4
    local.set 2
    local.get 0
    i32.load offset=8
    local.set 3
    local.get 1
    local.get 2
    local.get 3
    call 0
    nop)
  (func (;20;) (type 2) (param i32)
    (local i32 i32 i32)
    local.get 0
    i32.load
    local.set 1
    local.get 0
    i32.load offset=4
    local.set 2
    local.get 0
    i32.load offset=8
    local.set 3
    local.get 1
    local.get 2
    local.get 3
    call 1
    nop)
  (func (;21;) (type 10) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    call 8
    local.set 4
    i32.const 32
    local.set 5
    local.get 4
    i32.const 32
    i32.sub
    local.set 6
    local.get 6
    local.get 0
    i32.store offset=24
    local.get 6
    local.get 1
    i32.store offset=20
    local.get 6
    local.get 2
    i32.store offset=16
    local.get 6
    local.get 3
    i32.store offset=12
    local.get 6
    i32.load offset=24
    local.set 7
    local.get 6
    i32.load offset=16
    local.set 8
    local.get 7
    local.set 9
    local.get 8
    local.set 10
    local.get 9
    local.get 10
    i32.lt_s
    local.set 11
    i32.const 1
    local.set 12
    local.get 11
    i32.const 1
    i32.and
    local.set 13
    block  ;; label = @1
      local.get 13
      if  ;; label = @2
        nop
        local.get 6
        i32.load offset=16
        local.set 14
        local.get 6
        local.get 14
        i32.store offset=28
        br 1 (;@1;)
      end
      local.get 6
      i32.load offset=24
      local.set 15
      local.get 6
      i32.load offset=20
      local.set 16
      local.get 15
      local.get 16
      i32.add
      local.set 17
      local.get 6
      i32.load offset=12
      local.set 18
      local.get 17
      local.set 19
      local.get 18
      local.set 20
      local.get 19
      local.get 20
      i32.gt_s
      local.set 21
      i32.const 1
      local.set 22
      local.get 21
      i32.const 1
      i32.and
      local.set 23
      block  ;; label = @2
        local.get 23
        if  ;; label = @3
          nop
          local.get 6
          i32.load offset=12
          local.set 24
          local.get 6
          i32.load offset=20
          local.set 25
          local.get 24
          local.get 25
          i32.sub
          local.set 26
          local.get 26
          local.set 27
          br 1 (;@2;)
        end
        local.get 6
        i32.load offset=24
        local.set 28
        local.get 28
        local.set 27
      end
      local.get 27
      local.set 29
      local.get 6
      local.get 29
      i32.store offset=28
    end
    local.get 6
    i32.load offset=28
    local.set 30
    local.get 30)
  (func (;22;) (type 5) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f32 f32 f32 f32 f32 f32 f32 f32)
    i32.const 0
    local.set 2
    local.get 0
    f32.load
    local.set 27
    local.get 1
    i32.load
    local.set 3
    local.get 3
    f32.convert_i32_s
    local.set 28
    local.get 27
    local.get 28
    f32.gt
    local.set 4
    i32.const 1
    local.set 5
    local.get 4
    i32.const 1
    i32.and
    local.set 6
    i32.const 0
    local.set 7
    block  ;; label = @1
      local.get 6
      i32.eqz
      br_if 0 (;@1;)
      i32.const 0
      local.set 8
      local.get 0
      f32.load
      local.set 29
      local.get 1
      i32.load
      local.set 9
      local.get 1
      i32.load offset=8
      local.set 10
      local.get 9
      local.get 10
      i32.add
      local.set 11
      local.get 11
      f32.convert_i32_s
      local.set 30
      local.get 29
      local.get 30
      f32.lt
      local.set 12
      i32.const 1
      local.set 13
      local.get 12
      i32.const 1
      i32.and
      local.set 14
      i32.const 0
      local.set 7
      local.get 14
      i32.eqz
      br_if 0 (;@1;)
      i32.const 0
      local.set 15
      local.get 0
      f32.load offset=4
      local.set 31
      local.get 1
      i32.load offset=4
      local.set 16
      local.get 16
      f32.convert_i32_s
      local.set 32
      local.get 31
      local.get 32
      f32.gt
      local.set 17
      i32.const 1
      local.set 18
      local.get 17
      i32.const 1
      i32.and
      local.set 19
      i32.const 0
      local.set 7
      local.get 19
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      f32.load offset=4
      local.set 33
      local.get 1
      i32.load offset=4
      local.set 20
      local.get 1
      i32.load offset=12
      local.set 21
      local.get 20
      local.get 21
      i32.add
      local.set 22
      local.get 22
      f32.convert_i32_s
      local.set 34
      local.get 33
      local.get 34
      f32.lt
      local.set 23
      local.get 23
      local.set 7
    end
    local.get 7
    local.set 24
    i32.const 1
    local.set 25
    local.get 24
    i32.const 1
    i32.and
    local.set 26
    local.get 26)
  (func (;23;) (type 5) (param i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64 i64 i64 f32 f32 f32 f32 f32 f32 f32 f32)
    call 8
    local.set 2
    nop
    local.get 2
    i32.const 80
    i32.sub
    local.set 4
    local.get 4
    call 9
    nop
    local.get 0
    i32.load
    local.set 6
    local.get 6
    f32.convert_i32_s
    local.set 61
    local.get 4
    local.get 61
    f32.store offset=32
    local.get 0
    i32.load offset=4
    local.set 7
    local.get 7
    f32.convert_i32_s
    local.set 62
    local.get 4
    local.get 62
    f32.store offset=36
    local.get 0
    i32.load
    local.set 8
    local.get 0
    i32.load offset=8
    local.set 9
    local.get 8
    local.get 9
    i32.add
    local.set 10
    local.get 10
    f32.convert_i32_s
    local.set 63
    local.get 4
    local.get 63
    f32.store offset=40
    local.get 0
    i32.load offset=4
    local.set 11
    local.get 11
    f32.convert_i32_s
    local.set 64
    local.get 4
    local.get 64
    f32.store offset=44
    local.get 0
    i32.load
    local.set 12
    local.get 0
    i32.load offset=8
    local.set 13
    local.get 12
    local.get 13
    i32.add
    local.set 14
    local.get 14
    f32.convert_i32_s
    local.set 65
    local.get 4
    local.get 65
    f32.store offset=48
    local.get 0
    i32.load offset=4
    local.set 15
    local.get 0
    i32.load offset=12
    local.set 16
    local.get 15
    local.get 16
    i32.add
    local.set 17
    local.get 17
    f32.convert_i32_s
    local.set 66
    local.get 4
    local.get 66
    f32.store offset=52
    local.get 0
    i32.load
    local.set 18
    local.get 18
    f32.convert_i32_s
    local.set 67
    local.get 4
    local.get 67
    f32.store offset=56
    local.get 0
    i32.load offset=4
    local.set 19
    local.get 0
    i32.load offset=12
    local.set 20
    local.get 19
    local.get 20
    i32.add
    local.set 21
    local.get 21
    f32.convert_i32_s
    local.set 68
    local.get 4
    local.get 68
    f32.store offset=60
    local.get 4
    i32.const 0
    i32.store offset=28
    block  ;; label = @1
      loop  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            nop
            local.get 4
            i32.load offset=28
            local.set 23
            local.get 23
            local.set 24
            nop
            local.get 24
            i32.const 4
            i32.lt_s
            local.set 26
            nop
            local.get 26
            i32.const 1
            i32.and
            local.set 28
            local.get 28
            i32.eqz
            br_if 1 (;@3;)
            nop
            local.get 4
            i32.const 32
            i32.add
            local.set 30
            local.get 30
            local.set 31
            local.get 4
            i32.load offset=28
            local.set 32
            nop
            local.get 32
            i32.const 3
            i32.shl
            local.set 34
            local.get 31
            local.get 34
            i32.add
            local.set 35
            local.get 35
            i64.load align=4
            local.set 58
            local.get 4
            local.get 58
            i64.store offset=16
            nop
            nop
            local.get 1
            i64.load offset=8 align=4
            local.set 59
            nop
            local.get 4
            local.get 59
            i64.store offset=8
            local.get 1
            i64.load align=4
            local.set 60
            local.get 4
            local.get 60
            i64.store
            nop
            local.get 4
            i32.const 16
            i32.add
            local.set 40
            local.get 40
            local.get 4
            call 22
            local.set 41
            nop
            local.get 41
            i32.const 1
            i32.and
            local.set 43
            local.get 43
            if  ;; label = @5
              nop
              nop
              nop
              nop
              local.get 4
              i32.const 1
              i32.store8 offset=79
              br 4 (;@1;)
            end
            local.get 4
            i32.load offset=28
            local.set 47
            nop
            local.get 47
            i32.const 1
            i32.add
            local.set 49
            local.get 4
            local.get 49
            i32.store offset=28
            br 2 (;@2;)
            unreachable
          end
          unreachable
          unreachable
        end
      end
      nop
      nop
      nop
      local.get 4
      i32.const 0
      i32.store8 offset=79
    end
    local.get 4
    i32.load8_u offset=79
    local.set 53
    nop
    local.get 53
    i32.const 1
    i32.and
    local.set 55
    nop
    local.get 4
    i32.const 80
    i32.add
    local.set 57
    local.get 57
    call 9
    local.get 55)
  (func (;24;) (type 7) (param i32 i32 f32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f32 f32 f32 f32 f32 f32 f32 f64 f64 f64)
    call 8
    local.set 3
    i32.const 32
    local.set 4
    local.get 3
    i32.const 32
    i32.sub
    local.set 5
    local.get 5
    call 9
    i32.const 16
    local.set 6
    i32.const 584
    local.set 7
    local.get 5
    local.get 0
    i32.store offset=28
    local.get 5
    local.get 1
    i32.store offset=24
    local.get 5
    local.get 2
    f32.store offset=20
    local.get 5
    i32.load offset=28
    local.set 8
    local.get 8
    i32.load offset=4
    local.set 9
    local.get 5
    local.get 9
    i32.store offset=16
    local.get 5
    f32.load offset=20
    local.set 27
    f32.const 0x1.f4p+9 (;=1000;)
    local.set 28
    local.get 27
    f32.const 0x1.f4p+9 (;=1000;)
    f32.div
    local.set 29
    local.get 29
    f64.promote_f32
    local.set 34
    f64.const 0x1.9p+8 (;=400;)
    local.set 35
    local.get 34
    f64.const 0x1.9p+8 (;=400;)
    f64.mul
    local.set 36
    local.get 36
    f32.demote_f64
    local.set 30
    local.get 5
    local.get 30
    f32.store offset=12
    local.get 5
    i32.load offset=16
    local.set 10
    local.get 5
    f32.load offset=12
    local.set 31
    local.get 31
    f32.abs
    local.set 32
    f32.const 0x1p+31 (;=2.14748e+09;)
    local.set 33
    local.get 32
    f32.const 0x1p+31 (;=2.14748e+09;)
    f32.lt
    local.set 11
    local.get 11
    i32.eqz
    local.set 12
    block  ;; label = @1
      local.get 12
      i32.eqz
      if  ;; label = @2
        nop
        local.get 31
        i32.trunc_f32_s
        local.set 13
        local.get 13
        local.set 14
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 15
      i32.const -2147483648
      local.set 14
    end
    local.get 14
    local.set 16
    local.get 5
    i32.load offset=24
    local.set 17
    local.get 16
    local.get 17
    i32.mul
    local.set 18
    local.get 10
    local.get 18
    i32.add
    local.set 19
    local.get 5
    local.get 19
    i32.store offset=8
    local.get 5
    i32.load offset=8
    local.set 20
    local.get 5
    i32.load offset=28
    local.set 21
    local.get 21
    i32.load offset=12
    local.set 22
    local.get 20
    local.get 22
    i32.const 16
    i32.const 584
    call 21
    local.set 23
    local.get 5
    i32.load offset=28
    local.set 24
    local.get 24
    local.get 23
    i32.store offset=4
    i32.const 32
    local.set 25
    local.get 5
    i32.const 32
    i32.add
    local.set 26
    local.get 26
    call 9
    nop)
  (func (;25;) (type 8) (param i32 f32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32)
    call 8
    local.set 2
    i32.const 32
    local.set 3
    local.get 2
    i32.const 32
    i32.sub
    local.set 4
    f32.const 0x1.f4p+9 (;=1000;)
    local.set 42
    local.get 4
    local.get 0
    i32.store offset=28
    local.get 4
    local.get 1
    f32.store offset=24
    local.get 4
    i32.load offset=28
    local.set 5
    local.get 5
    i32.load
    local.set 6
    local.get 4
    local.get 6
    i32.store offset=20
    local.get 4
    i32.load offset=28
    local.set 7
    local.get 7
    i32.load offset=4
    local.set 8
    local.get 4
    local.get 8
    i32.store offset=16
    local.get 4
    f32.load offset=24
    local.set 43
    local.get 43
    f32.const 0x1.f4p+9 (;=1000;)
    f32.div
    local.set 44
    local.get 4
    i32.load offset=28
    local.set 9
    local.get 9
    f32.load offset=24
    local.set 45
    local.get 44
    local.get 45
    f32.mul
    local.set 46
    local.get 4
    local.get 46
    f32.store offset=12
    local.get 4
    i32.load offset=20
    local.set 10
    local.get 10
    f32.convert_i32_s
    local.set 47
    local.get 4
    f32.load offset=12
    local.set 48
    local.get 48
    f32.abs
    local.set 49
    f32.const 0x1p+31 (;=2.14748e+09;)
    local.set 50
    local.get 49
    f32.const 0x1p+31 (;=2.14748e+09;)
    f32.lt
    local.set 11
    local.get 11
    i32.eqz
    local.set 12
    block  ;; label = @1
      local.get 12
      i32.eqz
      if  ;; label = @2
        nop
        local.get 48
        i32.trunc_f32_s
        local.set 13
        local.get 13
        local.set 14
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 15
      i32.const -2147483648
      local.set 14
    end
    local.get 14
    local.set 16
    local.get 16
    f32.convert_i32_s
    local.set 51
    local.get 4
    i32.load offset=28
    local.set 17
    local.get 17
    f32.load offset=16
    local.set 52
    local.get 51
    local.get 52
    f32.mul
    local.set 53
    local.get 47
    local.get 53
    f32.add
    local.set 54
    local.get 54
    f32.abs
    local.set 55
    f32.const 0x1p+31 (;=2.14748e+09;)
    local.set 56
    local.get 55
    f32.const 0x1p+31 (;=2.14748e+09;)
    f32.lt
    local.set 18
    local.get 18
    i32.eqz
    local.set 19
    block  ;; label = @1
      local.get 19
      i32.eqz
      if  ;; label = @2
        nop
        local.get 54
        i32.trunc_f32_s
        local.set 20
        local.get 20
        local.set 21
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 22
      i32.const -2147483648
      local.set 21
    end
    local.get 21
    local.set 23
    local.get 4
    local.get 23
    i32.store offset=8
    local.get 4
    i32.load offset=16
    local.set 24
    local.get 24
    f32.convert_i32_s
    local.set 57
    local.get 4
    f32.load offset=12
    local.set 58
    local.get 58
    f32.abs
    local.set 59
    f32.const 0x1p+31 (;=2.14748e+09;)
    local.set 60
    local.get 59
    f32.const 0x1p+31 (;=2.14748e+09;)
    f32.lt
    local.set 25
    local.get 25
    i32.eqz
    local.set 26
    block  ;; label = @1
      local.get 26
      i32.eqz
      if  ;; label = @2
        nop
        local.get 58
        i32.trunc_f32_s
        local.set 27
        local.get 27
        local.set 28
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 29
      i32.const -2147483648
      local.set 28
    end
    local.get 28
    local.set 30
    local.get 30
    f32.convert_i32_s
    local.set 61
    local.get 4
    i32.load offset=28
    local.set 31
    local.get 31
    f32.load offset=20
    local.set 62
    local.get 61
    local.get 62
    f32.mul
    local.set 63
    local.get 57
    local.get 63
    f32.add
    local.set 64
    local.get 64
    f32.abs
    local.set 65
    f32.const 0x1p+31 (;=2.14748e+09;)
    local.set 66
    local.get 65
    f32.const 0x1p+31 (;=2.14748e+09;)
    f32.lt
    local.set 32
    local.get 32
    i32.eqz
    local.set 33
    block  ;; label = @1
      local.get 33
      i32.eqz
      if  ;; label = @2
        nop
        local.get 64
        i32.trunc_f32_s
        local.set 34
        local.get 34
        local.set 35
        br 1 (;@1;)
      end
      i32.const -2147483648
      local.set 36
      i32.const -2147483648
      local.set 35
    end
    local.get 35
    local.set 37
    local.get 4
    local.get 37
    i32.store offset=4
    local.get 4
    i32.load offset=8
    local.set 38
    local.get 4
    i32.load offset=28
    local.set 39
    local.get 39
    local.get 38
    i32.store
    local.get 4
    i32.load offset=4
    local.set 40
    local.get 4
    i32.load offset=28
    local.set 41
    local.get 41
    local.get 40
    i32.store offset=4
    nop)
  (func (;26;) (type 2) (param i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    call 8
    local.set 1
    nop
    local.get 1
    i32.const 16
    i32.sub
    local.set 3
    local.get 3
    call 9
    global.get 2
    local.set 4
    nop
    nop
    local.get 4
    i32.load offset=8
    local.set 7
    nop
    local.get 3
    local.get 7
    i32.store offset=8
    local.get 4
    i64.load align=4
    local.set 15
    local.get 3
    local.get 15
    i64.store
    local.get 3
    call 19
    local.get 0
    i32.load
    local.set 9
    local.get 0
    i32.load offset=4
    local.set 10
    local.get 0
    i32.load offset=8
    local.set 11
    local.get 0
    i32.load offset=12
    local.set 12
    local.get 9
    local.get 10
    local.get 11
    local.get 12
    call 2
    nop
    local.get 3
    i32.const 16
    i32.add
    local.set 14
    local.get 14
    call 9
    nop)
  (func (;27;) (type 1)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32)
    i32.const 464
    local.set 0
    i32.const 80
    local.set 1
    i32.const 64
    local.set 2
    i32.const 64
    call 3
    global.get 3
    local.set 3
    local.get 3
    i32.load offset=16
    local.set 4
    i32.const 80
    local.set 5
    i32.const 304
    local.set 6
    local.get 4
    i32.const 304
    i32.const 80
    call 4
    global.get 4
    local.set 7
    local.get 7
    i32.load offset=16
    local.set 8
    local.get 8
    i32.const 464
    i32.const 80
    call 4
    nop)
  (func (;28;) (type 3) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 f32 f32 f32)
    call 8
    local.set 3
    i32.const 16
    local.set 4
    local.get 3
    i32.const 16
    i32.sub
    local.set 5
    f32.const 0x1.2cp+7 (;=150;)
    local.set 18
    f32.const 0x1p+0 (;=1;)
    local.set 19
    i32.const 16
    local.set 6
    i32.const 392
    local.set 7
    local.get 5
    local.get 0
    i32.store offset=12
    local.get 5
    local.get 1
    i32.store offset=8
    local.get 5
    local.get 2
    i32.store offset=4
    local.get 5
    i32.load offset=12
    local.set 8
    local.get 8
    i32.load offset=16
    local.set 9
    i32.const 1
    local.set 10
    local.get 9
    i32.const 1
    i32.add
    local.set 11
    local.get 8
    local.get 11
    i32.store offset=16
    local.get 5
    i32.load offset=8
    local.set 12
    local.get 12
    i32.const 392
    i32.store
    local.get 5
    i32.load offset=8
    local.set 13
    local.get 13
    i32.const 16
    i32.store offset=4
    local.get 5
    i32.load offset=4
    local.set 14
    local.get 14
    f32.convert_i32_s
    local.set 20
    local.get 5
    i32.load offset=8
    local.set 15
    local.get 15
    local.get 20
    f32.store offset=16
    local.get 5
    i32.load offset=8
    local.set 16
    local.get 16
    f32.const 0x1p+0 (;=1;)
    f32.store offset=20
    local.get 5
    i32.load offset=8
    local.set 17
    local.get 17
    f32.const 0x1.2cp+7 (;=150;)
    f32.store offset=24
    nop)
  (func (;29;) (type 1)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64 i64 i64 i64 i64)
    call 8
    local.set 0
    nop
    local.get 0
    i32.const 80
    i32.sub
    local.set 2
    local.get 2
    call 9
    global.get 5
    local.set 3
    nop
    nop
    local.get 3
    i32.load offset=8
    local.set 6
    nop
    nop
    nop
    local.get 2
    local.get 6
    i32.store offset=48
    local.get 3
    i64.load align=4
    local.set 64
    local.get 2
    local.get 64
    i64.store offset=40
    nop
    local.get 2
    i32.const 40
    i32.add
    local.set 11
    local.get 11
    call 19
    nop
    nop
    nop
    i32.const 0
    i32.const 0
    i32.const 800
    i32.const 600
    call 2
    global.get 2
    local.set 15
    nop
    nop
    local.get 15
    i32.load offset=8
    local.set 18
    nop
    nop
    nop
    local.get 2
    local.get 18
    i32.store offset=64
    local.get 15
    i64.load align=4
    local.set 65
    local.get 2
    local.get 65
    i64.store offset=56
    nop
    local.get 2
    i32.const 56
    i32.add
    local.set 23
    local.get 23
    call 19
    nop
    nop
    local.get 2
    i32.const 2
    i32.store offset=76
    local.get 2
    i32.const 0
    i32.store offset=72
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 2
          i32.load offset=72
          local.set 26
          local.get 2
          i32.load offset=76
          local.set 27
          local.get 26
          local.set 28
          local.get 27
          local.set 29
          local.get 28
          local.get 29
          i32.lt_s
          local.set 30
          nop
          local.get 30
          i32.const 1
          i32.and
          local.set 32
          local.get 32
          i32.eqz
          br_if 1 (;@2;)
          local.get 2
          i32.load offset=72
          local.set 33
          nop
          local.get 33
          i32.const 4
          i32.shl
          local.set 35
          global.get 6
          local.set 36
          local.get 35
          local.get 36
          i32.add
          local.set 37
          nop
          nop
          local.get 37
          i64.load offset=8 align=4
          local.set 66
          nop
          nop
          nop
          local.get 2
          local.get 66
          i64.store offset=16
          local.get 37
          i64.load align=4
          local.set 67
          local.get 2
          local.get 67
          i64.store offset=8
          nop
          local.get 2
          i32.const 8
          i32.add
          local.set 44
          local.get 44
          call 26
          local.get 2
          i32.load offset=72
          local.set 45
          nop
          local.get 45
          i32.const 1
          i32.add
          local.set 47
          local.get 2
          local.get 47
          i32.store offset=72
          br 2 (;@1;)
          unreachable
        end
        unreachable
        unreachable
      end
    end
    global.get 2
    local.set 48
    nop
    nop
    local.get 48
    i32.load offset=8
    local.set 51
    nop
    nop
    nop
    local.get 2
    local.get 51
    i32.store offset=32
    local.get 48
    i64.load align=4
    local.set 68
    local.get 2
    local.get 68
    i64.store offset=24
    nop
    local.get 2
    i32.const 24
    i32.add
    local.set 56
    local.get 56
    call 20
    i32.const 8
    i32.const 8
    call 5
    i32.const 8
    call 6
    global.get 7
    local.set 57
    local.get 57
    i32.load
    local.set 58
    local.get 57
    i32.load offset=4
    local.set 59
    local.get 57
    i32.load offset=8
    local.set 60
    local.get 57
    i32.load offset=12
    local.set 61
    local.get 58
    local.get 59
    local.get 60
    local.get 61
    call 7
    nop
    local.get 2
    i32.const 80
    i32.add
    local.set 63
    local.get 63
    call 9
    nop)
  (func (;30;) (type 3) (param i32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 i64 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f32 f64 f64 f64)
    call 8
    local.set 3
    nop
    local.get 3
    i32.const 128
    i32.sub
    local.set 5
    local.get 5
    call 9
    nop
    nop
    local.get 5
    local.get 0
    i32.store offset=124
    local.get 5
    local.get 1
    i32.store offset=120
    local.get 5
    local.get 2
    i32.store offset=116
    local.get 5
    i32.const 2
    i32.store offset=112
    local.get 5
    i32.const 0
    i32.store offset=108
    loop  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 5
          i32.load offset=108
          local.set 8
          local.get 5
          i32.load offset=112
          local.set 9
          local.get 8
          local.set 10
          local.get 9
          local.set 11
          local.get 10
          local.get 11
          i32.lt_s
          local.set 12
          nop
          local.get 12
          i32.const 1
          i32.and
          local.set 14
          local.get 14
          i32.eqz
          br_if 1 (;@2;)
          local.get 5
          i32.load offset=124
          local.set 15
          local.get 5
          i32.load offset=108
          local.set 16
          nop
          local.get 16
          i32.const 4
          i32.shl
          local.set 18
          global.get 6
          local.set 19
          local.get 18
          local.get 19
          i32.add
          local.set 20
          nop
          nop
          local.get 15
          i64.load offset=8 align=4
          local.set 128
          nop
          nop
          nop
          local.get 5
          local.get 128
          i64.store offset=32
          local.get 15
          i64.load align=4
          local.set 129
          local.get 5
          local.get 129
          i64.store offset=24
          nop
          local.get 20
          i64.load offset=8 align=4
          local.set 130
          nop
          nop
          nop
          local.get 5
          local.get 130
          i64.store offset=16
          local.get 20
          i64.load align=4
          local.set 131
          local.get 5
          local.get 131
          i64.store offset=8
          nop
          local.get 5
          i32.const 24
          i32.add
          local.set 31
          nop
          local.get 5
          i32.const 8
          i32.add
          local.set 33
          local.get 31
          local.get 33
          call 23
          local.set 34
          nop
          local.get 34
          i32.const 1
          i32.and
          local.set 36
          local.get 36
          if  ;; label = @4
            nop
            nop
            local.get 5
            i32.load offset=124
            local.set 37
            local.get 37
            f32.load offset=20
            local.set 141
            local.get 141
            f32.const -0x1p+0 (;=-1;)
            f32.mul
            local.set 142
            local.get 5
            i32.load offset=124
            local.set 38
            local.get 38
            local.get 142
            f32.store offset=20
          end
          local.get 5
          i32.load offset=108
          local.set 39
          nop
          local.get 39
          i32.const 1
          i32.add
          local.set 41
          local.get 5
          local.get 41
          i32.store offset=108
          br 2 (;@1;)
          unreachable
        end
        unreachable
        unreachable
      end
    end
    local.get 5
    i32.load offset=124
    local.set 42
    nop
    nop
    local.get 42
    i64.load offset=8 align=4
    local.set 132
    nop
    nop
    nop
    local.get 5
    local.get 132
    i64.store offset=64
    local.get 42
    i64.load align=4
    local.set 133
    local.get 5
    local.get 133
    i64.store offset=56
    global.get 3
    local.set 48
    nop
    local.get 48
    i64.load offset=8 align=4
    local.set 134
    nop
    nop
    nop
    local.get 5
    local.get 134
    i64.store offset=48
    local.get 48
    i64.load align=4
    local.set 135
    local.get 5
    local.get 135
    i64.store offset=40
    nop
    local.get 5
    i32.const 56
    i32.add
    local.set 54
    nop
    local.get 5
    i32.const 40
    i32.add
    local.set 56
    local.get 54
    local.get 56
    call 23
    local.set 57
    nop
    local.get 57
    i32.const 1
    i32.and
    local.set 59
    local.get 5
    local.get 59
    i32.store8 offset=107
    local.get 5
    i32.load offset=124
    local.set 60
    nop
    nop
    local.get 60
    i64.load offset=8 align=4
    local.set 136
    nop
    nop
    nop
    local.get 5
    local.get 136
    i64.store offset=96
    local.get 60
    i64.load align=4
    local.set 137
    local.get 5
    local.get 137
    i64.store offset=88
    global.get 4
    local.set 66
    nop
    local.get 66
    i64.load offset=8 align=4
    local.set 138
    nop
    nop
    nop
    local.get 5
    local.get 138
    i64.store offset=80
    local.get 66
    i64.load align=4
    local.set 139
    local.get 5
    local.get 139
    i64.store offset=72
    nop
    local.get 5
    i32.const 88
    i32.add
    local.set 72
    nop
    local.get 5
    i32.const 72
    i32.add
    local.set 74
    local.get 72
    local.get 74
    call 23
    local.set 75
    nop
    local.get 75
    i32.const 1
    i32.and
    local.set 77
    local.get 5
    local.get 77
    i32.store8 offset=106
    local.get 5
    i32.load8_u offset=107
    local.set 78
    nop
    local.get 78
    i32.const 1
    i32.and
    local.set 80
    block  ;; label = @1
      local.get 80
      i32.eqz
      if  ;; label = @2
        nop
        local.get 5
        i32.load8_u offset=106
        local.set 81
        nop
        local.get 81
        i32.const 1
        i32.and
        local.set 83
        local.get 83
        i32.eqz
        br_if 1 (;@1;)
      end
      local.get 5
      i32.load offset=124
      local.set 84
      local.get 84
      f32.load offset=16
      local.set 143
      local.get 143
      f32.neg
      local.set 144
      local.get 84
      local.get 144
      f32.store offset=16
      local.get 5
      i32.load offset=124
      local.set 85
      local.get 85
      f32.load offset=24
      local.set 145
      local.get 145
      f64.promote_f32
      local.set 153
      nop
      local.get 153
      f64.const 0x1.9p+4 (;=25;)
      f64.add
      local.set 155
      local.get 155
      f32.demote_f64
      local.set 146
      local.get 5
      i32.load offset=124
      local.set 86
      local.get 86
      local.get 146
      f32.store offset=24
      local.get 5
      i32.load8_u offset=107
      local.set 87
      nop
      local.get 87
      i32.const 1
      i32.and
      local.set 89
      block  ;; label = @2
        local.get 89
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i32.load offset=120
        local.set 90
        local.get 90
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i32.load offset=124
        local.set 91
        local.get 91
        f32.load offset=20
        local.set 147
        local.get 5
        i32.load offset=120
        local.set 92
        local.get 92
        f32.convert_i32_s
        local.set 148
        local.get 147
        local.get 148
        f32.mul
        local.set 149
        local.get 5
        i32.load offset=124
        local.set 93
        local.get 93
        local.get 149
        f32.store offset=20
      end
      local.get 5
      i32.load8_u offset=106
      local.set 94
      nop
      local.get 94
      i32.const 1
      i32.and
      local.set 96
      block  ;; label = @2
        local.get 96
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i32.load offset=116
        local.set 97
        local.get 97
        i32.eqz
        br_if 0 (;@2;)
        local.get 5
        i32.load offset=124
        local.set 98
        local.get 98
        f32.load offset=20
        local.set 150
        local.get 5
        i32.load offset=116
        local.set 99
        local.get 99
        f32.convert_i32_s
        local.set 151
        local.get 150
        local.get 151
        f32.mul
        local.set 152
        local.get 5
        i32.load offset=124
        local.set 100
        local.get 100
        local.get 152
        f32.store offset=20
      end
    end
    nop
    local.get 5
    i32.load offset=124
    local.set 102
    local.get 102
    i32.load
    local.set 103
    local.get 103
    local.set 104
    nop
    local.get 104
    i32.const 0
    i32.lt_s
    local.set 106
    nop
    local.get 106
    i32.const 1
    i32.and
    local.set 108
    block  ;; label = @1
      local.get 108
      if  ;; label = @2
        nop
        local.get 5
        i32.load offset=124
        local.set 109
        global.get 4
        local.set 110
        nop
        local.get 110
        local.get 109
        i32.const 1
        call 28
        br 1 (;@1;)
      end
      nop
      local.get 5
      i32.load offset=124
      local.set 113
      local.get 113
      i32.load
      local.set 114
      local.get 5
      i32.load offset=124
      local.set 115
      local.get 115
      i32.load offset=8
      local.set 116
      local.get 114
      local.get 116
      i32.add
      local.set 117
      local.get 117
      local.set 118
      nop
      local.get 118
      i32.const 800
      i32.gt_s
      local.set 120
      nop
      local.get 120
      i32.const 1
      i32.and
      local.set 122
      local.get 122
      if  ;; label = @2
        nop
        local.get 5
        i32.load offset=124
        local.set 123
        global.get 3
        local.set 124
        nop
        local.get 124
        local.get 123
        i32.const -1
        call 28
      end
    end
    nop
    local.get 5
    i32.const 128
    i32.add
    local.set 127
    local.get 127
    call 9
    nop)
  (func (;31;) (type 9) (param f32 i32 i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64 i64 i64 i64 i64 i64 f32 f32 f32)
    call 8
    local.set 3
    nop
    local.get 3
    i32.const 64
    i32.sub
    local.set 5
    local.get 5
    call 9
    local.get 5
    local.get 0
    f32.store offset=60
    local.get 5
    local.get 1
    i32.store offset=56
    local.get 5
    local.get 2
    i32.store offset=52
    call 29
    call 27
    local.get 5
    i32.load offset=56
    local.set 6
    local.get 5
    i32.load offset=52
    local.set 7
    global.get 8
    local.set 8
    local.get 8
    local.get 6
    local.get 7
    call 30
    local.get 5
    i32.load offset=56
    local.set 9
    local.get 5
    f32.load offset=60
    local.set 42
    global.get 3
    local.set 10
    local.get 10
    local.get 9
    local.get 42
    call 24
    local.get 5
    i32.load offset=52
    local.set 11
    local.get 5
    f32.load offset=60
    local.set 43
    global.get 4
    local.set 12
    local.get 12
    local.get 11
    local.get 43
    call 24
    local.get 5
    f32.load offset=60
    local.set 44
    global.get 8
    local.set 13
    local.get 13
    local.get 44
    call 25
    global.get 8
    local.set 14
    nop
    nop
    local.get 14
    i64.load offset=8 align=4
    local.set 36
    nop
    local.get 5
    local.get 36
    i64.store offset=8
    local.get 14
    i64.load align=4
    local.set 37
    local.get 5
    local.get 37
    i64.store
    local.get 5
    call 26
    global.get 3
    local.set 18
    nop
    nop
    local.get 18
    i64.load offset=8 align=4
    local.set 38
    nop
    nop
    nop
    local.get 5
    local.get 38
    i64.store offset=24
    local.get 18
    i64.load align=4
    local.set 39
    local.get 5
    local.get 39
    i64.store offset=16
    nop
    local.get 5
    i32.const 16
    i32.add
    local.set 25
    local.get 25
    call 26
    global.get 4
    local.set 26
    nop
    nop
    local.get 26
    i64.load offset=8 align=4
    local.set 40
    nop
    nop
    nop
    local.get 5
    local.get 40
    i64.store offset=40
    local.get 26
    i64.load align=4
    local.set 41
    local.get 5
    local.get 41
    i64.store offset=32
    nop
    local.get 5
    i32.const 32
    i32.add
    local.set 33
    local.get 33
    call 26
    nop
    local.get 5
    i32.const 64
    i32.add
    local.set 35
    local.get 35
    call 9
    nop)
  (func (;32;) (type 1)
    call 33
    call 17)
  (func (;33;) (type 1)
    call 10
    global.set 2
    call 11
    global.set 3
    call 12
    global.set 4
    call 13
    global.set 5
    call 14
    global.set 6
    call 15
    global.set 7
    call 16
    global.set 8)
  (global (;2;) (mut i32) (i32.const 0))
  (global (;3;) (mut i32) (i32.const 0))
  (global (;4;) (mut i32) (i32.const 0))
  (global (;5;) (mut i32) (i32.const 0))
  (global (;6;) (mut i32) (i32.const 0))
  (global (;7;) (mut i32) (i32.const 0))
  (global (;8;) (mut i32) (i32.const 0))
  (global (;9;) i32 (i32.const 0))
  (global (;10;) i32 (i32.const 80))
  (global (;11;) i32 (i32.const 100))
  (global (;12;) i32 (i32.const 12))
  (global (;13;) i32 (i32.const 32))
  (global (;14;) i32 (i32.const 64))
  (global (;15;) i32 (i32.const 120))
  (global (;16;) i32 (i32.const 0))
  (export "__wasm_apply_relocs" (func 18))
  (export "setFill" (func 19))
  (export "setStroke" (func 20))
  (export "clamp" (func 21))
  (export "isInBounds" (func 22))
  (export "doesCollide" (func 23))
  (export "movePlayer" (func 24))
  (export "moveBall" (func 25))
  (export "drawObject" (func 26))
  (export "white" (global 9))
  (export "drawScores" (func 27))
  (export "player1" (global 10))
  (export "player2" (global 11))
  (export "resetGame" (func 28))
  (export "drawCourt" (func 29))
  (export "black" (global 12))
  (export "walls" (global 13))
  (export "line" (global 14))
  (export "checkCollisions" (func 30))
  (export "tick" (func 31))
  (export "ball" (global 15))
  (export "__dso_handle" (global 16))
  (export "__post_instantiate" (func 32))
  (data (;0;) (global.get 0) "\ff\00\00\00\ff\00\00\00\ff\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00 \03\00\00\10\00\00\00\00\00\00\00H\02\00\00 \03\00\00\10\00\00\00\90\01\00\00\00\00\00\00\90\01\00\00X\02\00\00\00\00\00\00\0c\01\00\00\10\00\00\00@\00\00\00\00\00\00\00\10\03\00\00\0c\01\00\00\10\00\00\00@\00\00\00\00\00\00\00\88\01\00\00\10\00\00\00\10\00\00\00\10\00\00\00\00\00\80?\00\00\80?\00\00\16C"))
