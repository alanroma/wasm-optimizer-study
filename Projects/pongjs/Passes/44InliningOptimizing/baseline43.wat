(module
  (type (;0;) (func (result i32)))
  (type (;1;) (func))
  (type (;2;) (func (param i32)))
  (type (;3;) (func (param i32 i32 i32)))
  (type (;4;) (func (param i32 i32 i32 i32)))
  (type (;5;) (func (param i32 i32) (result i32)))
  (type (;6;) (func (param i32 i32)))
  (type (;7;) (func (param i32 i32 f32)))
  (type (;8;) (func (param i32 f32)))
  (type (;9;) (func (param f32 i32 i32)))
  (type (;10;) (func (param i32 i32 i32 i32) (result i32)))
  (import "env" "setFillStyle" (func (;0;) (type 3)))
  (import "env" "setStrokeStyle" (func (;1;) (type 3)))
  (import "env" "fillRect" (func (;2;) (type 4)))
  (import "env" "setTextSize" (func (;3;) (type 2)))
  (import "env" "fillText" (func (;4;) (type 3)))
  (import "env" "setLineDash" (func (;5;) (type 6)))
  (import "env" "setLineWidth" (func (;6;) (type 2)))
  (import "env" "drawLine" (func (;7;) (type 4)))
  (import "env" "stackSave" (func (;8;) (type 0)))
  (import "env" "stackRestore" (func (;9;) (type 2)))
  (import "env" "g$white" (func (;10;) (type 0)))
  (import "env" "g$player1" (func (;11;) (type 0)))
  (import "env" "g$player2" (func (;12;) (type 0)))
  (import "env" "g$black" (func (;13;) (type 0)))
  (import "env" "g$walls" (func (;14;) (type 0)))
  (import "env" "g$line" (func (;15;) (type 0)))
  (import "env" "g$ball" (func (;16;) (type 0)))
  (import "env" "__memory_base" (global (;0;) i32))
  (import "env" "__table_base" (global (;1;) i32))
  (import "env" "memory" (memory (;0;) 0))
  (import "env" "table" (table (;0;) 0 funcref))
  (func (;17;) (type 1)
    call 18)
  (func (;18;) (type 1)
    nop)
  (func (;19;) (type 2) (param i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    call 0)
  (func (;20;) (type 2) (param i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    call 1)
  (func (;21;) (type 10) (param i32 i32 i32 i32) (result i32)
    (local i32)
    call 8
    i32.const 32
    i32.sub
    local.tee 4
    local.get 0
    i32.store offset=24
    local.get 4
    local.get 1
    i32.store offset=20
    local.get 4
    local.get 2
    i32.store offset=16
    local.get 4
    local.get 3
    i32.store offset=12
    block  ;; label = @1
      local.get 4
      i32.load offset=24
      local.get 4
      i32.load offset=16
      i32.lt_s
      if  ;; label = @2
        local.get 4
        local.get 4
        i32.load offset=16
        i32.store offset=28
        br 1 (;@1;)
      end
      local.get 4
      block (result i32)  ;; label = @2
        local.get 4
        i32.load offset=24
        local.get 4
        i32.load offset=20
        i32.add
        local.get 4
        i32.load offset=12
        i32.gt_s
        if  ;; label = @3
          local.get 4
          i32.load offset=12
          local.get 4
          i32.load offset=20
          i32.sub
          br 1 (;@2;)
        end
        local.get 4
        i32.load offset=24
      end
      i32.store offset=28
    end
    local.get 4
    i32.load offset=28)
  (func (;22;) (type 5) (param i32 i32) (result i32)
    block (result i32)  ;; label = @1
      i32.const 0
      local.get 0
      f32.load
      local.get 1
      i32.load
      f32.convert_i32_s
      f32.gt
      i32.eqz
      br_if 0 (;@1;)
      drop
      i32.const 0
      local.get 0
      f32.load
      local.get 1
      i32.load
      local.get 1
      i32.load offset=8
      i32.add
      f32.convert_i32_s
      f32.lt
      i32.eqz
      br_if 0 (;@1;)
      drop
      i32.const 0
      local.get 0
      f32.load offset=4
      local.get 1
      i32.load offset=4
      f32.convert_i32_s
      f32.gt
      i32.eqz
      br_if 0 (;@1;)
      drop
      local.get 0
      f32.load offset=4
      local.get 1
      i32.load offset=4
      local.get 1
      i32.load offset=12
      i32.add
      f32.convert_i32_s
      f32.lt
    end
    i32.const 1
    i32.and)
  (func (;23;) (type 5) (param i32 i32) (result i32)
    (local i32)
    call 8
    i32.const 80
    i32.sub
    local.tee 2
    call 9
    local.get 2
    local.get 0
    i32.load
    f32.convert_i32_s
    f32.store offset=32
    local.get 2
    local.get 0
    i32.load offset=4
    f32.convert_i32_s
    f32.store offset=36
    local.get 2
    local.get 0
    i32.load
    local.get 0
    i32.load offset=8
    i32.add
    f32.convert_i32_s
    f32.store offset=40
    local.get 2
    local.get 0
    i32.load offset=4
    f32.convert_i32_s
    f32.store offset=44
    local.get 2
    local.get 0
    i32.load
    local.get 0
    i32.load offset=8
    i32.add
    f32.convert_i32_s
    f32.store offset=48
    local.get 2
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=12
    i32.add
    f32.convert_i32_s
    f32.store offset=52
    local.get 2
    local.get 0
    i32.load
    f32.convert_i32_s
    f32.store offset=56
    local.get 2
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=12
    i32.add
    f32.convert_i32_s
    f32.store offset=60
    local.get 2
    i32.const 0
    i32.store offset=28
    block  ;; label = @1
      loop  ;; label = @2
        local.get 2
        i32.load offset=28
        i32.const 4
        i32.lt_s
        if  ;; label = @3
          local.get 2
          local.get 2
          i32.const 32
          i32.add
          local.get 2
          i32.load offset=28
          i32.const 3
          i32.shl
          i32.add
          i64.load align=4
          i64.store offset=16
          i32.const 8
          local.tee 0
          local.get 2
          i32.add
          local.get 0
          local.get 1
          i32.add
          i64.load align=4
          i64.store
          local.get 2
          local.get 1
          i64.load align=4
          i64.store
          local.get 2
          i32.const 16
          i32.add
          local.get 2
          call 22
          i32.const 1
          i32.and
          if  ;; label = @4
            local.get 2
            i32.const 1
            i32.store8 offset=79
            br 3 (;@1;)
          else
            local.get 2
            local.get 2
            i32.load offset=28
            i32.const 1
            i32.add
            i32.store offset=28
            br 2 (;@2;)
          end
          unreachable
        end
      end
      local.get 2
      i32.const 0
      i32.store8 offset=79
    end
    local.get 2
    i32.load8_u offset=79
    i32.const 1
    i32.and
    local.set 0
    local.get 2
    i32.const 80
    i32.add
    call 9
    local.get 0)
  (func (;24;) (type 7) (param i32 i32 f32)
    (local i32 i32 i32)
    call 8
    i32.const 32
    i32.sub
    local.tee 3
    call 9
    i32.const 16
    local.set 4
    i32.const 584
    local.set 5
    local.get 3
    local.get 0
    i32.store offset=28
    local.get 3
    local.get 1
    i32.store offset=24
    local.get 3
    local.get 2
    f32.store offset=20
    local.get 3
    local.get 3
    i32.load offset=28
    i32.load offset=4
    i32.store offset=16
    local.get 3
    local.get 3
    f32.load offset=20
    f32.const 0x1.f4p+9 (;=1000;)
    f32.div
    f64.promote_f32
    f64.const 0x1.9p+8 (;=400;)
    f64.mul
    f32.demote_f64
    f32.store offset=12
    local.get 3
    local.get 3
    i32.load offset=16
    block (result i32)  ;; label = @1
      local.get 3
      f32.load offset=12
      local.tee 2
      f32.abs
      f32.const 0x1p+31 (;=2.14748e+09;)
      f32.lt
      if  ;; label = @2
        local.get 2
        i32.trunc_f32_s
        br 1 (;@1;)
      end
      i32.const -2147483648
    end
    local.get 3
    i32.load offset=24
    i32.mul
    i32.add
    i32.store offset=8
    local.get 3
    i32.load offset=8
    local.get 3
    i32.load offset=28
    i32.load offset=12
    local.get 4
    local.get 5
    call 21
    local.set 0
    local.get 3
    i32.load offset=28
    local.get 0
    i32.store offset=4
    local.get 3
    i32.const 32
    i32.add
    call 9)
  (func (;25;) (type 8) (param i32 f32)
    (local i32)
    call 8
    i32.const 32
    i32.sub
    local.tee 2
    local.get 0
    i32.store offset=28
    local.get 2
    local.get 1
    f32.store offset=24
    local.get 2
    local.get 2
    i32.load offset=28
    i32.load
    i32.store offset=20
    local.get 2
    local.get 2
    i32.load offset=28
    i32.load offset=4
    i32.store offset=16
    local.get 2
    local.get 2
    f32.load offset=24
    f32.const 0x1.f4p+9 (;=1000;)
    f32.div
    local.get 2
    i32.load offset=28
    f32.load offset=24
    f32.mul
    f32.store offset=12
    local.get 2
    block (result i32)  ;; label = @1
      local.get 2
      i32.load offset=20
      f32.convert_i32_s
      block (result i32)  ;; label = @2
        local.get 2
        f32.load offset=12
        local.tee 1
        f32.abs
        f32.const 0x1p+31 (;=2.14748e+09;)
        f32.lt
        if  ;; label = @3
          local.get 1
          i32.trunc_f32_s
          br 1 (;@2;)
        end
        i32.const -2147483648
      end
      f32.convert_i32_s
      local.get 2
      i32.load offset=28
      f32.load offset=16
      f32.mul
      f32.add
      local.tee 1
      f32.abs
      f32.const 0x1p+31 (;=2.14748e+09;)
      f32.lt
      if  ;; label = @2
        local.get 1
        i32.trunc_f32_s
        br 1 (;@1;)
      end
      i32.const -2147483648
    end
    i32.store offset=8
    local.get 2
    block (result i32)  ;; label = @1
      local.get 2
      i32.load offset=16
      f32.convert_i32_s
      block (result i32)  ;; label = @2
        local.get 2
        f32.load offset=12
        local.tee 1
        f32.abs
        f32.const 0x1p+31 (;=2.14748e+09;)
        f32.lt
        if  ;; label = @3
          local.get 1
          i32.trunc_f32_s
          br 1 (;@2;)
        end
        i32.const -2147483648
      end
      f32.convert_i32_s
      local.get 2
      i32.load offset=28
      f32.load offset=20
      f32.mul
      f32.add
      local.tee 1
      f32.abs
      f32.const 0x1p+31 (;=2.14748e+09;)
      f32.lt
      if  ;; label = @2
        local.get 1
        i32.trunc_f32_s
        br 1 (;@1;)
      end
      i32.const -2147483648
    end
    i32.store offset=4
    local.get 2
    i32.load offset=28
    local.get 2
    i32.load offset=8
    i32.store
    local.get 2
    i32.load offset=28
    local.get 2
    i32.load offset=4
    i32.store offset=4)
  (func (;26;) (type 2) (param i32)
    (local i32 i32 i32)
    call 8
    i32.const 16
    i32.sub
    local.tee 1
    call 9
    i32.const 8
    local.tee 2
    local.get 1
    i32.add
    local.get 2
    global.get 2
    local.tee 3
    i32.add
    i32.load
    i32.store
    local.get 1
    local.get 3
    i64.load align=4
    i64.store
    local.get 1
    call 19
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    local.get 0
    i32.load offset=12
    call 2
    local.get 1
    i32.const 16
    i32.add
    call 9)
  (func (;27;) (type 1)
    i32.const 64
    call 3
    global.get 3
    i32.load offset=16
    i32.const 304
    i32.const 80
    call 4
    global.get 4
    i32.load offset=16
    i32.const 464
    i32.const 80
    call 4)
  (func (;28;) (type 3) (param i32 i32 i32)
    (local i32)
    call 8
    i32.const 16
    i32.sub
    local.tee 3
    local.get 0
    i32.store offset=12
    local.get 3
    local.get 1
    i32.store offset=8
    local.get 3
    local.get 2
    i32.store offset=4
    local.get 3
    i32.load offset=12
    local.tee 0
    local.get 0
    i32.load offset=16
    i32.const 1
    i32.add
    i32.store offset=16
    local.get 3
    i32.load offset=8
    i32.const 392
    i32.store
    local.get 3
    i32.load offset=8
    i32.const 16
    i32.store offset=4
    local.get 3
    i32.load offset=8
    local.get 3
    i32.load offset=4
    f32.convert_i32_s
    f32.store offset=16
    local.get 3
    i32.load offset=8
    f32.const 0x1p+0 (;=1;)
    f32.store offset=20
    local.get 3
    i32.load offset=8
    f32.const 0x1.2cp+7 (;=150;)
    f32.store offset=24)
  (func (;29;) (type 1)
    (local i32 i32 i32)
    call 8
    i32.const 80
    i32.sub
    local.tee 0
    call 9
    i32.const 8
    local.tee 1
    local.get 0
    i32.const 40
    i32.add
    i32.add
    local.get 1
    global.get 5
    local.tee 2
    i32.add
    i32.load
    i32.store
    local.get 0
    local.get 2
    i64.load align=4
    i64.store offset=40
    local.get 0
    i32.const 40
    i32.add
    call 19
    i32.const 0
    local.tee 1
    local.get 1
    i32.const 800
    i32.const 600
    call 2
    i32.const 8
    local.tee 1
    local.get 0
    i32.const 56
    i32.add
    i32.add
    local.get 1
    global.get 2
    local.tee 2
    i32.add
    i32.load
    i32.store
    local.get 0
    local.get 2
    i64.load align=4
    i64.store offset=56
    local.get 0
    i32.const 56
    i32.add
    call 19
    local.get 0
    i32.const 2
    i32.store offset=76
    local.get 0
    i32.const 0
    i32.store offset=72
    loop  ;; label = @1
      local.get 0
      i32.load offset=72
      local.get 0
      i32.load offset=76
      i32.ge_s
      i32.eqz
      if  ;; label = @2
        i32.const 8
        local.get 0
        i32.const 8
        i32.add
        i32.add
        local.get 1
        global.get 6
        local.get 0
        i32.load offset=72
        i32.const 4
        i32.shl
        i32.add
        local.tee 2
        i32.add
        i64.load align=4
        i64.store
        local.get 0
        local.get 2
        i64.load align=4
        i64.store offset=8
        local.get 0
        i32.const 8
        i32.add
        call 26
        local.get 0
        local.get 0
        i32.load offset=72
        i32.const 1
        i32.add
        i32.store offset=72
        br 1 (;@1;)
      end
    end
    i32.const 8
    local.get 0
    i32.const 24
    i32.add
    i32.add
    local.get 1
    global.get 2
    local.tee 2
    i32.add
    i32.load
    i32.store
    local.get 0
    local.get 2
    i64.load align=4
    i64.store offset=24
    local.get 0
    i32.const 24
    i32.add
    call 20
    local.get 1
    local.get 1
    call 5
    local.get 1
    call 6
    global.get 7
    local.tee 1
    i32.load
    local.get 1
    i32.load offset=4
    local.get 1
    i32.load offset=8
    local.get 1
    i32.load offset=12
    call 7
    local.get 0
    i32.const 80
    i32.add
    call 9)
  (func (;30;) (type 3) (param i32 i32 i32)
    (local i32)
    call 8
    i32.const 128
    i32.sub
    local.tee 3
    call 9
    local.get 3
    local.get 0
    i32.store offset=124
    local.get 3
    local.get 1
    i32.store offset=120
    local.get 3
    local.get 2
    i32.store offset=116
    local.get 3
    i32.const 2
    i32.store offset=112
    local.get 3
    i32.const 0
    i32.store offset=108
    loop  ;; label = @1
      local.get 3
      i32.load offset=108
      local.get 3
      i32.load offset=112
      i32.lt_s
      if  ;; label = @2
        global.get 6
        local.get 3
        i32.load offset=108
        i32.const 4
        i32.shl
        i32.add
        local.set 1
        i32.const 8
        local.tee 0
        local.get 3
        i32.const 24
        i32.add
        i32.add
        local.get 0
        local.get 3
        i32.load offset=124
        local.tee 2
        i32.add
        i64.load align=4
        i64.store
        local.get 3
        local.get 2
        i64.load align=4
        i64.store offset=24
        local.get 3
        i32.const 8
        i32.add
        local.get 0
        i32.add
        local.get 0
        local.get 1
        i32.add
        i64.load align=4
        i64.store
        local.get 3
        local.get 1
        i64.load align=4
        i64.store offset=8
        local.get 3
        i32.const 24
        i32.add
        local.get 3
        i32.const 8
        i32.add
        call 23
        i32.const 1
        i32.and
        if  ;; label = @3
          local.get 3
          i32.load offset=124
          local.get 3
          i32.load offset=124
          f32.load offset=20
          f32.const -0x1p+0 (;=-1;)
          f32.mul
          f32.store offset=20
        end
        local.get 3
        local.get 3
        i32.load offset=108
        i32.const 1
        i32.add
        i32.store offset=108
        br 1 (;@1;)
      end
    end
    i32.const 8
    local.tee 0
    local.get 3
    i32.const 56
    i32.add
    i32.add
    local.get 0
    local.get 3
    i32.load offset=124
    local.tee 1
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 1
    i64.load align=4
    i64.store offset=56
    local.get 3
    i32.const 40
    i32.add
    local.get 0
    i32.add
    local.get 0
    global.get 3
    local.tee 0
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 0
    i64.load align=4
    i64.store offset=40
    local.get 3
    local.get 3
    i32.const 56
    i32.add
    local.get 3
    i32.const 40
    i32.add
    call 23
    i32.const 1
    i32.and
    i32.store8 offset=107
    i32.const 8
    local.tee 0
    local.get 3
    i32.const 88
    i32.add
    i32.add
    local.get 0
    local.get 3
    i32.load offset=124
    local.tee 1
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 1
    i64.load align=4
    i64.store offset=88
    local.get 3
    i32.const 72
    i32.add
    local.get 0
    i32.add
    local.get 0
    global.get 4
    local.tee 0
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 0
    i64.load align=4
    i64.store offset=72
    local.get 3
    local.get 3
    i32.const 88
    i32.add
    local.get 3
    i32.const 72
    i32.add
    call 23
    i32.const 1
    i32.and
    i32.store8 offset=106
    block  ;; label = @1
      local.get 3
      i32.load8_u offset=107
      i32.const 1
      i32.and
      i32.eqz
      if  ;; label = @2
        local.get 3
        i32.load8_u offset=106
        i32.const 1
        i32.and
        i32.eqz
        br_if 1 (;@1;)
      end
      local.get 3
      i32.load offset=124
      local.tee 0
      local.get 0
      f32.load offset=16
      f32.neg
      f32.store offset=16
      local.get 3
      i32.load offset=124
      local.get 3
      i32.load offset=124
      f32.load offset=24
      f64.promote_f32
      f64.const 0x1.9p+4 (;=25;)
      f64.add
      f32.demote_f64
      f32.store offset=24
      block  ;; label = @2
        local.get 3
        i32.load8_u offset=107
        i32.const 1
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.load offset=120
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.load offset=124
        local.get 3
        i32.load offset=124
        f32.load offset=20
        local.get 3
        i32.load offset=120
        f32.convert_i32_s
        f32.mul
        f32.store offset=20
      end
      block  ;; label = @2
        local.get 3
        i32.load8_u offset=106
        i32.const 1
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.load offset=116
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        i32.load offset=124
        local.get 3
        i32.load offset=124
        f32.load offset=20
        local.get 3
        i32.load offset=116
        f32.convert_i32_s
        f32.mul
        f32.store offset=20
      end
    end
    block  ;; label = @1
      local.get 3
      i32.load offset=124
      i32.load
      i32.const 0
      i32.lt_s
      if  ;; label = @2
        global.get 4
        local.get 3
        i32.load offset=124
        i32.const 1
        call 28
        br 1 (;@1;)
      end
      local.get 3
      i32.load offset=124
      i32.load
      local.get 3
      i32.load offset=124
      i32.load offset=8
      i32.add
      i32.const 800
      i32.gt_s
      if  ;; label = @2
        global.get 3
        local.get 3
        i32.load offset=124
        i32.const -1
        call 28
      end
    end
    local.get 3
    i32.const 128
    i32.add
    call 9)
  (func (;31;) (type 9) (param f32 i32 i32)
    (local i32)
    call 8
    i32.const -64
    i32.add
    local.tee 3
    call 9
    local.get 3
    local.get 0
    f32.store offset=60
    local.get 3
    local.get 1
    i32.store offset=56
    local.get 3
    local.get 2
    i32.store offset=52
    call 29
    call 27
    global.get 8
    local.get 3
    i32.load offset=56
    local.get 3
    i32.load offset=52
    call 30
    global.get 3
    local.get 3
    i32.load offset=56
    local.get 3
    f32.load offset=60
    call 24
    global.get 4
    local.get 3
    i32.load offset=52
    local.get 3
    f32.load offset=60
    call 24
    global.get 8
    local.get 3
    f32.load offset=60
    call 25
    i32.const 8
    local.tee 1
    local.get 3
    i32.add
    local.get 1
    global.get 8
    local.tee 2
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 2
    i64.load align=4
    i64.store
    local.get 3
    call 26
    i32.const 8
    local.get 3
    i32.const 16
    i32.add
    i32.add
    local.get 1
    global.get 3
    local.tee 2
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 2
    i64.load align=4
    i64.store offset=16
    local.get 3
    i32.const 16
    i32.add
    call 26
    i32.const 8
    local.get 3
    i32.const 32
    i32.add
    i32.add
    local.get 1
    global.get 4
    local.tee 2
    i32.add
    i64.load align=4
    i64.store
    local.get 3
    local.get 2
    i64.load align=4
    i64.store offset=32
    local.get 3
    i32.const 32
    i32.add
    call 26
    local.get 3
    i32.const -64
    i32.sub
    call 9)
  (func (;32;) (type 1)
    call 33
    call 17)
  (func (;33;) (type 1)
    call 10
    global.set 2
    call 11
    global.set 3
    call 12
    global.set 4
    call 13
    global.set 5
    call 14
    global.set 6
    call 15
    global.set 7
    call 16
    global.set 8)
  (global (;2;) (mut i32) (i32.const 0))
  (global (;3;) (mut i32) (i32.const 0))
  (global (;4;) (mut i32) (i32.const 0))
  (global (;5;) (mut i32) (i32.const 0))
  (global (;6;) (mut i32) (i32.const 0))
  (global (;7;) (mut i32) (i32.const 0))
  (global (;8;) (mut i32) (i32.const 0))
  (global (;9;) i32 (i32.const 0))
  (global (;10;) i32 (i32.const 80))
  (global (;11;) i32 (i32.const 100))
  (global (;12;) i32 (i32.const 12))
  (global (;13;) i32 (i32.const 32))
  (global (;14;) i32 (i32.const 64))
  (global (;15;) i32 (i32.const 120))
  (global (;16;) i32 (i32.const 0))
  (export "__wasm_apply_relocs" (func 18))
  (export "setFill" (func 19))
  (export "setStroke" (func 20))
  (export "clamp" (func 21))
  (export "isInBounds" (func 22))
  (export "doesCollide" (func 23))
  (export "movePlayer" (func 24))
  (export "moveBall" (func 25))
  (export "drawObject" (func 26))
  (export "white" (global 9))
  (export "drawScores" (func 27))
  (export "player1" (global 10))
  (export "player2" (global 11))
  (export "resetGame" (func 28))
  (export "drawCourt" (func 29))
  (export "black" (global 12))
  (export "walls" (global 13))
  (export "line" (global 14))
  (export "checkCollisions" (func 30))
  (export "tick" (func 31))
  (export "ball" (global 15))
  (export "__dso_handle" (global 16))
  (export "__post_instantiate" (func 32))
  (data (;0;) (global.get 0) "\ff\00\00\00\ff\00\00\00\ff\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00 \03\00\00\10\00\00\00\00\00\00\00H\02\00\00 \03\00\00\10\00\00\00\90\01\00\00\00\00\00\00\90\01\00\00X\02\00\00\00\00\00\00\0c\01\00\00\10\00\00\00@\00\00\00\00\00\00\00\10\03\00\00\0c\01\00\00\10\00\00\00@\00\00\00\00\00\00\00\88\01\00\00\10\00\00\00\10\00\00\00\10\00\00\00\00\00\80?\00\00\80?\00\00\16C"))
