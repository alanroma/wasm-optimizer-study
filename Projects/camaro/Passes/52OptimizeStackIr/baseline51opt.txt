[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000132418 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00144536 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000141699 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00686092 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00913012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000190509 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0421486 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0564052 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00637222 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0226369 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0102951 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0259726 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00580144 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0233139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.101181 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.050258 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0162881 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0227153 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.027345 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0653808 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0347553 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0088753 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0265541 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0087897 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0335254 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0175377 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0299768 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00490826 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0166424 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0187012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0164542 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0181152 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0315666 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.12726 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0597474 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00544675 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   8.747e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00300064 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00726423 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      4.3876e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00505234 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.96832 seconds.
[PassRunner] (final validation)
