[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000115676 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00153042 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000145178 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.0067438 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00934215 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000171931 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0419969 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.056194 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0061811 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0223176 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0101717 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0261759 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00567053 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0229448 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.101302 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0502043 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0160288 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.022474 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0269635 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0647295 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0348599 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00891388 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0266715 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00936668 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0340237 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0176891 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0300201 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.0050452 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0167608 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0189301 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0167228 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0182496 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0317551 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.13005 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.059862 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00558222 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   6.5135e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00304787 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.959019 seconds.
[PassRunner] (final validation)
