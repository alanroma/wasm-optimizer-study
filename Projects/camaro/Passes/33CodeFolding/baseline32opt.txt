[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        0.000136386 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.00149314 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        0.000129466 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    0.00713947 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.00891171 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     0.000169798 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.134221 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.0421701 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0572417 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                0.00634648 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.0228795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.0103607 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.345404 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.152092 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.0231298 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.141942 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0506532 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.0169575 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0226333 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.0187585 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0274371 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.0651978 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.0348996 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.0089092 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.0267225 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.00878106 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.033879 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.2686 seconds.
[PassRunner] (final validation)
