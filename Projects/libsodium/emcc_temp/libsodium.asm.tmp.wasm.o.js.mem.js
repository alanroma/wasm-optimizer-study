/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// The Module object: Our interface to the outside world. We import
// and export values on it. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to check if Module already exists (e.g. case 3 above).
// Substitution will be replaced with actual code on later stage of the build,
// this way Closure Compiler will not mangle it (e.g. case 4. above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module = typeof Module !== 'undefined' ? Module : {};



// --pre-jses are emitted after the Module integration code, so that they can
// refer to Module (if they choose; they can also define Module)
// {{PRE_JSES}}

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
var key;
for (key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

var arguments_ = [];
var thisProgram = './this.program';
var quit_ = function(status, toThrow) {
  throw toThrow;
};

// Determine the runtime environment we are in. You can customize this by
// setting the ENVIRONMENT setting at compile time (see settings.js).

var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;
ENVIRONMENT_IS_WEB = typeof window === 'object';
ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
// N.b. Electron.js environment is simultaneously a NODE-environment, but
// also a web environment.
ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof process.versions === 'object' && typeof process.versions.node === 'string';
ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;




// `/` should be present at the end if `scriptDirectory` is not empty
var scriptDirectory = '';
function locateFile(path) {
  if (Module['locateFile']) {
    return Module['locateFile'](path, scriptDirectory);
  }
  return scriptDirectory + path;
}

// Hooks that are implemented differently in different runtime environments.
var read_,
    readAsync,
    readBinary,
    setWindowTitle;

var nodeFS;
var nodePath;

if (ENVIRONMENT_IS_NODE) {
  if (ENVIRONMENT_IS_WORKER) {
    scriptDirectory = require('path').dirname(scriptDirectory) + '/';
  } else {
    scriptDirectory = __dirname + '/';
  }


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(filename, binary) {
    if (!nodeFS) nodeFS = require('fs');
    if (!nodePath) nodePath = require('path');
    filename = nodePath['normalize'](filename);
    return nodeFS['readFileSync'](filename, binary ? null : 'utf8');
  };

  readBinary = function readBinary(filename) {
    var ret = read_(filename, true);
    if (!ret.buffer) {
      ret = new Uint8Array(ret);
    }
    assert(ret.buffer);
    return ret;
  };




  if (process['argv'].length > 1) {
    thisProgram = process['argv'][1].replace(/\\/g, '/');
  }

  arguments_ = process['argv'].slice(2);

  if (typeof module !== 'undefined') {
    module['exports'] = Module;
  }


  process['on']('unhandledRejection', abort);

  quit_ = function(status) {
    process['exit'](status);
  };

  Module['inspect'] = function () { return '[Emscripten Module object]'; };



} else
if (ENVIRONMENT_IS_SHELL) {


  if (typeof read != 'undefined') {
    read_ = function shell_read(f) {
      return read(f);
    };
  }

  readBinary = function readBinary(f) {
    var data;
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    arguments_ = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    arguments_ = arguments;
  }

  if (typeof quit === 'function') {
    quit_ = function(status) {
      quit(status);
    };
  }

  if (typeof print !== 'undefined') {
    // Prefer to use print/printErr where they exist, as they usually work better.
    if (typeof console === 'undefined') console = /** @type{!Console} */({});
    console.log = /** @type{!function(this:Console, ...*): undefined} */ (print);
    console.warn = console.error = /** @type{!function(this:Console, ...*): undefined} */ (typeof printErr !== 'undefined' ? printErr : print);
  }


} else

// Note that this includes Node.js workers when relevant (pthreads is enabled).
// Node.js workers are detected as a combination of ENVIRONMENT_IS_WORKER and
// ENVIRONMENT_IS_NODE.
if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  if (ENVIRONMENT_IS_WORKER) { // Check worker, not web, since window could be polyfilled
    scriptDirectory = self.location.href;
  } else if (document.currentScript) { // web
    scriptDirectory = document.currentScript.src;
  }
  // blob urls look like blob:http://site.com/etc/etc and we cannot infer anything from them.
  // otherwise, slice off the final part of the url to find the script directory.
  // if scriptDirectory does not contain a slash, lastIndexOf will return -1,
  // and scriptDirectory will correctly be replaced with an empty string.
  if (scriptDirectory.indexOf('blob:') !== 0) {
    scriptDirectory = scriptDirectory.substr(0, scriptDirectory.lastIndexOf('/')+1);
  } else {
    scriptDirectory = '';
  }


  // Differentiate the Web Worker from the Node Worker case, as reading must
  // be done differently.
  {


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(url) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, false);
      xhr.send(null);
      return xhr.responseText;
  };

  if (ENVIRONMENT_IS_WORKER) {
    readBinary = function readBinary(url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.responseType = 'arraybuffer';
        xhr.send(null);
        return new Uint8Array(/** @type{!ArrayBuffer} */(xhr.response));
    };
  }

  readAsync = function readAsync(url, onload, onerror) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function xhr_onload() {
      if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
        onload(xhr.response);
        return;
      }
      onerror();
    };
    xhr.onerror = onerror;
    xhr.send(null);
  };




  }

  setWindowTitle = function(title) { document.title = title };
} else
{
}


// Set up the out() and err() hooks, which are how we can print to stdout or
// stderr, respectively.
var out = Module['print'] || console.log.bind(console);
var err = Module['printErr'] || console.warn.bind(console);

// Merge back in the overrides
for (key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}
// Free the object hierarchy contained in the overrides, this lets the GC
// reclaim data used e.g. in memoryInitializerRequest, which is a large typed array.
moduleOverrides = null;

// Emit code to handle expected values on the Module object. This applies Module.x
// to the proper local x. This has two benefits: first, we only emit it if it is
// expected to arrive, and second, by using a local everywhere else that can be
// minified.
if (Module['arguments']) arguments_ = Module['arguments'];
if (Module['thisProgram']) thisProgram = Module['thisProgram'];
if (Module['quit']) quit_ = Module['quit'];

// perform assertions in shell.js after we set up out() and err(), as otherwise if an assertion fails it cannot print the message



/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// {{PREAMBLE_ADDITIONS}}

var STACK_ALIGN = 16;

function dynamicAlloc(size) {
  var ret = HEAP32[DYNAMICTOP_PTR>>2];
  var end = (ret + size + 15) & -16;
  HEAP32[DYNAMICTOP_PTR>>2] = end;
  return ret;
}

function alignMemory(size, factor) {
  if (!factor) factor = STACK_ALIGN; // stack alignment (16-byte) by default
  return Math.ceil(size / factor) * factor;
}

function getNativeTypeSize(type) {
  switch (type) {
    case 'i1': case 'i8': return 1;
    case 'i16': return 2;
    case 'i32': return 4;
    case 'i64': return 8;
    case 'float': return 4;
    case 'double': return 8;
    default: {
      if (type[type.length-1] === '*') {
        return 4; // A pointer
      } else if (type[0] === 'i') {
        var bits = Number(type.substr(1));
        assert(bits % 8 === 0, 'getNativeTypeSize invalid bits ' + bits + ', type ' + type);
        return bits / 8;
      } else {
        return 0;
      }
    }
  }
}

function warnOnce(text) {
  if (!warnOnce.shown) warnOnce.shown = {};
  if (!warnOnce.shown[text]) {
    warnOnce.shown[text] = 1;
    err(text);
  }
}





/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */


// Wraps a JS function as a wasm function with a given signature.
function convertJsFunctionToWasm(func, sig) {
  return func;
}

var freeTableIndexes = [];

// Weak map of functions in the table to their indexes, created on first use.
var functionsInTableMap;

// Add a wasm function to the table.
function addFunctionWasm(func, sig) {
  var table = wasmTable;

  // Check if the function is already in the table, to ensure each function
  // gets a unique index. First, create the map if this is the first use.
  if (!functionsInTableMap) {
    functionsInTableMap = new WeakMap();
    for (var i = 0; i < table.length; i++) {
      var item = table.get(i);
      // Ignore null values.
      if (item) {
        functionsInTableMap.set(item, i);
      }
    }
  }
  if (functionsInTableMap.has(func)) {
    return functionsInTableMap.get(func);
  }

  // It's not in the table, add it now.


  var ret;
  // Reuse a free index if there is one, otherwise grow.
  if (freeTableIndexes.length) {
    ret = freeTableIndexes.pop();
  } else {
    ret = table.length;
    // Grow the table
    try {
      table.grow(1);
    } catch (err) {
      if (!(err instanceof RangeError)) {
        throw err;
      }
      throw 'Unable to grow wasm table. Set ALLOW_TABLE_GROWTH.';
    }
  }

  // Set the new value.
  try {
    // Attempting to call this with JS function will cause of table.set() to fail
    table.set(ret, func);
  } catch (err) {
    if (!(err instanceof TypeError)) {
      throw err;
    }
    var wrapped = convertJsFunctionToWasm(func, sig);
    table.set(ret, wrapped);
  }

  functionsInTableMap.set(func, ret);

  return ret;
}

function removeFunctionWasm(index) {
  functionsInTableMap.delete(wasmTable.get(index));
  freeTableIndexes.push(index);
}

// 'sig' parameter is required for the llvm backend but only when func is not
// already a WebAssembly function.
function addFunction(func, sig) {

  return addFunctionWasm(func, sig);
}

function removeFunction(index) {
  removeFunctionWasm(index);
}



var funcWrappers = {};

function getFuncWrapper(func, sig) {
  if (!func) return; // on null pointer, return undefined
  assert(sig);
  if (!funcWrappers[sig]) {
    funcWrappers[sig] = {};
  }
  var sigCache = funcWrappers[sig];
  if (!sigCache[func]) {
    // optimize away arguments usage in common cases
    if (sig.length === 1) {
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func);
      };
    } else if (sig.length === 2) {
      sigCache[func] = function dynCall_wrapper(arg) {
        return dynCall(sig, func, [arg]);
      };
    } else {
      // general case
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func, Array.prototype.slice.call(arguments));
      };
    }
  }
  return sigCache[func];
}


/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function makeBigInt(low, high, unsigned) {
  return unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0));
}

/** @param {Array=} args */
function dynCall(sig, ptr, args) {
  if (args && args.length) {
    return Module['dynCall_' + sig].apply(null, [ptr].concat(args));
  } else {
    return Module['dynCall_' + sig].call(null, ptr);
  }
}

var tempRet0 = 0;

var setTempRet0 = function(value) {
  tempRet0 = value;
};

var getTempRet0 = function() {
  return tempRet0;
};


// The address globals begin at. Very low in memory, for code size and optimization opportunities.
// Above 0 is static memory, starting with globals.
// Then the stack.
// Then 'dynamic' memory for sbrk.
var GLOBAL_BASE = 1024;



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in:
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at:
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html


var wasmBinary;if (Module['wasmBinary']) wasmBinary = Module['wasmBinary'];
var noExitRuntime;if (Module['noExitRuntime']) noExitRuntime = Module['noExitRuntime'];


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// wasm2js.js - enough of a polyfill for the WebAssembly object so that we can load
// wasm2js code that way.

// Emit "var WebAssembly" if definitely using wasm2js. Otherwise, in MAYBE_WASM2JS
// mode, we can't use a "var" since it would prevent normal wasm from working.
/** @suppress{const} */
var
WebAssembly = {
  Memory: /** @constructor */ function(opts) {
    return {
      buffer: new ArrayBuffer(opts['initial'] * 65536),
      grow: function(amount) {
        var ret = __growWasmMemory(amount);
        return ret;
      }
    };
  },

  Table: function(opts) {
    var ret = new Array(opts['initial']);
    ret.grow = function(by) {
      if (ret.length >= 14 + 8) {
        abort('Unable to grow wasm table. Use a higher value for RESERVED_FUNCTION_POINTERS or set ALLOW_TABLE_GROWTH.')
      }
      ret.push(null);
    };
    ret.set = function(i, func) {
      ret[i] = func;
    };
    ret.get = function(i) {
      return ret[i];
    };
    return ret;
  },

  Module: function(binary) {
    // TODO: use the binary and info somehow - right now the wasm2js output is embedded in
    // the main JS
    return {};
  },

  Instance: function(module, info) {
    // TODO: use the module and info somehow - right now the wasm2js output is embedded in
    // the main JS
    // This will be replaced by the actual wasm2js code.
    var exports = Module['__wasm2jsInstantiate__'](asmLibraryArg, wasmMemory, wasmTable);
    return {
      'exports': exports
    };
  },

  instantiate: /** @suppress{checkTypes} */ function(binary, info) {
    return {
      then: function(ok) {
        ok({
          'instance': new WebAssembly.Instance(new WebAssembly.Module(binary))
        });
      }
    };
  },

  RuntimeError: Error
};

// We don't need to actually download a wasm binary, mark it as present but empty.
wasmBinary = [];



if (typeof WebAssembly !== 'object') {
  err('no native wasm support detected');
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// In MINIMAL_RUNTIME, setValue() and getValue() are only available when building with safe heap enabled, for heap safety checking.
// In traditional runtime, setValue() and getValue() are always available (although their use is highly discouraged due to perf penalties)

/** @param {number} ptr
    @param {number} value
    @param {string} type
    @param {number|boolean=} noSafe */
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}

/** @param {number} ptr
    @param {string} type
    @param {number|boolean=} noSafe */
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for getValue: ' + type);
    }
  return null;
}






// Wasm globals

var wasmMemory;

// In fastcomp asm.js, we don't need a wasm Table at all.
// In the wasm backend, we polyfill the WebAssembly object,
// so this creates a (non-native-wasm) table for us.
var wasmTable = new WebAssembly.Table({
  'initial': 14,
  'maximum': 14 + 8,
  'element': 'anyfunc'
});


//========================================
// Runtime essentials
//========================================

// whether we are quitting the application. no code should run after this.
// set in exit() and abort()
var ABORT = false;

// set by exit() and abort().  Passed to 'onExit' handler.
// NOTE: This is also used as the process return code code in shell environments
// but only when noExitRuntime is false.
var EXITSTATUS = 0;

/** @type {function(*, string=)} */
function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  assert(func, 'Cannot call unknown function ' + ident + ', make sure it is exported');
  return func;
}

// C calling interface.
/** @param {string|null=} returnType
    @param {Array=} argTypes
    @param {Arguments|Array=} args
    @param {Object=} opts */
function ccall(ident, returnType, argTypes, args, opts) {
  // For fast lookup of conversion functions
  var toC = {
    'string': function(str) {
      var ret = 0;
      if (str !== null && str !== undefined && str !== 0) { // null string
        // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
        var len = (str.length << 2) + 1;
        ret = stackAlloc(len);
        stringToUTF8(str, ret, len);
      }
      return ret;
    },
    'array': function(arr) {
      var ret = stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    }
  };

  function convertReturnValue(ret) {
    if (returnType === 'string') return UTF8ToString(ret);
    if (returnType === 'boolean') return Boolean(ret);
    return ret;
  }

  var func = getCFunc(ident);
  var cArgs = [];
  var stack = 0;
  if (args) {
    for (var i = 0; i < args.length; i++) {
      var converter = toC[argTypes[i]];
      if (converter) {
        if (stack === 0) stack = stackSave();
        cArgs[i] = converter(args[i]);
      } else {
        cArgs[i] = args[i];
      }
    }
  }
  var ret = func.apply(null, cArgs);

  ret = convertReturnValue(ret);
  if (stack !== 0) stackRestore(stack);
  return ret;
}

/** @param {string=} returnType
    @param {Array=} argTypes
    @param {Object=} opts */
function cwrap(ident, returnType, argTypes, opts) {
  argTypes = argTypes || [];
  // When the function takes numbers and returns a number, we can just return
  // the original function
  var numericArgs = argTypes.every(function(type){ return type === 'number'});
  var numericRet = returnType !== 'string';
  if (numericRet && numericArgs && !opts) {
    return getCFunc(ident);
  }
  return function() {
    return ccall(ident, returnType, argTypes, arguments, opts);
  }
}

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_DYNAMIC = 2; // Cannot be freed except through sbrk
var ALLOC_NONE = 3; // Do not allocate

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
/** @type {function((TypedArray|Array<number>|number), string, number, number=)} */
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc,
    stackAlloc,
    dynamicAlloc][allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var stop;
    ptr = ret;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(/** @type {!Uint8Array} */ (slab), ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}

// Allocate memory during any stage of startup - static memory early on, dynamic memory later, malloc when ready
function getMemory(size) {
  if (!runtimeInitialized) return dynamicAlloc(size);
  return _malloc(size);
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings.js: Strings related runtime functions that are part of both MINIMAL_RUNTIME and regular runtime.

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

var UTF8Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf8') : undefined;

/**
 * @param {number} idx
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ArrayToString(heap, idx, maxBytesToRead) {
  var endIdx = idx + maxBytesToRead;
  var endPtr = idx;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  // (As a tiny code save trick, compare endPtr against endIdx using a negation, so that undefined means Infinity)
  while (heap[endPtr] && !(endPtr >= endIdx)) ++endPtr;

  if (endPtr - idx > 16 && heap.subarray && UTF8Decoder) {
    return UTF8Decoder.decode(heap.subarray(idx, endPtr));
  } else {
    var str = '';
    // If building with TextDecoder, we have already computed the string length above, so test loop end condition against that
    while (idx < endPtr) {
      // For UTF8 byte structure, see:
      // http://en.wikipedia.org/wiki/UTF-8#Description
      // https://www.ietf.org/rfc/rfc2279.txt
      // https://tools.ietf.org/html/rfc3629
      var u0 = heap[idx++];
      if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
      var u1 = heap[idx++] & 63;
      if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
      var u2 = heap[idx++] & 63;
      if ((u0 & 0xF0) == 0xE0) {
        u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
      } else {
        u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | (heap[idx++] & 63);
      }

      if (u0 < 0x10000) {
        str += String.fromCharCode(u0);
      } else {
        var ch = u0 - 0x10000;
        str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
      }
    }
  }
  return str;
}

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns a
// copy of that string as a Javascript String object.
// maxBytesToRead: an optional length that specifies the maximum number of bytes to read. You can omit
//                 this parameter to scan the string until the first \0 byte. If maxBytesToRead is
//                 passed, and the string at [ptr, ptr+maxBytesToReadr[ contains a null byte in the
//                 middle, then the string will cut short at that byte index (i.e. maxBytesToRead will
//                 not produce a string of exact length [ptr, ptr+maxBytesToRead[)
//                 N.B. mixing frequent uses of UTF8ToString() with and without maxBytesToRead may
//                 throw JS JIT optimizations off, so it is worth to consider consistently using one
//                 style or the other.
/**
 * @param {number} ptr
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ToString(ptr, maxBytesToRead) {
  return ptr ? UTF8ArrayToString(HEAPU8, ptr, maxBytesToRead) : '';
}

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   heap: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array.
//                    This count should include the null terminator,
//                    i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, heap, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) {
      var u1 = str.charCodeAt(++i);
      u = 0x10000 + ((u & 0x3FF) << 10) | (u1 & 0x3FF);
    }
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      heap[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      heap[outIdx++] = 0xC0 | (u >> 6);
      heap[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      heap[outIdx++] = 0xE0 | (u >> 12);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 3 >= endIdx) break;
      heap[outIdx++] = 0xF0 | (u >> 18);
      heap[outIdx++] = 0x80 | ((u >> 12) & 63);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  heap[outIdx] = 0;
  return outIdx - startIdx;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  return stringToUTF8Array(str, HEAPU8,outPtr, maxBytesToWrite);
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.
function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) ++len;
    else if (u <= 0x7FF) len += 2;
    else if (u <= 0xFFFF) len += 3;
    else len += 4;
  }
  return len;
}



/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings_extra.js: Strings related runtime functions that are available only in regular runtime.

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAPU8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

var UTF16Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-16le') : undefined;

function UTF16ToString(ptr, maxBytesToRead) {
  var endPtr = ptr;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  var idx = endPtr >> 1;
  var maxIdx = idx + maxBytesToRead / 2;
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(idx >= maxIdx) && HEAPU16[idx]) ++idx;
  endPtr = idx << 1;

  if (endPtr - ptr > 32 && UTF16Decoder) {
    return UTF16Decoder.decode(HEAPU8.subarray(ptr, endPtr));
  } else {
    var i = 0;

    var str = '';
    while (1) {
      var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
      if (codeUnit == 0 || i == maxBytesToRead / 2) return str;
      ++i;
      // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
      str += String.fromCharCode(codeUnit);
    }
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}

function UTF32ToString(ptr, maxBytesToRead) {
  var i = 0;

  var str = '';
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(i >= maxBytesToRead / 4)) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0) break;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
  return str;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}

// Allocate heap space for a JS string, and write it there.
// It is the responsibility of the caller to free() that memory.
function allocateUTF8(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = _malloc(size);
  if (ret) stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Allocate stack space for a JS string, and write it there.
function allocateUTF8OnStack(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = stackAlloc(size);
  stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Deprecated: This function should not be called because it is unsafe and does not provide
// a maximum length limit of how many bytes it is allowed to write. Prefer calling the
// function stringToUTF8Array() instead, which takes in a maximum length that can be used
// to be secure from out of bounds writes.
/** @deprecated
    @param {boolean=} dontAddNull */
function writeStringToMemory(string, buffer, dontAddNull) {
  warnOnce('writeStringToMemory is deprecated and should not be called! Use stringToUTF8() instead!');

  var /** @type {number} */ lastChar, /** @type {number} */ end;
  if (dontAddNull) {
    // stringToUTF8Array always appends null. If we don't want to do that, remember the
    // character that existed at the location where the null will be placed, and restore
    // that after the write (below).
    end = buffer + lengthBytesUTF8(string);
    lastChar = HEAP8[end];
  }
  stringToUTF8(string, buffer, Infinity);
  if (dontAddNull) HEAP8[end] = lastChar; // Restore the value under the null character.
}

function writeArrayToMemory(array, buffer) {
  HEAP8.set(array, buffer);
}

/** @param {boolean=} dontAddNull */
function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}



// Memory management

var PAGE_SIZE = 16384;
var WASM_PAGE_SIZE = 65536;
var ASMJS_PAGE_SIZE = 16777216;

function alignUp(x, multiple) {
  if (x % multiple > 0) {
    x += multiple - (x % multiple);
  }
  return x;
}

var HEAP,
/** @type {ArrayBuffer} */
  buffer,
/** @type {Int8Array} */
  HEAP8,
/** @type {Uint8Array} */
  HEAPU8,
/** @type {Int16Array} */
  HEAP16,
/** @type {Uint16Array} */
  HEAPU16,
/** @type {Int32Array} */
  HEAP32,
/** @type {Uint32Array} */
  HEAPU32,
/** @type {Float32Array} */
  HEAPF32,
/** @type {Float64Array} */
  HEAPF64;

function updateGlobalBufferAndViews(buf) {
  buffer = buf;
  Module['HEAP8'] = HEAP8 = new Int8Array(buf);
  Module['HEAP16'] = HEAP16 = new Int16Array(buf);
  Module['HEAP32'] = HEAP32 = new Int32Array(buf);
  Module['HEAPU8'] = HEAPU8 = new Uint8Array(buf);
  Module['HEAPU16'] = HEAPU16 = new Uint16Array(buf);
  Module['HEAPU32'] = HEAPU32 = new Uint32Array(buf);
  Module['HEAPF32'] = HEAPF32 = new Float32Array(buf);
  Module['HEAPF64'] = HEAPF64 = new Float64Array(buf);
}

var STATIC_BASE = 1024,
    STACK_BASE = 5279472,
    STACKTOP = STACK_BASE,
    STACK_MAX = 36592,
    DYNAMIC_BASE = 5279472,
    DYNAMICTOP_PTR = 36432;



var TOTAL_STACK = 5242880;

var INITIAL_INITIAL_MEMORY = Module['INITIAL_MEMORY'] || 16777216;




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




// In non-standalone/normal mode, we create the memory here.

/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// Create the main memory. (Note: this isn't used in STANDALONE_WASM mode since the wasm
// memory is created in the wasm, not in JS.)

  if (Module['wasmMemory']) {
    wasmMemory = Module['wasmMemory'];
  } else
  {
    wasmMemory = new WebAssembly.Memory({
      'initial': INITIAL_INITIAL_MEMORY / WASM_PAGE_SIZE
      ,
      'maximum': 2147483648 / WASM_PAGE_SIZE
    });
  }


if (wasmMemory) {
  buffer = wasmMemory.buffer;
}

// If the user provides an incorrect length, just use that length instead rather than providing the user to
// specifically provide the memory length with Module['INITIAL_MEMORY'].
INITIAL_INITIAL_MEMORY = buffer.byteLength;
updateGlobalBufferAndViews(buffer);

HEAP32[DYNAMICTOP_PTR>>2] = DYNAMIC_BASE;




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback(Module); // Pass the module as the first argument.
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Module['dynCall_v'](func);
      } else {
        Module['dynCall_vi'](func, callback.arg);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the main() is called

var runtimeInitialized = false;
var runtimeExited = false;


function preRun() {

  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPRERUN__);
}

function initRuntime() {
  runtimeInitialized = true;
  
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  runtimeExited = true;
}

function postRun() {

  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}

function addOnExit(cb) {
}

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}

/** @param {number|boolean=} ignore */
function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
/** @param {number|boolean=} ignore */
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/imul

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/fround

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc


var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_round = Math.round;
var Math_min = Math.min;
var Math_max = Math.max;
var Math_clz32 = Math.clz32;
var Math_trunc = Math.trunc;



// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// Module.preRun (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function getUniqueRunDependency(id) {
  return id;
}

function addRunDependency(id) {
  runDependencies++;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

}

function removeRunDependency(id) {
  runDependencies--;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data

/** @param {string|number=} what */
function abort(what) {
  if (Module['onAbort']) {
    Module['onAbort'](what);
  }

  what += '';
  out(what);
  err(what);

  ABORT = true;
  EXITSTATUS = 1;

  what = 'abort(' + what + '). Build with -s ASSERTIONS=1 for more info.';

  // Throw a wasm runtime error, because a JS error might be seen as a foreign
  // exception, which means we'd run destructors on it. We need the error to
  // simply make the program stop.
  throw new WebAssembly.RuntimeError(what);
}


var memoryInitializer = "libsodium.asm.tmp.js.mem";


/**
 * @license
 * Copyright 2015 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */







/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

function hasPrefix(str, prefix) {
  return String.prototype.startsWith ?
      str.startsWith(prefix) :
      str.indexOf(prefix) === 0;
}

// Prefix of data URIs emitted by SINGLE_FILE and related options.
var dataURIPrefix = 'data:application/octet-stream;base64,';

// Indicates whether filename is a base64 data URI.
function isDataURI(filename) {
  return hasPrefix(filename, dataURIPrefix);
}

var fileURIPrefix = "file://";

// Indicates whether filename is delivered via file protocol (as opposed to http/https)
function isFileURI(filename) {
  return hasPrefix(filename, fileURIPrefix);
}




var wasmBinaryFile = 'libsodium.asm.tmp.wasm';
if (!isDataURI(wasmBinaryFile)) {
  wasmBinaryFile = locateFile(wasmBinaryFile);
}

function getBinary() {
  try {
    if (wasmBinary) {
      return new Uint8Array(wasmBinary);
    }

    if (readBinary) {
      return readBinary(wasmBinaryFile);
    } else {
      throw "both async and sync fetching of the wasm failed";
    }
  }
  catch (err) {
    abort(err);
  }
}

function getBinaryPromise() {
  // If we don't have the binary yet, and have the Fetch api, use that;
  // in some environments, like Electron's render process, Fetch api may be present, but have a different context than expected, let's only use it on the Web
  if (!wasmBinary && (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) && typeof fetch === 'function'
      // Let's not use fetch to get objects over file:// as it's most likely Cordova which doesn't support fetch for file://
      && !isFileURI(wasmBinaryFile)
      ) {
    return fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function(response) {
      if (!response['ok']) {
        throw "failed to load wasm binary file at '" + wasmBinaryFile + "'";
      }
      return response['arrayBuffer']();
    }).catch(function () {
      return getBinary();
    });
  }
  // Otherwise, getBinary should be able to get it synchronously
  return new Promise(function(resolve, reject) {
    resolve(getBinary());
  });
}



// Create the wasm instance.
// Receives the wasm imports, returns the exports.
function createWasm() {
  // prepare imports
  var info = {
    'env': asmLibraryArg,
    'wasi_snapshot_preview1': asmLibraryArg
  };
  // Load the wasm module and create an instance of using native support in the JS engine.
  // handle a generated wasm instance, receiving its exports and
  // performing other necessary setup
  /** @param {WebAssembly.Module=} module*/
  function receiveInstance(instance, module) {
    var exports = instance.exports;
    Module['asm'] = exports;
    removeRunDependency('wasm-instantiate');
  }
  // we can't run yet (except in a pthread, where we have a custom sync instantiator)
  addRunDependency('wasm-instantiate');


  function receiveInstantiatedSource(output) {
    // 'output' is a WebAssemblyInstantiatedSource object which has both the module and instance.
    // receiveInstance() will swap in the exports (to Module.asm) so they can be called
    // TODO: Due to Closure regression https://github.com/google/closure-compiler/issues/3193, the above line no longer optimizes out down to the following line.
    // When the regression is fixed, can restore the above USE_PTHREADS-enabled path.
    receiveInstance(output['instance']);
  }


  function instantiateArrayBuffer(receiver) {
    return getBinaryPromise().then(function(binary) {
      return WebAssembly.instantiate(binary, info);
    }).then(receiver, function(reason) {
      err('failed to asynchronously prepare wasm: ' + reason);
      abort(reason);
    });
  }

  // Prefer streaming instantiation if available.
  function instantiateAsync() {
    if (!wasmBinary &&
        typeof WebAssembly.instantiateStreaming === 'function' &&
        !isDataURI(wasmBinaryFile) &&
        // Don't use streaming for file:// delivered objects in a webview, fetch them synchronously.
        !isFileURI(wasmBinaryFile) &&
        typeof fetch === 'function') {
      fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function (response) {
        var result = WebAssembly.instantiateStreaming(response, info);
        return result.then(receiveInstantiatedSource, function(reason) {
            // We expect the most common failure cause to be a bad MIME type for the binary,
            // in which case falling back to ArrayBuffer instantiation should work.
            err('wasm streaming compile failed: ' + reason);
            err('falling back to ArrayBuffer instantiation');
            return instantiateArrayBuffer(receiveInstantiatedSource);
          });
      });
    } else {
      return instantiateArrayBuffer(receiveInstantiatedSource);
    }
  }
  // User shell pages can write their own Module.instantiateWasm = function(imports, successCallback) callback
  // to manually instantiate the Wasm module themselves. This allows pages to run the instantiation parallel
  // to any other async startup actions they are performing.
  if (Module['instantiateWasm']) {
    try {
      var exports = Module['instantiateWasm'](info, receiveInstance);
      return exports;
    } catch(e) {
      err('Module.instantiateWasm callback failed with error: ' + e);
      return false;
    }
  }

  instantiateAsync();
  return {}; // no exports yet; we'll fill them in later
}


// Globals used by JS i64 conversions
var tempDouble;
var tempI64;

// === Body ===

var ASM_CONSTS = {
  1024: function() {return Module.getRandomValue();},  
 1062: function() {if (Module.getRandomValue === undefined) { try { var window_ = 'object' === typeof window ? window : self; var crypto_ = typeof window_.crypto !== 'undefined' ? window_.crypto : window_.msCrypto; var randomValuesStandard = function() { var buf = new Uint32Array(1); crypto_.getRandomValues(buf); return buf[0] >>> 0; }; randomValuesStandard(); Module.getRandomValue = randomValuesStandard; } catch (e) { try { var crypto = require('crypto'); var randomValueNodeJS = function() { var buf = crypto['randomBytes'](4); return (buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3]) >>> 0; }; randomValueNodeJS(); Module.getRandomValue = randomValueNodeJS; } catch (e) { throw 'No secure random number generator found'; } } }}
};

function _emscripten_asm_const_iii(code, sigPtr, argbuf) {
  var args = readAsmConstArgs(sigPtr, argbuf);
  return ASM_CONSTS[code].apply(null, args);
}



// STATICTOP = STATIC_BASE + 35568;
/* global initializers */  __ATINIT__.push({ func: function() { ___wasm_call_ctors() } });




/* no memory initializer */
// {{PRE_LIBRARY}}


  function demangle(func) {
      return func;
    }

  function demangleAll(text) {
      var regex =
        /\b_Z[\w\d_]+/g;
      return text.replace(regex,
        function(x) {
          var y = demangle(x);
          return x === y ? x : (y + ' [' + x + ']');
        });
    }

  function jsStackTrace() {
      var err = new Error();
      if (!err.stack) {
        // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
        // so try that as a special-case.
        try {
          throw new Error();
        } catch(e) {
          err = e;
        }
        if (!err.stack) {
          return '(no stack trace available)';
        }
      }
      return err.stack.toString();
    }

  function stackTrace() {
      var js = jsStackTrace();
      if (Module['extraStackTrace']) js += '\n' + Module['extraStackTrace']();
      return demangleAll(js);
    }

  function ___assert_fail(condition, filename, line, func) {
      abort('Assertion failed: ' + UTF8ToString(condition) + ', at: ' + [filename ? UTF8ToString(filename) : 'unknown filename', line, func ? UTF8ToString(func) : 'unknown function']);
    }

  function _abort() {
      abort();
    }

  function _emscripten_get_sbrk_ptr() {
      return 36432;
    }

  function _emscripten_memcpy_big(dest, src, num) {
      HEAPU8.copyWithin(dest, src, src + num);
    }

  
  function _emscripten_get_heap_size() {
      return HEAPU8.length;
    }
  
  function emscripten_realloc_buffer(size) {
      try {
        // round size grow request up to wasm page size (fixed 64KB per spec)
        wasmMemory.grow((size - buffer.byteLength + 65535) >>> 16); // .grow() takes a delta compared to the previous size
        updateGlobalBufferAndViews(wasmMemory.buffer);
        return 1 /*success*/;
      } catch(e) {
      }
    }function _emscripten_resize_heap(requestedSize) {
      requestedSize = requestedSize >>> 0;
      var oldSize = _emscripten_get_heap_size();
      // With pthreads, races can happen (another thread might increase the size in between), so return a failure, and let the caller retry.
  
  
      var PAGE_MULTIPLE = 65536;
  
      // Memory resize rules:
      // 1. When resizing, always produce a resized heap that is at least 16MB (to avoid tiny heap sizes receiving lots of repeated resizes at startup)
      // 2. Always increase heap size to at least the requested size, rounded up to next page multiple.
      // 3a. If MEMORY_GROWTH_LINEAR_STEP == -1, excessively resize the heap geometrically: increase the heap size according to 
      //                                         MEMORY_GROWTH_GEOMETRIC_STEP factor (default +20%),
      //                                         At most overreserve by MEMORY_GROWTH_GEOMETRIC_CAP bytes (default 96MB).
      // 3b. If MEMORY_GROWTH_LINEAR_STEP != -1, excessively resize the heap linearly: increase the heap size by at least MEMORY_GROWTH_LINEAR_STEP bytes.
      // 4. Max size for the heap is capped at 2048MB-PAGE_MULTIPLE, or by MAXIMUM_MEMORY, or by ASAN limit, depending on which is smallest
      // 5. If we were unable to allocate as much memory, it may be due to over-eager decision to excessively reserve due to (3) above.
      //    Hence if an allocation fails, cut down on the amount of excess growth, in an attempt to succeed to perform a smaller allocation.
  
      // A limit was set for how much we can grow. We should not exceed that
      // (the wasm binary specifies it, so if we tried, we'd fail anyhow).
      var maxHeapSize = 2147483648;
      if (requestedSize > maxHeapSize) {
        return false;
      }
  
      var minHeapSize = 16777216;
  
      // Loop through potential heap size increases. If we attempt a too eager reservation that fails, cut down on the
      // attempted size and reserve a smaller bump instead. (max 3 times, chosen somewhat arbitrarily)
      for(var cutDown = 1; cutDown <= 4; cutDown *= 2) {
        var overGrownHeapSize = oldSize * (1 + 0.2 / cutDown); // ensure geometric growth
        // but limit overreserving (default to capping at +96MB overgrowth at most)
        overGrownHeapSize = Math.min(overGrownHeapSize, requestedSize + 100663296 );
  
  
        var newSize = Math.min(maxHeapSize, alignUp(Math.max(minHeapSize, requestedSize, overGrownHeapSize), PAGE_MULTIPLE));
  
        var replacement = emscripten_realloc_buffer(newSize);
        if (replacement) {
  
          return true;
        }
      }
      return false;
    }

  
  function setErrNo(value) {
      HEAP32[((___errno_location())>>2)]=value;
      return value;
    }function _sysconf(name) {
      // long sysconf(int name);
      // http://pubs.opengroup.org/onlinepubs/009695399/functions/sysconf.html
      switch(name) {
        case 30: return 16384;
        case 85:
          var maxHeapSize = 2147483648;
          return maxHeapSize / 16384;
        case 132:
        case 133:
        case 12:
        case 137:
        case 138:
        case 15:
        case 235:
        case 16:
        case 17:
        case 18:
        case 19:
        case 20:
        case 149:
        case 13:
        case 10:
        case 236:
        case 153:
        case 9:
        case 21:
        case 22:
        case 159:
        case 154:
        case 14:
        case 77:
        case 78:
        case 139:
        case 80:
        case 81:
        case 82:
        case 68:
        case 67:
        case 164:
        case 11:
        case 29:
        case 47:
        case 48:
        case 95:
        case 52:
        case 51:
        case 46:
        case 79:
          return 200809;
        case 27:
        case 246:
        case 127:
        case 128:
        case 23:
        case 24:
        case 160:
        case 161:
        case 181:
        case 182:
        case 242:
        case 183:
        case 184:
        case 243:
        case 244:
        case 245:
        case 165:
        case 178:
        case 179:
        case 49:
        case 50:
        case 168:
        case 169:
        case 175:
        case 170:
        case 171:
        case 172:
        case 97:
        case 76:
        case 32:
        case 173:
        case 35:
          return -1;
        case 176:
        case 177:
        case 7:
        case 155:
        case 8:
        case 157:
        case 125:
        case 126:
        case 92:
        case 93:
        case 129:
        case 130:
        case 131:
        case 94:
        case 91:
          return 1;
        case 74:
        case 60:
        case 69:
        case 70:
        case 4:
          return 1024;
        case 31:
        case 42:
        case 72:
          return 32;
        case 87:
        case 26:
        case 33:
          return 2147483647;
        case 34:
        case 1:
          return 47839;
        case 38:
        case 36:
          return 99;
        case 43:
        case 37:
          return 2048;
        case 0: return 2097152;
        case 3: return 65536;
        case 28: return 32768;
        case 44: return 32767;
        case 75: return 16384;
        case 39: return 1000;
        case 89: return 700;
        case 71: return 256;
        case 40: return 255;
        case 2: return 100;
        case 180: return 64;
        case 25: return 20;
        case 5: return 16;
        case 6: return 6;
        case 73: return 4;
        case 84: {
          if (typeof navigator === 'object') return navigator['hardwareConcurrency'] || 1;
          return 1;
        }
      }
      setErrNo(28);
      return -1;
    }

  
  var __readAsmConstArgsArray=[];function readAsmConstArgs(sigPtr, buf) {
      __readAsmConstArgsArray.length = 0;
      var ch;
      buf >>= 2; // Align buf up front to index Int32Array (HEAP32)
      while (ch = HEAPU8[sigPtr++]) {
        __readAsmConstArgsArray.push(ch < 105 ? HEAPF64[++buf >> 1] : HEAP32[buf]);
        ++buf;
      }
      return __readAsmConstArgsArray;
    }
var ASSERTIONS = false;

/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

/** @type {function(string, boolean=, number=)} */
function intArrayFromString(stringy, dontAddNull, length) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      if (ASSERTIONS) {
        assert(false, 'Character code ' + chr + ' (' + String.fromCharCode(chr) + ')  at offset ' + i + ' not in 0x00-0xFF.');
      }
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}


var asmGlobalArg = {};
var asmLibraryArg = { "__assert_fail": ___assert_fail, "abort": _abort, "emscripten_asm_const_iii": _emscripten_asm_const_iii, "emscripten_get_sbrk_ptr": _emscripten_get_sbrk_ptr, "emscripten_memcpy_big": _emscripten_memcpy_big, "emscripten_resize_heap": _emscripten_resize_heap, "getTempRet0": getTempRet0, "memory": wasmMemory, "setTempRet0": setTempRet0, "sysconf": _sysconf, "table": wasmTable };
var asm = createWasm();
/** @type {function(...*):?} */
var ___wasm_call_ctors = Module["___wasm_call_ctors"] = function() {
  return (___wasm_call_ctors = Module["___wasm_call_ctors"] = Module["asm"]["__wasm_call_ctors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_encrypt_detached = Module["_crypto_aead_chacha20poly1305_encrypt_detached"] = function() {
  return (_crypto_aead_chacha20poly1305_encrypt_detached = Module["_crypto_aead_chacha20poly1305_encrypt_detached"] = Module["asm"]["crypto_aead_chacha20poly1305_encrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_encrypt = Module["_crypto_aead_chacha20poly1305_encrypt"] = function() {
  return (_crypto_aead_chacha20poly1305_encrypt = Module["_crypto_aead_chacha20poly1305_encrypt"] = Module["asm"]["crypto_aead_chacha20poly1305_encrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_encrypt_detached = Module["_crypto_aead_chacha20poly1305_ietf_encrypt_detached"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_encrypt_detached = Module["_crypto_aead_chacha20poly1305_ietf_encrypt_detached"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_encrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_encrypt = Module["_crypto_aead_chacha20poly1305_ietf_encrypt"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_encrypt = Module["_crypto_aead_chacha20poly1305_ietf_encrypt"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_encrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_decrypt_detached = Module["_crypto_aead_chacha20poly1305_decrypt_detached"] = function() {
  return (_crypto_aead_chacha20poly1305_decrypt_detached = Module["_crypto_aead_chacha20poly1305_decrypt_detached"] = Module["asm"]["crypto_aead_chacha20poly1305_decrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_decrypt = Module["_crypto_aead_chacha20poly1305_decrypt"] = function() {
  return (_crypto_aead_chacha20poly1305_decrypt = Module["_crypto_aead_chacha20poly1305_decrypt"] = Module["asm"]["crypto_aead_chacha20poly1305_decrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_decrypt_detached = Module["_crypto_aead_chacha20poly1305_ietf_decrypt_detached"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_decrypt_detached = Module["_crypto_aead_chacha20poly1305_ietf_decrypt_detached"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_decrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_decrypt = Module["_crypto_aead_chacha20poly1305_ietf_decrypt"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_decrypt = Module["_crypto_aead_chacha20poly1305_ietf_decrypt"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_decrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_keybytes = Module["_crypto_aead_chacha20poly1305_ietf_keybytes"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_keybytes = Module["_crypto_aead_chacha20poly1305_ietf_keybytes"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_npubbytes = Module["_crypto_aead_chacha20poly1305_ietf_npubbytes"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_npubbytes = Module["_crypto_aead_chacha20poly1305_ietf_npubbytes"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_npubbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_nsecbytes = Module["_crypto_aead_chacha20poly1305_ietf_nsecbytes"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_nsecbytes = Module["_crypto_aead_chacha20poly1305_ietf_nsecbytes"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_nsecbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_abytes = Module["_crypto_aead_chacha20poly1305_ietf_abytes"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_abytes = Module["_crypto_aead_chacha20poly1305_ietf_abytes"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_abytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_messagebytes_max = Module["_crypto_aead_chacha20poly1305_ietf_messagebytes_max"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_messagebytes_max = Module["_crypto_aead_chacha20poly1305_ietf_messagebytes_max"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_ietf_keygen = Module["_crypto_aead_chacha20poly1305_ietf_keygen"] = function() {
  return (_crypto_aead_chacha20poly1305_ietf_keygen = Module["_crypto_aead_chacha20poly1305_ietf_keygen"] = Module["asm"]["crypto_aead_chacha20poly1305_ietf_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_keybytes = Module["_crypto_aead_chacha20poly1305_keybytes"] = function() {
  return (_crypto_aead_chacha20poly1305_keybytes = Module["_crypto_aead_chacha20poly1305_keybytes"] = Module["asm"]["crypto_aead_chacha20poly1305_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_npubbytes = Module["_crypto_aead_chacha20poly1305_npubbytes"] = function() {
  return (_crypto_aead_chacha20poly1305_npubbytes = Module["_crypto_aead_chacha20poly1305_npubbytes"] = Module["asm"]["crypto_aead_chacha20poly1305_npubbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_nsecbytes = Module["_crypto_aead_chacha20poly1305_nsecbytes"] = function() {
  return (_crypto_aead_chacha20poly1305_nsecbytes = Module["_crypto_aead_chacha20poly1305_nsecbytes"] = Module["asm"]["crypto_aead_chacha20poly1305_nsecbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_abytes = Module["_crypto_aead_chacha20poly1305_abytes"] = function() {
  return (_crypto_aead_chacha20poly1305_abytes = Module["_crypto_aead_chacha20poly1305_abytes"] = Module["asm"]["crypto_aead_chacha20poly1305_abytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_messagebytes_max = Module["_crypto_aead_chacha20poly1305_messagebytes_max"] = function() {
  return (_crypto_aead_chacha20poly1305_messagebytes_max = Module["_crypto_aead_chacha20poly1305_messagebytes_max"] = Module["asm"]["crypto_aead_chacha20poly1305_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_chacha20poly1305_keygen = Module["_crypto_aead_chacha20poly1305_keygen"] = function() {
  return (_crypto_aead_chacha20poly1305_keygen = Module["_crypto_aead_chacha20poly1305_keygen"] = Module["asm"]["crypto_aead_chacha20poly1305_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_encrypt_detached = Module["_crypto_aead_xchacha20poly1305_ietf_encrypt_detached"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_encrypt_detached = Module["_crypto_aead_xchacha20poly1305_ietf_encrypt_detached"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_encrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_encrypt = Module["_crypto_aead_xchacha20poly1305_ietf_encrypt"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_encrypt = Module["_crypto_aead_xchacha20poly1305_ietf_encrypt"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_encrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_decrypt_detached = Module["_crypto_aead_xchacha20poly1305_ietf_decrypt_detached"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_decrypt_detached = Module["_crypto_aead_xchacha20poly1305_ietf_decrypt_detached"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_decrypt_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_decrypt = Module["_crypto_aead_xchacha20poly1305_ietf_decrypt"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_decrypt = Module["_crypto_aead_xchacha20poly1305_ietf_decrypt"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_decrypt"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_keybytes = Module["_crypto_aead_xchacha20poly1305_ietf_keybytes"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_keybytes = Module["_crypto_aead_xchacha20poly1305_ietf_keybytes"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_npubbytes = Module["_crypto_aead_xchacha20poly1305_ietf_npubbytes"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_npubbytes = Module["_crypto_aead_xchacha20poly1305_ietf_npubbytes"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_npubbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_nsecbytes = Module["_crypto_aead_xchacha20poly1305_ietf_nsecbytes"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_nsecbytes = Module["_crypto_aead_xchacha20poly1305_ietf_nsecbytes"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_nsecbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_abytes = Module["_crypto_aead_xchacha20poly1305_ietf_abytes"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_abytes = Module["_crypto_aead_xchacha20poly1305_ietf_abytes"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_abytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_messagebytes_max = Module["_crypto_aead_xchacha20poly1305_ietf_messagebytes_max"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_messagebytes_max = Module["_crypto_aead_xchacha20poly1305_ietf_messagebytes_max"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_aead_xchacha20poly1305_ietf_keygen = Module["_crypto_aead_xchacha20poly1305_ietf_keygen"] = function() {
  return (_crypto_aead_xchacha20poly1305_ietf_keygen = Module["_crypto_aead_xchacha20poly1305_ietf_keygen"] = Module["asm"]["crypto_aead_xchacha20poly1305_ietf_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_auth_bytes = Module["_crypto_auth_bytes"] = function() {
  return (_crypto_auth_bytes = Module["_crypto_auth_bytes"] = Module["asm"]["crypto_auth_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_auth_keybytes = Module["_crypto_auth_keybytes"] = function() {
  return (_crypto_auth_keybytes = Module["_crypto_auth_keybytes"] = Module["asm"]["crypto_auth_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_auth = Module["_crypto_auth"] = function() {
  return (_crypto_auth = Module["_crypto_auth"] = Module["asm"]["crypto_auth"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_auth_verify = Module["_crypto_auth_verify"] = function() {
  return (_crypto_auth_verify = Module["_crypto_auth_verify"] = Module["asm"]["crypto_auth_verify"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_auth_keygen = Module["_crypto_auth_keygen"] = function() {
  return (_crypto_auth_keygen = Module["_crypto_auth_keygen"] = Module["asm"]["crypto_auth_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_seedbytes = Module["_crypto_box_seedbytes"] = function() {
  return (_crypto_box_seedbytes = Module["_crypto_box_seedbytes"] = Module["asm"]["crypto_box_seedbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_publickeybytes = Module["_crypto_box_publickeybytes"] = function() {
  return (_crypto_box_publickeybytes = Module["_crypto_box_publickeybytes"] = Module["asm"]["crypto_box_publickeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_secretkeybytes = Module["_crypto_box_secretkeybytes"] = function() {
  return (_crypto_box_secretkeybytes = Module["_crypto_box_secretkeybytes"] = Module["asm"]["crypto_box_secretkeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_beforenmbytes = Module["_crypto_box_beforenmbytes"] = function() {
  return (_crypto_box_beforenmbytes = Module["_crypto_box_beforenmbytes"] = Module["asm"]["crypto_box_beforenmbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_noncebytes = Module["_crypto_box_noncebytes"] = function() {
  return (_crypto_box_noncebytes = Module["_crypto_box_noncebytes"] = Module["asm"]["crypto_box_noncebytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_macbytes = Module["_crypto_box_macbytes"] = function() {
  return (_crypto_box_macbytes = Module["_crypto_box_macbytes"] = Module["asm"]["crypto_box_macbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_messagebytes_max = Module["_crypto_box_messagebytes_max"] = function() {
  return (_crypto_box_messagebytes_max = Module["_crypto_box_messagebytes_max"] = Module["asm"]["crypto_box_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_seed_keypair = Module["_crypto_box_seed_keypair"] = function() {
  return (_crypto_box_seed_keypair = Module["_crypto_box_seed_keypair"] = Module["asm"]["crypto_box_seed_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_keypair = Module["_crypto_box_keypair"] = function() {
  return (_crypto_box_keypair = Module["_crypto_box_keypair"] = Module["asm"]["crypto_box_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_beforenm = Module["_crypto_box_beforenm"] = function() {
  return (_crypto_box_beforenm = Module["_crypto_box_beforenm"] = Module["asm"]["crypto_box_beforenm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_detached_afternm = Module["_crypto_box_detached_afternm"] = function() {
  return (_crypto_box_detached_afternm = Module["_crypto_box_detached_afternm"] = Module["asm"]["crypto_box_detached_afternm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_detached = Module["_crypto_box_detached"] = function() {
  return (_crypto_box_detached = Module["_crypto_box_detached"] = Module["asm"]["crypto_box_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_easy_afternm = Module["_crypto_box_easy_afternm"] = function() {
  return (_crypto_box_easy_afternm = Module["_crypto_box_easy_afternm"] = Module["asm"]["crypto_box_easy_afternm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_easy = Module["_crypto_box_easy"] = function() {
  return (_crypto_box_easy = Module["_crypto_box_easy"] = Module["asm"]["crypto_box_easy"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_open_detached_afternm = Module["_crypto_box_open_detached_afternm"] = function() {
  return (_crypto_box_open_detached_afternm = Module["_crypto_box_open_detached_afternm"] = Module["asm"]["crypto_box_open_detached_afternm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_open_detached = Module["_crypto_box_open_detached"] = function() {
  return (_crypto_box_open_detached = Module["_crypto_box_open_detached"] = Module["asm"]["crypto_box_open_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_open_easy_afternm = Module["_crypto_box_open_easy_afternm"] = function() {
  return (_crypto_box_open_easy_afternm = Module["_crypto_box_open_easy_afternm"] = Module["asm"]["crypto_box_open_easy_afternm"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_open_easy = Module["_crypto_box_open_easy"] = function() {
  return (_crypto_box_open_easy = Module["_crypto_box_open_easy"] = Module["asm"]["crypto_box_open_easy"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_seal = Module["_crypto_box_seal"] = function() {
  return (_crypto_box_seal = Module["_crypto_box_seal"] = Module["asm"]["crypto_box_seal"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_seal_open = Module["_crypto_box_seal_open"] = function() {
  return (_crypto_box_seal_open = Module["_crypto_box_seal_open"] = Module["asm"]["crypto_box_seal_open"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_box_sealbytes = Module["_crypto_box_sealbytes"] = function() {
  return (_crypto_box_sealbytes = Module["_crypto_box_sealbytes"] = Module["asm"]["crypto_box_sealbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_bytes_min = Module["_crypto_generichash_bytes_min"] = function() {
  return (_crypto_generichash_bytes_min = Module["_crypto_generichash_bytes_min"] = Module["asm"]["crypto_generichash_bytes_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_bytes_max = Module["_crypto_generichash_bytes_max"] = function() {
  return (_crypto_generichash_bytes_max = Module["_crypto_generichash_bytes_max"] = Module["asm"]["crypto_generichash_bytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_bytes = Module["_crypto_generichash_bytes"] = function() {
  return (_crypto_generichash_bytes = Module["_crypto_generichash_bytes"] = Module["asm"]["crypto_generichash_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_keybytes_min = Module["_crypto_generichash_keybytes_min"] = function() {
  return (_crypto_generichash_keybytes_min = Module["_crypto_generichash_keybytes_min"] = Module["asm"]["crypto_generichash_keybytes_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_keybytes_max = Module["_crypto_generichash_keybytes_max"] = function() {
  return (_crypto_generichash_keybytes_max = Module["_crypto_generichash_keybytes_max"] = Module["asm"]["crypto_generichash_keybytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_keybytes = Module["_crypto_generichash_keybytes"] = function() {
  return (_crypto_generichash_keybytes = Module["_crypto_generichash_keybytes"] = Module["asm"]["crypto_generichash_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_statebytes = Module["_crypto_generichash_statebytes"] = function() {
  return (_crypto_generichash_statebytes = Module["_crypto_generichash_statebytes"] = Module["asm"]["crypto_generichash_statebytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash = Module["_crypto_generichash"] = function() {
  return (_crypto_generichash = Module["_crypto_generichash"] = Module["asm"]["crypto_generichash"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_init = Module["_crypto_generichash_init"] = function() {
  return (_crypto_generichash_init = Module["_crypto_generichash_init"] = Module["asm"]["crypto_generichash_init"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_update = Module["_crypto_generichash_update"] = function() {
  return (_crypto_generichash_update = Module["_crypto_generichash_update"] = Module["asm"]["crypto_generichash_update"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_final = Module["_crypto_generichash_final"] = function() {
  return (_crypto_generichash_final = Module["_crypto_generichash_final"] = Module["asm"]["crypto_generichash_final"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_generichash_keygen = Module["_crypto_generichash_keygen"] = function() {
  return (_crypto_generichash_keygen = Module["_crypto_generichash_keygen"] = Module["asm"]["crypto_generichash_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_hash_bytes = Module["_crypto_hash_bytes"] = function() {
  return (_crypto_hash_bytes = Module["_crypto_hash_bytes"] = Module["asm"]["crypto_hash_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_hash = Module["_crypto_hash"] = function() {
  return (_crypto_hash = Module["_crypto_hash"] = Module["asm"]["crypto_hash"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_bytes_min = Module["_crypto_kdf_bytes_min"] = function() {
  return (_crypto_kdf_bytes_min = Module["_crypto_kdf_bytes_min"] = Module["asm"]["crypto_kdf_bytes_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_bytes_max = Module["_crypto_kdf_bytes_max"] = function() {
  return (_crypto_kdf_bytes_max = Module["_crypto_kdf_bytes_max"] = Module["asm"]["crypto_kdf_bytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_contextbytes = Module["_crypto_kdf_contextbytes"] = function() {
  return (_crypto_kdf_contextbytes = Module["_crypto_kdf_contextbytes"] = Module["asm"]["crypto_kdf_contextbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_keybytes = Module["_crypto_kdf_keybytes"] = function() {
  return (_crypto_kdf_keybytes = Module["_crypto_kdf_keybytes"] = Module["asm"]["crypto_kdf_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_derive_from_key = Module["_crypto_kdf_derive_from_key"] = function() {
  return (_crypto_kdf_derive_from_key = Module["_crypto_kdf_derive_from_key"] = Module["asm"]["crypto_kdf_derive_from_key"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kdf_keygen = Module["_crypto_kdf_keygen"] = function() {
  return (_crypto_kdf_keygen = Module["_crypto_kdf_keygen"] = Module["asm"]["crypto_kdf_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_seed_keypair = Module["_crypto_kx_seed_keypair"] = function() {
  return (_crypto_kx_seed_keypair = Module["_crypto_kx_seed_keypair"] = Module["asm"]["crypto_kx_seed_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_keypair = Module["_crypto_kx_keypair"] = function() {
  return (_crypto_kx_keypair = Module["_crypto_kx_keypair"] = Module["asm"]["crypto_kx_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_client_session_keys = Module["_crypto_kx_client_session_keys"] = function() {
  return (_crypto_kx_client_session_keys = Module["_crypto_kx_client_session_keys"] = Module["asm"]["crypto_kx_client_session_keys"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_server_session_keys = Module["_crypto_kx_server_session_keys"] = function() {
  return (_crypto_kx_server_session_keys = Module["_crypto_kx_server_session_keys"] = Module["asm"]["crypto_kx_server_session_keys"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_publickeybytes = Module["_crypto_kx_publickeybytes"] = function() {
  return (_crypto_kx_publickeybytes = Module["_crypto_kx_publickeybytes"] = Module["asm"]["crypto_kx_publickeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_secretkeybytes = Module["_crypto_kx_secretkeybytes"] = function() {
  return (_crypto_kx_secretkeybytes = Module["_crypto_kx_secretkeybytes"] = Module["asm"]["crypto_kx_secretkeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_seedbytes = Module["_crypto_kx_seedbytes"] = function() {
  return (_crypto_kx_seedbytes = Module["_crypto_kx_seedbytes"] = Module["asm"]["crypto_kx_seedbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_kx_sessionkeybytes = Module["_crypto_kx_sessionkeybytes"] = function() {
  return (_crypto_kx_sessionkeybytes = Module["_crypto_kx_sessionkeybytes"] = Module["asm"]["crypto_kx_sessionkeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_alg_argon2i13 = Module["_crypto_pwhash_alg_argon2i13"] = function() {
  return (_crypto_pwhash_alg_argon2i13 = Module["_crypto_pwhash_alg_argon2i13"] = Module["asm"]["crypto_pwhash_alg_argon2i13"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_alg_argon2id13 = Module["_crypto_pwhash_alg_argon2id13"] = function() {
  return (_crypto_pwhash_alg_argon2id13 = Module["_crypto_pwhash_alg_argon2id13"] = Module["asm"]["crypto_pwhash_alg_argon2id13"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_alg_default = Module["_crypto_pwhash_alg_default"] = function() {
  return (_crypto_pwhash_alg_default = Module["_crypto_pwhash_alg_default"] = Module["asm"]["crypto_pwhash_alg_default"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_bytes_min = Module["_crypto_pwhash_bytes_min"] = function() {
  return (_crypto_pwhash_bytes_min = Module["_crypto_pwhash_bytes_min"] = Module["asm"]["crypto_pwhash_bytes_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_bytes_max = Module["_crypto_pwhash_bytes_max"] = function() {
  return (_crypto_pwhash_bytes_max = Module["_crypto_pwhash_bytes_max"] = Module["asm"]["crypto_pwhash_bytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_passwd_min = Module["_crypto_pwhash_passwd_min"] = function() {
  return (_crypto_pwhash_passwd_min = Module["_crypto_pwhash_passwd_min"] = Module["asm"]["crypto_pwhash_passwd_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_passwd_max = Module["_crypto_pwhash_passwd_max"] = function() {
  return (_crypto_pwhash_passwd_max = Module["_crypto_pwhash_passwd_max"] = Module["asm"]["crypto_pwhash_passwd_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_saltbytes = Module["_crypto_pwhash_saltbytes"] = function() {
  return (_crypto_pwhash_saltbytes = Module["_crypto_pwhash_saltbytes"] = Module["asm"]["crypto_pwhash_saltbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_strbytes = Module["_crypto_pwhash_strbytes"] = function() {
  return (_crypto_pwhash_strbytes = Module["_crypto_pwhash_strbytes"] = Module["asm"]["crypto_pwhash_strbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_strprefix = Module["_crypto_pwhash_strprefix"] = function() {
  return (_crypto_pwhash_strprefix = Module["_crypto_pwhash_strprefix"] = Module["asm"]["crypto_pwhash_strprefix"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_opslimit_min = Module["_crypto_pwhash_opslimit_min"] = function() {
  return (_crypto_pwhash_opslimit_min = Module["_crypto_pwhash_opslimit_min"] = Module["asm"]["crypto_pwhash_opslimit_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_opslimit_max = Module["_crypto_pwhash_opslimit_max"] = function() {
  return (_crypto_pwhash_opslimit_max = Module["_crypto_pwhash_opslimit_max"] = Module["asm"]["crypto_pwhash_opslimit_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_memlimit_min = Module["_crypto_pwhash_memlimit_min"] = function() {
  return (_crypto_pwhash_memlimit_min = Module["_crypto_pwhash_memlimit_min"] = Module["asm"]["crypto_pwhash_memlimit_min"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_memlimit_max = Module["_crypto_pwhash_memlimit_max"] = function() {
  return (_crypto_pwhash_memlimit_max = Module["_crypto_pwhash_memlimit_max"] = Module["asm"]["crypto_pwhash_memlimit_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_opslimit_interactive = Module["_crypto_pwhash_opslimit_interactive"] = function() {
  return (_crypto_pwhash_opslimit_interactive = Module["_crypto_pwhash_opslimit_interactive"] = Module["asm"]["crypto_pwhash_opslimit_interactive"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_memlimit_interactive = Module["_crypto_pwhash_memlimit_interactive"] = function() {
  return (_crypto_pwhash_memlimit_interactive = Module["_crypto_pwhash_memlimit_interactive"] = Module["asm"]["crypto_pwhash_memlimit_interactive"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_opslimit_moderate = Module["_crypto_pwhash_opslimit_moderate"] = function() {
  return (_crypto_pwhash_opslimit_moderate = Module["_crypto_pwhash_opslimit_moderate"] = Module["asm"]["crypto_pwhash_opslimit_moderate"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_memlimit_moderate = Module["_crypto_pwhash_memlimit_moderate"] = function() {
  return (_crypto_pwhash_memlimit_moderate = Module["_crypto_pwhash_memlimit_moderate"] = Module["asm"]["crypto_pwhash_memlimit_moderate"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_opslimit_sensitive = Module["_crypto_pwhash_opslimit_sensitive"] = function() {
  return (_crypto_pwhash_opslimit_sensitive = Module["_crypto_pwhash_opslimit_sensitive"] = Module["asm"]["crypto_pwhash_opslimit_sensitive"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_memlimit_sensitive = Module["_crypto_pwhash_memlimit_sensitive"] = function() {
  return (_crypto_pwhash_memlimit_sensitive = Module["_crypto_pwhash_memlimit_sensitive"] = Module["asm"]["crypto_pwhash_memlimit_sensitive"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash = Module["_crypto_pwhash"] = function() {
  return (_crypto_pwhash = Module["_crypto_pwhash"] = Module["asm"]["crypto_pwhash"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_str = Module["_crypto_pwhash_str"] = function() {
  return (_crypto_pwhash_str = Module["_crypto_pwhash_str"] = Module["asm"]["crypto_pwhash_str"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_str_alg = Module["_crypto_pwhash_str_alg"] = function() {
  return (_crypto_pwhash_str_alg = Module["_crypto_pwhash_str_alg"] = Module["asm"]["crypto_pwhash_str_alg"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_str_verify = Module["_crypto_pwhash_str_verify"] = function() {
  return (_crypto_pwhash_str_verify = Module["_crypto_pwhash_str_verify"] = Module["asm"]["crypto_pwhash_str_verify"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_pwhash_str_needs_rehash = Module["_crypto_pwhash_str_needs_rehash"] = function() {
  return (_crypto_pwhash_str_needs_rehash = Module["_crypto_pwhash_str_needs_rehash"] = Module["asm"]["crypto_pwhash_str_needs_rehash"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_scalarmult_base = Module["_crypto_scalarmult_base"] = function() {
  return (_crypto_scalarmult_base = Module["_crypto_scalarmult_base"] = Module["asm"]["crypto_scalarmult_base"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_scalarmult = Module["_crypto_scalarmult"] = function() {
  return (_crypto_scalarmult = Module["_crypto_scalarmult"] = Module["asm"]["crypto_scalarmult"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_scalarmult_bytes = Module["_crypto_scalarmult_bytes"] = function() {
  return (_crypto_scalarmult_bytes = Module["_crypto_scalarmult_bytes"] = Module["asm"]["crypto_scalarmult_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_scalarmult_scalarbytes = Module["_crypto_scalarmult_scalarbytes"] = function() {
  return (_crypto_scalarmult_scalarbytes = Module["_crypto_scalarmult_scalarbytes"] = Module["asm"]["crypto_scalarmult_scalarbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_keybytes = Module["_crypto_secretbox_keybytes"] = function() {
  return (_crypto_secretbox_keybytes = Module["_crypto_secretbox_keybytes"] = Module["asm"]["crypto_secretbox_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_noncebytes = Module["_crypto_secretbox_noncebytes"] = function() {
  return (_crypto_secretbox_noncebytes = Module["_crypto_secretbox_noncebytes"] = Module["asm"]["crypto_secretbox_noncebytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_macbytes = Module["_crypto_secretbox_macbytes"] = function() {
  return (_crypto_secretbox_macbytes = Module["_crypto_secretbox_macbytes"] = Module["asm"]["crypto_secretbox_macbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_messagebytes_max = Module["_crypto_secretbox_messagebytes_max"] = function() {
  return (_crypto_secretbox_messagebytes_max = Module["_crypto_secretbox_messagebytes_max"] = Module["asm"]["crypto_secretbox_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_keygen = Module["_crypto_secretbox_keygen"] = function() {
  return (_crypto_secretbox_keygen = Module["_crypto_secretbox_keygen"] = Module["asm"]["crypto_secretbox_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_detached = Module["_crypto_secretbox_detached"] = function() {
  return (_crypto_secretbox_detached = Module["_crypto_secretbox_detached"] = Module["asm"]["crypto_secretbox_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_easy = Module["_crypto_secretbox_easy"] = function() {
  return (_crypto_secretbox_easy = Module["_crypto_secretbox_easy"] = Module["asm"]["crypto_secretbox_easy"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_open_detached = Module["_crypto_secretbox_open_detached"] = function() {
  return (_crypto_secretbox_open_detached = Module["_crypto_secretbox_open_detached"] = Module["asm"]["crypto_secretbox_open_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretbox_open_easy = Module["_crypto_secretbox_open_easy"] = function() {
  return (_crypto_secretbox_open_easy = Module["_crypto_secretbox_open_easy"] = Module["asm"]["crypto_secretbox_open_easy"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_keygen = Module["_crypto_secretstream_xchacha20poly1305_keygen"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_keygen = Module["_crypto_secretstream_xchacha20poly1305_keygen"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_init_push = Module["_crypto_secretstream_xchacha20poly1305_init_push"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_init_push = Module["_crypto_secretstream_xchacha20poly1305_init_push"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_init_push"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_init_pull = Module["_crypto_secretstream_xchacha20poly1305_init_pull"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_init_pull = Module["_crypto_secretstream_xchacha20poly1305_init_pull"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_init_pull"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_rekey = Module["_crypto_secretstream_xchacha20poly1305_rekey"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_rekey = Module["_crypto_secretstream_xchacha20poly1305_rekey"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_rekey"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_push = Module["_crypto_secretstream_xchacha20poly1305_push"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_push = Module["_crypto_secretstream_xchacha20poly1305_push"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_push"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_pull = Module["_crypto_secretstream_xchacha20poly1305_pull"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_pull = Module["_crypto_secretstream_xchacha20poly1305_pull"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_pull"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_statebytes = Module["_crypto_secretstream_xchacha20poly1305_statebytes"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_statebytes = Module["_crypto_secretstream_xchacha20poly1305_statebytes"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_statebytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_abytes = Module["_crypto_secretstream_xchacha20poly1305_abytes"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_abytes = Module["_crypto_secretstream_xchacha20poly1305_abytes"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_abytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_headerbytes = Module["_crypto_secretstream_xchacha20poly1305_headerbytes"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_headerbytes = Module["_crypto_secretstream_xchacha20poly1305_headerbytes"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_headerbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_keybytes = Module["_crypto_secretstream_xchacha20poly1305_keybytes"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_keybytes = Module["_crypto_secretstream_xchacha20poly1305_keybytes"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_messagebytes_max = Module["_crypto_secretstream_xchacha20poly1305_messagebytes_max"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_messagebytes_max = Module["_crypto_secretstream_xchacha20poly1305_messagebytes_max"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_tag_message = Module["_crypto_secretstream_xchacha20poly1305_tag_message"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_tag_message = Module["_crypto_secretstream_xchacha20poly1305_tag_message"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_tag_message"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_tag_push = Module["_crypto_secretstream_xchacha20poly1305_tag_push"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_tag_push = Module["_crypto_secretstream_xchacha20poly1305_tag_push"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_tag_push"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_tag_rekey = Module["_crypto_secretstream_xchacha20poly1305_tag_rekey"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_tag_rekey = Module["_crypto_secretstream_xchacha20poly1305_tag_rekey"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_tag_rekey"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_secretstream_xchacha20poly1305_tag_final = Module["_crypto_secretstream_xchacha20poly1305_tag_final"] = function() {
  return (_crypto_secretstream_xchacha20poly1305_tag_final = Module["_crypto_secretstream_xchacha20poly1305_tag_final"] = Module["asm"]["crypto_secretstream_xchacha20poly1305_tag_final"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_shorthash_bytes = Module["_crypto_shorthash_bytes"] = function() {
  return (_crypto_shorthash_bytes = Module["_crypto_shorthash_bytes"] = Module["asm"]["crypto_shorthash_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_shorthash_keybytes = Module["_crypto_shorthash_keybytes"] = function() {
  return (_crypto_shorthash_keybytes = Module["_crypto_shorthash_keybytes"] = Module["asm"]["crypto_shorthash_keybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_shorthash = Module["_crypto_shorthash"] = function() {
  return (_crypto_shorthash = Module["_crypto_shorthash"] = Module["asm"]["crypto_shorthash"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_shorthash_keygen = Module["_crypto_shorthash_keygen"] = function() {
  return (_crypto_shorthash_keygen = Module["_crypto_shorthash_keygen"] = Module["asm"]["crypto_shorthash_keygen"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_statebytes = Module["_crypto_sign_statebytes"] = function() {
  return (_crypto_sign_statebytes = Module["_crypto_sign_statebytes"] = Module["asm"]["crypto_sign_statebytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_bytes = Module["_crypto_sign_bytes"] = function() {
  return (_crypto_sign_bytes = Module["_crypto_sign_bytes"] = Module["asm"]["crypto_sign_bytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_seedbytes = Module["_crypto_sign_seedbytes"] = function() {
  return (_crypto_sign_seedbytes = Module["_crypto_sign_seedbytes"] = Module["asm"]["crypto_sign_seedbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_publickeybytes = Module["_crypto_sign_publickeybytes"] = function() {
  return (_crypto_sign_publickeybytes = Module["_crypto_sign_publickeybytes"] = Module["asm"]["crypto_sign_publickeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_secretkeybytes = Module["_crypto_sign_secretkeybytes"] = function() {
  return (_crypto_sign_secretkeybytes = Module["_crypto_sign_secretkeybytes"] = Module["asm"]["crypto_sign_secretkeybytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_messagebytes_max = Module["_crypto_sign_messagebytes_max"] = function() {
  return (_crypto_sign_messagebytes_max = Module["_crypto_sign_messagebytes_max"] = Module["asm"]["crypto_sign_messagebytes_max"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_seed_keypair = Module["_crypto_sign_seed_keypair"] = function() {
  return (_crypto_sign_seed_keypair = Module["_crypto_sign_seed_keypair"] = Module["asm"]["crypto_sign_seed_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_keypair = Module["_crypto_sign_keypair"] = function() {
  return (_crypto_sign_keypair = Module["_crypto_sign_keypair"] = Module["asm"]["crypto_sign_keypair"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign = Module["_crypto_sign"] = function() {
  return (_crypto_sign = Module["_crypto_sign"] = Module["asm"]["crypto_sign"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_open = Module["_crypto_sign_open"] = function() {
  return (_crypto_sign_open = Module["_crypto_sign_open"] = Module["asm"]["crypto_sign_open"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_detached = Module["_crypto_sign_detached"] = function() {
  return (_crypto_sign_detached = Module["_crypto_sign_detached"] = Module["asm"]["crypto_sign_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_verify_detached = Module["_crypto_sign_verify_detached"] = function() {
  return (_crypto_sign_verify_detached = Module["_crypto_sign_verify_detached"] = Module["asm"]["crypto_sign_verify_detached"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_init = Module["_crypto_sign_init"] = function() {
  return (_crypto_sign_init = Module["_crypto_sign_init"] = Module["asm"]["crypto_sign_init"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_update = Module["_crypto_sign_update"] = function() {
  return (_crypto_sign_update = Module["_crypto_sign_update"] = Module["asm"]["crypto_sign_update"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_final_create = Module["_crypto_sign_final_create"] = function() {
  return (_crypto_sign_final_create = Module["_crypto_sign_final_create"] = Module["asm"]["crypto_sign_final_create"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_final_verify = Module["_crypto_sign_final_verify"] = function() {
  return (_crypto_sign_final_verify = Module["_crypto_sign_final_verify"] = Module["asm"]["crypto_sign_final_verify"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_ed25519_pk_to_curve25519 = Module["_crypto_sign_ed25519_pk_to_curve25519"] = function() {
  return (_crypto_sign_ed25519_pk_to_curve25519 = Module["_crypto_sign_ed25519_pk_to_curve25519"] = Module["asm"]["crypto_sign_ed25519_pk_to_curve25519"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _crypto_sign_ed25519_sk_to_curve25519 = Module["_crypto_sign_ed25519_sk_to_curve25519"] = function() {
  return (_crypto_sign_ed25519_sk_to_curve25519 = Module["_crypto_sign_ed25519_sk_to_curve25519"] = Module["asm"]["crypto_sign_ed25519_sk_to_curve25519"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_random = Module["_randombytes_random"] = function() {
  return (_randombytes_random = Module["_randombytes_random"] = Module["asm"]["randombytes_random"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_stir = Module["_randombytes_stir"] = function() {
  return (_randombytes_stir = Module["_randombytes_stir"] = Module["asm"]["randombytes_stir"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_uniform = Module["_randombytes_uniform"] = function() {
  return (_randombytes_uniform = Module["_randombytes_uniform"] = Module["asm"]["randombytes_uniform"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_buf = Module["_randombytes_buf"] = function() {
  return (_randombytes_buf = Module["_randombytes_buf"] = Module["asm"]["randombytes_buf"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_buf_deterministic = Module["_randombytes_buf_deterministic"] = function() {
  return (_randombytes_buf_deterministic = Module["_randombytes_buf_deterministic"] = Module["asm"]["randombytes_buf_deterministic"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_seedbytes = Module["_randombytes_seedbytes"] = function() {
  return (_randombytes_seedbytes = Module["_randombytes_seedbytes"] = Module["asm"]["randombytes_seedbytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes_close = Module["_randombytes_close"] = function() {
  return (_randombytes_close = Module["_randombytes_close"] = Module["asm"]["randombytes_close"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _randombytes = Module["_randombytes"] = function() {
  return (_randombytes = Module["_randombytes"] = Module["asm"]["randombytes"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_bin2hex = Module["_sodium_bin2hex"] = function() {
  return (_sodium_bin2hex = Module["_sodium_bin2hex"] = Module["asm"]["sodium_bin2hex"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_hex2bin = Module["_sodium_hex2bin"] = function() {
  return (_sodium_hex2bin = Module["_sodium_hex2bin"] = Module["asm"]["sodium_hex2bin"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_base64_encoded_len = Module["_sodium_base64_encoded_len"] = function() {
  return (_sodium_base64_encoded_len = Module["_sodium_base64_encoded_len"] = Module["asm"]["sodium_base64_encoded_len"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_bin2base64 = Module["_sodium_bin2base64"] = function() {
  return (_sodium_bin2base64 = Module["_sodium_bin2base64"] = Module["asm"]["sodium_bin2base64"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_base642bin = Module["_sodium_base642bin"] = function() {
  return (_sodium_base642bin = Module["_sodium_base642bin"] = Module["asm"]["sodium_base642bin"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_init = Module["_sodium_init"] = function() {
  return (_sodium_init = Module["_sodium_init"] = Module["asm"]["sodium_init"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_pad = Module["_sodium_pad"] = function() {
  return (_sodium_pad = Module["_sodium_pad"] = Module["asm"]["sodium_pad"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_unpad = Module["_sodium_unpad"] = function() {
  return (_sodium_unpad = Module["_sodium_unpad"] = Module["asm"]["sodium_unpad"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_version_string = Module["_sodium_version_string"] = function() {
  return (_sodium_version_string = Module["_sodium_version_string"] = Module["asm"]["sodium_version_string"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_library_version_major = Module["_sodium_library_version_major"] = function() {
  return (_sodium_library_version_major = Module["_sodium_library_version_major"] = Module["asm"]["sodium_library_version_major"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_library_version_minor = Module["_sodium_library_version_minor"] = function() {
  return (_sodium_library_version_minor = Module["_sodium_library_version_minor"] = Module["asm"]["sodium_library_version_minor"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _sodium_library_minimal = Module["_sodium_library_minimal"] = function() {
  return (_sodium_library_minimal = Module["_sodium_library_minimal"] = Module["asm"]["sodium_library_minimal"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var ___errno_location = Module["___errno_location"] = function() {
  return (___errno_location = Module["___errno_location"] = Module["asm"]["__errno_location"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackSave = Module["stackSave"] = function() {
  return (stackSave = Module["stackSave"] = Module["asm"]["stackSave"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackRestore = Module["stackRestore"] = function() {
  return (stackRestore = Module["stackRestore"] = Module["asm"]["stackRestore"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackAlloc = Module["stackAlloc"] = function() {
  return (stackAlloc = Module["stackAlloc"] = Module["asm"]["stackAlloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _malloc = Module["_malloc"] = function() {
  return (_malloc = Module["_malloc"] = Module["asm"]["malloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _free = Module["_free"] = function() {
  return (_free = Module["_free"] = Module["asm"]["free"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var __growWasmMemory = Module["__growWasmMemory"] = function() {
  return (__growWasmMemory = Module["__growWasmMemory"] = Module["asm"]["__growWasmMemory"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiiji = Module["dynCall_iiiji"] = function() {
  return (dynCall_iiiji = Module["dynCall_iiiji"] = Module["asm"]["dynCall_iiiji"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iii = Module["dynCall_iii"] = function() {
  return (dynCall_iii = Module["dynCall_iii"] = Module["asm"]["dynCall_iii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiij = Module["dynCall_iiij"] = function() {
  return (dynCall_iiij = Module["dynCall_iiij"] = Module["asm"]["dynCall_iiij"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiii = Module["dynCall_iiii"] = function() {
  return (dynCall_iiii = Module["dynCall_iiii"] = Module["asm"]["dynCall_iiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iijii = Module["dynCall_iijii"] = function() {
  return (dynCall_iijii = Module["dynCall_iijii"] = Module["asm"]["dynCall_iijii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiijiji = Module["dynCall_iiijiji"] = function() {
  return (dynCall_iiijiji = Module["dynCall_iiijiji"] = Module["asm"]["dynCall_iiijiji"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiijiii = Module["dynCall_iiijiii"] = function() {
  return (dynCall_iiijiii = Module["dynCall_iiijiii"] = Module["asm"]["dynCall_iiijiii"]).apply(null, arguments);
};



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Auto-generated postamble setup entry stuff ===






Module["setValue"] = setValue;
Module["getValue"] = getValue;



Module["UTF8ToString"] = UTF8ToString;




























































































































if (memoryInitializer) {
  if (!isDataURI(memoryInitializer)) {
    memoryInitializer = locateFile(memoryInitializer);
  }
  if (ENVIRONMENT_IS_NODE || ENVIRONMENT_IS_SHELL) {
    var data = readBinary(memoryInitializer);
    HEAPU8.set(data, GLOBAL_BASE);
  } else {
    addRunDependency('memory initializer');
    var applyMemoryInitializer = function(data) {
      if (data.byteLength) data = new Uint8Array(data);
      HEAPU8.set(data, GLOBAL_BASE);
      // Delete the typed array that contains the large blob of the memory initializer request response so that
      // we won't keep unnecessary memory lying around. However, keep the XHR object itself alive so that e.g.
      // its .status field can still be accessed later.
      if (Module['memoryInitializerRequest']) delete Module['memoryInitializerRequest'].response;
      removeRunDependency('memory initializer');
    };
    var doBrowserLoad = function() {
      readAsync(memoryInitializer, applyMemoryInitializer, function() {
        var e = new Error('could not load memory initializer ' + memoryInitializer);
          throw e;
      });
    };
    if (Module['memoryInitializerRequest']) {
      // a network request has already been created, just use that
      var useRequest = function() {
        var request = Module['memoryInitializerRequest'];
        var response = request.response;
        if (request.status !== 200 && request.status !== 0) {
            // If you see this warning, the issue may be that you are using locateFile and defining it in JS. That
            // means that the HTML file doesn't know about it, and when it tries to create the mem init request early, does it to the wrong place.
            // Look in your browser's devtools network console to see what's going on.
            console.warn('a problem seems to have happened with Module.memoryInitializerRequest, status: ' + request.status + ', retrying ' + memoryInitializer);
            doBrowserLoad();
            return;
        }
        applyMemoryInitializer(response);
      };
      if (Module['memoryInitializerRequest'].response) {
        setTimeout(useRequest, 0); // it's already here; but, apply it asynchronously
      } else {
        Module['memoryInitializerRequest'].addEventListener('load', useRequest); // wait for it
      }
    } else {
      // fetch it from the network ourselves
      doBrowserLoad();
    }
  }
}


var calledRun;

/**
 * @constructor
 * @this {ExitStatus}
 */
function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
}

var calledMain = false;


dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!calledRun) run();
  if (!calledRun) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
};





/** @type {function(Array=)} */
function run(args) {
  args = args || arguments_;

  if (runDependencies > 0) {
    return;
  }


  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later

  function doRun() {
    // run may have just been called through dependencies being fulfilled just in this very frame,
    // or while the async setStatus time below was happening
    if (calledRun) return;
    calledRun = true;
    Module['calledRun'] = true;

    if (ABORT) return;

    initRuntime();

    preMain();

    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();


    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else
  {
    doRun();
  }
}
Module['run'] = run;


/** @param {boolean|number=} implicit */
function exit(status, implicit) {

  // if this is just main exit-ing implicitly, and the status is 0, then we
  // don't need to do anything here and can just leave. if the status is
  // non-zero, though, then we need to report it.
  // (we may have warned about this earlier, if a situation justifies doing so)
  if (implicit && noExitRuntime && status === 0) {
    return;
  }

  if (noExitRuntime) {
  } else {

    ABORT = true;
    EXITSTATUS = status;

    exitRuntime();

    if (Module['onExit']) Module['onExit'](status);
  }

  quit_(status, new ExitStatus(status));
}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}


  noExitRuntime = true;

run();






// {{MODULE_ADDITIONS}}


// EMSCRIPTEN_GENERATED_FUNCTIONS: []


