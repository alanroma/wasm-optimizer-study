; ModuleID = 'crypto_pwhash/argon2/blake2b-long.c'
source_filename = "crypto_pwhash/argon2/blake2b-long.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_generichash_blake2b_state = type { [384 x i8] }

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_long(i8* %pout, i32 %outlen, i8* %in, i32 %inlen) #0 {
entry:
  %pout.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i32, align 4
  %out = alloca i8*, align 4
  %blake_state = alloca %struct.crypto_generichash_blake2b_state, align 64
  %outlen_bytes = alloca [4 x i8], align 1
  %ret = alloca i32, align 4
  %toproduce = alloca i32, align 4
  %out_buffer = alloca [64 x i8], align 16
  %in_buffer = alloca [64 x i8], align 16
  store i8* %pout, i8** %pout.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %inlen, i32* %inlen.addr, align 4
  %0 = load i8*, i8** %pout.addr, align 4
  store i8* %0, i8** %out, align 4
  %1 = bitcast [4 x i8]* %outlen_bytes to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %1, i8 0, i32 4, i1 false)
  store i32 -1, i32* %ret, align 4
  %2 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ugt i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %fail

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %3 = load i32, i32* %outlen.addr, align 4
  call void @store32_le(i8* %arraydecay, i32 %3)
  %4 = load i32, i32* %outlen.addr, align 4
  %cmp1 = icmp ule i32 %4, 64
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  br label %do.body

do.body:                                          ; preds = %if.then2
  %5 = load i32, i32* %outlen.addr, align 4
  %call = call i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state* %blake_state, i8* null, i32 0, i32 %5)
  store i32 %call, i32* %ret, align 4
  %6 = load i32, i32* %ret, align 4
  %cmp3 = icmp slt i32 %6, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %do.body
  br label %fail

if.end5:                                          ; preds = %do.body
  br label %do.end

do.end:                                           ; preds = %if.end5
  br label %do.body6

do.body6:                                         ; preds = %do.end
  %arraydecay7 = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %call8 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %arraydecay7, i64 4)
  store i32 %call8, i32* %ret, align 4
  %7 = load i32, i32* %ret, align 4
  %cmp9 = icmp slt i32 %7, 0
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %do.body6
  br label %fail

if.end11:                                         ; preds = %do.body6
  br label %do.end12

do.end12:                                         ; preds = %if.end11
  br label %do.body13

do.body13:                                        ; preds = %do.end12
  %8 = load i8*, i8** %in.addr, align 4
  %9 = load i32, i32* %inlen.addr, align 4
  %conv = zext i32 %9 to i64
  %call14 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %8, i64 %conv)
  store i32 %call14, i32* %ret, align 4
  %10 = load i32, i32* %ret, align 4
  %cmp15 = icmp slt i32 %10, 0
  br i1 %cmp15, label %if.then17, label %if.end18

if.then17:                                        ; preds = %do.body13
  br label %fail

if.end18:                                         ; preds = %do.body13
  br label %do.end19

do.end19:                                         ; preds = %if.end18
  br label %do.body20

do.body20:                                        ; preds = %do.end19
  %11 = load i8*, i8** %out, align 4
  %12 = load i32, i32* %outlen.addr, align 4
  %call21 = call i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %11, i32 %12)
  store i32 %call21, i32* %ret, align 4
  %13 = load i32, i32* %ret, align 4
  %cmp22 = icmp slt i32 %13, 0
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %do.body20
  br label %fail

if.end25:                                         ; preds = %do.body20
  br label %do.end26

do.end26:                                         ; preds = %if.end25
  br label %if.end87

if.else:                                          ; preds = %if.end
  br label %do.body27

do.body27:                                        ; preds = %if.else
  %call28 = call i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state* %blake_state, i8* null, i32 0, i32 64)
  store i32 %call28, i32* %ret, align 4
  %14 = load i32, i32* %ret, align 4
  %cmp29 = icmp slt i32 %14, 0
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %do.body27
  br label %fail

if.end32:                                         ; preds = %do.body27
  br label %do.end33

do.end33:                                         ; preds = %if.end32
  br label %do.body34

do.body34:                                        ; preds = %do.end33
  %arraydecay35 = getelementptr inbounds [4 x i8], [4 x i8]* %outlen_bytes, i32 0, i32 0
  %call36 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %arraydecay35, i64 4)
  store i32 %call36, i32* %ret, align 4
  %15 = load i32, i32* %ret, align 4
  %cmp37 = icmp slt i32 %15, 0
  br i1 %cmp37, label %if.then39, label %if.end40

if.then39:                                        ; preds = %do.body34
  br label %fail

if.end40:                                         ; preds = %do.body34
  br label %do.end41

do.end41:                                         ; preds = %if.end40
  br label %do.body42

do.body42:                                        ; preds = %do.end41
  %16 = load i8*, i8** %in.addr, align 4
  %17 = load i32, i32* %inlen.addr, align 4
  %conv43 = zext i32 %17 to i64
  %call44 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %16, i64 %conv43)
  store i32 %call44, i32* %ret, align 4
  %18 = load i32, i32* %ret, align 4
  %cmp45 = icmp slt i32 %18, 0
  br i1 %cmp45, label %if.then47, label %if.end48

if.then47:                                        ; preds = %do.body42
  br label %fail

if.end48:                                         ; preds = %do.body42
  br label %do.end49

do.end49:                                         ; preds = %if.end48
  br label %do.body50

do.body50:                                        ; preds = %do.end49
  %arraydecay51 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %call52 = call i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state* %blake_state, i8* %arraydecay51, i32 64)
  store i32 %call52, i32* %ret, align 4
  %19 = load i32, i32* %ret, align 4
  %cmp53 = icmp slt i32 %19, 0
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %do.body50
  br label %fail

if.end56:                                         ; preds = %do.body50
  br label %do.end57

do.end57:                                         ; preds = %if.end56
  %20 = load i8*, i8** %out, align 4
  %arraydecay58 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %20, i8* align 16 %arraydecay58, i32 32, i1 false)
  %21 = load i8*, i8** %out, align 4
  %add.ptr = getelementptr i8, i8* %21, i32 32
  store i8* %add.ptr, i8** %out, align 4
  %22 = load i32, i32* %outlen.addr, align 4
  %sub = sub i32 %22, 32
  store i32 %sub, i32* %toproduce, align 4
  br label %while.cond

while.cond:                                       ; preds = %do.end71, %do.end57
  %23 = load i32, i32* %toproduce, align 4
  %cmp59 = icmp ugt i32 %23, 64
  br i1 %cmp59, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay61 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %arraydecay62 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay61, i8* align 16 %arraydecay62, i32 64, i1 false)
  br label %do.body63

do.body63:                                        ; preds = %while.body
  %arraydecay64 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %arraydecay65 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %call66 = call i32 @crypto_generichash_blake2b(i8* %arraydecay64, i32 64, i8* %arraydecay65, i64 64, i8* null, i32 0)
  store i32 %call66, i32* %ret, align 4
  %24 = load i32, i32* %ret, align 4
  %cmp67 = icmp slt i32 %24, 0
  br i1 %cmp67, label %if.then69, label %if.end70

if.then69:                                        ; preds = %do.body63
  br label %fail

if.end70:                                         ; preds = %do.body63
  br label %do.end71

do.end71:                                         ; preds = %if.end70
  %25 = load i8*, i8** %out, align 4
  %arraydecay72 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %25, i8* align 16 %arraydecay72, i32 32, i1 false)
  %26 = load i8*, i8** %out, align 4
  %add.ptr73 = getelementptr i8, i8* %26, i32 32
  store i8* %add.ptr73, i8** %out, align 4
  %27 = load i32, i32* %toproduce, align 4
  %sub74 = sub i32 %27, 32
  store i32 %sub74, i32* %toproduce, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %arraydecay75 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %arraydecay76 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay75, i8* align 16 %arraydecay76, i32 64, i1 false)
  br label %do.body77

do.body77:                                        ; preds = %while.end
  %arraydecay78 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %28 = load i32, i32* %toproduce, align 4
  %arraydecay79 = getelementptr inbounds [64 x i8], [64 x i8]* %in_buffer, i32 0, i32 0
  %call80 = call i32 @crypto_generichash_blake2b(i8* %arraydecay78, i32 %28, i8* %arraydecay79, i64 64, i8* null, i32 0)
  store i32 %call80, i32* %ret, align 4
  %29 = load i32, i32* %ret, align 4
  %cmp81 = icmp slt i32 %29, 0
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %do.body77
  br label %fail

if.end84:                                         ; preds = %do.body77
  br label %do.end85

do.end85:                                         ; preds = %if.end84
  %30 = load i8*, i8** %out, align 4
  %arraydecay86 = getelementptr inbounds [64 x i8], [64 x i8]* %out_buffer, i32 0, i32 0
  %31 = load i32, i32* %toproduce, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %30, i8* align 16 %arraydecay86, i32 %31, i1 false)
  br label %if.end87

if.end87:                                         ; preds = %do.end85, %do.end26
  br label %fail

fail:                                             ; preds = %if.end87, %if.then83, %if.then69, %if.then55, %if.then47, %if.then39, %if.then31, %if.then24, %if.then17, %if.then10, %if.then4, %if.then
  %32 = bitcast %struct.crypto_generichash_blake2b_state* %blake_state to i8*
  call void @sodium_memzero(i8* %32, i32 384)
  %33 = load i32, i32* %ret, align 4
  ret i32 %33
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

declare i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state*, i8*, i32, i32) #2

declare i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state*, i8*, i64) #2

declare i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state*, i8*, i32) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare i32 @crypto_generichash_blake2b(i8*, i32, i8*, i64, i8*, i32) #2

declare void @sodium_memzero(i8*, i32) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
