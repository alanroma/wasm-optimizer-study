; ModuleID = 'crypto_generichash/crypto_generichash.c'
source_filename = "crypto_generichash/crypto_generichash.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_generichash_blake2b_state = type { [384 x i8] }

@.str = private unnamed_addr constant [8 x i8] c"blake2b\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_bytes_max() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_keybytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_keybytes_max() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_generichash_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_statebytes() #0 {
entry:
  ret i32 384
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash(i8* nonnull %out, i32 %outlen, i8* %in, i64 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i32, i32* %outlen.addr, align 4
  %2 = load i8*, i8** %in.addr, align 4
  %3 = load i64, i64* %inlen.addr, align 8
  %4 = load i8*, i8** %key.addr, align 4
  %5 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @crypto_generichash_blake2b(i8* %0, i32 %1, i8* %2, i64 %3, i8* %4, i32 %5)
  ret i32 %call
}

declare i32 @crypto_generichash_blake2b(i8*, i32, i8*, i64, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* %key, i32 %keylen, i32 %outlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %outlen.addr = alloca i32, align 4
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %1 = load i8*, i8** %key.addr, align 4
  %2 = load i32, i32* %keylen.addr, align 4
  %3 = load i32, i32* %outlen.addr, align 4
  %call = call i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state* %0, i8* %1, i32 %2, i32 %3)
  ret i32 %call
}

declare i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state*, i8*, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  %call = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %0, i8* %1, i64 %2)
  ret i32 %call
}

declare i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* nonnull %out, i32 %outlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %1 = load i8*, i8** %out.addr, align 4
  %2 = load i32, i32* %outlen.addr, align 4
  %call = call i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state* %0, i8* %1, i32 %2)
  ret i32 %call
}

declare i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_generichash_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
