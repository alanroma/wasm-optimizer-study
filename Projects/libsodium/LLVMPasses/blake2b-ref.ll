; ModuleID = 'crypto_generichash/blake2b/ref/blake2b-ref.c'
source_filename = "crypto_generichash/blake2b/ref/blake2b-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.blake2b_state = type <{ [8 x i64], [2 x i64], [2 x i64], [256 x i8], i32, i8 }>
%struct.blake2b_param_ = type { i8, i8, i8, i8, [4 x i8], [8 x i8], i8, i8, [14 x i8], [16 x i8], [16 x i8] }

@blake2b_compress = internal global i32 (%struct.blake2b_state*, i8*)* @blake2b_compress_ref, align 4
@.str = private unnamed_addr constant [32 x i8] c"S->buflen <= BLAKE2B_BLOCKBYTES\00", align 1
@.str.1 = private unnamed_addr constant [45 x i8] c"crypto_generichash/blake2b/ref/blake2b-ref.c\00", align 1
@__func__.blake2b_final = private unnamed_addr constant [14 x i8] c"blake2b_final\00", align 1
@blake2b_IV = internal constant [8 x i64] [i64 7640891576956012808, i64 -4942790177534073029, i64 4354685564936845355, i64 -6534734903238641935, i64 5840696475078001361, i64 -7276294671716946913, i64 2270897969802886507, i64 6620516959819538809], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_param(%struct.blake2b_state* %S, %struct.blake2b_param_* %P) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %P.addr = alloca %struct.blake2b_param_*, align 4
  %i = alloca i32, align 4
  %p = alloca i8*, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store %struct.blake2b_param_* %P, %struct.blake2b_param_** %P.addr, align 4
  %0 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call = call i32 @blake2b_init0(%struct.blake2b_state* %0)
  %1 = load %struct.blake2b_param_*, %struct.blake2b_param_** %P.addr, align 4
  %2 = bitcast %struct.blake2b_param_* %1 to i8*
  store i8* %2, i8** %p, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %3, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8*, i8** %p, align 4
  %5 = load i32, i32* %i, align 4
  %mul = mul i32 8, %5
  %add.ptr = getelementptr i8, i8* %4, i32 %mul
  %call1 = call i64 @load64_le(i8* %add.ptr)
  %6 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [8 x i64], [8 x i64]* %h, i32 0, i32 %7
  %8 = load i64, i64* %arrayidx, align 1
  %xor = xor i64 %8, %call1
  store i64 %xor, i64* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_init0(%struct.blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %i = alloca i32, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 %1
  %2 = load i64, i64* %arrayidx, align 8
  %3 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [8 x i64], [8 x i64]* %h, i32 0, i32 %4
  store i64 %2, i64* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %6, i32 0, i32 1
  %7 = bitcast [2 x i64]* %t to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %7, i8 0, i32 293, i1 false)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i64* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 1 %1, i32 8, i1 false)
  %2 = load i64, i64* %w, align 8
  ret i64 %2
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init(%struct.blake2b_state* %S, i8 zeroext %outlen) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %outlen.addr = alloca i8, align 1
  %P = alloca [1 x %struct.blake2b_param_], align 16
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  %0 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8, i8* %outlen.addr, align 1
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay, i32 0, i32 0
  store i8 %2, i8* %digest_length, align 16
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay2, i32 0, i32 1
  store i8 0, i8* %key_length, align 1
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay3, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay4, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay5, i32 0, i32 4
  %arraydecay6 = getelementptr inbounds [4 x i8], [4 x i8]* %leaf_length, i32 0, i32 0
  call void @store32_le(i8* %arraydecay6, i32 0)
  %arraydecay7 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay7, i32 0, i32 5
  %arraydecay8 = getelementptr inbounds [8 x i8], [8 x i8]* %node_offset, i32 0, i32 0
  call void @store64_le(i8* %arraydecay8, i64 0)
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay9, i32 0, i32 6
  store i8 0, i8* %node_depth, align 16
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay10, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay11, i32 0, i32 8
  %arraydecay12 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay12, i8 0, i32 14, i1 false)
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay13, i32 0, i32 9
  %arraydecay14 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay14, i8 0, i32 16, i1 false)
  %arraydecay15 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay15, i32 0, i32 10
  %arraydecay16 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay16, i8 0, i32 16, i1 false)
  %3 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %call = call i32 @blake2b_init_param(%struct.blake2b_state* %3, %struct.blake2b_param_* %arraydecay17)
  ret i32 %call
}

; Function Attrs: noreturn
declare void @sodium_misuse() #1

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_salt_personal(%struct.blake2b_state* %S, i8 zeroext %outlen, i8* %salt, i8* %personal) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %outlen.addr = alloca i8, align 1
  %salt.addr = alloca i8*, align 4
  %personal.addr = alloca i8*, align 4
  %P = alloca [1 x %struct.blake2b_param_], align 16
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  store i8* %salt, i8** %salt.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8, i8* %outlen.addr, align 1
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay, i32 0, i32 0
  store i8 %2, i8* %digest_length, align 16
  %arraydecay2 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay2, i32 0, i32 1
  store i8 0, i8* %key_length, align 1
  %arraydecay3 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay3, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay4 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay4, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay5 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay5, i32 0, i32 4
  %arraydecay6 = getelementptr inbounds [4 x i8], [4 x i8]* %leaf_length, i32 0, i32 0
  call void @store32_le(i8* %arraydecay6, i32 0)
  %arraydecay7 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay7, i32 0, i32 5
  %arraydecay8 = getelementptr inbounds [8 x i8], [8 x i8]* %node_offset, i32 0, i32 0
  call void @store64_le(i8* %arraydecay8, i64 0)
  %arraydecay9 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay9, i32 0, i32 6
  store i8 0, i8* %node_depth, align 16
  %arraydecay10 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay10, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay11, i32 0, i32 8
  %arraydecay12 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay12, i8 0, i32 14, i1 false)
  %3 = load i8*, i8** %salt.addr, align 4
  %cmp13 = icmp ne i8* %3, null
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.end
  %arraydecay16 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %4 = load i8*, i8** %salt.addr, align 4
  %call = call i32 @blake2b_param_set_salt(%struct.blake2b_param_* %arraydecay16, i8* %4)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %arraydecay17 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %salt18 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay17, i32 0, i32 9
  %arraydecay19 = getelementptr inbounds [16 x i8], [16 x i8]* %salt18, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay19, i8 0, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then15
  %5 = load i8*, i8** %personal.addr, align 4
  %cmp21 = icmp ne i8* %5, null
  br i1 %cmp21, label %if.then23, label %if.else26

if.then23:                                        ; preds = %if.end20
  %arraydecay24 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %6 = load i8*, i8** %personal.addr, align 4
  %call25 = call i32 @blake2b_param_set_personal(%struct.blake2b_param_* %arraydecay24, i8* %6)
  br label %if.end30

if.else26:                                        ; preds = %if.end20
  %arraydecay27 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %personal28 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay27, i32 0, i32 10
  %arraydecay29 = getelementptr inbounds [16 x i8], [16 x i8]* %personal28, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay29, i8 0, i32 16, i1 false)
  br label %if.end30

if.end30:                                         ; preds = %if.else26, %if.then23
  %7 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay31 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %call32 = call i32 @blake2b_init_param(%struct.blake2b_state* %7, %struct.blake2b_param_* %arraydecay31)
  ret i32 %call32
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_param_set_salt(%struct.blake2b_param_* %P, i8* %salt) #0 {
entry:
  %P.addr = alloca %struct.blake2b_param_*, align 4
  %salt.addr = alloca i8*, align 4
  store %struct.blake2b_param_* %P, %struct.blake2b_param_** %P.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  %0 = load %struct.blake2b_param_*, %struct.blake2b_param_** %P.addr, align 4
  %salt1 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %0, i32 0, i32 9
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %salt1, i32 0, i32 0
  %1 = load i8*, i8** %salt.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay, i8* align 1 %1, i32 16, i1 false)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_param_set_personal(%struct.blake2b_param_* %P, i8* %personal) #0 {
entry:
  %P.addr = alloca %struct.blake2b_param_*, align 4
  %personal.addr = alloca i8*, align 4
  store %struct.blake2b_param_* %P, %struct.blake2b_param_** %P.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load %struct.blake2b_param_*, %struct.blake2b_param_** %P.addr, align 4
  %personal1 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %0, i32 0, i32 10
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %personal1, i32 0, i32 0
  %1 = load i8*, i8** %personal.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay, i8* align 1 %1, i32 16, i1 false)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_key(%struct.blake2b_state* %S, i8 zeroext %outlen, i8* %key, i8 zeroext %keylen) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %outlen.addr = alloca i8, align 1
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i8, align 1
  %P = alloca [1 x %struct.blake2b_param_], align 16
  %block = alloca [128 x i8], align 16
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  store i8* %key, i8** %key.addr, align 4
  store i8 %keylen, i8* %keylen.addr, align 1
  %0 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %key.addr, align 4
  %tobool2 = icmp ne i8* %2, null
  br i1 %tobool2, label %lor.lhs.false3, label %if.then9

lor.lhs.false3:                                   ; preds = %if.end
  %3 = load i8, i8* %keylen.addr, align 1
  %tobool4 = icmp ne i8 %3, 0
  br i1 %tobool4, label %lor.lhs.false5, label %if.then9

lor.lhs.false5:                                   ; preds = %lor.lhs.false3
  %4 = load i8, i8* %keylen.addr, align 1
  %conv6 = zext i8 %4 to i32
  %cmp7 = icmp sgt i32 %conv6, 64
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false5, %lor.lhs.false3, %if.end
  call void @sodium_misuse() #5
  unreachable

if.end10:                                         ; preds = %lor.lhs.false5
  %5 = load i8, i8* %outlen.addr, align 1
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay, i32 0, i32 0
  store i8 %5, i8* %digest_length, align 16
  %6 = load i8, i8* %keylen.addr, align 1
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay11, i32 0, i32 1
  store i8 %6, i8* %key_length, align 1
  %arraydecay12 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay12, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay13, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay14 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay14, i32 0, i32 4
  %arraydecay15 = getelementptr inbounds [4 x i8], [4 x i8]* %leaf_length, i32 0, i32 0
  call void @store32_le(i8* %arraydecay15, i32 0)
  %arraydecay16 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay16, i32 0, i32 5
  %arraydecay17 = getelementptr inbounds [8 x i8], [8 x i8]* %node_offset, i32 0, i32 0
  call void @store64_le(i8* %arraydecay17, i64 0)
  %arraydecay18 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay18, i32 0, i32 6
  store i8 0, i8* %node_depth, align 16
  %arraydecay19 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay19, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1
  %arraydecay20 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay20, i32 0, i32 8
  %arraydecay21 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay21, i8 0, i32 14, i1 false)
  %arraydecay22 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %salt = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay22, i32 0, i32 9
  %arraydecay23 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay23, i8 0, i32 16, i1 false)
  %arraydecay24 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %personal = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay24, i32 0, i32 10
  %arraydecay25 = getelementptr inbounds [16 x i8], [16 x i8]* %personal, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay25, i8 0, i32 16, i1 false)
  %7 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay26 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %call = call i32 @blake2b_init_param(%struct.blake2b_state* %7, %struct.blake2b_param_* %arraydecay26)
  %cmp27 = icmp slt i32 %call, 0
  br i1 %cmp27, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end10
  call void @sodium_misuse() #5
  unreachable

if.end30:                                         ; preds = %if.end10
  %arraydecay31 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay31, i8 0, i32 128, i1 false)
  %arraydecay32 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %8 = load i8*, i8** %key.addr, align 4
  %9 = load i8, i8* %keylen.addr, align 1
  %conv33 = zext i8 %9 to i32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay32, i8* align 1 %8, i32 %conv33, i1 false)
  %10 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay34 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %call35 = call i32 @blake2b_update(%struct.blake2b_state* %10, i8* %arraydecay34, i64 128)
  %arraydecay36 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay36, i32 128)
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_update(%struct.blake2b_state* %S, i8* %in, i64 %inlen) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %left = alloca i32, align 4
  %fill = alloca i32, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load i64, i64* %inlen.addr, align 8
  %cmp = icmp ugt i64 %0, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %1, i32 0, i32 4
  %2 = load i32, i32* %buflen, align 1
  store i32 %2, i32* %left, align 4
  %3 = load i32, i32* %left, align 4
  %sub = sub i32 256, %3
  store i32 %sub, i32* %fill, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  %5 = load i32, i32* %fill, align 4
  %conv = zext i32 %5 to i64
  %cmp1 = icmp ugt i64 %4, %conv
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %6 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %6, i32 0, i32 3
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %buf, i32 0, i32 0
  %7 = load i32, i32* %left, align 4
  %add.ptr = getelementptr i8, i8* %arraydecay, i32 %7
  %8 = load i8*, i8** %in.addr, align 4
  %9 = load i32, i32* %fill, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %8, i32 %9, i1 false)
  %10 = load i32, i32* %fill, align 4
  %11 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen3 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen3, align 1
  %add = add i32 %12, %10
  store i32 %add, i32* %buflen3, align 1
  %13 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call = call i32 @blake2b_increment_counter(%struct.blake2b_state* %13, i64 128)
  %14 = load i32 (%struct.blake2b_state*, i8*)*, i32 (%struct.blake2b_state*, i8*)** @blake2b_compress, align 4
  %15 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %16 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf4 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %16, i32 0, i32 3
  %arraydecay5 = getelementptr inbounds [256 x i8], [256 x i8]* %buf4, i32 0, i32 0
  %call6 = call i32 %14(%struct.blake2b_state* %15, i8* %arraydecay5)
  %17 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf7 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %17, i32 0, i32 3
  %arraydecay8 = getelementptr inbounds [256 x i8], [256 x i8]* %buf7, i32 0, i32 0
  %18 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf9 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %18, i32 0, i32 3
  %arraydecay10 = getelementptr inbounds [256 x i8], [256 x i8]* %buf9, i32 0, i32 0
  %add.ptr11 = getelementptr i8, i8* %arraydecay10, i32 128
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay8, i8* align 1 %add.ptr11, i32 128, i1 false)
  %19 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen12 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %19, i32 0, i32 4
  %20 = load i32, i32* %buflen12, align 1
  %sub13 = sub i32 %20, 128
  store i32 %sub13, i32* %buflen12, align 1
  %21 = load i32, i32* %fill, align 4
  %22 = load i8*, i8** %in.addr, align 4
  %add.ptr14 = getelementptr i8, i8* %22, i32 %21
  store i8* %add.ptr14, i8** %in.addr, align 4
  %23 = load i32, i32* %fill, align 4
  %conv15 = zext i32 %23 to i64
  %24 = load i64, i64* %inlen.addr, align 8
  %sub16 = sub i64 %24, %conv15
  store i64 %sub16, i64* %inlen.addr, align 8
  br label %if.end

if.else:                                          ; preds = %while.body
  %25 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf17 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %25, i32 0, i32 3
  %arraydecay18 = getelementptr inbounds [256 x i8], [256 x i8]* %buf17, i32 0, i32 0
  %26 = load i32, i32* %left, align 4
  %add.ptr19 = getelementptr i8, i8* %arraydecay18, i32 %26
  %27 = load i8*, i8** %in.addr, align 4
  %28 = load i64, i64* %inlen.addr, align 8
  %conv20 = trunc i64 %28 to i32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr19, i8* align 1 %27, i32 %conv20, i1 false)
  %29 = load i64, i64* %inlen.addr, align 8
  %30 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen21 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %30, i32 0, i32 4
  %31 = load i32, i32* %buflen21, align 1
  %conv22 = zext i32 %31 to i64
  %add23 = add i64 %conv22, %29
  %conv24 = trunc i64 %add23 to i32
  store i32 %conv24, i32* %buflen21, align 1
  %32 = load i64, i64* %inlen.addr, align 8
  %33 = load i8*, i8** %in.addr, align 4
  %idx.ext = trunc i64 %32 to i32
  %add.ptr25 = getelementptr i8, i8* %33, i32 %idx.ext
  store i8* %add.ptr25, i8** %in.addr, align 4
  %34 = load i64, i64* %inlen.addr, align 8
  %35 = load i64, i64* %inlen.addr, align 8
  %sub26 = sub i64 %35, %34
  store i64 %sub26, i64* %inlen.addr, align 8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret i32 0
}

declare void @sodium_memzero(i8*, i32) #4

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_init_key_salt_personal(%struct.blake2b_state* %S, i8 zeroext %outlen, i8* %key, i8 zeroext %keylen, i8* %salt, i8* %personal) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %outlen.addr = alloca i8, align 1
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i8, align 1
  %salt.addr = alloca i8*, align 4
  %personal.addr = alloca i8*, align 4
  %P = alloca [1 x %struct.blake2b_param_], align 16
  %block = alloca [128 x i8], align 16
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  store i8* %key, i8** %key.addr, align 4
  store i8 %keylen, i8* %keylen.addr, align 1
  store i8* %salt, i8** %salt.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %key.addr, align 4
  %tobool2 = icmp ne i8* %2, null
  br i1 %tobool2, label %lor.lhs.false3, label %if.then9

lor.lhs.false3:                                   ; preds = %if.end
  %3 = load i8, i8* %keylen.addr, align 1
  %tobool4 = icmp ne i8 %3, 0
  br i1 %tobool4, label %lor.lhs.false5, label %if.then9

lor.lhs.false5:                                   ; preds = %lor.lhs.false3
  %4 = load i8, i8* %keylen.addr, align 1
  %conv6 = zext i8 %4 to i32
  %cmp7 = icmp sgt i32 %conv6, 64
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false5, %lor.lhs.false3, %if.end
  call void @sodium_misuse() #5
  unreachable

if.end10:                                         ; preds = %lor.lhs.false5
  %5 = load i8, i8* %outlen.addr, align 1
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %digest_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay, i32 0, i32 0
  store i8 %5, i8* %digest_length, align 16
  %6 = load i8, i8* %keylen.addr, align 1
  %arraydecay11 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %key_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay11, i32 0, i32 1
  store i8 %6, i8* %key_length, align 1
  %arraydecay12 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %fanout = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay12, i32 0, i32 2
  store i8 1, i8* %fanout, align 2
  %arraydecay13 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay13, i32 0, i32 3
  store i8 1, i8* %depth, align 1
  %arraydecay14 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %leaf_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay14, i32 0, i32 4
  %arraydecay15 = getelementptr inbounds [4 x i8], [4 x i8]* %leaf_length, i32 0, i32 0
  call void @store32_le(i8* %arraydecay15, i32 0)
  %arraydecay16 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_offset = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay16, i32 0, i32 5
  %arraydecay17 = getelementptr inbounds [8 x i8], [8 x i8]* %node_offset, i32 0, i32 0
  call void @store64_le(i8* %arraydecay17, i64 0)
  %arraydecay18 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %node_depth = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay18, i32 0, i32 6
  store i8 0, i8* %node_depth, align 16
  %arraydecay19 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %inner_length = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay19, i32 0, i32 7
  store i8 0, i8* %inner_length, align 1
  %arraydecay20 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %reserved = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay20, i32 0, i32 8
  %arraydecay21 = getelementptr inbounds [14 x i8], [14 x i8]* %reserved, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 2 %arraydecay21, i8 0, i32 14, i1 false)
  %7 = load i8*, i8** %salt.addr, align 4
  %cmp22 = icmp ne i8* %7, null
  br i1 %cmp22, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.end10
  %arraydecay25 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %8 = load i8*, i8** %salt.addr, align 4
  %call = call i32 @blake2b_param_set_salt(%struct.blake2b_param_* %arraydecay25, i8* %8)
  br label %if.end29

if.else:                                          ; preds = %if.end10
  %arraydecay26 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %salt27 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay26, i32 0, i32 9
  %arraydecay28 = getelementptr inbounds [16 x i8], [16 x i8]* %salt27, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay28, i8 0, i32 16, i1 false)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.then24
  %9 = load i8*, i8** %personal.addr, align 4
  %cmp30 = icmp ne i8* %9, null
  br i1 %cmp30, label %if.then32, label %if.else35

if.then32:                                        ; preds = %if.end29
  %arraydecay33 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %10 = load i8*, i8** %personal.addr, align 4
  %call34 = call i32 @blake2b_param_set_personal(%struct.blake2b_param_* %arraydecay33, i8* %10)
  br label %if.end39

if.else35:                                        ; preds = %if.end29
  %arraydecay36 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %personal37 = getelementptr inbounds %struct.blake2b_param_, %struct.blake2b_param_* %arraydecay36, i32 0, i32 10
  %arraydecay38 = getelementptr inbounds [16 x i8], [16 x i8]* %personal37, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay38, i8 0, i32 16, i1 false)
  br label %if.end39

if.end39:                                         ; preds = %if.else35, %if.then32
  %11 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay40 = getelementptr inbounds [1 x %struct.blake2b_param_], [1 x %struct.blake2b_param_]* %P, i32 0, i32 0
  %call41 = call i32 @blake2b_init_param(%struct.blake2b_state* %11, %struct.blake2b_param_* %arraydecay40)
  %cmp42 = icmp slt i32 %call41, 0
  br i1 %cmp42, label %if.then44, label %if.end45

if.then44:                                        ; preds = %if.end39
  call void @sodium_misuse() #5
  unreachable

if.end45:                                         ; preds = %if.end39
  %arraydecay46 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay46, i8 0, i32 128, i1 false)
  %arraydecay47 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %12 = load i8*, i8** %key.addr, align 4
  %13 = load i8, i8* %keylen.addr, align 1
  %conv48 = zext i8 %13 to i32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay47, i8* align 1 %12, i32 %conv48, i1 false)
  %14 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %arraydecay49 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  %call50 = call i32 @blake2b_update(%struct.blake2b_state* %14, i8* %arraydecay49, i64 128)
  %arraydecay51 = getelementptr inbounds [128 x i8], [128 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay51, i32 128)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_increment_counter(%struct.blake2b_state* %S, i64 %inc) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %inc.addr = alloca i64, align 8
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i64 %inc, i64* %inc.addr, align 8
  %0 = load i64, i64* %inc.addr, align 8
  %1 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %1, i32 0, i32 1
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %2 = load i64, i64* %arrayidx, align 1
  %add = add i64 %2, %0
  store i64 %add, i64* %arrayidx, align 1
  %3 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t1 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %3, i32 0, i32 1
  %arrayidx2 = getelementptr [2 x i64], [2 x i64]* %t1, i32 0, i32 0
  %4 = load i64, i64* %arrayidx2, align 1
  %5 = load i64, i64* %inc.addr, align 8
  %cmp = icmp ult i64 %4, %5
  %conv = zext i1 %cmp to i32
  %conv3 = sext i32 %conv to i64
  %6 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t4 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %6, i32 0, i32 1
  %arrayidx5 = getelementptr [2 x i64], [2 x i64]* %t4, i32 0, i32 1
  %7 = load i64, i64* %arrayidx5, align 1
  %add6 = add i64 %7, %conv3
  store i64 %add6, i64* %arrayidx5, align 1
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_final(%struct.blake2b_state* %S, i8* %out, i8 zeroext %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %S.addr = alloca %struct.blake2b_state*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i8, align 1
  %buffer = alloca [64 x i8], align 16
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  %0 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %0, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv, 64
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call = call i32 @blake2b_is_lastblock(%struct.blake2b_state* %2)
  %tobool2 = icmp ne i32 %call, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %3 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %3, i32 0, i32 4
  %4 = load i32, i32* %buflen, align 1
  %cmp5 = icmp ugt i32 %4, 128
  br i1 %cmp5, label %if.then7, label %if.end19

if.then7:                                         ; preds = %if.end4
  %5 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call8 = call i32 @blake2b_increment_counter(%struct.blake2b_state* %5, i64 128)
  %6 = load i32 (%struct.blake2b_state*, i8*)*, i32 (%struct.blake2b_state*, i8*)** @blake2b_compress, align 4
  %7 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %8 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %8, i32 0, i32 3
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %buf, i32 0, i32 0
  %call9 = call i32 %6(%struct.blake2b_state* %7, i8* %arraydecay)
  %9 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen10 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %9, i32 0, i32 4
  %10 = load i32, i32* %buflen10, align 1
  %sub = sub i32 %10, 128
  store i32 %sub, i32* %buflen10, align 1
  %11 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen11 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %11, i32 0, i32 4
  %12 = load i32, i32* %buflen11, align 1
  %cmp12 = icmp ule i32 %12, 128
  br i1 %cmp12, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then7
  call void @__assert_fail(i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([45 x i8], [45 x i8]* @.str.1, i32 0, i32 0), i32 306, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @__func__.blake2b_final, i32 0, i32 0)) #5
  unreachable

13:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %13, %if.then7
  %14 = phi i1 [ true, %if.then7 ], [ false, %13 ]
  %lor.ext = zext i1 %14 to i32
  %15 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf14 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %15, i32 0, i32 3
  %arraydecay15 = getelementptr inbounds [256 x i8], [256 x i8]* %buf14, i32 0, i32 0
  %16 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf16 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %16, i32 0, i32 3
  %arraydecay17 = getelementptr inbounds [256 x i8], [256 x i8]* %buf16, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay17, i32 128
  %17 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen18 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %17, i32 0, i32 4
  %18 = load i32, i32* %buflen18, align 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arraydecay15, i8* align 1 %add.ptr, i32 %18, i1 false)
  br label %if.end19

if.end19:                                         ; preds = %lor.end, %if.end4
  %19 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %20 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen20 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %20, i32 0, i32 4
  %21 = load i32, i32* %buflen20, align 1
  %conv21 = zext i32 %21 to i64
  %call22 = call i32 @blake2b_increment_counter(%struct.blake2b_state* %19, i64 %conv21)
  %22 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call23 = call i32 @blake2b_set_lastblock(%struct.blake2b_state* %22)
  %23 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf24 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %23, i32 0, i32 3
  %arraydecay25 = getelementptr inbounds [256 x i8], [256 x i8]* %buf24, i32 0, i32 0
  %24 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen26 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %24, i32 0, i32 4
  %25 = load i32, i32* %buflen26, align 1
  %add.ptr27 = getelementptr i8, i8* %arraydecay25, i32 %25
  %26 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buflen28 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %26, i32 0, i32 4
  %27 = load i32, i32* %buflen28, align 1
  %sub29 = sub i32 256, %27
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr27, i8 0, i32 %sub29, i1 false)
  %28 = load i32 (%struct.blake2b_state*, i8*)*, i32 (%struct.blake2b_state*, i8*)** @blake2b_compress, align 4
  %29 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %30 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf30 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %30, i32 0, i32 3
  %arraydecay31 = getelementptr inbounds [256 x i8], [256 x i8]* %buf30, i32 0, i32 0
  %call32 = call i32 %28(%struct.blake2b_state* %29, i8* %arraydecay31)
  %arraydecay33 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr34 = getelementptr i8, i8* %arraydecay33, i32 0
  %31 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %31, i32 0, i32 0
  %arrayidx = getelementptr [8 x i64], [8 x i64]* %h, i32 0, i32 0
  %32 = load i64, i64* %arrayidx, align 1
  call void @store64_le(i8* %add.ptr34, i64 %32)
  %arraydecay35 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr36 = getelementptr i8, i8* %arraydecay35, i32 8
  %33 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h37 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %33, i32 0, i32 0
  %arrayidx38 = getelementptr [8 x i64], [8 x i64]* %h37, i32 0, i32 1
  %34 = load i64, i64* %arrayidx38, align 1
  call void @store64_le(i8* %add.ptr36, i64 %34)
  %arraydecay39 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr40 = getelementptr i8, i8* %arraydecay39, i32 16
  %35 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h41 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %35, i32 0, i32 0
  %arrayidx42 = getelementptr [8 x i64], [8 x i64]* %h41, i32 0, i32 2
  %36 = load i64, i64* %arrayidx42, align 1
  call void @store64_le(i8* %add.ptr40, i64 %36)
  %arraydecay43 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr44 = getelementptr i8, i8* %arraydecay43, i32 24
  %37 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h45 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %37, i32 0, i32 0
  %arrayidx46 = getelementptr [8 x i64], [8 x i64]* %h45, i32 0, i32 3
  %38 = load i64, i64* %arrayidx46, align 1
  call void @store64_le(i8* %add.ptr44, i64 %38)
  %arraydecay47 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr48 = getelementptr i8, i8* %arraydecay47, i32 32
  %39 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h49 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %39, i32 0, i32 0
  %arrayidx50 = getelementptr [8 x i64], [8 x i64]* %h49, i32 0, i32 4
  %40 = load i64, i64* %arrayidx50, align 1
  call void @store64_le(i8* %add.ptr48, i64 %40)
  %arraydecay51 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr52 = getelementptr i8, i8* %arraydecay51, i32 40
  %41 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h53 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %41, i32 0, i32 0
  %arrayidx54 = getelementptr [8 x i64], [8 x i64]* %h53, i32 0, i32 5
  %42 = load i64, i64* %arrayidx54, align 1
  call void @store64_le(i8* %add.ptr52, i64 %42)
  %arraydecay55 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr56 = getelementptr i8, i8* %arraydecay55, i32 48
  %43 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h57 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %43, i32 0, i32 0
  %arrayidx58 = getelementptr [8 x i64], [8 x i64]* %h57, i32 0, i32 6
  %44 = load i64, i64* %arrayidx58, align 1
  call void @store64_le(i8* %add.ptr56, i64 %44)
  %arraydecay59 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %add.ptr60 = getelementptr i8, i8* %arraydecay59, i32 56
  %45 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h61 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %45, i32 0, i32 0
  %arrayidx62 = getelementptr [8 x i64], [8 x i64]* %h61, i32 0, i32 7
  %46 = load i64, i64* %arrayidx62, align 1
  call void @store64_le(i8* %add.ptr60, i64 %46)
  %47 = load i8*, i8** %out.addr, align 4
  %arraydecay63 = getelementptr inbounds [64 x i8], [64 x i8]* %buffer, i32 0, i32 0
  %48 = load i8, i8* %outlen.addr, align 1
  %conv64 = zext i8 %48 to i32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %47, i8* align 16 %arraydecay63, i32 %conv64, i1 false)
  %49 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h65 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %49, i32 0, i32 0
  %arraydecay66 = getelementptr inbounds [8 x i64], [8 x i64]* %h65, i32 0, i32 0
  %50 = bitcast i64* %arraydecay66 to i8*
  call void @sodium_memzero(i8* %50, i32 64)
  %51 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %buf67 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %51, i32 0, i32 3
  %arraydecay68 = getelementptr inbounds [256 x i8], [256 x i8]* %buf67, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay68, i32 256)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.then3
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_is_lastblock(%struct.blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  %0 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %0, i32 0, i32 2
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %1 = load i64, i64* %arrayidx, align 1
  %cmp = icmp ne i64 %1, 0
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_set_lastblock(%struct.blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  %0 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %last_node = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %0, i32 0, i32 5
  %1 = load i8, i8* %last_node, align 1
  %tobool = icmp ne i8 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %call = call i32 @blake2b_set_lastnode(%struct.blake2b_state* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %3, i32 0, i32 2
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %f, i32 0, i32 0
  store i64 -1, i64* %arrayidx, align 1
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b(i8* %out, i8* %in, i8* %key, i8 zeroext %outlen, i64 %inlen, i8 zeroext %keylen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %key.addr = alloca i8*, align 4
  %outlen.addr = alloca i8, align 1
  %inlen.addr = alloca i64, align 8
  %keylen.addr = alloca i8, align 1
  %S = alloca [1 x %struct.blake2b_state], align 64
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8 %keylen, i8* %keylen.addr, align 1
  %0 = load i8*, i8** %in.addr, align 4
  %cmp = icmp eq i8* null, %0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i64, i64* %inlen.addr, align 8
  %cmp1 = icmp ugt i64 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %out.addr, align 4
  %cmp2 = icmp eq i8* null, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  call void @sodium_misuse() #5
  unreachable

if.end4:                                          ; preds = %if.end
  %3 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %3, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then7

lor.lhs.false:                                    ; preds = %if.end4
  %4 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %4 to i32
  %cmp5 = icmp sgt i32 %conv, 64
  br i1 %cmp5, label %if.then7, label %if.end8

if.then7:                                         ; preds = %lor.lhs.false, %if.end4
  call void @sodium_misuse() #5
  unreachable

if.end8:                                          ; preds = %lor.lhs.false
  %5 = load i8*, i8** %key.addr, align 4
  %cmp9 = icmp eq i8* null, %5
  br i1 %cmp9, label %land.lhs.true11, label %if.end16

land.lhs.true11:                                  ; preds = %if.end8
  %6 = load i8, i8* %keylen.addr, align 1
  %conv12 = zext i8 %6 to i32
  %cmp13 = icmp sgt i32 %conv12, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true11
  call void @sodium_misuse() #5
  unreachable

if.end16:                                         ; preds = %land.lhs.true11, %if.end8
  %7 = load i8, i8* %keylen.addr, align 1
  %conv17 = zext i8 %7 to i32
  %cmp18 = icmp sgt i32 %conv17, 64
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end16
  call void @sodium_misuse() #5
  unreachable

if.end21:                                         ; preds = %if.end16
  %8 = load i8, i8* %keylen.addr, align 1
  %conv22 = zext i8 %8 to i32
  %cmp23 = icmp sgt i32 %conv22, 0
  br i1 %cmp23, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end21
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %9 = load i8, i8* %outlen.addr, align 1
  %10 = load i8*, i8** %key.addr, align 4
  %11 = load i8, i8* %keylen.addr, align 1
  %call = call i32 @blake2b_init_key(%struct.blake2b_state* %arraydecay, i8 zeroext %9, i8* %10, i8 zeroext %11)
  %cmp26 = icmp slt i32 %call, 0
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then25
  call void @sodium_misuse() #5
  unreachable

if.end29:                                         ; preds = %if.then25
  br label %if.end36

if.else:                                          ; preds = %if.end21
  %arraydecay30 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %12 = load i8, i8* %outlen.addr, align 1
  %call31 = call i32 @blake2b_init(%struct.blake2b_state* %arraydecay30, i8 zeroext %12)
  %cmp32 = icmp slt i32 %call31, 0
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.else
  call void @sodium_misuse() #5
  unreachable

if.end35:                                         ; preds = %if.else
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end29
  %arraydecay37 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %13 = load i8*, i8** %in.addr, align 4
  %14 = load i64, i64* %inlen.addr, align 8
  %call38 = call i32 @blake2b_update(%struct.blake2b_state* %arraydecay37, i8* %13, i64 %14)
  %arraydecay39 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %15 = load i8*, i8** %out.addr, align 4
  %16 = load i8, i8* %outlen.addr, align 1
  %call40 = call i32 @blake2b_final(%struct.blake2b_state* %arraydecay39, i8* %15, i8 zeroext %16)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_salt_personal(i8* %out, i8* %in, i8* %key, i8 zeroext %outlen, i64 %inlen, i8 zeroext %keylen, i8* %salt, i8* %personal) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %key.addr = alloca i8*, align 4
  %outlen.addr = alloca i8, align 1
  %inlen.addr = alloca i64, align 8
  %keylen.addr = alloca i8, align 1
  %salt.addr = alloca i8*, align 4
  %personal.addr = alloca i8*, align 4
  %S = alloca [1 x %struct.blake2b_state], align 64
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i8 %outlen, i8* %outlen.addr, align 1
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8 %keylen, i8* %keylen.addr, align 1
  store i8* %salt, i8** %salt.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load i8*, i8** %in.addr, align 4
  %cmp = icmp eq i8* null, %0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i64, i64* %inlen.addr, align 8
  %cmp1 = icmp ugt i64 %1, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %land.lhs.true, %entry
  %2 = load i8*, i8** %out.addr, align 4
  %cmp2 = icmp eq i8* null, %2
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  call void @sodium_misuse() #5
  unreachable

if.end4:                                          ; preds = %if.end
  %3 = load i8, i8* %outlen.addr, align 1
  %tobool = icmp ne i8 %3, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then7

lor.lhs.false:                                    ; preds = %if.end4
  %4 = load i8, i8* %outlen.addr, align 1
  %conv = zext i8 %4 to i32
  %cmp5 = icmp sgt i32 %conv, 64
  br i1 %cmp5, label %if.then7, label %if.end8

if.then7:                                         ; preds = %lor.lhs.false, %if.end4
  call void @sodium_misuse() #5
  unreachable

if.end8:                                          ; preds = %lor.lhs.false
  %5 = load i8*, i8** %key.addr, align 4
  %cmp9 = icmp eq i8* null, %5
  br i1 %cmp9, label %land.lhs.true11, label %if.end16

land.lhs.true11:                                  ; preds = %if.end8
  %6 = load i8, i8* %keylen.addr, align 1
  %conv12 = zext i8 %6 to i32
  %cmp13 = icmp sgt i32 %conv12, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true11
  call void @sodium_misuse() #5
  unreachable

if.end16:                                         ; preds = %land.lhs.true11, %if.end8
  %7 = load i8, i8* %keylen.addr, align 1
  %conv17 = zext i8 %7 to i32
  %cmp18 = icmp sgt i32 %conv17, 64
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end16
  call void @sodium_misuse() #5
  unreachable

if.end21:                                         ; preds = %if.end16
  %8 = load i8, i8* %keylen.addr, align 1
  %conv22 = zext i8 %8 to i32
  %cmp23 = icmp sgt i32 %conv22, 0
  br i1 %cmp23, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end21
  %arraydecay = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %9 = load i8, i8* %outlen.addr, align 1
  %10 = load i8*, i8** %key.addr, align 4
  %11 = load i8, i8* %keylen.addr, align 1
  %12 = load i8*, i8** %salt.addr, align 4
  %13 = load i8*, i8** %personal.addr, align 4
  %call = call i32 @blake2b_init_key_salt_personal(%struct.blake2b_state* %arraydecay, i8 zeroext %9, i8* %10, i8 zeroext %11, i8* %12, i8* %13)
  %cmp26 = icmp slt i32 %call, 0
  br i1 %cmp26, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then25
  call void @sodium_misuse() #5
  unreachable

if.end29:                                         ; preds = %if.then25
  br label %if.end36

if.else:                                          ; preds = %if.end21
  %arraydecay30 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %14 = load i8, i8* %outlen.addr, align 1
  %15 = load i8*, i8** %salt.addr, align 4
  %16 = load i8*, i8** %personal.addr, align 4
  %call31 = call i32 @blake2b_init_salt_personal(%struct.blake2b_state* %arraydecay30, i8 zeroext %14, i8* %15, i8* %16)
  %cmp32 = icmp slt i32 %call31, 0
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.else
  call void @sodium_misuse() #5
  unreachable

if.end35:                                         ; preds = %if.else
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end29
  %arraydecay37 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %17 = load i8*, i8** %in.addr, align 4
  %18 = load i64, i64* %inlen.addr, align 8
  %call38 = call i32 @blake2b_update(%struct.blake2b_state* %arraydecay37, i8* %17, i64 %18)
  %arraydecay39 = getelementptr inbounds [1 x %struct.blake2b_state], [1 x %struct.blake2b_state]* %S, i32 0, i32 0
  %19 = load i8*, i8** %out.addr, align 4
  %20 = load i8, i8* %outlen.addr, align 1
  %call40 = call i32 @blake2b_final(%struct.blake2b_state* %arraydecay39, i8* %19, i8 zeroext %20)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_pick_best_implementation() #0 {
entry:
  store i32 (%struct.blake2b_state*, i8*)* @blake2b_compress_ref, i32 (%struct.blake2b_state*, i8*)** @blake2b_compress, align 4
  ret i32 0
}

declare i32 @blake2b_compress_ref(%struct.blake2b_state*, i8*) #4

; Function Attrs: noinline nounwind optnone
define internal i32 @blake2b_set_lastnode(%struct.blake2b_state* %S) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  %0 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %0, i32 0, i32 2
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %f, i32 0, i32 1
  store i64 -1, i64* %arrayidx, align 1
  ret i32 0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
