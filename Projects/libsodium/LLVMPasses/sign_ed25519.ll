; ModuleID = 'crypto_sign/ed25519/sign_ed25519.c'
source_filename = "crypto_sign/ed25519/sign_ed25519.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_sign_ed25519ph_state = type { %struct.crypto_hash_sha512_state }
%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519ph_statebytes() #0 {
entry:
  ret i32 208
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_bytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_seedbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_publickeybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_secretkeybytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_messagebytes_max() #0 {
entry:
  ret i32 -65
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_sk_to_seed(i8* nonnull %seed, i8* nonnull %sk) #0 {
entry:
  %seed.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %seed, i8** %seed.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %seed.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %1, i32 32, i1 false)
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_sk_to_pk(i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %add.ptr, i32 32, i1 false)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519ph_init(%struct.crypto_sign_ed25519ph_state* nonnull %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %hs = getelementptr inbounds %struct.crypto_sign_ed25519ph_state, %struct.crypto_sign_ed25519ph_state* %0, i32 0, i32 0
  %call = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %hs)
  ret i32 0
}

declare i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state*) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519ph_update(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* %m, i64 %mlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %hs = getelementptr inbounds %struct.crypto_sign_ed25519ph_state, %struct.crypto_sign_ed25519ph_state* %0, i32 0, i32 0
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %call = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %1, i64 %2)
  ret i32 %call
}

declare i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state*, i8*, i64) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519ph_final_create(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* nonnull %sig, i64* %siglen_p, i8* nonnull %sk) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %sig.addr = alloca i8*, align 4
  %siglen_p.addr = alloca i64*, align 4
  %sk.addr = alloca i8*, align 4
  %ph = alloca [64 x i8], align 16
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i64* %siglen_p, i64** %siglen_p.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %hs = getelementptr inbounds %struct.crypto_sign_ed25519ph_state, %struct.crypto_sign_ed25519ph_state* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %ph, i32 0, i32 0
  %call = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay)
  %1 = load i8*, i8** %sig.addr, align 4
  %2 = load i64*, i64** %siglen_p.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %ph, i32 0, i32 0
  %3 = load i8*, i8** %sk.addr, align 4
  %call2 = call i32 @_crypto_sign_ed25519_detached(i8* %1, i64* %2, i8* %arraydecay1, i64 64, i8* %3, i32 1)
  ret i32 %call2
}

declare i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state*, i8*) #2

declare i32 @_crypto_sign_ed25519_detached(i8*, i64*, i8*, i64, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519ph_final_verify(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* nonnull %sig, i8* nonnull %pk) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %sig.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  %ph = alloca [64 x i8], align 16
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %hs = getelementptr inbounds %struct.crypto_sign_ed25519ph_state, %struct.crypto_sign_ed25519ph_state* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %ph, i32 0, i32 0
  %call = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay)
  %1 = load i8*, i8** %sig.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %ph, i32 0, i32 0
  %2 = load i8*, i8** %pk.addr, align 4
  %call2 = call i32 @_crypto_sign_ed25519_verify_detached(i8* %1, i8* %arraydecay1, i64 64, i8* %2, i32 1)
  ret i32 %call2
}

declare i32 @_crypto_sign_ed25519_verify_detached(i8*, i8*, i64, i8*, i32) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
