; ModuleID = 'crypto_pwhash/scryptsalsa208sha256/pbkdf2-sha256.c'
source_filename = "crypto_pwhash/scryptsalsa208sha256/pbkdf2-sha256.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_auth_hmacsha256_state = type { %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state }
%struct.crypto_hash_sha256_state = type { [8 x i32], i64, [64 x i8] }

; Function Attrs: noinline nounwind optnone
define hidden void @escrypt_PBKDF2_SHA256(i8* %passwd, i32 %passwdlen, i8* %salt, i32 %saltlen, i64 %c, i8* %buf, i32 %dkLen) #0 {
entry:
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %c.addr = alloca i64, align 8
  %buf.addr = alloca i8*, align 4
  %dkLen.addr = alloca i32, align 4
  %PShctx = alloca %struct.crypto_auth_hmacsha256_state, align 8
  %hctx = alloca %struct.crypto_auth_hmacsha256_state, align 8
  %i = alloca i32, align 4
  %ivec = alloca [4 x i8], align 1
  %U = alloca [32 x i8], align 16
  %T = alloca [32 x i8], align 16
  %j = alloca i64, align 8
  %k = alloca i32, align 4
  %clen = alloca i32, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i32 %passwdlen, i32* %passwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i64 %c, i64* %c.addr, align 8
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %dkLen, i32* %dkLen.addr, align 4
  %0 = load i8*, i8** %passwd.addr, align 4
  %1 = load i32, i32* %passwdlen.addr, align 4
  %call = call i32 @crypto_auth_hmacsha256_init(%struct.crypto_auth_hmacsha256_state* %PShctx, i8* %0, i32 %1)
  %2 = load i8*, i8** %salt.addr, align 4
  %3 = load i32, i32* %saltlen.addr, align 4
  %conv = zext i32 %3 to i64
  %call1 = call i32 @crypto_auth_hmacsha256_update(%struct.crypto_auth_hmacsha256_state* %PShctx, i8* %2, i64 %conv)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %4 = load i32, i32* %i, align 4
  %mul = mul i32 %4, 32
  %5 = load i32, i32* %dkLen.addr, align 4
  %cmp = icmp ult i32 %mul, %5
  br i1 %cmp, label %for.body, label %for.end37

for.body:                                         ; preds = %for.cond
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %ivec, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %add = add i32 %6, 1
  call void @store32_be(i8* %arraydecay, i32 %add)
  %7 = bitcast %struct.crypto_auth_hmacsha256_state* %hctx to i8*
  %8 = bitcast %struct.crypto_auth_hmacsha256_state* %PShctx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %7, i8* align 8 %8, i32 208, i1 false)
  %arraydecay3 = getelementptr inbounds [4 x i8], [4 x i8]* %ivec, i32 0, i32 0
  %call4 = call i32 @crypto_auth_hmacsha256_update(%struct.crypto_auth_hmacsha256_state* %hctx, i8* %arraydecay3, i64 4)
  %arraydecay5 = getelementptr inbounds [32 x i8], [32 x i8]* %U, i32 0, i32 0
  %call6 = call i32 @crypto_auth_hmacsha256_final(%struct.crypto_auth_hmacsha256_state* %hctx, i8* %arraydecay5)
  %arraydecay7 = getelementptr inbounds [32 x i8], [32 x i8]* %T, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [32 x i8], [32 x i8]* %U, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay7, i8* align 16 %arraydecay8, i32 32, i1 false)
  store i64 2, i64* %j, align 8
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc26, %for.body
  %9 = load i64, i64* %j, align 8
  %10 = load i64, i64* %c.addr, align 8
  %cmp10 = icmp ule i64 %9, %10
  br i1 %cmp10, label %for.body12, label %for.end28

for.body12:                                       ; preds = %for.cond9
  %11 = load i8*, i8** %passwd.addr, align 4
  %12 = load i32, i32* %passwdlen.addr, align 4
  %call13 = call i32 @crypto_auth_hmacsha256_init(%struct.crypto_auth_hmacsha256_state* %hctx, i8* %11, i32 %12)
  %arraydecay14 = getelementptr inbounds [32 x i8], [32 x i8]* %U, i32 0, i32 0
  %call15 = call i32 @crypto_auth_hmacsha256_update(%struct.crypto_auth_hmacsha256_state* %hctx, i8* %arraydecay14, i64 32)
  %arraydecay16 = getelementptr inbounds [32 x i8], [32 x i8]* %U, i32 0, i32 0
  %call17 = call i32 @crypto_auth_hmacsha256_final(%struct.crypto_auth_hmacsha256_state* %hctx, i8* %arraydecay16)
  store i32 0, i32* %k, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body12
  %13 = load i32, i32* %k, align 4
  %cmp19 = icmp slt i32 %13, 32
  br i1 %cmp19, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond18
  %14 = load i32, i32* %k, align 4
  %arrayidx = getelementptr [32 x i8], [32 x i8]* %U, i32 0, i32 %14
  %15 = load i8, i8* %arrayidx, align 1
  %conv22 = zext i8 %15 to i32
  %16 = load i32, i32* %k, align 4
  %arrayidx23 = getelementptr [32 x i8], [32 x i8]* %T, i32 0, i32 %16
  %17 = load i8, i8* %arrayidx23, align 1
  %conv24 = zext i8 %17 to i32
  %xor = xor i32 %conv24, %conv22
  %conv25 = trunc i32 %xor to i8
  store i8 %conv25, i8* %arrayidx23, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body21
  %18 = load i32, i32* %k, align 4
  %inc = add i32 %18, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  br label %for.inc26

for.inc26:                                        ; preds = %for.end
  %19 = load i64, i64* %j, align 8
  %inc27 = add i64 %19, 1
  store i64 %inc27, i64* %j, align 8
  br label %for.cond9

for.end28:                                        ; preds = %for.cond9
  %20 = load i32, i32* %dkLen.addr, align 4
  %21 = load i32, i32* %i, align 4
  %mul29 = mul i32 %21, 32
  %sub = sub i32 %20, %mul29
  store i32 %sub, i32* %clen, align 4
  %22 = load i32, i32* %clen, align 4
  %cmp30 = icmp ugt i32 %22, 32
  br i1 %cmp30, label %if.then, label %if.end

if.then:                                          ; preds = %for.end28
  store i32 32, i32* %clen, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end28
  %23 = load i8*, i8** %buf.addr, align 4
  %24 = load i32, i32* %i, align 4
  %mul32 = mul i32 %24, 32
  %arrayidx33 = getelementptr i8, i8* %23, i32 %mul32
  %arraydecay34 = getelementptr inbounds [32 x i8], [32 x i8]* %T, i32 0, i32 0
  %25 = load i32, i32* %clen, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx33, i8* align 16 %arraydecay34, i32 %25, i1 false)
  br label %for.inc35

for.inc35:                                        ; preds = %if.end
  %26 = load i32, i32* %i, align 4
  %inc36 = add i32 %26, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond

for.end37:                                        ; preds = %for.cond
  %27 = bitcast %struct.crypto_auth_hmacsha256_state* %PShctx to i8*
  call void @sodium_memzero(i8* %27, i32 208)
  ret void
}

declare i32 @crypto_auth_hmacsha256_init(%struct.crypto_auth_hmacsha256_state*, i8*, i32) #1

declare i32 @crypto_auth_hmacsha256_update(%struct.crypto_auth_hmacsha256_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define internal void @store32_be(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i32, i32* %w.addr, align 4
  %conv = trunc i32 %0 to i8
  %1 = load i8*, i8** %dst.addr, align 4
  %arrayidx = getelementptr i8, i8* %1, i32 3
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i32, i32* %w.addr, align 4
  %shr = lshr i32 %2, 8
  store i32 %shr, i32* %w.addr, align 4
  %3 = load i32, i32* %w.addr, align 4
  %conv1 = trunc i32 %3 to i8
  %4 = load i8*, i8** %dst.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %4, i32 2
  store i8 %conv1, i8* %arrayidx2, align 1
  %5 = load i32, i32* %w.addr, align 4
  %shr3 = lshr i32 %5, 8
  store i32 %shr3, i32* %w.addr, align 4
  %6 = load i32, i32* %w.addr, align 4
  %conv4 = trunc i32 %6 to i8
  %7 = load i8*, i8** %dst.addr, align 4
  %arrayidx5 = getelementptr i8, i8* %7, i32 1
  store i8 %conv4, i8* %arrayidx5, align 1
  %8 = load i32, i32* %w.addr, align 4
  %shr6 = lshr i32 %8, 8
  store i32 %shr6, i32* %w.addr, align 4
  %9 = load i32, i32* %w.addr, align 4
  %conv7 = trunc i32 %9 to i8
  %10 = load i8*, i8** %dst.addr, align 4
  %arrayidx8 = getelementptr i8, i8* %10, i32 0
  store i8 %conv7, i8* %arrayidx8, align 1
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare i32 @crypto_auth_hmacsha256_final(%struct.crypto_auth_hmacsha256_state*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
