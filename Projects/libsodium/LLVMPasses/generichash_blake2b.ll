; ModuleID = 'crypto_generichash/blake2b/ref/generichash_blake2b.c'
source_filename = "crypto_generichash/blake2b/ref/generichash_blake2b.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_generichash_blake2b_state = type { [384 x i8] }
%struct.blake2b_state = type <{ [8 x i64], [2 x i64], [2 x i64], [256 x i8], i32, i8 }>

@.str = private unnamed_addr constant [20 x i8] c"outlen <= UINT8_MAX\00", align 1
@.str.1 = private unnamed_addr constant [53 x i8] c"crypto_generichash/blake2b/ref/generichash_blake2b.c\00", align 1
@__func__.crypto_generichash_blake2b = private unnamed_addr constant [27 x i8] c"crypto_generichash_blake2b\00", align 1
@.str.2 = private unnamed_addr constant [20 x i8] c"keylen <= UINT8_MAX\00", align 1
@__func__.crypto_generichash_blake2b_salt_personal = private unnamed_addr constant [41 x i8] c"crypto_generichash_blake2b_salt_personal\00", align 1
@__func__.crypto_generichash_blake2b_init = private unnamed_addr constant [32 x i8] c"crypto_generichash_blake2b_init\00", align 1
@__func__.crypto_generichash_blake2b_init_salt_personal = private unnamed_addr constant [46 x i8] c"crypto_generichash_blake2b_init_salt_personal\00", align 1
@__func__.crypto_generichash_blake2b_final = private unnamed_addr constant [33 x i8] c"crypto_generichash_blake2b_final\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b(i8* nonnull %out, i32 %outlen, i8* %in, i64 %inlen, i8* %key, i32 %keylen) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ule i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 64
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %2 = load i32, i32* %keylen.addr, align 4
  %cmp3 = icmp ugt i32 %2, 64
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %3 = load i64, i64* %inlen.addr, align 8
  %cmp5 = icmp ugt i64 %3, -1
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false4
  %4 = load i32, i32* %outlen.addr, align 4
  %cmp6 = icmp ule i32 %4, 255
  br i1 %cmp6, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 20, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.crypto_generichash_blake2b, i32 0, i32 0)) #3
  unreachable

5:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %5, %if.end
  %6 = phi i1 [ true, %if.end ], [ false, %5 ]
  %lor.ext = zext i1 %6 to i32
  %7 = load i32, i32* %keylen.addr, align 4
  %cmp7 = icmp ule i32 %7, 255
  br i1 %cmp7, label %lor.end9, label %lor.rhs8

lor.rhs8:                                         ; preds = %lor.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 21, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @__func__.crypto_generichash_blake2b, i32 0, i32 0)) #3
  unreachable

8:                                                ; No predecessors!
  br label %lor.end9

lor.end9:                                         ; preds = %8, %lor.end
  %9 = phi i1 [ true, %lor.end ], [ false, %8 ]
  %lor.ext10 = zext i1 %9 to i32
  %10 = load i8*, i8** %out.addr, align 4
  %11 = load i8*, i8** %in.addr, align 4
  %12 = load i8*, i8** %key.addr, align 4
  %13 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %13 to i8
  %14 = load i64, i64* %inlen.addr, align 8
  %15 = load i32, i32* %keylen.addr, align 4
  %conv11 = trunc i32 %15 to i8
  %call = call i32 @blake2b(i8* %10, i8* %11, i8* %12, i8 zeroext %conv, i64 %14, i8 zeroext %conv11)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %lor.end9, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #1

declare i32 @blake2b(i8*, i8*, i8*, i8 zeroext, i64, i8 zeroext) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b_salt_personal(i8* nonnull %out, i32 %outlen, i8* %in, i64 %inlen, i8* %key, i32 %keylen, i8* %salt, i8* %personal) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %personal.addr = alloca i8*, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ule i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 64
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %2 = load i32, i32* %keylen.addr, align 4
  %cmp3 = icmp ugt i32 %2, 64
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false2
  %3 = load i64, i64* %inlen.addr, align 8
  %cmp5 = icmp ugt i64 %3, -1
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false4
  %4 = load i32, i32* %outlen.addr, align 4
  %cmp6 = icmp ule i32 %4, 255
  br i1 %cmp6, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 37, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @__func__.crypto_generichash_blake2b_salt_personal, i32 0, i32 0)) #3
  unreachable

5:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %5, %if.end
  %6 = phi i1 [ true, %if.end ], [ false, %5 ]
  %lor.ext = zext i1 %6 to i32
  %7 = load i32, i32* %keylen.addr, align 4
  %cmp7 = icmp ule i32 %7, 255
  br i1 %cmp7, label %lor.end9, label %lor.rhs8

lor.rhs8:                                         ; preds = %lor.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 38, i8* getelementptr inbounds ([41 x i8], [41 x i8]* @__func__.crypto_generichash_blake2b_salt_personal, i32 0, i32 0)) #3
  unreachable

8:                                                ; No predecessors!
  br label %lor.end9

lor.end9:                                         ; preds = %8, %lor.end
  %9 = phi i1 [ true, %lor.end ], [ false, %8 ]
  %lor.ext10 = zext i1 %9 to i32
  %10 = load i8*, i8** %out.addr, align 4
  %11 = load i8*, i8** %in.addr, align 4
  %12 = load i8*, i8** %key.addr, align 4
  %13 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %13 to i8
  %14 = load i64, i64* %inlen.addr, align 8
  %15 = load i32, i32* %keylen.addr, align 4
  %conv11 = trunc i32 %15 to i8
  %16 = load i8*, i8** %salt.addr, align 4
  %17 = load i8*, i8** %personal.addr, align 4
  %call = call i32 @blake2b_salt_personal(i8* %10, i8* %11, i8* %12, i8 zeroext %conv, i64 %14, i8 zeroext %conv11, i8* %16, i8* %17)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %lor.end9, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

declare i32 @blake2b_salt_personal(i8*, i8*, i8*, i8 zeroext, i64, i8 zeroext, i8*, i8*) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* %key, i32 %keylen, i32 %outlen) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %outlen.addr = alloca i32, align 4
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ule i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 64
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %2 = load i32, i32* %keylen.addr, align 4
  %cmp3 = icmp ugt i32 %2, 64
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false2
  %3 = load i32, i32* %outlen.addr, align 4
  %cmp4 = icmp ule i32 %3, 255
  br i1 %cmp4, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 54, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @__func__.crypto_generichash_blake2b_init, i32 0, i32 0)) #3
  unreachable

4:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %4, %if.end
  %5 = phi i1 [ true, %if.end ], [ false, %4 ]
  %lor.ext = zext i1 %5 to i32
  %6 = load i32, i32* %keylen.addr, align 4
  %cmp5 = icmp ule i32 %6, 255
  br i1 %cmp5, label %lor.end7, label %lor.rhs6

lor.rhs6:                                         ; preds = %lor.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 55, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @__func__.crypto_generichash_blake2b_init, i32 0, i32 0)) #3
  unreachable

7:                                                ; No predecessors!
  br label %lor.end7

lor.end7:                                         ; preds = %7, %lor.end
  %8 = phi i1 [ true, %lor.end ], [ false, %7 ]
  %lor.ext8 = zext i1 %8 to i32
  %9 = load i8*, i8** %key.addr, align 4
  %cmp9 = icmp eq i8* %9, null
  br i1 %cmp9, label %if.then12, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.end7
  %10 = load i32, i32* %keylen.addr, align 4
  %cmp11 = icmp ule i32 %10, 0
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %lor.lhs.false10, %lor.end7
  %11 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %12 = bitcast %struct.crypto_generichash_blake2b_state* %11 to i8*
  %13 = bitcast i8* %12 to %struct.blake2b_state*
  %14 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %14 to i8
  %call = call i32 @blake2b_init(%struct.blake2b_state* %13, i8 zeroext %conv)
  %cmp13 = icmp ne i32 %call, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.then12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.then12
  br label %if.end24

if.else:                                          ; preds = %lor.lhs.false10
  %15 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %16 = bitcast %struct.crypto_generichash_blake2b_state* %15 to i8*
  %17 = bitcast i8* %16 to %struct.blake2b_state*
  %18 = load i32, i32* %outlen.addr, align 4
  %conv17 = trunc i32 %18 to i8
  %19 = load i8*, i8** %key.addr, align 4
  %20 = load i32, i32* %keylen.addr, align 4
  %conv18 = trunc i32 %20 to i8
  %call19 = call i32 @blake2b_init_key(%struct.blake2b_state* %17, i8 zeroext %conv17, i8* %19, i8 zeroext %conv18)
  %cmp20 = icmp ne i32 %call19, 0
  br i1 %cmp20, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.else
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.end16
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end24, %if.then22, %if.then15, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

declare i32 @blake2b_init(%struct.blake2b_state*, i8 zeroext) #2

declare i32 @blake2b_init_key(%struct.blake2b_state*, i8 zeroext, i8*, i8 zeroext) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b_init_salt_personal(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* %key, i32 %keylen, i32 %outlen, i8* %salt, i8* %personal) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %outlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %personal.addr = alloca i8*, align 4
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i8* %personal, i8** %personal.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ule i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %outlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, 64
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %2 = load i32, i32* %keylen.addr, align 4
  %cmp3 = icmp ugt i32 %2, 64
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false2
  %3 = load i32, i32* %outlen.addr, align 4
  %cmp4 = icmp ule i32 %3, 255
  br i1 %cmp4, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 78, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @__func__.crypto_generichash_blake2b_init_salt_personal, i32 0, i32 0)) #3
  unreachable

4:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %4, %if.end
  %5 = phi i1 [ true, %if.end ], [ false, %4 ]
  %lor.ext = zext i1 %5 to i32
  %6 = load i32, i32* %keylen.addr, align 4
  %cmp5 = icmp ule i32 %6, 255
  br i1 %cmp5, label %lor.end7, label %lor.rhs6

lor.rhs6:                                         ; preds = %lor.end
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.2, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 79, i8* getelementptr inbounds ([46 x i8], [46 x i8]* @__func__.crypto_generichash_blake2b_init_salt_personal, i32 0, i32 0)) #3
  unreachable

7:                                                ; No predecessors!
  br label %lor.end7

lor.end7:                                         ; preds = %7, %lor.end
  %8 = phi i1 [ true, %lor.end ], [ false, %7 ]
  %lor.ext8 = zext i1 %8 to i32
  %9 = load i8*, i8** %key.addr, align 4
  %cmp9 = icmp eq i8* %9, null
  br i1 %cmp9, label %if.then12, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.end7
  %10 = load i32, i32* %keylen.addr, align 4
  %cmp11 = icmp ule i32 %10, 0
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %lor.lhs.false10, %lor.end7
  %11 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %12 = bitcast %struct.crypto_generichash_blake2b_state* %11 to i8*
  %13 = bitcast i8* %12 to %struct.blake2b_state*
  %14 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %14 to i8
  %15 = load i8*, i8** %salt.addr, align 4
  %16 = load i8*, i8** %personal.addr, align 4
  %call = call i32 @blake2b_init_salt_personal(%struct.blake2b_state* %13, i8 zeroext %conv, i8* %15, i8* %16)
  %cmp13 = icmp ne i32 %call, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.then12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.then12
  br label %if.end24

if.else:                                          ; preds = %lor.lhs.false10
  %17 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %18 = bitcast %struct.crypto_generichash_blake2b_state* %17 to i8*
  %19 = bitcast i8* %18 to %struct.blake2b_state*
  %20 = load i32, i32* %outlen.addr, align 4
  %conv17 = trunc i32 %20 to i8
  %21 = load i8*, i8** %key.addr, align 4
  %22 = load i32, i32* %keylen.addr, align 4
  %conv18 = trunc i32 %22 to i8
  %23 = load i8*, i8** %salt.addr, align 4
  %24 = load i8*, i8** %personal.addr, align 4
  %call19 = call i32 @blake2b_init_key_salt_personal(%struct.blake2b_state* %19, i8 zeroext %conv17, i8* %21, i8 zeroext %conv18, i8* %23, i8* %24)
  %cmp20 = icmp ne i32 %call19, 0
  br i1 %cmp20, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.else
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.end16
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end24, %if.then22, %if.then15, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

declare i32 @blake2b_init_salt_personal(%struct.blake2b_state*, i8 zeroext, i8*, i8*) #2

declare i32 @blake2b_init_key_salt_personal(%struct.blake2b_state*, i8 zeroext, i8*, i8 zeroext, i8*, i8*) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %1 = bitcast %struct.crypto_generichash_blake2b_state* %0 to i8*
  %2 = bitcast i8* %1 to %struct.blake2b_state*
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  %call = call i32 @blake2b_update(%struct.blake2b_state* %2, i8* %3, i64 %4)
  ret i32 %call
}

declare i32 @blake2b_update(%struct.blake2b_state*, i8*, i64) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state* nonnull %state, i8* nonnull %out, i32 %outlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_generichash_blake2b_state*, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i32, align 4
  store %struct.crypto_generichash_blake2b_state* %state, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i32 %outlen, i32* %outlen.addr, align 4
  %0 = load i32, i32* %outlen.addr, align 4
  %cmp = icmp ule i32 %0, 255
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0), i32 107, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @__func__.crypto_generichash_blake2b_final, i32 0, i32 0)) #3
  unreachable

1:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %1, %entry
  %2 = phi i1 [ true, %entry ], [ false, %1 ]
  %lor.ext = zext i1 %2 to i32
  %3 = load %struct.crypto_generichash_blake2b_state*, %struct.crypto_generichash_blake2b_state** %state.addr, align 4
  %4 = bitcast %struct.crypto_generichash_blake2b_state* %3 to i8*
  %5 = bitcast i8* %4 to %struct.blake2b_state*
  %6 = load i8*, i8** %out.addr, align 4
  %7 = load i32, i32* %outlen.addr, align 4
  %conv = trunc i32 %7 to i8
  %call = call i32 @blake2b_final(%struct.blake2b_state* %5, i8* %6, i8 zeroext %conv)
  ret i32 %call
}

declare i32 @blake2b_final(%struct.blake2b_state*, i8*, i8 zeroext) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_generichash_blake2b_pick_best_implementation() #0 {
entry:
  %call = call i32 @blake2b_pick_best_implementation()
  ret i32 %call
}

declare i32 @blake2b_pick_best_implementation() #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
