; ModuleID = 'crypto_scalarmult/ristretto255/ref10/scalarmult_ristretto255_ref10.c'
source_filename = "crypto_scalarmult/ristretto255/ref10/scalarmult_ristretto255_ref10.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ristretto255(i8* nonnull %q, i8* nonnull %n, i8* nonnull %p) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %t = alloca i8*, align 4
  %Q = alloca %struct.ge25519_p3, align 4
  %P = alloca %struct.ge25519_p3, align 4
  %i = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  %1 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %P, i8* %1)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4
  %cmp1 = icmp ult i32 %2, 32
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 %4
  %5 = load i8, i8* %arrayidx, align 1
  %6 = load i8*, i8** %t, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr i8, i8* %6, i32 %7
  store i8 %5, i8* %arrayidx2, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i8*, i8** %t, align 4
  %arrayidx3 = getelementptr i8, i8* %9, i32 31
  %10 = load i8, i8* %arrayidx3, align 1
  %conv = zext i8 %10 to i32
  %and = and i32 %conv, 127
  %conv4 = trunc i32 %and to i8
  store i8 %conv4, i8* %arrayidx3, align 1
  %11 = load i8*, i8** %t, align 4
  call void @ge25519_scalarmult(%struct.ge25519_p3* %Q, i8* %11, %struct.ge25519_p3* %P)
  %12 = load i8*, i8** %q.addr, align 4
  call void @ristretto255_p3_tobytes(i8* %12, %struct.ge25519_p3* %Q)
  %13 = load i8*, i8** %q.addr, align 4
  %call5 = call i32 @sodium_is_zero(i8* %13, i32 32)
  %tobool = icmp ne i32 %call5, 0
  br i1 %tobool, label %if.then6, label %if.end7

if.then6:                                         ; preds = %for.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %for.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end7, %if.then6, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i32 @ristretto255_frombytes(%struct.ge25519_p3*, i8*) #1

declare void @ge25519_scalarmult(%struct.ge25519_p3*, i8*, %struct.ge25519_p3*) #1

declare void @ristretto255_p3_tobytes(i8*, %struct.ge25519_p3*) #1

declare i32 @sodium_is_zero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ristretto255_base(i8* nonnull %q, i8* nonnull %n) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %t = alloca i8*, align 4
  %Q = alloca %struct.ge25519_p3, align 4
  %i = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i8*, i8** %t, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %t, align 4
  %arrayidx2 = getelementptr i8, i8* %8, i32 31
  %9 = load i8, i8* %arrayidx2, align 1
  %conv = zext i8 %9 to i32
  %and = and i32 %conv, 127
  %conv3 = trunc i32 %and to i8
  store i8 %conv3, i8* %arrayidx2, align 1
  %10 = load i8*, i8** %t, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %Q, i8* %10)
  %11 = load i8*, i8** %q.addr, align 4
  call void @ristretto255_p3_tobytes(i8* %11, %struct.ge25519_p3* %Q)
  %12 = load i8*, i8** %q.addr, align 4
  %call = call i32 @sodium_is_zero(i8* %12, i32 32)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ristretto255_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ristretto255_scalarbytes() #0 {
entry:
  ret i32 32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
