; ModuleID = 'crypto_box/crypto_box_easy.c'
source_filename = "crypto_box/crypto_box_easy.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_detached_afternm(i8* nonnull %c, i8* nonnull %mac, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %c.addr, align 4
  %1 = load i8*, i8** %mac.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %n.addr, align 4
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_detached(i8* %0, i8* %1, i8* %2, i64 %3, i8* %4, i8* %5)
  ret i32 %call
}

declare i32 @crypto_secretbox_detached(i8*, i8*, i8*, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_detached(i8* nonnull %c, i8* nonnull %mac, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %k = alloca [32 x i8], align 16
  %ret = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_box_beforenm(i8* %arraydecay, i8* %0, i8* %1)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i8*, i8** %mac.addr, align 4
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %call2 = call i32 @crypto_box_detached_afternm(i8* %2, i8* %3, i8* %4, i64 %5, i8* %6, i8* %arraydecay1)
  store i32 %call2, i32* %ret, align 4
  %arraydecay3 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 32)
  %7 = load i32, i32* %ret, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @crypto_box_beforenm(i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_easy_afternm(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967279
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 16
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i8*, i8** %m.addr, align 4
  %4 = load i64, i64* %mlen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_box_detached_afternm(i8* %add.ptr, i8* %2, i8* %3, i64 %4, i8* %5, i8* %6)
  ret i32 %call
}

; Function Attrs: noreturn
declare void @sodium_misuse() #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_easy(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967279
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 16
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i8*, i8** %m.addr, align 4
  %4 = load i64, i64* %mlen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %pk.addr, align 4
  %7 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_box_detached(i8* %add.ptr, i8* %2, i8* %3, i64 %4, i8* %5, i8* %6, i8* %7)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_open_detached_afternm(i8* %m, i8* nonnull %c, i8* nonnull %mac, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %m.addr, align 4
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %mac.addr, align 4
  %3 = load i64, i64* %clen.addr, align 8
  %4 = load i8*, i8** %n.addr, align 4
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_open_detached(i8* %0, i8* %1, i8* %2, i64 %3, i8* %4, i8* %5)
  ret i32 %call
}

declare i32 @crypto_secretbox_open_detached(i8*, i8*, i8*, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_open_detached(i8* %m, i8* nonnull %c, i8* nonnull %mac, i64 %clen, i8* nonnull %n, i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %k = alloca [32 x i8], align 16
  %ret = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_box_beforenm(i8* %arraydecay, i8* %0, i8* %1)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i8*, i8** %mac.addr, align 4
  %5 = load i64, i64* %clen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %call2 = call i32 @crypto_box_open_detached_afternm(i8* %2, i8* %3, i8* %4, i64 %5, i8* %6, i8* %arraydecay1)
  store i32 %call2, i32* %ret, align 4
  %arraydecay3 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 32)
  %7 = load i32, i32* %ret, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_open_easy_afternm(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ult i64 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %2, i32 16
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %4, 16
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_box_open_detached_afternm(i8* %1, i8* %add.ptr, i8* %3, i64 %sub, i8* %5, i8* %6)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_open_easy(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ult i64 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %2, i32 16
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %4, 16
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %pk.addr, align 4
  %7 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_box_open_detached(i8* %1, i8* %add.ptr, i8* %3, i64 %sub, i8* %5, i8* %6, i8* %7)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
