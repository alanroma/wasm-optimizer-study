; ModuleID = 'crypto_core/hsalsa20/ref2/core_hsalsa20_ref2.c'
source_filename = "crypto_core/hsalsa20/ref2/core_hsalsa20_ref2.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_hsalsa20(i8* nonnull %out, i8* nonnull %in, i8* nonnull %k, i8* %c) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x3 = alloca i32, align 4
  %x4 = alloca i32, align 4
  %x5 = alloca i32, align 4
  %x6 = alloca i32, align 4
  %x7 = alloca i32, align 4
  %x8 = alloca i32, align 4
  %x9 = alloca i32, align 4
  %x10 = alloca i32, align 4
  %x11 = alloca i32, align 4
  %x12 = alloca i32, align 4
  %x13 = alloca i32, align 4
  %x14 = alloca i32, align 4
  %x15 = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  %0 = load i8*, i8** %c.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 1634760805, i32* %x0, align 4
  store i32 857760878, i32* %x5, align 4
  store i32 2036477234, i32* %x10, align 4
  store i32 1797285236, i32* %x15, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 0
  %call = call i32 @load32_le(i8* %add.ptr)
  store i32 %call, i32* %x0, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %add.ptr1 = getelementptr i8, i8* %2, i32 4
  %call2 = call i32 @load32_le(i8* %add.ptr1)
  store i32 %call2, i32* %x5, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %add.ptr3 = getelementptr i8, i8* %3, i32 8
  %call4 = call i32 @load32_le(i8* %add.ptr3)
  store i32 %call4, i32* %x10, align 4
  %4 = load i8*, i8** %c.addr, align 4
  %add.ptr5 = getelementptr i8, i8* %4, i32 12
  %call6 = call i32 @load32_le(i8* %add.ptr5)
  store i32 %call6, i32* %x15, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %5 = load i8*, i8** %k.addr, align 4
  %add.ptr7 = getelementptr i8, i8* %5, i32 0
  %call8 = call i32 @load32_le(i8* %add.ptr7)
  store i32 %call8, i32* %x1, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %add.ptr9 = getelementptr i8, i8* %6, i32 4
  %call10 = call i32 @load32_le(i8* %add.ptr9)
  store i32 %call10, i32* %x2, align 4
  %7 = load i8*, i8** %k.addr, align 4
  %add.ptr11 = getelementptr i8, i8* %7, i32 8
  %call12 = call i32 @load32_le(i8* %add.ptr11)
  store i32 %call12, i32* %x3, align 4
  %8 = load i8*, i8** %k.addr, align 4
  %add.ptr13 = getelementptr i8, i8* %8, i32 12
  %call14 = call i32 @load32_le(i8* %add.ptr13)
  store i32 %call14, i32* %x4, align 4
  %9 = load i8*, i8** %k.addr, align 4
  %add.ptr15 = getelementptr i8, i8* %9, i32 16
  %call16 = call i32 @load32_le(i8* %add.ptr15)
  store i32 %call16, i32* %x11, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %add.ptr17 = getelementptr i8, i8* %10, i32 20
  %call18 = call i32 @load32_le(i8* %add.ptr17)
  store i32 %call18, i32* %x12, align 4
  %11 = load i8*, i8** %k.addr, align 4
  %add.ptr19 = getelementptr i8, i8* %11, i32 24
  %call20 = call i32 @load32_le(i8* %add.ptr19)
  store i32 %call20, i32* %x13, align 4
  %12 = load i8*, i8** %k.addr, align 4
  %add.ptr21 = getelementptr i8, i8* %12, i32 28
  %call22 = call i32 @load32_le(i8* %add.ptr21)
  store i32 %call22, i32* %x14, align 4
  %13 = load i8*, i8** %in.addr, align 4
  %add.ptr23 = getelementptr i8, i8* %13, i32 0
  %call24 = call i32 @load32_le(i8* %add.ptr23)
  store i32 %call24, i32* %x6, align 4
  %14 = load i8*, i8** %in.addr, align 4
  %add.ptr25 = getelementptr i8, i8* %14, i32 4
  %call26 = call i32 @load32_le(i8* %add.ptr25)
  store i32 %call26, i32* %x7, align 4
  %15 = load i8*, i8** %in.addr, align 4
  %add.ptr27 = getelementptr i8, i8* %15, i32 8
  %call28 = call i32 @load32_le(i8* %add.ptr27)
  store i32 %call28, i32* %x8, align 4
  %16 = load i8*, i8** %in.addr, align 4
  %add.ptr29 = getelementptr i8, i8* %16, i32 12
  %call30 = call i32 @load32_le(i8* %add.ptr29)
  store i32 %call30, i32* %x9, align 4
  store i32 20, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %i, align 4
  %cmp31 = icmp sgt i32 %17, 0
  br i1 %cmp31, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i32, i32* %x0, align 4
  %19 = load i32, i32* %x12, align 4
  %add = add i32 %18, %19
  %call32 = call i32 @rotl32(i32 %add, i32 7)
  %20 = load i32, i32* %x4, align 4
  %xor = xor i32 %20, %call32
  store i32 %xor, i32* %x4, align 4
  %21 = load i32, i32* %x4, align 4
  %22 = load i32, i32* %x0, align 4
  %add33 = add i32 %21, %22
  %call34 = call i32 @rotl32(i32 %add33, i32 9)
  %23 = load i32, i32* %x8, align 4
  %xor35 = xor i32 %23, %call34
  store i32 %xor35, i32* %x8, align 4
  %24 = load i32, i32* %x8, align 4
  %25 = load i32, i32* %x4, align 4
  %add36 = add i32 %24, %25
  %call37 = call i32 @rotl32(i32 %add36, i32 13)
  %26 = load i32, i32* %x12, align 4
  %xor38 = xor i32 %26, %call37
  store i32 %xor38, i32* %x12, align 4
  %27 = load i32, i32* %x12, align 4
  %28 = load i32, i32* %x8, align 4
  %add39 = add i32 %27, %28
  %call40 = call i32 @rotl32(i32 %add39, i32 18)
  %29 = load i32, i32* %x0, align 4
  %xor41 = xor i32 %29, %call40
  store i32 %xor41, i32* %x0, align 4
  %30 = load i32, i32* %x5, align 4
  %31 = load i32, i32* %x1, align 4
  %add42 = add i32 %30, %31
  %call43 = call i32 @rotl32(i32 %add42, i32 7)
  %32 = load i32, i32* %x9, align 4
  %xor44 = xor i32 %32, %call43
  store i32 %xor44, i32* %x9, align 4
  %33 = load i32, i32* %x9, align 4
  %34 = load i32, i32* %x5, align 4
  %add45 = add i32 %33, %34
  %call46 = call i32 @rotl32(i32 %add45, i32 9)
  %35 = load i32, i32* %x13, align 4
  %xor47 = xor i32 %35, %call46
  store i32 %xor47, i32* %x13, align 4
  %36 = load i32, i32* %x13, align 4
  %37 = load i32, i32* %x9, align 4
  %add48 = add i32 %36, %37
  %call49 = call i32 @rotl32(i32 %add48, i32 13)
  %38 = load i32, i32* %x1, align 4
  %xor50 = xor i32 %38, %call49
  store i32 %xor50, i32* %x1, align 4
  %39 = load i32, i32* %x1, align 4
  %40 = load i32, i32* %x13, align 4
  %add51 = add i32 %39, %40
  %call52 = call i32 @rotl32(i32 %add51, i32 18)
  %41 = load i32, i32* %x5, align 4
  %xor53 = xor i32 %41, %call52
  store i32 %xor53, i32* %x5, align 4
  %42 = load i32, i32* %x10, align 4
  %43 = load i32, i32* %x6, align 4
  %add54 = add i32 %42, %43
  %call55 = call i32 @rotl32(i32 %add54, i32 7)
  %44 = load i32, i32* %x14, align 4
  %xor56 = xor i32 %44, %call55
  store i32 %xor56, i32* %x14, align 4
  %45 = load i32, i32* %x14, align 4
  %46 = load i32, i32* %x10, align 4
  %add57 = add i32 %45, %46
  %call58 = call i32 @rotl32(i32 %add57, i32 9)
  %47 = load i32, i32* %x2, align 4
  %xor59 = xor i32 %47, %call58
  store i32 %xor59, i32* %x2, align 4
  %48 = load i32, i32* %x2, align 4
  %49 = load i32, i32* %x14, align 4
  %add60 = add i32 %48, %49
  %call61 = call i32 @rotl32(i32 %add60, i32 13)
  %50 = load i32, i32* %x6, align 4
  %xor62 = xor i32 %50, %call61
  store i32 %xor62, i32* %x6, align 4
  %51 = load i32, i32* %x6, align 4
  %52 = load i32, i32* %x2, align 4
  %add63 = add i32 %51, %52
  %call64 = call i32 @rotl32(i32 %add63, i32 18)
  %53 = load i32, i32* %x10, align 4
  %xor65 = xor i32 %53, %call64
  store i32 %xor65, i32* %x10, align 4
  %54 = load i32, i32* %x15, align 4
  %55 = load i32, i32* %x11, align 4
  %add66 = add i32 %54, %55
  %call67 = call i32 @rotl32(i32 %add66, i32 7)
  %56 = load i32, i32* %x3, align 4
  %xor68 = xor i32 %56, %call67
  store i32 %xor68, i32* %x3, align 4
  %57 = load i32, i32* %x3, align 4
  %58 = load i32, i32* %x15, align 4
  %add69 = add i32 %57, %58
  %call70 = call i32 @rotl32(i32 %add69, i32 9)
  %59 = load i32, i32* %x7, align 4
  %xor71 = xor i32 %59, %call70
  store i32 %xor71, i32* %x7, align 4
  %60 = load i32, i32* %x7, align 4
  %61 = load i32, i32* %x3, align 4
  %add72 = add i32 %60, %61
  %call73 = call i32 @rotl32(i32 %add72, i32 13)
  %62 = load i32, i32* %x11, align 4
  %xor74 = xor i32 %62, %call73
  store i32 %xor74, i32* %x11, align 4
  %63 = load i32, i32* %x11, align 4
  %64 = load i32, i32* %x7, align 4
  %add75 = add i32 %63, %64
  %call76 = call i32 @rotl32(i32 %add75, i32 18)
  %65 = load i32, i32* %x15, align 4
  %xor77 = xor i32 %65, %call76
  store i32 %xor77, i32* %x15, align 4
  %66 = load i32, i32* %x0, align 4
  %67 = load i32, i32* %x3, align 4
  %add78 = add i32 %66, %67
  %call79 = call i32 @rotl32(i32 %add78, i32 7)
  %68 = load i32, i32* %x1, align 4
  %xor80 = xor i32 %68, %call79
  store i32 %xor80, i32* %x1, align 4
  %69 = load i32, i32* %x1, align 4
  %70 = load i32, i32* %x0, align 4
  %add81 = add i32 %69, %70
  %call82 = call i32 @rotl32(i32 %add81, i32 9)
  %71 = load i32, i32* %x2, align 4
  %xor83 = xor i32 %71, %call82
  store i32 %xor83, i32* %x2, align 4
  %72 = load i32, i32* %x2, align 4
  %73 = load i32, i32* %x1, align 4
  %add84 = add i32 %72, %73
  %call85 = call i32 @rotl32(i32 %add84, i32 13)
  %74 = load i32, i32* %x3, align 4
  %xor86 = xor i32 %74, %call85
  store i32 %xor86, i32* %x3, align 4
  %75 = load i32, i32* %x3, align 4
  %76 = load i32, i32* %x2, align 4
  %add87 = add i32 %75, %76
  %call88 = call i32 @rotl32(i32 %add87, i32 18)
  %77 = load i32, i32* %x0, align 4
  %xor89 = xor i32 %77, %call88
  store i32 %xor89, i32* %x0, align 4
  %78 = load i32, i32* %x5, align 4
  %79 = load i32, i32* %x4, align 4
  %add90 = add i32 %78, %79
  %call91 = call i32 @rotl32(i32 %add90, i32 7)
  %80 = load i32, i32* %x6, align 4
  %xor92 = xor i32 %80, %call91
  store i32 %xor92, i32* %x6, align 4
  %81 = load i32, i32* %x6, align 4
  %82 = load i32, i32* %x5, align 4
  %add93 = add i32 %81, %82
  %call94 = call i32 @rotl32(i32 %add93, i32 9)
  %83 = load i32, i32* %x7, align 4
  %xor95 = xor i32 %83, %call94
  store i32 %xor95, i32* %x7, align 4
  %84 = load i32, i32* %x7, align 4
  %85 = load i32, i32* %x6, align 4
  %add96 = add i32 %84, %85
  %call97 = call i32 @rotl32(i32 %add96, i32 13)
  %86 = load i32, i32* %x4, align 4
  %xor98 = xor i32 %86, %call97
  store i32 %xor98, i32* %x4, align 4
  %87 = load i32, i32* %x4, align 4
  %88 = load i32, i32* %x7, align 4
  %add99 = add i32 %87, %88
  %call100 = call i32 @rotl32(i32 %add99, i32 18)
  %89 = load i32, i32* %x5, align 4
  %xor101 = xor i32 %89, %call100
  store i32 %xor101, i32* %x5, align 4
  %90 = load i32, i32* %x10, align 4
  %91 = load i32, i32* %x9, align 4
  %add102 = add i32 %90, %91
  %call103 = call i32 @rotl32(i32 %add102, i32 7)
  %92 = load i32, i32* %x11, align 4
  %xor104 = xor i32 %92, %call103
  store i32 %xor104, i32* %x11, align 4
  %93 = load i32, i32* %x11, align 4
  %94 = load i32, i32* %x10, align 4
  %add105 = add i32 %93, %94
  %call106 = call i32 @rotl32(i32 %add105, i32 9)
  %95 = load i32, i32* %x8, align 4
  %xor107 = xor i32 %95, %call106
  store i32 %xor107, i32* %x8, align 4
  %96 = load i32, i32* %x8, align 4
  %97 = load i32, i32* %x11, align 4
  %add108 = add i32 %96, %97
  %call109 = call i32 @rotl32(i32 %add108, i32 13)
  %98 = load i32, i32* %x9, align 4
  %xor110 = xor i32 %98, %call109
  store i32 %xor110, i32* %x9, align 4
  %99 = load i32, i32* %x9, align 4
  %100 = load i32, i32* %x8, align 4
  %add111 = add i32 %99, %100
  %call112 = call i32 @rotl32(i32 %add111, i32 18)
  %101 = load i32, i32* %x10, align 4
  %xor113 = xor i32 %101, %call112
  store i32 %xor113, i32* %x10, align 4
  %102 = load i32, i32* %x15, align 4
  %103 = load i32, i32* %x14, align 4
  %add114 = add i32 %102, %103
  %call115 = call i32 @rotl32(i32 %add114, i32 7)
  %104 = load i32, i32* %x12, align 4
  %xor116 = xor i32 %104, %call115
  store i32 %xor116, i32* %x12, align 4
  %105 = load i32, i32* %x12, align 4
  %106 = load i32, i32* %x15, align 4
  %add117 = add i32 %105, %106
  %call118 = call i32 @rotl32(i32 %add117, i32 9)
  %107 = load i32, i32* %x13, align 4
  %xor119 = xor i32 %107, %call118
  store i32 %xor119, i32* %x13, align 4
  %108 = load i32, i32* %x13, align 4
  %109 = load i32, i32* %x12, align 4
  %add120 = add i32 %108, %109
  %call121 = call i32 @rotl32(i32 %add120, i32 13)
  %110 = load i32, i32* %x14, align 4
  %xor122 = xor i32 %110, %call121
  store i32 %xor122, i32* %x14, align 4
  %111 = load i32, i32* %x14, align 4
  %112 = load i32, i32* %x13, align 4
  %add123 = add i32 %111, %112
  %call124 = call i32 @rotl32(i32 %add123, i32 18)
  %113 = load i32, i32* %x15, align 4
  %xor125 = xor i32 %113, %call124
  store i32 %xor125, i32* %x15, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %114 = load i32, i32* %i, align 4
  %sub = sub i32 %114, 2
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %115 = load i8*, i8** %out.addr, align 4
  %add.ptr126 = getelementptr i8, i8* %115, i32 0
  %116 = load i32, i32* %x0, align 4
  call void @store32_le(i8* %add.ptr126, i32 %116)
  %117 = load i8*, i8** %out.addr, align 4
  %add.ptr127 = getelementptr i8, i8* %117, i32 4
  %118 = load i32, i32* %x5, align 4
  call void @store32_le(i8* %add.ptr127, i32 %118)
  %119 = load i8*, i8** %out.addr, align 4
  %add.ptr128 = getelementptr i8, i8* %119, i32 8
  %120 = load i32, i32* %x10, align 4
  call void @store32_le(i8* %add.ptr128, i32 %120)
  %121 = load i8*, i8** %out.addr, align 4
  %add.ptr129 = getelementptr i8, i8* %121, i32 12
  %122 = load i32, i32* %x15, align 4
  call void @store32_le(i8* %add.ptr129, i32 %122)
  %123 = load i8*, i8** %out.addr, align 4
  %add.ptr130 = getelementptr i8, i8* %123, i32 16
  %124 = load i32, i32* %x6, align 4
  call void @store32_le(i8* %add.ptr130, i32 %124)
  %125 = load i8*, i8** %out.addr, align 4
  %add.ptr131 = getelementptr i8, i8* %125, i32 20
  %126 = load i32, i32* %x7, align 4
  call void @store32_le(i8* %add.ptr131, i32 %126)
  %127 = load i8*, i8** %out.addr, align 4
  %add.ptr132 = getelementptr i8, i8* %127, i32 24
  %128 = load i32, i32* %x8, align 4
  call void @store32_le(i8* %add.ptr132, i32 %128)
  %129 = load i8*, i8** %out.addr, align 4
  %add.ptr133 = getelementptr i8, i8* %129, i32 28
  %130 = load i32, i32* %x9, align 4
  call void @store32_le(i8* %add.ptr133, i32 %130)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @load32_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i32* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 1 %1, i32 4, i1 false)
  %2 = load i32, i32* %w, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define internal i32 @rotl32(i32 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %shl = shl i32 %0, %1
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 32, %3
  %shr = lshr i32 %2, %sub
  %or = or i32 %shl, %shr
  ret i32 %or
}

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
