; ModuleID = 'crypto_pwhash/argon2/argon2-fill-block-ref.c'
source_filename = "crypto_pwhash/argon2/argon2-fill-block-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_instance_t = type { %struct.block_region_*, i64*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.block_region_ = type { i8*, %struct.block_*, i32 }
%struct.block_ = type { [128 x i64] }
%struct.Argon2_position_t = type { i32, i32, i8, i32 }

; Function Attrs: noinline nounwind optnone
define hidden void @argon2_fill_segment_ref(%struct.Argon2_instance_t* %instance, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4 %position) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %ref_block = alloca %struct.block_*, align 4
  %curr_block = alloca %struct.block_*, align 4
  %pseudo_rands = alloca i64*, align 4
  %pseudo_rand = alloca i64, align 8
  %ref_index = alloca i64, align 8
  %ref_lane = alloca i64, align 8
  %prev_offset = alloca i32, align 4
  %curr_offset = alloca i32, align 4
  %starting_index = alloca i32, align 4
  %i = alloca i32, align 4
  %data_independent_addressing = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store %struct.block_* null, %struct.block_** %ref_block, align 4
  store %struct.block_* null, %struct.block_** %curr_block, align 4
  store i64* null, i64** %pseudo_rands, align 4
  store i32 1, i32* %data_independent_addressing, align 4
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %cmp = icmp eq %struct.Argon2_instance_t* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end

if.end:                                           ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %type = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %1, i32 0, i32 9
  %2 = load i32, i32* %type, align 4
  %cmp1 = icmp eq i32 %2, 2
  br i1 %cmp1, label %land.lhs.true, label %if.end6

land.lhs.true:                                    ; preds = %if.end
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %3 = load i32, i32* %pass, align 4
  %cmp2 = icmp ne i32 %3, 0
  br i1 %cmp2, label %if.then5, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %4 = load i8, i8* %slice, align 4
  %conv = zext i8 %4 to i32
  %cmp3 = icmp uge i32 %conv, 2
  br i1 %cmp3, label %if.then5, label %if.end6

if.then5:                                         ; preds = %lor.lhs.false, %land.lhs.true
  store i32 0, i32* %data_independent_addressing, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %lor.lhs.false, %if.end
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands7 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 1
  %6 = load i64*, i64** %pseudo_rands7, align 4
  store i64* %6, i64** %pseudo_rands, align 4
  %7 = load i32, i32* %data_independent_addressing, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  %8 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %9 = load i64*, i64** %pseudo_rands, align 4
  call void @generate_addresses(%struct.Argon2_instance_t* %8, %struct.Argon2_position_t* %position, i64* %9)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end6
  store i32 0, i32* %starting_index, align 4
  %pass10 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %10 = load i32, i32* %pass10, align 4
  %cmp11 = icmp eq i32 0, %10
  br i1 %cmp11, label %land.lhs.true13, label %if.end19

land.lhs.true13:                                  ; preds = %if.end9
  %slice14 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %11 = load i8, i8* %slice14, align 4
  %conv15 = zext i8 %11 to i32
  %cmp16 = icmp eq i32 0, %conv15
  br i1 %cmp16, label %if.then18, label %if.end19

if.then18:                                        ; preds = %land.lhs.true13
  store i32 2, i32* %starting_index, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %land.lhs.true13, %if.end9
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %12 = load i32, i32* %lane, align 4
  %13 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %13, i32 0, i32 6
  %14 = load i32, i32* %lane_length, align 4
  %mul = mul i32 %12, %14
  %slice20 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %15 = load i8, i8* %slice20, align 4
  %conv21 = zext i8 %15 to i32
  %16 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %16, i32 0, i32 5
  %17 = load i32, i32* %segment_length, align 4
  %mul22 = mul i32 %conv21, %17
  %add = add i32 %mul, %mul22
  %18 = load i32, i32* %starting_index, align 4
  %add23 = add i32 %add, %18
  store i32 %add23, i32* %curr_offset, align 4
  %19 = load i32, i32* %curr_offset, align 4
  %20 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length24 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %20, i32 0, i32 6
  %21 = load i32, i32* %lane_length24, align 4
  %rem = urem i32 %19, %21
  %cmp25 = icmp eq i32 0, %rem
  br i1 %cmp25, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.end19
  %22 = load i32, i32* %curr_offset, align 4
  %23 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length28 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %23, i32 0, i32 6
  %24 = load i32, i32* %lane_length28, align 4
  %add29 = add i32 %22, %24
  %sub = sub i32 %add29, 1
  store i32 %sub, i32* %prev_offset, align 4
  br label %if.end31

if.else:                                          ; preds = %if.end19
  %25 = load i32, i32* %curr_offset, align 4
  %sub30 = sub i32 %25, 1
  store i32 %sub30, i32* %prev_offset, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.then27
  %26 = load i32, i32* %starting_index, align 4
  store i32 %26, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end31
  %27 = load i32, i32* %i, align 4
  %28 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length32 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %28, i32 0, i32 5
  %29 = load i32, i32* %segment_length32, align 4
  %cmp33 = icmp ult i32 %27, %29
  br i1 %cmp33, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %30 = load i32, i32* %curr_offset, align 4
  %31 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length35 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %31, i32 0, i32 6
  %32 = load i32, i32* %lane_length35, align 4
  %rem36 = urem i32 %30, %32
  %cmp37 = icmp eq i32 %rem36, 1
  br i1 %cmp37, label %if.then39, label %if.end41

if.then39:                                        ; preds = %for.body
  %33 = load i32, i32* %curr_offset, align 4
  %sub40 = sub i32 %33, 1
  store i32 %sub40, i32* %prev_offset, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then39, %for.body
  %34 = load i32, i32* %data_independent_addressing, align 4
  %tobool42 = icmp ne i32 %34, 0
  br i1 %tobool42, label %if.then43, label %if.else44

if.then43:                                        ; preds = %if.end41
  %35 = load i64*, i64** %pseudo_rands, align 4
  %36 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i64, i64* %35, i32 %36
  %37 = load i64, i64* %arrayidx, align 8
  store i64 %37, i64* %pseudo_rand, align 8
  br label %if.end47

if.else44:                                        ; preds = %if.end41
  %38 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %38, i32 0, i32 0
  %39 = load %struct.block_region_*, %struct.block_region_** %region, align 4
  %memory = getelementptr inbounds %struct.block_region_, %struct.block_region_* %39, i32 0, i32 1
  %40 = load %struct.block_*, %struct.block_** %memory, align 4
  %41 = load i32, i32* %prev_offset, align 4
  %arrayidx45 = getelementptr %struct.block_, %struct.block_* %40, i32 %41
  %v = getelementptr inbounds %struct.block_, %struct.block_* %arrayidx45, i32 0, i32 0
  %arrayidx46 = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %42 = load i64, i64* %arrayidx46, align 8
  store i64 %42, i64* %pseudo_rand, align 8
  br label %if.end47

if.end47:                                         ; preds = %if.else44, %if.then43
  %43 = load i64, i64* %pseudo_rand, align 8
  %shr = lshr i64 %43, 32
  %44 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %44, i32 0, i32 7
  %45 = load i32, i32* %lanes, align 4
  %conv48 = zext i32 %45 to i64
  %rem49 = urem i64 %shr, %conv48
  store i64 %rem49, i64* %ref_lane, align 8
  %pass50 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %46 = load i32, i32* %pass50, align 4
  %cmp51 = icmp eq i32 %46, 0
  br i1 %cmp51, label %land.lhs.true53, label %if.end61

land.lhs.true53:                                  ; preds = %if.end47
  %slice54 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  %47 = load i8, i8* %slice54, align 4
  %conv55 = zext i8 %47 to i32
  %cmp56 = icmp eq i32 %conv55, 0
  br i1 %cmp56, label %if.then58, label %if.end61

if.then58:                                        ; preds = %land.lhs.true53
  %lane59 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %48 = load i32, i32* %lane59, align 4
  %conv60 = zext i32 %48 to i64
  store i64 %conv60, i64* %ref_lane, align 8
  br label %if.end61

if.end61:                                         ; preds = %if.then58, %land.lhs.true53, %if.end47
  %49 = load i32, i32* %i, align 4
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 3
  store i32 %49, i32* %index, align 4
  %50 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %51 = load i64, i64* %pseudo_rand, align 8
  %and = and i64 %51, 4294967295
  %conv62 = trunc i64 %and to i32
  %52 = load i64, i64* %ref_lane, align 8
  %lane63 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  %53 = load i32, i32* %lane63, align 4
  %conv64 = zext i32 %53 to i64
  %cmp65 = icmp eq i64 %52, %conv64
  %conv66 = zext i1 %cmp65 to i32
  %call = call i32 @index_alpha(%struct.Argon2_instance_t* %50, %struct.Argon2_position_t* %position, i32 %conv62, i32 %conv66)
  %conv67 = zext i32 %call to i64
  store i64 %conv67, i64* %ref_index, align 8
  %54 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region68 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %54, i32 0, i32 0
  %55 = load %struct.block_region_*, %struct.block_region_** %region68, align 4
  %memory69 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %55, i32 0, i32 1
  %56 = load %struct.block_*, %struct.block_** %memory69, align 4
  %57 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length70 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %57, i32 0, i32 6
  %58 = load i32, i32* %lane_length70, align 4
  %conv71 = zext i32 %58 to i64
  %59 = load i64, i64* %ref_lane, align 8
  %mul72 = mul i64 %conv71, %59
  %idx.ext = trunc i64 %mul72 to i32
  %add.ptr = getelementptr %struct.block_, %struct.block_* %56, i32 %idx.ext
  %60 = load i64, i64* %ref_index, align 8
  %idx.ext73 = trunc i64 %60 to i32
  %add.ptr74 = getelementptr %struct.block_, %struct.block_* %add.ptr, i32 %idx.ext73
  store %struct.block_* %add.ptr74, %struct.block_** %ref_block, align 4
  %61 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region75 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %61, i32 0, i32 0
  %62 = load %struct.block_region_*, %struct.block_region_** %region75, align 4
  %memory76 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %62, i32 0, i32 1
  %63 = load %struct.block_*, %struct.block_** %memory76, align 4
  %64 = load i32, i32* %curr_offset, align 4
  %add.ptr77 = getelementptr %struct.block_, %struct.block_* %63, i32 %64
  store %struct.block_* %add.ptr77, %struct.block_** %curr_block, align 4
  %pass78 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  %65 = load i32, i32* %pass78, align 4
  %cmp79 = icmp ne i32 %65, 0
  br i1 %cmp79, label %if.then81, label %if.else85

if.then81:                                        ; preds = %if.end61
  %66 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region82 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %66, i32 0, i32 0
  %67 = load %struct.block_region_*, %struct.block_region_** %region82, align 4
  %memory83 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %67, i32 0, i32 1
  %68 = load %struct.block_*, %struct.block_** %memory83, align 4
  %69 = load i32, i32* %prev_offset, align 4
  %add.ptr84 = getelementptr %struct.block_, %struct.block_* %68, i32 %69
  %70 = load %struct.block_*, %struct.block_** %ref_block, align 4
  %71 = load %struct.block_*, %struct.block_** %curr_block, align 4
  call void @fill_block_with_xor(%struct.block_* %add.ptr84, %struct.block_* %70, %struct.block_* %71)
  br label %if.end89

if.else85:                                        ; preds = %if.end61
  %72 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region86 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %72, i32 0, i32 0
  %73 = load %struct.block_region_*, %struct.block_region_** %region86, align 4
  %memory87 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %73, i32 0, i32 1
  %74 = load %struct.block_*, %struct.block_** %memory87, align 4
  %75 = load i32, i32* %prev_offset, align 4
  %add.ptr88 = getelementptr %struct.block_, %struct.block_* %74, i32 %75
  %76 = load %struct.block_*, %struct.block_** %ref_block, align 4
  %77 = load %struct.block_*, %struct.block_** %curr_block, align 4
  call void @fill_block(%struct.block_* %add.ptr88, %struct.block_* %76, %struct.block_* %77)
  br label %if.end89

if.end89:                                         ; preds = %if.else85, %if.then81
  br label %for.inc

for.inc:                                          ; preds = %if.end89
  %78 = load i32, i32* %i, align 4
  %inc = add i32 %78, 1
  store i32 %inc, i32* %i, align 4
  %79 = load i32, i32* %curr_offset, align 4
  %inc90 = add i32 %79, 1
  store i32 %inc90, i32* %curr_offset, align 4
  %80 = load i32, i32* %prev_offset, align 4
  %inc91 = add i32 %80, 1
  store i32 %inc91, i32* %prev_offset, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @generate_addresses(%struct.Argon2_instance_t* %instance, %struct.Argon2_position_t* %position, i64* %pseudo_rands) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %position.addr = alloca %struct.Argon2_position_t*, align 4
  %pseudo_rands.addr = alloca i64*, align 4
  %zero_block = alloca %struct.block_, align 8
  %input_block = alloca %struct.block_, align 8
  %address_block = alloca %struct.block_, align 8
  %tmp_block = alloca %struct.block_, align 8
  %i = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store %struct.Argon2_position_t* %position, %struct.Argon2_position_t** %position.addr, align 4
  store i64* %pseudo_rands, i64** %pseudo_rands.addr, align 4
  call void @init_block_value(%struct.block_* %zero_block, i8 zeroext 0)
  call void @init_block_value(%struct.block_* %input_block, i8 zeroext 0)
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %cmp = icmp ne %struct.Argon2_instance_t* %0, null
  br i1 %cmp, label %land.lhs.true, label %if.end29

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %cmp1 = icmp ne %struct.Argon2_position_t* %1, null
  br i1 %cmp1, label %if.then, label %if.end29

if.then:                                          ; preds = %land.lhs.true
  %2 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %2, i32 0, i32 0
  %3 = load i32, i32* %pass, align 4
  %conv = zext i32 %3 to i64
  %v = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 0
  store i64 %conv, i64* %arrayidx, align 8
  %4 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %4, i32 0, i32 1
  %5 = load i32, i32* %lane, align 4
  %conv2 = zext i32 %5 to i64
  %v3 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx4 = getelementptr [128 x i64], [128 x i64]* %v3, i32 0, i32 1
  store i64 %conv2, i64* %arrayidx4, align 8
  %6 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %6, i32 0, i32 2
  %7 = load i8, i8* %slice, align 4
  %conv5 = zext i8 %7 to i64
  %v6 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx7 = getelementptr [128 x i64], [128 x i64]* %v6, i32 0, i32 2
  store i64 %conv5, i64* %arrayidx7, align 8
  %8 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %8, i32 0, i32 4
  %9 = load i32, i32* %memory_blocks, align 4
  %conv8 = zext i32 %9 to i64
  %v9 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx10 = getelementptr [128 x i64], [128 x i64]* %v9, i32 0, i32 3
  store i64 %conv8, i64* %arrayidx10, align 8
  %10 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %10, i32 0, i32 2
  %11 = load i32, i32* %passes, align 4
  %conv11 = zext i32 %11 to i64
  %v12 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx13 = getelementptr [128 x i64], [128 x i64]* %v12, i32 0, i32 4
  store i64 %conv11, i64* %arrayidx13, align 8
  %12 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %type = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %12, i32 0, i32 9
  %13 = load i32, i32* %type, align 4
  %conv14 = zext i32 %13 to i64
  %v15 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx16 = getelementptr [128 x i64], [128 x i64]* %v15, i32 0, i32 5
  store i64 %conv14, i64* %arrayidx16, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %14 = load i32, i32* %i, align 4
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 5
  %16 = load i32, i32* %segment_length, align 4
  %cmp17 = icmp ult i32 %14, %16
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load i32, i32* %i, align 4
  %rem = urem i32 %17, 128
  %cmp19 = icmp eq i32 %rem, 0
  br i1 %cmp19, label %if.then21, label %if.end

if.then21:                                        ; preds = %for.body
  %v22 = getelementptr inbounds %struct.block_, %struct.block_* %input_block, i32 0, i32 0
  %arrayidx23 = getelementptr [128 x i64], [128 x i64]* %v22, i32 0, i32 6
  %18 = load i64, i64* %arrayidx23, align 8
  %inc = add i64 %18, 1
  store i64 %inc, i64* %arrayidx23, align 8
  call void @init_block_value(%struct.block_* %tmp_block, i8 zeroext 0)
  call void @init_block_value(%struct.block_* %address_block, i8 zeroext 0)
  call void @fill_block_with_xor(%struct.block_* %zero_block, %struct.block_* %input_block, %struct.block_* %tmp_block)
  call void @fill_block_with_xor(%struct.block_* %zero_block, %struct.block_* %tmp_block, %struct.block_* %address_block)
  br label %if.end

if.end:                                           ; preds = %if.then21, %for.body
  %v24 = getelementptr inbounds %struct.block_, %struct.block_* %address_block, i32 0, i32 0
  %19 = load i32, i32* %i, align 4
  %rem25 = urem i32 %19, 128
  %arrayidx26 = getelementptr [128 x i64], [128 x i64]* %v24, i32 0, i32 %rem25
  %20 = load i64, i64* %arrayidx26, align 8
  %21 = load i64*, i64** %pseudo_rands.addr, align 4
  %22 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr i64, i64* %21, i32 %22
  store i64 %20, i64* %arrayidx27, align 8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc28 = add i32 %23, 1
  store i32 %inc28, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end29

if.end29:                                         ; preds = %for.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @index_alpha(%struct.Argon2_instance_t* %instance, %struct.Argon2_position_t* %position, i32 %pseudo_rand, i32 %same_lane) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %position.addr = alloca %struct.Argon2_position_t*, align 4
  %pseudo_rand.addr = alloca i32, align 4
  %same_lane.addr = alloca i32, align 4
  %reference_area_size = alloca i32, align 4
  %relative_position = alloca i64, align 8
  %start_position = alloca i32, align 4
  %absolute_position = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store %struct.Argon2_position_t* %position, %struct.Argon2_position_t** %position.addr, align 4
  store i32 %pseudo_rand, i32* %pseudo_rand.addr, align 4
  store i32 %same_lane, i32* %same_lane.addr, align 4
  %0 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %pass = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %0, i32 0, i32 0
  %1 = load i32, i32* %pass, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else19

if.then:                                          ; preds = %entry
  %2 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %2, i32 0, i32 2
  %3 = load i8, i8* %slice, align 4
  %conv = zext i8 %3 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %4 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %4, i32 0, i32 3
  %5 = load i32, i32* %index, align 4
  %sub = sub i32 %5, 1
  store i32 %sub, i32* %reference_area_size, align 4
  br label %if.end18

if.else:                                          ; preds = %if.then
  %6 = load i32, i32* %same_lane.addr, align 4
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then4, label %if.else9

if.then4:                                         ; preds = %if.else
  %7 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice5 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %7, i32 0, i32 2
  %8 = load i8, i8* %slice5, align 4
  %conv6 = zext i8 %8 to i32
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 5
  %10 = load i32, i32* %segment_length, align 4
  %mul = mul i32 %conv6, %10
  %11 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %index7 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %11, i32 0, i32 3
  %12 = load i32, i32* %index7, align 4
  %add = add i32 %mul, %12
  %sub8 = sub i32 %add, 1
  store i32 %sub8, i32* %reference_area_size, align 4
  br label %if.end

if.else9:                                         ; preds = %if.else
  %13 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice10 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %13, i32 0, i32 2
  %14 = load i8, i8* %slice10, align 4
  %conv11 = zext i8 %14 to i32
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length12 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 5
  %16 = load i32, i32* %segment_length12, align 4
  %mul13 = mul i32 %conv11, %16
  %17 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %index14 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %17, i32 0, i32 3
  %18 = load i32, i32* %index14, align 4
  %cmp15 = icmp eq i32 %18, 0
  %19 = zext i1 %cmp15 to i64
  %cond = select i1 %cmp15, i32 -1, i32 0
  %add17 = add i32 %mul13, %cond
  store i32 %add17, i32* %reference_area_size, align 4
  br label %if.end

if.end:                                           ; preds = %if.else9, %if.then4
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then3
  br label %if.end37

if.else19:                                        ; preds = %entry
  %20 = load i32, i32* %same_lane.addr, align 4
  %tobool20 = icmp ne i32 %20, 0
  br i1 %tobool20, label %if.then21, label %if.else27

if.then21:                                        ; preds = %if.else19
  %21 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %21, i32 0, i32 6
  %22 = load i32, i32* %lane_length, align 4
  %23 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length22 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %23, i32 0, i32 5
  %24 = load i32, i32* %segment_length22, align 4
  %sub23 = sub i32 %22, %24
  %25 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %index24 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %25, i32 0, i32 3
  %26 = load i32, i32* %index24, align 4
  %add25 = add i32 %sub23, %26
  %sub26 = sub i32 %add25, 1
  store i32 %sub26, i32* %reference_area_size, align 4
  br label %if.end36

if.else27:                                        ; preds = %if.else19
  %27 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length28 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %27, i32 0, i32 6
  %28 = load i32, i32* %lane_length28, align 4
  %29 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length29 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %29, i32 0, i32 5
  %30 = load i32, i32* %segment_length29, align 4
  %sub30 = sub i32 %28, %30
  %31 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %index31 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %31, i32 0, i32 3
  %32 = load i32, i32* %index31, align 4
  %cmp32 = icmp eq i32 %32, 0
  %33 = zext i1 %cmp32 to i64
  %cond34 = select i1 %cmp32, i32 -1, i32 0
  %add35 = add i32 %sub30, %cond34
  store i32 %add35, i32* %reference_area_size, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.else27, %if.then21
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %if.end18
  %34 = load i32, i32* %pseudo_rand.addr, align 4
  %conv38 = zext i32 %34 to i64
  store i64 %conv38, i64* %relative_position, align 8
  %35 = load i64, i64* %relative_position, align 8
  %36 = load i64, i64* %relative_position, align 8
  %mul39 = mul i64 %35, %36
  %shr = lshr i64 %mul39, 32
  store i64 %shr, i64* %relative_position, align 8
  %37 = load i32, i32* %reference_area_size, align 4
  %sub40 = sub i32 %37, 1
  %conv41 = zext i32 %sub40 to i64
  %38 = load i32, i32* %reference_area_size, align 4
  %conv42 = zext i32 %38 to i64
  %39 = load i64, i64* %relative_position, align 8
  %mul43 = mul i64 %conv42, %39
  %shr44 = lshr i64 %mul43, 32
  %sub45 = sub i64 %conv41, %shr44
  store i64 %sub45, i64* %relative_position, align 8
  store i32 0, i32* %start_position, align 4
  %40 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %pass46 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %40, i32 0, i32 0
  %41 = load i32, i32* %pass46, align 4
  %cmp47 = icmp ne i32 %41, 0
  br i1 %cmp47, label %if.then49, label %if.end60

if.then49:                                        ; preds = %if.end37
  %42 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice50 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %42, i32 0, i32 2
  %43 = load i8, i8* %slice50, align 4
  %conv51 = zext i8 %43 to i32
  %cmp52 = icmp eq i32 %conv51, 3
  br i1 %cmp52, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then49
  br label %cond.end

cond.false:                                       ; preds = %if.then49
  %44 = load %struct.Argon2_position_t*, %struct.Argon2_position_t** %position.addr, align 4
  %slice54 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %44, i32 0, i32 2
  %45 = load i8, i8* %slice54, align 4
  %conv55 = zext i8 %45 to i32
  %add56 = add i32 %conv55, 1
  %46 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length57 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %46, i32 0, i32 5
  %47 = load i32, i32* %segment_length57, align 4
  %mul58 = mul i32 %add56, %47
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond59 = phi i32 [ 0, %cond.true ], [ %mul58, %cond.false ]
  store i32 %cond59, i32* %start_position, align 4
  br label %if.end60

if.end60:                                         ; preds = %cond.end, %if.end37
  %48 = load i32, i32* %start_position, align 4
  %conv61 = zext i32 %48 to i64
  %49 = load i64, i64* %relative_position, align 8
  %add62 = add i64 %conv61, %49
  %50 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length63 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %50, i32 0, i32 6
  %51 = load i32, i32* %lane_length63, align 4
  %conv64 = zext i32 %51 to i64
  %rem = urem i64 %add62, %conv64
  %conv65 = trunc i64 %rem to i32
  store i32 %conv65, i32* %absolute_position, align 4
  %52 = load i32, i32* %absolute_position, align 4
  ret i32 %52
}

; Function Attrs: noinline nounwind optnone
define internal void @fill_block_with_xor(%struct.block_* %prev_block, %struct.block_* %ref_block, %struct.block_* %next_block) #0 {
entry:
  %prev_block.addr = alloca %struct.block_*, align 4
  %ref_block.addr = alloca %struct.block_*, align 4
  %next_block.addr = alloca %struct.block_*, align 4
  %blockR = alloca %struct.block_, align 8
  %block_tmp = alloca %struct.block_, align 8
  %i = alloca i32, align 4
  store %struct.block_* %prev_block, %struct.block_** %prev_block.addr, align 4
  store %struct.block_* %ref_block, %struct.block_** %ref_block.addr, align 4
  store %struct.block_* %next_block, %struct.block_** %next_block.addr, align 4
  %0 = load %struct.block_*, %struct.block_** %ref_block.addr, align 4
  call void @copy_block(%struct.block_* %blockR, %struct.block_* %0)
  %1 = load %struct.block_*, %struct.block_** %prev_block.addr, align 4
  call void @xor_block(%struct.block_* %blockR, %struct.block_* %1)
  call void @copy_block(%struct.block_* %block_tmp, %struct.block_* %blockR)
  %2 = load %struct.block_*, %struct.block_** %next_block.addr, align 4
  call void @xor_block(%struct.block_* %block_tmp, %struct.block_* %2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %3, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body

do.body:                                          ; preds = %for.body
  br label %do.body1

do.body1:                                         ; preds = %do.body
  %v = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %mul = mul i32 16, %4
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %mul
  %5 = load i64, i64* %arrayidx, align 8
  %v2 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %mul3 = mul i32 16, %6
  %add = add i32 %mul3, 4
  %arrayidx4 = getelementptr [128 x i64], [128 x i64]* %v2, i32 0, i32 %add
  %7 = load i64, i64* %arrayidx4, align 8
  %call = call i64 @fBlaMka(i64 %5, i64 %7)
  %v5 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %8 = load i32, i32* %i, align 4
  %mul6 = mul i32 16, %8
  %arrayidx7 = getelementptr [128 x i64], [128 x i64]* %v5, i32 0, i32 %mul6
  store i64 %call, i64* %arrayidx7, align 8
  %v8 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %9 = load i32, i32* %i, align 4
  %mul9 = mul i32 16, %9
  %add10 = add i32 %mul9, 12
  %arrayidx11 = getelementptr [128 x i64], [128 x i64]* %v8, i32 0, i32 %add10
  %10 = load i64, i64* %arrayidx11, align 8
  %v12 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %11 = load i32, i32* %i, align 4
  %mul13 = mul i32 16, %11
  %arrayidx14 = getelementptr [128 x i64], [128 x i64]* %v12, i32 0, i32 %mul13
  %12 = load i64, i64* %arrayidx14, align 8
  %xor = xor i64 %10, %12
  %call15 = call i64 @rotr64(i64 %xor, i32 32)
  %v16 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %mul17 = mul i32 16, %13
  %add18 = add i32 %mul17, 12
  %arrayidx19 = getelementptr [128 x i64], [128 x i64]* %v16, i32 0, i32 %add18
  store i64 %call15, i64* %arrayidx19, align 8
  %v20 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %14 = load i32, i32* %i, align 4
  %mul21 = mul i32 16, %14
  %add22 = add i32 %mul21, 8
  %arrayidx23 = getelementptr [128 x i64], [128 x i64]* %v20, i32 0, i32 %add22
  %15 = load i64, i64* %arrayidx23, align 8
  %v24 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %16 = load i32, i32* %i, align 4
  %mul25 = mul i32 16, %16
  %add26 = add i32 %mul25, 12
  %arrayidx27 = getelementptr [128 x i64], [128 x i64]* %v24, i32 0, i32 %add26
  %17 = load i64, i64* %arrayidx27, align 8
  %call28 = call i64 @fBlaMka(i64 %15, i64 %17)
  %v29 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %mul30 = mul i32 16, %18
  %add31 = add i32 %mul30, 8
  %arrayidx32 = getelementptr [128 x i64], [128 x i64]* %v29, i32 0, i32 %add31
  store i64 %call28, i64* %arrayidx32, align 8
  %v33 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %19 = load i32, i32* %i, align 4
  %mul34 = mul i32 16, %19
  %add35 = add i32 %mul34, 4
  %arrayidx36 = getelementptr [128 x i64], [128 x i64]* %v33, i32 0, i32 %add35
  %20 = load i64, i64* %arrayidx36, align 8
  %v37 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %21 = load i32, i32* %i, align 4
  %mul38 = mul i32 16, %21
  %add39 = add i32 %mul38, 8
  %arrayidx40 = getelementptr [128 x i64], [128 x i64]* %v37, i32 0, i32 %add39
  %22 = load i64, i64* %arrayidx40, align 8
  %xor41 = xor i64 %20, %22
  %call42 = call i64 @rotr64(i64 %xor41, i32 24)
  %v43 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %23 = load i32, i32* %i, align 4
  %mul44 = mul i32 16, %23
  %add45 = add i32 %mul44, 4
  %arrayidx46 = getelementptr [128 x i64], [128 x i64]* %v43, i32 0, i32 %add45
  store i64 %call42, i64* %arrayidx46, align 8
  %v47 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %24 = load i32, i32* %i, align 4
  %mul48 = mul i32 16, %24
  %arrayidx49 = getelementptr [128 x i64], [128 x i64]* %v47, i32 0, i32 %mul48
  %25 = load i64, i64* %arrayidx49, align 8
  %v50 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %mul51 = mul i32 16, %26
  %add52 = add i32 %mul51, 4
  %arrayidx53 = getelementptr [128 x i64], [128 x i64]* %v50, i32 0, i32 %add52
  %27 = load i64, i64* %arrayidx53, align 8
  %call54 = call i64 @fBlaMka(i64 %25, i64 %27)
  %v55 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %28 = load i32, i32* %i, align 4
  %mul56 = mul i32 16, %28
  %arrayidx57 = getelementptr [128 x i64], [128 x i64]* %v55, i32 0, i32 %mul56
  store i64 %call54, i64* %arrayidx57, align 8
  %v58 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %29 = load i32, i32* %i, align 4
  %mul59 = mul i32 16, %29
  %add60 = add i32 %mul59, 12
  %arrayidx61 = getelementptr [128 x i64], [128 x i64]* %v58, i32 0, i32 %add60
  %30 = load i64, i64* %arrayidx61, align 8
  %v62 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %31 = load i32, i32* %i, align 4
  %mul63 = mul i32 16, %31
  %arrayidx64 = getelementptr [128 x i64], [128 x i64]* %v62, i32 0, i32 %mul63
  %32 = load i64, i64* %arrayidx64, align 8
  %xor65 = xor i64 %30, %32
  %call66 = call i64 @rotr64(i64 %xor65, i32 16)
  %v67 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %33 = load i32, i32* %i, align 4
  %mul68 = mul i32 16, %33
  %add69 = add i32 %mul68, 12
  %arrayidx70 = getelementptr [128 x i64], [128 x i64]* %v67, i32 0, i32 %add69
  store i64 %call66, i64* %arrayidx70, align 8
  %v71 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %34 = load i32, i32* %i, align 4
  %mul72 = mul i32 16, %34
  %add73 = add i32 %mul72, 8
  %arrayidx74 = getelementptr [128 x i64], [128 x i64]* %v71, i32 0, i32 %add73
  %35 = load i64, i64* %arrayidx74, align 8
  %v75 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %36 = load i32, i32* %i, align 4
  %mul76 = mul i32 16, %36
  %add77 = add i32 %mul76, 12
  %arrayidx78 = getelementptr [128 x i64], [128 x i64]* %v75, i32 0, i32 %add77
  %37 = load i64, i64* %arrayidx78, align 8
  %call79 = call i64 @fBlaMka(i64 %35, i64 %37)
  %v80 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %38 = load i32, i32* %i, align 4
  %mul81 = mul i32 16, %38
  %add82 = add i32 %mul81, 8
  %arrayidx83 = getelementptr [128 x i64], [128 x i64]* %v80, i32 0, i32 %add82
  store i64 %call79, i64* %arrayidx83, align 8
  %v84 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %39 = load i32, i32* %i, align 4
  %mul85 = mul i32 16, %39
  %add86 = add i32 %mul85, 4
  %arrayidx87 = getelementptr [128 x i64], [128 x i64]* %v84, i32 0, i32 %add86
  %40 = load i64, i64* %arrayidx87, align 8
  %v88 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %41 = load i32, i32* %i, align 4
  %mul89 = mul i32 16, %41
  %add90 = add i32 %mul89, 8
  %arrayidx91 = getelementptr [128 x i64], [128 x i64]* %v88, i32 0, i32 %add90
  %42 = load i64, i64* %arrayidx91, align 8
  %xor92 = xor i64 %40, %42
  %call93 = call i64 @rotr64(i64 %xor92, i32 63)
  %v94 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %43 = load i32, i32* %i, align 4
  %mul95 = mul i32 16, %43
  %add96 = add i32 %mul95, 4
  %arrayidx97 = getelementptr [128 x i64], [128 x i64]* %v94, i32 0, i32 %add96
  store i64 %call93, i64* %arrayidx97, align 8
  br label %do.end

do.end:                                           ; preds = %do.body1
  br label %do.body98

do.body98:                                        ; preds = %do.end
  %v99 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %44 = load i32, i32* %i, align 4
  %mul100 = mul i32 16, %44
  %add101 = add i32 %mul100, 1
  %arrayidx102 = getelementptr [128 x i64], [128 x i64]* %v99, i32 0, i32 %add101
  %45 = load i64, i64* %arrayidx102, align 8
  %v103 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %46 = load i32, i32* %i, align 4
  %mul104 = mul i32 16, %46
  %add105 = add i32 %mul104, 5
  %arrayidx106 = getelementptr [128 x i64], [128 x i64]* %v103, i32 0, i32 %add105
  %47 = load i64, i64* %arrayidx106, align 8
  %call107 = call i64 @fBlaMka(i64 %45, i64 %47)
  %v108 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %mul109 = mul i32 16, %48
  %add110 = add i32 %mul109, 1
  %arrayidx111 = getelementptr [128 x i64], [128 x i64]* %v108, i32 0, i32 %add110
  store i64 %call107, i64* %arrayidx111, align 8
  %v112 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %49 = load i32, i32* %i, align 4
  %mul113 = mul i32 16, %49
  %add114 = add i32 %mul113, 13
  %arrayidx115 = getelementptr [128 x i64], [128 x i64]* %v112, i32 0, i32 %add114
  %50 = load i64, i64* %arrayidx115, align 8
  %v116 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %51 = load i32, i32* %i, align 4
  %mul117 = mul i32 16, %51
  %add118 = add i32 %mul117, 1
  %arrayidx119 = getelementptr [128 x i64], [128 x i64]* %v116, i32 0, i32 %add118
  %52 = load i64, i64* %arrayidx119, align 8
  %xor120 = xor i64 %50, %52
  %call121 = call i64 @rotr64(i64 %xor120, i32 32)
  %v122 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %53 = load i32, i32* %i, align 4
  %mul123 = mul i32 16, %53
  %add124 = add i32 %mul123, 13
  %arrayidx125 = getelementptr [128 x i64], [128 x i64]* %v122, i32 0, i32 %add124
  store i64 %call121, i64* %arrayidx125, align 8
  %v126 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %54 = load i32, i32* %i, align 4
  %mul127 = mul i32 16, %54
  %add128 = add i32 %mul127, 9
  %arrayidx129 = getelementptr [128 x i64], [128 x i64]* %v126, i32 0, i32 %add128
  %55 = load i64, i64* %arrayidx129, align 8
  %v130 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %mul131 = mul i32 16, %56
  %add132 = add i32 %mul131, 13
  %arrayidx133 = getelementptr [128 x i64], [128 x i64]* %v130, i32 0, i32 %add132
  %57 = load i64, i64* %arrayidx133, align 8
  %call134 = call i64 @fBlaMka(i64 %55, i64 %57)
  %v135 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %58 = load i32, i32* %i, align 4
  %mul136 = mul i32 16, %58
  %add137 = add i32 %mul136, 9
  %arrayidx138 = getelementptr [128 x i64], [128 x i64]* %v135, i32 0, i32 %add137
  store i64 %call134, i64* %arrayidx138, align 8
  %v139 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %59 = load i32, i32* %i, align 4
  %mul140 = mul i32 16, %59
  %add141 = add i32 %mul140, 5
  %arrayidx142 = getelementptr [128 x i64], [128 x i64]* %v139, i32 0, i32 %add141
  %60 = load i64, i64* %arrayidx142, align 8
  %v143 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %61 = load i32, i32* %i, align 4
  %mul144 = mul i32 16, %61
  %add145 = add i32 %mul144, 9
  %arrayidx146 = getelementptr [128 x i64], [128 x i64]* %v143, i32 0, i32 %add145
  %62 = load i64, i64* %arrayidx146, align 8
  %xor147 = xor i64 %60, %62
  %call148 = call i64 @rotr64(i64 %xor147, i32 24)
  %v149 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %63 = load i32, i32* %i, align 4
  %mul150 = mul i32 16, %63
  %add151 = add i32 %mul150, 5
  %arrayidx152 = getelementptr [128 x i64], [128 x i64]* %v149, i32 0, i32 %add151
  store i64 %call148, i64* %arrayidx152, align 8
  %v153 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %64 = load i32, i32* %i, align 4
  %mul154 = mul i32 16, %64
  %add155 = add i32 %mul154, 1
  %arrayidx156 = getelementptr [128 x i64], [128 x i64]* %v153, i32 0, i32 %add155
  %65 = load i64, i64* %arrayidx156, align 8
  %v157 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %66 = load i32, i32* %i, align 4
  %mul158 = mul i32 16, %66
  %add159 = add i32 %mul158, 5
  %arrayidx160 = getelementptr [128 x i64], [128 x i64]* %v157, i32 0, i32 %add159
  %67 = load i64, i64* %arrayidx160, align 8
  %call161 = call i64 @fBlaMka(i64 %65, i64 %67)
  %v162 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %68 = load i32, i32* %i, align 4
  %mul163 = mul i32 16, %68
  %add164 = add i32 %mul163, 1
  %arrayidx165 = getelementptr [128 x i64], [128 x i64]* %v162, i32 0, i32 %add164
  store i64 %call161, i64* %arrayidx165, align 8
  %v166 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %69 = load i32, i32* %i, align 4
  %mul167 = mul i32 16, %69
  %add168 = add i32 %mul167, 13
  %arrayidx169 = getelementptr [128 x i64], [128 x i64]* %v166, i32 0, i32 %add168
  %70 = load i64, i64* %arrayidx169, align 8
  %v170 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %71 = load i32, i32* %i, align 4
  %mul171 = mul i32 16, %71
  %add172 = add i32 %mul171, 1
  %arrayidx173 = getelementptr [128 x i64], [128 x i64]* %v170, i32 0, i32 %add172
  %72 = load i64, i64* %arrayidx173, align 8
  %xor174 = xor i64 %70, %72
  %call175 = call i64 @rotr64(i64 %xor174, i32 16)
  %v176 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %73 = load i32, i32* %i, align 4
  %mul177 = mul i32 16, %73
  %add178 = add i32 %mul177, 13
  %arrayidx179 = getelementptr [128 x i64], [128 x i64]* %v176, i32 0, i32 %add178
  store i64 %call175, i64* %arrayidx179, align 8
  %v180 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %74 = load i32, i32* %i, align 4
  %mul181 = mul i32 16, %74
  %add182 = add i32 %mul181, 9
  %arrayidx183 = getelementptr [128 x i64], [128 x i64]* %v180, i32 0, i32 %add182
  %75 = load i64, i64* %arrayidx183, align 8
  %v184 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %76 = load i32, i32* %i, align 4
  %mul185 = mul i32 16, %76
  %add186 = add i32 %mul185, 13
  %arrayidx187 = getelementptr [128 x i64], [128 x i64]* %v184, i32 0, i32 %add186
  %77 = load i64, i64* %arrayidx187, align 8
  %call188 = call i64 @fBlaMka(i64 %75, i64 %77)
  %v189 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %78 = load i32, i32* %i, align 4
  %mul190 = mul i32 16, %78
  %add191 = add i32 %mul190, 9
  %arrayidx192 = getelementptr [128 x i64], [128 x i64]* %v189, i32 0, i32 %add191
  store i64 %call188, i64* %arrayidx192, align 8
  %v193 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %79 = load i32, i32* %i, align 4
  %mul194 = mul i32 16, %79
  %add195 = add i32 %mul194, 5
  %arrayidx196 = getelementptr [128 x i64], [128 x i64]* %v193, i32 0, i32 %add195
  %80 = load i64, i64* %arrayidx196, align 8
  %v197 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %81 = load i32, i32* %i, align 4
  %mul198 = mul i32 16, %81
  %add199 = add i32 %mul198, 9
  %arrayidx200 = getelementptr [128 x i64], [128 x i64]* %v197, i32 0, i32 %add199
  %82 = load i64, i64* %arrayidx200, align 8
  %xor201 = xor i64 %80, %82
  %call202 = call i64 @rotr64(i64 %xor201, i32 63)
  %v203 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %83 = load i32, i32* %i, align 4
  %mul204 = mul i32 16, %83
  %add205 = add i32 %mul204, 5
  %arrayidx206 = getelementptr [128 x i64], [128 x i64]* %v203, i32 0, i32 %add205
  store i64 %call202, i64* %arrayidx206, align 8
  br label %do.end207

do.end207:                                        ; preds = %do.body98
  br label %do.body208

do.body208:                                       ; preds = %do.end207
  %v209 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %84 = load i32, i32* %i, align 4
  %mul210 = mul i32 16, %84
  %add211 = add i32 %mul210, 2
  %arrayidx212 = getelementptr [128 x i64], [128 x i64]* %v209, i32 0, i32 %add211
  %85 = load i64, i64* %arrayidx212, align 8
  %v213 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %86 = load i32, i32* %i, align 4
  %mul214 = mul i32 16, %86
  %add215 = add i32 %mul214, 6
  %arrayidx216 = getelementptr [128 x i64], [128 x i64]* %v213, i32 0, i32 %add215
  %87 = load i64, i64* %arrayidx216, align 8
  %call217 = call i64 @fBlaMka(i64 %85, i64 %87)
  %v218 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %88 = load i32, i32* %i, align 4
  %mul219 = mul i32 16, %88
  %add220 = add i32 %mul219, 2
  %arrayidx221 = getelementptr [128 x i64], [128 x i64]* %v218, i32 0, i32 %add220
  store i64 %call217, i64* %arrayidx221, align 8
  %v222 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %89 = load i32, i32* %i, align 4
  %mul223 = mul i32 16, %89
  %add224 = add i32 %mul223, 14
  %arrayidx225 = getelementptr [128 x i64], [128 x i64]* %v222, i32 0, i32 %add224
  %90 = load i64, i64* %arrayidx225, align 8
  %v226 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %91 = load i32, i32* %i, align 4
  %mul227 = mul i32 16, %91
  %add228 = add i32 %mul227, 2
  %arrayidx229 = getelementptr [128 x i64], [128 x i64]* %v226, i32 0, i32 %add228
  %92 = load i64, i64* %arrayidx229, align 8
  %xor230 = xor i64 %90, %92
  %call231 = call i64 @rotr64(i64 %xor230, i32 32)
  %v232 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %93 = load i32, i32* %i, align 4
  %mul233 = mul i32 16, %93
  %add234 = add i32 %mul233, 14
  %arrayidx235 = getelementptr [128 x i64], [128 x i64]* %v232, i32 0, i32 %add234
  store i64 %call231, i64* %arrayidx235, align 8
  %v236 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %94 = load i32, i32* %i, align 4
  %mul237 = mul i32 16, %94
  %add238 = add i32 %mul237, 10
  %arrayidx239 = getelementptr [128 x i64], [128 x i64]* %v236, i32 0, i32 %add238
  %95 = load i64, i64* %arrayidx239, align 8
  %v240 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %96 = load i32, i32* %i, align 4
  %mul241 = mul i32 16, %96
  %add242 = add i32 %mul241, 14
  %arrayidx243 = getelementptr [128 x i64], [128 x i64]* %v240, i32 0, i32 %add242
  %97 = load i64, i64* %arrayidx243, align 8
  %call244 = call i64 @fBlaMka(i64 %95, i64 %97)
  %v245 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %98 = load i32, i32* %i, align 4
  %mul246 = mul i32 16, %98
  %add247 = add i32 %mul246, 10
  %arrayidx248 = getelementptr [128 x i64], [128 x i64]* %v245, i32 0, i32 %add247
  store i64 %call244, i64* %arrayidx248, align 8
  %v249 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %99 = load i32, i32* %i, align 4
  %mul250 = mul i32 16, %99
  %add251 = add i32 %mul250, 6
  %arrayidx252 = getelementptr [128 x i64], [128 x i64]* %v249, i32 0, i32 %add251
  %100 = load i64, i64* %arrayidx252, align 8
  %v253 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %101 = load i32, i32* %i, align 4
  %mul254 = mul i32 16, %101
  %add255 = add i32 %mul254, 10
  %arrayidx256 = getelementptr [128 x i64], [128 x i64]* %v253, i32 0, i32 %add255
  %102 = load i64, i64* %arrayidx256, align 8
  %xor257 = xor i64 %100, %102
  %call258 = call i64 @rotr64(i64 %xor257, i32 24)
  %v259 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %103 = load i32, i32* %i, align 4
  %mul260 = mul i32 16, %103
  %add261 = add i32 %mul260, 6
  %arrayidx262 = getelementptr [128 x i64], [128 x i64]* %v259, i32 0, i32 %add261
  store i64 %call258, i64* %arrayidx262, align 8
  %v263 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %104 = load i32, i32* %i, align 4
  %mul264 = mul i32 16, %104
  %add265 = add i32 %mul264, 2
  %arrayidx266 = getelementptr [128 x i64], [128 x i64]* %v263, i32 0, i32 %add265
  %105 = load i64, i64* %arrayidx266, align 8
  %v267 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %106 = load i32, i32* %i, align 4
  %mul268 = mul i32 16, %106
  %add269 = add i32 %mul268, 6
  %arrayidx270 = getelementptr [128 x i64], [128 x i64]* %v267, i32 0, i32 %add269
  %107 = load i64, i64* %arrayidx270, align 8
  %call271 = call i64 @fBlaMka(i64 %105, i64 %107)
  %v272 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %108 = load i32, i32* %i, align 4
  %mul273 = mul i32 16, %108
  %add274 = add i32 %mul273, 2
  %arrayidx275 = getelementptr [128 x i64], [128 x i64]* %v272, i32 0, i32 %add274
  store i64 %call271, i64* %arrayidx275, align 8
  %v276 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %109 = load i32, i32* %i, align 4
  %mul277 = mul i32 16, %109
  %add278 = add i32 %mul277, 14
  %arrayidx279 = getelementptr [128 x i64], [128 x i64]* %v276, i32 0, i32 %add278
  %110 = load i64, i64* %arrayidx279, align 8
  %v280 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %111 = load i32, i32* %i, align 4
  %mul281 = mul i32 16, %111
  %add282 = add i32 %mul281, 2
  %arrayidx283 = getelementptr [128 x i64], [128 x i64]* %v280, i32 0, i32 %add282
  %112 = load i64, i64* %arrayidx283, align 8
  %xor284 = xor i64 %110, %112
  %call285 = call i64 @rotr64(i64 %xor284, i32 16)
  %v286 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %113 = load i32, i32* %i, align 4
  %mul287 = mul i32 16, %113
  %add288 = add i32 %mul287, 14
  %arrayidx289 = getelementptr [128 x i64], [128 x i64]* %v286, i32 0, i32 %add288
  store i64 %call285, i64* %arrayidx289, align 8
  %v290 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %114 = load i32, i32* %i, align 4
  %mul291 = mul i32 16, %114
  %add292 = add i32 %mul291, 10
  %arrayidx293 = getelementptr [128 x i64], [128 x i64]* %v290, i32 0, i32 %add292
  %115 = load i64, i64* %arrayidx293, align 8
  %v294 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %116 = load i32, i32* %i, align 4
  %mul295 = mul i32 16, %116
  %add296 = add i32 %mul295, 14
  %arrayidx297 = getelementptr [128 x i64], [128 x i64]* %v294, i32 0, i32 %add296
  %117 = load i64, i64* %arrayidx297, align 8
  %call298 = call i64 @fBlaMka(i64 %115, i64 %117)
  %v299 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %118 = load i32, i32* %i, align 4
  %mul300 = mul i32 16, %118
  %add301 = add i32 %mul300, 10
  %arrayidx302 = getelementptr [128 x i64], [128 x i64]* %v299, i32 0, i32 %add301
  store i64 %call298, i64* %arrayidx302, align 8
  %v303 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %119 = load i32, i32* %i, align 4
  %mul304 = mul i32 16, %119
  %add305 = add i32 %mul304, 6
  %arrayidx306 = getelementptr [128 x i64], [128 x i64]* %v303, i32 0, i32 %add305
  %120 = load i64, i64* %arrayidx306, align 8
  %v307 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %121 = load i32, i32* %i, align 4
  %mul308 = mul i32 16, %121
  %add309 = add i32 %mul308, 10
  %arrayidx310 = getelementptr [128 x i64], [128 x i64]* %v307, i32 0, i32 %add309
  %122 = load i64, i64* %arrayidx310, align 8
  %xor311 = xor i64 %120, %122
  %call312 = call i64 @rotr64(i64 %xor311, i32 63)
  %v313 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %123 = load i32, i32* %i, align 4
  %mul314 = mul i32 16, %123
  %add315 = add i32 %mul314, 6
  %arrayidx316 = getelementptr [128 x i64], [128 x i64]* %v313, i32 0, i32 %add315
  store i64 %call312, i64* %arrayidx316, align 8
  br label %do.end317

do.end317:                                        ; preds = %do.body208
  br label %do.body318

do.body318:                                       ; preds = %do.end317
  %v319 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %124 = load i32, i32* %i, align 4
  %mul320 = mul i32 16, %124
  %add321 = add i32 %mul320, 3
  %arrayidx322 = getelementptr [128 x i64], [128 x i64]* %v319, i32 0, i32 %add321
  %125 = load i64, i64* %arrayidx322, align 8
  %v323 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %126 = load i32, i32* %i, align 4
  %mul324 = mul i32 16, %126
  %add325 = add i32 %mul324, 7
  %arrayidx326 = getelementptr [128 x i64], [128 x i64]* %v323, i32 0, i32 %add325
  %127 = load i64, i64* %arrayidx326, align 8
  %call327 = call i64 @fBlaMka(i64 %125, i64 %127)
  %v328 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %128 = load i32, i32* %i, align 4
  %mul329 = mul i32 16, %128
  %add330 = add i32 %mul329, 3
  %arrayidx331 = getelementptr [128 x i64], [128 x i64]* %v328, i32 0, i32 %add330
  store i64 %call327, i64* %arrayidx331, align 8
  %v332 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %129 = load i32, i32* %i, align 4
  %mul333 = mul i32 16, %129
  %add334 = add i32 %mul333, 15
  %arrayidx335 = getelementptr [128 x i64], [128 x i64]* %v332, i32 0, i32 %add334
  %130 = load i64, i64* %arrayidx335, align 8
  %v336 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %131 = load i32, i32* %i, align 4
  %mul337 = mul i32 16, %131
  %add338 = add i32 %mul337, 3
  %arrayidx339 = getelementptr [128 x i64], [128 x i64]* %v336, i32 0, i32 %add338
  %132 = load i64, i64* %arrayidx339, align 8
  %xor340 = xor i64 %130, %132
  %call341 = call i64 @rotr64(i64 %xor340, i32 32)
  %v342 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %133 = load i32, i32* %i, align 4
  %mul343 = mul i32 16, %133
  %add344 = add i32 %mul343, 15
  %arrayidx345 = getelementptr [128 x i64], [128 x i64]* %v342, i32 0, i32 %add344
  store i64 %call341, i64* %arrayidx345, align 8
  %v346 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %134 = load i32, i32* %i, align 4
  %mul347 = mul i32 16, %134
  %add348 = add i32 %mul347, 11
  %arrayidx349 = getelementptr [128 x i64], [128 x i64]* %v346, i32 0, i32 %add348
  %135 = load i64, i64* %arrayidx349, align 8
  %v350 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %136 = load i32, i32* %i, align 4
  %mul351 = mul i32 16, %136
  %add352 = add i32 %mul351, 15
  %arrayidx353 = getelementptr [128 x i64], [128 x i64]* %v350, i32 0, i32 %add352
  %137 = load i64, i64* %arrayidx353, align 8
  %call354 = call i64 @fBlaMka(i64 %135, i64 %137)
  %v355 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %138 = load i32, i32* %i, align 4
  %mul356 = mul i32 16, %138
  %add357 = add i32 %mul356, 11
  %arrayidx358 = getelementptr [128 x i64], [128 x i64]* %v355, i32 0, i32 %add357
  store i64 %call354, i64* %arrayidx358, align 8
  %v359 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %139 = load i32, i32* %i, align 4
  %mul360 = mul i32 16, %139
  %add361 = add i32 %mul360, 7
  %arrayidx362 = getelementptr [128 x i64], [128 x i64]* %v359, i32 0, i32 %add361
  %140 = load i64, i64* %arrayidx362, align 8
  %v363 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %141 = load i32, i32* %i, align 4
  %mul364 = mul i32 16, %141
  %add365 = add i32 %mul364, 11
  %arrayidx366 = getelementptr [128 x i64], [128 x i64]* %v363, i32 0, i32 %add365
  %142 = load i64, i64* %arrayidx366, align 8
  %xor367 = xor i64 %140, %142
  %call368 = call i64 @rotr64(i64 %xor367, i32 24)
  %v369 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %143 = load i32, i32* %i, align 4
  %mul370 = mul i32 16, %143
  %add371 = add i32 %mul370, 7
  %arrayidx372 = getelementptr [128 x i64], [128 x i64]* %v369, i32 0, i32 %add371
  store i64 %call368, i64* %arrayidx372, align 8
  %v373 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %144 = load i32, i32* %i, align 4
  %mul374 = mul i32 16, %144
  %add375 = add i32 %mul374, 3
  %arrayidx376 = getelementptr [128 x i64], [128 x i64]* %v373, i32 0, i32 %add375
  %145 = load i64, i64* %arrayidx376, align 8
  %v377 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %146 = load i32, i32* %i, align 4
  %mul378 = mul i32 16, %146
  %add379 = add i32 %mul378, 7
  %arrayidx380 = getelementptr [128 x i64], [128 x i64]* %v377, i32 0, i32 %add379
  %147 = load i64, i64* %arrayidx380, align 8
  %call381 = call i64 @fBlaMka(i64 %145, i64 %147)
  %v382 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %148 = load i32, i32* %i, align 4
  %mul383 = mul i32 16, %148
  %add384 = add i32 %mul383, 3
  %arrayidx385 = getelementptr [128 x i64], [128 x i64]* %v382, i32 0, i32 %add384
  store i64 %call381, i64* %arrayidx385, align 8
  %v386 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %149 = load i32, i32* %i, align 4
  %mul387 = mul i32 16, %149
  %add388 = add i32 %mul387, 15
  %arrayidx389 = getelementptr [128 x i64], [128 x i64]* %v386, i32 0, i32 %add388
  %150 = load i64, i64* %arrayidx389, align 8
  %v390 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %151 = load i32, i32* %i, align 4
  %mul391 = mul i32 16, %151
  %add392 = add i32 %mul391, 3
  %arrayidx393 = getelementptr [128 x i64], [128 x i64]* %v390, i32 0, i32 %add392
  %152 = load i64, i64* %arrayidx393, align 8
  %xor394 = xor i64 %150, %152
  %call395 = call i64 @rotr64(i64 %xor394, i32 16)
  %v396 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %153 = load i32, i32* %i, align 4
  %mul397 = mul i32 16, %153
  %add398 = add i32 %mul397, 15
  %arrayidx399 = getelementptr [128 x i64], [128 x i64]* %v396, i32 0, i32 %add398
  store i64 %call395, i64* %arrayidx399, align 8
  %v400 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %154 = load i32, i32* %i, align 4
  %mul401 = mul i32 16, %154
  %add402 = add i32 %mul401, 11
  %arrayidx403 = getelementptr [128 x i64], [128 x i64]* %v400, i32 0, i32 %add402
  %155 = load i64, i64* %arrayidx403, align 8
  %v404 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %156 = load i32, i32* %i, align 4
  %mul405 = mul i32 16, %156
  %add406 = add i32 %mul405, 15
  %arrayidx407 = getelementptr [128 x i64], [128 x i64]* %v404, i32 0, i32 %add406
  %157 = load i64, i64* %arrayidx407, align 8
  %call408 = call i64 @fBlaMka(i64 %155, i64 %157)
  %v409 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %158 = load i32, i32* %i, align 4
  %mul410 = mul i32 16, %158
  %add411 = add i32 %mul410, 11
  %arrayidx412 = getelementptr [128 x i64], [128 x i64]* %v409, i32 0, i32 %add411
  store i64 %call408, i64* %arrayidx412, align 8
  %v413 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %159 = load i32, i32* %i, align 4
  %mul414 = mul i32 16, %159
  %add415 = add i32 %mul414, 7
  %arrayidx416 = getelementptr [128 x i64], [128 x i64]* %v413, i32 0, i32 %add415
  %160 = load i64, i64* %arrayidx416, align 8
  %v417 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %161 = load i32, i32* %i, align 4
  %mul418 = mul i32 16, %161
  %add419 = add i32 %mul418, 11
  %arrayidx420 = getelementptr [128 x i64], [128 x i64]* %v417, i32 0, i32 %add419
  %162 = load i64, i64* %arrayidx420, align 8
  %xor421 = xor i64 %160, %162
  %call422 = call i64 @rotr64(i64 %xor421, i32 63)
  %v423 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %163 = load i32, i32* %i, align 4
  %mul424 = mul i32 16, %163
  %add425 = add i32 %mul424, 7
  %arrayidx426 = getelementptr [128 x i64], [128 x i64]* %v423, i32 0, i32 %add425
  store i64 %call422, i64* %arrayidx426, align 8
  br label %do.end427

do.end427:                                        ; preds = %do.body318
  br label %do.body428

do.body428:                                       ; preds = %do.end427
  %v429 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %164 = load i32, i32* %i, align 4
  %mul430 = mul i32 16, %164
  %arrayidx431 = getelementptr [128 x i64], [128 x i64]* %v429, i32 0, i32 %mul430
  %165 = load i64, i64* %arrayidx431, align 8
  %v432 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %166 = load i32, i32* %i, align 4
  %mul433 = mul i32 16, %166
  %add434 = add i32 %mul433, 5
  %arrayidx435 = getelementptr [128 x i64], [128 x i64]* %v432, i32 0, i32 %add434
  %167 = load i64, i64* %arrayidx435, align 8
  %call436 = call i64 @fBlaMka(i64 %165, i64 %167)
  %v437 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %168 = load i32, i32* %i, align 4
  %mul438 = mul i32 16, %168
  %arrayidx439 = getelementptr [128 x i64], [128 x i64]* %v437, i32 0, i32 %mul438
  store i64 %call436, i64* %arrayidx439, align 8
  %v440 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %169 = load i32, i32* %i, align 4
  %mul441 = mul i32 16, %169
  %add442 = add i32 %mul441, 15
  %arrayidx443 = getelementptr [128 x i64], [128 x i64]* %v440, i32 0, i32 %add442
  %170 = load i64, i64* %arrayidx443, align 8
  %v444 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %171 = load i32, i32* %i, align 4
  %mul445 = mul i32 16, %171
  %arrayidx446 = getelementptr [128 x i64], [128 x i64]* %v444, i32 0, i32 %mul445
  %172 = load i64, i64* %arrayidx446, align 8
  %xor447 = xor i64 %170, %172
  %call448 = call i64 @rotr64(i64 %xor447, i32 32)
  %v449 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %173 = load i32, i32* %i, align 4
  %mul450 = mul i32 16, %173
  %add451 = add i32 %mul450, 15
  %arrayidx452 = getelementptr [128 x i64], [128 x i64]* %v449, i32 0, i32 %add451
  store i64 %call448, i64* %arrayidx452, align 8
  %v453 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %174 = load i32, i32* %i, align 4
  %mul454 = mul i32 16, %174
  %add455 = add i32 %mul454, 10
  %arrayidx456 = getelementptr [128 x i64], [128 x i64]* %v453, i32 0, i32 %add455
  %175 = load i64, i64* %arrayidx456, align 8
  %v457 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %176 = load i32, i32* %i, align 4
  %mul458 = mul i32 16, %176
  %add459 = add i32 %mul458, 15
  %arrayidx460 = getelementptr [128 x i64], [128 x i64]* %v457, i32 0, i32 %add459
  %177 = load i64, i64* %arrayidx460, align 8
  %call461 = call i64 @fBlaMka(i64 %175, i64 %177)
  %v462 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %178 = load i32, i32* %i, align 4
  %mul463 = mul i32 16, %178
  %add464 = add i32 %mul463, 10
  %arrayidx465 = getelementptr [128 x i64], [128 x i64]* %v462, i32 0, i32 %add464
  store i64 %call461, i64* %arrayidx465, align 8
  %v466 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %179 = load i32, i32* %i, align 4
  %mul467 = mul i32 16, %179
  %add468 = add i32 %mul467, 5
  %arrayidx469 = getelementptr [128 x i64], [128 x i64]* %v466, i32 0, i32 %add468
  %180 = load i64, i64* %arrayidx469, align 8
  %v470 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %181 = load i32, i32* %i, align 4
  %mul471 = mul i32 16, %181
  %add472 = add i32 %mul471, 10
  %arrayidx473 = getelementptr [128 x i64], [128 x i64]* %v470, i32 0, i32 %add472
  %182 = load i64, i64* %arrayidx473, align 8
  %xor474 = xor i64 %180, %182
  %call475 = call i64 @rotr64(i64 %xor474, i32 24)
  %v476 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %183 = load i32, i32* %i, align 4
  %mul477 = mul i32 16, %183
  %add478 = add i32 %mul477, 5
  %arrayidx479 = getelementptr [128 x i64], [128 x i64]* %v476, i32 0, i32 %add478
  store i64 %call475, i64* %arrayidx479, align 8
  %v480 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %184 = load i32, i32* %i, align 4
  %mul481 = mul i32 16, %184
  %arrayidx482 = getelementptr [128 x i64], [128 x i64]* %v480, i32 0, i32 %mul481
  %185 = load i64, i64* %arrayidx482, align 8
  %v483 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %186 = load i32, i32* %i, align 4
  %mul484 = mul i32 16, %186
  %add485 = add i32 %mul484, 5
  %arrayidx486 = getelementptr [128 x i64], [128 x i64]* %v483, i32 0, i32 %add485
  %187 = load i64, i64* %arrayidx486, align 8
  %call487 = call i64 @fBlaMka(i64 %185, i64 %187)
  %v488 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %188 = load i32, i32* %i, align 4
  %mul489 = mul i32 16, %188
  %arrayidx490 = getelementptr [128 x i64], [128 x i64]* %v488, i32 0, i32 %mul489
  store i64 %call487, i64* %arrayidx490, align 8
  %v491 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %189 = load i32, i32* %i, align 4
  %mul492 = mul i32 16, %189
  %add493 = add i32 %mul492, 15
  %arrayidx494 = getelementptr [128 x i64], [128 x i64]* %v491, i32 0, i32 %add493
  %190 = load i64, i64* %arrayidx494, align 8
  %v495 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %191 = load i32, i32* %i, align 4
  %mul496 = mul i32 16, %191
  %arrayidx497 = getelementptr [128 x i64], [128 x i64]* %v495, i32 0, i32 %mul496
  %192 = load i64, i64* %arrayidx497, align 8
  %xor498 = xor i64 %190, %192
  %call499 = call i64 @rotr64(i64 %xor498, i32 16)
  %v500 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %193 = load i32, i32* %i, align 4
  %mul501 = mul i32 16, %193
  %add502 = add i32 %mul501, 15
  %arrayidx503 = getelementptr [128 x i64], [128 x i64]* %v500, i32 0, i32 %add502
  store i64 %call499, i64* %arrayidx503, align 8
  %v504 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %194 = load i32, i32* %i, align 4
  %mul505 = mul i32 16, %194
  %add506 = add i32 %mul505, 10
  %arrayidx507 = getelementptr [128 x i64], [128 x i64]* %v504, i32 0, i32 %add506
  %195 = load i64, i64* %arrayidx507, align 8
  %v508 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %196 = load i32, i32* %i, align 4
  %mul509 = mul i32 16, %196
  %add510 = add i32 %mul509, 15
  %arrayidx511 = getelementptr [128 x i64], [128 x i64]* %v508, i32 0, i32 %add510
  %197 = load i64, i64* %arrayidx511, align 8
  %call512 = call i64 @fBlaMka(i64 %195, i64 %197)
  %v513 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %198 = load i32, i32* %i, align 4
  %mul514 = mul i32 16, %198
  %add515 = add i32 %mul514, 10
  %arrayidx516 = getelementptr [128 x i64], [128 x i64]* %v513, i32 0, i32 %add515
  store i64 %call512, i64* %arrayidx516, align 8
  %v517 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %199 = load i32, i32* %i, align 4
  %mul518 = mul i32 16, %199
  %add519 = add i32 %mul518, 5
  %arrayidx520 = getelementptr [128 x i64], [128 x i64]* %v517, i32 0, i32 %add519
  %200 = load i64, i64* %arrayidx520, align 8
  %v521 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %201 = load i32, i32* %i, align 4
  %mul522 = mul i32 16, %201
  %add523 = add i32 %mul522, 10
  %arrayidx524 = getelementptr [128 x i64], [128 x i64]* %v521, i32 0, i32 %add523
  %202 = load i64, i64* %arrayidx524, align 8
  %xor525 = xor i64 %200, %202
  %call526 = call i64 @rotr64(i64 %xor525, i32 63)
  %v527 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %203 = load i32, i32* %i, align 4
  %mul528 = mul i32 16, %203
  %add529 = add i32 %mul528, 5
  %arrayidx530 = getelementptr [128 x i64], [128 x i64]* %v527, i32 0, i32 %add529
  store i64 %call526, i64* %arrayidx530, align 8
  br label %do.end531

do.end531:                                        ; preds = %do.body428
  br label %do.body532

do.body532:                                       ; preds = %do.end531
  %v533 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %204 = load i32, i32* %i, align 4
  %mul534 = mul i32 16, %204
  %add535 = add i32 %mul534, 1
  %arrayidx536 = getelementptr [128 x i64], [128 x i64]* %v533, i32 0, i32 %add535
  %205 = load i64, i64* %arrayidx536, align 8
  %v537 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %206 = load i32, i32* %i, align 4
  %mul538 = mul i32 16, %206
  %add539 = add i32 %mul538, 6
  %arrayidx540 = getelementptr [128 x i64], [128 x i64]* %v537, i32 0, i32 %add539
  %207 = load i64, i64* %arrayidx540, align 8
  %call541 = call i64 @fBlaMka(i64 %205, i64 %207)
  %v542 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %208 = load i32, i32* %i, align 4
  %mul543 = mul i32 16, %208
  %add544 = add i32 %mul543, 1
  %arrayidx545 = getelementptr [128 x i64], [128 x i64]* %v542, i32 0, i32 %add544
  store i64 %call541, i64* %arrayidx545, align 8
  %v546 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %209 = load i32, i32* %i, align 4
  %mul547 = mul i32 16, %209
  %add548 = add i32 %mul547, 12
  %arrayidx549 = getelementptr [128 x i64], [128 x i64]* %v546, i32 0, i32 %add548
  %210 = load i64, i64* %arrayidx549, align 8
  %v550 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %211 = load i32, i32* %i, align 4
  %mul551 = mul i32 16, %211
  %add552 = add i32 %mul551, 1
  %arrayidx553 = getelementptr [128 x i64], [128 x i64]* %v550, i32 0, i32 %add552
  %212 = load i64, i64* %arrayidx553, align 8
  %xor554 = xor i64 %210, %212
  %call555 = call i64 @rotr64(i64 %xor554, i32 32)
  %v556 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %213 = load i32, i32* %i, align 4
  %mul557 = mul i32 16, %213
  %add558 = add i32 %mul557, 12
  %arrayidx559 = getelementptr [128 x i64], [128 x i64]* %v556, i32 0, i32 %add558
  store i64 %call555, i64* %arrayidx559, align 8
  %v560 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %214 = load i32, i32* %i, align 4
  %mul561 = mul i32 16, %214
  %add562 = add i32 %mul561, 11
  %arrayidx563 = getelementptr [128 x i64], [128 x i64]* %v560, i32 0, i32 %add562
  %215 = load i64, i64* %arrayidx563, align 8
  %v564 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %216 = load i32, i32* %i, align 4
  %mul565 = mul i32 16, %216
  %add566 = add i32 %mul565, 12
  %arrayidx567 = getelementptr [128 x i64], [128 x i64]* %v564, i32 0, i32 %add566
  %217 = load i64, i64* %arrayidx567, align 8
  %call568 = call i64 @fBlaMka(i64 %215, i64 %217)
  %v569 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %218 = load i32, i32* %i, align 4
  %mul570 = mul i32 16, %218
  %add571 = add i32 %mul570, 11
  %arrayidx572 = getelementptr [128 x i64], [128 x i64]* %v569, i32 0, i32 %add571
  store i64 %call568, i64* %arrayidx572, align 8
  %v573 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %219 = load i32, i32* %i, align 4
  %mul574 = mul i32 16, %219
  %add575 = add i32 %mul574, 6
  %arrayidx576 = getelementptr [128 x i64], [128 x i64]* %v573, i32 0, i32 %add575
  %220 = load i64, i64* %arrayidx576, align 8
  %v577 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %221 = load i32, i32* %i, align 4
  %mul578 = mul i32 16, %221
  %add579 = add i32 %mul578, 11
  %arrayidx580 = getelementptr [128 x i64], [128 x i64]* %v577, i32 0, i32 %add579
  %222 = load i64, i64* %arrayidx580, align 8
  %xor581 = xor i64 %220, %222
  %call582 = call i64 @rotr64(i64 %xor581, i32 24)
  %v583 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %223 = load i32, i32* %i, align 4
  %mul584 = mul i32 16, %223
  %add585 = add i32 %mul584, 6
  %arrayidx586 = getelementptr [128 x i64], [128 x i64]* %v583, i32 0, i32 %add585
  store i64 %call582, i64* %arrayidx586, align 8
  %v587 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %224 = load i32, i32* %i, align 4
  %mul588 = mul i32 16, %224
  %add589 = add i32 %mul588, 1
  %arrayidx590 = getelementptr [128 x i64], [128 x i64]* %v587, i32 0, i32 %add589
  %225 = load i64, i64* %arrayidx590, align 8
  %v591 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %226 = load i32, i32* %i, align 4
  %mul592 = mul i32 16, %226
  %add593 = add i32 %mul592, 6
  %arrayidx594 = getelementptr [128 x i64], [128 x i64]* %v591, i32 0, i32 %add593
  %227 = load i64, i64* %arrayidx594, align 8
  %call595 = call i64 @fBlaMka(i64 %225, i64 %227)
  %v596 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %228 = load i32, i32* %i, align 4
  %mul597 = mul i32 16, %228
  %add598 = add i32 %mul597, 1
  %arrayidx599 = getelementptr [128 x i64], [128 x i64]* %v596, i32 0, i32 %add598
  store i64 %call595, i64* %arrayidx599, align 8
  %v600 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %229 = load i32, i32* %i, align 4
  %mul601 = mul i32 16, %229
  %add602 = add i32 %mul601, 12
  %arrayidx603 = getelementptr [128 x i64], [128 x i64]* %v600, i32 0, i32 %add602
  %230 = load i64, i64* %arrayidx603, align 8
  %v604 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %231 = load i32, i32* %i, align 4
  %mul605 = mul i32 16, %231
  %add606 = add i32 %mul605, 1
  %arrayidx607 = getelementptr [128 x i64], [128 x i64]* %v604, i32 0, i32 %add606
  %232 = load i64, i64* %arrayidx607, align 8
  %xor608 = xor i64 %230, %232
  %call609 = call i64 @rotr64(i64 %xor608, i32 16)
  %v610 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %233 = load i32, i32* %i, align 4
  %mul611 = mul i32 16, %233
  %add612 = add i32 %mul611, 12
  %arrayidx613 = getelementptr [128 x i64], [128 x i64]* %v610, i32 0, i32 %add612
  store i64 %call609, i64* %arrayidx613, align 8
  %v614 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %234 = load i32, i32* %i, align 4
  %mul615 = mul i32 16, %234
  %add616 = add i32 %mul615, 11
  %arrayidx617 = getelementptr [128 x i64], [128 x i64]* %v614, i32 0, i32 %add616
  %235 = load i64, i64* %arrayidx617, align 8
  %v618 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %236 = load i32, i32* %i, align 4
  %mul619 = mul i32 16, %236
  %add620 = add i32 %mul619, 12
  %arrayidx621 = getelementptr [128 x i64], [128 x i64]* %v618, i32 0, i32 %add620
  %237 = load i64, i64* %arrayidx621, align 8
  %call622 = call i64 @fBlaMka(i64 %235, i64 %237)
  %v623 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %238 = load i32, i32* %i, align 4
  %mul624 = mul i32 16, %238
  %add625 = add i32 %mul624, 11
  %arrayidx626 = getelementptr [128 x i64], [128 x i64]* %v623, i32 0, i32 %add625
  store i64 %call622, i64* %arrayidx626, align 8
  %v627 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %239 = load i32, i32* %i, align 4
  %mul628 = mul i32 16, %239
  %add629 = add i32 %mul628, 6
  %arrayidx630 = getelementptr [128 x i64], [128 x i64]* %v627, i32 0, i32 %add629
  %240 = load i64, i64* %arrayidx630, align 8
  %v631 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %241 = load i32, i32* %i, align 4
  %mul632 = mul i32 16, %241
  %add633 = add i32 %mul632, 11
  %arrayidx634 = getelementptr [128 x i64], [128 x i64]* %v631, i32 0, i32 %add633
  %242 = load i64, i64* %arrayidx634, align 8
  %xor635 = xor i64 %240, %242
  %call636 = call i64 @rotr64(i64 %xor635, i32 63)
  %v637 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %243 = load i32, i32* %i, align 4
  %mul638 = mul i32 16, %243
  %add639 = add i32 %mul638, 6
  %arrayidx640 = getelementptr [128 x i64], [128 x i64]* %v637, i32 0, i32 %add639
  store i64 %call636, i64* %arrayidx640, align 8
  br label %do.end641

do.end641:                                        ; preds = %do.body532
  br label %do.body642

do.body642:                                       ; preds = %do.end641
  %v643 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %244 = load i32, i32* %i, align 4
  %mul644 = mul i32 16, %244
  %add645 = add i32 %mul644, 2
  %arrayidx646 = getelementptr [128 x i64], [128 x i64]* %v643, i32 0, i32 %add645
  %245 = load i64, i64* %arrayidx646, align 8
  %v647 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %246 = load i32, i32* %i, align 4
  %mul648 = mul i32 16, %246
  %add649 = add i32 %mul648, 7
  %arrayidx650 = getelementptr [128 x i64], [128 x i64]* %v647, i32 0, i32 %add649
  %247 = load i64, i64* %arrayidx650, align 8
  %call651 = call i64 @fBlaMka(i64 %245, i64 %247)
  %v652 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %248 = load i32, i32* %i, align 4
  %mul653 = mul i32 16, %248
  %add654 = add i32 %mul653, 2
  %arrayidx655 = getelementptr [128 x i64], [128 x i64]* %v652, i32 0, i32 %add654
  store i64 %call651, i64* %arrayidx655, align 8
  %v656 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %249 = load i32, i32* %i, align 4
  %mul657 = mul i32 16, %249
  %add658 = add i32 %mul657, 13
  %arrayidx659 = getelementptr [128 x i64], [128 x i64]* %v656, i32 0, i32 %add658
  %250 = load i64, i64* %arrayidx659, align 8
  %v660 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %251 = load i32, i32* %i, align 4
  %mul661 = mul i32 16, %251
  %add662 = add i32 %mul661, 2
  %arrayidx663 = getelementptr [128 x i64], [128 x i64]* %v660, i32 0, i32 %add662
  %252 = load i64, i64* %arrayidx663, align 8
  %xor664 = xor i64 %250, %252
  %call665 = call i64 @rotr64(i64 %xor664, i32 32)
  %v666 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %253 = load i32, i32* %i, align 4
  %mul667 = mul i32 16, %253
  %add668 = add i32 %mul667, 13
  %arrayidx669 = getelementptr [128 x i64], [128 x i64]* %v666, i32 0, i32 %add668
  store i64 %call665, i64* %arrayidx669, align 8
  %v670 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %254 = load i32, i32* %i, align 4
  %mul671 = mul i32 16, %254
  %add672 = add i32 %mul671, 8
  %arrayidx673 = getelementptr [128 x i64], [128 x i64]* %v670, i32 0, i32 %add672
  %255 = load i64, i64* %arrayidx673, align 8
  %v674 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %256 = load i32, i32* %i, align 4
  %mul675 = mul i32 16, %256
  %add676 = add i32 %mul675, 13
  %arrayidx677 = getelementptr [128 x i64], [128 x i64]* %v674, i32 0, i32 %add676
  %257 = load i64, i64* %arrayidx677, align 8
  %call678 = call i64 @fBlaMka(i64 %255, i64 %257)
  %v679 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %258 = load i32, i32* %i, align 4
  %mul680 = mul i32 16, %258
  %add681 = add i32 %mul680, 8
  %arrayidx682 = getelementptr [128 x i64], [128 x i64]* %v679, i32 0, i32 %add681
  store i64 %call678, i64* %arrayidx682, align 8
  %v683 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %259 = load i32, i32* %i, align 4
  %mul684 = mul i32 16, %259
  %add685 = add i32 %mul684, 7
  %arrayidx686 = getelementptr [128 x i64], [128 x i64]* %v683, i32 0, i32 %add685
  %260 = load i64, i64* %arrayidx686, align 8
  %v687 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %261 = load i32, i32* %i, align 4
  %mul688 = mul i32 16, %261
  %add689 = add i32 %mul688, 8
  %arrayidx690 = getelementptr [128 x i64], [128 x i64]* %v687, i32 0, i32 %add689
  %262 = load i64, i64* %arrayidx690, align 8
  %xor691 = xor i64 %260, %262
  %call692 = call i64 @rotr64(i64 %xor691, i32 24)
  %v693 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %263 = load i32, i32* %i, align 4
  %mul694 = mul i32 16, %263
  %add695 = add i32 %mul694, 7
  %arrayidx696 = getelementptr [128 x i64], [128 x i64]* %v693, i32 0, i32 %add695
  store i64 %call692, i64* %arrayidx696, align 8
  %v697 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %264 = load i32, i32* %i, align 4
  %mul698 = mul i32 16, %264
  %add699 = add i32 %mul698, 2
  %arrayidx700 = getelementptr [128 x i64], [128 x i64]* %v697, i32 0, i32 %add699
  %265 = load i64, i64* %arrayidx700, align 8
  %v701 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %266 = load i32, i32* %i, align 4
  %mul702 = mul i32 16, %266
  %add703 = add i32 %mul702, 7
  %arrayidx704 = getelementptr [128 x i64], [128 x i64]* %v701, i32 0, i32 %add703
  %267 = load i64, i64* %arrayidx704, align 8
  %call705 = call i64 @fBlaMka(i64 %265, i64 %267)
  %v706 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %268 = load i32, i32* %i, align 4
  %mul707 = mul i32 16, %268
  %add708 = add i32 %mul707, 2
  %arrayidx709 = getelementptr [128 x i64], [128 x i64]* %v706, i32 0, i32 %add708
  store i64 %call705, i64* %arrayidx709, align 8
  %v710 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %269 = load i32, i32* %i, align 4
  %mul711 = mul i32 16, %269
  %add712 = add i32 %mul711, 13
  %arrayidx713 = getelementptr [128 x i64], [128 x i64]* %v710, i32 0, i32 %add712
  %270 = load i64, i64* %arrayidx713, align 8
  %v714 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %271 = load i32, i32* %i, align 4
  %mul715 = mul i32 16, %271
  %add716 = add i32 %mul715, 2
  %arrayidx717 = getelementptr [128 x i64], [128 x i64]* %v714, i32 0, i32 %add716
  %272 = load i64, i64* %arrayidx717, align 8
  %xor718 = xor i64 %270, %272
  %call719 = call i64 @rotr64(i64 %xor718, i32 16)
  %v720 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %273 = load i32, i32* %i, align 4
  %mul721 = mul i32 16, %273
  %add722 = add i32 %mul721, 13
  %arrayidx723 = getelementptr [128 x i64], [128 x i64]* %v720, i32 0, i32 %add722
  store i64 %call719, i64* %arrayidx723, align 8
  %v724 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %274 = load i32, i32* %i, align 4
  %mul725 = mul i32 16, %274
  %add726 = add i32 %mul725, 8
  %arrayidx727 = getelementptr [128 x i64], [128 x i64]* %v724, i32 0, i32 %add726
  %275 = load i64, i64* %arrayidx727, align 8
  %v728 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %276 = load i32, i32* %i, align 4
  %mul729 = mul i32 16, %276
  %add730 = add i32 %mul729, 13
  %arrayidx731 = getelementptr [128 x i64], [128 x i64]* %v728, i32 0, i32 %add730
  %277 = load i64, i64* %arrayidx731, align 8
  %call732 = call i64 @fBlaMka(i64 %275, i64 %277)
  %v733 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %278 = load i32, i32* %i, align 4
  %mul734 = mul i32 16, %278
  %add735 = add i32 %mul734, 8
  %arrayidx736 = getelementptr [128 x i64], [128 x i64]* %v733, i32 0, i32 %add735
  store i64 %call732, i64* %arrayidx736, align 8
  %v737 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %279 = load i32, i32* %i, align 4
  %mul738 = mul i32 16, %279
  %add739 = add i32 %mul738, 7
  %arrayidx740 = getelementptr [128 x i64], [128 x i64]* %v737, i32 0, i32 %add739
  %280 = load i64, i64* %arrayidx740, align 8
  %v741 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %281 = load i32, i32* %i, align 4
  %mul742 = mul i32 16, %281
  %add743 = add i32 %mul742, 8
  %arrayidx744 = getelementptr [128 x i64], [128 x i64]* %v741, i32 0, i32 %add743
  %282 = load i64, i64* %arrayidx744, align 8
  %xor745 = xor i64 %280, %282
  %call746 = call i64 @rotr64(i64 %xor745, i32 63)
  %v747 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %283 = load i32, i32* %i, align 4
  %mul748 = mul i32 16, %283
  %add749 = add i32 %mul748, 7
  %arrayidx750 = getelementptr [128 x i64], [128 x i64]* %v747, i32 0, i32 %add749
  store i64 %call746, i64* %arrayidx750, align 8
  br label %do.end751

do.end751:                                        ; preds = %do.body642
  br label %do.body752

do.body752:                                       ; preds = %do.end751
  %v753 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %284 = load i32, i32* %i, align 4
  %mul754 = mul i32 16, %284
  %add755 = add i32 %mul754, 3
  %arrayidx756 = getelementptr [128 x i64], [128 x i64]* %v753, i32 0, i32 %add755
  %285 = load i64, i64* %arrayidx756, align 8
  %v757 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %286 = load i32, i32* %i, align 4
  %mul758 = mul i32 16, %286
  %add759 = add i32 %mul758, 4
  %arrayidx760 = getelementptr [128 x i64], [128 x i64]* %v757, i32 0, i32 %add759
  %287 = load i64, i64* %arrayidx760, align 8
  %call761 = call i64 @fBlaMka(i64 %285, i64 %287)
  %v762 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %288 = load i32, i32* %i, align 4
  %mul763 = mul i32 16, %288
  %add764 = add i32 %mul763, 3
  %arrayidx765 = getelementptr [128 x i64], [128 x i64]* %v762, i32 0, i32 %add764
  store i64 %call761, i64* %arrayidx765, align 8
  %v766 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %289 = load i32, i32* %i, align 4
  %mul767 = mul i32 16, %289
  %add768 = add i32 %mul767, 14
  %arrayidx769 = getelementptr [128 x i64], [128 x i64]* %v766, i32 0, i32 %add768
  %290 = load i64, i64* %arrayidx769, align 8
  %v770 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %291 = load i32, i32* %i, align 4
  %mul771 = mul i32 16, %291
  %add772 = add i32 %mul771, 3
  %arrayidx773 = getelementptr [128 x i64], [128 x i64]* %v770, i32 0, i32 %add772
  %292 = load i64, i64* %arrayidx773, align 8
  %xor774 = xor i64 %290, %292
  %call775 = call i64 @rotr64(i64 %xor774, i32 32)
  %v776 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %293 = load i32, i32* %i, align 4
  %mul777 = mul i32 16, %293
  %add778 = add i32 %mul777, 14
  %arrayidx779 = getelementptr [128 x i64], [128 x i64]* %v776, i32 0, i32 %add778
  store i64 %call775, i64* %arrayidx779, align 8
  %v780 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %294 = load i32, i32* %i, align 4
  %mul781 = mul i32 16, %294
  %add782 = add i32 %mul781, 9
  %arrayidx783 = getelementptr [128 x i64], [128 x i64]* %v780, i32 0, i32 %add782
  %295 = load i64, i64* %arrayidx783, align 8
  %v784 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %296 = load i32, i32* %i, align 4
  %mul785 = mul i32 16, %296
  %add786 = add i32 %mul785, 14
  %arrayidx787 = getelementptr [128 x i64], [128 x i64]* %v784, i32 0, i32 %add786
  %297 = load i64, i64* %arrayidx787, align 8
  %call788 = call i64 @fBlaMka(i64 %295, i64 %297)
  %v789 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %298 = load i32, i32* %i, align 4
  %mul790 = mul i32 16, %298
  %add791 = add i32 %mul790, 9
  %arrayidx792 = getelementptr [128 x i64], [128 x i64]* %v789, i32 0, i32 %add791
  store i64 %call788, i64* %arrayidx792, align 8
  %v793 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %299 = load i32, i32* %i, align 4
  %mul794 = mul i32 16, %299
  %add795 = add i32 %mul794, 4
  %arrayidx796 = getelementptr [128 x i64], [128 x i64]* %v793, i32 0, i32 %add795
  %300 = load i64, i64* %arrayidx796, align 8
  %v797 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %301 = load i32, i32* %i, align 4
  %mul798 = mul i32 16, %301
  %add799 = add i32 %mul798, 9
  %arrayidx800 = getelementptr [128 x i64], [128 x i64]* %v797, i32 0, i32 %add799
  %302 = load i64, i64* %arrayidx800, align 8
  %xor801 = xor i64 %300, %302
  %call802 = call i64 @rotr64(i64 %xor801, i32 24)
  %v803 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %303 = load i32, i32* %i, align 4
  %mul804 = mul i32 16, %303
  %add805 = add i32 %mul804, 4
  %arrayidx806 = getelementptr [128 x i64], [128 x i64]* %v803, i32 0, i32 %add805
  store i64 %call802, i64* %arrayidx806, align 8
  %v807 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %304 = load i32, i32* %i, align 4
  %mul808 = mul i32 16, %304
  %add809 = add i32 %mul808, 3
  %arrayidx810 = getelementptr [128 x i64], [128 x i64]* %v807, i32 0, i32 %add809
  %305 = load i64, i64* %arrayidx810, align 8
  %v811 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %306 = load i32, i32* %i, align 4
  %mul812 = mul i32 16, %306
  %add813 = add i32 %mul812, 4
  %arrayidx814 = getelementptr [128 x i64], [128 x i64]* %v811, i32 0, i32 %add813
  %307 = load i64, i64* %arrayidx814, align 8
  %call815 = call i64 @fBlaMka(i64 %305, i64 %307)
  %v816 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %308 = load i32, i32* %i, align 4
  %mul817 = mul i32 16, %308
  %add818 = add i32 %mul817, 3
  %arrayidx819 = getelementptr [128 x i64], [128 x i64]* %v816, i32 0, i32 %add818
  store i64 %call815, i64* %arrayidx819, align 8
  %v820 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %309 = load i32, i32* %i, align 4
  %mul821 = mul i32 16, %309
  %add822 = add i32 %mul821, 14
  %arrayidx823 = getelementptr [128 x i64], [128 x i64]* %v820, i32 0, i32 %add822
  %310 = load i64, i64* %arrayidx823, align 8
  %v824 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %311 = load i32, i32* %i, align 4
  %mul825 = mul i32 16, %311
  %add826 = add i32 %mul825, 3
  %arrayidx827 = getelementptr [128 x i64], [128 x i64]* %v824, i32 0, i32 %add826
  %312 = load i64, i64* %arrayidx827, align 8
  %xor828 = xor i64 %310, %312
  %call829 = call i64 @rotr64(i64 %xor828, i32 16)
  %v830 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %313 = load i32, i32* %i, align 4
  %mul831 = mul i32 16, %313
  %add832 = add i32 %mul831, 14
  %arrayidx833 = getelementptr [128 x i64], [128 x i64]* %v830, i32 0, i32 %add832
  store i64 %call829, i64* %arrayidx833, align 8
  %v834 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %314 = load i32, i32* %i, align 4
  %mul835 = mul i32 16, %314
  %add836 = add i32 %mul835, 9
  %arrayidx837 = getelementptr [128 x i64], [128 x i64]* %v834, i32 0, i32 %add836
  %315 = load i64, i64* %arrayidx837, align 8
  %v838 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %316 = load i32, i32* %i, align 4
  %mul839 = mul i32 16, %316
  %add840 = add i32 %mul839, 14
  %arrayidx841 = getelementptr [128 x i64], [128 x i64]* %v838, i32 0, i32 %add840
  %317 = load i64, i64* %arrayidx841, align 8
  %call842 = call i64 @fBlaMka(i64 %315, i64 %317)
  %v843 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %318 = load i32, i32* %i, align 4
  %mul844 = mul i32 16, %318
  %add845 = add i32 %mul844, 9
  %arrayidx846 = getelementptr [128 x i64], [128 x i64]* %v843, i32 0, i32 %add845
  store i64 %call842, i64* %arrayidx846, align 8
  %v847 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %319 = load i32, i32* %i, align 4
  %mul848 = mul i32 16, %319
  %add849 = add i32 %mul848, 4
  %arrayidx850 = getelementptr [128 x i64], [128 x i64]* %v847, i32 0, i32 %add849
  %320 = load i64, i64* %arrayidx850, align 8
  %v851 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %321 = load i32, i32* %i, align 4
  %mul852 = mul i32 16, %321
  %add853 = add i32 %mul852, 9
  %arrayidx854 = getelementptr [128 x i64], [128 x i64]* %v851, i32 0, i32 %add853
  %322 = load i64, i64* %arrayidx854, align 8
  %xor855 = xor i64 %320, %322
  %call856 = call i64 @rotr64(i64 %xor855, i32 63)
  %v857 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %323 = load i32, i32* %i, align 4
  %mul858 = mul i32 16, %323
  %add859 = add i32 %mul858, 4
  %arrayidx860 = getelementptr [128 x i64], [128 x i64]* %v857, i32 0, i32 %add859
  store i64 %call856, i64* %arrayidx860, align 8
  br label %do.end861

do.end861:                                        ; preds = %do.body752
  br label %do.end862

do.end862:                                        ; preds = %do.end861
  br label %for.inc

for.inc:                                          ; preds = %do.end862
  %324 = load i32, i32* %i, align 4
  %inc = add i32 %324, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond863

for.cond863:                                      ; preds = %for.inc1736, %for.end
  %325 = load i32, i32* %i, align 4
  %cmp864 = icmp ult i32 %325, 8
  br i1 %cmp864, label %for.body865, label %for.end1738

for.body865:                                      ; preds = %for.cond863
  br label %do.body866

do.body866:                                       ; preds = %for.body865
  br label %do.body867

do.body867:                                       ; preds = %do.body866
  %v868 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %326 = load i32, i32* %i, align 4
  %mul869 = mul i32 2, %326
  %arrayidx870 = getelementptr [128 x i64], [128 x i64]* %v868, i32 0, i32 %mul869
  %327 = load i64, i64* %arrayidx870, align 8
  %v871 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %328 = load i32, i32* %i, align 4
  %mul872 = mul i32 2, %328
  %add873 = add i32 %mul872, 32
  %arrayidx874 = getelementptr [128 x i64], [128 x i64]* %v871, i32 0, i32 %add873
  %329 = load i64, i64* %arrayidx874, align 8
  %call875 = call i64 @fBlaMka(i64 %327, i64 %329)
  %v876 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %330 = load i32, i32* %i, align 4
  %mul877 = mul i32 2, %330
  %arrayidx878 = getelementptr [128 x i64], [128 x i64]* %v876, i32 0, i32 %mul877
  store i64 %call875, i64* %arrayidx878, align 8
  %v879 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %331 = load i32, i32* %i, align 4
  %mul880 = mul i32 2, %331
  %add881 = add i32 %mul880, 96
  %arrayidx882 = getelementptr [128 x i64], [128 x i64]* %v879, i32 0, i32 %add881
  %332 = load i64, i64* %arrayidx882, align 8
  %v883 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %333 = load i32, i32* %i, align 4
  %mul884 = mul i32 2, %333
  %arrayidx885 = getelementptr [128 x i64], [128 x i64]* %v883, i32 0, i32 %mul884
  %334 = load i64, i64* %arrayidx885, align 8
  %xor886 = xor i64 %332, %334
  %call887 = call i64 @rotr64(i64 %xor886, i32 32)
  %v888 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %335 = load i32, i32* %i, align 4
  %mul889 = mul i32 2, %335
  %add890 = add i32 %mul889, 96
  %arrayidx891 = getelementptr [128 x i64], [128 x i64]* %v888, i32 0, i32 %add890
  store i64 %call887, i64* %arrayidx891, align 8
  %v892 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %336 = load i32, i32* %i, align 4
  %mul893 = mul i32 2, %336
  %add894 = add i32 %mul893, 64
  %arrayidx895 = getelementptr [128 x i64], [128 x i64]* %v892, i32 0, i32 %add894
  %337 = load i64, i64* %arrayidx895, align 8
  %v896 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %338 = load i32, i32* %i, align 4
  %mul897 = mul i32 2, %338
  %add898 = add i32 %mul897, 96
  %arrayidx899 = getelementptr [128 x i64], [128 x i64]* %v896, i32 0, i32 %add898
  %339 = load i64, i64* %arrayidx899, align 8
  %call900 = call i64 @fBlaMka(i64 %337, i64 %339)
  %v901 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %340 = load i32, i32* %i, align 4
  %mul902 = mul i32 2, %340
  %add903 = add i32 %mul902, 64
  %arrayidx904 = getelementptr [128 x i64], [128 x i64]* %v901, i32 0, i32 %add903
  store i64 %call900, i64* %arrayidx904, align 8
  %v905 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %341 = load i32, i32* %i, align 4
  %mul906 = mul i32 2, %341
  %add907 = add i32 %mul906, 32
  %arrayidx908 = getelementptr [128 x i64], [128 x i64]* %v905, i32 0, i32 %add907
  %342 = load i64, i64* %arrayidx908, align 8
  %v909 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %343 = load i32, i32* %i, align 4
  %mul910 = mul i32 2, %343
  %add911 = add i32 %mul910, 64
  %arrayidx912 = getelementptr [128 x i64], [128 x i64]* %v909, i32 0, i32 %add911
  %344 = load i64, i64* %arrayidx912, align 8
  %xor913 = xor i64 %342, %344
  %call914 = call i64 @rotr64(i64 %xor913, i32 24)
  %v915 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %345 = load i32, i32* %i, align 4
  %mul916 = mul i32 2, %345
  %add917 = add i32 %mul916, 32
  %arrayidx918 = getelementptr [128 x i64], [128 x i64]* %v915, i32 0, i32 %add917
  store i64 %call914, i64* %arrayidx918, align 8
  %v919 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %346 = load i32, i32* %i, align 4
  %mul920 = mul i32 2, %346
  %arrayidx921 = getelementptr [128 x i64], [128 x i64]* %v919, i32 0, i32 %mul920
  %347 = load i64, i64* %arrayidx921, align 8
  %v922 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %348 = load i32, i32* %i, align 4
  %mul923 = mul i32 2, %348
  %add924 = add i32 %mul923, 32
  %arrayidx925 = getelementptr [128 x i64], [128 x i64]* %v922, i32 0, i32 %add924
  %349 = load i64, i64* %arrayidx925, align 8
  %call926 = call i64 @fBlaMka(i64 %347, i64 %349)
  %v927 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %350 = load i32, i32* %i, align 4
  %mul928 = mul i32 2, %350
  %arrayidx929 = getelementptr [128 x i64], [128 x i64]* %v927, i32 0, i32 %mul928
  store i64 %call926, i64* %arrayidx929, align 8
  %v930 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %351 = load i32, i32* %i, align 4
  %mul931 = mul i32 2, %351
  %add932 = add i32 %mul931, 96
  %arrayidx933 = getelementptr [128 x i64], [128 x i64]* %v930, i32 0, i32 %add932
  %352 = load i64, i64* %arrayidx933, align 8
  %v934 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %353 = load i32, i32* %i, align 4
  %mul935 = mul i32 2, %353
  %arrayidx936 = getelementptr [128 x i64], [128 x i64]* %v934, i32 0, i32 %mul935
  %354 = load i64, i64* %arrayidx936, align 8
  %xor937 = xor i64 %352, %354
  %call938 = call i64 @rotr64(i64 %xor937, i32 16)
  %v939 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %355 = load i32, i32* %i, align 4
  %mul940 = mul i32 2, %355
  %add941 = add i32 %mul940, 96
  %arrayidx942 = getelementptr [128 x i64], [128 x i64]* %v939, i32 0, i32 %add941
  store i64 %call938, i64* %arrayidx942, align 8
  %v943 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %356 = load i32, i32* %i, align 4
  %mul944 = mul i32 2, %356
  %add945 = add i32 %mul944, 64
  %arrayidx946 = getelementptr [128 x i64], [128 x i64]* %v943, i32 0, i32 %add945
  %357 = load i64, i64* %arrayidx946, align 8
  %v947 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %358 = load i32, i32* %i, align 4
  %mul948 = mul i32 2, %358
  %add949 = add i32 %mul948, 96
  %arrayidx950 = getelementptr [128 x i64], [128 x i64]* %v947, i32 0, i32 %add949
  %359 = load i64, i64* %arrayidx950, align 8
  %call951 = call i64 @fBlaMka(i64 %357, i64 %359)
  %v952 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %360 = load i32, i32* %i, align 4
  %mul953 = mul i32 2, %360
  %add954 = add i32 %mul953, 64
  %arrayidx955 = getelementptr [128 x i64], [128 x i64]* %v952, i32 0, i32 %add954
  store i64 %call951, i64* %arrayidx955, align 8
  %v956 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %361 = load i32, i32* %i, align 4
  %mul957 = mul i32 2, %361
  %add958 = add i32 %mul957, 32
  %arrayidx959 = getelementptr [128 x i64], [128 x i64]* %v956, i32 0, i32 %add958
  %362 = load i64, i64* %arrayidx959, align 8
  %v960 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %363 = load i32, i32* %i, align 4
  %mul961 = mul i32 2, %363
  %add962 = add i32 %mul961, 64
  %arrayidx963 = getelementptr [128 x i64], [128 x i64]* %v960, i32 0, i32 %add962
  %364 = load i64, i64* %arrayidx963, align 8
  %xor964 = xor i64 %362, %364
  %call965 = call i64 @rotr64(i64 %xor964, i32 63)
  %v966 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %365 = load i32, i32* %i, align 4
  %mul967 = mul i32 2, %365
  %add968 = add i32 %mul967, 32
  %arrayidx969 = getelementptr [128 x i64], [128 x i64]* %v966, i32 0, i32 %add968
  store i64 %call965, i64* %arrayidx969, align 8
  br label %do.end970

do.end970:                                        ; preds = %do.body867
  br label %do.body971

do.body971:                                       ; preds = %do.end970
  %v972 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %366 = load i32, i32* %i, align 4
  %mul973 = mul i32 2, %366
  %add974 = add i32 %mul973, 1
  %arrayidx975 = getelementptr [128 x i64], [128 x i64]* %v972, i32 0, i32 %add974
  %367 = load i64, i64* %arrayidx975, align 8
  %v976 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %368 = load i32, i32* %i, align 4
  %mul977 = mul i32 2, %368
  %add978 = add i32 %mul977, 33
  %arrayidx979 = getelementptr [128 x i64], [128 x i64]* %v976, i32 0, i32 %add978
  %369 = load i64, i64* %arrayidx979, align 8
  %call980 = call i64 @fBlaMka(i64 %367, i64 %369)
  %v981 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %370 = load i32, i32* %i, align 4
  %mul982 = mul i32 2, %370
  %add983 = add i32 %mul982, 1
  %arrayidx984 = getelementptr [128 x i64], [128 x i64]* %v981, i32 0, i32 %add983
  store i64 %call980, i64* %arrayidx984, align 8
  %v985 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %371 = load i32, i32* %i, align 4
  %mul986 = mul i32 2, %371
  %add987 = add i32 %mul986, 97
  %arrayidx988 = getelementptr [128 x i64], [128 x i64]* %v985, i32 0, i32 %add987
  %372 = load i64, i64* %arrayidx988, align 8
  %v989 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %373 = load i32, i32* %i, align 4
  %mul990 = mul i32 2, %373
  %add991 = add i32 %mul990, 1
  %arrayidx992 = getelementptr [128 x i64], [128 x i64]* %v989, i32 0, i32 %add991
  %374 = load i64, i64* %arrayidx992, align 8
  %xor993 = xor i64 %372, %374
  %call994 = call i64 @rotr64(i64 %xor993, i32 32)
  %v995 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %375 = load i32, i32* %i, align 4
  %mul996 = mul i32 2, %375
  %add997 = add i32 %mul996, 97
  %arrayidx998 = getelementptr [128 x i64], [128 x i64]* %v995, i32 0, i32 %add997
  store i64 %call994, i64* %arrayidx998, align 8
  %v999 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %376 = load i32, i32* %i, align 4
  %mul1000 = mul i32 2, %376
  %add1001 = add i32 %mul1000, 65
  %arrayidx1002 = getelementptr [128 x i64], [128 x i64]* %v999, i32 0, i32 %add1001
  %377 = load i64, i64* %arrayidx1002, align 8
  %v1003 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %378 = load i32, i32* %i, align 4
  %mul1004 = mul i32 2, %378
  %add1005 = add i32 %mul1004, 97
  %arrayidx1006 = getelementptr [128 x i64], [128 x i64]* %v1003, i32 0, i32 %add1005
  %379 = load i64, i64* %arrayidx1006, align 8
  %call1007 = call i64 @fBlaMka(i64 %377, i64 %379)
  %v1008 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %380 = load i32, i32* %i, align 4
  %mul1009 = mul i32 2, %380
  %add1010 = add i32 %mul1009, 65
  %arrayidx1011 = getelementptr [128 x i64], [128 x i64]* %v1008, i32 0, i32 %add1010
  store i64 %call1007, i64* %arrayidx1011, align 8
  %v1012 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %381 = load i32, i32* %i, align 4
  %mul1013 = mul i32 2, %381
  %add1014 = add i32 %mul1013, 33
  %arrayidx1015 = getelementptr [128 x i64], [128 x i64]* %v1012, i32 0, i32 %add1014
  %382 = load i64, i64* %arrayidx1015, align 8
  %v1016 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %383 = load i32, i32* %i, align 4
  %mul1017 = mul i32 2, %383
  %add1018 = add i32 %mul1017, 65
  %arrayidx1019 = getelementptr [128 x i64], [128 x i64]* %v1016, i32 0, i32 %add1018
  %384 = load i64, i64* %arrayidx1019, align 8
  %xor1020 = xor i64 %382, %384
  %call1021 = call i64 @rotr64(i64 %xor1020, i32 24)
  %v1022 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %385 = load i32, i32* %i, align 4
  %mul1023 = mul i32 2, %385
  %add1024 = add i32 %mul1023, 33
  %arrayidx1025 = getelementptr [128 x i64], [128 x i64]* %v1022, i32 0, i32 %add1024
  store i64 %call1021, i64* %arrayidx1025, align 8
  %v1026 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %386 = load i32, i32* %i, align 4
  %mul1027 = mul i32 2, %386
  %add1028 = add i32 %mul1027, 1
  %arrayidx1029 = getelementptr [128 x i64], [128 x i64]* %v1026, i32 0, i32 %add1028
  %387 = load i64, i64* %arrayidx1029, align 8
  %v1030 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %388 = load i32, i32* %i, align 4
  %mul1031 = mul i32 2, %388
  %add1032 = add i32 %mul1031, 33
  %arrayidx1033 = getelementptr [128 x i64], [128 x i64]* %v1030, i32 0, i32 %add1032
  %389 = load i64, i64* %arrayidx1033, align 8
  %call1034 = call i64 @fBlaMka(i64 %387, i64 %389)
  %v1035 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %390 = load i32, i32* %i, align 4
  %mul1036 = mul i32 2, %390
  %add1037 = add i32 %mul1036, 1
  %arrayidx1038 = getelementptr [128 x i64], [128 x i64]* %v1035, i32 0, i32 %add1037
  store i64 %call1034, i64* %arrayidx1038, align 8
  %v1039 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %391 = load i32, i32* %i, align 4
  %mul1040 = mul i32 2, %391
  %add1041 = add i32 %mul1040, 97
  %arrayidx1042 = getelementptr [128 x i64], [128 x i64]* %v1039, i32 0, i32 %add1041
  %392 = load i64, i64* %arrayidx1042, align 8
  %v1043 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %393 = load i32, i32* %i, align 4
  %mul1044 = mul i32 2, %393
  %add1045 = add i32 %mul1044, 1
  %arrayidx1046 = getelementptr [128 x i64], [128 x i64]* %v1043, i32 0, i32 %add1045
  %394 = load i64, i64* %arrayidx1046, align 8
  %xor1047 = xor i64 %392, %394
  %call1048 = call i64 @rotr64(i64 %xor1047, i32 16)
  %v1049 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %395 = load i32, i32* %i, align 4
  %mul1050 = mul i32 2, %395
  %add1051 = add i32 %mul1050, 97
  %arrayidx1052 = getelementptr [128 x i64], [128 x i64]* %v1049, i32 0, i32 %add1051
  store i64 %call1048, i64* %arrayidx1052, align 8
  %v1053 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %396 = load i32, i32* %i, align 4
  %mul1054 = mul i32 2, %396
  %add1055 = add i32 %mul1054, 65
  %arrayidx1056 = getelementptr [128 x i64], [128 x i64]* %v1053, i32 0, i32 %add1055
  %397 = load i64, i64* %arrayidx1056, align 8
  %v1057 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %398 = load i32, i32* %i, align 4
  %mul1058 = mul i32 2, %398
  %add1059 = add i32 %mul1058, 97
  %arrayidx1060 = getelementptr [128 x i64], [128 x i64]* %v1057, i32 0, i32 %add1059
  %399 = load i64, i64* %arrayidx1060, align 8
  %call1061 = call i64 @fBlaMka(i64 %397, i64 %399)
  %v1062 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %400 = load i32, i32* %i, align 4
  %mul1063 = mul i32 2, %400
  %add1064 = add i32 %mul1063, 65
  %arrayidx1065 = getelementptr [128 x i64], [128 x i64]* %v1062, i32 0, i32 %add1064
  store i64 %call1061, i64* %arrayidx1065, align 8
  %v1066 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %401 = load i32, i32* %i, align 4
  %mul1067 = mul i32 2, %401
  %add1068 = add i32 %mul1067, 33
  %arrayidx1069 = getelementptr [128 x i64], [128 x i64]* %v1066, i32 0, i32 %add1068
  %402 = load i64, i64* %arrayidx1069, align 8
  %v1070 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %403 = load i32, i32* %i, align 4
  %mul1071 = mul i32 2, %403
  %add1072 = add i32 %mul1071, 65
  %arrayidx1073 = getelementptr [128 x i64], [128 x i64]* %v1070, i32 0, i32 %add1072
  %404 = load i64, i64* %arrayidx1073, align 8
  %xor1074 = xor i64 %402, %404
  %call1075 = call i64 @rotr64(i64 %xor1074, i32 63)
  %v1076 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %405 = load i32, i32* %i, align 4
  %mul1077 = mul i32 2, %405
  %add1078 = add i32 %mul1077, 33
  %arrayidx1079 = getelementptr [128 x i64], [128 x i64]* %v1076, i32 0, i32 %add1078
  store i64 %call1075, i64* %arrayidx1079, align 8
  br label %do.end1080

do.end1080:                                       ; preds = %do.body971
  br label %do.body1081

do.body1081:                                      ; preds = %do.end1080
  %v1082 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %406 = load i32, i32* %i, align 4
  %mul1083 = mul i32 2, %406
  %add1084 = add i32 %mul1083, 16
  %arrayidx1085 = getelementptr [128 x i64], [128 x i64]* %v1082, i32 0, i32 %add1084
  %407 = load i64, i64* %arrayidx1085, align 8
  %v1086 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %408 = load i32, i32* %i, align 4
  %mul1087 = mul i32 2, %408
  %add1088 = add i32 %mul1087, 48
  %arrayidx1089 = getelementptr [128 x i64], [128 x i64]* %v1086, i32 0, i32 %add1088
  %409 = load i64, i64* %arrayidx1089, align 8
  %call1090 = call i64 @fBlaMka(i64 %407, i64 %409)
  %v1091 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %410 = load i32, i32* %i, align 4
  %mul1092 = mul i32 2, %410
  %add1093 = add i32 %mul1092, 16
  %arrayidx1094 = getelementptr [128 x i64], [128 x i64]* %v1091, i32 0, i32 %add1093
  store i64 %call1090, i64* %arrayidx1094, align 8
  %v1095 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %411 = load i32, i32* %i, align 4
  %mul1096 = mul i32 2, %411
  %add1097 = add i32 %mul1096, 112
  %arrayidx1098 = getelementptr [128 x i64], [128 x i64]* %v1095, i32 0, i32 %add1097
  %412 = load i64, i64* %arrayidx1098, align 8
  %v1099 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %413 = load i32, i32* %i, align 4
  %mul1100 = mul i32 2, %413
  %add1101 = add i32 %mul1100, 16
  %arrayidx1102 = getelementptr [128 x i64], [128 x i64]* %v1099, i32 0, i32 %add1101
  %414 = load i64, i64* %arrayidx1102, align 8
  %xor1103 = xor i64 %412, %414
  %call1104 = call i64 @rotr64(i64 %xor1103, i32 32)
  %v1105 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %415 = load i32, i32* %i, align 4
  %mul1106 = mul i32 2, %415
  %add1107 = add i32 %mul1106, 112
  %arrayidx1108 = getelementptr [128 x i64], [128 x i64]* %v1105, i32 0, i32 %add1107
  store i64 %call1104, i64* %arrayidx1108, align 8
  %v1109 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %416 = load i32, i32* %i, align 4
  %mul1110 = mul i32 2, %416
  %add1111 = add i32 %mul1110, 80
  %arrayidx1112 = getelementptr [128 x i64], [128 x i64]* %v1109, i32 0, i32 %add1111
  %417 = load i64, i64* %arrayidx1112, align 8
  %v1113 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %418 = load i32, i32* %i, align 4
  %mul1114 = mul i32 2, %418
  %add1115 = add i32 %mul1114, 112
  %arrayidx1116 = getelementptr [128 x i64], [128 x i64]* %v1113, i32 0, i32 %add1115
  %419 = load i64, i64* %arrayidx1116, align 8
  %call1117 = call i64 @fBlaMka(i64 %417, i64 %419)
  %v1118 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %420 = load i32, i32* %i, align 4
  %mul1119 = mul i32 2, %420
  %add1120 = add i32 %mul1119, 80
  %arrayidx1121 = getelementptr [128 x i64], [128 x i64]* %v1118, i32 0, i32 %add1120
  store i64 %call1117, i64* %arrayidx1121, align 8
  %v1122 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %421 = load i32, i32* %i, align 4
  %mul1123 = mul i32 2, %421
  %add1124 = add i32 %mul1123, 48
  %arrayidx1125 = getelementptr [128 x i64], [128 x i64]* %v1122, i32 0, i32 %add1124
  %422 = load i64, i64* %arrayidx1125, align 8
  %v1126 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %423 = load i32, i32* %i, align 4
  %mul1127 = mul i32 2, %423
  %add1128 = add i32 %mul1127, 80
  %arrayidx1129 = getelementptr [128 x i64], [128 x i64]* %v1126, i32 0, i32 %add1128
  %424 = load i64, i64* %arrayidx1129, align 8
  %xor1130 = xor i64 %422, %424
  %call1131 = call i64 @rotr64(i64 %xor1130, i32 24)
  %v1132 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %425 = load i32, i32* %i, align 4
  %mul1133 = mul i32 2, %425
  %add1134 = add i32 %mul1133, 48
  %arrayidx1135 = getelementptr [128 x i64], [128 x i64]* %v1132, i32 0, i32 %add1134
  store i64 %call1131, i64* %arrayidx1135, align 8
  %v1136 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %426 = load i32, i32* %i, align 4
  %mul1137 = mul i32 2, %426
  %add1138 = add i32 %mul1137, 16
  %arrayidx1139 = getelementptr [128 x i64], [128 x i64]* %v1136, i32 0, i32 %add1138
  %427 = load i64, i64* %arrayidx1139, align 8
  %v1140 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %428 = load i32, i32* %i, align 4
  %mul1141 = mul i32 2, %428
  %add1142 = add i32 %mul1141, 48
  %arrayidx1143 = getelementptr [128 x i64], [128 x i64]* %v1140, i32 0, i32 %add1142
  %429 = load i64, i64* %arrayidx1143, align 8
  %call1144 = call i64 @fBlaMka(i64 %427, i64 %429)
  %v1145 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %430 = load i32, i32* %i, align 4
  %mul1146 = mul i32 2, %430
  %add1147 = add i32 %mul1146, 16
  %arrayidx1148 = getelementptr [128 x i64], [128 x i64]* %v1145, i32 0, i32 %add1147
  store i64 %call1144, i64* %arrayidx1148, align 8
  %v1149 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %431 = load i32, i32* %i, align 4
  %mul1150 = mul i32 2, %431
  %add1151 = add i32 %mul1150, 112
  %arrayidx1152 = getelementptr [128 x i64], [128 x i64]* %v1149, i32 0, i32 %add1151
  %432 = load i64, i64* %arrayidx1152, align 8
  %v1153 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %433 = load i32, i32* %i, align 4
  %mul1154 = mul i32 2, %433
  %add1155 = add i32 %mul1154, 16
  %arrayidx1156 = getelementptr [128 x i64], [128 x i64]* %v1153, i32 0, i32 %add1155
  %434 = load i64, i64* %arrayidx1156, align 8
  %xor1157 = xor i64 %432, %434
  %call1158 = call i64 @rotr64(i64 %xor1157, i32 16)
  %v1159 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %435 = load i32, i32* %i, align 4
  %mul1160 = mul i32 2, %435
  %add1161 = add i32 %mul1160, 112
  %arrayidx1162 = getelementptr [128 x i64], [128 x i64]* %v1159, i32 0, i32 %add1161
  store i64 %call1158, i64* %arrayidx1162, align 8
  %v1163 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %436 = load i32, i32* %i, align 4
  %mul1164 = mul i32 2, %436
  %add1165 = add i32 %mul1164, 80
  %arrayidx1166 = getelementptr [128 x i64], [128 x i64]* %v1163, i32 0, i32 %add1165
  %437 = load i64, i64* %arrayidx1166, align 8
  %v1167 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %438 = load i32, i32* %i, align 4
  %mul1168 = mul i32 2, %438
  %add1169 = add i32 %mul1168, 112
  %arrayidx1170 = getelementptr [128 x i64], [128 x i64]* %v1167, i32 0, i32 %add1169
  %439 = load i64, i64* %arrayidx1170, align 8
  %call1171 = call i64 @fBlaMka(i64 %437, i64 %439)
  %v1172 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %440 = load i32, i32* %i, align 4
  %mul1173 = mul i32 2, %440
  %add1174 = add i32 %mul1173, 80
  %arrayidx1175 = getelementptr [128 x i64], [128 x i64]* %v1172, i32 0, i32 %add1174
  store i64 %call1171, i64* %arrayidx1175, align 8
  %v1176 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %441 = load i32, i32* %i, align 4
  %mul1177 = mul i32 2, %441
  %add1178 = add i32 %mul1177, 48
  %arrayidx1179 = getelementptr [128 x i64], [128 x i64]* %v1176, i32 0, i32 %add1178
  %442 = load i64, i64* %arrayidx1179, align 8
  %v1180 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %443 = load i32, i32* %i, align 4
  %mul1181 = mul i32 2, %443
  %add1182 = add i32 %mul1181, 80
  %arrayidx1183 = getelementptr [128 x i64], [128 x i64]* %v1180, i32 0, i32 %add1182
  %444 = load i64, i64* %arrayidx1183, align 8
  %xor1184 = xor i64 %442, %444
  %call1185 = call i64 @rotr64(i64 %xor1184, i32 63)
  %v1186 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %445 = load i32, i32* %i, align 4
  %mul1187 = mul i32 2, %445
  %add1188 = add i32 %mul1187, 48
  %arrayidx1189 = getelementptr [128 x i64], [128 x i64]* %v1186, i32 0, i32 %add1188
  store i64 %call1185, i64* %arrayidx1189, align 8
  br label %do.end1190

do.end1190:                                       ; preds = %do.body1081
  br label %do.body1191

do.body1191:                                      ; preds = %do.end1190
  %v1192 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %446 = load i32, i32* %i, align 4
  %mul1193 = mul i32 2, %446
  %add1194 = add i32 %mul1193, 17
  %arrayidx1195 = getelementptr [128 x i64], [128 x i64]* %v1192, i32 0, i32 %add1194
  %447 = load i64, i64* %arrayidx1195, align 8
  %v1196 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %448 = load i32, i32* %i, align 4
  %mul1197 = mul i32 2, %448
  %add1198 = add i32 %mul1197, 49
  %arrayidx1199 = getelementptr [128 x i64], [128 x i64]* %v1196, i32 0, i32 %add1198
  %449 = load i64, i64* %arrayidx1199, align 8
  %call1200 = call i64 @fBlaMka(i64 %447, i64 %449)
  %v1201 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %450 = load i32, i32* %i, align 4
  %mul1202 = mul i32 2, %450
  %add1203 = add i32 %mul1202, 17
  %arrayidx1204 = getelementptr [128 x i64], [128 x i64]* %v1201, i32 0, i32 %add1203
  store i64 %call1200, i64* %arrayidx1204, align 8
  %v1205 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %451 = load i32, i32* %i, align 4
  %mul1206 = mul i32 2, %451
  %add1207 = add i32 %mul1206, 113
  %arrayidx1208 = getelementptr [128 x i64], [128 x i64]* %v1205, i32 0, i32 %add1207
  %452 = load i64, i64* %arrayidx1208, align 8
  %v1209 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %453 = load i32, i32* %i, align 4
  %mul1210 = mul i32 2, %453
  %add1211 = add i32 %mul1210, 17
  %arrayidx1212 = getelementptr [128 x i64], [128 x i64]* %v1209, i32 0, i32 %add1211
  %454 = load i64, i64* %arrayidx1212, align 8
  %xor1213 = xor i64 %452, %454
  %call1214 = call i64 @rotr64(i64 %xor1213, i32 32)
  %v1215 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %455 = load i32, i32* %i, align 4
  %mul1216 = mul i32 2, %455
  %add1217 = add i32 %mul1216, 113
  %arrayidx1218 = getelementptr [128 x i64], [128 x i64]* %v1215, i32 0, i32 %add1217
  store i64 %call1214, i64* %arrayidx1218, align 8
  %v1219 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %456 = load i32, i32* %i, align 4
  %mul1220 = mul i32 2, %456
  %add1221 = add i32 %mul1220, 81
  %arrayidx1222 = getelementptr [128 x i64], [128 x i64]* %v1219, i32 0, i32 %add1221
  %457 = load i64, i64* %arrayidx1222, align 8
  %v1223 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %458 = load i32, i32* %i, align 4
  %mul1224 = mul i32 2, %458
  %add1225 = add i32 %mul1224, 113
  %arrayidx1226 = getelementptr [128 x i64], [128 x i64]* %v1223, i32 0, i32 %add1225
  %459 = load i64, i64* %arrayidx1226, align 8
  %call1227 = call i64 @fBlaMka(i64 %457, i64 %459)
  %v1228 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %460 = load i32, i32* %i, align 4
  %mul1229 = mul i32 2, %460
  %add1230 = add i32 %mul1229, 81
  %arrayidx1231 = getelementptr [128 x i64], [128 x i64]* %v1228, i32 0, i32 %add1230
  store i64 %call1227, i64* %arrayidx1231, align 8
  %v1232 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %461 = load i32, i32* %i, align 4
  %mul1233 = mul i32 2, %461
  %add1234 = add i32 %mul1233, 49
  %arrayidx1235 = getelementptr [128 x i64], [128 x i64]* %v1232, i32 0, i32 %add1234
  %462 = load i64, i64* %arrayidx1235, align 8
  %v1236 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %463 = load i32, i32* %i, align 4
  %mul1237 = mul i32 2, %463
  %add1238 = add i32 %mul1237, 81
  %arrayidx1239 = getelementptr [128 x i64], [128 x i64]* %v1236, i32 0, i32 %add1238
  %464 = load i64, i64* %arrayidx1239, align 8
  %xor1240 = xor i64 %462, %464
  %call1241 = call i64 @rotr64(i64 %xor1240, i32 24)
  %v1242 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %465 = load i32, i32* %i, align 4
  %mul1243 = mul i32 2, %465
  %add1244 = add i32 %mul1243, 49
  %arrayidx1245 = getelementptr [128 x i64], [128 x i64]* %v1242, i32 0, i32 %add1244
  store i64 %call1241, i64* %arrayidx1245, align 8
  %v1246 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %466 = load i32, i32* %i, align 4
  %mul1247 = mul i32 2, %466
  %add1248 = add i32 %mul1247, 17
  %arrayidx1249 = getelementptr [128 x i64], [128 x i64]* %v1246, i32 0, i32 %add1248
  %467 = load i64, i64* %arrayidx1249, align 8
  %v1250 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %468 = load i32, i32* %i, align 4
  %mul1251 = mul i32 2, %468
  %add1252 = add i32 %mul1251, 49
  %arrayidx1253 = getelementptr [128 x i64], [128 x i64]* %v1250, i32 0, i32 %add1252
  %469 = load i64, i64* %arrayidx1253, align 8
  %call1254 = call i64 @fBlaMka(i64 %467, i64 %469)
  %v1255 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %470 = load i32, i32* %i, align 4
  %mul1256 = mul i32 2, %470
  %add1257 = add i32 %mul1256, 17
  %arrayidx1258 = getelementptr [128 x i64], [128 x i64]* %v1255, i32 0, i32 %add1257
  store i64 %call1254, i64* %arrayidx1258, align 8
  %v1259 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %471 = load i32, i32* %i, align 4
  %mul1260 = mul i32 2, %471
  %add1261 = add i32 %mul1260, 113
  %arrayidx1262 = getelementptr [128 x i64], [128 x i64]* %v1259, i32 0, i32 %add1261
  %472 = load i64, i64* %arrayidx1262, align 8
  %v1263 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %473 = load i32, i32* %i, align 4
  %mul1264 = mul i32 2, %473
  %add1265 = add i32 %mul1264, 17
  %arrayidx1266 = getelementptr [128 x i64], [128 x i64]* %v1263, i32 0, i32 %add1265
  %474 = load i64, i64* %arrayidx1266, align 8
  %xor1267 = xor i64 %472, %474
  %call1268 = call i64 @rotr64(i64 %xor1267, i32 16)
  %v1269 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %475 = load i32, i32* %i, align 4
  %mul1270 = mul i32 2, %475
  %add1271 = add i32 %mul1270, 113
  %arrayidx1272 = getelementptr [128 x i64], [128 x i64]* %v1269, i32 0, i32 %add1271
  store i64 %call1268, i64* %arrayidx1272, align 8
  %v1273 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %476 = load i32, i32* %i, align 4
  %mul1274 = mul i32 2, %476
  %add1275 = add i32 %mul1274, 81
  %arrayidx1276 = getelementptr [128 x i64], [128 x i64]* %v1273, i32 0, i32 %add1275
  %477 = load i64, i64* %arrayidx1276, align 8
  %v1277 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %478 = load i32, i32* %i, align 4
  %mul1278 = mul i32 2, %478
  %add1279 = add i32 %mul1278, 113
  %arrayidx1280 = getelementptr [128 x i64], [128 x i64]* %v1277, i32 0, i32 %add1279
  %479 = load i64, i64* %arrayidx1280, align 8
  %call1281 = call i64 @fBlaMka(i64 %477, i64 %479)
  %v1282 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %480 = load i32, i32* %i, align 4
  %mul1283 = mul i32 2, %480
  %add1284 = add i32 %mul1283, 81
  %arrayidx1285 = getelementptr [128 x i64], [128 x i64]* %v1282, i32 0, i32 %add1284
  store i64 %call1281, i64* %arrayidx1285, align 8
  %v1286 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %481 = load i32, i32* %i, align 4
  %mul1287 = mul i32 2, %481
  %add1288 = add i32 %mul1287, 49
  %arrayidx1289 = getelementptr [128 x i64], [128 x i64]* %v1286, i32 0, i32 %add1288
  %482 = load i64, i64* %arrayidx1289, align 8
  %v1290 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %483 = load i32, i32* %i, align 4
  %mul1291 = mul i32 2, %483
  %add1292 = add i32 %mul1291, 81
  %arrayidx1293 = getelementptr [128 x i64], [128 x i64]* %v1290, i32 0, i32 %add1292
  %484 = load i64, i64* %arrayidx1293, align 8
  %xor1294 = xor i64 %482, %484
  %call1295 = call i64 @rotr64(i64 %xor1294, i32 63)
  %v1296 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %485 = load i32, i32* %i, align 4
  %mul1297 = mul i32 2, %485
  %add1298 = add i32 %mul1297, 49
  %arrayidx1299 = getelementptr [128 x i64], [128 x i64]* %v1296, i32 0, i32 %add1298
  store i64 %call1295, i64* %arrayidx1299, align 8
  br label %do.end1300

do.end1300:                                       ; preds = %do.body1191
  br label %do.body1301

do.body1301:                                      ; preds = %do.end1300
  %v1302 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %486 = load i32, i32* %i, align 4
  %mul1303 = mul i32 2, %486
  %arrayidx1304 = getelementptr [128 x i64], [128 x i64]* %v1302, i32 0, i32 %mul1303
  %487 = load i64, i64* %arrayidx1304, align 8
  %v1305 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %488 = load i32, i32* %i, align 4
  %mul1306 = mul i32 2, %488
  %add1307 = add i32 %mul1306, 33
  %arrayidx1308 = getelementptr [128 x i64], [128 x i64]* %v1305, i32 0, i32 %add1307
  %489 = load i64, i64* %arrayidx1308, align 8
  %call1309 = call i64 @fBlaMka(i64 %487, i64 %489)
  %v1310 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %490 = load i32, i32* %i, align 4
  %mul1311 = mul i32 2, %490
  %arrayidx1312 = getelementptr [128 x i64], [128 x i64]* %v1310, i32 0, i32 %mul1311
  store i64 %call1309, i64* %arrayidx1312, align 8
  %v1313 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %491 = load i32, i32* %i, align 4
  %mul1314 = mul i32 2, %491
  %add1315 = add i32 %mul1314, 113
  %arrayidx1316 = getelementptr [128 x i64], [128 x i64]* %v1313, i32 0, i32 %add1315
  %492 = load i64, i64* %arrayidx1316, align 8
  %v1317 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %493 = load i32, i32* %i, align 4
  %mul1318 = mul i32 2, %493
  %arrayidx1319 = getelementptr [128 x i64], [128 x i64]* %v1317, i32 0, i32 %mul1318
  %494 = load i64, i64* %arrayidx1319, align 8
  %xor1320 = xor i64 %492, %494
  %call1321 = call i64 @rotr64(i64 %xor1320, i32 32)
  %v1322 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %495 = load i32, i32* %i, align 4
  %mul1323 = mul i32 2, %495
  %add1324 = add i32 %mul1323, 113
  %arrayidx1325 = getelementptr [128 x i64], [128 x i64]* %v1322, i32 0, i32 %add1324
  store i64 %call1321, i64* %arrayidx1325, align 8
  %v1326 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %496 = load i32, i32* %i, align 4
  %mul1327 = mul i32 2, %496
  %add1328 = add i32 %mul1327, 80
  %arrayidx1329 = getelementptr [128 x i64], [128 x i64]* %v1326, i32 0, i32 %add1328
  %497 = load i64, i64* %arrayidx1329, align 8
  %v1330 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %498 = load i32, i32* %i, align 4
  %mul1331 = mul i32 2, %498
  %add1332 = add i32 %mul1331, 113
  %arrayidx1333 = getelementptr [128 x i64], [128 x i64]* %v1330, i32 0, i32 %add1332
  %499 = load i64, i64* %arrayidx1333, align 8
  %call1334 = call i64 @fBlaMka(i64 %497, i64 %499)
  %v1335 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %500 = load i32, i32* %i, align 4
  %mul1336 = mul i32 2, %500
  %add1337 = add i32 %mul1336, 80
  %arrayidx1338 = getelementptr [128 x i64], [128 x i64]* %v1335, i32 0, i32 %add1337
  store i64 %call1334, i64* %arrayidx1338, align 8
  %v1339 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %501 = load i32, i32* %i, align 4
  %mul1340 = mul i32 2, %501
  %add1341 = add i32 %mul1340, 33
  %arrayidx1342 = getelementptr [128 x i64], [128 x i64]* %v1339, i32 0, i32 %add1341
  %502 = load i64, i64* %arrayidx1342, align 8
  %v1343 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %503 = load i32, i32* %i, align 4
  %mul1344 = mul i32 2, %503
  %add1345 = add i32 %mul1344, 80
  %arrayidx1346 = getelementptr [128 x i64], [128 x i64]* %v1343, i32 0, i32 %add1345
  %504 = load i64, i64* %arrayidx1346, align 8
  %xor1347 = xor i64 %502, %504
  %call1348 = call i64 @rotr64(i64 %xor1347, i32 24)
  %v1349 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %505 = load i32, i32* %i, align 4
  %mul1350 = mul i32 2, %505
  %add1351 = add i32 %mul1350, 33
  %arrayidx1352 = getelementptr [128 x i64], [128 x i64]* %v1349, i32 0, i32 %add1351
  store i64 %call1348, i64* %arrayidx1352, align 8
  %v1353 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %506 = load i32, i32* %i, align 4
  %mul1354 = mul i32 2, %506
  %arrayidx1355 = getelementptr [128 x i64], [128 x i64]* %v1353, i32 0, i32 %mul1354
  %507 = load i64, i64* %arrayidx1355, align 8
  %v1356 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %508 = load i32, i32* %i, align 4
  %mul1357 = mul i32 2, %508
  %add1358 = add i32 %mul1357, 33
  %arrayidx1359 = getelementptr [128 x i64], [128 x i64]* %v1356, i32 0, i32 %add1358
  %509 = load i64, i64* %arrayidx1359, align 8
  %call1360 = call i64 @fBlaMka(i64 %507, i64 %509)
  %v1361 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %510 = load i32, i32* %i, align 4
  %mul1362 = mul i32 2, %510
  %arrayidx1363 = getelementptr [128 x i64], [128 x i64]* %v1361, i32 0, i32 %mul1362
  store i64 %call1360, i64* %arrayidx1363, align 8
  %v1364 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %511 = load i32, i32* %i, align 4
  %mul1365 = mul i32 2, %511
  %add1366 = add i32 %mul1365, 113
  %arrayidx1367 = getelementptr [128 x i64], [128 x i64]* %v1364, i32 0, i32 %add1366
  %512 = load i64, i64* %arrayidx1367, align 8
  %v1368 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %513 = load i32, i32* %i, align 4
  %mul1369 = mul i32 2, %513
  %arrayidx1370 = getelementptr [128 x i64], [128 x i64]* %v1368, i32 0, i32 %mul1369
  %514 = load i64, i64* %arrayidx1370, align 8
  %xor1371 = xor i64 %512, %514
  %call1372 = call i64 @rotr64(i64 %xor1371, i32 16)
  %v1373 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %515 = load i32, i32* %i, align 4
  %mul1374 = mul i32 2, %515
  %add1375 = add i32 %mul1374, 113
  %arrayidx1376 = getelementptr [128 x i64], [128 x i64]* %v1373, i32 0, i32 %add1375
  store i64 %call1372, i64* %arrayidx1376, align 8
  %v1377 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %516 = load i32, i32* %i, align 4
  %mul1378 = mul i32 2, %516
  %add1379 = add i32 %mul1378, 80
  %arrayidx1380 = getelementptr [128 x i64], [128 x i64]* %v1377, i32 0, i32 %add1379
  %517 = load i64, i64* %arrayidx1380, align 8
  %v1381 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %518 = load i32, i32* %i, align 4
  %mul1382 = mul i32 2, %518
  %add1383 = add i32 %mul1382, 113
  %arrayidx1384 = getelementptr [128 x i64], [128 x i64]* %v1381, i32 0, i32 %add1383
  %519 = load i64, i64* %arrayidx1384, align 8
  %call1385 = call i64 @fBlaMka(i64 %517, i64 %519)
  %v1386 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %520 = load i32, i32* %i, align 4
  %mul1387 = mul i32 2, %520
  %add1388 = add i32 %mul1387, 80
  %arrayidx1389 = getelementptr [128 x i64], [128 x i64]* %v1386, i32 0, i32 %add1388
  store i64 %call1385, i64* %arrayidx1389, align 8
  %v1390 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %521 = load i32, i32* %i, align 4
  %mul1391 = mul i32 2, %521
  %add1392 = add i32 %mul1391, 33
  %arrayidx1393 = getelementptr [128 x i64], [128 x i64]* %v1390, i32 0, i32 %add1392
  %522 = load i64, i64* %arrayidx1393, align 8
  %v1394 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %523 = load i32, i32* %i, align 4
  %mul1395 = mul i32 2, %523
  %add1396 = add i32 %mul1395, 80
  %arrayidx1397 = getelementptr [128 x i64], [128 x i64]* %v1394, i32 0, i32 %add1396
  %524 = load i64, i64* %arrayidx1397, align 8
  %xor1398 = xor i64 %522, %524
  %call1399 = call i64 @rotr64(i64 %xor1398, i32 63)
  %v1400 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %525 = load i32, i32* %i, align 4
  %mul1401 = mul i32 2, %525
  %add1402 = add i32 %mul1401, 33
  %arrayidx1403 = getelementptr [128 x i64], [128 x i64]* %v1400, i32 0, i32 %add1402
  store i64 %call1399, i64* %arrayidx1403, align 8
  br label %do.end1404

do.end1404:                                       ; preds = %do.body1301
  br label %do.body1405

do.body1405:                                      ; preds = %do.end1404
  %v1406 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %526 = load i32, i32* %i, align 4
  %mul1407 = mul i32 2, %526
  %add1408 = add i32 %mul1407, 1
  %arrayidx1409 = getelementptr [128 x i64], [128 x i64]* %v1406, i32 0, i32 %add1408
  %527 = load i64, i64* %arrayidx1409, align 8
  %v1410 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %528 = load i32, i32* %i, align 4
  %mul1411 = mul i32 2, %528
  %add1412 = add i32 %mul1411, 48
  %arrayidx1413 = getelementptr [128 x i64], [128 x i64]* %v1410, i32 0, i32 %add1412
  %529 = load i64, i64* %arrayidx1413, align 8
  %call1414 = call i64 @fBlaMka(i64 %527, i64 %529)
  %v1415 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %530 = load i32, i32* %i, align 4
  %mul1416 = mul i32 2, %530
  %add1417 = add i32 %mul1416, 1
  %arrayidx1418 = getelementptr [128 x i64], [128 x i64]* %v1415, i32 0, i32 %add1417
  store i64 %call1414, i64* %arrayidx1418, align 8
  %v1419 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %531 = load i32, i32* %i, align 4
  %mul1420 = mul i32 2, %531
  %add1421 = add i32 %mul1420, 96
  %arrayidx1422 = getelementptr [128 x i64], [128 x i64]* %v1419, i32 0, i32 %add1421
  %532 = load i64, i64* %arrayidx1422, align 8
  %v1423 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %533 = load i32, i32* %i, align 4
  %mul1424 = mul i32 2, %533
  %add1425 = add i32 %mul1424, 1
  %arrayidx1426 = getelementptr [128 x i64], [128 x i64]* %v1423, i32 0, i32 %add1425
  %534 = load i64, i64* %arrayidx1426, align 8
  %xor1427 = xor i64 %532, %534
  %call1428 = call i64 @rotr64(i64 %xor1427, i32 32)
  %v1429 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %535 = load i32, i32* %i, align 4
  %mul1430 = mul i32 2, %535
  %add1431 = add i32 %mul1430, 96
  %arrayidx1432 = getelementptr [128 x i64], [128 x i64]* %v1429, i32 0, i32 %add1431
  store i64 %call1428, i64* %arrayidx1432, align 8
  %v1433 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %536 = load i32, i32* %i, align 4
  %mul1434 = mul i32 2, %536
  %add1435 = add i32 %mul1434, 81
  %arrayidx1436 = getelementptr [128 x i64], [128 x i64]* %v1433, i32 0, i32 %add1435
  %537 = load i64, i64* %arrayidx1436, align 8
  %v1437 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %538 = load i32, i32* %i, align 4
  %mul1438 = mul i32 2, %538
  %add1439 = add i32 %mul1438, 96
  %arrayidx1440 = getelementptr [128 x i64], [128 x i64]* %v1437, i32 0, i32 %add1439
  %539 = load i64, i64* %arrayidx1440, align 8
  %call1441 = call i64 @fBlaMka(i64 %537, i64 %539)
  %v1442 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %540 = load i32, i32* %i, align 4
  %mul1443 = mul i32 2, %540
  %add1444 = add i32 %mul1443, 81
  %arrayidx1445 = getelementptr [128 x i64], [128 x i64]* %v1442, i32 0, i32 %add1444
  store i64 %call1441, i64* %arrayidx1445, align 8
  %v1446 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %541 = load i32, i32* %i, align 4
  %mul1447 = mul i32 2, %541
  %add1448 = add i32 %mul1447, 48
  %arrayidx1449 = getelementptr [128 x i64], [128 x i64]* %v1446, i32 0, i32 %add1448
  %542 = load i64, i64* %arrayidx1449, align 8
  %v1450 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %543 = load i32, i32* %i, align 4
  %mul1451 = mul i32 2, %543
  %add1452 = add i32 %mul1451, 81
  %arrayidx1453 = getelementptr [128 x i64], [128 x i64]* %v1450, i32 0, i32 %add1452
  %544 = load i64, i64* %arrayidx1453, align 8
  %xor1454 = xor i64 %542, %544
  %call1455 = call i64 @rotr64(i64 %xor1454, i32 24)
  %v1456 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %545 = load i32, i32* %i, align 4
  %mul1457 = mul i32 2, %545
  %add1458 = add i32 %mul1457, 48
  %arrayidx1459 = getelementptr [128 x i64], [128 x i64]* %v1456, i32 0, i32 %add1458
  store i64 %call1455, i64* %arrayidx1459, align 8
  %v1460 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %546 = load i32, i32* %i, align 4
  %mul1461 = mul i32 2, %546
  %add1462 = add i32 %mul1461, 1
  %arrayidx1463 = getelementptr [128 x i64], [128 x i64]* %v1460, i32 0, i32 %add1462
  %547 = load i64, i64* %arrayidx1463, align 8
  %v1464 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %548 = load i32, i32* %i, align 4
  %mul1465 = mul i32 2, %548
  %add1466 = add i32 %mul1465, 48
  %arrayidx1467 = getelementptr [128 x i64], [128 x i64]* %v1464, i32 0, i32 %add1466
  %549 = load i64, i64* %arrayidx1467, align 8
  %call1468 = call i64 @fBlaMka(i64 %547, i64 %549)
  %v1469 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %550 = load i32, i32* %i, align 4
  %mul1470 = mul i32 2, %550
  %add1471 = add i32 %mul1470, 1
  %arrayidx1472 = getelementptr [128 x i64], [128 x i64]* %v1469, i32 0, i32 %add1471
  store i64 %call1468, i64* %arrayidx1472, align 8
  %v1473 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %551 = load i32, i32* %i, align 4
  %mul1474 = mul i32 2, %551
  %add1475 = add i32 %mul1474, 96
  %arrayidx1476 = getelementptr [128 x i64], [128 x i64]* %v1473, i32 0, i32 %add1475
  %552 = load i64, i64* %arrayidx1476, align 8
  %v1477 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %553 = load i32, i32* %i, align 4
  %mul1478 = mul i32 2, %553
  %add1479 = add i32 %mul1478, 1
  %arrayidx1480 = getelementptr [128 x i64], [128 x i64]* %v1477, i32 0, i32 %add1479
  %554 = load i64, i64* %arrayidx1480, align 8
  %xor1481 = xor i64 %552, %554
  %call1482 = call i64 @rotr64(i64 %xor1481, i32 16)
  %v1483 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %555 = load i32, i32* %i, align 4
  %mul1484 = mul i32 2, %555
  %add1485 = add i32 %mul1484, 96
  %arrayidx1486 = getelementptr [128 x i64], [128 x i64]* %v1483, i32 0, i32 %add1485
  store i64 %call1482, i64* %arrayidx1486, align 8
  %v1487 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %556 = load i32, i32* %i, align 4
  %mul1488 = mul i32 2, %556
  %add1489 = add i32 %mul1488, 81
  %arrayidx1490 = getelementptr [128 x i64], [128 x i64]* %v1487, i32 0, i32 %add1489
  %557 = load i64, i64* %arrayidx1490, align 8
  %v1491 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %558 = load i32, i32* %i, align 4
  %mul1492 = mul i32 2, %558
  %add1493 = add i32 %mul1492, 96
  %arrayidx1494 = getelementptr [128 x i64], [128 x i64]* %v1491, i32 0, i32 %add1493
  %559 = load i64, i64* %arrayidx1494, align 8
  %call1495 = call i64 @fBlaMka(i64 %557, i64 %559)
  %v1496 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %560 = load i32, i32* %i, align 4
  %mul1497 = mul i32 2, %560
  %add1498 = add i32 %mul1497, 81
  %arrayidx1499 = getelementptr [128 x i64], [128 x i64]* %v1496, i32 0, i32 %add1498
  store i64 %call1495, i64* %arrayidx1499, align 8
  %v1500 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %561 = load i32, i32* %i, align 4
  %mul1501 = mul i32 2, %561
  %add1502 = add i32 %mul1501, 48
  %arrayidx1503 = getelementptr [128 x i64], [128 x i64]* %v1500, i32 0, i32 %add1502
  %562 = load i64, i64* %arrayidx1503, align 8
  %v1504 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %563 = load i32, i32* %i, align 4
  %mul1505 = mul i32 2, %563
  %add1506 = add i32 %mul1505, 81
  %arrayidx1507 = getelementptr [128 x i64], [128 x i64]* %v1504, i32 0, i32 %add1506
  %564 = load i64, i64* %arrayidx1507, align 8
  %xor1508 = xor i64 %562, %564
  %call1509 = call i64 @rotr64(i64 %xor1508, i32 63)
  %v1510 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %565 = load i32, i32* %i, align 4
  %mul1511 = mul i32 2, %565
  %add1512 = add i32 %mul1511, 48
  %arrayidx1513 = getelementptr [128 x i64], [128 x i64]* %v1510, i32 0, i32 %add1512
  store i64 %call1509, i64* %arrayidx1513, align 8
  br label %do.end1514

do.end1514:                                       ; preds = %do.body1405
  br label %do.body1515

do.body1515:                                      ; preds = %do.end1514
  %v1516 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %566 = load i32, i32* %i, align 4
  %mul1517 = mul i32 2, %566
  %add1518 = add i32 %mul1517, 16
  %arrayidx1519 = getelementptr [128 x i64], [128 x i64]* %v1516, i32 0, i32 %add1518
  %567 = load i64, i64* %arrayidx1519, align 8
  %v1520 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %568 = load i32, i32* %i, align 4
  %mul1521 = mul i32 2, %568
  %add1522 = add i32 %mul1521, 49
  %arrayidx1523 = getelementptr [128 x i64], [128 x i64]* %v1520, i32 0, i32 %add1522
  %569 = load i64, i64* %arrayidx1523, align 8
  %call1524 = call i64 @fBlaMka(i64 %567, i64 %569)
  %v1525 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %570 = load i32, i32* %i, align 4
  %mul1526 = mul i32 2, %570
  %add1527 = add i32 %mul1526, 16
  %arrayidx1528 = getelementptr [128 x i64], [128 x i64]* %v1525, i32 0, i32 %add1527
  store i64 %call1524, i64* %arrayidx1528, align 8
  %v1529 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %571 = load i32, i32* %i, align 4
  %mul1530 = mul i32 2, %571
  %add1531 = add i32 %mul1530, 97
  %arrayidx1532 = getelementptr [128 x i64], [128 x i64]* %v1529, i32 0, i32 %add1531
  %572 = load i64, i64* %arrayidx1532, align 8
  %v1533 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %573 = load i32, i32* %i, align 4
  %mul1534 = mul i32 2, %573
  %add1535 = add i32 %mul1534, 16
  %arrayidx1536 = getelementptr [128 x i64], [128 x i64]* %v1533, i32 0, i32 %add1535
  %574 = load i64, i64* %arrayidx1536, align 8
  %xor1537 = xor i64 %572, %574
  %call1538 = call i64 @rotr64(i64 %xor1537, i32 32)
  %v1539 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %575 = load i32, i32* %i, align 4
  %mul1540 = mul i32 2, %575
  %add1541 = add i32 %mul1540, 97
  %arrayidx1542 = getelementptr [128 x i64], [128 x i64]* %v1539, i32 0, i32 %add1541
  store i64 %call1538, i64* %arrayidx1542, align 8
  %v1543 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %576 = load i32, i32* %i, align 4
  %mul1544 = mul i32 2, %576
  %add1545 = add i32 %mul1544, 64
  %arrayidx1546 = getelementptr [128 x i64], [128 x i64]* %v1543, i32 0, i32 %add1545
  %577 = load i64, i64* %arrayidx1546, align 8
  %v1547 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %578 = load i32, i32* %i, align 4
  %mul1548 = mul i32 2, %578
  %add1549 = add i32 %mul1548, 97
  %arrayidx1550 = getelementptr [128 x i64], [128 x i64]* %v1547, i32 0, i32 %add1549
  %579 = load i64, i64* %arrayidx1550, align 8
  %call1551 = call i64 @fBlaMka(i64 %577, i64 %579)
  %v1552 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %580 = load i32, i32* %i, align 4
  %mul1553 = mul i32 2, %580
  %add1554 = add i32 %mul1553, 64
  %arrayidx1555 = getelementptr [128 x i64], [128 x i64]* %v1552, i32 0, i32 %add1554
  store i64 %call1551, i64* %arrayidx1555, align 8
  %v1556 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %581 = load i32, i32* %i, align 4
  %mul1557 = mul i32 2, %581
  %add1558 = add i32 %mul1557, 49
  %arrayidx1559 = getelementptr [128 x i64], [128 x i64]* %v1556, i32 0, i32 %add1558
  %582 = load i64, i64* %arrayidx1559, align 8
  %v1560 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %583 = load i32, i32* %i, align 4
  %mul1561 = mul i32 2, %583
  %add1562 = add i32 %mul1561, 64
  %arrayidx1563 = getelementptr [128 x i64], [128 x i64]* %v1560, i32 0, i32 %add1562
  %584 = load i64, i64* %arrayidx1563, align 8
  %xor1564 = xor i64 %582, %584
  %call1565 = call i64 @rotr64(i64 %xor1564, i32 24)
  %v1566 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %585 = load i32, i32* %i, align 4
  %mul1567 = mul i32 2, %585
  %add1568 = add i32 %mul1567, 49
  %arrayidx1569 = getelementptr [128 x i64], [128 x i64]* %v1566, i32 0, i32 %add1568
  store i64 %call1565, i64* %arrayidx1569, align 8
  %v1570 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %586 = load i32, i32* %i, align 4
  %mul1571 = mul i32 2, %586
  %add1572 = add i32 %mul1571, 16
  %arrayidx1573 = getelementptr [128 x i64], [128 x i64]* %v1570, i32 0, i32 %add1572
  %587 = load i64, i64* %arrayidx1573, align 8
  %v1574 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %588 = load i32, i32* %i, align 4
  %mul1575 = mul i32 2, %588
  %add1576 = add i32 %mul1575, 49
  %arrayidx1577 = getelementptr [128 x i64], [128 x i64]* %v1574, i32 0, i32 %add1576
  %589 = load i64, i64* %arrayidx1577, align 8
  %call1578 = call i64 @fBlaMka(i64 %587, i64 %589)
  %v1579 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %590 = load i32, i32* %i, align 4
  %mul1580 = mul i32 2, %590
  %add1581 = add i32 %mul1580, 16
  %arrayidx1582 = getelementptr [128 x i64], [128 x i64]* %v1579, i32 0, i32 %add1581
  store i64 %call1578, i64* %arrayidx1582, align 8
  %v1583 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %591 = load i32, i32* %i, align 4
  %mul1584 = mul i32 2, %591
  %add1585 = add i32 %mul1584, 97
  %arrayidx1586 = getelementptr [128 x i64], [128 x i64]* %v1583, i32 0, i32 %add1585
  %592 = load i64, i64* %arrayidx1586, align 8
  %v1587 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %593 = load i32, i32* %i, align 4
  %mul1588 = mul i32 2, %593
  %add1589 = add i32 %mul1588, 16
  %arrayidx1590 = getelementptr [128 x i64], [128 x i64]* %v1587, i32 0, i32 %add1589
  %594 = load i64, i64* %arrayidx1590, align 8
  %xor1591 = xor i64 %592, %594
  %call1592 = call i64 @rotr64(i64 %xor1591, i32 16)
  %v1593 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %595 = load i32, i32* %i, align 4
  %mul1594 = mul i32 2, %595
  %add1595 = add i32 %mul1594, 97
  %arrayidx1596 = getelementptr [128 x i64], [128 x i64]* %v1593, i32 0, i32 %add1595
  store i64 %call1592, i64* %arrayidx1596, align 8
  %v1597 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %596 = load i32, i32* %i, align 4
  %mul1598 = mul i32 2, %596
  %add1599 = add i32 %mul1598, 64
  %arrayidx1600 = getelementptr [128 x i64], [128 x i64]* %v1597, i32 0, i32 %add1599
  %597 = load i64, i64* %arrayidx1600, align 8
  %v1601 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %598 = load i32, i32* %i, align 4
  %mul1602 = mul i32 2, %598
  %add1603 = add i32 %mul1602, 97
  %arrayidx1604 = getelementptr [128 x i64], [128 x i64]* %v1601, i32 0, i32 %add1603
  %599 = load i64, i64* %arrayidx1604, align 8
  %call1605 = call i64 @fBlaMka(i64 %597, i64 %599)
  %v1606 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %600 = load i32, i32* %i, align 4
  %mul1607 = mul i32 2, %600
  %add1608 = add i32 %mul1607, 64
  %arrayidx1609 = getelementptr [128 x i64], [128 x i64]* %v1606, i32 0, i32 %add1608
  store i64 %call1605, i64* %arrayidx1609, align 8
  %v1610 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %601 = load i32, i32* %i, align 4
  %mul1611 = mul i32 2, %601
  %add1612 = add i32 %mul1611, 49
  %arrayidx1613 = getelementptr [128 x i64], [128 x i64]* %v1610, i32 0, i32 %add1612
  %602 = load i64, i64* %arrayidx1613, align 8
  %v1614 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %603 = load i32, i32* %i, align 4
  %mul1615 = mul i32 2, %603
  %add1616 = add i32 %mul1615, 64
  %arrayidx1617 = getelementptr [128 x i64], [128 x i64]* %v1614, i32 0, i32 %add1616
  %604 = load i64, i64* %arrayidx1617, align 8
  %xor1618 = xor i64 %602, %604
  %call1619 = call i64 @rotr64(i64 %xor1618, i32 63)
  %v1620 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %605 = load i32, i32* %i, align 4
  %mul1621 = mul i32 2, %605
  %add1622 = add i32 %mul1621, 49
  %arrayidx1623 = getelementptr [128 x i64], [128 x i64]* %v1620, i32 0, i32 %add1622
  store i64 %call1619, i64* %arrayidx1623, align 8
  br label %do.end1624

do.end1624:                                       ; preds = %do.body1515
  br label %do.body1625

do.body1625:                                      ; preds = %do.end1624
  %v1626 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %606 = load i32, i32* %i, align 4
  %mul1627 = mul i32 2, %606
  %add1628 = add i32 %mul1627, 17
  %arrayidx1629 = getelementptr [128 x i64], [128 x i64]* %v1626, i32 0, i32 %add1628
  %607 = load i64, i64* %arrayidx1629, align 8
  %v1630 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %608 = load i32, i32* %i, align 4
  %mul1631 = mul i32 2, %608
  %add1632 = add i32 %mul1631, 32
  %arrayidx1633 = getelementptr [128 x i64], [128 x i64]* %v1630, i32 0, i32 %add1632
  %609 = load i64, i64* %arrayidx1633, align 8
  %call1634 = call i64 @fBlaMka(i64 %607, i64 %609)
  %v1635 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %610 = load i32, i32* %i, align 4
  %mul1636 = mul i32 2, %610
  %add1637 = add i32 %mul1636, 17
  %arrayidx1638 = getelementptr [128 x i64], [128 x i64]* %v1635, i32 0, i32 %add1637
  store i64 %call1634, i64* %arrayidx1638, align 8
  %v1639 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %611 = load i32, i32* %i, align 4
  %mul1640 = mul i32 2, %611
  %add1641 = add i32 %mul1640, 112
  %arrayidx1642 = getelementptr [128 x i64], [128 x i64]* %v1639, i32 0, i32 %add1641
  %612 = load i64, i64* %arrayidx1642, align 8
  %v1643 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %613 = load i32, i32* %i, align 4
  %mul1644 = mul i32 2, %613
  %add1645 = add i32 %mul1644, 17
  %arrayidx1646 = getelementptr [128 x i64], [128 x i64]* %v1643, i32 0, i32 %add1645
  %614 = load i64, i64* %arrayidx1646, align 8
  %xor1647 = xor i64 %612, %614
  %call1648 = call i64 @rotr64(i64 %xor1647, i32 32)
  %v1649 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %615 = load i32, i32* %i, align 4
  %mul1650 = mul i32 2, %615
  %add1651 = add i32 %mul1650, 112
  %arrayidx1652 = getelementptr [128 x i64], [128 x i64]* %v1649, i32 0, i32 %add1651
  store i64 %call1648, i64* %arrayidx1652, align 8
  %v1653 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %616 = load i32, i32* %i, align 4
  %mul1654 = mul i32 2, %616
  %add1655 = add i32 %mul1654, 65
  %arrayidx1656 = getelementptr [128 x i64], [128 x i64]* %v1653, i32 0, i32 %add1655
  %617 = load i64, i64* %arrayidx1656, align 8
  %v1657 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %618 = load i32, i32* %i, align 4
  %mul1658 = mul i32 2, %618
  %add1659 = add i32 %mul1658, 112
  %arrayidx1660 = getelementptr [128 x i64], [128 x i64]* %v1657, i32 0, i32 %add1659
  %619 = load i64, i64* %arrayidx1660, align 8
  %call1661 = call i64 @fBlaMka(i64 %617, i64 %619)
  %v1662 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %620 = load i32, i32* %i, align 4
  %mul1663 = mul i32 2, %620
  %add1664 = add i32 %mul1663, 65
  %arrayidx1665 = getelementptr [128 x i64], [128 x i64]* %v1662, i32 0, i32 %add1664
  store i64 %call1661, i64* %arrayidx1665, align 8
  %v1666 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %621 = load i32, i32* %i, align 4
  %mul1667 = mul i32 2, %621
  %add1668 = add i32 %mul1667, 32
  %arrayidx1669 = getelementptr [128 x i64], [128 x i64]* %v1666, i32 0, i32 %add1668
  %622 = load i64, i64* %arrayidx1669, align 8
  %v1670 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %623 = load i32, i32* %i, align 4
  %mul1671 = mul i32 2, %623
  %add1672 = add i32 %mul1671, 65
  %arrayidx1673 = getelementptr [128 x i64], [128 x i64]* %v1670, i32 0, i32 %add1672
  %624 = load i64, i64* %arrayidx1673, align 8
  %xor1674 = xor i64 %622, %624
  %call1675 = call i64 @rotr64(i64 %xor1674, i32 24)
  %v1676 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %625 = load i32, i32* %i, align 4
  %mul1677 = mul i32 2, %625
  %add1678 = add i32 %mul1677, 32
  %arrayidx1679 = getelementptr [128 x i64], [128 x i64]* %v1676, i32 0, i32 %add1678
  store i64 %call1675, i64* %arrayidx1679, align 8
  %v1680 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %626 = load i32, i32* %i, align 4
  %mul1681 = mul i32 2, %626
  %add1682 = add i32 %mul1681, 17
  %arrayidx1683 = getelementptr [128 x i64], [128 x i64]* %v1680, i32 0, i32 %add1682
  %627 = load i64, i64* %arrayidx1683, align 8
  %v1684 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %628 = load i32, i32* %i, align 4
  %mul1685 = mul i32 2, %628
  %add1686 = add i32 %mul1685, 32
  %arrayidx1687 = getelementptr [128 x i64], [128 x i64]* %v1684, i32 0, i32 %add1686
  %629 = load i64, i64* %arrayidx1687, align 8
  %call1688 = call i64 @fBlaMka(i64 %627, i64 %629)
  %v1689 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %630 = load i32, i32* %i, align 4
  %mul1690 = mul i32 2, %630
  %add1691 = add i32 %mul1690, 17
  %arrayidx1692 = getelementptr [128 x i64], [128 x i64]* %v1689, i32 0, i32 %add1691
  store i64 %call1688, i64* %arrayidx1692, align 8
  %v1693 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %631 = load i32, i32* %i, align 4
  %mul1694 = mul i32 2, %631
  %add1695 = add i32 %mul1694, 112
  %arrayidx1696 = getelementptr [128 x i64], [128 x i64]* %v1693, i32 0, i32 %add1695
  %632 = load i64, i64* %arrayidx1696, align 8
  %v1697 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %633 = load i32, i32* %i, align 4
  %mul1698 = mul i32 2, %633
  %add1699 = add i32 %mul1698, 17
  %arrayidx1700 = getelementptr [128 x i64], [128 x i64]* %v1697, i32 0, i32 %add1699
  %634 = load i64, i64* %arrayidx1700, align 8
  %xor1701 = xor i64 %632, %634
  %call1702 = call i64 @rotr64(i64 %xor1701, i32 16)
  %v1703 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %635 = load i32, i32* %i, align 4
  %mul1704 = mul i32 2, %635
  %add1705 = add i32 %mul1704, 112
  %arrayidx1706 = getelementptr [128 x i64], [128 x i64]* %v1703, i32 0, i32 %add1705
  store i64 %call1702, i64* %arrayidx1706, align 8
  %v1707 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %636 = load i32, i32* %i, align 4
  %mul1708 = mul i32 2, %636
  %add1709 = add i32 %mul1708, 65
  %arrayidx1710 = getelementptr [128 x i64], [128 x i64]* %v1707, i32 0, i32 %add1709
  %637 = load i64, i64* %arrayidx1710, align 8
  %v1711 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %638 = load i32, i32* %i, align 4
  %mul1712 = mul i32 2, %638
  %add1713 = add i32 %mul1712, 112
  %arrayidx1714 = getelementptr [128 x i64], [128 x i64]* %v1711, i32 0, i32 %add1713
  %639 = load i64, i64* %arrayidx1714, align 8
  %call1715 = call i64 @fBlaMka(i64 %637, i64 %639)
  %v1716 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %640 = load i32, i32* %i, align 4
  %mul1717 = mul i32 2, %640
  %add1718 = add i32 %mul1717, 65
  %arrayidx1719 = getelementptr [128 x i64], [128 x i64]* %v1716, i32 0, i32 %add1718
  store i64 %call1715, i64* %arrayidx1719, align 8
  %v1720 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %641 = load i32, i32* %i, align 4
  %mul1721 = mul i32 2, %641
  %add1722 = add i32 %mul1721, 32
  %arrayidx1723 = getelementptr [128 x i64], [128 x i64]* %v1720, i32 0, i32 %add1722
  %642 = load i64, i64* %arrayidx1723, align 8
  %v1724 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %643 = load i32, i32* %i, align 4
  %mul1725 = mul i32 2, %643
  %add1726 = add i32 %mul1725, 65
  %arrayidx1727 = getelementptr [128 x i64], [128 x i64]* %v1724, i32 0, i32 %add1726
  %644 = load i64, i64* %arrayidx1727, align 8
  %xor1728 = xor i64 %642, %644
  %call1729 = call i64 @rotr64(i64 %xor1728, i32 63)
  %v1730 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %645 = load i32, i32* %i, align 4
  %mul1731 = mul i32 2, %645
  %add1732 = add i32 %mul1731, 32
  %arrayidx1733 = getelementptr [128 x i64], [128 x i64]* %v1730, i32 0, i32 %add1732
  store i64 %call1729, i64* %arrayidx1733, align 8
  br label %do.end1734

do.end1734:                                       ; preds = %do.body1625
  br label %do.end1735

do.end1735:                                       ; preds = %do.end1734
  br label %for.inc1736

for.inc1736:                                      ; preds = %do.end1735
  %646 = load i32, i32* %i, align 4
  %inc1737 = add i32 %646, 1
  store i32 %inc1737, i32* %i, align 4
  br label %for.cond863

for.end1738:                                      ; preds = %for.cond863
  %647 = load %struct.block_*, %struct.block_** %next_block.addr, align 4
  call void @copy_block(%struct.block_* %647, %struct.block_* %block_tmp)
  %648 = load %struct.block_*, %struct.block_** %next_block.addr, align 4
  call void @xor_block(%struct.block_* %648, %struct.block_* %blockR)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fill_block(%struct.block_* %prev_block, %struct.block_* %ref_block, %struct.block_* %next_block) #0 {
entry:
  %prev_block.addr = alloca %struct.block_*, align 4
  %ref_block.addr = alloca %struct.block_*, align 4
  %next_block.addr = alloca %struct.block_*, align 4
  %blockR = alloca %struct.block_, align 8
  %block_tmp = alloca %struct.block_, align 8
  %i = alloca i32, align 4
  store %struct.block_* %prev_block, %struct.block_** %prev_block.addr, align 4
  store %struct.block_* %ref_block, %struct.block_** %ref_block.addr, align 4
  store %struct.block_* %next_block, %struct.block_** %next_block.addr, align 4
  %0 = load %struct.block_*, %struct.block_** %ref_block.addr, align 4
  call void @copy_block(%struct.block_* %blockR, %struct.block_* %0)
  %1 = load %struct.block_*, %struct.block_** %prev_block.addr, align 4
  call void @xor_block(%struct.block_* %blockR, %struct.block_* %1)
  call void @copy_block(%struct.block_* %block_tmp, %struct.block_* %blockR)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %do.body

do.body:                                          ; preds = %for.body
  br label %do.body1

do.body1:                                         ; preds = %do.body
  %v = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %mul = mul i32 16, %3
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %mul
  %4 = load i64, i64* %arrayidx, align 8
  %v2 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %mul3 = mul i32 16, %5
  %add = add i32 %mul3, 4
  %arrayidx4 = getelementptr [128 x i64], [128 x i64]* %v2, i32 0, i32 %add
  %6 = load i64, i64* %arrayidx4, align 8
  %call = call i64 @fBlaMka(i64 %4, i64 %6)
  %v5 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %mul6 = mul i32 16, %7
  %arrayidx7 = getelementptr [128 x i64], [128 x i64]* %v5, i32 0, i32 %mul6
  store i64 %call, i64* %arrayidx7, align 8
  %v8 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %8 = load i32, i32* %i, align 4
  %mul9 = mul i32 16, %8
  %add10 = add i32 %mul9, 12
  %arrayidx11 = getelementptr [128 x i64], [128 x i64]* %v8, i32 0, i32 %add10
  %9 = load i64, i64* %arrayidx11, align 8
  %v12 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %10 = load i32, i32* %i, align 4
  %mul13 = mul i32 16, %10
  %arrayidx14 = getelementptr [128 x i64], [128 x i64]* %v12, i32 0, i32 %mul13
  %11 = load i64, i64* %arrayidx14, align 8
  %xor = xor i64 %9, %11
  %call15 = call i64 @rotr64(i64 %xor, i32 32)
  %v16 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %mul17 = mul i32 16, %12
  %add18 = add i32 %mul17, 12
  %arrayidx19 = getelementptr [128 x i64], [128 x i64]* %v16, i32 0, i32 %add18
  store i64 %call15, i64* %arrayidx19, align 8
  %v20 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %mul21 = mul i32 16, %13
  %add22 = add i32 %mul21, 8
  %arrayidx23 = getelementptr [128 x i64], [128 x i64]* %v20, i32 0, i32 %add22
  %14 = load i64, i64* %arrayidx23, align 8
  %v24 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %15 = load i32, i32* %i, align 4
  %mul25 = mul i32 16, %15
  %add26 = add i32 %mul25, 12
  %arrayidx27 = getelementptr [128 x i64], [128 x i64]* %v24, i32 0, i32 %add26
  %16 = load i64, i64* %arrayidx27, align 8
  %call28 = call i64 @fBlaMka(i64 %14, i64 %16)
  %v29 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %mul30 = mul i32 16, %17
  %add31 = add i32 %mul30, 8
  %arrayidx32 = getelementptr [128 x i64], [128 x i64]* %v29, i32 0, i32 %add31
  store i64 %call28, i64* %arrayidx32, align 8
  %v33 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %mul34 = mul i32 16, %18
  %add35 = add i32 %mul34, 4
  %arrayidx36 = getelementptr [128 x i64], [128 x i64]* %v33, i32 0, i32 %add35
  %19 = load i64, i64* %arrayidx36, align 8
  %v37 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %mul38 = mul i32 16, %20
  %add39 = add i32 %mul38, 8
  %arrayidx40 = getelementptr [128 x i64], [128 x i64]* %v37, i32 0, i32 %add39
  %21 = load i64, i64* %arrayidx40, align 8
  %xor41 = xor i64 %19, %21
  %call42 = call i64 @rotr64(i64 %xor41, i32 24)
  %v43 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %22 = load i32, i32* %i, align 4
  %mul44 = mul i32 16, %22
  %add45 = add i32 %mul44, 4
  %arrayidx46 = getelementptr [128 x i64], [128 x i64]* %v43, i32 0, i32 %add45
  store i64 %call42, i64* %arrayidx46, align 8
  %v47 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %23 = load i32, i32* %i, align 4
  %mul48 = mul i32 16, %23
  %arrayidx49 = getelementptr [128 x i64], [128 x i64]* %v47, i32 0, i32 %mul48
  %24 = load i64, i64* %arrayidx49, align 8
  %v50 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %25 = load i32, i32* %i, align 4
  %mul51 = mul i32 16, %25
  %add52 = add i32 %mul51, 4
  %arrayidx53 = getelementptr [128 x i64], [128 x i64]* %v50, i32 0, i32 %add52
  %26 = load i64, i64* %arrayidx53, align 8
  %call54 = call i64 @fBlaMka(i64 %24, i64 %26)
  %v55 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %27 = load i32, i32* %i, align 4
  %mul56 = mul i32 16, %27
  %arrayidx57 = getelementptr [128 x i64], [128 x i64]* %v55, i32 0, i32 %mul56
  store i64 %call54, i64* %arrayidx57, align 8
  %v58 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %28 = load i32, i32* %i, align 4
  %mul59 = mul i32 16, %28
  %add60 = add i32 %mul59, 12
  %arrayidx61 = getelementptr [128 x i64], [128 x i64]* %v58, i32 0, i32 %add60
  %29 = load i64, i64* %arrayidx61, align 8
  %v62 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %30 = load i32, i32* %i, align 4
  %mul63 = mul i32 16, %30
  %arrayidx64 = getelementptr [128 x i64], [128 x i64]* %v62, i32 0, i32 %mul63
  %31 = load i64, i64* %arrayidx64, align 8
  %xor65 = xor i64 %29, %31
  %call66 = call i64 @rotr64(i64 %xor65, i32 16)
  %v67 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %mul68 = mul i32 16, %32
  %add69 = add i32 %mul68, 12
  %arrayidx70 = getelementptr [128 x i64], [128 x i64]* %v67, i32 0, i32 %add69
  store i64 %call66, i64* %arrayidx70, align 8
  %v71 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %33 = load i32, i32* %i, align 4
  %mul72 = mul i32 16, %33
  %add73 = add i32 %mul72, 8
  %arrayidx74 = getelementptr [128 x i64], [128 x i64]* %v71, i32 0, i32 %add73
  %34 = load i64, i64* %arrayidx74, align 8
  %v75 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %35 = load i32, i32* %i, align 4
  %mul76 = mul i32 16, %35
  %add77 = add i32 %mul76, 12
  %arrayidx78 = getelementptr [128 x i64], [128 x i64]* %v75, i32 0, i32 %add77
  %36 = load i64, i64* %arrayidx78, align 8
  %call79 = call i64 @fBlaMka(i64 %34, i64 %36)
  %v80 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %37 = load i32, i32* %i, align 4
  %mul81 = mul i32 16, %37
  %add82 = add i32 %mul81, 8
  %arrayidx83 = getelementptr [128 x i64], [128 x i64]* %v80, i32 0, i32 %add82
  store i64 %call79, i64* %arrayidx83, align 8
  %v84 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %38 = load i32, i32* %i, align 4
  %mul85 = mul i32 16, %38
  %add86 = add i32 %mul85, 4
  %arrayidx87 = getelementptr [128 x i64], [128 x i64]* %v84, i32 0, i32 %add86
  %39 = load i64, i64* %arrayidx87, align 8
  %v88 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %40 = load i32, i32* %i, align 4
  %mul89 = mul i32 16, %40
  %add90 = add i32 %mul89, 8
  %arrayidx91 = getelementptr [128 x i64], [128 x i64]* %v88, i32 0, i32 %add90
  %41 = load i64, i64* %arrayidx91, align 8
  %xor92 = xor i64 %39, %41
  %call93 = call i64 @rotr64(i64 %xor92, i32 63)
  %v94 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %42 = load i32, i32* %i, align 4
  %mul95 = mul i32 16, %42
  %add96 = add i32 %mul95, 4
  %arrayidx97 = getelementptr [128 x i64], [128 x i64]* %v94, i32 0, i32 %add96
  store i64 %call93, i64* %arrayidx97, align 8
  br label %do.end

do.end:                                           ; preds = %do.body1
  br label %do.body98

do.body98:                                        ; preds = %do.end
  %v99 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %43 = load i32, i32* %i, align 4
  %mul100 = mul i32 16, %43
  %add101 = add i32 %mul100, 1
  %arrayidx102 = getelementptr [128 x i64], [128 x i64]* %v99, i32 0, i32 %add101
  %44 = load i64, i64* %arrayidx102, align 8
  %v103 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %45 = load i32, i32* %i, align 4
  %mul104 = mul i32 16, %45
  %add105 = add i32 %mul104, 5
  %arrayidx106 = getelementptr [128 x i64], [128 x i64]* %v103, i32 0, i32 %add105
  %46 = load i64, i64* %arrayidx106, align 8
  %call107 = call i64 @fBlaMka(i64 %44, i64 %46)
  %v108 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %47 = load i32, i32* %i, align 4
  %mul109 = mul i32 16, %47
  %add110 = add i32 %mul109, 1
  %arrayidx111 = getelementptr [128 x i64], [128 x i64]* %v108, i32 0, i32 %add110
  store i64 %call107, i64* %arrayidx111, align 8
  %v112 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %mul113 = mul i32 16, %48
  %add114 = add i32 %mul113, 13
  %arrayidx115 = getelementptr [128 x i64], [128 x i64]* %v112, i32 0, i32 %add114
  %49 = load i64, i64* %arrayidx115, align 8
  %v116 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %50 = load i32, i32* %i, align 4
  %mul117 = mul i32 16, %50
  %add118 = add i32 %mul117, 1
  %arrayidx119 = getelementptr [128 x i64], [128 x i64]* %v116, i32 0, i32 %add118
  %51 = load i64, i64* %arrayidx119, align 8
  %xor120 = xor i64 %49, %51
  %call121 = call i64 @rotr64(i64 %xor120, i32 32)
  %v122 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %52 = load i32, i32* %i, align 4
  %mul123 = mul i32 16, %52
  %add124 = add i32 %mul123, 13
  %arrayidx125 = getelementptr [128 x i64], [128 x i64]* %v122, i32 0, i32 %add124
  store i64 %call121, i64* %arrayidx125, align 8
  %v126 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %53 = load i32, i32* %i, align 4
  %mul127 = mul i32 16, %53
  %add128 = add i32 %mul127, 9
  %arrayidx129 = getelementptr [128 x i64], [128 x i64]* %v126, i32 0, i32 %add128
  %54 = load i64, i64* %arrayidx129, align 8
  %v130 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %55 = load i32, i32* %i, align 4
  %mul131 = mul i32 16, %55
  %add132 = add i32 %mul131, 13
  %arrayidx133 = getelementptr [128 x i64], [128 x i64]* %v130, i32 0, i32 %add132
  %56 = load i64, i64* %arrayidx133, align 8
  %call134 = call i64 @fBlaMka(i64 %54, i64 %56)
  %v135 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %57 = load i32, i32* %i, align 4
  %mul136 = mul i32 16, %57
  %add137 = add i32 %mul136, 9
  %arrayidx138 = getelementptr [128 x i64], [128 x i64]* %v135, i32 0, i32 %add137
  store i64 %call134, i64* %arrayidx138, align 8
  %v139 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %58 = load i32, i32* %i, align 4
  %mul140 = mul i32 16, %58
  %add141 = add i32 %mul140, 5
  %arrayidx142 = getelementptr [128 x i64], [128 x i64]* %v139, i32 0, i32 %add141
  %59 = load i64, i64* %arrayidx142, align 8
  %v143 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %60 = load i32, i32* %i, align 4
  %mul144 = mul i32 16, %60
  %add145 = add i32 %mul144, 9
  %arrayidx146 = getelementptr [128 x i64], [128 x i64]* %v143, i32 0, i32 %add145
  %61 = load i64, i64* %arrayidx146, align 8
  %xor147 = xor i64 %59, %61
  %call148 = call i64 @rotr64(i64 %xor147, i32 24)
  %v149 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %62 = load i32, i32* %i, align 4
  %mul150 = mul i32 16, %62
  %add151 = add i32 %mul150, 5
  %arrayidx152 = getelementptr [128 x i64], [128 x i64]* %v149, i32 0, i32 %add151
  store i64 %call148, i64* %arrayidx152, align 8
  %v153 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %63 = load i32, i32* %i, align 4
  %mul154 = mul i32 16, %63
  %add155 = add i32 %mul154, 1
  %arrayidx156 = getelementptr [128 x i64], [128 x i64]* %v153, i32 0, i32 %add155
  %64 = load i64, i64* %arrayidx156, align 8
  %v157 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %65 = load i32, i32* %i, align 4
  %mul158 = mul i32 16, %65
  %add159 = add i32 %mul158, 5
  %arrayidx160 = getelementptr [128 x i64], [128 x i64]* %v157, i32 0, i32 %add159
  %66 = load i64, i64* %arrayidx160, align 8
  %call161 = call i64 @fBlaMka(i64 %64, i64 %66)
  %v162 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %67 = load i32, i32* %i, align 4
  %mul163 = mul i32 16, %67
  %add164 = add i32 %mul163, 1
  %arrayidx165 = getelementptr [128 x i64], [128 x i64]* %v162, i32 0, i32 %add164
  store i64 %call161, i64* %arrayidx165, align 8
  %v166 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %68 = load i32, i32* %i, align 4
  %mul167 = mul i32 16, %68
  %add168 = add i32 %mul167, 13
  %arrayidx169 = getelementptr [128 x i64], [128 x i64]* %v166, i32 0, i32 %add168
  %69 = load i64, i64* %arrayidx169, align 8
  %v170 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %70 = load i32, i32* %i, align 4
  %mul171 = mul i32 16, %70
  %add172 = add i32 %mul171, 1
  %arrayidx173 = getelementptr [128 x i64], [128 x i64]* %v170, i32 0, i32 %add172
  %71 = load i64, i64* %arrayidx173, align 8
  %xor174 = xor i64 %69, %71
  %call175 = call i64 @rotr64(i64 %xor174, i32 16)
  %v176 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %72 = load i32, i32* %i, align 4
  %mul177 = mul i32 16, %72
  %add178 = add i32 %mul177, 13
  %arrayidx179 = getelementptr [128 x i64], [128 x i64]* %v176, i32 0, i32 %add178
  store i64 %call175, i64* %arrayidx179, align 8
  %v180 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %73 = load i32, i32* %i, align 4
  %mul181 = mul i32 16, %73
  %add182 = add i32 %mul181, 9
  %arrayidx183 = getelementptr [128 x i64], [128 x i64]* %v180, i32 0, i32 %add182
  %74 = load i64, i64* %arrayidx183, align 8
  %v184 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %75 = load i32, i32* %i, align 4
  %mul185 = mul i32 16, %75
  %add186 = add i32 %mul185, 13
  %arrayidx187 = getelementptr [128 x i64], [128 x i64]* %v184, i32 0, i32 %add186
  %76 = load i64, i64* %arrayidx187, align 8
  %call188 = call i64 @fBlaMka(i64 %74, i64 %76)
  %v189 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %77 = load i32, i32* %i, align 4
  %mul190 = mul i32 16, %77
  %add191 = add i32 %mul190, 9
  %arrayidx192 = getelementptr [128 x i64], [128 x i64]* %v189, i32 0, i32 %add191
  store i64 %call188, i64* %arrayidx192, align 8
  %v193 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %78 = load i32, i32* %i, align 4
  %mul194 = mul i32 16, %78
  %add195 = add i32 %mul194, 5
  %arrayidx196 = getelementptr [128 x i64], [128 x i64]* %v193, i32 0, i32 %add195
  %79 = load i64, i64* %arrayidx196, align 8
  %v197 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %80 = load i32, i32* %i, align 4
  %mul198 = mul i32 16, %80
  %add199 = add i32 %mul198, 9
  %arrayidx200 = getelementptr [128 x i64], [128 x i64]* %v197, i32 0, i32 %add199
  %81 = load i64, i64* %arrayidx200, align 8
  %xor201 = xor i64 %79, %81
  %call202 = call i64 @rotr64(i64 %xor201, i32 63)
  %v203 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %82 = load i32, i32* %i, align 4
  %mul204 = mul i32 16, %82
  %add205 = add i32 %mul204, 5
  %arrayidx206 = getelementptr [128 x i64], [128 x i64]* %v203, i32 0, i32 %add205
  store i64 %call202, i64* %arrayidx206, align 8
  br label %do.end207

do.end207:                                        ; preds = %do.body98
  br label %do.body208

do.body208:                                       ; preds = %do.end207
  %v209 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %83 = load i32, i32* %i, align 4
  %mul210 = mul i32 16, %83
  %add211 = add i32 %mul210, 2
  %arrayidx212 = getelementptr [128 x i64], [128 x i64]* %v209, i32 0, i32 %add211
  %84 = load i64, i64* %arrayidx212, align 8
  %v213 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %85 = load i32, i32* %i, align 4
  %mul214 = mul i32 16, %85
  %add215 = add i32 %mul214, 6
  %arrayidx216 = getelementptr [128 x i64], [128 x i64]* %v213, i32 0, i32 %add215
  %86 = load i64, i64* %arrayidx216, align 8
  %call217 = call i64 @fBlaMka(i64 %84, i64 %86)
  %v218 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %87 = load i32, i32* %i, align 4
  %mul219 = mul i32 16, %87
  %add220 = add i32 %mul219, 2
  %arrayidx221 = getelementptr [128 x i64], [128 x i64]* %v218, i32 0, i32 %add220
  store i64 %call217, i64* %arrayidx221, align 8
  %v222 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %88 = load i32, i32* %i, align 4
  %mul223 = mul i32 16, %88
  %add224 = add i32 %mul223, 14
  %arrayidx225 = getelementptr [128 x i64], [128 x i64]* %v222, i32 0, i32 %add224
  %89 = load i64, i64* %arrayidx225, align 8
  %v226 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %90 = load i32, i32* %i, align 4
  %mul227 = mul i32 16, %90
  %add228 = add i32 %mul227, 2
  %arrayidx229 = getelementptr [128 x i64], [128 x i64]* %v226, i32 0, i32 %add228
  %91 = load i64, i64* %arrayidx229, align 8
  %xor230 = xor i64 %89, %91
  %call231 = call i64 @rotr64(i64 %xor230, i32 32)
  %v232 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %92 = load i32, i32* %i, align 4
  %mul233 = mul i32 16, %92
  %add234 = add i32 %mul233, 14
  %arrayidx235 = getelementptr [128 x i64], [128 x i64]* %v232, i32 0, i32 %add234
  store i64 %call231, i64* %arrayidx235, align 8
  %v236 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %93 = load i32, i32* %i, align 4
  %mul237 = mul i32 16, %93
  %add238 = add i32 %mul237, 10
  %arrayidx239 = getelementptr [128 x i64], [128 x i64]* %v236, i32 0, i32 %add238
  %94 = load i64, i64* %arrayidx239, align 8
  %v240 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %95 = load i32, i32* %i, align 4
  %mul241 = mul i32 16, %95
  %add242 = add i32 %mul241, 14
  %arrayidx243 = getelementptr [128 x i64], [128 x i64]* %v240, i32 0, i32 %add242
  %96 = load i64, i64* %arrayidx243, align 8
  %call244 = call i64 @fBlaMka(i64 %94, i64 %96)
  %v245 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %97 = load i32, i32* %i, align 4
  %mul246 = mul i32 16, %97
  %add247 = add i32 %mul246, 10
  %arrayidx248 = getelementptr [128 x i64], [128 x i64]* %v245, i32 0, i32 %add247
  store i64 %call244, i64* %arrayidx248, align 8
  %v249 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %98 = load i32, i32* %i, align 4
  %mul250 = mul i32 16, %98
  %add251 = add i32 %mul250, 6
  %arrayidx252 = getelementptr [128 x i64], [128 x i64]* %v249, i32 0, i32 %add251
  %99 = load i64, i64* %arrayidx252, align 8
  %v253 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %100 = load i32, i32* %i, align 4
  %mul254 = mul i32 16, %100
  %add255 = add i32 %mul254, 10
  %arrayidx256 = getelementptr [128 x i64], [128 x i64]* %v253, i32 0, i32 %add255
  %101 = load i64, i64* %arrayidx256, align 8
  %xor257 = xor i64 %99, %101
  %call258 = call i64 @rotr64(i64 %xor257, i32 24)
  %v259 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %102 = load i32, i32* %i, align 4
  %mul260 = mul i32 16, %102
  %add261 = add i32 %mul260, 6
  %arrayidx262 = getelementptr [128 x i64], [128 x i64]* %v259, i32 0, i32 %add261
  store i64 %call258, i64* %arrayidx262, align 8
  %v263 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %103 = load i32, i32* %i, align 4
  %mul264 = mul i32 16, %103
  %add265 = add i32 %mul264, 2
  %arrayidx266 = getelementptr [128 x i64], [128 x i64]* %v263, i32 0, i32 %add265
  %104 = load i64, i64* %arrayidx266, align 8
  %v267 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %105 = load i32, i32* %i, align 4
  %mul268 = mul i32 16, %105
  %add269 = add i32 %mul268, 6
  %arrayidx270 = getelementptr [128 x i64], [128 x i64]* %v267, i32 0, i32 %add269
  %106 = load i64, i64* %arrayidx270, align 8
  %call271 = call i64 @fBlaMka(i64 %104, i64 %106)
  %v272 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %107 = load i32, i32* %i, align 4
  %mul273 = mul i32 16, %107
  %add274 = add i32 %mul273, 2
  %arrayidx275 = getelementptr [128 x i64], [128 x i64]* %v272, i32 0, i32 %add274
  store i64 %call271, i64* %arrayidx275, align 8
  %v276 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %108 = load i32, i32* %i, align 4
  %mul277 = mul i32 16, %108
  %add278 = add i32 %mul277, 14
  %arrayidx279 = getelementptr [128 x i64], [128 x i64]* %v276, i32 0, i32 %add278
  %109 = load i64, i64* %arrayidx279, align 8
  %v280 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %110 = load i32, i32* %i, align 4
  %mul281 = mul i32 16, %110
  %add282 = add i32 %mul281, 2
  %arrayidx283 = getelementptr [128 x i64], [128 x i64]* %v280, i32 0, i32 %add282
  %111 = load i64, i64* %arrayidx283, align 8
  %xor284 = xor i64 %109, %111
  %call285 = call i64 @rotr64(i64 %xor284, i32 16)
  %v286 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %112 = load i32, i32* %i, align 4
  %mul287 = mul i32 16, %112
  %add288 = add i32 %mul287, 14
  %arrayidx289 = getelementptr [128 x i64], [128 x i64]* %v286, i32 0, i32 %add288
  store i64 %call285, i64* %arrayidx289, align 8
  %v290 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %113 = load i32, i32* %i, align 4
  %mul291 = mul i32 16, %113
  %add292 = add i32 %mul291, 10
  %arrayidx293 = getelementptr [128 x i64], [128 x i64]* %v290, i32 0, i32 %add292
  %114 = load i64, i64* %arrayidx293, align 8
  %v294 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %115 = load i32, i32* %i, align 4
  %mul295 = mul i32 16, %115
  %add296 = add i32 %mul295, 14
  %arrayidx297 = getelementptr [128 x i64], [128 x i64]* %v294, i32 0, i32 %add296
  %116 = load i64, i64* %arrayidx297, align 8
  %call298 = call i64 @fBlaMka(i64 %114, i64 %116)
  %v299 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %117 = load i32, i32* %i, align 4
  %mul300 = mul i32 16, %117
  %add301 = add i32 %mul300, 10
  %arrayidx302 = getelementptr [128 x i64], [128 x i64]* %v299, i32 0, i32 %add301
  store i64 %call298, i64* %arrayidx302, align 8
  %v303 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %118 = load i32, i32* %i, align 4
  %mul304 = mul i32 16, %118
  %add305 = add i32 %mul304, 6
  %arrayidx306 = getelementptr [128 x i64], [128 x i64]* %v303, i32 0, i32 %add305
  %119 = load i64, i64* %arrayidx306, align 8
  %v307 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %120 = load i32, i32* %i, align 4
  %mul308 = mul i32 16, %120
  %add309 = add i32 %mul308, 10
  %arrayidx310 = getelementptr [128 x i64], [128 x i64]* %v307, i32 0, i32 %add309
  %121 = load i64, i64* %arrayidx310, align 8
  %xor311 = xor i64 %119, %121
  %call312 = call i64 @rotr64(i64 %xor311, i32 63)
  %v313 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %122 = load i32, i32* %i, align 4
  %mul314 = mul i32 16, %122
  %add315 = add i32 %mul314, 6
  %arrayidx316 = getelementptr [128 x i64], [128 x i64]* %v313, i32 0, i32 %add315
  store i64 %call312, i64* %arrayidx316, align 8
  br label %do.end317

do.end317:                                        ; preds = %do.body208
  br label %do.body318

do.body318:                                       ; preds = %do.end317
  %v319 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %123 = load i32, i32* %i, align 4
  %mul320 = mul i32 16, %123
  %add321 = add i32 %mul320, 3
  %arrayidx322 = getelementptr [128 x i64], [128 x i64]* %v319, i32 0, i32 %add321
  %124 = load i64, i64* %arrayidx322, align 8
  %v323 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %125 = load i32, i32* %i, align 4
  %mul324 = mul i32 16, %125
  %add325 = add i32 %mul324, 7
  %arrayidx326 = getelementptr [128 x i64], [128 x i64]* %v323, i32 0, i32 %add325
  %126 = load i64, i64* %arrayidx326, align 8
  %call327 = call i64 @fBlaMka(i64 %124, i64 %126)
  %v328 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %127 = load i32, i32* %i, align 4
  %mul329 = mul i32 16, %127
  %add330 = add i32 %mul329, 3
  %arrayidx331 = getelementptr [128 x i64], [128 x i64]* %v328, i32 0, i32 %add330
  store i64 %call327, i64* %arrayidx331, align 8
  %v332 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %128 = load i32, i32* %i, align 4
  %mul333 = mul i32 16, %128
  %add334 = add i32 %mul333, 15
  %arrayidx335 = getelementptr [128 x i64], [128 x i64]* %v332, i32 0, i32 %add334
  %129 = load i64, i64* %arrayidx335, align 8
  %v336 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %130 = load i32, i32* %i, align 4
  %mul337 = mul i32 16, %130
  %add338 = add i32 %mul337, 3
  %arrayidx339 = getelementptr [128 x i64], [128 x i64]* %v336, i32 0, i32 %add338
  %131 = load i64, i64* %arrayidx339, align 8
  %xor340 = xor i64 %129, %131
  %call341 = call i64 @rotr64(i64 %xor340, i32 32)
  %v342 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %132 = load i32, i32* %i, align 4
  %mul343 = mul i32 16, %132
  %add344 = add i32 %mul343, 15
  %arrayidx345 = getelementptr [128 x i64], [128 x i64]* %v342, i32 0, i32 %add344
  store i64 %call341, i64* %arrayidx345, align 8
  %v346 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %133 = load i32, i32* %i, align 4
  %mul347 = mul i32 16, %133
  %add348 = add i32 %mul347, 11
  %arrayidx349 = getelementptr [128 x i64], [128 x i64]* %v346, i32 0, i32 %add348
  %134 = load i64, i64* %arrayidx349, align 8
  %v350 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %135 = load i32, i32* %i, align 4
  %mul351 = mul i32 16, %135
  %add352 = add i32 %mul351, 15
  %arrayidx353 = getelementptr [128 x i64], [128 x i64]* %v350, i32 0, i32 %add352
  %136 = load i64, i64* %arrayidx353, align 8
  %call354 = call i64 @fBlaMka(i64 %134, i64 %136)
  %v355 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %137 = load i32, i32* %i, align 4
  %mul356 = mul i32 16, %137
  %add357 = add i32 %mul356, 11
  %arrayidx358 = getelementptr [128 x i64], [128 x i64]* %v355, i32 0, i32 %add357
  store i64 %call354, i64* %arrayidx358, align 8
  %v359 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %138 = load i32, i32* %i, align 4
  %mul360 = mul i32 16, %138
  %add361 = add i32 %mul360, 7
  %arrayidx362 = getelementptr [128 x i64], [128 x i64]* %v359, i32 0, i32 %add361
  %139 = load i64, i64* %arrayidx362, align 8
  %v363 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %140 = load i32, i32* %i, align 4
  %mul364 = mul i32 16, %140
  %add365 = add i32 %mul364, 11
  %arrayidx366 = getelementptr [128 x i64], [128 x i64]* %v363, i32 0, i32 %add365
  %141 = load i64, i64* %arrayidx366, align 8
  %xor367 = xor i64 %139, %141
  %call368 = call i64 @rotr64(i64 %xor367, i32 24)
  %v369 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %142 = load i32, i32* %i, align 4
  %mul370 = mul i32 16, %142
  %add371 = add i32 %mul370, 7
  %arrayidx372 = getelementptr [128 x i64], [128 x i64]* %v369, i32 0, i32 %add371
  store i64 %call368, i64* %arrayidx372, align 8
  %v373 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %143 = load i32, i32* %i, align 4
  %mul374 = mul i32 16, %143
  %add375 = add i32 %mul374, 3
  %arrayidx376 = getelementptr [128 x i64], [128 x i64]* %v373, i32 0, i32 %add375
  %144 = load i64, i64* %arrayidx376, align 8
  %v377 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %145 = load i32, i32* %i, align 4
  %mul378 = mul i32 16, %145
  %add379 = add i32 %mul378, 7
  %arrayidx380 = getelementptr [128 x i64], [128 x i64]* %v377, i32 0, i32 %add379
  %146 = load i64, i64* %arrayidx380, align 8
  %call381 = call i64 @fBlaMka(i64 %144, i64 %146)
  %v382 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %147 = load i32, i32* %i, align 4
  %mul383 = mul i32 16, %147
  %add384 = add i32 %mul383, 3
  %arrayidx385 = getelementptr [128 x i64], [128 x i64]* %v382, i32 0, i32 %add384
  store i64 %call381, i64* %arrayidx385, align 8
  %v386 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %148 = load i32, i32* %i, align 4
  %mul387 = mul i32 16, %148
  %add388 = add i32 %mul387, 15
  %arrayidx389 = getelementptr [128 x i64], [128 x i64]* %v386, i32 0, i32 %add388
  %149 = load i64, i64* %arrayidx389, align 8
  %v390 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %150 = load i32, i32* %i, align 4
  %mul391 = mul i32 16, %150
  %add392 = add i32 %mul391, 3
  %arrayidx393 = getelementptr [128 x i64], [128 x i64]* %v390, i32 0, i32 %add392
  %151 = load i64, i64* %arrayidx393, align 8
  %xor394 = xor i64 %149, %151
  %call395 = call i64 @rotr64(i64 %xor394, i32 16)
  %v396 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %152 = load i32, i32* %i, align 4
  %mul397 = mul i32 16, %152
  %add398 = add i32 %mul397, 15
  %arrayidx399 = getelementptr [128 x i64], [128 x i64]* %v396, i32 0, i32 %add398
  store i64 %call395, i64* %arrayidx399, align 8
  %v400 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %153 = load i32, i32* %i, align 4
  %mul401 = mul i32 16, %153
  %add402 = add i32 %mul401, 11
  %arrayidx403 = getelementptr [128 x i64], [128 x i64]* %v400, i32 0, i32 %add402
  %154 = load i64, i64* %arrayidx403, align 8
  %v404 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %155 = load i32, i32* %i, align 4
  %mul405 = mul i32 16, %155
  %add406 = add i32 %mul405, 15
  %arrayidx407 = getelementptr [128 x i64], [128 x i64]* %v404, i32 0, i32 %add406
  %156 = load i64, i64* %arrayidx407, align 8
  %call408 = call i64 @fBlaMka(i64 %154, i64 %156)
  %v409 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %157 = load i32, i32* %i, align 4
  %mul410 = mul i32 16, %157
  %add411 = add i32 %mul410, 11
  %arrayidx412 = getelementptr [128 x i64], [128 x i64]* %v409, i32 0, i32 %add411
  store i64 %call408, i64* %arrayidx412, align 8
  %v413 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %158 = load i32, i32* %i, align 4
  %mul414 = mul i32 16, %158
  %add415 = add i32 %mul414, 7
  %arrayidx416 = getelementptr [128 x i64], [128 x i64]* %v413, i32 0, i32 %add415
  %159 = load i64, i64* %arrayidx416, align 8
  %v417 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %160 = load i32, i32* %i, align 4
  %mul418 = mul i32 16, %160
  %add419 = add i32 %mul418, 11
  %arrayidx420 = getelementptr [128 x i64], [128 x i64]* %v417, i32 0, i32 %add419
  %161 = load i64, i64* %arrayidx420, align 8
  %xor421 = xor i64 %159, %161
  %call422 = call i64 @rotr64(i64 %xor421, i32 63)
  %v423 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %162 = load i32, i32* %i, align 4
  %mul424 = mul i32 16, %162
  %add425 = add i32 %mul424, 7
  %arrayidx426 = getelementptr [128 x i64], [128 x i64]* %v423, i32 0, i32 %add425
  store i64 %call422, i64* %arrayidx426, align 8
  br label %do.end427

do.end427:                                        ; preds = %do.body318
  br label %do.body428

do.body428:                                       ; preds = %do.end427
  %v429 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %163 = load i32, i32* %i, align 4
  %mul430 = mul i32 16, %163
  %arrayidx431 = getelementptr [128 x i64], [128 x i64]* %v429, i32 0, i32 %mul430
  %164 = load i64, i64* %arrayidx431, align 8
  %v432 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %165 = load i32, i32* %i, align 4
  %mul433 = mul i32 16, %165
  %add434 = add i32 %mul433, 5
  %arrayidx435 = getelementptr [128 x i64], [128 x i64]* %v432, i32 0, i32 %add434
  %166 = load i64, i64* %arrayidx435, align 8
  %call436 = call i64 @fBlaMka(i64 %164, i64 %166)
  %v437 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %167 = load i32, i32* %i, align 4
  %mul438 = mul i32 16, %167
  %arrayidx439 = getelementptr [128 x i64], [128 x i64]* %v437, i32 0, i32 %mul438
  store i64 %call436, i64* %arrayidx439, align 8
  %v440 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %168 = load i32, i32* %i, align 4
  %mul441 = mul i32 16, %168
  %add442 = add i32 %mul441, 15
  %arrayidx443 = getelementptr [128 x i64], [128 x i64]* %v440, i32 0, i32 %add442
  %169 = load i64, i64* %arrayidx443, align 8
  %v444 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %170 = load i32, i32* %i, align 4
  %mul445 = mul i32 16, %170
  %arrayidx446 = getelementptr [128 x i64], [128 x i64]* %v444, i32 0, i32 %mul445
  %171 = load i64, i64* %arrayidx446, align 8
  %xor447 = xor i64 %169, %171
  %call448 = call i64 @rotr64(i64 %xor447, i32 32)
  %v449 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %172 = load i32, i32* %i, align 4
  %mul450 = mul i32 16, %172
  %add451 = add i32 %mul450, 15
  %arrayidx452 = getelementptr [128 x i64], [128 x i64]* %v449, i32 0, i32 %add451
  store i64 %call448, i64* %arrayidx452, align 8
  %v453 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %173 = load i32, i32* %i, align 4
  %mul454 = mul i32 16, %173
  %add455 = add i32 %mul454, 10
  %arrayidx456 = getelementptr [128 x i64], [128 x i64]* %v453, i32 0, i32 %add455
  %174 = load i64, i64* %arrayidx456, align 8
  %v457 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %175 = load i32, i32* %i, align 4
  %mul458 = mul i32 16, %175
  %add459 = add i32 %mul458, 15
  %arrayidx460 = getelementptr [128 x i64], [128 x i64]* %v457, i32 0, i32 %add459
  %176 = load i64, i64* %arrayidx460, align 8
  %call461 = call i64 @fBlaMka(i64 %174, i64 %176)
  %v462 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %177 = load i32, i32* %i, align 4
  %mul463 = mul i32 16, %177
  %add464 = add i32 %mul463, 10
  %arrayidx465 = getelementptr [128 x i64], [128 x i64]* %v462, i32 0, i32 %add464
  store i64 %call461, i64* %arrayidx465, align 8
  %v466 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %178 = load i32, i32* %i, align 4
  %mul467 = mul i32 16, %178
  %add468 = add i32 %mul467, 5
  %arrayidx469 = getelementptr [128 x i64], [128 x i64]* %v466, i32 0, i32 %add468
  %179 = load i64, i64* %arrayidx469, align 8
  %v470 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %180 = load i32, i32* %i, align 4
  %mul471 = mul i32 16, %180
  %add472 = add i32 %mul471, 10
  %arrayidx473 = getelementptr [128 x i64], [128 x i64]* %v470, i32 0, i32 %add472
  %181 = load i64, i64* %arrayidx473, align 8
  %xor474 = xor i64 %179, %181
  %call475 = call i64 @rotr64(i64 %xor474, i32 24)
  %v476 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %182 = load i32, i32* %i, align 4
  %mul477 = mul i32 16, %182
  %add478 = add i32 %mul477, 5
  %arrayidx479 = getelementptr [128 x i64], [128 x i64]* %v476, i32 0, i32 %add478
  store i64 %call475, i64* %arrayidx479, align 8
  %v480 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %183 = load i32, i32* %i, align 4
  %mul481 = mul i32 16, %183
  %arrayidx482 = getelementptr [128 x i64], [128 x i64]* %v480, i32 0, i32 %mul481
  %184 = load i64, i64* %arrayidx482, align 8
  %v483 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %185 = load i32, i32* %i, align 4
  %mul484 = mul i32 16, %185
  %add485 = add i32 %mul484, 5
  %arrayidx486 = getelementptr [128 x i64], [128 x i64]* %v483, i32 0, i32 %add485
  %186 = load i64, i64* %arrayidx486, align 8
  %call487 = call i64 @fBlaMka(i64 %184, i64 %186)
  %v488 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %187 = load i32, i32* %i, align 4
  %mul489 = mul i32 16, %187
  %arrayidx490 = getelementptr [128 x i64], [128 x i64]* %v488, i32 0, i32 %mul489
  store i64 %call487, i64* %arrayidx490, align 8
  %v491 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %188 = load i32, i32* %i, align 4
  %mul492 = mul i32 16, %188
  %add493 = add i32 %mul492, 15
  %arrayidx494 = getelementptr [128 x i64], [128 x i64]* %v491, i32 0, i32 %add493
  %189 = load i64, i64* %arrayidx494, align 8
  %v495 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %190 = load i32, i32* %i, align 4
  %mul496 = mul i32 16, %190
  %arrayidx497 = getelementptr [128 x i64], [128 x i64]* %v495, i32 0, i32 %mul496
  %191 = load i64, i64* %arrayidx497, align 8
  %xor498 = xor i64 %189, %191
  %call499 = call i64 @rotr64(i64 %xor498, i32 16)
  %v500 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %192 = load i32, i32* %i, align 4
  %mul501 = mul i32 16, %192
  %add502 = add i32 %mul501, 15
  %arrayidx503 = getelementptr [128 x i64], [128 x i64]* %v500, i32 0, i32 %add502
  store i64 %call499, i64* %arrayidx503, align 8
  %v504 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %193 = load i32, i32* %i, align 4
  %mul505 = mul i32 16, %193
  %add506 = add i32 %mul505, 10
  %arrayidx507 = getelementptr [128 x i64], [128 x i64]* %v504, i32 0, i32 %add506
  %194 = load i64, i64* %arrayidx507, align 8
  %v508 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %195 = load i32, i32* %i, align 4
  %mul509 = mul i32 16, %195
  %add510 = add i32 %mul509, 15
  %arrayidx511 = getelementptr [128 x i64], [128 x i64]* %v508, i32 0, i32 %add510
  %196 = load i64, i64* %arrayidx511, align 8
  %call512 = call i64 @fBlaMka(i64 %194, i64 %196)
  %v513 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %197 = load i32, i32* %i, align 4
  %mul514 = mul i32 16, %197
  %add515 = add i32 %mul514, 10
  %arrayidx516 = getelementptr [128 x i64], [128 x i64]* %v513, i32 0, i32 %add515
  store i64 %call512, i64* %arrayidx516, align 8
  %v517 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %198 = load i32, i32* %i, align 4
  %mul518 = mul i32 16, %198
  %add519 = add i32 %mul518, 5
  %arrayidx520 = getelementptr [128 x i64], [128 x i64]* %v517, i32 0, i32 %add519
  %199 = load i64, i64* %arrayidx520, align 8
  %v521 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %200 = load i32, i32* %i, align 4
  %mul522 = mul i32 16, %200
  %add523 = add i32 %mul522, 10
  %arrayidx524 = getelementptr [128 x i64], [128 x i64]* %v521, i32 0, i32 %add523
  %201 = load i64, i64* %arrayidx524, align 8
  %xor525 = xor i64 %199, %201
  %call526 = call i64 @rotr64(i64 %xor525, i32 63)
  %v527 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %202 = load i32, i32* %i, align 4
  %mul528 = mul i32 16, %202
  %add529 = add i32 %mul528, 5
  %arrayidx530 = getelementptr [128 x i64], [128 x i64]* %v527, i32 0, i32 %add529
  store i64 %call526, i64* %arrayidx530, align 8
  br label %do.end531

do.end531:                                        ; preds = %do.body428
  br label %do.body532

do.body532:                                       ; preds = %do.end531
  %v533 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %203 = load i32, i32* %i, align 4
  %mul534 = mul i32 16, %203
  %add535 = add i32 %mul534, 1
  %arrayidx536 = getelementptr [128 x i64], [128 x i64]* %v533, i32 0, i32 %add535
  %204 = load i64, i64* %arrayidx536, align 8
  %v537 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %205 = load i32, i32* %i, align 4
  %mul538 = mul i32 16, %205
  %add539 = add i32 %mul538, 6
  %arrayidx540 = getelementptr [128 x i64], [128 x i64]* %v537, i32 0, i32 %add539
  %206 = load i64, i64* %arrayidx540, align 8
  %call541 = call i64 @fBlaMka(i64 %204, i64 %206)
  %v542 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %207 = load i32, i32* %i, align 4
  %mul543 = mul i32 16, %207
  %add544 = add i32 %mul543, 1
  %arrayidx545 = getelementptr [128 x i64], [128 x i64]* %v542, i32 0, i32 %add544
  store i64 %call541, i64* %arrayidx545, align 8
  %v546 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %208 = load i32, i32* %i, align 4
  %mul547 = mul i32 16, %208
  %add548 = add i32 %mul547, 12
  %arrayidx549 = getelementptr [128 x i64], [128 x i64]* %v546, i32 0, i32 %add548
  %209 = load i64, i64* %arrayidx549, align 8
  %v550 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %210 = load i32, i32* %i, align 4
  %mul551 = mul i32 16, %210
  %add552 = add i32 %mul551, 1
  %arrayidx553 = getelementptr [128 x i64], [128 x i64]* %v550, i32 0, i32 %add552
  %211 = load i64, i64* %arrayidx553, align 8
  %xor554 = xor i64 %209, %211
  %call555 = call i64 @rotr64(i64 %xor554, i32 32)
  %v556 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %212 = load i32, i32* %i, align 4
  %mul557 = mul i32 16, %212
  %add558 = add i32 %mul557, 12
  %arrayidx559 = getelementptr [128 x i64], [128 x i64]* %v556, i32 0, i32 %add558
  store i64 %call555, i64* %arrayidx559, align 8
  %v560 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %213 = load i32, i32* %i, align 4
  %mul561 = mul i32 16, %213
  %add562 = add i32 %mul561, 11
  %arrayidx563 = getelementptr [128 x i64], [128 x i64]* %v560, i32 0, i32 %add562
  %214 = load i64, i64* %arrayidx563, align 8
  %v564 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %215 = load i32, i32* %i, align 4
  %mul565 = mul i32 16, %215
  %add566 = add i32 %mul565, 12
  %arrayidx567 = getelementptr [128 x i64], [128 x i64]* %v564, i32 0, i32 %add566
  %216 = load i64, i64* %arrayidx567, align 8
  %call568 = call i64 @fBlaMka(i64 %214, i64 %216)
  %v569 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %217 = load i32, i32* %i, align 4
  %mul570 = mul i32 16, %217
  %add571 = add i32 %mul570, 11
  %arrayidx572 = getelementptr [128 x i64], [128 x i64]* %v569, i32 0, i32 %add571
  store i64 %call568, i64* %arrayidx572, align 8
  %v573 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %218 = load i32, i32* %i, align 4
  %mul574 = mul i32 16, %218
  %add575 = add i32 %mul574, 6
  %arrayidx576 = getelementptr [128 x i64], [128 x i64]* %v573, i32 0, i32 %add575
  %219 = load i64, i64* %arrayidx576, align 8
  %v577 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %220 = load i32, i32* %i, align 4
  %mul578 = mul i32 16, %220
  %add579 = add i32 %mul578, 11
  %arrayidx580 = getelementptr [128 x i64], [128 x i64]* %v577, i32 0, i32 %add579
  %221 = load i64, i64* %arrayidx580, align 8
  %xor581 = xor i64 %219, %221
  %call582 = call i64 @rotr64(i64 %xor581, i32 24)
  %v583 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %222 = load i32, i32* %i, align 4
  %mul584 = mul i32 16, %222
  %add585 = add i32 %mul584, 6
  %arrayidx586 = getelementptr [128 x i64], [128 x i64]* %v583, i32 0, i32 %add585
  store i64 %call582, i64* %arrayidx586, align 8
  %v587 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %223 = load i32, i32* %i, align 4
  %mul588 = mul i32 16, %223
  %add589 = add i32 %mul588, 1
  %arrayidx590 = getelementptr [128 x i64], [128 x i64]* %v587, i32 0, i32 %add589
  %224 = load i64, i64* %arrayidx590, align 8
  %v591 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %225 = load i32, i32* %i, align 4
  %mul592 = mul i32 16, %225
  %add593 = add i32 %mul592, 6
  %arrayidx594 = getelementptr [128 x i64], [128 x i64]* %v591, i32 0, i32 %add593
  %226 = load i64, i64* %arrayidx594, align 8
  %call595 = call i64 @fBlaMka(i64 %224, i64 %226)
  %v596 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %227 = load i32, i32* %i, align 4
  %mul597 = mul i32 16, %227
  %add598 = add i32 %mul597, 1
  %arrayidx599 = getelementptr [128 x i64], [128 x i64]* %v596, i32 0, i32 %add598
  store i64 %call595, i64* %arrayidx599, align 8
  %v600 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %228 = load i32, i32* %i, align 4
  %mul601 = mul i32 16, %228
  %add602 = add i32 %mul601, 12
  %arrayidx603 = getelementptr [128 x i64], [128 x i64]* %v600, i32 0, i32 %add602
  %229 = load i64, i64* %arrayidx603, align 8
  %v604 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %230 = load i32, i32* %i, align 4
  %mul605 = mul i32 16, %230
  %add606 = add i32 %mul605, 1
  %arrayidx607 = getelementptr [128 x i64], [128 x i64]* %v604, i32 0, i32 %add606
  %231 = load i64, i64* %arrayidx607, align 8
  %xor608 = xor i64 %229, %231
  %call609 = call i64 @rotr64(i64 %xor608, i32 16)
  %v610 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %232 = load i32, i32* %i, align 4
  %mul611 = mul i32 16, %232
  %add612 = add i32 %mul611, 12
  %arrayidx613 = getelementptr [128 x i64], [128 x i64]* %v610, i32 0, i32 %add612
  store i64 %call609, i64* %arrayidx613, align 8
  %v614 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %233 = load i32, i32* %i, align 4
  %mul615 = mul i32 16, %233
  %add616 = add i32 %mul615, 11
  %arrayidx617 = getelementptr [128 x i64], [128 x i64]* %v614, i32 0, i32 %add616
  %234 = load i64, i64* %arrayidx617, align 8
  %v618 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %235 = load i32, i32* %i, align 4
  %mul619 = mul i32 16, %235
  %add620 = add i32 %mul619, 12
  %arrayidx621 = getelementptr [128 x i64], [128 x i64]* %v618, i32 0, i32 %add620
  %236 = load i64, i64* %arrayidx621, align 8
  %call622 = call i64 @fBlaMka(i64 %234, i64 %236)
  %v623 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %237 = load i32, i32* %i, align 4
  %mul624 = mul i32 16, %237
  %add625 = add i32 %mul624, 11
  %arrayidx626 = getelementptr [128 x i64], [128 x i64]* %v623, i32 0, i32 %add625
  store i64 %call622, i64* %arrayidx626, align 8
  %v627 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %238 = load i32, i32* %i, align 4
  %mul628 = mul i32 16, %238
  %add629 = add i32 %mul628, 6
  %arrayidx630 = getelementptr [128 x i64], [128 x i64]* %v627, i32 0, i32 %add629
  %239 = load i64, i64* %arrayidx630, align 8
  %v631 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %240 = load i32, i32* %i, align 4
  %mul632 = mul i32 16, %240
  %add633 = add i32 %mul632, 11
  %arrayidx634 = getelementptr [128 x i64], [128 x i64]* %v631, i32 0, i32 %add633
  %241 = load i64, i64* %arrayidx634, align 8
  %xor635 = xor i64 %239, %241
  %call636 = call i64 @rotr64(i64 %xor635, i32 63)
  %v637 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %242 = load i32, i32* %i, align 4
  %mul638 = mul i32 16, %242
  %add639 = add i32 %mul638, 6
  %arrayidx640 = getelementptr [128 x i64], [128 x i64]* %v637, i32 0, i32 %add639
  store i64 %call636, i64* %arrayidx640, align 8
  br label %do.end641

do.end641:                                        ; preds = %do.body532
  br label %do.body642

do.body642:                                       ; preds = %do.end641
  %v643 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %243 = load i32, i32* %i, align 4
  %mul644 = mul i32 16, %243
  %add645 = add i32 %mul644, 2
  %arrayidx646 = getelementptr [128 x i64], [128 x i64]* %v643, i32 0, i32 %add645
  %244 = load i64, i64* %arrayidx646, align 8
  %v647 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %245 = load i32, i32* %i, align 4
  %mul648 = mul i32 16, %245
  %add649 = add i32 %mul648, 7
  %arrayidx650 = getelementptr [128 x i64], [128 x i64]* %v647, i32 0, i32 %add649
  %246 = load i64, i64* %arrayidx650, align 8
  %call651 = call i64 @fBlaMka(i64 %244, i64 %246)
  %v652 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %247 = load i32, i32* %i, align 4
  %mul653 = mul i32 16, %247
  %add654 = add i32 %mul653, 2
  %arrayidx655 = getelementptr [128 x i64], [128 x i64]* %v652, i32 0, i32 %add654
  store i64 %call651, i64* %arrayidx655, align 8
  %v656 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %248 = load i32, i32* %i, align 4
  %mul657 = mul i32 16, %248
  %add658 = add i32 %mul657, 13
  %arrayidx659 = getelementptr [128 x i64], [128 x i64]* %v656, i32 0, i32 %add658
  %249 = load i64, i64* %arrayidx659, align 8
  %v660 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %250 = load i32, i32* %i, align 4
  %mul661 = mul i32 16, %250
  %add662 = add i32 %mul661, 2
  %arrayidx663 = getelementptr [128 x i64], [128 x i64]* %v660, i32 0, i32 %add662
  %251 = load i64, i64* %arrayidx663, align 8
  %xor664 = xor i64 %249, %251
  %call665 = call i64 @rotr64(i64 %xor664, i32 32)
  %v666 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %252 = load i32, i32* %i, align 4
  %mul667 = mul i32 16, %252
  %add668 = add i32 %mul667, 13
  %arrayidx669 = getelementptr [128 x i64], [128 x i64]* %v666, i32 0, i32 %add668
  store i64 %call665, i64* %arrayidx669, align 8
  %v670 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %253 = load i32, i32* %i, align 4
  %mul671 = mul i32 16, %253
  %add672 = add i32 %mul671, 8
  %arrayidx673 = getelementptr [128 x i64], [128 x i64]* %v670, i32 0, i32 %add672
  %254 = load i64, i64* %arrayidx673, align 8
  %v674 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %255 = load i32, i32* %i, align 4
  %mul675 = mul i32 16, %255
  %add676 = add i32 %mul675, 13
  %arrayidx677 = getelementptr [128 x i64], [128 x i64]* %v674, i32 0, i32 %add676
  %256 = load i64, i64* %arrayidx677, align 8
  %call678 = call i64 @fBlaMka(i64 %254, i64 %256)
  %v679 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %257 = load i32, i32* %i, align 4
  %mul680 = mul i32 16, %257
  %add681 = add i32 %mul680, 8
  %arrayidx682 = getelementptr [128 x i64], [128 x i64]* %v679, i32 0, i32 %add681
  store i64 %call678, i64* %arrayidx682, align 8
  %v683 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %258 = load i32, i32* %i, align 4
  %mul684 = mul i32 16, %258
  %add685 = add i32 %mul684, 7
  %arrayidx686 = getelementptr [128 x i64], [128 x i64]* %v683, i32 0, i32 %add685
  %259 = load i64, i64* %arrayidx686, align 8
  %v687 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %260 = load i32, i32* %i, align 4
  %mul688 = mul i32 16, %260
  %add689 = add i32 %mul688, 8
  %arrayidx690 = getelementptr [128 x i64], [128 x i64]* %v687, i32 0, i32 %add689
  %261 = load i64, i64* %arrayidx690, align 8
  %xor691 = xor i64 %259, %261
  %call692 = call i64 @rotr64(i64 %xor691, i32 24)
  %v693 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %262 = load i32, i32* %i, align 4
  %mul694 = mul i32 16, %262
  %add695 = add i32 %mul694, 7
  %arrayidx696 = getelementptr [128 x i64], [128 x i64]* %v693, i32 0, i32 %add695
  store i64 %call692, i64* %arrayidx696, align 8
  %v697 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %263 = load i32, i32* %i, align 4
  %mul698 = mul i32 16, %263
  %add699 = add i32 %mul698, 2
  %arrayidx700 = getelementptr [128 x i64], [128 x i64]* %v697, i32 0, i32 %add699
  %264 = load i64, i64* %arrayidx700, align 8
  %v701 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %265 = load i32, i32* %i, align 4
  %mul702 = mul i32 16, %265
  %add703 = add i32 %mul702, 7
  %arrayidx704 = getelementptr [128 x i64], [128 x i64]* %v701, i32 0, i32 %add703
  %266 = load i64, i64* %arrayidx704, align 8
  %call705 = call i64 @fBlaMka(i64 %264, i64 %266)
  %v706 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %267 = load i32, i32* %i, align 4
  %mul707 = mul i32 16, %267
  %add708 = add i32 %mul707, 2
  %arrayidx709 = getelementptr [128 x i64], [128 x i64]* %v706, i32 0, i32 %add708
  store i64 %call705, i64* %arrayidx709, align 8
  %v710 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %268 = load i32, i32* %i, align 4
  %mul711 = mul i32 16, %268
  %add712 = add i32 %mul711, 13
  %arrayidx713 = getelementptr [128 x i64], [128 x i64]* %v710, i32 0, i32 %add712
  %269 = load i64, i64* %arrayidx713, align 8
  %v714 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %270 = load i32, i32* %i, align 4
  %mul715 = mul i32 16, %270
  %add716 = add i32 %mul715, 2
  %arrayidx717 = getelementptr [128 x i64], [128 x i64]* %v714, i32 0, i32 %add716
  %271 = load i64, i64* %arrayidx717, align 8
  %xor718 = xor i64 %269, %271
  %call719 = call i64 @rotr64(i64 %xor718, i32 16)
  %v720 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %272 = load i32, i32* %i, align 4
  %mul721 = mul i32 16, %272
  %add722 = add i32 %mul721, 13
  %arrayidx723 = getelementptr [128 x i64], [128 x i64]* %v720, i32 0, i32 %add722
  store i64 %call719, i64* %arrayidx723, align 8
  %v724 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %273 = load i32, i32* %i, align 4
  %mul725 = mul i32 16, %273
  %add726 = add i32 %mul725, 8
  %arrayidx727 = getelementptr [128 x i64], [128 x i64]* %v724, i32 0, i32 %add726
  %274 = load i64, i64* %arrayidx727, align 8
  %v728 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %275 = load i32, i32* %i, align 4
  %mul729 = mul i32 16, %275
  %add730 = add i32 %mul729, 13
  %arrayidx731 = getelementptr [128 x i64], [128 x i64]* %v728, i32 0, i32 %add730
  %276 = load i64, i64* %arrayidx731, align 8
  %call732 = call i64 @fBlaMka(i64 %274, i64 %276)
  %v733 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %277 = load i32, i32* %i, align 4
  %mul734 = mul i32 16, %277
  %add735 = add i32 %mul734, 8
  %arrayidx736 = getelementptr [128 x i64], [128 x i64]* %v733, i32 0, i32 %add735
  store i64 %call732, i64* %arrayidx736, align 8
  %v737 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %278 = load i32, i32* %i, align 4
  %mul738 = mul i32 16, %278
  %add739 = add i32 %mul738, 7
  %arrayidx740 = getelementptr [128 x i64], [128 x i64]* %v737, i32 0, i32 %add739
  %279 = load i64, i64* %arrayidx740, align 8
  %v741 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %280 = load i32, i32* %i, align 4
  %mul742 = mul i32 16, %280
  %add743 = add i32 %mul742, 8
  %arrayidx744 = getelementptr [128 x i64], [128 x i64]* %v741, i32 0, i32 %add743
  %281 = load i64, i64* %arrayidx744, align 8
  %xor745 = xor i64 %279, %281
  %call746 = call i64 @rotr64(i64 %xor745, i32 63)
  %v747 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %282 = load i32, i32* %i, align 4
  %mul748 = mul i32 16, %282
  %add749 = add i32 %mul748, 7
  %arrayidx750 = getelementptr [128 x i64], [128 x i64]* %v747, i32 0, i32 %add749
  store i64 %call746, i64* %arrayidx750, align 8
  br label %do.end751

do.end751:                                        ; preds = %do.body642
  br label %do.body752

do.body752:                                       ; preds = %do.end751
  %v753 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %283 = load i32, i32* %i, align 4
  %mul754 = mul i32 16, %283
  %add755 = add i32 %mul754, 3
  %arrayidx756 = getelementptr [128 x i64], [128 x i64]* %v753, i32 0, i32 %add755
  %284 = load i64, i64* %arrayidx756, align 8
  %v757 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %285 = load i32, i32* %i, align 4
  %mul758 = mul i32 16, %285
  %add759 = add i32 %mul758, 4
  %arrayidx760 = getelementptr [128 x i64], [128 x i64]* %v757, i32 0, i32 %add759
  %286 = load i64, i64* %arrayidx760, align 8
  %call761 = call i64 @fBlaMka(i64 %284, i64 %286)
  %v762 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %287 = load i32, i32* %i, align 4
  %mul763 = mul i32 16, %287
  %add764 = add i32 %mul763, 3
  %arrayidx765 = getelementptr [128 x i64], [128 x i64]* %v762, i32 0, i32 %add764
  store i64 %call761, i64* %arrayidx765, align 8
  %v766 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %288 = load i32, i32* %i, align 4
  %mul767 = mul i32 16, %288
  %add768 = add i32 %mul767, 14
  %arrayidx769 = getelementptr [128 x i64], [128 x i64]* %v766, i32 0, i32 %add768
  %289 = load i64, i64* %arrayidx769, align 8
  %v770 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %290 = load i32, i32* %i, align 4
  %mul771 = mul i32 16, %290
  %add772 = add i32 %mul771, 3
  %arrayidx773 = getelementptr [128 x i64], [128 x i64]* %v770, i32 0, i32 %add772
  %291 = load i64, i64* %arrayidx773, align 8
  %xor774 = xor i64 %289, %291
  %call775 = call i64 @rotr64(i64 %xor774, i32 32)
  %v776 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %292 = load i32, i32* %i, align 4
  %mul777 = mul i32 16, %292
  %add778 = add i32 %mul777, 14
  %arrayidx779 = getelementptr [128 x i64], [128 x i64]* %v776, i32 0, i32 %add778
  store i64 %call775, i64* %arrayidx779, align 8
  %v780 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %293 = load i32, i32* %i, align 4
  %mul781 = mul i32 16, %293
  %add782 = add i32 %mul781, 9
  %arrayidx783 = getelementptr [128 x i64], [128 x i64]* %v780, i32 0, i32 %add782
  %294 = load i64, i64* %arrayidx783, align 8
  %v784 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %295 = load i32, i32* %i, align 4
  %mul785 = mul i32 16, %295
  %add786 = add i32 %mul785, 14
  %arrayidx787 = getelementptr [128 x i64], [128 x i64]* %v784, i32 0, i32 %add786
  %296 = load i64, i64* %arrayidx787, align 8
  %call788 = call i64 @fBlaMka(i64 %294, i64 %296)
  %v789 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %297 = load i32, i32* %i, align 4
  %mul790 = mul i32 16, %297
  %add791 = add i32 %mul790, 9
  %arrayidx792 = getelementptr [128 x i64], [128 x i64]* %v789, i32 0, i32 %add791
  store i64 %call788, i64* %arrayidx792, align 8
  %v793 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %298 = load i32, i32* %i, align 4
  %mul794 = mul i32 16, %298
  %add795 = add i32 %mul794, 4
  %arrayidx796 = getelementptr [128 x i64], [128 x i64]* %v793, i32 0, i32 %add795
  %299 = load i64, i64* %arrayidx796, align 8
  %v797 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %300 = load i32, i32* %i, align 4
  %mul798 = mul i32 16, %300
  %add799 = add i32 %mul798, 9
  %arrayidx800 = getelementptr [128 x i64], [128 x i64]* %v797, i32 0, i32 %add799
  %301 = load i64, i64* %arrayidx800, align 8
  %xor801 = xor i64 %299, %301
  %call802 = call i64 @rotr64(i64 %xor801, i32 24)
  %v803 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %302 = load i32, i32* %i, align 4
  %mul804 = mul i32 16, %302
  %add805 = add i32 %mul804, 4
  %arrayidx806 = getelementptr [128 x i64], [128 x i64]* %v803, i32 0, i32 %add805
  store i64 %call802, i64* %arrayidx806, align 8
  %v807 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %303 = load i32, i32* %i, align 4
  %mul808 = mul i32 16, %303
  %add809 = add i32 %mul808, 3
  %arrayidx810 = getelementptr [128 x i64], [128 x i64]* %v807, i32 0, i32 %add809
  %304 = load i64, i64* %arrayidx810, align 8
  %v811 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %305 = load i32, i32* %i, align 4
  %mul812 = mul i32 16, %305
  %add813 = add i32 %mul812, 4
  %arrayidx814 = getelementptr [128 x i64], [128 x i64]* %v811, i32 0, i32 %add813
  %306 = load i64, i64* %arrayidx814, align 8
  %call815 = call i64 @fBlaMka(i64 %304, i64 %306)
  %v816 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %307 = load i32, i32* %i, align 4
  %mul817 = mul i32 16, %307
  %add818 = add i32 %mul817, 3
  %arrayidx819 = getelementptr [128 x i64], [128 x i64]* %v816, i32 0, i32 %add818
  store i64 %call815, i64* %arrayidx819, align 8
  %v820 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %308 = load i32, i32* %i, align 4
  %mul821 = mul i32 16, %308
  %add822 = add i32 %mul821, 14
  %arrayidx823 = getelementptr [128 x i64], [128 x i64]* %v820, i32 0, i32 %add822
  %309 = load i64, i64* %arrayidx823, align 8
  %v824 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %310 = load i32, i32* %i, align 4
  %mul825 = mul i32 16, %310
  %add826 = add i32 %mul825, 3
  %arrayidx827 = getelementptr [128 x i64], [128 x i64]* %v824, i32 0, i32 %add826
  %311 = load i64, i64* %arrayidx827, align 8
  %xor828 = xor i64 %309, %311
  %call829 = call i64 @rotr64(i64 %xor828, i32 16)
  %v830 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %312 = load i32, i32* %i, align 4
  %mul831 = mul i32 16, %312
  %add832 = add i32 %mul831, 14
  %arrayidx833 = getelementptr [128 x i64], [128 x i64]* %v830, i32 0, i32 %add832
  store i64 %call829, i64* %arrayidx833, align 8
  %v834 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %313 = load i32, i32* %i, align 4
  %mul835 = mul i32 16, %313
  %add836 = add i32 %mul835, 9
  %arrayidx837 = getelementptr [128 x i64], [128 x i64]* %v834, i32 0, i32 %add836
  %314 = load i64, i64* %arrayidx837, align 8
  %v838 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %315 = load i32, i32* %i, align 4
  %mul839 = mul i32 16, %315
  %add840 = add i32 %mul839, 14
  %arrayidx841 = getelementptr [128 x i64], [128 x i64]* %v838, i32 0, i32 %add840
  %316 = load i64, i64* %arrayidx841, align 8
  %call842 = call i64 @fBlaMka(i64 %314, i64 %316)
  %v843 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %317 = load i32, i32* %i, align 4
  %mul844 = mul i32 16, %317
  %add845 = add i32 %mul844, 9
  %arrayidx846 = getelementptr [128 x i64], [128 x i64]* %v843, i32 0, i32 %add845
  store i64 %call842, i64* %arrayidx846, align 8
  %v847 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %318 = load i32, i32* %i, align 4
  %mul848 = mul i32 16, %318
  %add849 = add i32 %mul848, 4
  %arrayidx850 = getelementptr [128 x i64], [128 x i64]* %v847, i32 0, i32 %add849
  %319 = load i64, i64* %arrayidx850, align 8
  %v851 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %320 = load i32, i32* %i, align 4
  %mul852 = mul i32 16, %320
  %add853 = add i32 %mul852, 9
  %arrayidx854 = getelementptr [128 x i64], [128 x i64]* %v851, i32 0, i32 %add853
  %321 = load i64, i64* %arrayidx854, align 8
  %xor855 = xor i64 %319, %321
  %call856 = call i64 @rotr64(i64 %xor855, i32 63)
  %v857 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %322 = load i32, i32* %i, align 4
  %mul858 = mul i32 16, %322
  %add859 = add i32 %mul858, 4
  %arrayidx860 = getelementptr [128 x i64], [128 x i64]* %v857, i32 0, i32 %add859
  store i64 %call856, i64* %arrayidx860, align 8
  br label %do.end861

do.end861:                                        ; preds = %do.body752
  br label %do.end862

do.end862:                                        ; preds = %do.end861
  br label %for.inc

for.inc:                                          ; preds = %do.end862
  %323 = load i32, i32* %i, align 4
  %inc = add i32 %323, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond863

for.cond863:                                      ; preds = %for.inc1736, %for.end
  %324 = load i32, i32* %i, align 4
  %cmp864 = icmp ult i32 %324, 8
  br i1 %cmp864, label %for.body865, label %for.end1738

for.body865:                                      ; preds = %for.cond863
  br label %do.body866

do.body866:                                       ; preds = %for.body865
  br label %do.body867

do.body867:                                       ; preds = %do.body866
  %v868 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %325 = load i32, i32* %i, align 4
  %mul869 = mul i32 2, %325
  %arrayidx870 = getelementptr [128 x i64], [128 x i64]* %v868, i32 0, i32 %mul869
  %326 = load i64, i64* %arrayidx870, align 8
  %v871 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %327 = load i32, i32* %i, align 4
  %mul872 = mul i32 2, %327
  %add873 = add i32 %mul872, 32
  %arrayidx874 = getelementptr [128 x i64], [128 x i64]* %v871, i32 0, i32 %add873
  %328 = load i64, i64* %arrayidx874, align 8
  %call875 = call i64 @fBlaMka(i64 %326, i64 %328)
  %v876 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %329 = load i32, i32* %i, align 4
  %mul877 = mul i32 2, %329
  %arrayidx878 = getelementptr [128 x i64], [128 x i64]* %v876, i32 0, i32 %mul877
  store i64 %call875, i64* %arrayidx878, align 8
  %v879 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %330 = load i32, i32* %i, align 4
  %mul880 = mul i32 2, %330
  %add881 = add i32 %mul880, 96
  %arrayidx882 = getelementptr [128 x i64], [128 x i64]* %v879, i32 0, i32 %add881
  %331 = load i64, i64* %arrayidx882, align 8
  %v883 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %332 = load i32, i32* %i, align 4
  %mul884 = mul i32 2, %332
  %arrayidx885 = getelementptr [128 x i64], [128 x i64]* %v883, i32 0, i32 %mul884
  %333 = load i64, i64* %arrayidx885, align 8
  %xor886 = xor i64 %331, %333
  %call887 = call i64 @rotr64(i64 %xor886, i32 32)
  %v888 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %334 = load i32, i32* %i, align 4
  %mul889 = mul i32 2, %334
  %add890 = add i32 %mul889, 96
  %arrayidx891 = getelementptr [128 x i64], [128 x i64]* %v888, i32 0, i32 %add890
  store i64 %call887, i64* %arrayidx891, align 8
  %v892 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %335 = load i32, i32* %i, align 4
  %mul893 = mul i32 2, %335
  %add894 = add i32 %mul893, 64
  %arrayidx895 = getelementptr [128 x i64], [128 x i64]* %v892, i32 0, i32 %add894
  %336 = load i64, i64* %arrayidx895, align 8
  %v896 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %337 = load i32, i32* %i, align 4
  %mul897 = mul i32 2, %337
  %add898 = add i32 %mul897, 96
  %arrayidx899 = getelementptr [128 x i64], [128 x i64]* %v896, i32 0, i32 %add898
  %338 = load i64, i64* %arrayidx899, align 8
  %call900 = call i64 @fBlaMka(i64 %336, i64 %338)
  %v901 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %339 = load i32, i32* %i, align 4
  %mul902 = mul i32 2, %339
  %add903 = add i32 %mul902, 64
  %arrayidx904 = getelementptr [128 x i64], [128 x i64]* %v901, i32 0, i32 %add903
  store i64 %call900, i64* %arrayidx904, align 8
  %v905 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %340 = load i32, i32* %i, align 4
  %mul906 = mul i32 2, %340
  %add907 = add i32 %mul906, 32
  %arrayidx908 = getelementptr [128 x i64], [128 x i64]* %v905, i32 0, i32 %add907
  %341 = load i64, i64* %arrayidx908, align 8
  %v909 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %342 = load i32, i32* %i, align 4
  %mul910 = mul i32 2, %342
  %add911 = add i32 %mul910, 64
  %arrayidx912 = getelementptr [128 x i64], [128 x i64]* %v909, i32 0, i32 %add911
  %343 = load i64, i64* %arrayidx912, align 8
  %xor913 = xor i64 %341, %343
  %call914 = call i64 @rotr64(i64 %xor913, i32 24)
  %v915 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %344 = load i32, i32* %i, align 4
  %mul916 = mul i32 2, %344
  %add917 = add i32 %mul916, 32
  %arrayidx918 = getelementptr [128 x i64], [128 x i64]* %v915, i32 0, i32 %add917
  store i64 %call914, i64* %arrayidx918, align 8
  %v919 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %345 = load i32, i32* %i, align 4
  %mul920 = mul i32 2, %345
  %arrayidx921 = getelementptr [128 x i64], [128 x i64]* %v919, i32 0, i32 %mul920
  %346 = load i64, i64* %arrayidx921, align 8
  %v922 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %347 = load i32, i32* %i, align 4
  %mul923 = mul i32 2, %347
  %add924 = add i32 %mul923, 32
  %arrayidx925 = getelementptr [128 x i64], [128 x i64]* %v922, i32 0, i32 %add924
  %348 = load i64, i64* %arrayidx925, align 8
  %call926 = call i64 @fBlaMka(i64 %346, i64 %348)
  %v927 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %349 = load i32, i32* %i, align 4
  %mul928 = mul i32 2, %349
  %arrayidx929 = getelementptr [128 x i64], [128 x i64]* %v927, i32 0, i32 %mul928
  store i64 %call926, i64* %arrayidx929, align 8
  %v930 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %350 = load i32, i32* %i, align 4
  %mul931 = mul i32 2, %350
  %add932 = add i32 %mul931, 96
  %arrayidx933 = getelementptr [128 x i64], [128 x i64]* %v930, i32 0, i32 %add932
  %351 = load i64, i64* %arrayidx933, align 8
  %v934 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %352 = load i32, i32* %i, align 4
  %mul935 = mul i32 2, %352
  %arrayidx936 = getelementptr [128 x i64], [128 x i64]* %v934, i32 0, i32 %mul935
  %353 = load i64, i64* %arrayidx936, align 8
  %xor937 = xor i64 %351, %353
  %call938 = call i64 @rotr64(i64 %xor937, i32 16)
  %v939 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %354 = load i32, i32* %i, align 4
  %mul940 = mul i32 2, %354
  %add941 = add i32 %mul940, 96
  %arrayidx942 = getelementptr [128 x i64], [128 x i64]* %v939, i32 0, i32 %add941
  store i64 %call938, i64* %arrayidx942, align 8
  %v943 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %355 = load i32, i32* %i, align 4
  %mul944 = mul i32 2, %355
  %add945 = add i32 %mul944, 64
  %arrayidx946 = getelementptr [128 x i64], [128 x i64]* %v943, i32 0, i32 %add945
  %356 = load i64, i64* %arrayidx946, align 8
  %v947 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %357 = load i32, i32* %i, align 4
  %mul948 = mul i32 2, %357
  %add949 = add i32 %mul948, 96
  %arrayidx950 = getelementptr [128 x i64], [128 x i64]* %v947, i32 0, i32 %add949
  %358 = load i64, i64* %arrayidx950, align 8
  %call951 = call i64 @fBlaMka(i64 %356, i64 %358)
  %v952 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %359 = load i32, i32* %i, align 4
  %mul953 = mul i32 2, %359
  %add954 = add i32 %mul953, 64
  %arrayidx955 = getelementptr [128 x i64], [128 x i64]* %v952, i32 0, i32 %add954
  store i64 %call951, i64* %arrayidx955, align 8
  %v956 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %360 = load i32, i32* %i, align 4
  %mul957 = mul i32 2, %360
  %add958 = add i32 %mul957, 32
  %arrayidx959 = getelementptr [128 x i64], [128 x i64]* %v956, i32 0, i32 %add958
  %361 = load i64, i64* %arrayidx959, align 8
  %v960 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %362 = load i32, i32* %i, align 4
  %mul961 = mul i32 2, %362
  %add962 = add i32 %mul961, 64
  %arrayidx963 = getelementptr [128 x i64], [128 x i64]* %v960, i32 0, i32 %add962
  %363 = load i64, i64* %arrayidx963, align 8
  %xor964 = xor i64 %361, %363
  %call965 = call i64 @rotr64(i64 %xor964, i32 63)
  %v966 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %364 = load i32, i32* %i, align 4
  %mul967 = mul i32 2, %364
  %add968 = add i32 %mul967, 32
  %arrayidx969 = getelementptr [128 x i64], [128 x i64]* %v966, i32 0, i32 %add968
  store i64 %call965, i64* %arrayidx969, align 8
  br label %do.end970

do.end970:                                        ; preds = %do.body867
  br label %do.body971

do.body971:                                       ; preds = %do.end970
  %v972 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %365 = load i32, i32* %i, align 4
  %mul973 = mul i32 2, %365
  %add974 = add i32 %mul973, 1
  %arrayidx975 = getelementptr [128 x i64], [128 x i64]* %v972, i32 0, i32 %add974
  %366 = load i64, i64* %arrayidx975, align 8
  %v976 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %367 = load i32, i32* %i, align 4
  %mul977 = mul i32 2, %367
  %add978 = add i32 %mul977, 33
  %arrayidx979 = getelementptr [128 x i64], [128 x i64]* %v976, i32 0, i32 %add978
  %368 = load i64, i64* %arrayidx979, align 8
  %call980 = call i64 @fBlaMka(i64 %366, i64 %368)
  %v981 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %369 = load i32, i32* %i, align 4
  %mul982 = mul i32 2, %369
  %add983 = add i32 %mul982, 1
  %arrayidx984 = getelementptr [128 x i64], [128 x i64]* %v981, i32 0, i32 %add983
  store i64 %call980, i64* %arrayidx984, align 8
  %v985 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %370 = load i32, i32* %i, align 4
  %mul986 = mul i32 2, %370
  %add987 = add i32 %mul986, 97
  %arrayidx988 = getelementptr [128 x i64], [128 x i64]* %v985, i32 0, i32 %add987
  %371 = load i64, i64* %arrayidx988, align 8
  %v989 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %372 = load i32, i32* %i, align 4
  %mul990 = mul i32 2, %372
  %add991 = add i32 %mul990, 1
  %arrayidx992 = getelementptr [128 x i64], [128 x i64]* %v989, i32 0, i32 %add991
  %373 = load i64, i64* %arrayidx992, align 8
  %xor993 = xor i64 %371, %373
  %call994 = call i64 @rotr64(i64 %xor993, i32 32)
  %v995 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %374 = load i32, i32* %i, align 4
  %mul996 = mul i32 2, %374
  %add997 = add i32 %mul996, 97
  %arrayidx998 = getelementptr [128 x i64], [128 x i64]* %v995, i32 0, i32 %add997
  store i64 %call994, i64* %arrayidx998, align 8
  %v999 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %375 = load i32, i32* %i, align 4
  %mul1000 = mul i32 2, %375
  %add1001 = add i32 %mul1000, 65
  %arrayidx1002 = getelementptr [128 x i64], [128 x i64]* %v999, i32 0, i32 %add1001
  %376 = load i64, i64* %arrayidx1002, align 8
  %v1003 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %377 = load i32, i32* %i, align 4
  %mul1004 = mul i32 2, %377
  %add1005 = add i32 %mul1004, 97
  %arrayidx1006 = getelementptr [128 x i64], [128 x i64]* %v1003, i32 0, i32 %add1005
  %378 = load i64, i64* %arrayidx1006, align 8
  %call1007 = call i64 @fBlaMka(i64 %376, i64 %378)
  %v1008 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %379 = load i32, i32* %i, align 4
  %mul1009 = mul i32 2, %379
  %add1010 = add i32 %mul1009, 65
  %arrayidx1011 = getelementptr [128 x i64], [128 x i64]* %v1008, i32 0, i32 %add1010
  store i64 %call1007, i64* %arrayidx1011, align 8
  %v1012 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %380 = load i32, i32* %i, align 4
  %mul1013 = mul i32 2, %380
  %add1014 = add i32 %mul1013, 33
  %arrayidx1015 = getelementptr [128 x i64], [128 x i64]* %v1012, i32 0, i32 %add1014
  %381 = load i64, i64* %arrayidx1015, align 8
  %v1016 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %382 = load i32, i32* %i, align 4
  %mul1017 = mul i32 2, %382
  %add1018 = add i32 %mul1017, 65
  %arrayidx1019 = getelementptr [128 x i64], [128 x i64]* %v1016, i32 0, i32 %add1018
  %383 = load i64, i64* %arrayidx1019, align 8
  %xor1020 = xor i64 %381, %383
  %call1021 = call i64 @rotr64(i64 %xor1020, i32 24)
  %v1022 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %384 = load i32, i32* %i, align 4
  %mul1023 = mul i32 2, %384
  %add1024 = add i32 %mul1023, 33
  %arrayidx1025 = getelementptr [128 x i64], [128 x i64]* %v1022, i32 0, i32 %add1024
  store i64 %call1021, i64* %arrayidx1025, align 8
  %v1026 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %385 = load i32, i32* %i, align 4
  %mul1027 = mul i32 2, %385
  %add1028 = add i32 %mul1027, 1
  %arrayidx1029 = getelementptr [128 x i64], [128 x i64]* %v1026, i32 0, i32 %add1028
  %386 = load i64, i64* %arrayidx1029, align 8
  %v1030 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %387 = load i32, i32* %i, align 4
  %mul1031 = mul i32 2, %387
  %add1032 = add i32 %mul1031, 33
  %arrayidx1033 = getelementptr [128 x i64], [128 x i64]* %v1030, i32 0, i32 %add1032
  %388 = load i64, i64* %arrayidx1033, align 8
  %call1034 = call i64 @fBlaMka(i64 %386, i64 %388)
  %v1035 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %389 = load i32, i32* %i, align 4
  %mul1036 = mul i32 2, %389
  %add1037 = add i32 %mul1036, 1
  %arrayidx1038 = getelementptr [128 x i64], [128 x i64]* %v1035, i32 0, i32 %add1037
  store i64 %call1034, i64* %arrayidx1038, align 8
  %v1039 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %390 = load i32, i32* %i, align 4
  %mul1040 = mul i32 2, %390
  %add1041 = add i32 %mul1040, 97
  %arrayidx1042 = getelementptr [128 x i64], [128 x i64]* %v1039, i32 0, i32 %add1041
  %391 = load i64, i64* %arrayidx1042, align 8
  %v1043 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %392 = load i32, i32* %i, align 4
  %mul1044 = mul i32 2, %392
  %add1045 = add i32 %mul1044, 1
  %arrayidx1046 = getelementptr [128 x i64], [128 x i64]* %v1043, i32 0, i32 %add1045
  %393 = load i64, i64* %arrayidx1046, align 8
  %xor1047 = xor i64 %391, %393
  %call1048 = call i64 @rotr64(i64 %xor1047, i32 16)
  %v1049 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %394 = load i32, i32* %i, align 4
  %mul1050 = mul i32 2, %394
  %add1051 = add i32 %mul1050, 97
  %arrayidx1052 = getelementptr [128 x i64], [128 x i64]* %v1049, i32 0, i32 %add1051
  store i64 %call1048, i64* %arrayidx1052, align 8
  %v1053 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %395 = load i32, i32* %i, align 4
  %mul1054 = mul i32 2, %395
  %add1055 = add i32 %mul1054, 65
  %arrayidx1056 = getelementptr [128 x i64], [128 x i64]* %v1053, i32 0, i32 %add1055
  %396 = load i64, i64* %arrayidx1056, align 8
  %v1057 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %397 = load i32, i32* %i, align 4
  %mul1058 = mul i32 2, %397
  %add1059 = add i32 %mul1058, 97
  %arrayidx1060 = getelementptr [128 x i64], [128 x i64]* %v1057, i32 0, i32 %add1059
  %398 = load i64, i64* %arrayidx1060, align 8
  %call1061 = call i64 @fBlaMka(i64 %396, i64 %398)
  %v1062 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %399 = load i32, i32* %i, align 4
  %mul1063 = mul i32 2, %399
  %add1064 = add i32 %mul1063, 65
  %arrayidx1065 = getelementptr [128 x i64], [128 x i64]* %v1062, i32 0, i32 %add1064
  store i64 %call1061, i64* %arrayidx1065, align 8
  %v1066 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %400 = load i32, i32* %i, align 4
  %mul1067 = mul i32 2, %400
  %add1068 = add i32 %mul1067, 33
  %arrayidx1069 = getelementptr [128 x i64], [128 x i64]* %v1066, i32 0, i32 %add1068
  %401 = load i64, i64* %arrayidx1069, align 8
  %v1070 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %402 = load i32, i32* %i, align 4
  %mul1071 = mul i32 2, %402
  %add1072 = add i32 %mul1071, 65
  %arrayidx1073 = getelementptr [128 x i64], [128 x i64]* %v1070, i32 0, i32 %add1072
  %403 = load i64, i64* %arrayidx1073, align 8
  %xor1074 = xor i64 %401, %403
  %call1075 = call i64 @rotr64(i64 %xor1074, i32 63)
  %v1076 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %404 = load i32, i32* %i, align 4
  %mul1077 = mul i32 2, %404
  %add1078 = add i32 %mul1077, 33
  %arrayidx1079 = getelementptr [128 x i64], [128 x i64]* %v1076, i32 0, i32 %add1078
  store i64 %call1075, i64* %arrayidx1079, align 8
  br label %do.end1080

do.end1080:                                       ; preds = %do.body971
  br label %do.body1081

do.body1081:                                      ; preds = %do.end1080
  %v1082 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %405 = load i32, i32* %i, align 4
  %mul1083 = mul i32 2, %405
  %add1084 = add i32 %mul1083, 16
  %arrayidx1085 = getelementptr [128 x i64], [128 x i64]* %v1082, i32 0, i32 %add1084
  %406 = load i64, i64* %arrayidx1085, align 8
  %v1086 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %407 = load i32, i32* %i, align 4
  %mul1087 = mul i32 2, %407
  %add1088 = add i32 %mul1087, 48
  %arrayidx1089 = getelementptr [128 x i64], [128 x i64]* %v1086, i32 0, i32 %add1088
  %408 = load i64, i64* %arrayidx1089, align 8
  %call1090 = call i64 @fBlaMka(i64 %406, i64 %408)
  %v1091 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %409 = load i32, i32* %i, align 4
  %mul1092 = mul i32 2, %409
  %add1093 = add i32 %mul1092, 16
  %arrayidx1094 = getelementptr [128 x i64], [128 x i64]* %v1091, i32 0, i32 %add1093
  store i64 %call1090, i64* %arrayidx1094, align 8
  %v1095 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %410 = load i32, i32* %i, align 4
  %mul1096 = mul i32 2, %410
  %add1097 = add i32 %mul1096, 112
  %arrayidx1098 = getelementptr [128 x i64], [128 x i64]* %v1095, i32 0, i32 %add1097
  %411 = load i64, i64* %arrayidx1098, align 8
  %v1099 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %412 = load i32, i32* %i, align 4
  %mul1100 = mul i32 2, %412
  %add1101 = add i32 %mul1100, 16
  %arrayidx1102 = getelementptr [128 x i64], [128 x i64]* %v1099, i32 0, i32 %add1101
  %413 = load i64, i64* %arrayidx1102, align 8
  %xor1103 = xor i64 %411, %413
  %call1104 = call i64 @rotr64(i64 %xor1103, i32 32)
  %v1105 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %414 = load i32, i32* %i, align 4
  %mul1106 = mul i32 2, %414
  %add1107 = add i32 %mul1106, 112
  %arrayidx1108 = getelementptr [128 x i64], [128 x i64]* %v1105, i32 0, i32 %add1107
  store i64 %call1104, i64* %arrayidx1108, align 8
  %v1109 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %415 = load i32, i32* %i, align 4
  %mul1110 = mul i32 2, %415
  %add1111 = add i32 %mul1110, 80
  %arrayidx1112 = getelementptr [128 x i64], [128 x i64]* %v1109, i32 0, i32 %add1111
  %416 = load i64, i64* %arrayidx1112, align 8
  %v1113 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %417 = load i32, i32* %i, align 4
  %mul1114 = mul i32 2, %417
  %add1115 = add i32 %mul1114, 112
  %arrayidx1116 = getelementptr [128 x i64], [128 x i64]* %v1113, i32 0, i32 %add1115
  %418 = load i64, i64* %arrayidx1116, align 8
  %call1117 = call i64 @fBlaMka(i64 %416, i64 %418)
  %v1118 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %419 = load i32, i32* %i, align 4
  %mul1119 = mul i32 2, %419
  %add1120 = add i32 %mul1119, 80
  %arrayidx1121 = getelementptr [128 x i64], [128 x i64]* %v1118, i32 0, i32 %add1120
  store i64 %call1117, i64* %arrayidx1121, align 8
  %v1122 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %420 = load i32, i32* %i, align 4
  %mul1123 = mul i32 2, %420
  %add1124 = add i32 %mul1123, 48
  %arrayidx1125 = getelementptr [128 x i64], [128 x i64]* %v1122, i32 0, i32 %add1124
  %421 = load i64, i64* %arrayidx1125, align 8
  %v1126 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %422 = load i32, i32* %i, align 4
  %mul1127 = mul i32 2, %422
  %add1128 = add i32 %mul1127, 80
  %arrayidx1129 = getelementptr [128 x i64], [128 x i64]* %v1126, i32 0, i32 %add1128
  %423 = load i64, i64* %arrayidx1129, align 8
  %xor1130 = xor i64 %421, %423
  %call1131 = call i64 @rotr64(i64 %xor1130, i32 24)
  %v1132 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %424 = load i32, i32* %i, align 4
  %mul1133 = mul i32 2, %424
  %add1134 = add i32 %mul1133, 48
  %arrayidx1135 = getelementptr [128 x i64], [128 x i64]* %v1132, i32 0, i32 %add1134
  store i64 %call1131, i64* %arrayidx1135, align 8
  %v1136 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %425 = load i32, i32* %i, align 4
  %mul1137 = mul i32 2, %425
  %add1138 = add i32 %mul1137, 16
  %arrayidx1139 = getelementptr [128 x i64], [128 x i64]* %v1136, i32 0, i32 %add1138
  %426 = load i64, i64* %arrayidx1139, align 8
  %v1140 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %427 = load i32, i32* %i, align 4
  %mul1141 = mul i32 2, %427
  %add1142 = add i32 %mul1141, 48
  %arrayidx1143 = getelementptr [128 x i64], [128 x i64]* %v1140, i32 0, i32 %add1142
  %428 = load i64, i64* %arrayidx1143, align 8
  %call1144 = call i64 @fBlaMka(i64 %426, i64 %428)
  %v1145 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %429 = load i32, i32* %i, align 4
  %mul1146 = mul i32 2, %429
  %add1147 = add i32 %mul1146, 16
  %arrayidx1148 = getelementptr [128 x i64], [128 x i64]* %v1145, i32 0, i32 %add1147
  store i64 %call1144, i64* %arrayidx1148, align 8
  %v1149 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %430 = load i32, i32* %i, align 4
  %mul1150 = mul i32 2, %430
  %add1151 = add i32 %mul1150, 112
  %arrayidx1152 = getelementptr [128 x i64], [128 x i64]* %v1149, i32 0, i32 %add1151
  %431 = load i64, i64* %arrayidx1152, align 8
  %v1153 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %432 = load i32, i32* %i, align 4
  %mul1154 = mul i32 2, %432
  %add1155 = add i32 %mul1154, 16
  %arrayidx1156 = getelementptr [128 x i64], [128 x i64]* %v1153, i32 0, i32 %add1155
  %433 = load i64, i64* %arrayidx1156, align 8
  %xor1157 = xor i64 %431, %433
  %call1158 = call i64 @rotr64(i64 %xor1157, i32 16)
  %v1159 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %434 = load i32, i32* %i, align 4
  %mul1160 = mul i32 2, %434
  %add1161 = add i32 %mul1160, 112
  %arrayidx1162 = getelementptr [128 x i64], [128 x i64]* %v1159, i32 0, i32 %add1161
  store i64 %call1158, i64* %arrayidx1162, align 8
  %v1163 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %435 = load i32, i32* %i, align 4
  %mul1164 = mul i32 2, %435
  %add1165 = add i32 %mul1164, 80
  %arrayidx1166 = getelementptr [128 x i64], [128 x i64]* %v1163, i32 0, i32 %add1165
  %436 = load i64, i64* %arrayidx1166, align 8
  %v1167 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %437 = load i32, i32* %i, align 4
  %mul1168 = mul i32 2, %437
  %add1169 = add i32 %mul1168, 112
  %arrayidx1170 = getelementptr [128 x i64], [128 x i64]* %v1167, i32 0, i32 %add1169
  %438 = load i64, i64* %arrayidx1170, align 8
  %call1171 = call i64 @fBlaMka(i64 %436, i64 %438)
  %v1172 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %439 = load i32, i32* %i, align 4
  %mul1173 = mul i32 2, %439
  %add1174 = add i32 %mul1173, 80
  %arrayidx1175 = getelementptr [128 x i64], [128 x i64]* %v1172, i32 0, i32 %add1174
  store i64 %call1171, i64* %arrayidx1175, align 8
  %v1176 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %440 = load i32, i32* %i, align 4
  %mul1177 = mul i32 2, %440
  %add1178 = add i32 %mul1177, 48
  %arrayidx1179 = getelementptr [128 x i64], [128 x i64]* %v1176, i32 0, i32 %add1178
  %441 = load i64, i64* %arrayidx1179, align 8
  %v1180 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %442 = load i32, i32* %i, align 4
  %mul1181 = mul i32 2, %442
  %add1182 = add i32 %mul1181, 80
  %arrayidx1183 = getelementptr [128 x i64], [128 x i64]* %v1180, i32 0, i32 %add1182
  %443 = load i64, i64* %arrayidx1183, align 8
  %xor1184 = xor i64 %441, %443
  %call1185 = call i64 @rotr64(i64 %xor1184, i32 63)
  %v1186 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %444 = load i32, i32* %i, align 4
  %mul1187 = mul i32 2, %444
  %add1188 = add i32 %mul1187, 48
  %arrayidx1189 = getelementptr [128 x i64], [128 x i64]* %v1186, i32 0, i32 %add1188
  store i64 %call1185, i64* %arrayidx1189, align 8
  br label %do.end1190

do.end1190:                                       ; preds = %do.body1081
  br label %do.body1191

do.body1191:                                      ; preds = %do.end1190
  %v1192 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %445 = load i32, i32* %i, align 4
  %mul1193 = mul i32 2, %445
  %add1194 = add i32 %mul1193, 17
  %arrayidx1195 = getelementptr [128 x i64], [128 x i64]* %v1192, i32 0, i32 %add1194
  %446 = load i64, i64* %arrayidx1195, align 8
  %v1196 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %447 = load i32, i32* %i, align 4
  %mul1197 = mul i32 2, %447
  %add1198 = add i32 %mul1197, 49
  %arrayidx1199 = getelementptr [128 x i64], [128 x i64]* %v1196, i32 0, i32 %add1198
  %448 = load i64, i64* %arrayidx1199, align 8
  %call1200 = call i64 @fBlaMka(i64 %446, i64 %448)
  %v1201 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %449 = load i32, i32* %i, align 4
  %mul1202 = mul i32 2, %449
  %add1203 = add i32 %mul1202, 17
  %arrayidx1204 = getelementptr [128 x i64], [128 x i64]* %v1201, i32 0, i32 %add1203
  store i64 %call1200, i64* %arrayidx1204, align 8
  %v1205 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %450 = load i32, i32* %i, align 4
  %mul1206 = mul i32 2, %450
  %add1207 = add i32 %mul1206, 113
  %arrayidx1208 = getelementptr [128 x i64], [128 x i64]* %v1205, i32 0, i32 %add1207
  %451 = load i64, i64* %arrayidx1208, align 8
  %v1209 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %452 = load i32, i32* %i, align 4
  %mul1210 = mul i32 2, %452
  %add1211 = add i32 %mul1210, 17
  %arrayidx1212 = getelementptr [128 x i64], [128 x i64]* %v1209, i32 0, i32 %add1211
  %453 = load i64, i64* %arrayidx1212, align 8
  %xor1213 = xor i64 %451, %453
  %call1214 = call i64 @rotr64(i64 %xor1213, i32 32)
  %v1215 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %454 = load i32, i32* %i, align 4
  %mul1216 = mul i32 2, %454
  %add1217 = add i32 %mul1216, 113
  %arrayidx1218 = getelementptr [128 x i64], [128 x i64]* %v1215, i32 0, i32 %add1217
  store i64 %call1214, i64* %arrayidx1218, align 8
  %v1219 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %455 = load i32, i32* %i, align 4
  %mul1220 = mul i32 2, %455
  %add1221 = add i32 %mul1220, 81
  %arrayidx1222 = getelementptr [128 x i64], [128 x i64]* %v1219, i32 0, i32 %add1221
  %456 = load i64, i64* %arrayidx1222, align 8
  %v1223 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %457 = load i32, i32* %i, align 4
  %mul1224 = mul i32 2, %457
  %add1225 = add i32 %mul1224, 113
  %arrayidx1226 = getelementptr [128 x i64], [128 x i64]* %v1223, i32 0, i32 %add1225
  %458 = load i64, i64* %arrayidx1226, align 8
  %call1227 = call i64 @fBlaMka(i64 %456, i64 %458)
  %v1228 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %459 = load i32, i32* %i, align 4
  %mul1229 = mul i32 2, %459
  %add1230 = add i32 %mul1229, 81
  %arrayidx1231 = getelementptr [128 x i64], [128 x i64]* %v1228, i32 0, i32 %add1230
  store i64 %call1227, i64* %arrayidx1231, align 8
  %v1232 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %460 = load i32, i32* %i, align 4
  %mul1233 = mul i32 2, %460
  %add1234 = add i32 %mul1233, 49
  %arrayidx1235 = getelementptr [128 x i64], [128 x i64]* %v1232, i32 0, i32 %add1234
  %461 = load i64, i64* %arrayidx1235, align 8
  %v1236 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %462 = load i32, i32* %i, align 4
  %mul1237 = mul i32 2, %462
  %add1238 = add i32 %mul1237, 81
  %arrayidx1239 = getelementptr [128 x i64], [128 x i64]* %v1236, i32 0, i32 %add1238
  %463 = load i64, i64* %arrayidx1239, align 8
  %xor1240 = xor i64 %461, %463
  %call1241 = call i64 @rotr64(i64 %xor1240, i32 24)
  %v1242 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %464 = load i32, i32* %i, align 4
  %mul1243 = mul i32 2, %464
  %add1244 = add i32 %mul1243, 49
  %arrayidx1245 = getelementptr [128 x i64], [128 x i64]* %v1242, i32 0, i32 %add1244
  store i64 %call1241, i64* %arrayidx1245, align 8
  %v1246 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %465 = load i32, i32* %i, align 4
  %mul1247 = mul i32 2, %465
  %add1248 = add i32 %mul1247, 17
  %arrayidx1249 = getelementptr [128 x i64], [128 x i64]* %v1246, i32 0, i32 %add1248
  %466 = load i64, i64* %arrayidx1249, align 8
  %v1250 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %467 = load i32, i32* %i, align 4
  %mul1251 = mul i32 2, %467
  %add1252 = add i32 %mul1251, 49
  %arrayidx1253 = getelementptr [128 x i64], [128 x i64]* %v1250, i32 0, i32 %add1252
  %468 = load i64, i64* %arrayidx1253, align 8
  %call1254 = call i64 @fBlaMka(i64 %466, i64 %468)
  %v1255 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %469 = load i32, i32* %i, align 4
  %mul1256 = mul i32 2, %469
  %add1257 = add i32 %mul1256, 17
  %arrayidx1258 = getelementptr [128 x i64], [128 x i64]* %v1255, i32 0, i32 %add1257
  store i64 %call1254, i64* %arrayidx1258, align 8
  %v1259 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %470 = load i32, i32* %i, align 4
  %mul1260 = mul i32 2, %470
  %add1261 = add i32 %mul1260, 113
  %arrayidx1262 = getelementptr [128 x i64], [128 x i64]* %v1259, i32 0, i32 %add1261
  %471 = load i64, i64* %arrayidx1262, align 8
  %v1263 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %472 = load i32, i32* %i, align 4
  %mul1264 = mul i32 2, %472
  %add1265 = add i32 %mul1264, 17
  %arrayidx1266 = getelementptr [128 x i64], [128 x i64]* %v1263, i32 0, i32 %add1265
  %473 = load i64, i64* %arrayidx1266, align 8
  %xor1267 = xor i64 %471, %473
  %call1268 = call i64 @rotr64(i64 %xor1267, i32 16)
  %v1269 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %474 = load i32, i32* %i, align 4
  %mul1270 = mul i32 2, %474
  %add1271 = add i32 %mul1270, 113
  %arrayidx1272 = getelementptr [128 x i64], [128 x i64]* %v1269, i32 0, i32 %add1271
  store i64 %call1268, i64* %arrayidx1272, align 8
  %v1273 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %475 = load i32, i32* %i, align 4
  %mul1274 = mul i32 2, %475
  %add1275 = add i32 %mul1274, 81
  %arrayidx1276 = getelementptr [128 x i64], [128 x i64]* %v1273, i32 0, i32 %add1275
  %476 = load i64, i64* %arrayidx1276, align 8
  %v1277 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %477 = load i32, i32* %i, align 4
  %mul1278 = mul i32 2, %477
  %add1279 = add i32 %mul1278, 113
  %arrayidx1280 = getelementptr [128 x i64], [128 x i64]* %v1277, i32 0, i32 %add1279
  %478 = load i64, i64* %arrayidx1280, align 8
  %call1281 = call i64 @fBlaMka(i64 %476, i64 %478)
  %v1282 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %479 = load i32, i32* %i, align 4
  %mul1283 = mul i32 2, %479
  %add1284 = add i32 %mul1283, 81
  %arrayidx1285 = getelementptr [128 x i64], [128 x i64]* %v1282, i32 0, i32 %add1284
  store i64 %call1281, i64* %arrayidx1285, align 8
  %v1286 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %480 = load i32, i32* %i, align 4
  %mul1287 = mul i32 2, %480
  %add1288 = add i32 %mul1287, 49
  %arrayidx1289 = getelementptr [128 x i64], [128 x i64]* %v1286, i32 0, i32 %add1288
  %481 = load i64, i64* %arrayidx1289, align 8
  %v1290 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %482 = load i32, i32* %i, align 4
  %mul1291 = mul i32 2, %482
  %add1292 = add i32 %mul1291, 81
  %arrayidx1293 = getelementptr [128 x i64], [128 x i64]* %v1290, i32 0, i32 %add1292
  %483 = load i64, i64* %arrayidx1293, align 8
  %xor1294 = xor i64 %481, %483
  %call1295 = call i64 @rotr64(i64 %xor1294, i32 63)
  %v1296 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %484 = load i32, i32* %i, align 4
  %mul1297 = mul i32 2, %484
  %add1298 = add i32 %mul1297, 49
  %arrayidx1299 = getelementptr [128 x i64], [128 x i64]* %v1296, i32 0, i32 %add1298
  store i64 %call1295, i64* %arrayidx1299, align 8
  br label %do.end1300

do.end1300:                                       ; preds = %do.body1191
  br label %do.body1301

do.body1301:                                      ; preds = %do.end1300
  %v1302 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %485 = load i32, i32* %i, align 4
  %mul1303 = mul i32 2, %485
  %arrayidx1304 = getelementptr [128 x i64], [128 x i64]* %v1302, i32 0, i32 %mul1303
  %486 = load i64, i64* %arrayidx1304, align 8
  %v1305 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %487 = load i32, i32* %i, align 4
  %mul1306 = mul i32 2, %487
  %add1307 = add i32 %mul1306, 33
  %arrayidx1308 = getelementptr [128 x i64], [128 x i64]* %v1305, i32 0, i32 %add1307
  %488 = load i64, i64* %arrayidx1308, align 8
  %call1309 = call i64 @fBlaMka(i64 %486, i64 %488)
  %v1310 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %489 = load i32, i32* %i, align 4
  %mul1311 = mul i32 2, %489
  %arrayidx1312 = getelementptr [128 x i64], [128 x i64]* %v1310, i32 0, i32 %mul1311
  store i64 %call1309, i64* %arrayidx1312, align 8
  %v1313 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %490 = load i32, i32* %i, align 4
  %mul1314 = mul i32 2, %490
  %add1315 = add i32 %mul1314, 113
  %arrayidx1316 = getelementptr [128 x i64], [128 x i64]* %v1313, i32 0, i32 %add1315
  %491 = load i64, i64* %arrayidx1316, align 8
  %v1317 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %492 = load i32, i32* %i, align 4
  %mul1318 = mul i32 2, %492
  %arrayidx1319 = getelementptr [128 x i64], [128 x i64]* %v1317, i32 0, i32 %mul1318
  %493 = load i64, i64* %arrayidx1319, align 8
  %xor1320 = xor i64 %491, %493
  %call1321 = call i64 @rotr64(i64 %xor1320, i32 32)
  %v1322 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %494 = load i32, i32* %i, align 4
  %mul1323 = mul i32 2, %494
  %add1324 = add i32 %mul1323, 113
  %arrayidx1325 = getelementptr [128 x i64], [128 x i64]* %v1322, i32 0, i32 %add1324
  store i64 %call1321, i64* %arrayidx1325, align 8
  %v1326 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %495 = load i32, i32* %i, align 4
  %mul1327 = mul i32 2, %495
  %add1328 = add i32 %mul1327, 80
  %arrayidx1329 = getelementptr [128 x i64], [128 x i64]* %v1326, i32 0, i32 %add1328
  %496 = load i64, i64* %arrayidx1329, align 8
  %v1330 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %497 = load i32, i32* %i, align 4
  %mul1331 = mul i32 2, %497
  %add1332 = add i32 %mul1331, 113
  %arrayidx1333 = getelementptr [128 x i64], [128 x i64]* %v1330, i32 0, i32 %add1332
  %498 = load i64, i64* %arrayidx1333, align 8
  %call1334 = call i64 @fBlaMka(i64 %496, i64 %498)
  %v1335 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %499 = load i32, i32* %i, align 4
  %mul1336 = mul i32 2, %499
  %add1337 = add i32 %mul1336, 80
  %arrayidx1338 = getelementptr [128 x i64], [128 x i64]* %v1335, i32 0, i32 %add1337
  store i64 %call1334, i64* %arrayidx1338, align 8
  %v1339 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %500 = load i32, i32* %i, align 4
  %mul1340 = mul i32 2, %500
  %add1341 = add i32 %mul1340, 33
  %arrayidx1342 = getelementptr [128 x i64], [128 x i64]* %v1339, i32 0, i32 %add1341
  %501 = load i64, i64* %arrayidx1342, align 8
  %v1343 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %502 = load i32, i32* %i, align 4
  %mul1344 = mul i32 2, %502
  %add1345 = add i32 %mul1344, 80
  %arrayidx1346 = getelementptr [128 x i64], [128 x i64]* %v1343, i32 0, i32 %add1345
  %503 = load i64, i64* %arrayidx1346, align 8
  %xor1347 = xor i64 %501, %503
  %call1348 = call i64 @rotr64(i64 %xor1347, i32 24)
  %v1349 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %504 = load i32, i32* %i, align 4
  %mul1350 = mul i32 2, %504
  %add1351 = add i32 %mul1350, 33
  %arrayidx1352 = getelementptr [128 x i64], [128 x i64]* %v1349, i32 0, i32 %add1351
  store i64 %call1348, i64* %arrayidx1352, align 8
  %v1353 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %505 = load i32, i32* %i, align 4
  %mul1354 = mul i32 2, %505
  %arrayidx1355 = getelementptr [128 x i64], [128 x i64]* %v1353, i32 0, i32 %mul1354
  %506 = load i64, i64* %arrayidx1355, align 8
  %v1356 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %507 = load i32, i32* %i, align 4
  %mul1357 = mul i32 2, %507
  %add1358 = add i32 %mul1357, 33
  %arrayidx1359 = getelementptr [128 x i64], [128 x i64]* %v1356, i32 0, i32 %add1358
  %508 = load i64, i64* %arrayidx1359, align 8
  %call1360 = call i64 @fBlaMka(i64 %506, i64 %508)
  %v1361 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %509 = load i32, i32* %i, align 4
  %mul1362 = mul i32 2, %509
  %arrayidx1363 = getelementptr [128 x i64], [128 x i64]* %v1361, i32 0, i32 %mul1362
  store i64 %call1360, i64* %arrayidx1363, align 8
  %v1364 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %510 = load i32, i32* %i, align 4
  %mul1365 = mul i32 2, %510
  %add1366 = add i32 %mul1365, 113
  %arrayidx1367 = getelementptr [128 x i64], [128 x i64]* %v1364, i32 0, i32 %add1366
  %511 = load i64, i64* %arrayidx1367, align 8
  %v1368 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %512 = load i32, i32* %i, align 4
  %mul1369 = mul i32 2, %512
  %arrayidx1370 = getelementptr [128 x i64], [128 x i64]* %v1368, i32 0, i32 %mul1369
  %513 = load i64, i64* %arrayidx1370, align 8
  %xor1371 = xor i64 %511, %513
  %call1372 = call i64 @rotr64(i64 %xor1371, i32 16)
  %v1373 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %514 = load i32, i32* %i, align 4
  %mul1374 = mul i32 2, %514
  %add1375 = add i32 %mul1374, 113
  %arrayidx1376 = getelementptr [128 x i64], [128 x i64]* %v1373, i32 0, i32 %add1375
  store i64 %call1372, i64* %arrayidx1376, align 8
  %v1377 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %515 = load i32, i32* %i, align 4
  %mul1378 = mul i32 2, %515
  %add1379 = add i32 %mul1378, 80
  %arrayidx1380 = getelementptr [128 x i64], [128 x i64]* %v1377, i32 0, i32 %add1379
  %516 = load i64, i64* %arrayidx1380, align 8
  %v1381 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %517 = load i32, i32* %i, align 4
  %mul1382 = mul i32 2, %517
  %add1383 = add i32 %mul1382, 113
  %arrayidx1384 = getelementptr [128 x i64], [128 x i64]* %v1381, i32 0, i32 %add1383
  %518 = load i64, i64* %arrayidx1384, align 8
  %call1385 = call i64 @fBlaMka(i64 %516, i64 %518)
  %v1386 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %519 = load i32, i32* %i, align 4
  %mul1387 = mul i32 2, %519
  %add1388 = add i32 %mul1387, 80
  %arrayidx1389 = getelementptr [128 x i64], [128 x i64]* %v1386, i32 0, i32 %add1388
  store i64 %call1385, i64* %arrayidx1389, align 8
  %v1390 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %520 = load i32, i32* %i, align 4
  %mul1391 = mul i32 2, %520
  %add1392 = add i32 %mul1391, 33
  %arrayidx1393 = getelementptr [128 x i64], [128 x i64]* %v1390, i32 0, i32 %add1392
  %521 = load i64, i64* %arrayidx1393, align 8
  %v1394 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %522 = load i32, i32* %i, align 4
  %mul1395 = mul i32 2, %522
  %add1396 = add i32 %mul1395, 80
  %arrayidx1397 = getelementptr [128 x i64], [128 x i64]* %v1394, i32 0, i32 %add1396
  %523 = load i64, i64* %arrayidx1397, align 8
  %xor1398 = xor i64 %521, %523
  %call1399 = call i64 @rotr64(i64 %xor1398, i32 63)
  %v1400 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %524 = load i32, i32* %i, align 4
  %mul1401 = mul i32 2, %524
  %add1402 = add i32 %mul1401, 33
  %arrayidx1403 = getelementptr [128 x i64], [128 x i64]* %v1400, i32 0, i32 %add1402
  store i64 %call1399, i64* %arrayidx1403, align 8
  br label %do.end1404

do.end1404:                                       ; preds = %do.body1301
  br label %do.body1405

do.body1405:                                      ; preds = %do.end1404
  %v1406 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %525 = load i32, i32* %i, align 4
  %mul1407 = mul i32 2, %525
  %add1408 = add i32 %mul1407, 1
  %arrayidx1409 = getelementptr [128 x i64], [128 x i64]* %v1406, i32 0, i32 %add1408
  %526 = load i64, i64* %arrayidx1409, align 8
  %v1410 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %527 = load i32, i32* %i, align 4
  %mul1411 = mul i32 2, %527
  %add1412 = add i32 %mul1411, 48
  %arrayidx1413 = getelementptr [128 x i64], [128 x i64]* %v1410, i32 0, i32 %add1412
  %528 = load i64, i64* %arrayidx1413, align 8
  %call1414 = call i64 @fBlaMka(i64 %526, i64 %528)
  %v1415 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %529 = load i32, i32* %i, align 4
  %mul1416 = mul i32 2, %529
  %add1417 = add i32 %mul1416, 1
  %arrayidx1418 = getelementptr [128 x i64], [128 x i64]* %v1415, i32 0, i32 %add1417
  store i64 %call1414, i64* %arrayidx1418, align 8
  %v1419 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %530 = load i32, i32* %i, align 4
  %mul1420 = mul i32 2, %530
  %add1421 = add i32 %mul1420, 96
  %arrayidx1422 = getelementptr [128 x i64], [128 x i64]* %v1419, i32 0, i32 %add1421
  %531 = load i64, i64* %arrayidx1422, align 8
  %v1423 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %532 = load i32, i32* %i, align 4
  %mul1424 = mul i32 2, %532
  %add1425 = add i32 %mul1424, 1
  %arrayidx1426 = getelementptr [128 x i64], [128 x i64]* %v1423, i32 0, i32 %add1425
  %533 = load i64, i64* %arrayidx1426, align 8
  %xor1427 = xor i64 %531, %533
  %call1428 = call i64 @rotr64(i64 %xor1427, i32 32)
  %v1429 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %534 = load i32, i32* %i, align 4
  %mul1430 = mul i32 2, %534
  %add1431 = add i32 %mul1430, 96
  %arrayidx1432 = getelementptr [128 x i64], [128 x i64]* %v1429, i32 0, i32 %add1431
  store i64 %call1428, i64* %arrayidx1432, align 8
  %v1433 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %535 = load i32, i32* %i, align 4
  %mul1434 = mul i32 2, %535
  %add1435 = add i32 %mul1434, 81
  %arrayidx1436 = getelementptr [128 x i64], [128 x i64]* %v1433, i32 0, i32 %add1435
  %536 = load i64, i64* %arrayidx1436, align 8
  %v1437 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %537 = load i32, i32* %i, align 4
  %mul1438 = mul i32 2, %537
  %add1439 = add i32 %mul1438, 96
  %arrayidx1440 = getelementptr [128 x i64], [128 x i64]* %v1437, i32 0, i32 %add1439
  %538 = load i64, i64* %arrayidx1440, align 8
  %call1441 = call i64 @fBlaMka(i64 %536, i64 %538)
  %v1442 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %539 = load i32, i32* %i, align 4
  %mul1443 = mul i32 2, %539
  %add1444 = add i32 %mul1443, 81
  %arrayidx1445 = getelementptr [128 x i64], [128 x i64]* %v1442, i32 0, i32 %add1444
  store i64 %call1441, i64* %arrayidx1445, align 8
  %v1446 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %540 = load i32, i32* %i, align 4
  %mul1447 = mul i32 2, %540
  %add1448 = add i32 %mul1447, 48
  %arrayidx1449 = getelementptr [128 x i64], [128 x i64]* %v1446, i32 0, i32 %add1448
  %541 = load i64, i64* %arrayidx1449, align 8
  %v1450 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %542 = load i32, i32* %i, align 4
  %mul1451 = mul i32 2, %542
  %add1452 = add i32 %mul1451, 81
  %arrayidx1453 = getelementptr [128 x i64], [128 x i64]* %v1450, i32 0, i32 %add1452
  %543 = load i64, i64* %arrayidx1453, align 8
  %xor1454 = xor i64 %541, %543
  %call1455 = call i64 @rotr64(i64 %xor1454, i32 24)
  %v1456 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %544 = load i32, i32* %i, align 4
  %mul1457 = mul i32 2, %544
  %add1458 = add i32 %mul1457, 48
  %arrayidx1459 = getelementptr [128 x i64], [128 x i64]* %v1456, i32 0, i32 %add1458
  store i64 %call1455, i64* %arrayidx1459, align 8
  %v1460 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %545 = load i32, i32* %i, align 4
  %mul1461 = mul i32 2, %545
  %add1462 = add i32 %mul1461, 1
  %arrayidx1463 = getelementptr [128 x i64], [128 x i64]* %v1460, i32 0, i32 %add1462
  %546 = load i64, i64* %arrayidx1463, align 8
  %v1464 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %547 = load i32, i32* %i, align 4
  %mul1465 = mul i32 2, %547
  %add1466 = add i32 %mul1465, 48
  %arrayidx1467 = getelementptr [128 x i64], [128 x i64]* %v1464, i32 0, i32 %add1466
  %548 = load i64, i64* %arrayidx1467, align 8
  %call1468 = call i64 @fBlaMka(i64 %546, i64 %548)
  %v1469 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %549 = load i32, i32* %i, align 4
  %mul1470 = mul i32 2, %549
  %add1471 = add i32 %mul1470, 1
  %arrayidx1472 = getelementptr [128 x i64], [128 x i64]* %v1469, i32 0, i32 %add1471
  store i64 %call1468, i64* %arrayidx1472, align 8
  %v1473 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %550 = load i32, i32* %i, align 4
  %mul1474 = mul i32 2, %550
  %add1475 = add i32 %mul1474, 96
  %arrayidx1476 = getelementptr [128 x i64], [128 x i64]* %v1473, i32 0, i32 %add1475
  %551 = load i64, i64* %arrayidx1476, align 8
  %v1477 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %552 = load i32, i32* %i, align 4
  %mul1478 = mul i32 2, %552
  %add1479 = add i32 %mul1478, 1
  %arrayidx1480 = getelementptr [128 x i64], [128 x i64]* %v1477, i32 0, i32 %add1479
  %553 = load i64, i64* %arrayidx1480, align 8
  %xor1481 = xor i64 %551, %553
  %call1482 = call i64 @rotr64(i64 %xor1481, i32 16)
  %v1483 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %554 = load i32, i32* %i, align 4
  %mul1484 = mul i32 2, %554
  %add1485 = add i32 %mul1484, 96
  %arrayidx1486 = getelementptr [128 x i64], [128 x i64]* %v1483, i32 0, i32 %add1485
  store i64 %call1482, i64* %arrayidx1486, align 8
  %v1487 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %555 = load i32, i32* %i, align 4
  %mul1488 = mul i32 2, %555
  %add1489 = add i32 %mul1488, 81
  %arrayidx1490 = getelementptr [128 x i64], [128 x i64]* %v1487, i32 0, i32 %add1489
  %556 = load i64, i64* %arrayidx1490, align 8
  %v1491 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %557 = load i32, i32* %i, align 4
  %mul1492 = mul i32 2, %557
  %add1493 = add i32 %mul1492, 96
  %arrayidx1494 = getelementptr [128 x i64], [128 x i64]* %v1491, i32 0, i32 %add1493
  %558 = load i64, i64* %arrayidx1494, align 8
  %call1495 = call i64 @fBlaMka(i64 %556, i64 %558)
  %v1496 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %559 = load i32, i32* %i, align 4
  %mul1497 = mul i32 2, %559
  %add1498 = add i32 %mul1497, 81
  %arrayidx1499 = getelementptr [128 x i64], [128 x i64]* %v1496, i32 0, i32 %add1498
  store i64 %call1495, i64* %arrayidx1499, align 8
  %v1500 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %560 = load i32, i32* %i, align 4
  %mul1501 = mul i32 2, %560
  %add1502 = add i32 %mul1501, 48
  %arrayidx1503 = getelementptr [128 x i64], [128 x i64]* %v1500, i32 0, i32 %add1502
  %561 = load i64, i64* %arrayidx1503, align 8
  %v1504 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %562 = load i32, i32* %i, align 4
  %mul1505 = mul i32 2, %562
  %add1506 = add i32 %mul1505, 81
  %arrayidx1507 = getelementptr [128 x i64], [128 x i64]* %v1504, i32 0, i32 %add1506
  %563 = load i64, i64* %arrayidx1507, align 8
  %xor1508 = xor i64 %561, %563
  %call1509 = call i64 @rotr64(i64 %xor1508, i32 63)
  %v1510 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %564 = load i32, i32* %i, align 4
  %mul1511 = mul i32 2, %564
  %add1512 = add i32 %mul1511, 48
  %arrayidx1513 = getelementptr [128 x i64], [128 x i64]* %v1510, i32 0, i32 %add1512
  store i64 %call1509, i64* %arrayidx1513, align 8
  br label %do.end1514

do.end1514:                                       ; preds = %do.body1405
  br label %do.body1515

do.body1515:                                      ; preds = %do.end1514
  %v1516 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %565 = load i32, i32* %i, align 4
  %mul1517 = mul i32 2, %565
  %add1518 = add i32 %mul1517, 16
  %arrayidx1519 = getelementptr [128 x i64], [128 x i64]* %v1516, i32 0, i32 %add1518
  %566 = load i64, i64* %arrayidx1519, align 8
  %v1520 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %567 = load i32, i32* %i, align 4
  %mul1521 = mul i32 2, %567
  %add1522 = add i32 %mul1521, 49
  %arrayidx1523 = getelementptr [128 x i64], [128 x i64]* %v1520, i32 0, i32 %add1522
  %568 = load i64, i64* %arrayidx1523, align 8
  %call1524 = call i64 @fBlaMka(i64 %566, i64 %568)
  %v1525 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %569 = load i32, i32* %i, align 4
  %mul1526 = mul i32 2, %569
  %add1527 = add i32 %mul1526, 16
  %arrayidx1528 = getelementptr [128 x i64], [128 x i64]* %v1525, i32 0, i32 %add1527
  store i64 %call1524, i64* %arrayidx1528, align 8
  %v1529 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %570 = load i32, i32* %i, align 4
  %mul1530 = mul i32 2, %570
  %add1531 = add i32 %mul1530, 97
  %arrayidx1532 = getelementptr [128 x i64], [128 x i64]* %v1529, i32 0, i32 %add1531
  %571 = load i64, i64* %arrayidx1532, align 8
  %v1533 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %572 = load i32, i32* %i, align 4
  %mul1534 = mul i32 2, %572
  %add1535 = add i32 %mul1534, 16
  %arrayidx1536 = getelementptr [128 x i64], [128 x i64]* %v1533, i32 0, i32 %add1535
  %573 = load i64, i64* %arrayidx1536, align 8
  %xor1537 = xor i64 %571, %573
  %call1538 = call i64 @rotr64(i64 %xor1537, i32 32)
  %v1539 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %574 = load i32, i32* %i, align 4
  %mul1540 = mul i32 2, %574
  %add1541 = add i32 %mul1540, 97
  %arrayidx1542 = getelementptr [128 x i64], [128 x i64]* %v1539, i32 0, i32 %add1541
  store i64 %call1538, i64* %arrayidx1542, align 8
  %v1543 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %575 = load i32, i32* %i, align 4
  %mul1544 = mul i32 2, %575
  %add1545 = add i32 %mul1544, 64
  %arrayidx1546 = getelementptr [128 x i64], [128 x i64]* %v1543, i32 0, i32 %add1545
  %576 = load i64, i64* %arrayidx1546, align 8
  %v1547 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %577 = load i32, i32* %i, align 4
  %mul1548 = mul i32 2, %577
  %add1549 = add i32 %mul1548, 97
  %arrayidx1550 = getelementptr [128 x i64], [128 x i64]* %v1547, i32 0, i32 %add1549
  %578 = load i64, i64* %arrayidx1550, align 8
  %call1551 = call i64 @fBlaMka(i64 %576, i64 %578)
  %v1552 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %579 = load i32, i32* %i, align 4
  %mul1553 = mul i32 2, %579
  %add1554 = add i32 %mul1553, 64
  %arrayidx1555 = getelementptr [128 x i64], [128 x i64]* %v1552, i32 0, i32 %add1554
  store i64 %call1551, i64* %arrayidx1555, align 8
  %v1556 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %580 = load i32, i32* %i, align 4
  %mul1557 = mul i32 2, %580
  %add1558 = add i32 %mul1557, 49
  %arrayidx1559 = getelementptr [128 x i64], [128 x i64]* %v1556, i32 0, i32 %add1558
  %581 = load i64, i64* %arrayidx1559, align 8
  %v1560 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %582 = load i32, i32* %i, align 4
  %mul1561 = mul i32 2, %582
  %add1562 = add i32 %mul1561, 64
  %arrayidx1563 = getelementptr [128 x i64], [128 x i64]* %v1560, i32 0, i32 %add1562
  %583 = load i64, i64* %arrayidx1563, align 8
  %xor1564 = xor i64 %581, %583
  %call1565 = call i64 @rotr64(i64 %xor1564, i32 24)
  %v1566 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %584 = load i32, i32* %i, align 4
  %mul1567 = mul i32 2, %584
  %add1568 = add i32 %mul1567, 49
  %arrayidx1569 = getelementptr [128 x i64], [128 x i64]* %v1566, i32 0, i32 %add1568
  store i64 %call1565, i64* %arrayidx1569, align 8
  %v1570 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %585 = load i32, i32* %i, align 4
  %mul1571 = mul i32 2, %585
  %add1572 = add i32 %mul1571, 16
  %arrayidx1573 = getelementptr [128 x i64], [128 x i64]* %v1570, i32 0, i32 %add1572
  %586 = load i64, i64* %arrayidx1573, align 8
  %v1574 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %587 = load i32, i32* %i, align 4
  %mul1575 = mul i32 2, %587
  %add1576 = add i32 %mul1575, 49
  %arrayidx1577 = getelementptr [128 x i64], [128 x i64]* %v1574, i32 0, i32 %add1576
  %588 = load i64, i64* %arrayidx1577, align 8
  %call1578 = call i64 @fBlaMka(i64 %586, i64 %588)
  %v1579 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %589 = load i32, i32* %i, align 4
  %mul1580 = mul i32 2, %589
  %add1581 = add i32 %mul1580, 16
  %arrayidx1582 = getelementptr [128 x i64], [128 x i64]* %v1579, i32 0, i32 %add1581
  store i64 %call1578, i64* %arrayidx1582, align 8
  %v1583 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %590 = load i32, i32* %i, align 4
  %mul1584 = mul i32 2, %590
  %add1585 = add i32 %mul1584, 97
  %arrayidx1586 = getelementptr [128 x i64], [128 x i64]* %v1583, i32 0, i32 %add1585
  %591 = load i64, i64* %arrayidx1586, align 8
  %v1587 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %592 = load i32, i32* %i, align 4
  %mul1588 = mul i32 2, %592
  %add1589 = add i32 %mul1588, 16
  %arrayidx1590 = getelementptr [128 x i64], [128 x i64]* %v1587, i32 0, i32 %add1589
  %593 = load i64, i64* %arrayidx1590, align 8
  %xor1591 = xor i64 %591, %593
  %call1592 = call i64 @rotr64(i64 %xor1591, i32 16)
  %v1593 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %594 = load i32, i32* %i, align 4
  %mul1594 = mul i32 2, %594
  %add1595 = add i32 %mul1594, 97
  %arrayidx1596 = getelementptr [128 x i64], [128 x i64]* %v1593, i32 0, i32 %add1595
  store i64 %call1592, i64* %arrayidx1596, align 8
  %v1597 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %595 = load i32, i32* %i, align 4
  %mul1598 = mul i32 2, %595
  %add1599 = add i32 %mul1598, 64
  %arrayidx1600 = getelementptr [128 x i64], [128 x i64]* %v1597, i32 0, i32 %add1599
  %596 = load i64, i64* %arrayidx1600, align 8
  %v1601 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %597 = load i32, i32* %i, align 4
  %mul1602 = mul i32 2, %597
  %add1603 = add i32 %mul1602, 97
  %arrayidx1604 = getelementptr [128 x i64], [128 x i64]* %v1601, i32 0, i32 %add1603
  %598 = load i64, i64* %arrayidx1604, align 8
  %call1605 = call i64 @fBlaMka(i64 %596, i64 %598)
  %v1606 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %599 = load i32, i32* %i, align 4
  %mul1607 = mul i32 2, %599
  %add1608 = add i32 %mul1607, 64
  %arrayidx1609 = getelementptr [128 x i64], [128 x i64]* %v1606, i32 0, i32 %add1608
  store i64 %call1605, i64* %arrayidx1609, align 8
  %v1610 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %600 = load i32, i32* %i, align 4
  %mul1611 = mul i32 2, %600
  %add1612 = add i32 %mul1611, 49
  %arrayidx1613 = getelementptr [128 x i64], [128 x i64]* %v1610, i32 0, i32 %add1612
  %601 = load i64, i64* %arrayidx1613, align 8
  %v1614 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %602 = load i32, i32* %i, align 4
  %mul1615 = mul i32 2, %602
  %add1616 = add i32 %mul1615, 64
  %arrayidx1617 = getelementptr [128 x i64], [128 x i64]* %v1614, i32 0, i32 %add1616
  %603 = load i64, i64* %arrayidx1617, align 8
  %xor1618 = xor i64 %601, %603
  %call1619 = call i64 @rotr64(i64 %xor1618, i32 63)
  %v1620 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %604 = load i32, i32* %i, align 4
  %mul1621 = mul i32 2, %604
  %add1622 = add i32 %mul1621, 49
  %arrayidx1623 = getelementptr [128 x i64], [128 x i64]* %v1620, i32 0, i32 %add1622
  store i64 %call1619, i64* %arrayidx1623, align 8
  br label %do.end1624

do.end1624:                                       ; preds = %do.body1515
  br label %do.body1625

do.body1625:                                      ; preds = %do.end1624
  %v1626 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %605 = load i32, i32* %i, align 4
  %mul1627 = mul i32 2, %605
  %add1628 = add i32 %mul1627, 17
  %arrayidx1629 = getelementptr [128 x i64], [128 x i64]* %v1626, i32 0, i32 %add1628
  %606 = load i64, i64* %arrayidx1629, align 8
  %v1630 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %607 = load i32, i32* %i, align 4
  %mul1631 = mul i32 2, %607
  %add1632 = add i32 %mul1631, 32
  %arrayidx1633 = getelementptr [128 x i64], [128 x i64]* %v1630, i32 0, i32 %add1632
  %608 = load i64, i64* %arrayidx1633, align 8
  %call1634 = call i64 @fBlaMka(i64 %606, i64 %608)
  %v1635 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %609 = load i32, i32* %i, align 4
  %mul1636 = mul i32 2, %609
  %add1637 = add i32 %mul1636, 17
  %arrayidx1638 = getelementptr [128 x i64], [128 x i64]* %v1635, i32 0, i32 %add1637
  store i64 %call1634, i64* %arrayidx1638, align 8
  %v1639 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %610 = load i32, i32* %i, align 4
  %mul1640 = mul i32 2, %610
  %add1641 = add i32 %mul1640, 112
  %arrayidx1642 = getelementptr [128 x i64], [128 x i64]* %v1639, i32 0, i32 %add1641
  %611 = load i64, i64* %arrayidx1642, align 8
  %v1643 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %612 = load i32, i32* %i, align 4
  %mul1644 = mul i32 2, %612
  %add1645 = add i32 %mul1644, 17
  %arrayidx1646 = getelementptr [128 x i64], [128 x i64]* %v1643, i32 0, i32 %add1645
  %613 = load i64, i64* %arrayidx1646, align 8
  %xor1647 = xor i64 %611, %613
  %call1648 = call i64 @rotr64(i64 %xor1647, i32 32)
  %v1649 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %614 = load i32, i32* %i, align 4
  %mul1650 = mul i32 2, %614
  %add1651 = add i32 %mul1650, 112
  %arrayidx1652 = getelementptr [128 x i64], [128 x i64]* %v1649, i32 0, i32 %add1651
  store i64 %call1648, i64* %arrayidx1652, align 8
  %v1653 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %615 = load i32, i32* %i, align 4
  %mul1654 = mul i32 2, %615
  %add1655 = add i32 %mul1654, 65
  %arrayidx1656 = getelementptr [128 x i64], [128 x i64]* %v1653, i32 0, i32 %add1655
  %616 = load i64, i64* %arrayidx1656, align 8
  %v1657 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %617 = load i32, i32* %i, align 4
  %mul1658 = mul i32 2, %617
  %add1659 = add i32 %mul1658, 112
  %arrayidx1660 = getelementptr [128 x i64], [128 x i64]* %v1657, i32 0, i32 %add1659
  %618 = load i64, i64* %arrayidx1660, align 8
  %call1661 = call i64 @fBlaMka(i64 %616, i64 %618)
  %v1662 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %619 = load i32, i32* %i, align 4
  %mul1663 = mul i32 2, %619
  %add1664 = add i32 %mul1663, 65
  %arrayidx1665 = getelementptr [128 x i64], [128 x i64]* %v1662, i32 0, i32 %add1664
  store i64 %call1661, i64* %arrayidx1665, align 8
  %v1666 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %620 = load i32, i32* %i, align 4
  %mul1667 = mul i32 2, %620
  %add1668 = add i32 %mul1667, 32
  %arrayidx1669 = getelementptr [128 x i64], [128 x i64]* %v1666, i32 0, i32 %add1668
  %621 = load i64, i64* %arrayidx1669, align 8
  %v1670 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %622 = load i32, i32* %i, align 4
  %mul1671 = mul i32 2, %622
  %add1672 = add i32 %mul1671, 65
  %arrayidx1673 = getelementptr [128 x i64], [128 x i64]* %v1670, i32 0, i32 %add1672
  %623 = load i64, i64* %arrayidx1673, align 8
  %xor1674 = xor i64 %621, %623
  %call1675 = call i64 @rotr64(i64 %xor1674, i32 24)
  %v1676 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %624 = load i32, i32* %i, align 4
  %mul1677 = mul i32 2, %624
  %add1678 = add i32 %mul1677, 32
  %arrayidx1679 = getelementptr [128 x i64], [128 x i64]* %v1676, i32 0, i32 %add1678
  store i64 %call1675, i64* %arrayidx1679, align 8
  %v1680 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %625 = load i32, i32* %i, align 4
  %mul1681 = mul i32 2, %625
  %add1682 = add i32 %mul1681, 17
  %arrayidx1683 = getelementptr [128 x i64], [128 x i64]* %v1680, i32 0, i32 %add1682
  %626 = load i64, i64* %arrayidx1683, align 8
  %v1684 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %627 = load i32, i32* %i, align 4
  %mul1685 = mul i32 2, %627
  %add1686 = add i32 %mul1685, 32
  %arrayidx1687 = getelementptr [128 x i64], [128 x i64]* %v1684, i32 0, i32 %add1686
  %628 = load i64, i64* %arrayidx1687, align 8
  %call1688 = call i64 @fBlaMka(i64 %626, i64 %628)
  %v1689 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %629 = load i32, i32* %i, align 4
  %mul1690 = mul i32 2, %629
  %add1691 = add i32 %mul1690, 17
  %arrayidx1692 = getelementptr [128 x i64], [128 x i64]* %v1689, i32 0, i32 %add1691
  store i64 %call1688, i64* %arrayidx1692, align 8
  %v1693 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %630 = load i32, i32* %i, align 4
  %mul1694 = mul i32 2, %630
  %add1695 = add i32 %mul1694, 112
  %arrayidx1696 = getelementptr [128 x i64], [128 x i64]* %v1693, i32 0, i32 %add1695
  %631 = load i64, i64* %arrayidx1696, align 8
  %v1697 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %632 = load i32, i32* %i, align 4
  %mul1698 = mul i32 2, %632
  %add1699 = add i32 %mul1698, 17
  %arrayidx1700 = getelementptr [128 x i64], [128 x i64]* %v1697, i32 0, i32 %add1699
  %633 = load i64, i64* %arrayidx1700, align 8
  %xor1701 = xor i64 %631, %633
  %call1702 = call i64 @rotr64(i64 %xor1701, i32 16)
  %v1703 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %634 = load i32, i32* %i, align 4
  %mul1704 = mul i32 2, %634
  %add1705 = add i32 %mul1704, 112
  %arrayidx1706 = getelementptr [128 x i64], [128 x i64]* %v1703, i32 0, i32 %add1705
  store i64 %call1702, i64* %arrayidx1706, align 8
  %v1707 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %635 = load i32, i32* %i, align 4
  %mul1708 = mul i32 2, %635
  %add1709 = add i32 %mul1708, 65
  %arrayidx1710 = getelementptr [128 x i64], [128 x i64]* %v1707, i32 0, i32 %add1709
  %636 = load i64, i64* %arrayidx1710, align 8
  %v1711 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %637 = load i32, i32* %i, align 4
  %mul1712 = mul i32 2, %637
  %add1713 = add i32 %mul1712, 112
  %arrayidx1714 = getelementptr [128 x i64], [128 x i64]* %v1711, i32 0, i32 %add1713
  %638 = load i64, i64* %arrayidx1714, align 8
  %call1715 = call i64 @fBlaMka(i64 %636, i64 %638)
  %v1716 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %639 = load i32, i32* %i, align 4
  %mul1717 = mul i32 2, %639
  %add1718 = add i32 %mul1717, 65
  %arrayidx1719 = getelementptr [128 x i64], [128 x i64]* %v1716, i32 0, i32 %add1718
  store i64 %call1715, i64* %arrayidx1719, align 8
  %v1720 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %640 = load i32, i32* %i, align 4
  %mul1721 = mul i32 2, %640
  %add1722 = add i32 %mul1721, 32
  %arrayidx1723 = getelementptr [128 x i64], [128 x i64]* %v1720, i32 0, i32 %add1722
  %641 = load i64, i64* %arrayidx1723, align 8
  %v1724 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %642 = load i32, i32* %i, align 4
  %mul1725 = mul i32 2, %642
  %add1726 = add i32 %mul1725, 65
  %arrayidx1727 = getelementptr [128 x i64], [128 x i64]* %v1724, i32 0, i32 %add1726
  %643 = load i64, i64* %arrayidx1727, align 8
  %xor1728 = xor i64 %641, %643
  %call1729 = call i64 @rotr64(i64 %xor1728, i32 63)
  %v1730 = getelementptr inbounds %struct.block_, %struct.block_* %blockR, i32 0, i32 0
  %644 = load i32, i32* %i, align 4
  %mul1731 = mul i32 2, %644
  %add1732 = add i32 %mul1731, 32
  %arrayidx1733 = getelementptr [128 x i64], [128 x i64]* %v1730, i32 0, i32 %add1732
  store i64 %call1729, i64* %arrayidx1733, align 8
  br label %do.end1734

do.end1734:                                       ; preds = %do.body1625
  br label %do.end1735

do.end1735:                                       ; preds = %do.end1734
  br label %for.inc1736

for.inc1736:                                      ; preds = %do.end1735
  %645 = load i32, i32* %i, align 4
  %inc1737 = add i32 %645, 1
  store i32 %inc1737, i32* %i, align 4
  br label %for.cond863

for.end1738:                                      ; preds = %for.cond863
  %646 = load %struct.block_*, %struct.block_** %next_block.addr, align 4
  call void @copy_block(%struct.block_* %646, %struct.block_* %block_tmp)
  %647 = load %struct.block_*, %struct.block_** %next_block.addr, align 4
  call void @xor_block(%struct.block_* %647, %struct.block_* %blockR)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @init_block_value(%struct.block_* %b, i8 zeroext %in) #0 {
entry:
  %b.addr = alloca %struct.block_*, align 4
  %in.addr = alloca i8, align 1
  store %struct.block_* %b, %struct.block_** %b.addr, align 4
  store i8 %in, i8* %in.addr, align 1
  %0 = load %struct.block_*, %struct.block_** %b.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %1 = bitcast i64* %arraydecay to i8*
  %2 = load i8, i8* %in.addr, align 1
  %conv = zext i8 %2 to i32
  %3 = trunc i32 %conv to i8
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 %3, i32 1024, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @copy_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4
  store %struct.block_* %src, %struct.block_** %src.addr, align 4
  %0 = load %struct.block_*, %struct.block_** %dst.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %1 = bitcast i64* %arraydecay to i8*
  %2 = load %struct.block_*, %struct.block_** %src.addr, align 4
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [128 x i64], [128 x i64]* %v1, i32 0, i32 0
  %3 = bitcast i64* %arraydecay2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 8 %3, i32 1024, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @xor_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  %i = alloca i32, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4
  store %struct.block_* %src, %struct.block_** %src.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.block_*, %struct.block_** %src.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %2
  %3 = load i64, i64* %arrayidx, align 8
  %4 = load %struct.block_*, %struct.block_** %dst.addr, align 4
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [128 x i64], [128 x i64]* %v1, i32 0, i32 %5
  %6 = load i64, i64* %arrayidx2, align 8
  %xor = xor i64 %6, %3
  store i64 %xor, i64* %arrayidx2, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i64 @fBlaMka(i64 %x, i64 %y) #0 {
entry:
  %x.addr = alloca i64, align 8
  %y.addr = alloca i64, align 8
  %m = alloca i64, align 8
  %xy = alloca i64, align 8
  store i64 %x, i64* %x.addr, align 8
  store i64 %y, i64* %y.addr, align 8
  store i64 4294967295, i64* %m, align 8
  %0 = load i64, i64* %x.addr, align 8
  %and = and i64 %0, 4294967295
  %1 = load i64, i64* %y.addr, align 8
  %and1 = and i64 %1, 4294967295
  %mul = mul i64 %and, %and1
  store i64 %mul, i64* %xy, align 8
  %2 = load i64, i64* %x.addr, align 8
  %3 = load i64, i64* %y.addr, align 8
  %add = add i64 %2, %3
  %4 = load i64, i64* %xy, align 8
  %mul2 = mul i64 2, %4
  %add3 = add i64 %add, %mul2
  ret i64 %add3
}

; Function Attrs: noinline nounwind optnone
define internal i64 @rotr64(i64 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i64, align 8
  %b.addr = alloca i32, align 4
  store i64 %x, i64* %x.addr, align 8
  store i32 %b, i32* %b.addr, align 4
  %0 = load i64, i64* %x.addr, align 8
  %1 = load i32, i32* %b.addr, align 4
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %x.addr, align 8
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
