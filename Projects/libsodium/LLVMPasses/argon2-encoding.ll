; ModuleID = 'crypto_pwhash/argon2/argon2-encoding.c'
source_filename = "crypto_pwhash/argon2/argon2-encoding.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32 }

@.str = private unnamed_addr constant [10 x i8] c"$argon2id\00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c"$argon2i\00", align 1
@.str.2 = private unnamed_addr constant [4 x i8] c"$v=\00", align 1
@.str.3 = private unnamed_addr constant [4 x i8] c"$m=\00", align 1
@.str.4 = private unnamed_addr constant [4 x i8] c",t=\00", align 1
@.str.5 = private unnamed_addr constant [4 x i8] c",p=\00", align 1
@.str.6 = private unnamed_addr constant [2 x i8] c"$\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"$argon2id$v=\00", align 1
@.str.8 = private unnamed_addr constant [12 x i8] c"$argon2i$v=\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_decode_string(%struct.Argon2_Context* %ctx, i8* %str, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %ctx.addr = alloca %struct.Argon2_Context*, align 4
  %str.addr = alloca i8*, align 4
  %type.addr = alloca i32, align 4
  %maxsaltlen = alloca i32, align 4
  %maxoutlen = alloca i32, align 4
  %validation_result = alloca i32, align 4
  %version = alloca i32, align 4
  %cc_len = alloca i32, align 4
  %cc_len8 = alloca i32, align 4
  %cc_len19 = alloca i32, align 4
  %dec_x = alloca i32, align 4
  %cc_len37 = alloca i32, align 4
  %dec_x45 = alloca i32, align 4
  %cc_len58 = alloca i32, align 4
  %dec_x66 = alloca i32, align 4
  %cc_len79 = alloca i32, align 4
  %dec_x87 = alloca i32, align 4
  %cc_len101 = alloca i32, align 4
  %bin_len = alloca i32, align 4
  %str_end = alloca i8*, align 4
  %cc_len119 = alloca i32, align 4
  %bin_len127 = alloca i32, align 4
  %str_end128 = alloca i8*, align 4
  store %struct.Argon2_Context* %ctx, %struct.Argon2_Context** %ctx.addr, align 4
  store i8* %str, i8** %str.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %0, i32 0, i32 5
  %1 = load i32, i32* %saltlen, align 4
  store i32 %1, i32* %maxsaltlen, align 4
  %2 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %2, i32 0, i32 1
  %3 = load i32, i32* %outlen, align 4
  store i32 %3, i32* %maxoutlen, align 4
  store i32 0, i32* %version, align 4
  %4 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %saltlen1 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %4, i32 0, i32 5
  store i32 0, i32* %saltlen1, align 4
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %outlen2 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 1
  store i32 0, i32* %outlen2, align 4
  %6 = load i32, i32* %type.addr, align 4
  %cmp = icmp eq i32 %6, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %if.then
  store i32 9, i32* %cc_len, align 4
  %7 = load i8*, i8** %str.addr, align 4
  %8 = load i32, i32* %cc_len, align 4
  %call = call i32 @strncmp(i8* %7, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str, i32 0, i32 0), i32 %8)
  %cmp3 = icmp ne i32 %call, 0
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %do.body
  store i32 -32, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %do.body
  %9 = load i32, i32* %cc_len, align 4
  %10 = load i8*, i8** %str.addr, align 4
  %add.ptr = getelementptr i8, i8* %10, i32 %9
  store i8* %add.ptr, i8** %str.addr, align 4
  br label %do.end

do.end:                                           ; preds = %if.end
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %type.addr, align 4
  %cmp5 = icmp eq i32 %11, 1
  br i1 %cmp5, label %if.then6, label %if.else15

if.then6:                                         ; preds = %if.else
  br label %do.body7

do.body7:                                         ; preds = %if.then6
  store i32 8, i32* %cc_len8, align 4
  %12 = load i8*, i8** %str.addr, align 4
  %13 = load i32, i32* %cc_len8, align 4
  %call9 = call i32 @strncmp(i8* %12, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 %13)
  %cmp10 = icmp ne i32 %call9, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %do.body7
  store i32 -32, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %do.body7
  %14 = load i32, i32* %cc_len8, align 4
  %15 = load i8*, i8** %str.addr, align 4
  %add.ptr13 = getelementptr i8, i8* %15, i32 %14
  store i8* %add.ptr13, i8** %str.addr, align 4
  br label %do.end14

do.end14:                                         ; preds = %if.end12
  br label %if.end16

if.else15:                                        ; preds = %if.else
  store i32 -26, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %do.end14
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %do.end
  br label %do.body18

do.body18:                                        ; preds = %if.end17
  store i32 3, i32* %cc_len19, align 4
  %16 = load i8*, i8** %str.addr, align 4
  %17 = load i32, i32* %cc_len19, align 4
  %call20 = call i32 @strncmp(i8* %16, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.2, i32 0, i32 0), i32 %17)
  %cmp21 = icmp ne i32 %call20, 0
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %do.body18
  store i32 -32, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %do.body18
  %18 = load i32, i32* %cc_len19, align 4
  %19 = load i8*, i8** %str.addr, align 4
  %add.ptr24 = getelementptr i8, i8* %19, i32 %18
  store i8* %add.ptr24, i8** %str.addr, align 4
  br label %do.end25

do.end25:                                         ; preds = %if.end23
  br label %do.body26

do.body26:                                        ; preds = %do.end25
  %20 = load i8*, i8** %str.addr, align 4
  %call27 = call i8* @decode_decimal(i8* %20, i32* %dec_x)
  store i8* %call27, i8** %str.addr, align 4
  %21 = load i8*, i8** %str.addr, align 4
  %cmp28 = icmp eq i8* %21, null
  br i1 %cmp28, label %if.then30, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %do.body26
  %22 = load i32, i32* %dec_x, align 4
  %cmp29 = icmp ugt i32 %22, -1
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %lor.lhs.false, %do.body26
  store i32 -32, i32* %retval, align 4
  br label %return

if.end31:                                         ; preds = %lor.lhs.false
  %23 = load i32, i32* %dec_x, align 4
  store i32 %23, i32* %version, align 4
  br label %do.end32

do.end32:                                         ; preds = %if.end31
  %24 = load i32, i32* %version, align 4
  %cmp33 = icmp ne i32 %24, 19
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %do.end32
  store i32 -26, i32* %retval, align 4
  br label %return

if.end35:                                         ; preds = %do.end32
  br label %do.body36

do.body36:                                        ; preds = %if.end35
  store i32 3, i32* %cc_len37, align 4
  %25 = load i8*, i8** %str.addr, align 4
  %26 = load i32, i32* %cc_len37, align 4
  %call38 = call i32 @strncmp(i8* %25, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0), i32 %26)
  %cmp39 = icmp ne i32 %call38, 0
  br i1 %cmp39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %do.body36
  store i32 -32, i32* %retval, align 4
  br label %return

if.end41:                                         ; preds = %do.body36
  %27 = load i32, i32* %cc_len37, align 4
  %28 = load i8*, i8** %str.addr, align 4
  %add.ptr42 = getelementptr i8, i8* %28, i32 %27
  store i8* %add.ptr42, i8** %str.addr, align 4
  br label %do.end43

do.end43:                                         ; preds = %if.end41
  br label %do.body44

do.body44:                                        ; preds = %do.end43
  %29 = load i8*, i8** %str.addr, align 4
  %call46 = call i8* @decode_decimal(i8* %29, i32* %dec_x45)
  store i8* %call46, i8** %str.addr, align 4
  %30 = load i8*, i8** %str.addr, align 4
  %cmp47 = icmp eq i8* %30, null
  br i1 %cmp47, label %if.then50, label %lor.lhs.false48

lor.lhs.false48:                                  ; preds = %do.body44
  %31 = load i32, i32* %dec_x45, align 4
  %cmp49 = icmp ugt i32 %31, -1
  br i1 %cmp49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %lor.lhs.false48, %do.body44
  store i32 -32, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %lor.lhs.false48
  %32 = load i32, i32* %dec_x45, align 4
  %33 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %33, i32 0, i32 11
  store i32 %32, i32* %m_cost, align 4
  br label %do.end52

do.end52:                                         ; preds = %if.end51
  %34 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %m_cost53 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %34, i32 0, i32 11
  %35 = load i32, i32* %m_cost53, align 4
  %cmp54 = icmp ugt i32 %35, -1
  br i1 %cmp54, label %if.then55, label %if.end56

if.then55:                                        ; preds = %do.end52
  store i32 -26, i32* %retval, align 4
  br label %return

if.end56:                                         ; preds = %do.end52
  br label %do.body57

do.body57:                                        ; preds = %if.end56
  store i32 3, i32* %cc_len58, align 4
  %36 = load i8*, i8** %str.addr, align 4
  %37 = load i32, i32* %cc_len58, align 4
  %call59 = call i32 @strncmp(i8* %36, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %37)
  %cmp60 = icmp ne i32 %call59, 0
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %do.body57
  store i32 -32, i32* %retval, align 4
  br label %return

if.end62:                                         ; preds = %do.body57
  %38 = load i32, i32* %cc_len58, align 4
  %39 = load i8*, i8** %str.addr, align 4
  %add.ptr63 = getelementptr i8, i8* %39, i32 %38
  store i8* %add.ptr63, i8** %str.addr, align 4
  br label %do.end64

do.end64:                                         ; preds = %if.end62
  br label %do.body65

do.body65:                                        ; preds = %do.end64
  %40 = load i8*, i8** %str.addr, align 4
  %call67 = call i8* @decode_decimal(i8* %40, i32* %dec_x66)
  store i8* %call67, i8** %str.addr, align 4
  %41 = load i8*, i8** %str.addr, align 4
  %cmp68 = icmp eq i8* %41, null
  br i1 %cmp68, label %if.then71, label %lor.lhs.false69

lor.lhs.false69:                                  ; preds = %do.body65
  %42 = load i32, i32* %dec_x66, align 4
  %cmp70 = icmp ugt i32 %42, -1
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %lor.lhs.false69, %do.body65
  store i32 -32, i32* %retval, align 4
  br label %return

if.end72:                                         ; preds = %lor.lhs.false69
  %43 = load i32, i32* %dec_x66, align 4
  %44 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %44, i32 0, i32 10
  store i32 %43, i32* %t_cost, align 4
  br label %do.end73

do.end73:                                         ; preds = %if.end72
  %45 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %t_cost74 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %45, i32 0, i32 10
  %46 = load i32, i32* %t_cost74, align 4
  %cmp75 = icmp ugt i32 %46, -1
  br i1 %cmp75, label %if.then76, label %if.end77

if.then76:                                        ; preds = %do.end73
  store i32 -26, i32* %retval, align 4
  br label %return

if.end77:                                         ; preds = %do.end73
  br label %do.body78

do.body78:                                        ; preds = %if.end77
  store i32 3, i32* %cc_len79, align 4
  %47 = load i8*, i8** %str.addr, align 4
  %48 = load i32, i32* %cc_len79, align 4
  %call80 = call i32 @strncmp(i8* %47, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %48)
  %cmp81 = icmp ne i32 %call80, 0
  br i1 %cmp81, label %if.then82, label %if.end83

if.then82:                                        ; preds = %do.body78
  store i32 -32, i32* %retval, align 4
  br label %return

if.end83:                                         ; preds = %do.body78
  %49 = load i32, i32* %cc_len79, align 4
  %50 = load i8*, i8** %str.addr, align 4
  %add.ptr84 = getelementptr i8, i8* %50, i32 %49
  store i8* %add.ptr84, i8** %str.addr, align 4
  br label %do.end85

do.end85:                                         ; preds = %if.end83
  br label %do.body86

do.body86:                                        ; preds = %do.end85
  %51 = load i8*, i8** %str.addr, align 4
  %call88 = call i8* @decode_decimal(i8* %51, i32* %dec_x87)
  store i8* %call88, i8** %str.addr, align 4
  %52 = load i8*, i8** %str.addr, align 4
  %cmp89 = icmp eq i8* %52, null
  br i1 %cmp89, label %if.then92, label %lor.lhs.false90

lor.lhs.false90:                                  ; preds = %do.body86
  %53 = load i32, i32* %dec_x87, align 4
  %cmp91 = icmp ugt i32 %53, -1
  br i1 %cmp91, label %if.then92, label %if.end93

if.then92:                                        ; preds = %lor.lhs.false90, %do.body86
  store i32 -32, i32* %retval, align 4
  br label %return

if.end93:                                         ; preds = %lor.lhs.false90
  %54 = load i32, i32* %dec_x87, align 4
  %55 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %55, i32 0, i32 12
  store i32 %54, i32* %lanes, align 4
  br label %do.end94

do.end94:                                         ; preds = %if.end93
  %56 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %lanes95 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %56, i32 0, i32 12
  %57 = load i32, i32* %lanes95, align 4
  %cmp96 = icmp ugt i32 %57, -1
  br i1 %cmp96, label %if.then97, label %if.end98

if.then97:                                        ; preds = %do.end94
  store i32 -26, i32* %retval, align 4
  br label %return

if.end98:                                         ; preds = %do.end94
  %58 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %lanes99 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %58, i32 0, i32 12
  %59 = load i32, i32* %lanes99, align 4
  %60 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %60, i32 0, i32 13
  store i32 %59, i32* %threads, align 4
  br label %do.body100

do.body100:                                       ; preds = %if.end98
  store i32 1, i32* %cc_len101, align 4
  %61 = load i8*, i8** %str.addr, align 4
  %62 = load i32, i32* %cc_len101, align 4
  %call102 = call i32 @strncmp(i8* %61, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0), i32 %62)
  %cmp103 = icmp ne i32 %call102, 0
  br i1 %cmp103, label %if.then104, label %if.end105

if.then104:                                       ; preds = %do.body100
  store i32 -32, i32* %retval, align 4
  br label %return

if.end105:                                        ; preds = %do.body100
  %63 = load i32, i32* %cc_len101, align 4
  %64 = load i8*, i8** %str.addr, align 4
  %add.ptr106 = getelementptr i8, i8* %64, i32 %63
  store i8* %add.ptr106, i8** %str.addr, align 4
  br label %do.end107

do.end107:                                        ; preds = %if.end105
  br label %do.body108

do.body108:                                       ; preds = %do.end107
  %65 = load i32, i32* %maxsaltlen, align 4
  store i32 %65, i32* %bin_len, align 4
  %66 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %66, i32 0, i32 4
  %67 = load i8*, i8** %salt, align 4
  %68 = load i32, i32* %maxsaltlen, align 4
  %69 = load i8*, i8** %str.addr, align 4
  %70 = load i8*, i8** %str.addr, align 4
  %call109 = call i32 @strlen(i8* %70)
  %call110 = call i32 @sodium_base642bin(i8* %67, i32 %68, i8* %69, i32 %call109, i8* null, i32* %bin_len, i8** %str_end, i32 3)
  %cmp111 = icmp ne i32 %call110, 0
  br i1 %cmp111, label %if.then114, label %lor.lhs.false112

lor.lhs.false112:                                 ; preds = %do.body108
  %71 = load i32, i32* %bin_len, align 4
  %cmp113 = icmp ugt i32 %71, -1
  br i1 %cmp113, label %if.then114, label %if.end115

if.then114:                                       ; preds = %lor.lhs.false112, %do.body108
  store i32 -32, i32* %retval, align 4
  br label %return

if.end115:                                        ; preds = %lor.lhs.false112
  %72 = load i32, i32* %bin_len, align 4
  %73 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %saltlen116 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %73, i32 0, i32 5
  store i32 %72, i32* %saltlen116, align 4
  %74 = load i8*, i8** %str_end, align 4
  store i8* %74, i8** %str.addr, align 4
  br label %do.end117

do.end117:                                        ; preds = %if.end115
  br label %do.body118

do.body118:                                       ; preds = %do.end117
  store i32 1, i32* %cc_len119, align 4
  %75 = load i8*, i8** %str.addr, align 4
  %76 = load i32, i32* %cc_len119, align 4
  %call120 = call i32 @strncmp(i8* %75, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0), i32 %76)
  %cmp121 = icmp ne i32 %call120, 0
  br i1 %cmp121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %do.body118
  store i32 -32, i32* %retval, align 4
  br label %return

if.end123:                                        ; preds = %do.body118
  %77 = load i32, i32* %cc_len119, align 4
  %78 = load i8*, i8** %str.addr, align 4
  %add.ptr124 = getelementptr i8, i8* %78, i32 %77
  store i8* %add.ptr124, i8** %str.addr, align 4
  br label %do.end125

do.end125:                                        ; preds = %if.end123
  br label %do.body126

do.body126:                                       ; preds = %do.end125
  %79 = load i32, i32* %maxoutlen, align 4
  store i32 %79, i32* %bin_len127, align 4
  %80 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %80, i32 0, i32 0
  %81 = load i8*, i8** %out, align 4
  %82 = load i32, i32* %maxoutlen, align 4
  %83 = load i8*, i8** %str.addr, align 4
  %84 = load i8*, i8** %str.addr, align 4
  %call129 = call i32 @strlen(i8* %84)
  %call130 = call i32 @sodium_base642bin(i8* %81, i32 %82, i8* %83, i32 %call129, i8* null, i32* %bin_len127, i8** %str_end128, i32 3)
  %cmp131 = icmp ne i32 %call130, 0
  br i1 %cmp131, label %if.then134, label %lor.lhs.false132

lor.lhs.false132:                                 ; preds = %do.body126
  %85 = load i32, i32* %bin_len127, align 4
  %cmp133 = icmp ugt i32 %85, -1
  br i1 %cmp133, label %if.then134, label %if.end135

if.then134:                                       ; preds = %lor.lhs.false132, %do.body126
  store i32 -32, i32* %retval, align 4
  br label %return

if.end135:                                        ; preds = %lor.lhs.false132
  %86 = load i32, i32* %bin_len127, align 4
  %87 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %outlen136 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %87, i32 0, i32 1
  store i32 %86, i32* %outlen136, align 4
  %88 = load i8*, i8** %str_end128, align 4
  store i8* %88, i8** %str.addr, align 4
  br label %do.end137

do.end137:                                        ; preds = %if.end135
  %89 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %call138 = call i32 @argon2_validate_inputs(%struct.Argon2_Context* %89)
  store i32 %call138, i32* %validation_result, align 4
  %90 = load i32, i32* %validation_result, align 4
  %cmp139 = icmp ne i32 %90, 0
  br i1 %cmp139, label %if.then140, label %if.end141

if.then140:                                       ; preds = %do.end137
  %91 = load i32, i32* %validation_result, align 4
  store i32 %91, i32* %retval, align 4
  br label %return

if.end141:                                        ; preds = %do.end137
  %92 = load i8*, i8** %str.addr, align 4
  %93 = load i8, i8* %92, align 1
  %conv = sext i8 %93 to i32
  %cmp142 = icmp eq i32 %conv, 0
  br i1 %cmp142, label %if.then144, label %if.end145

if.then144:                                       ; preds = %if.end141
  store i32 0, i32* %retval, align 4
  br label %return

if.end145:                                        ; preds = %if.end141
  store i32 -32, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end145, %if.then144, %if.then140, %if.then134, %if.then122, %if.then114, %if.then104, %if.then97, %if.then92, %if.then82, %if.then76, %if.then71, %if.then61, %if.then55, %if.then50, %if.then40, %if.then34, %if.then30, %if.then22, %if.else15, %if.then11, %if.then4
  %94 = load i32, i32* %retval, align 4
  ret i32 %94
}

declare i32 @strncmp(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i8* @decode_decimal(i8* %str, i32* %v) #0 {
entry:
  %retval = alloca i8*, align 4
  %str.addr = alloca i8*, align 4
  %v.addr = alloca i32*, align 4
  %orig = alloca i8*, align 4
  %acc = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i32* %v, i32** %v.addr, align 4
  store i32 0, i32* %acc, align 4
  %0 = load i8*, i8** %str.addr, align 4
  store i8* %0, i8** %orig, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i8*, i8** %str.addr, align 4
  %2 = load i8, i8* %1, align 1
  %conv = sext i8 %2 to i32
  store i32 %conv, i32* %c, align 4
  %3 = load i32, i32* %c, align 4
  %cmp = icmp slt i32 %3, 48
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.cond
  %4 = load i32, i32* %c, align 4
  %cmp2 = icmp sgt i32 %4, 57
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.cond
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false
  %5 = load i32, i32* %c, align 4
  %sub = sub i32 %5, 48
  store i32 %sub, i32* %c, align 4
  %6 = load i32, i32* %acc, align 4
  %cmp4 = icmp ugt i32 %6, 429496729
  br i1 %cmp4, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %7 = load i32, i32* %acc, align 4
  %mul = mul i32 %7, 10
  store i32 %mul, i32* %acc, align 4
  %8 = load i32, i32* %c, align 4
  %9 = load i32, i32* %acc, align 4
  %sub8 = sub i32 -1, %9
  %cmp9 = icmp ugt i32 %8, %sub8
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end7
  store i8* null, i8** %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end7
  %10 = load i32, i32* %c, align 4
  %11 = load i32, i32* %acc, align 4
  %add = add i32 %11, %10
  store i32 %add, i32* %acc, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %12 = load i8*, i8** %str.addr, align 4
  %incdec.ptr = getelementptr i8, i8* %12, i32 1
  store i8* %incdec.ptr, i8** %str.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then
  %13 = load i8*, i8** %str.addr, align 4
  %14 = load i8*, i8** %orig, align 4
  %cmp13 = icmp eq i8* %13, %14
  br i1 %cmp13, label %if.then21, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %for.end
  %15 = load i8*, i8** %orig, align 4
  %16 = load i8, i8* %15, align 1
  %conv16 = sext i8 %16 to i32
  %cmp17 = icmp eq i32 %conv16, 48
  br i1 %cmp17, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %lor.lhs.false15
  %17 = load i8*, i8** %str.addr, align 4
  %18 = load i8*, i8** %orig, align 4
  %add.ptr = getelementptr i8, i8* %18, i32 1
  %cmp19 = icmp ne i8* %17, %add.ptr
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %land.lhs.true, %for.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end22:                                         ; preds = %land.lhs.true, %lor.lhs.false15
  %19 = load i32, i32* %acc, align 4
  %20 = load i32*, i32** %v.addr, align 4
  store i32 %19, i32* %20, align 4
  %21 = load i8*, i8** %str.addr, align 4
  store i8* %21, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then21, %if.then11, %if.then6
  %22 = load i8*, i8** %retval, align 4
  ret i8* %22
}

declare i32 @sodium_base642bin(i8*, i32, i8*, i32, i8*, i32*, i8**, i32) #1

declare i32 @strlen(i8*) #1

declare i32 @argon2_validate_inputs(%struct.Argon2_Context*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_encode_string(i8* %dst, i32 %dst_len, %struct.Argon2_Context* %ctx, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i8*, align 4
  %dst_len.addr = alloca i32, align 4
  %ctx.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %validation_result = alloca i32, align 4
  %pp_len = alloca i32, align 4
  %pp_len3 = alloca i32, align 4
  %tmp = alloca [11 x i8], align 1
  %pp_len16 = alloca i32, align 4
  %pp_len29 = alloca i32, align 4
  %tmp38 = alloca [11 x i8], align 1
  %pp_len41 = alloca i32, align 4
  %pp_len54 = alloca i32, align 4
  %tmp63 = alloca [11 x i8], align 1
  %pp_len66 = alloca i32, align 4
  %pp_len79 = alloca i32, align 4
  %tmp88 = alloca [11 x i8], align 1
  %pp_len91 = alloca i32, align 4
  %pp_len104 = alloca i32, align 4
  %sb_len = alloca i32, align 4
  %pp_len122 = alloca i32, align 4
  %sb_len131 = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %dst_len, i32* %dst_len.addr, align 4
  store %struct.Argon2_Context* %ctx, %struct.Argon2_Context** %ctx.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  switch i32 %0, label %sw.default [
    i32 2, label %sw.bb
    i32 1, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %sw.bb
  store i32 12, i32* %pp_len, align 4
  %1 = load i32, i32* %pp_len, align 4
  %2 = load i32, i32* %dst_len.addr, align 4
  %cmp = icmp uge i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  store i32 -31, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %do.body
  %3 = load i8*, i8** %dst.addr, align 4
  %4 = load i32, i32* %pp_len, align 4
  %add = add i32 %4, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %3, i8* align 1 getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0), i32 %add, i1 false)
  %5 = load i32, i32* %pp_len, align 4
  %6 = load i8*, i8** %dst.addr, align 4
  %add.ptr = getelementptr i8, i8* %6, i32 %5
  store i8* %add.ptr, i8** %dst.addr, align 4
  %7 = load i32, i32* %pp_len, align 4
  %8 = load i32, i32* %dst_len.addr, align 4
  %sub = sub i32 %8, %7
  store i32 %sub, i32* %dst_len.addr, align 4
  br label %do.end

do.end:                                           ; preds = %if.end
  br label %sw.epilog

sw.bb1:                                           ; preds = %entry
  br label %do.body2

do.body2:                                         ; preds = %sw.bb1
  store i32 11, i32* %pp_len3, align 4
  %9 = load i32, i32* %pp_len3, align 4
  %10 = load i32, i32* %dst_len.addr, align 4
  %cmp4 = icmp uge i32 %9, %10
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %do.body2
  store i32 -31, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %do.body2
  %11 = load i8*, i8** %dst.addr, align 4
  %12 = load i32, i32* %pp_len3, align 4
  %add7 = add i32 %12, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %11, i8* align 1 getelementptr inbounds ([12 x i8], [12 x i8]* @.str.8, i32 0, i32 0), i32 %add7, i1 false)
  %13 = load i32, i32* %pp_len3, align 4
  %14 = load i8*, i8** %dst.addr, align 4
  %add.ptr8 = getelementptr i8, i8* %14, i32 %13
  store i8* %add.ptr8, i8** %dst.addr, align 4
  %15 = load i32, i32* %pp_len3, align 4
  %16 = load i32, i32* %dst_len.addr, align 4
  %sub9 = sub i32 %16, %15
  store i32 %sub9, i32* %dst_len.addr, align 4
  br label %do.end10

do.end10:                                         ; preds = %if.end6
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  store i32 -31, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %do.end10, %do.end
  %17 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %call = call i32 @argon2_validate_inputs(%struct.Argon2_Context* %17)
  store i32 %call, i32* %validation_result, align 4
  %18 = load i32, i32* %validation_result, align 4
  %cmp11 = icmp ne i32 %18, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %sw.epilog
  %19 = load i32, i32* %validation_result, align 4
  store i32 %19, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %sw.epilog
  br label %do.body14

do.body14:                                        ; preds = %if.end13
  %arraydecay = getelementptr inbounds [11 x i8], [11 x i8]* %tmp, i32 0, i32 0
  call void @u32_to_string(i8* %arraydecay, i32 19)
  br label %do.body15

do.body15:                                        ; preds = %do.body14
  %arraydecay17 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp, i32 0, i32 0
  %call18 = call i32 @strlen(i8* %arraydecay17)
  store i32 %call18, i32* %pp_len16, align 4
  %20 = load i32, i32* %pp_len16, align 4
  %21 = load i32, i32* %dst_len.addr, align 4
  %cmp19 = icmp uge i32 %20, %21
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %do.body15
  store i32 -31, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %do.body15
  %22 = load i8*, i8** %dst.addr, align 4
  %arraydecay22 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp, i32 0, i32 0
  %23 = load i32, i32* %pp_len16, align 4
  %add23 = add i32 %23, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %22, i8* align 1 %arraydecay22, i32 %add23, i1 false)
  %24 = load i32, i32* %pp_len16, align 4
  %25 = load i8*, i8** %dst.addr, align 4
  %add.ptr24 = getelementptr i8, i8* %25, i32 %24
  store i8* %add.ptr24, i8** %dst.addr, align 4
  %26 = load i32, i32* %pp_len16, align 4
  %27 = load i32, i32* %dst_len.addr, align 4
  %sub25 = sub i32 %27, %26
  store i32 %sub25, i32* %dst_len.addr, align 4
  br label %do.end26

do.end26:                                         ; preds = %if.end21
  br label %do.end27

do.end27:                                         ; preds = %do.end26
  br label %do.body28

do.body28:                                        ; preds = %do.end27
  store i32 3, i32* %pp_len29, align 4
  %28 = load i32, i32* %pp_len29, align 4
  %29 = load i32, i32* %dst_len.addr, align 4
  %cmp30 = icmp uge i32 %28, %29
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %do.body28
  store i32 -31, i32* %retval, align 4
  br label %return

if.end32:                                         ; preds = %do.body28
  %30 = load i8*, i8** %dst.addr, align 4
  %31 = load i32, i32* %pp_len29, align 4
  %add33 = add i32 %31, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %30, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.3, i32 0, i32 0), i32 %add33, i1 false)
  %32 = load i32, i32* %pp_len29, align 4
  %33 = load i8*, i8** %dst.addr, align 4
  %add.ptr34 = getelementptr i8, i8* %33, i32 %32
  store i8* %add.ptr34, i8** %dst.addr, align 4
  %34 = load i32, i32* %pp_len29, align 4
  %35 = load i32, i32* %dst_len.addr, align 4
  %sub35 = sub i32 %35, %34
  store i32 %sub35, i32* %dst_len.addr, align 4
  br label %do.end36

do.end36:                                         ; preds = %if.end32
  br label %do.body37

do.body37:                                        ; preds = %do.end36
  %arraydecay39 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp38, i32 0, i32 0
  %36 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %36, i32 0, i32 11
  %37 = load i32, i32* %m_cost, align 4
  call void @u32_to_string(i8* %arraydecay39, i32 %37)
  br label %do.body40

do.body40:                                        ; preds = %do.body37
  %arraydecay42 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp38, i32 0, i32 0
  %call43 = call i32 @strlen(i8* %arraydecay42)
  store i32 %call43, i32* %pp_len41, align 4
  %38 = load i32, i32* %pp_len41, align 4
  %39 = load i32, i32* %dst_len.addr, align 4
  %cmp44 = icmp uge i32 %38, %39
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %do.body40
  store i32 -31, i32* %retval, align 4
  br label %return

if.end46:                                         ; preds = %do.body40
  %40 = load i8*, i8** %dst.addr, align 4
  %arraydecay47 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp38, i32 0, i32 0
  %41 = load i32, i32* %pp_len41, align 4
  %add48 = add i32 %41, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %40, i8* align 1 %arraydecay47, i32 %add48, i1 false)
  %42 = load i32, i32* %pp_len41, align 4
  %43 = load i8*, i8** %dst.addr, align 4
  %add.ptr49 = getelementptr i8, i8* %43, i32 %42
  store i8* %add.ptr49, i8** %dst.addr, align 4
  %44 = load i32, i32* %pp_len41, align 4
  %45 = load i32, i32* %dst_len.addr, align 4
  %sub50 = sub i32 %45, %44
  store i32 %sub50, i32* %dst_len.addr, align 4
  br label %do.end51

do.end51:                                         ; preds = %if.end46
  br label %do.end52

do.end52:                                         ; preds = %do.end51
  br label %do.body53

do.body53:                                        ; preds = %do.end52
  store i32 3, i32* %pp_len54, align 4
  %46 = load i32, i32* %pp_len54, align 4
  %47 = load i32, i32* %dst_len.addr, align 4
  %cmp55 = icmp uge i32 %46, %47
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %do.body53
  store i32 -31, i32* %retval, align 4
  br label %return

if.end57:                                         ; preds = %do.body53
  %48 = load i8*, i8** %dst.addr, align 4
  %49 = load i32, i32* %pp_len54, align 4
  %add58 = add i32 %49, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %48, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.4, i32 0, i32 0), i32 %add58, i1 false)
  %50 = load i32, i32* %pp_len54, align 4
  %51 = load i8*, i8** %dst.addr, align 4
  %add.ptr59 = getelementptr i8, i8* %51, i32 %50
  store i8* %add.ptr59, i8** %dst.addr, align 4
  %52 = load i32, i32* %pp_len54, align 4
  %53 = load i32, i32* %dst_len.addr, align 4
  %sub60 = sub i32 %53, %52
  store i32 %sub60, i32* %dst_len.addr, align 4
  br label %do.end61

do.end61:                                         ; preds = %if.end57
  br label %do.body62

do.body62:                                        ; preds = %do.end61
  %arraydecay64 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp63, i32 0, i32 0
  %54 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %54, i32 0, i32 10
  %55 = load i32, i32* %t_cost, align 4
  call void @u32_to_string(i8* %arraydecay64, i32 %55)
  br label %do.body65

do.body65:                                        ; preds = %do.body62
  %arraydecay67 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp63, i32 0, i32 0
  %call68 = call i32 @strlen(i8* %arraydecay67)
  store i32 %call68, i32* %pp_len66, align 4
  %56 = load i32, i32* %pp_len66, align 4
  %57 = load i32, i32* %dst_len.addr, align 4
  %cmp69 = icmp uge i32 %56, %57
  br i1 %cmp69, label %if.then70, label %if.end71

if.then70:                                        ; preds = %do.body65
  store i32 -31, i32* %retval, align 4
  br label %return

if.end71:                                         ; preds = %do.body65
  %58 = load i8*, i8** %dst.addr, align 4
  %arraydecay72 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp63, i32 0, i32 0
  %59 = load i32, i32* %pp_len66, align 4
  %add73 = add i32 %59, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %58, i8* align 1 %arraydecay72, i32 %add73, i1 false)
  %60 = load i32, i32* %pp_len66, align 4
  %61 = load i8*, i8** %dst.addr, align 4
  %add.ptr74 = getelementptr i8, i8* %61, i32 %60
  store i8* %add.ptr74, i8** %dst.addr, align 4
  %62 = load i32, i32* %pp_len66, align 4
  %63 = load i32, i32* %dst_len.addr, align 4
  %sub75 = sub i32 %63, %62
  store i32 %sub75, i32* %dst_len.addr, align 4
  br label %do.end76

do.end76:                                         ; preds = %if.end71
  br label %do.end77

do.end77:                                         ; preds = %do.end76
  br label %do.body78

do.body78:                                        ; preds = %do.end77
  store i32 3, i32* %pp_len79, align 4
  %64 = load i32, i32* %pp_len79, align 4
  %65 = load i32, i32* %dst_len.addr, align 4
  %cmp80 = icmp uge i32 %64, %65
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %do.body78
  store i32 -31, i32* %retval, align 4
  br label %return

if.end82:                                         ; preds = %do.body78
  %66 = load i8*, i8** %dst.addr, align 4
  %67 = load i32, i32* %pp_len79, align 4
  %add83 = add i32 %67, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %66, i8* align 1 getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0), i32 %add83, i1 false)
  %68 = load i32, i32* %pp_len79, align 4
  %69 = load i8*, i8** %dst.addr, align 4
  %add.ptr84 = getelementptr i8, i8* %69, i32 %68
  store i8* %add.ptr84, i8** %dst.addr, align 4
  %70 = load i32, i32* %pp_len79, align 4
  %71 = load i32, i32* %dst_len.addr, align 4
  %sub85 = sub i32 %71, %70
  store i32 %sub85, i32* %dst_len.addr, align 4
  br label %do.end86

do.end86:                                         ; preds = %if.end82
  br label %do.body87

do.body87:                                        ; preds = %do.end86
  %arraydecay89 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp88, i32 0, i32 0
  %72 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %72, i32 0, i32 12
  %73 = load i32, i32* %lanes, align 4
  call void @u32_to_string(i8* %arraydecay89, i32 %73)
  br label %do.body90

do.body90:                                        ; preds = %do.body87
  %arraydecay92 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp88, i32 0, i32 0
  %call93 = call i32 @strlen(i8* %arraydecay92)
  store i32 %call93, i32* %pp_len91, align 4
  %74 = load i32, i32* %pp_len91, align 4
  %75 = load i32, i32* %dst_len.addr, align 4
  %cmp94 = icmp uge i32 %74, %75
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %do.body90
  store i32 -31, i32* %retval, align 4
  br label %return

if.end96:                                         ; preds = %do.body90
  %76 = load i8*, i8** %dst.addr, align 4
  %arraydecay97 = getelementptr inbounds [11 x i8], [11 x i8]* %tmp88, i32 0, i32 0
  %77 = load i32, i32* %pp_len91, align 4
  %add98 = add i32 %77, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %76, i8* align 1 %arraydecay97, i32 %add98, i1 false)
  %78 = load i32, i32* %pp_len91, align 4
  %79 = load i8*, i8** %dst.addr, align 4
  %add.ptr99 = getelementptr i8, i8* %79, i32 %78
  store i8* %add.ptr99, i8** %dst.addr, align 4
  %80 = load i32, i32* %pp_len91, align 4
  %81 = load i32, i32* %dst_len.addr, align 4
  %sub100 = sub i32 %81, %80
  store i32 %sub100, i32* %dst_len.addr, align 4
  br label %do.end101

do.end101:                                        ; preds = %if.end96
  br label %do.end102

do.end102:                                        ; preds = %do.end101
  br label %do.body103

do.body103:                                       ; preds = %do.end102
  store i32 1, i32* %pp_len104, align 4
  %82 = load i32, i32* %pp_len104, align 4
  %83 = load i32, i32* %dst_len.addr, align 4
  %cmp105 = icmp uge i32 %82, %83
  br i1 %cmp105, label %if.then106, label %if.end107

if.then106:                                       ; preds = %do.body103
  store i32 -31, i32* %retval, align 4
  br label %return

if.end107:                                        ; preds = %do.body103
  %84 = load i8*, i8** %dst.addr, align 4
  %85 = load i32, i32* %pp_len104, align 4
  %add108 = add i32 %85, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %84, i8* align 1 getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0), i32 %add108, i1 false)
  %86 = load i32, i32* %pp_len104, align 4
  %87 = load i8*, i8** %dst.addr, align 4
  %add.ptr109 = getelementptr i8, i8* %87, i32 %86
  store i8* %add.ptr109, i8** %dst.addr, align 4
  %88 = load i32, i32* %pp_len104, align 4
  %89 = load i32, i32* %dst_len.addr, align 4
  %sub110 = sub i32 %89, %88
  store i32 %sub110, i32* %dst_len.addr, align 4
  br label %do.end111

do.end111:                                        ; preds = %if.end107
  br label %do.body112

do.body112:                                       ; preds = %do.end111
  %90 = load i8*, i8** %dst.addr, align 4
  %91 = load i32, i32* %dst_len.addr, align 4
  %92 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %92, i32 0, i32 4
  %93 = load i8*, i8** %salt, align 4
  %94 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %94, i32 0, i32 5
  %95 = load i32, i32* %saltlen, align 4
  %call113 = call i8* @sodium_bin2base64(i8* %90, i32 %91, i8* %93, i32 %95, i32 3)
  %cmp114 = icmp eq i8* %call113, null
  br i1 %cmp114, label %if.then115, label %if.end116

if.then115:                                       ; preds = %do.body112
  store i32 -31, i32* %retval, align 4
  br label %return

if.end116:                                        ; preds = %do.body112
  %96 = load i8*, i8** %dst.addr, align 4
  %call117 = call i32 @strlen(i8* %96)
  store i32 %call117, i32* %sb_len, align 4
  %97 = load i32, i32* %sb_len, align 4
  %98 = load i8*, i8** %dst.addr, align 4
  %add.ptr118 = getelementptr i8, i8* %98, i32 %97
  store i8* %add.ptr118, i8** %dst.addr, align 4
  %99 = load i32, i32* %sb_len, align 4
  %100 = load i32, i32* %dst_len.addr, align 4
  %sub119 = sub i32 %100, %99
  store i32 %sub119, i32* %dst_len.addr, align 4
  br label %do.end120

do.end120:                                        ; preds = %if.end116
  br label %do.body121

do.body121:                                       ; preds = %do.end120
  store i32 1, i32* %pp_len122, align 4
  %101 = load i32, i32* %pp_len122, align 4
  %102 = load i32, i32* %dst_len.addr, align 4
  %cmp123 = icmp uge i32 %101, %102
  br i1 %cmp123, label %if.then124, label %if.end125

if.then124:                                       ; preds = %do.body121
  store i32 -31, i32* %retval, align 4
  br label %return

if.end125:                                        ; preds = %do.body121
  %103 = load i8*, i8** %dst.addr, align 4
  %104 = load i32, i32* %pp_len122, align 4
  %add126 = add i32 %104, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %103, i8* align 1 getelementptr inbounds ([2 x i8], [2 x i8]* @.str.6, i32 0, i32 0), i32 %add126, i1 false)
  %105 = load i32, i32* %pp_len122, align 4
  %106 = load i8*, i8** %dst.addr, align 4
  %add.ptr127 = getelementptr i8, i8* %106, i32 %105
  store i8* %add.ptr127, i8** %dst.addr, align 4
  %107 = load i32, i32* %pp_len122, align 4
  %108 = load i32, i32* %dst_len.addr, align 4
  %sub128 = sub i32 %108, %107
  store i32 %sub128, i32* %dst_len.addr, align 4
  br label %do.end129

do.end129:                                        ; preds = %if.end125
  br label %do.body130

do.body130:                                       ; preds = %do.end129
  %109 = load i8*, i8** %dst.addr, align 4
  %110 = load i32, i32* %dst_len.addr, align 4
  %111 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %111, i32 0, i32 0
  %112 = load i8*, i8** %out, align 4
  %113 = load %struct.Argon2_Context*, %struct.Argon2_Context** %ctx.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %113, i32 0, i32 1
  %114 = load i32, i32* %outlen, align 4
  %call132 = call i8* @sodium_bin2base64(i8* %109, i32 %110, i8* %112, i32 %114, i32 3)
  %cmp133 = icmp eq i8* %call132, null
  br i1 %cmp133, label %if.then134, label %if.end135

if.then134:                                       ; preds = %do.body130
  store i32 -31, i32* %retval, align 4
  br label %return

if.end135:                                        ; preds = %do.body130
  %115 = load i8*, i8** %dst.addr, align 4
  %call136 = call i32 @strlen(i8* %115)
  store i32 %call136, i32* %sb_len131, align 4
  %116 = load i32, i32* %sb_len131, align 4
  %117 = load i8*, i8** %dst.addr, align 4
  %add.ptr137 = getelementptr i8, i8* %117, i32 %116
  store i8* %add.ptr137, i8** %dst.addr, align 4
  %118 = load i32, i32* %sb_len131, align 4
  %119 = load i32, i32* %dst_len.addr, align 4
  %sub138 = sub i32 %119, %118
  store i32 %sub138, i32* %dst_len.addr, align 4
  br label %do.end139

do.end139:                                        ; preds = %if.end135
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %do.end139, %if.then134, %if.then124, %if.then115, %if.then106, %if.then95, %if.then81, %if.then70, %if.then56, %if.then45, %if.then31, %if.then20, %if.then12, %sw.default, %if.then5, %if.then
  %120 = load i32, i32* %retval, align 4
  ret i32 %120
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @u32_to_string(i8* %str, i32 %x) #0 {
entry:
  %str.addr = alloca i8*, align 4
  %x.addr = alloca i32, align 4
  %tmp = alloca [10 x i8], align 1
  %i = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 10, i32* %i, align 4
  br label %do.body

do.body:                                          ; preds = %land.end, %entry
  %0 = load i32, i32* %x.addr, align 4
  %rem = urem i32 %0, 10
  %add = add i32 %rem, 48
  %conv = trunc i32 %add to i8
  %1 = load i32, i32* %i, align 4
  %dec = add i32 %1, -1
  store i32 %dec, i32* %i, align 4
  %arrayidx = getelementptr [10 x i8], [10 x i8]* %tmp, i32 0, i32 %dec
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i32, i32* %x.addr, align 4
  %div = udiv i32 %2, 10
  store i32 %div, i32* %x.addr, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %3 = load i32, i32* %x.addr, align 4
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %4 = load i32, i32* %i, align 4
  %cmp2 = icmp ne i32 %4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %5 = phi i1 [ false, %do.cond ], [ %cmp2, %land.rhs ]
  br i1 %5, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %6 = load i8*, i8** %str.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr [10 x i8], [10 x i8]* %tmp, i32 0, i32 %7
  %8 = load i32, i32* %i, align 4
  %sub = sub i32 10, %8
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %arrayidx4, i32 %sub, i1 false)
  %9 = load i8*, i8** %str.addr, align 4
  %10 = load i32, i32* %i, align 4
  %sub5 = sub i32 10, %10
  %arrayidx6 = getelementptr i8, i8* %9, i32 %sub5
  store i8 0, i8* %arrayidx6, align 1
  ret void
}

declare i8* @sodium_bin2base64(i8*, i32, i8*, i32, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
