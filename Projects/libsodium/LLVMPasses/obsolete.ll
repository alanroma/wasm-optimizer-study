; ModuleID = 'crypto_sign/ed25519/ref10/obsolete.c'
source_filename = "crypto_sign/ed25519/ref10/obsolete.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }
%struct.ge25519_cached = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_p1p1 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_p2 = type { [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_edwards25519sha512batch_keypair(i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %A = alloca %struct.ge25519_p3, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sk.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  %1 = load i8*, i8** %sk.addr, align 4
  %2 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_hash_sha512(i8* %1, i8* %2, i64 32)
  %3 = load i8*, i8** %sk.addr, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %and = and i32 %conv, 248
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1
  %5 = load i8*, i8** %sk.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %5, i32 31
  %6 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %6 to i32
  %and4 = and i32 %conv3, 127
  %conv5 = trunc i32 %and4 to i8
  store i8 %conv5, i8* %arrayidx2, align 1
  %7 = load i8*, i8** %sk.addr, align 4
  %arrayidx6 = getelementptr i8, i8* %7, i32 31
  %8 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %8 to i32
  %or = or i32 %conv7, 64
  %conv8 = trunc i32 %or to i8
  store i8 %conv8, i8* %arrayidx6, align 1
  %9 = load i8*, i8** %sk.addr, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %A, i8* %9)
  %10 = load i8*, i8** %pk.addr, align 4
  call void @ge25519_p3_tobytes(i8* %10, %struct.ge25519_p3* %A)
  ret i32 0
}

declare void @randombytes_buf(i8*, i32) #1

declare i32 @crypto_hash_sha512(i8*, i8*, i64) #1

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

declare void @ge25519_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_edwards25519sha512batch(i8* nonnull %sm, i64* %smlen_p, i8* %m, i64 %mlen, i8* nonnull %sk) #0 {
entry:
  %sm.addr = alloca i8*, align 4
  %smlen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  %hs = alloca %struct.crypto_hash_sha512_state, align 8
  %nonce = alloca [64 x i8], align 16
  %hram = alloca [64 x i8], align 16
  %sig = alloca [64 x i8], align 16
  %A = alloca %struct.ge25519_p3, align 4
  %R = alloca %struct.ge25519_p3, align 4
  store i8* %sm, i8** %sm.addr, align 4
  store i64* %smlen_p, i64** %smlen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  %call = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %hs)
  %0 = load i8*, i8** %sk.addr, align 4
  %add.ptr = getelementptr i8, i8* %0, i32 32
  %call1 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %add.ptr, i64 32)
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %call2 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %1, i64 %2)
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  %call3 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay)
  %3 = load i8*, i8** %sk.addr, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %A, i8* %3)
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  %add.ptr5 = getelementptr i8, i8* %arraydecay4, i32 32
  call void @ge25519_p3_tobytes(i8* %add.ptr5, %struct.ge25519_p3* %A)
  %arraydecay6 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay6)
  %arraydecay7 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %R, i8* %arraydecay7)
  %arraydecay8 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  call void @ge25519_p3_tobytes(i8* %arraydecay8, %struct.ge25519_p3* %R)
  %call9 = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %hs)
  %arraydecay10 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  %call11 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay10, i64 32)
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %call12 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %4, i64 %5)
  %arraydecay13 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  %call14 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay13)
  %arraydecay15 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay15)
  %arraydecay16 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  %add.ptr17 = getelementptr i8, i8* %arraydecay16, i32 32
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  %arraydecay19 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  %6 = load i8*, i8** %sk.addr, align 4
  call void @sc25519_muladd(i8* %add.ptr17, i8* %arraydecay18, i8* %arraydecay19, i8* %6)
  %arraydecay20 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay20, i32 64)
  %7 = load i8*, i8** %sm.addr, align 4
  %add.ptr21 = getelementptr i8, i8* %7, i32 32
  %8 = load i8*, i8** %m.addr, align 4
  %9 = load i64, i64* %mlen.addr, align 8
  %conv = trunc i64 %9 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr21, i8* align 1 %8, i32 %conv, i1 false)
  %10 = load i8*, i8** %sm.addr, align 4
  %arraydecay22 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %10, i8* align 16 %arraydecay22, i32 32, i1 false)
  %11 = load i8*, i8** %sm.addr, align 4
  %add.ptr23 = getelementptr i8, i8* %11, i32 32
  %12 = load i64, i64* %mlen.addr, align 8
  %idx.ext = trunc i64 %12 to i32
  %add.ptr24 = getelementptr i8, i8* %add.ptr23, i32 %idx.ext
  %arraydecay25 = getelementptr inbounds [64 x i8], [64 x i8]* %sig, i32 0, i32 0
  %add.ptr26 = getelementptr i8, i8* %arraydecay25, i32 32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr24, i8* align 1 %add.ptr26, i32 32, i1 false)
  %13 = load i64, i64* %mlen.addr, align 8
  %add = add i64 %13, 64
  %14 = load i64*, i64** %smlen_p.addr, align 4
  store i64 %add, i64* %14, align 8
  ret i32 0
}

declare i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state*) #1

declare i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state*, i8*, i64) #1

declare i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state*, i8*) #1

declare void @sc25519_reduce(i8*) #1

declare void @sc25519_muladd(i8*, i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_edwards25519sha512batch_open(i8* %m, i64* %mlen_p, i8* nonnull %sm, i64 %smlen, i8* nonnull %pk) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %sm.addr = alloca i8*, align 4
  %smlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  %h = alloca [64 x i8], align 16
  %t1 = alloca [32 x i8], align 16
  %t2 = alloca [32 x i8], align 16
  %mlen = alloca i64, align 8
  %Ai = alloca %struct.ge25519_cached, align 4
  %csa = alloca %struct.ge25519_p1p1, align 4
  %cs = alloca %struct.ge25519_p2, align 4
  %A = alloca %struct.ge25519_p3, align 4
  %R = alloca %struct.ge25519_p3, align 4
  %cs3 = alloca %struct.ge25519_p3, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %sm, i8** %sm.addr, align 4
  store i64 %smlen, i64* %smlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load i64*, i64** %mlen_p.addr, align 4
  store i64 0, i64* %0, align 8
  %1 = load i64, i64* %smlen.addr, align 8
  %cmp = icmp ult i64 %1, 64
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i64, i64* %smlen.addr, align 8
  %sub = sub i64 %2, 64
  %cmp1 = icmp ugt i64 %sub, 4294967231
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i64, i64* %smlen.addr, align 8
  %sub2 = sub i64 %3, 64
  store i64 %sub2, i64* %mlen, align 8
  %4 = load i8*, i8** %sm.addr, align 4
  %5 = load i64, i64* %smlen.addr, align 8
  %sub3 = sub i64 %5, 1
  %idxprom = trunc i64 %sub3 to i32
  %arrayidx = getelementptr i8, i8* %4, i32 %idxprom
  %6 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %and = and i32 %conv, 224
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %7 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @ge25519_has_small_order(i8* %7)
  %cmp6 = icmp ne i32 %call, 0
  br i1 %cmp6, label %if.then20, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %if.end5
  %8 = load i8*, i8** %pk.addr, align 4
  %call9 = call i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3* %A, i8* %8)
  %cmp10 = icmp ne i32 %call9, 0
  br i1 %cmp10, label %if.then20, label %lor.lhs.false12

lor.lhs.false12:                                  ; preds = %lor.lhs.false8
  %9 = load i8*, i8** %sm.addr, align 4
  %call13 = call i32 @ge25519_has_small_order(i8* %9)
  %cmp14 = icmp ne i32 %call13, 0
  br i1 %cmp14, label %if.then20, label %lor.lhs.false16

lor.lhs.false16:                                  ; preds = %lor.lhs.false12
  %10 = load i8*, i8** %sm.addr, align 4
  %call17 = call i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3* %R, i8* %10)
  %cmp18 = icmp ne i32 %call17, 0
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %lor.lhs.false16, %lor.lhs.false12, %lor.lhs.false8, %if.end5
  store i32 -1, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %lor.lhs.false16
  call void @ge25519_p3_to_cached(%struct.ge25519_cached* %Ai, %struct.ge25519_p3* %A)
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %11 = load i8*, i8** %sm.addr, align 4
  %12 = load i64, i64* %mlen, align 8
  %add = add i64 %12, 32
  %call22 = call i32 @crypto_hash_sha512(i8* %arraydecay, i8* %11, i64 %add)
  %arraydecay23 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay23)
  %arraydecay24 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @ge25519_scalarmult(%struct.ge25519_p3* %cs3, i8* %arraydecay24, %struct.ge25519_p3* %R)
  call void @ge25519_add(%struct.ge25519_p1p1* %csa, %struct.ge25519_p3* %cs3, %struct.ge25519_cached* %Ai)
  call void @ge25519_p1p1_to_p2(%struct.ge25519_p2* %cs, %struct.ge25519_p1p1* %csa)
  %arraydecay25 = getelementptr inbounds [32 x i8], [32 x i8]* %t1, i32 0, i32 0
  call void @ge25519_tobytes(i8* %arraydecay25, %struct.ge25519_p2* %cs)
  %arrayidx26 = getelementptr [32 x i8], [32 x i8]* %t1, i32 0, i32 31
  %13 = load i8, i8* %arrayidx26, align 1
  %conv27 = zext i8 %13 to i32
  %xor = xor i32 %conv27, 128
  %conv28 = trunc i32 %xor to i8
  store i8 %conv28, i8* %arrayidx26, align 1
  %14 = load i8*, i8** %sm.addr, align 4
  %add.ptr = getelementptr i8, i8* %14, i32 32
  %15 = load i64, i64* %mlen, align 8
  %idx.ext = trunc i64 %15 to i32
  %add.ptr29 = getelementptr i8, i8* %add.ptr, i32 %idx.ext
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %R, i8* %add.ptr29)
  %arraydecay30 = getelementptr inbounds [32 x i8], [32 x i8]* %t2, i32 0, i32 0
  call void @ge25519_p3_tobytes(i8* %arraydecay30, %struct.ge25519_p3* %R)
  %arraydecay31 = getelementptr inbounds [32 x i8], [32 x i8]* %t1, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [32 x i8], [32 x i8]* %t2, i32 0, i32 0
  %call33 = call i32 @crypto_verify_32(i8* %arraydecay31, i8* %arraydecay32)
  %cmp34 = icmp ne i32 %call33, 0
  br i1 %cmp34, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.end21
  store i32 -1, i32* %retval, align 4
  br label %return

if.end37:                                         ; preds = %if.end21
  %16 = load i64, i64* %mlen, align 8
  %17 = load i64*, i64** %mlen_p.addr, align 4
  store i64 %16, i64* %17, align 8
  %18 = load i8*, i8** %m.addr, align 4
  %19 = load i8*, i8** %sm.addr, align 4
  %add.ptr38 = getelementptr i8, i8* %19, i32 32
  %20 = load i64, i64* %mlen, align 8
  %conv39 = trunc i64 %20 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %18, i8* align 1 %add.ptr38, i32 %conv39, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end37, %if.then36, %if.then20, %if.then4, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

declare i32 @ge25519_has_small_order(i8*) #1

declare i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3*, i8*) #1

declare void @ge25519_p3_to_cached(%struct.ge25519_cached*, %struct.ge25519_p3*) #1

declare void @ge25519_scalarmult(%struct.ge25519_p3*, i8*, %struct.ge25519_p3*) #1

declare void @ge25519_add(%struct.ge25519_p1p1*, %struct.ge25519_p3*, %struct.ge25519_cached*) #1

declare void @ge25519_p1p1_to_p2(%struct.ge25519_p2*, %struct.ge25519_p1p1*) #1

declare void @ge25519_tobytes(i8*, %struct.ge25519_p2*) #1

declare i32 @crypto_verify_32(i8*, i8*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
