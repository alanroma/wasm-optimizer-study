; ModuleID = 'crypto_pwhash/scryptsalsa208sha256/pwhash_scryptsalsa208sha256.c'
source_filename = "crypto_pwhash/scryptsalsa208sha256/pwhash_scryptsalsa208sha256.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.escrypt_region_t = type { i8*, i8*, i32 }

@.str = private unnamed_addr constant [4 x i8] c"$7$\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_bytes_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_passwd_min() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_passwd_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_saltbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_strbytes() #0 {
entry:
  ret i32 102
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_pwhash_scryptsalsa208sha256_strprefix() #0 {
entry:
  ret i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_opslimit_min() #0 {
entry:
  ret i32 32768
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_opslimit_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_memlimit_min() #0 {
entry:
  ret i32 16777216
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_memlimit_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_opslimit_interactive() #0 {
entry:
  ret i32 524288
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_memlimit_interactive() #0 {
entry:
  ret i32 16777216
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_opslimit_sensitive() #0 {
entry:
  ret i32 33554432
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_memlimit_sensitive() #0 {
entry:
  ret i32 1073741824
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256(i8* nonnull %out, i64 %outlen, i8* nonnull %passwd, i64 %passwdlen, i8* nonnull %salt, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i64, align 8
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %salt.addr = alloca i8*, align 4
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %N_log2 = alloca i32, align 4
  %p = alloca i32, align 4
  %r = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i64 %outlen, i64* %outlen.addr, align 8
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i8* %salt, i8** %salt.addr, align 4
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i64, i64* %outlen.addr, align 8
  %conv = trunc i64 %1 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 %conv, i1 false)
  %2 = load i64, i64* %passwdlen.addr, align 8
  %cmp = icmp ugt i64 %2, 4294967295
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i64, i64* %outlen.addr, align 8
  %cmp2 = icmp ugt i64 %3, 4294967295
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i64, i64* %outlen.addr, align 8
  %cmp4 = icmp ult i64 %4, 16
  br i1 %cmp4, label %if.then10, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %if.end
  %5 = load i64, i64* %opslimit.addr, align 8
  %6 = load i32, i32* %memlimit.addr, align 4
  %call7 = call i32 @pickparams(i64 %5, i32 %6, i32* %N_log2, i32* %p, i32* %r)
  %cmp8 = icmp ne i32 %call7, 0
  br i1 %cmp8, label %if.then10, label %if.end12

if.then10:                                        ; preds = %lor.lhs.false6, %if.end
  %call11 = call i32* @__errno_location()
  store i32 28, i32* %call11, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %lor.lhs.false6
  %7 = load i8*, i8** %passwd.addr, align 4
  %8 = load i64, i64* %passwdlen.addr, align 8
  %conv13 = trunc i64 %8 to i32
  %9 = load i8*, i8** %salt.addr, align 4
  %10 = load i32, i32* %N_log2, align 4
  %sh_prom = zext i32 %10 to i64
  %shl = shl i64 1, %sh_prom
  %11 = load i32, i32* %r, align 4
  %12 = load i32, i32* %p, align 4
  %13 = load i8*, i8** %out.addr, align 4
  %14 = load i64, i64* %outlen.addr, align 8
  %conv14 = trunc i64 %14 to i32
  %call15 = call i32 @crypto_pwhash_scryptsalsa208sha256_ll(i8* %7, i32 %conv13, i8* %9, i32 32, i64 %shl, i32 %11, i32 %12, i8* %13, i32 %conv14)
  store i32 %call15, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.then10, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

declare i32* @__errno_location() #2

; Function Attrs: noinline nounwind optnone
define internal i32 @pickparams(i64 %opslimit, i32 %memlimit, i32* %N_log2, i32* %p, i32* %r) #0 {
entry:
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %N_log2.addr = alloca i32*, align 4
  %p.addr = alloca i32*, align 4
  %r.addr = alloca i32*, align 4
  %maxN = alloca i64, align 8
  %maxrp = alloca i64, align 8
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  store i32* %N_log2, i32** %N_log2.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %r, i32** %r.addr, align 4
  %0 = load i64, i64* %opslimit.addr, align 8
  %cmp = icmp ult i64 %0, 32768
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i64 32768, i64* %opslimit.addr, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i32*, i32** %r.addr, align 4
  store i32 8, i32* %1, align 4
  %2 = load i64, i64* %opslimit.addr, align 8
  %3 = load i32, i32* %memlimit.addr, align 4
  %div = udiv i32 %3, 32
  %conv = zext i32 %div to i64
  %cmp1 = icmp ult i64 %2, %conv
  br i1 %cmp1, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %4 = load i32*, i32** %p.addr, align 4
  store i32 1, i32* %4, align 4
  %5 = load i64, i64* %opslimit.addr, align 8
  %6 = load i32*, i32** %r.addr, align 4
  %7 = load i32, i32* %6, align 4
  %mul = mul i32 %7, 4
  %conv4 = zext i32 %mul to i64
  %div5 = udiv i64 %5, %conv4
  store i64 %div5, i64* %maxN, align 8
  %8 = load i32*, i32** %N_log2.addr, align 4
  store i32 1, i32* %8, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %9 = load i32*, i32** %N_log2.addr, align 4
  %10 = load i32, i32* %9, align 4
  %cmp6 = icmp ult i32 %10, 63
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32*, i32** %N_log2.addr, align 4
  %12 = load i32, i32* %11, align 4
  %sh_prom = zext i32 %12 to i64
  %shl = shl i64 1, %sh_prom
  %13 = load i64, i64* %maxN, align 8
  %div8 = udiv i64 %13, 2
  %cmp9 = icmp ugt i64 %shl, %div8
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.body
  br label %for.end

if.end12:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %14 = load i32*, i32** %N_log2.addr, align 4
  %15 = load i32, i32* %14, align 4
  %add = add i32 %15, 1
  store i32 %add, i32* %14, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then11, %for.cond
  br label %if.end40

if.else:                                          ; preds = %if.end
  %16 = load i32, i32* %memlimit.addr, align 4
  %17 = load i32*, i32** %r.addr, align 4
  %18 = load i32, i32* %17, align 4
  %mul13 = mul i32 %18, 128
  %div14 = udiv i32 %16, %mul13
  %conv15 = zext i32 %div14 to i64
  store i64 %conv15, i64* %maxN, align 8
  %19 = load i32*, i32** %N_log2.addr, align 4
  store i32 1, i32* %19, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc27, %if.else
  %20 = load i32*, i32** %N_log2.addr, align 4
  %21 = load i32, i32* %20, align 4
  %cmp17 = icmp ult i32 %21, 63
  br i1 %cmp17, label %for.body19, label %for.end29

for.body19:                                       ; preds = %for.cond16
  %22 = load i32*, i32** %N_log2.addr, align 4
  %23 = load i32, i32* %22, align 4
  %sh_prom20 = zext i32 %23 to i64
  %shl21 = shl i64 1, %sh_prom20
  %24 = load i64, i64* %maxN, align 8
  %div22 = udiv i64 %24, 2
  %cmp23 = icmp ugt i64 %shl21, %div22
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %for.body19
  br label %for.end29

if.end26:                                         ; preds = %for.body19
  br label %for.inc27

for.inc27:                                        ; preds = %if.end26
  %25 = load i32*, i32** %N_log2.addr, align 4
  %26 = load i32, i32* %25, align 4
  %add28 = add i32 %26, 1
  store i32 %add28, i32* %25, align 4
  br label %for.cond16

for.end29:                                        ; preds = %if.then25, %for.cond16
  %27 = load i64, i64* %opslimit.addr, align 8
  %div30 = udiv i64 %27, 4
  %28 = load i32*, i32** %N_log2.addr, align 4
  %29 = load i32, i32* %28, align 4
  %sh_prom31 = zext i32 %29 to i64
  %shl32 = shl i64 1, %sh_prom31
  %div33 = udiv i64 %div30, %shl32
  store i64 %div33, i64* %maxrp, align 8
  %30 = load i64, i64* %maxrp, align 8
  %cmp34 = icmp ugt i64 %30, 1073741823
  br i1 %cmp34, label %if.then36, label %if.end37

if.then36:                                        ; preds = %for.end29
  store i64 1073741823, i64* %maxrp, align 8
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %for.end29
  %31 = load i64, i64* %maxrp, align 8
  %conv38 = trunc i64 %31 to i32
  %32 = load i32*, i32** %r.addr, align 4
  %33 = load i32, i32* %32, align 4
  %div39 = udiv i32 %conv38, %33
  %34 = load i32*, i32** %p.addr, align 4
  store i32 %div39, i32* %34, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.end37, %for.end
  ret i32 0
}

declare i32 @crypto_pwhash_scryptsalsa208sha256_ll(i8*, i32, i8*, i32, i64, i32, i32, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_str(i8* nonnull %out, i8* nonnull %passwd, i64 %passwdlen, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %salt = alloca [32 x i8], align 16
  %setting = alloca [58 x i8], align 16
  %escrypt_local = alloca %struct.escrypt_region_t, align 4
  %N_log2 = alloca i32, align 4
  %p = alloca i32, align 4
  %r = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 102, i1 false)
  %1 = load i64, i64* %passwdlen.addr, align 8
  %cmp = icmp ugt i64 %1, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i64, i64* %passwdlen.addr, align 8
  %cmp1 = icmp ult i64 %2, 0
  br i1 %cmp1, label %if.then4, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i64, i64* %opslimit.addr, align 8
  %4 = load i32, i32* %memlimit.addr, align 4
  %call2 = call i32 @pickparams(i64 %3, i32 %4, i32* %N_log2, i32* %p, i32* %r)
  %cmp3 = icmp ne i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %lor.lhs.false, %if.end
  %call5 = call i32* @__errno_location()
  store i32 28, i32* %call5, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %lor.lhs.false
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %salt, i32 0, i32 0
  call void @randombytes_buf(i8* %arraydecay, i32 32)
  %5 = load i32, i32* %N_log2, align 4
  %6 = load i32, i32* %r, align 4
  %7 = load i32, i32* %p, align 4
  %arraydecay7 = getelementptr inbounds [32 x i8], [32 x i8]* %salt, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [58 x i8], [58 x i8]* %setting, i32 0, i32 0
  %call9 = call i8* @escrypt_gensalt_r(i32 %5, i32 %6, i32 %7, i8* %arraydecay7, i32 32, i8* %arraydecay8, i32 58)
  %cmp10 = icmp eq i8* %call9, null
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end6
  %call12 = call i32* @__errno_location()
  store i32 28, i32* %call12, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end6
  %call14 = call i32 @escrypt_init_local(%struct.escrypt_region_t* %escrypt_local)
  %cmp15 = icmp ne i32 %call14, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end13
  store i32 -1, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.end13
  %8 = load i8*, i8** %passwd.addr, align 4
  %9 = load i64, i64* %passwdlen.addr, align 8
  %conv = trunc i64 %9 to i32
  %arraydecay18 = getelementptr inbounds [58 x i8], [58 x i8]* %setting, i32 0, i32 0
  %10 = load i8*, i8** %out.addr, align 4
  %call19 = call i8* @escrypt_r(%struct.escrypt_region_t* %escrypt_local, i8* %8, i32 %conv, i8* %arraydecay18, i8* %10, i32 102)
  %cmp20 = icmp eq i8* %call19, null
  br i1 %cmp20, label %if.then22, label %if.end25

if.then22:                                        ; preds = %if.end17
  %call23 = call i32 @escrypt_free_local(%struct.escrypt_region_t* %escrypt_local)
  %call24 = call i32* @__errno_location()
  store i32 28, i32* %call24, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end17
  %call26 = call i32 @escrypt_free_local(%struct.escrypt_region_t* %escrypt_local)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end25, %if.then22, %if.then16, %if.then11, %if.then4, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare void @randombytes_buf(i8*, i32) #2

declare i8* @escrypt_gensalt_r(i32, i32, i32, i8*, i32, i8*, i32) #2

declare i32 @escrypt_init_local(%struct.escrypt_region_t*) #2

declare i8* @escrypt_r(%struct.escrypt_region_t*, i8*, i32, i8*, i8*, i32) #2

declare i32 @escrypt_free_local(%struct.escrypt_region_t*) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_str_verify(i8* nonnull %str, i8* nonnull %passwd, i64 %passwdlen) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %wanted = alloca [102 x i8], align 16
  %escrypt_local = alloca %struct.escrypt_region_t, align 4
  %ret = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i32 -1, i32* %ret, align 4
  %0 = load i8*, i8** %str.addr, align 4
  %call = call i32 @sodium_strnlen(i8* %0, i32 102)
  %cmp = icmp ne i32 %call, 101
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call1 = call i32 @escrypt_init_local(%struct.escrypt_region_t* %escrypt_local)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %arraydecay = getelementptr inbounds [102 x i8], [102 x i8]* %wanted, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 102, i1 false)
  %1 = load i8*, i8** %passwd.addr, align 4
  %2 = load i64, i64* %passwdlen.addr, align 8
  %conv = trunc i64 %2 to i32
  %3 = load i8*, i8** %str.addr, align 4
  %arraydecay5 = getelementptr inbounds [102 x i8], [102 x i8]* %wanted, i32 0, i32 0
  %call6 = call i8* @escrypt_r(%struct.escrypt_region_t* %escrypt_local, i8* %1, i32 %conv, i8* %3, i8* %arraydecay5, i32 102)
  %cmp7 = icmp eq i8* %call6, null
  br i1 %cmp7, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end4
  %call10 = call i32 @escrypt_free_local(%struct.escrypt_region_t* %escrypt_local)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end4
  %call12 = call i32 @escrypt_free_local(%struct.escrypt_region_t* %escrypt_local)
  %arraydecay13 = getelementptr inbounds [102 x i8], [102 x i8]* %wanted, i32 0, i32 0
  %4 = load i8*, i8** %str.addr, align 4
  %call14 = call i32 @sodium_memcmp(i8* %arraydecay13, i8* %4, i32 102)
  store i32 %call14, i32* %ret, align 4
  %arraydecay15 = getelementptr inbounds [102 x i8], [102 x i8]* %wanted, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay15, i32 102)
  %5 = load i32, i32* %ret, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end11, %if.then9, %if.then3, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sodium_strnlen(i8* %str, i32 %maxlen) #0 {
entry:
  %str.addr = alloca i8*, align 4
  %maxlen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i32 %maxlen, i32* %maxlen.addr, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %maxlen.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load i8*, i8** %str.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %4 to i32
  %cmp1 = icmp ne i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %5 = phi i1 [ false, %while.cond ], [ %cmp1, %land.rhs ]
  br i1 %5, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %7 = load i32, i32* %i, align 4
  ret i32 %7
}

declare i32 @sodium_memcmp(i8*, i8*, i32) #2

declare void @sodium_memzero(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_str_needs_rehash(i8* nonnull %str, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %N_log2 = alloca i32, align 4
  %N_log2_ = alloca i32, align 4
  %p = alloca i32, align 4
  %p_ = alloca i32, align 4
  %r = alloca i32, align 4
  %r_ = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i64, i64* %opslimit.addr, align 8
  %1 = load i32, i32* %memlimit.addr, align 4
  %call = call i32 @pickparams(i64 %0, i32 %1, i32* %N_log2, i32* %p, i32* %r)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call i32* @__errno_location()
  store i32 28, i32* %call1, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %str.addr, align 4
  %call2 = call i32 @sodium_strnlen(i8* %2, i32 102)
  %cmp3 = icmp ne i32 %call2, 101
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %call5 = call i32* @__errno_location()
  store i32 28, i32* %call5, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %3 = load i8*, i8** %str.addr, align 4
  %call7 = call i8* @escrypt_parse_setting(i8* %3, i32* %N_log2_, i32* %r_, i32* %p_)
  %cmp8 = icmp eq i8* %call7, null
  br i1 %cmp8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end6
  %call10 = call i32* @__errno_location()
  store i32 28, i32* %call10, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end6
  %4 = load i32, i32* %N_log2, align 4
  %5 = load i32, i32* %N_log2_, align 4
  %cmp12 = icmp ne i32 %4, %5
  br i1 %cmp12, label %if.then16, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end11
  %6 = load i32, i32* %r, align 4
  %7 = load i32, i32* %r_, align 4
  %cmp13 = icmp ne i32 %6, %7
  br i1 %cmp13, label %if.then16, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %lor.lhs.false
  %8 = load i32, i32* %p, align 4
  %9 = load i32, i32* %p_, align 4
  %cmp15 = icmp ne i32 %8, %9
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %lor.lhs.false14, %lor.lhs.false, %if.end11
  store i32 1, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %lor.lhs.false14
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then9, %if.then4, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

declare i8* @escrypt_parse_setting(i8*, i32*, i32*, i32*) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
