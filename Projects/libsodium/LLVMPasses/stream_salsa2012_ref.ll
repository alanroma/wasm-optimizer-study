; ModuleID = 'crypto_stream/salsa2012/ref/stream_salsa2012_ref.c'
source_filename = "crypto_stream/salsa2012/ref/stream_salsa2012_ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_salsa2012(i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %in = alloca [16 x i8], align 16
  %block = alloca [64 x i8], align 16
  %kcopy = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  %u = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %k.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [32 x i8], [32 x i8]* %kcopy, i32 0, i32 %5
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %7, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %8 = load i8*, i8** %n.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx5, align 1
  %11 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %11
  store i8 %10, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %12 = load i32, i32* %i, align 4
  %inc8 = add i32 %12, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 8, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc14, %for.end9
  %13 = load i32, i32* %i, align 4
  %cmp11 = icmp ult i32 %13, 16
  br i1 %cmp11, label %for.body12, label %for.end16

for.body12:                                       ; preds = %for.cond10
  %14 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %14
  store i8 0, i8* %arrayidx13, align 1
  br label %for.inc14

for.inc14:                                        ; preds = %for.body12
  %15 = load i32, i32* %i, align 4
  %inc15 = add i32 %15, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond10

for.end16:                                        ; preds = %for.cond10
  br label %while.cond

while.cond:                                       ; preds = %for.end27, %for.end16
  %16 = load i64, i64* %clen.addr, align 8
  %cmp17 = icmp uge i64 %16, 64
  br i1 %cmp17, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8*, i8** %c.addr, align 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call = call i32 @crypto_core_salsa2012(i8* %17, i8* %arraydecay, i8* %arraydecay18, i8* null)
  store i32 1, i32* %u, align 4
  store i32 8, i32* %i, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc25, %while.body
  %18 = load i32, i32* %i, align 4
  %cmp20 = icmp ult i32 %18, 16
  br i1 %cmp20, label %for.body21, label %for.end27

for.body21:                                       ; preds = %for.cond19
  %19 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %19
  %20 = load i8, i8* %arrayidx22, align 1
  %conv = zext i8 %20 to i32
  %21 = load i32, i32* %u, align 4
  %add = add i32 %21, %conv
  store i32 %add, i32* %u, align 4
  %22 = load i32, i32* %u, align 4
  %conv23 = trunc i32 %22 to i8
  %23 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %23
  store i8 %conv23, i8* %arrayidx24, align 1
  %24 = load i32, i32* %u, align 4
  %shr = lshr i32 %24, 8
  store i32 %shr, i32* %u, align 4
  br label %for.inc25

for.inc25:                                        ; preds = %for.body21
  %25 = load i32, i32* %i, align 4
  %inc26 = add i32 %25, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond19

for.end27:                                        ; preds = %for.cond19
  %26 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %26, 64
  store i64 %sub, i64* %clen.addr, align 8
  %27 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %27, i32 64
  store i8* %add.ptr, i8** %c.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i64, i64* %clen.addr, align 8
  %tobool28 = icmp ne i64 %28, 0
  br i1 %tobool28, label %if.then29, label %if.end44

if.then29:                                        ; preds = %while.end
  %arraydecay30 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay31 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call33 = call i32 @crypto_core_salsa2012(i8* %arraydecay30, i8* %arraydecay31, i8* %arraydecay32, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc41, %if.then29
  %29 = load i32, i32* %i, align 4
  %30 = load i64, i64* %clen.addr, align 8
  %conv35 = trunc i64 %30 to i32
  %cmp36 = icmp ult i32 %29, %conv35
  br i1 %cmp36, label %for.body38, label %for.end43

for.body38:                                       ; preds = %for.cond34
  %31 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %31
  %32 = load i8, i8* %arrayidx39, align 1
  %33 = load i8*, i8** %c.addr, align 4
  %34 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr i8, i8* %33, i32 %34
  store i8 %32, i8* %arrayidx40, align 1
  br label %for.inc41

for.inc41:                                        ; preds = %for.body38
  %35 = load i32, i32* %i, align 4
  %inc42 = add i32 %35, 1
  store i32 %inc42, i32* %i, align 4
  br label %for.cond34

for.end43:                                        ; preds = %for.cond34
  br label %if.end44

if.end44:                                         ; preds = %for.end43, %while.end
  %arraydecay45 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay45, i32 64)
  %arraydecay46 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay46, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end44, %if.then
  %36 = load i32, i32* %retval, align 4
  ret i32 %36
}

declare i32 @crypto_core_salsa2012(i8*, i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_salsa2012_xor(i8* nonnull %c, i8* nonnull %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %in = alloca [16 x i8], align 16
  %block = alloca [64 x i8], align 16
  %kcopy = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  %u = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %k.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [32 x i8], [32 x i8]* %kcopy, i32 0, i32 %5
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %7, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %8 = load i8*, i8** %n.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx5, align 1
  %11 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %11
  store i8 %10, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %12 = load i32, i32* %i, align 4
  %inc8 = add i32 %12, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 8, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc14, %for.end9
  %13 = load i32, i32* %i, align 4
  %cmp11 = icmp ult i32 %13, 16
  br i1 %cmp11, label %for.body12, label %for.end16

for.body12:                                       ; preds = %for.cond10
  %14 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %14
  store i8 0, i8* %arrayidx13, align 1
  br label %for.inc14

for.inc14:                                        ; preds = %for.body12
  %15 = load i32, i32* %i, align 4
  %inc15 = add i32 %15, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond10

for.end16:                                        ; preds = %for.cond10
  br label %while.cond

while.cond:                                       ; preds = %for.end41, %for.end16
  %16 = load i64, i64* %mlen.addr, align 8
  %cmp17 = icmp uge i64 %16, 64
  br i1 %cmp17, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay19 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call = call i32 @crypto_core_salsa2012(i8* %arraydecay, i8* %arraydecay18, i8* %arraydecay19, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc28, %while.body
  %17 = load i32, i32* %i, align 4
  %cmp21 = icmp ult i32 %17, 64
  br i1 %cmp21, label %for.body22, label %for.end30

for.body22:                                       ; preds = %for.cond20
  %18 = load i8*, i8** %m.addr, align 4
  %19 = load i32, i32* %i, align 4
  %arrayidx23 = getelementptr i8, i8* %18, i32 %19
  %20 = load i8, i8* %arrayidx23, align 1
  %conv = zext i8 %20 to i32
  %21 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %21
  %22 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %22 to i32
  %xor = xor i32 %conv, %conv25
  %conv26 = trunc i32 %xor to i8
  %23 = load i8*, i8** %c.addr, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr i8, i8* %23, i32 %24
  store i8 %conv26, i8* %arrayidx27, align 1
  br label %for.inc28

for.inc28:                                        ; preds = %for.body22
  %25 = load i32, i32* %i, align 4
  %inc29 = add i32 %25, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond20

for.end30:                                        ; preds = %for.cond20
  store i32 1, i32* %u, align 4
  store i32 8, i32* %i, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc39, %for.end30
  %26 = load i32, i32* %i, align 4
  %cmp32 = icmp ult i32 %26, 16
  br i1 %cmp32, label %for.body34, label %for.end41

for.body34:                                       ; preds = %for.cond31
  %27 = load i32, i32* %i, align 4
  %arrayidx35 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %27
  %28 = load i8, i8* %arrayidx35, align 1
  %conv36 = zext i8 %28 to i32
  %29 = load i32, i32* %u, align 4
  %add = add i32 %29, %conv36
  store i32 %add, i32* %u, align 4
  %30 = load i32, i32* %u, align 4
  %conv37 = trunc i32 %30 to i8
  %31 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %31
  store i8 %conv37, i8* %arrayidx38, align 1
  %32 = load i32, i32* %u, align 4
  %shr = lshr i32 %32, 8
  store i32 %shr, i32* %u, align 4
  br label %for.inc39

for.inc39:                                        ; preds = %for.body34
  %33 = load i32, i32* %i, align 4
  %inc40 = add i32 %33, 1
  store i32 %inc40, i32* %i, align 4
  br label %for.cond31

for.end41:                                        ; preds = %for.cond31
  %34 = load i64, i64* %mlen.addr, align 8
  %sub = sub i64 %34, 64
  store i64 %sub, i64* %mlen.addr, align 8
  %35 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %35, i32 64
  store i8* %add.ptr, i8** %c.addr, align 4
  %36 = load i8*, i8** %m.addr, align 4
  %add.ptr42 = getelementptr i8, i8* %36, i32 64
  store i8* %add.ptr42, i8** %m.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %37 = load i64, i64* %mlen.addr, align 8
  %tobool43 = icmp ne i64 %37, 0
  br i1 %tobool43, label %if.then44, label %if.end64

if.then44:                                        ; preds = %while.end
  %arraydecay45 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay46 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay47 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call48 = call i32 @crypto_core_salsa2012(i8* %arraydecay45, i8* %arraydecay46, i8* %arraydecay47, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc61, %if.then44
  %38 = load i32, i32* %i, align 4
  %39 = load i64, i64* %mlen.addr, align 8
  %conv50 = trunc i64 %39 to i32
  %cmp51 = icmp ult i32 %38, %conv50
  br i1 %cmp51, label %for.body53, label %for.end63

for.body53:                                       ; preds = %for.cond49
  %40 = load i8*, i8** %m.addr, align 4
  %41 = load i32, i32* %i, align 4
  %arrayidx54 = getelementptr i8, i8* %40, i32 %41
  %42 = load i8, i8* %arrayidx54, align 1
  %conv55 = zext i8 %42 to i32
  %43 = load i32, i32* %i, align 4
  %arrayidx56 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %43
  %44 = load i8, i8* %arrayidx56, align 1
  %conv57 = zext i8 %44 to i32
  %xor58 = xor i32 %conv55, %conv57
  %conv59 = trunc i32 %xor58 to i8
  %45 = load i8*, i8** %c.addr, align 4
  %46 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr i8, i8* %45, i32 %46
  store i8 %conv59, i8* %arrayidx60, align 1
  br label %for.inc61

for.inc61:                                        ; preds = %for.body53
  %47 = load i32, i32* %i, align 4
  %inc62 = add i32 %47, 1
  store i32 %inc62, i32* %i, align 4
  br label %for.cond49

for.end63:                                        ; preds = %for.cond49
  br label %if.end64

if.end64:                                         ; preds = %for.end63, %while.end
  %arraydecay65 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay65, i32 64)
  %arraydecay66 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay66, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end64, %if.then
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
