; ModuleID = 'crypto_pwhash/argon2/argon2.c'
source_filename = "crypto_pwhash/argon2/argon2.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32 }
%struct.Argon2_instance_t = type { %struct.block_region_*, i64*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.block_region_ = type { i8*, %struct.block_*, i32 }
%struct.block_ = type { [128 x i64] }

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_ctx(%struct.Argon2_Context* %context, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %memory_blocks = alloca i32, align 4
  %segment_length = alloca i32, align 4
  %pass = alloca i32, align 4
  %instance = alloca %struct.Argon2_instance_t, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %call = call i32 @argon2_validate_inputs(%struct.Argon2_Context* %0)
  store i32 %call, i32* %result, align 4
  %1 = load i32, i32* %result, align 4
  %cmp = icmp ne i32 0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %result, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %type.addr, align 4
  %cmp1 = icmp ne i32 %3, 2
  br i1 %cmp1, label %land.lhs.true, label %if.end4

land.lhs.true:                                    ; preds = %if.end
  %4 = load i32, i32* %type.addr, align 4
  %cmp2 = icmp ne i32 %4, 1
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %land.lhs.true
  store i32 -26, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %land.lhs.true, %if.end
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 11
  %6 = load i32, i32* %m_cost, align 4
  store i32 %6, i32* %memory_blocks, align 4
  %7 = load i32, i32* %memory_blocks, align 4
  %8 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %8, i32 0, i32 12
  %9 = load i32, i32* %lanes, align 4
  %mul = mul i32 8, %9
  %cmp5 = icmp ult i32 %7, %mul
  br i1 %cmp5, label %if.then6, label %if.end9

if.then6:                                         ; preds = %if.end4
  %10 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes7 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %10, i32 0, i32 12
  %11 = load i32, i32* %lanes7, align 4
  %mul8 = mul i32 8, %11
  store i32 %mul8, i32* %memory_blocks, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.then6, %if.end4
  %12 = load i32, i32* %memory_blocks, align 4
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes10 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 12
  %14 = load i32, i32* %lanes10, align 4
  %mul11 = mul i32 %14, 4
  %div = udiv i32 %12, %mul11
  store i32 %div, i32* %segment_length, align 4
  %15 = load i32, i32* %segment_length, align 4
  %16 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %16, i32 0, i32 12
  %17 = load i32, i32* %lanes12, align 4
  %mul13 = mul i32 %17, 4
  %mul14 = mul i32 %15, %mul13
  store i32 %mul14, i32* %memory_blocks, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 0
  store %struct.block_region_* null, %struct.block_region_** %region, align 4
  %18 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %18, i32 0, i32 10
  %19 = load i32, i32* %t_cost, align 4
  %passes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 2
  store i32 %19, i32* %passes, align 4
  %current_pass = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 3
  store i32 -1, i32* %current_pass, align 4
  %20 = load i32, i32* %memory_blocks, align 4
  %memory_blocks15 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 4
  store i32 %20, i32* %memory_blocks15, align 4
  %21 = load i32, i32* %segment_length, align 4
  %segment_length16 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 5
  store i32 %21, i32* %segment_length16, align 4
  %22 = load i32, i32* %segment_length, align 4
  %mul17 = mul i32 %22, 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 6
  store i32 %mul17, i32* %lane_length, align 4
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes18 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 12
  %24 = load i32, i32* %lanes18, align 4
  %lanes19 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 7
  store i32 %24, i32* %lanes19, align 4
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 13
  %26 = load i32, i32* %threads, align 4
  %threads20 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 8
  store i32 %26, i32* %threads20, align 4
  %27 = load i32, i32* %type.addr, align 4
  %type21 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 9
  store i32 %27, i32* %type21, align 4
  %28 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %call22 = call i32 @argon2_initialize(%struct.Argon2_instance_t* %instance, %struct.Argon2_Context* %28)
  store i32 %call22, i32* %result, align 4
  %29 = load i32, i32* %result, align 4
  %cmp23 = icmp ne i32 0, %29
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end9
  %30 = load i32, i32* %result, align 4
  store i32 %30, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end9
  store i32 0, i32* %pass, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end25
  %31 = load i32, i32* %pass, align 4
  %passes26 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %instance, i32 0, i32 2
  %32 = load i32, i32* %passes26, align 4
  %cmp27 = icmp ult i32 %31, %32
  br i1 %cmp27, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load i32, i32* %pass, align 4
  call void @argon2_fill_memory_blocks(%struct.Argon2_instance_t* %instance, i32 %33)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %pass, align 4
  %inc = add i32 %34, 1
  store i32 %inc, i32* %pass, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  call void @argon2_finalize(%struct.Argon2_Context* %35, %struct.Argon2_instance_t* %instance)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then24, %if.then3, %if.then
  %36 = load i32, i32* %retval, align 4
  ret i32 %36
}

declare i32 @argon2_validate_inputs(%struct.Argon2_Context*) #1

declare i32 @argon2_initialize(%struct.Argon2_instance_t*, %struct.Argon2_Context*) #1

declare void @argon2_fill_memory_blocks(%struct.Argon2_instance_t*, i32) #1

declare void @argon2_finalize(%struct.Argon2_Context*, %struct.Argon2_instance_t*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_hash(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen, i8* %encoded, i32 %encodedlen, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %context = alloca %struct.Argon2_Context, align 4
  %result = alloca i32, align 4
  %out = alloca i8*, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  store i32 %parallelism, i32* %parallelism.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i8* %hash, i8** %hash.addr, align 4
  store i32 %hashlen, i32* %hashlen.addr, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i32 %encodedlen, i32* %encodedlen.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = load i32, i32* %pwdlen.addr, align 4
  %cmp = icmp ugt i32 %0, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %hashlen.addr, align 4
  %cmp1 = icmp ugt i32 %1, -1
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -3, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load i32, i32* %saltlen.addr, align 4
  %cmp4 = icmp ugt i32 %2, -1
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -7, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %3 = load i32, i32* %hashlen.addr, align 4
  %call = call i8* @malloc(i32 %3)
  store i8* %call, i8** %out, align 4
  %4 = load i8*, i8** %out, align 4
  %tobool = icmp ne i8* %4, null
  br i1 %tobool, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end6
  store i32 -22, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end6
  %5 = load i8*, i8** %out, align 4
  %out9 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 0
  store i8* %5, i8** %out9, align 4
  %6 = load i32, i32* %hashlen.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 1
  store i32 %6, i32* %outlen, align 4
  %7 = load i8*, i8** %pwd.addr, align 4
  %pwd10 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 2
  store i8* %7, i8** %pwd10, align 4
  %8 = load i32, i32* %pwdlen.addr, align 4
  %pwdlen11 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 3
  store i32 %8, i32* %pwdlen11, align 4
  %9 = load i8*, i8** %salt.addr, align 4
  %salt12 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 4
  store i8* %9, i8** %salt12, align 4
  %10 = load i32, i32* %saltlen.addr, align 4
  %saltlen13 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 5
  store i32 %10, i32* %saltlen13, align 4
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 6
  store i8* null, i8** %secret, align 4
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 7
  store i32 0, i32* %secretlen, align 4
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 8
  store i8* null, i8** %ad, align 4
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 9
  store i32 0, i32* %adlen, align 4
  %11 = load i32, i32* %t_cost.addr, align 4
  %t_cost14 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 10
  store i32 %11, i32* %t_cost14, align 4
  %12 = load i32, i32* %m_cost.addr, align 4
  %m_cost15 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 11
  store i32 %12, i32* %m_cost15, align 4
  %13 = load i32, i32* %parallelism.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 12
  store i32 %13, i32* %lanes, align 4
  %14 = load i32, i32* %parallelism.addr, align 4
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 13
  store i32 %14, i32* %threads, align 4
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %context, i32 0, i32 14
  store i32 0, i32* %flags, align 4
  %15 = load i32, i32* %type.addr, align 4
  %call16 = call i32 @argon2_ctx(%struct.Argon2_Context* %context, i32 %15)
  store i32 %call16, i32* %result, align 4
  %16 = load i32, i32* %result, align 4
  %cmp17 = icmp ne i32 %16, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end8
  %17 = load i8*, i8** %out, align 4
  %18 = load i32, i32* %hashlen.addr, align 4
  call void @sodium_memzero(i8* %17, i32 %18)
  %19 = load i8*, i8** %out, align 4
  call void @free(i8* %19)
  %20 = load i32, i32* %result, align 4
  store i32 %20, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end8
  %21 = load i8*, i8** %hash.addr, align 4
  %tobool20 = icmp ne i8* %21, null
  br i1 %tobool20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end19
  %22 = load i8*, i8** %hash.addr, align 4
  %23 = load i8*, i8** %out, align 4
  %24 = load i32, i32* %hashlen.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %22, i8* align 1 %23, i32 %24, i1 false)
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %if.end19
  %25 = load i8*, i8** %encoded.addr, align 4
  %tobool23 = icmp ne i8* %25, null
  br i1 %tobool23, label %land.lhs.true, label %if.end30

land.lhs.true:                                    ; preds = %if.end22
  %26 = load i32, i32* %encodedlen.addr, align 4
  %tobool24 = icmp ne i32 %26, 0
  br i1 %tobool24, label %if.then25, label %if.end30

if.then25:                                        ; preds = %land.lhs.true
  %27 = load i8*, i8** %encoded.addr, align 4
  %28 = load i32, i32* %encodedlen.addr, align 4
  %29 = load i32, i32* %type.addr, align 4
  %call26 = call i32 @argon2_encode_string(i8* %27, i32 %28, %struct.Argon2_Context* %context, i32 %29)
  %cmp27 = icmp ne i32 %call26, 0
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then25
  %30 = load i8*, i8** %out, align 4
  %31 = load i32, i32* %hashlen.addr, align 4
  call void @sodium_memzero(i8* %30, i32 %31)
  %32 = load i8*, i8** %encoded.addr, align 4
  %33 = load i32, i32* %encodedlen.addr, align 4
  call void @sodium_memzero(i8* %32, i32 %33)
  %34 = load i8*, i8** %out, align 4
  call void @free(i8* %34)
  store i32 -31, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.then25
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %land.lhs.true, %if.end22
  %35 = load i8*, i8** %out, align 4
  %36 = load i32, i32* %hashlen.addr, align 4
  call void @sodium_memzero(i8* %35, i32 %36)
  %37 = load i8*, i8** %out, align 4
  call void @free(i8* %37)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end30, %if.then28, %if.then18, %if.then7, %if.then5, %if.then2, %if.then
  %38 = load i32, i32* %retval, align 4
  ret i32 %38
}

declare i8* @malloc(i32) #1

declare void @sodium_memzero(i8*, i32) #1

declare void @free(i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare i32 @argon2_encode_string(i8*, i32, %struct.Argon2_Context*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2i_hash_encoded(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i32 %hashlen, i8* %encoded, i32 %encodedlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  store i32 %parallelism, i32* %parallelism.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i32 %hashlen, i32* %hashlen.addr, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i32 %encodedlen, i32* %encodedlen.addr, align 4
  %0 = load i32, i32* %t_cost.addr, align 4
  %1 = load i32, i32* %m_cost.addr, align 4
  %2 = load i32, i32* %parallelism.addr, align 4
  %3 = load i8*, i8** %pwd.addr, align 4
  %4 = load i32, i32* %pwdlen.addr, align 4
  %5 = load i8*, i8** %salt.addr, align 4
  %6 = load i32, i32* %saltlen.addr, align 4
  %7 = load i32, i32* %hashlen.addr, align 4
  %8 = load i8*, i8** %encoded.addr, align 4
  %9 = load i32, i32* %encodedlen.addr, align 4
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* null, i32 %7, i8* %8, i32 %9, i32 1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2i_hash_raw(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  store i32 %parallelism, i32* %parallelism.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i8* %hash, i8** %hash.addr, align 4
  store i32 %hashlen, i32* %hashlen.addr, align 4
  %0 = load i32, i32* %t_cost.addr, align 4
  %1 = load i32, i32* %m_cost.addr, align 4
  %2 = load i32, i32* %parallelism.addr, align 4
  %3 = load i8*, i8** %pwd.addr, align 4
  %4 = load i32, i32* %pwdlen.addr, align 4
  %5 = load i8*, i8** %salt.addr, align 4
  %6 = load i32, i32* %saltlen.addr, align 4
  %7 = load i8*, i8** %hash.addr, align 4
  %8 = load i32, i32* %hashlen.addr, align 4
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* %7, i32 %8, i8* null, i32 0, i32 1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2id_hash_encoded(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i32 %hashlen, i8* %encoded, i32 %encodedlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hashlen.addr = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %encodedlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  store i32 %parallelism, i32* %parallelism.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i32 %hashlen, i32* %hashlen.addr, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i32 %encodedlen, i32* %encodedlen.addr, align 4
  %0 = load i32, i32* %t_cost.addr, align 4
  %1 = load i32, i32* %m_cost.addr, align 4
  %2 = load i32, i32* %parallelism.addr, align 4
  %3 = load i8*, i8** %pwd.addr, align 4
  %4 = load i32, i32* %pwdlen.addr, align 4
  %5 = load i8*, i8** %salt.addr, align 4
  %6 = load i32, i32* %saltlen.addr, align 4
  %7 = load i32, i32* %hashlen.addr, align 4
  %8 = load i8*, i8** %encoded.addr, align 4
  %9 = load i32, i32* %encodedlen.addr, align 4
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* null, i32 %7, i8* %8, i32 %9, i32 2)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2id_hash_raw(i32 %t_cost, i32 %m_cost, i32 %parallelism, i8* %pwd, i32 %pwdlen, i8* %salt, i32 %saltlen, i8* %hash, i32 %hashlen) #0 {
entry:
  %t_cost.addr = alloca i32, align 4
  %m_cost.addr = alloca i32, align 4
  %parallelism.addr = alloca i32, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %hash.addr = alloca i8*, align 4
  %hashlen.addr = alloca i32, align 4
  store i32 %t_cost, i32* %t_cost.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  store i32 %parallelism, i32* %parallelism.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i8* %hash, i8** %hash.addr, align 4
  store i32 %hashlen, i32* %hashlen.addr, align 4
  %0 = load i32, i32* %t_cost.addr, align 4
  %1 = load i32, i32* %m_cost.addr, align 4
  %2 = load i32, i32* %parallelism.addr, align 4
  %3 = load i8*, i8** %pwd.addr, align 4
  %4 = load i32, i32* %pwdlen.addr, align 4
  %5 = load i8*, i8** %salt.addr, align 4
  %6 = load i32, i32* %saltlen.addr, align 4
  %7 = load i8*, i8** %hash.addr, align 4
  %8 = load i32, i32* %hashlen.addr, align 4
  %call = call i32 @argon2_hash(i32 %0, i32 %1, i32 %2, i8* %3, i32 %4, i8* %5, i32 %6, i8* %7, i32 %8, i8* null, i32 0, i32 2)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_verify(i8* %encoded, i8* %pwd, i32 %pwdlen, i32 %type) #0 {
entry:
  %retval = alloca i32, align 4
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  %type.addr = alloca i32, align 4
  %ctx = alloca %struct.Argon2_Context, align 4
  %out = alloca i8*, align 4
  %decode_result = alloca i32, align 4
  %ret = alloca i32, align 4
  %encoded_len = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = bitcast %struct.Argon2_Context* %ctx to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %0, i8 0, i32 60, i1 false)
  %pwd1 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 2
  store i8* null, i8** %pwd1, align 4
  %pwdlen2 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 3
  store i32 0, i32* %pwdlen2, align 4
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 6
  store i8* null, i8** %secret, align 4
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 7
  store i32 0, i32* %secretlen, align 4
  %1 = load i8*, i8** %encoded.addr, align 4
  %call = call i32 @strlen(i8* %1)
  store i32 %call, i32* %encoded_len, align 4
  %2 = load i32, i32* %encoded_len, align 4
  %cmp = icmp ugt i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -34, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %encoded_len, align 4
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 9
  store i32 %3, i32* %adlen, align 4
  %4 = load i32, i32* %encoded_len, align 4
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 5
  store i32 %4, i32* %saltlen, align 4
  %5 = load i32, i32* %encoded_len, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  store i32 %5, i32* %outlen, align 4
  %adlen3 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 9
  %6 = load i32, i32* %adlen3, align 4
  %call4 = call i8* @malloc(i32 %6)
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  store i8* %call4, i8** %ad, align 4
  %saltlen5 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 5
  %7 = load i32, i32* %saltlen5, align 4
  %call6 = call i8* @malloc(i32 %7)
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  store i8* %call6, i8** %salt, align 4
  %outlen7 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %8 = load i32, i32* %outlen7, align 4
  %call8 = call i8* @malloc(i32 %8)
  %out9 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  store i8* %call8, i8** %out9, align 4
  %out10 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %9 = load i8*, i8** %out10, align 4
  %tobool = icmp ne i8* %9, null
  br i1 %tobool, label %lor.lhs.false, label %if.then16

lor.lhs.false:                                    ; preds = %if.end
  %salt11 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %10 = load i8*, i8** %salt11, align 4
  %tobool12 = icmp ne i8* %10, null
  br i1 %tobool12, label %lor.lhs.false13, label %if.then16

lor.lhs.false13:                                  ; preds = %lor.lhs.false
  %ad14 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  %11 = load i8*, i8** %ad14, align 4
  %tobool15 = icmp ne i8* %11, null
  br i1 %tobool15, label %if.end20, label %if.then16

if.then16:                                        ; preds = %lor.lhs.false13, %lor.lhs.false, %if.end
  %ad17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  %12 = load i8*, i8** %ad17, align 4
  call void @free(i8* %12)
  %salt18 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %13 = load i8*, i8** %salt18, align 4
  call void @free(i8* %13)
  %out19 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %14 = load i8*, i8** %out19, align 4
  call void @free(i8* %14)
  store i32 -22, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %lor.lhs.false13
  %outlen21 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %15 = load i32, i32* %outlen21, align 4
  %call22 = call i8* @malloc(i32 %15)
  store i8* %call22, i8** %out, align 4
  %16 = load i8*, i8** %out, align 4
  %tobool23 = icmp ne i8* %16, null
  br i1 %tobool23, label %if.end28, label %if.then24

if.then24:                                        ; preds = %if.end20
  %ad25 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  %17 = load i8*, i8** %ad25, align 4
  call void @free(i8* %17)
  %salt26 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %18 = load i8*, i8** %salt26, align 4
  call void @free(i8* %18)
  %out27 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %19 = load i8*, i8** %out27, align 4
  call void @free(i8* %19)
  store i32 -22, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %if.end20
  %20 = load i8*, i8** %encoded.addr, align 4
  %21 = load i32, i32* %type.addr, align 4
  %call29 = call i32 @argon2_decode_string(%struct.Argon2_Context* %ctx, i8* %20, i32 %21)
  store i32 %call29, i32* %decode_result, align 4
  %22 = load i32, i32* %decode_result, align 4
  %cmp30 = icmp ne i32 %22, 0
  br i1 %cmp30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %if.end28
  %ad32 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  %23 = load i8*, i8** %ad32, align 4
  call void @free(i8* %23)
  %salt33 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %24 = load i8*, i8** %salt33, align 4
  call void @free(i8* %24)
  %out34 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %25 = load i8*, i8** %out34, align 4
  call void @free(i8* %25)
  %26 = load i8*, i8** %out, align 4
  call void @free(i8* %26)
  %27 = load i32, i32* %decode_result, align 4
  store i32 %27, i32* %retval, align 4
  br label %return

if.end35:                                         ; preds = %if.end28
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 10
  %28 = load i32, i32* %t_cost, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 11
  %29 = load i32, i32* %m_cost, align 4
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 13
  %30 = load i32, i32* %threads, align 4
  %31 = load i8*, i8** %pwd.addr, align 4
  %32 = load i32, i32* %pwdlen.addr, align 4
  %salt36 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %33 = load i8*, i8** %salt36, align 4
  %saltlen37 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 5
  %34 = load i32, i32* %saltlen37, align 4
  %35 = load i8*, i8** %out, align 4
  %outlen38 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %36 = load i32, i32* %outlen38, align 4
  %37 = load i32, i32* %type.addr, align 4
  %call39 = call i32 @argon2_hash(i32 %28, i32 %29, i32 %30, i8* %31, i32 %32, i8* %33, i32 %34, i8* %35, i32 %36, i8* null, i32 0, i32 %37)
  store i32 %call39, i32* %ret, align 4
  %ad40 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 8
  %38 = load i8*, i8** %ad40, align 4
  call void @free(i8* %38)
  %salt41 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 4
  %39 = load i8*, i8** %salt41, align 4
  call void @free(i8* %39)
  %40 = load i32, i32* %ret, align 4
  %cmp42 = icmp ne i32 %40, 0
  br i1 %cmp42, label %if.then48, label %lor.lhs.false43

lor.lhs.false43:                                  ; preds = %if.end35
  %41 = load i8*, i8** %out, align 4
  %out44 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %42 = load i8*, i8** %out44, align 4
  %outlen45 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 1
  %43 = load i32, i32* %outlen45, align 4
  %call46 = call i32 @sodium_memcmp(i8* %41, i8* %42, i32 %43)
  %cmp47 = icmp ne i32 %call46, 0
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %lor.lhs.false43, %if.end35
  store i32 -35, i32* %ret, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then48, %lor.lhs.false43
  %44 = load i8*, i8** %out, align 4
  call void @free(i8* %44)
  %out50 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %ctx, i32 0, i32 0
  %45 = load i8*, i8** %out50, align 4
  call void @free(i8* %45)
  %46 = load i32, i32* %ret, align 4
  store i32 %46, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end49, %if.then31, %if.then24, %if.then16, %if.then
  %47 = load i32, i32* %retval, align 4
  ret i32 %47
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare i32 @strlen(i8*) #1

declare i32 @argon2_decode_string(%struct.Argon2_Context*, i8*, i32) #1

declare i32 @sodium_memcmp(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2i_verify(i8* %encoded, i8* %pwd, i32 %pwdlen) #0 {
entry:
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  %0 = load i8*, i8** %encoded.addr, align 4
  %1 = load i8*, i8** %pwd.addr, align 4
  %2 = load i32, i32* %pwdlen.addr, align 4
  %call = call i32 @argon2_verify(i8* %0, i8* %1, i32 %2, i32 1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2id_verify(i8* %encoded, i8* %pwd, i32 %pwdlen) #0 {
entry:
  %encoded.addr = alloca i8*, align 4
  %pwd.addr = alloca i8*, align 4
  %pwdlen.addr = alloca i32, align 4
  store i8* %encoded, i8** %encoded.addr, align 4
  store i8* %pwd, i8** %pwd.addr, align 4
  store i32 %pwdlen, i32* %pwdlen.addr, align 4
  %0 = load i8*, i8** %encoded.addr, align 4
  %1 = load i8*, i8** %pwd.addr, align 4
  %2 = load i32, i32* %pwdlen.addr, align 4
  %call = call i32 @argon2_verify(i8* %0, i8* %1, i32 %2, i32 2)
  ret i32 %call
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
