; ModuleID = 'crypto_scalarmult/ed25519/ref10/scalarmult_ed25519_ref10.c'
source_filename = "crypto_scalarmult/ed25519/ref10/scalarmult_ed25519_ref10.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519(i8* nonnull %q, i8* nonnull %n, i8* nonnull %p) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %2 = load i8*, i8** %p.addr, align 4
  %call = call i32 @_crypto_scalarmult_ed25519(i8* %0, i8* %1, i8* %2, i32 1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_crypto_scalarmult_ed25519(i8* %q, i8* %n, i8* %p, i32 %clamp) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %clamp.addr = alloca i32, align 4
  %t = alloca i8*, align 4
  %Q = alloca %struct.ge25519_p3, align 4
  %P = alloca %struct.ge25519_p3, align 4
  %i = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store i32 %clamp, i32* %clamp.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  %1 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ge25519_is_canonical(i8* %1)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8*, i8** %p.addr, align 4
  %call1 = call i32 @ge25519_has_small_order(i8* %2)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %3 = load i8*, i8** %p.addr, align 4
  %call4 = call i32 @ge25519_frombytes(%struct.ge25519_p3* %P, i8* %3)
  %cmp5 = icmp ne i32 %call4, 0
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %call7 = call i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3* %P)
  %cmp8 = icmp eq i32 %call7, 0
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4
  %cmp9 = icmp ult i32 %4, 32
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1
  %8 = load i8*, i8** %t, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr i8, i8* %8, i32 %9
  store i8 %7, i8* %arrayidx10, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load i32, i32* %clamp.addr, align 4
  %cmp11 = icmp ne i32 %11, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.end
  %12 = load i8*, i8** %t, align 4
  call void @_crypto_scalarmult_ed25519_clamp(i8* %12)
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %for.end
  %13 = load i8*, i8** %t, align 4
  %arrayidx14 = getelementptr i8, i8* %13, i32 31
  %14 = load i8, i8* %arrayidx14, align 1
  %conv = zext i8 %14 to i32
  %and = and i32 %conv, 127
  %conv15 = trunc i32 %and to i8
  store i8 %conv15, i8* %arrayidx14, align 1
  %15 = load i8*, i8** %t, align 4
  call void @ge25519_scalarmult(%struct.ge25519_p3* %Q, i8* %15, %struct.ge25519_p3* %P)
  %16 = load i8*, i8** %q.addr, align 4
  call void @ge25519_p3_tobytes(i8* %16, %struct.ge25519_p3* %Q)
  %17 = load i8*, i8** %q.addr, align 4
  %call16 = call i32 @_crypto_scalarmult_ed25519_is_inf(i8* %17)
  %cmp17 = icmp ne i32 %call16, 0
  br i1 %cmp17, label %if.then21, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %if.end13
  %18 = load i8*, i8** %n.addr, align 4
  %call20 = call i32 @sodium_is_zero(i8* %18, i32 32)
  %tobool = icmp ne i32 %call20, 0
  br i1 %tobool, label %if.then21, label %if.end22

if.then21:                                        ; preds = %lor.lhs.false19, %if.end13
  store i32 -1, i32* %retval, align 4
  br label %return

if.end22:                                         ; preds = %lor.lhs.false19
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then21, %if.then
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519_noclamp(i8* nonnull %q, i8* nonnull %n, i8* nonnull %p) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %2 = load i8*, i8** %p.addr, align 4
  %call = call i32 @_crypto_scalarmult_ed25519(i8* %0, i8* %1, i8* %2, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519_base(i8* nonnull %q, i8* nonnull %n) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %call = call i32 @_crypto_scalarmult_ed25519_base(i8* %0, i8* %1, i32 1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_crypto_scalarmult_ed25519_base(i8* %q, i8* %n, i32 %clamp) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %clamp.addr = alloca i32, align 4
  %t = alloca i8*, align 4
  %Q = alloca %struct.ge25519_p3, align 4
  %i = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i32 %clamp, i32* %clamp.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i8*, i8** %t, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i32, i32* %clamp.addr, align 4
  %cmp2 = icmp ne i32 %8, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %9 = load i8*, i8** %t, align 4
  call void @_crypto_scalarmult_ed25519_clamp(i8* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %10 = load i8*, i8** %t, align 4
  %arrayidx3 = getelementptr i8, i8* %10, i32 31
  %11 = load i8, i8* %arrayidx3, align 1
  %conv = zext i8 %11 to i32
  %and = and i32 %conv, 127
  %conv4 = trunc i32 %and to i8
  store i8 %conv4, i8* %arrayidx3, align 1
  %12 = load i8*, i8** %t, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %Q, i8* %12)
  %13 = load i8*, i8** %q.addr, align 4
  call void @ge25519_p3_tobytes(i8* %13, %struct.ge25519_p3* %Q)
  %14 = load i8*, i8** %q.addr, align 4
  %call = call i32 @_crypto_scalarmult_ed25519_is_inf(i8* %14)
  %cmp5 = icmp ne i32 %call, 0
  br i1 %cmp5, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %15 = load i8*, i8** %n.addr, align 4
  %call7 = call i32 @sodium_is_zero(i8* %15, i32 32)
  %tobool = icmp ne i32 %call7, 0
  br i1 %tobool, label %if.then8, label %if.end9

if.then8:                                         ; preds = %lor.lhs.false, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %lor.lhs.false
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then8
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519_base_noclamp(i8* nonnull %q, i8* nonnull %n) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %call = call i32 @_crypto_scalarmult_ed25519_base(i8* %0, i8* %1, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_ed25519_scalarbytes() #0 {
entry:
  ret i32 32
}

declare i32 @ge25519_is_canonical(i8*) #1

declare i32 @ge25519_has_small_order(i8*) #1

declare i32 @ge25519_frombytes(%struct.ge25519_p3*, i8*) #1

declare i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define internal void @_crypto_scalarmult_ed25519_clamp(i8* %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 248
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1
  %2 = load i8*, i8** %k.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %2, i32 31
  %3 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %3 to i32
  %or = or i32 %conv3, 64
  %conv4 = trunc i32 %or to i8
  store i8 %conv4, i8* %arrayidx2, align 1
  ret void
}

declare void @ge25519_scalarmult(%struct.ge25519_p3*, i8*, %struct.ge25519_p3*) #1

declare void @ge25519_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @_crypto_scalarmult_ed25519_is_inf(i8* %s) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %c = alloca i8, align 1
  %i = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %s.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %xor = xor i32 %conv, 1
  %conv1 = trunc i32 %xor to i8
  store i8 %conv1, i8* %c, align 1
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 31
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %s.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr i8, i8* %3, i32 %4
  %5 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %5 to i32
  %6 = load i8, i8* %c, align 1
  %conv5 = zext i8 %6 to i32
  %or = or i32 %conv5, %conv4
  %conv6 = trunc i32 %or to i8
  store i8 %conv6, i8* %c, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %s.addr, align 4
  %arrayidx7 = getelementptr i8, i8* %8, i32 31
  %9 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %9 to i32
  %and = and i32 %conv8, 127
  %10 = load i8, i8* %c, align 1
  %conv9 = zext i8 %10 to i32
  %or10 = or i32 %conv9, %and
  %conv11 = trunc i32 %or10 to i8
  store i8 %conv11, i8* %c, align 1
  %11 = load i8, i8* %c, align 1
  %conv12 = zext i8 %11 to i32
  %sub = sub i32 %conv12, 1
  %shr = lshr i32 %sub, 8
  %and13 = and i32 %shr, 1
  ret i32 %and13
}

declare i32 @sodium_is_zero(i8*, i32) #1

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
