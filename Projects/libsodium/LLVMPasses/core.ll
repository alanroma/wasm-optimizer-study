; ModuleID = 'sodium/core.c'
source_filename = "sodium/core.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@initialized = internal global i32 0, align 4
@_misuse_handler = internal global void ()* null, align 4

; Function Attrs: noinline nounwind optnone
define i32 @sodium_init() #0 {
entry:
  %retval = alloca i32, align 4
  %call = call i32 @sodium_crit_enter()
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %0 = load volatile i32, i32* @initialized, align 4
  %cmp1 = icmp ne i32 %0, 0
  br i1 %cmp1, label %if.then2, label %if.end7

if.then2:                                         ; preds = %if.end
  %call3 = call i32 @sodium_crit_leave()
  %cmp4 = icmp ne i32 %call3, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.then2
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.then2
  store i32 1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %call8 = call i32 @_sodium_runtime_get_cpu_features()
  call void @randombytes_stir()
  %call9 = call i32 @_sodium_alloc_init()
  %call10 = call i32 @_crypto_pwhash_argon2_pick_best_implementation()
  %call11 = call i32 @_crypto_generichash_blake2b_pick_best_implementation()
  %call12 = call i32 @_crypto_onetimeauth_poly1305_pick_best_implementation()
  %call13 = call i32 @_crypto_scalarmult_curve25519_pick_best_implementation()
  %call14 = call i32 @_crypto_stream_chacha20_pick_best_implementation()
  %call15 = call i32 @_crypto_stream_salsa20_pick_best_implementation()
  store volatile i32 1, i32* @initialized, align 4
  %call16 = call i32 @sodium_crit_leave()
  %cmp17 = icmp ne i32 %call16, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end7
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.then18, %if.end6, %if.then5, %if.then
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @sodium_crit_enter() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @sodium_crit_leave() #0 {
entry:
  ret i32 0
}

declare i32 @_sodium_runtime_get_cpu_features() #1

declare void @randombytes_stir() #1

declare i32 @_sodium_alloc_init() #1

declare i32 @_crypto_pwhash_argon2_pick_best_implementation() #1

declare i32 @_crypto_generichash_blake2b_pick_best_implementation() #1

declare i32 @_crypto_onetimeauth_poly1305_pick_best_implementation() #1

declare i32 @_crypto_scalarmult_curve25519_pick_best_implementation() #1

declare i32 @_crypto_stream_chacha20_pick_best_implementation() #1

declare i32 @_crypto_stream_salsa20_pick_best_implementation() #1

; Function Attrs: noinline noreturn nounwind optnone
define void @sodium_misuse() #2 {
entry:
  %handler = alloca void ()*, align 4
  %call = call i32 @sodium_crit_leave()
  %call1 = call i32 @sodium_crit_enter()
  %cmp = icmp eq i32 %call1, 0
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %0 = load void ()*, void ()** @_misuse_handler, align 4
  store void ()* %0, void ()** %handler, align 4
  %1 = load void ()*, void ()** %handler, align 4
  %cmp2 = icmp ne void ()* %1, null
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %2 = load void ()*, void ()** %handler, align 4
  call void %2()
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  call void @abort() #4
  unreachable
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: noinline nounwind optnone
define i32 @sodium_set_misuse_handler(void ()* %handler) #0 {
entry:
  %retval = alloca i32, align 4
  %handler.addr = alloca void ()*, align 4
  store void ()* %handler, void ()** %handler.addr, align 4
  %call = call i32 @sodium_crit_enter()
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %0 = load void ()*, void ()** %handler.addr, align 4
  store void ()* %0, void ()** @_misuse_handler, align 4
  %call1 = call i32 @sodium_crit_leave()
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
