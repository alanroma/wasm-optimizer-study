; ModuleID = 'crypto_pwhash/argon2/argon2-core.c'
source_filename = "crypto_pwhash/argon2/argon2-core.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.Argon2_instance_t = type { %struct.block_region_*, i64*, i32, i32, i32, i32, i32, i32, i32, i32, i32 }
%struct.block_region_ = type { i8*, %struct.block_*, i32 }
%struct.block_ = type { [128 x i64] }
%struct.Argon2_position_t = type { i32, i32, i8, i32 }
%struct.Argon2_Context = type { i8*, i32, i8*, i32, i8*, i32, i8*, i32, i8*, i32, i32, i32, i32, i32, i32 }
%struct.crypto_generichash_blake2b_state = type { [384 x i8] }

@fill_segment = internal global void (%struct.Argon2_instance_t*, %struct.Argon2_position_t*)* @argon2_fill_segment_ref, align 4

; Function Attrs: noinline nounwind optnone
define hidden void @argon2_finalize(%struct.Argon2_Context* %context, %struct.Argon2_instance_t* %instance) #0 {
entry:
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %blockhash = alloca %struct.block_, align 8
  %l = alloca i32, align 4
  %last_block_in_lane = alloca i32, align 4
  %blockhash_bytes = alloca [1024 x i8], align 16
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %cmp = icmp ne %struct.Argon2_Context* %0, null
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %cmp1 = icmp ne %struct.Argon2_instance_t* %1, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %2, i32 0, i32 0
  %3 = load %struct.block_region_*, %struct.block_region_** %region, align 4
  %memory = getelementptr inbounds %struct.block_region_, %struct.block_region_* %3, i32 0, i32 1
  %4 = load %struct.block_*, %struct.block_** %memory, align 4
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 6
  %6 = load i32, i32* %lane_length, align 4
  %add.ptr = getelementptr %struct.block_, %struct.block_* %4, i32 %6
  %add.ptr2 = getelementptr %struct.block_, %struct.block_* %add.ptr, i32 -1
  call void @copy_block(%struct.block_* %blockhash, %struct.block_* %add.ptr2)
  store i32 1, i32* %l, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %l, align 4
  %8 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %8, i32 0, i32 7
  %9 = load i32, i32* %lanes, align 4
  %cmp3 = icmp ult i32 %7, %9
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i32, i32* %l, align 4
  %11 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length4 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %11, i32 0, i32 6
  %12 = load i32, i32* %lane_length4, align 4
  %mul = mul i32 %10, %12
  %13 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length5 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %13, i32 0, i32 6
  %14 = load i32, i32* %lane_length5, align 4
  %sub = sub i32 %14, 1
  %add = add i32 %mul, %sub
  store i32 %add, i32* %last_block_in_lane, align 4
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region6 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 0
  %16 = load %struct.block_region_*, %struct.block_region_** %region6, align 4
  %memory7 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %16, i32 0, i32 1
  %17 = load %struct.block_*, %struct.block_** %memory7, align 4
  %18 = load i32, i32* %last_block_in_lane, align 4
  %add.ptr8 = getelementptr %struct.block_, %struct.block_* %17, i32 %18
  call void @xor_block(%struct.block_* %blockhash, %struct.block_* %add.ptr8)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %l, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @store_block(i8* %arraydecay, %struct.block_* %blockhash)
  %20 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %20, i32 0, i32 0
  %21 = load i8*, i8** %out, align 4
  %22 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %22, i32 0, i32 1
  %23 = load i32, i32* %outlen, align 4
  %arraydecay9 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %call = call i32 @blake2b_long(i8* %21, i32 %23, i8* %arraydecay9, i32 1024)
  %v = getelementptr inbounds %struct.block_, %struct.block_* %blockhash, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %24 = bitcast i64* %arraydecay10 to i8*
  call void @sodium_memzero(i8* %24, i32 1024)
  %arraydecay11 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay11, i32 1024)
  %25 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %26 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %26, i32 0, i32 14
  %27 = load i32, i32* %flags, align 4
  call void @argon2_free_instance(%struct.Argon2_instance_t* %25, i32 %27)
  br label %if.end

if.end:                                           ; preds = %for.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @copy_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4
  store %struct.block_* %src, %struct.block_** %src.addr, align 4
  %0 = load %struct.block_*, %struct.block_** %dst.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [128 x i64], [128 x i64]* %v, i32 0, i32 0
  %1 = bitcast i64* %arraydecay to i8*
  %2 = load %struct.block_*, %struct.block_** %src.addr, align 4
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [128 x i64], [128 x i64]* %v1, i32 0, i32 0
  %3 = bitcast i64* %arraydecay2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %1, i8* align 8 %3, i32 1024, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @xor_block(%struct.block_* %dst, %struct.block_* %src) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %src.addr = alloca %struct.block_*, align 4
  %i = alloca i32, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4
  store %struct.block_* %src, %struct.block_** %src.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.block_*, %struct.block_** %src.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %2
  %3 = load i64, i64* %arrayidx, align 8
  %4 = load %struct.block_*, %struct.block_** %dst.addr, align 4
  %v1 = getelementptr inbounds %struct.block_, %struct.block_* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [128 x i64], [128 x i64]* %v1, i32 0, i32 %5
  %6 = load i64, i64* %arrayidx2, align 8
  %xor = xor i64 %6, %3
  store i64 %xor, i64* %arrayidx2, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store_block(i8* %output, %struct.block_* %src) #0 {
entry:
  %output.addr = alloca i8*, align 4
  %src.addr = alloca %struct.block_*, align 4
  %i = alloca i32, align 4
  store i8* %output, i8** %output.addr, align 4
  store %struct.block_* %src, %struct.block_** %src.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %output.addr, align 4
  %2 = load i32, i32* %i, align 4
  %mul = mul i32 %2, 8
  %add.ptr = getelementptr i8, i8* %1, i32 %mul
  %3 = load %struct.block_*, %struct.block_** %src.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %4
  %5 = load i64, i64* %arrayidx, align 8
  call void @store64_le(i8* %add.ptr, i64 %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare i32 @blake2b_long(i8*, i32, i8*, i32) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @argon2_free_instance(%struct.Argon2_instance_t* %instance, i32 %flags) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %flags.addr = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store i32 %flags, i32* %flags.addr, align 4
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %1 = load i32, i32* %flags.addr, align 4
  %and = and i32 %1, 4
  call void @clear_memory(%struct.Argon2_instance_t* %0, i32 %and)
  %2 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %2, i32 0, i32 1
  %3 = load i64*, i64** %pseudo_rands, align 4
  %4 = bitcast i64* %3 to i8*
  call void @free(i8* %4)
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands1 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 1
  store i64* null, i64** %pseudo_rands1, align 4
  %6 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %6, i32 0, i32 0
  %7 = load %struct.block_region_*, %struct.block_region_** %region, align 4
  call void @free_memory(%struct.block_region_* %7)
  %8 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region2 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %8, i32 0, i32 0
  store %struct.block_region_* null, %struct.block_region_** %region2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @argon2_fill_memory_blocks(%struct.Argon2_instance_t* %instance, i32 %pass) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %pass.addr = alloca i32, align 4
  %position = alloca %struct.Argon2_position_t, align 4
  %l = alloca i32, align 4
  %s = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store i32 %pass, i32* %pass.addr, align 4
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %cmp = icmp eq %struct.Argon2_instance_t* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %1, i32 0, i32 7
  %2 = load i32, i32* %lanes, align 4
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %for.end11

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i32, i32* %pass.addr, align 4
  %pass2 = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 0
  store i32 %3, i32* %pass2, align 4
  store i32 0, i32* %s, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %if.end
  %4 = load i32, i32* %s, align 4
  %cmp3 = icmp ult i32 %4, 4
  br i1 %cmp3, label %for.body, label %for.end11

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %s, align 4
  %conv = trunc i32 %5 to i8
  %slice = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 2
  store i8 %conv, i8* %slice, align 4
  store i32 0, i32* %l, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %l, align 4
  %7 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lanes5 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %7, i32 0, i32 7
  %8 = load i32, i32* %lanes5, align 4
  %cmp6 = icmp ult i32 %6, %8
  br i1 %cmp6, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond4
  %9 = load i32, i32* %l, align 4
  %lane = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 1
  store i32 %9, i32* %lane, align 4
  %index = getelementptr inbounds %struct.Argon2_position_t, %struct.Argon2_position_t* %position, i32 0, i32 3
  store i32 0, i32* %index, align 4
  %10 = load void (%struct.Argon2_instance_t*, %struct.Argon2_position_t*)*, void (%struct.Argon2_instance_t*, %struct.Argon2_position_t*)** @fill_segment, align 4
  %11 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  call void %10(%struct.Argon2_instance_t* %11, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4 %position)
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %12 = load i32, i32* %l, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %13 = load i32, i32* %s, align 4
  %inc10 = add i32 %13, 1
  store i32 %inc10, i32* %s, align 4
  br label %for.cond

for.end11:                                        ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_validate_inputs(%struct.Argon2_Context* %context) #0 {
entry:
  %retval = alloca i32, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %cmp = icmp eq %struct.Argon2_Context* null, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -25, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %out = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %1, i32 0, i32 0
  %2 = load i8*, i8** %out, align 4
  %cmp1 = icmp eq i8* null, %2
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %3, i32 0, i32 1
  %4 = load i32, i32* %outlen, align 4
  %cmp4 = icmp ugt i32 16, %4
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -2, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %outlen7 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %5, i32 0, i32 1
  %6 = load i32, i32* %outlen7, align 4
  %cmp8 = icmp ult i32 -1, %6
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end6
  store i32 -3, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.end6
  %7 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwd = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %7, i32 0, i32 2
  %8 = load i8*, i8** %pwd, align 4
  %cmp11 = icmp eq i8* null, %8
  br i1 %cmp11, label %if.then12, label %if.end16

if.then12:                                        ; preds = %if.end10
  %9 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %9, i32 0, i32 3
  %10 = load i32, i32* %pwdlen, align 4
  %cmp13 = icmp ne i32 0, %10
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.then12
  store i32 -18, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then12
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.end10
  %11 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen17 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %11, i32 0, i32 3
  %12 = load i32, i32* %pwdlen17, align 4
  %cmp18 = icmp ugt i32 0, %12
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  store i32 -4, i32* %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.end16
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen21 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 3
  %14 = load i32, i32* %pwdlen21, align 4
  %cmp22 = icmp ult i32 -1, %14
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end20
  store i32 -5, i32* %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.end20
  %15 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %15, i32 0, i32 4
  %16 = load i8*, i8** %salt, align 4
  %cmp25 = icmp eq i8* null, %16
  br i1 %cmp25, label %if.then26, label %if.end30

if.then26:                                        ; preds = %if.end24
  %17 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %17, i32 0, i32 5
  %18 = load i32, i32* %saltlen, align 4
  %cmp27 = icmp ne i32 0, %18
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.then26
  store i32 -19, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.then26
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.end24
  %19 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %saltlen31 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %19, i32 0, i32 5
  %20 = load i32, i32* %saltlen31, align 4
  %cmp32 = icmp ugt i32 8, %20
  br i1 %cmp32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.end30
  store i32 -6, i32* %retval, align 4
  br label %return

if.end34:                                         ; preds = %if.end30
  %21 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %saltlen35 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %21, i32 0, i32 5
  %22 = load i32, i32* %saltlen35, align 4
  %cmp36 = icmp ult i32 -1, %22
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end34
  store i32 -7, i32* %retval, align 4
  br label %return

if.end38:                                         ; preds = %if.end34
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 6
  %24 = load i8*, i8** %secret, align 4
  %cmp39 = icmp eq i8* null, %24
  br i1 %cmp39, label %if.then40, label %if.else

if.then40:                                        ; preds = %if.end38
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 7
  %26 = load i32, i32* %secretlen, align 4
  %cmp41 = icmp ne i32 0, %26
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.then40
  store i32 -20, i32* %retval, align 4
  br label %return

if.end43:                                         ; preds = %if.then40
  br label %if.end52

if.else:                                          ; preds = %if.end38
  %27 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen44 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %27, i32 0, i32 7
  %28 = load i32, i32* %secretlen44, align 4
  %cmp45 = icmp ugt i32 0, %28
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.else
  store i32 -10, i32* %retval, align 4
  br label %return

if.end47:                                         ; preds = %if.else
  %29 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen48 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %29, i32 0, i32 7
  %30 = load i32, i32* %secretlen48, align 4
  %cmp49 = icmp ult i32 -1, %30
  br i1 %cmp49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end47
  store i32 -11, i32* %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.end47
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.end43
  %31 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %31, i32 0, i32 8
  %32 = load i8*, i8** %ad, align 4
  %cmp53 = icmp eq i8* null, %32
  br i1 %cmp53, label %if.then54, label %if.else58

if.then54:                                        ; preds = %if.end52
  %33 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %33, i32 0, i32 9
  %34 = load i32, i32* %adlen, align 4
  %cmp55 = icmp ne i32 0, %34
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %if.then54
  store i32 -21, i32* %retval, align 4
  br label %return

if.end57:                                         ; preds = %if.then54
  br label %if.end67

if.else58:                                        ; preds = %if.end52
  %35 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %adlen59 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %35, i32 0, i32 9
  %36 = load i32, i32* %adlen59, align 4
  %cmp60 = icmp ugt i32 0, %36
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %if.else58
  store i32 -8, i32* %retval, align 4
  br label %return

if.end62:                                         ; preds = %if.else58
  %37 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %adlen63 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %37, i32 0, i32 9
  %38 = load i32, i32* %adlen63, align 4
  %cmp64 = icmp ult i32 -1, %38
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end62
  store i32 -9, i32* %retval, align 4
  br label %return

if.end66:                                         ; preds = %if.end62
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.end57
  %39 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %39, i32 0, i32 11
  %40 = load i32, i32* %m_cost, align 4
  %cmp68 = icmp ugt i32 8, %40
  br i1 %cmp68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.end67
  store i32 -14, i32* %retval, align 4
  br label %return

if.end70:                                         ; preds = %if.end67
  %41 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %m_cost71 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %41, i32 0, i32 11
  %42 = load i32, i32* %m_cost71, align 4
  %conv = zext i32 %42 to i64
  %cmp72 = icmp ult i64 2097152, %conv
  br i1 %cmp72, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.end70
  store i32 -15, i32* %retval, align 4
  br label %return

if.end75:                                         ; preds = %if.end70
  %43 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %m_cost76 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %43, i32 0, i32 11
  %44 = load i32, i32* %m_cost76, align 4
  %45 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %45, i32 0, i32 12
  %46 = load i32, i32* %lanes, align 4
  %mul = mul i32 8, %46
  %cmp77 = icmp ult i32 %44, %mul
  br i1 %cmp77, label %if.then79, label %if.end80

if.then79:                                        ; preds = %if.end75
  store i32 -14, i32* %retval, align 4
  br label %return

if.end80:                                         ; preds = %if.end75
  %47 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %47, i32 0, i32 10
  %48 = load i32, i32* %t_cost, align 4
  %cmp81 = icmp ugt i32 1, %48
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end80
  store i32 -12, i32* %retval, align 4
  br label %return

if.end84:                                         ; preds = %if.end80
  %49 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %t_cost85 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %49, i32 0, i32 10
  %50 = load i32, i32* %t_cost85, align 4
  %cmp86 = icmp ult i32 -1, %50
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %if.end84
  store i32 -13, i32* %retval, align 4
  br label %return

if.end89:                                         ; preds = %if.end84
  %51 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes90 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %51, i32 0, i32 12
  %52 = load i32, i32* %lanes90, align 4
  %cmp91 = icmp ugt i32 1, %52
  br i1 %cmp91, label %if.then93, label %if.end94

if.then93:                                        ; preds = %if.end89
  store i32 -16, i32* %retval, align 4
  br label %return

if.end94:                                         ; preds = %if.end89
  %53 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes95 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %53, i32 0, i32 12
  %54 = load i32, i32* %lanes95, align 4
  %cmp96 = icmp ult i32 16777215, %54
  br i1 %cmp96, label %if.then98, label %if.end99

if.then98:                                        ; preds = %if.end94
  store i32 -17, i32* %retval, align 4
  br label %return

if.end99:                                         ; preds = %if.end94
  %55 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %threads = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %55, i32 0, i32 13
  %56 = load i32, i32* %threads, align 4
  %cmp100 = icmp ugt i32 1, %56
  br i1 %cmp100, label %if.then102, label %if.end103

if.then102:                                       ; preds = %if.end99
  store i32 -28, i32* %retval, align 4
  br label %return

if.end103:                                        ; preds = %if.end99
  %57 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %threads104 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %57, i32 0, i32 13
  %58 = load i32, i32* %threads104, align 4
  %cmp105 = icmp ult i32 16777215, %58
  br i1 %cmp105, label %if.then107, label %if.end108

if.then107:                                       ; preds = %if.end103
  store i32 -29, i32* %retval, align 4
  br label %return

if.end108:                                        ; preds = %if.end103
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end108, %if.then107, %if.then102, %if.then98, %if.then93, %if.then88, %if.then83, %if.then79, %if.then74, %if.then69, %if.then65, %if.then61, %if.then56, %if.then50, %if.then46, %if.then42, %if.then37, %if.then33, %if.then28, %if.then23, %if.then19, %if.then14, %if.then9, %if.then5, %if.then2, %if.then
  %59 = load i32, i32* %retval, align 4
  ret i32 %59
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @argon2_initialize(%struct.Argon2_instance_t* %instance, %struct.Argon2_Context* %context) #0 {
entry:
  %retval = alloca i32, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %blockhash = alloca [72 x i8], align 16
  %result = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4
  store i32 0, i32* %result, align 4
  %0 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %cmp = icmp eq %struct.Argon2_instance_t* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %cmp1 = icmp eq %struct.Argon2_Context* %1, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -25, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %2, i32 0, i32 5
  %3 = load i32, i32* %segment_length, align 4
  %mul = mul i32 8, %3
  %call = call i8* @malloc(i32 %mul)
  %4 = bitcast i8* %call to i64*
  %5 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %5, i32 0, i32 1
  store i64* %4, i64** %pseudo_rands, align 4
  %cmp2 = icmp eq i64* %4, null
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -22, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %6, i32 0, i32 0
  %7 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %7, i32 0, i32 4
  %8 = load i32, i32* %memory_blocks, align 4
  %call5 = call i32 @allocate_memory(%struct.block_region_** %region, i32 %8)
  store i32 %call5, i32* %result, align 4
  %9 = load i32, i32* %result, align 4
  %cmp6 = icmp ne i32 0, %9
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end4
  %10 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %11 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %11, i32 0, i32 14
  %12 = load i32, i32* %flags, align 4
  call void @argon2_free_instance(%struct.Argon2_instance_t* %10, i32 %12)
  %13 = load i32, i32* %result, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end4
  %arraydecay = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %14 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %type = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 9
  %16 = load i32, i32* %type, align 4
  call void @argon2_initial_hash(i8* %arraydecay, %struct.Argon2_Context* %14, i32 %16)
  %arraydecay9 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay9, i32 64
  call void @sodium_memzero(i8* %add.ptr, i32 8)
  %arraydecay10 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  %17 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  call void @argon2_fill_first_blocks(i8* %arraydecay10, %struct.Argon2_instance_t* %17)
  %arraydecay11 = getelementptr inbounds [72 x i8], [72 x i8]* %blockhash, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay11, i32 72)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.then7, %if.then3, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

declare i8* @malloc(i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @allocate_memory(%struct.block_region_** %region, i32 %m_cost) #0 {
entry:
  %retval = alloca i32, align 4
  %region.addr = alloca %struct.block_region_**, align 4
  %m_cost.addr = alloca i32, align 4
  %base = alloca i8*, align 4
  %memory = alloca %struct.block_*, align 4
  %memory_size = alloca i32, align 4
  store %struct.block_region_** %region, %struct.block_region_*** %region.addr, align 4
  store i32 %m_cost, i32* %m_cost.addr, align 4
  %0 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %cmp = icmp eq %struct.block_region_** %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -22, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %m_cost.addr, align 4
  %mul = mul i32 1024, %1
  store i32 %mul, i32* %memory_size, align 4
  %2 = load i32, i32* %m_cost.addr, align 4
  %cmp1 = icmp eq i32 %2, 0
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i32, i32* %memory_size, align 4
  %4 = load i32, i32* %m_cost.addr, align 4
  %div = udiv i32 %3, %4
  %cmp2 = icmp ne i32 %div, 1024
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  store i32 -22, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %lor.lhs.false
  %call = call i8* @malloc(i32 12)
  %5 = bitcast i8* %call to %struct.block_region_*
  %6 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  store %struct.block_region_* %5, %struct.block_region_** %6, align 4
  %7 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %8 = load %struct.block_region_*, %struct.block_region_** %7, align 4
  %cmp5 = icmp eq %struct.block_region_* %8, null
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 -22, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end4
  %9 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %10 = load %struct.block_region_*, %struct.block_region_** %9, align 4
  %memory8 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %10, i32 0, i32 1
  store %struct.block_* null, %struct.block_** %memory8, align 4
  %11 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %12 = load %struct.block_region_*, %struct.block_region_** %11, align 4
  %base9 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %12, i32 0, i32 0
  store i8* null, i8** %base9, align 4
  %13 = load i32, i32* %memory_size, align 4
  %call10 = call i32 @posix_memalign(i8** %base, i32 64, i32 %13)
  %call11 = call i32* @__errno_location()
  store i32 %call10, i32* %call11, align 4
  %cmp12 = icmp ne i32 %call10, 0
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end7
  store i8* null, i8** %base, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end7
  %14 = bitcast %struct.block_** %memory to i8*
  %15 = bitcast i8** %base to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %16 = load i8*, i8** %base, align 4
  %cmp15 = icmp eq i8* %16, null
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end14
  %17 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %18 = load %struct.block_region_*, %struct.block_region_** %17, align 4
  %19 = bitcast %struct.block_region_* %18 to i8*
  call void @free(i8* %19)
  %20 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  store %struct.block_region_* null, %struct.block_region_** %20, align 4
  store i32 -22, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.end14
  %21 = load i8*, i8** %base, align 4
  %22 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %23 = load %struct.block_region_*, %struct.block_region_** %22, align 4
  %base18 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %23, i32 0, i32 0
  store i8* %21, i8** %base18, align 4
  %24 = load %struct.block_*, %struct.block_** %memory, align 4
  %25 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %26 = load %struct.block_region_*, %struct.block_region_** %25, align 4
  %memory19 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %26, i32 0, i32 1
  store %struct.block_* %24, %struct.block_** %memory19, align 4
  %27 = load i32, i32* %memory_size, align 4
  %28 = load %struct.block_region_**, %struct.block_region_*** %region.addr, align 4
  %29 = load %struct.block_region_*, %struct.block_region_** %28, align 4
  %size = getelementptr inbounds %struct.block_region_, %struct.block_region_* %29, i32 0, i32 2
  store i32 %27, i32* %size, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end17, %if.then16, %if.then6, %if.then3, %if.then
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: noinline nounwind optnone
define internal void @argon2_initial_hash(i8* %blockhash, %struct.Argon2_Context* %context, i32 %type) #0 {
entry:
  %blockhash.addr = alloca i8*, align 4
  %context.addr = alloca %struct.Argon2_Context*, align 4
  %type.addr = alloca i32, align 4
  %BlakeHash = alloca %struct.crypto_generichash_blake2b_state, align 64
  %value = alloca [4 x i8], align 1
  store i8* %blockhash, i8** %blockhash.addr, align 4
  store %struct.Argon2_Context* %context, %struct.Argon2_Context** %context.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %0 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %cmp = icmp eq %struct.Argon2_Context* null, %0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %blockhash.addr, align 4
  %cmp1 = icmp eq i8* null, %1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %call = call i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* null, i32 0, i32 64)
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %2 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %2, i32 0, i32 12
  %3 = load i32, i32* %lanes, align 4
  call void @store32_le(i8* %arraydecay, i32 %3)
  %arraydecay2 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call3 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay2, i64 4)
  %arraydecay4 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %4 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %outlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %4, i32 0, i32 1
  %5 = load i32, i32* %outlen, align 4
  call void @store32_le(i8* %arraydecay4, i32 %5)
  %arraydecay5 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call6 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay5, i64 4)
  %arraydecay7 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %6 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %m_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %6, i32 0, i32 11
  %7 = load i32, i32* %m_cost, align 4
  call void @store32_le(i8* %arraydecay7, i32 %7)
  %arraydecay8 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call9 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay8, i64 4)
  %arraydecay10 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %8 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %t_cost = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %8, i32 0, i32 10
  %9 = load i32, i32* %t_cost, align 4
  call void @store32_le(i8* %arraydecay10, i32 %9)
  %arraydecay11 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call12 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay11, i64 4)
  %arraydecay13 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  call void @store32_le(i8* %arraydecay13, i32 19)
  %arraydecay14 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call15 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay14, i64 4)
  %arraydecay16 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %10 = load i32, i32* %type.addr, align 4
  call void @store32_le(i8* %arraydecay16, i32 %10)
  %arraydecay17 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call18 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay17, i64 4)
  %arraydecay19 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %11 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %11, i32 0, i32 3
  %12 = load i32, i32* %pwdlen, align 4
  call void @store32_le(i8* %arraydecay19, i32 %12)
  %arraydecay20 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call21 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay20, i64 4)
  %13 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwd = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %13, i32 0, i32 2
  %14 = load i8*, i8** %pwd, align 4
  %cmp22 = icmp ne i8* %14, null
  br i1 %cmp22, label %if.then23, label %if.end32

if.then23:                                        ; preds = %if.end
  %15 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwd24 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %15, i32 0, i32 2
  %16 = load i8*, i8** %pwd24, align 4
  %17 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen25 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %17, i32 0, i32 3
  %18 = load i32, i32* %pwdlen25, align 4
  %conv = zext i32 %18 to i64
  %call26 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %16, i64 %conv)
  %19 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %flags = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %19, i32 0, i32 14
  %20 = load i32, i32* %flags, align 4
  %and = and i32 %20, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then27, label %if.end31

if.then27:                                        ; preds = %if.then23
  %21 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwd28 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %21, i32 0, i32 2
  %22 = load i8*, i8** %pwd28, align 4
  %23 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen29 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %23, i32 0, i32 3
  %24 = load i32, i32* %pwdlen29, align 4
  call void @sodium_memzero(i8* %22, i32 %24)
  %25 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %pwdlen30 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %25, i32 0, i32 3
  store i32 0, i32* %pwdlen30, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then27, %if.then23
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.end
  %arraydecay33 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %26 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %saltlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %26, i32 0, i32 5
  %27 = load i32, i32* %saltlen, align 4
  call void @store32_le(i8* %arraydecay33, i32 %27)
  %arraydecay34 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call35 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay34, i64 4)
  %28 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %salt = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %28, i32 0, i32 4
  %29 = load i8*, i8** %salt, align 4
  %cmp36 = icmp ne i8* %29, null
  br i1 %cmp36, label %if.then38, label %if.end43

if.then38:                                        ; preds = %if.end32
  %30 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %salt39 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %30, i32 0, i32 4
  %31 = load i8*, i8** %salt39, align 4
  %32 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %saltlen40 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %32, i32 0, i32 5
  %33 = load i32, i32* %saltlen40, align 4
  %conv41 = zext i32 %33 to i64
  %call42 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %31, i64 %conv41)
  br label %if.end43

if.end43:                                         ; preds = %if.then38, %if.end32
  %arraydecay44 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %34 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %34, i32 0, i32 7
  %35 = load i32, i32* %secretlen, align 4
  call void @store32_le(i8* %arraydecay44, i32 %35)
  %arraydecay45 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call46 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay45, i64 4)
  %36 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secret = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %36, i32 0, i32 6
  %37 = load i8*, i8** %secret, align 4
  %cmp47 = icmp ne i8* %37, null
  br i1 %cmp47, label %if.then49, label %if.end62

if.then49:                                        ; preds = %if.end43
  %38 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secret50 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %38, i32 0, i32 6
  %39 = load i8*, i8** %secret50, align 4
  %40 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen51 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %40, i32 0, i32 7
  %41 = load i32, i32* %secretlen51, align 4
  %conv52 = zext i32 %41 to i64
  %call53 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %39, i64 %conv52)
  %42 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %flags54 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %42, i32 0, i32 14
  %43 = load i32, i32* %flags54, align 4
  %and55 = and i32 %43, 2
  %tobool56 = icmp ne i32 %and55, 0
  br i1 %tobool56, label %if.then57, label %if.end61

if.then57:                                        ; preds = %if.then49
  %44 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secret58 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %44, i32 0, i32 6
  %45 = load i8*, i8** %secret58, align 4
  %46 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen59 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %46, i32 0, i32 7
  %47 = load i32, i32* %secretlen59, align 4
  call void @sodium_memzero(i8* %45, i32 %47)
  %48 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %secretlen60 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %48, i32 0, i32 7
  store i32 0, i32* %secretlen60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.then57, %if.then49
  br label %if.end62

if.end62:                                         ; preds = %if.end61, %if.end43
  %arraydecay63 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %49 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %adlen = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %49, i32 0, i32 9
  %50 = load i32, i32* %adlen, align 4
  call void @store32_le(i8* %arraydecay63, i32 %50)
  %arraydecay64 = getelementptr inbounds [4 x i8], [4 x i8]* %value, i32 0, i32 0
  %call65 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %arraydecay64, i64 4)
  %51 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %ad = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %51, i32 0, i32 8
  %52 = load i8*, i8** %ad, align 4
  %cmp66 = icmp ne i8* %52, null
  br i1 %cmp66, label %if.then68, label %if.end73

if.then68:                                        ; preds = %if.end62
  %53 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %ad69 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %53, i32 0, i32 8
  %54 = load i8*, i8** %ad69, align 4
  %55 = load %struct.Argon2_Context*, %struct.Argon2_Context** %context.addr, align 4
  %adlen70 = getelementptr inbounds %struct.Argon2_Context, %struct.Argon2_Context* %55, i32 0, i32 9
  %56 = load i32, i32* %adlen70, align 4
  %conv71 = zext i32 %56 to i64
  %call72 = call i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %54, i64 %conv71)
  br label %if.end73

if.end73:                                         ; preds = %if.then68, %if.end62
  %57 = load i8*, i8** %blockhash.addr, align 4
  %call74 = call i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state* %BlakeHash, i8* %57, i32 64)
  br label %return

return:                                           ; preds = %if.end73, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @argon2_fill_first_blocks(i8* %blockhash, %struct.Argon2_instance_t* %instance) #0 {
entry:
  %blockhash.addr = alloca i8*, align 4
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %l = alloca i32, align 4
  %blockhash_bytes = alloca [1024 x i8], align 16
  store i8* %blockhash, i8** %blockhash.addr, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store i32 0, i32* %l, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %l, align 4
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lanes = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %1, i32 0, i32 7
  %2 = load i32, i32* %lanes, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %blockhash.addr, align 4
  %add.ptr = getelementptr i8, i8* %3, i32 64
  call void @store32_le(i8* %add.ptr, i32 0)
  %4 = load i8*, i8** %blockhash.addr, align 4
  %add.ptr1 = getelementptr i8, i8* %4, i32 64
  %add.ptr2 = getelementptr i8, i8* %add.ptr1, i32 4
  %5 = load i32, i32* %l, align 4
  call void @store32_le(i8* %add.ptr2, i32 %5)
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %6 = load i8*, i8** %blockhash.addr, align 4
  %call = call i32 @blake2b_long(i8* %arraydecay, i32 1024, i8* %6, i32 72)
  %7 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %7, i32 0, i32 0
  %8 = load %struct.block_region_*, %struct.block_region_** %region, align 4
  %memory = getelementptr inbounds %struct.block_region_, %struct.block_region_* %8, i32 0, i32 1
  %9 = load %struct.block_*, %struct.block_** %memory, align 4
  %10 = load i32, i32* %l, align 4
  %11 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %11, i32 0, i32 6
  %12 = load i32, i32* %lane_length, align 4
  %mul = mul i32 %10, %12
  %add = add i32 %mul, 0
  %arrayidx = getelementptr %struct.block_, %struct.block_* %9, i32 %add
  %arraydecay3 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @load_block(%struct.block_* %arrayidx, i8* %arraydecay3)
  %13 = load i8*, i8** %blockhash.addr, align 4
  %add.ptr4 = getelementptr i8, i8* %13, i32 64
  call void @store32_le(i8* %add.ptr4, i32 1)
  %arraydecay5 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  %14 = load i8*, i8** %blockhash.addr, align 4
  %call6 = call i32 @blake2b_long(i8* %arraydecay5, i32 1024, i8* %14, i32 72)
  %15 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region7 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %15, i32 0, i32 0
  %16 = load %struct.block_region_*, %struct.block_region_** %region7, align 4
  %memory8 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %16, i32 0, i32 1
  %17 = load %struct.block_*, %struct.block_** %memory8, align 4
  %18 = load i32, i32* %l, align 4
  %19 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %lane_length9 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %19, i32 0, i32 6
  %20 = load i32, i32* %lane_length9, align 4
  %mul10 = mul i32 %18, %20
  %add11 = add i32 %mul10, 1
  %arrayidx12 = getelementptr %struct.block_, %struct.block_* %17, i32 %add11
  %arraydecay13 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @load_block(%struct.block_* %arrayidx12, i8* %arraydecay13)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %l, align 4
  %inc = add i32 %21, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay14 = getelementptr inbounds [1024 x i8], [1024 x i8]* %blockhash_bytes, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay14, i32 1024)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_pwhash_argon2_pick_best_implementation() #0 {
entry:
  %call = call i32 @argon2_pick_best_implementation()
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @argon2_pick_best_implementation() #0 {
entry:
  store void (%struct.Argon2_instance_t*, %struct.Argon2_position_t*)* @argon2_fill_segment_ref, void (%struct.Argon2_instance_t*, %struct.Argon2_position_t*)** @fill_segment, align 4
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @clear_memory(%struct.Argon2_instance_t* %instance, i32 %clear) #0 {
entry:
  %instance.addr = alloca %struct.Argon2_instance_t*, align 4
  %clear.addr = alloca i32, align 4
  store %struct.Argon2_instance_t* %instance, %struct.Argon2_instance_t** %instance.addr, align 4
  store i32 %clear, i32* %clear.addr, align 4
  %0 = load i32, i32* %clear.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %1 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %1, i32 0, i32 0
  %2 = load %struct.block_region_*, %struct.block_region_** %region, align 4
  %cmp = icmp ne %struct.block_region_* %2, null
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %3 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %region2 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %3, i32 0, i32 0
  %4 = load %struct.block_region_*, %struct.block_region_** %region2, align 4
  %memory = getelementptr inbounds %struct.block_region_, %struct.block_region_* %4, i32 0, i32 1
  %5 = load %struct.block_*, %struct.block_** %memory, align 4
  %6 = bitcast %struct.block_* %5 to i8*
  %7 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %memory_blocks = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %7, i32 0, i32 4
  %8 = load i32, i32* %memory_blocks, align 4
  %mul = mul i32 1024, %8
  call void @sodium_memzero(i8* %6, i32 %mul)
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  %9 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %9, i32 0, i32 1
  %10 = load i64*, i64** %pseudo_rands, align 4
  %cmp3 = icmp ne i64* %10, null
  br i1 %cmp3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %11 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %pseudo_rands5 = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %11, i32 0, i32 1
  %12 = load i64*, i64** %pseudo_rands5, align 4
  %13 = bitcast i64* %12 to i8*
  %14 = load %struct.Argon2_instance_t*, %struct.Argon2_instance_t** %instance.addr, align 4
  %segment_length = getelementptr inbounds %struct.Argon2_instance_t, %struct.Argon2_instance_t* %14, i32 0, i32 5
  %15 = load i32, i32* %segment_length, align 4
  %mul6 = mul i32 8, %15
  call void @sodium_memzero(i8* %13, i32 %mul6)
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %entry
  ret void
}

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @free_memory(%struct.block_region_* %region) #0 {
entry:
  %region.addr = alloca %struct.block_region_*, align 4
  store %struct.block_region_* %region, %struct.block_region_** %region.addr, align 4
  %0 = load %struct.block_region_*, %struct.block_region_** %region.addr, align 4
  %tobool = icmp ne %struct.block_region_* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.block_region_*, %struct.block_region_** %region.addr, align 4
  %base = getelementptr inbounds %struct.block_region_, %struct.block_region_* %1, i32 0, i32 0
  %2 = load i8*, i8** %base, align 4
  %tobool1 = icmp ne i8* %2, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.block_region_*, %struct.block_region_** %region.addr, align 4
  %base2 = getelementptr inbounds %struct.block_region_, %struct.block_region_* %3, i32 0, i32 0
  %4 = load i8*, i8** %base2, align 4
  call void @free(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %5 = load %struct.block_region_*, %struct.block_region_** %region.addr, align 4
  %6 = bitcast %struct.block_region_* %5 to i8*
  call void @free(i8* %6)
  ret void
}

declare void @argon2_fill_segment_ref(%struct.Argon2_instance_t*, %struct.Argon2_position_t* byval(%struct.Argon2_position_t) align 4) #1

declare i32 @posix_memalign(i8**, i32, i32) #1

declare i32* @__errno_location() #1

declare i32 @crypto_generichash_blake2b_init(%struct.crypto_generichash_blake2b_state*, i8*, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

declare i32 @crypto_generichash_blake2b_update(%struct.crypto_generichash_blake2b_state*, i8*, i64) #1

declare i32 @crypto_generichash_blake2b_final(%struct.crypto_generichash_blake2b_state*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @load_block(%struct.block_* %dst, i8* %input) #0 {
entry:
  %dst.addr = alloca %struct.block_*, align 4
  %input.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.block_* %dst, %struct.block_** %dst.addr, align 4
  store i8* %input, i8** %input.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 128
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %input.addr, align 4
  %2 = load i32, i32* %i, align 4
  %mul = mul i32 %2, 8
  %add.ptr = getelementptr i8, i8* %1, i32 %mul
  %call = call i64 @load64_le(i8* %add.ptr)
  %3 = load %struct.block_*, %struct.block_** %dst.addr, align 4
  %v = getelementptr inbounds %struct.block_, %struct.block_* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [128 x i64], [128 x i64]* %v, i32 0, i32 %4
  store i64 %call, i64* %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i64* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 1 %1, i32 8, i1 false)
  %2 = load i64, i64* %w, align 8
  ret i64 %2
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
