; ModuleID = 'crypto_verify/sodium/verify.c'
source_filename = "crypto_verify/sodium/verify.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_16_bytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_32_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_64_bytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_16(i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %x.addr, align 4
  %1 = load i8*, i8** %y.addr, align 4
  %call = call i32 @crypto_verify_n(i8* %0, i8* %1, i32 16)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_verify_n(i8* %x_, i8* %y_, i32 %n) #0 {
entry:
  %x_.addr = alloca i8*, align 4
  %y_.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %x = alloca i8*, align 4
  %y = alloca i8*, align 4
  %d = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %x_, i8** %x_.addr, align 4
  store i8* %y_, i8** %y_.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load i8*, i8** %x_.addr, align 4
  store volatile i8* %0, i8** %x, align 4
  %1 = load i8*, i8** %y_.addr, align 4
  store volatile i8* %1, i8** %y, align 4
  store volatile i32 0, i32* %d, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load volatile i8*, i8** %x, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %4, i32 %5
  %6 = load volatile i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %7 = load volatile i8*, i8** %y, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %7, i32 %8
  %9 = load volatile i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %9 to i32
  %xor = xor i32 %conv, %conv2
  %10 = load volatile i32, i32* %d, align 4
  %or = or i32 %10, %xor
  store volatile i32 %or, i32* %d, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load volatile i32, i32* %d, align 4
  %sub = sub i32 %12, 1
  %shr = lshr i32 %sub, 8
  %and = and i32 1, %shr
  %sub3 = sub i32 %and, 1
  ret i32 %sub3
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_32(i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %x.addr, align 4
  %1 = load i8*, i8** %y.addr, align 4
  %call = call i32 @crypto_verify_n(i8* %0, i8* %1, i32 32)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_verify_64(i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %x.addr, align 4
  %1 = load i8*, i8** %y.addr, align 4
  %call = call i32 @crypto_verify_n(i8* %0, i8* %1, i32 64)
  ret i32 %call
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
