; ModuleID = 'crypto_kdf/blake2b/kdf_blake2b.c'
source_filename = "crypto_kdf/blake2b/kdf_blake2b.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_blake2b_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_blake2b_bytes_max() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_blake2b_contextbytes() #0 {
entry:
  ret i32 8
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_blake2b_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_blake2b_derive_from_key(i8* nonnull %subkey, i32 %subkey_len, i64 %subkey_id, i8* nonnull %ctx, i8* nonnull %key) #0 {
entry:
  %retval = alloca i32, align 4
  %subkey.addr = alloca i8*, align 4
  %subkey_len.addr = alloca i32, align 4
  %subkey_id.addr = alloca i64, align 8
  %ctx.addr = alloca i8*, align 4
  %key.addr = alloca i8*, align 4
  %ctx_padded = alloca [16 x i8], align 16
  %salt = alloca [16 x i8], align 16
  store i8* %subkey, i8** %subkey.addr, align 4
  store i32 %subkey_len, i32* %subkey_len.addr, align 4
  store i64 %subkey_id, i64* %subkey_id.addr, align 8
  store i8* %ctx, i8** %ctx.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %ctx_padded, i32 0, i32 0
  %0 = load i8*, i8** %ctx.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay, i8* align 1 %0, i32 8, i1 false)
  %arraydecay1 = getelementptr inbounds [16 x i8], [16 x i8]* %ctx_padded, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay1, i32 8
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr, i8 0, i32 8, i1 false)
  %arraydecay2 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  %1 = load i64, i64* %subkey_id.addr, align 8
  call void @store64_le(i8* %arraydecay2, i64 %1)
  %arraydecay3 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  %add.ptr4 = getelementptr i8, i8* %arraydecay3, i32 8
  call void @llvm.memset.p0i8.i32(i8* align 1 %add.ptr4, i8 0, i32 8, i1 false)
  %2 = load i32, i32* %subkey_len.addr, align 4
  %cmp = icmp ult i32 %2, 16
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i32, i32* %subkey_len.addr, align 4
  %cmp5 = icmp ugt i32 %3, 64
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %call = call i32* @__errno_location()
  store i32 28, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %4 = load i8*, i8** %subkey.addr, align 4
  %5 = load i32, i32* %subkey_len.addr, align 4
  %6 = load i8*, i8** %key.addr, align 4
  %arraydecay6 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [16 x i8], [16 x i8]* %ctx_padded, i32 0, i32 0
  %call8 = call i32 @crypto_generichash_blake2b_salt_personal(i8* %4, i32 %5, i8* null, i64 0, i8* %6, i32 32, i8* %arraydecay6, i8* %arraydecay7)
  store i32 %call8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

declare i32* @__errno_location() #3

declare i32 @crypto_generichash_blake2b_salt_personal(i8*, i32, i8*, i64, i8*, i32, i8*, i8*) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
