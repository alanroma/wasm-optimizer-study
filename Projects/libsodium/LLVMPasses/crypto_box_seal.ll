; ModuleID = 'crypto_box/crypto_box_seal.c'
source_filename = "crypto_box/crypto_box_seal.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_generichash_blake2b_state = type { [384 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_seal(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %pk) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  %nonce = alloca [24 x i8], align 16
  %epk = alloca [32 x i8], align 16
  %esk = alloca [32 x i8], align 16
  %ret = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %epk, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %esk, i32 0, i32 0
  %call = call i32 @crypto_box_keypair(i8* %arraydecay, i8* %arraydecay1)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay2 = getelementptr inbounds [24 x i8], [24 x i8]* %nonce, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [32 x i8], [32 x i8]* %epk, i32 0, i32 0
  %0 = load i8*, i8** %pk.addr, align 4
  %call4 = call i32 @_crypto_box_seal_nonce(i8* %arraydecay2, i8* %arraydecay3, i8* %0)
  %1 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 32
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %arraydecay5 = getelementptr inbounds [24 x i8], [24 x i8]* %nonce, i32 0, i32 0
  %4 = load i8*, i8** %pk.addr, align 4
  %arraydecay6 = getelementptr inbounds [32 x i8], [32 x i8]* %esk, i32 0, i32 0
  %call7 = call i32 @crypto_box_easy(i8* %add.ptr, i8* %2, i64 %3, i8* %arraydecay5, i8* %4, i8* %arraydecay6)
  store i32 %call7, i32* %ret, align 4
  %5 = load i8*, i8** %c.addr, align 4
  %arraydecay8 = getelementptr inbounds [32 x i8], [32 x i8]* %epk, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 16 %arraydecay8, i32 32, i1 false)
  %arraydecay9 = getelementptr inbounds [32 x i8], [32 x i8]* %esk, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay9, i32 32)
  %arraydecay10 = getelementptr inbounds [32 x i8], [32 x i8]* %epk, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay10, i32 32)
  %arraydecay11 = getelementptr inbounds [24 x i8], [24 x i8]* %nonce, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay11, i32 24)
  %6 = load i32, i32* %ret, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

declare i32 @crypto_box_keypair(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @_crypto_box_seal_nonce(i8* %nonce, i8* %pk1, i8* %pk2) #0 {
entry:
  %nonce.addr = alloca i8*, align 4
  %pk1.addr = alloca i8*, align 4
  %pk2.addr = alloca i8*, align 4
  %st = alloca %struct.crypto_generichash_blake2b_state, align 64
  store i8* %nonce, i8** %nonce.addr, align 4
  store i8* %pk1, i8** %pk1.addr, align 4
  store i8* %pk2, i8** %pk2.addr, align 4
  %call = call i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state* %st, i8* null, i32 0, i32 24)
  %0 = load i8*, i8** %pk1.addr, align 4
  %call1 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %st, i8* %0, i64 32)
  %1 = load i8*, i8** %pk2.addr, align 4
  %call2 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %st, i8* %1, i64 32)
  %2 = load i8*, i8** %nonce.addr, align 4
  %call3 = call i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state* %st, i8* %2, i32 24)
  ret i32 0
}

declare i32 @crypto_box_easy(i8*, i8*, i64, i8*, i8*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_seal_open(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %nonce = alloca [24 x i8], align 16
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ult i64 %0, 48
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [24 x i8], [24 x i8]* %nonce, i32 0, i32 0
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @_crypto_box_seal_nonce(i8* %arraydecay, i8* %1, i8* %2)
  %3 = load i8*, i8** %m.addr, align 4
  %4 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %4, i32 32
  %5 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %5, 32
  %arraydecay1 = getelementptr inbounds [24 x i8], [24 x i8]* %nonce, i32 0, i32 0
  %6 = load i8*, i8** %c.addr, align 4
  %7 = load i8*, i8** %sk.addr, align 4
  %call2 = call i32 @crypto_box_open_easy(i8* %3, i8* %add.ptr, i64 %sub, i8* %arraydecay1, i8* %6, i8* %7)
  store i32 %call2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @crypto_box_open_easy(i8*, i8*, i64, i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_box_sealbytes() #0 {
entry:
  ret i32 48
}

declare i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state*, i8*, i32, i32) #1

declare i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state*, i8*, i64) #1

declare i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state*, i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
