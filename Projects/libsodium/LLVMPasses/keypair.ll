; ModuleID = 'crypto_sign/ed25519/ref10/keypair.c'
source_filename = "crypto_sign/ed25519/ref10/keypair.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_seed_keypair(i8* nonnull %pk, i8* nonnull %sk, i8* nonnull %seed) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %seed.addr = alloca i8*, align 4
  %A = alloca %struct.ge25519_p3, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  store i8* %seed, i8** %seed.addr, align 4
  %0 = load i8*, i8** %sk.addr, align 4
  %1 = load i8*, i8** %seed.addr, align 4
  %call = call i32 @crypto_hash_sha512(i8* %0, i8* %1, i64 32)
  %2 = load i8*, i8** %sk.addr, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 0
  %3 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %3 to i32
  %and = and i32 %conv, 248
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1
  %4 = load i8*, i8** %sk.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %4, i32 31
  %5 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %5 to i32
  %and4 = and i32 %conv3, 127
  %conv5 = trunc i32 %and4 to i8
  store i8 %conv5, i8* %arrayidx2, align 1
  %6 = load i8*, i8** %sk.addr, align 4
  %arrayidx6 = getelementptr i8, i8* %6, i32 31
  %7 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %7 to i32
  %or = or i32 %conv7, 64
  %conv8 = trunc i32 %or to i8
  store i8 %conv8, i8* %arrayidx6, align 1
  %8 = load i8*, i8** %sk.addr, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %A, i8* %8)
  %9 = load i8*, i8** %pk.addr, align 4
  call void @ge25519_p3_tobytes(i8* %9, %struct.ge25519_p3* %A)
  %10 = load i8*, i8** %sk.addr, align 4
  %11 = load i8*, i8** %seed.addr, align 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %10, i8* align 1 %11, i32 32, i1 false)
  %12 = load i8*, i8** %sk.addr, align 4
  %add.ptr = getelementptr i8, i8* %12, i32 32
  %13 = load i8*, i8** %pk.addr, align 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %13, i32 32, i1 false)
  ret i32 0
}

declare i32 @crypto_hash_sha512(i8*, i8*, i64) #1

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

declare void @ge25519_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_keypair(i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %seed = alloca [32 x i8], align 16
  %ret = alloca i32, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %seed, i32 0, i32 0
  call void @randombytes_buf(i8* %arraydecay, i32 32)
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %seed, i32 0, i32 0
  %call = call i32 @crypto_sign_ed25519_seed_keypair(i8* %0, i8* %1, i8* %arraydecay1)
  store i32 %call, i32* %ret, align 4
  %arraydecay2 = getelementptr inbounds [32 x i8], [32 x i8]* %seed, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay2, i32 32)
  %2 = load i32, i32* %ret, align 4
  ret i32 %2
}

declare void @randombytes_buf(i8*, i32) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_pk_to_curve25519(i8* nonnull %curve25519_pk, i8* nonnull %ed25519_pk) #0 {
entry:
  %retval = alloca i32, align 4
  %curve25519_pk.addr = alloca i8*, align 4
  %ed25519_pk.addr = alloca i8*, align 4
  %A = alloca %struct.ge25519_p3, align 4
  %x = alloca [10 x i32], align 16
  %one_minus_y = alloca [10 x i32], align 16
  store i8* %curve25519_pk, i8** %curve25519_pk.addr, align 4
  store i8* %ed25519_pk, i8** %ed25519_pk.addr, align 4
  %0 = load i8*, i8** %ed25519_pk.addr, align 4
  %call = call i32 @ge25519_has_small_order(i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %ed25519_pk.addr, align 4
  %call1 = call i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3* %A, i8* %1)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %call4 = call i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3* %A)
  %cmp5 = icmp eq i32 %call4, 0
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false3, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false3
  %arraydecay = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  call void @fe25519_1(i32* %arraydecay)
  %arraydecay6 = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  %Y = getelementptr inbounds %struct.ge25519_p3, %struct.ge25519_p3* %A, i32 0, i32 1
  %arraydecay8 = getelementptr inbounds [10 x i32], [10 x i32]* %Y, i32 0, i32 0
  call void @fe25519_sub(i32* %arraydecay6, i32* %arraydecay7, i32* %arraydecay8)
  %arraydecay9 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  call void @fe25519_1(i32* %arraydecay9)
  %arraydecay10 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  %Y12 = getelementptr inbounds %struct.ge25519_p3, %struct.ge25519_p3* %A, i32 0, i32 1
  %arraydecay13 = getelementptr inbounds [10 x i32], [10 x i32]* %Y12, i32 0, i32 0
  call void @fe25519_add(i32* %arraydecay10, i32* %arraydecay11, i32* %arraydecay13)
  %arraydecay14 = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  %arraydecay15 = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  call void @fe25519_invert(i32* %arraydecay14, i32* %arraydecay15)
  %arraydecay16 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  %arraydecay17 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [10 x i32], [10 x i32]* %one_minus_y, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay16, i32* %arraydecay17, i32* %arraydecay18)
  %2 = load i8*, i8** %curve25519_pk.addr, align 4
  %arraydecay19 = getelementptr inbounds [10 x i32], [10 x i32]* %x, i32 0, i32 0
  call void @fe25519_tobytes(i8* %2, i32* %arraydecay19)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare i32 @ge25519_has_small_order(i8*) #1

declare i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3*, i8*) #1

declare i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_1(i32* %h) #0 {
entry:
  %h.addr = alloca i32*, align 4
  store i32* %h, i32** %h.addr, align 4
  %0 = load i32*, i32** %h.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  store i32 1, i32* %arrayidx, align 4
  %1 = load i32*, i32** %h.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %1, i32 1
  store i32 0, i32* %arrayidx1, align 4
  %2 = load i32*, i32** %h.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %2, i32 2
  %3 = bitcast i32* %arrayidx2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_sub(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %h5 = alloca i32, align 4
  %h6 = alloca i32, align 4
  %h7 = alloca i32, align 4
  %h8 = alloca i32, align 4
  %h9 = alloca i32, align 4
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %2 = load i32*, i32** %g.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 0
  %3 = load i32, i32* %arrayidx1, align 4
  %sub = sub i32 %1, %3
  store i32 %sub, i32* %h0, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 1
  %5 = load i32, i32* %arrayidx2, align 4
  %6 = load i32*, i32** %g.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 1
  %7 = load i32, i32* %arrayidx3, align 4
  %sub4 = sub i32 %5, %7
  store i32 %sub4, i32* %h1, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %8, i32 2
  %9 = load i32, i32* %arrayidx5, align 4
  %10 = load i32*, i32** %g.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %10, i32 2
  %11 = load i32, i32* %arrayidx6, align 4
  %sub7 = sub i32 %9, %11
  store i32 %sub7, i32* %h2, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %12, i32 3
  %13 = load i32, i32* %arrayidx8, align 4
  %14 = load i32*, i32** %g.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %14, i32 3
  %15 = load i32, i32* %arrayidx9, align 4
  %sub10 = sub i32 %13, %15
  store i32 %sub10, i32* %h3, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %16, i32 4
  %17 = load i32, i32* %arrayidx11, align 4
  %18 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %18, i32 4
  %19 = load i32, i32* %arrayidx12, align 4
  %sub13 = sub i32 %17, %19
  store i32 %sub13, i32* %h4, align 4
  %20 = load i32*, i32** %f.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %20, i32 5
  %21 = load i32, i32* %arrayidx14, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %22, i32 5
  %23 = load i32, i32* %arrayidx15, align 4
  %sub16 = sub i32 %21, %23
  store i32 %sub16, i32* %h5, align 4
  %24 = load i32*, i32** %f.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %24, i32 6
  %25 = load i32, i32* %arrayidx17, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %26, i32 6
  %27 = load i32, i32* %arrayidx18, align 4
  %sub19 = sub i32 %25, %27
  store i32 %sub19, i32* %h6, align 4
  %28 = load i32*, i32** %f.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %28, i32 7
  %29 = load i32, i32* %arrayidx20, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %30, i32 7
  %31 = load i32, i32* %arrayidx21, align 4
  %sub22 = sub i32 %29, %31
  store i32 %sub22, i32* %h7, align 4
  %32 = load i32*, i32** %f.addr, align 4
  %arrayidx23 = getelementptr i32, i32* %32, i32 8
  %33 = load i32, i32* %arrayidx23, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx24 = getelementptr i32, i32* %34, i32 8
  %35 = load i32, i32* %arrayidx24, align 4
  %sub25 = sub i32 %33, %35
  store i32 %sub25, i32* %h8, align 4
  %36 = load i32*, i32** %f.addr, align 4
  %arrayidx26 = getelementptr i32, i32* %36, i32 9
  %37 = load i32, i32* %arrayidx26, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx27 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx27, align 4
  %sub28 = sub i32 %37, %39
  store i32 %sub28, i32* %h9, align 4
  %40 = load i32, i32* %h0, align 4
  %41 = load i32*, i32** %h.addr, align 4
  %arrayidx29 = getelementptr i32, i32* %41, i32 0
  store i32 %40, i32* %arrayidx29, align 4
  %42 = load i32, i32* %h1, align 4
  %43 = load i32*, i32** %h.addr, align 4
  %arrayidx30 = getelementptr i32, i32* %43, i32 1
  store i32 %42, i32* %arrayidx30, align 4
  %44 = load i32, i32* %h2, align 4
  %45 = load i32*, i32** %h.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %45, i32 2
  store i32 %44, i32* %arrayidx31, align 4
  %46 = load i32, i32* %h3, align 4
  %47 = load i32*, i32** %h.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %47, i32 3
  store i32 %46, i32* %arrayidx32, align 4
  %48 = load i32, i32* %h4, align 4
  %49 = load i32*, i32** %h.addr, align 4
  %arrayidx33 = getelementptr i32, i32* %49, i32 4
  store i32 %48, i32* %arrayidx33, align 4
  %50 = load i32, i32* %h5, align 4
  %51 = load i32*, i32** %h.addr, align 4
  %arrayidx34 = getelementptr i32, i32* %51, i32 5
  store i32 %50, i32* %arrayidx34, align 4
  %52 = load i32, i32* %h6, align 4
  %53 = load i32*, i32** %h.addr, align 4
  %arrayidx35 = getelementptr i32, i32* %53, i32 6
  store i32 %52, i32* %arrayidx35, align 4
  %54 = load i32, i32* %h7, align 4
  %55 = load i32*, i32** %h.addr, align 4
  %arrayidx36 = getelementptr i32, i32* %55, i32 7
  store i32 %54, i32* %arrayidx36, align 4
  %56 = load i32, i32* %h8, align 4
  %57 = load i32*, i32** %h.addr, align 4
  %arrayidx37 = getelementptr i32, i32* %57, i32 8
  store i32 %56, i32* %arrayidx37, align 4
  %58 = load i32, i32* %h9, align 4
  %59 = load i32*, i32** %h.addr, align 4
  %arrayidx38 = getelementptr i32, i32* %59, i32 9
  store i32 %58, i32* %arrayidx38, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_add(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %h5 = alloca i32, align 4
  %h6 = alloca i32, align 4
  %h7 = alloca i32, align 4
  %h8 = alloca i32, align 4
  %h9 = alloca i32, align 4
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %2 = load i32*, i32** %g.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 0
  %3 = load i32, i32* %arrayidx1, align 4
  %add = add i32 %1, %3
  store i32 %add, i32* %h0, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 1
  %5 = load i32, i32* %arrayidx2, align 4
  %6 = load i32*, i32** %g.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 1
  %7 = load i32, i32* %arrayidx3, align 4
  %add4 = add i32 %5, %7
  store i32 %add4, i32* %h1, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %8, i32 2
  %9 = load i32, i32* %arrayidx5, align 4
  %10 = load i32*, i32** %g.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %10, i32 2
  %11 = load i32, i32* %arrayidx6, align 4
  %add7 = add i32 %9, %11
  store i32 %add7, i32* %h2, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %12, i32 3
  %13 = load i32, i32* %arrayidx8, align 4
  %14 = load i32*, i32** %g.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %14, i32 3
  %15 = load i32, i32* %arrayidx9, align 4
  %add10 = add i32 %13, %15
  store i32 %add10, i32* %h3, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %16, i32 4
  %17 = load i32, i32* %arrayidx11, align 4
  %18 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %18, i32 4
  %19 = load i32, i32* %arrayidx12, align 4
  %add13 = add i32 %17, %19
  store i32 %add13, i32* %h4, align 4
  %20 = load i32*, i32** %f.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %20, i32 5
  %21 = load i32, i32* %arrayidx14, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %22, i32 5
  %23 = load i32, i32* %arrayidx15, align 4
  %add16 = add i32 %21, %23
  store i32 %add16, i32* %h5, align 4
  %24 = load i32*, i32** %f.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %24, i32 6
  %25 = load i32, i32* %arrayidx17, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %26, i32 6
  %27 = load i32, i32* %arrayidx18, align 4
  %add19 = add i32 %25, %27
  store i32 %add19, i32* %h6, align 4
  %28 = load i32*, i32** %f.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %28, i32 7
  %29 = load i32, i32* %arrayidx20, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %30, i32 7
  %31 = load i32, i32* %arrayidx21, align 4
  %add22 = add i32 %29, %31
  store i32 %add22, i32* %h7, align 4
  %32 = load i32*, i32** %f.addr, align 4
  %arrayidx23 = getelementptr i32, i32* %32, i32 8
  %33 = load i32, i32* %arrayidx23, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx24 = getelementptr i32, i32* %34, i32 8
  %35 = load i32, i32* %arrayidx24, align 4
  %add25 = add i32 %33, %35
  store i32 %add25, i32* %h8, align 4
  %36 = load i32*, i32** %f.addr, align 4
  %arrayidx26 = getelementptr i32, i32* %36, i32 9
  %37 = load i32, i32* %arrayidx26, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx27 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx27, align 4
  %add28 = add i32 %37, %39
  store i32 %add28, i32* %h9, align 4
  %40 = load i32, i32* %h0, align 4
  %41 = load i32*, i32** %h.addr, align 4
  %arrayidx29 = getelementptr i32, i32* %41, i32 0
  store i32 %40, i32* %arrayidx29, align 4
  %42 = load i32, i32* %h1, align 4
  %43 = load i32*, i32** %h.addr, align 4
  %arrayidx30 = getelementptr i32, i32* %43, i32 1
  store i32 %42, i32* %arrayidx30, align 4
  %44 = load i32, i32* %h2, align 4
  %45 = load i32*, i32** %h.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %45, i32 2
  store i32 %44, i32* %arrayidx31, align 4
  %46 = load i32, i32* %h3, align 4
  %47 = load i32*, i32** %h.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %47, i32 3
  store i32 %46, i32* %arrayidx32, align 4
  %48 = load i32, i32* %h4, align 4
  %49 = load i32*, i32** %h.addr, align 4
  %arrayidx33 = getelementptr i32, i32* %49, i32 4
  store i32 %48, i32* %arrayidx33, align 4
  %50 = load i32, i32* %h5, align 4
  %51 = load i32*, i32** %h.addr, align 4
  %arrayidx34 = getelementptr i32, i32* %51, i32 5
  store i32 %50, i32* %arrayidx34, align 4
  %52 = load i32, i32* %h6, align 4
  %53 = load i32*, i32** %h.addr, align 4
  %arrayidx35 = getelementptr i32, i32* %53, i32 6
  store i32 %52, i32* %arrayidx35, align 4
  %54 = load i32, i32* %h7, align 4
  %55 = load i32*, i32** %h.addr, align 4
  %arrayidx36 = getelementptr i32, i32* %55, i32 7
  store i32 %54, i32* %arrayidx36, align 4
  %56 = load i32, i32* %h8, align 4
  %57 = load i32*, i32** %h.addr, align 4
  %arrayidx37 = getelementptr i32, i32* %57, i32 8
  store i32 %56, i32* %arrayidx37, align 4
  %58 = load i32, i32* %h9, align 4
  %59 = load i32*, i32** %h.addr, align 4
  %arrayidx38 = getelementptr i32, i32* %59, i32 9
  store i32 %58, i32* %arrayidx38, align 4
  ret void
}

declare void @fe25519_invert(i32*, i32*) #1

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_mul(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  %g0 = alloca i32, align 4
  %g1 = alloca i32, align 4
  %g2 = alloca i32, align 4
  %g3 = alloca i32, align 4
  %g4 = alloca i32, align 4
  %g5 = alloca i32, align 4
  %g6 = alloca i32, align 4
  %g7 = alloca i32, align 4
  %g8 = alloca i32, align 4
  %g9 = alloca i32, align 4
  %g1_19 = alloca i32, align 4
  %g2_19 = alloca i32, align 4
  %g3_19 = alloca i32, align 4
  %g4_19 = alloca i32, align 4
  %g5_19 = alloca i32, align 4
  %g6_19 = alloca i32, align 4
  %g7_19 = alloca i32, align 4
  %g8_19 = alloca i32, align 4
  %g9_19 = alloca i32, align 4
  %f1_2 = alloca i32, align 4
  %f3_2 = alloca i32, align 4
  %f5_2 = alloca i32, align 4
  %f7_2 = alloca i32, align 4
  %f9_2 = alloca i32, align 4
  %f0g0 = alloca i64, align 8
  %f0g1 = alloca i64, align 8
  %f0g2 = alloca i64, align 8
  %f0g3 = alloca i64, align 8
  %f0g4 = alloca i64, align 8
  %f0g5 = alloca i64, align 8
  %f0g6 = alloca i64, align 8
  %f0g7 = alloca i64, align 8
  %f0g8 = alloca i64, align 8
  %f0g9 = alloca i64, align 8
  %f1g0 = alloca i64, align 8
  %f1g1_2 = alloca i64, align 8
  %f1g2 = alloca i64, align 8
  %f1g3_2 = alloca i64, align 8
  %f1g4 = alloca i64, align 8
  %f1g5_2 = alloca i64, align 8
  %f1g6 = alloca i64, align 8
  %f1g7_2 = alloca i64, align 8
  %f1g8 = alloca i64, align 8
  %f1g9_38 = alloca i64, align 8
  %f2g0 = alloca i64, align 8
  %f2g1 = alloca i64, align 8
  %f2g2 = alloca i64, align 8
  %f2g3 = alloca i64, align 8
  %f2g4 = alloca i64, align 8
  %f2g5 = alloca i64, align 8
  %f2g6 = alloca i64, align 8
  %f2g7 = alloca i64, align 8
  %f2g8_19 = alloca i64, align 8
  %f2g9_19 = alloca i64, align 8
  %f3g0 = alloca i64, align 8
  %f3g1_2 = alloca i64, align 8
  %f3g2 = alloca i64, align 8
  %f3g3_2 = alloca i64, align 8
  %f3g4 = alloca i64, align 8
  %f3g5_2 = alloca i64, align 8
  %f3g6 = alloca i64, align 8
  %f3g7_38 = alloca i64, align 8
  %f3g8_19 = alloca i64, align 8
  %f3g9_38 = alloca i64, align 8
  %f4g0 = alloca i64, align 8
  %f4g1 = alloca i64, align 8
  %f4g2 = alloca i64, align 8
  %f4g3 = alloca i64, align 8
  %f4g4 = alloca i64, align 8
  %f4g5 = alloca i64, align 8
  %f4g6_19 = alloca i64, align 8
  %f4g7_19 = alloca i64, align 8
  %f4g8_19 = alloca i64, align 8
  %f4g9_19 = alloca i64, align 8
  %f5g0 = alloca i64, align 8
  %f5g1_2 = alloca i64, align 8
  %f5g2 = alloca i64, align 8
  %f5g3_2 = alloca i64, align 8
  %f5g4 = alloca i64, align 8
  %f5g5_38 = alloca i64, align 8
  %f5g6_19 = alloca i64, align 8
  %f5g7_38 = alloca i64, align 8
  %f5g8_19 = alloca i64, align 8
  %f5g9_38 = alloca i64, align 8
  %f6g0 = alloca i64, align 8
  %f6g1 = alloca i64, align 8
  %f6g2 = alloca i64, align 8
  %f6g3 = alloca i64, align 8
  %f6g4_19 = alloca i64, align 8
  %f6g5_19 = alloca i64, align 8
  %f6g6_19 = alloca i64, align 8
  %f6g7_19 = alloca i64, align 8
  %f6g8_19 = alloca i64, align 8
  %f6g9_19 = alloca i64, align 8
  %f7g0 = alloca i64, align 8
  %f7g1_2 = alloca i64, align 8
  %f7g2 = alloca i64, align 8
  %f7g3_38 = alloca i64, align 8
  %f7g4_19 = alloca i64, align 8
  %f7g5_38 = alloca i64, align 8
  %f7g6_19 = alloca i64, align 8
  %f7g7_38 = alloca i64, align 8
  %f7g8_19 = alloca i64, align 8
  %f7g9_38 = alloca i64, align 8
  %f8g0 = alloca i64, align 8
  %f8g1 = alloca i64, align 8
  %f8g2_19 = alloca i64, align 8
  %f8g3_19 = alloca i64, align 8
  %f8g4_19 = alloca i64, align 8
  %f8g5_19 = alloca i64, align 8
  %f8g6_19 = alloca i64, align 8
  %f8g7_19 = alloca i64, align 8
  %f8g8_19 = alloca i64, align 8
  %f8g9_19 = alloca i64, align 8
  %f9g0 = alloca i64, align 8
  %f9g1_38 = alloca i64, align 8
  %f9g2_19 = alloca i64, align 8
  %f9g3_38 = alloca i64, align 8
  %f9g4_19 = alloca i64, align 8
  %f9g5_38 = alloca i64, align 8
  %f9g6_19 = alloca i64, align 8
  %f9g7_38 = alloca i64, align 8
  %f9g8_19 = alloca i64, align 8
  %f9g9_38 = alloca i64, align 8
  %h0 = alloca i64, align 8
  %h1 = alloca i64, align 8
  %h2 = alloca i64, align 8
  %h3 = alloca i64, align 8
  %h4 = alloca i64, align 8
  %h5 = alloca i64, align 8
  %h6 = alloca i64, align 8
  %h7 = alloca i64, align 8
  %h8 = alloca i64, align 8
  %h9 = alloca i64, align 8
  %carry0 = alloca i64, align 8
  %carry1 = alloca i64, align 8
  %carry2 = alloca i64, align 8
  %carry3 = alloca i64, align 8
  %carry4 = alloca i64, align 8
  %carry5 = alloca i64, align 8
  %carry6 = alloca i64, align 8
  %carry7 = alloca i64, align 8
  %carry8 = alloca i64, align 8
  %carry9 = alloca i64, align 8
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %f0, align 4
  %2 = load i32*, i32** %f.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 1
  %3 = load i32, i32* %arrayidx1, align 4
  store i32 %3, i32* %f1, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 2
  %5 = load i32, i32* %arrayidx2, align 4
  store i32 %5, i32* %f2, align 4
  %6 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 3
  %7 = load i32, i32* %arrayidx3, align 4
  store i32 %7, i32* %f3, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %8, i32 4
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %f4, align 4
  %10 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %10, i32 5
  %11 = load i32, i32* %arrayidx5, align 4
  store i32 %11, i32* %f5, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %12, i32 6
  %13 = load i32, i32* %arrayidx6, align 4
  store i32 %13, i32* %f6, align 4
  %14 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %14, i32 7
  %15 = load i32, i32* %arrayidx7, align 4
  store i32 %15, i32* %f7, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %16, i32 8
  %17 = load i32, i32* %arrayidx8, align 4
  store i32 %17, i32* %f8, align 4
  %18 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %18, i32 9
  %19 = load i32, i32* %arrayidx9, align 4
  store i32 %19, i32* %f9, align 4
  %20 = load i32*, i32** %g.addr, align 4
  %arrayidx10 = getelementptr i32, i32* %20, i32 0
  %21 = load i32, i32* %arrayidx10, align 4
  store i32 %21, i32* %g0, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %22, i32 1
  %23 = load i32, i32* %arrayidx11, align 4
  store i32 %23, i32* %g1, align 4
  %24 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx12, align 4
  store i32 %25, i32* %g2, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx13 = getelementptr i32, i32* %26, i32 3
  %27 = load i32, i32* %arrayidx13, align 4
  store i32 %27, i32* %g3, align 4
  %28 = load i32*, i32** %g.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %28, i32 4
  %29 = load i32, i32* %arrayidx14, align 4
  store i32 %29, i32* %g4, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %30, i32 5
  %31 = load i32, i32* %arrayidx15, align 4
  store i32 %31, i32* %g5, align 4
  %32 = load i32*, i32** %g.addr, align 4
  %arrayidx16 = getelementptr i32, i32* %32, i32 6
  %33 = load i32, i32* %arrayidx16, align 4
  store i32 %33, i32* %g6, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %34, i32 7
  %35 = load i32, i32* %arrayidx17, align 4
  store i32 %35, i32* %g7, align 4
  %36 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %36, i32 8
  %37 = load i32, i32* %arrayidx18, align 4
  store i32 %37, i32* %g8, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx19 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx19, align 4
  store i32 %39, i32* %g9, align 4
  %40 = load i32, i32* %g1, align 4
  %mul = mul i32 19, %40
  store i32 %mul, i32* %g1_19, align 4
  %41 = load i32, i32* %g2, align 4
  %mul20 = mul i32 19, %41
  store i32 %mul20, i32* %g2_19, align 4
  %42 = load i32, i32* %g3, align 4
  %mul21 = mul i32 19, %42
  store i32 %mul21, i32* %g3_19, align 4
  %43 = load i32, i32* %g4, align 4
  %mul22 = mul i32 19, %43
  store i32 %mul22, i32* %g4_19, align 4
  %44 = load i32, i32* %g5, align 4
  %mul23 = mul i32 19, %44
  store i32 %mul23, i32* %g5_19, align 4
  %45 = load i32, i32* %g6, align 4
  %mul24 = mul i32 19, %45
  store i32 %mul24, i32* %g6_19, align 4
  %46 = load i32, i32* %g7, align 4
  %mul25 = mul i32 19, %46
  store i32 %mul25, i32* %g7_19, align 4
  %47 = load i32, i32* %g8, align 4
  %mul26 = mul i32 19, %47
  store i32 %mul26, i32* %g8_19, align 4
  %48 = load i32, i32* %g9, align 4
  %mul27 = mul i32 19, %48
  store i32 %mul27, i32* %g9_19, align 4
  %49 = load i32, i32* %f1, align 4
  %mul28 = mul i32 2, %49
  store i32 %mul28, i32* %f1_2, align 4
  %50 = load i32, i32* %f3, align 4
  %mul29 = mul i32 2, %50
  store i32 %mul29, i32* %f3_2, align 4
  %51 = load i32, i32* %f5, align 4
  %mul30 = mul i32 2, %51
  store i32 %mul30, i32* %f5_2, align 4
  %52 = load i32, i32* %f7, align 4
  %mul31 = mul i32 2, %52
  store i32 %mul31, i32* %f7_2, align 4
  %53 = load i32, i32* %f9, align 4
  %mul32 = mul i32 2, %53
  store i32 %mul32, i32* %f9_2, align 4
  %54 = load i32, i32* %f0, align 4
  %conv = sext i32 %54 to i64
  %55 = load i32, i32* %g0, align 4
  %conv33 = sext i32 %55 to i64
  %mul34 = mul i64 %conv, %conv33
  store i64 %mul34, i64* %f0g0, align 8
  %56 = load i32, i32* %f0, align 4
  %conv35 = sext i32 %56 to i64
  %57 = load i32, i32* %g1, align 4
  %conv36 = sext i32 %57 to i64
  %mul37 = mul i64 %conv35, %conv36
  store i64 %mul37, i64* %f0g1, align 8
  %58 = load i32, i32* %f0, align 4
  %conv38 = sext i32 %58 to i64
  %59 = load i32, i32* %g2, align 4
  %conv39 = sext i32 %59 to i64
  %mul40 = mul i64 %conv38, %conv39
  store i64 %mul40, i64* %f0g2, align 8
  %60 = load i32, i32* %f0, align 4
  %conv41 = sext i32 %60 to i64
  %61 = load i32, i32* %g3, align 4
  %conv42 = sext i32 %61 to i64
  %mul43 = mul i64 %conv41, %conv42
  store i64 %mul43, i64* %f0g3, align 8
  %62 = load i32, i32* %f0, align 4
  %conv44 = sext i32 %62 to i64
  %63 = load i32, i32* %g4, align 4
  %conv45 = sext i32 %63 to i64
  %mul46 = mul i64 %conv44, %conv45
  store i64 %mul46, i64* %f0g4, align 8
  %64 = load i32, i32* %f0, align 4
  %conv47 = sext i32 %64 to i64
  %65 = load i32, i32* %g5, align 4
  %conv48 = sext i32 %65 to i64
  %mul49 = mul i64 %conv47, %conv48
  store i64 %mul49, i64* %f0g5, align 8
  %66 = load i32, i32* %f0, align 4
  %conv50 = sext i32 %66 to i64
  %67 = load i32, i32* %g6, align 4
  %conv51 = sext i32 %67 to i64
  %mul52 = mul i64 %conv50, %conv51
  store i64 %mul52, i64* %f0g6, align 8
  %68 = load i32, i32* %f0, align 4
  %conv53 = sext i32 %68 to i64
  %69 = load i32, i32* %g7, align 4
  %conv54 = sext i32 %69 to i64
  %mul55 = mul i64 %conv53, %conv54
  store i64 %mul55, i64* %f0g7, align 8
  %70 = load i32, i32* %f0, align 4
  %conv56 = sext i32 %70 to i64
  %71 = load i32, i32* %g8, align 4
  %conv57 = sext i32 %71 to i64
  %mul58 = mul i64 %conv56, %conv57
  store i64 %mul58, i64* %f0g8, align 8
  %72 = load i32, i32* %f0, align 4
  %conv59 = sext i32 %72 to i64
  %73 = load i32, i32* %g9, align 4
  %conv60 = sext i32 %73 to i64
  %mul61 = mul i64 %conv59, %conv60
  store i64 %mul61, i64* %f0g9, align 8
  %74 = load i32, i32* %f1, align 4
  %conv62 = sext i32 %74 to i64
  %75 = load i32, i32* %g0, align 4
  %conv63 = sext i32 %75 to i64
  %mul64 = mul i64 %conv62, %conv63
  store i64 %mul64, i64* %f1g0, align 8
  %76 = load i32, i32* %f1_2, align 4
  %conv65 = sext i32 %76 to i64
  %77 = load i32, i32* %g1, align 4
  %conv66 = sext i32 %77 to i64
  %mul67 = mul i64 %conv65, %conv66
  store i64 %mul67, i64* %f1g1_2, align 8
  %78 = load i32, i32* %f1, align 4
  %conv68 = sext i32 %78 to i64
  %79 = load i32, i32* %g2, align 4
  %conv69 = sext i32 %79 to i64
  %mul70 = mul i64 %conv68, %conv69
  store i64 %mul70, i64* %f1g2, align 8
  %80 = load i32, i32* %f1_2, align 4
  %conv71 = sext i32 %80 to i64
  %81 = load i32, i32* %g3, align 4
  %conv72 = sext i32 %81 to i64
  %mul73 = mul i64 %conv71, %conv72
  store i64 %mul73, i64* %f1g3_2, align 8
  %82 = load i32, i32* %f1, align 4
  %conv74 = sext i32 %82 to i64
  %83 = load i32, i32* %g4, align 4
  %conv75 = sext i32 %83 to i64
  %mul76 = mul i64 %conv74, %conv75
  store i64 %mul76, i64* %f1g4, align 8
  %84 = load i32, i32* %f1_2, align 4
  %conv77 = sext i32 %84 to i64
  %85 = load i32, i32* %g5, align 4
  %conv78 = sext i32 %85 to i64
  %mul79 = mul i64 %conv77, %conv78
  store i64 %mul79, i64* %f1g5_2, align 8
  %86 = load i32, i32* %f1, align 4
  %conv80 = sext i32 %86 to i64
  %87 = load i32, i32* %g6, align 4
  %conv81 = sext i32 %87 to i64
  %mul82 = mul i64 %conv80, %conv81
  store i64 %mul82, i64* %f1g6, align 8
  %88 = load i32, i32* %f1_2, align 4
  %conv83 = sext i32 %88 to i64
  %89 = load i32, i32* %g7, align 4
  %conv84 = sext i32 %89 to i64
  %mul85 = mul i64 %conv83, %conv84
  store i64 %mul85, i64* %f1g7_2, align 8
  %90 = load i32, i32* %f1, align 4
  %conv86 = sext i32 %90 to i64
  %91 = load i32, i32* %g8, align 4
  %conv87 = sext i32 %91 to i64
  %mul88 = mul i64 %conv86, %conv87
  store i64 %mul88, i64* %f1g8, align 8
  %92 = load i32, i32* %f1_2, align 4
  %conv89 = sext i32 %92 to i64
  %93 = load i32, i32* %g9_19, align 4
  %conv90 = sext i32 %93 to i64
  %mul91 = mul i64 %conv89, %conv90
  store i64 %mul91, i64* %f1g9_38, align 8
  %94 = load i32, i32* %f2, align 4
  %conv92 = sext i32 %94 to i64
  %95 = load i32, i32* %g0, align 4
  %conv93 = sext i32 %95 to i64
  %mul94 = mul i64 %conv92, %conv93
  store i64 %mul94, i64* %f2g0, align 8
  %96 = load i32, i32* %f2, align 4
  %conv95 = sext i32 %96 to i64
  %97 = load i32, i32* %g1, align 4
  %conv96 = sext i32 %97 to i64
  %mul97 = mul i64 %conv95, %conv96
  store i64 %mul97, i64* %f2g1, align 8
  %98 = load i32, i32* %f2, align 4
  %conv98 = sext i32 %98 to i64
  %99 = load i32, i32* %g2, align 4
  %conv99 = sext i32 %99 to i64
  %mul100 = mul i64 %conv98, %conv99
  store i64 %mul100, i64* %f2g2, align 8
  %100 = load i32, i32* %f2, align 4
  %conv101 = sext i32 %100 to i64
  %101 = load i32, i32* %g3, align 4
  %conv102 = sext i32 %101 to i64
  %mul103 = mul i64 %conv101, %conv102
  store i64 %mul103, i64* %f2g3, align 8
  %102 = load i32, i32* %f2, align 4
  %conv104 = sext i32 %102 to i64
  %103 = load i32, i32* %g4, align 4
  %conv105 = sext i32 %103 to i64
  %mul106 = mul i64 %conv104, %conv105
  store i64 %mul106, i64* %f2g4, align 8
  %104 = load i32, i32* %f2, align 4
  %conv107 = sext i32 %104 to i64
  %105 = load i32, i32* %g5, align 4
  %conv108 = sext i32 %105 to i64
  %mul109 = mul i64 %conv107, %conv108
  store i64 %mul109, i64* %f2g5, align 8
  %106 = load i32, i32* %f2, align 4
  %conv110 = sext i32 %106 to i64
  %107 = load i32, i32* %g6, align 4
  %conv111 = sext i32 %107 to i64
  %mul112 = mul i64 %conv110, %conv111
  store i64 %mul112, i64* %f2g6, align 8
  %108 = load i32, i32* %f2, align 4
  %conv113 = sext i32 %108 to i64
  %109 = load i32, i32* %g7, align 4
  %conv114 = sext i32 %109 to i64
  %mul115 = mul i64 %conv113, %conv114
  store i64 %mul115, i64* %f2g7, align 8
  %110 = load i32, i32* %f2, align 4
  %conv116 = sext i32 %110 to i64
  %111 = load i32, i32* %g8_19, align 4
  %conv117 = sext i32 %111 to i64
  %mul118 = mul i64 %conv116, %conv117
  store i64 %mul118, i64* %f2g8_19, align 8
  %112 = load i32, i32* %f2, align 4
  %conv119 = sext i32 %112 to i64
  %113 = load i32, i32* %g9_19, align 4
  %conv120 = sext i32 %113 to i64
  %mul121 = mul i64 %conv119, %conv120
  store i64 %mul121, i64* %f2g9_19, align 8
  %114 = load i32, i32* %f3, align 4
  %conv122 = sext i32 %114 to i64
  %115 = load i32, i32* %g0, align 4
  %conv123 = sext i32 %115 to i64
  %mul124 = mul i64 %conv122, %conv123
  store i64 %mul124, i64* %f3g0, align 8
  %116 = load i32, i32* %f3_2, align 4
  %conv125 = sext i32 %116 to i64
  %117 = load i32, i32* %g1, align 4
  %conv126 = sext i32 %117 to i64
  %mul127 = mul i64 %conv125, %conv126
  store i64 %mul127, i64* %f3g1_2, align 8
  %118 = load i32, i32* %f3, align 4
  %conv128 = sext i32 %118 to i64
  %119 = load i32, i32* %g2, align 4
  %conv129 = sext i32 %119 to i64
  %mul130 = mul i64 %conv128, %conv129
  store i64 %mul130, i64* %f3g2, align 8
  %120 = load i32, i32* %f3_2, align 4
  %conv131 = sext i32 %120 to i64
  %121 = load i32, i32* %g3, align 4
  %conv132 = sext i32 %121 to i64
  %mul133 = mul i64 %conv131, %conv132
  store i64 %mul133, i64* %f3g3_2, align 8
  %122 = load i32, i32* %f3, align 4
  %conv134 = sext i32 %122 to i64
  %123 = load i32, i32* %g4, align 4
  %conv135 = sext i32 %123 to i64
  %mul136 = mul i64 %conv134, %conv135
  store i64 %mul136, i64* %f3g4, align 8
  %124 = load i32, i32* %f3_2, align 4
  %conv137 = sext i32 %124 to i64
  %125 = load i32, i32* %g5, align 4
  %conv138 = sext i32 %125 to i64
  %mul139 = mul i64 %conv137, %conv138
  store i64 %mul139, i64* %f3g5_2, align 8
  %126 = load i32, i32* %f3, align 4
  %conv140 = sext i32 %126 to i64
  %127 = load i32, i32* %g6, align 4
  %conv141 = sext i32 %127 to i64
  %mul142 = mul i64 %conv140, %conv141
  store i64 %mul142, i64* %f3g6, align 8
  %128 = load i32, i32* %f3_2, align 4
  %conv143 = sext i32 %128 to i64
  %129 = load i32, i32* %g7_19, align 4
  %conv144 = sext i32 %129 to i64
  %mul145 = mul i64 %conv143, %conv144
  store i64 %mul145, i64* %f3g7_38, align 8
  %130 = load i32, i32* %f3, align 4
  %conv146 = sext i32 %130 to i64
  %131 = load i32, i32* %g8_19, align 4
  %conv147 = sext i32 %131 to i64
  %mul148 = mul i64 %conv146, %conv147
  store i64 %mul148, i64* %f3g8_19, align 8
  %132 = load i32, i32* %f3_2, align 4
  %conv149 = sext i32 %132 to i64
  %133 = load i32, i32* %g9_19, align 4
  %conv150 = sext i32 %133 to i64
  %mul151 = mul i64 %conv149, %conv150
  store i64 %mul151, i64* %f3g9_38, align 8
  %134 = load i32, i32* %f4, align 4
  %conv152 = sext i32 %134 to i64
  %135 = load i32, i32* %g0, align 4
  %conv153 = sext i32 %135 to i64
  %mul154 = mul i64 %conv152, %conv153
  store i64 %mul154, i64* %f4g0, align 8
  %136 = load i32, i32* %f4, align 4
  %conv155 = sext i32 %136 to i64
  %137 = load i32, i32* %g1, align 4
  %conv156 = sext i32 %137 to i64
  %mul157 = mul i64 %conv155, %conv156
  store i64 %mul157, i64* %f4g1, align 8
  %138 = load i32, i32* %f4, align 4
  %conv158 = sext i32 %138 to i64
  %139 = load i32, i32* %g2, align 4
  %conv159 = sext i32 %139 to i64
  %mul160 = mul i64 %conv158, %conv159
  store i64 %mul160, i64* %f4g2, align 8
  %140 = load i32, i32* %f4, align 4
  %conv161 = sext i32 %140 to i64
  %141 = load i32, i32* %g3, align 4
  %conv162 = sext i32 %141 to i64
  %mul163 = mul i64 %conv161, %conv162
  store i64 %mul163, i64* %f4g3, align 8
  %142 = load i32, i32* %f4, align 4
  %conv164 = sext i32 %142 to i64
  %143 = load i32, i32* %g4, align 4
  %conv165 = sext i32 %143 to i64
  %mul166 = mul i64 %conv164, %conv165
  store i64 %mul166, i64* %f4g4, align 8
  %144 = load i32, i32* %f4, align 4
  %conv167 = sext i32 %144 to i64
  %145 = load i32, i32* %g5, align 4
  %conv168 = sext i32 %145 to i64
  %mul169 = mul i64 %conv167, %conv168
  store i64 %mul169, i64* %f4g5, align 8
  %146 = load i32, i32* %f4, align 4
  %conv170 = sext i32 %146 to i64
  %147 = load i32, i32* %g6_19, align 4
  %conv171 = sext i32 %147 to i64
  %mul172 = mul i64 %conv170, %conv171
  store i64 %mul172, i64* %f4g6_19, align 8
  %148 = load i32, i32* %f4, align 4
  %conv173 = sext i32 %148 to i64
  %149 = load i32, i32* %g7_19, align 4
  %conv174 = sext i32 %149 to i64
  %mul175 = mul i64 %conv173, %conv174
  store i64 %mul175, i64* %f4g7_19, align 8
  %150 = load i32, i32* %f4, align 4
  %conv176 = sext i32 %150 to i64
  %151 = load i32, i32* %g8_19, align 4
  %conv177 = sext i32 %151 to i64
  %mul178 = mul i64 %conv176, %conv177
  store i64 %mul178, i64* %f4g8_19, align 8
  %152 = load i32, i32* %f4, align 4
  %conv179 = sext i32 %152 to i64
  %153 = load i32, i32* %g9_19, align 4
  %conv180 = sext i32 %153 to i64
  %mul181 = mul i64 %conv179, %conv180
  store i64 %mul181, i64* %f4g9_19, align 8
  %154 = load i32, i32* %f5, align 4
  %conv182 = sext i32 %154 to i64
  %155 = load i32, i32* %g0, align 4
  %conv183 = sext i32 %155 to i64
  %mul184 = mul i64 %conv182, %conv183
  store i64 %mul184, i64* %f5g0, align 8
  %156 = load i32, i32* %f5_2, align 4
  %conv185 = sext i32 %156 to i64
  %157 = load i32, i32* %g1, align 4
  %conv186 = sext i32 %157 to i64
  %mul187 = mul i64 %conv185, %conv186
  store i64 %mul187, i64* %f5g1_2, align 8
  %158 = load i32, i32* %f5, align 4
  %conv188 = sext i32 %158 to i64
  %159 = load i32, i32* %g2, align 4
  %conv189 = sext i32 %159 to i64
  %mul190 = mul i64 %conv188, %conv189
  store i64 %mul190, i64* %f5g2, align 8
  %160 = load i32, i32* %f5_2, align 4
  %conv191 = sext i32 %160 to i64
  %161 = load i32, i32* %g3, align 4
  %conv192 = sext i32 %161 to i64
  %mul193 = mul i64 %conv191, %conv192
  store i64 %mul193, i64* %f5g3_2, align 8
  %162 = load i32, i32* %f5, align 4
  %conv194 = sext i32 %162 to i64
  %163 = load i32, i32* %g4, align 4
  %conv195 = sext i32 %163 to i64
  %mul196 = mul i64 %conv194, %conv195
  store i64 %mul196, i64* %f5g4, align 8
  %164 = load i32, i32* %f5_2, align 4
  %conv197 = sext i32 %164 to i64
  %165 = load i32, i32* %g5_19, align 4
  %conv198 = sext i32 %165 to i64
  %mul199 = mul i64 %conv197, %conv198
  store i64 %mul199, i64* %f5g5_38, align 8
  %166 = load i32, i32* %f5, align 4
  %conv200 = sext i32 %166 to i64
  %167 = load i32, i32* %g6_19, align 4
  %conv201 = sext i32 %167 to i64
  %mul202 = mul i64 %conv200, %conv201
  store i64 %mul202, i64* %f5g6_19, align 8
  %168 = load i32, i32* %f5_2, align 4
  %conv203 = sext i32 %168 to i64
  %169 = load i32, i32* %g7_19, align 4
  %conv204 = sext i32 %169 to i64
  %mul205 = mul i64 %conv203, %conv204
  store i64 %mul205, i64* %f5g7_38, align 8
  %170 = load i32, i32* %f5, align 4
  %conv206 = sext i32 %170 to i64
  %171 = load i32, i32* %g8_19, align 4
  %conv207 = sext i32 %171 to i64
  %mul208 = mul i64 %conv206, %conv207
  store i64 %mul208, i64* %f5g8_19, align 8
  %172 = load i32, i32* %f5_2, align 4
  %conv209 = sext i32 %172 to i64
  %173 = load i32, i32* %g9_19, align 4
  %conv210 = sext i32 %173 to i64
  %mul211 = mul i64 %conv209, %conv210
  store i64 %mul211, i64* %f5g9_38, align 8
  %174 = load i32, i32* %f6, align 4
  %conv212 = sext i32 %174 to i64
  %175 = load i32, i32* %g0, align 4
  %conv213 = sext i32 %175 to i64
  %mul214 = mul i64 %conv212, %conv213
  store i64 %mul214, i64* %f6g0, align 8
  %176 = load i32, i32* %f6, align 4
  %conv215 = sext i32 %176 to i64
  %177 = load i32, i32* %g1, align 4
  %conv216 = sext i32 %177 to i64
  %mul217 = mul i64 %conv215, %conv216
  store i64 %mul217, i64* %f6g1, align 8
  %178 = load i32, i32* %f6, align 4
  %conv218 = sext i32 %178 to i64
  %179 = load i32, i32* %g2, align 4
  %conv219 = sext i32 %179 to i64
  %mul220 = mul i64 %conv218, %conv219
  store i64 %mul220, i64* %f6g2, align 8
  %180 = load i32, i32* %f6, align 4
  %conv221 = sext i32 %180 to i64
  %181 = load i32, i32* %g3, align 4
  %conv222 = sext i32 %181 to i64
  %mul223 = mul i64 %conv221, %conv222
  store i64 %mul223, i64* %f6g3, align 8
  %182 = load i32, i32* %f6, align 4
  %conv224 = sext i32 %182 to i64
  %183 = load i32, i32* %g4_19, align 4
  %conv225 = sext i32 %183 to i64
  %mul226 = mul i64 %conv224, %conv225
  store i64 %mul226, i64* %f6g4_19, align 8
  %184 = load i32, i32* %f6, align 4
  %conv227 = sext i32 %184 to i64
  %185 = load i32, i32* %g5_19, align 4
  %conv228 = sext i32 %185 to i64
  %mul229 = mul i64 %conv227, %conv228
  store i64 %mul229, i64* %f6g5_19, align 8
  %186 = load i32, i32* %f6, align 4
  %conv230 = sext i32 %186 to i64
  %187 = load i32, i32* %g6_19, align 4
  %conv231 = sext i32 %187 to i64
  %mul232 = mul i64 %conv230, %conv231
  store i64 %mul232, i64* %f6g6_19, align 8
  %188 = load i32, i32* %f6, align 4
  %conv233 = sext i32 %188 to i64
  %189 = load i32, i32* %g7_19, align 4
  %conv234 = sext i32 %189 to i64
  %mul235 = mul i64 %conv233, %conv234
  store i64 %mul235, i64* %f6g7_19, align 8
  %190 = load i32, i32* %f6, align 4
  %conv236 = sext i32 %190 to i64
  %191 = load i32, i32* %g8_19, align 4
  %conv237 = sext i32 %191 to i64
  %mul238 = mul i64 %conv236, %conv237
  store i64 %mul238, i64* %f6g8_19, align 8
  %192 = load i32, i32* %f6, align 4
  %conv239 = sext i32 %192 to i64
  %193 = load i32, i32* %g9_19, align 4
  %conv240 = sext i32 %193 to i64
  %mul241 = mul i64 %conv239, %conv240
  store i64 %mul241, i64* %f6g9_19, align 8
  %194 = load i32, i32* %f7, align 4
  %conv242 = sext i32 %194 to i64
  %195 = load i32, i32* %g0, align 4
  %conv243 = sext i32 %195 to i64
  %mul244 = mul i64 %conv242, %conv243
  store i64 %mul244, i64* %f7g0, align 8
  %196 = load i32, i32* %f7_2, align 4
  %conv245 = sext i32 %196 to i64
  %197 = load i32, i32* %g1, align 4
  %conv246 = sext i32 %197 to i64
  %mul247 = mul i64 %conv245, %conv246
  store i64 %mul247, i64* %f7g1_2, align 8
  %198 = load i32, i32* %f7, align 4
  %conv248 = sext i32 %198 to i64
  %199 = load i32, i32* %g2, align 4
  %conv249 = sext i32 %199 to i64
  %mul250 = mul i64 %conv248, %conv249
  store i64 %mul250, i64* %f7g2, align 8
  %200 = load i32, i32* %f7_2, align 4
  %conv251 = sext i32 %200 to i64
  %201 = load i32, i32* %g3_19, align 4
  %conv252 = sext i32 %201 to i64
  %mul253 = mul i64 %conv251, %conv252
  store i64 %mul253, i64* %f7g3_38, align 8
  %202 = load i32, i32* %f7, align 4
  %conv254 = sext i32 %202 to i64
  %203 = load i32, i32* %g4_19, align 4
  %conv255 = sext i32 %203 to i64
  %mul256 = mul i64 %conv254, %conv255
  store i64 %mul256, i64* %f7g4_19, align 8
  %204 = load i32, i32* %f7_2, align 4
  %conv257 = sext i32 %204 to i64
  %205 = load i32, i32* %g5_19, align 4
  %conv258 = sext i32 %205 to i64
  %mul259 = mul i64 %conv257, %conv258
  store i64 %mul259, i64* %f7g5_38, align 8
  %206 = load i32, i32* %f7, align 4
  %conv260 = sext i32 %206 to i64
  %207 = load i32, i32* %g6_19, align 4
  %conv261 = sext i32 %207 to i64
  %mul262 = mul i64 %conv260, %conv261
  store i64 %mul262, i64* %f7g6_19, align 8
  %208 = load i32, i32* %f7_2, align 4
  %conv263 = sext i32 %208 to i64
  %209 = load i32, i32* %g7_19, align 4
  %conv264 = sext i32 %209 to i64
  %mul265 = mul i64 %conv263, %conv264
  store i64 %mul265, i64* %f7g7_38, align 8
  %210 = load i32, i32* %f7, align 4
  %conv266 = sext i32 %210 to i64
  %211 = load i32, i32* %g8_19, align 4
  %conv267 = sext i32 %211 to i64
  %mul268 = mul i64 %conv266, %conv267
  store i64 %mul268, i64* %f7g8_19, align 8
  %212 = load i32, i32* %f7_2, align 4
  %conv269 = sext i32 %212 to i64
  %213 = load i32, i32* %g9_19, align 4
  %conv270 = sext i32 %213 to i64
  %mul271 = mul i64 %conv269, %conv270
  store i64 %mul271, i64* %f7g9_38, align 8
  %214 = load i32, i32* %f8, align 4
  %conv272 = sext i32 %214 to i64
  %215 = load i32, i32* %g0, align 4
  %conv273 = sext i32 %215 to i64
  %mul274 = mul i64 %conv272, %conv273
  store i64 %mul274, i64* %f8g0, align 8
  %216 = load i32, i32* %f8, align 4
  %conv275 = sext i32 %216 to i64
  %217 = load i32, i32* %g1, align 4
  %conv276 = sext i32 %217 to i64
  %mul277 = mul i64 %conv275, %conv276
  store i64 %mul277, i64* %f8g1, align 8
  %218 = load i32, i32* %f8, align 4
  %conv278 = sext i32 %218 to i64
  %219 = load i32, i32* %g2_19, align 4
  %conv279 = sext i32 %219 to i64
  %mul280 = mul i64 %conv278, %conv279
  store i64 %mul280, i64* %f8g2_19, align 8
  %220 = load i32, i32* %f8, align 4
  %conv281 = sext i32 %220 to i64
  %221 = load i32, i32* %g3_19, align 4
  %conv282 = sext i32 %221 to i64
  %mul283 = mul i64 %conv281, %conv282
  store i64 %mul283, i64* %f8g3_19, align 8
  %222 = load i32, i32* %f8, align 4
  %conv284 = sext i32 %222 to i64
  %223 = load i32, i32* %g4_19, align 4
  %conv285 = sext i32 %223 to i64
  %mul286 = mul i64 %conv284, %conv285
  store i64 %mul286, i64* %f8g4_19, align 8
  %224 = load i32, i32* %f8, align 4
  %conv287 = sext i32 %224 to i64
  %225 = load i32, i32* %g5_19, align 4
  %conv288 = sext i32 %225 to i64
  %mul289 = mul i64 %conv287, %conv288
  store i64 %mul289, i64* %f8g5_19, align 8
  %226 = load i32, i32* %f8, align 4
  %conv290 = sext i32 %226 to i64
  %227 = load i32, i32* %g6_19, align 4
  %conv291 = sext i32 %227 to i64
  %mul292 = mul i64 %conv290, %conv291
  store i64 %mul292, i64* %f8g6_19, align 8
  %228 = load i32, i32* %f8, align 4
  %conv293 = sext i32 %228 to i64
  %229 = load i32, i32* %g7_19, align 4
  %conv294 = sext i32 %229 to i64
  %mul295 = mul i64 %conv293, %conv294
  store i64 %mul295, i64* %f8g7_19, align 8
  %230 = load i32, i32* %f8, align 4
  %conv296 = sext i32 %230 to i64
  %231 = load i32, i32* %g8_19, align 4
  %conv297 = sext i32 %231 to i64
  %mul298 = mul i64 %conv296, %conv297
  store i64 %mul298, i64* %f8g8_19, align 8
  %232 = load i32, i32* %f8, align 4
  %conv299 = sext i32 %232 to i64
  %233 = load i32, i32* %g9_19, align 4
  %conv300 = sext i32 %233 to i64
  %mul301 = mul i64 %conv299, %conv300
  store i64 %mul301, i64* %f8g9_19, align 8
  %234 = load i32, i32* %f9, align 4
  %conv302 = sext i32 %234 to i64
  %235 = load i32, i32* %g0, align 4
  %conv303 = sext i32 %235 to i64
  %mul304 = mul i64 %conv302, %conv303
  store i64 %mul304, i64* %f9g0, align 8
  %236 = load i32, i32* %f9_2, align 4
  %conv305 = sext i32 %236 to i64
  %237 = load i32, i32* %g1_19, align 4
  %conv306 = sext i32 %237 to i64
  %mul307 = mul i64 %conv305, %conv306
  store i64 %mul307, i64* %f9g1_38, align 8
  %238 = load i32, i32* %f9, align 4
  %conv308 = sext i32 %238 to i64
  %239 = load i32, i32* %g2_19, align 4
  %conv309 = sext i32 %239 to i64
  %mul310 = mul i64 %conv308, %conv309
  store i64 %mul310, i64* %f9g2_19, align 8
  %240 = load i32, i32* %f9_2, align 4
  %conv311 = sext i32 %240 to i64
  %241 = load i32, i32* %g3_19, align 4
  %conv312 = sext i32 %241 to i64
  %mul313 = mul i64 %conv311, %conv312
  store i64 %mul313, i64* %f9g3_38, align 8
  %242 = load i32, i32* %f9, align 4
  %conv314 = sext i32 %242 to i64
  %243 = load i32, i32* %g4_19, align 4
  %conv315 = sext i32 %243 to i64
  %mul316 = mul i64 %conv314, %conv315
  store i64 %mul316, i64* %f9g4_19, align 8
  %244 = load i32, i32* %f9_2, align 4
  %conv317 = sext i32 %244 to i64
  %245 = load i32, i32* %g5_19, align 4
  %conv318 = sext i32 %245 to i64
  %mul319 = mul i64 %conv317, %conv318
  store i64 %mul319, i64* %f9g5_38, align 8
  %246 = load i32, i32* %f9, align 4
  %conv320 = sext i32 %246 to i64
  %247 = load i32, i32* %g6_19, align 4
  %conv321 = sext i32 %247 to i64
  %mul322 = mul i64 %conv320, %conv321
  store i64 %mul322, i64* %f9g6_19, align 8
  %248 = load i32, i32* %f9_2, align 4
  %conv323 = sext i32 %248 to i64
  %249 = load i32, i32* %g7_19, align 4
  %conv324 = sext i32 %249 to i64
  %mul325 = mul i64 %conv323, %conv324
  store i64 %mul325, i64* %f9g7_38, align 8
  %250 = load i32, i32* %f9, align 4
  %conv326 = sext i32 %250 to i64
  %251 = load i32, i32* %g8_19, align 4
  %conv327 = sext i32 %251 to i64
  %mul328 = mul i64 %conv326, %conv327
  store i64 %mul328, i64* %f9g8_19, align 8
  %252 = load i32, i32* %f9_2, align 4
  %conv329 = sext i32 %252 to i64
  %253 = load i32, i32* %g9_19, align 4
  %conv330 = sext i32 %253 to i64
  %mul331 = mul i64 %conv329, %conv330
  store i64 %mul331, i64* %f9g9_38, align 8
  %254 = load i64, i64* %f0g0, align 8
  %255 = load i64, i64* %f1g9_38, align 8
  %add = add i64 %254, %255
  %256 = load i64, i64* %f2g8_19, align 8
  %add332 = add i64 %add, %256
  %257 = load i64, i64* %f3g7_38, align 8
  %add333 = add i64 %add332, %257
  %258 = load i64, i64* %f4g6_19, align 8
  %add334 = add i64 %add333, %258
  %259 = load i64, i64* %f5g5_38, align 8
  %add335 = add i64 %add334, %259
  %260 = load i64, i64* %f6g4_19, align 8
  %add336 = add i64 %add335, %260
  %261 = load i64, i64* %f7g3_38, align 8
  %add337 = add i64 %add336, %261
  %262 = load i64, i64* %f8g2_19, align 8
  %add338 = add i64 %add337, %262
  %263 = load i64, i64* %f9g1_38, align 8
  %add339 = add i64 %add338, %263
  store i64 %add339, i64* %h0, align 8
  %264 = load i64, i64* %f0g1, align 8
  %265 = load i64, i64* %f1g0, align 8
  %add340 = add i64 %264, %265
  %266 = load i64, i64* %f2g9_19, align 8
  %add341 = add i64 %add340, %266
  %267 = load i64, i64* %f3g8_19, align 8
  %add342 = add i64 %add341, %267
  %268 = load i64, i64* %f4g7_19, align 8
  %add343 = add i64 %add342, %268
  %269 = load i64, i64* %f5g6_19, align 8
  %add344 = add i64 %add343, %269
  %270 = load i64, i64* %f6g5_19, align 8
  %add345 = add i64 %add344, %270
  %271 = load i64, i64* %f7g4_19, align 8
  %add346 = add i64 %add345, %271
  %272 = load i64, i64* %f8g3_19, align 8
  %add347 = add i64 %add346, %272
  %273 = load i64, i64* %f9g2_19, align 8
  %add348 = add i64 %add347, %273
  store i64 %add348, i64* %h1, align 8
  %274 = load i64, i64* %f0g2, align 8
  %275 = load i64, i64* %f1g1_2, align 8
  %add349 = add i64 %274, %275
  %276 = load i64, i64* %f2g0, align 8
  %add350 = add i64 %add349, %276
  %277 = load i64, i64* %f3g9_38, align 8
  %add351 = add i64 %add350, %277
  %278 = load i64, i64* %f4g8_19, align 8
  %add352 = add i64 %add351, %278
  %279 = load i64, i64* %f5g7_38, align 8
  %add353 = add i64 %add352, %279
  %280 = load i64, i64* %f6g6_19, align 8
  %add354 = add i64 %add353, %280
  %281 = load i64, i64* %f7g5_38, align 8
  %add355 = add i64 %add354, %281
  %282 = load i64, i64* %f8g4_19, align 8
  %add356 = add i64 %add355, %282
  %283 = load i64, i64* %f9g3_38, align 8
  %add357 = add i64 %add356, %283
  store i64 %add357, i64* %h2, align 8
  %284 = load i64, i64* %f0g3, align 8
  %285 = load i64, i64* %f1g2, align 8
  %add358 = add i64 %284, %285
  %286 = load i64, i64* %f2g1, align 8
  %add359 = add i64 %add358, %286
  %287 = load i64, i64* %f3g0, align 8
  %add360 = add i64 %add359, %287
  %288 = load i64, i64* %f4g9_19, align 8
  %add361 = add i64 %add360, %288
  %289 = load i64, i64* %f5g8_19, align 8
  %add362 = add i64 %add361, %289
  %290 = load i64, i64* %f6g7_19, align 8
  %add363 = add i64 %add362, %290
  %291 = load i64, i64* %f7g6_19, align 8
  %add364 = add i64 %add363, %291
  %292 = load i64, i64* %f8g5_19, align 8
  %add365 = add i64 %add364, %292
  %293 = load i64, i64* %f9g4_19, align 8
  %add366 = add i64 %add365, %293
  store i64 %add366, i64* %h3, align 8
  %294 = load i64, i64* %f0g4, align 8
  %295 = load i64, i64* %f1g3_2, align 8
  %add367 = add i64 %294, %295
  %296 = load i64, i64* %f2g2, align 8
  %add368 = add i64 %add367, %296
  %297 = load i64, i64* %f3g1_2, align 8
  %add369 = add i64 %add368, %297
  %298 = load i64, i64* %f4g0, align 8
  %add370 = add i64 %add369, %298
  %299 = load i64, i64* %f5g9_38, align 8
  %add371 = add i64 %add370, %299
  %300 = load i64, i64* %f6g8_19, align 8
  %add372 = add i64 %add371, %300
  %301 = load i64, i64* %f7g7_38, align 8
  %add373 = add i64 %add372, %301
  %302 = load i64, i64* %f8g6_19, align 8
  %add374 = add i64 %add373, %302
  %303 = load i64, i64* %f9g5_38, align 8
  %add375 = add i64 %add374, %303
  store i64 %add375, i64* %h4, align 8
  %304 = load i64, i64* %f0g5, align 8
  %305 = load i64, i64* %f1g4, align 8
  %add376 = add i64 %304, %305
  %306 = load i64, i64* %f2g3, align 8
  %add377 = add i64 %add376, %306
  %307 = load i64, i64* %f3g2, align 8
  %add378 = add i64 %add377, %307
  %308 = load i64, i64* %f4g1, align 8
  %add379 = add i64 %add378, %308
  %309 = load i64, i64* %f5g0, align 8
  %add380 = add i64 %add379, %309
  %310 = load i64, i64* %f6g9_19, align 8
  %add381 = add i64 %add380, %310
  %311 = load i64, i64* %f7g8_19, align 8
  %add382 = add i64 %add381, %311
  %312 = load i64, i64* %f8g7_19, align 8
  %add383 = add i64 %add382, %312
  %313 = load i64, i64* %f9g6_19, align 8
  %add384 = add i64 %add383, %313
  store i64 %add384, i64* %h5, align 8
  %314 = load i64, i64* %f0g6, align 8
  %315 = load i64, i64* %f1g5_2, align 8
  %add385 = add i64 %314, %315
  %316 = load i64, i64* %f2g4, align 8
  %add386 = add i64 %add385, %316
  %317 = load i64, i64* %f3g3_2, align 8
  %add387 = add i64 %add386, %317
  %318 = load i64, i64* %f4g2, align 8
  %add388 = add i64 %add387, %318
  %319 = load i64, i64* %f5g1_2, align 8
  %add389 = add i64 %add388, %319
  %320 = load i64, i64* %f6g0, align 8
  %add390 = add i64 %add389, %320
  %321 = load i64, i64* %f7g9_38, align 8
  %add391 = add i64 %add390, %321
  %322 = load i64, i64* %f8g8_19, align 8
  %add392 = add i64 %add391, %322
  %323 = load i64, i64* %f9g7_38, align 8
  %add393 = add i64 %add392, %323
  store i64 %add393, i64* %h6, align 8
  %324 = load i64, i64* %f0g7, align 8
  %325 = load i64, i64* %f1g6, align 8
  %add394 = add i64 %324, %325
  %326 = load i64, i64* %f2g5, align 8
  %add395 = add i64 %add394, %326
  %327 = load i64, i64* %f3g4, align 8
  %add396 = add i64 %add395, %327
  %328 = load i64, i64* %f4g3, align 8
  %add397 = add i64 %add396, %328
  %329 = load i64, i64* %f5g2, align 8
  %add398 = add i64 %add397, %329
  %330 = load i64, i64* %f6g1, align 8
  %add399 = add i64 %add398, %330
  %331 = load i64, i64* %f7g0, align 8
  %add400 = add i64 %add399, %331
  %332 = load i64, i64* %f8g9_19, align 8
  %add401 = add i64 %add400, %332
  %333 = load i64, i64* %f9g8_19, align 8
  %add402 = add i64 %add401, %333
  store i64 %add402, i64* %h7, align 8
  %334 = load i64, i64* %f0g8, align 8
  %335 = load i64, i64* %f1g7_2, align 8
  %add403 = add i64 %334, %335
  %336 = load i64, i64* %f2g6, align 8
  %add404 = add i64 %add403, %336
  %337 = load i64, i64* %f3g5_2, align 8
  %add405 = add i64 %add404, %337
  %338 = load i64, i64* %f4g4, align 8
  %add406 = add i64 %add405, %338
  %339 = load i64, i64* %f5g3_2, align 8
  %add407 = add i64 %add406, %339
  %340 = load i64, i64* %f6g2, align 8
  %add408 = add i64 %add407, %340
  %341 = load i64, i64* %f7g1_2, align 8
  %add409 = add i64 %add408, %341
  %342 = load i64, i64* %f8g0, align 8
  %add410 = add i64 %add409, %342
  %343 = load i64, i64* %f9g9_38, align 8
  %add411 = add i64 %add410, %343
  store i64 %add411, i64* %h8, align 8
  %344 = load i64, i64* %f0g9, align 8
  %345 = load i64, i64* %f1g8, align 8
  %add412 = add i64 %344, %345
  %346 = load i64, i64* %f2g7, align 8
  %add413 = add i64 %add412, %346
  %347 = load i64, i64* %f3g6, align 8
  %add414 = add i64 %add413, %347
  %348 = load i64, i64* %f4g5, align 8
  %add415 = add i64 %add414, %348
  %349 = load i64, i64* %f5g4, align 8
  %add416 = add i64 %add415, %349
  %350 = load i64, i64* %f6g3, align 8
  %add417 = add i64 %add416, %350
  %351 = load i64, i64* %f7g2, align 8
  %add418 = add i64 %add417, %351
  %352 = load i64, i64* %f8g1, align 8
  %add419 = add i64 %add418, %352
  %353 = load i64, i64* %f9g0, align 8
  %add420 = add i64 %add419, %353
  store i64 %add420, i64* %h9, align 8
  %354 = load i64, i64* %h0, align 8
  %add421 = add i64 %354, 33554432
  %shr = ashr i64 %add421, 26
  store i64 %shr, i64* %carry0, align 8
  %355 = load i64, i64* %carry0, align 8
  %356 = load i64, i64* %h1, align 8
  %add422 = add i64 %356, %355
  store i64 %add422, i64* %h1, align 8
  %357 = load i64, i64* %carry0, align 8
  %mul423 = mul i64 %357, 67108864
  %358 = load i64, i64* %h0, align 8
  %sub = sub i64 %358, %mul423
  store i64 %sub, i64* %h0, align 8
  %359 = load i64, i64* %h4, align 8
  %add424 = add i64 %359, 33554432
  %shr425 = ashr i64 %add424, 26
  store i64 %shr425, i64* %carry4, align 8
  %360 = load i64, i64* %carry4, align 8
  %361 = load i64, i64* %h5, align 8
  %add426 = add i64 %361, %360
  store i64 %add426, i64* %h5, align 8
  %362 = load i64, i64* %carry4, align 8
  %mul427 = mul i64 %362, 67108864
  %363 = load i64, i64* %h4, align 8
  %sub428 = sub i64 %363, %mul427
  store i64 %sub428, i64* %h4, align 8
  %364 = load i64, i64* %h1, align 8
  %add429 = add i64 %364, 16777216
  %shr430 = ashr i64 %add429, 25
  store i64 %shr430, i64* %carry1, align 8
  %365 = load i64, i64* %carry1, align 8
  %366 = load i64, i64* %h2, align 8
  %add431 = add i64 %366, %365
  store i64 %add431, i64* %h2, align 8
  %367 = load i64, i64* %carry1, align 8
  %mul432 = mul i64 %367, 33554432
  %368 = load i64, i64* %h1, align 8
  %sub433 = sub i64 %368, %mul432
  store i64 %sub433, i64* %h1, align 8
  %369 = load i64, i64* %h5, align 8
  %add434 = add i64 %369, 16777216
  %shr435 = ashr i64 %add434, 25
  store i64 %shr435, i64* %carry5, align 8
  %370 = load i64, i64* %carry5, align 8
  %371 = load i64, i64* %h6, align 8
  %add436 = add i64 %371, %370
  store i64 %add436, i64* %h6, align 8
  %372 = load i64, i64* %carry5, align 8
  %mul437 = mul i64 %372, 33554432
  %373 = load i64, i64* %h5, align 8
  %sub438 = sub i64 %373, %mul437
  store i64 %sub438, i64* %h5, align 8
  %374 = load i64, i64* %h2, align 8
  %add439 = add i64 %374, 33554432
  %shr440 = ashr i64 %add439, 26
  store i64 %shr440, i64* %carry2, align 8
  %375 = load i64, i64* %carry2, align 8
  %376 = load i64, i64* %h3, align 8
  %add441 = add i64 %376, %375
  store i64 %add441, i64* %h3, align 8
  %377 = load i64, i64* %carry2, align 8
  %mul442 = mul i64 %377, 67108864
  %378 = load i64, i64* %h2, align 8
  %sub443 = sub i64 %378, %mul442
  store i64 %sub443, i64* %h2, align 8
  %379 = load i64, i64* %h6, align 8
  %add444 = add i64 %379, 33554432
  %shr445 = ashr i64 %add444, 26
  store i64 %shr445, i64* %carry6, align 8
  %380 = load i64, i64* %carry6, align 8
  %381 = load i64, i64* %h7, align 8
  %add446 = add i64 %381, %380
  store i64 %add446, i64* %h7, align 8
  %382 = load i64, i64* %carry6, align 8
  %mul447 = mul i64 %382, 67108864
  %383 = load i64, i64* %h6, align 8
  %sub448 = sub i64 %383, %mul447
  store i64 %sub448, i64* %h6, align 8
  %384 = load i64, i64* %h3, align 8
  %add449 = add i64 %384, 16777216
  %shr450 = ashr i64 %add449, 25
  store i64 %shr450, i64* %carry3, align 8
  %385 = load i64, i64* %carry3, align 8
  %386 = load i64, i64* %h4, align 8
  %add451 = add i64 %386, %385
  store i64 %add451, i64* %h4, align 8
  %387 = load i64, i64* %carry3, align 8
  %mul452 = mul i64 %387, 33554432
  %388 = load i64, i64* %h3, align 8
  %sub453 = sub i64 %388, %mul452
  store i64 %sub453, i64* %h3, align 8
  %389 = load i64, i64* %h7, align 8
  %add454 = add i64 %389, 16777216
  %shr455 = ashr i64 %add454, 25
  store i64 %shr455, i64* %carry7, align 8
  %390 = load i64, i64* %carry7, align 8
  %391 = load i64, i64* %h8, align 8
  %add456 = add i64 %391, %390
  store i64 %add456, i64* %h8, align 8
  %392 = load i64, i64* %carry7, align 8
  %mul457 = mul i64 %392, 33554432
  %393 = load i64, i64* %h7, align 8
  %sub458 = sub i64 %393, %mul457
  store i64 %sub458, i64* %h7, align 8
  %394 = load i64, i64* %h4, align 8
  %add459 = add i64 %394, 33554432
  %shr460 = ashr i64 %add459, 26
  store i64 %shr460, i64* %carry4, align 8
  %395 = load i64, i64* %carry4, align 8
  %396 = load i64, i64* %h5, align 8
  %add461 = add i64 %396, %395
  store i64 %add461, i64* %h5, align 8
  %397 = load i64, i64* %carry4, align 8
  %mul462 = mul i64 %397, 67108864
  %398 = load i64, i64* %h4, align 8
  %sub463 = sub i64 %398, %mul462
  store i64 %sub463, i64* %h4, align 8
  %399 = load i64, i64* %h8, align 8
  %add464 = add i64 %399, 33554432
  %shr465 = ashr i64 %add464, 26
  store i64 %shr465, i64* %carry8, align 8
  %400 = load i64, i64* %carry8, align 8
  %401 = load i64, i64* %h9, align 8
  %add466 = add i64 %401, %400
  store i64 %add466, i64* %h9, align 8
  %402 = load i64, i64* %carry8, align 8
  %mul467 = mul i64 %402, 67108864
  %403 = load i64, i64* %h8, align 8
  %sub468 = sub i64 %403, %mul467
  store i64 %sub468, i64* %h8, align 8
  %404 = load i64, i64* %h9, align 8
  %add469 = add i64 %404, 16777216
  %shr470 = ashr i64 %add469, 25
  store i64 %shr470, i64* %carry9, align 8
  %405 = load i64, i64* %carry9, align 8
  %mul471 = mul i64 %405, 19
  %406 = load i64, i64* %h0, align 8
  %add472 = add i64 %406, %mul471
  store i64 %add472, i64* %h0, align 8
  %407 = load i64, i64* %carry9, align 8
  %mul473 = mul i64 %407, 33554432
  %408 = load i64, i64* %h9, align 8
  %sub474 = sub i64 %408, %mul473
  store i64 %sub474, i64* %h9, align 8
  %409 = load i64, i64* %h0, align 8
  %add475 = add i64 %409, 33554432
  %shr476 = ashr i64 %add475, 26
  store i64 %shr476, i64* %carry0, align 8
  %410 = load i64, i64* %carry0, align 8
  %411 = load i64, i64* %h1, align 8
  %add477 = add i64 %411, %410
  store i64 %add477, i64* %h1, align 8
  %412 = load i64, i64* %carry0, align 8
  %mul478 = mul i64 %412, 67108864
  %413 = load i64, i64* %h0, align 8
  %sub479 = sub i64 %413, %mul478
  store i64 %sub479, i64* %h0, align 8
  %414 = load i64, i64* %h0, align 8
  %conv480 = trunc i64 %414 to i32
  %415 = load i32*, i32** %h.addr, align 4
  %arrayidx481 = getelementptr i32, i32* %415, i32 0
  store i32 %conv480, i32* %arrayidx481, align 4
  %416 = load i64, i64* %h1, align 8
  %conv482 = trunc i64 %416 to i32
  %417 = load i32*, i32** %h.addr, align 4
  %arrayidx483 = getelementptr i32, i32* %417, i32 1
  store i32 %conv482, i32* %arrayidx483, align 4
  %418 = load i64, i64* %h2, align 8
  %conv484 = trunc i64 %418 to i32
  %419 = load i32*, i32** %h.addr, align 4
  %arrayidx485 = getelementptr i32, i32* %419, i32 2
  store i32 %conv484, i32* %arrayidx485, align 4
  %420 = load i64, i64* %h3, align 8
  %conv486 = trunc i64 %420 to i32
  %421 = load i32*, i32** %h.addr, align 4
  %arrayidx487 = getelementptr i32, i32* %421, i32 3
  store i32 %conv486, i32* %arrayidx487, align 4
  %422 = load i64, i64* %h4, align 8
  %conv488 = trunc i64 %422 to i32
  %423 = load i32*, i32** %h.addr, align 4
  %arrayidx489 = getelementptr i32, i32* %423, i32 4
  store i32 %conv488, i32* %arrayidx489, align 4
  %424 = load i64, i64* %h5, align 8
  %conv490 = trunc i64 %424 to i32
  %425 = load i32*, i32** %h.addr, align 4
  %arrayidx491 = getelementptr i32, i32* %425, i32 5
  store i32 %conv490, i32* %arrayidx491, align 4
  %426 = load i64, i64* %h6, align 8
  %conv492 = trunc i64 %426 to i32
  %427 = load i32*, i32** %h.addr, align 4
  %arrayidx493 = getelementptr i32, i32* %427, i32 6
  store i32 %conv492, i32* %arrayidx493, align 4
  %428 = load i64, i64* %h7, align 8
  %conv494 = trunc i64 %428 to i32
  %429 = load i32*, i32** %h.addr, align 4
  %arrayidx495 = getelementptr i32, i32* %429, i32 7
  store i32 %conv494, i32* %arrayidx495, align 4
  %430 = load i64, i64* %h8, align 8
  %conv496 = trunc i64 %430 to i32
  %431 = load i32*, i32** %h.addr, align 4
  %arrayidx497 = getelementptr i32, i32* %431, i32 8
  store i32 %conv496, i32* %arrayidx497, align 4
  %432 = load i64, i64* %h9, align 8
  %conv498 = trunc i64 %432 to i32
  %433 = load i32*, i32** %h.addr, align 4
  %arrayidx499 = getelementptr i32, i32* %433, i32 9
  store i32 %conv498, i32* %arrayidx499, align 4
  ret void
}

declare void @fe25519_tobytes(i8*, i32*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_sk_to_curve25519(i8* nonnull %curve25519_sk, i8* nonnull %ed25519_sk) #0 {
entry:
  %curve25519_sk.addr = alloca i8*, align 4
  %ed25519_sk.addr = alloca i8*, align 4
  %h = alloca [64 x i8], align 16
  store i8* %curve25519_sk, i8** %curve25519_sk.addr, align 4
  store i8* %ed25519_sk, i8** %ed25519_sk.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %0 = load i8*, i8** %ed25519_sk.addr, align 4
  %call = call i32 @crypto_hash_sha512(i8* %arraydecay, i8* %0, i64 32)
  %arrayidx = getelementptr [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %1 = load i8, i8* %arrayidx, align 16
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 248
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 16
  %arrayidx2 = getelementptr [64 x i8], [64 x i8]* %h, i32 0, i32 31
  %2 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %2 to i32
  %and4 = and i32 %conv3, 127
  %conv5 = trunc i32 %and4 to i8
  store i8 %conv5, i8* %arrayidx2, align 1
  %arrayidx6 = getelementptr [64 x i8], [64 x i8]* %h, i32 0, i32 31
  %3 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %3 to i32
  %or = or i32 %conv7, 64
  %conv8 = trunc i32 %or to i8
  store i8 %conv8, i8* %arrayidx6, align 1
  %4 = load i8*, i8** %curve25519_sk.addr, align 4
  %arraydecay9 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %4, i8* align 16 %arraydecay9, i32 32, i1 false)
  %arraydecay10 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay10, i32 64)
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
