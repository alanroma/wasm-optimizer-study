; ModuleID = 'crypto_sign/ed25519/ref10/open.c'
source_filename = "crypto_sign/ed25519/ref10/open.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }
%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_p2 = type { [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_sign_ed25519_verify_detached(i8* %sig, i8* %m, i64 %mlen, i8* %pk, i32 %prehashed) #0 {
entry:
  %retval = alloca i32, align 4
  %sig.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  %prehashed.addr = alloca i32, align 4
  %hs = alloca %struct.crypto_hash_sha512_state, align 8
  %h = alloca [64 x i8], align 16
  %rcheck = alloca [32 x i8], align 16
  %A = alloca %struct.ge25519_p3, align 4
  %R = alloca %struct.ge25519_p2, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  store i32 %prehashed, i32* %prehashed.addr, align 4
  %0 = load i8*, i8** %sig.addr, align 4
  %add.ptr = getelementptr i8, i8* %0, i32 32
  %call = call i32 @sc25519_is_canonical(i8* %add.ptr)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %sig.addr, align 4
  %call1 = call i32 @ge25519_has_small_order(i8* %1)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i8*, i8** %pk.addr, align 4
  %call3 = call i32 @ge25519_is_canonical(i8* %2)
  %cmp4 = icmp eq i32 %call3, 0
  br i1 %cmp4, label %if.then8, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %if.end
  %3 = load i8*, i8** %pk.addr, align 4
  %call6 = call i32 @ge25519_has_small_order(i8* %3)
  %cmp7 = icmp ne i32 %call6, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %lor.lhs.false5, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %lor.lhs.false5
  %4 = load i8*, i8** %pk.addr, align 4
  %call10 = call i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3* %A, i8* %4)
  %cmp11 = icmp ne i32 %call10, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end9
  %5 = load i32, i32* %prehashed.addr, align 4
  call void @_crypto_sign_ed25519_ref10_hinit(%struct.crypto_hash_sha512_state* %hs, i32 %5)
  %6 = load i8*, i8** %sig.addr, align 4
  %call14 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %6, i64 32)
  %7 = load i8*, i8** %pk.addr, align 4
  %call15 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %7, i64 32)
  %8 = load i8*, i8** %m.addr, align 4
  %9 = load i64, i64* %mlen.addr, align 8
  %call16 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %8, i64 %9)
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %call17 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay)
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay18)
  %arraydecay19 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %10 = load i8*, i8** %sig.addr, align 4
  %add.ptr20 = getelementptr i8, i8* %10, i32 32
  call void @ge25519_double_scalarmult_vartime(%struct.ge25519_p2* %R, i8* %arraydecay19, %struct.ge25519_p3* %A, i8* %add.ptr20)
  %arraydecay21 = getelementptr inbounds [32 x i8], [32 x i8]* %rcheck, i32 0, i32 0
  call void @ge25519_tobytes(i8* %arraydecay21, %struct.ge25519_p2* %R)
  %arraydecay22 = getelementptr inbounds [32 x i8], [32 x i8]* %rcheck, i32 0, i32 0
  %11 = load i8*, i8** %sig.addr, align 4
  %call23 = call i32 @crypto_verify_32(i8* %arraydecay22, i8* %11)
  %arraydecay24 = getelementptr inbounds [32 x i8], [32 x i8]* %rcheck, i32 0, i32 0
  %12 = load i8*, i8** %sig.addr, align 4
  %cmp25 = icmp eq i8* %arraydecay24, %12
  %conv = zext i1 %cmp25 to i32
  %sub = sub i32 0, %conv
  %or = or i32 %call23, %sub
  %13 = load i8*, i8** %sig.addr, align 4
  %arraydecay26 = getelementptr inbounds [32 x i8], [32 x i8]* %rcheck, i32 0, i32 0
  %call27 = call i32 @sodium_memcmp(i8* %13, i8* %arraydecay26, i32 32)
  %or28 = or i32 %or, %call27
  store i32 %or28, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end13, %if.then12, %if.then8, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i32 @sc25519_is_canonical(i8*) #1

declare i32 @ge25519_has_small_order(i8*) #1

declare i32 @ge25519_is_canonical(i8*) #1

declare i32 @ge25519_frombytes_negate_vartime(%struct.ge25519_p3*, i8*) #1

declare void @_crypto_sign_ed25519_ref10_hinit(%struct.crypto_hash_sha512_state*, i32) #1

declare i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state*, i8*, i64) #1

declare i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state*, i8*) #1

declare void @sc25519_reduce(i8*) #1

declare void @ge25519_double_scalarmult_vartime(%struct.ge25519_p2*, i8*, %struct.ge25519_p3*, i8*) #1

declare void @ge25519_tobytes(i8*, %struct.ge25519_p2*) #1

declare i32 @crypto_verify_32(i8*, i8*) #1

declare i32 @sodium_memcmp(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_verify_detached(i8* nonnull %sig, i8* %m, i64 %mlen, i8* nonnull %pk) #0 {
entry:
  %sig.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load i8*, i8** %sig.addr, align 4
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %3 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @_crypto_sign_ed25519_verify_detached(i8* %0, i8* %1, i64 %2, i8* %3, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_open(i8* %m, i64* %mlen_p, i8* nonnull %sm, i64 %smlen, i8* nonnull %pk) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %sm.addr = alloca i8*, align 4
  %smlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  %mlen = alloca i64, align 8
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %sm, i8** %sm.addr, align 4
  store i64 %smlen, i64* %smlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load i64, i64* %smlen.addr, align 8
  %cmp = icmp ult i64 %0, 64
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i64, i64* %smlen.addr, align 8
  %sub = sub i64 %1, 64
  %cmp1 = icmp ugt i64 %sub, 4294967231
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  br label %badsig

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i64, i64* %smlen.addr, align 8
  %sub2 = sub i64 %2, 64
  store i64 %sub2, i64* %mlen, align 8
  %3 = load i8*, i8** %sm.addr, align 4
  %4 = load i8*, i8** %sm.addr, align 4
  %add.ptr = getelementptr i8, i8* %4, i32 64
  %5 = load i64, i64* %mlen, align 8
  %6 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_verify_detached(i8* %3, i8* %add.ptr, i64 %5, i8* %6)
  %cmp3 = icmp ne i32 %call, 0
  br i1 %cmp3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %if.end
  %7 = load i8*, i8** %m.addr, align 4
  %cmp5 = icmp ne i8* %7, null
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.then4
  %8 = load i8*, i8** %m.addr, align 4
  %9 = load i64, i64* %mlen, align 8
  %conv = trunc i64 %9 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %8, i8 0, i32 %conv, i1 false)
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.then4
  br label %badsig

if.end8:                                          ; preds = %if.end
  %10 = load i64*, i64** %mlen_p.addr, align 4
  %cmp9 = icmp ne i64* %10, null
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end8
  %11 = load i64, i64* %mlen, align 8
  %12 = load i64*, i64** %mlen_p.addr, align 4
  store i64 %11, i64* %12, align 8
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end8
  %13 = load i8*, i8** %m.addr, align 4
  %cmp13 = icmp ne i8* %13, null
  br i1 %cmp13, label %if.then15, label %if.end18

if.then15:                                        ; preds = %if.end12
  %14 = load i8*, i8** %m.addr, align 4
  %15 = load i8*, i8** %sm.addr, align 4
  %add.ptr16 = getelementptr i8, i8* %15, i32 64
  %16 = load i64, i64* %mlen, align 8
  %conv17 = trunc i64 %16 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %14, i8* align 1 %add.ptr16, i32 %conv17, i1 false)
  br label %if.end18

if.end18:                                         ; preds = %if.then15, %if.end12
  store i32 0, i32* %retval, align 4
  br label %return

badsig:                                           ; preds = %if.end7, %if.then
  %17 = load i64*, i64** %mlen_p.addr, align 4
  %cmp19 = icmp ne i64* %17, null
  br i1 %cmp19, label %if.then21, label %if.end22

if.then21:                                        ; preds = %badsig
  %18 = load i64*, i64** %mlen_p.addr, align 4
  store i64 0, i64* %18, align 8
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %badsig
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.end18
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
