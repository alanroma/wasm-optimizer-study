; ModuleID = 'crypto_secretstream/xchacha20poly1305/secretstream_xchacha20poly1305.c'
source_filename = "crypto_secretstream/xchacha20poly1305/secretstream_xchacha20poly1305.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_secretstream_xchacha20poly1305_state = type { [32 x i8], [12 x i8], [8 x i8] }
%struct.crypto_onetimeauth_poly1305_state = type { [256 x i8] }

@_pad0 = internal constant [16 x i8] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone
define void @crypto_secretstream_xchacha20poly1305_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_init_push(%struct.crypto_secretstream_xchacha20poly1305_state* nonnull %state, i8* nonnull %out, i8* nonnull %k) #0 {
entry:
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  %out.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  call void @randombytes_buf(i8* %0, i32 24)
  %1 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k1 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %k1, i32 0, i32 0
  %2 = load i8*, i8** %out.addr, align 4
  %3 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_core_hchacha20(i8* %arraydecay, i8* %2, i8* %3, i8* null)
  %4 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  call void @_crypto_secretstream_xchacha20poly1305_counter_reset(%struct.crypto_secretstream_xchacha20poly1305_state* %4)
  %5 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %5, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay2, i32 4
  %6 = load i8*, i8** %out.addr, align 4
  %add.ptr3 = getelementptr i8, i8* %6, i32 16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %add.ptr3, i32 8, i1 false)
  %7 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %_pad = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %7, i32 0, i32 2
  %arraydecay4 = getelementptr inbounds [8 x i8], [8 x i8]* %_pad, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay4, i8 0, i32 8, i1 false)
  ret i32 0
}

declare i32 @crypto_core_hchacha20(i8*, i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @_crypto_secretstream_xchacha20poly1305_counter_reset(%struct.crypto_secretstream_xchacha20poly1305_state* %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %0 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %0, i32 0, i32 1
  %arraydecay = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay, i8 0, i32 4, i1 false)
  %1 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce1 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %1, i32 0, i32 1
  %arrayidx = getelementptr [12 x i8], [12 x i8]* %nonce1, i32 0, i32 0
  store i8 1, i8* %arrayidx, align 1
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_init_pull(%struct.crypto_secretstream_xchacha20poly1305_state* nonnull %state, i8* nonnull %in, i8* nonnull %k) #0 {
entry:
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  %in.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k1 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %k1, i32 0, i32 0
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_core_hchacha20(i8* %arraydecay, i8* %1, i8* %2, i8* null)
  %3 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  call void @_crypto_secretstream_xchacha20poly1305_counter_reset(%struct.crypto_secretstream_xchacha20poly1305_state* %3)
  %4 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %4, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay2, i32 4
  %5 = load i8*, i8** %in.addr, align 4
  %add.ptr3 = getelementptr i8, i8* %5, i32 16
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %add.ptr3, i32 8, i1 false)
  %6 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %_pad = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %6, i32 0, i32 2
  %arraydecay4 = getelementptr inbounds [8 x i8], [8 x i8]* %_pad, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 1 %arraydecay4, i8 0, i32 8, i1 false)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define void @crypto_secretstream_xchacha20poly1305_rekey(%struct.crypto_secretstream_xchacha20poly1305_state* %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  %new_key_and_inonce = alloca [40 x i8], align 16
  %i = alloca i32, align 4
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [32 x i8], [32 x i8]* %k, i32 0, i32 %2
  %3 = load i8, i8* %arrayidx, align 1
  %4 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 %4
  store i8 %3, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %6 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %6, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %7 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %7, i32 0, i32 1
  %arraydecay = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay, i32 4
  %8 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %add.ptr, i32 %8
  %9 = load i8, i8* %arrayidx5, align 1
  %10 = load i32, i32* %i, align 4
  %add = add i32 32, %10
  %arrayidx6 = getelementptr [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 %add
  store i8 %9, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %11 = load i32, i32* %i, align 4
  %inc8 = add i32 %11, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  %arraydecay10 = getelementptr inbounds [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 0
  %12 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce12 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %12, i32 0, i32 1
  %arraydecay13 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce12, i32 0, i32 0
  %13 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k14 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %13, i32 0, i32 0
  %arraydecay15 = getelementptr inbounds [32 x i8], [32 x i8]* %k14, i32 0, i32 0
  %call = call i32 @crypto_stream_chacha20_ietf_xor(i8* %arraydecay10, i8* %arraydecay11, i64 40, i8* %arraydecay13, i8* %arraydecay15)
  store i32 0, i32* %i, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc22, %for.end9
  %14 = load i32, i32* %i, align 4
  %cmp17 = icmp ult i32 %14, 32
  br i1 %cmp17, label %for.body18, label %for.end24

for.body18:                                       ; preds = %for.cond16
  %15 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 %15
  %16 = load i8, i8* %arrayidx19, align 1
  %17 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k20 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr [32 x i8], [32 x i8]* %k20, i32 0, i32 %18
  store i8 %16, i8* %arrayidx21, align 1
  br label %for.inc22

for.inc22:                                        ; preds = %for.body18
  %19 = load i32, i32* %i, align 4
  %inc23 = add i32 %19, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond16

for.end24:                                        ; preds = %for.cond16
  store i32 0, i32* %i, align 4
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc34, %for.end24
  %20 = load i32, i32* %i, align 4
  %cmp26 = icmp ult i32 %20, 8
  br i1 %cmp26, label %for.body27, label %for.end36

for.body27:                                       ; preds = %for.cond25
  %21 = load i32, i32* %i, align 4
  %add28 = add i32 32, %21
  %arrayidx29 = getelementptr [40 x i8], [40 x i8]* %new_key_and_inonce, i32 0, i32 %add28
  %22 = load i8, i8* %arrayidx29, align 1
  %23 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce30 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %23, i32 0, i32 1
  %arraydecay31 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce30, i32 0, i32 0
  %add.ptr32 = getelementptr i8, i8* %arraydecay31, i32 4
  %24 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr i8, i8* %add.ptr32, i32 %24
  store i8 %22, i8* %arrayidx33, align 1
  br label %for.inc34

for.inc34:                                        ; preds = %for.body27
  %25 = load i32, i32* %i, align 4
  %inc35 = add i32 %25, 1
  store i32 %inc35, i32* %i, align 4
  br label %for.cond25

for.end36:                                        ; preds = %for.cond25
  %26 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  call void @_crypto_secretstream_xchacha20poly1305_counter_reset(%struct.crypto_secretstream_xchacha20poly1305_state* %26)
  ret void
}

declare i32 @crypto_stream_chacha20_ietf_xor(i8*, i8*, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_push(%struct.crypto_secretstream_xchacha20poly1305_state* nonnull %state, i8* %out, i64* %outlen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8 zeroext %tag) #0 {
entry:
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  %out.addr = alloca i8*, align 4
  %outlen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %tag.addr = alloca i8, align 1
  %poly1305_state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  %c = alloca i8*, align 4
  %mac = alloca i8*, align 4
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  store i64* %outlen_p, i64** %outlen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8 %tag, i8* %tag.addr, align 1
  %0 = load i64*, i64** %outlen_p.addr, align 4
  %cmp = icmp ne i64* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i64*, i64** %outlen_p.addr, align 4
  store i64 0, i64* %1, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i64, i64* %mlen.addr, align 8
  %cmp1 = icmp ugt i64 %2, 4294967278
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  call void @sodium_misuse() #5
  unreachable

if.end3:                                          ; preds = %if.end
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %3 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %3, i32 0, i32 1
  %arraydecay4 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  %4 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %4, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %call = call i32 @crypto_stream_chacha20_ietf(i8* %arraydecay, i64 64, i8* %arraydecay4, i8* %arraydecay5)
  %arraydecay6 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call7 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay6)
  %arraydecay8 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay8, i32 64)
  %5 = load i8*, i8** %ad.addr, align 4
  %6 = load i64, i64* %adlen.addr, align 8
  %call9 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %5, i64 %6)
  %7 = load i64, i64* %adlen.addr, align 8
  %sub = sub i64 16, %7
  %and = and i64 %sub, 15
  %call10 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and)
  %arraydecay11 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay11, i8 0, i32 64, i1 false)
  %8 = load i8, i8* %tag.addr, align 1
  %arrayidx = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 0
  store i8 %8, i8* %arrayidx, align 16
  %arraydecay12 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay13 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %9 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce14 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %9, i32 0, i32 1
  %arraydecay15 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce14, i32 0, i32 0
  %10 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k16 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %10, i32 0, i32 0
  %arraydecay17 = getelementptr inbounds [32 x i8], [32 x i8]* %k16, i32 0, i32 0
  %call18 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %arraydecay12, i8* %arraydecay13, i64 64, i8* %arraydecay15, i32 1, i8* %arraydecay17)
  %arraydecay19 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call20 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay19, i64 64)
  %arrayidx21 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %11 = load i8, i8* %arrayidx21, align 16
  %12 = load i8*, i8** %out.addr, align 4
  %arrayidx22 = getelementptr i8, i8* %12, i32 0
  store i8 %11, i8* %arrayidx22, align 1
  %13 = load i8*, i8** %out.addr, align 4
  %add.ptr = getelementptr i8, i8* %13, i32 1
  store i8* %add.ptr, i8** %c, align 4
  %14 = load i8*, i8** %c, align 4
  %15 = load i8*, i8** %m.addr, align 4
  %16 = load i64, i64* %mlen.addr, align 8
  %17 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce23 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %17, i32 0, i32 1
  %arraydecay24 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce23, i32 0, i32 0
  %18 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k25 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %18, i32 0, i32 0
  %arraydecay26 = getelementptr inbounds [32 x i8], [32 x i8]* %k25, i32 0, i32 0
  %call27 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %14, i8* %15, i64 %16, i8* %arraydecay24, i32 2, i8* %arraydecay26)
  %19 = load i8*, i8** %c, align 4
  %20 = load i64, i64* %mlen.addr, align 8
  %call28 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %19, i64 %20)
  %21 = load i64, i64* %mlen.addr, align 8
  %add = add i64 4294967248, %21
  %and29 = and i64 %add, 15
  %call30 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and29)
  %arraydecay31 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %22 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay31, i64 %22)
  %arraydecay32 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call33 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay32, i64 8)
  %arraydecay34 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %23 = load i64, i64* %mlen.addr, align 8
  %add35 = add i64 64, %23
  call void @store64_le(i8* %arraydecay34, i64 %add35)
  %arraydecay36 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call37 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay36, i64 8)
  %24 = load i8*, i8** %c, align 4
  %25 = load i64, i64* %mlen.addr, align 8
  %idx.ext = trunc i64 %25 to i32
  %add.ptr38 = getelementptr i8, i8* %24, i32 %idx.ext
  store i8* %add.ptr38, i8** %mac, align 4
  %26 = load i8*, i8** %mac, align 4
  %call39 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %26)
  %27 = bitcast %struct.crypto_onetimeauth_poly1305_state* %poly1305_state to i8*
  call void @sodium_memzero(i8* %27, i32 256)
  %28 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce40 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %28, i32 0, i32 1
  %arraydecay41 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce40, i32 0, i32 0
  %add.ptr42 = getelementptr i8, i8* %arraydecay41, i32 4
  %29 = load i8*, i8** %mac, align 4
  call void @xor_buf(i8* %add.ptr42, i8* %29, i32 8)
  %30 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce43 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %30, i32 0, i32 1
  %arraydecay44 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce43, i32 0, i32 0
  call void @sodium_increment(i8* %arraydecay44, i32 4)
  %31 = load i8, i8* %tag.addr, align 1
  %conv = zext i8 %31 to i32
  %and45 = and i32 %conv, 2
  %cmp46 = icmp ne i32 %and45, 0
  br i1 %cmp46, label %if.then51, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end3
  %32 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce48 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %32, i32 0, i32 1
  %arraydecay49 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce48, i32 0, i32 0
  %call50 = call i32 @sodium_is_zero(i8* %arraydecay49, i32 4)
  %tobool = icmp ne i32 %call50, 0
  br i1 %tobool, label %if.then51, label %if.end52

if.then51:                                        ; preds = %lor.lhs.false, %if.end3
  %33 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  call void @crypto_secretstream_xchacha20poly1305_rekey(%struct.crypto_secretstream_xchacha20poly1305_state* %33)
  br label %if.end52

if.end52:                                         ; preds = %if.then51, %lor.lhs.false
  %34 = load i64*, i64** %outlen_p.addr, align 4
  %cmp53 = icmp ne i64* %34, null
  br i1 %cmp53, label %if.then55, label %if.end57

if.then55:                                        ; preds = %if.end52
  %35 = load i64, i64* %mlen.addr, align 8
  %add56 = add i64 17, %35
  %36 = load i64*, i64** %outlen_p.addr, align 4
  store i64 %add56, i64* %36, align 8
  br label %if.end57

if.end57:                                         ; preds = %if.then55, %if.end52
  ret i32 0
}

; Function Attrs: noreturn
declare void @sodium_misuse() #4

declare i32 @crypto_stream_chacha20_ietf(i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

declare i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state*, i8*, i64) #1

declare i32 @crypto_stream_chacha20_ietf_xor_ic(i8*, i8*, i64, i8*, i32, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

declare i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @xor_buf(i8* %out, i8* %in, i32 %n) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %in.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %out.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %7 to i32
  %xor = xor i32 %conv2, %conv
  %conv3 = trunc i32 %xor to i8
  store i8 %conv3, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @sodium_increment(i8*, i32) #1

declare i32 @sodium_is_zero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_pull(%struct.crypto_secretstream_xchacha20poly1305_state* nonnull %state, i8* %m, i64* %mlen_p, i8* %tag_p, i8* %in, i64 %inlen, i8* %ad, i64 %adlen) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.crypto_secretstream_xchacha20poly1305_state*, align 4
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %tag_p.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %poly1305_state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  %mac = alloca [16 x i8], align 16
  %c = alloca i8*, align 4
  %stored_mac = alloca i8*, align 4
  %mlen = alloca i64, align 8
  %tag = alloca i8, align 1
  store %struct.crypto_secretstream_xchacha20poly1305_state* %state, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %tag_p, i8** %tag_p.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  %0 = load i64*, i64** %mlen_p.addr, align 4
  %cmp = icmp ne i64* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i64*, i64** %mlen_p.addr, align 4
  store i64 0, i64* %1, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8*, i8** %tag_p.addr, align 4
  %cmp1 = icmp ne i8* %2, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load i8*, i8** %tag_p.addr, align 4
  store i8 -1, i8* %3, align 1
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %4 = load i64, i64* %inlen.addr, align 8
  %cmp4 = icmp ult i64 %4, 17
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = load i64, i64* %inlen.addr, align 8
  %sub = sub i64 %5, 17
  store i64 %sub, i64* %mlen, align 8
  %6 = load i64, i64* %mlen, align 8
  %cmp7 = icmp ugt i64 %6, 4294967278
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  call void @sodium_misuse() #5
  unreachable

if.end9:                                          ; preds = %if.end6
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %7 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %7, i32 0, i32 1
  %arraydecay10 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce, i32 0, i32 0
  %8 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %8, i32 0, i32 0
  %arraydecay11 = getelementptr inbounds [32 x i8], [32 x i8]* %k, i32 0, i32 0
  %call = call i32 @crypto_stream_chacha20_ietf(i8* %arraydecay, i64 64, i8* %arraydecay10, i8* %arraydecay11)
  %arraydecay12 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call13 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay12)
  %arraydecay14 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay14, i32 64)
  %9 = load i8*, i8** %ad.addr, align 4
  %10 = load i64, i64* %adlen.addr, align 8
  %call15 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %9, i64 %10)
  %11 = load i64, i64* %adlen.addr, align 8
  %sub16 = sub i64 16, %11
  %and = and i64 %sub16, 15
  %call17 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and)
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay18, i8 0, i32 64, i1 false)
  %12 = load i8*, i8** %in.addr, align 4
  %arrayidx = getelementptr i8, i8* %12, i32 0
  %13 = load i8, i8* %arrayidx, align 1
  %arrayidx19 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 0
  store i8 %13, i8* %arrayidx19, align 16
  %arraydecay20 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay21 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %14 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce22 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %14, i32 0, i32 1
  %arraydecay23 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce22, i32 0, i32 0
  %15 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k24 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %15, i32 0, i32 0
  %arraydecay25 = getelementptr inbounds [32 x i8], [32 x i8]* %k24, i32 0, i32 0
  %call26 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %arraydecay20, i8* %arraydecay21, i64 64, i8* %arraydecay23, i32 1, i8* %arraydecay25)
  %arrayidx27 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %16 = load i8, i8* %arrayidx27, align 16
  store i8 %16, i8* %tag, align 1
  %17 = load i8*, i8** %in.addr, align 4
  %arrayidx28 = getelementptr i8, i8* %17, i32 0
  %18 = load i8, i8* %arrayidx28, align 1
  %arrayidx29 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 0
  store i8 %18, i8* %arrayidx29, align 16
  %arraydecay30 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %call31 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay30, i64 64)
  %19 = load i8*, i8** %in.addr, align 4
  %add.ptr = getelementptr i8, i8* %19, i32 1
  store i8* %add.ptr, i8** %c, align 4
  %20 = load i8*, i8** %c, align 4
  %21 = load i64, i64* %mlen, align 8
  %call32 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %20, i64 %21)
  %22 = load i64, i64* %mlen, align 8
  %add = add i64 4294967248, %22
  %and33 = and i64 %add, 15
  %call34 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and33)
  %arraydecay35 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %23 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay35, i64 %23)
  %arraydecay36 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call37 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay36, i64 8)
  %arraydecay38 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %24 = load i64, i64* %mlen, align 8
  %add39 = add i64 64, %24
  call void @store64_le(i8* %arraydecay38, i64 %add39)
  %arraydecay40 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call41 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay40, i64 8)
  %arraydecay42 = getelementptr inbounds [16 x i8], [16 x i8]* %mac, i32 0, i32 0
  %call43 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %poly1305_state, i8* %arraydecay42)
  %25 = bitcast %struct.crypto_onetimeauth_poly1305_state* %poly1305_state to i8*
  call void @sodium_memzero(i8* %25, i32 256)
  %26 = load i8*, i8** %c, align 4
  %27 = load i64, i64* %mlen, align 8
  %idx.ext = trunc i64 %27 to i32
  %add.ptr44 = getelementptr i8, i8* %26, i32 %idx.ext
  store i8* %add.ptr44, i8** %stored_mac, align 4
  %arraydecay45 = getelementptr inbounds [16 x i8], [16 x i8]* %mac, i32 0, i32 0
  %28 = load i8*, i8** %stored_mac, align 4
  %call46 = call i32 @sodium_memcmp(i8* %arraydecay45, i8* %28, i32 16)
  %cmp47 = icmp ne i32 %call46, 0
  br i1 %cmp47, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.end9
  %arraydecay49 = getelementptr inbounds [16 x i8], [16 x i8]* %mac, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay49, i32 16)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end50:                                         ; preds = %if.end9
  %29 = load i8*, i8** %m.addr, align 4
  %30 = load i8*, i8** %c, align 4
  %31 = load i64, i64* %mlen, align 8
  %32 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce51 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %32, i32 0, i32 1
  %arraydecay52 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce51, i32 0, i32 0
  %33 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %k53 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %33, i32 0, i32 0
  %arraydecay54 = getelementptr inbounds [32 x i8], [32 x i8]* %k53, i32 0, i32 0
  %call55 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %29, i8* %30, i64 %31, i8* %arraydecay52, i32 2, i8* %arraydecay54)
  %34 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce56 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %34, i32 0, i32 1
  %arraydecay57 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce56, i32 0, i32 0
  %add.ptr58 = getelementptr i8, i8* %arraydecay57, i32 4
  %arraydecay59 = getelementptr inbounds [16 x i8], [16 x i8]* %mac, i32 0, i32 0
  call void @xor_buf(i8* %add.ptr58, i8* %arraydecay59, i32 8)
  %35 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce60 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %35, i32 0, i32 1
  %arraydecay61 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce60, i32 0, i32 0
  call void @sodium_increment(i8* %arraydecay61, i32 4)
  %36 = load i8, i8* %tag, align 1
  %conv = zext i8 %36 to i32
  %and62 = and i32 %conv, 2
  %cmp63 = icmp ne i32 %and62, 0
  br i1 %cmp63, label %if.then68, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end50
  %37 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  %nonce65 = getelementptr inbounds %struct.crypto_secretstream_xchacha20poly1305_state, %struct.crypto_secretstream_xchacha20poly1305_state* %37, i32 0, i32 1
  %arraydecay66 = getelementptr inbounds [12 x i8], [12 x i8]* %nonce65, i32 0, i32 0
  %call67 = call i32 @sodium_is_zero(i8* %arraydecay66, i32 4)
  %tobool = icmp ne i32 %call67, 0
  br i1 %tobool, label %if.then68, label %if.end69

if.then68:                                        ; preds = %lor.lhs.false, %if.end50
  %38 = load %struct.crypto_secretstream_xchacha20poly1305_state*, %struct.crypto_secretstream_xchacha20poly1305_state** %state.addr, align 4
  call void @crypto_secretstream_xchacha20poly1305_rekey(%struct.crypto_secretstream_xchacha20poly1305_state* %38)
  br label %if.end69

if.end69:                                         ; preds = %if.then68, %lor.lhs.false
  %39 = load i64*, i64** %mlen_p.addr, align 4
  %cmp70 = icmp ne i64* %39, null
  br i1 %cmp70, label %if.then72, label %if.end73

if.then72:                                        ; preds = %if.end69
  %40 = load i64, i64* %mlen, align 8
  %41 = load i64*, i64** %mlen_p.addr, align 4
  store i64 %40, i64* %41, align 8
  br label %if.end73

if.end73:                                         ; preds = %if.then72, %if.end69
  %42 = load i8*, i8** %tag_p.addr, align 4
  %cmp74 = icmp ne i8* %42, null
  br i1 %cmp74, label %if.then76, label %if.end77

if.then76:                                        ; preds = %if.end73
  %43 = load i8, i8* %tag, align 1
  %44 = load i8*, i8** %tag_p.addr, align 4
  store i8 %43, i8* %44, align 1
  br label %if.end77

if.end77:                                         ; preds = %if.then76, %if.end73
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end77, %if.then48, %if.then5
  %45 = load i32, i32* %retval, align 4
  ret i32 %45
}

declare i32 @sodium_memcmp(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_statebytes() #0 {
entry:
  ret i32 52
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_abytes() #0 {
entry:
  ret i32 17
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_headerbytes() #0 {
entry:
  ret i32 24
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretstream_xchacha20poly1305_messagebytes_max() #0 {
entry:
  ret i32 -18
}

; Function Attrs: noinline nounwind optnone
define zeroext i8 @crypto_secretstream_xchacha20poly1305_tag_message() #0 {
entry:
  ret i8 0
}

; Function Attrs: noinline nounwind optnone
define zeroext i8 @crypto_secretstream_xchacha20poly1305_tag_push() #0 {
entry:
  ret i8 1
}

; Function Attrs: noinline nounwind optnone
define zeroext i8 @crypto_secretstream_xchacha20poly1305_tag_rekey() #0 {
entry:
  ret i8 2
}

; Function Attrs: noinline nounwind optnone
define zeroext i8 @crypto_secretstream_xchacha20poly1305_tag_final() #0 {
entry:
  ret i8 3
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
