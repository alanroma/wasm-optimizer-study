; ModuleID = 'crypto_hash/sha512/cp/hash_sha512_cp.c'
source_filename = "crypto_hash/sha512/cp/hash_sha512_cp.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }

@crypto_hash_sha512_init.sha512_initial_state = internal constant [8 x i64] [i64 7640891576956012808, i64 -4942790177534073029, i64 4354685564936845355, i64 -6534734903238641935, i64 5840696475078001361, i64 -7276294671716946913, i64 2270897969802886507, i64 6620516959819538809], align 16
@Krnd = internal constant [80 x i64] [i64 4794697086780616226, i64 8158064640168781261, i64 -5349999486874862801, i64 -1606136188198331460, i64 4131703408338449720, i64 6480981068601479193, i64 -7908458776815382629, i64 -6116909921290321640, i64 -2880145864133508542, i64 1334009975649890238, i64 2608012711638119052, i64 6128411473006802146, i64 8268148722764581231, i64 -9160688886553864527, i64 -7215885187991268811, i64 -4495734319001033068, i64 -1973867731355612462, i64 -1171420211273849373, i64 1135362057144423861, i64 2597628984639134821, i64 3308224258029322869, i64 5365058923640841347, i64 6679025012923562964, i64 8573033837759648693, i64 -7476448914759557205, i64 -6327057829258317296, i64 -5763719355590565569, i64 -4658551843659510044, i64 -4116276920077217854, i64 -3051310485924567259, i64 489312712824947311, i64 1452737877330783856, i64 2861767655752347644, i64 3322285676063803686, i64 5560940570517711597, i64 5996557281743188959, i64 7280758554555802590, i64 8532644243296465576, i64 -9096487096722542874, i64 -7894198246740708037, i64 -6719396339535248540, i64 -6333637450476146687, i64 -4446306890439682159, i64 -4076793802049405392, i64 -3345356375505022440, i64 -2983346525034927856, i64 -860691631967231958, i64 1182934255886127544, i64 1847814050463011016, i64 2177327727835720531, i64 2830643537854262169, i64 3796741975233480872, i64 4115178125766777443, i64 5681478168544905931, i64 6601373596472566643, i64 7507060721942968483, i64 8399075790359081724, i64 8693463985226723168, i64 -8878714635349349518, i64 -8302665154208450068, i64 -8016688836872298968, i64 -6606660893046293015, i64 -4685533653050689259, i64 -4147400797238176981, i64 -3880063495543823972, i64 -3348786107499101689, i64 -1523767162380948706, i64 -757361751448694408, i64 500013540394364858, i64 748580250866718886, i64 1242879168328830382, i64 1977374033974150939, i64 2944078676154940804, i64 3659926193048069267, i64 4368137639120453308, i64 4836135668995329356, i64 5532061633213252278, i64 6448918945643986474, i64 6902733635092675308, i64 7801388544844847127], align 16
@PAD = internal constant <{ i8, [127 x i8] }> <{ i8 -128, [127 x i8] zeroinitializer }>, align 16

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* nonnull %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha512_state*, align 4
  store %struct.crypto_hash_sha512_state* %state, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %0 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %0, i32 0, i32 1
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %count, i32 0, i32 1
  store i64 0, i64* %arrayidx, align 8
  %1 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count1 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %1, i32 0, i32 1
  %arrayidx2 = getelementptr [2 x i64], [2 x i64]* %count1, i32 0, i32 0
  store i64 0, i64* %arrayidx2, align 8
  %2 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state3 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %2, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i64], [8 x i64]* %state3, i32 0, i32 0
  %3 = bitcast i64* %arraydecay to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 16 bitcast ([8 x i64]* @crypto_hash_sha512_init.sha512_initial_state to i8*), i32 64, i1 false)
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.crypto_hash_sha512_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %tmp64 = alloca [88 x i64], align 16
  %bitlen = alloca [2 x i64], align 16
  %i = alloca i64, align 8
  %r = alloca i64, align 8
  store %struct.crypto_hash_sha512_state* %state, %struct.crypto_hash_sha512_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load i64, i64* %inlen.addr, align 8
  %cmp = icmp ule i64 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %1, i32 0, i32 1
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %count, i32 0, i32 1
  %2 = load i64, i64* %arrayidx, align 8
  %shr = lshr i64 %2, 3
  %and = and i64 %shr, 127
  store i64 %and, i64* %r, align 8
  %3 = load i64, i64* %inlen.addr, align 8
  %shl = shl i64 %3, 3
  %arrayidx1 = getelementptr [2 x i64], [2 x i64]* %bitlen, i32 0, i32 1
  store i64 %shl, i64* %arrayidx1, align 8
  %4 = load i64, i64* %inlen.addr, align 8
  %shr2 = lshr i64 %4, 61
  %arrayidx3 = getelementptr [2 x i64], [2 x i64]* %bitlen, i32 0, i32 0
  store i64 %shr2, i64* %arrayidx3, align 16
  %arrayidx4 = getelementptr [2 x i64], [2 x i64]* %bitlen, i32 0, i32 1
  %5 = load i64, i64* %arrayidx4, align 8
  %6 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count5 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %6, i32 0, i32 1
  %arrayidx6 = getelementptr [2 x i64], [2 x i64]* %count5, i32 0, i32 1
  %7 = load i64, i64* %arrayidx6, align 8
  %add = add i64 %7, %5
  store i64 %add, i64* %arrayidx6, align 8
  %arrayidx7 = getelementptr [2 x i64], [2 x i64]* %bitlen, i32 0, i32 1
  %8 = load i64, i64* %arrayidx7, align 8
  %cmp8 = icmp ult i64 %add, %8
  br i1 %cmp8, label %if.then9, label %if.end12

if.then9:                                         ; preds = %if.end
  %9 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count10 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %9, i32 0, i32 1
  %arrayidx11 = getelementptr [2 x i64], [2 x i64]* %count10, i32 0, i32 0
  %10 = load i64, i64* %arrayidx11, align 8
  %inc = add i64 %10, 1
  store i64 %inc, i64* %arrayidx11, align 8
  br label %if.end12

if.end12:                                         ; preds = %if.then9, %if.end
  %arrayidx13 = getelementptr [2 x i64], [2 x i64]* %bitlen, i32 0, i32 0
  %11 = load i64, i64* %arrayidx13, align 16
  %12 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count14 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %12, i32 0, i32 1
  %arrayidx15 = getelementptr [2 x i64], [2 x i64]* %count14, i32 0, i32 0
  %13 = load i64, i64* %arrayidx15, align 8
  %add16 = add i64 %13, %11
  store i64 %add16, i64* %arrayidx15, align 8
  %14 = load i64, i64* %inlen.addr, align 8
  %15 = load i64, i64* %r, align 8
  %sub = sub i64 128, %15
  %cmp17 = icmp ult i64 %14, %sub
  br i1 %cmp17, label %if.then18, label %if.end25

if.then18:                                        ; preds = %if.end12
  store i64 0, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then18
  %16 = load i64, i64* %i, align 8
  %17 = load i64, i64* %inlen.addr, align 8
  %cmp19 = icmp ult i64 %16, %17
  br i1 %cmp19, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i8*, i8** %in.addr, align 4
  %19 = load i64, i64* %i, align 8
  %idxprom = trunc i64 %19 to i32
  %arrayidx20 = getelementptr i8, i8* %18, i32 %idxprom
  %20 = load i8, i8* %arrayidx20, align 1
  %21 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %21, i32 0, i32 2
  %22 = load i64, i64* %r, align 8
  %23 = load i64, i64* %i, align 8
  %add21 = add i64 %22, %23
  %idxprom22 = trunc i64 %add21 to i32
  %arrayidx23 = getelementptr [128 x i8], [128 x i8]* %buf, i32 0, i32 %idxprom22
  store i8 %20, i8* %arrayidx23, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i64, i64* %i, align 8
  %inc24 = add i64 %24, 1
  store i64 %inc24, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end12
  store i64 0, i64* %i, align 8
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc36, %if.end25
  %25 = load i64, i64* %i, align 8
  %26 = load i64, i64* %r, align 8
  %sub27 = sub i64 128, %26
  %cmp28 = icmp ult i64 %25, %sub27
  br i1 %cmp28, label %for.body29, label %for.end38

for.body29:                                       ; preds = %for.cond26
  %27 = load i8*, i8** %in.addr, align 4
  %28 = load i64, i64* %i, align 8
  %idxprom30 = trunc i64 %28 to i32
  %arrayidx31 = getelementptr i8, i8* %27, i32 %idxprom30
  %29 = load i8, i8* %arrayidx31, align 1
  %30 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf32 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %30, i32 0, i32 2
  %31 = load i64, i64* %r, align 8
  %32 = load i64, i64* %i, align 8
  %add33 = add i64 %31, %32
  %idxprom34 = trunc i64 %add33 to i32
  %arrayidx35 = getelementptr [128 x i8], [128 x i8]* %buf32, i32 0, i32 %idxprom34
  store i8 %29, i8* %arrayidx35, align 1
  br label %for.inc36

for.inc36:                                        ; preds = %for.body29
  %33 = load i64, i64* %i, align 8
  %inc37 = add i64 %33, 1
  store i64 %inc37, i64* %i, align 8
  br label %for.cond26

for.end38:                                        ; preds = %for.cond26
  %34 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state39 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %34, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i64], [8 x i64]* %state39, i32 0, i32 0
  %35 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf40 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %35, i32 0, i32 2
  %arraydecay41 = getelementptr inbounds [128 x i8], [128 x i8]* %buf40, i32 0, i32 0
  %arrayidx42 = getelementptr [88 x i64], [88 x i64]* %tmp64, i32 0, i32 0
  %arrayidx43 = getelementptr [88 x i64], [88 x i64]* %tmp64, i32 0, i32 80
  call void @SHA512_Transform(i64* %arraydecay, i8* %arraydecay41, i64* %arrayidx42, i64* %arrayidx43)
  %36 = load i64, i64* %r, align 8
  %sub44 = sub i64 128, %36
  %37 = load i8*, i8** %in.addr, align 4
  %idx.ext = trunc i64 %sub44 to i32
  %add.ptr = getelementptr i8, i8* %37, i32 %idx.ext
  store i8* %add.ptr, i8** %in.addr, align 4
  %38 = load i64, i64* %r, align 8
  %sub45 = sub i64 128, %38
  %39 = load i64, i64* %inlen.addr, align 8
  %sub46 = sub i64 %39, %sub45
  store i64 %sub46, i64* %inlen.addr, align 8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.end38
  %40 = load i64, i64* %inlen.addr, align 8
  %cmp47 = icmp uge i64 %40, 128
  br i1 %cmp47, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %41 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state48 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %41, i32 0, i32 0
  %arraydecay49 = getelementptr inbounds [8 x i64], [8 x i64]* %state48, i32 0, i32 0
  %42 = load i8*, i8** %in.addr, align 4
  %arrayidx50 = getelementptr [88 x i64], [88 x i64]* %tmp64, i32 0, i32 0
  %arrayidx51 = getelementptr [88 x i64], [88 x i64]* %tmp64, i32 0, i32 80
  call void @SHA512_Transform(i64* %arraydecay49, i8* %42, i64* %arrayidx50, i64* %arrayidx51)
  %43 = load i8*, i8** %in.addr, align 4
  %add.ptr52 = getelementptr i8, i8* %43, i32 128
  store i8* %add.ptr52, i8** %in.addr, align 4
  %44 = load i64, i64* %inlen.addr, align 8
  %sub53 = sub i64 %44, 128
  store i64 %sub53, i64* %inlen.addr, align 8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %45 = load i64, i64* %inlen.addr, align 8
  %and54 = and i64 %45, 127
  store i64 %and54, i64* %inlen.addr, align 8
  store i64 0, i64* %i, align 8
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc63, %while.end
  %46 = load i64, i64* %i, align 8
  %47 = load i64, i64* %inlen.addr, align 8
  %cmp56 = icmp ult i64 %46, %47
  br i1 %cmp56, label %for.body57, label %for.end65

for.body57:                                       ; preds = %for.cond55
  %48 = load i8*, i8** %in.addr, align 4
  %49 = load i64, i64* %i, align 8
  %idxprom58 = trunc i64 %49 to i32
  %arrayidx59 = getelementptr i8, i8* %48, i32 %idxprom58
  %50 = load i8, i8* %arrayidx59, align 1
  %51 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf60 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %51, i32 0, i32 2
  %52 = load i64, i64* %i, align 8
  %idxprom61 = trunc i64 %52 to i32
  %arrayidx62 = getelementptr [128 x i8], [128 x i8]* %buf60, i32 0, i32 %idxprom61
  store i8 %50, i8* %arrayidx62, align 1
  br label %for.inc63

for.inc63:                                        ; preds = %for.body57
  %53 = load i64, i64* %i, align 8
  %inc64 = add i64 %53, 1
  store i64 %inc64, i64* %i, align 8
  br label %for.cond55

for.end65:                                        ; preds = %for.cond55
  %arraydecay66 = getelementptr inbounds [88 x i64], [88 x i64]* %tmp64, i32 0, i32 0
  %54 = bitcast i64* %arraydecay66 to i8*
  call void @sodium_memzero(i8* %54, i32 704)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end65, %for.end, %if.then
  %55 = load i32, i32* %retval, align 4
  ret i32 %55
}

; Function Attrs: noinline nounwind optnone
define internal void @SHA512_Transform(i64* %state, i8* %block, i64* %W, i64* %S) #0 {
entry:
  %state.addr = alloca i64*, align 4
  %block.addr = alloca i8*, align 4
  %W.addr = alloca i64*, align 4
  %S.addr = alloca i64*, align 4
  %i = alloca i32, align 4
  store i64* %state, i64** %state.addr, align 4
  store i8* %block, i8** %block.addr, align 4
  store i64* %W, i64** %W.addr, align 4
  store i64* %S, i64** %S.addr, align 4
  %0 = load i64*, i64** %W.addr, align 4
  %1 = load i8*, i8** %block.addr, align 4
  call void @be64dec_vect(i64* %0, i8* %1, i32 128)
  %2 = load i64*, i64** %S.addr, align 4
  %3 = bitcast i64* %2 to i8*
  %4 = load i64*, i64** %state.addr, align 4
  %5 = bitcast i64* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 8 %5, i32 64, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 80
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i64*, i64** %S.addr, align 4
  %arrayidx = getelementptr i64, i64* %7, i32 4
  %8 = load i64, i64* %arrayidx, align 8
  %call = call i64 @rotr64(i64 %8, i32 14)
  %9 = load i64*, i64** %S.addr, align 4
  %arrayidx1 = getelementptr i64, i64* %9, i32 4
  %10 = load i64, i64* %arrayidx1, align 8
  %call2 = call i64 @rotr64(i64 %10, i32 18)
  %xor = xor i64 %call, %call2
  %11 = load i64*, i64** %S.addr, align 4
  %arrayidx3 = getelementptr i64, i64* %11, i32 4
  %12 = load i64, i64* %arrayidx3, align 8
  %call4 = call i64 @rotr64(i64 %12, i32 41)
  %xor5 = xor i64 %xor, %call4
  %13 = load i64*, i64** %S.addr, align 4
  %arrayidx6 = getelementptr i64, i64* %13, i32 4
  %14 = load i64, i64* %arrayidx6, align 8
  %15 = load i64*, i64** %S.addr, align 4
  %arrayidx7 = getelementptr i64, i64* %15, i32 5
  %16 = load i64, i64* %arrayidx7, align 8
  %17 = load i64*, i64** %S.addr, align 4
  %arrayidx8 = getelementptr i64, i64* %17, i32 6
  %18 = load i64, i64* %arrayidx8, align 8
  %xor9 = xor i64 %16, %18
  %and = and i64 %14, %xor9
  %19 = load i64*, i64** %S.addr, align 4
  %arrayidx10 = getelementptr i64, i64* %19, i32 6
  %20 = load i64, i64* %arrayidx10, align 8
  %xor11 = xor i64 %and, %20
  %add = add i64 %xor5, %xor11
  %21 = load i64*, i64** %W.addr, align 4
  %22 = load i32, i32* %i, align 4
  %add12 = add i32 0, %22
  %arrayidx13 = getelementptr i64, i64* %21, i32 %add12
  %23 = load i64, i64* %arrayidx13, align 8
  %add14 = add i64 %add, %23
  %24 = load i32, i32* %i, align 4
  %add15 = add i32 0, %24
  %arrayidx16 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add15
  %25 = load i64, i64* %arrayidx16, align 8
  %add17 = add i64 %add14, %25
  %26 = load i64*, i64** %S.addr, align 4
  %arrayidx18 = getelementptr i64, i64* %26, i32 7
  %27 = load i64, i64* %arrayidx18, align 8
  %add19 = add i64 %27, %add17
  store i64 %add19, i64* %arrayidx18, align 8
  %28 = load i64*, i64** %S.addr, align 4
  %arrayidx20 = getelementptr i64, i64* %28, i32 7
  %29 = load i64, i64* %arrayidx20, align 8
  %30 = load i64*, i64** %S.addr, align 4
  %arrayidx21 = getelementptr i64, i64* %30, i32 3
  %31 = load i64, i64* %arrayidx21, align 8
  %add22 = add i64 %31, %29
  store i64 %add22, i64* %arrayidx21, align 8
  %32 = load i64*, i64** %S.addr, align 4
  %arrayidx23 = getelementptr i64, i64* %32, i32 0
  %33 = load i64, i64* %arrayidx23, align 8
  %call24 = call i64 @rotr64(i64 %33, i32 28)
  %34 = load i64*, i64** %S.addr, align 4
  %arrayidx25 = getelementptr i64, i64* %34, i32 0
  %35 = load i64, i64* %arrayidx25, align 8
  %call26 = call i64 @rotr64(i64 %35, i32 34)
  %xor27 = xor i64 %call24, %call26
  %36 = load i64*, i64** %S.addr, align 4
  %arrayidx28 = getelementptr i64, i64* %36, i32 0
  %37 = load i64, i64* %arrayidx28, align 8
  %call29 = call i64 @rotr64(i64 %37, i32 39)
  %xor30 = xor i64 %xor27, %call29
  %38 = load i64*, i64** %S.addr, align 4
  %arrayidx31 = getelementptr i64, i64* %38, i32 0
  %39 = load i64, i64* %arrayidx31, align 8
  %40 = load i64*, i64** %S.addr, align 4
  %arrayidx32 = getelementptr i64, i64* %40, i32 1
  %41 = load i64, i64* %arrayidx32, align 8
  %42 = load i64*, i64** %S.addr, align 4
  %arrayidx33 = getelementptr i64, i64* %42, i32 2
  %43 = load i64, i64* %arrayidx33, align 8
  %or = or i64 %41, %43
  %and34 = and i64 %39, %or
  %44 = load i64*, i64** %S.addr, align 4
  %arrayidx35 = getelementptr i64, i64* %44, i32 1
  %45 = load i64, i64* %arrayidx35, align 8
  %46 = load i64*, i64** %S.addr, align 4
  %arrayidx36 = getelementptr i64, i64* %46, i32 2
  %47 = load i64, i64* %arrayidx36, align 8
  %and37 = and i64 %45, %47
  %or38 = or i64 %and34, %and37
  %add39 = add i64 %xor30, %or38
  %48 = load i64*, i64** %S.addr, align 4
  %arrayidx40 = getelementptr i64, i64* %48, i32 7
  %49 = load i64, i64* %arrayidx40, align 8
  %add41 = add i64 %49, %add39
  store i64 %add41, i64* %arrayidx40, align 8
  %50 = load i64*, i64** %S.addr, align 4
  %arrayidx42 = getelementptr i64, i64* %50, i32 3
  %51 = load i64, i64* %arrayidx42, align 8
  %call43 = call i64 @rotr64(i64 %51, i32 14)
  %52 = load i64*, i64** %S.addr, align 4
  %arrayidx44 = getelementptr i64, i64* %52, i32 3
  %53 = load i64, i64* %arrayidx44, align 8
  %call45 = call i64 @rotr64(i64 %53, i32 18)
  %xor46 = xor i64 %call43, %call45
  %54 = load i64*, i64** %S.addr, align 4
  %arrayidx47 = getelementptr i64, i64* %54, i32 3
  %55 = load i64, i64* %arrayidx47, align 8
  %call48 = call i64 @rotr64(i64 %55, i32 41)
  %xor49 = xor i64 %xor46, %call48
  %56 = load i64*, i64** %S.addr, align 4
  %arrayidx50 = getelementptr i64, i64* %56, i32 3
  %57 = load i64, i64* %arrayidx50, align 8
  %58 = load i64*, i64** %S.addr, align 4
  %arrayidx51 = getelementptr i64, i64* %58, i32 4
  %59 = load i64, i64* %arrayidx51, align 8
  %60 = load i64*, i64** %S.addr, align 4
  %arrayidx52 = getelementptr i64, i64* %60, i32 5
  %61 = load i64, i64* %arrayidx52, align 8
  %xor53 = xor i64 %59, %61
  %and54 = and i64 %57, %xor53
  %62 = load i64*, i64** %S.addr, align 4
  %arrayidx55 = getelementptr i64, i64* %62, i32 5
  %63 = load i64, i64* %arrayidx55, align 8
  %xor56 = xor i64 %and54, %63
  %add57 = add i64 %xor49, %xor56
  %64 = load i64*, i64** %W.addr, align 4
  %65 = load i32, i32* %i, align 4
  %add58 = add i32 1, %65
  %arrayidx59 = getelementptr i64, i64* %64, i32 %add58
  %66 = load i64, i64* %arrayidx59, align 8
  %add60 = add i64 %add57, %66
  %67 = load i32, i32* %i, align 4
  %add61 = add i32 1, %67
  %arrayidx62 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add61
  %68 = load i64, i64* %arrayidx62, align 8
  %add63 = add i64 %add60, %68
  %69 = load i64*, i64** %S.addr, align 4
  %arrayidx64 = getelementptr i64, i64* %69, i32 6
  %70 = load i64, i64* %arrayidx64, align 8
  %add65 = add i64 %70, %add63
  store i64 %add65, i64* %arrayidx64, align 8
  %71 = load i64*, i64** %S.addr, align 4
  %arrayidx66 = getelementptr i64, i64* %71, i32 6
  %72 = load i64, i64* %arrayidx66, align 8
  %73 = load i64*, i64** %S.addr, align 4
  %arrayidx67 = getelementptr i64, i64* %73, i32 2
  %74 = load i64, i64* %arrayidx67, align 8
  %add68 = add i64 %74, %72
  store i64 %add68, i64* %arrayidx67, align 8
  %75 = load i64*, i64** %S.addr, align 4
  %arrayidx69 = getelementptr i64, i64* %75, i32 7
  %76 = load i64, i64* %arrayidx69, align 8
  %call70 = call i64 @rotr64(i64 %76, i32 28)
  %77 = load i64*, i64** %S.addr, align 4
  %arrayidx71 = getelementptr i64, i64* %77, i32 7
  %78 = load i64, i64* %arrayidx71, align 8
  %call72 = call i64 @rotr64(i64 %78, i32 34)
  %xor73 = xor i64 %call70, %call72
  %79 = load i64*, i64** %S.addr, align 4
  %arrayidx74 = getelementptr i64, i64* %79, i32 7
  %80 = load i64, i64* %arrayidx74, align 8
  %call75 = call i64 @rotr64(i64 %80, i32 39)
  %xor76 = xor i64 %xor73, %call75
  %81 = load i64*, i64** %S.addr, align 4
  %arrayidx77 = getelementptr i64, i64* %81, i32 7
  %82 = load i64, i64* %arrayidx77, align 8
  %83 = load i64*, i64** %S.addr, align 4
  %arrayidx78 = getelementptr i64, i64* %83, i32 0
  %84 = load i64, i64* %arrayidx78, align 8
  %85 = load i64*, i64** %S.addr, align 4
  %arrayidx79 = getelementptr i64, i64* %85, i32 1
  %86 = load i64, i64* %arrayidx79, align 8
  %or80 = or i64 %84, %86
  %and81 = and i64 %82, %or80
  %87 = load i64*, i64** %S.addr, align 4
  %arrayidx82 = getelementptr i64, i64* %87, i32 0
  %88 = load i64, i64* %arrayidx82, align 8
  %89 = load i64*, i64** %S.addr, align 4
  %arrayidx83 = getelementptr i64, i64* %89, i32 1
  %90 = load i64, i64* %arrayidx83, align 8
  %and84 = and i64 %88, %90
  %or85 = or i64 %and81, %and84
  %add86 = add i64 %xor76, %or85
  %91 = load i64*, i64** %S.addr, align 4
  %arrayidx87 = getelementptr i64, i64* %91, i32 6
  %92 = load i64, i64* %arrayidx87, align 8
  %add88 = add i64 %92, %add86
  store i64 %add88, i64* %arrayidx87, align 8
  %93 = load i64*, i64** %S.addr, align 4
  %arrayidx89 = getelementptr i64, i64* %93, i32 2
  %94 = load i64, i64* %arrayidx89, align 8
  %call90 = call i64 @rotr64(i64 %94, i32 14)
  %95 = load i64*, i64** %S.addr, align 4
  %arrayidx91 = getelementptr i64, i64* %95, i32 2
  %96 = load i64, i64* %arrayidx91, align 8
  %call92 = call i64 @rotr64(i64 %96, i32 18)
  %xor93 = xor i64 %call90, %call92
  %97 = load i64*, i64** %S.addr, align 4
  %arrayidx94 = getelementptr i64, i64* %97, i32 2
  %98 = load i64, i64* %arrayidx94, align 8
  %call95 = call i64 @rotr64(i64 %98, i32 41)
  %xor96 = xor i64 %xor93, %call95
  %99 = load i64*, i64** %S.addr, align 4
  %arrayidx97 = getelementptr i64, i64* %99, i32 2
  %100 = load i64, i64* %arrayidx97, align 8
  %101 = load i64*, i64** %S.addr, align 4
  %arrayidx98 = getelementptr i64, i64* %101, i32 3
  %102 = load i64, i64* %arrayidx98, align 8
  %103 = load i64*, i64** %S.addr, align 4
  %arrayidx99 = getelementptr i64, i64* %103, i32 4
  %104 = load i64, i64* %arrayidx99, align 8
  %xor100 = xor i64 %102, %104
  %and101 = and i64 %100, %xor100
  %105 = load i64*, i64** %S.addr, align 4
  %arrayidx102 = getelementptr i64, i64* %105, i32 4
  %106 = load i64, i64* %arrayidx102, align 8
  %xor103 = xor i64 %and101, %106
  %add104 = add i64 %xor96, %xor103
  %107 = load i64*, i64** %W.addr, align 4
  %108 = load i32, i32* %i, align 4
  %add105 = add i32 2, %108
  %arrayidx106 = getelementptr i64, i64* %107, i32 %add105
  %109 = load i64, i64* %arrayidx106, align 8
  %add107 = add i64 %add104, %109
  %110 = load i32, i32* %i, align 4
  %add108 = add i32 2, %110
  %arrayidx109 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add108
  %111 = load i64, i64* %arrayidx109, align 8
  %add110 = add i64 %add107, %111
  %112 = load i64*, i64** %S.addr, align 4
  %arrayidx111 = getelementptr i64, i64* %112, i32 5
  %113 = load i64, i64* %arrayidx111, align 8
  %add112 = add i64 %113, %add110
  store i64 %add112, i64* %arrayidx111, align 8
  %114 = load i64*, i64** %S.addr, align 4
  %arrayidx113 = getelementptr i64, i64* %114, i32 5
  %115 = load i64, i64* %arrayidx113, align 8
  %116 = load i64*, i64** %S.addr, align 4
  %arrayidx114 = getelementptr i64, i64* %116, i32 1
  %117 = load i64, i64* %arrayidx114, align 8
  %add115 = add i64 %117, %115
  store i64 %add115, i64* %arrayidx114, align 8
  %118 = load i64*, i64** %S.addr, align 4
  %arrayidx116 = getelementptr i64, i64* %118, i32 6
  %119 = load i64, i64* %arrayidx116, align 8
  %call117 = call i64 @rotr64(i64 %119, i32 28)
  %120 = load i64*, i64** %S.addr, align 4
  %arrayidx118 = getelementptr i64, i64* %120, i32 6
  %121 = load i64, i64* %arrayidx118, align 8
  %call119 = call i64 @rotr64(i64 %121, i32 34)
  %xor120 = xor i64 %call117, %call119
  %122 = load i64*, i64** %S.addr, align 4
  %arrayidx121 = getelementptr i64, i64* %122, i32 6
  %123 = load i64, i64* %arrayidx121, align 8
  %call122 = call i64 @rotr64(i64 %123, i32 39)
  %xor123 = xor i64 %xor120, %call122
  %124 = load i64*, i64** %S.addr, align 4
  %arrayidx124 = getelementptr i64, i64* %124, i32 6
  %125 = load i64, i64* %arrayidx124, align 8
  %126 = load i64*, i64** %S.addr, align 4
  %arrayidx125 = getelementptr i64, i64* %126, i32 7
  %127 = load i64, i64* %arrayidx125, align 8
  %128 = load i64*, i64** %S.addr, align 4
  %arrayidx126 = getelementptr i64, i64* %128, i32 0
  %129 = load i64, i64* %arrayidx126, align 8
  %or127 = or i64 %127, %129
  %and128 = and i64 %125, %or127
  %130 = load i64*, i64** %S.addr, align 4
  %arrayidx129 = getelementptr i64, i64* %130, i32 7
  %131 = load i64, i64* %arrayidx129, align 8
  %132 = load i64*, i64** %S.addr, align 4
  %arrayidx130 = getelementptr i64, i64* %132, i32 0
  %133 = load i64, i64* %arrayidx130, align 8
  %and131 = and i64 %131, %133
  %or132 = or i64 %and128, %and131
  %add133 = add i64 %xor123, %or132
  %134 = load i64*, i64** %S.addr, align 4
  %arrayidx134 = getelementptr i64, i64* %134, i32 5
  %135 = load i64, i64* %arrayidx134, align 8
  %add135 = add i64 %135, %add133
  store i64 %add135, i64* %arrayidx134, align 8
  %136 = load i64*, i64** %S.addr, align 4
  %arrayidx136 = getelementptr i64, i64* %136, i32 1
  %137 = load i64, i64* %arrayidx136, align 8
  %call137 = call i64 @rotr64(i64 %137, i32 14)
  %138 = load i64*, i64** %S.addr, align 4
  %arrayidx138 = getelementptr i64, i64* %138, i32 1
  %139 = load i64, i64* %arrayidx138, align 8
  %call139 = call i64 @rotr64(i64 %139, i32 18)
  %xor140 = xor i64 %call137, %call139
  %140 = load i64*, i64** %S.addr, align 4
  %arrayidx141 = getelementptr i64, i64* %140, i32 1
  %141 = load i64, i64* %arrayidx141, align 8
  %call142 = call i64 @rotr64(i64 %141, i32 41)
  %xor143 = xor i64 %xor140, %call142
  %142 = load i64*, i64** %S.addr, align 4
  %arrayidx144 = getelementptr i64, i64* %142, i32 1
  %143 = load i64, i64* %arrayidx144, align 8
  %144 = load i64*, i64** %S.addr, align 4
  %arrayidx145 = getelementptr i64, i64* %144, i32 2
  %145 = load i64, i64* %arrayidx145, align 8
  %146 = load i64*, i64** %S.addr, align 4
  %arrayidx146 = getelementptr i64, i64* %146, i32 3
  %147 = load i64, i64* %arrayidx146, align 8
  %xor147 = xor i64 %145, %147
  %and148 = and i64 %143, %xor147
  %148 = load i64*, i64** %S.addr, align 4
  %arrayidx149 = getelementptr i64, i64* %148, i32 3
  %149 = load i64, i64* %arrayidx149, align 8
  %xor150 = xor i64 %and148, %149
  %add151 = add i64 %xor143, %xor150
  %150 = load i64*, i64** %W.addr, align 4
  %151 = load i32, i32* %i, align 4
  %add152 = add i32 3, %151
  %arrayidx153 = getelementptr i64, i64* %150, i32 %add152
  %152 = load i64, i64* %arrayidx153, align 8
  %add154 = add i64 %add151, %152
  %153 = load i32, i32* %i, align 4
  %add155 = add i32 3, %153
  %arrayidx156 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add155
  %154 = load i64, i64* %arrayidx156, align 8
  %add157 = add i64 %add154, %154
  %155 = load i64*, i64** %S.addr, align 4
  %arrayidx158 = getelementptr i64, i64* %155, i32 4
  %156 = load i64, i64* %arrayidx158, align 8
  %add159 = add i64 %156, %add157
  store i64 %add159, i64* %arrayidx158, align 8
  %157 = load i64*, i64** %S.addr, align 4
  %arrayidx160 = getelementptr i64, i64* %157, i32 4
  %158 = load i64, i64* %arrayidx160, align 8
  %159 = load i64*, i64** %S.addr, align 4
  %arrayidx161 = getelementptr i64, i64* %159, i32 0
  %160 = load i64, i64* %arrayidx161, align 8
  %add162 = add i64 %160, %158
  store i64 %add162, i64* %arrayidx161, align 8
  %161 = load i64*, i64** %S.addr, align 4
  %arrayidx163 = getelementptr i64, i64* %161, i32 5
  %162 = load i64, i64* %arrayidx163, align 8
  %call164 = call i64 @rotr64(i64 %162, i32 28)
  %163 = load i64*, i64** %S.addr, align 4
  %arrayidx165 = getelementptr i64, i64* %163, i32 5
  %164 = load i64, i64* %arrayidx165, align 8
  %call166 = call i64 @rotr64(i64 %164, i32 34)
  %xor167 = xor i64 %call164, %call166
  %165 = load i64*, i64** %S.addr, align 4
  %arrayidx168 = getelementptr i64, i64* %165, i32 5
  %166 = load i64, i64* %arrayidx168, align 8
  %call169 = call i64 @rotr64(i64 %166, i32 39)
  %xor170 = xor i64 %xor167, %call169
  %167 = load i64*, i64** %S.addr, align 4
  %arrayidx171 = getelementptr i64, i64* %167, i32 5
  %168 = load i64, i64* %arrayidx171, align 8
  %169 = load i64*, i64** %S.addr, align 4
  %arrayidx172 = getelementptr i64, i64* %169, i32 6
  %170 = load i64, i64* %arrayidx172, align 8
  %171 = load i64*, i64** %S.addr, align 4
  %arrayidx173 = getelementptr i64, i64* %171, i32 7
  %172 = load i64, i64* %arrayidx173, align 8
  %or174 = or i64 %170, %172
  %and175 = and i64 %168, %or174
  %173 = load i64*, i64** %S.addr, align 4
  %arrayidx176 = getelementptr i64, i64* %173, i32 6
  %174 = load i64, i64* %arrayidx176, align 8
  %175 = load i64*, i64** %S.addr, align 4
  %arrayidx177 = getelementptr i64, i64* %175, i32 7
  %176 = load i64, i64* %arrayidx177, align 8
  %and178 = and i64 %174, %176
  %or179 = or i64 %and175, %and178
  %add180 = add i64 %xor170, %or179
  %177 = load i64*, i64** %S.addr, align 4
  %arrayidx181 = getelementptr i64, i64* %177, i32 4
  %178 = load i64, i64* %arrayidx181, align 8
  %add182 = add i64 %178, %add180
  store i64 %add182, i64* %arrayidx181, align 8
  %179 = load i64*, i64** %S.addr, align 4
  %arrayidx183 = getelementptr i64, i64* %179, i32 0
  %180 = load i64, i64* %arrayidx183, align 8
  %call184 = call i64 @rotr64(i64 %180, i32 14)
  %181 = load i64*, i64** %S.addr, align 4
  %arrayidx185 = getelementptr i64, i64* %181, i32 0
  %182 = load i64, i64* %arrayidx185, align 8
  %call186 = call i64 @rotr64(i64 %182, i32 18)
  %xor187 = xor i64 %call184, %call186
  %183 = load i64*, i64** %S.addr, align 4
  %arrayidx188 = getelementptr i64, i64* %183, i32 0
  %184 = load i64, i64* %arrayidx188, align 8
  %call189 = call i64 @rotr64(i64 %184, i32 41)
  %xor190 = xor i64 %xor187, %call189
  %185 = load i64*, i64** %S.addr, align 4
  %arrayidx191 = getelementptr i64, i64* %185, i32 0
  %186 = load i64, i64* %arrayidx191, align 8
  %187 = load i64*, i64** %S.addr, align 4
  %arrayidx192 = getelementptr i64, i64* %187, i32 1
  %188 = load i64, i64* %arrayidx192, align 8
  %189 = load i64*, i64** %S.addr, align 4
  %arrayidx193 = getelementptr i64, i64* %189, i32 2
  %190 = load i64, i64* %arrayidx193, align 8
  %xor194 = xor i64 %188, %190
  %and195 = and i64 %186, %xor194
  %191 = load i64*, i64** %S.addr, align 4
  %arrayidx196 = getelementptr i64, i64* %191, i32 2
  %192 = load i64, i64* %arrayidx196, align 8
  %xor197 = xor i64 %and195, %192
  %add198 = add i64 %xor190, %xor197
  %193 = load i64*, i64** %W.addr, align 4
  %194 = load i32, i32* %i, align 4
  %add199 = add i32 4, %194
  %arrayidx200 = getelementptr i64, i64* %193, i32 %add199
  %195 = load i64, i64* %arrayidx200, align 8
  %add201 = add i64 %add198, %195
  %196 = load i32, i32* %i, align 4
  %add202 = add i32 4, %196
  %arrayidx203 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add202
  %197 = load i64, i64* %arrayidx203, align 8
  %add204 = add i64 %add201, %197
  %198 = load i64*, i64** %S.addr, align 4
  %arrayidx205 = getelementptr i64, i64* %198, i32 3
  %199 = load i64, i64* %arrayidx205, align 8
  %add206 = add i64 %199, %add204
  store i64 %add206, i64* %arrayidx205, align 8
  %200 = load i64*, i64** %S.addr, align 4
  %arrayidx207 = getelementptr i64, i64* %200, i32 3
  %201 = load i64, i64* %arrayidx207, align 8
  %202 = load i64*, i64** %S.addr, align 4
  %arrayidx208 = getelementptr i64, i64* %202, i32 7
  %203 = load i64, i64* %arrayidx208, align 8
  %add209 = add i64 %203, %201
  store i64 %add209, i64* %arrayidx208, align 8
  %204 = load i64*, i64** %S.addr, align 4
  %arrayidx210 = getelementptr i64, i64* %204, i32 4
  %205 = load i64, i64* %arrayidx210, align 8
  %call211 = call i64 @rotr64(i64 %205, i32 28)
  %206 = load i64*, i64** %S.addr, align 4
  %arrayidx212 = getelementptr i64, i64* %206, i32 4
  %207 = load i64, i64* %arrayidx212, align 8
  %call213 = call i64 @rotr64(i64 %207, i32 34)
  %xor214 = xor i64 %call211, %call213
  %208 = load i64*, i64** %S.addr, align 4
  %arrayidx215 = getelementptr i64, i64* %208, i32 4
  %209 = load i64, i64* %arrayidx215, align 8
  %call216 = call i64 @rotr64(i64 %209, i32 39)
  %xor217 = xor i64 %xor214, %call216
  %210 = load i64*, i64** %S.addr, align 4
  %arrayidx218 = getelementptr i64, i64* %210, i32 4
  %211 = load i64, i64* %arrayidx218, align 8
  %212 = load i64*, i64** %S.addr, align 4
  %arrayidx219 = getelementptr i64, i64* %212, i32 5
  %213 = load i64, i64* %arrayidx219, align 8
  %214 = load i64*, i64** %S.addr, align 4
  %arrayidx220 = getelementptr i64, i64* %214, i32 6
  %215 = load i64, i64* %arrayidx220, align 8
  %or221 = or i64 %213, %215
  %and222 = and i64 %211, %or221
  %216 = load i64*, i64** %S.addr, align 4
  %arrayidx223 = getelementptr i64, i64* %216, i32 5
  %217 = load i64, i64* %arrayidx223, align 8
  %218 = load i64*, i64** %S.addr, align 4
  %arrayidx224 = getelementptr i64, i64* %218, i32 6
  %219 = load i64, i64* %arrayidx224, align 8
  %and225 = and i64 %217, %219
  %or226 = or i64 %and222, %and225
  %add227 = add i64 %xor217, %or226
  %220 = load i64*, i64** %S.addr, align 4
  %arrayidx228 = getelementptr i64, i64* %220, i32 3
  %221 = load i64, i64* %arrayidx228, align 8
  %add229 = add i64 %221, %add227
  store i64 %add229, i64* %arrayidx228, align 8
  %222 = load i64*, i64** %S.addr, align 4
  %arrayidx230 = getelementptr i64, i64* %222, i32 7
  %223 = load i64, i64* %arrayidx230, align 8
  %call231 = call i64 @rotr64(i64 %223, i32 14)
  %224 = load i64*, i64** %S.addr, align 4
  %arrayidx232 = getelementptr i64, i64* %224, i32 7
  %225 = load i64, i64* %arrayidx232, align 8
  %call233 = call i64 @rotr64(i64 %225, i32 18)
  %xor234 = xor i64 %call231, %call233
  %226 = load i64*, i64** %S.addr, align 4
  %arrayidx235 = getelementptr i64, i64* %226, i32 7
  %227 = load i64, i64* %arrayidx235, align 8
  %call236 = call i64 @rotr64(i64 %227, i32 41)
  %xor237 = xor i64 %xor234, %call236
  %228 = load i64*, i64** %S.addr, align 4
  %arrayidx238 = getelementptr i64, i64* %228, i32 7
  %229 = load i64, i64* %arrayidx238, align 8
  %230 = load i64*, i64** %S.addr, align 4
  %arrayidx239 = getelementptr i64, i64* %230, i32 0
  %231 = load i64, i64* %arrayidx239, align 8
  %232 = load i64*, i64** %S.addr, align 4
  %arrayidx240 = getelementptr i64, i64* %232, i32 1
  %233 = load i64, i64* %arrayidx240, align 8
  %xor241 = xor i64 %231, %233
  %and242 = and i64 %229, %xor241
  %234 = load i64*, i64** %S.addr, align 4
  %arrayidx243 = getelementptr i64, i64* %234, i32 1
  %235 = load i64, i64* %arrayidx243, align 8
  %xor244 = xor i64 %and242, %235
  %add245 = add i64 %xor237, %xor244
  %236 = load i64*, i64** %W.addr, align 4
  %237 = load i32, i32* %i, align 4
  %add246 = add i32 5, %237
  %arrayidx247 = getelementptr i64, i64* %236, i32 %add246
  %238 = load i64, i64* %arrayidx247, align 8
  %add248 = add i64 %add245, %238
  %239 = load i32, i32* %i, align 4
  %add249 = add i32 5, %239
  %arrayidx250 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add249
  %240 = load i64, i64* %arrayidx250, align 8
  %add251 = add i64 %add248, %240
  %241 = load i64*, i64** %S.addr, align 4
  %arrayidx252 = getelementptr i64, i64* %241, i32 2
  %242 = load i64, i64* %arrayidx252, align 8
  %add253 = add i64 %242, %add251
  store i64 %add253, i64* %arrayidx252, align 8
  %243 = load i64*, i64** %S.addr, align 4
  %arrayidx254 = getelementptr i64, i64* %243, i32 2
  %244 = load i64, i64* %arrayidx254, align 8
  %245 = load i64*, i64** %S.addr, align 4
  %arrayidx255 = getelementptr i64, i64* %245, i32 6
  %246 = load i64, i64* %arrayidx255, align 8
  %add256 = add i64 %246, %244
  store i64 %add256, i64* %arrayidx255, align 8
  %247 = load i64*, i64** %S.addr, align 4
  %arrayidx257 = getelementptr i64, i64* %247, i32 3
  %248 = load i64, i64* %arrayidx257, align 8
  %call258 = call i64 @rotr64(i64 %248, i32 28)
  %249 = load i64*, i64** %S.addr, align 4
  %arrayidx259 = getelementptr i64, i64* %249, i32 3
  %250 = load i64, i64* %arrayidx259, align 8
  %call260 = call i64 @rotr64(i64 %250, i32 34)
  %xor261 = xor i64 %call258, %call260
  %251 = load i64*, i64** %S.addr, align 4
  %arrayidx262 = getelementptr i64, i64* %251, i32 3
  %252 = load i64, i64* %arrayidx262, align 8
  %call263 = call i64 @rotr64(i64 %252, i32 39)
  %xor264 = xor i64 %xor261, %call263
  %253 = load i64*, i64** %S.addr, align 4
  %arrayidx265 = getelementptr i64, i64* %253, i32 3
  %254 = load i64, i64* %arrayidx265, align 8
  %255 = load i64*, i64** %S.addr, align 4
  %arrayidx266 = getelementptr i64, i64* %255, i32 4
  %256 = load i64, i64* %arrayidx266, align 8
  %257 = load i64*, i64** %S.addr, align 4
  %arrayidx267 = getelementptr i64, i64* %257, i32 5
  %258 = load i64, i64* %arrayidx267, align 8
  %or268 = or i64 %256, %258
  %and269 = and i64 %254, %or268
  %259 = load i64*, i64** %S.addr, align 4
  %arrayidx270 = getelementptr i64, i64* %259, i32 4
  %260 = load i64, i64* %arrayidx270, align 8
  %261 = load i64*, i64** %S.addr, align 4
  %arrayidx271 = getelementptr i64, i64* %261, i32 5
  %262 = load i64, i64* %arrayidx271, align 8
  %and272 = and i64 %260, %262
  %or273 = or i64 %and269, %and272
  %add274 = add i64 %xor264, %or273
  %263 = load i64*, i64** %S.addr, align 4
  %arrayidx275 = getelementptr i64, i64* %263, i32 2
  %264 = load i64, i64* %arrayidx275, align 8
  %add276 = add i64 %264, %add274
  store i64 %add276, i64* %arrayidx275, align 8
  %265 = load i64*, i64** %S.addr, align 4
  %arrayidx277 = getelementptr i64, i64* %265, i32 6
  %266 = load i64, i64* %arrayidx277, align 8
  %call278 = call i64 @rotr64(i64 %266, i32 14)
  %267 = load i64*, i64** %S.addr, align 4
  %arrayidx279 = getelementptr i64, i64* %267, i32 6
  %268 = load i64, i64* %arrayidx279, align 8
  %call280 = call i64 @rotr64(i64 %268, i32 18)
  %xor281 = xor i64 %call278, %call280
  %269 = load i64*, i64** %S.addr, align 4
  %arrayidx282 = getelementptr i64, i64* %269, i32 6
  %270 = load i64, i64* %arrayidx282, align 8
  %call283 = call i64 @rotr64(i64 %270, i32 41)
  %xor284 = xor i64 %xor281, %call283
  %271 = load i64*, i64** %S.addr, align 4
  %arrayidx285 = getelementptr i64, i64* %271, i32 6
  %272 = load i64, i64* %arrayidx285, align 8
  %273 = load i64*, i64** %S.addr, align 4
  %arrayidx286 = getelementptr i64, i64* %273, i32 7
  %274 = load i64, i64* %arrayidx286, align 8
  %275 = load i64*, i64** %S.addr, align 4
  %arrayidx287 = getelementptr i64, i64* %275, i32 0
  %276 = load i64, i64* %arrayidx287, align 8
  %xor288 = xor i64 %274, %276
  %and289 = and i64 %272, %xor288
  %277 = load i64*, i64** %S.addr, align 4
  %arrayidx290 = getelementptr i64, i64* %277, i32 0
  %278 = load i64, i64* %arrayidx290, align 8
  %xor291 = xor i64 %and289, %278
  %add292 = add i64 %xor284, %xor291
  %279 = load i64*, i64** %W.addr, align 4
  %280 = load i32, i32* %i, align 4
  %add293 = add i32 6, %280
  %arrayidx294 = getelementptr i64, i64* %279, i32 %add293
  %281 = load i64, i64* %arrayidx294, align 8
  %add295 = add i64 %add292, %281
  %282 = load i32, i32* %i, align 4
  %add296 = add i32 6, %282
  %arrayidx297 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add296
  %283 = load i64, i64* %arrayidx297, align 8
  %add298 = add i64 %add295, %283
  %284 = load i64*, i64** %S.addr, align 4
  %arrayidx299 = getelementptr i64, i64* %284, i32 1
  %285 = load i64, i64* %arrayidx299, align 8
  %add300 = add i64 %285, %add298
  store i64 %add300, i64* %arrayidx299, align 8
  %286 = load i64*, i64** %S.addr, align 4
  %arrayidx301 = getelementptr i64, i64* %286, i32 1
  %287 = load i64, i64* %arrayidx301, align 8
  %288 = load i64*, i64** %S.addr, align 4
  %arrayidx302 = getelementptr i64, i64* %288, i32 5
  %289 = load i64, i64* %arrayidx302, align 8
  %add303 = add i64 %289, %287
  store i64 %add303, i64* %arrayidx302, align 8
  %290 = load i64*, i64** %S.addr, align 4
  %arrayidx304 = getelementptr i64, i64* %290, i32 2
  %291 = load i64, i64* %arrayidx304, align 8
  %call305 = call i64 @rotr64(i64 %291, i32 28)
  %292 = load i64*, i64** %S.addr, align 4
  %arrayidx306 = getelementptr i64, i64* %292, i32 2
  %293 = load i64, i64* %arrayidx306, align 8
  %call307 = call i64 @rotr64(i64 %293, i32 34)
  %xor308 = xor i64 %call305, %call307
  %294 = load i64*, i64** %S.addr, align 4
  %arrayidx309 = getelementptr i64, i64* %294, i32 2
  %295 = load i64, i64* %arrayidx309, align 8
  %call310 = call i64 @rotr64(i64 %295, i32 39)
  %xor311 = xor i64 %xor308, %call310
  %296 = load i64*, i64** %S.addr, align 4
  %arrayidx312 = getelementptr i64, i64* %296, i32 2
  %297 = load i64, i64* %arrayidx312, align 8
  %298 = load i64*, i64** %S.addr, align 4
  %arrayidx313 = getelementptr i64, i64* %298, i32 3
  %299 = load i64, i64* %arrayidx313, align 8
  %300 = load i64*, i64** %S.addr, align 4
  %arrayidx314 = getelementptr i64, i64* %300, i32 4
  %301 = load i64, i64* %arrayidx314, align 8
  %or315 = or i64 %299, %301
  %and316 = and i64 %297, %or315
  %302 = load i64*, i64** %S.addr, align 4
  %arrayidx317 = getelementptr i64, i64* %302, i32 3
  %303 = load i64, i64* %arrayidx317, align 8
  %304 = load i64*, i64** %S.addr, align 4
  %arrayidx318 = getelementptr i64, i64* %304, i32 4
  %305 = load i64, i64* %arrayidx318, align 8
  %and319 = and i64 %303, %305
  %or320 = or i64 %and316, %and319
  %add321 = add i64 %xor311, %or320
  %306 = load i64*, i64** %S.addr, align 4
  %arrayidx322 = getelementptr i64, i64* %306, i32 1
  %307 = load i64, i64* %arrayidx322, align 8
  %add323 = add i64 %307, %add321
  store i64 %add323, i64* %arrayidx322, align 8
  %308 = load i64*, i64** %S.addr, align 4
  %arrayidx324 = getelementptr i64, i64* %308, i32 5
  %309 = load i64, i64* %arrayidx324, align 8
  %call325 = call i64 @rotr64(i64 %309, i32 14)
  %310 = load i64*, i64** %S.addr, align 4
  %arrayidx326 = getelementptr i64, i64* %310, i32 5
  %311 = load i64, i64* %arrayidx326, align 8
  %call327 = call i64 @rotr64(i64 %311, i32 18)
  %xor328 = xor i64 %call325, %call327
  %312 = load i64*, i64** %S.addr, align 4
  %arrayidx329 = getelementptr i64, i64* %312, i32 5
  %313 = load i64, i64* %arrayidx329, align 8
  %call330 = call i64 @rotr64(i64 %313, i32 41)
  %xor331 = xor i64 %xor328, %call330
  %314 = load i64*, i64** %S.addr, align 4
  %arrayidx332 = getelementptr i64, i64* %314, i32 5
  %315 = load i64, i64* %arrayidx332, align 8
  %316 = load i64*, i64** %S.addr, align 4
  %arrayidx333 = getelementptr i64, i64* %316, i32 6
  %317 = load i64, i64* %arrayidx333, align 8
  %318 = load i64*, i64** %S.addr, align 4
  %arrayidx334 = getelementptr i64, i64* %318, i32 7
  %319 = load i64, i64* %arrayidx334, align 8
  %xor335 = xor i64 %317, %319
  %and336 = and i64 %315, %xor335
  %320 = load i64*, i64** %S.addr, align 4
  %arrayidx337 = getelementptr i64, i64* %320, i32 7
  %321 = load i64, i64* %arrayidx337, align 8
  %xor338 = xor i64 %and336, %321
  %add339 = add i64 %xor331, %xor338
  %322 = load i64*, i64** %W.addr, align 4
  %323 = load i32, i32* %i, align 4
  %add340 = add i32 7, %323
  %arrayidx341 = getelementptr i64, i64* %322, i32 %add340
  %324 = load i64, i64* %arrayidx341, align 8
  %add342 = add i64 %add339, %324
  %325 = load i32, i32* %i, align 4
  %add343 = add i32 7, %325
  %arrayidx344 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add343
  %326 = load i64, i64* %arrayidx344, align 8
  %add345 = add i64 %add342, %326
  %327 = load i64*, i64** %S.addr, align 4
  %arrayidx346 = getelementptr i64, i64* %327, i32 0
  %328 = load i64, i64* %arrayidx346, align 8
  %add347 = add i64 %328, %add345
  store i64 %add347, i64* %arrayidx346, align 8
  %329 = load i64*, i64** %S.addr, align 4
  %arrayidx348 = getelementptr i64, i64* %329, i32 0
  %330 = load i64, i64* %arrayidx348, align 8
  %331 = load i64*, i64** %S.addr, align 4
  %arrayidx349 = getelementptr i64, i64* %331, i32 4
  %332 = load i64, i64* %arrayidx349, align 8
  %add350 = add i64 %332, %330
  store i64 %add350, i64* %arrayidx349, align 8
  %333 = load i64*, i64** %S.addr, align 4
  %arrayidx351 = getelementptr i64, i64* %333, i32 1
  %334 = load i64, i64* %arrayidx351, align 8
  %call352 = call i64 @rotr64(i64 %334, i32 28)
  %335 = load i64*, i64** %S.addr, align 4
  %arrayidx353 = getelementptr i64, i64* %335, i32 1
  %336 = load i64, i64* %arrayidx353, align 8
  %call354 = call i64 @rotr64(i64 %336, i32 34)
  %xor355 = xor i64 %call352, %call354
  %337 = load i64*, i64** %S.addr, align 4
  %arrayidx356 = getelementptr i64, i64* %337, i32 1
  %338 = load i64, i64* %arrayidx356, align 8
  %call357 = call i64 @rotr64(i64 %338, i32 39)
  %xor358 = xor i64 %xor355, %call357
  %339 = load i64*, i64** %S.addr, align 4
  %arrayidx359 = getelementptr i64, i64* %339, i32 1
  %340 = load i64, i64* %arrayidx359, align 8
  %341 = load i64*, i64** %S.addr, align 4
  %arrayidx360 = getelementptr i64, i64* %341, i32 2
  %342 = load i64, i64* %arrayidx360, align 8
  %343 = load i64*, i64** %S.addr, align 4
  %arrayidx361 = getelementptr i64, i64* %343, i32 3
  %344 = load i64, i64* %arrayidx361, align 8
  %or362 = or i64 %342, %344
  %and363 = and i64 %340, %or362
  %345 = load i64*, i64** %S.addr, align 4
  %arrayidx364 = getelementptr i64, i64* %345, i32 2
  %346 = load i64, i64* %arrayidx364, align 8
  %347 = load i64*, i64** %S.addr, align 4
  %arrayidx365 = getelementptr i64, i64* %347, i32 3
  %348 = load i64, i64* %arrayidx365, align 8
  %and366 = and i64 %346, %348
  %or367 = or i64 %and363, %and366
  %add368 = add i64 %xor358, %or367
  %349 = load i64*, i64** %S.addr, align 4
  %arrayidx369 = getelementptr i64, i64* %349, i32 0
  %350 = load i64, i64* %arrayidx369, align 8
  %add370 = add i64 %350, %add368
  store i64 %add370, i64* %arrayidx369, align 8
  %351 = load i64*, i64** %S.addr, align 4
  %arrayidx371 = getelementptr i64, i64* %351, i32 4
  %352 = load i64, i64* %arrayidx371, align 8
  %call372 = call i64 @rotr64(i64 %352, i32 14)
  %353 = load i64*, i64** %S.addr, align 4
  %arrayidx373 = getelementptr i64, i64* %353, i32 4
  %354 = load i64, i64* %arrayidx373, align 8
  %call374 = call i64 @rotr64(i64 %354, i32 18)
  %xor375 = xor i64 %call372, %call374
  %355 = load i64*, i64** %S.addr, align 4
  %arrayidx376 = getelementptr i64, i64* %355, i32 4
  %356 = load i64, i64* %arrayidx376, align 8
  %call377 = call i64 @rotr64(i64 %356, i32 41)
  %xor378 = xor i64 %xor375, %call377
  %357 = load i64*, i64** %S.addr, align 4
  %arrayidx379 = getelementptr i64, i64* %357, i32 4
  %358 = load i64, i64* %arrayidx379, align 8
  %359 = load i64*, i64** %S.addr, align 4
  %arrayidx380 = getelementptr i64, i64* %359, i32 5
  %360 = load i64, i64* %arrayidx380, align 8
  %361 = load i64*, i64** %S.addr, align 4
  %arrayidx381 = getelementptr i64, i64* %361, i32 6
  %362 = load i64, i64* %arrayidx381, align 8
  %xor382 = xor i64 %360, %362
  %and383 = and i64 %358, %xor382
  %363 = load i64*, i64** %S.addr, align 4
  %arrayidx384 = getelementptr i64, i64* %363, i32 6
  %364 = load i64, i64* %arrayidx384, align 8
  %xor385 = xor i64 %and383, %364
  %add386 = add i64 %xor378, %xor385
  %365 = load i64*, i64** %W.addr, align 4
  %366 = load i32, i32* %i, align 4
  %add387 = add i32 8, %366
  %arrayidx388 = getelementptr i64, i64* %365, i32 %add387
  %367 = load i64, i64* %arrayidx388, align 8
  %add389 = add i64 %add386, %367
  %368 = load i32, i32* %i, align 4
  %add390 = add i32 8, %368
  %arrayidx391 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add390
  %369 = load i64, i64* %arrayidx391, align 8
  %add392 = add i64 %add389, %369
  %370 = load i64*, i64** %S.addr, align 4
  %arrayidx393 = getelementptr i64, i64* %370, i32 7
  %371 = load i64, i64* %arrayidx393, align 8
  %add394 = add i64 %371, %add392
  store i64 %add394, i64* %arrayidx393, align 8
  %372 = load i64*, i64** %S.addr, align 4
  %arrayidx395 = getelementptr i64, i64* %372, i32 7
  %373 = load i64, i64* %arrayidx395, align 8
  %374 = load i64*, i64** %S.addr, align 4
  %arrayidx396 = getelementptr i64, i64* %374, i32 3
  %375 = load i64, i64* %arrayidx396, align 8
  %add397 = add i64 %375, %373
  store i64 %add397, i64* %arrayidx396, align 8
  %376 = load i64*, i64** %S.addr, align 4
  %arrayidx398 = getelementptr i64, i64* %376, i32 0
  %377 = load i64, i64* %arrayidx398, align 8
  %call399 = call i64 @rotr64(i64 %377, i32 28)
  %378 = load i64*, i64** %S.addr, align 4
  %arrayidx400 = getelementptr i64, i64* %378, i32 0
  %379 = load i64, i64* %arrayidx400, align 8
  %call401 = call i64 @rotr64(i64 %379, i32 34)
  %xor402 = xor i64 %call399, %call401
  %380 = load i64*, i64** %S.addr, align 4
  %arrayidx403 = getelementptr i64, i64* %380, i32 0
  %381 = load i64, i64* %arrayidx403, align 8
  %call404 = call i64 @rotr64(i64 %381, i32 39)
  %xor405 = xor i64 %xor402, %call404
  %382 = load i64*, i64** %S.addr, align 4
  %arrayidx406 = getelementptr i64, i64* %382, i32 0
  %383 = load i64, i64* %arrayidx406, align 8
  %384 = load i64*, i64** %S.addr, align 4
  %arrayidx407 = getelementptr i64, i64* %384, i32 1
  %385 = load i64, i64* %arrayidx407, align 8
  %386 = load i64*, i64** %S.addr, align 4
  %arrayidx408 = getelementptr i64, i64* %386, i32 2
  %387 = load i64, i64* %arrayidx408, align 8
  %or409 = or i64 %385, %387
  %and410 = and i64 %383, %or409
  %388 = load i64*, i64** %S.addr, align 4
  %arrayidx411 = getelementptr i64, i64* %388, i32 1
  %389 = load i64, i64* %arrayidx411, align 8
  %390 = load i64*, i64** %S.addr, align 4
  %arrayidx412 = getelementptr i64, i64* %390, i32 2
  %391 = load i64, i64* %arrayidx412, align 8
  %and413 = and i64 %389, %391
  %or414 = or i64 %and410, %and413
  %add415 = add i64 %xor405, %or414
  %392 = load i64*, i64** %S.addr, align 4
  %arrayidx416 = getelementptr i64, i64* %392, i32 7
  %393 = load i64, i64* %arrayidx416, align 8
  %add417 = add i64 %393, %add415
  store i64 %add417, i64* %arrayidx416, align 8
  %394 = load i64*, i64** %S.addr, align 4
  %arrayidx418 = getelementptr i64, i64* %394, i32 3
  %395 = load i64, i64* %arrayidx418, align 8
  %call419 = call i64 @rotr64(i64 %395, i32 14)
  %396 = load i64*, i64** %S.addr, align 4
  %arrayidx420 = getelementptr i64, i64* %396, i32 3
  %397 = load i64, i64* %arrayidx420, align 8
  %call421 = call i64 @rotr64(i64 %397, i32 18)
  %xor422 = xor i64 %call419, %call421
  %398 = load i64*, i64** %S.addr, align 4
  %arrayidx423 = getelementptr i64, i64* %398, i32 3
  %399 = load i64, i64* %arrayidx423, align 8
  %call424 = call i64 @rotr64(i64 %399, i32 41)
  %xor425 = xor i64 %xor422, %call424
  %400 = load i64*, i64** %S.addr, align 4
  %arrayidx426 = getelementptr i64, i64* %400, i32 3
  %401 = load i64, i64* %arrayidx426, align 8
  %402 = load i64*, i64** %S.addr, align 4
  %arrayidx427 = getelementptr i64, i64* %402, i32 4
  %403 = load i64, i64* %arrayidx427, align 8
  %404 = load i64*, i64** %S.addr, align 4
  %arrayidx428 = getelementptr i64, i64* %404, i32 5
  %405 = load i64, i64* %arrayidx428, align 8
  %xor429 = xor i64 %403, %405
  %and430 = and i64 %401, %xor429
  %406 = load i64*, i64** %S.addr, align 4
  %arrayidx431 = getelementptr i64, i64* %406, i32 5
  %407 = load i64, i64* %arrayidx431, align 8
  %xor432 = xor i64 %and430, %407
  %add433 = add i64 %xor425, %xor432
  %408 = load i64*, i64** %W.addr, align 4
  %409 = load i32, i32* %i, align 4
  %add434 = add i32 9, %409
  %arrayidx435 = getelementptr i64, i64* %408, i32 %add434
  %410 = load i64, i64* %arrayidx435, align 8
  %add436 = add i64 %add433, %410
  %411 = load i32, i32* %i, align 4
  %add437 = add i32 9, %411
  %arrayidx438 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add437
  %412 = load i64, i64* %arrayidx438, align 8
  %add439 = add i64 %add436, %412
  %413 = load i64*, i64** %S.addr, align 4
  %arrayidx440 = getelementptr i64, i64* %413, i32 6
  %414 = load i64, i64* %arrayidx440, align 8
  %add441 = add i64 %414, %add439
  store i64 %add441, i64* %arrayidx440, align 8
  %415 = load i64*, i64** %S.addr, align 4
  %arrayidx442 = getelementptr i64, i64* %415, i32 6
  %416 = load i64, i64* %arrayidx442, align 8
  %417 = load i64*, i64** %S.addr, align 4
  %arrayidx443 = getelementptr i64, i64* %417, i32 2
  %418 = load i64, i64* %arrayidx443, align 8
  %add444 = add i64 %418, %416
  store i64 %add444, i64* %arrayidx443, align 8
  %419 = load i64*, i64** %S.addr, align 4
  %arrayidx445 = getelementptr i64, i64* %419, i32 7
  %420 = load i64, i64* %arrayidx445, align 8
  %call446 = call i64 @rotr64(i64 %420, i32 28)
  %421 = load i64*, i64** %S.addr, align 4
  %arrayidx447 = getelementptr i64, i64* %421, i32 7
  %422 = load i64, i64* %arrayidx447, align 8
  %call448 = call i64 @rotr64(i64 %422, i32 34)
  %xor449 = xor i64 %call446, %call448
  %423 = load i64*, i64** %S.addr, align 4
  %arrayidx450 = getelementptr i64, i64* %423, i32 7
  %424 = load i64, i64* %arrayidx450, align 8
  %call451 = call i64 @rotr64(i64 %424, i32 39)
  %xor452 = xor i64 %xor449, %call451
  %425 = load i64*, i64** %S.addr, align 4
  %arrayidx453 = getelementptr i64, i64* %425, i32 7
  %426 = load i64, i64* %arrayidx453, align 8
  %427 = load i64*, i64** %S.addr, align 4
  %arrayidx454 = getelementptr i64, i64* %427, i32 0
  %428 = load i64, i64* %arrayidx454, align 8
  %429 = load i64*, i64** %S.addr, align 4
  %arrayidx455 = getelementptr i64, i64* %429, i32 1
  %430 = load i64, i64* %arrayidx455, align 8
  %or456 = or i64 %428, %430
  %and457 = and i64 %426, %or456
  %431 = load i64*, i64** %S.addr, align 4
  %arrayidx458 = getelementptr i64, i64* %431, i32 0
  %432 = load i64, i64* %arrayidx458, align 8
  %433 = load i64*, i64** %S.addr, align 4
  %arrayidx459 = getelementptr i64, i64* %433, i32 1
  %434 = load i64, i64* %arrayidx459, align 8
  %and460 = and i64 %432, %434
  %or461 = or i64 %and457, %and460
  %add462 = add i64 %xor452, %or461
  %435 = load i64*, i64** %S.addr, align 4
  %arrayidx463 = getelementptr i64, i64* %435, i32 6
  %436 = load i64, i64* %arrayidx463, align 8
  %add464 = add i64 %436, %add462
  store i64 %add464, i64* %arrayidx463, align 8
  %437 = load i64*, i64** %S.addr, align 4
  %arrayidx465 = getelementptr i64, i64* %437, i32 2
  %438 = load i64, i64* %arrayidx465, align 8
  %call466 = call i64 @rotr64(i64 %438, i32 14)
  %439 = load i64*, i64** %S.addr, align 4
  %arrayidx467 = getelementptr i64, i64* %439, i32 2
  %440 = load i64, i64* %arrayidx467, align 8
  %call468 = call i64 @rotr64(i64 %440, i32 18)
  %xor469 = xor i64 %call466, %call468
  %441 = load i64*, i64** %S.addr, align 4
  %arrayidx470 = getelementptr i64, i64* %441, i32 2
  %442 = load i64, i64* %arrayidx470, align 8
  %call471 = call i64 @rotr64(i64 %442, i32 41)
  %xor472 = xor i64 %xor469, %call471
  %443 = load i64*, i64** %S.addr, align 4
  %arrayidx473 = getelementptr i64, i64* %443, i32 2
  %444 = load i64, i64* %arrayidx473, align 8
  %445 = load i64*, i64** %S.addr, align 4
  %arrayidx474 = getelementptr i64, i64* %445, i32 3
  %446 = load i64, i64* %arrayidx474, align 8
  %447 = load i64*, i64** %S.addr, align 4
  %arrayidx475 = getelementptr i64, i64* %447, i32 4
  %448 = load i64, i64* %arrayidx475, align 8
  %xor476 = xor i64 %446, %448
  %and477 = and i64 %444, %xor476
  %449 = load i64*, i64** %S.addr, align 4
  %arrayidx478 = getelementptr i64, i64* %449, i32 4
  %450 = load i64, i64* %arrayidx478, align 8
  %xor479 = xor i64 %and477, %450
  %add480 = add i64 %xor472, %xor479
  %451 = load i64*, i64** %W.addr, align 4
  %452 = load i32, i32* %i, align 4
  %add481 = add i32 10, %452
  %arrayidx482 = getelementptr i64, i64* %451, i32 %add481
  %453 = load i64, i64* %arrayidx482, align 8
  %add483 = add i64 %add480, %453
  %454 = load i32, i32* %i, align 4
  %add484 = add i32 10, %454
  %arrayidx485 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add484
  %455 = load i64, i64* %arrayidx485, align 8
  %add486 = add i64 %add483, %455
  %456 = load i64*, i64** %S.addr, align 4
  %arrayidx487 = getelementptr i64, i64* %456, i32 5
  %457 = load i64, i64* %arrayidx487, align 8
  %add488 = add i64 %457, %add486
  store i64 %add488, i64* %arrayidx487, align 8
  %458 = load i64*, i64** %S.addr, align 4
  %arrayidx489 = getelementptr i64, i64* %458, i32 5
  %459 = load i64, i64* %arrayidx489, align 8
  %460 = load i64*, i64** %S.addr, align 4
  %arrayidx490 = getelementptr i64, i64* %460, i32 1
  %461 = load i64, i64* %arrayidx490, align 8
  %add491 = add i64 %461, %459
  store i64 %add491, i64* %arrayidx490, align 8
  %462 = load i64*, i64** %S.addr, align 4
  %arrayidx492 = getelementptr i64, i64* %462, i32 6
  %463 = load i64, i64* %arrayidx492, align 8
  %call493 = call i64 @rotr64(i64 %463, i32 28)
  %464 = load i64*, i64** %S.addr, align 4
  %arrayidx494 = getelementptr i64, i64* %464, i32 6
  %465 = load i64, i64* %arrayidx494, align 8
  %call495 = call i64 @rotr64(i64 %465, i32 34)
  %xor496 = xor i64 %call493, %call495
  %466 = load i64*, i64** %S.addr, align 4
  %arrayidx497 = getelementptr i64, i64* %466, i32 6
  %467 = load i64, i64* %arrayidx497, align 8
  %call498 = call i64 @rotr64(i64 %467, i32 39)
  %xor499 = xor i64 %xor496, %call498
  %468 = load i64*, i64** %S.addr, align 4
  %arrayidx500 = getelementptr i64, i64* %468, i32 6
  %469 = load i64, i64* %arrayidx500, align 8
  %470 = load i64*, i64** %S.addr, align 4
  %arrayidx501 = getelementptr i64, i64* %470, i32 7
  %471 = load i64, i64* %arrayidx501, align 8
  %472 = load i64*, i64** %S.addr, align 4
  %arrayidx502 = getelementptr i64, i64* %472, i32 0
  %473 = load i64, i64* %arrayidx502, align 8
  %or503 = or i64 %471, %473
  %and504 = and i64 %469, %or503
  %474 = load i64*, i64** %S.addr, align 4
  %arrayidx505 = getelementptr i64, i64* %474, i32 7
  %475 = load i64, i64* %arrayidx505, align 8
  %476 = load i64*, i64** %S.addr, align 4
  %arrayidx506 = getelementptr i64, i64* %476, i32 0
  %477 = load i64, i64* %arrayidx506, align 8
  %and507 = and i64 %475, %477
  %or508 = or i64 %and504, %and507
  %add509 = add i64 %xor499, %or508
  %478 = load i64*, i64** %S.addr, align 4
  %arrayidx510 = getelementptr i64, i64* %478, i32 5
  %479 = load i64, i64* %arrayidx510, align 8
  %add511 = add i64 %479, %add509
  store i64 %add511, i64* %arrayidx510, align 8
  %480 = load i64*, i64** %S.addr, align 4
  %arrayidx512 = getelementptr i64, i64* %480, i32 1
  %481 = load i64, i64* %arrayidx512, align 8
  %call513 = call i64 @rotr64(i64 %481, i32 14)
  %482 = load i64*, i64** %S.addr, align 4
  %arrayidx514 = getelementptr i64, i64* %482, i32 1
  %483 = load i64, i64* %arrayidx514, align 8
  %call515 = call i64 @rotr64(i64 %483, i32 18)
  %xor516 = xor i64 %call513, %call515
  %484 = load i64*, i64** %S.addr, align 4
  %arrayidx517 = getelementptr i64, i64* %484, i32 1
  %485 = load i64, i64* %arrayidx517, align 8
  %call518 = call i64 @rotr64(i64 %485, i32 41)
  %xor519 = xor i64 %xor516, %call518
  %486 = load i64*, i64** %S.addr, align 4
  %arrayidx520 = getelementptr i64, i64* %486, i32 1
  %487 = load i64, i64* %arrayidx520, align 8
  %488 = load i64*, i64** %S.addr, align 4
  %arrayidx521 = getelementptr i64, i64* %488, i32 2
  %489 = load i64, i64* %arrayidx521, align 8
  %490 = load i64*, i64** %S.addr, align 4
  %arrayidx522 = getelementptr i64, i64* %490, i32 3
  %491 = load i64, i64* %arrayidx522, align 8
  %xor523 = xor i64 %489, %491
  %and524 = and i64 %487, %xor523
  %492 = load i64*, i64** %S.addr, align 4
  %arrayidx525 = getelementptr i64, i64* %492, i32 3
  %493 = load i64, i64* %arrayidx525, align 8
  %xor526 = xor i64 %and524, %493
  %add527 = add i64 %xor519, %xor526
  %494 = load i64*, i64** %W.addr, align 4
  %495 = load i32, i32* %i, align 4
  %add528 = add i32 11, %495
  %arrayidx529 = getelementptr i64, i64* %494, i32 %add528
  %496 = load i64, i64* %arrayidx529, align 8
  %add530 = add i64 %add527, %496
  %497 = load i32, i32* %i, align 4
  %add531 = add i32 11, %497
  %arrayidx532 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add531
  %498 = load i64, i64* %arrayidx532, align 8
  %add533 = add i64 %add530, %498
  %499 = load i64*, i64** %S.addr, align 4
  %arrayidx534 = getelementptr i64, i64* %499, i32 4
  %500 = load i64, i64* %arrayidx534, align 8
  %add535 = add i64 %500, %add533
  store i64 %add535, i64* %arrayidx534, align 8
  %501 = load i64*, i64** %S.addr, align 4
  %arrayidx536 = getelementptr i64, i64* %501, i32 4
  %502 = load i64, i64* %arrayidx536, align 8
  %503 = load i64*, i64** %S.addr, align 4
  %arrayidx537 = getelementptr i64, i64* %503, i32 0
  %504 = load i64, i64* %arrayidx537, align 8
  %add538 = add i64 %504, %502
  store i64 %add538, i64* %arrayidx537, align 8
  %505 = load i64*, i64** %S.addr, align 4
  %arrayidx539 = getelementptr i64, i64* %505, i32 5
  %506 = load i64, i64* %arrayidx539, align 8
  %call540 = call i64 @rotr64(i64 %506, i32 28)
  %507 = load i64*, i64** %S.addr, align 4
  %arrayidx541 = getelementptr i64, i64* %507, i32 5
  %508 = load i64, i64* %arrayidx541, align 8
  %call542 = call i64 @rotr64(i64 %508, i32 34)
  %xor543 = xor i64 %call540, %call542
  %509 = load i64*, i64** %S.addr, align 4
  %arrayidx544 = getelementptr i64, i64* %509, i32 5
  %510 = load i64, i64* %arrayidx544, align 8
  %call545 = call i64 @rotr64(i64 %510, i32 39)
  %xor546 = xor i64 %xor543, %call545
  %511 = load i64*, i64** %S.addr, align 4
  %arrayidx547 = getelementptr i64, i64* %511, i32 5
  %512 = load i64, i64* %arrayidx547, align 8
  %513 = load i64*, i64** %S.addr, align 4
  %arrayidx548 = getelementptr i64, i64* %513, i32 6
  %514 = load i64, i64* %arrayidx548, align 8
  %515 = load i64*, i64** %S.addr, align 4
  %arrayidx549 = getelementptr i64, i64* %515, i32 7
  %516 = load i64, i64* %arrayidx549, align 8
  %or550 = or i64 %514, %516
  %and551 = and i64 %512, %or550
  %517 = load i64*, i64** %S.addr, align 4
  %arrayidx552 = getelementptr i64, i64* %517, i32 6
  %518 = load i64, i64* %arrayidx552, align 8
  %519 = load i64*, i64** %S.addr, align 4
  %arrayidx553 = getelementptr i64, i64* %519, i32 7
  %520 = load i64, i64* %arrayidx553, align 8
  %and554 = and i64 %518, %520
  %or555 = or i64 %and551, %and554
  %add556 = add i64 %xor546, %or555
  %521 = load i64*, i64** %S.addr, align 4
  %arrayidx557 = getelementptr i64, i64* %521, i32 4
  %522 = load i64, i64* %arrayidx557, align 8
  %add558 = add i64 %522, %add556
  store i64 %add558, i64* %arrayidx557, align 8
  %523 = load i64*, i64** %S.addr, align 4
  %arrayidx559 = getelementptr i64, i64* %523, i32 0
  %524 = load i64, i64* %arrayidx559, align 8
  %call560 = call i64 @rotr64(i64 %524, i32 14)
  %525 = load i64*, i64** %S.addr, align 4
  %arrayidx561 = getelementptr i64, i64* %525, i32 0
  %526 = load i64, i64* %arrayidx561, align 8
  %call562 = call i64 @rotr64(i64 %526, i32 18)
  %xor563 = xor i64 %call560, %call562
  %527 = load i64*, i64** %S.addr, align 4
  %arrayidx564 = getelementptr i64, i64* %527, i32 0
  %528 = load i64, i64* %arrayidx564, align 8
  %call565 = call i64 @rotr64(i64 %528, i32 41)
  %xor566 = xor i64 %xor563, %call565
  %529 = load i64*, i64** %S.addr, align 4
  %arrayidx567 = getelementptr i64, i64* %529, i32 0
  %530 = load i64, i64* %arrayidx567, align 8
  %531 = load i64*, i64** %S.addr, align 4
  %arrayidx568 = getelementptr i64, i64* %531, i32 1
  %532 = load i64, i64* %arrayidx568, align 8
  %533 = load i64*, i64** %S.addr, align 4
  %arrayidx569 = getelementptr i64, i64* %533, i32 2
  %534 = load i64, i64* %arrayidx569, align 8
  %xor570 = xor i64 %532, %534
  %and571 = and i64 %530, %xor570
  %535 = load i64*, i64** %S.addr, align 4
  %arrayidx572 = getelementptr i64, i64* %535, i32 2
  %536 = load i64, i64* %arrayidx572, align 8
  %xor573 = xor i64 %and571, %536
  %add574 = add i64 %xor566, %xor573
  %537 = load i64*, i64** %W.addr, align 4
  %538 = load i32, i32* %i, align 4
  %add575 = add i32 12, %538
  %arrayidx576 = getelementptr i64, i64* %537, i32 %add575
  %539 = load i64, i64* %arrayidx576, align 8
  %add577 = add i64 %add574, %539
  %540 = load i32, i32* %i, align 4
  %add578 = add i32 12, %540
  %arrayidx579 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add578
  %541 = load i64, i64* %arrayidx579, align 8
  %add580 = add i64 %add577, %541
  %542 = load i64*, i64** %S.addr, align 4
  %arrayidx581 = getelementptr i64, i64* %542, i32 3
  %543 = load i64, i64* %arrayidx581, align 8
  %add582 = add i64 %543, %add580
  store i64 %add582, i64* %arrayidx581, align 8
  %544 = load i64*, i64** %S.addr, align 4
  %arrayidx583 = getelementptr i64, i64* %544, i32 3
  %545 = load i64, i64* %arrayidx583, align 8
  %546 = load i64*, i64** %S.addr, align 4
  %arrayidx584 = getelementptr i64, i64* %546, i32 7
  %547 = load i64, i64* %arrayidx584, align 8
  %add585 = add i64 %547, %545
  store i64 %add585, i64* %arrayidx584, align 8
  %548 = load i64*, i64** %S.addr, align 4
  %arrayidx586 = getelementptr i64, i64* %548, i32 4
  %549 = load i64, i64* %arrayidx586, align 8
  %call587 = call i64 @rotr64(i64 %549, i32 28)
  %550 = load i64*, i64** %S.addr, align 4
  %arrayidx588 = getelementptr i64, i64* %550, i32 4
  %551 = load i64, i64* %arrayidx588, align 8
  %call589 = call i64 @rotr64(i64 %551, i32 34)
  %xor590 = xor i64 %call587, %call589
  %552 = load i64*, i64** %S.addr, align 4
  %arrayidx591 = getelementptr i64, i64* %552, i32 4
  %553 = load i64, i64* %arrayidx591, align 8
  %call592 = call i64 @rotr64(i64 %553, i32 39)
  %xor593 = xor i64 %xor590, %call592
  %554 = load i64*, i64** %S.addr, align 4
  %arrayidx594 = getelementptr i64, i64* %554, i32 4
  %555 = load i64, i64* %arrayidx594, align 8
  %556 = load i64*, i64** %S.addr, align 4
  %arrayidx595 = getelementptr i64, i64* %556, i32 5
  %557 = load i64, i64* %arrayidx595, align 8
  %558 = load i64*, i64** %S.addr, align 4
  %arrayidx596 = getelementptr i64, i64* %558, i32 6
  %559 = load i64, i64* %arrayidx596, align 8
  %or597 = or i64 %557, %559
  %and598 = and i64 %555, %or597
  %560 = load i64*, i64** %S.addr, align 4
  %arrayidx599 = getelementptr i64, i64* %560, i32 5
  %561 = load i64, i64* %arrayidx599, align 8
  %562 = load i64*, i64** %S.addr, align 4
  %arrayidx600 = getelementptr i64, i64* %562, i32 6
  %563 = load i64, i64* %arrayidx600, align 8
  %and601 = and i64 %561, %563
  %or602 = or i64 %and598, %and601
  %add603 = add i64 %xor593, %or602
  %564 = load i64*, i64** %S.addr, align 4
  %arrayidx604 = getelementptr i64, i64* %564, i32 3
  %565 = load i64, i64* %arrayidx604, align 8
  %add605 = add i64 %565, %add603
  store i64 %add605, i64* %arrayidx604, align 8
  %566 = load i64*, i64** %S.addr, align 4
  %arrayidx606 = getelementptr i64, i64* %566, i32 7
  %567 = load i64, i64* %arrayidx606, align 8
  %call607 = call i64 @rotr64(i64 %567, i32 14)
  %568 = load i64*, i64** %S.addr, align 4
  %arrayidx608 = getelementptr i64, i64* %568, i32 7
  %569 = load i64, i64* %arrayidx608, align 8
  %call609 = call i64 @rotr64(i64 %569, i32 18)
  %xor610 = xor i64 %call607, %call609
  %570 = load i64*, i64** %S.addr, align 4
  %arrayidx611 = getelementptr i64, i64* %570, i32 7
  %571 = load i64, i64* %arrayidx611, align 8
  %call612 = call i64 @rotr64(i64 %571, i32 41)
  %xor613 = xor i64 %xor610, %call612
  %572 = load i64*, i64** %S.addr, align 4
  %arrayidx614 = getelementptr i64, i64* %572, i32 7
  %573 = load i64, i64* %arrayidx614, align 8
  %574 = load i64*, i64** %S.addr, align 4
  %arrayidx615 = getelementptr i64, i64* %574, i32 0
  %575 = load i64, i64* %arrayidx615, align 8
  %576 = load i64*, i64** %S.addr, align 4
  %arrayidx616 = getelementptr i64, i64* %576, i32 1
  %577 = load i64, i64* %arrayidx616, align 8
  %xor617 = xor i64 %575, %577
  %and618 = and i64 %573, %xor617
  %578 = load i64*, i64** %S.addr, align 4
  %arrayidx619 = getelementptr i64, i64* %578, i32 1
  %579 = load i64, i64* %arrayidx619, align 8
  %xor620 = xor i64 %and618, %579
  %add621 = add i64 %xor613, %xor620
  %580 = load i64*, i64** %W.addr, align 4
  %581 = load i32, i32* %i, align 4
  %add622 = add i32 13, %581
  %arrayidx623 = getelementptr i64, i64* %580, i32 %add622
  %582 = load i64, i64* %arrayidx623, align 8
  %add624 = add i64 %add621, %582
  %583 = load i32, i32* %i, align 4
  %add625 = add i32 13, %583
  %arrayidx626 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add625
  %584 = load i64, i64* %arrayidx626, align 8
  %add627 = add i64 %add624, %584
  %585 = load i64*, i64** %S.addr, align 4
  %arrayidx628 = getelementptr i64, i64* %585, i32 2
  %586 = load i64, i64* %arrayidx628, align 8
  %add629 = add i64 %586, %add627
  store i64 %add629, i64* %arrayidx628, align 8
  %587 = load i64*, i64** %S.addr, align 4
  %arrayidx630 = getelementptr i64, i64* %587, i32 2
  %588 = load i64, i64* %arrayidx630, align 8
  %589 = load i64*, i64** %S.addr, align 4
  %arrayidx631 = getelementptr i64, i64* %589, i32 6
  %590 = load i64, i64* %arrayidx631, align 8
  %add632 = add i64 %590, %588
  store i64 %add632, i64* %arrayidx631, align 8
  %591 = load i64*, i64** %S.addr, align 4
  %arrayidx633 = getelementptr i64, i64* %591, i32 3
  %592 = load i64, i64* %arrayidx633, align 8
  %call634 = call i64 @rotr64(i64 %592, i32 28)
  %593 = load i64*, i64** %S.addr, align 4
  %arrayidx635 = getelementptr i64, i64* %593, i32 3
  %594 = load i64, i64* %arrayidx635, align 8
  %call636 = call i64 @rotr64(i64 %594, i32 34)
  %xor637 = xor i64 %call634, %call636
  %595 = load i64*, i64** %S.addr, align 4
  %arrayidx638 = getelementptr i64, i64* %595, i32 3
  %596 = load i64, i64* %arrayidx638, align 8
  %call639 = call i64 @rotr64(i64 %596, i32 39)
  %xor640 = xor i64 %xor637, %call639
  %597 = load i64*, i64** %S.addr, align 4
  %arrayidx641 = getelementptr i64, i64* %597, i32 3
  %598 = load i64, i64* %arrayidx641, align 8
  %599 = load i64*, i64** %S.addr, align 4
  %arrayidx642 = getelementptr i64, i64* %599, i32 4
  %600 = load i64, i64* %arrayidx642, align 8
  %601 = load i64*, i64** %S.addr, align 4
  %arrayidx643 = getelementptr i64, i64* %601, i32 5
  %602 = load i64, i64* %arrayidx643, align 8
  %or644 = or i64 %600, %602
  %and645 = and i64 %598, %or644
  %603 = load i64*, i64** %S.addr, align 4
  %arrayidx646 = getelementptr i64, i64* %603, i32 4
  %604 = load i64, i64* %arrayidx646, align 8
  %605 = load i64*, i64** %S.addr, align 4
  %arrayidx647 = getelementptr i64, i64* %605, i32 5
  %606 = load i64, i64* %arrayidx647, align 8
  %and648 = and i64 %604, %606
  %or649 = or i64 %and645, %and648
  %add650 = add i64 %xor640, %or649
  %607 = load i64*, i64** %S.addr, align 4
  %arrayidx651 = getelementptr i64, i64* %607, i32 2
  %608 = load i64, i64* %arrayidx651, align 8
  %add652 = add i64 %608, %add650
  store i64 %add652, i64* %arrayidx651, align 8
  %609 = load i64*, i64** %S.addr, align 4
  %arrayidx653 = getelementptr i64, i64* %609, i32 6
  %610 = load i64, i64* %arrayidx653, align 8
  %call654 = call i64 @rotr64(i64 %610, i32 14)
  %611 = load i64*, i64** %S.addr, align 4
  %arrayidx655 = getelementptr i64, i64* %611, i32 6
  %612 = load i64, i64* %arrayidx655, align 8
  %call656 = call i64 @rotr64(i64 %612, i32 18)
  %xor657 = xor i64 %call654, %call656
  %613 = load i64*, i64** %S.addr, align 4
  %arrayidx658 = getelementptr i64, i64* %613, i32 6
  %614 = load i64, i64* %arrayidx658, align 8
  %call659 = call i64 @rotr64(i64 %614, i32 41)
  %xor660 = xor i64 %xor657, %call659
  %615 = load i64*, i64** %S.addr, align 4
  %arrayidx661 = getelementptr i64, i64* %615, i32 6
  %616 = load i64, i64* %arrayidx661, align 8
  %617 = load i64*, i64** %S.addr, align 4
  %arrayidx662 = getelementptr i64, i64* %617, i32 7
  %618 = load i64, i64* %arrayidx662, align 8
  %619 = load i64*, i64** %S.addr, align 4
  %arrayidx663 = getelementptr i64, i64* %619, i32 0
  %620 = load i64, i64* %arrayidx663, align 8
  %xor664 = xor i64 %618, %620
  %and665 = and i64 %616, %xor664
  %621 = load i64*, i64** %S.addr, align 4
  %arrayidx666 = getelementptr i64, i64* %621, i32 0
  %622 = load i64, i64* %arrayidx666, align 8
  %xor667 = xor i64 %and665, %622
  %add668 = add i64 %xor660, %xor667
  %623 = load i64*, i64** %W.addr, align 4
  %624 = load i32, i32* %i, align 4
  %add669 = add i32 14, %624
  %arrayidx670 = getelementptr i64, i64* %623, i32 %add669
  %625 = load i64, i64* %arrayidx670, align 8
  %add671 = add i64 %add668, %625
  %626 = load i32, i32* %i, align 4
  %add672 = add i32 14, %626
  %arrayidx673 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add672
  %627 = load i64, i64* %arrayidx673, align 8
  %add674 = add i64 %add671, %627
  %628 = load i64*, i64** %S.addr, align 4
  %arrayidx675 = getelementptr i64, i64* %628, i32 1
  %629 = load i64, i64* %arrayidx675, align 8
  %add676 = add i64 %629, %add674
  store i64 %add676, i64* %arrayidx675, align 8
  %630 = load i64*, i64** %S.addr, align 4
  %arrayidx677 = getelementptr i64, i64* %630, i32 1
  %631 = load i64, i64* %arrayidx677, align 8
  %632 = load i64*, i64** %S.addr, align 4
  %arrayidx678 = getelementptr i64, i64* %632, i32 5
  %633 = load i64, i64* %arrayidx678, align 8
  %add679 = add i64 %633, %631
  store i64 %add679, i64* %arrayidx678, align 8
  %634 = load i64*, i64** %S.addr, align 4
  %arrayidx680 = getelementptr i64, i64* %634, i32 2
  %635 = load i64, i64* %arrayidx680, align 8
  %call681 = call i64 @rotr64(i64 %635, i32 28)
  %636 = load i64*, i64** %S.addr, align 4
  %arrayidx682 = getelementptr i64, i64* %636, i32 2
  %637 = load i64, i64* %arrayidx682, align 8
  %call683 = call i64 @rotr64(i64 %637, i32 34)
  %xor684 = xor i64 %call681, %call683
  %638 = load i64*, i64** %S.addr, align 4
  %arrayidx685 = getelementptr i64, i64* %638, i32 2
  %639 = load i64, i64* %arrayidx685, align 8
  %call686 = call i64 @rotr64(i64 %639, i32 39)
  %xor687 = xor i64 %xor684, %call686
  %640 = load i64*, i64** %S.addr, align 4
  %arrayidx688 = getelementptr i64, i64* %640, i32 2
  %641 = load i64, i64* %arrayidx688, align 8
  %642 = load i64*, i64** %S.addr, align 4
  %arrayidx689 = getelementptr i64, i64* %642, i32 3
  %643 = load i64, i64* %arrayidx689, align 8
  %644 = load i64*, i64** %S.addr, align 4
  %arrayidx690 = getelementptr i64, i64* %644, i32 4
  %645 = load i64, i64* %arrayidx690, align 8
  %or691 = or i64 %643, %645
  %and692 = and i64 %641, %or691
  %646 = load i64*, i64** %S.addr, align 4
  %arrayidx693 = getelementptr i64, i64* %646, i32 3
  %647 = load i64, i64* %arrayidx693, align 8
  %648 = load i64*, i64** %S.addr, align 4
  %arrayidx694 = getelementptr i64, i64* %648, i32 4
  %649 = load i64, i64* %arrayidx694, align 8
  %and695 = and i64 %647, %649
  %or696 = or i64 %and692, %and695
  %add697 = add i64 %xor687, %or696
  %650 = load i64*, i64** %S.addr, align 4
  %arrayidx698 = getelementptr i64, i64* %650, i32 1
  %651 = load i64, i64* %arrayidx698, align 8
  %add699 = add i64 %651, %add697
  store i64 %add699, i64* %arrayidx698, align 8
  %652 = load i64*, i64** %S.addr, align 4
  %arrayidx700 = getelementptr i64, i64* %652, i32 5
  %653 = load i64, i64* %arrayidx700, align 8
  %call701 = call i64 @rotr64(i64 %653, i32 14)
  %654 = load i64*, i64** %S.addr, align 4
  %arrayidx702 = getelementptr i64, i64* %654, i32 5
  %655 = load i64, i64* %arrayidx702, align 8
  %call703 = call i64 @rotr64(i64 %655, i32 18)
  %xor704 = xor i64 %call701, %call703
  %656 = load i64*, i64** %S.addr, align 4
  %arrayidx705 = getelementptr i64, i64* %656, i32 5
  %657 = load i64, i64* %arrayidx705, align 8
  %call706 = call i64 @rotr64(i64 %657, i32 41)
  %xor707 = xor i64 %xor704, %call706
  %658 = load i64*, i64** %S.addr, align 4
  %arrayidx708 = getelementptr i64, i64* %658, i32 5
  %659 = load i64, i64* %arrayidx708, align 8
  %660 = load i64*, i64** %S.addr, align 4
  %arrayidx709 = getelementptr i64, i64* %660, i32 6
  %661 = load i64, i64* %arrayidx709, align 8
  %662 = load i64*, i64** %S.addr, align 4
  %arrayidx710 = getelementptr i64, i64* %662, i32 7
  %663 = load i64, i64* %arrayidx710, align 8
  %xor711 = xor i64 %661, %663
  %and712 = and i64 %659, %xor711
  %664 = load i64*, i64** %S.addr, align 4
  %arrayidx713 = getelementptr i64, i64* %664, i32 7
  %665 = load i64, i64* %arrayidx713, align 8
  %xor714 = xor i64 %and712, %665
  %add715 = add i64 %xor707, %xor714
  %666 = load i64*, i64** %W.addr, align 4
  %667 = load i32, i32* %i, align 4
  %add716 = add i32 15, %667
  %arrayidx717 = getelementptr i64, i64* %666, i32 %add716
  %668 = load i64, i64* %arrayidx717, align 8
  %add718 = add i64 %add715, %668
  %669 = load i32, i32* %i, align 4
  %add719 = add i32 15, %669
  %arrayidx720 = getelementptr [80 x i64], [80 x i64]* @Krnd, i32 0, i32 %add719
  %670 = load i64, i64* %arrayidx720, align 8
  %add721 = add i64 %add718, %670
  %671 = load i64*, i64** %S.addr, align 4
  %arrayidx722 = getelementptr i64, i64* %671, i32 0
  %672 = load i64, i64* %arrayidx722, align 8
  %add723 = add i64 %672, %add721
  store i64 %add723, i64* %arrayidx722, align 8
  %673 = load i64*, i64** %S.addr, align 4
  %arrayidx724 = getelementptr i64, i64* %673, i32 0
  %674 = load i64, i64* %arrayidx724, align 8
  %675 = load i64*, i64** %S.addr, align 4
  %arrayidx725 = getelementptr i64, i64* %675, i32 4
  %676 = load i64, i64* %arrayidx725, align 8
  %add726 = add i64 %676, %674
  store i64 %add726, i64* %arrayidx725, align 8
  %677 = load i64*, i64** %S.addr, align 4
  %arrayidx727 = getelementptr i64, i64* %677, i32 1
  %678 = load i64, i64* %arrayidx727, align 8
  %call728 = call i64 @rotr64(i64 %678, i32 28)
  %679 = load i64*, i64** %S.addr, align 4
  %arrayidx729 = getelementptr i64, i64* %679, i32 1
  %680 = load i64, i64* %arrayidx729, align 8
  %call730 = call i64 @rotr64(i64 %680, i32 34)
  %xor731 = xor i64 %call728, %call730
  %681 = load i64*, i64** %S.addr, align 4
  %arrayidx732 = getelementptr i64, i64* %681, i32 1
  %682 = load i64, i64* %arrayidx732, align 8
  %call733 = call i64 @rotr64(i64 %682, i32 39)
  %xor734 = xor i64 %xor731, %call733
  %683 = load i64*, i64** %S.addr, align 4
  %arrayidx735 = getelementptr i64, i64* %683, i32 1
  %684 = load i64, i64* %arrayidx735, align 8
  %685 = load i64*, i64** %S.addr, align 4
  %arrayidx736 = getelementptr i64, i64* %685, i32 2
  %686 = load i64, i64* %arrayidx736, align 8
  %687 = load i64*, i64** %S.addr, align 4
  %arrayidx737 = getelementptr i64, i64* %687, i32 3
  %688 = load i64, i64* %arrayidx737, align 8
  %or738 = or i64 %686, %688
  %and739 = and i64 %684, %or738
  %689 = load i64*, i64** %S.addr, align 4
  %arrayidx740 = getelementptr i64, i64* %689, i32 2
  %690 = load i64, i64* %arrayidx740, align 8
  %691 = load i64*, i64** %S.addr, align 4
  %arrayidx741 = getelementptr i64, i64* %691, i32 3
  %692 = load i64, i64* %arrayidx741, align 8
  %and742 = and i64 %690, %692
  %or743 = or i64 %and739, %and742
  %add744 = add i64 %xor734, %or743
  %693 = load i64*, i64** %S.addr, align 4
  %arrayidx745 = getelementptr i64, i64* %693, i32 0
  %694 = load i64, i64* %arrayidx745, align 8
  %add746 = add i64 %694, %add744
  store i64 %add746, i64* %arrayidx745, align 8
  %695 = load i32, i32* %i, align 4
  %cmp747 = icmp eq i32 %695, 64
  br i1 %cmp747, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  %696 = load i64*, i64** %W.addr, align 4
  %697 = load i32, i32* %i, align 4
  %add748 = add i32 %697, 0
  %add749 = add i32 %add748, 14
  %arrayidx750 = getelementptr i64, i64* %696, i32 %add749
  %698 = load i64, i64* %arrayidx750, align 8
  %call751 = call i64 @rotr64(i64 %698, i32 19)
  %699 = load i64*, i64** %W.addr, align 4
  %700 = load i32, i32* %i, align 4
  %add752 = add i32 %700, 0
  %add753 = add i32 %add752, 14
  %arrayidx754 = getelementptr i64, i64* %699, i32 %add753
  %701 = load i64, i64* %arrayidx754, align 8
  %call755 = call i64 @rotr64(i64 %701, i32 61)
  %xor756 = xor i64 %call751, %call755
  %702 = load i64*, i64** %W.addr, align 4
  %703 = load i32, i32* %i, align 4
  %add757 = add i32 %703, 0
  %add758 = add i32 %add757, 14
  %arrayidx759 = getelementptr i64, i64* %702, i32 %add758
  %704 = load i64, i64* %arrayidx759, align 8
  %shr = lshr i64 %704, 6
  %xor760 = xor i64 %xor756, %shr
  %705 = load i64*, i64** %W.addr, align 4
  %706 = load i32, i32* %i, align 4
  %add761 = add i32 %706, 0
  %add762 = add i32 %add761, 9
  %arrayidx763 = getelementptr i64, i64* %705, i32 %add762
  %707 = load i64, i64* %arrayidx763, align 8
  %add764 = add i64 %xor760, %707
  %708 = load i64*, i64** %W.addr, align 4
  %709 = load i32, i32* %i, align 4
  %add765 = add i32 %709, 0
  %add766 = add i32 %add765, 1
  %arrayidx767 = getelementptr i64, i64* %708, i32 %add766
  %710 = load i64, i64* %arrayidx767, align 8
  %call768 = call i64 @rotr64(i64 %710, i32 1)
  %711 = load i64*, i64** %W.addr, align 4
  %712 = load i32, i32* %i, align 4
  %add769 = add i32 %712, 0
  %add770 = add i32 %add769, 1
  %arrayidx771 = getelementptr i64, i64* %711, i32 %add770
  %713 = load i64, i64* %arrayidx771, align 8
  %call772 = call i64 @rotr64(i64 %713, i32 8)
  %xor773 = xor i64 %call768, %call772
  %714 = load i64*, i64** %W.addr, align 4
  %715 = load i32, i32* %i, align 4
  %add774 = add i32 %715, 0
  %add775 = add i32 %add774, 1
  %arrayidx776 = getelementptr i64, i64* %714, i32 %add775
  %716 = load i64, i64* %arrayidx776, align 8
  %shr777 = lshr i64 %716, 7
  %xor778 = xor i64 %xor773, %shr777
  %add779 = add i64 %add764, %xor778
  %717 = load i64*, i64** %W.addr, align 4
  %718 = load i32, i32* %i, align 4
  %add780 = add i32 %718, 0
  %arrayidx781 = getelementptr i64, i64* %717, i32 %add780
  %719 = load i64, i64* %arrayidx781, align 8
  %add782 = add i64 %add779, %719
  %720 = load i64*, i64** %W.addr, align 4
  %721 = load i32, i32* %i, align 4
  %add783 = add i32 %721, 0
  %add784 = add i32 %add783, 16
  %arrayidx785 = getelementptr i64, i64* %720, i32 %add784
  store i64 %add782, i64* %arrayidx785, align 8
  %722 = load i64*, i64** %W.addr, align 4
  %723 = load i32, i32* %i, align 4
  %add786 = add i32 %723, 1
  %add787 = add i32 %add786, 14
  %arrayidx788 = getelementptr i64, i64* %722, i32 %add787
  %724 = load i64, i64* %arrayidx788, align 8
  %call789 = call i64 @rotr64(i64 %724, i32 19)
  %725 = load i64*, i64** %W.addr, align 4
  %726 = load i32, i32* %i, align 4
  %add790 = add i32 %726, 1
  %add791 = add i32 %add790, 14
  %arrayidx792 = getelementptr i64, i64* %725, i32 %add791
  %727 = load i64, i64* %arrayidx792, align 8
  %call793 = call i64 @rotr64(i64 %727, i32 61)
  %xor794 = xor i64 %call789, %call793
  %728 = load i64*, i64** %W.addr, align 4
  %729 = load i32, i32* %i, align 4
  %add795 = add i32 %729, 1
  %add796 = add i32 %add795, 14
  %arrayidx797 = getelementptr i64, i64* %728, i32 %add796
  %730 = load i64, i64* %arrayidx797, align 8
  %shr798 = lshr i64 %730, 6
  %xor799 = xor i64 %xor794, %shr798
  %731 = load i64*, i64** %W.addr, align 4
  %732 = load i32, i32* %i, align 4
  %add800 = add i32 %732, 1
  %add801 = add i32 %add800, 9
  %arrayidx802 = getelementptr i64, i64* %731, i32 %add801
  %733 = load i64, i64* %arrayidx802, align 8
  %add803 = add i64 %xor799, %733
  %734 = load i64*, i64** %W.addr, align 4
  %735 = load i32, i32* %i, align 4
  %add804 = add i32 %735, 1
  %add805 = add i32 %add804, 1
  %arrayidx806 = getelementptr i64, i64* %734, i32 %add805
  %736 = load i64, i64* %arrayidx806, align 8
  %call807 = call i64 @rotr64(i64 %736, i32 1)
  %737 = load i64*, i64** %W.addr, align 4
  %738 = load i32, i32* %i, align 4
  %add808 = add i32 %738, 1
  %add809 = add i32 %add808, 1
  %arrayidx810 = getelementptr i64, i64* %737, i32 %add809
  %739 = load i64, i64* %arrayidx810, align 8
  %call811 = call i64 @rotr64(i64 %739, i32 8)
  %xor812 = xor i64 %call807, %call811
  %740 = load i64*, i64** %W.addr, align 4
  %741 = load i32, i32* %i, align 4
  %add813 = add i32 %741, 1
  %add814 = add i32 %add813, 1
  %arrayidx815 = getelementptr i64, i64* %740, i32 %add814
  %742 = load i64, i64* %arrayidx815, align 8
  %shr816 = lshr i64 %742, 7
  %xor817 = xor i64 %xor812, %shr816
  %add818 = add i64 %add803, %xor817
  %743 = load i64*, i64** %W.addr, align 4
  %744 = load i32, i32* %i, align 4
  %add819 = add i32 %744, 1
  %arrayidx820 = getelementptr i64, i64* %743, i32 %add819
  %745 = load i64, i64* %arrayidx820, align 8
  %add821 = add i64 %add818, %745
  %746 = load i64*, i64** %W.addr, align 4
  %747 = load i32, i32* %i, align 4
  %add822 = add i32 %747, 1
  %add823 = add i32 %add822, 16
  %arrayidx824 = getelementptr i64, i64* %746, i32 %add823
  store i64 %add821, i64* %arrayidx824, align 8
  %748 = load i64*, i64** %W.addr, align 4
  %749 = load i32, i32* %i, align 4
  %add825 = add i32 %749, 2
  %add826 = add i32 %add825, 14
  %arrayidx827 = getelementptr i64, i64* %748, i32 %add826
  %750 = load i64, i64* %arrayidx827, align 8
  %call828 = call i64 @rotr64(i64 %750, i32 19)
  %751 = load i64*, i64** %W.addr, align 4
  %752 = load i32, i32* %i, align 4
  %add829 = add i32 %752, 2
  %add830 = add i32 %add829, 14
  %arrayidx831 = getelementptr i64, i64* %751, i32 %add830
  %753 = load i64, i64* %arrayidx831, align 8
  %call832 = call i64 @rotr64(i64 %753, i32 61)
  %xor833 = xor i64 %call828, %call832
  %754 = load i64*, i64** %W.addr, align 4
  %755 = load i32, i32* %i, align 4
  %add834 = add i32 %755, 2
  %add835 = add i32 %add834, 14
  %arrayidx836 = getelementptr i64, i64* %754, i32 %add835
  %756 = load i64, i64* %arrayidx836, align 8
  %shr837 = lshr i64 %756, 6
  %xor838 = xor i64 %xor833, %shr837
  %757 = load i64*, i64** %W.addr, align 4
  %758 = load i32, i32* %i, align 4
  %add839 = add i32 %758, 2
  %add840 = add i32 %add839, 9
  %arrayidx841 = getelementptr i64, i64* %757, i32 %add840
  %759 = load i64, i64* %arrayidx841, align 8
  %add842 = add i64 %xor838, %759
  %760 = load i64*, i64** %W.addr, align 4
  %761 = load i32, i32* %i, align 4
  %add843 = add i32 %761, 2
  %add844 = add i32 %add843, 1
  %arrayidx845 = getelementptr i64, i64* %760, i32 %add844
  %762 = load i64, i64* %arrayidx845, align 8
  %call846 = call i64 @rotr64(i64 %762, i32 1)
  %763 = load i64*, i64** %W.addr, align 4
  %764 = load i32, i32* %i, align 4
  %add847 = add i32 %764, 2
  %add848 = add i32 %add847, 1
  %arrayidx849 = getelementptr i64, i64* %763, i32 %add848
  %765 = load i64, i64* %arrayidx849, align 8
  %call850 = call i64 @rotr64(i64 %765, i32 8)
  %xor851 = xor i64 %call846, %call850
  %766 = load i64*, i64** %W.addr, align 4
  %767 = load i32, i32* %i, align 4
  %add852 = add i32 %767, 2
  %add853 = add i32 %add852, 1
  %arrayidx854 = getelementptr i64, i64* %766, i32 %add853
  %768 = load i64, i64* %arrayidx854, align 8
  %shr855 = lshr i64 %768, 7
  %xor856 = xor i64 %xor851, %shr855
  %add857 = add i64 %add842, %xor856
  %769 = load i64*, i64** %W.addr, align 4
  %770 = load i32, i32* %i, align 4
  %add858 = add i32 %770, 2
  %arrayidx859 = getelementptr i64, i64* %769, i32 %add858
  %771 = load i64, i64* %arrayidx859, align 8
  %add860 = add i64 %add857, %771
  %772 = load i64*, i64** %W.addr, align 4
  %773 = load i32, i32* %i, align 4
  %add861 = add i32 %773, 2
  %add862 = add i32 %add861, 16
  %arrayidx863 = getelementptr i64, i64* %772, i32 %add862
  store i64 %add860, i64* %arrayidx863, align 8
  %774 = load i64*, i64** %W.addr, align 4
  %775 = load i32, i32* %i, align 4
  %add864 = add i32 %775, 3
  %add865 = add i32 %add864, 14
  %arrayidx866 = getelementptr i64, i64* %774, i32 %add865
  %776 = load i64, i64* %arrayidx866, align 8
  %call867 = call i64 @rotr64(i64 %776, i32 19)
  %777 = load i64*, i64** %W.addr, align 4
  %778 = load i32, i32* %i, align 4
  %add868 = add i32 %778, 3
  %add869 = add i32 %add868, 14
  %arrayidx870 = getelementptr i64, i64* %777, i32 %add869
  %779 = load i64, i64* %arrayidx870, align 8
  %call871 = call i64 @rotr64(i64 %779, i32 61)
  %xor872 = xor i64 %call867, %call871
  %780 = load i64*, i64** %W.addr, align 4
  %781 = load i32, i32* %i, align 4
  %add873 = add i32 %781, 3
  %add874 = add i32 %add873, 14
  %arrayidx875 = getelementptr i64, i64* %780, i32 %add874
  %782 = load i64, i64* %arrayidx875, align 8
  %shr876 = lshr i64 %782, 6
  %xor877 = xor i64 %xor872, %shr876
  %783 = load i64*, i64** %W.addr, align 4
  %784 = load i32, i32* %i, align 4
  %add878 = add i32 %784, 3
  %add879 = add i32 %add878, 9
  %arrayidx880 = getelementptr i64, i64* %783, i32 %add879
  %785 = load i64, i64* %arrayidx880, align 8
  %add881 = add i64 %xor877, %785
  %786 = load i64*, i64** %W.addr, align 4
  %787 = load i32, i32* %i, align 4
  %add882 = add i32 %787, 3
  %add883 = add i32 %add882, 1
  %arrayidx884 = getelementptr i64, i64* %786, i32 %add883
  %788 = load i64, i64* %arrayidx884, align 8
  %call885 = call i64 @rotr64(i64 %788, i32 1)
  %789 = load i64*, i64** %W.addr, align 4
  %790 = load i32, i32* %i, align 4
  %add886 = add i32 %790, 3
  %add887 = add i32 %add886, 1
  %arrayidx888 = getelementptr i64, i64* %789, i32 %add887
  %791 = load i64, i64* %arrayidx888, align 8
  %call889 = call i64 @rotr64(i64 %791, i32 8)
  %xor890 = xor i64 %call885, %call889
  %792 = load i64*, i64** %W.addr, align 4
  %793 = load i32, i32* %i, align 4
  %add891 = add i32 %793, 3
  %add892 = add i32 %add891, 1
  %arrayidx893 = getelementptr i64, i64* %792, i32 %add892
  %794 = load i64, i64* %arrayidx893, align 8
  %shr894 = lshr i64 %794, 7
  %xor895 = xor i64 %xor890, %shr894
  %add896 = add i64 %add881, %xor895
  %795 = load i64*, i64** %W.addr, align 4
  %796 = load i32, i32* %i, align 4
  %add897 = add i32 %796, 3
  %arrayidx898 = getelementptr i64, i64* %795, i32 %add897
  %797 = load i64, i64* %arrayidx898, align 8
  %add899 = add i64 %add896, %797
  %798 = load i64*, i64** %W.addr, align 4
  %799 = load i32, i32* %i, align 4
  %add900 = add i32 %799, 3
  %add901 = add i32 %add900, 16
  %arrayidx902 = getelementptr i64, i64* %798, i32 %add901
  store i64 %add899, i64* %arrayidx902, align 8
  %800 = load i64*, i64** %W.addr, align 4
  %801 = load i32, i32* %i, align 4
  %add903 = add i32 %801, 4
  %add904 = add i32 %add903, 14
  %arrayidx905 = getelementptr i64, i64* %800, i32 %add904
  %802 = load i64, i64* %arrayidx905, align 8
  %call906 = call i64 @rotr64(i64 %802, i32 19)
  %803 = load i64*, i64** %W.addr, align 4
  %804 = load i32, i32* %i, align 4
  %add907 = add i32 %804, 4
  %add908 = add i32 %add907, 14
  %arrayidx909 = getelementptr i64, i64* %803, i32 %add908
  %805 = load i64, i64* %arrayidx909, align 8
  %call910 = call i64 @rotr64(i64 %805, i32 61)
  %xor911 = xor i64 %call906, %call910
  %806 = load i64*, i64** %W.addr, align 4
  %807 = load i32, i32* %i, align 4
  %add912 = add i32 %807, 4
  %add913 = add i32 %add912, 14
  %arrayidx914 = getelementptr i64, i64* %806, i32 %add913
  %808 = load i64, i64* %arrayidx914, align 8
  %shr915 = lshr i64 %808, 6
  %xor916 = xor i64 %xor911, %shr915
  %809 = load i64*, i64** %W.addr, align 4
  %810 = load i32, i32* %i, align 4
  %add917 = add i32 %810, 4
  %add918 = add i32 %add917, 9
  %arrayidx919 = getelementptr i64, i64* %809, i32 %add918
  %811 = load i64, i64* %arrayidx919, align 8
  %add920 = add i64 %xor916, %811
  %812 = load i64*, i64** %W.addr, align 4
  %813 = load i32, i32* %i, align 4
  %add921 = add i32 %813, 4
  %add922 = add i32 %add921, 1
  %arrayidx923 = getelementptr i64, i64* %812, i32 %add922
  %814 = load i64, i64* %arrayidx923, align 8
  %call924 = call i64 @rotr64(i64 %814, i32 1)
  %815 = load i64*, i64** %W.addr, align 4
  %816 = load i32, i32* %i, align 4
  %add925 = add i32 %816, 4
  %add926 = add i32 %add925, 1
  %arrayidx927 = getelementptr i64, i64* %815, i32 %add926
  %817 = load i64, i64* %arrayidx927, align 8
  %call928 = call i64 @rotr64(i64 %817, i32 8)
  %xor929 = xor i64 %call924, %call928
  %818 = load i64*, i64** %W.addr, align 4
  %819 = load i32, i32* %i, align 4
  %add930 = add i32 %819, 4
  %add931 = add i32 %add930, 1
  %arrayidx932 = getelementptr i64, i64* %818, i32 %add931
  %820 = load i64, i64* %arrayidx932, align 8
  %shr933 = lshr i64 %820, 7
  %xor934 = xor i64 %xor929, %shr933
  %add935 = add i64 %add920, %xor934
  %821 = load i64*, i64** %W.addr, align 4
  %822 = load i32, i32* %i, align 4
  %add936 = add i32 %822, 4
  %arrayidx937 = getelementptr i64, i64* %821, i32 %add936
  %823 = load i64, i64* %arrayidx937, align 8
  %add938 = add i64 %add935, %823
  %824 = load i64*, i64** %W.addr, align 4
  %825 = load i32, i32* %i, align 4
  %add939 = add i32 %825, 4
  %add940 = add i32 %add939, 16
  %arrayidx941 = getelementptr i64, i64* %824, i32 %add940
  store i64 %add938, i64* %arrayidx941, align 8
  %826 = load i64*, i64** %W.addr, align 4
  %827 = load i32, i32* %i, align 4
  %add942 = add i32 %827, 5
  %add943 = add i32 %add942, 14
  %arrayidx944 = getelementptr i64, i64* %826, i32 %add943
  %828 = load i64, i64* %arrayidx944, align 8
  %call945 = call i64 @rotr64(i64 %828, i32 19)
  %829 = load i64*, i64** %W.addr, align 4
  %830 = load i32, i32* %i, align 4
  %add946 = add i32 %830, 5
  %add947 = add i32 %add946, 14
  %arrayidx948 = getelementptr i64, i64* %829, i32 %add947
  %831 = load i64, i64* %arrayidx948, align 8
  %call949 = call i64 @rotr64(i64 %831, i32 61)
  %xor950 = xor i64 %call945, %call949
  %832 = load i64*, i64** %W.addr, align 4
  %833 = load i32, i32* %i, align 4
  %add951 = add i32 %833, 5
  %add952 = add i32 %add951, 14
  %arrayidx953 = getelementptr i64, i64* %832, i32 %add952
  %834 = load i64, i64* %arrayidx953, align 8
  %shr954 = lshr i64 %834, 6
  %xor955 = xor i64 %xor950, %shr954
  %835 = load i64*, i64** %W.addr, align 4
  %836 = load i32, i32* %i, align 4
  %add956 = add i32 %836, 5
  %add957 = add i32 %add956, 9
  %arrayidx958 = getelementptr i64, i64* %835, i32 %add957
  %837 = load i64, i64* %arrayidx958, align 8
  %add959 = add i64 %xor955, %837
  %838 = load i64*, i64** %W.addr, align 4
  %839 = load i32, i32* %i, align 4
  %add960 = add i32 %839, 5
  %add961 = add i32 %add960, 1
  %arrayidx962 = getelementptr i64, i64* %838, i32 %add961
  %840 = load i64, i64* %arrayidx962, align 8
  %call963 = call i64 @rotr64(i64 %840, i32 1)
  %841 = load i64*, i64** %W.addr, align 4
  %842 = load i32, i32* %i, align 4
  %add964 = add i32 %842, 5
  %add965 = add i32 %add964, 1
  %arrayidx966 = getelementptr i64, i64* %841, i32 %add965
  %843 = load i64, i64* %arrayidx966, align 8
  %call967 = call i64 @rotr64(i64 %843, i32 8)
  %xor968 = xor i64 %call963, %call967
  %844 = load i64*, i64** %W.addr, align 4
  %845 = load i32, i32* %i, align 4
  %add969 = add i32 %845, 5
  %add970 = add i32 %add969, 1
  %arrayidx971 = getelementptr i64, i64* %844, i32 %add970
  %846 = load i64, i64* %arrayidx971, align 8
  %shr972 = lshr i64 %846, 7
  %xor973 = xor i64 %xor968, %shr972
  %add974 = add i64 %add959, %xor973
  %847 = load i64*, i64** %W.addr, align 4
  %848 = load i32, i32* %i, align 4
  %add975 = add i32 %848, 5
  %arrayidx976 = getelementptr i64, i64* %847, i32 %add975
  %849 = load i64, i64* %arrayidx976, align 8
  %add977 = add i64 %add974, %849
  %850 = load i64*, i64** %W.addr, align 4
  %851 = load i32, i32* %i, align 4
  %add978 = add i32 %851, 5
  %add979 = add i32 %add978, 16
  %arrayidx980 = getelementptr i64, i64* %850, i32 %add979
  store i64 %add977, i64* %arrayidx980, align 8
  %852 = load i64*, i64** %W.addr, align 4
  %853 = load i32, i32* %i, align 4
  %add981 = add i32 %853, 6
  %add982 = add i32 %add981, 14
  %arrayidx983 = getelementptr i64, i64* %852, i32 %add982
  %854 = load i64, i64* %arrayidx983, align 8
  %call984 = call i64 @rotr64(i64 %854, i32 19)
  %855 = load i64*, i64** %W.addr, align 4
  %856 = load i32, i32* %i, align 4
  %add985 = add i32 %856, 6
  %add986 = add i32 %add985, 14
  %arrayidx987 = getelementptr i64, i64* %855, i32 %add986
  %857 = load i64, i64* %arrayidx987, align 8
  %call988 = call i64 @rotr64(i64 %857, i32 61)
  %xor989 = xor i64 %call984, %call988
  %858 = load i64*, i64** %W.addr, align 4
  %859 = load i32, i32* %i, align 4
  %add990 = add i32 %859, 6
  %add991 = add i32 %add990, 14
  %arrayidx992 = getelementptr i64, i64* %858, i32 %add991
  %860 = load i64, i64* %arrayidx992, align 8
  %shr993 = lshr i64 %860, 6
  %xor994 = xor i64 %xor989, %shr993
  %861 = load i64*, i64** %W.addr, align 4
  %862 = load i32, i32* %i, align 4
  %add995 = add i32 %862, 6
  %add996 = add i32 %add995, 9
  %arrayidx997 = getelementptr i64, i64* %861, i32 %add996
  %863 = load i64, i64* %arrayidx997, align 8
  %add998 = add i64 %xor994, %863
  %864 = load i64*, i64** %W.addr, align 4
  %865 = load i32, i32* %i, align 4
  %add999 = add i32 %865, 6
  %add1000 = add i32 %add999, 1
  %arrayidx1001 = getelementptr i64, i64* %864, i32 %add1000
  %866 = load i64, i64* %arrayidx1001, align 8
  %call1002 = call i64 @rotr64(i64 %866, i32 1)
  %867 = load i64*, i64** %W.addr, align 4
  %868 = load i32, i32* %i, align 4
  %add1003 = add i32 %868, 6
  %add1004 = add i32 %add1003, 1
  %arrayidx1005 = getelementptr i64, i64* %867, i32 %add1004
  %869 = load i64, i64* %arrayidx1005, align 8
  %call1006 = call i64 @rotr64(i64 %869, i32 8)
  %xor1007 = xor i64 %call1002, %call1006
  %870 = load i64*, i64** %W.addr, align 4
  %871 = load i32, i32* %i, align 4
  %add1008 = add i32 %871, 6
  %add1009 = add i32 %add1008, 1
  %arrayidx1010 = getelementptr i64, i64* %870, i32 %add1009
  %872 = load i64, i64* %arrayidx1010, align 8
  %shr1011 = lshr i64 %872, 7
  %xor1012 = xor i64 %xor1007, %shr1011
  %add1013 = add i64 %add998, %xor1012
  %873 = load i64*, i64** %W.addr, align 4
  %874 = load i32, i32* %i, align 4
  %add1014 = add i32 %874, 6
  %arrayidx1015 = getelementptr i64, i64* %873, i32 %add1014
  %875 = load i64, i64* %arrayidx1015, align 8
  %add1016 = add i64 %add1013, %875
  %876 = load i64*, i64** %W.addr, align 4
  %877 = load i32, i32* %i, align 4
  %add1017 = add i32 %877, 6
  %add1018 = add i32 %add1017, 16
  %arrayidx1019 = getelementptr i64, i64* %876, i32 %add1018
  store i64 %add1016, i64* %arrayidx1019, align 8
  %878 = load i64*, i64** %W.addr, align 4
  %879 = load i32, i32* %i, align 4
  %add1020 = add i32 %879, 7
  %add1021 = add i32 %add1020, 14
  %arrayidx1022 = getelementptr i64, i64* %878, i32 %add1021
  %880 = load i64, i64* %arrayidx1022, align 8
  %call1023 = call i64 @rotr64(i64 %880, i32 19)
  %881 = load i64*, i64** %W.addr, align 4
  %882 = load i32, i32* %i, align 4
  %add1024 = add i32 %882, 7
  %add1025 = add i32 %add1024, 14
  %arrayidx1026 = getelementptr i64, i64* %881, i32 %add1025
  %883 = load i64, i64* %arrayidx1026, align 8
  %call1027 = call i64 @rotr64(i64 %883, i32 61)
  %xor1028 = xor i64 %call1023, %call1027
  %884 = load i64*, i64** %W.addr, align 4
  %885 = load i32, i32* %i, align 4
  %add1029 = add i32 %885, 7
  %add1030 = add i32 %add1029, 14
  %arrayidx1031 = getelementptr i64, i64* %884, i32 %add1030
  %886 = load i64, i64* %arrayidx1031, align 8
  %shr1032 = lshr i64 %886, 6
  %xor1033 = xor i64 %xor1028, %shr1032
  %887 = load i64*, i64** %W.addr, align 4
  %888 = load i32, i32* %i, align 4
  %add1034 = add i32 %888, 7
  %add1035 = add i32 %add1034, 9
  %arrayidx1036 = getelementptr i64, i64* %887, i32 %add1035
  %889 = load i64, i64* %arrayidx1036, align 8
  %add1037 = add i64 %xor1033, %889
  %890 = load i64*, i64** %W.addr, align 4
  %891 = load i32, i32* %i, align 4
  %add1038 = add i32 %891, 7
  %add1039 = add i32 %add1038, 1
  %arrayidx1040 = getelementptr i64, i64* %890, i32 %add1039
  %892 = load i64, i64* %arrayidx1040, align 8
  %call1041 = call i64 @rotr64(i64 %892, i32 1)
  %893 = load i64*, i64** %W.addr, align 4
  %894 = load i32, i32* %i, align 4
  %add1042 = add i32 %894, 7
  %add1043 = add i32 %add1042, 1
  %arrayidx1044 = getelementptr i64, i64* %893, i32 %add1043
  %895 = load i64, i64* %arrayidx1044, align 8
  %call1045 = call i64 @rotr64(i64 %895, i32 8)
  %xor1046 = xor i64 %call1041, %call1045
  %896 = load i64*, i64** %W.addr, align 4
  %897 = load i32, i32* %i, align 4
  %add1047 = add i32 %897, 7
  %add1048 = add i32 %add1047, 1
  %arrayidx1049 = getelementptr i64, i64* %896, i32 %add1048
  %898 = load i64, i64* %arrayidx1049, align 8
  %shr1050 = lshr i64 %898, 7
  %xor1051 = xor i64 %xor1046, %shr1050
  %add1052 = add i64 %add1037, %xor1051
  %899 = load i64*, i64** %W.addr, align 4
  %900 = load i32, i32* %i, align 4
  %add1053 = add i32 %900, 7
  %arrayidx1054 = getelementptr i64, i64* %899, i32 %add1053
  %901 = load i64, i64* %arrayidx1054, align 8
  %add1055 = add i64 %add1052, %901
  %902 = load i64*, i64** %W.addr, align 4
  %903 = load i32, i32* %i, align 4
  %add1056 = add i32 %903, 7
  %add1057 = add i32 %add1056, 16
  %arrayidx1058 = getelementptr i64, i64* %902, i32 %add1057
  store i64 %add1055, i64* %arrayidx1058, align 8
  %904 = load i64*, i64** %W.addr, align 4
  %905 = load i32, i32* %i, align 4
  %add1059 = add i32 %905, 8
  %add1060 = add i32 %add1059, 14
  %arrayidx1061 = getelementptr i64, i64* %904, i32 %add1060
  %906 = load i64, i64* %arrayidx1061, align 8
  %call1062 = call i64 @rotr64(i64 %906, i32 19)
  %907 = load i64*, i64** %W.addr, align 4
  %908 = load i32, i32* %i, align 4
  %add1063 = add i32 %908, 8
  %add1064 = add i32 %add1063, 14
  %arrayidx1065 = getelementptr i64, i64* %907, i32 %add1064
  %909 = load i64, i64* %arrayidx1065, align 8
  %call1066 = call i64 @rotr64(i64 %909, i32 61)
  %xor1067 = xor i64 %call1062, %call1066
  %910 = load i64*, i64** %W.addr, align 4
  %911 = load i32, i32* %i, align 4
  %add1068 = add i32 %911, 8
  %add1069 = add i32 %add1068, 14
  %arrayidx1070 = getelementptr i64, i64* %910, i32 %add1069
  %912 = load i64, i64* %arrayidx1070, align 8
  %shr1071 = lshr i64 %912, 6
  %xor1072 = xor i64 %xor1067, %shr1071
  %913 = load i64*, i64** %W.addr, align 4
  %914 = load i32, i32* %i, align 4
  %add1073 = add i32 %914, 8
  %add1074 = add i32 %add1073, 9
  %arrayidx1075 = getelementptr i64, i64* %913, i32 %add1074
  %915 = load i64, i64* %arrayidx1075, align 8
  %add1076 = add i64 %xor1072, %915
  %916 = load i64*, i64** %W.addr, align 4
  %917 = load i32, i32* %i, align 4
  %add1077 = add i32 %917, 8
  %add1078 = add i32 %add1077, 1
  %arrayidx1079 = getelementptr i64, i64* %916, i32 %add1078
  %918 = load i64, i64* %arrayidx1079, align 8
  %call1080 = call i64 @rotr64(i64 %918, i32 1)
  %919 = load i64*, i64** %W.addr, align 4
  %920 = load i32, i32* %i, align 4
  %add1081 = add i32 %920, 8
  %add1082 = add i32 %add1081, 1
  %arrayidx1083 = getelementptr i64, i64* %919, i32 %add1082
  %921 = load i64, i64* %arrayidx1083, align 8
  %call1084 = call i64 @rotr64(i64 %921, i32 8)
  %xor1085 = xor i64 %call1080, %call1084
  %922 = load i64*, i64** %W.addr, align 4
  %923 = load i32, i32* %i, align 4
  %add1086 = add i32 %923, 8
  %add1087 = add i32 %add1086, 1
  %arrayidx1088 = getelementptr i64, i64* %922, i32 %add1087
  %924 = load i64, i64* %arrayidx1088, align 8
  %shr1089 = lshr i64 %924, 7
  %xor1090 = xor i64 %xor1085, %shr1089
  %add1091 = add i64 %add1076, %xor1090
  %925 = load i64*, i64** %W.addr, align 4
  %926 = load i32, i32* %i, align 4
  %add1092 = add i32 %926, 8
  %arrayidx1093 = getelementptr i64, i64* %925, i32 %add1092
  %927 = load i64, i64* %arrayidx1093, align 8
  %add1094 = add i64 %add1091, %927
  %928 = load i64*, i64** %W.addr, align 4
  %929 = load i32, i32* %i, align 4
  %add1095 = add i32 %929, 8
  %add1096 = add i32 %add1095, 16
  %arrayidx1097 = getelementptr i64, i64* %928, i32 %add1096
  store i64 %add1094, i64* %arrayidx1097, align 8
  %930 = load i64*, i64** %W.addr, align 4
  %931 = load i32, i32* %i, align 4
  %add1098 = add i32 %931, 9
  %add1099 = add i32 %add1098, 14
  %arrayidx1100 = getelementptr i64, i64* %930, i32 %add1099
  %932 = load i64, i64* %arrayidx1100, align 8
  %call1101 = call i64 @rotr64(i64 %932, i32 19)
  %933 = load i64*, i64** %W.addr, align 4
  %934 = load i32, i32* %i, align 4
  %add1102 = add i32 %934, 9
  %add1103 = add i32 %add1102, 14
  %arrayidx1104 = getelementptr i64, i64* %933, i32 %add1103
  %935 = load i64, i64* %arrayidx1104, align 8
  %call1105 = call i64 @rotr64(i64 %935, i32 61)
  %xor1106 = xor i64 %call1101, %call1105
  %936 = load i64*, i64** %W.addr, align 4
  %937 = load i32, i32* %i, align 4
  %add1107 = add i32 %937, 9
  %add1108 = add i32 %add1107, 14
  %arrayidx1109 = getelementptr i64, i64* %936, i32 %add1108
  %938 = load i64, i64* %arrayidx1109, align 8
  %shr1110 = lshr i64 %938, 6
  %xor1111 = xor i64 %xor1106, %shr1110
  %939 = load i64*, i64** %W.addr, align 4
  %940 = load i32, i32* %i, align 4
  %add1112 = add i32 %940, 9
  %add1113 = add i32 %add1112, 9
  %arrayidx1114 = getelementptr i64, i64* %939, i32 %add1113
  %941 = load i64, i64* %arrayidx1114, align 8
  %add1115 = add i64 %xor1111, %941
  %942 = load i64*, i64** %W.addr, align 4
  %943 = load i32, i32* %i, align 4
  %add1116 = add i32 %943, 9
  %add1117 = add i32 %add1116, 1
  %arrayidx1118 = getelementptr i64, i64* %942, i32 %add1117
  %944 = load i64, i64* %arrayidx1118, align 8
  %call1119 = call i64 @rotr64(i64 %944, i32 1)
  %945 = load i64*, i64** %W.addr, align 4
  %946 = load i32, i32* %i, align 4
  %add1120 = add i32 %946, 9
  %add1121 = add i32 %add1120, 1
  %arrayidx1122 = getelementptr i64, i64* %945, i32 %add1121
  %947 = load i64, i64* %arrayidx1122, align 8
  %call1123 = call i64 @rotr64(i64 %947, i32 8)
  %xor1124 = xor i64 %call1119, %call1123
  %948 = load i64*, i64** %W.addr, align 4
  %949 = load i32, i32* %i, align 4
  %add1125 = add i32 %949, 9
  %add1126 = add i32 %add1125, 1
  %arrayidx1127 = getelementptr i64, i64* %948, i32 %add1126
  %950 = load i64, i64* %arrayidx1127, align 8
  %shr1128 = lshr i64 %950, 7
  %xor1129 = xor i64 %xor1124, %shr1128
  %add1130 = add i64 %add1115, %xor1129
  %951 = load i64*, i64** %W.addr, align 4
  %952 = load i32, i32* %i, align 4
  %add1131 = add i32 %952, 9
  %arrayidx1132 = getelementptr i64, i64* %951, i32 %add1131
  %953 = load i64, i64* %arrayidx1132, align 8
  %add1133 = add i64 %add1130, %953
  %954 = load i64*, i64** %W.addr, align 4
  %955 = load i32, i32* %i, align 4
  %add1134 = add i32 %955, 9
  %add1135 = add i32 %add1134, 16
  %arrayidx1136 = getelementptr i64, i64* %954, i32 %add1135
  store i64 %add1133, i64* %arrayidx1136, align 8
  %956 = load i64*, i64** %W.addr, align 4
  %957 = load i32, i32* %i, align 4
  %add1137 = add i32 %957, 10
  %add1138 = add i32 %add1137, 14
  %arrayidx1139 = getelementptr i64, i64* %956, i32 %add1138
  %958 = load i64, i64* %arrayidx1139, align 8
  %call1140 = call i64 @rotr64(i64 %958, i32 19)
  %959 = load i64*, i64** %W.addr, align 4
  %960 = load i32, i32* %i, align 4
  %add1141 = add i32 %960, 10
  %add1142 = add i32 %add1141, 14
  %arrayidx1143 = getelementptr i64, i64* %959, i32 %add1142
  %961 = load i64, i64* %arrayidx1143, align 8
  %call1144 = call i64 @rotr64(i64 %961, i32 61)
  %xor1145 = xor i64 %call1140, %call1144
  %962 = load i64*, i64** %W.addr, align 4
  %963 = load i32, i32* %i, align 4
  %add1146 = add i32 %963, 10
  %add1147 = add i32 %add1146, 14
  %arrayidx1148 = getelementptr i64, i64* %962, i32 %add1147
  %964 = load i64, i64* %arrayidx1148, align 8
  %shr1149 = lshr i64 %964, 6
  %xor1150 = xor i64 %xor1145, %shr1149
  %965 = load i64*, i64** %W.addr, align 4
  %966 = load i32, i32* %i, align 4
  %add1151 = add i32 %966, 10
  %add1152 = add i32 %add1151, 9
  %arrayidx1153 = getelementptr i64, i64* %965, i32 %add1152
  %967 = load i64, i64* %arrayidx1153, align 8
  %add1154 = add i64 %xor1150, %967
  %968 = load i64*, i64** %W.addr, align 4
  %969 = load i32, i32* %i, align 4
  %add1155 = add i32 %969, 10
  %add1156 = add i32 %add1155, 1
  %arrayidx1157 = getelementptr i64, i64* %968, i32 %add1156
  %970 = load i64, i64* %arrayidx1157, align 8
  %call1158 = call i64 @rotr64(i64 %970, i32 1)
  %971 = load i64*, i64** %W.addr, align 4
  %972 = load i32, i32* %i, align 4
  %add1159 = add i32 %972, 10
  %add1160 = add i32 %add1159, 1
  %arrayidx1161 = getelementptr i64, i64* %971, i32 %add1160
  %973 = load i64, i64* %arrayidx1161, align 8
  %call1162 = call i64 @rotr64(i64 %973, i32 8)
  %xor1163 = xor i64 %call1158, %call1162
  %974 = load i64*, i64** %W.addr, align 4
  %975 = load i32, i32* %i, align 4
  %add1164 = add i32 %975, 10
  %add1165 = add i32 %add1164, 1
  %arrayidx1166 = getelementptr i64, i64* %974, i32 %add1165
  %976 = load i64, i64* %arrayidx1166, align 8
  %shr1167 = lshr i64 %976, 7
  %xor1168 = xor i64 %xor1163, %shr1167
  %add1169 = add i64 %add1154, %xor1168
  %977 = load i64*, i64** %W.addr, align 4
  %978 = load i32, i32* %i, align 4
  %add1170 = add i32 %978, 10
  %arrayidx1171 = getelementptr i64, i64* %977, i32 %add1170
  %979 = load i64, i64* %arrayidx1171, align 8
  %add1172 = add i64 %add1169, %979
  %980 = load i64*, i64** %W.addr, align 4
  %981 = load i32, i32* %i, align 4
  %add1173 = add i32 %981, 10
  %add1174 = add i32 %add1173, 16
  %arrayidx1175 = getelementptr i64, i64* %980, i32 %add1174
  store i64 %add1172, i64* %arrayidx1175, align 8
  %982 = load i64*, i64** %W.addr, align 4
  %983 = load i32, i32* %i, align 4
  %add1176 = add i32 %983, 11
  %add1177 = add i32 %add1176, 14
  %arrayidx1178 = getelementptr i64, i64* %982, i32 %add1177
  %984 = load i64, i64* %arrayidx1178, align 8
  %call1179 = call i64 @rotr64(i64 %984, i32 19)
  %985 = load i64*, i64** %W.addr, align 4
  %986 = load i32, i32* %i, align 4
  %add1180 = add i32 %986, 11
  %add1181 = add i32 %add1180, 14
  %arrayidx1182 = getelementptr i64, i64* %985, i32 %add1181
  %987 = load i64, i64* %arrayidx1182, align 8
  %call1183 = call i64 @rotr64(i64 %987, i32 61)
  %xor1184 = xor i64 %call1179, %call1183
  %988 = load i64*, i64** %W.addr, align 4
  %989 = load i32, i32* %i, align 4
  %add1185 = add i32 %989, 11
  %add1186 = add i32 %add1185, 14
  %arrayidx1187 = getelementptr i64, i64* %988, i32 %add1186
  %990 = load i64, i64* %arrayidx1187, align 8
  %shr1188 = lshr i64 %990, 6
  %xor1189 = xor i64 %xor1184, %shr1188
  %991 = load i64*, i64** %W.addr, align 4
  %992 = load i32, i32* %i, align 4
  %add1190 = add i32 %992, 11
  %add1191 = add i32 %add1190, 9
  %arrayidx1192 = getelementptr i64, i64* %991, i32 %add1191
  %993 = load i64, i64* %arrayidx1192, align 8
  %add1193 = add i64 %xor1189, %993
  %994 = load i64*, i64** %W.addr, align 4
  %995 = load i32, i32* %i, align 4
  %add1194 = add i32 %995, 11
  %add1195 = add i32 %add1194, 1
  %arrayidx1196 = getelementptr i64, i64* %994, i32 %add1195
  %996 = load i64, i64* %arrayidx1196, align 8
  %call1197 = call i64 @rotr64(i64 %996, i32 1)
  %997 = load i64*, i64** %W.addr, align 4
  %998 = load i32, i32* %i, align 4
  %add1198 = add i32 %998, 11
  %add1199 = add i32 %add1198, 1
  %arrayidx1200 = getelementptr i64, i64* %997, i32 %add1199
  %999 = load i64, i64* %arrayidx1200, align 8
  %call1201 = call i64 @rotr64(i64 %999, i32 8)
  %xor1202 = xor i64 %call1197, %call1201
  %1000 = load i64*, i64** %W.addr, align 4
  %1001 = load i32, i32* %i, align 4
  %add1203 = add i32 %1001, 11
  %add1204 = add i32 %add1203, 1
  %arrayidx1205 = getelementptr i64, i64* %1000, i32 %add1204
  %1002 = load i64, i64* %arrayidx1205, align 8
  %shr1206 = lshr i64 %1002, 7
  %xor1207 = xor i64 %xor1202, %shr1206
  %add1208 = add i64 %add1193, %xor1207
  %1003 = load i64*, i64** %W.addr, align 4
  %1004 = load i32, i32* %i, align 4
  %add1209 = add i32 %1004, 11
  %arrayidx1210 = getelementptr i64, i64* %1003, i32 %add1209
  %1005 = load i64, i64* %arrayidx1210, align 8
  %add1211 = add i64 %add1208, %1005
  %1006 = load i64*, i64** %W.addr, align 4
  %1007 = load i32, i32* %i, align 4
  %add1212 = add i32 %1007, 11
  %add1213 = add i32 %add1212, 16
  %arrayidx1214 = getelementptr i64, i64* %1006, i32 %add1213
  store i64 %add1211, i64* %arrayidx1214, align 8
  %1008 = load i64*, i64** %W.addr, align 4
  %1009 = load i32, i32* %i, align 4
  %add1215 = add i32 %1009, 12
  %add1216 = add i32 %add1215, 14
  %arrayidx1217 = getelementptr i64, i64* %1008, i32 %add1216
  %1010 = load i64, i64* %arrayidx1217, align 8
  %call1218 = call i64 @rotr64(i64 %1010, i32 19)
  %1011 = load i64*, i64** %W.addr, align 4
  %1012 = load i32, i32* %i, align 4
  %add1219 = add i32 %1012, 12
  %add1220 = add i32 %add1219, 14
  %arrayidx1221 = getelementptr i64, i64* %1011, i32 %add1220
  %1013 = load i64, i64* %arrayidx1221, align 8
  %call1222 = call i64 @rotr64(i64 %1013, i32 61)
  %xor1223 = xor i64 %call1218, %call1222
  %1014 = load i64*, i64** %W.addr, align 4
  %1015 = load i32, i32* %i, align 4
  %add1224 = add i32 %1015, 12
  %add1225 = add i32 %add1224, 14
  %arrayidx1226 = getelementptr i64, i64* %1014, i32 %add1225
  %1016 = load i64, i64* %arrayidx1226, align 8
  %shr1227 = lshr i64 %1016, 6
  %xor1228 = xor i64 %xor1223, %shr1227
  %1017 = load i64*, i64** %W.addr, align 4
  %1018 = load i32, i32* %i, align 4
  %add1229 = add i32 %1018, 12
  %add1230 = add i32 %add1229, 9
  %arrayidx1231 = getelementptr i64, i64* %1017, i32 %add1230
  %1019 = load i64, i64* %arrayidx1231, align 8
  %add1232 = add i64 %xor1228, %1019
  %1020 = load i64*, i64** %W.addr, align 4
  %1021 = load i32, i32* %i, align 4
  %add1233 = add i32 %1021, 12
  %add1234 = add i32 %add1233, 1
  %arrayidx1235 = getelementptr i64, i64* %1020, i32 %add1234
  %1022 = load i64, i64* %arrayidx1235, align 8
  %call1236 = call i64 @rotr64(i64 %1022, i32 1)
  %1023 = load i64*, i64** %W.addr, align 4
  %1024 = load i32, i32* %i, align 4
  %add1237 = add i32 %1024, 12
  %add1238 = add i32 %add1237, 1
  %arrayidx1239 = getelementptr i64, i64* %1023, i32 %add1238
  %1025 = load i64, i64* %arrayidx1239, align 8
  %call1240 = call i64 @rotr64(i64 %1025, i32 8)
  %xor1241 = xor i64 %call1236, %call1240
  %1026 = load i64*, i64** %W.addr, align 4
  %1027 = load i32, i32* %i, align 4
  %add1242 = add i32 %1027, 12
  %add1243 = add i32 %add1242, 1
  %arrayidx1244 = getelementptr i64, i64* %1026, i32 %add1243
  %1028 = load i64, i64* %arrayidx1244, align 8
  %shr1245 = lshr i64 %1028, 7
  %xor1246 = xor i64 %xor1241, %shr1245
  %add1247 = add i64 %add1232, %xor1246
  %1029 = load i64*, i64** %W.addr, align 4
  %1030 = load i32, i32* %i, align 4
  %add1248 = add i32 %1030, 12
  %arrayidx1249 = getelementptr i64, i64* %1029, i32 %add1248
  %1031 = load i64, i64* %arrayidx1249, align 8
  %add1250 = add i64 %add1247, %1031
  %1032 = load i64*, i64** %W.addr, align 4
  %1033 = load i32, i32* %i, align 4
  %add1251 = add i32 %1033, 12
  %add1252 = add i32 %add1251, 16
  %arrayidx1253 = getelementptr i64, i64* %1032, i32 %add1252
  store i64 %add1250, i64* %arrayidx1253, align 8
  %1034 = load i64*, i64** %W.addr, align 4
  %1035 = load i32, i32* %i, align 4
  %add1254 = add i32 %1035, 13
  %add1255 = add i32 %add1254, 14
  %arrayidx1256 = getelementptr i64, i64* %1034, i32 %add1255
  %1036 = load i64, i64* %arrayidx1256, align 8
  %call1257 = call i64 @rotr64(i64 %1036, i32 19)
  %1037 = load i64*, i64** %W.addr, align 4
  %1038 = load i32, i32* %i, align 4
  %add1258 = add i32 %1038, 13
  %add1259 = add i32 %add1258, 14
  %arrayidx1260 = getelementptr i64, i64* %1037, i32 %add1259
  %1039 = load i64, i64* %arrayidx1260, align 8
  %call1261 = call i64 @rotr64(i64 %1039, i32 61)
  %xor1262 = xor i64 %call1257, %call1261
  %1040 = load i64*, i64** %W.addr, align 4
  %1041 = load i32, i32* %i, align 4
  %add1263 = add i32 %1041, 13
  %add1264 = add i32 %add1263, 14
  %arrayidx1265 = getelementptr i64, i64* %1040, i32 %add1264
  %1042 = load i64, i64* %arrayidx1265, align 8
  %shr1266 = lshr i64 %1042, 6
  %xor1267 = xor i64 %xor1262, %shr1266
  %1043 = load i64*, i64** %W.addr, align 4
  %1044 = load i32, i32* %i, align 4
  %add1268 = add i32 %1044, 13
  %add1269 = add i32 %add1268, 9
  %arrayidx1270 = getelementptr i64, i64* %1043, i32 %add1269
  %1045 = load i64, i64* %arrayidx1270, align 8
  %add1271 = add i64 %xor1267, %1045
  %1046 = load i64*, i64** %W.addr, align 4
  %1047 = load i32, i32* %i, align 4
  %add1272 = add i32 %1047, 13
  %add1273 = add i32 %add1272, 1
  %arrayidx1274 = getelementptr i64, i64* %1046, i32 %add1273
  %1048 = load i64, i64* %arrayidx1274, align 8
  %call1275 = call i64 @rotr64(i64 %1048, i32 1)
  %1049 = load i64*, i64** %W.addr, align 4
  %1050 = load i32, i32* %i, align 4
  %add1276 = add i32 %1050, 13
  %add1277 = add i32 %add1276, 1
  %arrayidx1278 = getelementptr i64, i64* %1049, i32 %add1277
  %1051 = load i64, i64* %arrayidx1278, align 8
  %call1279 = call i64 @rotr64(i64 %1051, i32 8)
  %xor1280 = xor i64 %call1275, %call1279
  %1052 = load i64*, i64** %W.addr, align 4
  %1053 = load i32, i32* %i, align 4
  %add1281 = add i32 %1053, 13
  %add1282 = add i32 %add1281, 1
  %arrayidx1283 = getelementptr i64, i64* %1052, i32 %add1282
  %1054 = load i64, i64* %arrayidx1283, align 8
  %shr1284 = lshr i64 %1054, 7
  %xor1285 = xor i64 %xor1280, %shr1284
  %add1286 = add i64 %add1271, %xor1285
  %1055 = load i64*, i64** %W.addr, align 4
  %1056 = load i32, i32* %i, align 4
  %add1287 = add i32 %1056, 13
  %arrayidx1288 = getelementptr i64, i64* %1055, i32 %add1287
  %1057 = load i64, i64* %arrayidx1288, align 8
  %add1289 = add i64 %add1286, %1057
  %1058 = load i64*, i64** %W.addr, align 4
  %1059 = load i32, i32* %i, align 4
  %add1290 = add i32 %1059, 13
  %add1291 = add i32 %add1290, 16
  %arrayidx1292 = getelementptr i64, i64* %1058, i32 %add1291
  store i64 %add1289, i64* %arrayidx1292, align 8
  %1060 = load i64*, i64** %W.addr, align 4
  %1061 = load i32, i32* %i, align 4
  %add1293 = add i32 %1061, 14
  %add1294 = add i32 %add1293, 14
  %arrayidx1295 = getelementptr i64, i64* %1060, i32 %add1294
  %1062 = load i64, i64* %arrayidx1295, align 8
  %call1296 = call i64 @rotr64(i64 %1062, i32 19)
  %1063 = load i64*, i64** %W.addr, align 4
  %1064 = load i32, i32* %i, align 4
  %add1297 = add i32 %1064, 14
  %add1298 = add i32 %add1297, 14
  %arrayidx1299 = getelementptr i64, i64* %1063, i32 %add1298
  %1065 = load i64, i64* %arrayidx1299, align 8
  %call1300 = call i64 @rotr64(i64 %1065, i32 61)
  %xor1301 = xor i64 %call1296, %call1300
  %1066 = load i64*, i64** %W.addr, align 4
  %1067 = load i32, i32* %i, align 4
  %add1302 = add i32 %1067, 14
  %add1303 = add i32 %add1302, 14
  %arrayidx1304 = getelementptr i64, i64* %1066, i32 %add1303
  %1068 = load i64, i64* %arrayidx1304, align 8
  %shr1305 = lshr i64 %1068, 6
  %xor1306 = xor i64 %xor1301, %shr1305
  %1069 = load i64*, i64** %W.addr, align 4
  %1070 = load i32, i32* %i, align 4
  %add1307 = add i32 %1070, 14
  %add1308 = add i32 %add1307, 9
  %arrayidx1309 = getelementptr i64, i64* %1069, i32 %add1308
  %1071 = load i64, i64* %arrayidx1309, align 8
  %add1310 = add i64 %xor1306, %1071
  %1072 = load i64*, i64** %W.addr, align 4
  %1073 = load i32, i32* %i, align 4
  %add1311 = add i32 %1073, 14
  %add1312 = add i32 %add1311, 1
  %arrayidx1313 = getelementptr i64, i64* %1072, i32 %add1312
  %1074 = load i64, i64* %arrayidx1313, align 8
  %call1314 = call i64 @rotr64(i64 %1074, i32 1)
  %1075 = load i64*, i64** %W.addr, align 4
  %1076 = load i32, i32* %i, align 4
  %add1315 = add i32 %1076, 14
  %add1316 = add i32 %add1315, 1
  %arrayidx1317 = getelementptr i64, i64* %1075, i32 %add1316
  %1077 = load i64, i64* %arrayidx1317, align 8
  %call1318 = call i64 @rotr64(i64 %1077, i32 8)
  %xor1319 = xor i64 %call1314, %call1318
  %1078 = load i64*, i64** %W.addr, align 4
  %1079 = load i32, i32* %i, align 4
  %add1320 = add i32 %1079, 14
  %add1321 = add i32 %add1320, 1
  %arrayidx1322 = getelementptr i64, i64* %1078, i32 %add1321
  %1080 = load i64, i64* %arrayidx1322, align 8
  %shr1323 = lshr i64 %1080, 7
  %xor1324 = xor i64 %xor1319, %shr1323
  %add1325 = add i64 %add1310, %xor1324
  %1081 = load i64*, i64** %W.addr, align 4
  %1082 = load i32, i32* %i, align 4
  %add1326 = add i32 %1082, 14
  %arrayidx1327 = getelementptr i64, i64* %1081, i32 %add1326
  %1083 = load i64, i64* %arrayidx1327, align 8
  %add1328 = add i64 %add1325, %1083
  %1084 = load i64*, i64** %W.addr, align 4
  %1085 = load i32, i32* %i, align 4
  %add1329 = add i32 %1085, 14
  %add1330 = add i32 %add1329, 16
  %arrayidx1331 = getelementptr i64, i64* %1084, i32 %add1330
  store i64 %add1328, i64* %arrayidx1331, align 8
  %1086 = load i64*, i64** %W.addr, align 4
  %1087 = load i32, i32* %i, align 4
  %add1332 = add i32 %1087, 15
  %add1333 = add i32 %add1332, 14
  %arrayidx1334 = getelementptr i64, i64* %1086, i32 %add1333
  %1088 = load i64, i64* %arrayidx1334, align 8
  %call1335 = call i64 @rotr64(i64 %1088, i32 19)
  %1089 = load i64*, i64** %W.addr, align 4
  %1090 = load i32, i32* %i, align 4
  %add1336 = add i32 %1090, 15
  %add1337 = add i32 %add1336, 14
  %arrayidx1338 = getelementptr i64, i64* %1089, i32 %add1337
  %1091 = load i64, i64* %arrayidx1338, align 8
  %call1339 = call i64 @rotr64(i64 %1091, i32 61)
  %xor1340 = xor i64 %call1335, %call1339
  %1092 = load i64*, i64** %W.addr, align 4
  %1093 = load i32, i32* %i, align 4
  %add1341 = add i32 %1093, 15
  %add1342 = add i32 %add1341, 14
  %arrayidx1343 = getelementptr i64, i64* %1092, i32 %add1342
  %1094 = load i64, i64* %arrayidx1343, align 8
  %shr1344 = lshr i64 %1094, 6
  %xor1345 = xor i64 %xor1340, %shr1344
  %1095 = load i64*, i64** %W.addr, align 4
  %1096 = load i32, i32* %i, align 4
  %add1346 = add i32 %1096, 15
  %add1347 = add i32 %add1346, 9
  %arrayidx1348 = getelementptr i64, i64* %1095, i32 %add1347
  %1097 = load i64, i64* %arrayidx1348, align 8
  %add1349 = add i64 %xor1345, %1097
  %1098 = load i64*, i64** %W.addr, align 4
  %1099 = load i32, i32* %i, align 4
  %add1350 = add i32 %1099, 15
  %add1351 = add i32 %add1350, 1
  %arrayidx1352 = getelementptr i64, i64* %1098, i32 %add1351
  %1100 = load i64, i64* %arrayidx1352, align 8
  %call1353 = call i64 @rotr64(i64 %1100, i32 1)
  %1101 = load i64*, i64** %W.addr, align 4
  %1102 = load i32, i32* %i, align 4
  %add1354 = add i32 %1102, 15
  %add1355 = add i32 %add1354, 1
  %arrayidx1356 = getelementptr i64, i64* %1101, i32 %add1355
  %1103 = load i64, i64* %arrayidx1356, align 8
  %call1357 = call i64 @rotr64(i64 %1103, i32 8)
  %xor1358 = xor i64 %call1353, %call1357
  %1104 = load i64*, i64** %W.addr, align 4
  %1105 = load i32, i32* %i, align 4
  %add1359 = add i32 %1105, 15
  %add1360 = add i32 %add1359, 1
  %arrayidx1361 = getelementptr i64, i64* %1104, i32 %add1360
  %1106 = load i64, i64* %arrayidx1361, align 8
  %shr1362 = lshr i64 %1106, 7
  %xor1363 = xor i64 %xor1358, %shr1362
  %add1364 = add i64 %add1349, %xor1363
  %1107 = load i64*, i64** %W.addr, align 4
  %1108 = load i32, i32* %i, align 4
  %add1365 = add i32 %1108, 15
  %arrayidx1366 = getelementptr i64, i64* %1107, i32 %add1365
  %1109 = load i64, i64* %arrayidx1366, align 8
  %add1367 = add i64 %add1364, %1109
  %1110 = load i64*, i64** %W.addr, align 4
  %1111 = load i32, i32* %i, align 4
  %add1368 = add i32 %1111, 15
  %add1369 = add i32 %add1368, 16
  %arrayidx1370 = getelementptr i64, i64* %1110, i32 %add1369
  store i64 %add1367, i64* %arrayidx1370, align 8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %1112 = load i32, i32* %i, align 4
  %add1371 = add i32 %1112, 16
  store i32 %add1371, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1372

for.cond1372:                                     ; preds = %for.inc1378, %for.end
  %1113 = load i32, i32* %i, align 4
  %cmp1373 = icmp slt i32 %1113, 8
  br i1 %cmp1373, label %for.body1374, label %for.end1379

for.body1374:                                     ; preds = %for.cond1372
  %1114 = load i64*, i64** %S.addr, align 4
  %1115 = load i32, i32* %i, align 4
  %arrayidx1375 = getelementptr i64, i64* %1114, i32 %1115
  %1116 = load i64, i64* %arrayidx1375, align 8
  %1117 = load i64*, i64** %state.addr, align 4
  %1118 = load i32, i32* %i, align 4
  %arrayidx1376 = getelementptr i64, i64* %1117, i32 %1118
  %1119 = load i64, i64* %arrayidx1376, align 8
  %add1377 = add i64 %1119, %1116
  store i64 %add1377, i64* %arrayidx1376, align 8
  br label %for.inc1378

for.inc1378:                                      ; preds = %for.body1374
  %1120 = load i32, i32* %i, align 4
  %inc = add i32 %1120, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond1372

for.end1379:                                      ; preds = %for.cond1372
  ret void
}

declare void @sodium_memzero(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* nonnull %state, i8* nonnull %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha512_state*, align 4
  %out.addr = alloca i8*, align 4
  %tmp64 = alloca [88 x i64], align 16
  store %struct.crypto_hash_sha512_state* %state, %struct.crypto_hash_sha512_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %arraydecay = getelementptr inbounds [88 x i64], [88 x i64]* %tmp64, i32 0, i32 0
  call void @SHA512_Pad(%struct.crypto_hash_sha512_state* %0, i64* %arraydecay)
  %1 = load i8*, i8** %out.addr, align 4
  %2 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state1 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [8 x i64], [8 x i64]* %state1, i32 0, i32 0
  call void @be64enc_vect(i8* %1, i64* %arraydecay2, i32 64)
  %arraydecay3 = getelementptr inbounds [88 x i64], [88 x i64]* %tmp64, i32 0, i32 0
  %3 = bitcast i64* %arraydecay3 to i8*
  call void @sodium_memzero(i8* %3, i32 704)
  %4 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %5 = bitcast %struct.crypto_hash_sha512_state* %4 to i8*
  call void @sodium_memzero(i8* %5, i32 208)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @SHA512_Pad(%struct.crypto_hash_sha512_state* %state, i64* %tmp64) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha512_state*, align 4
  %tmp64.addr = alloca i64*, align 4
  %r = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.crypto_hash_sha512_state* %state, %struct.crypto_hash_sha512_state** %state.addr, align 4
  store i64* %tmp64, i64** %tmp64.addr, align 4
  %0 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %0, i32 0, i32 1
  %arrayidx = getelementptr [2 x i64], [2 x i64]* %count, i32 0, i32 1
  %1 = load i64, i64* %arrayidx, align 8
  %shr = lshr i64 %1, 3
  %and = and i64 %shr, 127
  %conv = trunc i64 %and to i32
  store i32 %conv, i32* %r, align 4
  %2 = load i32, i32* %r, align 4
  %cmp = icmp ult i32 %2, 112
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %r, align 4
  %sub = sub i32 112, %4
  %cmp2 = icmp ult i32 %3, %sub
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr [128 x i8], [128 x i8]* bitcast (<{ i8, [127 x i8] }>* @PAD to [128 x i8]*), i32 0, i32 %5
  %6 = load i8, i8* %arrayidx4, align 1
  %7 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %7, i32 0, i32 2
  %8 = load i32, i32* %r, align 4
  %9 = load i32, i32* %i, align 4
  %add = add i32 %8, %9
  %arrayidx5 = getelementptr [128 x i8], [128 x i8]* %buf, i32 0, i32 %add
  store i8 %6, i8* %arrayidx5, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc15, %if.else
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %r, align 4
  %sub7 = sub i32 128, %12
  %cmp8 = icmp ult i32 %11, %sub7
  br i1 %cmp8, label %for.body10, label %for.end17

for.body10:                                       ; preds = %for.cond6
  %13 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr [128 x i8], [128 x i8]* bitcast (<{ i8, [127 x i8] }>* @PAD to [128 x i8]*), i32 0, i32 %13
  %14 = load i8, i8* %arrayidx11, align 1
  %15 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf12 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %15, i32 0, i32 2
  %16 = load i32, i32* %r, align 4
  %17 = load i32, i32* %i, align 4
  %add13 = add i32 %16, %17
  %arrayidx14 = getelementptr [128 x i8], [128 x i8]* %buf12, i32 0, i32 %add13
  store i8 %14, i8* %arrayidx14, align 1
  br label %for.inc15

for.inc15:                                        ; preds = %for.body10
  %18 = load i32, i32* %i, align 4
  %inc16 = add i32 %18, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond6

for.end17:                                        ; preds = %for.cond6
  %19 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state18 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %19, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i64], [8 x i64]* %state18, i32 0, i32 0
  %20 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf19 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %20, i32 0, i32 2
  %arraydecay20 = getelementptr inbounds [128 x i8], [128 x i8]* %buf19, i32 0, i32 0
  %21 = load i64*, i64** %tmp64.addr, align 4
  %arrayidx21 = getelementptr i64, i64* %21, i32 0
  %22 = load i64*, i64** %tmp64.addr, align 4
  %arrayidx22 = getelementptr i64, i64* %22, i32 80
  call void @SHA512_Transform(i64* %arraydecay, i8* %arraydecay20, i64* %arrayidx21, i64* %arrayidx22)
  %23 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf23 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %23, i32 0, i32 2
  %arrayidx24 = getelementptr [128 x i8], [128 x i8]* %buf23, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arrayidx24, i8 0, i32 112, i1 false)
  br label %if.end

if.end:                                           ; preds = %for.end17, %for.end
  %24 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf25 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %24, i32 0, i32 2
  %arrayidx26 = getelementptr [128 x i8], [128 x i8]* %buf25, i32 0, i32 112
  %25 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %count27 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %25, i32 0, i32 1
  %arraydecay28 = getelementptr inbounds [2 x i64], [2 x i64]* %count27, i32 0, i32 0
  call void @be64enc_vect(i8* %arrayidx26, i64* %arraydecay28, i32 16)
  %26 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %state29 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %26, i32 0, i32 0
  %arraydecay30 = getelementptr inbounds [8 x i64], [8 x i64]* %state29, i32 0, i32 0
  %27 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %state.addr, align 4
  %buf31 = getelementptr inbounds %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state* %27, i32 0, i32 2
  %arraydecay32 = getelementptr inbounds [128 x i8], [128 x i8]* %buf31, i32 0, i32 0
  %28 = load i64*, i64** %tmp64.addr, align 4
  %arrayidx33 = getelementptr i64, i64* %28, i32 0
  %29 = load i64*, i64** %tmp64.addr, align 4
  %arrayidx34 = getelementptr i64, i64* %29, i32 80
  call void @SHA512_Transform(i64* %arraydecay30, i8* %arraydecay32, i64* %arrayidx33, i64* %arrayidx34)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @be64enc_vect(i8* %dst, i64* %src, i32 %len) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %src.addr = alloca i64*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i64* %src, i64** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %div = udiv i32 %1, 8
  %cmp = icmp ult i32 %0, %div
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %dst.addr, align 4
  %3 = load i32, i32* %i, align 4
  %mul = mul i32 %3, 8
  %add.ptr = getelementptr i8, i8* %2, i32 %mul
  %4 = load i64*, i64** %src.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i64, i64* %4, i32 %5
  %6 = load i64, i64* %arrayidx, align 8
  call void @store64_be(i8* %add.ptr, i64 %6)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha512(i8* nonnull %out, i8* %in, i64 %inlen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %state = alloca %struct.crypto_hash_sha512_state, align 8
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %call = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %state)
  %0 = load i8*, i8** %in.addr, align 4
  %1 = load i64, i64* %inlen.addr, align 8
  %call1 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %state, i8* %0, i64 %1)
  %2 = load i8*, i8** %out.addr, align 4
  %call2 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %state, i8* %2)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @be64dec_vect(i64* %dst, i8* %src, i32 %len) #0 {
entry:
  %dst.addr = alloca i64*, align 4
  %src.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i64* %dst, i64** %dst.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %div = udiv i32 %1, 8
  %cmp = icmp ult i32 %0, %div
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %src.addr, align 4
  %3 = load i32, i32* %i, align 4
  %mul = mul i32 %3, 8
  %add.ptr = getelementptr i8, i8* %2, i32 %mul
  %call = call i64 @load64_be(i8* %add.ptr)
  %4 = load i64*, i64** %dst.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i64, i64* %4, i32 %5
  store i64 %call, i64* %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i64 @rotr64(i64 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i64, align 8
  %b.addr = alloca i32, align 4
  store i64 %x, i64* %x.addr, align 8
  store i32 %b, i32* %b.addr, align 4
  %0 = load i64, i64* %x.addr, align 8
  %1 = load i32, i32* %b.addr, align 4
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %x.addr, align 8
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64_be(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8*, i8** %src.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 7
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i64
  store i64 %conv, i64* %w, align 8
  %2 = load i8*, i8** %src.addr, align 4
  %arrayidx1 = getelementptr i8, i8* %2, i32 6
  %3 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %3 to i64
  %shl = shl i64 %conv2, 8
  %4 = load i64, i64* %w, align 8
  %or = or i64 %4, %shl
  store i64 %or, i64* %w, align 8
  %5 = load i8*, i8** %src.addr, align 4
  %arrayidx3 = getelementptr i8, i8* %5, i32 5
  %6 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %6 to i64
  %shl5 = shl i64 %conv4, 16
  %7 = load i64, i64* %w, align 8
  %or6 = or i64 %7, %shl5
  store i64 %or6, i64* %w, align 8
  %8 = load i8*, i8** %src.addr, align 4
  %arrayidx7 = getelementptr i8, i8* %8, i32 4
  %9 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %9 to i64
  %shl9 = shl i64 %conv8, 24
  %10 = load i64, i64* %w, align 8
  %or10 = or i64 %10, %shl9
  store i64 %or10, i64* %w, align 8
  %11 = load i8*, i8** %src.addr, align 4
  %arrayidx11 = getelementptr i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx11, align 1
  %conv12 = zext i8 %12 to i64
  %shl13 = shl i64 %conv12, 32
  %13 = load i64, i64* %w, align 8
  %or14 = or i64 %13, %shl13
  store i64 %or14, i64* %w, align 8
  %14 = load i8*, i8** %src.addr, align 4
  %arrayidx15 = getelementptr i8, i8* %14, i32 2
  %15 = load i8, i8* %arrayidx15, align 1
  %conv16 = zext i8 %15 to i64
  %shl17 = shl i64 %conv16, 40
  %16 = load i64, i64* %w, align 8
  %or18 = or i64 %16, %shl17
  store i64 %or18, i64* %w, align 8
  %17 = load i8*, i8** %src.addr, align 4
  %arrayidx19 = getelementptr i8, i8* %17, i32 1
  %18 = load i8, i8* %arrayidx19, align 1
  %conv20 = zext i8 %18 to i64
  %shl21 = shl i64 %conv20, 48
  %19 = load i64, i64* %w, align 8
  %or22 = or i64 %19, %shl21
  store i64 %or22, i64* %w, align 8
  %20 = load i8*, i8** %src.addr, align 4
  %arrayidx23 = getelementptr i8, i8* %20, i32 0
  %21 = load i8, i8* %arrayidx23, align 1
  %conv24 = zext i8 %21 to i64
  %shl25 = shl i64 %conv24, 56
  %22 = load i64, i64* %w, align 8
  %or26 = or i64 %22, %shl25
  store i64 %or26, i64* %w, align 8
  %23 = load i64, i64* %w, align 8
  ret i64 %23
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal void @store64_be(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i64, i64* %w.addr, align 8
  %conv = trunc i64 %0 to i8
  %1 = load i8*, i8** %dst.addr, align 4
  %arrayidx = getelementptr i8, i8* %1, i32 7
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i64, i64* %w.addr, align 8
  %shr = lshr i64 %2, 8
  store i64 %shr, i64* %w.addr, align 8
  %3 = load i64, i64* %w.addr, align 8
  %conv1 = trunc i64 %3 to i8
  %4 = load i8*, i8** %dst.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %4, i32 6
  store i8 %conv1, i8* %arrayidx2, align 1
  %5 = load i64, i64* %w.addr, align 8
  %shr3 = lshr i64 %5, 8
  store i64 %shr3, i64* %w.addr, align 8
  %6 = load i64, i64* %w.addr, align 8
  %conv4 = trunc i64 %6 to i8
  %7 = load i8*, i8** %dst.addr, align 4
  %arrayidx5 = getelementptr i8, i8* %7, i32 5
  store i8 %conv4, i8* %arrayidx5, align 1
  %8 = load i64, i64* %w.addr, align 8
  %shr6 = lshr i64 %8, 8
  store i64 %shr6, i64* %w.addr, align 8
  %9 = load i64, i64* %w.addr, align 8
  %conv7 = trunc i64 %9 to i8
  %10 = load i8*, i8** %dst.addr, align 4
  %arrayidx8 = getelementptr i8, i8* %10, i32 4
  store i8 %conv7, i8* %arrayidx8, align 1
  %11 = load i64, i64* %w.addr, align 8
  %shr9 = lshr i64 %11, 8
  store i64 %shr9, i64* %w.addr, align 8
  %12 = load i64, i64* %w.addr, align 8
  %conv10 = trunc i64 %12 to i8
  %13 = load i8*, i8** %dst.addr, align 4
  %arrayidx11 = getelementptr i8, i8* %13, i32 3
  store i8 %conv10, i8* %arrayidx11, align 1
  %14 = load i64, i64* %w.addr, align 8
  %shr12 = lshr i64 %14, 8
  store i64 %shr12, i64* %w.addr, align 8
  %15 = load i64, i64* %w.addr, align 8
  %conv13 = trunc i64 %15 to i8
  %16 = load i8*, i8** %dst.addr, align 4
  %arrayidx14 = getelementptr i8, i8* %16, i32 2
  store i8 %conv13, i8* %arrayidx14, align 1
  %17 = load i64, i64* %w.addr, align 8
  %shr15 = lshr i64 %17, 8
  store i64 %shr15, i64* %w.addr, align 8
  %18 = load i64, i64* %w.addr, align 8
  %conv16 = trunc i64 %18 to i8
  %19 = load i8*, i8** %dst.addr, align 4
  %arrayidx17 = getelementptr i8, i8* %19, i32 1
  store i8 %conv16, i8* %arrayidx17, align 1
  %20 = load i64, i64* %w.addr, align 8
  %shr18 = lshr i64 %20, 8
  store i64 %shr18, i64* %w.addr, align 8
  %21 = load i64, i64* %w.addr, align 8
  %conv19 = trunc i64 %21 to i8
  %22 = load i8*, i8** %dst.addr, align 4
  %arrayidx20 = getelementptr i8, i8* %22, i32 0
  store i8 %conv19, i8* %arrayidx20, align 1
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
