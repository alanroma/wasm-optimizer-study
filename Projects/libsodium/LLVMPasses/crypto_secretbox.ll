; ModuleID = 'crypto_secretbox/crypto_secretbox.c'
source_filename = "crypto_secretbox/crypto_secretbox.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [17 x i8] c"xsalsa20poly1305\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_noncebytes() #0 {
entry:
  ret i32 24
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_zerobytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_boxzerobytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_macbytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_messagebytes_max() #0 {
entry:
  ret i32 -17
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_secretbox_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %c.addr, align 4
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_xsalsa20poly1305(i8* %0, i8* %1, i64 %2, i8* %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_secretbox_xsalsa20poly1305(i8*, i8*, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_open(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %m.addr, align 4
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i64, i64* %clen.addr, align 8
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_xsalsa20poly1305_open(i8* %0, i8* %1, i64 %2, i8* %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_secretbox_xsalsa20poly1305_open(i8*, i8*, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_secretbox_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
