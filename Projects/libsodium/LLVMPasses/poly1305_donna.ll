; ModuleID = 'crypto_onetimeauth/poly1305/donna/poly1305_donna.c'
source_filename = "crypto_onetimeauth/poly1305/donna/poly1305_donna.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_onetimeauth_poly1305_implementation = type { i32 (i8*, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*, i64)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)* }
%struct.crypto_onetimeauth_poly1305_state = type { [256 x i8] }
%struct.poly1305_state_internal_t = type { [5 x i32], [5 x i32], [4 x i32], i64, [16 x i8], i8 }

@crypto_onetimeauth_poly1305_donna_implementation = hidden global %struct.crypto_onetimeauth_poly1305_implementation { i32 (i8*, i8*, i64, i8*)* @crypto_onetimeauth_poly1305_donna, i32 (i8*, i8*, i64, i8*)* @crypto_onetimeauth_poly1305_donna_verify, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)* @crypto_onetimeauth_poly1305_donna_init, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*, i64)* @crypto_onetimeauth_poly1305_donna_update, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)* @crypto_onetimeauth_poly1305_donna_final }, align 4

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_onetimeauth_poly1305_donna(i8* %out, i8* %m, i64 %inlen, i8* %key) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %key.addr = alloca i8*, align 4
  %state = alloca %struct.poly1305_state_internal_t, align 64
  store i8* %out, i8** %out.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %key, i8** %key.addr, align 4
  %0 = load i8*, i8** %key.addr, align 4
  call void @poly1305_init(%struct.poly1305_state_internal_t* %state, i8* %0)
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  call void @poly1305_update(%struct.poly1305_state_internal_t* %state, i8* %1, i64 %2)
  %3 = load i8*, i8** %out.addr, align 4
  call void @poly1305_finish(%struct.poly1305_state_internal_t* %state, i8* %3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_onetimeauth_poly1305_donna_verify(i8* %h, i8* %in, i64 %inlen, i8* %k) #0 {
entry:
  %h.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %correct = alloca [16 x i8], align 16
  store i8* %h, i8** %h.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %correct, i32 0, i32 0
  %0 = load i8*, i8** %in.addr, align 4
  %1 = load i64, i64* %inlen.addr, align 8
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_onetimeauth_poly1305_donna(i8* %arraydecay, i8* %0, i64 %1, i8* %2)
  %3 = load i8*, i8** %h.addr, align 4
  %arraydecay1 = getelementptr inbounds [16 x i8], [16 x i8]* %correct, i32 0, i32 0
  %call2 = call i32 @crypto_verify_16(i8* %3, i8* %arraydecay1)
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_onetimeauth_poly1305_donna_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %key) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %key.addr = alloca i8*, align 4
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %1 = bitcast %struct.crypto_onetimeauth_poly1305_state* %0 to i8*
  %2 = bitcast i8* %1 to %struct.poly1305_state_internal_t*
  %3 = load i8*, i8** %key.addr, align 4
  call void @poly1305_init(%struct.poly1305_state_internal_t* %2, i8* %3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_onetimeauth_poly1305_donna_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %1 = bitcast %struct.crypto_onetimeauth_poly1305_state* %0 to i8*
  %2 = bitcast i8* %1 to %struct.poly1305_state_internal_t*
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  call void @poly1305_update(%struct.poly1305_state_internal_t* %2, i8* %3, i64 %4)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_onetimeauth_poly1305_donna_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %out.addr = alloca i8*, align 4
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %1 = bitcast %struct.crypto_onetimeauth_poly1305_state* %0 to i8*
  %2 = bitcast i8* %1 to %struct.poly1305_state_internal_t*
  %3 = load i8*, i8** %out.addr, align 4
  call void @poly1305_finish(%struct.poly1305_state_internal_t* %2, i8* %3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @poly1305_init(%struct.poly1305_state_internal_t* %st, i8* %key) #0 {
entry:
  %st.addr = alloca %struct.poly1305_state_internal_t*, align 4
  %key.addr = alloca i8*, align 4
  store %struct.poly1305_state_internal_t* %st, %struct.poly1305_state_internal_t** %st.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  %0 = load i8*, i8** %key.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 0
  %call = call i32 @load32_le(i8* %arrayidx)
  %and = and i32 %call, 67108863
  %1 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %1, i32 0, i32 0
  %arrayidx1 = getelementptr [5 x i32], [5 x i32]* %r, i32 0, i32 0
  store i32 %and, i32* %arrayidx1, align 8
  %2 = load i8*, i8** %key.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %2, i32 3
  %call3 = call i32 @load32_le(i8* %arrayidx2)
  %shr = lshr i32 %call3, 2
  %and4 = and i32 %shr, 67108611
  %3 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r5 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %3, i32 0, i32 0
  %arrayidx6 = getelementptr [5 x i32], [5 x i32]* %r5, i32 0, i32 1
  store i32 %and4, i32* %arrayidx6, align 4
  %4 = load i8*, i8** %key.addr, align 4
  %arrayidx7 = getelementptr i8, i8* %4, i32 6
  %call8 = call i32 @load32_le(i8* %arrayidx7)
  %shr9 = lshr i32 %call8, 4
  %and10 = and i32 %shr9, 67092735
  %5 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r11 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %5, i32 0, i32 0
  %arrayidx12 = getelementptr [5 x i32], [5 x i32]* %r11, i32 0, i32 2
  store i32 %and10, i32* %arrayidx12, align 8
  %6 = load i8*, i8** %key.addr, align 4
  %arrayidx13 = getelementptr i8, i8* %6, i32 9
  %call14 = call i32 @load32_le(i8* %arrayidx13)
  %shr15 = lshr i32 %call14, 6
  %and16 = and i32 %shr15, 66076671
  %7 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r17 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %7, i32 0, i32 0
  %arrayidx18 = getelementptr [5 x i32], [5 x i32]* %r17, i32 0, i32 3
  store i32 %and16, i32* %arrayidx18, align 4
  %8 = load i8*, i8** %key.addr, align 4
  %arrayidx19 = getelementptr i8, i8* %8, i32 12
  %call20 = call i32 @load32_le(i8* %arrayidx19)
  %shr21 = lshr i32 %call20, 8
  %and22 = and i32 %shr21, 1048575
  %9 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r23 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %9, i32 0, i32 0
  %arrayidx24 = getelementptr [5 x i32], [5 x i32]* %r23, i32 0, i32 4
  store i32 %and22, i32* %arrayidx24, align 8
  %10 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %10, i32 0, i32 1
  %arrayidx25 = getelementptr [5 x i32], [5 x i32]* %h, i32 0, i32 0
  store i32 0, i32* %arrayidx25, align 4
  %11 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h26 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %11, i32 0, i32 1
  %arrayidx27 = getelementptr [5 x i32], [5 x i32]* %h26, i32 0, i32 1
  store i32 0, i32* %arrayidx27, align 4
  %12 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h28 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %12, i32 0, i32 1
  %arrayidx29 = getelementptr [5 x i32], [5 x i32]* %h28, i32 0, i32 2
  store i32 0, i32* %arrayidx29, align 4
  %13 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h30 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %13, i32 0, i32 1
  %arrayidx31 = getelementptr [5 x i32], [5 x i32]* %h30, i32 0, i32 3
  store i32 0, i32* %arrayidx31, align 4
  %14 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h32 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %14, i32 0, i32 1
  %arrayidx33 = getelementptr [5 x i32], [5 x i32]* %h32, i32 0, i32 4
  store i32 0, i32* %arrayidx33, align 4
  %15 = load i8*, i8** %key.addr, align 4
  %arrayidx34 = getelementptr i8, i8* %15, i32 16
  %call35 = call i32 @load32_le(i8* %arrayidx34)
  %16 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %16, i32 0, i32 2
  %arrayidx36 = getelementptr [4 x i32], [4 x i32]* %pad, i32 0, i32 0
  store i32 %call35, i32* %arrayidx36, align 8
  %17 = load i8*, i8** %key.addr, align 4
  %arrayidx37 = getelementptr i8, i8* %17, i32 20
  %call38 = call i32 @load32_le(i8* %arrayidx37)
  %18 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad39 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %18, i32 0, i32 2
  %arrayidx40 = getelementptr [4 x i32], [4 x i32]* %pad39, i32 0, i32 1
  store i32 %call38, i32* %arrayidx40, align 4
  %19 = load i8*, i8** %key.addr, align 4
  %arrayidx41 = getelementptr i8, i8* %19, i32 24
  %call42 = call i32 @load32_le(i8* %arrayidx41)
  %20 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad43 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %20, i32 0, i32 2
  %arrayidx44 = getelementptr [4 x i32], [4 x i32]* %pad43, i32 0, i32 2
  store i32 %call42, i32* %arrayidx44, align 8
  %21 = load i8*, i8** %key.addr, align 4
  %arrayidx45 = getelementptr i8, i8* %21, i32 28
  %call46 = call i32 @load32_le(i8* %arrayidx45)
  %22 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad47 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %22, i32 0, i32 2
  %arrayidx48 = getelementptr [4 x i32], [4 x i32]* %pad47, i32 0, i32 3
  store i32 %call46, i32* %arrayidx48, align 4
  %23 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %23, i32 0, i32 3
  store i64 0, i64* %leftover, align 8
  %24 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %final = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %24, i32 0, i32 5
  store i8 0, i8* %final, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @poly1305_update(%struct.poly1305_state_internal_t* %st, i8* %m, i64 %bytes) #0 {
entry:
  %st.addr = alloca %struct.poly1305_state_internal_t*, align 4
  %m.addr = alloca i8*, align 4
  %bytes.addr = alloca i64, align 8
  %i = alloca i64, align 8
  %want = alloca i64, align 8
  %want19 = alloca i64, align 8
  store %struct.poly1305_state_internal_t* %st, %struct.poly1305_state_internal_t** %st.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  %0 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %0, i32 0, i32 3
  %1 = load i64, i64* %leftover, align 8
  %tobool = icmp ne i64 %1, 0
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %2 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover1 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %2, i32 0, i32 3
  %3 = load i64, i64* %leftover1, align 8
  %sub = sub i64 16, %3
  store i64 %sub, i64* %want, align 8
  %4 = load i64, i64* %want, align 8
  %5 = load i64, i64* %bytes.addr, align 8
  %cmp = icmp ugt i64 %4, %5
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %6 = load i64, i64* %bytes.addr, align 8
  store i64 %6, i64* %want, align 8
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  store i64 0, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i64, i64* %i, align 8
  %8 = load i64, i64* %want, align 8
  %cmp3 = icmp ult i64 %7, %8
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8*, i8** %m.addr, align 4
  %10 = load i64, i64* %i, align 8
  %idxprom = trunc i64 %10 to i32
  %arrayidx = getelementptr i8, i8* %9, i32 %idxprom
  %11 = load i8, i8* %arrayidx, align 1
  %12 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %12, i32 0, i32 4
  %13 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover4 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %13, i32 0, i32 3
  %14 = load i64, i64* %leftover4, align 8
  %15 = load i64, i64* %i, align 8
  %add = add i64 %14, %15
  %idxprom5 = trunc i64 %add to i32
  %arrayidx6 = getelementptr [16 x i8], [16 x i8]* %buffer, i32 0, i32 %idxprom5
  store i8 %11, i8* %arrayidx6, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i64, i64* %i, align 8
  %inc = add i64 %16, 1
  store i64 %inc, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load i64, i64* %want, align 8
  %18 = load i64, i64* %bytes.addr, align 8
  %sub7 = sub i64 %18, %17
  store i64 %sub7, i64* %bytes.addr, align 8
  %19 = load i64, i64* %want, align 8
  %20 = load i8*, i8** %m.addr, align 4
  %idx.ext = trunc i64 %19 to i32
  %add.ptr = getelementptr i8, i8* %20, i32 %idx.ext
  store i8* %add.ptr, i8** %m.addr, align 4
  %21 = load i64, i64* %want, align 8
  %22 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover8 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %22, i32 0, i32 3
  %23 = load i64, i64* %leftover8, align 8
  %add9 = add i64 %23, %21
  store i64 %add9, i64* %leftover8, align 8
  %24 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover10 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %24, i32 0, i32 3
  %25 = load i64, i64* %leftover10, align 8
  %cmp11 = icmp ult i64 %25, 16
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.end
  br label %if.end41

if.end13:                                         ; preds = %for.end
  %26 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %27 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer14 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %27, i32 0, i32 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %buffer14, i32 0, i32 0
  call void @poly1305_blocks(%struct.poly1305_state_internal_t* %26, i8* %arraydecay, i64 16)
  %28 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover15 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %28, i32 0, i32 3
  store i64 0, i64* %leftover15, align 8
  br label %if.end16

if.end16:                                         ; preds = %if.end13, %entry
  %29 = load i64, i64* %bytes.addr, align 8
  %cmp17 = icmp uge i64 %29, 16
  br i1 %cmp17, label %if.then18, label %if.end23

if.then18:                                        ; preds = %if.end16
  %30 = load i64, i64* %bytes.addr, align 8
  %and = and i64 %30, -16
  store i64 %and, i64* %want19, align 8
  %31 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %32 = load i8*, i8** %m.addr, align 4
  %33 = load i64, i64* %want19, align 8
  call void @poly1305_blocks(%struct.poly1305_state_internal_t* %31, i8* %32, i64 %33)
  %34 = load i64, i64* %want19, align 8
  %35 = load i8*, i8** %m.addr, align 4
  %idx.ext20 = trunc i64 %34 to i32
  %add.ptr21 = getelementptr i8, i8* %35, i32 %idx.ext20
  store i8* %add.ptr21, i8** %m.addr, align 4
  %36 = load i64, i64* %want19, align 8
  %37 = load i64, i64* %bytes.addr, align 8
  %sub22 = sub i64 %37, %36
  store i64 %sub22, i64* %bytes.addr, align 8
  br label %if.end23

if.end23:                                         ; preds = %if.then18, %if.end16
  %38 = load i64, i64* %bytes.addr, align 8
  %tobool24 = icmp ne i64 %38, 0
  br i1 %tobool24, label %if.then25, label %if.end41

if.then25:                                        ; preds = %if.end23
  store i64 0, i64* %i, align 8
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc36, %if.then25
  %39 = load i64, i64* %i, align 8
  %40 = load i64, i64* %bytes.addr, align 8
  %cmp27 = icmp ult i64 %39, %40
  br i1 %cmp27, label %for.body28, label %for.end38

for.body28:                                       ; preds = %for.cond26
  %41 = load i8*, i8** %m.addr, align 4
  %42 = load i64, i64* %i, align 8
  %idxprom29 = trunc i64 %42 to i32
  %arrayidx30 = getelementptr i8, i8* %41, i32 %idxprom29
  %43 = load i8, i8* %arrayidx30, align 1
  %44 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer31 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %44, i32 0, i32 4
  %45 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover32 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %45, i32 0, i32 3
  %46 = load i64, i64* %leftover32, align 8
  %47 = load i64, i64* %i, align 8
  %add33 = add i64 %46, %47
  %idxprom34 = trunc i64 %add33 to i32
  %arrayidx35 = getelementptr [16 x i8], [16 x i8]* %buffer31, i32 0, i32 %idxprom34
  store i8 %43, i8* %arrayidx35, align 1
  br label %for.inc36

for.inc36:                                        ; preds = %for.body28
  %48 = load i64, i64* %i, align 8
  %inc37 = add i64 %48, 1
  store i64 %inc37, i64* %i, align 8
  br label %for.cond26

for.end38:                                        ; preds = %for.cond26
  %49 = load i64, i64* %bytes.addr, align 8
  %50 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover39 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %50, i32 0, i32 3
  %51 = load i64, i64* %leftover39, align 8
  %add40 = add i64 %51, %49
  store i64 %add40, i64* %leftover39, align 8
  br label %if.end41

if.end41:                                         ; preds = %if.then12, %for.end38, %if.end23
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @poly1305_finish(%struct.poly1305_state_internal_t* %st, i8* %mac) #0 {
entry:
  %st.addr = alloca %struct.poly1305_state_internal_t*, align 4
  %mac.addr = alloca i8*, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %c = alloca i32, align 4
  %g0 = alloca i32, align 4
  %g1 = alloca i32, align 4
  %g2 = alloca i32, align 4
  %g3 = alloca i32, align 4
  %g4 = alloca i32, align 4
  %f = alloca i64, align 8
  %mask = alloca i32, align 4
  %i = alloca i64, align 8
  store %struct.poly1305_state_internal_t* %st, %struct.poly1305_state_internal_t** %st.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  %0 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %0, i32 0, i32 3
  %1 = load i64, i64* %leftover, align 8
  %tobool = icmp ne i64 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %leftover1 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %2, i32 0, i32 3
  %3 = load i64, i64* %leftover1, align 8
  store i64 %3, i64* %i, align 8
  %4 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %4, i32 0, i32 4
  %5 = load i64, i64* %i, align 8
  %inc = add i64 %5, 1
  store i64 %inc, i64* %i, align 8
  %idxprom = trunc i64 %5 to i32
  %arrayidx = getelementptr [16 x i8], [16 x i8]* %buffer, i32 0, i32 %idxprom
  store i8 1, i8* %arrayidx, align 1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %6 = load i64, i64* %i, align 8
  %cmp = icmp ult i64 %6, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer2 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %7, i32 0, i32 4
  %8 = load i64, i64* %i, align 8
  %idxprom3 = trunc i64 %8 to i32
  %arrayidx4 = getelementptr [16 x i8], [16 x i8]* %buffer2, i32 0, i32 %idxprom3
  store i8 0, i8* %arrayidx4, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i64, i64* %i, align 8
  %inc5 = add i64 %9, 1
  store i64 %inc5, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %final = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %10, i32 0, i32 5
  store i8 1, i8* %final, align 8
  %11 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %12 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %buffer6 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %12, i32 0, i32 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %buffer6, i32 0, i32 0
  call void @poly1305_blocks(%struct.poly1305_state_internal_t* %11, i8* %arraydecay, i64 16)
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %13 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %13, i32 0, i32 1
  %arrayidx7 = getelementptr [5 x i32], [5 x i32]* %h, i32 0, i32 0
  %14 = load i32, i32* %arrayidx7, align 4
  store i32 %14, i32* %h0, align 4
  %15 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h8 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %15, i32 0, i32 1
  %arrayidx9 = getelementptr [5 x i32], [5 x i32]* %h8, i32 0, i32 1
  %16 = load i32, i32* %arrayidx9, align 4
  store i32 %16, i32* %h1, align 4
  %17 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h10 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %17, i32 0, i32 1
  %arrayidx11 = getelementptr [5 x i32], [5 x i32]* %h10, i32 0, i32 2
  %18 = load i32, i32* %arrayidx11, align 4
  store i32 %18, i32* %h2, align 4
  %19 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h12 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %19, i32 0, i32 1
  %arrayidx13 = getelementptr [5 x i32], [5 x i32]* %h12, i32 0, i32 3
  %20 = load i32, i32* %arrayidx13, align 4
  store i32 %20, i32* %h3, align 4
  %21 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h14 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %21, i32 0, i32 1
  %arrayidx15 = getelementptr [5 x i32], [5 x i32]* %h14, i32 0, i32 4
  %22 = load i32, i32* %arrayidx15, align 4
  store i32 %22, i32* %h4, align 4
  %23 = load i32, i32* %h1, align 4
  %shr = lshr i32 %23, 26
  store i32 %shr, i32* %c, align 4
  %24 = load i32, i32* %h1, align 4
  %and = and i32 %24, 67108863
  store i32 %and, i32* %h1, align 4
  %25 = load i32, i32* %c, align 4
  %26 = load i32, i32* %h2, align 4
  %add = add i32 %26, %25
  store i32 %add, i32* %h2, align 4
  %27 = load i32, i32* %h2, align 4
  %shr16 = lshr i32 %27, 26
  store i32 %shr16, i32* %c, align 4
  %28 = load i32, i32* %h2, align 4
  %and17 = and i32 %28, 67108863
  store i32 %and17, i32* %h2, align 4
  %29 = load i32, i32* %c, align 4
  %30 = load i32, i32* %h3, align 4
  %add18 = add i32 %30, %29
  store i32 %add18, i32* %h3, align 4
  %31 = load i32, i32* %h3, align 4
  %shr19 = lshr i32 %31, 26
  store i32 %shr19, i32* %c, align 4
  %32 = load i32, i32* %h3, align 4
  %and20 = and i32 %32, 67108863
  store i32 %and20, i32* %h3, align 4
  %33 = load i32, i32* %c, align 4
  %34 = load i32, i32* %h4, align 4
  %add21 = add i32 %34, %33
  store i32 %add21, i32* %h4, align 4
  %35 = load i32, i32* %h4, align 4
  %shr22 = lshr i32 %35, 26
  store i32 %shr22, i32* %c, align 4
  %36 = load i32, i32* %h4, align 4
  %and23 = and i32 %36, 67108863
  store i32 %and23, i32* %h4, align 4
  %37 = load i32, i32* %c, align 4
  %mul = mul i32 %37, 5
  %38 = load i32, i32* %h0, align 4
  %add24 = add i32 %38, %mul
  store i32 %add24, i32* %h0, align 4
  %39 = load i32, i32* %h0, align 4
  %shr25 = lshr i32 %39, 26
  store i32 %shr25, i32* %c, align 4
  %40 = load i32, i32* %h0, align 4
  %and26 = and i32 %40, 67108863
  store i32 %and26, i32* %h0, align 4
  %41 = load i32, i32* %c, align 4
  %42 = load i32, i32* %h1, align 4
  %add27 = add i32 %42, %41
  store i32 %add27, i32* %h1, align 4
  %43 = load i32, i32* %h0, align 4
  %add28 = add i32 %43, 5
  store i32 %add28, i32* %g0, align 4
  %44 = load i32, i32* %g0, align 4
  %shr29 = lshr i32 %44, 26
  store i32 %shr29, i32* %c, align 4
  %45 = load i32, i32* %g0, align 4
  %and30 = and i32 %45, 67108863
  store i32 %and30, i32* %g0, align 4
  %46 = load i32, i32* %h1, align 4
  %47 = load i32, i32* %c, align 4
  %add31 = add i32 %46, %47
  store i32 %add31, i32* %g1, align 4
  %48 = load i32, i32* %g1, align 4
  %shr32 = lshr i32 %48, 26
  store i32 %shr32, i32* %c, align 4
  %49 = load i32, i32* %g1, align 4
  %and33 = and i32 %49, 67108863
  store i32 %and33, i32* %g1, align 4
  %50 = load i32, i32* %h2, align 4
  %51 = load i32, i32* %c, align 4
  %add34 = add i32 %50, %51
  store i32 %add34, i32* %g2, align 4
  %52 = load i32, i32* %g2, align 4
  %shr35 = lshr i32 %52, 26
  store i32 %shr35, i32* %c, align 4
  %53 = load i32, i32* %g2, align 4
  %and36 = and i32 %53, 67108863
  store i32 %and36, i32* %g2, align 4
  %54 = load i32, i32* %h3, align 4
  %55 = load i32, i32* %c, align 4
  %add37 = add i32 %54, %55
  store i32 %add37, i32* %g3, align 4
  %56 = load i32, i32* %g3, align 4
  %shr38 = lshr i32 %56, 26
  store i32 %shr38, i32* %c, align 4
  %57 = load i32, i32* %g3, align 4
  %and39 = and i32 %57, 67108863
  store i32 %and39, i32* %g3, align 4
  %58 = load i32, i32* %h4, align 4
  %59 = load i32, i32* %c, align 4
  %add40 = add i32 %58, %59
  %sub = sub i32 %add40, 67108864
  store i32 %sub, i32* %g4, align 4
  %60 = load i32, i32* %g4, align 4
  %shr41 = lshr i32 %60, 31
  %sub42 = sub i32 %shr41, 1
  store i32 %sub42, i32* %mask, align 4
  %61 = load i32, i32* %mask, align 4
  %62 = load i32, i32* %g0, align 4
  %and43 = and i32 %62, %61
  store i32 %and43, i32* %g0, align 4
  %63 = load i32, i32* %mask, align 4
  %64 = load i32, i32* %g1, align 4
  %and44 = and i32 %64, %63
  store i32 %and44, i32* %g1, align 4
  %65 = load i32, i32* %mask, align 4
  %66 = load i32, i32* %g2, align 4
  %and45 = and i32 %66, %65
  store i32 %and45, i32* %g2, align 4
  %67 = load i32, i32* %mask, align 4
  %68 = load i32, i32* %g3, align 4
  %and46 = and i32 %68, %67
  store i32 %and46, i32* %g3, align 4
  %69 = load i32, i32* %mask, align 4
  %70 = load i32, i32* %g4, align 4
  %and47 = and i32 %70, %69
  store i32 %and47, i32* %g4, align 4
  %71 = load i32, i32* %mask, align 4
  %neg = xor i32 %71, -1
  store i32 %neg, i32* %mask, align 4
  %72 = load i32, i32* %h0, align 4
  %73 = load i32, i32* %mask, align 4
  %and48 = and i32 %72, %73
  %74 = load i32, i32* %g0, align 4
  %or = or i32 %and48, %74
  store i32 %or, i32* %h0, align 4
  %75 = load i32, i32* %h1, align 4
  %76 = load i32, i32* %mask, align 4
  %and49 = and i32 %75, %76
  %77 = load i32, i32* %g1, align 4
  %or50 = or i32 %and49, %77
  store i32 %or50, i32* %h1, align 4
  %78 = load i32, i32* %h2, align 4
  %79 = load i32, i32* %mask, align 4
  %and51 = and i32 %78, %79
  %80 = load i32, i32* %g2, align 4
  %or52 = or i32 %and51, %80
  store i32 %or52, i32* %h2, align 4
  %81 = load i32, i32* %h3, align 4
  %82 = load i32, i32* %mask, align 4
  %and53 = and i32 %81, %82
  %83 = load i32, i32* %g3, align 4
  %or54 = or i32 %and53, %83
  store i32 %or54, i32* %h3, align 4
  %84 = load i32, i32* %h4, align 4
  %85 = load i32, i32* %mask, align 4
  %and55 = and i32 %84, %85
  %86 = load i32, i32* %g4, align 4
  %or56 = or i32 %and55, %86
  store i32 %or56, i32* %h4, align 4
  %87 = load i32, i32* %h0, align 4
  %88 = load i32, i32* %h1, align 4
  %shl = shl i32 %88, 26
  %or57 = or i32 %87, %shl
  store i32 %or57, i32* %h0, align 4
  %89 = load i32, i32* %h1, align 4
  %shr58 = lshr i32 %89, 6
  %90 = load i32, i32* %h2, align 4
  %shl59 = shl i32 %90, 20
  %or60 = or i32 %shr58, %shl59
  store i32 %or60, i32* %h1, align 4
  %91 = load i32, i32* %h2, align 4
  %shr61 = lshr i32 %91, 12
  %92 = load i32, i32* %h3, align 4
  %shl62 = shl i32 %92, 14
  %or63 = or i32 %shr61, %shl62
  store i32 %or63, i32* %h2, align 4
  %93 = load i32, i32* %h3, align 4
  %shr64 = lshr i32 %93, 18
  %94 = load i32, i32* %h4, align 4
  %shl65 = shl i32 %94, 8
  %or66 = or i32 %shr64, %shl65
  store i32 %or66, i32* %h3, align 4
  %95 = load i32, i32* %h0, align 4
  %conv = zext i32 %95 to i64
  %96 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %96, i32 0, i32 2
  %arrayidx67 = getelementptr [4 x i32], [4 x i32]* %pad, i32 0, i32 0
  %97 = load i32, i32* %arrayidx67, align 8
  %conv68 = zext i32 %97 to i64
  %add69 = add i64 %conv, %conv68
  store i64 %add69, i64* %f, align 8
  %98 = load i64, i64* %f, align 8
  %conv70 = trunc i64 %98 to i32
  store i32 %conv70, i32* %h0, align 4
  %99 = load i32, i32* %h1, align 4
  %conv71 = zext i32 %99 to i64
  %100 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad72 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %100, i32 0, i32 2
  %arrayidx73 = getelementptr [4 x i32], [4 x i32]* %pad72, i32 0, i32 1
  %101 = load i32, i32* %arrayidx73, align 4
  %conv74 = zext i32 %101 to i64
  %add75 = add i64 %conv71, %conv74
  %102 = load i64, i64* %f, align 8
  %shr76 = lshr i64 %102, 32
  %add77 = add i64 %add75, %shr76
  store i64 %add77, i64* %f, align 8
  %103 = load i64, i64* %f, align 8
  %conv78 = trunc i64 %103 to i32
  store i32 %conv78, i32* %h1, align 4
  %104 = load i32, i32* %h2, align 4
  %conv79 = zext i32 %104 to i64
  %105 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad80 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %105, i32 0, i32 2
  %arrayidx81 = getelementptr [4 x i32], [4 x i32]* %pad80, i32 0, i32 2
  %106 = load i32, i32* %arrayidx81, align 8
  %conv82 = zext i32 %106 to i64
  %add83 = add i64 %conv79, %conv82
  %107 = load i64, i64* %f, align 8
  %shr84 = lshr i64 %107, 32
  %add85 = add i64 %add83, %shr84
  store i64 %add85, i64* %f, align 8
  %108 = load i64, i64* %f, align 8
  %conv86 = trunc i64 %108 to i32
  store i32 %conv86, i32* %h2, align 4
  %109 = load i32, i32* %h3, align 4
  %conv87 = zext i32 %109 to i64
  %110 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %pad88 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %110, i32 0, i32 2
  %arrayidx89 = getelementptr [4 x i32], [4 x i32]* %pad88, i32 0, i32 3
  %111 = load i32, i32* %arrayidx89, align 4
  %conv90 = zext i32 %111 to i64
  %add91 = add i64 %conv87, %conv90
  %112 = load i64, i64* %f, align 8
  %shr92 = lshr i64 %112, 32
  %add93 = add i64 %add91, %shr92
  store i64 %add93, i64* %f, align 8
  %113 = load i64, i64* %f, align 8
  %conv94 = trunc i64 %113 to i32
  store i32 %conv94, i32* %h3, align 4
  %114 = load i8*, i8** %mac.addr, align 4
  %add.ptr = getelementptr i8, i8* %114, i32 0
  %115 = load i32, i32* %h0, align 4
  call void @store32_le(i8* %add.ptr, i32 %115)
  %116 = load i8*, i8** %mac.addr, align 4
  %add.ptr95 = getelementptr i8, i8* %116, i32 4
  %117 = load i32, i32* %h1, align 4
  call void @store32_le(i8* %add.ptr95, i32 %117)
  %118 = load i8*, i8** %mac.addr, align 4
  %add.ptr96 = getelementptr i8, i8* %118, i32 8
  %119 = load i32, i32* %h2, align 4
  call void @store32_le(i8* %add.ptr96, i32 %119)
  %120 = load i8*, i8** %mac.addr, align 4
  %add.ptr97 = getelementptr i8, i8* %120, i32 12
  %121 = load i32, i32* %h3, align 4
  call void @store32_le(i8* %add.ptr97, i32 %121)
  %122 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %123 = bitcast %struct.poly1305_state_internal_t* %122 to i8*
  call void @sodium_memzero(i8* %123, i32 88)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @load32_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i32* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 1 %1, i32 4, i1 false)
  %2 = load i32, i32* %w, align 4
  ret i32 %2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @poly1305_blocks(%struct.poly1305_state_internal_t* %st, i8* %m, i64 %bytes) #0 {
entry:
  %st.addr = alloca %struct.poly1305_state_internal_t*, align 4
  %m.addr = alloca i8*, align 4
  %bytes.addr = alloca i64, align 8
  %hibit = alloca i32, align 4
  %r0 = alloca i32, align 4
  %r1 = alloca i32, align 4
  %r2 = alloca i32, align 4
  %r3 = alloca i32, align 4
  %r4 = alloca i32, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %d0 = alloca i64, align 8
  %d1 = alloca i64, align 8
  %d2 = alloca i64, align 8
  %d3 = alloca i64, align 8
  %d4 = alloca i64, align 8
  %c = alloca i32, align 4
  store %struct.poly1305_state_internal_t* %st, %struct.poly1305_state_internal_t** %st.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  %0 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %final = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %0, i32 0, i32 5
  %1 = load i8, i8* %final, align 8
  %conv = zext i8 %1 to i32
  %tobool = icmp ne i32 %conv, 0
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 0, i32 16777216
  store i32 %cond, i32* %hibit, align 4
  %3 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %3, i32 0, i32 0
  %arrayidx = getelementptr [5 x i32], [5 x i32]* %r, i32 0, i32 0
  %4 = load i32, i32* %arrayidx, align 8
  store i32 %4, i32* %r0, align 4
  %5 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r5 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %5, i32 0, i32 0
  %arrayidx6 = getelementptr [5 x i32], [5 x i32]* %r5, i32 0, i32 1
  %6 = load i32, i32* %arrayidx6, align 4
  store i32 %6, i32* %r1, align 4
  %7 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r7 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %7, i32 0, i32 0
  %arrayidx8 = getelementptr [5 x i32], [5 x i32]* %r7, i32 0, i32 2
  %8 = load i32, i32* %arrayidx8, align 8
  store i32 %8, i32* %r2, align 4
  %9 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r9 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %9, i32 0, i32 0
  %arrayidx10 = getelementptr [5 x i32], [5 x i32]* %r9, i32 0, i32 3
  %10 = load i32, i32* %arrayidx10, align 4
  store i32 %10, i32* %r3, align 4
  %11 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %r11 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %11, i32 0, i32 0
  %arrayidx12 = getelementptr [5 x i32], [5 x i32]* %r11, i32 0, i32 4
  %12 = load i32, i32* %arrayidx12, align 8
  store i32 %12, i32* %r4, align 4
  %13 = load i32, i32* %r1, align 4
  %mul = mul i32 %13, 5
  store i32 %mul, i32* %s1, align 4
  %14 = load i32, i32* %r2, align 4
  %mul13 = mul i32 %14, 5
  store i32 %mul13, i32* %s2, align 4
  %15 = load i32, i32* %r3, align 4
  %mul14 = mul i32 %15, 5
  store i32 %mul14, i32* %s3, align 4
  %16 = load i32, i32* %r4, align 4
  %mul15 = mul i32 %16, 5
  store i32 %mul15, i32* %s4, align 4
  %17 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %17, i32 0, i32 1
  %arrayidx16 = getelementptr [5 x i32], [5 x i32]* %h, i32 0, i32 0
  %18 = load i32, i32* %arrayidx16, align 4
  store i32 %18, i32* %h0, align 4
  %19 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h17 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %19, i32 0, i32 1
  %arrayidx18 = getelementptr [5 x i32], [5 x i32]* %h17, i32 0, i32 1
  %20 = load i32, i32* %arrayidx18, align 4
  store i32 %20, i32* %h1, align 4
  %21 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h19 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %21, i32 0, i32 1
  %arrayidx20 = getelementptr [5 x i32], [5 x i32]* %h19, i32 0, i32 2
  %22 = load i32, i32* %arrayidx20, align 4
  store i32 %22, i32* %h2, align 4
  %23 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h21 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %23, i32 0, i32 1
  %arrayidx22 = getelementptr [5 x i32], [5 x i32]* %h21, i32 0, i32 3
  %24 = load i32, i32* %arrayidx22, align 4
  store i32 %24, i32* %h3, align 4
  %25 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h23 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %25, i32 0, i32 1
  %arrayidx24 = getelementptr [5 x i32], [5 x i32]* %h23, i32 0, i32 4
  %26 = load i32, i32* %arrayidx24, align 4
  store i32 %26, i32* %h4, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %27 = load i64, i64* %bytes.addr, align 8
  %cmp = icmp uge i64 %27, 16
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %28 = load i8*, i8** %m.addr, align 4
  %add.ptr = getelementptr i8, i8* %28, i32 0
  %call = call i32 @load32_le(i8* %add.ptr)
  %and = and i32 %call, 67108863
  %29 = load i32, i32* %h0, align 4
  %add = add i32 %29, %and
  store i32 %add, i32* %h0, align 4
  %30 = load i8*, i8** %m.addr, align 4
  %add.ptr26 = getelementptr i8, i8* %30, i32 3
  %call27 = call i32 @load32_le(i8* %add.ptr26)
  %shr = lshr i32 %call27, 2
  %and28 = and i32 %shr, 67108863
  %31 = load i32, i32* %h1, align 4
  %add29 = add i32 %31, %and28
  store i32 %add29, i32* %h1, align 4
  %32 = load i8*, i8** %m.addr, align 4
  %add.ptr30 = getelementptr i8, i8* %32, i32 6
  %call31 = call i32 @load32_le(i8* %add.ptr30)
  %shr32 = lshr i32 %call31, 4
  %and33 = and i32 %shr32, 67108863
  %33 = load i32, i32* %h2, align 4
  %add34 = add i32 %33, %and33
  store i32 %add34, i32* %h2, align 4
  %34 = load i8*, i8** %m.addr, align 4
  %add.ptr35 = getelementptr i8, i8* %34, i32 9
  %call36 = call i32 @load32_le(i8* %add.ptr35)
  %shr37 = lshr i32 %call36, 6
  %and38 = and i32 %shr37, 67108863
  %35 = load i32, i32* %h3, align 4
  %add39 = add i32 %35, %and38
  store i32 %add39, i32* %h3, align 4
  %36 = load i8*, i8** %m.addr, align 4
  %add.ptr40 = getelementptr i8, i8* %36, i32 12
  %call41 = call i32 @load32_le(i8* %add.ptr40)
  %shr42 = lshr i32 %call41, 8
  %37 = load i32, i32* %hibit, align 4
  %or = or i32 %shr42, %37
  %38 = load i32, i32* %h4, align 4
  %add43 = add i32 %38, %or
  store i32 %add43, i32* %h4, align 4
  %39 = load i32, i32* %h0, align 4
  %conv44 = zext i32 %39 to i64
  %40 = load i32, i32* %r0, align 4
  %conv45 = zext i32 %40 to i64
  %mul46 = mul i64 %conv44, %conv45
  %41 = load i32, i32* %h1, align 4
  %conv47 = zext i32 %41 to i64
  %42 = load i32, i32* %s4, align 4
  %conv48 = zext i32 %42 to i64
  %mul49 = mul i64 %conv47, %conv48
  %add50 = add i64 %mul46, %mul49
  %43 = load i32, i32* %h2, align 4
  %conv51 = zext i32 %43 to i64
  %44 = load i32, i32* %s3, align 4
  %conv52 = zext i32 %44 to i64
  %mul53 = mul i64 %conv51, %conv52
  %add54 = add i64 %add50, %mul53
  %45 = load i32, i32* %h3, align 4
  %conv55 = zext i32 %45 to i64
  %46 = load i32, i32* %s2, align 4
  %conv56 = zext i32 %46 to i64
  %mul57 = mul i64 %conv55, %conv56
  %add58 = add i64 %add54, %mul57
  %47 = load i32, i32* %h4, align 4
  %conv59 = zext i32 %47 to i64
  %48 = load i32, i32* %s1, align 4
  %conv60 = zext i32 %48 to i64
  %mul61 = mul i64 %conv59, %conv60
  %add62 = add i64 %add58, %mul61
  store i64 %add62, i64* %d0, align 8
  %49 = load i32, i32* %h0, align 4
  %conv63 = zext i32 %49 to i64
  %50 = load i32, i32* %r1, align 4
  %conv64 = zext i32 %50 to i64
  %mul65 = mul i64 %conv63, %conv64
  %51 = load i32, i32* %h1, align 4
  %conv66 = zext i32 %51 to i64
  %52 = load i32, i32* %r0, align 4
  %conv67 = zext i32 %52 to i64
  %mul68 = mul i64 %conv66, %conv67
  %add69 = add i64 %mul65, %mul68
  %53 = load i32, i32* %h2, align 4
  %conv70 = zext i32 %53 to i64
  %54 = load i32, i32* %s4, align 4
  %conv71 = zext i32 %54 to i64
  %mul72 = mul i64 %conv70, %conv71
  %add73 = add i64 %add69, %mul72
  %55 = load i32, i32* %h3, align 4
  %conv74 = zext i32 %55 to i64
  %56 = load i32, i32* %s3, align 4
  %conv75 = zext i32 %56 to i64
  %mul76 = mul i64 %conv74, %conv75
  %add77 = add i64 %add73, %mul76
  %57 = load i32, i32* %h4, align 4
  %conv78 = zext i32 %57 to i64
  %58 = load i32, i32* %s2, align 4
  %conv79 = zext i32 %58 to i64
  %mul80 = mul i64 %conv78, %conv79
  %add81 = add i64 %add77, %mul80
  store i64 %add81, i64* %d1, align 8
  %59 = load i32, i32* %h0, align 4
  %conv82 = zext i32 %59 to i64
  %60 = load i32, i32* %r2, align 4
  %conv83 = zext i32 %60 to i64
  %mul84 = mul i64 %conv82, %conv83
  %61 = load i32, i32* %h1, align 4
  %conv85 = zext i32 %61 to i64
  %62 = load i32, i32* %r1, align 4
  %conv86 = zext i32 %62 to i64
  %mul87 = mul i64 %conv85, %conv86
  %add88 = add i64 %mul84, %mul87
  %63 = load i32, i32* %h2, align 4
  %conv89 = zext i32 %63 to i64
  %64 = load i32, i32* %r0, align 4
  %conv90 = zext i32 %64 to i64
  %mul91 = mul i64 %conv89, %conv90
  %add92 = add i64 %add88, %mul91
  %65 = load i32, i32* %h3, align 4
  %conv93 = zext i32 %65 to i64
  %66 = load i32, i32* %s4, align 4
  %conv94 = zext i32 %66 to i64
  %mul95 = mul i64 %conv93, %conv94
  %add96 = add i64 %add92, %mul95
  %67 = load i32, i32* %h4, align 4
  %conv97 = zext i32 %67 to i64
  %68 = load i32, i32* %s3, align 4
  %conv98 = zext i32 %68 to i64
  %mul99 = mul i64 %conv97, %conv98
  %add100 = add i64 %add96, %mul99
  store i64 %add100, i64* %d2, align 8
  %69 = load i32, i32* %h0, align 4
  %conv101 = zext i32 %69 to i64
  %70 = load i32, i32* %r3, align 4
  %conv102 = zext i32 %70 to i64
  %mul103 = mul i64 %conv101, %conv102
  %71 = load i32, i32* %h1, align 4
  %conv104 = zext i32 %71 to i64
  %72 = load i32, i32* %r2, align 4
  %conv105 = zext i32 %72 to i64
  %mul106 = mul i64 %conv104, %conv105
  %add107 = add i64 %mul103, %mul106
  %73 = load i32, i32* %h2, align 4
  %conv108 = zext i32 %73 to i64
  %74 = load i32, i32* %r1, align 4
  %conv109 = zext i32 %74 to i64
  %mul110 = mul i64 %conv108, %conv109
  %add111 = add i64 %add107, %mul110
  %75 = load i32, i32* %h3, align 4
  %conv112 = zext i32 %75 to i64
  %76 = load i32, i32* %r0, align 4
  %conv113 = zext i32 %76 to i64
  %mul114 = mul i64 %conv112, %conv113
  %add115 = add i64 %add111, %mul114
  %77 = load i32, i32* %h4, align 4
  %conv116 = zext i32 %77 to i64
  %78 = load i32, i32* %s4, align 4
  %conv117 = zext i32 %78 to i64
  %mul118 = mul i64 %conv116, %conv117
  %add119 = add i64 %add115, %mul118
  store i64 %add119, i64* %d3, align 8
  %79 = load i32, i32* %h0, align 4
  %conv120 = zext i32 %79 to i64
  %80 = load i32, i32* %r4, align 4
  %conv121 = zext i32 %80 to i64
  %mul122 = mul i64 %conv120, %conv121
  %81 = load i32, i32* %h1, align 4
  %conv123 = zext i32 %81 to i64
  %82 = load i32, i32* %r3, align 4
  %conv124 = zext i32 %82 to i64
  %mul125 = mul i64 %conv123, %conv124
  %add126 = add i64 %mul122, %mul125
  %83 = load i32, i32* %h2, align 4
  %conv127 = zext i32 %83 to i64
  %84 = load i32, i32* %r2, align 4
  %conv128 = zext i32 %84 to i64
  %mul129 = mul i64 %conv127, %conv128
  %add130 = add i64 %add126, %mul129
  %85 = load i32, i32* %h3, align 4
  %conv131 = zext i32 %85 to i64
  %86 = load i32, i32* %r1, align 4
  %conv132 = zext i32 %86 to i64
  %mul133 = mul i64 %conv131, %conv132
  %add134 = add i64 %add130, %mul133
  %87 = load i32, i32* %h4, align 4
  %conv135 = zext i32 %87 to i64
  %88 = load i32, i32* %r0, align 4
  %conv136 = zext i32 %88 to i64
  %mul137 = mul i64 %conv135, %conv136
  %add138 = add i64 %add134, %mul137
  store i64 %add138, i64* %d4, align 8
  %89 = load i64, i64* %d0, align 8
  %shr139 = lshr i64 %89, 26
  %conv140 = trunc i64 %shr139 to i32
  store i32 %conv140, i32* %c, align 4
  %90 = load i64, i64* %d0, align 8
  %conv141 = trunc i64 %90 to i32
  %and142 = and i32 %conv141, 67108863
  store i32 %and142, i32* %h0, align 4
  %91 = load i32, i32* %c, align 4
  %conv143 = zext i32 %91 to i64
  %92 = load i64, i64* %d1, align 8
  %add144 = add i64 %92, %conv143
  store i64 %add144, i64* %d1, align 8
  %93 = load i64, i64* %d1, align 8
  %shr145 = lshr i64 %93, 26
  %conv146 = trunc i64 %shr145 to i32
  store i32 %conv146, i32* %c, align 4
  %94 = load i64, i64* %d1, align 8
  %conv147 = trunc i64 %94 to i32
  %and148 = and i32 %conv147, 67108863
  store i32 %and148, i32* %h1, align 4
  %95 = load i32, i32* %c, align 4
  %conv149 = zext i32 %95 to i64
  %96 = load i64, i64* %d2, align 8
  %add150 = add i64 %96, %conv149
  store i64 %add150, i64* %d2, align 8
  %97 = load i64, i64* %d2, align 8
  %shr151 = lshr i64 %97, 26
  %conv152 = trunc i64 %shr151 to i32
  store i32 %conv152, i32* %c, align 4
  %98 = load i64, i64* %d2, align 8
  %conv153 = trunc i64 %98 to i32
  %and154 = and i32 %conv153, 67108863
  store i32 %and154, i32* %h2, align 4
  %99 = load i32, i32* %c, align 4
  %conv155 = zext i32 %99 to i64
  %100 = load i64, i64* %d3, align 8
  %add156 = add i64 %100, %conv155
  store i64 %add156, i64* %d3, align 8
  %101 = load i64, i64* %d3, align 8
  %shr157 = lshr i64 %101, 26
  %conv158 = trunc i64 %shr157 to i32
  store i32 %conv158, i32* %c, align 4
  %102 = load i64, i64* %d3, align 8
  %conv159 = trunc i64 %102 to i32
  %and160 = and i32 %conv159, 67108863
  store i32 %and160, i32* %h3, align 4
  %103 = load i32, i32* %c, align 4
  %conv161 = zext i32 %103 to i64
  %104 = load i64, i64* %d4, align 8
  %add162 = add i64 %104, %conv161
  store i64 %add162, i64* %d4, align 8
  %105 = load i64, i64* %d4, align 8
  %shr163 = lshr i64 %105, 26
  %conv164 = trunc i64 %shr163 to i32
  store i32 %conv164, i32* %c, align 4
  %106 = load i64, i64* %d4, align 8
  %conv165 = trunc i64 %106 to i32
  %and166 = and i32 %conv165, 67108863
  store i32 %and166, i32* %h4, align 4
  %107 = load i32, i32* %c, align 4
  %mul167 = mul i32 %107, 5
  %108 = load i32, i32* %h0, align 4
  %add168 = add i32 %108, %mul167
  store i32 %add168, i32* %h0, align 4
  %109 = load i32, i32* %h0, align 4
  %shr169 = lshr i32 %109, 26
  store i32 %shr169, i32* %c, align 4
  %110 = load i32, i32* %h0, align 4
  %and170 = and i32 %110, 67108863
  store i32 %and170, i32* %h0, align 4
  %111 = load i32, i32* %c, align 4
  %112 = load i32, i32* %h1, align 4
  %add171 = add i32 %112, %111
  store i32 %add171, i32* %h1, align 4
  %113 = load i8*, i8** %m.addr, align 4
  %add.ptr172 = getelementptr i8, i8* %113, i32 16
  store i8* %add.ptr172, i8** %m.addr, align 4
  %114 = load i64, i64* %bytes.addr, align 8
  %sub = sub i64 %114, 16
  store i64 %sub, i64* %bytes.addr, align 8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %115 = load i32, i32* %h0, align 4
  %116 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h173 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %116, i32 0, i32 1
  %arrayidx174 = getelementptr [5 x i32], [5 x i32]* %h173, i32 0, i32 0
  store i32 %115, i32* %arrayidx174, align 4
  %117 = load i32, i32* %h1, align 4
  %118 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h175 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %118, i32 0, i32 1
  %arrayidx176 = getelementptr [5 x i32], [5 x i32]* %h175, i32 0, i32 1
  store i32 %117, i32* %arrayidx176, align 4
  %119 = load i32, i32* %h2, align 4
  %120 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h177 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %120, i32 0, i32 1
  %arrayidx178 = getelementptr [5 x i32], [5 x i32]* %h177, i32 0, i32 2
  store i32 %119, i32* %arrayidx178, align 4
  %121 = load i32, i32* %h3, align 4
  %122 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h179 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %122, i32 0, i32 1
  %arrayidx180 = getelementptr [5 x i32], [5 x i32]* %h179, i32 0, i32 3
  store i32 %121, i32* %arrayidx180, align 4
  %123 = load i32, i32* %h4, align 4
  %124 = load %struct.poly1305_state_internal_t*, %struct.poly1305_state_internal_t** %st.addr, align 4
  %h181 = getelementptr inbounds %struct.poly1305_state_internal_t, %struct.poly1305_state_internal_t* %124, i32 0, i32 1
  %arrayidx182 = getelementptr [5 x i32], [5 x i32]* %h181, i32 0, i32 4
  store i32 %123, i32* %arrayidx182, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

declare void @sodium_memzero(i8*, i32) #2

declare i32 @crypto_verify_16(i8*, i8*) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
