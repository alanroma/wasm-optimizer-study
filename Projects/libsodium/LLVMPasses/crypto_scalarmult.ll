; ModuleID = 'crypto_scalarmult/crypto_scalarmult.c'
source_filename = "crypto_scalarmult/crypto_scalarmult.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [11 x i8] c"curve25519\00", align 1

; Function Attrs: noinline nounwind optnone
define i8* @crypto_scalarmult_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_base(i8* nonnull %q, i8* nonnull %n) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %call = call i32 @crypto_scalarmult_curve25519_base(i8* %0, i8* %1)
  ret i32 %call
}

declare i32 @crypto_scalarmult_curve25519_base(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult(i8* nonnull %q, i8* nonnull %n, i8* nonnull %p) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  %1 = load i8*, i8** %n.addr, align 4
  %2 = load i8*, i8** %p.addr, align 4
  %call = call i32 @crypto_scalarmult_curve25519(i8* %0, i8* %1, i8* %2)
  ret i32 %call
}

declare i32 @crypto_scalarmult_curve25519(i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_scalarbytes() #0 {
entry:
  ret i32 32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
