; ModuleID = 'crypto_shorthash/siphash24/ref/shorthash_siphashx24_ref.c'
source_filename = "crypto_shorthash/siphash24/ref/shorthash_siphashx24_ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_shorthash_siphashx24(i8* nonnull %out, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %v0 = alloca i64, align 8
  %v1 = alloca i64, align 8
  %v2 = alloca i64, align 8
  %v3 = alloca i64, align 8
  %b = alloca i64, align 8
  %k0 = alloca i64, align 8
  %k1 = alloca i64, align 8
  %m = alloca i64, align 8
  %end = alloca i8*, align 4
  %left = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  store i64 8317987319222330741, i64* %v0, align 8
  store i64 7237128888997146499, i64* %v1, align 8
  store i64 7816392313619706465, i64* %v2, align 8
  store i64 8387220255154660723, i64* %v3, align 8
  %0 = load i8*, i8** %k.addr, align 4
  %call = call i64 @load64_le(i8* %0)
  store i64 %call, i64* %k0, align 8
  %1 = load i8*, i8** %k.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 8
  %call1 = call i64 @load64_le(i8* %add.ptr)
  store i64 %call1, i64* %k1, align 8
  %2 = load i8*, i8** %in.addr, align 4
  %3 = load i64, i64* %inlen.addr, align 8
  %idx.ext = trunc i64 %3 to i32
  %add.ptr2 = getelementptr i8, i8* %2, i32 %idx.ext
  %4 = load i64, i64* %inlen.addr, align 8
  %rem = urem i64 %4, 8
  %idx.ext3 = trunc i64 %rem to i32
  %idx.neg = sub i32 0, %idx.ext3
  %add.ptr4 = getelementptr i8, i8* %add.ptr2, i32 %idx.neg
  store i8* %add.ptr4, i8** %end, align 4
  %5 = load i64, i64* %inlen.addr, align 8
  %and = and i64 %5, 7
  %conv = trunc i64 %and to i32
  store i32 %conv, i32* %left, align 4
  %6 = load i64, i64* %inlen.addr, align 8
  %shl = shl i64 %6, 56
  store i64 %shl, i64* %b, align 8
  %7 = load i64, i64* %k1, align 8
  %8 = load i64, i64* %v3, align 8
  %xor = xor i64 %8, %7
  store i64 %xor, i64* %v3, align 8
  %9 = load i64, i64* %k0, align 8
  %10 = load i64, i64* %v2, align 8
  %xor5 = xor i64 %10, %9
  store i64 %xor5, i64* %v2, align 8
  %11 = load i64, i64* %k1, align 8
  %12 = load i64, i64* %v1, align 8
  %xor6 = xor i64 %12, %11
  store i64 %xor6, i64* %v1, align 8
  %13 = load i64, i64* %k0, align 8
  %14 = load i64, i64* %v0, align 8
  %xor7 = xor i64 %14, %13
  store i64 %xor7, i64* %v0, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i8*, i8** %in.addr, align 4
  %16 = load i8*, i8** %end, align 4
  %cmp = icmp ne i8* %15, %16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = load i8*, i8** %in.addr, align 4
  %call9 = call i64 @load64_le(i8* %17)
  store i64 %call9, i64* %m, align 8
  %18 = load i64, i64* %m, align 8
  %19 = load i64, i64* %v3, align 8
  %xor10 = xor i64 %19, %18
  store i64 %xor10, i64* %v3, align 8
  br label %do.body

do.body:                                          ; preds = %for.body
  %20 = load i64, i64* %v1, align 8
  %21 = load i64, i64* %v0, align 8
  %add = add i64 %21, %20
  store i64 %add, i64* %v0, align 8
  %22 = load i64, i64* %v1, align 8
  %call11 = call i64 @rotl64(i64 %22, i32 13)
  store i64 %call11, i64* %v1, align 8
  %23 = load i64, i64* %v0, align 8
  %24 = load i64, i64* %v1, align 8
  %xor12 = xor i64 %24, %23
  store i64 %xor12, i64* %v1, align 8
  %25 = load i64, i64* %v0, align 8
  %call13 = call i64 @rotl64(i64 %25, i32 32)
  store i64 %call13, i64* %v0, align 8
  %26 = load i64, i64* %v3, align 8
  %27 = load i64, i64* %v2, align 8
  %add14 = add i64 %27, %26
  store i64 %add14, i64* %v2, align 8
  %28 = load i64, i64* %v3, align 8
  %call15 = call i64 @rotl64(i64 %28, i32 16)
  store i64 %call15, i64* %v3, align 8
  %29 = load i64, i64* %v2, align 8
  %30 = load i64, i64* %v3, align 8
  %xor16 = xor i64 %30, %29
  store i64 %xor16, i64* %v3, align 8
  %31 = load i64, i64* %v3, align 8
  %32 = load i64, i64* %v0, align 8
  %add17 = add i64 %32, %31
  store i64 %add17, i64* %v0, align 8
  %33 = load i64, i64* %v3, align 8
  %call18 = call i64 @rotl64(i64 %33, i32 21)
  store i64 %call18, i64* %v3, align 8
  %34 = load i64, i64* %v0, align 8
  %35 = load i64, i64* %v3, align 8
  %xor19 = xor i64 %35, %34
  store i64 %xor19, i64* %v3, align 8
  %36 = load i64, i64* %v1, align 8
  %37 = load i64, i64* %v2, align 8
  %add20 = add i64 %37, %36
  store i64 %add20, i64* %v2, align 8
  %38 = load i64, i64* %v1, align 8
  %call21 = call i64 @rotl64(i64 %38, i32 17)
  store i64 %call21, i64* %v1, align 8
  %39 = load i64, i64* %v2, align 8
  %40 = load i64, i64* %v1, align 8
  %xor22 = xor i64 %40, %39
  store i64 %xor22, i64* %v1, align 8
  %41 = load i64, i64* %v2, align 8
  %call23 = call i64 @rotl64(i64 %41, i32 32)
  store i64 %call23, i64* %v2, align 8
  br label %do.end

do.end:                                           ; preds = %do.body
  br label %do.body24

do.body24:                                        ; preds = %do.end
  %42 = load i64, i64* %v1, align 8
  %43 = load i64, i64* %v0, align 8
  %add25 = add i64 %43, %42
  store i64 %add25, i64* %v0, align 8
  %44 = load i64, i64* %v1, align 8
  %call26 = call i64 @rotl64(i64 %44, i32 13)
  store i64 %call26, i64* %v1, align 8
  %45 = load i64, i64* %v0, align 8
  %46 = load i64, i64* %v1, align 8
  %xor27 = xor i64 %46, %45
  store i64 %xor27, i64* %v1, align 8
  %47 = load i64, i64* %v0, align 8
  %call28 = call i64 @rotl64(i64 %47, i32 32)
  store i64 %call28, i64* %v0, align 8
  %48 = load i64, i64* %v3, align 8
  %49 = load i64, i64* %v2, align 8
  %add29 = add i64 %49, %48
  store i64 %add29, i64* %v2, align 8
  %50 = load i64, i64* %v3, align 8
  %call30 = call i64 @rotl64(i64 %50, i32 16)
  store i64 %call30, i64* %v3, align 8
  %51 = load i64, i64* %v2, align 8
  %52 = load i64, i64* %v3, align 8
  %xor31 = xor i64 %52, %51
  store i64 %xor31, i64* %v3, align 8
  %53 = load i64, i64* %v3, align 8
  %54 = load i64, i64* %v0, align 8
  %add32 = add i64 %54, %53
  store i64 %add32, i64* %v0, align 8
  %55 = load i64, i64* %v3, align 8
  %call33 = call i64 @rotl64(i64 %55, i32 21)
  store i64 %call33, i64* %v3, align 8
  %56 = load i64, i64* %v0, align 8
  %57 = load i64, i64* %v3, align 8
  %xor34 = xor i64 %57, %56
  store i64 %xor34, i64* %v3, align 8
  %58 = load i64, i64* %v1, align 8
  %59 = load i64, i64* %v2, align 8
  %add35 = add i64 %59, %58
  store i64 %add35, i64* %v2, align 8
  %60 = load i64, i64* %v1, align 8
  %call36 = call i64 @rotl64(i64 %60, i32 17)
  store i64 %call36, i64* %v1, align 8
  %61 = load i64, i64* %v2, align 8
  %62 = load i64, i64* %v1, align 8
  %xor37 = xor i64 %62, %61
  store i64 %xor37, i64* %v1, align 8
  %63 = load i64, i64* %v2, align 8
  %call38 = call i64 @rotl64(i64 %63, i32 32)
  store i64 %call38, i64* %v2, align 8
  br label %do.end39

do.end39:                                         ; preds = %do.body24
  %64 = load i64, i64* %m, align 8
  %65 = load i64, i64* %v0, align 8
  %xor40 = xor i64 %65, %64
  store i64 %xor40, i64* %v0, align 8
  br label %for.inc

for.inc:                                          ; preds = %do.end39
  %66 = load i8*, i8** %in.addr, align 4
  %add.ptr41 = getelementptr i8, i8* %66, i32 8
  store i8* %add.ptr41, i8** %in.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %67 = load i32, i32* %left, align 4
  switch i32 %67, label %sw.epilog [
    i32 7, label %sw.bb
    i32 6, label %sw.bb44
    i32 5, label %sw.bb49
    i32 4, label %sw.bb54
    i32 3, label %sw.bb59
    i32 2, label %sw.bb64
    i32 1, label %sw.bb69
    i32 0, label %sw.bb73
  ]

sw.bb:                                            ; preds = %for.end
  %68 = load i8*, i8** %in.addr, align 4
  %arrayidx = getelementptr i8, i8* %68, i32 6
  %69 = load i8, i8* %arrayidx, align 1
  %conv42 = zext i8 %69 to i64
  %shl43 = shl i64 %conv42, 48
  %70 = load i64, i64* %b, align 8
  %or = or i64 %70, %shl43
  store i64 %or, i64* %b, align 8
  br label %sw.bb44

sw.bb44:                                          ; preds = %for.end, %sw.bb
  %71 = load i8*, i8** %in.addr, align 4
  %arrayidx45 = getelementptr i8, i8* %71, i32 5
  %72 = load i8, i8* %arrayidx45, align 1
  %conv46 = zext i8 %72 to i64
  %shl47 = shl i64 %conv46, 40
  %73 = load i64, i64* %b, align 8
  %or48 = or i64 %73, %shl47
  store i64 %or48, i64* %b, align 8
  br label %sw.bb49

sw.bb49:                                          ; preds = %for.end, %sw.bb44
  %74 = load i8*, i8** %in.addr, align 4
  %arrayidx50 = getelementptr i8, i8* %74, i32 4
  %75 = load i8, i8* %arrayidx50, align 1
  %conv51 = zext i8 %75 to i64
  %shl52 = shl i64 %conv51, 32
  %76 = load i64, i64* %b, align 8
  %or53 = or i64 %76, %shl52
  store i64 %or53, i64* %b, align 8
  br label %sw.bb54

sw.bb54:                                          ; preds = %for.end, %sw.bb49
  %77 = load i8*, i8** %in.addr, align 4
  %arrayidx55 = getelementptr i8, i8* %77, i32 3
  %78 = load i8, i8* %arrayidx55, align 1
  %conv56 = zext i8 %78 to i64
  %shl57 = shl i64 %conv56, 24
  %79 = load i64, i64* %b, align 8
  %or58 = or i64 %79, %shl57
  store i64 %or58, i64* %b, align 8
  br label %sw.bb59

sw.bb59:                                          ; preds = %for.end, %sw.bb54
  %80 = load i8*, i8** %in.addr, align 4
  %arrayidx60 = getelementptr i8, i8* %80, i32 2
  %81 = load i8, i8* %arrayidx60, align 1
  %conv61 = zext i8 %81 to i64
  %shl62 = shl i64 %conv61, 16
  %82 = load i64, i64* %b, align 8
  %or63 = or i64 %82, %shl62
  store i64 %or63, i64* %b, align 8
  br label %sw.bb64

sw.bb64:                                          ; preds = %for.end, %sw.bb59
  %83 = load i8*, i8** %in.addr, align 4
  %arrayidx65 = getelementptr i8, i8* %83, i32 1
  %84 = load i8, i8* %arrayidx65, align 1
  %conv66 = zext i8 %84 to i64
  %shl67 = shl i64 %conv66, 8
  %85 = load i64, i64* %b, align 8
  %or68 = or i64 %85, %shl67
  store i64 %or68, i64* %b, align 8
  br label %sw.bb69

sw.bb69:                                          ; preds = %for.end, %sw.bb64
  %86 = load i8*, i8** %in.addr, align 4
  %arrayidx70 = getelementptr i8, i8* %86, i32 0
  %87 = load i8, i8* %arrayidx70, align 1
  %conv71 = zext i8 %87 to i64
  %88 = load i64, i64* %b, align 8
  %or72 = or i64 %88, %conv71
  store i64 %or72, i64* %b, align 8
  br label %sw.epilog

sw.bb73:                                          ; preds = %for.end
  br label %sw.epilog

sw.epilog:                                        ; preds = %for.end, %sw.bb73, %sw.bb69
  %89 = load i64, i64* %b, align 8
  %90 = load i64, i64* %v3, align 8
  %xor74 = xor i64 %90, %89
  store i64 %xor74, i64* %v3, align 8
  br label %do.body75

do.body75:                                        ; preds = %sw.epilog
  %91 = load i64, i64* %v1, align 8
  %92 = load i64, i64* %v0, align 8
  %add76 = add i64 %92, %91
  store i64 %add76, i64* %v0, align 8
  %93 = load i64, i64* %v1, align 8
  %call77 = call i64 @rotl64(i64 %93, i32 13)
  store i64 %call77, i64* %v1, align 8
  %94 = load i64, i64* %v0, align 8
  %95 = load i64, i64* %v1, align 8
  %xor78 = xor i64 %95, %94
  store i64 %xor78, i64* %v1, align 8
  %96 = load i64, i64* %v0, align 8
  %call79 = call i64 @rotl64(i64 %96, i32 32)
  store i64 %call79, i64* %v0, align 8
  %97 = load i64, i64* %v3, align 8
  %98 = load i64, i64* %v2, align 8
  %add80 = add i64 %98, %97
  store i64 %add80, i64* %v2, align 8
  %99 = load i64, i64* %v3, align 8
  %call81 = call i64 @rotl64(i64 %99, i32 16)
  store i64 %call81, i64* %v3, align 8
  %100 = load i64, i64* %v2, align 8
  %101 = load i64, i64* %v3, align 8
  %xor82 = xor i64 %101, %100
  store i64 %xor82, i64* %v3, align 8
  %102 = load i64, i64* %v3, align 8
  %103 = load i64, i64* %v0, align 8
  %add83 = add i64 %103, %102
  store i64 %add83, i64* %v0, align 8
  %104 = load i64, i64* %v3, align 8
  %call84 = call i64 @rotl64(i64 %104, i32 21)
  store i64 %call84, i64* %v3, align 8
  %105 = load i64, i64* %v0, align 8
  %106 = load i64, i64* %v3, align 8
  %xor85 = xor i64 %106, %105
  store i64 %xor85, i64* %v3, align 8
  %107 = load i64, i64* %v1, align 8
  %108 = load i64, i64* %v2, align 8
  %add86 = add i64 %108, %107
  store i64 %add86, i64* %v2, align 8
  %109 = load i64, i64* %v1, align 8
  %call87 = call i64 @rotl64(i64 %109, i32 17)
  store i64 %call87, i64* %v1, align 8
  %110 = load i64, i64* %v2, align 8
  %111 = load i64, i64* %v1, align 8
  %xor88 = xor i64 %111, %110
  store i64 %xor88, i64* %v1, align 8
  %112 = load i64, i64* %v2, align 8
  %call89 = call i64 @rotl64(i64 %112, i32 32)
  store i64 %call89, i64* %v2, align 8
  br label %do.end90

do.end90:                                         ; preds = %do.body75
  br label %do.body91

do.body91:                                        ; preds = %do.end90
  %113 = load i64, i64* %v1, align 8
  %114 = load i64, i64* %v0, align 8
  %add92 = add i64 %114, %113
  store i64 %add92, i64* %v0, align 8
  %115 = load i64, i64* %v1, align 8
  %call93 = call i64 @rotl64(i64 %115, i32 13)
  store i64 %call93, i64* %v1, align 8
  %116 = load i64, i64* %v0, align 8
  %117 = load i64, i64* %v1, align 8
  %xor94 = xor i64 %117, %116
  store i64 %xor94, i64* %v1, align 8
  %118 = load i64, i64* %v0, align 8
  %call95 = call i64 @rotl64(i64 %118, i32 32)
  store i64 %call95, i64* %v0, align 8
  %119 = load i64, i64* %v3, align 8
  %120 = load i64, i64* %v2, align 8
  %add96 = add i64 %120, %119
  store i64 %add96, i64* %v2, align 8
  %121 = load i64, i64* %v3, align 8
  %call97 = call i64 @rotl64(i64 %121, i32 16)
  store i64 %call97, i64* %v3, align 8
  %122 = load i64, i64* %v2, align 8
  %123 = load i64, i64* %v3, align 8
  %xor98 = xor i64 %123, %122
  store i64 %xor98, i64* %v3, align 8
  %124 = load i64, i64* %v3, align 8
  %125 = load i64, i64* %v0, align 8
  %add99 = add i64 %125, %124
  store i64 %add99, i64* %v0, align 8
  %126 = load i64, i64* %v3, align 8
  %call100 = call i64 @rotl64(i64 %126, i32 21)
  store i64 %call100, i64* %v3, align 8
  %127 = load i64, i64* %v0, align 8
  %128 = load i64, i64* %v3, align 8
  %xor101 = xor i64 %128, %127
  store i64 %xor101, i64* %v3, align 8
  %129 = load i64, i64* %v1, align 8
  %130 = load i64, i64* %v2, align 8
  %add102 = add i64 %130, %129
  store i64 %add102, i64* %v2, align 8
  %131 = load i64, i64* %v1, align 8
  %call103 = call i64 @rotl64(i64 %131, i32 17)
  store i64 %call103, i64* %v1, align 8
  %132 = load i64, i64* %v2, align 8
  %133 = load i64, i64* %v1, align 8
  %xor104 = xor i64 %133, %132
  store i64 %xor104, i64* %v1, align 8
  %134 = load i64, i64* %v2, align 8
  %call105 = call i64 @rotl64(i64 %134, i32 32)
  store i64 %call105, i64* %v2, align 8
  br label %do.end106

do.end106:                                        ; preds = %do.body91
  %135 = load i64, i64* %b, align 8
  %136 = load i64, i64* %v0, align 8
  %xor107 = xor i64 %136, %135
  store i64 %xor107, i64* %v0, align 8
  %137 = load i64, i64* %v2, align 8
  %xor108 = xor i64 %137, 238
  store i64 %xor108, i64* %v2, align 8
  br label %do.body109

do.body109:                                       ; preds = %do.end106
  %138 = load i64, i64* %v1, align 8
  %139 = load i64, i64* %v0, align 8
  %add110 = add i64 %139, %138
  store i64 %add110, i64* %v0, align 8
  %140 = load i64, i64* %v1, align 8
  %call111 = call i64 @rotl64(i64 %140, i32 13)
  store i64 %call111, i64* %v1, align 8
  %141 = load i64, i64* %v0, align 8
  %142 = load i64, i64* %v1, align 8
  %xor112 = xor i64 %142, %141
  store i64 %xor112, i64* %v1, align 8
  %143 = load i64, i64* %v0, align 8
  %call113 = call i64 @rotl64(i64 %143, i32 32)
  store i64 %call113, i64* %v0, align 8
  %144 = load i64, i64* %v3, align 8
  %145 = load i64, i64* %v2, align 8
  %add114 = add i64 %145, %144
  store i64 %add114, i64* %v2, align 8
  %146 = load i64, i64* %v3, align 8
  %call115 = call i64 @rotl64(i64 %146, i32 16)
  store i64 %call115, i64* %v3, align 8
  %147 = load i64, i64* %v2, align 8
  %148 = load i64, i64* %v3, align 8
  %xor116 = xor i64 %148, %147
  store i64 %xor116, i64* %v3, align 8
  %149 = load i64, i64* %v3, align 8
  %150 = load i64, i64* %v0, align 8
  %add117 = add i64 %150, %149
  store i64 %add117, i64* %v0, align 8
  %151 = load i64, i64* %v3, align 8
  %call118 = call i64 @rotl64(i64 %151, i32 21)
  store i64 %call118, i64* %v3, align 8
  %152 = load i64, i64* %v0, align 8
  %153 = load i64, i64* %v3, align 8
  %xor119 = xor i64 %153, %152
  store i64 %xor119, i64* %v3, align 8
  %154 = load i64, i64* %v1, align 8
  %155 = load i64, i64* %v2, align 8
  %add120 = add i64 %155, %154
  store i64 %add120, i64* %v2, align 8
  %156 = load i64, i64* %v1, align 8
  %call121 = call i64 @rotl64(i64 %156, i32 17)
  store i64 %call121, i64* %v1, align 8
  %157 = load i64, i64* %v2, align 8
  %158 = load i64, i64* %v1, align 8
  %xor122 = xor i64 %158, %157
  store i64 %xor122, i64* %v1, align 8
  %159 = load i64, i64* %v2, align 8
  %call123 = call i64 @rotl64(i64 %159, i32 32)
  store i64 %call123, i64* %v2, align 8
  br label %do.end124

do.end124:                                        ; preds = %do.body109
  br label %do.body125

do.body125:                                       ; preds = %do.end124
  %160 = load i64, i64* %v1, align 8
  %161 = load i64, i64* %v0, align 8
  %add126 = add i64 %161, %160
  store i64 %add126, i64* %v0, align 8
  %162 = load i64, i64* %v1, align 8
  %call127 = call i64 @rotl64(i64 %162, i32 13)
  store i64 %call127, i64* %v1, align 8
  %163 = load i64, i64* %v0, align 8
  %164 = load i64, i64* %v1, align 8
  %xor128 = xor i64 %164, %163
  store i64 %xor128, i64* %v1, align 8
  %165 = load i64, i64* %v0, align 8
  %call129 = call i64 @rotl64(i64 %165, i32 32)
  store i64 %call129, i64* %v0, align 8
  %166 = load i64, i64* %v3, align 8
  %167 = load i64, i64* %v2, align 8
  %add130 = add i64 %167, %166
  store i64 %add130, i64* %v2, align 8
  %168 = load i64, i64* %v3, align 8
  %call131 = call i64 @rotl64(i64 %168, i32 16)
  store i64 %call131, i64* %v3, align 8
  %169 = load i64, i64* %v2, align 8
  %170 = load i64, i64* %v3, align 8
  %xor132 = xor i64 %170, %169
  store i64 %xor132, i64* %v3, align 8
  %171 = load i64, i64* %v3, align 8
  %172 = load i64, i64* %v0, align 8
  %add133 = add i64 %172, %171
  store i64 %add133, i64* %v0, align 8
  %173 = load i64, i64* %v3, align 8
  %call134 = call i64 @rotl64(i64 %173, i32 21)
  store i64 %call134, i64* %v3, align 8
  %174 = load i64, i64* %v0, align 8
  %175 = load i64, i64* %v3, align 8
  %xor135 = xor i64 %175, %174
  store i64 %xor135, i64* %v3, align 8
  %176 = load i64, i64* %v1, align 8
  %177 = load i64, i64* %v2, align 8
  %add136 = add i64 %177, %176
  store i64 %add136, i64* %v2, align 8
  %178 = load i64, i64* %v1, align 8
  %call137 = call i64 @rotl64(i64 %178, i32 17)
  store i64 %call137, i64* %v1, align 8
  %179 = load i64, i64* %v2, align 8
  %180 = load i64, i64* %v1, align 8
  %xor138 = xor i64 %180, %179
  store i64 %xor138, i64* %v1, align 8
  %181 = load i64, i64* %v2, align 8
  %call139 = call i64 @rotl64(i64 %181, i32 32)
  store i64 %call139, i64* %v2, align 8
  br label %do.end140

do.end140:                                        ; preds = %do.body125
  br label %do.body141

do.body141:                                       ; preds = %do.end140
  %182 = load i64, i64* %v1, align 8
  %183 = load i64, i64* %v0, align 8
  %add142 = add i64 %183, %182
  store i64 %add142, i64* %v0, align 8
  %184 = load i64, i64* %v1, align 8
  %call143 = call i64 @rotl64(i64 %184, i32 13)
  store i64 %call143, i64* %v1, align 8
  %185 = load i64, i64* %v0, align 8
  %186 = load i64, i64* %v1, align 8
  %xor144 = xor i64 %186, %185
  store i64 %xor144, i64* %v1, align 8
  %187 = load i64, i64* %v0, align 8
  %call145 = call i64 @rotl64(i64 %187, i32 32)
  store i64 %call145, i64* %v0, align 8
  %188 = load i64, i64* %v3, align 8
  %189 = load i64, i64* %v2, align 8
  %add146 = add i64 %189, %188
  store i64 %add146, i64* %v2, align 8
  %190 = load i64, i64* %v3, align 8
  %call147 = call i64 @rotl64(i64 %190, i32 16)
  store i64 %call147, i64* %v3, align 8
  %191 = load i64, i64* %v2, align 8
  %192 = load i64, i64* %v3, align 8
  %xor148 = xor i64 %192, %191
  store i64 %xor148, i64* %v3, align 8
  %193 = load i64, i64* %v3, align 8
  %194 = load i64, i64* %v0, align 8
  %add149 = add i64 %194, %193
  store i64 %add149, i64* %v0, align 8
  %195 = load i64, i64* %v3, align 8
  %call150 = call i64 @rotl64(i64 %195, i32 21)
  store i64 %call150, i64* %v3, align 8
  %196 = load i64, i64* %v0, align 8
  %197 = load i64, i64* %v3, align 8
  %xor151 = xor i64 %197, %196
  store i64 %xor151, i64* %v3, align 8
  %198 = load i64, i64* %v1, align 8
  %199 = load i64, i64* %v2, align 8
  %add152 = add i64 %199, %198
  store i64 %add152, i64* %v2, align 8
  %200 = load i64, i64* %v1, align 8
  %call153 = call i64 @rotl64(i64 %200, i32 17)
  store i64 %call153, i64* %v1, align 8
  %201 = load i64, i64* %v2, align 8
  %202 = load i64, i64* %v1, align 8
  %xor154 = xor i64 %202, %201
  store i64 %xor154, i64* %v1, align 8
  %203 = load i64, i64* %v2, align 8
  %call155 = call i64 @rotl64(i64 %203, i32 32)
  store i64 %call155, i64* %v2, align 8
  br label %do.end156

do.end156:                                        ; preds = %do.body141
  br label %do.body157

do.body157:                                       ; preds = %do.end156
  %204 = load i64, i64* %v1, align 8
  %205 = load i64, i64* %v0, align 8
  %add158 = add i64 %205, %204
  store i64 %add158, i64* %v0, align 8
  %206 = load i64, i64* %v1, align 8
  %call159 = call i64 @rotl64(i64 %206, i32 13)
  store i64 %call159, i64* %v1, align 8
  %207 = load i64, i64* %v0, align 8
  %208 = load i64, i64* %v1, align 8
  %xor160 = xor i64 %208, %207
  store i64 %xor160, i64* %v1, align 8
  %209 = load i64, i64* %v0, align 8
  %call161 = call i64 @rotl64(i64 %209, i32 32)
  store i64 %call161, i64* %v0, align 8
  %210 = load i64, i64* %v3, align 8
  %211 = load i64, i64* %v2, align 8
  %add162 = add i64 %211, %210
  store i64 %add162, i64* %v2, align 8
  %212 = load i64, i64* %v3, align 8
  %call163 = call i64 @rotl64(i64 %212, i32 16)
  store i64 %call163, i64* %v3, align 8
  %213 = load i64, i64* %v2, align 8
  %214 = load i64, i64* %v3, align 8
  %xor164 = xor i64 %214, %213
  store i64 %xor164, i64* %v3, align 8
  %215 = load i64, i64* %v3, align 8
  %216 = load i64, i64* %v0, align 8
  %add165 = add i64 %216, %215
  store i64 %add165, i64* %v0, align 8
  %217 = load i64, i64* %v3, align 8
  %call166 = call i64 @rotl64(i64 %217, i32 21)
  store i64 %call166, i64* %v3, align 8
  %218 = load i64, i64* %v0, align 8
  %219 = load i64, i64* %v3, align 8
  %xor167 = xor i64 %219, %218
  store i64 %xor167, i64* %v3, align 8
  %220 = load i64, i64* %v1, align 8
  %221 = load i64, i64* %v2, align 8
  %add168 = add i64 %221, %220
  store i64 %add168, i64* %v2, align 8
  %222 = load i64, i64* %v1, align 8
  %call169 = call i64 @rotl64(i64 %222, i32 17)
  store i64 %call169, i64* %v1, align 8
  %223 = load i64, i64* %v2, align 8
  %224 = load i64, i64* %v1, align 8
  %xor170 = xor i64 %224, %223
  store i64 %xor170, i64* %v1, align 8
  %225 = load i64, i64* %v2, align 8
  %call171 = call i64 @rotl64(i64 %225, i32 32)
  store i64 %call171, i64* %v2, align 8
  br label %do.end172

do.end172:                                        ; preds = %do.body157
  %226 = load i64, i64* %v0, align 8
  %227 = load i64, i64* %v1, align 8
  %xor173 = xor i64 %226, %227
  %228 = load i64, i64* %v2, align 8
  %xor174 = xor i64 %xor173, %228
  %229 = load i64, i64* %v3, align 8
  %xor175 = xor i64 %xor174, %229
  store i64 %xor175, i64* %b, align 8
  %230 = load i8*, i8** %out.addr, align 4
  %231 = load i64, i64* %b, align 8
  call void @store64_le(i8* %230, i64 %231)
  %232 = load i64, i64* %v1, align 8
  %xor176 = xor i64 %232, 221
  store i64 %xor176, i64* %v1, align 8
  br label %do.body177

do.body177:                                       ; preds = %do.end172
  %233 = load i64, i64* %v1, align 8
  %234 = load i64, i64* %v0, align 8
  %add178 = add i64 %234, %233
  store i64 %add178, i64* %v0, align 8
  %235 = load i64, i64* %v1, align 8
  %call179 = call i64 @rotl64(i64 %235, i32 13)
  store i64 %call179, i64* %v1, align 8
  %236 = load i64, i64* %v0, align 8
  %237 = load i64, i64* %v1, align 8
  %xor180 = xor i64 %237, %236
  store i64 %xor180, i64* %v1, align 8
  %238 = load i64, i64* %v0, align 8
  %call181 = call i64 @rotl64(i64 %238, i32 32)
  store i64 %call181, i64* %v0, align 8
  %239 = load i64, i64* %v3, align 8
  %240 = load i64, i64* %v2, align 8
  %add182 = add i64 %240, %239
  store i64 %add182, i64* %v2, align 8
  %241 = load i64, i64* %v3, align 8
  %call183 = call i64 @rotl64(i64 %241, i32 16)
  store i64 %call183, i64* %v3, align 8
  %242 = load i64, i64* %v2, align 8
  %243 = load i64, i64* %v3, align 8
  %xor184 = xor i64 %243, %242
  store i64 %xor184, i64* %v3, align 8
  %244 = load i64, i64* %v3, align 8
  %245 = load i64, i64* %v0, align 8
  %add185 = add i64 %245, %244
  store i64 %add185, i64* %v0, align 8
  %246 = load i64, i64* %v3, align 8
  %call186 = call i64 @rotl64(i64 %246, i32 21)
  store i64 %call186, i64* %v3, align 8
  %247 = load i64, i64* %v0, align 8
  %248 = load i64, i64* %v3, align 8
  %xor187 = xor i64 %248, %247
  store i64 %xor187, i64* %v3, align 8
  %249 = load i64, i64* %v1, align 8
  %250 = load i64, i64* %v2, align 8
  %add188 = add i64 %250, %249
  store i64 %add188, i64* %v2, align 8
  %251 = load i64, i64* %v1, align 8
  %call189 = call i64 @rotl64(i64 %251, i32 17)
  store i64 %call189, i64* %v1, align 8
  %252 = load i64, i64* %v2, align 8
  %253 = load i64, i64* %v1, align 8
  %xor190 = xor i64 %253, %252
  store i64 %xor190, i64* %v1, align 8
  %254 = load i64, i64* %v2, align 8
  %call191 = call i64 @rotl64(i64 %254, i32 32)
  store i64 %call191, i64* %v2, align 8
  br label %do.end192

do.end192:                                        ; preds = %do.body177
  br label %do.body193

do.body193:                                       ; preds = %do.end192
  %255 = load i64, i64* %v1, align 8
  %256 = load i64, i64* %v0, align 8
  %add194 = add i64 %256, %255
  store i64 %add194, i64* %v0, align 8
  %257 = load i64, i64* %v1, align 8
  %call195 = call i64 @rotl64(i64 %257, i32 13)
  store i64 %call195, i64* %v1, align 8
  %258 = load i64, i64* %v0, align 8
  %259 = load i64, i64* %v1, align 8
  %xor196 = xor i64 %259, %258
  store i64 %xor196, i64* %v1, align 8
  %260 = load i64, i64* %v0, align 8
  %call197 = call i64 @rotl64(i64 %260, i32 32)
  store i64 %call197, i64* %v0, align 8
  %261 = load i64, i64* %v3, align 8
  %262 = load i64, i64* %v2, align 8
  %add198 = add i64 %262, %261
  store i64 %add198, i64* %v2, align 8
  %263 = load i64, i64* %v3, align 8
  %call199 = call i64 @rotl64(i64 %263, i32 16)
  store i64 %call199, i64* %v3, align 8
  %264 = load i64, i64* %v2, align 8
  %265 = load i64, i64* %v3, align 8
  %xor200 = xor i64 %265, %264
  store i64 %xor200, i64* %v3, align 8
  %266 = load i64, i64* %v3, align 8
  %267 = load i64, i64* %v0, align 8
  %add201 = add i64 %267, %266
  store i64 %add201, i64* %v0, align 8
  %268 = load i64, i64* %v3, align 8
  %call202 = call i64 @rotl64(i64 %268, i32 21)
  store i64 %call202, i64* %v3, align 8
  %269 = load i64, i64* %v0, align 8
  %270 = load i64, i64* %v3, align 8
  %xor203 = xor i64 %270, %269
  store i64 %xor203, i64* %v3, align 8
  %271 = load i64, i64* %v1, align 8
  %272 = load i64, i64* %v2, align 8
  %add204 = add i64 %272, %271
  store i64 %add204, i64* %v2, align 8
  %273 = load i64, i64* %v1, align 8
  %call205 = call i64 @rotl64(i64 %273, i32 17)
  store i64 %call205, i64* %v1, align 8
  %274 = load i64, i64* %v2, align 8
  %275 = load i64, i64* %v1, align 8
  %xor206 = xor i64 %275, %274
  store i64 %xor206, i64* %v1, align 8
  %276 = load i64, i64* %v2, align 8
  %call207 = call i64 @rotl64(i64 %276, i32 32)
  store i64 %call207, i64* %v2, align 8
  br label %do.end208

do.end208:                                        ; preds = %do.body193
  br label %do.body209

do.body209:                                       ; preds = %do.end208
  %277 = load i64, i64* %v1, align 8
  %278 = load i64, i64* %v0, align 8
  %add210 = add i64 %278, %277
  store i64 %add210, i64* %v0, align 8
  %279 = load i64, i64* %v1, align 8
  %call211 = call i64 @rotl64(i64 %279, i32 13)
  store i64 %call211, i64* %v1, align 8
  %280 = load i64, i64* %v0, align 8
  %281 = load i64, i64* %v1, align 8
  %xor212 = xor i64 %281, %280
  store i64 %xor212, i64* %v1, align 8
  %282 = load i64, i64* %v0, align 8
  %call213 = call i64 @rotl64(i64 %282, i32 32)
  store i64 %call213, i64* %v0, align 8
  %283 = load i64, i64* %v3, align 8
  %284 = load i64, i64* %v2, align 8
  %add214 = add i64 %284, %283
  store i64 %add214, i64* %v2, align 8
  %285 = load i64, i64* %v3, align 8
  %call215 = call i64 @rotl64(i64 %285, i32 16)
  store i64 %call215, i64* %v3, align 8
  %286 = load i64, i64* %v2, align 8
  %287 = load i64, i64* %v3, align 8
  %xor216 = xor i64 %287, %286
  store i64 %xor216, i64* %v3, align 8
  %288 = load i64, i64* %v3, align 8
  %289 = load i64, i64* %v0, align 8
  %add217 = add i64 %289, %288
  store i64 %add217, i64* %v0, align 8
  %290 = load i64, i64* %v3, align 8
  %call218 = call i64 @rotl64(i64 %290, i32 21)
  store i64 %call218, i64* %v3, align 8
  %291 = load i64, i64* %v0, align 8
  %292 = load i64, i64* %v3, align 8
  %xor219 = xor i64 %292, %291
  store i64 %xor219, i64* %v3, align 8
  %293 = load i64, i64* %v1, align 8
  %294 = load i64, i64* %v2, align 8
  %add220 = add i64 %294, %293
  store i64 %add220, i64* %v2, align 8
  %295 = load i64, i64* %v1, align 8
  %call221 = call i64 @rotl64(i64 %295, i32 17)
  store i64 %call221, i64* %v1, align 8
  %296 = load i64, i64* %v2, align 8
  %297 = load i64, i64* %v1, align 8
  %xor222 = xor i64 %297, %296
  store i64 %xor222, i64* %v1, align 8
  %298 = load i64, i64* %v2, align 8
  %call223 = call i64 @rotl64(i64 %298, i32 32)
  store i64 %call223, i64* %v2, align 8
  br label %do.end224

do.end224:                                        ; preds = %do.body209
  br label %do.body225

do.body225:                                       ; preds = %do.end224
  %299 = load i64, i64* %v1, align 8
  %300 = load i64, i64* %v0, align 8
  %add226 = add i64 %300, %299
  store i64 %add226, i64* %v0, align 8
  %301 = load i64, i64* %v1, align 8
  %call227 = call i64 @rotl64(i64 %301, i32 13)
  store i64 %call227, i64* %v1, align 8
  %302 = load i64, i64* %v0, align 8
  %303 = load i64, i64* %v1, align 8
  %xor228 = xor i64 %303, %302
  store i64 %xor228, i64* %v1, align 8
  %304 = load i64, i64* %v0, align 8
  %call229 = call i64 @rotl64(i64 %304, i32 32)
  store i64 %call229, i64* %v0, align 8
  %305 = load i64, i64* %v3, align 8
  %306 = load i64, i64* %v2, align 8
  %add230 = add i64 %306, %305
  store i64 %add230, i64* %v2, align 8
  %307 = load i64, i64* %v3, align 8
  %call231 = call i64 @rotl64(i64 %307, i32 16)
  store i64 %call231, i64* %v3, align 8
  %308 = load i64, i64* %v2, align 8
  %309 = load i64, i64* %v3, align 8
  %xor232 = xor i64 %309, %308
  store i64 %xor232, i64* %v3, align 8
  %310 = load i64, i64* %v3, align 8
  %311 = load i64, i64* %v0, align 8
  %add233 = add i64 %311, %310
  store i64 %add233, i64* %v0, align 8
  %312 = load i64, i64* %v3, align 8
  %call234 = call i64 @rotl64(i64 %312, i32 21)
  store i64 %call234, i64* %v3, align 8
  %313 = load i64, i64* %v0, align 8
  %314 = load i64, i64* %v3, align 8
  %xor235 = xor i64 %314, %313
  store i64 %xor235, i64* %v3, align 8
  %315 = load i64, i64* %v1, align 8
  %316 = load i64, i64* %v2, align 8
  %add236 = add i64 %316, %315
  store i64 %add236, i64* %v2, align 8
  %317 = load i64, i64* %v1, align 8
  %call237 = call i64 @rotl64(i64 %317, i32 17)
  store i64 %call237, i64* %v1, align 8
  %318 = load i64, i64* %v2, align 8
  %319 = load i64, i64* %v1, align 8
  %xor238 = xor i64 %319, %318
  store i64 %xor238, i64* %v1, align 8
  %320 = load i64, i64* %v2, align 8
  %call239 = call i64 @rotl64(i64 %320, i32 32)
  store i64 %call239, i64* %v2, align 8
  br label %do.end240

do.end240:                                        ; preds = %do.body225
  %321 = load i64, i64* %v0, align 8
  %322 = load i64, i64* %v1, align 8
  %xor241 = xor i64 %321, %322
  %323 = load i64, i64* %v2, align 8
  %xor242 = xor i64 %xor241, %323
  %324 = load i64, i64* %v3, align 8
  %xor243 = xor i64 %xor242, %324
  store i64 %xor243, i64* %b, align 8
  %325 = load i8*, i8** %out.addr, align 4
  %add.ptr244 = getelementptr i8, i8* %325, i32 8
  %326 = load i64, i64* %b, align 8
  call void @store64_le(i8* %add.ptr244, i64 %326)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i64* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 1 %1, i32 8, i1 false)
  %2 = load i64, i64* %w, align 8
  ret i64 %2
}

; Function Attrs: noinline nounwind optnone
define internal i64 @rotl64(i64 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i64, align 8
  %b.addr = alloca i32, align 4
  store i64 %x, i64* %x.addr, align 8
  store i32 %b, i32* %b.addr, align 4
  %0 = load i64, i64* %x.addr, align 8
  %1 = load i32, i32* %b.addr, align 4
  %sh_prom = zext i32 %1 to i64
  %shl = shl i64 %0, %sh_prom
  %2 = load i64, i64* %x.addr, align 8
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shr = lshr i64 %2, %sh_prom1
  %or = or i64 %shl, %shr
  ret i64 %or
}

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
