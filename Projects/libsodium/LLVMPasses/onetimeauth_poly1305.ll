; ModuleID = 'crypto_onetimeauth/poly1305/onetimeauth_poly1305.c'
source_filename = "crypto_onetimeauth/poly1305/onetimeauth_poly1305.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_onetimeauth_poly1305_implementation = type { i32 (i8*, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*, i64)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)* }
%struct.crypto_onetimeauth_poly1305_state = type { [256 x i8] }

@implementation = internal global %struct.crypto_onetimeauth_poly1305_implementation* @crypto_onetimeauth_poly1305_donna_implementation, align 4
@crypto_onetimeauth_poly1305_donna_implementation = external global %struct.crypto_onetimeauth_poly1305_implementation, align 4

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305(i8* nonnull %out, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_implementation*, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  %onetimeauth = getelementptr inbounds %struct.crypto_onetimeauth_poly1305_implementation, %struct.crypto_onetimeauth_poly1305_implementation* %0, i32 0, i32 0
  %1 = load i32 (i8*, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*)** %onetimeauth, align 4
  %2 = load i8*, i8** %out.addr, align 4
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 %1(i8* %2, i8* %3, i64 %4, i8* %5)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_verify(i8* nonnull %h, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %h.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  store i8* %h, i8** %h.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_implementation*, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  %onetimeauth_verify = getelementptr inbounds %struct.crypto_onetimeauth_poly1305_implementation, %struct.crypto_onetimeauth_poly1305_implementation* %0, i32 0, i32 1
  %1 = load i32 (i8*, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*)** %onetimeauth_verify, align 4
  %2 = load i8*, i8** %h.addr, align 4
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 %1(i8* %2, i8* %3, i64 %4, i8* %5)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* nonnull %state, i8* nonnull %key) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %key.addr = alloca i8*, align 4
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_implementation*, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  %onetimeauth_init = getelementptr inbounds %struct.crypto_onetimeauth_poly1305_implementation, %struct.crypto_onetimeauth_poly1305_implementation* %0, i32 0, i32 2
  %1 = load i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)** %onetimeauth_init, align 4
  %2 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %3 = load i8*, i8** %key.addr, align 4
  %call = call i32 %1(%struct.crypto_onetimeauth_poly1305_state* %2, i8* %3)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_onetimeauth_poly1305_implementation*, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  %onetimeauth_update = getelementptr inbounds %struct.crypto_onetimeauth_poly1305_implementation, %struct.crypto_onetimeauth_poly1305_implementation* %0, i32 0, i32 3
  %1 = load i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*, i64)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*, i64)** %onetimeauth_update, align 4
  %2 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %3 = load i8*, i8** %in.addr, align 4
  %4 = load i64, i64* %inlen.addr, align 8
  %call = call i32 %1(%struct.crypto_onetimeauth_poly1305_state* %2, i8* %3, i64 %4)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* nonnull %state, i8* nonnull %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_onetimeauth_poly1305_state*, align 4
  %out.addr = alloca i8*, align 4
  store %struct.crypto_onetimeauth_poly1305_state* %state, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_onetimeauth_poly1305_implementation*, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  %onetimeauth_final = getelementptr inbounds %struct.crypto_onetimeauth_poly1305_implementation, %struct.crypto_onetimeauth_poly1305_implementation* %0, i32 0, i32 4
  %1 = load i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)*, i32 (%struct.crypto_onetimeauth_poly1305_state*, i8*)** %onetimeauth_final, align 4
  %2 = load %struct.crypto_onetimeauth_poly1305_state*, %struct.crypto_onetimeauth_poly1305_state** %state.addr, align 4
  %3 = load i8*, i8** %out.addr, align 4
  %call = call i32 %1(%struct.crypto_onetimeauth_poly1305_state* %2, i8* %3)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_bytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_onetimeauth_poly1305_statebytes() #0 {
entry:
  ret i32 256
}

; Function Attrs: noinline nounwind optnone
define void @crypto_onetimeauth_poly1305_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_onetimeauth_poly1305_pick_best_implementation() #0 {
entry:
  store %struct.crypto_onetimeauth_poly1305_implementation* @crypto_onetimeauth_poly1305_donna_implementation, %struct.crypto_onetimeauth_poly1305_implementation** @implementation, align 4
  ret i32 0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
