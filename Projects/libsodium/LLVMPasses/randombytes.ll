; ModuleID = 'randombytes/randombytes.c'
source_filename = "randombytes/randombytes.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.randombytes_implementation = type { i8* ()*, i32 ()*, void ()*, i32 (i32)*, void (i8*, i32)*, i32 ()* }

@implementation = internal global %struct.randombytes_implementation* null, align 4
@.str = private unnamed_addr constant [3 x i8] c"js\00", align 1
@.str.1 = private unnamed_addr constant [38 x i8] c"\22{ return Module.getRandomValue(); }\22\00", align 1
@.str.2 = private unnamed_addr constant [722 x i8] c"{ if (Module.getRandomValue === undefined) { try { var window_ = 'object' === typeof window ? window : self; var crypto_ = typeof window_.crypto !== 'undefined' ? window_.crypto : window_.msCrypto; var randomValuesStandard = function() { var buf = new Uint32Array(1); crypto_.getRandomValues(buf); return buf[0] >>> 0; }; randomValuesStandard(); Module.getRandomValue = randomValuesStandard; } catch (e) { try { var crypto = require('crypto'); var randomValueNodeJS = function() { var buf = crypto['randomBytes'](4); return (buf[0] << 24 | buf[1] << 16 | buf[2] << 8 | buf[3]) >>> 0; }; randomValueNodeJS(); Module.getRandomValue = randomValueNodeJS; } catch (e) { throw 'No secure random number generator found'; } } } }\00", align 1
@randombytes_buf_deterministic.nonce = internal constant [12 x i8] c"LibsodiumDRG", align 1
@.str.3 = private unnamed_addr constant [20 x i8] c"buf_len <= SIZE_MAX\00", align 1
@.str.4 = private unnamed_addr constant [26 x i8] c"randombytes/randombytes.c\00", align 1
@__func__.randombytes = private unnamed_addr constant [12 x i8] c"randombytes\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @randombytes_set_implementation(%struct.randombytes_implementation* nonnull %impl) #0 {
entry:
  %impl.addr = alloca %struct.randombytes_implementation*, align 4
  store %struct.randombytes_implementation* %impl, %struct.randombytes_implementation** %impl.addr, align 4
  %0 = load %struct.randombytes_implementation*, %struct.randombytes_implementation** %impl.addr, align 4
  store %struct.randombytes_implementation* %0, %struct.randombytes_implementation** @implementation, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i8* @randombytes_implementation_name() #0 {
entry:
  ret i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @randombytes_random() #0 {
entry:
  %.compoundliteral = alloca [1 x i8], align 1
  %arrayinit.begin = getelementptr inbounds [1 x i8], [1 x i8]* %.compoundliteral, i32 0, i32 0
  store i8 0, i8* %arrayinit.begin, align 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %.compoundliteral, i32 0, i32 0
  %call = call i32 (i8*, i8*, ...) @emscripten_asm_const_int(i8* getelementptr inbounds ([38 x i8], [38 x i8]* @.str.1, i32 0, i32 0), i8* %arraydecay) #4
  ret i32 %call
}

; Function Attrs: nounwind
declare i32 @emscripten_asm_const_int(i8*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define void @randombytes_stir() #0 {
entry:
  %.compoundliteral = alloca [1 x i8], align 1
  %arrayinit.begin = getelementptr inbounds [1 x i8], [1 x i8]* %.compoundliteral, i32 0, i32 0
  store i8 0, i8* %arrayinit.begin, align 1
  %arraydecay = getelementptr inbounds [1 x i8], [1 x i8]* %.compoundliteral, i32 0, i32 0
  %call = call i32 (i8*, i8*, ...) @emscripten_asm_const_int(i8* getelementptr inbounds ([722 x i8], [722 x i8]* @.str.2, i32 0, i32 0), i8* %arraydecay) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define i32 @randombytes_uniform(i32 %upper_bound) #0 {
entry:
  %retval = alloca i32, align 4
  %upper_bound.addr = alloca i32, align 4
  %min = alloca i32, align 4
  %r = alloca i32, align 4
  store i32 %upper_bound, i32* %upper_bound.addr, align 4
  %0 = load i32, i32* %upper_bound.addr, align 4
  %cmp = icmp ult i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %upper_bound.addr, align 4
  %neg = xor i32 %1, -1
  %add = add i32 1, %neg
  %2 = load i32, i32* %upper_bound.addr, align 4
  %rem = urem i32 %add, %2
  store i32 %rem, i32* %min, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end
  %call = call i32 @randombytes_random()
  store i32 %call, i32* %r, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %3 = load i32, i32* %r, align 4
  %4 = load i32, i32* %min, align 4
  %cmp1 = icmp ult i32 %3, %4
  br i1 %cmp1, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %5 = load i32, i32* %r, align 4
  %6 = load i32, i32* %upper_bound.addr, align 4
  %rem2 = urem i32 %5, %6
  store i32 %rem2, i32* %retval, align 4
  br label %return

return:                                           ; preds = %do.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define void @randombytes_buf(i8* nonnull %buf, i32 %size) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i8*, i8** %buf.addr, align 4
  store i8* %0, i8** %p, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %size.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call = call i32 @randombytes_random()
  %conv = trunc i32 %call to i8
  %3 = load i8*, i8** %p, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 %4
  store i8 %conv, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @randombytes_buf_deterministic(i8* nonnull %buf, i32 %size, i8* nonnull %seed) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %seed.addr = alloca i8*, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %seed, i8** %seed.addr, align 4
  %0 = load i8*, i8** %buf.addr, align 4
  %1 = load i32, i32* %size.addr, align 4
  %conv = zext i32 %1 to i64
  %2 = load i8*, i8** %seed.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf(i8* %0, i64 %conv, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @randombytes_buf_deterministic.nonce, i32 0, i32 0), i8* %2)
  ret void
}

declare i32 @crypto_stream_chacha20_ietf(i8*, i64, i8*, i8*) #2

; Function Attrs: noinline nounwind optnone
define i32 @randombytes_seedbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @randombytes_close() #0 {
entry:
  %retval = alloca i32, align 4
  %0 = load %struct.randombytes_implementation*, %struct.randombytes_implementation** @implementation, align 4
  %cmp = icmp ne %struct.randombytes_implementation* %0, null
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.randombytes_implementation*, %struct.randombytes_implementation** @implementation, align 4
  %close = getelementptr inbounds %struct.randombytes_implementation, %struct.randombytes_implementation* %1, i32 0, i32 5
  %2 = load i32 ()*, i32 ()** %close, align 4
  %cmp1 = icmp ne i32 ()* %2, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.randombytes_implementation*, %struct.randombytes_implementation** @implementation, align 4
  %close2 = getelementptr inbounds %struct.randombytes_implementation, %struct.randombytes_implementation* %3, i32 0, i32 5
  %4 = load i32 ()*, i32 ()** %close2, align 4
  %call = call i32 %4()
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define void @randombytes(i8* nonnull %buf, i64 %buf_len) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %buf_len.addr = alloca i64, align 8
  store i8* %buf, i8** %buf.addr, align 4
  store i64 %buf_len, i64* %buf_len.addr, align 8
  %0 = load i64, i64* %buf_len.addr, align 8
  %cmp = icmp ule i64 %0, 4294967295
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  call void @__assert_fail(i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.3, i32 0, i32 0), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.4, i32 0, i32 0), i32 197, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @__func__.randombytes, i32 0, i32 0)) #5
  unreachable

1:                                                ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %1, %entry
  %2 = phi i1 [ true, %entry ], [ false, %1 ]
  %lor.ext = zext i1 %2 to i32
  %3 = load i8*, i8** %buf.addr, align 4
  %4 = load i64, i64* %buf_len.addr, align 8
  %conv = trunc i64 %4 to i32
  call void @randombytes_buf(i8* %3, i32 %conv)
  ret void
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
