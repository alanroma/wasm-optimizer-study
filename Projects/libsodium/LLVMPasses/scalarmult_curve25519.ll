; ModuleID = 'crypto_scalarmult/curve25519/scalarmult_curve25519.c'
source_filename = "crypto_scalarmult/curve25519/scalarmult_curve25519.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_scalarmult_curve25519_implementation = type { i32 (i8*, i8*, i8*)*, i32 (i8*, i8*)* }

@implementation = internal global %struct.crypto_scalarmult_curve25519_implementation* @crypto_scalarmult_curve25519_ref10_implementation, align 4
@crypto_scalarmult_curve25519_ref10_implementation = external global %struct.crypto_scalarmult_curve25519_implementation, align 4

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_curve25519(i8* nonnull %q, i8* nonnull %n, i8* nonnull %p) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %d = alloca i8, align 1
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store volatile i8 0, i8* %d, align 1
  %0 = load %struct.crypto_scalarmult_curve25519_implementation*, %struct.crypto_scalarmult_curve25519_implementation** @implementation, align 4
  %mult = getelementptr inbounds %struct.crypto_scalarmult_curve25519_implementation, %struct.crypto_scalarmult_curve25519_implementation* %0, i32 0, i32 0
  %1 = load i32 (i8*, i8*, i8*)*, i32 (i8*, i8*, i8*)** %mult, align 4
  %2 = load i8*, i8** %q.addr, align 4
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i8*, i8** %p.addr, align 4
  %call = call i32 %1(i8* %2, i8* %3, i8* %4)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4
  %cmp1 = icmp ult i32 %5, 32
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load i8*, i8** %q.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %8 to i32
  %9 = load volatile i8, i8* %d, align 1
  %conv2 = zext i8 %9 to i32
  %or = or i32 %conv2, %conv
  %conv3 = trunc i32 %or to i8
  store volatile i8 %conv3, i8* %d, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load volatile i8, i8* %d, align 1
  %conv4 = zext i8 %11 to i32
  %sub = sub i32 %conv4, 1
  %shr = ashr i32 %sub, 8
  %and = and i32 1, %shr
  %sub5 = sub i32 0, %and
  store i32 %sub5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_curve25519_base(i8* nonnull %q, i8* nonnull %n) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i32 (i8*, i8*)*, i32 (i8*, i8*)** getelementptr inbounds (%struct.crypto_scalarmult_curve25519_implementation, %struct.crypto_scalarmult_curve25519_implementation* @crypto_scalarmult_curve25519_ref10_implementation, i32 0, i32 1), align 4
  %1 = load i8*, i8** %q.addr, align 4
  %2 = load i8*, i8** %n.addr, align 4
  %call = call i32 %0(i8* %1, i8* %2)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_curve25519_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_scalarmult_curve25519_scalarbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_scalarmult_curve25519_pick_best_implementation() #0 {
entry:
  store %struct.crypto_scalarmult_curve25519_implementation* @crypto_scalarmult_curve25519_ref10_implementation, %struct.crypto_scalarmult_curve25519_implementation** @implementation, align 4
  ret i32 0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
