; ModuleID = 'crypto_hash/sha256/cp/hash_sha256_cp.c'
source_filename = "crypto_hash/sha256/cp/hash_sha256_cp.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_hash_sha256_state = type { [8 x i32], i64, [64 x i8] }

@crypto_hash_sha256_init.sha256_initial_state = internal constant [8 x i32] [i32 1779033703, i32 -1150833019, i32 1013904242, i32 -1521486534, i32 1359893119, i32 -1694144372, i32 528734635, i32 1541459225], align 16
@Krnd = internal constant [64 x i32] [i32 1116352408, i32 1899447441, i32 -1245643825, i32 -373957723, i32 961987163, i32 1508970993, i32 -1841331548, i32 -1424204075, i32 -670586216, i32 310598401, i32 607225278, i32 1426881987, i32 1925078388, i32 -2132889090, i32 -1680079193, i32 -1046744716, i32 -459576895, i32 -272742522, i32 264347078, i32 604807628, i32 770255983, i32 1249150122, i32 1555081692, i32 1996064986, i32 -1740746414, i32 -1473132947, i32 -1341970488, i32 -1084653625, i32 -958395405, i32 -710438585, i32 113926993, i32 338241895, i32 666307205, i32 773529912, i32 1294757372, i32 1396182291, i32 1695183700, i32 1986661051, i32 -2117940946, i32 -1838011259, i32 -1564481375, i32 -1474664885, i32 -1035236496, i32 -949202525, i32 -778901479, i32 -694614492, i32 -200395387, i32 275423344, i32 430227734, i32 506948616, i32 659060556, i32 883997877, i32 958139571, i32 1322822218, i32 1537002063, i32 1747873779, i32 1955562222, i32 2024104815, i32 -2067236844, i32 -1933114872, i32 -1866530822, i32 -1538233109, i32 -1090935817, i32 -965641998], align 16
@PAD = internal constant <{ i8, [63 x i8] }> <{ i8 -128, [63 x i8] zeroinitializer }>, align 16

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha256_init(%struct.crypto_hash_sha256_state* nonnull %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha256_state*, align 4
  store %struct.crypto_hash_sha256_state* %state, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %0 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %0, i32 0, i32 1
  store i64 0, i64* %count, align 8
  %1 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state1 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %1, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %state1, i32 0, i32 0
  %2 = bitcast i32* %arraydecay to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 16 bitcast ([8 x i32]* @crypto_hash_sha256_init.sha256_initial_state to i8*), i32 32, i1 false)
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha256_update(%struct.crypto_hash_sha256_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %retval = alloca i32, align 4
  %state.addr = alloca %struct.crypto_hash_sha256_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %tmp32 = alloca [72 x i32], align 16
  %i = alloca i64, align 8
  %r = alloca i64, align 8
  store %struct.crypto_hash_sha256_state* %state, %struct.crypto_hash_sha256_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load i64, i64* %inlen.addr, align 8
  %cmp = icmp ule i64 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %1, i32 0, i32 1
  %2 = load i64, i64* %count, align 8
  %shr = lshr i64 %2, 3
  %and = and i64 %shr, 63
  store i64 %and, i64* %r, align 8
  %3 = load i64, i64* %inlen.addr, align 8
  %shl = shl i64 %3, 3
  %4 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %count1 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %4, i32 0, i32 1
  %5 = load i64, i64* %count1, align 8
  %add = add i64 %5, %shl
  store i64 %add, i64* %count1, align 8
  %6 = load i64, i64* %inlen.addr, align 8
  %7 = load i64, i64* %r, align 8
  %sub = sub i64 64, %7
  %cmp2 = icmp ult i64 %6, %sub
  br i1 %cmp2, label %if.then3, label %if.end8

if.then3:                                         ; preds = %if.end
  store i64 0, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %8 = load i64, i64* %i, align 8
  %9 = load i64, i64* %inlen.addr, align 8
  %cmp4 = icmp ult i64 %8, %9
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8*, i8** %in.addr, align 4
  %11 = load i64, i64* %i, align 8
  %idxprom = trunc i64 %11 to i32
  %arrayidx = getelementptr i8, i8* %10, i32 %idxprom
  %12 = load i8, i8* %arrayidx, align 1
  %13 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %13, i32 0, i32 2
  %14 = load i64, i64* %r, align 8
  %15 = load i64, i64* %i, align 8
  %add5 = add i64 %14, %15
  %idxprom6 = trunc i64 %add5 to i32
  %arrayidx7 = getelementptr [64 x i8], [64 x i8]* %buf, i32 0, i32 %idxprom6
  store i8 %12, i8* %arrayidx7, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i64, i64* %i, align 8
  %inc = add i64 %16, 1
  store i64 %inc, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end
  store i64 0, i64* %i, align 8
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc19, %if.end8
  %17 = load i64, i64* %i, align 8
  %18 = load i64, i64* %r, align 8
  %sub10 = sub i64 64, %18
  %cmp11 = icmp ult i64 %17, %sub10
  br i1 %cmp11, label %for.body12, label %for.end21

for.body12:                                       ; preds = %for.cond9
  %19 = load i8*, i8** %in.addr, align 4
  %20 = load i64, i64* %i, align 8
  %idxprom13 = trunc i64 %20 to i32
  %arrayidx14 = getelementptr i8, i8* %19, i32 %idxprom13
  %21 = load i8, i8* %arrayidx14, align 1
  %22 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf15 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %22, i32 0, i32 2
  %23 = load i64, i64* %r, align 8
  %24 = load i64, i64* %i, align 8
  %add16 = add i64 %23, %24
  %idxprom17 = trunc i64 %add16 to i32
  %arrayidx18 = getelementptr [64 x i8], [64 x i8]* %buf15, i32 0, i32 %idxprom17
  store i8 %21, i8* %arrayidx18, align 1
  br label %for.inc19

for.inc19:                                        ; preds = %for.body12
  %25 = load i64, i64* %i, align 8
  %inc20 = add i64 %25, 1
  store i64 %inc20, i64* %i, align 8
  br label %for.cond9

for.end21:                                        ; preds = %for.cond9
  %26 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state22 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %26, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %state22, i32 0, i32 0
  %27 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf23 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %27, i32 0, i32 2
  %arraydecay24 = getelementptr inbounds [64 x i8], [64 x i8]* %buf23, i32 0, i32 0
  %arrayidx25 = getelementptr [72 x i32], [72 x i32]* %tmp32, i32 0, i32 0
  %arrayidx26 = getelementptr [72 x i32], [72 x i32]* %tmp32, i32 0, i32 64
  call void @SHA256_Transform(i32* %arraydecay, i8* %arraydecay24, i32* %arrayidx25, i32* %arrayidx26)
  %28 = load i64, i64* %r, align 8
  %sub27 = sub i64 64, %28
  %29 = load i8*, i8** %in.addr, align 4
  %idx.ext = trunc i64 %sub27 to i32
  %add.ptr = getelementptr i8, i8* %29, i32 %idx.ext
  store i8* %add.ptr, i8** %in.addr, align 4
  %30 = load i64, i64* %r, align 8
  %sub28 = sub i64 64, %30
  %31 = load i64, i64* %inlen.addr, align 8
  %sub29 = sub i64 %31, %sub28
  store i64 %sub29, i64* %inlen.addr, align 8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.end21
  %32 = load i64, i64* %inlen.addr, align 8
  %cmp30 = icmp uge i64 %32, 64
  br i1 %cmp30, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state31 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %33, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [8 x i32], [8 x i32]* %state31, i32 0, i32 0
  %34 = load i8*, i8** %in.addr, align 4
  %arrayidx33 = getelementptr [72 x i32], [72 x i32]* %tmp32, i32 0, i32 0
  %arrayidx34 = getelementptr [72 x i32], [72 x i32]* %tmp32, i32 0, i32 64
  call void @SHA256_Transform(i32* %arraydecay32, i8* %34, i32* %arrayidx33, i32* %arrayidx34)
  %35 = load i8*, i8** %in.addr, align 4
  %add.ptr35 = getelementptr i8, i8* %35, i32 64
  store i8* %add.ptr35, i8** %in.addr, align 4
  %36 = load i64, i64* %inlen.addr, align 8
  %sub36 = sub i64 %36, 64
  store i64 %sub36, i64* %inlen.addr, align 8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %37 = load i64, i64* %inlen.addr, align 8
  %and37 = and i64 %37, 63
  store i64 %and37, i64* %inlen.addr, align 8
  store i64 0, i64* %i, align 8
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc46, %while.end
  %38 = load i64, i64* %i, align 8
  %39 = load i64, i64* %inlen.addr, align 8
  %cmp39 = icmp ult i64 %38, %39
  br i1 %cmp39, label %for.body40, label %for.end48

for.body40:                                       ; preds = %for.cond38
  %40 = load i8*, i8** %in.addr, align 4
  %41 = load i64, i64* %i, align 8
  %idxprom41 = trunc i64 %41 to i32
  %arrayidx42 = getelementptr i8, i8* %40, i32 %idxprom41
  %42 = load i8, i8* %arrayidx42, align 1
  %43 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf43 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %43, i32 0, i32 2
  %44 = load i64, i64* %i, align 8
  %idxprom44 = trunc i64 %44 to i32
  %arrayidx45 = getelementptr [64 x i8], [64 x i8]* %buf43, i32 0, i32 %idxprom44
  store i8 %42, i8* %arrayidx45, align 1
  br label %for.inc46

for.inc46:                                        ; preds = %for.body40
  %45 = load i64, i64* %i, align 8
  %inc47 = add i64 %45, 1
  store i64 %inc47, i64* %i, align 8
  br label %for.cond38

for.end48:                                        ; preds = %for.cond38
  %arraydecay49 = getelementptr inbounds [72 x i32], [72 x i32]* %tmp32, i32 0, i32 0
  %46 = bitcast i32* %arraydecay49 to i8*
  call void @sodium_memzero(i8* %46, i32 288)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end48, %for.end, %if.then
  %47 = load i32, i32* %retval, align 4
  ret i32 %47
}

; Function Attrs: noinline nounwind optnone
define internal void @SHA256_Transform(i32* %state, i8* %block, i32* %W, i32* %S) #0 {
entry:
  %state.addr = alloca i32*, align 4
  %block.addr = alloca i8*, align 4
  %W.addr = alloca i32*, align 4
  %S.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store i32* %state, i32** %state.addr, align 4
  store i8* %block, i8** %block.addr, align 4
  store i32* %W, i32** %W.addr, align 4
  store i32* %S, i32** %S.addr, align 4
  %0 = load i32*, i32** %W.addr, align 4
  %1 = load i8*, i8** %block.addr, align 4
  call void @be32dec_vect(i32* %0, i8* %1, i32 64)
  %2 = load i32*, i32** %S.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %4 = load i32*, i32** %state.addr, align 4
  %5 = bitcast i32* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %5, i32 32, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 64
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32*, i32** %S.addr, align 4
  %arrayidx = getelementptr i32, i32* %7, i32 4
  %8 = load i32, i32* %arrayidx, align 4
  %call = call i32 @rotr32(i32 %8, i32 6)
  %9 = load i32*, i32** %S.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %9, i32 4
  %10 = load i32, i32* %arrayidx1, align 4
  %call2 = call i32 @rotr32(i32 %10, i32 11)
  %xor = xor i32 %call, %call2
  %11 = load i32*, i32** %S.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %11, i32 4
  %12 = load i32, i32* %arrayidx3, align 4
  %call4 = call i32 @rotr32(i32 %12, i32 25)
  %xor5 = xor i32 %xor, %call4
  %13 = load i32*, i32** %S.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %13, i32 4
  %14 = load i32, i32* %arrayidx6, align 4
  %15 = load i32*, i32** %S.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %15, i32 5
  %16 = load i32, i32* %arrayidx7, align 4
  %17 = load i32*, i32** %S.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %17, i32 6
  %18 = load i32, i32* %arrayidx8, align 4
  %xor9 = xor i32 %16, %18
  %and = and i32 %14, %xor9
  %19 = load i32*, i32** %S.addr, align 4
  %arrayidx10 = getelementptr i32, i32* %19, i32 6
  %20 = load i32, i32* %arrayidx10, align 4
  %xor11 = xor i32 %and, %20
  %add = add i32 %xor5, %xor11
  %21 = load i32*, i32** %W.addr, align 4
  %22 = load i32, i32* %i, align 4
  %add12 = add i32 0, %22
  %arrayidx13 = getelementptr i32, i32* %21, i32 %add12
  %23 = load i32, i32* %arrayidx13, align 4
  %add14 = add i32 %add, %23
  %24 = load i32, i32* %i, align 4
  %add15 = add i32 0, %24
  %arrayidx16 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add15
  %25 = load i32, i32* %arrayidx16, align 4
  %add17 = add i32 %add14, %25
  %26 = load i32*, i32** %S.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %26, i32 7
  %27 = load i32, i32* %arrayidx18, align 4
  %add19 = add i32 %27, %add17
  store i32 %add19, i32* %arrayidx18, align 4
  %28 = load i32*, i32** %S.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %28, i32 7
  %29 = load i32, i32* %arrayidx20, align 4
  %30 = load i32*, i32** %S.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %30, i32 3
  %31 = load i32, i32* %arrayidx21, align 4
  %add22 = add i32 %31, %29
  store i32 %add22, i32* %arrayidx21, align 4
  %32 = load i32*, i32** %S.addr, align 4
  %arrayidx23 = getelementptr i32, i32* %32, i32 0
  %33 = load i32, i32* %arrayidx23, align 4
  %call24 = call i32 @rotr32(i32 %33, i32 2)
  %34 = load i32*, i32** %S.addr, align 4
  %arrayidx25 = getelementptr i32, i32* %34, i32 0
  %35 = load i32, i32* %arrayidx25, align 4
  %call26 = call i32 @rotr32(i32 %35, i32 13)
  %xor27 = xor i32 %call24, %call26
  %36 = load i32*, i32** %S.addr, align 4
  %arrayidx28 = getelementptr i32, i32* %36, i32 0
  %37 = load i32, i32* %arrayidx28, align 4
  %call29 = call i32 @rotr32(i32 %37, i32 22)
  %xor30 = xor i32 %xor27, %call29
  %38 = load i32*, i32** %S.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %38, i32 0
  %39 = load i32, i32* %arrayidx31, align 4
  %40 = load i32*, i32** %S.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %40, i32 1
  %41 = load i32, i32* %arrayidx32, align 4
  %42 = load i32*, i32** %S.addr, align 4
  %arrayidx33 = getelementptr i32, i32* %42, i32 2
  %43 = load i32, i32* %arrayidx33, align 4
  %or = or i32 %41, %43
  %and34 = and i32 %39, %or
  %44 = load i32*, i32** %S.addr, align 4
  %arrayidx35 = getelementptr i32, i32* %44, i32 1
  %45 = load i32, i32* %arrayidx35, align 4
  %46 = load i32*, i32** %S.addr, align 4
  %arrayidx36 = getelementptr i32, i32* %46, i32 2
  %47 = load i32, i32* %arrayidx36, align 4
  %and37 = and i32 %45, %47
  %or38 = or i32 %and34, %and37
  %add39 = add i32 %xor30, %or38
  %48 = load i32*, i32** %S.addr, align 4
  %arrayidx40 = getelementptr i32, i32* %48, i32 7
  %49 = load i32, i32* %arrayidx40, align 4
  %add41 = add i32 %49, %add39
  store i32 %add41, i32* %arrayidx40, align 4
  %50 = load i32*, i32** %S.addr, align 4
  %arrayidx42 = getelementptr i32, i32* %50, i32 3
  %51 = load i32, i32* %arrayidx42, align 4
  %call43 = call i32 @rotr32(i32 %51, i32 6)
  %52 = load i32*, i32** %S.addr, align 4
  %arrayidx44 = getelementptr i32, i32* %52, i32 3
  %53 = load i32, i32* %arrayidx44, align 4
  %call45 = call i32 @rotr32(i32 %53, i32 11)
  %xor46 = xor i32 %call43, %call45
  %54 = load i32*, i32** %S.addr, align 4
  %arrayidx47 = getelementptr i32, i32* %54, i32 3
  %55 = load i32, i32* %arrayidx47, align 4
  %call48 = call i32 @rotr32(i32 %55, i32 25)
  %xor49 = xor i32 %xor46, %call48
  %56 = load i32*, i32** %S.addr, align 4
  %arrayidx50 = getelementptr i32, i32* %56, i32 3
  %57 = load i32, i32* %arrayidx50, align 4
  %58 = load i32*, i32** %S.addr, align 4
  %arrayidx51 = getelementptr i32, i32* %58, i32 4
  %59 = load i32, i32* %arrayidx51, align 4
  %60 = load i32*, i32** %S.addr, align 4
  %arrayidx52 = getelementptr i32, i32* %60, i32 5
  %61 = load i32, i32* %arrayidx52, align 4
  %xor53 = xor i32 %59, %61
  %and54 = and i32 %57, %xor53
  %62 = load i32*, i32** %S.addr, align 4
  %arrayidx55 = getelementptr i32, i32* %62, i32 5
  %63 = load i32, i32* %arrayidx55, align 4
  %xor56 = xor i32 %and54, %63
  %add57 = add i32 %xor49, %xor56
  %64 = load i32*, i32** %W.addr, align 4
  %65 = load i32, i32* %i, align 4
  %add58 = add i32 1, %65
  %arrayidx59 = getelementptr i32, i32* %64, i32 %add58
  %66 = load i32, i32* %arrayidx59, align 4
  %add60 = add i32 %add57, %66
  %67 = load i32, i32* %i, align 4
  %add61 = add i32 1, %67
  %arrayidx62 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add61
  %68 = load i32, i32* %arrayidx62, align 4
  %add63 = add i32 %add60, %68
  %69 = load i32*, i32** %S.addr, align 4
  %arrayidx64 = getelementptr i32, i32* %69, i32 6
  %70 = load i32, i32* %arrayidx64, align 4
  %add65 = add i32 %70, %add63
  store i32 %add65, i32* %arrayidx64, align 4
  %71 = load i32*, i32** %S.addr, align 4
  %arrayidx66 = getelementptr i32, i32* %71, i32 6
  %72 = load i32, i32* %arrayidx66, align 4
  %73 = load i32*, i32** %S.addr, align 4
  %arrayidx67 = getelementptr i32, i32* %73, i32 2
  %74 = load i32, i32* %arrayidx67, align 4
  %add68 = add i32 %74, %72
  store i32 %add68, i32* %arrayidx67, align 4
  %75 = load i32*, i32** %S.addr, align 4
  %arrayidx69 = getelementptr i32, i32* %75, i32 7
  %76 = load i32, i32* %arrayidx69, align 4
  %call70 = call i32 @rotr32(i32 %76, i32 2)
  %77 = load i32*, i32** %S.addr, align 4
  %arrayidx71 = getelementptr i32, i32* %77, i32 7
  %78 = load i32, i32* %arrayidx71, align 4
  %call72 = call i32 @rotr32(i32 %78, i32 13)
  %xor73 = xor i32 %call70, %call72
  %79 = load i32*, i32** %S.addr, align 4
  %arrayidx74 = getelementptr i32, i32* %79, i32 7
  %80 = load i32, i32* %arrayidx74, align 4
  %call75 = call i32 @rotr32(i32 %80, i32 22)
  %xor76 = xor i32 %xor73, %call75
  %81 = load i32*, i32** %S.addr, align 4
  %arrayidx77 = getelementptr i32, i32* %81, i32 7
  %82 = load i32, i32* %arrayidx77, align 4
  %83 = load i32*, i32** %S.addr, align 4
  %arrayidx78 = getelementptr i32, i32* %83, i32 0
  %84 = load i32, i32* %arrayidx78, align 4
  %85 = load i32*, i32** %S.addr, align 4
  %arrayidx79 = getelementptr i32, i32* %85, i32 1
  %86 = load i32, i32* %arrayidx79, align 4
  %or80 = or i32 %84, %86
  %and81 = and i32 %82, %or80
  %87 = load i32*, i32** %S.addr, align 4
  %arrayidx82 = getelementptr i32, i32* %87, i32 0
  %88 = load i32, i32* %arrayidx82, align 4
  %89 = load i32*, i32** %S.addr, align 4
  %arrayidx83 = getelementptr i32, i32* %89, i32 1
  %90 = load i32, i32* %arrayidx83, align 4
  %and84 = and i32 %88, %90
  %or85 = or i32 %and81, %and84
  %add86 = add i32 %xor76, %or85
  %91 = load i32*, i32** %S.addr, align 4
  %arrayidx87 = getelementptr i32, i32* %91, i32 6
  %92 = load i32, i32* %arrayidx87, align 4
  %add88 = add i32 %92, %add86
  store i32 %add88, i32* %arrayidx87, align 4
  %93 = load i32*, i32** %S.addr, align 4
  %arrayidx89 = getelementptr i32, i32* %93, i32 2
  %94 = load i32, i32* %arrayidx89, align 4
  %call90 = call i32 @rotr32(i32 %94, i32 6)
  %95 = load i32*, i32** %S.addr, align 4
  %arrayidx91 = getelementptr i32, i32* %95, i32 2
  %96 = load i32, i32* %arrayidx91, align 4
  %call92 = call i32 @rotr32(i32 %96, i32 11)
  %xor93 = xor i32 %call90, %call92
  %97 = load i32*, i32** %S.addr, align 4
  %arrayidx94 = getelementptr i32, i32* %97, i32 2
  %98 = load i32, i32* %arrayidx94, align 4
  %call95 = call i32 @rotr32(i32 %98, i32 25)
  %xor96 = xor i32 %xor93, %call95
  %99 = load i32*, i32** %S.addr, align 4
  %arrayidx97 = getelementptr i32, i32* %99, i32 2
  %100 = load i32, i32* %arrayidx97, align 4
  %101 = load i32*, i32** %S.addr, align 4
  %arrayidx98 = getelementptr i32, i32* %101, i32 3
  %102 = load i32, i32* %arrayidx98, align 4
  %103 = load i32*, i32** %S.addr, align 4
  %arrayidx99 = getelementptr i32, i32* %103, i32 4
  %104 = load i32, i32* %arrayidx99, align 4
  %xor100 = xor i32 %102, %104
  %and101 = and i32 %100, %xor100
  %105 = load i32*, i32** %S.addr, align 4
  %arrayidx102 = getelementptr i32, i32* %105, i32 4
  %106 = load i32, i32* %arrayidx102, align 4
  %xor103 = xor i32 %and101, %106
  %add104 = add i32 %xor96, %xor103
  %107 = load i32*, i32** %W.addr, align 4
  %108 = load i32, i32* %i, align 4
  %add105 = add i32 2, %108
  %arrayidx106 = getelementptr i32, i32* %107, i32 %add105
  %109 = load i32, i32* %arrayidx106, align 4
  %add107 = add i32 %add104, %109
  %110 = load i32, i32* %i, align 4
  %add108 = add i32 2, %110
  %arrayidx109 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add108
  %111 = load i32, i32* %arrayidx109, align 4
  %add110 = add i32 %add107, %111
  %112 = load i32*, i32** %S.addr, align 4
  %arrayidx111 = getelementptr i32, i32* %112, i32 5
  %113 = load i32, i32* %arrayidx111, align 4
  %add112 = add i32 %113, %add110
  store i32 %add112, i32* %arrayidx111, align 4
  %114 = load i32*, i32** %S.addr, align 4
  %arrayidx113 = getelementptr i32, i32* %114, i32 5
  %115 = load i32, i32* %arrayidx113, align 4
  %116 = load i32*, i32** %S.addr, align 4
  %arrayidx114 = getelementptr i32, i32* %116, i32 1
  %117 = load i32, i32* %arrayidx114, align 4
  %add115 = add i32 %117, %115
  store i32 %add115, i32* %arrayidx114, align 4
  %118 = load i32*, i32** %S.addr, align 4
  %arrayidx116 = getelementptr i32, i32* %118, i32 6
  %119 = load i32, i32* %arrayidx116, align 4
  %call117 = call i32 @rotr32(i32 %119, i32 2)
  %120 = load i32*, i32** %S.addr, align 4
  %arrayidx118 = getelementptr i32, i32* %120, i32 6
  %121 = load i32, i32* %arrayidx118, align 4
  %call119 = call i32 @rotr32(i32 %121, i32 13)
  %xor120 = xor i32 %call117, %call119
  %122 = load i32*, i32** %S.addr, align 4
  %arrayidx121 = getelementptr i32, i32* %122, i32 6
  %123 = load i32, i32* %arrayidx121, align 4
  %call122 = call i32 @rotr32(i32 %123, i32 22)
  %xor123 = xor i32 %xor120, %call122
  %124 = load i32*, i32** %S.addr, align 4
  %arrayidx124 = getelementptr i32, i32* %124, i32 6
  %125 = load i32, i32* %arrayidx124, align 4
  %126 = load i32*, i32** %S.addr, align 4
  %arrayidx125 = getelementptr i32, i32* %126, i32 7
  %127 = load i32, i32* %arrayidx125, align 4
  %128 = load i32*, i32** %S.addr, align 4
  %arrayidx126 = getelementptr i32, i32* %128, i32 0
  %129 = load i32, i32* %arrayidx126, align 4
  %or127 = or i32 %127, %129
  %and128 = and i32 %125, %or127
  %130 = load i32*, i32** %S.addr, align 4
  %arrayidx129 = getelementptr i32, i32* %130, i32 7
  %131 = load i32, i32* %arrayidx129, align 4
  %132 = load i32*, i32** %S.addr, align 4
  %arrayidx130 = getelementptr i32, i32* %132, i32 0
  %133 = load i32, i32* %arrayidx130, align 4
  %and131 = and i32 %131, %133
  %or132 = or i32 %and128, %and131
  %add133 = add i32 %xor123, %or132
  %134 = load i32*, i32** %S.addr, align 4
  %arrayidx134 = getelementptr i32, i32* %134, i32 5
  %135 = load i32, i32* %arrayidx134, align 4
  %add135 = add i32 %135, %add133
  store i32 %add135, i32* %arrayidx134, align 4
  %136 = load i32*, i32** %S.addr, align 4
  %arrayidx136 = getelementptr i32, i32* %136, i32 1
  %137 = load i32, i32* %arrayidx136, align 4
  %call137 = call i32 @rotr32(i32 %137, i32 6)
  %138 = load i32*, i32** %S.addr, align 4
  %arrayidx138 = getelementptr i32, i32* %138, i32 1
  %139 = load i32, i32* %arrayidx138, align 4
  %call139 = call i32 @rotr32(i32 %139, i32 11)
  %xor140 = xor i32 %call137, %call139
  %140 = load i32*, i32** %S.addr, align 4
  %arrayidx141 = getelementptr i32, i32* %140, i32 1
  %141 = load i32, i32* %arrayidx141, align 4
  %call142 = call i32 @rotr32(i32 %141, i32 25)
  %xor143 = xor i32 %xor140, %call142
  %142 = load i32*, i32** %S.addr, align 4
  %arrayidx144 = getelementptr i32, i32* %142, i32 1
  %143 = load i32, i32* %arrayidx144, align 4
  %144 = load i32*, i32** %S.addr, align 4
  %arrayidx145 = getelementptr i32, i32* %144, i32 2
  %145 = load i32, i32* %arrayidx145, align 4
  %146 = load i32*, i32** %S.addr, align 4
  %arrayidx146 = getelementptr i32, i32* %146, i32 3
  %147 = load i32, i32* %arrayidx146, align 4
  %xor147 = xor i32 %145, %147
  %and148 = and i32 %143, %xor147
  %148 = load i32*, i32** %S.addr, align 4
  %arrayidx149 = getelementptr i32, i32* %148, i32 3
  %149 = load i32, i32* %arrayidx149, align 4
  %xor150 = xor i32 %and148, %149
  %add151 = add i32 %xor143, %xor150
  %150 = load i32*, i32** %W.addr, align 4
  %151 = load i32, i32* %i, align 4
  %add152 = add i32 3, %151
  %arrayidx153 = getelementptr i32, i32* %150, i32 %add152
  %152 = load i32, i32* %arrayidx153, align 4
  %add154 = add i32 %add151, %152
  %153 = load i32, i32* %i, align 4
  %add155 = add i32 3, %153
  %arrayidx156 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add155
  %154 = load i32, i32* %arrayidx156, align 4
  %add157 = add i32 %add154, %154
  %155 = load i32*, i32** %S.addr, align 4
  %arrayidx158 = getelementptr i32, i32* %155, i32 4
  %156 = load i32, i32* %arrayidx158, align 4
  %add159 = add i32 %156, %add157
  store i32 %add159, i32* %arrayidx158, align 4
  %157 = load i32*, i32** %S.addr, align 4
  %arrayidx160 = getelementptr i32, i32* %157, i32 4
  %158 = load i32, i32* %arrayidx160, align 4
  %159 = load i32*, i32** %S.addr, align 4
  %arrayidx161 = getelementptr i32, i32* %159, i32 0
  %160 = load i32, i32* %arrayidx161, align 4
  %add162 = add i32 %160, %158
  store i32 %add162, i32* %arrayidx161, align 4
  %161 = load i32*, i32** %S.addr, align 4
  %arrayidx163 = getelementptr i32, i32* %161, i32 5
  %162 = load i32, i32* %arrayidx163, align 4
  %call164 = call i32 @rotr32(i32 %162, i32 2)
  %163 = load i32*, i32** %S.addr, align 4
  %arrayidx165 = getelementptr i32, i32* %163, i32 5
  %164 = load i32, i32* %arrayidx165, align 4
  %call166 = call i32 @rotr32(i32 %164, i32 13)
  %xor167 = xor i32 %call164, %call166
  %165 = load i32*, i32** %S.addr, align 4
  %arrayidx168 = getelementptr i32, i32* %165, i32 5
  %166 = load i32, i32* %arrayidx168, align 4
  %call169 = call i32 @rotr32(i32 %166, i32 22)
  %xor170 = xor i32 %xor167, %call169
  %167 = load i32*, i32** %S.addr, align 4
  %arrayidx171 = getelementptr i32, i32* %167, i32 5
  %168 = load i32, i32* %arrayidx171, align 4
  %169 = load i32*, i32** %S.addr, align 4
  %arrayidx172 = getelementptr i32, i32* %169, i32 6
  %170 = load i32, i32* %arrayidx172, align 4
  %171 = load i32*, i32** %S.addr, align 4
  %arrayidx173 = getelementptr i32, i32* %171, i32 7
  %172 = load i32, i32* %arrayidx173, align 4
  %or174 = or i32 %170, %172
  %and175 = and i32 %168, %or174
  %173 = load i32*, i32** %S.addr, align 4
  %arrayidx176 = getelementptr i32, i32* %173, i32 6
  %174 = load i32, i32* %arrayidx176, align 4
  %175 = load i32*, i32** %S.addr, align 4
  %arrayidx177 = getelementptr i32, i32* %175, i32 7
  %176 = load i32, i32* %arrayidx177, align 4
  %and178 = and i32 %174, %176
  %or179 = or i32 %and175, %and178
  %add180 = add i32 %xor170, %or179
  %177 = load i32*, i32** %S.addr, align 4
  %arrayidx181 = getelementptr i32, i32* %177, i32 4
  %178 = load i32, i32* %arrayidx181, align 4
  %add182 = add i32 %178, %add180
  store i32 %add182, i32* %arrayidx181, align 4
  %179 = load i32*, i32** %S.addr, align 4
  %arrayidx183 = getelementptr i32, i32* %179, i32 0
  %180 = load i32, i32* %arrayidx183, align 4
  %call184 = call i32 @rotr32(i32 %180, i32 6)
  %181 = load i32*, i32** %S.addr, align 4
  %arrayidx185 = getelementptr i32, i32* %181, i32 0
  %182 = load i32, i32* %arrayidx185, align 4
  %call186 = call i32 @rotr32(i32 %182, i32 11)
  %xor187 = xor i32 %call184, %call186
  %183 = load i32*, i32** %S.addr, align 4
  %arrayidx188 = getelementptr i32, i32* %183, i32 0
  %184 = load i32, i32* %arrayidx188, align 4
  %call189 = call i32 @rotr32(i32 %184, i32 25)
  %xor190 = xor i32 %xor187, %call189
  %185 = load i32*, i32** %S.addr, align 4
  %arrayidx191 = getelementptr i32, i32* %185, i32 0
  %186 = load i32, i32* %arrayidx191, align 4
  %187 = load i32*, i32** %S.addr, align 4
  %arrayidx192 = getelementptr i32, i32* %187, i32 1
  %188 = load i32, i32* %arrayidx192, align 4
  %189 = load i32*, i32** %S.addr, align 4
  %arrayidx193 = getelementptr i32, i32* %189, i32 2
  %190 = load i32, i32* %arrayidx193, align 4
  %xor194 = xor i32 %188, %190
  %and195 = and i32 %186, %xor194
  %191 = load i32*, i32** %S.addr, align 4
  %arrayidx196 = getelementptr i32, i32* %191, i32 2
  %192 = load i32, i32* %arrayidx196, align 4
  %xor197 = xor i32 %and195, %192
  %add198 = add i32 %xor190, %xor197
  %193 = load i32*, i32** %W.addr, align 4
  %194 = load i32, i32* %i, align 4
  %add199 = add i32 4, %194
  %arrayidx200 = getelementptr i32, i32* %193, i32 %add199
  %195 = load i32, i32* %arrayidx200, align 4
  %add201 = add i32 %add198, %195
  %196 = load i32, i32* %i, align 4
  %add202 = add i32 4, %196
  %arrayidx203 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add202
  %197 = load i32, i32* %arrayidx203, align 4
  %add204 = add i32 %add201, %197
  %198 = load i32*, i32** %S.addr, align 4
  %arrayidx205 = getelementptr i32, i32* %198, i32 3
  %199 = load i32, i32* %arrayidx205, align 4
  %add206 = add i32 %199, %add204
  store i32 %add206, i32* %arrayidx205, align 4
  %200 = load i32*, i32** %S.addr, align 4
  %arrayidx207 = getelementptr i32, i32* %200, i32 3
  %201 = load i32, i32* %arrayidx207, align 4
  %202 = load i32*, i32** %S.addr, align 4
  %arrayidx208 = getelementptr i32, i32* %202, i32 7
  %203 = load i32, i32* %arrayidx208, align 4
  %add209 = add i32 %203, %201
  store i32 %add209, i32* %arrayidx208, align 4
  %204 = load i32*, i32** %S.addr, align 4
  %arrayidx210 = getelementptr i32, i32* %204, i32 4
  %205 = load i32, i32* %arrayidx210, align 4
  %call211 = call i32 @rotr32(i32 %205, i32 2)
  %206 = load i32*, i32** %S.addr, align 4
  %arrayidx212 = getelementptr i32, i32* %206, i32 4
  %207 = load i32, i32* %arrayidx212, align 4
  %call213 = call i32 @rotr32(i32 %207, i32 13)
  %xor214 = xor i32 %call211, %call213
  %208 = load i32*, i32** %S.addr, align 4
  %arrayidx215 = getelementptr i32, i32* %208, i32 4
  %209 = load i32, i32* %arrayidx215, align 4
  %call216 = call i32 @rotr32(i32 %209, i32 22)
  %xor217 = xor i32 %xor214, %call216
  %210 = load i32*, i32** %S.addr, align 4
  %arrayidx218 = getelementptr i32, i32* %210, i32 4
  %211 = load i32, i32* %arrayidx218, align 4
  %212 = load i32*, i32** %S.addr, align 4
  %arrayidx219 = getelementptr i32, i32* %212, i32 5
  %213 = load i32, i32* %arrayidx219, align 4
  %214 = load i32*, i32** %S.addr, align 4
  %arrayidx220 = getelementptr i32, i32* %214, i32 6
  %215 = load i32, i32* %arrayidx220, align 4
  %or221 = or i32 %213, %215
  %and222 = and i32 %211, %or221
  %216 = load i32*, i32** %S.addr, align 4
  %arrayidx223 = getelementptr i32, i32* %216, i32 5
  %217 = load i32, i32* %arrayidx223, align 4
  %218 = load i32*, i32** %S.addr, align 4
  %arrayidx224 = getelementptr i32, i32* %218, i32 6
  %219 = load i32, i32* %arrayidx224, align 4
  %and225 = and i32 %217, %219
  %or226 = or i32 %and222, %and225
  %add227 = add i32 %xor217, %or226
  %220 = load i32*, i32** %S.addr, align 4
  %arrayidx228 = getelementptr i32, i32* %220, i32 3
  %221 = load i32, i32* %arrayidx228, align 4
  %add229 = add i32 %221, %add227
  store i32 %add229, i32* %arrayidx228, align 4
  %222 = load i32*, i32** %S.addr, align 4
  %arrayidx230 = getelementptr i32, i32* %222, i32 7
  %223 = load i32, i32* %arrayidx230, align 4
  %call231 = call i32 @rotr32(i32 %223, i32 6)
  %224 = load i32*, i32** %S.addr, align 4
  %arrayidx232 = getelementptr i32, i32* %224, i32 7
  %225 = load i32, i32* %arrayidx232, align 4
  %call233 = call i32 @rotr32(i32 %225, i32 11)
  %xor234 = xor i32 %call231, %call233
  %226 = load i32*, i32** %S.addr, align 4
  %arrayidx235 = getelementptr i32, i32* %226, i32 7
  %227 = load i32, i32* %arrayidx235, align 4
  %call236 = call i32 @rotr32(i32 %227, i32 25)
  %xor237 = xor i32 %xor234, %call236
  %228 = load i32*, i32** %S.addr, align 4
  %arrayidx238 = getelementptr i32, i32* %228, i32 7
  %229 = load i32, i32* %arrayidx238, align 4
  %230 = load i32*, i32** %S.addr, align 4
  %arrayidx239 = getelementptr i32, i32* %230, i32 0
  %231 = load i32, i32* %arrayidx239, align 4
  %232 = load i32*, i32** %S.addr, align 4
  %arrayidx240 = getelementptr i32, i32* %232, i32 1
  %233 = load i32, i32* %arrayidx240, align 4
  %xor241 = xor i32 %231, %233
  %and242 = and i32 %229, %xor241
  %234 = load i32*, i32** %S.addr, align 4
  %arrayidx243 = getelementptr i32, i32* %234, i32 1
  %235 = load i32, i32* %arrayidx243, align 4
  %xor244 = xor i32 %and242, %235
  %add245 = add i32 %xor237, %xor244
  %236 = load i32*, i32** %W.addr, align 4
  %237 = load i32, i32* %i, align 4
  %add246 = add i32 5, %237
  %arrayidx247 = getelementptr i32, i32* %236, i32 %add246
  %238 = load i32, i32* %arrayidx247, align 4
  %add248 = add i32 %add245, %238
  %239 = load i32, i32* %i, align 4
  %add249 = add i32 5, %239
  %arrayidx250 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add249
  %240 = load i32, i32* %arrayidx250, align 4
  %add251 = add i32 %add248, %240
  %241 = load i32*, i32** %S.addr, align 4
  %arrayidx252 = getelementptr i32, i32* %241, i32 2
  %242 = load i32, i32* %arrayidx252, align 4
  %add253 = add i32 %242, %add251
  store i32 %add253, i32* %arrayidx252, align 4
  %243 = load i32*, i32** %S.addr, align 4
  %arrayidx254 = getelementptr i32, i32* %243, i32 2
  %244 = load i32, i32* %arrayidx254, align 4
  %245 = load i32*, i32** %S.addr, align 4
  %arrayidx255 = getelementptr i32, i32* %245, i32 6
  %246 = load i32, i32* %arrayidx255, align 4
  %add256 = add i32 %246, %244
  store i32 %add256, i32* %arrayidx255, align 4
  %247 = load i32*, i32** %S.addr, align 4
  %arrayidx257 = getelementptr i32, i32* %247, i32 3
  %248 = load i32, i32* %arrayidx257, align 4
  %call258 = call i32 @rotr32(i32 %248, i32 2)
  %249 = load i32*, i32** %S.addr, align 4
  %arrayidx259 = getelementptr i32, i32* %249, i32 3
  %250 = load i32, i32* %arrayidx259, align 4
  %call260 = call i32 @rotr32(i32 %250, i32 13)
  %xor261 = xor i32 %call258, %call260
  %251 = load i32*, i32** %S.addr, align 4
  %arrayidx262 = getelementptr i32, i32* %251, i32 3
  %252 = load i32, i32* %arrayidx262, align 4
  %call263 = call i32 @rotr32(i32 %252, i32 22)
  %xor264 = xor i32 %xor261, %call263
  %253 = load i32*, i32** %S.addr, align 4
  %arrayidx265 = getelementptr i32, i32* %253, i32 3
  %254 = load i32, i32* %arrayidx265, align 4
  %255 = load i32*, i32** %S.addr, align 4
  %arrayidx266 = getelementptr i32, i32* %255, i32 4
  %256 = load i32, i32* %arrayidx266, align 4
  %257 = load i32*, i32** %S.addr, align 4
  %arrayidx267 = getelementptr i32, i32* %257, i32 5
  %258 = load i32, i32* %arrayidx267, align 4
  %or268 = or i32 %256, %258
  %and269 = and i32 %254, %or268
  %259 = load i32*, i32** %S.addr, align 4
  %arrayidx270 = getelementptr i32, i32* %259, i32 4
  %260 = load i32, i32* %arrayidx270, align 4
  %261 = load i32*, i32** %S.addr, align 4
  %arrayidx271 = getelementptr i32, i32* %261, i32 5
  %262 = load i32, i32* %arrayidx271, align 4
  %and272 = and i32 %260, %262
  %or273 = or i32 %and269, %and272
  %add274 = add i32 %xor264, %or273
  %263 = load i32*, i32** %S.addr, align 4
  %arrayidx275 = getelementptr i32, i32* %263, i32 2
  %264 = load i32, i32* %arrayidx275, align 4
  %add276 = add i32 %264, %add274
  store i32 %add276, i32* %arrayidx275, align 4
  %265 = load i32*, i32** %S.addr, align 4
  %arrayidx277 = getelementptr i32, i32* %265, i32 6
  %266 = load i32, i32* %arrayidx277, align 4
  %call278 = call i32 @rotr32(i32 %266, i32 6)
  %267 = load i32*, i32** %S.addr, align 4
  %arrayidx279 = getelementptr i32, i32* %267, i32 6
  %268 = load i32, i32* %arrayidx279, align 4
  %call280 = call i32 @rotr32(i32 %268, i32 11)
  %xor281 = xor i32 %call278, %call280
  %269 = load i32*, i32** %S.addr, align 4
  %arrayidx282 = getelementptr i32, i32* %269, i32 6
  %270 = load i32, i32* %arrayidx282, align 4
  %call283 = call i32 @rotr32(i32 %270, i32 25)
  %xor284 = xor i32 %xor281, %call283
  %271 = load i32*, i32** %S.addr, align 4
  %arrayidx285 = getelementptr i32, i32* %271, i32 6
  %272 = load i32, i32* %arrayidx285, align 4
  %273 = load i32*, i32** %S.addr, align 4
  %arrayidx286 = getelementptr i32, i32* %273, i32 7
  %274 = load i32, i32* %arrayidx286, align 4
  %275 = load i32*, i32** %S.addr, align 4
  %arrayidx287 = getelementptr i32, i32* %275, i32 0
  %276 = load i32, i32* %arrayidx287, align 4
  %xor288 = xor i32 %274, %276
  %and289 = and i32 %272, %xor288
  %277 = load i32*, i32** %S.addr, align 4
  %arrayidx290 = getelementptr i32, i32* %277, i32 0
  %278 = load i32, i32* %arrayidx290, align 4
  %xor291 = xor i32 %and289, %278
  %add292 = add i32 %xor284, %xor291
  %279 = load i32*, i32** %W.addr, align 4
  %280 = load i32, i32* %i, align 4
  %add293 = add i32 6, %280
  %arrayidx294 = getelementptr i32, i32* %279, i32 %add293
  %281 = load i32, i32* %arrayidx294, align 4
  %add295 = add i32 %add292, %281
  %282 = load i32, i32* %i, align 4
  %add296 = add i32 6, %282
  %arrayidx297 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add296
  %283 = load i32, i32* %arrayidx297, align 4
  %add298 = add i32 %add295, %283
  %284 = load i32*, i32** %S.addr, align 4
  %arrayidx299 = getelementptr i32, i32* %284, i32 1
  %285 = load i32, i32* %arrayidx299, align 4
  %add300 = add i32 %285, %add298
  store i32 %add300, i32* %arrayidx299, align 4
  %286 = load i32*, i32** %S.addr, align 4
  %arrayidx301 = getelementptr i32, i32* %286, i32 1
  %287 = load i32, i32* %arrayidx301, align 4
  %288 = load i32*, i32** %S.addr, align 4
  %arrayidx302 = getelementptr i32, i32* %288, i32 5
  %289 = load i32, i32* %arrayidx302, align 4
  %add303 = add i32 %289, %287
  store i32 %add303, i32* %arrayidx302, align 4
  %290 = load i32*, i32** %S.addr, align 4
  %arrayidx304 = getelementptr i32, i32* %290, i32 2
  %291 = load i32, i32* %arrayidx304, align 4
  %call305 = call i32 @rotr32(i32 %291, i32 2)
  %292 = load i32*, i32** %S.addr, align 4
  %arrayidx306 = getelementptr i32, i32* %292, i32 2
  %293 = load i32, i32* %arrayidx306, align 4
  %call307 = call i32 @rotr32(i32 %293, i32 13)
  %xor308 = xor i32 %call305, %call307
  %294 = load i32*, i32** %S.addr, align 4
  %arrayidx309 = getelementptr i32, i32* %294, i32 2
  %295 = load i32, i32* %arrayidx309, align 4
  %call310 = call i32 @rotr32(i32 %295, i32 22)
  %xor311 = xor i32 %xor308, %call310
  %296 = load i32*, i32** %S.addr, align 4
  %arrayidx312 = getelementptr i32, i32* %296, i32 2
  %297 = load i32, i32* %arrayidx312, align 4
  %298 = load i32*, i32** %S.addr, align 4
  %arrayidx313 = getelementptr i32, i32* %298, i32 3
  %299 = load i32, i32* %arrayidx313, align 4
  %300 = load i32*, i32** %S.addr, align 4
  %arrayidx314 = getelementptr i32, i32* %300, i32 4
  %301 = load i32, i32* %arrayidx314, align 4
  %or315 = or i32 %299, %301
  %and316 = and i32 %297, %or315
  %302 = load i32*, i32** %S.addr, align 4
  %arrayidx317 = getelementptr i32, i32* %302, i32 3
  %303 = load i32, i32* %arrayidx317, align 4
  %304 = load i32*, i32** %S.addr, align 4
  %arrayidx318 = getelementptr i32, i32* %304, i32 4
  %305 = load i32, i32* %arrayidx318, align 4
  %and319 = and i32 %303, %305
  %or320 = or i32 %and316, %and319
  %add321 = add i32 %xor311, %or320
  %306 = load i32*, i32** %S.addr, align 4
  %arrayidx322 = getelementptr i32, i32* %306, i32 1
  %307 = load i32, i32* %arrayidx322, align 4
  %add323 = add i32 %307, %add321
  store i32 %add323, i32* %arrayidx322, align 4
  %308 = load i32*, i32** %S.addr, align 4
  %arrayidx324 = getelementptr i32, i32* %308, i32 5
  %309 = load i32, i32* %arrayidx324, align 4
  %call325 = call i32 @rotr32(i32 %309, i32 6)
  %310 = load i32*, i32** %S.addr, align 4
  %arrayidx326 = getelementptr i32, i32* %310, i32 5
  %311 = load i32, i32* %arrayidx326, align 4
  %call327 = call i32 @rotr32(i32 %311, i32 11)
  %xor328 = xor i32 %call325, %call327
  %312 = load i32*, i32** %S.addr, align 4
  %arrayidx329 = getelementptr i32, i32* %312, i32 5
  %313 = load i32, i32* %arrayidx329, align 4
  %call330 = call i32 @rotr32(i32 %313, i32 25)
  %xor331 = xor i32 %xor328, %call330
  %314 = load i32*, i32** %S.addr, align 4
  %arrayidx332 = getelementptr i32, i32* %314, i32 5
  %315 = load i32, i32* %arrayidx332, align 4
  %316 = load i32*, i32** %S.addr, align 4
  %arrayidx333 = getelementptr i32, i32* %316, i32 6
  %317 = load i32, i32* %arrayidx333, align 4
  %318 = load i32*, i32** %S.addr, align 4
  %arrayidx334 = getelementptr i32, i32* %318, i32 7
  %319 = load i32, i32* %arrayidx334, align 4
  %xor335 = xor i32 %317, %319
  %and336 = and i32 %315, %xor335
  %320 = load i32*, i32** %S.addr, align 4
  %arrayidx337 = getelementptr i32, i32* %320, i32 7
  %321 = load i32, i32* %arrayidx337, align 4
  %xor338 = xor i32 %and336, %321
  %add339 = add i32 %xor331, %xor338
  %322 = load i32*, i32** %W.addr, align 4
  %323 = load i32, i32* %i, align 4
  %add340 = add i32 7, %323
  %arrayidx341 = getelementptr i32, i32* %322, i32 %add340
  %324 = load i32, i32* %arrayidx341, align 4
  %add342 = add i32 %add339, %324
  %325 = load i32, i32* %i, align 4
  %add343 = add i32 7, %325
  %arrayidx344 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add343
  %326 = load i32, i32* %arrayidx344, align 4
  %add345 = add i32 %add342, %326
  %327 = load i32*, i32** %S.addr, align 4
  %arrayidx346 = getelementptr i32, i32* %327, i32 0
  %328 = load i32, i32* %arrayidx346, align 4
  %add347 = add i32 %328, %add345
  store i32 %add347, i32* %arrayidx346, align 4
  %329 = load i32*, i32** %S.addr, align 4
  %arrayidx348 = getelementptr i32, i32* %329, i32 0
  %330 = load i32, i32* %arrayidx348, align 4
  %331 = load i32*, i32** %S.addr, align 4
  %arrayidx349 = getelementptr i32, i32* %331, i32 4
  %332 = load i32, i32* %arrayidx349, align 4
  %add350 = add i32 %332, %330
  store i32 %add350, i32* %arrayidx349, align 4
  %333 = load i32*, i32** %S.addr, align 4
  %arrayidx351 = getelementptr i32, i32* %333, i32 1
  %334 = load i32, i32* %arrayidx351, align 4
  %call352 = call i32 @rotr32(i32 %334, i32 2)
  %335 = load i32*, i32** %S.addr, align 4
  %arrayidx353 = getelementptr i32, i32* %335, i32 1
  %336 = load i32, i32* %arrayidx353, align 4
  %call354 = call i32 @rotr32(i32 %336, i32 13)
  %xor355 = xor i32 %call352, %call354
  %337 = load i32*, i32** %S.addr, align 4
  %arrayidx356 = getelementptr i32, i32* %337, i32 1
  %338 = load i32, i32* %arrayidx356, align 4
  %call357 = call i32 @rotr32(i32 %338, i32 22)
  %xor358 = xor i32 %xor355, %call357
  %339 = load i32*, i32** %S.addr, align 4
  %arrayidx359 = getelementptr i32, i32* %339, i32 1
  %340 = load i32, i32* %arrayidx359, align 4
  %341 = load i32*, i32** %S.addr, align 4
  %arrayidx360 = getelementptr i32, i32* %341, i32 2
  %342 = load i32, i32* %arrayidx360, align 4
  %343 = load i32*, i32** %S.addr, align 4
  %arrayidx361 = getelementptr i32, i32* %343, i32 3
  %344 = load i32, i32* %arrayidx361, align 4
  %or362 = or i32 %342, %344
  %and363 = and i32 %340, %or362
  %345 = load i32*, i32** %S.addr, align 4
  %arrayidx364 = getelementptr i32, i32* %345, i32 2
  %346 = load i32, i32* %arrayidx364, align 4
  %347 = load i32*, i32** %S.addr, align 4
  %arrayidx365 = getelementptr i32, i32* %347, i32 3
  %348 = load i32, i32* %arrayidx365, align 4
  %and366 = and i32 %346, %348
  %or367 = or i32 %and363, %and366
  %add368 = add i32 %xor358, %or367
  %349 = load i32*, i32** %S.addr, align 4
  %arrayidx369 = getelementptr i32, i32* %349, i32 0
  %350 = load i32, i32* %arrayidx369, align 4
  %add370 = add i32 %350, %add368
  store i32 %add370, i32* %arrayidx369, align 4
  %351 = load i32*, i32** %S.addr, align 4
  %arrayidx371 = getelementptr i32, i32* %351, i32 4
  %352 = load i32, i32* %arrayidx371, align 4
  %call372 = call i32 @rotr32(i32 %352, i32 6)
  %353 = load i32*, i32** %S.addr, align 4
  %arrayidx373 = getelementptr i32, i32* %353, i32 4
  %354 = load i32, i32* %arrayidx373, align 4
  %call374 = call i32 @rotr32(i32 %354, i32 11)
  %xor375 = xor i32 %call372, %call374
  %355 = load i32*, i32** %S.addr, align 4
  %arrayidx376 = getelementptr i32, i32* %355, i32 4
  %356 = load i32, i32* %arrayidx376, align 4
  %call377 = call i32 @rotr32(i32 %356, i32 25)
  %xor378 = xor i32 %xor375, %call377
  %357 = load i32*, i32** %S.addr, align 4
  %arrayidx379 = getelementptr i32, i32* %357, i32 4
  %358 = load i32, i32* %arrayidx379, align 4
  %359 = load i32*, i32** %S.addr, align 4
  %arrayidx380 = getelementptr i32, i32* %359, i32 5
  %360 = load i32, i32* %arrayidx380, align 4
  %361 = load i32*, i32** %S.addr, align 4
  %arrayidx381 = getelementptr i32, i32* %361, i32 6
  %362 = load i32, i32* %arrayidx381, align 4
  %xor382 = xor i32 %360, %362
  %and383 = and i32 %358, %xor382
  %363 = load i32*, i32** %S.addr, align 4
  %arrayidx384 = getelementptr i32, i32* %363, i32 6
  %364 = load i32, i32* %arrayidx384, align 4
  %xor385 = xor i32 %and383, %364
  %add386 = add i32 %xor378, %xor385
  %365 = load i32*, i32** %W.addr, align 4
  %366 = load i32, i32* %i, align 4
  %add387 = add i32 8, %366
  %arrayidx388 = getelementptr i32, i32* %365, i32 %add387
  %367 = load i32, i32* %arrayidx388, align 4
  %add389 = add i32 %add386, %367
  %368 = load i32, i32* %i, align 4
  %add390 = add i32 8, %368
  %arrayidx391 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add390
  %369 = load i32, i32* %arrayidx391, align 4
  %add392 = add i32 %add389, %369
  %370 = load i32*, i32** %S.addr, align 4
  %arrayidx393 = getelementptr i32, i32* %370, i32 7
  %371 = load i32, i32* %arrayidx393, align 4
  %add394 = add i32 %371, %add392
  store i32 %add394, i32* %arrayidx393, align 4
  %372 = load i32*, i32** %S.addr, align 4
  %arrayidx395 = getelementptr i32, i32* %372, i32 7
  %373 = load i32, i32* %arrayidx395, align 4
  %374 = load i32*, i32** %S.addr, align 4
  %arrayidx396 = getelementptr i32, i32* %374, i32 3
  %375 = load i32, i32* %arrayidx396, align 4
  %add397 = add i32 %375, %373
  store i32 %add397, i32* %arrayidx396, align 4
  %376 = load i32*, i32** %S.addr, align 4
  %arrayidx398 = getelementptr i32, i32* %376, i32 0
  %377 = load i32, i32* %arrayidx398, align 4
  %call399 = call i32 @rotr32(i32 %377, i32 2)
  %378 = load i32*, i32** %S.addr, align 4
  %arrayidx400 = getelementptr i32, i32* %378, i32 0
  %379 = load i32, i32* %arrayidx400, align 4
  %call401 = call i32 @rotr32(i32 %379, i32 13)
  %xor402 = xor i32 %call399, %call401
  %380 = load i32*, i32** %S.addr, align 4
  %arrayidx403 = getelementptr i32, i32* %380, i32 0
  %381 = load i32, i32* %arrayidx403, align 4
  %call404 = call i32 @rotr32(i32 %381, i32 22)
  %xor405 = xor i32 %xor402, %call404
  %382 = load i32*, i32** %S.addr, align 4
  %arrayidx406 = getelementptr i32, i32* %382, i32 0
  %383 = load i32, i32* %arrayidx406, align 4
  %384 = load i32*, i32** %S.addr, align 4
  %arrayidx407 = getelementptr i32, i32* %384, i32 1
  %385 = load i32, i32* %arrayidx407, align 4
  %386 = load i32*, i32** %S.addr, align 4
  %arrayidx408 = getelementptr i32, i32* %386, i32 2
  %387 = load i32, i32* %arrayidx408, align 4
  %or409 = or i32 %385, %387
  %and410 = and i32 %383, %or409
  %388 = load i32*, i32** %S.addr, align 4
  %arrayidx411 = getelementptr i32, i32* %388, i32 1
  %389 = load i32, i32* %arrayidx411, align 4
  %390 = load i32*, i32** %S.addr, align 4
  %arrayidx412 = getelementptr i32, i32* %390, i32 2
  %391 = load i32, i32* %arrayidx412, align 4
  %and413 = and i32 %389, %391
  %or414 = or i32 %and410, %and413
  %add415 = add i32 %xor405, %or414
  %392 = load i32*, i32** %S.addr, align 4
  %arrayidx416 = getelementptr i32, i32* %392, i32 7
  %393 = load i32, i32* %arrayidx416, align 4
  %add417 = add i32 %393, %add415
  store i32 %add417, i32* %arrayidx416, align 4
  %394 = load i32*, i32** %S.addr, align 4
  %arrayidx418 = getelementptr i32, i32* %394, i32 3
  %395 = load i32, i32* %arrayidx418, align 4
  %call419 = call i32 @rotr32(i32 %395, i32 6)
  %396 = load i32*, i32** %S.addr, align 4
  %arrayidx420 = getelementptr i32, i32* %396, i32 3
  %397 = load i32, i32* %arrayidx420, align 4
  %call421 = call i32 @rotr32(i32 %397, i32 11)
  %xor422 = xor i32 %call419, %call421
  %398 = load i32*, i32** %S.addr, align 4
  %arrayidx423 = getelementptr i32, i32* %398, i32 3
  %399 = load i32, i32* %arrayidx423, align 4
  %call424 = call i32 @rotr32(i32 %399, i32 25)
  %xor425 = xor i32 %xor422, %call424
  %400 = load i32*, i32** %S.addr, align 4
  %arrayidx426 = getelementptr i32, i32* %400, i32 3
  %401 = load i32, i32* %arrayidx426, align 4
  %402 = load i32*, i32** %S.addr, align 4
  %arrayidx427 = getelementptr i32, i32* %402, i32 4
  %403 = load i32, i32* %arrayidx427, align 4
  %404 = load i32*, i32** %S.addr, align 4
  %arrayidx428 = getelementptr i32, i32* %404, i32 5
  %405 = load i32, i32* %arrayidx428, align 4
  %xor429 = xor i32 %403, %405
  %and430 = and i32 %401, %xor429
  %406 = load i32*, i32** %S.addr, align 4
  %arrayidx431 = getelementptr i32, i32* %406, i32 5
  %407 = load i32, i32* %arrayidx431, align 4
  %xor432 = xor i32 %and430, %407
  %add433 = add i32 %xor425, %xor432
  %408 = load i32*, i32** %W.addr, align 4
  %409 = load i32, i32* %i, align 4
  %add434 = add i32 9, %409
  %arrayidx435 = getelementptr i32, i32* %408, i32 %add434
  %410 = load i32, i32* %arrayidx435, align 4
  %add436 = add i32 %add433, %410
  %411 = load i32, i32* %i, align 4
  %add437 = add i32 9, %411
  %arrayidx438 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add437
  %412 = load i32, i32* %arrayidx438, align 4
  %add439 = add i32 %add436, %412
  %413 = load i32*, i32** %S.addr, align 4
  %arrayidx440 = getelementptr i32, i32* %413, i32 6
  %414 = load i32, i32* %arrayidx440, align 4
  %add441 = add i32 %414, %add439
  store i32 %add441, i32* %arrayidx440, align 4
  %415 = load i32*, i32** %S.addr, align 4
  %arrayidx442 = getelementptr i32, i32* %415, i32 6
  %416 = load i32, i32* %arrayidx442, align 4
  %417 = load i32*, i32** %S.addr, align 4
  %arrayidx443 = getelementptr i32, i32* %417, i32 2
  %418 = load i32, i32* %arrayidx443, align 4
  %add444 = add i32 %418, %416
  store i32 %add444, i32* %arrayidx443, align 4
  %419 = load i32*, i32** %S.addr, align 4
  %arrayidx445 = getelementptr i32, i32* %419, i32 7
  %420 = load i32, i32* %arrayidx445, align 4
  %call446 = call i32 @rotr32(i32 %420, i32 2)
  %421 = load i32*, i32** %S.addr, align 4
  %arrayidx447 = getelementptr i32, i32* %421, i32 7
  %422 = load i32, i32* %arrayidx447, align 4
  %call448 = call i32 @rotr32(i32 %422, i32 13)
  %xor449 = xor i32 %call446, %call448
  %423 = load i32*, i32** %S.addr, align 4
  %arrayidx450 = getelementptr i32, i32* %423, i32 7
  %424 = load i32, i32* %arrayidx450, align 4
  %call451 = call i32 @rotr32(i32 %424, i32 22)
  %xor452 = xor i32 %xor449, %call451
  %425 = load i32*, i32** %S.addr, align 4
  %arrayidx453 = getelementptr i32, i32* %425, i32 7
  %426 = load i32, i32* %arrayidx453, align 4
  %427 = load i32*, i32** %S.addr, align 4
  %arrayidx454 = getelementptr i32, i32* %427, i32 0
  %428 = load i32, i32* %arrayidx454, align 4
  %429 = load i32*, i32** %S.addr, align 4
  %arrayidx455 = getelementptr i32, i32* %429, i32 1
  %430 = load i32, i32* %arrayidx455, align 4
  %or456 = or i32 %428, %430
  %and457 = and i32 %426, %or456
  %431 = load i32*, i32** %S.addr, align 4
  %arrayidx458 = getelementptr i32, i32* %431, i32 0
  %432 = load i32, i32* %arrayidx458, align 4
  %433 = load i32*, i32** %S.addr, align 4
  %arrayidx459 = getelementptr i32, i32* %433, i32 1
  %434 = load i32, i32* %arrayidx459, align 4
  %and460 = and i32 %432, %434
  %or461 = or i32 %and457, %and460
  %add462 = add i32 %xor452, %or461
  %435 = load i32*, i32** %S.addr, align 4
  %arrayidx463 = getelementptr i32, i32* %435, i32 6
  %436 = load i32, i32* %arrayidx463, align 4
  %add464 = add i32 %436, %add462
  store i32 %add464, i32* %arrayidx463, align 4
  %437 = load i32*, i32** %S.addr, align 4
  %arrayidx465 = getelementptr i32, i32* %437, i32 2
  %438 = load i32, i32* %arrayidx465, align 4
  %call466 = call i32 @rotr32(i32 %438, i32 6)
  %439 = load i32*, i32** %S.addr, align 4
  %arrayidx467 = getelementptr i32, i32* %439, i32 2
  %440 = load i32, i32* %arrayidx467, align 4
  %call468 = call i32 @rotr32(i32 %440, i32 11)
  %xor469 = xor i32 %call466, %call468
  %441 = load i32*, i32** %S.addr, align 4
  %arrayidx470 = getelementptr i32, i32* %441, i32 2
  %442 = load i32, i32* %arrayidx470, align 4
  %call471 = call i32 @rotr32(i32 %442, i32 25)
  %xor472 = xor i32 %xor469, %call471
  %443 = load i32*, i32** %S.addr, align 4
  %arrayidx473 = getelementptr i32, i32* %443, i32 2
  %444 = load i32, i32* %arrayidx473, align 4
  %445 = load i32*, i32** %S.addr, align 4
  %arrayidx474 = getelementptr i32, i32* %445, i32 3
  %446 = load i32, i32* %arrayidx474, align 4
  %447 = load i32*, i32** %S.addr, align 4
  %arrayidx475 = getelementptr i32, i32* %447, i32 4
  %448 = load i32, i32* %arrayidx475, align 4
  %xor476 = xor i32 %446, %448
  %and477 = and i32 %444, %xor476
  %449 = load i32*, i32** %S.addr, align 4
  %arrayidx478 = getelementptr i32, i32* %449, i32 4
  %450 = load i32, i32* %arrayidx478, align 4
  %xor479 = xor i32 %and477, %450
  %add480 = add i32 %xor472, %xor479
  %451 = load i32*, i32** %W.addr, align 4
  %452 = load i32, i32* %i, align 4
  %add481 = add i32 10, %452
  %arrayidx482 = getelementptr i32, i32* %451, i32 %add481
  %453 = load i32, i32* %arrayidx482, align 4
  %add483 = add i32 %add480, %453
  %454 = load i32, i32* %i, align 4
  %add484 = add i32 10, %454
  %arrayidx485 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add484
  %455 = load i32, i32* %arrayidx485, align 4
  %add486 = add i32 %add483, %455
  %456 = load i32*, i32** %S.addr, align 4
  %arrayidx487 = getelementptr i32, i32* %456, i32 5
  %457 = load i32, i32* %arrayidx487, align 4
  %add488 = add i32 %457, %add486
  store i32 %add488, i32* %arrayidx487, align 4
  %458 = load i32*, i32** %S.addr, align 4
  %arrayidx489 = getelementptr i32, i32* %458, i32 5
  %459 = load i32, i32* %arrayidx489, align 4
  %460 = load i32*, i32** %S.addr, align 4
  %arrayidx490 = getelementptr i32, i32* %460, i32 1
  %461 = load i32, i32* %arrayidx490, align 4
  %add491 = add i32 %461, %459
  store i32 %add491, i32* %arrayidx490, align 4
  %462 = load i32*, i32** %S.addr, align 4
  %arrayidx492 = getelementptr i32, i32* %462, i32 6
  %463 = load i32, i32* %arrayidx492, align 4
  %call493 = call i32 @rotr32(i32 %463, i32 2)
  %464 = load i32*, i32** %S.addr, align 4
  %arrayidx494 = getelementptr i32, i32* %464, i32 6
  %465 = load i32, i32* %arrayidx494, align 4
  %call495 = call i32 @rotr32(i32 %465, i32 13)
  %xor496 = xor i32 %call493, %call495
  %466 = load i32*, i32** %S.addr, align 4
  %arrayidx497 = getelementptr i32, i32* %466, i32 6
  %467 = load i32, i32* %arrayidx497, align 4
  %call498 = call i32 @rotr32(i32 %467, i32 22)
  %xor499 = xor i32 %xor496, %call498
  %468 = load i32*, i32** %S.addr, align 4
  %arrayidx500 = getelementptr i32, i32* %468, i32 6
  %469 = load i32, i32* %arrayidx500, align 4
  %470 = load i32*, i32** %S.addr, align 4
  %arrayidx501 = getelementptr i32, i32* %470, i32 7
  %471 = load i32, i32* %arrayidx501, align 4
  %472 = load i32*, i32** %S.addr, align 4
  %arrayidx502 = getelementptr i32, i32* %472, i32 0
  %473 = load i32, i32* %arrayidx502, align 4
  %or503 = or i32 %471, %473
  %and504 = and i32 %469, %or503
  %474 = load i32*, i32** %S.addr, align 4
  %arrayidx505 = getelementptr i32, i32* %474, i32 7
  %475 = load i32, i32* %arrayidx505, align 4
  %476 = load i32*, i32** %S.addr, align 4
  %arrayidx506 = getelementptr i32, i32* %476, i32 0
  %477 = load i32, i32* %arrayidx506, align 4
  %and507 = and i32 %475, %477
  %or508 = or i32 %and504, %and507
  %add509 = add i32 %xor499, %or508
  %478 = load i32*, i32** %S.addr, align 4
  %arrayidx510 = getelementptr i32, i32* %478, i32 5
  %479 = load i32, i32* %arrayidx510, align 4
  %add511 = add i32 %479, %add509
  store i32 %add511, i32* %arrayidx510, align 4
  %480 = load i32*, i32** %S.addr, align 4
  %arrayidx512 = getelementptr i32, i32* %480, i32 1
  %481 = load i32, i32* %arrayidx512, align 4
  %call513 = call i32 @rotr32(i32 %481, i32 6)
  %482 = load i32*, i32** %S.addr, align 4
  %arrayidx514 = getelementptr i32, i32* %482, i32 1
  %483 = load i32, i32* %arrayidx514, align 4
  %call515 = call i32 @rotr32(i32 %483, i32 11)
  %xor516 = xor i32 %call513, %call515
  %484 = load i32*, i32** %S.addr, align 4
  %arrayidx517 = getelementptr i32, i32* %484, i32 1
  %485 = load i32, i32* %arrayidx517, align 4
  %call518 = call i32 @rotr32(i32 %485, i32 25)
  %xor519 = xor i32 %xor516, %call518
  %486 = load i32*, i32** %S.addr, align 4
  %arrayidx520 = getelementptr i32, i32* %486, i32 1
  %487 = load i32, i32* %arrayidx520, align 4
  %488 = load i32*, i32** %S.addr, align 4
  %arrayidx521 = getelementptr i32, i32* %488, i32 2
  %489 = load i32, i32* %arrayidx521, align 4
  %490 = load i32*, i32** %S.addr, align 4
  %arrayidx522 = getelementptr i32, i32* %490, i32 3
  %491 = load i32, i32* %arrayidx522, align 4
  %xor523 = xor i32 %489, %491
  %and524 = and i32 %487, %xor523
  %492 = load i32*, i32** %S.addr, align 4
  %arrayidx525 = getelementptr i32, i32* %492, i32 3
  %493 = load i32, i32* %arrayidx525, align 4
  %xor526 = xor i32 %and524, %493
  %add527 = add i32 %xor519, %xor526
  %494 = load i32*, i32** %W.addr, align 4
  %495 = load i32, i32* %i, align 4
  %add528 = add i32 11, %495
  %arrayidx529 = getelementptr i32, i32* %494, i32 %add528
  %496 = load i32, i32* %arrayidx529, align 4
  %add530 = add i32 %add527, %496
  %497 = load i32, i32* %i, align 4
  %add531 = add i32 11, %497
  %arrayidx532 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add531
  %498 = load i32, i32* %arrayidx532, align 4
  %add533 = add i32 %add530, %498
  %499 = load i32*, i32** %S.addr, align 4
  %arrayidx534 = getelementptr i32, i32* %499, i32 4
  %500 = load i32, i32* %arrayidx534, align 4
  %add535 = add i32 %500, %add533
  store i32 %add535, i32* %arrayidx534, align 4
  %501 = load i32*, i32** %S.addr, align 4
  %arrayidx536 = getelementptr i32, i32* %501, i32 4
  %502 = load i32, i32* %arrayidx536, align 4
  %503 = load i32*, i32** %S.addr, align 4
  %arrayidx537 = getelementptr i32, i32* %503, i32 0
  %504 = load i32, i32* %arrayidx537, align 4
  %add538 = add i32 %504, %502
  store i32 %add538, i32* %arrayidx537, align 4
  %505 = load i32*, i32** %S.addr, align 4
  %arrayidx539 = getelementptr i32, i32* %505, i32 5
  %506 = load i32, i32* %arrayidx539, align 4
  %call540 = call i32 @rotr32(i32 %506, i32 2)
  %507 = load i32*, i32** %S.addr, align 4
  %arrayidx541 = getelementptr i32, i32* %507, i32 5
  %508 = load i32, i32* %arrayidx541, align 4
  %call542 = call i32 @rotr32(i32 %508, i32 13)
  %xor543 = xor i32 %call540, %call542
  %509 = load i32*, i32** %S.addr, align 4
  %arrayidx544 = getelementptr i32, i32* %509, i32 5
  %510 = load i32, i32* %arrayidx544, align 4
  %call545 = call i32 @rotr32(i32 %510, i32 22)
  %xor546 = xor i32 %xor543, %call545
  %511 = load i32*, i32** %S.addr, align 4
  %arrayidx547 = getelementptr i32, i32* %511, i32 5
  %512 = load i32, i32* %arrayidx547, align 4
  %513 = load i32*, i32** %S.addr, align 4
  %arrayidx548 = getelementptr i32, i32* %513, i32 6
  %514 = load i32, i32* %arrayidx548, align 4
  %515 = load i32*, i32** %S.addr, align 4
  %arrayidx549 = getelementptr i32, i32* %515, i32 7
  %516 = load i32, i32* %arrayidx549, align 4
  %or550 = or i32 %514, %516
  %and551 = and i32 %512, %or550
  %517 = load i32*, i32** %S.addr, align 4
  %arrayidx552 = getelementptr i32, i32* %517, i32 6
  %518 = load i32, i32* %arrayidx552, align 4
  %519 = load i32*, i32** %S.addr, align 4
  %arrayidx553 = getelementptr i32, i32* %519, i32 7
  %520 = load i32, i32* %arrayidx553, align 4
  %and554 = and i32 %518, %520
  %or555 = or i32 %and551, %and554
  %add556 = add i32 %xor546, %or555
  %521 = load i32*, i32** %S.addr, align 4
  %arrayidx557 = getelementptr i32, i32* %521, i32 4
  %522 = load i32, i32* %arrayidx557, align 4
  %add558 = add i32 %522, %add556
  store i32 %add558, i32* %arrayidx557, align 4
  %523 = load i32*, i32** %S.addr, align 4
  %arrayidx559 = getelementptr i32, i32* %523, i32 0
  %524 = load i32, i32* %arrayidx559, align 4
  %call560 = call i32 @rotr32(i32 %524, i32 6)
  %525 = load i32*, i32** %S.addr, align 4
  %arrayidx561 = getelementptr i32, i32* %525, i32 0
  %526 = load i32, i32* %arrayidx561, align 4
  %call562 = call i32 @rotr32(i32 %526, i32 11)
  %xor563 = xor i32 %call560, %call562
  %527 = load i32*, i32** %S.addr, align 4
  %arrayidx564 = getelementptr i32, i32* %527, i32 0
  %528 = load i32, i32* %arrayidx564, align 4
  %call565 = call i32 @rotr32(i32 %528, i32 25)
  %xor566 = xor i32 %xor563, %call565
  %529 = load i32*, i32** %S.addr, align 4
  %arrayidx567 = getelementptr i32, i32* %529, i32 0
  %530 = load i32, i32* %arrayidx567, align 4
  %531 = load i32*, i32** %S.addr, align 4
  %arrayidx568 = getelementptr i32, i32* %531, i32 1
  %532 = load i32, i32* %arrayidx568, align 4
  %533 = load i32*, i32** %S.addr, align 4
  %arrayidx569 = getelementptr i32, i32* %533, i32 2
  %534 = load i32, i32* %arrayidx569, align 4
  %xor570 = xor i32 %532, %534
  %and571 = and i32 %530, %xor570
  %535 = load i32*, i32** %S.addr, align 4
  %arrayidx572 = getelementptr i32, i32* %535, i32 2
  %536 = load i32, i32* %arrayidx572, align 4
  %xor573 = xor i32 %and571, %536
  %add574 = add i32 %xor566, %xor573
  %537 = load i32*, i32** %W.addr, align 4
  %538 = load i32, i32* %i, align 4
  %add575 = add i32 12, %538
  %arrayidx576 = getelementptr i32, i32* %537, i32 %add575
  %539 = load i32, i32* %arrayidx576, align 4
  %add577 = add i32 %add574, %539
  %540 = load i32, i32* %i, align 4
  %add578 = add i32 12, %540
  %arrayidx579 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add578
  %541 = load i32, i32* %arrayidx579, align 4
  %add580 = add i32 %add577, %541
  %542 = load i32*, i32** %S.addr, align 4
  %arrayidx581 = getelementptr i32, i32* %542, i32 3
  %543 = load i32, i32* %arrayidx581, align 4
  %add582 = add i32 %543, %add580
  store i32 %add582, i32* %arrayidx581, align 4
  %544 = load i32*, i32** %S.addr, align 4
  %arrayidx583 = getelementptr i32, i32* %544, i32 3
  %545 = load i32, i32* %arrayidx583, align 4
  %546 = load i32*, i32** %S.addr, align 4
  %arrayidx584 = getelementptr i32, i32* %546, i32 7
  %547 = load i32, i32* %arrayidx584, align 4
  %add585 = add i32 %547, %545
  store i32 %add585, i32* %arrayidx584, align 4
  %548 = load i32*, i32** %S.addr, align 4
  %arrayidx586 = getelementptr i32, i32* %548, i32 4
  %549 = load i32, i32* %arrayidx586, align 4
  %call587 = call i32 @rotr32(i32 %549, i32 2)
  %550 = load i32*, i32** %S.addr, align 4
  %arrayidx588 = getelementptr i32, i32* %550, i32 4
  %551 = load i32, i32* %arrayidx588, align 4
  %call589 = call i32 @rotr32(i32 %551, i32 13)
  %xor590 = xor i32 %call587, %call589
  %552 = load i32*, i32** %S.addr, align 4
  %arrayidx591 = getelementptr i32, i32* %552, i32 4
  %553 = load i32, i32* %arrayidx591, align 4
  %call592 = call i32 @rotr32(i32 %553, i32 22)
  %xor593 = xor i32 %xor590, %call592
  %554 = load i32*, i32** %S.addr, align 4
  %arrayidx594 = getelementptr i32, i32* %554, i32 4
  %555 = load i32, i32* %arrayidx594, align 4
  %556 = load i32*, i32** %S.addr, align 4
  %arrayidx595 = getelementptr i32, i32* %556, i32 5
  %557 = load i32, i32* %arrayidx595, align 4
  %558 = load i32*, i32** %S.addr, align 4
  %arrayidx596 = getelementptr i32, i32* %558, i32 6
  %559 = load i32, i32* %arrayidx596, align 4
  %or597 = or i32 %557, %559
  %and598 = and i32 %555, %or597
  %560 = load i32*, i32** %S.addr, align 4
  %arrayidx599 = getelementptr i32, i32* %560, i32 5
  %561 = load i32, i32* %arrayidx599, align 4
  %562 = load i32*, i32** %S.addr, align 4
  %arrayidx600 = getelementptr i32, i32* %562, i32 6
  %563 = load i32, i32* %arrayidx600, align 4
  %and601 = and i32 %561, %563
  %or602 = or i32 %and598, %and601
  %add603 = add i32 %xor593, %or602
  %564 = load i32*, i32** %S.addr, align 4
  %arrayidx604 = getelementptr i32, i32* %564, i32 3
  %565 = load i32, i32* %arrayidx604, align 4
  %add605 = add i32 %565, %add603
  store i32 %add605, i32* %arrayidx604, align 4
  %566 = load i32*, i32** %S.addr, align 4
  %arrayidx606 = getelementptr i32, i32* %566, i32 7
  %567 = load i32, i32* %arrayidx606, align 4
  %call607 = call i32 @rotr32(i32 %567, i32 6)
  %568 = load i32*, i32** %S.addr, align 4
  %arrayidx608 = getelementptr i32, i32* %568, i32 7
  %569 = load i32, i32* %arrayidx608, align 4
  %call609 = call i32 @rotr32(i32 %569, i32 11)
  %xor610 = xor i32 %call607, %call609
  %570 = load i32*, i32** %S.addr, align 4
  %arrayidx611 = getelementptr i32, i32* %570, i32 7
  %571 = load i32, i32* %arrayidx611, align 4
  %call612 = call i32 @rotr32(i32 %571, i32 25)
  %xor613 = xor i32 %xor610, %call612
  %572 = load i32*, i32** %S.addr, align 4
  %arrayidx614 = getelementptr i32, i32* %572, i32 7
  %573 = load i32, i32* %arrayidx614, align 4
  %574 = load i32*, i32** %S.addr, align 4
  %arrayidx615 = getelementptr i32, i32* %574, i32 0
  %575 = load i32, i32* %arrayidx615, align 4
  %576 = load i32*, i32** %S.addr, align 4
  %arrayidx616 = getelementptr i32, i32* %576, i32 1
  %577 = load i32, i32* %arrayidx616, align 4
  %xor617 = xor i32 %575, %577
  %and618 = and i32 %573, %xor617
  %578 = load i32*, i32** %S.addr, align 4
  %arrayidx619 = getelementptr i32, i32* %578, i32 1
  %579 = load i32, i32* %arrayidx619, align 4
  %xor620 = xor i32 %and618, %579
  %add621 = add i32 %xor613, %xor620
  %580 = load i32*, i32** %W.addr, align 4
  %581 = load i32, i32* %i, align 4
  %add622 = add i32 13, %581
  %arrayidx623 = getelementptr i32, i32* %580, i32 %add622
  %582 = load i32, i32* %arrayidx623, align 4
  %add624 = add i32 %add621, %582
  %583 = load i32, i32* %i, align 4
  %add625 = add i32 13, %583
  %arrayidx626 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add625
  %584 = load i32, i32* %arrayidx626, align 4
  %add627 = add i32 %add624, %584
  %585 = load i32*, i32** %S.addr, align 4
  %arrayidx628 = getelementptr i32, i32* %585, i32 2
  %586 = load i32, i32* %arrayidx628, align 4
  %add629 = add i32 %586, %add627
  store i32 %add629, i32* %arrayidx628, align 4
  %587 = load i32*, i32** %S.addr, align 4
  %arrayidx630 = getelementptr i32, i32* %587, i32 2
  %588 = load i32, i32* %arrayidx630, align 4
  %589 = load i32*, i32** %S.addr, align 4
  %arrayidx631 = getelementptr i32, i32* %589, i32 6
  %590 = load i32, i32* %arrayidx631, align 4
  %add632 = add i32 %590, %588
  store i32 %add632, i32* %arrayidx631, align 4
  %591 = load i32*, i32** %S.addr, align 4
  %arrayidx633 = getelementptr i32, i32* %591, i32 3
  %592 = load i32, i32* %arrayidx633, align 4
  %call634 = call i32 @rotr32(i32 %592, i32 2)
  %593 = load i32*, i32** %S.addr, align 4
  %arrayidx635 = getelementptr i32, i32* %593, i32 3
  %594 = load i32, i32* %arrayidx635, align 4
  %call636 = call i32 @rotr32(i32 %594, i32 13)
  %xor637 = xor i32 %call634, %call636
  %595 = load i32*, i32** %S.addr, align 4
  %arrayidx638 = getelementptr i32, i32* %595, i32 3
  %596 = load i32, i32* %arrayidx638, align 4
  %call639 = call i32 @rotr32(i32 %596, i32 22)
  %xor640 = xor i32 %xor637, %call639
  %597 = load i32*, i32** %S.addr, align 4
  %arrayidx641 = getelementptr i32, i32* %597, i32 3
  %598 = load i32, i32* %arrayidx641, align 4
  %599 = load i32*, i32** %S.addr, align 4
  %arrayidx642 = getelementptr i32, i32* %599, i32 4
  %600 = load i32, i32* %arrayidx642, align 4
  %601 = load i32*, i32** %S.addr, align 4
  %arrayidx643 = getelementptr i32, i32* %601, i32 5
  %602 = load i32, i32* %arrayidx643, align 4
  %or644 = or i32 %600, %602
  %and645 = and i32 %598, %or644
  %603 = load i32*, i32** %S.addr, align 4
  %arrayidx646 = getelementptr i32, i32* %603, i32 4
  %604 = load i32, i32* %arrayidx646, align 4
  %605 = load i32*, i32** %S.addr, align 4
  %arrayidx647 = getelementptr i32, i32* %605, i32 5
  %606 = load i32, i32* %arrayidx647, align 4
  %and648 = and i32 %604, %606
  %or649 = or i32 %and645, %and648
  %add650 = add i32 %xor640, %or649
  %607 = load i32*, i32** %S.addr, align 4
  %arrayidx651 = getelementptr i32, i32* %607, i32 2
  %608 = load i32, i32* %arrayidx651, align 4
  %add652 = add i32 %608, %add650
  store i32 %add652, i32* %arrayidx651, align 4
  %609 = load i32*, i32** %S.addr, align 4
  %arrayidx653 = getelementptr i32, i32* %609, i32 6
  %610 = load i32, i32* %arrayidx653, align 4
  %call654 = call i32 @rotr32(i32 %610, i32 6)
  %611 = load i32*, i32** %S.addr, align 4
  %arrayidx655 = getelementptr i32, i32* %611, i32 6
  %612 = load i32, i32* %arrayidx655, align 4
  %call656 = call i32 @rotr32(i32 %612, i32 11)
  %xor657 = xor i32 %call654, %call656
  %613 = load i32*, i32** %S.addr, align 4
  %arrayidx658 = getelementptr i32, i32* %613, i32 6
  %614 = load i32, i32* %arrayidx658, align 4
  %call659 = call i32 @rotr32(i32 %614, i32 25)
  %xor660 = xor i32 %xor657, %call659
  %615 = load i32*, i32** %S.addr, align 4
  %arrayidx661 = getelementptr i32, i32* %615, i32 6
  %616 = load i32, i32* %arrayidx661, align 4
  %617 = load i32*, i32** %S.addr, align 4
  %arrayidx662 = getelementptr i32, i32* %617, i32 7
  %618 = load i32, i32* %arrayidx662, align 4
  %619 = load i32*, i32** %S.addr, align 4
  %arrayidx663 = getelementptr i32, i32* %619, i32 0
  %620 = load i32, i32* %arrayidx663, align 4
  %xor664 = xor i32 %618, %620
  %and665 = and i32 %616, %xor664
  %621 = load i32*, i32** %S.addr, align 4
  %arrayidx666 = getelementptr i32, i32* %621, i32 0
  %622 = load i32, i32* %arrayidx666, align 4
  %xor667 = xor i32 %and665, %622
  %add668 = add i32 %xor660, %xor667
  %623 = load i32*, i32** %W.addr, align 4
  %624 = load i32, i32* %i, align 4
  %add669 = add i32 14, %624
  %arrayidx670 = getelementptr i32, i32* %623, i32 %add669
  %625 = load i32, i32* %arrayidx670, align 4
  %add671 = add i32 %add668, %625
  %626 = load i32, i32* %i, align 4
  %add672 = add i32 14, %626
  %arrayidx673 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add672
  %627 = load i32, i32* %arrayidx673, align 4
  %add674 = add i32 %add671, %627
  %628 = load i32*, i32** %S.addr, align 4
  %arrayidx675 = getelementptr i32, i32* %628, i32 1
  %629 = load i32, i32* %arrayidx675, align 4
  %add676 = add i32 %629, %add674
  store i32 %add676, i32* %arrayidx675, align 4
  %630 = load i32*, i32** %S.addr, align 4
  %arrayidx677 = getelementptr i32, i32* %630, i32 1
  %631 = load i32, i32* %arrayidx677, align 4
  %632 = load i32*, i32** %S.addr, align 4
  %arrayidx678 = getelementptr i32, i32* %632, i32 5
  %633 = load i32, i32* %arrayidx678, align 4
  %add679 = add i32 %633, %631
  store i32 %add679, i32* %arrayidx678, align 4
  %634 = load i32*, i32** %S.addr, align 4
  %arrayidx680 = getelementptr i32, i32* %634, i32 2
  %635 = load i32, i32* %arrayidx680, align 4
  %call681 = call i32 @rotr32(i32 %635, i32 2)
  %636 = load i32*, i32** %S.addr, align 4
  %arrayidx682 = getelementptr i32, i32* %636, i32 2
  %637 = load i32, i32* %arrayidx682, align 4
  %call683 = call i32 @rotr32(i32 %637, i32 13)
  %xor684 = xor i32 %call681, %call683
  %638 = load i32*, i32** %S.addr, align 4
  %arrayidx685 = getelementptr i32, i32* %638, i32 2
  %639 = load i32, i32* %arrayidx685, align 4
  %call686 = call i32 @rotr32(i32 %639, i32 22)
  %xor687 = xor i32 %xor684, %call686
  %640 = load i32*, i32** %S.addr, align 4
  %arrayidx688 = getelementptr i32, i32* %640, i32 2
  %641 = load i32, i32* %arrayidx688, align 4
  %642 = load i32*, i32** %S.addr, align 4
  %arrayidx689 = getelementptr i32, i32* %642, i32 3
  %643 = load i32, i32* %arrayidx689, align 4
  %644 = load i32*, i32** %S.addr, align 4
  %arrayidx690 = getelementptr i32, i32* %644, i32 4
  %645 = load i32, i32* %arrayidx690, align 4
  %or691 = or i32 %643, %645
  %and692 = and i32 %641, %or691
  %646 = load i32*, i32** %S.addr, align 4
  %arrayidx693 = getelementptr i32, i32* %646, i32 3
  %647 = load i32, i32* %arrayidx693, align 4
  %648 = load i32*, i32** %S.addr, align 4
  %arrayidx694 = getelementptr i32, i32* %648, i32 4
  %649 = load i32, i32* %arrayidx694, align 4
  %and695 = and i32 %647, %649
  %or696 = or i32 %and692, %and695
  %add697 = add i32 %xor687, %or696
  %650 = load i32*, i32** %S.addr, align 4
  %arrayidx698 = getelementptr i32, i32* %650, i32 1
  %651 = load i32, i32* %arrayidx698, align 4
  %add699 = add i32 %651, %add697
  store i32 %add699, i32* %arrayidx698, align 4
  %652 = load i32*, i32** %S.addr, align 4
  %arrayidx700 = getelementptr i32, i32* %652, i32 5
  %653 = load i32, i32* %arrayidx700, align 4
  %call701 = call i32 @rotr32(i32 %653, i32 6)
  %654 = load i32*, i32** %S.addr, align 4
  %arrayidx702 = getelementptr i32, i32* %654, i32 5
  %655 = load i32, i32* %arrayidx702, align 4
  %call703 = call i32 @rotr32(i32 %655, i32 11)
  %xor704 = xor i32 %call701, %call703
  %656 = load i32*, i32** %S.addr, align 4
  %arrayidx705 = getelementptr i32, i32* %656, i32 5
  %657 = load i32, i32* %arrayidx705, align 4
  %call706 = call i32 @rotr32(i32 %657, i32 25)
  %xor707 = xor i32 %xor704, %call706
  %658 = load i32*, i32** %S.addr, align 4
  %arrayidx708 = getelementptr i32, i32* %658, i32 5
  %659 = load i32, i32* %arrayidx708, align 4
  %660 = load i32*, i32** %S.addr, align 4
  %arrayidx709 = getelementptr i32, i32* %660, i32 6
  %661 = load i32, i32* %arrayidx709, align 4
  %662 = load i32*, i32** %S.addr, align 4
  %arrayidx710 = getelementptr i32, i32* %662, i32 7
  %663 = load i32, i32* %arrayidx710, align 4
  %xor711 = xor i32 %661, %663
  %and712 = and i32 %659, %xor711
  %664 = load i32*, i32** %S.addr, align 4
  %arrayidx713 = getelementptr i32, i32* %664, i32 7
  %665 = load i32, i32* %arrayidx713, align 4
  %xor714 = xor i32 %and712, %665
  %add715 = add i32 %xor707, %xor714
  %666 = load i32*, i32** %W.addr, align 4
  %667 = load i32, i32* %i, align 4
  %add716 = add i32 15, %667
  %arrayidx717 = getelementptr i32, i32* %666, i32 %add716
  %668 = load i32, i32* %arrayidx717, align 4
  %add718 = add i32 %add715, %668
  %669 = load i32, i32* %i, align 4
  %add719 = add i32 15, %669
  %arrayidx720 = getelementptr [64 x i32], [64 x i32]* @Krnd, i32 0, i32 %add719
  %670 = load i32, i32* %arrayidx720, align 4
  %add721 = add i32 %add718, %670
  %671 = load i32*, i32** %S.addr, align 4
  %arrayidx722 = getelementptr i32, i32* %671, i32 0
  %672 = load i32, i32* %arrayidx722, align 4
  %add723 = add i32 %672, %add721
  store i32 %add723, i32* %arrayidx722, align 4
  %673 = load i32*, i32** %S.addr, align 4
  %arrayidx724 = getelementptr i32, i32* %673, i32 0
  %674 = load i32, i32* %arrayidx724, align 4
  %675 = load i32*, i32** %S.addr, align 4
  %arrayidx725 = getelementptr i32, i32* %675, i32 4
  %676 = load i32, i32* %arrayidx725, align 4
  %add726 = add i32 %676, %674
  store i32 %add726, i32* %arrayidx725, align 4
  %677 = load i32*, i32** %S.addr, align 4
  %arrayidx727 = getelementptr i32, i32* %677, i32 1
  %678 = load i32, i32* %arrayidx727, align 4
  %call728 = call i32 @rotr32(i32 %678, i32 2)
  %679 = load i32*, i32** %S.addr, align 4
  %arrayidx729 = getelementptr i32, i32* %679, i32 1
  %680 = load i32, i32* %arrayidx729, align 4
  %call730 = call i32 @rotr32(i32 %680, i32 13)
  %xor731 = xor i32 %call728, %call730
  %681 = load i32*, i32** %S.addr, align 4
  %arrayidx732 = getelementptr i32, i32* %681, i32 1
  %682 = load i32, i32* %arrayidx732, align 4
  %call733 = call i32 @rotr32(i32 %682, i32 22)
  %xor734 = xor i32 %xor731, %call733
  %683 = load i32*, i32** %S.addr, align 4
  %arrayidx735 = getelementptr i32, i32* %683, i32 1
  %684 = load i32, i32* %arrayidx735, align 4
  %685 = load i32*, i32** %S.addr, align 4
  %arrayidx736 = getelementptr i32, i32* %685, i32 2
  %686 = load i32, i32* %arrayidx736, align 4
  %687 = load i32*, i32** %S.addr, align 4
  %arrayidx737 = getelementptr i32, i32* %687, i32 3
  %688 = load i32, i32* %arrayidx737, align 4
  %or738 = or i32 %686, %688
  %and739 = and i32 %684, %or738
  %689 = load i32*, i32** %S.addr, align 4
  %arrayidx740 = getelementptr i32, i32* %689, i32 2
  %690 = load i32, i32* %arrayidx740, align 4
  %691 = load i32*, i32** %S.addr, align 4
  %arrayidx741 = getelementptr i32, i32* %691, i32 3
  %692 = load i32, i32* %arrayidx741, align 4
  %and742 = and i32 %690, %692
  %or743 = or i32 %and739, %and742
  %add744 = add i32 %xor734, %or743
  %693 = load i32*, i32** %S.addr, align 4
  %arrayidx745 = getelementptr i32, i32* %693, i32 0
  %694 = load i32, i32* %arrayidx745, align 4
  %add746 = add i32 %694, %add744
  store i32 %add746, i32* %arrayidx745, align 4
  %695 = load i32, i32* %i, align 4
  %cmp747 = icmp eq i32 %695, 48
  br i1 %cmp747, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.end

if.end:                                           ; preds = %for.body
  %696 = load i32*, i32** %W.addr, align 4
  %697 = load i32, i32* %i, align 4
  %add748 = add i32 %697, 0
  %add749 = add i32 %add748, 14
  %arrayidx750 = getelementptr i32, i32* %696, i32 %add749
  %698 = load i32, i32* %arrayidx750, align 4
  %call751 = call i32 @rotr32(i32 %698, i32 17)
  %699 = load i32*, i32** %W.addr, align 4
  %700 = load i32, i32* %i, align 4
  %add752 = add i32 %700, 0
  %add753 = add i32 %add752, 14
  %arrayidx754 = getelementptr i32, i32* %699, i32 %add753
  %701 = load i32, i32* %arrayidx754, align 4
  %call755 = call i32 @rotr32(i32 %701, i32 19)
  %xor756 = xor i32 %call751, %call755
  %702 = load i32*, i32** %W.addr, align 4
  %703 = load i32, i32* %i, align 4
  %add757 = add i32 %703, 0
  %add758 = add i32 %add757, 14
  %arrayidx759 = getelementptr i32, i32* %702, i32 %add758
  %704 = load i32, i32* %arrayidx759, align 4
  %shr = lshr i32 %704, 10
  %xor760 = xor i32 %xor756, %shr
  %705 = load i32*, i32** %W.addr, align 4
  %706 = load i32, i32* %i, align 4
  %add761 = add i32 %706, 0
  %add762 = add i32 %add761, 9
  %arrayidx763 = getelementptr i32, i32* %705, i32 %add762
  %707 = load i32, i32* %arrayidx763, align 4
  %add764 = add i32 %xor760, %707
  %708 = load i32*, i32** %W.addr, align 4
  %709 = load i32, i32* %i, align 4
  %add765 = add i32 %709, 0
  %add766 = add i32 %add765, 1
  %arrayidx767 = getelementptr i32, i32* %708, i32 %add766
  %710 = load i32, i32* %arrayidx767, align 4
  %call768 = call i32 @rotr32(i32 %710, i32 7)
  %711 = load i32*, i32** %W.addr, align 4
  %712 = load i32, i32* %i, align 4
  %add769 = add i32 %712, 0
  %add770 = add i32 %add769, 1
  %arrayidx771 = getelementptr i32, i32* %711, i32 %add770
  %713 = load i32, i32* %arrayidx771, align 4
  %call772 = call i32 @rotr32(i32 %713, i32 18)
  %xor773 = xor i32 %call768, %call772
  %714 = load i32*, i32** %W.addr, align 4
  %715 = load i32, i32* %i, align 4
  %add774 = add i32 %715, 0
  %add775 = add i32 %add774, 1
  %arrayidx776 = getelementptr i32, i32* %714, i32 %add775
  %716 = load i32, i32* %arrayidx776, align 4
  %shr777 = lshr i32 %716, 3
  %xor778 = xor i32 %xor773, %shr777
  %add779 = add i32 %add764, %xor778
  %717 = load i32*, i32** %W.addr, align 4
  %718 = load i32, i32* %i, align 4
  %add780 = add i32 %718, 0
  %arrayidx781 = getelementptr i32, i32* %717, i32 %add780
  %719 = load i32, i32* %arrayidx781, align 4
  %add782 = add i32 %add779, %719
  %720 = load i32*, i32** %W.addr, align 4
  %721 = load i32, i32* %i, align 4
  %add783 = add i32 %721, 0
  %add784 = add i32 %add783, 16
  %arrayidx785 = getelementptr i32, i32* %720, i32 %add784
  store i32 %add782, i32* %arrayidx785, align 4
  %722 = load i32*, i32** %W.addr, align 4
  %723 = load i32, i32* %i, align 4
  %add786 = add i32 %723, 1
  %add787 = add i32 %add786, 14
  %arrayidx788 = getelementptr i32, i32* %722, i32 %add787
  %724 = load i32, i32* %arrayidx788, align 4
  %call789 = call i32 @rotr32(i32 %724, i32 17)
  %725 = load i32*, i32** %W.addr, align 4
  %726 = load i32, i32* %i, align 4
  %add790 = add i32 %726, 1
  %add791 = add i32 %add790, 14
  %arrayidx792 = getelementptr i32, i32* %725, i32 %add791
  %727 = load i32, i32* %arrayidx792, align 4
  %call793 = call i32 @rotr32(i32 %727, i32 19)
  %xor794 = xor i32 %call789, %call793
  %728 = load i32*, i32** %W.addr, align 4
  %729 = load i32, i32* %i, align 4
  %add795 = add i32 %729, 1
  %add796 = add i32 %add795, 14
  %arrayidx797 = getelementptr i32, i32* %728, i32 %add796
  %730 = load i32, i32* %arrayidx797, align 4
  %shr798 = lshr i32 %730, 10
  %xor799 = xor i32 %xor794, %shr798
  %731 = load i32*, i32** %W.addr, align 4
  %732 = load i32, i32* %i, align 4
  %add800 = add i32 %732, 1
  %add801 = add i32 %add800, 9
  %arrayidx802 = getelementptr i32, i32* %731, i32 %add801
  %733 = load i32, i32* %arrayidx802, align 4
  %add803 = add i32 %xor799, %733
  %734 = load i32*, i32** %W.addr, align 4
  %735 = load i32, i32* %i, align 4
  %add804 = add i32 %735, 1
  %add805 = add i32 %add804, 1
  %arrayidx806 = getelementptr i32, i32* %734, i32 %add805
  %736 = load i32, i32* %arrayidx806, align 4
  %call807 = call i32 @rotr32(i32 %736, i32 7)
  %737 = load i32*, i32** %W.addr, align 4
  %738 = load i32, i32* %i, align 4
  %add808 = add i32 %738, 1
  %add809 = add i32 %add808, 1
  %arrayidx810 = getelementptr i32, i32* %737, i32 %add809
  %739 = load i32, i32* %arrayidx810, align 4
  %call811 = call i32 @rotr32(i32 %739, i32 18)
  %xor812 = xor i32 %call807, %call811
  %740 = load i32*, i32** %W.addr, align 4
  %741 = load i32, i32* %i, align 4
  %add813 = add i32 %741, 1
  %add814 = add i32 %add813, 1
  %arrayidx815 = getelementptr i32, i32* %740, i32 %add814
  %742 = load i32, i32* %arrayidx815, align 4
  %shr816 = lshr i32 %742, 3
  %xor817 = xor i32 %xor812, %shr816
  %add818 = add i32 %add803, %xor817
  %743 = load i32*, i32** %W.addr, align 4
  %744 = load i32, i32* %i, align 4
  %add819 = add i32 %744, 1
  %arrayidx820 = getelementptr i32, i32* %743, i32 %add819
  %745 = load i32, i32* %arrayidx820, align 4
  %add821 = add i32 %add818, %745
  %746 = load i32*, i32** %W.addr, align 4
  %747 = load i32, i32* %i, align 4
  %add822 = add i32 %747, 1
  %add823 = add i32 %add822, 16
  %arrayidx824 = getelementptr i32, i32* %746, i32 %add823
  store i32 %add821, i32* %arrayidx824, align 4
  %748 = load i32*, i32** %W.addr, align 4
  %749 = load i32, i32* %i, align 4
  %add825 = add i32 %749, 2
  %add826 = add i32 %add825, 14
  %arrayidx827 = getelementptr i32, i32* %748, i32 %add826
  %750 = load i32, i32* %arrayidx827, align 4
  %call828 = call i32 @rotr32(i32 %750, i32 17)
  %751 = load i32*, i32** %W.addr, align 4
  %752 = load i32, i32* %i, align 4
  %add829 = add i32 %752, 2
  %add830 = add i32 %add829, 14
  %arrayidx831 = getelementptr i32, i32* %751, i32 %add830
  %753 = load i32, i32* %arrayidx831, align 4
  %call832 = call i32 @rotr32(i32 %753, i32 19)
  %xor833 = xor i32 %call828, %call832
  %754 = load i32*, i32** %W.addr, align 4
  %755 = load i32, i32* %i, align 4
  %add834 = add i32 %755, 2
  %add835 = add i32 %add834, 14
  %arrayidx836 = getelementptr i32, i32* %754, i32 %add835
  %756 = load i32, i32* %arrayidx836, align 4
  %shr837 = lshr i32 %756, 10
  %xor838 = xor i32 %xor833, %shr837
  %757 = load i32*, i32** %W.addr, align 4
  %758 = load i32, i32* %i, align 4
  %add839 = add i32 %758, 2
  %add840 = add i32 %add839, 9
  %arrayidx841 = getelementptr i32, i32* %757, i32 %add840
  %759 = load i32, i32* %arrayidx841, align 4
  %add842 = add i32 %xor838, %759
  %760 = load i32*, i32** %W.addr, align 4
  %761 = load i32, i32* %i, align 4
  %add843 = add i32 %761, 2
  %add844 = add i32 %add843, 1
  %arrayidx845 = getelementptr i32, i32* %760, i32 %add844
  %762 = load i32, i32* %arrayidx845, align 4
  %call846 = call i32 @rotr32(i32 %762, i32 7)
  %763 = load i32*, i32** %W.addr, align 4
  %764 = load i32, i32* %i, align 4
  %add847 = add i32 %764, 2
  %add848 = add i32 %add847, 1
  %arrayidx849 = getelementptr i32, i32* %763, i32 %add848
  %765 = load i32, i32* %arrayidx849, align 4
  %call850 = call i32 @rotr32(i32 %765, i32 18)
  %xor851 = xor i32 %call846, %call850
  %766 = load i32*, i32** %W.addr, align 4
  %767 = load i32, i32* %i, align 4
  %add852 = add i32 %767, 2
  %add853 = add i32 %add852, 1
  %arrayidx854 = getelementptr i32, i32* %766, i32 %add853
  %768 = load i32, i32* %arrayidx854, align 4
  %shr855 = lshr i32 %768, 3
  %xor856 = xor i32 %xor851, %shr855
  %add857 = add i32 %add842, %xor856
  %769 = load i32*, i32** %W.addr, align 4
  %770 = load i32, i32* %i, align 4
  %add858 = add i32 %770, 2
  %arrayidx859 = getelementptr i32, i32* %769, i32 %add858
  %771 = load i32, i32* %arrayidx859, align 4
  %add860 = add i32 %add857, %771
  %772 = load i32*, i32** %W.addr, align 4
  %773 = load i32, i32* %i, align 4
  %add861 = add i32 %773, 2
  %add862 = add i32 %add861, 16
  %arrayidx863 = getelementptr i32, i32* %772, i32 %add862
  store i32 %add860, i32* %arrayidx863, align 4
  %774 = load i32*, i32** %W.addr, align 4
  %775 = load i32, i32* %i, align 4
  %add864 = add i32 %775, 3
  %add865 = add i32 %add864, 14
  %arrayidx866 = getelementptr i32, i32* %774, i32 %add865
  %776 = load i32, i32* %arrayidx866, align 4
  %call867 = call i32 @rotr32(i32 %776, i32 17)
  %777 = load i32*, i32** %W.addr, align 4
  %778 = load i32, i32* %i, align 4
  %add868 = add i32 %778, 3
  %add869 = add i32 %add868, 14
  %arrayidx870 = getelementptr i32, i32* %777, i32 %add869
  %779 = load i32, i32* %arrayidx870, align 4
  %call871 = call i32 @rotr32(i32 %779, i32 19)
  %xor872 = xor i32 %call867, %call871
  %780 = load i32*, i32** %W.addr, align 4
  %781 = load i32, i32* %i, align 4
  %add873 = add i32 %781, 3
  %add874 = add i32 %add873, 14
  %arrayidx875 = getelementptr i32, i32* %780, i32 %add874
  %782 = load i32, i32* %arrayidx875, align 4
  %shr876 = lshr i32 %782, 10
  %xor877 = xor i32 %xor872, %shr876
  %783 = load i32*, i32** %W.addr, align 4
  %784 = load i32, i32* %i, align 4
  %add878 = add i32 %784, 3
  %add879 = add i32 %add878, 9
  %arrayidx880 = getelementptr i32, i32* %783, i32 %add879
  %785 = load i32, i32* %arrayidx880, align 4
  %add881 = add i32 %xor877, %785
  %786 = load i32*, i32** %W.addr, align 4
  %787 = load i32, i32* %i, align 4
  %add882 = add i32 %787, 3
  %add883 = add i32 %add882, 1
  %arrayidx884 = getelementptr i32, i32* %786, i32 %add883
  %788 = load i32, i32* %arrayidx884, align 4
  %call885 = call i32 @rotr32(i32 %788, i32 7)
  %789 = load i32*, i32** %W.addr, align 4
  %790 = load i32, i32* %i, align 4
  %add886 = add i32 %790, 3
  %add887 = add i32 %add886, 1
  %arrayidx888 = getelementptr i32, i32* %789, i32 %add887
  %791 = load i32, i32* %arrayidx888, align 4
  %call889 = call i32 @rotr32(i32 %791, i32 18)
  %xor890 = xor i32 %call885, %call889
  %792 = load i32*, i32** %W.addr, align 4
  %793 = load i32, i32* %i, align 4
  %add891 = add i32 %793, 3
  %add892 = add i32 %add891, 1
  %arrayidx893 = getelementptr i32, i32* %792, i32 %add892
  %794 = load i32, i32* %arrayidx893, align 4
  %shr894 = lshr i32 %794, 3
  %xor895 = xor i32 %xor890, %shr894
  %add896 = add i32 %add881, %xor895
  %795 = load i32*, i32** %W.addr, align 4
  %796 = load i32, i32* %i, align 4
  %add897 = add i32 %796, 3
  %arrayidx898 = getelementptr i32, i32* %795, i32 %add897
  %797 = load i32, i32* %arrayidx898, align 4
  %add899 = add i32 %add896, %797
  %798 = load i32*, i32** %W.addr, align 4
  %799 = load i32, i32* %i, align 4
  %add900 = add i32 %799, 3
  %add901 = add i32 %add900, 16
  %arrayidx902 = getelementptr i32, i32* %798, i32 %add901
  store i32 %add899, i32* %arrayidx902, align 4
  %800 = load i32*, i32** %W.addr, align 4
  %801 = load i32, i32* %i, align 4
  %add903 = add i32 %801, 4
  %add904 = add i32 %add903, 14
  %arrayidx905 = getelementptr i32, i32* %800, i32 %add904
  %802 = load i32, i32* %arrayidx905, align 4
  %call906 = call i32 @rotr32(i32 %802, i32 17)
  %803 = load i32*, i32** %W.addr, align 4
  %804 = load i32, i32* %i, align 4
  %add907 = add i32 %804, 4
  %add908 = add i32 %add907, 14
  %arrayidx909 = getelementptr i32, i32* %803, i32 %add908
  %805 = load i32, i32* %arrayidx909, align 4
  %call910 = call i32 @rotr32(i32 %805, i32 19)
  %xor911 = xor i32 %call906, %call910
  %806 = load i32*, i32** %W.addr, align 4
  %807 = load i32, i32* %i, align 4
  %add912 = add i32 %807, 4
  %add913 = add i32 %add912, 14
  %arrayidx914 = getelementptr i32, i32* %806, i32 %add913
  %808 = load i32, i32* %arrayidx914, align 4
  %shr915 = lshr i32 %808, 10
  %xor916 = xor i32 %xor911, %shr915
  %809 = load i32*, i32** %W.addr, align 4
  %810 = load i32, i32* %i, align 4
  %add917 = add i32 %810, 4
  %add918 = add i32 %add917, 9
  %arrayidx919 = getelementptr i32, i32* %809, i32 %add918
  %811 = load i32, i32* %arrayidx919, align 4
  %add920 = add i32 %xor916, %811
  %812 = load i32*, i32** %W.addr, align 4
  %813 = load i32, i32* %i, align 4
  %add921 = add i32 %813, 4
  %add922 = add i32 %add921, 1
  %arrayidx923 = getelementptr i32, i32* %812, i32 %add922
  %814 = load i32, i32* %arrayidx923, align 4
  %call924 = call i32 @rotr32(i32 %814, i32 7)
  %815 = load i32*, i32** %W.addr, align 4
  %816 = load i32, i32* %i, align 4
  %add925 = add i32 %816, 4
  %add926 = add i32 %add925, 1
  %arrayidx927 = getelementptr i32, i32* %815, i32 %add926
  %817 = load i32, i32* %arrayidx927, align 4
  %call928 = call i32 @rotr32(i32 %817, i32 18)
  %xor929 = xor i32 %call924, %call928
  %818 = load i32*, i32** %W.addr, align 4
  %819 = load i32, i32* %i, align 4
  %add930 = add i32 %819, 4
  %add931 = add i32 %add930, 1
  %arrayidx932 = getelementptr i32, i32* %818, i32 %add931
  %820 = load i32, i32* %arrayidx932, align 4
  %shr933 = lshr i32 %820, 3
  %xor934 = xor i32 %xor929, %shr933
  %add935 = add i32 %add920, %xor934
  %821 = load i32*, i32** %W.addr, align 4
  %822 = load i32, i32* %i, align 4
  %add936 = add i32 %822, 4
  %arrayidx937 = getelementptr i32, i32* %821, i32 %add936
  %823 = load i32, i32* %arrayidx937, align 4
  %add938 = add i32 %add935, %823
  %824 = load i32*, i32** %W.addr, align 4
  %825 = load i32, i32* %i, align 4
  %add939 = add i32 %825, 4
  %add940 = add i32 %add939, 16
  %arrayidx941 = getelementptr i32, i32* %824, i32 %add940
  store i32 %add938, i32* %arrayidx941, align 4
  %826 = load i32*, i32** %W.addr, align 4
  %827 = load i32, i32* %i, align 4
  %add942 = add i32 %827, 5
  %add943 = add i32 %add942, 14
  %arrayidx944 = getelementptr i32, i32* %826, i32 %add943
  %828 = load i32, i32* %arrayidx944, align 4
  %call945 = call i32 @rotr32(i32 %828, i32 17)
  %829 = load i32*, i32** %W.addr, align 4
  %830 = load i32, i32* %i, align 4
  %add946 = add i32 %830, 5
  %add947 = add i32 %add946, 14
  %arrayidx948 = getelementptr i32, i32* %829, i32 %add947
  %831 = load i32, i32* %arrayidx948, align 4
  %call949 = call i32 @rotr32(i32 %831, i32 19)
  %xor950 = xor i32 %call945, %call949
  %832 = load i32*, i32** %W.addr, align 4
  %833 = load i32, i32* %i, align 4
  %add951 = add i32 %833, 5
  %add952 = add i32 %add951, 14
  %arrayidx953 = getelementptr i32, i32* %832, i32 %add952
  %834 = load i32, i32* %arrayidx953, align 4
  %shr954 = lshr i32 %834, 10
  %xor955 = xor i32 %xor950, %shr954
  %835 = load i32*, i32** %W.addr, align 4
  %836 = load i32, i32* %i, align 4
  %add956 = add i32 %836, 5
  %add957 = add i32 %add956, 9
  %arrayidx958 = getelementptr i32, i32* %835, i32 %add957
  %837 = load i32, i32* %arrayidx958, align 4
  %add959 = add i32 %xor955, %837
  %838 = load i32*, i32** %W.addr, align 4
  %839 = load i32, i32* %i, align 4
  %add960 = add i32 %839, 5
  %add961 = add i32 %add960, 1
  %arrayidx962 = getelementptr i32, i32* %838, i32 %add961
  %840 = load i32, i32* %arrayidx962, align 4
  %call963 = call i32 @rotr32(i32 %840, i32 7)
  %841 = load i32*, i32** %W.addr, align 4
  %842 = load i32, i32* %i, align 4
  %add964 = add i32 %842, 5
  %add965 = add i32 %add964, 1
  %arrayidx966 = getelementptr i32, i32* %841, i32 %add965
  %843 = load i32, i32* %arrayidx966, align 4
  %call967 = call i32 @rotr32(i32 %843, i32 18)
  %xor968 = xor i32 %call963, %call967
  %844 = load i32*, i32** %W.addr, align 4
  %845 = load i32, i32* %i, align 4
  %add969 = add i32 %845, 5
  %add970 = add i32 %add969, 1
  %arrayidx971 = getelementptr i32, i32* %844, i32 %add970
  %846 = load i32, i32* %arrayidx971, align 4
  %shr972 = lshr i32 %846, 3
  %xor973 = xor i32 %xor968, %shr972
  %add974 = add i32 %add959, %xor973
  %847 = load i32*, i32** %W.addr, align 4
  %848 = load i32, i32* %i, align 4
  %add975 = add i32 %848, 5
  %arrayidx976 = getelementptr i32, i32* %847, i32 %add975
  %849 = load i32, i32* %arrayidx976, align 4
  %add977 = add i32 %add974, %849
  %850 = load i32*, i32** %W.addr, align 4
  %851 = load i32, i32* %i, align 4
  %add978 = add i32 %851, 5
  %add979 = add i32 %add978, 16
  %arrayidx980 = getelementptr i32, i32* %850, i32 %add979
  store i32 %add977, i32* %arrayidx980, align 4
  %852 = load i32*, i32** %W.addr, align 4
  %853 = load i32, i32* %i, align 4
  %add981 = add i32 %853, 6
  %add982 = add i32 %add981, 14
  %arrayidx983 = getelementptr i32, i32* %852, i32 %add982
  %854 = load i32, i32* %arrayidx983, align 4
  %call984 = call i32 @rotr32(i32 %854, i32 17)
  %855 = load i32*, i32** %W.addr, align 4
  %856 = load i32, i32* %i, align 4
  %add985 = add i32 %856, 6
  %add986 = add i32 %add985, 14
  %arrayidx987 = getelementptr i32, i32* %855, i32 %add986
  %857 = load i32, i32* %arrayidx987, align 4
  %call988 = call i32 @rotr32(i32 %857, i32 19)
  %xor989 = xor i32 %call984, %call988
  %858 = load i32*, i32** %W.addr, align 4
  %859 = load i32, i32* %i, align 4
  %add990 = add i32 %859, 6
  %add991 = add i32 %add990, 14
  %arrayidx992 = getelementptr i32, i32* %858, i32 %add991
  %860 = load i32, i32* %arrayidx992, align 4
  %shr993 = lshr i32 %860, 10
  %xor994 = xor i32 %xor989, %shr993
  %861 = load i32*, i32** %W.addr, align 4
  %862 = load i32, i32* %i, align 4
  %add995 = add i32 %862, 6
  %add996 = add i32 %add995, 9
  %arrayidx997 = getelementptr i32, i32* %861, i32 %add996
  %863 = load i32, i32* %arrayidx997, align 4
  %add998 = add i32 %xor994, %863
  %864 = load i32*, i32** %W.addr, align 4
  %865 = load i32, i32* %i, align 4
  %add999 = add i32 %865, 6
  %add1000 = add i32 %add999, 1
  %arrayidx1001 = getelementptr i32, i32* %864, i32 %add1000
  %866 = load i32, i32* %arrayidx1001, align 4
  %call1002 = call i32 @rotr32(i32 %866, i32 7)
  %867 = load i32*, i32** %W.addr, align 4
  %868 = load i32, i32* %i, align 4
  %add1003 = add i32 %868, 6
  %add1004 = add i32 %add1003, 1
  %arrayidx1005 = getelementptr i32, i32* %867, i32 %add1004
  %869 = load i32, i32* %arrayidx1005, align 4
  %call1006 = call i32 @rotr32(i32 %869, i32 18)
  %xor1007 = xor i32 %call1002, %call1006
  %870 = load i32*, i32** %W.addr, align 4
  %871 = load i32, i32* %i, align 4
  %add1008 = add i32 %871, 6
  %add1009 = add i32 %add1008, 1
  %arrayidx1010 = getelementptr i32, i32* %870, i32 %add1009
  %872 = load i32, i32* %arrayidx1010, align 4
  %shr1011 = lshr i32 %872, 3
  %xor1012 = xor i32 %xor1007, %shr1011
  %add1013 = add i32 %add998, %xor1012
  %873 = load i32*, i32** %W.addr, align 4
  %874 = load i32, i32* %i, align 4
  %add1014 = add i32 %874, 6
  %arrayidx1015 = getelementptr i32, i32* %873, i32 %add1014
  %875 = load i32, i32* %arrayidx1015, align 4
  %add1016 = add i32 %add1013, %875
  %876 = load i32*, i32** %W.addr, align 4
  %877 = load i32, i32* %i, align 4
  %add1017 = add i32 %877, 6
  %add1018 = add i32 %add1017, 16
  %arrayidx1019 = getelementptr i32, i32* %876, i32 %add1018
  store i32 %add1016, i32* %arrayidx1019, align 4
  %878 = load i32*, i32** %W.addr, align 4
  %879 = load i32, i32* %i, align 4
  %add1020 = add i32 %879, 7
  %add1021 = add i32 %add1020, 14
  %arrayidx1022 = getelementptr i32, i32* %878, i32 %add1021
  %880 = load i32, i32* %arrayidx1022, align 4
  %call1023 = call i32 @rotr32(i32 %880, i32 17)
  %881 = load i32*, i32** %W.addr, align 4
  %882 = load i32, i32* %i, align 4
  %add1024 = add i32 %882, 7
  %add1025 = add i32 %add1024, 14
  %arrayidx1026 = getelementptr i32, i32* %881, i32 %add1025
  %883 = load i32, i32* %arrayidx1026, align 4
  %call1027 = call i32 @rotr32(i32 %883, i32 19)
  %xor1028 = xor i32 %call1023, %call1027
  %884 = load i32*, i32** %W.addr, align 4
  %885 = load i32, i32* %i, align 4
  %add1029 = add i32 %885, 7
  %add1030 = add i32 %add1029, 14
  %arrayidx1031 = getelementptr i32, i32* %884, i32 %add1030
  %886 = load i32, i32* %arrayidx1031, align 4
  %shr1032 = lshr i32 %886, 10
  %xor1033 = xor i32 %xor1028, %shr1032
  %887 = load i32*, i32** %W.addr, align 4
  %888 = load i32, i32* %i, align 4
  %add1034 = add i32 %888, 7
  %add1035 = add i32 %add1034, 9
  %arrayidx1036 = getelementptr i32, i32* %887, i32 %add1035
  %889 = load i32, i32* %arrayidx1036, align 4
  %add1037 = add i32 %xor1033, %889
  %890 = load i32*, i32** %W.addr, align 4
  %891 = load i32, i32* %i, align 4
  %add1038 = add i32 %891, 7
  %add1039 = add i32 %add1038, 1
  %arrayidx1040 = getelementptr i32, i32* %890, i32 %add1039
  %892 = load i32, i32* %arrayidx1040, align 4
  %call1041 = call i32 @rotr32(i32 %892, i32 7)
  %893 = load i32*, i32** %W.addr, align 4
  %894 = load i32, i32* %i, align 4
  %add1042 = add i32 %894, 7
  %add1043 = add i32 %add1042, 1
  %arrayidx1044 = getelementptr i32, i32* %893, i32 %add1043
  %895 = load i32, i32* %arrayidx1044, align 4
  %call1045 = call i32 @rotr32(i32 %895, i32 18)
  %xor1046 = xor i32 %call1041, %call1045
  %896 = load i32*, i32** %W.addr, align 4
  %897 = load i32, i32* %i, align 4
  %add1047 = add i32 %897, 7
  %add1048 = add i32 %add1047, 1
  %arrayidx1049 = getelementptr i32, i32* %896, i32 %add1048
  %898 = load i32, i32* %arrayidx1049, align 4
  %shr1050 = lshr i32 %898, 3
  %xor1051 = xor i32 %xor1046, %shr1050
  %add1052 = add i32 %add1037, %xor1051
  %899 = load i32*, i32** %W.addr, align 4
  %900 = load i32, i32* %i, align 4
  %add1053 = add i32 %900, 7
  %arrayidx1054 = getelementptr i32, i32* %899, i32 %add1053
  %901 = load i32, i32* %arrayidx1054, align 4
  %add1055 = add i32 %add1052, %901
  %902 = load i32*, i32** %W.addr, align 4
  %903 = load i32, i32* %i, align 4
  %add1056 = add i32 %903, 7
  %add1057 = add i32 %add1056, 16
  %arrayidx1058 = getelementptr i32, i32* %902, i32 %add1057
  store i32 %add1055, i32* %arrayidx1058, align 4
  %904 = load i32*, i32** %W.addr, align 4
  %905 = load i32, i32* %i, align 4
  %add1059 = add i32 %905, 8
  %add1060 = add i32 %add1059, 14
  %arrayidx1061 = getelementptr i32, i32* %904, i32 %add1060
  %906 = load i32, i32* %arrayidx1061, align 4
  %call1062 = call i32 @rotr32(i32 %906, i32 17)
  %907 = load i32*, i32** %W.addr, align 4
  %908 = load i32, i32* %i, align 4
  %add1063 = add i32 %908, 8
  %add1064 = add i32 %add1063, 14
  %arrayidx1065 = getelementptr i32, i32* %907, i32 %add1064
  %909 = load i32, i32* %arrayidx1065, align 4
  %call1066 = call i32 @rotr32(i32 %909, i32 19)
  %xor1067 = xor i32 %call1062, %call1066
  %910 = load i32*, i32** %W.addr, align 4
  %911 = load i32, i32* %i, align 4
  %add1068 = add i32 %911, 8
  %add1069 = add i32 %add1068, 14
  %arrayidx1070 = getelementptr i32, i32* %910, i32 %add1069
  %912 = load i32, i32* %arrayidx1070, align 4
  %shr1071 = lshr i32 %912, 10
  %xor1072 = xor i32 %xor1067, %shr1071
  %913 = load i32*, i32** %W.addr, align 4
  %914 = load i32, i32* %i, align 4
  %add1073 = add i32 %914, 8
  %add1074 = add i32 %add1073, 9
  %arrayidx1075 = getelementptr i32, i32* %913, i32 %add1074
  %915 = load i32, i32* %arrayidx1075, align 4
  %add1076 = add i32 %xor1072, %915
  %916 = load i32*, i32** %W.addr, align 4
  %917 = load i32, i32* %i, align 4
  %add1077 = add i32 %917, 8
  %add1078 = add i32 %add1077, 1
  %arrayidx1079 = getelementptr i32, i32* %916, i32 %add1078
  %918 = load i32, i32* %arrayidx1079, align 4
  %call1080 = call i32 @rotr32(i32 %918, i32 7)
  %919 = load i32*, i32** %W.addr, align 4
  %920 = load i32, i32* %i, align 4
  %add1081 = add i32 %920, 8
  %add1082 = add i32 %add1081, 1
  %arrayidx1083 = getelementptr i32, i32* %919, i32 %add1082
  %921 = load i32, i32* %arrayidx1083, align 4
  %call1084 = call i32 @rotr32(i32 %921, i32 18)
  %xor1085 = xor i32 %call1080, %call1084
  %922 = load i32*, i32** %W.addr, align 4
  %923 = load i32, i32* %i, align 4
  %add1086 = add i32 %923, 8
  %add1087 = add i32 %add1086, 1
  %arrayidx1088 = getelementptr i32, i32* %922, i32 %add1087
  %924 = load i32, i32* %arrayidx1088, align 4
  %shr1089 = lshr i32 %924, 3
  %xor1090 = xor i32 %xor1085, %shr1089
  %add1091 = add i32 %add1076, %xor1090
  %925 = load i32*, i32** %W.addr, align 4
  %926 = load i32, i32* %i, align 4
  %add1092 = add i32 %926, 8
  %arrayidx1093 = getelementptr i32, i32* %925, i32 %add1092
  %927 = load i32, i32* %arrayidx1093, align 4
  %add1094 = add i32 %add1091, %927
  %928 = load i32*, i32** %W.addr, align 4
  %929 = load i32, i32* %i, align 4
  %add1095 = add i32 %929, 8
  %add1096 = add i32 %add1095, 16
  %arrayidx1097 = getelementptr i32, i32* %928, i32 %add1096
  store i32 %add1094, i32* %arrayidx1097, align 4
  %930 = load i32*, i32** %W.addr, align 4
  %931 = load i32, i32* %i, align 4
  %add1098 = add i32 %931, 9
  %add1099 = add i32 %add1098, 14
  %arrayidx1100 = getelementptr i32, i32* %930, i32 %add1099
  %932 = load i32, i32* %arrayidx1100, align 4
  %call1101 = call i32 @rotr32(i32 %932, i32 17)
  %933 = load i32*, i32** %W.addr, align 4
  %934 = load i32, i32* %i, align 4
  %add1102 = add i32 %934, 9
  %add1103 = add i32 %add1102, 14
  %arrayidx1104 = getelementptr i32, i32* %933, i32 %add1103
  %935 = load i32, i32* %arrayidx1104, align 4
  %call1105 = call i32 @rotr32(i32 %935, i32 19)
  %xor1106 = xor i32 %call1101, %call1105
  %936 = load i32*, i32** %W.addr, align 4
  %937 = load i32, i32* %i, align 4
  %add1107 = add i32 %937, 9
  %add1108 = add i32 %add1107, 14
  %arrayidx1109 = getelementptr i32, i32* %936, i32 %add1108
  %938 = load i32, i32* %arrayidx1109, align 4
  %shr1110 = lshr i32 %938, 10
  %xor1111 = xor i32 %xor1106, %shr1110
  %939 = load i32*, i32** %W.addr, align 4
  %940 = load i32, i32* %i, align 4
  %add1112 = add i32 %940, 9
  %add1113 = add i32 %add1112, 9
  %arrayidx1114 = getelementptr i32, i32* %939, i32 %add1113
  %941 = load i32, i32* %arrayidx1114, align 4
  %add1115 = add i32 %xor1111, %941
  %942 = load i32*, i32** %W.addr, align 4
  %943 = load i32, i32* %i, align 4
  %add1116 = add i32 %943, 9
  %add1117 = add i32 %add1116, 1
  %arrayidx1118 = getelementptr i32, i32* %942, i32 %add1117
  %944 = load i32, i32* %arrayidx1118, align 4
  %call1119 = call i32 @rotr32(i32 %944, i32 7)
  %945 = load i32*, i32** %W.addr, align 4
  %946 = load i32, i32* %i, align 4
  %add1120 = add i32 %946, 9
  %add1121 = add i32 %add1120, 1
  %arrayidx1122 = getelementptr i32, i32* %945, i32 %add1121
  %947 = load i32, i32* %arrayidx1122, align 4
  %call1123 = call i32 @rotr32(i32 %947, i32 18)
  %xor1124 = xor i32 %call1119, %call1123
  %948 = load i32*, i32** %W.addr, align 4
  %949 = load i32, i32* %i, align 4
  %add1125 = add i32 %949, 9
  %add1126 = add i32 %add1125, 1
  %arrayidx1127 = getelementptr i32, i32* %948, i32 %add1126
  %950 = load i32, i32* %arrayidx1127, align 4
  %shr1128 = lshr i32 %950, 3
  %xor1129 = xor i32 %xor1124, %shr1128
  %add1130 = add i32 %add1115, %xor1129
  %951 = load i32*, i32** %W.addr, align 4
  %952 = load i32, i32* %i, align 4
  %add1131 = add i32 %952, 9
  %arrayidx1132 = getelementptr i32, i32* %951, i32 %add1131
  %953 = load i32, i32* %arrayidx1132, align 4
  %add1133 = add i32 %add1130, %953
  %954 = load i32*, i32** %W.addr, align 4
  %955 = load i32, i32* %i, align 4
  %add1134 = add i32 %955, 9
  %add1135 = add i32 %add1134, 16
  %arrayidx1136 = getelementptr i32, i32* %954, i32 %add1135
  store i32 %add1133, i32* %arrayidx1136, align 4
  %956 = load i32*, i32** %W.addr, align 4
  %957 = load i32, i32* %i, align 4
  %add1137 = add i32 %957, 10
  %add1138 = add i32 %add1137, 14
  %arrayidx1139 = getelementptr i32, i32* %956, i32 %add1138
  %958 = load i32, i32* %arrayidx1139, align 4
  %call1140 = call i32 @rotr32(i32 %958, i32 17)
  %959 = load i32*, i32** %W.addr, align 4
  %960 = load i32, i32* %i, align 4
  %add1141 = add i32 %960, 10
  %add1142 = add i32 %add1141, 14
  %arrayidx1143 = getelementptr i32, i32* %959, i32 %add1142
  %961 = load i32, i32* %arrayidx1143, align 4
  %call1144 = call i32 @rotr32(i32 %961, i32 19)
  %xor1145 = xor i32 %call1140, %call1144
  %962 = load i32*, i32** %W.addr, align 4
  %963 = load i32, i32* %i, align 4
  %add1146 = add i32 %963, 10
  %add1147 = add i32 %add1146, 14
  %arrayidx1148 = getelementptr i32, i32* %962, i32 %add1147
  %964 = load i32, i32* %arrayidx1148, align 4
  %shr1149 = lshr i32 %964, 10
  %xor1150 = xor i32 %xor1145, %shr1149
  %965 = load i32*, i32** %W.addr, align 4
  %966 = load i32, i32* %i, align 4
  %add1151 = add i32 %966, 10
  %add1152 = add i32 %add1151, 9
  %arrayidx1153 = getelementptr i32, i32* %965, i32 %add1152
  %967 = load i32, i32* %arrayidx1153, align 4
  %add1154 = add i32 %xor1150, %967
  %968 = load i32*, i32** %W.addr, align 4
  %969 = load i32, i32* %i, align 4
  %add1155 = add i32 %969, 10
  %add1156 = add i32 %add1155, 1
  %arrayidx1157 = getelementptr i32, i32* %968, i32 %add1156
  %970 = load i32, i32* %arrayidx1157, align 4
  %call1158 = call i32 @rotr32(i32 %970, i32 7)
  %971 = load i32*, i32** %W.addr, align 4
  %972 = load i32, i32* %i, align 4
  %add1159 = add i32 %972, 10
  %add1160 = add i32 %add1159, 1
  %arrayidx1161 = getelementptr i32, i32* %971, i32 %add1160
  %973 = load i32, i32* %arrayidx1161, align 4
  %call1162 = call i32 @rotr32(i32 %973, i32 18)
  %xor1163 = xor i32 %call1158, %call1162
  %974 = load i32*, i32** %W.addr, align 4
  %975 = load i32, i32* %i, align 4
  %add1164 = add i32 %975, 10
  %add1165 = add i32 %add1164, 1
  %arrayidx1166 = getelementptr i32, i32* %974, i32 %add1165
  %976 = load i32, i32* %arrayidx1166, align 4
  %shr1167 = lshr i32 %976, 3
  %xor1168 = xor i32 %xor1163, %shr1167
  %add1169 = add i32 %add1154, %xor1168
  %977 = load i32*, i32** %W.addr, align 4
  %978 = load i32, i32* %i, align 4
  %add1170 = add i32 %978, 10
  %arrayidx1171 = getelementptr i32, i32* %977, i32 %add1170
  %979 = load i32, i32* %arrayidx1171, align 4
  %add1172 = add i32 %add1169, %979
  %980 = load i32*, i32** %W.addr, align 4
  %981 = load i32, i32* %i, align 4
  %add1173 = add i32 %981, 10
  %add1174 = add i32 %add1173, 16
  %arrayidx1175 = getelementptr i32, i32* %980, i32 %add1174
  store i32 %add1172, i32* %arrayidx1175, align 4
  %982 = load i32*, i32** %W.addr, align 4
  %983 = load i32, i32* %i, align 4
  %add1176 = add i32 %983, 11
  %add1177 = add i32 %add1176, 14
  %arrayidx1178 = getelementptr i32, i32* %982, i32 %add1177
  %984 = load i32, i32* %arrayidx1178, align 4
  %call1179 = call i32 @rotr32(i32 %984, i32 17)
  %985 = load i32*, i32** %W.addr, align 4
  %986 = load i32, i32* %i, align 4
  %add1180 = add i32 %986, 11
  %add1181 = add i32 %add1180, 14
  %arrayidx1182 = getelementptr i32, i32* %985, i32 %add1181
  %987 = load i32, i32* %arrayidx1182, align 4
  %call1183 = call i32 @rotr32(i32 %987, i32 19)
  %xor1184 = xor i32 %call1179, %call1183
  %988 = load i32*, i32** %W.addr, align 4
  %989 = load i32, i32* %i, align 4
  %add1185 = add i32 %989, 11
  %add1186 = add i32 %add1185, 14
  %arrayidx1187 = getelementptr i32, i32* %988, i32 %add1186
  %990 = load i32, i32* %arrayidx1187, align 4
  %shr1188 = lshr i32 %990, 10
  %xor1189 = xor i32 %xor1184, %shr1188
  %991 = load i32*, i32** %W.addr, align 4
  %992 = load i32, i32* %i, align 4
  %add1190 = add i32 %992, 11
  %add1191 = add i32 %add1190, 9
  %arrayidx1192 = getelementptr i32, i32* %991, i32 %add1191
  %993 = load i32, i32* %arrayidx1192, align 4
  %add1193 = add i32 %xor1189, %993
  %994 = load i32*, i32** %W.addr, align 4
  %995 = load i32, i32* %i, align 4
  %add1194 = add i32 %995, 11
  %add1195 = add i32 %add1194, 1
  %arrayidx1196 = getelementptr i32, i32* %994, i32 %add1195
  %996 = load i32, i32* %arrayidx1196, align 4
  %call1197 = call i32 @rotr32(i32 %996, i32 7)
  %997 = load i32*, i32** %W.addr, align 4
  %998 = load i32, i32* %i, align 4
  %add1198 = add i32 %998, 11
  %add1199 = add i32 %add1198, 1
  %arrayidx1200 = getelementptr i32, i32* %997, i32 %add1199
  %999 = load i32, i32* %arrayidx1200, align 4
  %call1201 = call i32 @rotr32(i32 %999, i32 18)
  %xor1202 = xor i32 %call1197, %call1201
  %1000 = load i32*, i32** %W.addr, align 4
  %1001 = load i32, i32* %i, align 4
  %add1203 = add i32 %1001, 11
  %add1204 = add i32 %add1203, 1
  %arrayidx1205 = getelementptr i32, i32* %1000, i32 %add1204
  %1002 = load i32, i32* %arrayidx1205, align 4
  %shr1206 = lshr i32 %1002, 3
  %xor1207 = xor i32 %xor1202, %shr1206
  %add1208 = add i32 %add1193, %xor1207
  %1003 = load i32*, i32** %W.addr, align 4
  %1004 = load i32, i32* %i, align 4
  %add1209 = add i32 %1004, 11
  %arrayidx1210 = getelementptr i32, i32* %1003, i32 %add1209
  %1005 = load i32, i32* %arrayidx1210, align 4
  %add1211 = add i32 %add1208, %1005
  %1006 = load i32*, i32** %W.addr, align 4
  %1007 = load i32, i32* %i, align 4
  %add1212 = add i32 %1007, 11
  %add1213 = add i32 %add1212, 16
  %arrayidx1214 = getelementptr i32, i32* %1006, i32 %add1213
  store i32 %add1211, i32* %arrayidx1214, align 4
  %1008 = load i32*, i32** %W.addr, align 4
  %1009 = load i32, i32* %i, align 4
  %add1215 = add i32 %1009, 12
  %add1216 = add i32 %add1215, 14
  %arrayidx1217 = getelementptr i32, i32* %1008, i32 %add1216
  %1010 = load i32, i32* %arrayidx1217, align 4
  %call1218 = call i32 @rotr32(i32 %1010, i32 17)
  %1011 = load i32*, i32** %W.addr, align 4
  %1012 = load i32, i32* %i, align 4
  %add1219 = add i32 %1012, 12
  %add1220 = add i32 %add1219, 14
  %arrayidx1221 = getelementptr i32, i32* %1011, i32 %add1220
  %1013 = load i32, i32* %arrayidx1221, align 4
  %call1222 = call i32 @rotr32(i32 %1013, i32 19)
  %xor1223 = xor i32 %call1218, %call1222
  %1014 = load i32*, i32** %W.addr, align 4
  %1015 = load i32, i32* %i, align 4
  %add1224 = add i32 %1015, 12
  %add1225 = add i32 %add1224, 14
  %arrayidx1226 = getelementptr i32, i32* %1014, i32 %add1225
  %1016 = load i32, i32* %arrayidx1226, align 4
  %shr1227 = lshr i32 %1016, 10
  %xor1228 = xor i32 %xor1223, %shr1227
  %1017 = load i32*, i32** %W.addr, align 4
  %1018 = load i32, i32* %i, align 4
  %add1229 = add i32 %1018, 12
  %add1230 = add i32 %add1229, 9
  %arrayidx1231 = getelementptr i32, i32* %1017, i32 %add1230
  %1019 = load i32, i32* %arrayidx1231, align 4
  %add1232 = add i32 %xor1228, %1019
  %1020 = load i32*, i32** %W.addr, align 4
  %1021 = load i32, i32* %i, align 4
  %add1233 = add i32 %1021, 12
  %add1234 = add i32 %add1233, 1
  %arrayidx1235 = getelementptr i32, i32* %1020, i32 %add1234
  %1022 = load i32, i32* %arrayidx1235, align 4
  %call1236 = call i32 @rotr32(i32 %1022, i32 7)
  %1023 = load i32*, i32** %W.addr, align 4
  %1024 = load i32, i32* %i, align 4
  %add1237 = add i32 %1024, 12
  %add1238 = add i32 %add1237, 1
  %arrayidx1239 = getelementptr i32, i32* %1023, i32 %add1238
  %1025 = load i32, i32* %arrayidx1239, align 4
  %call1240 = call i32 @rotr32(i32 %1025, i32 18)
  %xor1241 = xor i32 %call1236, %call1240
  %1026 = load i32*, i32** %W.addr, align 4
  %1027 = load i32, i32* %i, align 4
  %add1242 = add i32 %1027, 12
  %add1243 = add i32 %add1242, 1
  %arrayidx1244 = getelementptr i32, i32* %1026, i32 %add1243
  %1028 = load i32, i32* %arrayidx1244, align 4
  %shr1245 = lshr i32 %1028, 3
  %xor1246 = xor i32 %xor1241, %shr1245
  %add1247 = add i32 %add1232, %xor1246
  %1029 = load i32*, i32** %W.addr, align 4
  %1030 = load i32, i32* %i, align 4
  %add1248 = add i32 %1030, 12
  %arrayidx1249 = getelementptr i32, i32* %1029, i32 %add1248
  %1031 = load i32, i32* %arrayidx1249, align 4
  %add1250 = add i32 %add1247, %1031
  %1032 = load i32*, i32** %W.addr, align 4
  %1033 = load i32, i32* %i, align 4
  %add1251 = add i32 %1033, 12
  %add1252 = add i32 %add1251, 16
  %arrayidx1253 = getelementptr i32, i32* %1032, i32 %add1252
  store i32 %add1250, i32* %arrayidx1253, align 4
  %1034 = load i32*, i32** %W.addr, align 4
  %1035 = load i32, i32* %i, align 4
  %add1254 = add i32 %1035, 13
  %add1255 = add i32 %add1254, 14
  %arrayidx1256 = getelementptr i32, i32* %1034, i32 %add1255
  %1036 = load i32, i32* %arrayidx1256, align 4
  %call1257 = call i32 @rotr32(i32 %1036, i32 17)
  %1037 = load i32*, i32** %W.addr, align 4
  %1038 = load i32, i32* %i, align 4
  %add1258 = add i32 %1038, 13
  %add1259 = add i32 %add1258, 14
  %arrayidx1260 = getelementptr i32, i32* %1037, i32 %add1259
  %1039 = load i32, i32* %arrayidx1260, align 4
  %call1261 = call i32 @rotr32(i32 %1039, i32 19)
  %xor1262 = xor i32 %call1257, %call1261
  %1040 = load i32*, i32** %W.addr, align 4
  %1041 = load i32, i32* %i, align 4
  %add1263 = add i32 %1041, 13
  %add1264 = add i32 %add1263, 14
  %arrayidx1265 = getelementptr i32, i32* %1040, i32 %add1264
  %1042 = load i32, i32* %arrayidx1265, align 4
  %shr1266 = lshr i32 %1042, 10
  %xor1267 = xor i32 %xor1262, %shr1266
  %1043 = load i32*, i32** %W.addr, align 4
  %1044 = load i32, i32* %i, align 4
  %add1268 = add i32 %1044, 13
  %add1269 = add i32 %add1268, 9
  %arrayidx1270 = getelementptr i32, i32* %1043, i32 %add1269
  %1045 = load i32, i32* %arrayidx1270, align 4
  %add1271 = add i32 %xor1267, %1045
  %1046 = load i32*, i32** %W.addr, align 4
  %1047 = load i32, i32* %i, align 4
  %add1272 = add i32 %1047, 13
  %add1273 = add i32 %add1272, 1
  %arrayidx1274 = getelementptr i32, i32* %1046, i32 %add1273
  %1048 = load i32, i32* %arrayidx1274, align 4
  %call1275 = call i32 @rotr32(i32 %1048, i32 7)
  %1049 = load i32*, i32** %W.addr, align 4
  %1050 = load i32, i32* %i, align 4
  %add1276 = add i32 %1050, 13
  %add1277 = add i32 %add1276, 1
  %arrayidx1278 = getelementptr i32, i32* %1049, i32 %add1277
  %1051 = load i32, i32* %arrayidx1278, align 4
  %call1279 = call i32 @rotr32(i32 %1051, i32 18)
  %xor1280 = xor i32 %call1275, %call1279
  %1052 = load i32*, i32** %W.addr, align 4
  %1053 = load i32, i32* %i, align 4
  %add1281 = add i32 %1053, 13
  %add1282 = add i32 %add1281, 1
  %arrayidx1283 = getelementptr i32, i32* %1052, i32 %add1282
  %1054 = load i32, i32* %arrayidx1283, align 4
  %shr1284 = lshr i32 %1054, 3
  %xor1285 = xor i32 %xor1280, %shr1284
  %add1286 = add i32 %add1271, %xor1285
  %1055 = load i32*, i32** %W.addr, align 4
  %1056 = load i32, i32* %i, align 4
  %add1287 = add i32 %1056, 13
  %arrayidx1288 = getelementptr i32, i32* %1055, i32 %add1287
  %1057 = load i32, i32* %arrayidx1288, align 4
  %add1289 = add i32 %add1286, %1057
  %1058 = load i32*, i32** %W.addr, align 4
  %1059 = load i32, i32* %i, align 4
  %add1290 = add i32 %1059, 13
  %add1291 = add i32 %add1290, 16
  %arrayidx1292 = getelementptr i32, i32* %1058, i32 %add1291
  store i32 %add1289, i32* %arrayidx1292, align 4
  %1060 = load i32*, i32** %W.addr, align 4
  %1061 = load i32, i32* %i, align 4
  %add1293 = add i32 %1061, 14
  %add1294 = add i32 %add1293, 14
  %arrayidx1295 = getelementptr i32, i32* %1060, i32 %add1294
  %1062 = load i32, i32* %arrayidx1295, align 4
  %call1296 = call i32 @rotr32(i32 %1062, i32 17)
  %1063 = load i32*, i32** %W.addr, align 4
  %1064 = load i32, i32* %i, align 4
  %add1297 = add i32 %1064, 14
  %add1298 = add i32 %add1297, 14
  %arrayidx1299 = getelementptr i32, i32* %1063, i32 %add1298
  %1065 = load i32, i32* %arrayidx1299, align 4
  %call1300 = call i32 @rotr32(i32 %1065, i32 19)
  %xor1301 = xor i32 %call1296, %call1300
  %1066 = load i32*, i32** %W.addr, align 4
  %1067 = load i32, i32* %i, align 4
  %add1302 = add i32 %1067, 14
  %add1303 = add i32 %add1302, 14
  %arrayidx1304 = getelementptr i32, i32* %1066, i32 %add1303
  %1068 = load i32, i32* %arrayidx1304, align 4
  %shr1305 = lshr i32 %1068, 10
  %xor1306 = xor i32 %xor1301, %shr1305
  %1069 = load i32*, i32** %W.addr, align 4
  %1070 = load i32, i32* %i, align 4
  %add1307 = add i32 %1070, 14
  %add1308 = add i32 %add1307, 9
  %arrayidx1309 = getelementptr i32, i32* %1069, i32 %add1308
  %1071 = load i32, i32* %arrayidx1309, align 4
  %add1310 = add i32 %xor1306, %1071
  %1072 = load i32*, i32** %W.addr, align 4
  %1073 = load i32, i32* %i, align 4
  %add1311 = add i32 %1073, 14
  %add1312 = add i32 %add1311, 1
  %arrayidx1313 = getelementptr i32, i32* %1072, i32 %add1312
  %1074 = load i32, i32* %arrayidx1313, align 4
  %call1314 = call i32 @rotr32(i32 %1074, i32 7)
  %1075 = load i32*, i32** %W.addr, align 4
  %1076 = load i32, i32* %i, align 4
  %add1315 = add i32 %1076, 14
  %add1316 = add i32 %add1315, 1
  %arrayidx1317 = getelementptr i32, i32* %1075, i32 %add1316
  %1077 = load i32, i32* %arrayidx1317, align 4
  %call1318 = call i32 @rotr32(i32 %1077, i32 18)
  %xor1319 = xor i32 %call1314, %call1318
  %1078 = load i32*, i32** %W.addr, align 4
  %1079 = load i32, i32* %i, align 4
  %add1320 = add i32 %1079, 14
  %add1321 = add i32 %add1320, 1
  %arrayidx1322 = getelementptr i32, i32* %1078, i32 %add1321
  %1080 = load i32, i32* %arrayidx1322, align 4
  %shr1323 = lshr i32 %1080, 3
  %xor1324 = xor i32 %xor1319, %shr1323
  %add1325 = add i32 %add1310, %xor1324
  %1081 = load i32*, i32** %W.addr, align 4
  %1082 = load i32, i32* %i, align 4
  %add1326 = add i32 %1082, 14
  %arrayidx1327 = getelementptr i32, i32* %1081, i32 %add1326
  %1083 = load i32, i32* %arrayidx1327, align 4
  %add1328 = add i32 %add1325, %1083
  %1084 = load i32*, i32** %W.addr, align 4
  %1085 = load i32, i32* %i, align 4
  %add1329 = add i32 %1085, 14
  %add1330 = add i32 %add1329, 16
  %arrayidx1331 = getelementptr i32, i32* %1084, i32 %add1330
  store i32 %add1328, i32* %arrayidx1331, align 4
  %1086 = load i32*, i32** %W.addr, align 4
  %1087 = load i32, i32* %i, align 4
  %add1332 = add i32 %1087, 15
  %add1333 = add i32 %add1332, 14
  %arrayidx1334 = getelementptr i32, i32* %1086, i32 %add1333
  %1088 = load i32, i32* %arrayidx1334, align 4
  %call1335 = call i32 @rotr32(i32 %1088, i32 17)
  %1089 = load i32*, i32** %W.addr, align 4
  %1090 = load i32, i32* %i, align 4
  %add1336 = add i32 %1090, 15
  %add1337 = add i32 %add1336, 14
  %arrayidx1338 = getelementptr i32, i32* %1089, i32 %add1337
  %1091 = load i32, i32* %arrayidx1338, align 4
  %call1339 = call i32 @rotr32(i32 %1091, i32 19)
  %xor1340 = xor i32 %call1335, %call1339
  %1092 = load i32*, i32** %W.addr, align 4
  %1093 = load i32, i32* %i, align 4
  %add1341 = add i32 %1093, 15
  %add1342 = add i32 %add1341, 14
  %arrayidx1343 = getelementptr i32, i32* %1092, i32 %add1342
  %1094 = load i32, i32* %arrayidx1343, align 4
  %shr1344 = lshr i32 %1094, 10
  %xor1345 = xor i32 %xor1340, %shr1344
  %1095 = load i32*, i32** %W.addr, align 4
  %1096 = load i32, i32* %i, align 4
  %add1346 = add i32 %1096, 15
  %add1347 = add i32 %add1346, 9
  %arrayidx1348 = getelementptr i32, i32* %1095, i32 %add1347
  %1097 = load i32, i32* %arrayidx1348, align 4
  %add1349 = add i32 %xor1345, %1097
  %1098 = load i32*, i32** %W.addr, align 4
  %1099 = load i32, i32* %i, align 4
  %add1350 = add i32 %1099, 15
  %add1351 = add i32 %add1350, 1
  %arrayidx1352 = getelementptr i32, i32* %1098, i32 %add1351
  %1100 = load i32, i32* %arrayidx1352, align 4
  %call1353 = call i32 @rotr32(i32 %1100, i32 7)
  %1101 = load i32*, i32** %W.addr, align 4
  %1102 = load i32, i32* %i, align 4
  %add1354 = add i32 %1102, 15
  %add1355 = add i32 %add1354, 1
  %arrayidx1356 = getelementptr i32, i32* %1101, i32 %add1355
  %1103 = load i32, i32* %arrayidx1356, align 4
  %call1357 = call i32 @rotr32(i32 %1103, i32 18)
  %xor1358 = xor i32 %call1353, %call1357
  %1104 = load i32*, i32** %W.addr, align 4
  %1105 = load i32, i32* %i, align 4
  %add1359 = add i32 %1105, 15
  %add1360 = add i32 %add1359, 1
  %arrayidx1361 = getelementptr i32, i32* %1104, i32 %add1360
  %1106 = load i32, i32* %arrayidx1361, align 4
  %shr1362 = lshr i32 %1106, 3
  %xor1363 = xor i32 %xor1358, %shr1362
  %add1364 = add i32 %add1349, %xor1363
  %1107 = load i32*, i32** %W.addr, align 4
  %1108 = load i32, i32* %i, align 4
  %add1365 = add i32 %1108, 15
  %arrayidx1366 = getelementptr i32, i32* %1107, i32 %add1365
  %1109 = load i32, i32* %arrayidx1366, align 4
  %add1367 = add i32 %add1364, %1109
  %1110 = load i32*, i32** %W.addr, align 4
  %1111 = load i32, i32* %i, align 4
  %add1368 = add i32 %1111, 15
  %add1369 = add i32 %add1368, 16
  %arrayidx1370 = getelementptr i32, i32* %1110, i32 %add1369
  store i32 %add1367, i32* %arrayidx1370, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %1112 = load i32, i32* %i, align 4
  %add1371 = add i32 %1112, 16
  store i32 %add1371, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1372

for.cond1372:                                     ; preds = %for.inc1378, %for.end
  %1113 = load i32, i32* %i, align 4
  %cmp1373 = icmp slt i32 %1113, 8
  br i1 %cmp1373, label %for.body1374, label %for.end1379

for.body1374:                                     ; preds = %for.cond1372
  %1114 = load i32*, i32** %S.addr, align 4
  %1115 = load i32, i32* %i, align 4
  %arrayidx1375 = getelementptr i32, i32* %1114, i32 %1115
  %1116 = load i32, i32* %arrayidx1375, align 4
  %1117 = load i32*, i32** %state.addr, align 4
  %1118 = load i32, i32* %i, align 4
  %arrayidx1376 = getelementptr i32, i32* %1117, i32 %1118
  %1119 = load i32, i32* %arrayidx1376, align 4
  %add1377 = add i32 %1119, %1116
  store i32 %add1377, i32* %arrayidx1376, align 4
  br label %for.inc1378

for.inc1378:                                      ; preds = %for.body1374
  %1120 = load i32, i32* %i, align 4
  %inc = add i32 %1120, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond1372

for.end1379:                                      ; preds = %for.cond1372
  ret void
}

declare void @sodium_memzero(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha256_final(%struct.crypto_hash_sha256_state* nonnull %state, i8* nonnull %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha256_state*, align 4
  %out.addr = alloca i8*, align 4
  %tmp32 = alloca [72 x i32], align 16
  store %struct.crypto_hash_sha256_state* %state, %struct.crypto_hash_sha256_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %arraydecay = getelementptr inbounds [72 x i32], [72 x i32]* %tmp32, i32 0, i32 0
  call void @SHA256_Pad(%struct.crypto_hash_sha256_state* %0, i32* %arraydecay)
  %1 = load i8*, i8** %out.addr, align 4
  %2 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state1 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %2, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [8 x i32], [8 x i32]* %state1, i32 0, i32 0
  call void @be32enc_vect(i8* %1, i32* %arraydecay2, i32 32)
  %arraydecay3 = getelementptr inbounds [72 x i32], [72 x i32]* %tmp32, i32 0, i32 0
  %3 = bitcast i32* %arraydecay3 to i8*
  call void @sodium_memzero(i8* %3, i32 288)
  %4 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %5 = bitcast %struct.crypto_hash_sha256_state* %4 to i8*
  call void @sodium_memzero(i8* %5, i32 104)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @SHA256_Pad(%struct.crypto_hash_sha256_state* %state, i32* %tmp32) #0 {
entry:
  %state.addr = alloca %struct.crypto_hash_sha256_state*, align 4
  %tmp32.addr = alloca i32*, align 4
  %r = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.crypto_hash_sha256_state* %state, %struct.crypto_hash_sha256_state** %state.addr, align 4
  store i32* %tmp32, i32** %tmp32.addr, align 4
  %0 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %count = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %0, i32 0, i32 1
  %1 = load i64, i64* %count, align 8
  %shr = lshr i64 %1, 3
  %and = and i64 %shr, 63
  %conv = trunc i64 %and to i32
  store i32 %conv, i32* %r, align 4
  %2 = load i32, i32* %r, align 4
  %cmp = icmp ult i32 %2, 56
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %r, align 4
  %sub = sub i32 56, %4
  %cmp2 = icmp ult i32 %3, %sub
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [64 x i8], [64 x i8]* bitcast (<{ i8, [63 x i8] }>* @PAD to [64 x i8]*), i32 0, i32 %5
  %6 = load i8, i8* %arrayidx, align 1
  %7 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %7, i32 0, i32 2
  %8 = load i32, i32* %r, align 4
  %9 = load i32, i32* %i, align 4
  %add = add i32 %8, %9
  %arrayidx4 = getelementptr [64 x i8], [64 x i8]* %buf, i32 0, i32 %add
  store i8 %6, i8* %arrayidx4, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc14, %if.else
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %r, align 4
  %sub6 = sub i32 64, %12
  %cmp7 = icmp ult i32 %11, %sub6
  br i1 %cmp7, label %for.body9, label %for.end16

for.body9:                                        ; preds = %for.cond5
  %13 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr [64 x i8], [64 x i8]* bitcast (<{ i8, [63 x i8] }>* @PAD to [64 x i8]*), i32 0, i32 %13
  %14 = load i8, i8* %arrayidx10, align 1
  %15 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf11 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %15, i32 0, i32 2
  %16 = load i32, i32* %r, align 4
  %17 = load i32, i32* %i, align 4
  %add12 = add i32 %16, %17
  %arrayidx13 = getelementptr [64 x i8], [64 x i8]* %buf11, i32 0, i32 %add12
  store i8 %14, i8* %arrayidx13, align 1
  br label %for.inc14

for.inc14:                                        ; preds = %for.body9
  %18 = load i32, i32* %i, align 4
  %inc15 = add i32 %18, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond5

for.end16:                                        ; preds = %for.cond5
  %19 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state17 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %19, i32 0, i32 0
  %arraydecay = getelementptr inbounds [8 x i32], [8 x i32]* %state17, i32 0, i32 0
  %20 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf18 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %20, i32 0, i32 2
  %arraydecay19 = getelementptr inbounds [64 x i8], [64 x i8]* %buf18, i32 0, i32 0
  %21 = load i32*, i32** %tmp32.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %21, i32 0
  %22 = load i32*, i32** %tmp32.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %22, i32 64
  call void @SHA256_Transform(i32* %arraydecay, i8* %arraydecay19, i32* %arrayidx20, i32* %arrayidx21)
  %23 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf22 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %23, i32 0, i32 2
  %arrayidx23 = getelementptr [64 x i8], [64 x i8]* %buf22, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 8 %arrayidx23, i8 0, i32 56, i1 false)
  br label %if.end

if.end:                                           ; preds = %for.end16, %for.end
  %24 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf24 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %24, i32 0, i32 2
  %arrayidx25 = getelementptr [64 x i8], [64 x i8]* %buf24, i32 0, i32 56
  %25 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %count26 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %25, i32 0, i32 1
  %26 = load i64, i64* %count26, align 8
  call void @store64_be(i8* %arrayidx25, i64 %26)
  %27 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %state27 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %27, i32 0, i32 0
  %arraydecay28 = getelementptr inbounds [8 x i32], [8 x i32]* %state27, i32 0, i32 0
  %28 = load %struct.crypto_hash_sha256_state*, %struct.crypto_hash_sha256_state** %state.addr, align 4
  %buf29 = getelementptr inbounds %struct.crypto_hash_sha256_state, %struct.crypto_hash_sha256_state* %28, i32 0, i32 2
  %arraydecay30 = getelementptr inbounds [64 x i8], [64 x i8]* %buf29, i32 0, i32 0
  %29 = load i32*, i32** %tmp32.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %29, i32 0
  %30 = load i32*, i32** %tmp32.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %30, i32 64
  call void @SHA256_Transform(i32* %arraydecay28, i8* %arraydecay30, i32* %arrayidx31, i32* %arrayidx32)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @be32enc_vect(i8* %dst, i32* %src, i32 %len) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %src.addr = alloca i32*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32* %src, i32** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %div = udiv i32 %1, 4
  %cmp = icmp ult i32 %0, %div
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %dst.addr, align 4
  %3 = load i32, i32* %i, align 4
  %mul = mul i32 %3, 4
  %add.ptr = getelementptr i8, i8* %2, i32 %mul
  %4 = load i32*, i32** %src.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  call void @store32_be(i8* %add.ptr, i32 %6)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_hash_sha256(i8* nonnull %out, i8* %in, i64 %inlen) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %state = alloca %struct.crypto_hash_sha256_state, align 8
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %call = call i32 @crypto_hash_sha256_init(%struct.crypto_hash_sha256_state* %state)
  %0 = load i8*, i8** %in.addr, align 4
  %1 = load i64, i64* %inlen.addr, align 8
  %call1 = call i32 @crypto_hash_sha256_update(%struct.crypto_hash_sha256_state* %state, i8* %0, i64 %1)
  %2 = load i8*, i8** %out.addr, align 4
  %call2 = call i32 @crypto_hash_sha256_final(%struct.crypto_hash_sha256_state* %state, i8* %2)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @be32dec_vect(i32* %dst, i8* %src, i32 %len) #0 {
entry:
  %dst.addr = alloca i32*, align 4
  %src.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32* %dst, i32** %dst.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %div = udiv i32 %1, 4
  %cmp = icmp ult i32 %0, %div
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %src.addr, align 4
  %3 = load i32, i32* %i, align 4
  %mul = mul i32 %3, 4
  %add.ptr = getelementptr i8, i8* %2, i32 %mul
  %call = call i32 @load32_be(i8* %add.ptr)
  %4 = load i32*, i32** %dst.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i32, i32* %4, i32 %5
  store i32 %call, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @rotr32(i32 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %shr = lshr i32 %0, %1
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 32, %3
  %shl = shl i32 %2, %sub
  %or = or i32 %shr, %shl
  ret i32 %or
}

; Function Attrs: noinline nounwind optnone
define internal i32 @load32_be(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8*, i8** %src.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 3
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  store i32 %conv, i32* %w, align 4
  %2 = load i8*, i8** %src.addr, align 4
  %arrayidx1 = getelementptr i8, i8* %2, i32 2
  %3 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %3 to i32
  %shl = shl i32 %conv2, 8
  %4 = load i32, i32* %w, align 4
  %or = or i32 %4, %shl
  store i32 %or, i32* %w, align 4
  %5 = load i8*, i8** %src.addr, align 4
  %arrayidx3 = getelementptr i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %6 to i32
  %shl5 = shl i32 %conv4, 16
  %7 = load i32, i32* %w, align 4
  %or6 = or i32 %7, %shl5
  store i32 %or6, i32* %w, align 4
  %8 = load i8*, i8** %src.addr, align 4
  %arrayidx7 = getelementptr i8, i8* %8, i32 0
  %9 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %9 to i32
  %shl9 = shl i32 %conv8, 24
  %10 = load i32, i32* %w, align 4
  %or10 = or i32 %10, %shl9
  store i32 %or10, i32* %w, align 4
  %11 = load i32, i32* %w, align 4
  ret i32 %11
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal void @store64_be(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i64, i64* %w.addr, align 8
  %conv = trunc i64 %0 to i8
  %1 = load i8*, i8** %dst.addr, align 4
  %arrayidx = getelementptr i8, i8* %1, i32 7
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i64, i64* %w.addr, align 8
  %shr = lshr i64 %2, 8
  store i64 %shr, i64* %w.addr, align 8
  %3 = load i64, i64* %w.addr, align 8
  %conv1 = trunc i64 %3 to i8
  %4 = load i8*, i8** %dst.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %4, i32 6
  store i8 %conv1, i8* %arrayidx2, align 1
  %5 = load i64, i64* %w.addr, align 8
  %shr3 = lshr i64 %5, 8
  store i64 %shr3, i64* %w.addr, align 8
  %6 = load i64, i64* %w.addr, align 8
  %conv4 = trunc i64 %6 to i8
  %7 = load i8*, i8** %dst.addr, align 4
  %arrayidx5 = getelementptr i8, i8* %7, i32 5
  store i8 %conv4, i8* %arrayidx5, align 1
  %8 = load i64, i64* %w.addr, align 8
  %shr6 = lshr i64 %8, 8
  store i64 %shr6, i64* %w.addr, align 8
  %9 = load i64, i64* %w.addr, align 8
  %conv7 = trunc i64 %9 to i8
  %10 = load i8*, i8** %dst.addr, align 4
  %arrayidx8 = getelementptr i8, i8* %10, i32 4
  store i8 %conv7, i8* %arrayidx8, align 1
  %11 = load i64, i64* %w.addr, align 8
  %shr9 = lshr i64 %11, 8
  store i64 %shr9, i64* %w.addr, align 8
  %12 = load i64, i64* %w.addr, align 8
  %conv10 = trunc i64 %12 to i8
  %13 = load i8*, i8** %dst.addr, align 4
  %arrayidx11 = getelementptr i8, i8* %13, i32 3
  store i8 %conv10, i8* %arrayidx11, align 1
  %14 = load i64, i64* %w.addr, align 8
  %shr12 = lshr i64 %14, 8
  store i64 %shr12, i64* %w.addr, align 8
  %15 = load i64, i64* %w.addr, align 8
  %conv13 = trunc i64 %15 to i8
  %16 = load i8*, i8** %dst.addr, align 4
  %arrayidx14 = getelementptr i8, i8* %16, i32 2
  store i8 %conv13, i8* %arrayidx14, align 1
  %17 = load i64, i64* %w.addr, align 8
  %shr15 = lshr i64 %17, 8
  store i64 %shr15, i64* %w.addr, align 8
  %18 = load i64, i64* %w.addr, align 8
  %conv16 = trunc i64 %18 to i8
  %19 = load i8*, i8** %dst.addr, align 4
  %arrayidx17 = getelementptr i8, i8* %19, i32 1
  store i8 %conv16, i8* %arrayidx17, align 1
  %20 = load i64, i64* %w.addr, align 8
  %shr18 = lshr i64 %20, 8
  store i64 %shr18, i64* %w.addr, align 8
  %21 = load i64, i64* %w.addr, align 8
  %conv19 = trunc i64 %21 to i8
  %22 = load i8*, i8** %dst.addr, align 4
  %arrayidx20 = getelementptr i8, i8* %22, i32 0
  store i8 %conv19, i8* %arrayidx20, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store32_be(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i32, i32* %w.addr, align 4
  %conv = trunc i32 %0 to i8
  %1 = load i8*, i8** %dst.addr, align 4
  %arrayidx = getelementptr i8, i8* %1, i32 3
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i32, i32* %w.addr, align 4
  %shr = lshr i32 %2, 8
  store i32 %shr, i32* %w.addr, align 4
  %3 = load i32, i32* %w.addr, align 4
  %conv1 = trunc i32 %3 to i8
  %4 = load i8*, i8** %dst.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %4, i32 2
  store i8 %conv1, i8* %arrayidx2, align 1
  %5 = load i32, i32* %w.addr, align 4
  %shr3 = lshr i32 %5, 8
  store i32 %shr3, i32* %w.addr, align 4
  %6 = load i32, i32* %w.addr, align 4
  %conv4 = trunc i32 %6 to i8
  %7 = load i8*, i8** %dst.addr, align 4
  %arrayidx5 = getelementptr i8, i8* %7, i32 1
  store i8 %conv4, i8* %arrayidx5, align 1
  %8 = load i32, i32* %w.addr, align 4
  %shr6 = lshr i32 %8, 8
  store i32 %shr6, i32* %w.addr, align 4
  %9 = load i32, i32* %w.addr, align 4
  %conv7 = trunc i32 %9 to i8
  %10 = load i8*, i8** %dst.addr, align 4
  %arrayidx8 = getelementptr i8, i8* %10, i32 0
  store i8 %conv7, i8* %arrayidx8, align 1
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
