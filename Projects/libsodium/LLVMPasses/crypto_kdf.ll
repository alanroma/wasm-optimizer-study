; ModuleID = 'crypto_kdf/crypto_kdf.c'
source_filename = "crypto_kdf/crypto_kdf.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [8 x i8] c"blake2b\00", align 1

; Function Attrs: noinline nounwind optnone
define i8* @crypto_kdf_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_bytes_max() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_contextbytes() #0 {
entry:
  ret i32 8
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kdf_derive_from_key(i8* nonnull %subkey, i32 %subkey_len, i64 %subkey_id, i8* nonnull %ctx, i8* nonnull %key) #0 {
entry:
  %subkey.addr = alloca i8*, align 4
  %subkey_len.addr = alloca i32, align 4
  %subkey_id.addr = alloca i64, align 8
  %ctx.addr = alloca i8*, align 4
  %key.addr = alloca i8*, align 4
  store i8* %subkey, i8** %subkey.addr, align 4
  store i32 %subkey_len, i32* %subkey_len.addr, align 4
  store i64 %subkey_id, i64* %subkey_id.addr, align 8
  store i8* %ctx, i8** %ctx.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  %0 = load i8*, i8** %subkey.addr, align 4
  %1 = load i32, i32* %subkey_len.addr, align 4
  %2 = load i64, i64* %subkey_id.addr, align 8
  %3 = load i8*, i8** %ctx.addr, align 4
  %4 = load i8*, i8** %key.addr, align 4
  %call = call i32 @crypto_kdf_blake2b_derive_from_key(i8* %0, i32 %1, i64 %2, i8* %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_kdf_blake2b_derive_from_key(i8*, i32, i64, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_kdf_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
