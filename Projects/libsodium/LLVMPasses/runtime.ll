; ModuleID = 'sodium/runtime.c'
source_filename = "sodium/runtime.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.CPUFeatures_ = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }

@_cpu_features = internal global %struct.CPUFeatures_ zeroinitializer, align 4

; Function Attrs: noinline nounwind optnone
define hidden i32 @_sodium_runtime_get_cpu_features() #0 {
entry:
  %ret = alloca i32, align 4
  store i32 -1, i32* %ret, align 4
  %call = call i32 @_sodium_runtime_arm_cpu_features(%struct.CPUFeatures_* @_cpu_features)
  %0 = load i32, i32* %ret, align 4
  %and = and i32 %0, %call
  store i32 %and, i32* %ret, align 4
  %call1 = call i32 @_sodium_runtime_intel_cpu_features(%struct.CPUFeatures_* @_cpu_features)
  %1 = load i32, i32* %ret, align 4
  %and2 = and i32 %1, %call1
  store i32 %and2, i32* %ret, align 4
  store i32 1, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 0), align 4
  %2 = load i32, i32* %ret, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_sodium_runtime_arm_cpu_features(%struct.CPUFeatures_* %cpu_features) #0 {
entry:
  %cpu_features.addr = alloca %struct.CPUFeatures_*, align 4
  store %struct.CPUFeatures_* %cpu_features, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %0 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_neon = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %0, i32 0, i32 1
  store i32 0, i32* %has_neon, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_sodium_runtime_intel_cpu_features(%struct.CPUFeatures_* %cpu_features) #0 {
entry:
  %retval = alloca i32, align 4
  %cpu_features.addr = alloca %struct.CPUFeatures_*, align 4
  %cpu_info = alloca [4 x i32], align 16
  %id = alloca i32, align 4
  %xcr0 = alloca i32, align 4
  store %struct.CPUFeatures_* %cpu_features, %struct.CPUFeatures_** %cpu_features.addr, align 4
  store i32 0, i32* %xcr0, align 4
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %cpu_info, i32 0, i32 0
  call void @_cpuid(i32* %arraydecay, i32 0)
  %arrayidx = getelementptr [4 x i32], [4 x i32]* %cpu_info, i32 0, i32 0
  %0 = load i32, i32* %arrayidx, align 16
  store i32 %0, i32* %id, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay1 = getelementptr inbounds [4 x i32], [4 x i32]* %cpu_info, i32 0, i32 0
  call void @_cpuid(i32* %arraydecay1, i32 1)
  %1 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_sse2 = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %1, i32 0, i32 2
  store i32 0, i32* %has_sse2, align 4
  %2 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_sse3 = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %2, i32 0, i32 3
  store i32 0, i32* %has_sse3, align 4
  %3 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_ssse3 = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %3, i32 0, i32 4
  store i32 0, i32* %has_ssse3, align 4
  %4 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_sse41 = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %4, i32 0, i32 5
  store i32 0, i32* %has_sse41, align 4
  %5 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_avx = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %5, i32 0, i32 6
  store i32 0, i32* %has_avx, align 4
  %6 = load i32, i32* %xcr0, align 4
  %7 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_avx2 = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %7, i32 0, i32 7
  store i32 0, i32* %has_avx2, align 4
  %8 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_avx512f = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %8, i32 0, i32 8
  store i32 0, i32* %has_avx512f, align 4
  %9 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_pclmul = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %9, i32 0, i32 9
  store i32 0, i32* %has_pclmul, align 4
  %10 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_aesni = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %10, i32 0, i32 10
  store i32 0, i32* %has_aesni, align 4
  %11 = load %struct.CPUFeatures_*, %struct.CPUFeatures_** %cpu_features.addr, align 4
  %has_rdrand = getelementptr inbounds %struct.CPUFeatures_, %struct.CPUFeatures_* %11, i32 0, i32 11
  store i32 0, i32* %has_rdrand, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_neon() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 1), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_sse2() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 2), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_sse3() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 3), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_ssse3() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 4), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_sse41() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 5), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_avx() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 6), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_avx2() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 7), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_avx512f() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 8), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_pclmul() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 9), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_aesni() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 10), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_runtime_has_rdrand() #0 {
entry:
  %0 = load i32, i32* getelementptr inbounds (%struct.CPUFeatures_, %struct.CPUFeatures_* @_cpu_features, i32 0, i32 11), align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define internal void @_cpuid(i32* %cpu_info, i32 %cpu_info_type) #0 {
entry:
  %cpu_info.addr = alloca i32*, align 4
  %cpu_info_type.addr = alloca i32, align 4
  store i32* %cpu_info, i32** %cpu_info.addr, align 4
  store i32 %cpu_info_type, i32* %cpu_info_type.addr, align 4
  %0 = load i32, i32* %cpu_info_type.addr, align 4
  %1 = load i32*, i32** %cpu_info.addr, align 4
  %arrayidx = getelementptr i32, i32* %1, i32 3
  store i32 0, i32* %arrayidx, align 4
  %2 = load i32*, i32** %cpu_info.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 2
  store i32 0, i32* %arrayidx1, align 4
  %3 = load i32*, i32** %cpu_info.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %3, i32 1
  store i32 0, i32* %arrayidx2, align 4
  %4 = load i32*, i32** %cpu_info.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %4, i32 0
  store i32 0, i32* %arrayidx3, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
