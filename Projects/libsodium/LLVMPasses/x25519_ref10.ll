; ModuleID = 'crypto_scalarmult/curve25519/ref10/x25519_ref10.c'
source_filename = "crypto_scalarmult/curve25519/ref10/x25519_ref10.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_scalarmult_curve25519_implementation = type { i32 (i8*, i8*, i8*)*, i32 (i8*, i8*)* }
%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

@crypto_scalarmult_curve25519_ref10_implementation = hidden global %struct.crypto_scalarmult_curve25519_implementation { i32 (i8*, i8*, i8*)* @crypto_scalarmult_curve25519_ref10, i32 (i8*, i8*)* @crypto_scalarmult_curve25519_ref10_base }, align 4
@has_small_order.blacklist = internal constant <{ [32 x i8], <{ i8, [31 x i8] }>, [32 x i8], [32 x i8], [32 x i8], [32 x i8], [32 x i8] }> <{ [32 x i8] zeroinitializer, <{ i8, [31 x i8] }> <{ i8 1, [31 x i8] zeroinitializer }>, [32 x i8] c"\E0\EBz|;A\B8\AE\16V\E3\FA\F1\9F\C4j\DA\09\8D\EB\9C2\B1\FD\86b\05\16_I\B8\00", [32 x i8] c"_\9C\95\BC\A3P\8C$\B1\D0\B1U\9C\83\EF[\04D\\\C4X\1C\8E\86\D8\22N\DD\D0\9F\11W", [32 x i8] c"\EC\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\7F", [32 x i8] c"\ED\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\7F", [32 x i8] c"\EE\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\FF\7F" }>, align 16

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_scalarmult_curve25519_ref10(i8* %q, i8* %n, i8* %p) #0 {
entry:
  %retval = alloca i32, align 4
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %t = alloca i8*, align 4
  %i = alloca i32, align 4
  %x1 = alloca [10 x i32], align 16
  %x2 = alloca [10 x i32], align 16
  %z2 = alloca [10 x i32], align 16
  %x3 = alloca [10 x i32], align 16
  %z3 = alloca [10 x i32], align 16
  %tmp0 = alloca [10 x i32], align 16
  %tmp1 = alloca [10 x i32], align 16
  %pos = alloca i32, align 4
  %swap = alloca i32, align 4
  %b = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  %1 = load i8*, i8** %p.addr, align 4
  %call = call i32 @has_small_order(i8* %1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 %4
  %5 = load i8, i8* %arrayidx, align 1
  %6 = load i8*, i8** %t, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %6, i32 %7
  store i8 %5, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i8*, i8** %t, align 4
  %arrayidx2 = getelementptr i8, i8* %9, i32 0
  %10 = load i8, i8* %arrayidx2, align 1
  %conv = zext i8 %10 to i32
  %and = and i32 %conv, 248
  %conv3 = trunc i32 %and to i8
  store i8 %conv3, i8* %arrayidx2, align 1
  %11 = load i8*, i8** %t, align 4
  %arrayidx4 = getelementptr i8, i8* %11, i32 31
  %12 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %12 to i32
  %and6 = and i32 %conv5, 127
  %conv7 = trunc i32 %and6 to i8
  store i8 %conv7, i8* %arrayidx4, align 1
  %13 = load i8*, i8** %t, align 4
  %arrayidx8 = getelementptr i8, i8* %13, i32 31
  %14 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %14 to i32
  %or = or i32 %conv9, 64
  %conv10 = trunc i32 %or to i8
  store i8 %conv10, i8* %arrayidx8, align 1
  %arraydecay = getelementptr inbounds [10 x i32], [10 x i32]* %x1, i32 0, i32 0
  %15 = load i8*, i8** %p.addr, align 4
  call void @fe25519_frombytes(i32* %arraydecay, i8* %15)
  %arraydecay11 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  call void @fe25519_1(i32* %arraydecay11)
  %arraydecay12 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_0(i32* %arraydecay12)
  %arraydecay13 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %arraydecay14 = getelementptr inbounds [10 x i32], [10 x i32]* %x1, i32 0, i32 0
  call void @fe25519_copy(i32* %arraydecay13, i32* %arraydecay14)
  %arraydecay15 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  call void @fe25519_1(i32* %arraydecay15)
  store i32 0, i32* %swap, align 4
  store i32 254, i32* %pos, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc77, %for.end
  %16 = load i32, i32* %pos, align 4
  %cmp17 = icmp sge i32 %16, 0
  br i1 %cmp17, label %for.body19, label %for.end78

for.body19:                                       ; preds = %for.cond16
  %17 = load i8*, i8** %t, align 4
  %18 = load i32, i32* %pos, align 4
  %div = sdiv i32 %18, 8
  %arrayidx20 = getelementptr i8, i8* %17, i32 %div
  %19 = load i8, i8* %arrayidx20, align 1
  %conv21 = zext i8 %19 to i32
  %20 = load i32, i32* %pos, align 4
  %and22 = and i32 %20, 7
  %shr = ashr i32 %conv21, %and22
  store i32 %shr, i32* %b, align 4
  %21 = load i32, i32* %b, align 4
  %and23 = and i32 %21, 1
  store i32 %and23, i32* %b, align 4
  %22 = load i32, i32* %b, align 4
  %23 = load i32, i32* %swap, align 4
  %xor = xor i32 %23, %22
  store i32 %xor, i32* %swap, align 4
  %arraydecay24 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay25 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %24 = load i32, i32* %swap, align 4
  call void @fe25519_cswap(i32* %arraydecay24, i32* %arraydecay25, i32 %24)
  %arraydecay26 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay27 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %25 = load i32, i32* %swap, align 4
  call void @fe25519_cswap(i32* %arraydecay26, i32* %arraydecay27, i32 %25)
  %26 = load i32, i32* %b, align 4
  store i32 %26, i32* %swap, align 4
  %arraydecay28 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  %arraydecay29 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %arraydecay30 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  call void @fe25519_sub(i32* %arraydecay28, i32* %arraydecay29, i32* %arraydecay30)
  %arraydecay31 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay33 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_sub(i32* %arraydecay31, i32* %arraydecay32, i32* %arraydecay33)
  %arraydecay34 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay35 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay36 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_add(i32* %arraydecay34, i32* %arraydecay35, i32* %arraydecay36)
  %arraydecay37 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay38 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %arraydecay39 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  call void @fe25519_add(i32* %arraydecay37, i32* %arraydecay38, i32* %arraydecay39)
  %arraydecay40 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %arraydecay41 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  %arraydecay42 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay40, i32* %arraydecay41, i32* %arraydecay42)
  %arraydecay43 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay44 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay45 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay43, i32* %arraydecay44, i32* %arraydecay45)
  %arraydecay46 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  %arraydecay47 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  call void @fe25519_sq(i32* %arraydecay46, i32* %arraydecay47)
  %arraydecay48 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay49 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  call void @fe25519_sq(i32* %arraydecay48, i32* %arraydecay49)
  %arraydecay50 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %arraydecay51 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %arraydecay52 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_add(i32* %arraydecay50, i32* %arraydecay51, i32* %arraydecay52)
  %arraydecay53 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay54 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %arraydecay55 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_sub(i32* %arraydecay53, i32* %arraydecay54, i32* %arraydecay55)
  %arraydecay56 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay57 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay58 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay56, i32* %arraydecay57, i32* %arraydecay58)
  %arraydecay59 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay60 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  call void @fe25519_sub(i32* %arraydecay59, i32* %arraydecay60, i32* %arraydecay61)
  %arraydecay62 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay63 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_sq(i32* %arraydecay62, i32* %arraydecay63)
  %arraydecay64 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %arraydecay65 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  call void @fe25519_mul32(i32* %arraydecay64, i32* %arraydecay65, i32 121666)
  %arraydecay66 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %arraydecay67 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  call void @fe25519_sq(i32* %arraydecay66, i32* %arraydecay67)
  %arraydecay68 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  %arraydecay69 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  %arraydecay70 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  call void @fe25519_add(i32* %arraydecay68, i32* %arraydecay69, i32* %arraydecay70)
  %arraydecay71 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %arraydecay72 = getelementptr inbounds [10 x i32], [10 x i32]* %x1, i32 0, i32 0
  %arraydecay73 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay71, i32* %arraydecay72, i32* %arraydecay73)
  %arraydecay74 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay75 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp1, i32 0, i32 0
  %arraydecay76 = getelementptr inbounds [10 x i32], [10 x i32]* %tmp0, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay74, i32* %arraydecay75, i32* %arraydecay76)
  br label %for.inc77

for.inc77:                                        ; preds = %for.body19
  %27 = load i32, i32* %pos, align 4
  %dec = add i32 %27, -1
  store i32 %dec, i32* %pos, align 4
  br label %for.cond16

for.end78:                                        ; preds = %for.cond16
  %arraydecay79 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay80 = getelementptr inbounds [10 x i32], [10 x i32]* %x3, i32 0, i32 0
  %28 = load i32, i32* %swap, align 4
  call void @fe25519_cswap(i32* %arraydecay79, i32* %arraydecay80, i32 %28)
  %arraydecay81 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay82 = getelementptr inbounds [10 x i32], [10 x i32]* %z3, i32 0, i32 0
  %29 = load i32, i32* %swap, align 4
  call void @fe25519_cswap(i32* %arraydecay81, i32* %arraydecay82, i32 %29)
  %arraydecay83 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  %arraydecay84 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_invert(i32* %arraydecay83, i32* %arraydecay84)
  %arraydecay85 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay86 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  %arraydecay87 = getelementptr inbounds [10 x i32], [10 x i32]* %z2, i32 0, i32 0
  call void @fe25519_mul(i32* %arraydecay85, i32* %arraydecay86, i32* %arraydecay87)
  %30 = load i8*, i8** %q.addr, align 4
  %arraydecay88 = getelementptr inbounds [10 x i32], [10 x i32]* %x2, i32 0, i32 0
  call void @fe25519_tobytes(i8* %30, i32* %arraydecay88)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end78, %if.then
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_scalarmult_curve25519_ref10_base(i8* %q, i8* %n) #0 {
entry:
  %q.addr = alloca i8*, align 4
  %n.addr = alloca i8*, align 4
  %t = alloca i8*, align 4
  %A = alloca %struct.ge25519_p3, align 4
  %pk = alloca [10 x i32], align 16
  %i = alloca i32, align 4
  store i8* %q, i8** %q.addr, align 4
  store i8* %n, i8** %n.addr, align 4
  %0 = load i8*, i8** %q.addr, align 4
  store i8* %0, i8** %t, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i8*, i8** %t, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i8*, i8** %t, align 4
  %arrayidx2 = getelementptr i8, i8* %8, i32 0
  %9 = load i8, i8* %arrayidx2, align 1
  %conv = zext i8 %9 to i32
  %and = and i32 %conv, 248
  %conv3 = trunc i32 %and to i8
  store i8 %conv3, i8* %arrayidx2, align 1
  %10 = load i8*, i8** %t, align 4
  %arrayidx4 = getelementptr i8, i8* %10, i32 31
  %11 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %11 to i32
  %and6 = and i32 %conv5, 127
  %conv7 = trunc i32 %and6 to i8
  store i8 %conv7, i8* %arrayidx4, align 1
  %12 = load i8*, i8** %t, align 4
  %arrayidx8 = getelementptr i8, i8* %12, i32 31
  %13 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %13 to i32
  %or = or i32 %conv9, 64
  %conv10 = trunc i32 %or to i8
  store i8 %conv10, i8* %arrayidx8, align 1
  %14 = load i8*, i8** %t, align 4
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %A, i8* %14)
  %arraydecay = getelementptr inbounds [10 x i32], [10 x i32]* %pk, i32 0, i32 0
  %Y = getelementptr inbounds %struct.ge25519_p3, %struct.ge25519_p3* %A, i32 0, i32 1
  %arraydecay11 = getelementptr inbounds [10 x i32], [10 x i32]* %Y, i32 0, i32 0
  %Z = getelementptr inbounds %struct.ge25519_p3, %struct.ge25519_p3* %A, i32 0, i32 2
  %arraydecay12 = getelementptr inbounds [10 x i32], [10 x i32]* %Z, i32 0, i32 0
  call void @edwards_to_montgomery(i32* %arraydecay, i32* %arraydecay11, i32* %arraydecay12)
  %15 = load i8*, i8** %q.addr, align 4
  %arraydecay13 = getelementptr inbounds [10 x i32], [10 x i32]* %pk, i32 0, i32 0
  call void @fe25519_tobytes(i8* %15, i32* %arraydecay13)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @has_small_order(i8* %s) #0 {
entry:
  %s.addr = alloca i8*, align 4
  %c = alloca [7 x i8], align 1
  %k = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = bitcast [7 x i8]* %c to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 7, i1 false)
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %1 = load i32, i32* %j, align 4
  %cmp = icmp ult i32 %1, 31
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %2 = load i32, i32* %i, align 4
  %cmp2 = icmp ult i32 %2, 7
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %3 = load i8*, i8** %s.addr, align 4
  %4 = load i32, i32* %j, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 %4
  %5 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %5 to i32
  %6 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr [7 x [32 x i8]], [7 x [32 x i8]]* bitcast (<{ [32 x i8], <{ i8, [31 x i8] }>, [32 x i8], [32 x i8], [32 x i8], [32 x i8], [32 x i8] }>* @has_small_order.blacklist to [7 x [32 x i8]]*), i32 0, i32 %6
  %7 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr [32 x i8], [32 x i8]* %arrayidx4, i32 0, i32 %7
  %8 = load i8, i8* %arrayidx5, align 1
  %conv6 = zext i8 %8 to i32
  %xor = xor i32 %conv, %conv6
  %9 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr [7 x i8], [7 x i8]* %c, i32 0, i32 %9
  %10 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %10 to i32
  %or = or i32 %conv8, %xor
  %conv9 = trunc i32 %or to i8
  store i8 %conv9, i8* %arrayidx7, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %11 = load i32, i32* %i, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %12 = load i32, i32* %j, align 4
  %inc11 = add i32 %12, 1
  store i32 %inc11, i32* %j, align 4
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc27, %for.end12
  %13 = load i32, i32* %i, align 4
  %cmp14 = icmp ult i32 %13, 7
  br i1 %cmp14, label %for.body16, label %for.end29

for.body16:                                       ; preds = %for.cond13
  %14 = load i8*, i8** %s.addr, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr i8, i8* %14, i32 %15
  %16 = load i8, i8* %arrayidx17, align 1
  %conv18 = zext i8 %16 to i32
  %and = and i32 %conv18, 127
  %17 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr [7 x [32 x i8]], [7 x [32 x i8]]* bitcast (<{ [32 x i8], <{ i8, [31 x i8] }>, [32 x i8], [32 x i8], [32 x i8], [32 x i8], [32 x i8] }>* @has_small_order.blacklist to [7 x [32 x i8]]*), i32 0, i32 %17
  %18 = load i32, i32* %j, align 4
  %arrayidx20 = getelementptr [32 x i8], [32 x i8]* %arrayidx19, i32 0, i32 %18
  %19 = load i8, i8* %arrayidx20, align 1
  %conv21 = zext i8 %19 to i32
  %xor22 = xor i32 %and, %conv21
  %20 = load i32, i32* %i, align 4
  %arrayidx23 = getelementptr [7 x i8], [7 x i8]* %c, i32 0, i32 %20
  %21 = load i8, i8* %arrayidx23, align 1
  %conv24 = zext i8 %21 to i32
  %or25 = or i32 %conv24, %xor22
  %conv26 = trunc i32 %or25 to i8
  store i8 %conv26, i8* %arrayidx23, align 1
  br label %for.inc27

for.inc27:                                        ; preds = %for.body16
  %22 = load i32, i32* %i, align 4
  %inc28 = add i32 %22, 1
  store i32 %inc28, i32* %i, align 4
  br label %for.cond13

for.end29:                                        ; preds = %for.cond13
  store i32 0, i32* %k, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc37, %for.end29
  %23 = load i32, i32* %i, align 4
  %cmp31 = icmp ult i32 %23, 7
  br i1 %cmp31, label %for.body33, label %for.end39

for.body33:                                       ; preds = %for.cond30
  %24 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr [7 x i8], [7 x i8]* %c, i32 0, i32 %24
  %25 = load i8, i8* %arrayidx34, align 1
  %conv35 = zext i8 %25 to i32
  %sub = sub i32 %conv35, 1
  %26 = load i32, i32* %k, align 4
  %or36 = or i32 %26, %sub
  store i32 %or36, i32* %k, align 4
  br label %for.inc37

for.inc37:                                        ; preds = %for.body33
  %27 = load i32, i32* %i, align 4
  %inc38 = add i32 %27, 1
  store i32 %inc38, i32* %i, align 4
  br label %for.cond30

for.end39:                                        ; preds = %for.cond30
  %28 = load i32, i32* %k, align 4
  %shr = lshr i32 %28, 8
  %and40 = and i32 %shr, 1
  ret i32 %and40
}

declare void @fe25519_frombytes(i32*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_1(i32* %h) #0 {
entry:
  %h.addr = alloca i32*, align 4
  store i32* %h, i32** %h.addr, align 4
  %0 = load i32*, i32** %h.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  store i32 1, i32* %arrayidx, align 4
  %1 = load i32*, i32** %h.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %1, i32 1
  store i32 0, i32* %arrayidx1, align 4
  %2 = load i32*, i32** %h.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %2, i32 2
  %3 = bitcast i32* %arrayidx2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_0(i32* %h) #0 {
entry:
  %h.addr = alloca i32*, align 4
  store i32* %h, i32** %h.addr, align 4
  %0 = load i32*, i32** %h.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = bitcast i32* %arrayidx to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 40, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_copy(i32* %h, i32* %f) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %f0, align 4
  %2 = load i32*, i32** %f.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 1
  %3 = load i32, i32* %arrayidx1, align 4
  store i32 %3, i32* %f1, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 2
  %5 = load i32, i32* %arrayidx2, align 4
  store i32 %5, i32* %f2, align 4
  %6 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 3
  %7 = load i32, i32* %arrayidx3, align 4
  store i32 %7, i32* %f3, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %8, i32 4
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %f4, align 4
  %10 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %10, i32 5
  %11 = load i32, i32* %arrayidx5, align 4
  store i32 %11, i32* %f5, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %12, i32 6
  %13 = load i32, i32* %arrayidx6, align 4
  store i32 %13, i32* %f6, align 4
  %14 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %14, i32 7
  %15 = load i32, i32* %arrayidx7, align 4
  store i32 %15, i32* %f7, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %16, i32 8
  %17 = load i32, i32* %arrayidx8, align 4
  store i32 %17, i32* %f8, align 4
  %18 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %18, i32 9
  %19 = load i32, i32* %arrayidx9, align 4
  store i32 %19, i32* %f9, align 4
  %20 = load i32, i32* %f0, align 4
  %21 = load i32*, i32** %h.addr, align 4
  %arrayidx10 = getelementptr i32, i32* %21, i32 0
  store i32 %20, i32* %arrayidx10, align 4
  %22 = load i32, i32* %f1, align 4
  %23 = load i32*, i32** %h.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %23, i32 1
  store i32 %22, i32* %arrayidx11, align 4
  %24 = load i32, i32* %f2, align 4
  %25 = load i32*, i32** %h.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %25, i32 2
  store i32 %24, i32* %arrayidx12, align 4
  %26 = load i32, i32* %f3, align 4
  %27 = load i32*, i32** %h.addr, align 4
  %arrayidx13 = getelementptr i32, i32* %27, i32 3
  store i32 %26, i32* %arrayidx13, align 4
  %28 = load i32, i32* %f4, align 4
  %29 = load i32*, i32** %h.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %29, i32 4
  store i32 %28, i32* %arrayidx14, align 4
  %30 = load i32, i32* %f5, align 4
  %31 = load i32*, i32** %h.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %31, i32 5
  store i32 %30, i32* %arrayidx15, align 4
  %32 = load i32, i32* %f6, align 4
  %33 = load i32*, i32** %h.addr, align 4
  %arrayidx16 = getelementptr i32, i32* %33, i32 6
  store i32 %32, i32* %arrayidx16, align 4
  %34 = load i32, i32* %f7, align 4
  %35 = load i32*, i32** %h.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %35, i32 7
  store i32 %34, i32* %arrayidx17, align 4
  %36 = load i32, i32* %f8, align 4
  %37 = load i32*, i32** %h.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %37, i32 8
  store i32 %36, i32* %arrayidx18, align 4
  %38 = load i32, i32* %f9, align 4
  %39 = load i32*, i32** %h.addr, align 4
  %arrayidx19 = getelementptr i32, i32* %39, i32 9
  store i32 %38, i32* %arrayidx19, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_cswap(i32* %f, i32* %g, i32 %b) #0 {
entry:
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %b.addr = alloca i32, align 4
  %mask = alloca i32, align 4
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  %g0 = alloca i32, align 4
  %g1 = alloca i32, align 4
  %g2 = alloca i32, align 4
  %g3 = alloca i32, align 4
  %g4 = alloca i32, align 4
  %g5 = alloca i32, align 4
  %g6 = alloca i32, align 4
  %g7 = alloca i32, align 4
  %g8 = alloca i32, align 4
  %g9 = alloca i32, align 4
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x3 = alloca i32, align 4
  %x4 = alloca i32, align 4
  %x5 = alloca i32, align 4
  %x6 = alloca i32, align 4
  %x7 = alloca i32, align 4
  %x8 = alloca i32, align 4
  %x9 = alloca i32, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %b.addr, align 4
  %conv = zext i32 %0 to i64
  %sub = sub i64 0, %conv
  %conv1 = trunc i64 %sub to i32
  store i32 %conv1, i32* %mask, align 4
  %1 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %1, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  store i32 %2, i32* %f0, align 4
  %3 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %3, i32 1
  %4 = load i32, i32* %arrayidx2, align 4
  store i32 %4, i32* %f1, align 4
  %5 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %5, i32 2
  %6 = load i32, i32* %arrayidx3, align 4
  store i32 %6, i32* %f2, align 4
  %7 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %7, i32 3
  %8 = load i32, i32* %arrayidx4, align 4
  store i32 %8, i32* %f3, align 4
  %9 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %9, i32 4
  %10 = load i32, i32* %arrayidx5, align 4
  store i32 %10, i32* %f4, align 4
  %11 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %11, i32 5
  %12 = load i32, i32* %arrayidx6, align 4
  store i32 %12, i32* %f5, align 4
  %13 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %13, i32 6
  %14 = load i32, i32* %arrayidx7, align 4
  store i32 %14, i32* %f6, align 4
  %15 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %15, i32 7
  %16 = load i32, i32* %arrayidx8, align 4
  store i32 %16, i32* %f7, align 4
  %17 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %17, i32 8
  %18 = load i32, i32* %arrayidx9, align 4
  store i32 %18, i32* %f8, align 4
  %19 = load i32*, i32** %f.addr, align 4
  %arrayidx10 = getelementptr i32, i32* %19, i32 9
  %20 = load i32, i32* %arrayidx10, align 4
  store i32 %20, i32* %f9, align 4
  %21 = load i32*, i32** %g.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %21, i32 0
  %22 = load i32, i32* %arrayidx11, align 4
  store i32 %22, i32* %g0, align 4
  %23 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %23, i32 1
  %24 = load i32, i32* %arrayidx12, align 4
  store i32 %24, i32* %g1, align 4
  %25 = load i32*, i32** %g.addr, align 4
  %arrayidx13 = getelementptr i32, i32* %25, i32 2
  %26 = load i32, i32* %arrayidx13, align 4
  store i32 %26, i32* %g2, align 4
  %27 = load i32*, i32** %g.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %27, i32 3
  %28 = load i32, i32* %arrayidx14, align 4
  store i32 %28, i32* %g3, align 4
  %29 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %29, i32 4
  %30 = load i32, i32* %arrayidx15, align 4
  store i32 %30, i32* %g4, align 4
  %31 = load i32*, i32** %g.addr, align 4
  %arrayidx16 = getelementptr i32, i32* %31, i32 5
  %32 = load i32, i32* %arrayidx16, align 4
  store i32 %32, i32* %g5, align 4
  %33 = load i32*, i32** %g.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %33, i32 6
  %34 = load i32, i32* %arrayidx17, align 4
  store i32 %34, i32* %g6, align 4
  %35 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %35, i32 7
  %36 = load i32, i32* %arrayidx18, align 4
  store i32 %36, i32* %g7, align 4
  %37 = load i32*, i32** %g.addr, align 4
  %arrayidx19 = getelementptr i32, i32* %37, i32 8
  %38 = load i32, i32* %arrayidx19, align 4
  store i32 %38, i32* %g8, align 4
  %39 = load i32*, i32** %g.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %39, i32 9
  %40 = load i32, i32* %arrayidx20, align 4
  store i32 %40, i32* %g9, align 4
  %41 = load i32, i32* %f0, align 4
  %42 = load i32, i32* %g0, align 4
  %xor = xor i32 %41, %42
  store i32 %xor, i32* %x0, align 4
  %43 = load i32, i32* %f1, align 4
  %44 = load i32, i32* %g1, align 4
  %xor21 = xor i32 %43, %44
  store i32 %xor21, i32* %x1, align 4
  %45 = load i32, i32* %f2, align 4
  %46 = load i32, i32* %g2, align 4
  %xor22 = xor i32 %45, %46
  store i32 %xor22, i32* %x2, align 4
  %47 = load i32, i32* %f3, align 4
  %48 = load i32, i32* %g3, align 4
  %xor23 = xor i32 %47, %48
  store i32 %xor23, i32* %x3, align 4
  %49 = load i32, i32* %f4, align 4
  %50 = load i32, i32* %g4, align 4
  %xor24 = xor i32 %49, %50
  store i32 %xor24, i32* %x4, align 4
  %51 = load i32, i32* %f5, align 4
  %52 = load i32, i32* %g5, align 4
  %xor25 = xor i32 %51, %52
  store i32 %xor25, i32* %x5, align 4
  %53 = load i32, i32* %f6, align 4
  %54 = load i32, i32* %g6, align 4
  %xor26 = xor i32 %53, %54
  store i32 %xor26, i32* %x6, align 4
  %55 = load i32, i32* %f7, align 4
  %56 = load i32, i32* %g7, align 4
  %xor27 = xor i32 %55, %56
  store i32 %xor27, i32* %x7, align 4
  %57 = load i32, i32* %f8, align 4
  %58 = load i32, i32* %g8, align 4
  %xor28 = xor i32 %57, %58
  store i32 %xor28, i32* %x8, align 4
  %59 = load i32, i32* %f9, align 4
  %60 = load i32, i32* %g9, align 4
  %xor29 = xor i32 %59, %60
  store i32 %xor29, i32* %x9, align 4
  %61 = load i32, i32* %mask, align 4
  %62 = load i32, i32* %x0, align 4
  %and = and i32 %62, %61
  store i32 %and, i32* %x0, align 4
  %63 = load i32, i32* %mask, align 4
  %64 = load i32, i32* %x1, align 4
  %and30 = and i32 %64, %63
  store i32 %and30, i32* %x1, align 4
  %65 = load i32, i32* %mask, align 4
  %66 = load i32, i32* %x2, align 4
  %and31 = and i32 %66, %65
  store i32 %and31, i32* %x2, align 4
  %67 = load i32, i32* %mask, align 4
  %68 = load i32, i32* %x3, align 4
  %and32 = and i32 %68, %67
  store i32 %and32, i32* %x3, align 4
  %69 = load i32, i32* %mask, align 4
  %70 = load i32, i32* %x4, align 4
  %and33 = and i32 %70, %69
  store i32 %and33, i32* %x4, align 4
  %71 = load i32, i32* %mask, align 4
  %72 = load i32, i32* %x5, align 4
  %and34 = and i32 %72, %71
  store i32 %and34, i32* %x5, align 4
  %73 = load i32, i32* %mask, align 4
  %74 = load i32, i32* %x6, align 4
  %and35 = and i32 %74, %73
  store i32 %and35, i32* %x6, align 4
  %75 = load i32, i32* %mask, align 4
  %76 = load i32, i32* %x7, align 4
  %and36 = and i32 %76, %75
  store i32 %and36, i32* %x7, align 4
  %77 = load i32, i32* %mask, align 4
  %78 = load i32, i32* %x8, align 4
  %and37 = and i32 %78, %77
  store i32 %and37, i32* %x8, align 4
  %79 = load i32, i32* %mask, align 4
  %80 = load i32, i32* %x9, align 4
  %and38 = and i32 %80, %79
  store i32 %and38, i32* %x9, align 4
  %81 = load i32, i32* %f0, align 4
  %82 = load i32, i32* %x0, align 4
  %xor39 = xor i32 %81, %82
  %83 = load i32*, i32** %f.addr, align 4
  %arrayidx40 = getelementptr i32, i32* %83, i32 0
  store i32 %xor39, i32* %arrayidx40, align 4
  %84 = load i32, i32* %f1, align 4
  %85 = load i32, i32* %x1, align 4
  %xor41 = xor i32 %84, %85
  %86 = load i32*, i32** %f.addr, align 4
  %arrayidx42 = getelementptr i32, i32* %86, i32 1
  store i32 %xor41, i32* %arrayidx42, align 4
  %87 = load i32, i32* %f2, align 4
  %88 = load i32, i32* %x2, align 4
  %xor43 = xor i32 %87, %88
  %89 = load i32*, i32** %f.addr, align 4
  %arrayidx44 = getelementptr i32, i32* %89, i32 2
  store i32 %xor43, i32* %arrayidx44, align 4
  %90 = load i32, i32* %f3, align 4
  %91 = load i32, i32* %x3, align 4
  %xor45 = xor i32 %90, %91
  %92 = load i32*, i32** %f.addr, align 4
  %arrayidx46 = getelementptr i32, i32* %92, i32 3
  store i32 %xor45, i32* %arrayidx46, align 4
  %93 = load i32, i32* %f4, align 4
  %94 = load i32, i32* %x4, align 4
  %xor47 = xor i32 %93, %94
  %95 = load i32*, i32** %f.addr, align 4
  %arrayidx48 = getelementptr i32, i32* %95, i32 4
  store i32 %xor47, i32* %arrayidx48, align 4
  %96 = load i32, i32* %f5, align 4
  %97 = load i32, i32* %x5, align 4
  %xor49 = xor i32 %96, %97
  %98 = load i32*, i32** %f.addr, align 4
  %arrayidx50 = getelementptr i32, i32* %98, i32 5
  store i32 %xor49, i32* %arrayidx50, align 4
  %99 = load i32, i32* %f6, align 4
  %100 = load i32, i32* %x6, align 4
  %xor51 = xor i32 %99, %100
  %101 = load i32*, i32** %f.addr, align 4
  %arrayidx52 = getelementptr i32, i32* %101, i32 6
  store i32 %xor51, i32* %arrayidx52, align 4
  %102 = load i32, i32* %f7, align 4
  %103 = load i32, i32* %x7, align 4
  %xor53 = xor i32 %102, %103
  %104 = load i32*, i32** %f.addr, align 4
  %arrayidx54 = getelementptr i32, i32* %104, i32 7
  store i32 %xor53, i32* %arrayidx54, align 4
  %105 = load i32, i32* %f8, align 4
  %106 = load i32, i32* %x8, align 4
  %xor55 = xor i32 %105, %106
  %107 = load i32*, i32** %f.addr, align 4
  %arrayidx56 = getelementptr i32, i32* %107, i32 8
  store i32 %xor55, i32* %arrayidx56, align 4
  %108 = load i32, i32* %f9, align 4
  %109 = load i32, i32* %x9, align 4
  %xor57 = xor i32 %108, %109
  %110 = load i32*, i32** %f.addr, align 4
  %arrayidx58 = getelementptr i32, i32* %110, i32 9
  store i32 %xor57, i32* %arrayidx58, align 4
  %111 = load i32, i32* %g0, align 4
  %112 = load i32, i32* %x0, align 4
  %xor59 = xor i32 %111, %112
  %113 = load i32*, i32** %g.addr, align 4
  %arrayidx60 = getelementptr i32, i32* %113, i32 0
  store i32 %xor59, i32* %arrayidx60, align 4
  %114 = load i32, i32* %g1, align 4
  %115 = load i32, i32* %x1, align 4
  %xor61 = xor i32 %114, %115
  %116 = load i32*, i32** %g.addr, align 4
  %arrayidx62 = getelementptr i32, i32* %116, i32 1
  store i32 %xor61, i32* %arrayidx62, align 4
  %117 = load i32, i32* %g2, align 4
  %118 = load i32, i32* %x2, align 4
  %xor63 = xor i32 %117, %118
  %119 = load i32*, i32** %g.addr, align 4
  %arrayidx64 = getelementptr i32, i32* %119, i32 2
  store i32 %xor63, i32* %arrayidx64, align 4
  %120 = load i32, i32* %g3, align 4
  %121 = load i32, i32* %x3, align 4
  %xor65 = xor i32 %120, %121
  %122 = load i32*, i32** %g.addr, align 4
  %arrayidx66 = getelementptr i32, i32* %122, i32 3
  store i32 %xor65, i32* %arrayidx66, align 4
  %123 = load i32, i32* %g4, align 4
  %124 = load i32, i32* %x4, align 4
  %xor67 = xor i32 %123, %124
  %125 = load i32*, i32** %g.addr, align 4
  %arrayidx68 = getelementptr i32, i32* %125, i32 4
  store i32 %xor67, i32* %arrayidx68, align 4
  %126 = load i32, i32* %g5, align 4
  %127 = load i32, i32* %x5, align 4
  %xor69 = xor i32 %126, %127
  %128 = load i32*, i32** %g.addr, align 4
  %arrayidx70 = getelementptr i32, i32* %128, i32 5
  store i32 %xor69, i32* %arrayidx70, align 4
  %129 = load i32, i32* %g6, align 4
  %130 = load i32, i32* %x6, align 4
  %xor71 = xor i32 %129, %130
  %131 = load i32*, i32** %g.addr, align 4
  %arrayidx72 = getelementptr i32, i32* %131, i32 6
  store i32 %xor71, i32* %arrayidx72, align 4
  %132 = load i32, i32* %g7, align 4
  %133 = load i32, i32* %x7, align 4
  %xor73 = xor i32 %132, %133
  %134 = load i32*, i32** %g.addr, align 4
  %arrayidx74 = getelementptr i32, i32* %134, i32 7
  store i32 %xor73, i32* %arrayidx74, align 4
  %135 = load i32, i32* %g8, align 4
  %136 = load i32, i32* %x8, align 4
  %xor75 = xor i32 %135, %136
  %137 = load i32*, i32** %g.addr, align 4
  %arrayidx76 = getelementptr i32, i32* %137, i32 8
  store i32 %xor75, i32* %arrayidx76, align 4
  %138 = load i32, i32* %g9, align 4
  %139 = load i32, i32* %x9, align 4
  %xor77 = xor i32 %138, %139
  %140 = load i32*, i32** %g.addr, align 4
  %arrayidx78 = getelementptr i32, i32* %140, i32 9
  store i32 %xor77, i32* %arrayidx78, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_sub(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %h5 = alloca i32, align 4
  %h6 = alloca i32, align 4
  %h7 = alloca i32, align 4
  %h8 = alloca i32, align 4
  %h9 = alloca i32, align 4
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %2 = load i32*, i32** %g.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 0
  %3 = load i32, i32* %arrayidx1, align 4
  %sub = sub i32 %1, %3
  store i32 %sub, i32* %h0, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 1
  %5 = load i32, i32* %arrayidx2, align 4
  %6 = load i32*, i32** %g.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 1
  %7 = load i32, i32* %arrayidx3, align 4
  %sub4 = sub i32 %5, %7
  store i32 %sub4, i32* %h1, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %8, i32 2
  %9 = load i32, i32* %arrayidx5, align 4
  %10 = load i32*, i32** %g.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %10, i32 2
  %11 = load i32, i32* %arrayidx6, align 4
  %sub7 = sub i32 %9, %11
  store i32 %sub7, i32* %h2, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %12, i32 3
  %13 = load i32, i32* %arrayidx8, align 4
  %14 = load i32*, i32** %g.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %14, i32 3
  %15 = load i32, i32* %arrayidx9, align 4
  %sub10 = sub i32 %13, %15
  store i32 %sub10, i32* %h3, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %16, i32 4
  %17 = load i32, i32* %arrayidx11, align 4
  %18 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %18, i32 4
  %19 = load i32, i32* %arrayidx12, align 4
  %sub13 = sub i32 %17, %19
  store i32 %sub13, i32* %h4, align 4
  %20 = load i32*, i32** %f.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %20, i32 5
  %21 = load i32, i32* %arrayidx14, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %22, i32 5
  %23 = load i32, i32* %arrayidx15, align 4
  %sub16 = sub i32 %21, %23
  store i32 %sub16, i32* %h5, align 4
  %24 = load i32*, i32** %f.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %24, i32 6
  %25 = load i32, i32* %arrayidx17, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %26, i32 6
  %27 = load i32, i32* %arrayidx18, align 4
  %sub19 = sub i32 %25, %27
  store i32 %sub19, i32* %h6, align 4
  %28 = load i32*, i32** %f.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %28, i32 7
  %29 = load i32, i32* %arrayidx20, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %30, i32 7
  %31 = load i32, i32* %arrayidx21, align 4
  %sub22 = sub i32 %29, %31
  store i32 %sub22, i32* %h7, align 4
  %32 = load i32*, i32** %f.addr, align 4
  %arrayidx23 = getelementptr i32, i32* %32, i32 8
  %33 = load i32, i32* %arrayidx23, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx24 = getelementptr i32, i32* %34, i32 8
  %35 = load i32, i32* %arrayidx24, align 4
  %sub25 = sub i32 %33, %35
  store i32 %sub25, i32* %h8, align 4
  %36 = load i32*, i32** %f.addr, align 4
  %arrayidx26 = getelementptr i32, i32* %36, i32 9
  %37 = load i32, i32* %arrayidx26, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx27 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx27, align 4
  %sub28 = sub i32 %37, %39
  store i32 %sub28, i32* %h9, align 4
  %40 = load i32, i32* %h0, align 4
  %41 = load i32*, i32** %h.addr, align 4
  %arrayidx29 = getelementptr i32, i32* %41, i32 0
  store i32 %40, i32* %arrayidx29, align 4
  %42 = load i32, i32* %h1, align 4
  %43 = load i32*, i32** %h.addr, align 4
  %arrayidx30 = getelementptr i32, i32* %43, i32 1
  store i32 %42, i32* %arrayidx30, align 4
  %44 = load i32, i32* %h2, align 4
  %45 = load i32*, i32** %h.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %45, i32 2
  store i32 %44, i32* %arrayidx31, align 4
  %46 = load i32, i32* %h3, align 4
  %47 = load i32*, i32** %h.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %47, i32 3
  store i32 %46, i32* %arrayidx32, align 4
  %48 = load i32, i32* %h4, align 4
  %49 = load i32*, i32** %h.addr, align 4
  %arrayidx33 = getelementptr i32, i32* %49, i32 4
  store i32 %48, i32* %arrayidx33, align 4
  %50 = load i32, i32* %h5, align 4
  %51 = load i32*, i32** %h.addr, align 4
  %arrayidx34 = getelementptr i32, i32* %51, i32 5
  store i32 %50, i32* %arrayidx34, align 4
  %52 = load i32, i32* %h6, align 4
  %53 = load i32*, i32** %h.addr, align 4
  %arrayidx35 = getelementptr i32, i32* %53, i32 6
  store i32 %52, i32* %arrayidx35, align 4
  %54 = load i32, i32* %h7, align 4
  %55 = load i32*, i32** %h.addr, align 4
  %arrayidx36 = getelementptr i32, i32* %55, i32 7
  store i32 %54, i32* %arrayidx36, align 4
  %56 = load i32, i32* %h8, align 4
  %57 = load i32*, i32** %h.addr, align 4
  %arrayidx37 = getelementptr i32, i32* %57, i32 8
  store i32 %56, i32* %arrayidx37, align 4
  %58 = load i32, i32* %h9, align 4
  %59 = load i32*, i32** %h.addr, align 4
  %arrayidx38 = getelementptr i32, i32* %59, i32 9
  store i32 %58, i32* %arrayidx38, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_add(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %h0 = alloca i32, align 4
  %h1 = alloca i32, align 4
  %h2 = alloca i32, align 4
  %h3 = alloca i32, align 4
  %h4 = alloca i32, align 4
  %h5 = alloca i32, align 4
  %h6 = alloca i32, align 4
  %h7 = alloca i32, align 4
  %h8 = alloca i32, align 4
  %h9 = alloca i32, align 4
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %2 = load i32*, i32** %g.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 0
  %3 = load i32, i32* %arrayidx1, align 4
  %add = add i32 %1, %3
  store i32 %add, i32* %h0, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 1
  %5 = load i32, i32* %arrayidx2, align 4
  %6 = load i32*, i32** %g.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 1
  %7 = load i32, i32* %arrayidx3, align 4
  %add4 = add i32 %5, %7
  store i32 %add4, i32* %h1, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %8, i32 2
  %9 = load i32, i32* %arrayidx5, align 4
  %10 = load i32*, i32** %g.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %10, i32 2
  %11 = load i32, i32* %arrayidx6, align 4
  %add7 = add i32 %9, %11
  store i32 %add7, i32* %h2, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %12, i32 3
  %13 = load i32, i32* %arrayidx8, align 4
  %14 = load i32*, i32** %g.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %14, i32 3
  %15 = load i32, i32* %arrayidx9, align 4
  %add10 = add i32 %13, %15
  store i32 %add10, i32* %h3, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %16, i32 4
  %17 = load i32, i32* %arrayidx11, align 4
  %18 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %18, i32 4
  %19 = load i32, i32* %arrayidx12, align 4
  %add13 = add i32 %17, %19
  store i32 %add13, i32* %h4, align 4
  %20 = load i32*, i32** %f.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %20, i32 5
  %21 = load i32, i32* %arrayidx14, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %22, i32 5
  %23 = load i32, i32* %arrayidx15, align 4
  %add16 = add i32 %21, %23
  store i32 %add16, i32* %h5, align 4
  %24 = load i32*, i32** %f.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %24, i32 6
  %25 = load i32, i32* %arrayidx17, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %26, i32 6
  %27 = load i32, i32* %arrayidx18, align 4
  %add19 = add i32 %25, %27
  store i32 %add19, i32* %h6, align 4
  %28 = load i32*, i32** %f.addr, align 4
  %arrayidx20 = getelementptr i32, i32* %28, i32 7
  %29 = load i32, i32* %arrayidx20, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx21 = getelementptr i32, i32* %30, i32 7
  %31 = load i32, i32* %arrayidx21, align 4
  %add22 = add i32 %29, %31
  store i32 %add22, i32* %h7, align 4
  %32 = load i32*, i32** %f.addr, align 4
  %arrayidx23 = getelementptr i32, i32* %32, i32 8
  %33 = load i32, i32* %arrayidx23, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx24 = getelementptr i32, i32* %34, i32 8
  %35 = load i32, i32* %arrayidx24, align 4
  %add25 = add i32 %33, %35
  store i32 %add25, i32* %h8, align 4
  %36 = load i32*, i32** %f.addr, align 4
  %arrayidx26 = getelementptr i32, i32* %36, i32 9
  %37 = load i32, i32* %arrayidx26, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx27 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx27, align 4
  %add28 = add i32 %37, %39
  store i32 %add28, i32* %h9, align 4
  %40 = load i32, i32* %h0, align 4
  %41 = load i32*, i32** %h.addr, align 4
  %arrayidx29 = getelementptr i32, i32* %41, i32 0
  store i32 %40, i32* %arrayidx29, align 4
  %42 = load i32, i32* %h1, align 4
  %43 = load i32*, i32** %h.addr, align 4
  %arrayidx30 = getelementptr i32, i32* %43, i32 1
  store i32 %42, i32* %arrayidx30, align 4
  %44 = load i32, i32* %h2, align 4
  %45 = load i32*, i32** %h.addr, align 4
  %arrayidx31 = getelementptr i32, i32* %45, i32 2
  store i32 %44, i32* %arrayidx31, align 4
  %46 = load i32, i32* %h3, align 4
  %47 = load i32*, i32** %h.addr, align 4
  %arrayidx32 = getelementptr i32, i32* %47, i32 3
  store i32 %46, i32* %arrayidx32, align 4
  %48 = load i32, i32* %h4, align 4
  %49 = load i32*, i32** %h.addr, align 4
  %arrayidx33 = getelementptr i32, i32* %49, i32 4
  store i32 %48, i32* %arrayidx33, align 4
  %50 = load i32, i32* %h5, align 4
  %51 = load i32*, i32** %h.addr, align 4
  %arrayidx34 = getelementptr i32, i32* %51, i32 5
  store i32 %50, i32* %arrayidx34, align 4
  %52 = load i32, i32* %h6, align 4
  %53 = load i32*, i32** %h.addr, align 4
  %arrayidx35 = getelementptr i32, i32* %53, i32 6
  store i32 %52, i32* %arrayidx35, align 4
  %54 = load i32, i32* %h7, align 4
  %55 = load i32*, i32** %h.addr, align 4
  %arrayidx36 = getelementptr i32, i32* %55, i32 7
  store i32 %54, i32* %arrayidx36, align 4
  %56 = load i32, i32* %h8, align 4
  %57 = load i32*, i32** %h.addr, align 4
  %arrayidx37 = getelementptr i32, i32* %57, i32 8
  store i32 %56, i32* %arrayidx37, align 4
  %58 = load i32, i32* %h9, align 4
  %59 = load i32*, i32** %h.addr, align 4
  %arrayidx38 = getelementptr i32, i32* %59, i32 9
  store i32 %58, i32* %arrayidx38, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_mul(i32* %h, i32* %f, i32* %g) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %g.addr = alloca i32*, align 4
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  %g0 = alloca i32, align 4
  %g1 = alloca i32, align 4
  %g2 = alloca i32, align 4
  %g3 = alloca i32, align 4
  %g4 = alloca i32, align 4
  %g5 = alloca i32, align 4
  %g6 = alloca i32, align 4
  %g7 = alloca i32, align 4
  %g8 = alloca i32, align 4
  %g9 = alloca i32, align 4
  %g1_19 = alloca i32, align 4
  %g2_19 = alloca i32, align 4
  %g3_19 = alloca i32, align 4
  %g4_19 = alloca i32, align 4
  %g5_19 = alloca i32, align 4
  %g6_19 = alloca i32, align 4
  %g7_19 = alloca i32, align 4
  %g8_19 = alloca i32, align 4
  %g9_19 = alloca i32, align 4
  %f1_2 = alloca i32, align 4
  %f3_2 = alloca i32, align 4
  %f5_2 = alloca i32, align 4
  %f7_2 = alloca i32, align 4
  %f9_2 = alloca i32, align 4
  %f0g0 = alloca i64, align 8
  %f0g1 = alloca i64, align 8
  %f0g2 = alloca i64, align 8
  %f0g3 = alloca i64, align 8
  %f0g4 = alloca i64, align 8
  %f0g5 = alloca i64, align 8
  %f0g6 = alloca i64, align 8
  %f0g7 = alloca i64, align 8
  %f0g8 = alloca i64, align 8
  %f0g9 = alloca i64, align 8
  %f1g0 = alloca i64, align 8
  %f1g1_2 = alloca i64, align 8
  %f1g2 = alloca i64, align 8
  %f1g3_2 = alloca i64, align 8
  %f1g4 = alloca i64, align 8
  %f1g5_2 = alloca i64, align 8
  %f1g6 = alloca i64, align 8
  %f1g7_2 = alloca i64, align 8
  %f1g8 = alloca i64, align 8
  %f1g9_38 = alloca i64, align 8
  %f2g0 = alloca i64, align 8
  %f2g1 = alloca i64, align 8
  %f2g2 = alloca i64, align 8
  %f2g3 = alloca i64, align 8
  %f2g4 = alloca i64, align 8
  %f2g5 = alloca i64, align 8
  %f2g6 = alloca i64, align 8
  %f2g7 = alloca i64, align 8
  %f2g8_19 = alloca i64, align 8
  %f2g9_19 = alloca i64, align 8
  %f3g0 = alloca i64, align 8
  %f3g1_2 = alloca i64, align 8
  %f3g2 = alloca i64, align 8
  %f3g3_2 = alloca i64, align 8
  %f3g4 = alloca i64, align 8
  %f3g5_2 = alloca i64, align 8
  %f3g6 = alloca i64, align 8
  %f3g7_38 = alloca i64, align 8
  %f3g8_19 = alloca i64, align 8
  %f3g9_38 = alloca i64, align 8
  %f4g0 = alloca i64, align 8
  %f4g1 = alloca i64, align 8
  %f4g2 = alloca i64, align 8
  %f4g3 = alloca i64, align 8
  %f4g4 = alloca i64, align 8
  %f4g5 = alloca i64, align 8
  %f4g6_19 = alloca i64, align 8
  %f4g7_19 = alloca i64, align 8
  %f4g8_19 = alloca i64, align 8
  %f4g9_19 = alloca i64, align 8
  %f5g0 = alloca i64, align 8
  %f5g1_2 = alloca i64, align 8
  %f5g2 = alloca i64, align 8
  %f5g3_2 = alloca i64, align 8
  %f5g4 = alloca i64, align 8
  %f5g5_38 = alloca i64, align 8
  %f5g6_19 = alloca i64, align 8
  %f5g7_38 = alloca i64, align 8
  %f5g8_19 = alloca i64, align 8
  %f5g9_38 = alloca i64, align 8
  %f6g0 = alloca i64, align 8
  %f6g1 = alloca i64, align 8
  %f6g2 = alloca i64, align 8
  %f6g3 = alloca i64, align 8
  %f6g4_19 = alloca i64, align 8
  %f6g5_19 = alloca i64, align 8
  %f6g6_19 = alloca i64, align 8
  %f6g7_19 = alloca i64, align 8
  %f6g8_19 = alloca i64, align 8
  %f6g9_19 = alloca i64, align 8
  %f7g0 = alloca i64, align 8
  %f7g1_2 = alloca i64, align 8
  %f7g2 = alloca i64, align 8
  %f7g3_38 = alloca i64, align 8
  %f7g4_19 = alloca i64, align 8
  %f7g5_38 = alloca i64, align 8
  %f7g6_19 = alloca i64, align 8
  %f7g7_38 = alloca i64, align 8
  %f7g8_19 = alloca i64, align 8
  %f7g9_38 = alloca i64, align 8
  %f8g0 = alloca i64, align 8
  %f8g1 = alloca i64, align 8
  %f8g2_19 = alloca i64, align 8
  %f8g3_19 = alloca i64, align 8
  %f8g4_19 = alloca i64, align 8
  %f8g5_19 = alloca i64, align 8
  %f8g6_19 = alloca i64, align 8
  %f8g7_19 = alloca i64, align 8
  %f8g8_19 = alloca i64, align 8
  %f8g9_19 = alloca i64, align 8
  %f9g0 = alloca i64, align 8
  %f9g1_38 = alloca i64, align 8
  %f9g2_19 = alloca i64, align 8
  %f9g3_38 = alloca i64, align 8
  %f9g4_19 = alloca i64, align 8
  %f9g5_38 = alloca i64, align 8
  %f9g6_19 = alloca i64, align 8
  %f9g7_38 = alloca i64, align 8
  %f9g8_19 = alloca i64, align 8
  %f9g9_38 = alloca i64, align 8
  %h0 = alloca i64, align 8
  %h1 = alloca i64, align 8
  %h2 = alloca i64, align 8
  %h3 = alloca i64, align 8
  %h4 = alloca i64, align 8
  %h5 = alloca i64, align 8
  %h6 = alloca i64, align 8
  %h7 = alloca i64, align 8
  %h8 = alloca i64, align 8
  %h9 = alloca i64, align 8
  %carry0 = alloca i64, align 8
  %carry1 = alloca i64, align 8
  %carry2 = alloca i64, align 8
  %carry3 = alloca i64, align 8
  %carry4 = alloca i64, align 8
  %carry5 = alloca i64, align 8
  %carry6 = alloca i64, align 8
  %carry7 = alloca i64, align 8
  %carry8 = alloca i64, align 8
  %carry9 = alloca i64, align 8
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32* %g, i32** %g.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %f0, align 4
  %2 = load i32*, i32** %f.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 1
  %3 = load i32, i32* %arrayidx1, align 4
  store i32 %3, i32* %f1, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 2
  %5 = load i32, i32* %arrayidx2, align 4
  store i32 %5, i32* %f2, align 4
  %6 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 3
  %7 = load i32, i32* %arrayidx3, align 4
  store i32 %7, i32* %f3, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %8, i32 4
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %f4, align 4
  %10 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %10, i32 5
  %11 = load i32, i32* %arrayidx5, align 4
  store i32 %11, i32* %f5, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %12, i32 6
  %13 = load i32, i32* %arrayidx6, align 4
  store i32 %13, i32* %f6, align 4
  %14 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %14, i32 7
  %15 = load i32, i32* %arrayidx7, align 4
  store i32 %15, i32* %f7, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %16, i32 8
  %17 = load i32, i32* %arrayidx8, align 4
  store i32 %17, i32* %f8, align 4
  %18 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %18, i32 9
  %19 = load i32, i32* %arrayidx9, align 4
  store i32 %19, i32* %f9, align 4
  %20 = load i32*, i32** %g.addr, align 4
  %arrayidx10 = getelementptr i32, i32* %20, i32 0
  %21 = load i32, i32* %arrayidx10, align 4
  store i32 %21, i32* %g0, align 4
  %22 = load i32*, i32** %g.addr, align 4
  %arrayidx11 = getelementptr i32, i32* %22, i32 1
  %23 = load i32, i32* %arrayidx11, align 4
  store i32 %23, i32* %g1, align 4
  %24 = load i32*, i32** %g.addr, align 4
  %arrayidx12 = getelementptr i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx12, align 4
  store i32 %25, i32* %g2, align 4
  %26 = load i32*, i32** %g.addr, align 4
  %arrayidx13 = getelementptr i32, i32* %26, i32 3
  %27 = load i32, i32* %arrayidx13, align 4
  store i32 %27, i32* %g3, align 4
  %28 = load i32*, i32** %g.addr, align 4
  %arrayidx14 = getelementptr i32, i32* %28, i32 4
  %29 = load i32, i32* %arrayidx14, align 4
  store i32 %29, i32* %g4, align 4
  %30 = load i32*, i32** %g.addr, align 4
  %arrayidx15 = getelementptr i32, i32* %30, i32 5
  %31 = load i32, i32* %arrayidx15, align 4
  store i32 %31, i32* %g5, align 4
  %32 = load i32*, i32** %g.addr, align 4
  %arrayidx16 = getelementptr i32, i32* %32, i32 6
  %33 = load i32, i32* %arrayidx16, align 4
  store i32 %33, i32* %g6, align 4
  %34 = load i32*, i32** %g.addr, align 4
  %arrayidx17 = getelementptr i32, i32* %34, i32 7
  %35 = load i32, i32* %arrayidx17, align 4
  store i32 %35, i32* %g7, align 4
  %36 = load i32*, i32** %g.addr, align 4
  %arrayidx18 = getelementptr i32, i32* %36, i32 8
  %37 = load i32, i32* %arrayidx18, align 4
  store i32 %37, i32* %g8, align 4
  %38 = load i32*, i32** %g.addr, align 4
  %arrayidx19 = getelementptr i32, i32* %38, i32 9
  %39 = load i32, i32* %arrayidx19, align 4
  store i32 %39, i32* %g9, align 4
  %40 = load i32, i32* %g1, align 4
  %mul = mul i32 19, %40
  store i32 %mul, i32* %g1_19, align 4
  %41 = load i32, i32* %g2, align 4
  %mul20 = mul i32 19, %41
  store i32 %mul20, i32* %g2_19, align 4
  %42 = load i32, i32* %g3, align 4
  %mul21 = mul i32 19, %42
  store i32 %mul21, i32* %g3_19, align 4
  %43 = load i32, i32* %g4, align 4
  %mul22 = mul i32 19, %43
  store i32 %mul22, i32* %g4_19, align 4
  %44 = load i32, i32* %g5, align 4
  %mul23 = mul i32 19, %44
  store i32 %mul23, i32* %g5_19, align 4
  %45 = load i32, i32* %g6, align 4
  %mul24 = mul i32 19, %45
  store i32 %mul24, i32* %g6_19, align 4
  %46 = load i32, i32* %g7, align 4
  %mul25 = mul i32 19, %46
  store i32 %mul25, i32* %g7_19, align 4
  %47 = load i32, i32* %g8, align 4
  %mul26 = mul i32 19, %47
  store i32 %mul26, i32* %g8_19, align 4
  %48 = load i32, i32* %g9, align 4
  %mul27 = mul i32 19, %48
  store i32 %mul27, i32* %g9_19, align 4
  %49 = load i32, i32* %f1, align 4
  %mul28 = mul i32 2, %49
  store i32 %mul28, i32* %f1_2, align 4
  %50 = load i32, i32* %f3, align 4
  %mul29 = mul i32 2, %50
  store i32 %mul29, i32* %f3_2, align 4
  %51 = load i32, i32* %f5, align 4
  %mul30 = mul i32 2, %51
  store i32 %mul30, i32* %f5_2, align 4
  %52 = load i32, i32* %f7, align 4
  %mul31 = mul i32 2, %52
  store i32 %mul31, i32* %f7_2, align 4
  %53 = load i32, i32* %f9, align 4
  %mul32 = mul i32 2, %53
  store i32 %mul32, i32* %f9_2, align 4
  %54 = load i32, i32* %f0, align 4
  %conv = sext i32 %54 to i64
  %55 = load i32, i32* %g0, align 4
  %conv33 = sext i32 %55 to i64
  %mul34 = mul i64 %conv, %conv33
  store i64 %mul34, i64* %f0g0, align 8
  %56 = load i32, i32* %f0, align 4
  %conv35 = sext i32 %56 to i64
  %57 = load i32, i32* %g1, align 4
  %conv36 = sext i32 %57 to i64
  %mul37 = mul i64 %conv35, %conv36
  store i64 %mul37, i64* %f0g1, align 8
  %58 = load i32, i32* %f0, align 4
  %conv38 = sext i32 %58 to i64
  %59 = load i32, i32* %g2, align 4
  %conv39 = sext i32 %59 to i64
  %mul40 = mul i64 %conv38, %conv39
  store i64 %mul40, i64* %f0g2, align 8
  %60 = load i32, i32* %f0, align 4
  %conv41 = sext i32 %60 to i64
  %61 = load i32, i32* %g3, align 4
  %conv42 = sext i32 %61 to i64
  %mul43 = mul i64 %conv41, %conv42
  store i64 %mul43, i64* %f0g3, align 8
  %62 = load i32, i32* %f0, align 4
  %conv44 = sext i32 %62 to i64
  %63 = load i32, i32* %g4, align 4
  %conv45 = sext i32 %63 to i64
  %mul46 = mul i64 %conv44, %conv45
  store i64 %mul46, i64* %f0g4, align 8
  %64 = load i32, i32* %f0, align 4
  %conv47 = sext i32 %64 to i64
  %65 = load i32, i32* %g5, align 4
  %conv48 = sext i32 %65 to i64
  %mul49 = mul i64 %conv47, %conv48
  store i64 %mul49, i64* %f0g5, align 8
  %66 = load i32, i32* %f0, align 4
  %conv50 = sext i32 %66 to i64
  %67 = load i32, i32* %g6, align 4
  %conv51 = sext i32 %67 to i64
  %mul52 = mul i64 %conv50, %conv51
  store i64 %mul52, i64* %f0g6, align 8
  %68 = load i32, i32* %f0, align 4
  %conv53 = sext i32 %68 to i64
  %69 = load i32, i32* %g7, align 4
  %conv54 = sext i32 %69 to i64
  %mul55 = mul i64 %conv53, %conv54
  store i64 %mul55, i64* %f0g7, align 8
  %70 = load i32, i32* %f0, align 4
  %conv56 = sext i32 %70 to i64
  %71 = load i32, i32* %g8, align 4
  %conv57 = sext i32 %71 to i64
  %mul58 = mul i64 %conv56, %conv57
  store i64 %mul58, i64* %f0g8, align 8
  %72 = load i32, i32* %f0, align 4
  %conv59 = sext i32 %72 to i64
  %73 = load i32, i32* %g9, align 4
  %conv60 = sext i32 %73 to i64
  %mul61 = mul i64 %conv59, %conv60
  store i64 %mul61, i64* %f0g9, align 8
  %74 = load i32, i32* %f1, align 4
  %conv62 = sext i32 %74 to i64
  %75 = load i32, i32* %g0, align 4
  %conv63 = sext i32 %75 to i64
  %mul64 = mul i64 %conv62, %conv63
  store i64 %mul64, i64* %f1g0, align 8
  %76 = load i32, i32* %f1_2, align 4
  %conv65 = sext i32 %76 to i64
  %77 = load i32, i32* %g1, align 4
  %conv66 = sext i32 %77 to i64
  %mul67 = mul i64 %conv65, %conv66
  store i64 %mul67, i64* %f1g1_2, align 8
  %78 = load i32, i32* %f1, align 4
  %conv68 = sext i32 %78 to i64
  %79 = load i32, i32* %g2, align 4
  %conv69 = sext i32 %79 to i64
  %mul70 = mul i64 %conv68, %conv69
  store i64 %mul70, i64* %f1g2, align 8
  %80 = load i32, i32* %f1_2, align 4
  %conv71 = sext i32 %80 to i64
  %81 = load i32, i32* %g3, align 4
  %conv72 = sext i32 %81 to i64
  %mul73 = mul i64 %conv71, %conv72
  store i64 %mul73, i64* %f1g3_2, align 8
  %82 = load i32, i32* %f1, align 4
  %conv74 = sext i32 %82 to i64
  %83 = load i32, i32* %g4, align 4
  %conv75 = sext i32 %83 to i64
  %mul76 = mul i64 %conv74, %conv75
  store i64 %mul76, i64* %f1g4, align 8
  %84 = load i32, i32* %f1_2, align 4
  %conv77 = sext i32 %84 to i64
  %85 = load i32, i32* %g5, align 4
  %conv78 = sext i32 %85 to i64
  %mul79 = mul i64 %conv77, %conv78
  store i64 %mul79, i64* %f1g5_2, align 8
  %86 = load i32, i32* %f1, align 4
  %conv80 = sext i32 %86 to i64
  %87 = load i32, i32* %g6, align 4
  %conv81 = sext i32 %87 to i64
  %mul82 = mul i64 %conv80, %conv81
  store i64 %mul82, i64* %f1g6, align 8
  %88 = load i32, i32* %f1_2, align 4
  %conv83 = sext i32 %88 to i64
  %89 = load i32, i32* %g7, align 4
  %conv84 = sext i32 %89 to i64
  %mul85 = mul i64 %conv83, %conv84
  store i64 %mul85, i64* %f1g7_2, align 8
  %90 = load i32, i32* %f1, align 4
  %conv86 = sext i32 %90 to i64
  %91 = load i32, i32* %g8, align 4
  %conv87 = sext i32 %91 to i64
  %mul88 = mul i64 %conv86, %conv87
  store i64 %mul88, i64* %f1g8, align 8
  %92 = load i32, i32* %f1_2, align 4
  %conv89 = sext i32 %92 to i64
  %93 = load i32, i32* %g9_19, align 4
  %conv90 = sext i32 %93 to i64
  %mul91 = mul i64 %conv89, %conv90
  store i64 %mul91, i64* %f1g9_38, align 8
  %94 = load i32, i32* %f2, align 4
  %conv92 = sext i32 %94 to i64
  %95 = load i32, i32* %g0, align 4
  %conv93 = sext i32 %95 to i64
  %mul94 = mul i64 %conv92, %conv93
  store i64 %mul94, i64* %f2g0, align 8
  %96 = load i32, i32* %f2, align 4
  %conv95 = sext i32 %96 to i64
  %97 = load i32, i32* %g1, align 4
  %conv96 = sext i32 %97 to i64
  %mul97 = mul i64 %conv95, %conv96
  store i64 %mul97, i64* %f2g1, align 8
  %98 = load i32, i32* %f2, align 4
  %conv98 = sext i32 %98 to i64
  %99 = load i32, i32* %g2, align 4
  %conv99 = sext i32 %99 to i64
  %mul100 = mul i64 %conv98, %conv99
  store i64 %mul100, i64* %f2g2, align 8
  %100 = load i32, i32* %f2, align 4
  %conv101 = sext i32 %100 to i64
  %101 = load i32, i32* %g3, align 4
  %conv102 = sext i32 %101 to i64
  %mul103 = mul i64 %conv101, %conv102
  store i64 %mul103, i64* %f2g3, align 8
  %102 = load i32, i32* %f2, align 4
  %conv104 = sext i32 %102 to i64
  %103 = load i32, i32* %g4, align 4
  %conv105 = sext i32 %103 to i64
  %mul106 = mul i64 %conv104, %conv105
  store i64 %mul106, i64* %f2g4, align 8
  %104 = load i32, i32* %f2, align 4
  %conv107 = sext i32 %104 to i64
  %105 = load i32, i32* %g5, align 4
  %conv108 = sext i32 %105 to i64
  %mul109 = mul i64 %conv107, %conv108
  store i64 %mul109, i64* %f2g5, align 8
  %106 = load i32, i32* %f2, align 4
  %conv110 = sext i32 %106 to i64
  %107 = load i32, i32* %g6, align 4
  %conv111 = sext i32 %107 to i64
  %mul112 = mul i64 %conv110, %conv111
  store i64 %mul112, i64* %f2g6, align 8
  %108 = load i32, i32* %f2, align 4
  %conv113 = sext i32 %108 to i64
  %109 = load i32, i32* %g7, align 4
  %conv114 = sext i32 %109 to i64
  %mul115 = mul i64 %conv113, %conv114
  store i64 %mul115, i64* %f2g7, align 8
  %110 = load i32, i32* %f2, align 4
  %conv116 = sext i32 %110 to i64
  %111 = load i32, i32* %g8_19, align 4
  %conv117 = sext i32 %111 to i64
  %mul118 = mul i64 %conv116, %conv117
  store i64 %mul118, i64* %f2g8_19, align 8
  %112 = load i32, i32* %f2, align 4
  %conv119 = sext i32 %112 to i64
  %113 = load i32, i32* %g9_19, align 4
  %conv120 = sext i32 %113 to i64
  %mul121 = mul i64 %conv119, %conv120
  store i64 %mul121, i64* %f2g9_19, align 8
  %114 = load i32, i32* %f3, align 4
  %conv122 = sext i32 %114 to i64
  %115 = load i32, i32* %g0, align 4
  %conv123 = sext i32 %115 to i64
  %mul124 = mul i64 %conv122, %conv123
  store i64 %mul124, i64* %f3g0, align 8
  %116 = load i32, i32* %f3_2, align 4
  %conv125 = sext i32 %116 to i64
  %117 = load i32, i32* %g1, align 4
  %conv126 = sext i32 %117 to i64
  %mul127 = mul i64 %conv125, %conv126
  store i64 %mul127, i64* %f3g1_2, align 8
  %118 = load i32, i32* %f3, align 4
  %conv128 = sext i32 %118 to i64
  %119 = load i32, i32* %g2, align 4
  %conv129 = sext i32 %119 to i64
  %mul130 = mul i64 %conv128, %conv129
  store i64 %mul130, i64* %f3g2, align 8
  %120 = load i32, i32* %f3_2, align 4
  %conv131 = sext i32 %120 to i64
  %121 = load i32, i32* %g3, align 4
  %conv132 = sext i32 %121 to i64
  %mul133 = mul i64 %conv131, %conv132
  store i64 %mul133, i64* %f3g3_2, align 8
  %122 = load i32, i32* %f3, align 4
  %conv134 = sext i32 %122 to i64
  %123 = load i32, i32* %g4, align 4
  %conv135 = sext i32 %123 to i64
  %mul136 = mul i64 %conv134, %conv135
  store i64 %mul136, i64* %f3g4, align 8
  %124 = load i32, i32* %f3_2, align 4
  %conv137 = sext i32 %124 to i64
  %125 = load i32, i32* %g5, align 4
  %conv138 = sext i32 %125 to i64
  %mul139 = mul i64 %conv137, %conv138
  store i64 %mul139, i64* %f3g5_2, align 8
  %126 = load i32, i32* %f3, align 4
  %conv140 = sext i32 %126 to i64
  %127 = load i32, i32* %g6, align 4
  %conv141 = sext i32 %127 to i64
  %mul142 = mul i64 %conv140, %conv141
  store i64 %mul142, i64* %f3g6, align 8
  %128 = load i32, i32* %f3_2, align 4
  %conv143 = sext i32 %128 to i64
  %129 = load i32, i32* %g7_19, align 4
  %conv144 = sext i32 %129 to i64
  %mul145 = mul i64 %conv143, %conv144
  store i64 %mul145, i64* %f3g7_38, align 8
  %130 = load i32, i32* %f3, align 4
  %conv146 = sext i32 %130 to i64
  %131 = load i32, i32* %g8_19, align 4
  %conv147 = sext i32 %131 to i64
  %mul148 = mul i64 %conv146, %conv147
  store i64 %mul148, i64* %f3g8_19, align 8
  %132 = load i32, i32* %f3_2, align 4
  %conv149 = sext i32 %132 to i64
  %133 = load i32, i32* %g9_19, align 4
  %conv150 = sext i32 %133 to i64
  %mul151 = mul i64 %conv149, %conv150
  store i64 %mul151, i64* %f3g9_38, align 8
  %134 = load i32, i32* %f4, align 4
  %conv152 = sext i32 %134 to i64
  %135 = load i32, i32* %g0, align 4
  %conv153 = sext i32 %135 to i64
  %mul154 = mul i64 %conv152, %conv153
  store i64 %mul154, i64* %f4g0, align 8
  %136 = load i32, i32* %f4, align 4
  %conv155 = sext i32 %136 to i64
  %137 = load i32, i32* %g1, align 4
  %conv156 = sext i32 %137 to i64
  %mul157 = mul i64 %conv155, %conv156
  store i64 %mul157, i64* %f4g1, align 8
  %138 = load i32, i32* %f4, align 4
  %conv158 = sext i32 %138 to i64
  %139 = load i32, i32* %g2, align 4
  %conv159 = sext i32 %139 to i64
  %mul160 = mul i64 %conv158, %conv159
  store i64 %mul160, i64* %f4g2, align 8
  %140 = load i32, i32* %f4, align 4
  %conv161 = sext i32 %140 to i64
  %141 = load i32, i32* %g3, align 4
  %conv162 = sext i32 %141 to i64
  %mul163 = mul i64 %conv161, %conv162
  store i64 %mul163, i64* %f4g3, align 8
  %142 = load i32, i32* %f4, align 4
  %conv164 = sext i32 %142 to i64
  %143 = load i32, i32* %g4, align 4
  %conv165 = sext i32 %143 to i64
  %mul166 = mul i64 %conv164, %conv165
  store i64 %mul166, i64* %f4g4, align 8
  %144 = load i32, i32* %f4, align 4
  %conv167 = sext i32 %144 to i64
  %145 = load i32, i32* %g5, align 4
  %conv168 = sext i32 %145 to i64
  %mul169 = mul i64 %conv167, %conv168
  store i64 %mul169, i64* %f4g5, align 8
  %146 = load i32, i32* %f4, align 4
  %conv170 = sext i32 %146 to i64
  %147 = load i32, i32* %g6_19, align 4
  %conv171 = sext i32 %147 to i64
  %mul172 = mul i64 %conv170, %conv171
  store i64 %mul172, i64* %f4g6_19, align 8
  %148 = load i32, i32* %f4, align 4
  %conv173 = sext i32 %148 to i64
  %149 = load i32, i32* %g7_19, align 4
  %conv174 = sext i32 %149 to i64
  %mul175 = mul i64 %conv173, %conv174
  store i64 %mul175, i64* %f4g7_19, align 8
  %150 = load i32, i32* %f4, align 4
  %conv176 = sext i32 %150 to i64
  %151 = load i32, i32* %g8_19, align 4
  %conv177 = sext i32 %151 to i64
  %mul178 = mul i64 %conv176, %conv177
  store i64 %mul178, i64* %f4g8_19, align 8
  %152 = load i32, i32* %f4, align 4
  %conv179 = sext i32 %152 to i64
  %153 = load i32, i32* %g9_19, align 4
  %conv180 = sext i32 %153 to i64
  %mul181 = mul i64 %conv179, %conv180
  store i64 %mul181, i64* %f4g9_19, align 8
  %154 = load i32, i32* %f5, align 4
  %conv182 = sext i32 %154 to i64
  %155 = load i32, i32* %g0, align 4
  %conv183 = sext i32 %155 to i64
  %mul184 = mul i64 %conv182, %conv183
  store i64 %mul184, i64* %f5g0, align 8
  %156 = load i32, i32* %f5_2, align 4
  %conv185 = sext i32 %156 to i64
  %157 = load i32, i32* %g1, align 4
  %conv186 = sext i32 %157 to i64
  %mul187 = mul i64 %conv185, %conv186
  store i64 %mul187, i64* %f5g1_2, align 8
  %158 = load i32, i32* %f5, align 4
  %conv188 = sext i32 %158 to i64
  %159 = load i32, i32* %g2, align 4
  %conv189 = sext i32 %159 to i64
  %mul190 = mul i64 %conv188, %conv189
  store i64 %mul190, i64* %f5g2, align 8
  %160 = load i32, i32* %f5_2, align 4
  %conv191 = sext i32 %160 to i64
  %161 = load i32, i32* %g3, align 4
  %conv192 = sext i32 %161 to i64
  %mul193 = mul i64 %conv191, %conv192
  store i64 %mul193, i64* %f5g3_2, align 8
  %162 = load i32, i32* %f5, align 4
  %conv194 = sext i32 %162 to i64
  %163 = load i32, i32* %g4, align 4
  %conv195 = sext i32 %163 to i64
  %mul196 = mul i64 %conv194, %conv195
  store i64 %mul196, i64* %f5g4, align 8
  %164 = load i32, i32* %f5_2, align 4
  %conv197 = sext i32 %164 to i64
  %165 = load i32, i32* %g5_19, align 4
  %conv198 = sext i32 %165 to i64
  %mul199 = mul i64 %conv197, %conv198
  store i64 %mul199, i64* %f5g5_38, align 8
  %166 = load i32, i32* %f5, align 4
  %conv200 = sext i32 %166 to i64
  %167 = load i32, i32* %g6_19, align 4
  %conv201 = sext i32 %167 to i64
  %mul202 = mul i64 %conv200, %conv201
  store i64 %mul202, i64* %f5g6_19, align 8
  %168 = load i32, i32* %f5_2, align 4
  %conv203 = sext i32 %168 to i64
  %169 = load i32, i32* %g7_19, align 4
  %conv204 = sext i32 %169 to i64
  %mul205 = mul i64 %conv203, %conv204
  store i64 %mul205, i64* %f5g7_38, align 8
  %170 = load i32, i32* %f5, align 4
  %conv206 = sext i32 %170 to i64
  %171 = load i32, i32* %g8_19, align 4
  %conv207 = sext i32 %171 to i64
  %mul208 = mul i64 %conv206, %conv207
  store i64 %mul208, i64* %f5g8_19, align 8
  %172 = load i32, i32* %f5_2, align 4
  %conv209 = sext i32 %172 to i64
  %173 = load i32, i32* %g9_19, align 4
  %conv210 = sext i32 %173 to i64
  %mul211 = mul i64 %conv209, %conv210
  store i64 %mul211, i64* %f5g9_38, align 8
  %174 = load i32, i32* %f6, align 4
  %conv212 = sext i32 %174 to i64
  %175 = load i32, i32* %g0, align 4
  %conv213 = sext i32 %175 to i64
  %mul214 = mul i64 %conv212, %conv213
  store i64 %mul214, i64* %f6g0, align 8
  %176 = load i32, i32* %f6, align 4
  %conv215 = sext i32 %176 to i64
  %177 = load i32, i32* %g1, align 4
  %conv216 = sext i32 %177 to i64
  %mul217 = mul i64 %conv215, %conv216
  store i64 %mul217, i64* %f6g1, align 8
  %178 = load i32, i32* %f6, align 4
  %conv218 = sext i32 %178 to i64
  %179 = load i32, i32* %g2, align 4
  %conv219 = sext i32 %179 to i64
  %mul220 = mul i64 %conv218, %conv219
  store i64 %mul220, i64* %f6g2, align 8
  %180 = load i32, i32* %f6, align 4
  %conv221 = sext i32 %180 to i64
  %181 = load i32, i32* %g3, align 4
  %conv222 = sext i32 %181 to i64
  %mul223 = mul i64 %conv221, %conv222
  store i64 %mul223, i64* %f6g3, align 8
  %182 = load i32, i32* %f6, align 4
  %conv224 = sext i32 %182 to i64
  %183 = load i32, i32* %g4_19, align 4
  %conv225 = sext i32 %183 to i64
  %mul226 = mul i64 %conv224, %conv225
  store i64 %mul226, i64* %f6g4_19, align 8
  %184 = load i32, i32* %f6, align 4
  %conv227 = sext i32 %184 to i64
  %185 = load i32, i32* %g5_19, align 4
  %conv228 = sext i32 %185 to i64
  %mul229 = mul i64 %conv227, %conv228
  store i64 %mul229, i64* %f6g5_19, align 8
  %186 = load i32, i32* %f6, align 4
  %conv230 = sext i32 %186 to i64
  %187 = load i32, i32* %g6_19, align 4
  %conv231 = sext i32 %187 to i64
  %mul232 = mul i64 %conv230, %conv231
  store i64 %mul232, i64* %f6g6_19, align 8
  %188 = load i32, i32* %f6, align 4
  %conv233 = sext i32 %188 to i64
  %189 = load i32, i32* %g7_19, align 4
  %conv234 = sext i32 %189 to i64
  %mul235 = mul i64 %conv233, %conv234
  store i64 %mul235, i64* %f6g7_19, align 8
  %190 = load i32, i32* %f6, align 4
  %conv236 = sext i32 %190 to i64
  %191 = load i32, i32* %g8_19, align 4
  %conv237 = sext i32 %191 to i64
  %mul238 = mul i64 %conv236, %conv237
  store i64 %mul238, i64* %f6g8_19, align 8
  %192 = load i32, i32* %f6, align 4
  %conv239 = sext i32 %192 to i64
  %193 = load i32, i32* %g9_19, align 4
  %conv240 = sext i32 %193 to i64
  %mul241 = mul i64 %conv239, %conv240
  store i64 %mul241, i64* %f6g9_19, align 8
  %194 = load i32, i32* %f7, align 4
  %conv242 = sext i32 %194 to i64
  %195 = load i32, i32* %g0, align 4
  %conv243 = sext i32 %195 to i64
  %mul244 = mul i64 %conv242, %conv243
  store i64 %mul244, i64* %f7g0, align 8
  %196 = load i32, i32* %f7_2, align 4
  %conv245 = sext i32 %196 to i64
  %197 = load i32, i32* %g1, align 4
  %conv246 = sext i32 %197 to i64
  %mul247 = mul i64 %conv245, %conv246
  store i64 %mul247, i64* %f7g1_2, align 8
  %198 = load i32, i32* %f7, align 4
  %conv248 = sext i32 %198 to i64
  %199 = load i32, i32* %g2, align 4
  %conv249 = sext i32 %199 to i64
  %mul250 = mul i64 %conv248, %conv249
  store i64 %mul250, i64* %f7g2, align 8
  %200 = load i32, i32* %f7_2, align 4
  %conv251 = sext i32 %200 to i64
  %201 = load i32, i32* %g3_19, align 4
  %conv252 = sext i32 %201 to i64
  %mul253 = mul i64 %conv251, %conv252
  store i64 %mul253, i64* %f7g3_38, align 8
  %202 = load i32, i32* %f7, align 4
  %conv254 = sext i32 %202 to i64
  %203 = load i32, i32* %g4_19, align 4
  %conv255 = sext i32 %203 to i64
  %mul256 = mul i64 %conv254, %conv255
  store i64 %mul256, i64* %f7g4_19, align 8
  %204 = load i32, i32* %f7_2, align 4
  %conv257 = sext i32 %204 to i64
  %205 = load i32, i32* %g5_19, align 4
  %conv258 = sext i32 %205 to i64
  %mul259 = mul i64 %conv257, %conv258
  store i64 %mul259, i64* %f7g5_38, align 8
  %206 = load i32, i32* %f7, align 4
  %conv260 = sext i32 %206 to i64
  %207 = load i32, i32* %g6_19, align 4
  %conv261 = sext i32 %207 to i64
  %mul262 = mul i64 %conv260, %conv261
  store i64 %mul262, i64* %f7g6_19, align 8
  %208 = load i32, i32* %f7_2, align 4
  %conv263 = sext i32 %208 to i64
  %209 = load i32, i32* %g7_19, align 4
  %conv264 = sext i32 %209 to i64
  %mul265 = mul i64 %conv263, %conv264
  store i64 %mul265, i64* %f7g7_38, align 8
  %210 = load i32, i32* %f7, align 4
  %conv266 = sext i32 %210 to i64
  %211 = load i32, i32* %g8_19, align 4
  %conv267 = sext i32 %211 to i64
  %mul268 = mul i64 %conv266, %conv267
  store i64 %mul268, i64* %f7g8_19, align 8
  %212 = load i32, i32* %f7_2, align 4
  %conv269 = sext i32 %212 to i64
  %213 = load i32, i32* %g9_19, align 4
  %conv270 = sext i32 %213 to i64
  %mul271 = mul i64 %conv269, %conv270
  store i64 %mul271, i64* %f7g9_38, align 8
  %214 = load i32, i32* %f8, align 4
  %conv272 = sext i32 %214 to i64
  %215 = load i32, i32* %g0, align 4
  %conv273 = sext i32 %215 to i64
  %mul274 = mul i64 %conv272, %conv273
  store i64 %mul274, i64* %f8g0, align 8
  %216 = load i32, i32* %f8, align 4
  %conv275 = sext i32 %216 to i64
  %217 = load i32, i32* %g1, align 4
  %conv276 = sext i32 %217 to i64
  %mul277 = mul i64 %conv275, %conv276
  store i64 %mul277, i64* %f8g1, align 8
  %218 = load i32, i32* %f8, align 4
  %conv278 = sext i32 %218 to i64
  %219 = load i32, i32* %g2_19, align 4
  %conv279 = sext i32 %219 to i64
  %mul280 = mul i64 %conv278, %conv279
  store i64 %mul280, i64* %f8g2_19, align 8
  %220 = load i32, i32* %f8, align 4
  %conv281 = sext i32 %220 to i64
  %221 = load i32, i32* %g3_19, align 4
  %conv282 = sext i32 %221 to i64
  %mul283 = mul i64 %conv281, %conv282
  store i64 %mul283, i64* %f8g3_19, align 8
  %222 = load i32, i32* %f8, align 4
  %conv284 = sext i32 %222 to i64
  %223 = load i32, i32* %g4_19, align 4
  %conv285 = sext i32 %223 to i64
  %mul286 = mul i64 %conv284, %conv285
  store i64 %mul286, i64* %f8g4_19, align 8
  %224 = load i32, i32* %f8, align 4
  %conv287 = sext i32 %224 to i64
  %225 = load i32, i32* %g5_19, align 4
  %conv288 = sext i32 %225 to i64
  %mul289 = mul i64 %conv287, %conv288
  store i64 %mul289, i64* %f8g5_19, align 8
  %226 = load i32, i32* %f8, align 4
  %conv290 = sext i32 %226 to i64
  %227 = load i32, i32* %g6_19, align 4
  %conv291 = sext i32 %227 to i64
  %mul292 = mul i64 %conv290, %conv291
  store i64 %mul292, i64* %f8g6_19, align 8
  %228 = load i32, i32* %f8, align 4
  %conv293 = sext i32 %228 to i64
  %229 = load i32, i32* %g7_19, align 4
  %conv294 = sext i32 %229 to i64
  %mul295 = mul i64 %conv293, %conv294
  store i64 %mul295, i64* %f8g7_19, align 8
  %230 = load i32, i32* %f8, align 4
  %conv296 = sext i32 %230 to i64
  %231 = load i32, i32* %g8_19, align 4
  %conv297 = sext i32 %231 to i64
  %mul298 = mul i64 %conv296, %conv297
  store i64 %mul298, i64* %f8g8_19, align 8
  %232 = load i32, i32* %f8, align 4
  %conv299 = sext i32 %232 to i64
  %233 = load i32, i32* %g9_19, align 4
  %conv300 = sext i32 %233 to i64
  %mul301 = mul i64 %conv299, %conv300
  store i64 %mul301, i64* %f8g9_19, align 8
  %234 = load i32, i32* %f9, align 4
  %conv302 = sext i32 %234 to i64
  %235 = load i32, i32* %g0, align 4
  %conv303 = sext i32 %235 to i64
  %mul304 = mul i64 %conv302, %conv303
  store i64 %mul304, i64* %f9g0, align 8
  %236 = load i32, i32* %f9_2, align 4
  %conv305 = sext i32 %236 to i64
  %237 = load i32, i32* %g1_19, align 4
  %conv306 = sext i32 %237 to i64
  %mul307 = mul i64 %conv305, %conv306
  store i64 %mul307, i64* %f9g1_38, align 8
  %238 = load i32, i32* %f9, align 4
  %conv308 = sext i32 %238 to i64
  %239 = load i32, i32* %g2_19, align 4
  %conv309 = sext i32 %239 to i64
  %mul310 = mul i64 %conv308, %conv309
  store i64 %mul310, i64* %f9g2_19, align 8
  %240 = load i32, i32* %f9_2, align 4
  %conv311 = sext i32 %240 to i64
  %241 = load i32, i32* %g3_19, align 4
  %conv312 = sext i32 %241 to i64
  %mul313 = mul i64 %conv311, %conv312
  store i64 %mul313, i64* %f9g3_38, align 8
  %242 = load i32, i32* %f9, align 4
  %conv314 = sext i32 %242 to i64
  %243 = load i32, i32* %g4_19, align 4
  %conv315 = sext i32 %243 to i64
  %mul316 = mul i64 %conv314, %conv315
  store i64 %mul316, i64* %f9g4_19, align 8
  %244 = load i32, i32* %f9_2, align 4
  %conv317 = sext i32 %244 to i64
  %245 = load i32, i32* %g5_19, align 4
  %conv318 = sext i32 %245 to i64
  %mul319 = mul i64 %conv317, %conv318
  store i64 %mul319, i64* %f9g5_38, align 8
  %246 = load i32, i32* %f9, align 4
  %conv320 = sext i32 %246 to i64
  %247 = load i32, i32* %g6_19, align 4
  %conv321 = sext i32 %247 to i64
  %mul322 = mul i64 %conv320, %conv321
  store i64 %mul322, i64* %f9g6_19, align 8
  %248 = load i32, i32* %f9_2, align 4
  %conv323 = sext i32 %248 to i64
  %249 = load i32, i32* %g7_19, align 4
  %conv324 = sext i32 %249 to i64
  %mul325 = mul i64 %conv323, %conv324
  store i64 %mul325, i64* %f9g7_38, align 8
  %250 = load i32, i32* %f9, align 4
  %conv326 = sext i32 %250 to i64
  %251 = load i32, i32* %g8_19, align 4
  %conv327 = sext i32 %251 to i64
  %mul328 = mul i64 %conv326, %conv327
  store i64 %mul328, i64* %f9g8_19, align 8
  %252 = load i32, i32* %f9_2, align 4
  %conv329 = sext i32 %252 to i64
  %253 = load i32, i32* %g9_19, align 4
  %conv330 = sext i32 %253 to i64
  %mul331 = mul i64 %conv329, %conv330
  store i64 %mul331, i64* %f9g9_38, align 8
  %254 = load i64, i64* %f0g0, align 8
  %255 = load i64, i64* %f1g9_38, align 8
  %add = add i64 %254, %255
  %256 = load i64, i64* %f2g8_19, align 8
  %add332 = add i64 %add, %256
  %257 = load i64, i64* %f3g7_38, align 8
  %add333 = add i64 %add332, %257
  %258 = load i64, i64* %f4g6_19, align 8
  %add334 = add i64 %add333, %258
  %259 = load i64, i64* %f5g5_38, align 8
  %add335 = add i64 %add334, %259
  %260 = load i64, i64* %f6g4_19, align 8
  %add336 = add i64 %add335, %260
  %261 = load i64, i64* %f7g3_38, align 8
  %add337 = add i64 %add336, %261
  %262 = load i64, i64* %f8g2_19, align 8
  %add338 = add i64 %add337, %262
  %263 = load i64, i64* %f9g1_38, align 8
  %add339 = add i64 %add338, %263
  store i64 %add339, i64* %h0, align 8
  %264 = load i64, i64* %f0g1, align 8
  %265 = load i64, i64* %f1g0, align 8
  %add340 = add i64 %264, %265
  %266 = load i64, i64* %f2g9_19, align 8
  %add341 = add i64 %add340, %266
  %267 = load i64, i64* %f3g8_19, align 8
  %add342 = add i64 %add341, %267
  %268 = load i64, i64* %f4g7_19, align 8
  %add343 = add i64 %add342, %268
  %269 = load i64, i64* %f5g6_19, align 8
  %add344 = add i64 %add343, %269
  %270 = load i64, i64* %f6g5_19, align 8
  %add345 = add i64 %add344, %270
  %271 = load i64, i64* %f7g4_19, align 8
  %add346 = add i64 %add345, %271
  %272 = load i64, i64* %f8g3_19, align 8
  %add347 = add i64 %add346, %272
  %273 = load i64, i64* %f9g2_19, align 8
  %add348 = add i64 %add347, %273
  store i64 %add348, i64* %h1, align 8
  %274 = load i64, i64* %f0g2, align 8
  %275 = load i64, i64* %f1g1_2, align 8
  %add349 = add i64 %274, %275
  %276 = load i64, i64* %f2g0, align 8
  %add350 = add i64 %add349, %276
  %277 = load i64, i64* %f3g9_38, align 8
  %add351 = add i64 %add350, %277
  %278 = load i64, i64* %f4g8_19, align 8
  %add352 = add i64 %add351, %278
  %279 = load i64, i64* %f5g7_38, align 8
  %add353 = add i64 %add352, %279
  %280 = load i64, i64* %f6g6_19, align 8
  %add354 = add i64 %add353, %280
  %281 = load i64, i64* %f7g5_38, align 8
  %add355 = add i64 %add354, %281
  %282 = load i64, i64* %f8g4_19, align 8
  %add356 = add i64 %add355, %282
  %283 = load i64, i64* %f9g3_38, align 8
  %add357 = add i64 %add356, %283
  store i64 %add357, i64* %h2, align 8
  %284 = load i64, i64* %f0g3, align 8
  %285 = load i64, i64* %f1g2, align 8
  %add358 = add i64 %284, %285
  %286 = load i64, i64* %f2g1, align 8
  %add359 = add i64 %add358, %286
  %287 = load i64, i64* %f3g0, align 8
  %add360 = add i64 %add359, %287
  %288 = load i64, i64* %f4g9_19, align 8
  %add361 = add i64 %add360, %288
  %289 = load i64, i64* %f5g8_19, align 8
  %add362 = add i64 %add361, %289
  %290 = load i64, i64* %f6g7_19, align 8
  %add363 = add i64 %add362, %290
  %291 = load i64, i64* %f7g6_19, align 8
  %add364 = add i64 %add363, %291
  %292 = load i64, i64* %f8g5_19, align 8
  %add365 = add i64 %add364, %292
  %293 = load i64, i64* %f9g4_19, align 8
  %add366 = add i64 %add365, %293
  store i64 %add366, i64* %h3, align 8
  %294 = load i64, i64* %f0g4, align 8
  %295 = load i64, i64* %f1g3_2, align 8
  %add367 = add i64 %294, %295
  %296 = load i64, i64* %f2g2, align 8
  %add368 = add i64 %add367, %296
  %297 = load i64, i64* %f3g1_2, align 8
  %add369 = add i64 %add368, %297
  %298 = load i64, i64* %f4g0, align 8
  %add370 = add i64 %add369, %298
  %299 = load i64, i64* %f5g9_38, align 8
  %add371 = add i64 %add370, %299
  %300 = load i64, i64* %f6g8_19, align 8
  %add372 = add i64 %add371, %300
  %301 = load i64, i64* %f7g7_38, align 8
  %add373 = add i64 %add372, %301
  %302 = load i64, i64* %f8g6_19, align 8
  %add374 = add i64 %add373, %302
  %303 = load i64, i64* %f9g5_38, align 8
  %add375 = add i64 %add374, %303
  store i64 %add375, i64* %h4, align 8
  %304 = load i64, i64* %f0g5, align 8
  %305 = load i64, i64* %f1g4, align 8
  %add376 = add i64 %304, %305
  %306 = load i64, i64* %f2g3, align 8
  %add377 = add i64 %add376, %306
  %307 = load i64, i64* %f3g2, align 8
  %add378 = add i64 %add377, %307
  %308 = load i64, i64* %f4g1, align 8
  %add379 = add i64 %add378, %308
  %309 = load i64, i64* %f5g0, align 8
  %add380 = add i64 %add379, %309
  %310 = load i64, i64* %f6g9_19, align 8
  %add381 = add i64 %add380, %310
  %311 = load i64, i64* %f7g8_19, align 8
  %add382 = add i64 %add381, %311
  %312 = load i64, i64* %f8g7_19, align 8
  %add383 = add i64 %add382, %312
  %313 = load i64, i64* %f9g6_19, align 8
  %add384 = add i64 %add383, %313
  store i64 %add384, i64* %h5, align 8
  %314 = load i64, i64* %f0g6, align 8
  %315 = load i64, i64* %f1g5_2, align 8
  %add385 = add i64 %314, %315
  %316 = load i64, i64* %f2g4, align 8
  %add386 = add i64 %add385, %316
  %317 = load i64, i64* %f3g3_2, align 8
  %add387 = add i64 %add386, %317
  %318 = load i64, i64* %f4g2, align 8
  %add388 = add i64 %add387, %318
  %319 = load i64, i64* %f5g1_2, align 8
  %add389 = add i64 %add388, %319
  %320 = load i64, i64* %f6g0, align 8
  %add390 = add i64 %add389, %320
  %321 = load i64, i64* %f7g9_38, align 8
  %add391 = add i64 %add390, %321
  %322 = load i64, i64* %f8g8_19, align 8
  %add392 = add i64 %add391, %322
  %323 = load i64, i64* %f9g7_38, align 8
  %add393 = add i64 %add392, %323
  store i64 %add393, i64* %h6, align 8
  %324 = load i64, i64* %f0g7, align 8
  %325 = load i64, i64* %f1g6, align 8
  %add394 = add i64 %324, %325
  %326 = load i64, i64* %f2g5, align 8
  %add395 = add i64 %add394, %326
  %327 = load i64, i64* %f3g4, align 8
  %add396 = add i64 %add395, %327
  %328 = load i64, i64* %f4g3, align 8
  %add397 = add i64 %add396, %328
  %329 = load i64, i64* %f5g2, align 8
  %add398 = add i64 %add397, %329
  %330 = load i64, i64* %f6g1, align 8
  %add399 = add i64 %add398, %330
  %331 = load i64, i64* %f7g0, align 8
  %add400 = add i64 %add399, %331
  %332 = load i64, i64* %f8g9_19, align 8
  %add401 = add i64 %add400, %332
  %333 = load i64, i64* %f9g8_19, align 8
  %add402 = add i64 %add401, %333
  store i64 %add402, i64* %h7, align 8
  %334 = load i64, i64* %f0g8, align 8
  %335 = load i64, i64* %f1g7_2, align 8
  %add403 = add i64 %334, %335
  %336 = load i64, i64* %f2g6, align 8
  %add404 = add i64 %add403, %336
  %337 = load i64, i64* %f3g5_2, align 8
  %add405 = add i64 %add404, %337
  %338 = load i64, i64* %f4g4, align 8
  %add406 = add i64 %add405, %338
  %339 = load i64, i64* %f5g3_2, align 8
  %add407 = add i64 %add406, %339
  %340 = load i64, i64* %f6g2, align 8
  %add408 = add i64 %add407, %340
  %341 = load i64, i64* %f7g1_2, align 8
  %add409 = add i64 %add408, %341
  %342 = load i64, i64* %f8g0, align 8
  %add410 = add i64 %add409, %342
  %343 = load i64, i64* %f9g9_38, align 8
  %add411 = add i64 %add410, %343
  store i64 %add411, i64* %h8, align 8
  %344 = load i64, i64* %f0g9, align 8
  %345 = load i64, i64* %f1g8, align 8
  %add412 = add i64 %344, %345
  %346 = load i64, i64* %f2g7, align 8
  %add413 = add i64 %add412, %346
  %347 = load i64, i64* %f3g6, align 8
  %add414 = add i64 %add413, %347
  %348 = load i64, i64* %f4g5, align 8
  %add415 = add i64 %add414, %348
  %349 = load i64, i64* %f5g4, align 8
  %add416 = add i64 %add415, %349
  %350 = load i64, i64* %f6g3, align 8
  %add417 = add i64 %add416, %350
  %351 = load i64, i64* %f7g2, align 8
  %add418 = add i64 %add417, %351
  %352 = load i64, i64* %f8g1, align 8
  %add419 = add i64 %add418, %352
  %353 = load i64, i64* %f9g0, align 8
  %add420 = add i64 %add419, %353
  store i64 %add420, i64* %h9, align 8
  %354 = load i64, i64* %h0, align 8
  %add421 = add i64 %354, 33554432
  %shr = ashr i64 %add421, 26
  store i64 %shr, i64* %carry0, align 8
  %355 = load i64, i64* %carry0, align 8
  %356 = load i64, i64* %h1, align 8
  %add422 = add i64 %356, %355
  store i64 %add422, i64* %h1, align 8
  %357 = load i64, i64* %carry0, align 8
  %mul423 = mul i64 %357, 67108864
  %358 = load i64, i64* %h0, align 8
  %sub = sub i64 %358, %mul423
  store i64 %sub, i64* %h0, align 8
  %359 = load i64, i64* %h4, align 8
  %add424 = add i64 %359, 33554432
  %shr425 = ashr i64 %add424, 26
  store i64 %shr425, i64* %carry4, align 8
  %360 = load i64, i64* %carry4, align 8
  %361 = load i64, i64* %h5, align 8
  %add426 = add i64 %361, %360
  store i64 %add426, i64* %h5, align 8
  %362 = load i64, i64* %carry4, align 8
  %mul427 = mul i64 %362, 67108864
  %363 = load i64, i64* %h4, align 8
  %sub428 = sub i64 %363, %mul427
  store i64 %sub428, i64* %h4, align 8
  %364 = load i64, i64* %h1, align 8
  %add429 = add i64 %364, 16777216
  %shr430 = ashr i64 %add429, 25
  store i64 %shr430, i64* %carry1, align 8
  %365 = load i64, i64* %carry1, align 8
  %366 = load i64, i64* %h2, align 8
  %add431 = add i64 %366, %365
  store i64 %add431, i64* %h2, align 8
  %367 = load i64, i64* %carry1, align 8
  %mul432 = mul i64 %367, 33554432
  %368 = load i64, i64* %h1, align 8
  %sub433 = sub i64 %368, %mul432
  store i64 %sub433, i64* %h1, align 8
  %369 = load i64, i64* %h5, align 8
  %add434 = add i64 %369, 16777216
  %shr435 = ashr i64 %add434, 25
  store i64 %shr435, i64* %carry5, align 8
  %370 = load i64, i64* %carry5, align 8
  %371 = load i64, i64* %h6, align 8
  %add436 = add i64 %371, %370
  store i64 %add436, i64* %h6, align 8
  %372 = load i64, i64* %carry5, align 8
  %mul437 = mul i64 %372, 33554432
  %373 = load i64, i64* %h5, align 8
  %sub438 = sub i64 %373, %mul437
  store i64 %sub438, i64* %h5, align 8
  %374 = load i64, i64* %h2, align 8
  %add439 = add i64 %374, 33554432
  %shr440 = ashr i64 %add439, 26
  store i64 %shr440, i64* %carry2, align 8
  %375 = load i64, i64* %carry2, align 8
  %376 = load i64, i64* %h3, align 8
  %add441 = add i64 %376, %375
  store i64 %add441, i64* %h3, align 8
  %377 = load i64, i64* %carry2, align 8
  %mul442 = mul i64 %377, 67108864
  %378 = load i64, i64* %h2, align 8
  %sub443 = sub i64 %378, %mul442
  store i64 %sub443, i64* %h2, align 8
  %379 = load i64, i64* %h6, align 8
  %add444 = add i64 %379, 33554432
  %shr445 = ashr i64 %add444, 26
  store i64 %shr445, i64* %carry6, align 8
  %380 = load i64, i64* %carry6, align 8
  %381 = load i64, i64* %h7, align 8
  %add446 = add i64 %381, %380
  store i64 %add446, i64* %h7, align 8
  %382 = load i64, i64* %carry6, align 8
  %mul447 = mul i64 %382, 67108864
  %383 = load i64, i64* %h6, align 8
  %sub448 = sub i64 %383, %mul447
  store i64 %sub448, i64* %h6, align 8
  %384 = load i64, i64* %h3, align 8
  %add449 = add i64 %384, 16777216
  %shr450 = ashr i64 %add449, 25
  store i64 %shr450, i64* %carry3, align 8
  %385 = load i64, i64* %carry3, align 8
  %386 = load i64, i64* %h4, align 8
  %add451 = add i64 %386, %385
  store i64 %add451, i64* %h4, align 8
  %387 = load i64, i64* %carry3, align 8
  %mul452 = mul i64 %387, 33554432
  %388 = load i64, i64* %h3, align 8
  %sub453 = sub i64 %388, %mul452
  store i64 %sub453, i64* %h3, align 8
  %389 = load i64, i64* %h7, align 8
  %add454 = add i64 %389, 16777216
  %shr455 = ashr i64 %add454, 25
  store i64 %shr455, i64* %carry7, align 8
  %390 = load i64, i64* %carry7, align 8
  %391 = load i64, i64* %h8, align 8
  %add456 = add i64 %391, %390
  store i64 %add456, i64* %h8, align 8
  %392 = load i64, i64* %carry7, align 8
  %mul457 = mul i64 %392, 33554432
  %393 = load i64, i64* %h7, align 8
  %sub458 = sub i64 %393, %mul457
  store i64 %sub458, i64* %h7, align 8
  %394 = load i64, i64* %h4, align 8
  %add459 = add i64 %394, 33554432
  %shr460 = ashr i64 %add459, 26
  store i64 %shr460, i64* %carry4, align 8
  %395 = load i64, i64* %carry4, align 8
  %396 = load i64, i64* %h5, align 8
  %add461 = add i64 %396, %395
  store i64 %add461, i64* %h5, align 8
  %397 = load i64, i64* %carry4, align 8
  %mul462 = mul i64 %397, 67108864
  %398 = load i64, i64* %h4, align 8
  %sub463 = sub i64 %398, %mul462
  store i64 %sub463, i64* %h4, align 8
  %399 = load i64, i64* %h8, align 8
  %add464 = add i64 %399, 33554432
  %shr465 = ashr i64 %add464, 26
  store i64 %shr465, i64* %carry8, align 8
  %400 = load i64, i64* %carry8, align 8
  %401 = load i64, i64* %h9, align 8
  %add466 = add i64 %401, %400
  store i64 %add466, i64* %h9, align 8
  %402 = load i64, i64* %carry8, align 8
  %mul467 = mul i64 %402, 67108864
  %403 = load i64, i64* %h8, align 8
  %sub468 = sub i64 %403, %mul467
  store i64 %sub468, i64* %h8, align 8
  %404 = load i64, i64* %h9, align 8
  %add469 = add i64 %404, 16777216
  %shr470 = ashr i64 %add469, 25
  store i64 %shr470, i64* %carry9, align 8
  %405 = load i64, i64* %carry9, align 8
  %mul471 = mul i64 %405, 19
  %406 = load i64, i64* %h0, align 8
  %add472 = add i64 %406, %mul471
  store i64 %add472, i64* %h0, align 8
  %407 = load i64, i64* %carry9, align 8
  %mul473 = mul i64 %407, 33554432
  %408 = load i64, i64* %h9, align 8
  %sub474 = sub i64 %408, %mul473
  store i64 %sub474, i64* %h9, align 8
  %409 = load i64, i64* %h0, align 8
  %add475 = add i64 %409, 33554432
  %shr476 = ashr i64 %add475, 26
  store i64 %shr476, i64* %carry0, align 8
  %410 = load i64, i64* %carry0, align 8
  %411 = load i64, i64* %h1, align 8
  %add477 = add i64 %411, %410
  store i64 %add477, i64* %h1, align 8
  %412 = load i64, i64* %carry0, align 8
  %mul478 = mul i64 %412, 67108864
  %413 = load i64, i64* %h0, align 8
  %sub479 = sub i64 %413, %mul478
  store i64 %sub479, i64* %h0, align 8
  %414 = load i64, i64* %h0, align 8
  %conv480 = trunc i64 %414 to i32
  %415 = load i32*, i32** %h.addr, align 4
  %arrayidx481 = getelementptr i32, i32* %415, i32 0
  store i32 %conv480, i32* %arrayidx481, align 4
  %416 = load i64, i64* %h1, align 8
  %conv482 = trunc i64 %416 to i32
  %417 = load i32*, i32** %h.addr, align 4
  %arrayidx483 = getelementptr i32, i32* %417, i32 1
  store i32 %conv482, i32* %arrayidx483, align 4
  %418 = load i64, i64* %h2, align 8
  %conv484 = trunc i64 %418 to i32
  %419 = load i32*, i32** %h.addr, align 4
  %arrayidx485 = getelementptr i32, i32* %419, i32 2
  store i32 %conv484, i32* %arrayidx485, align 4
  %420 = load i64, i64* %h3, align 8
  %conv486 = trunc i64 %420 to i32
  %421 = load i32*, i32** %h.addr, align 4
  %arrayidx487 = getelementptr i32, i32* %421, i32 3
  store i32 %conv486, i32* %arrayidx487, align 4
  %422 = load i64, i64* %h4, align 8
  %conv488 = trunc i64 %422 to i32
  %423 = load i32*, i32** %h.addr, align 4
  %arrayidx489 = getelementptr i32, i32* %423, i32 4
  store i32 %conv488, i32* %arrayidx489, align 4
  %424 = load i64, i64* %h5, align 8
  %conv490 = trunc i64 %424 to i32
  %425 = load i32*, i32** %h.addr, align 4
  %arrayidx491 = getelementptr i32, i32* %425, i32 5
  store i32 %conv490, i32* %arrayidx491, align 4
  %426 = load i64, i64* %h6, align 8
  %conv492 = trunc i64 %426 to i32
  %427 = load i32*, i32** %h.addr, align 4
  %arrayidx493 = getelementptr i32, i32* %427, i32 6
  store i32 %conv492, i32* %arrayidx493, align 4
  %428 = load i64, i64* %h7, align 8
  %conv494 = trunc i64 %428 to i32
  %429 = load i32*, i32** %h.addr, align 4
  %arrayidx495 = getelementptr i32, i32* %429, i32 7
  store i32 %conv494, i32* %arrayidx495, align 4
  %430 = load i64, i64* %h8, align 8
  %conv496 = trunc i64 %430 to i32
  %431 = load i32*, i32** %h.addr, align 4
  %arrayidx497 = getelementptr i32, i32* %431, i32 8
  store i32 %conv496, i32* %arrayidx497, align 4
  %432 = load i64, i64* %h9, align 8
  %conv498 = trunc i64 %432 to i32
  %433 = load i32*, i32** %h.addr, align 4
  %arrayidx499 = getelementptr i32, i32* %433, i32 9
  store i32 %conv498, i32* %arrayidx499, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_sq(i32* %h, i32* %f) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  %f0_2 = alloca i32, align 4
  %f1_2 = alloca i32, align 4
  %f2_2 = alloca i32, align 4
  %f3_2 = alloca i32, align 4
  %f4_2 = alloca i32, align 4
  %f5_2 = alloca i32, align 4
  %f6_2 = alloca i32, align 4
  %f7_2 = alloca i32, align 4
  %f5_38 = alloca i32, align 4
  %f6_19 = alloca i32, align 4
  %f7_38 = alloca i32, align 4
  %f8_19 = alloca i32, align 4
  %f9_38 = alloca i32, align 4
  %f0f0 = alloca i64, align 8
  %f0f1_2 = alloca i64, align 8
  %f0f2_2 = alloca i64, align 8
  %f0f3_2 = alloca i64, align 8
  %f0f4_2 = alloca i64, align 8
  %f0f5_2 = alloca i64, align 8
  %f0f6_2 = alloca i64, align 8
  %f0f7_2 = alloca i64, align 8
  %f0f8_2 = alloca i64, align 8
  %f0f9_2 = alloca i64, align 8
  %f1f1_2 = alloca i64, align 8
  %f1f2_2 = alloca i64, align 8
  %f1f3_4 = alloca i64, align 8
  %f1f4_2 = alloca i64, align 8
  %f1f5_4 = alloca i64, align 8
  %f1f6_2 = alloca i64, align 8
  %f1f7_4 = alloca i64, align 8
  %f1f8_2 = alloca i64, align 8
  %f1f9_76 = alloca i64, align 8
  %f2f2 = alloca i64, align 8
  %f2f3_2 = alloca i64, align 8
  %f2f4_2 = alloca i64, align 8
  %f2f5_2 = alloca i64, align 8
  %f2f6_2 = alloca i64, align 8
  %f2f7_2 = alloca i64, align 8
  %f2f8_38 = alloca i64, align 8
  %f2f9_38 = alloca i64, align 8
  %f3f3_2 = alloca i64, align 8
  %f3f4_2 = alloca i64, align 8
  %f3f5_4 = alloca i64, align 8
  %f3f6_2 = alloca i64, align 8
  %f3f7_76 = alloca i64, align 8
  %f3f8_38 = alloca i64, align 8
  %f3f9_76 = alloca i64, align 8
  %f4f4 = alloca i64, align 8
  %f4f5_2 = alloca i64, align 8
  %f4f6_38 = alloca i64, align 8
  %f4f7_38 = alloca i64, align 8
  %f4f8_38 = alloca i64, align 8
  %f4f9_38 = alloca i64, align 8
  %f5f5_38 = alloca i64, align 8
  %f5f6_38 = alloca i64, align 8
  %f5f7_76 = alloca i64, align 8
  %f5f8_38 = alloca i64, align 8
  %f5f9_76 = alloca i64, align 8
  %f6f6_19 = alloca i64, align 8
  %f6f7_38 = alloca i64, align 8
  %f6f8_38 = alloca i64, align 8
  %f6f9_38 = alloca i64, align 8
  %f7f7_38 = alloca i64, align 8
  %f7f8_38 = alloca i64, align 8
  %f7f9_76 = alloca i64, align 8
  %f8f8_19 = alloca i64, align 8
  %f8f9_38 = alloca i64, align 8
  %f9f9_38 = alloca i64, align 8
  %h0 = alloca i64, align 8
  %h1 = alloca i64, align 8
  %h2 = alloca i64, align 8
  %h3 = alloca i64, align 8
  %h4 = alloca i64, align 8
  %h5 = alloca i64, align 8
  %h6 = alloca i64, align 8
  %h7 = alloca i64, align 8
  %h8 = alloca i64, align 8
  %h9 = alloca i64, align 8
  %carry0 = alloca i64, align 8
  %carry1 = alloca i64, align 8
  %carry2 = alloca i64, align 8
  %carry3 = alloca i64, align 8
  %carry4 = alloca i64, align 8
  %carry5 = alloca i64, align 8
  %carry6 = alloca i64, align 8
  %carry7 = alloca i64, align 8
  %carry8 = alloca i64, align 8
  %carry9 = alloca i64, align 8
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  %0 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  store i32 %1, i32* %f0, align 4
  %2 = load i32*, i32** %f.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %2, i32 1
  %3 = load i32, i32* %arrayidx1, align 4
  store i32 %3, i32* %f1, align 4
  %4 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %4, i32 2
  %5 = load i32, i32* %arrayidx2, align 4
  store i32 %5, i32* %f2, align 4
  %6 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %6, i32 3
  %7 = load i32, i32* %arrayidx3, align 4
  store i32 %7, i32* %f3, align 4
  %8 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %8, i32 4
  %9 = load i32, i32* %arrayidx4, align 4
  store i32 %9, i32* %f4, align 4
  %10 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %10, i32 5
  %11 = load i32, i32* %arrayidx5, align 4
  store i32 %11, i32* %f5, align 4
  %12 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %12, i32 6
  %13 = load i32, i32* %arrayidx6, align 4
  store i32 %13, i32* %f6, align 4
  %14 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %14, i32 7
  %15 = load i32, i32* %arrayidx7, align 4
  store i32 %15, i32* %f7, align 4
  %16 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %16, i32 8
  %17 = load i32, i32* %arrayidx8, align 4
  store i32 %17, i32* %f8, align 4
  %18 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %18, i32 9
  %19 = load i32, i32* %arrayidx9, align 4
  store i32 %19, i32* %f9, align 4
  %20 = load i32, i32* %f0, align 4
  %mul = mul i32 2, %20
  store i32 %mul, i32* %f0_2, align 4
  %21 = load i32, i32* %f1, align 4
  %mul10 = mul i32 2, %21
  store i32 %mul10, i32* %f1_2, align 4
  %22 = load i32, i32* %f2, align 4
  %mul11 = mul i32 2, %22
  store i32 %mul11, i32* %f2_2, align 4
  %23 = load i32, i32* %f3, align 4
  %mul12 = mul i32 2, %23
  store i32 %mul12, i32* %f3_2, align 4
  %24 = load i32, i32* %f4, align 4
  %mul13 = mul i32 2, %24
  store i32 %mul13, i32* %f4_2, align 4
  %25 = load i32, i32* %f5, align 4
  %mul14 = mul i32 2, %25
  store i32 %mul14, i32* %f5_2, align 4
  %26 = load i32, i32* %f6, align 4
  %mul15 = mul i32 2, %26
  store i32 %mul15, i32* %f6_2, align 4
  %27 = load i32, i32* %f7, align 4
  %mul16 = mul i32 2, %27
  store i32 %mul16, i32* %f7_2, align 4
  %28 = load i32, i32* %f5, align 4
  %mul17 = mul i32 38, %28
  store i32 %mul17, i32* %f5_38, align 4
  %29 = load i32, i32* %f6, align 4
  %mul18 = mul i32 19, %29
  store i32 %mul18, i32* %f6_19, align 4
  %30 = load i32, i32* %f7, align 4
  %mul19 = mul i32 38, %30
  store i32 %mul19, i32* %f7_38, align 4
  %31 = load i32, i32* %f8, align 4
  %mul20 = mul i32 19, %31
  store i32 %mul20, i32* %f8_19, align 4
  %32 = load i32, i32* %f9, align 4
  %mul21 = mul i32 38, %32
  store i32 %mul21, i32* %f9_38, align 4
  %33 = load i32, i32* %f0, align 4
  %conv = sext i32 %33 to i64
  %34 = load i32, i32* %f0, align 4
  %conv22 = sext i32 %34 to i64
  %mul23 = mul i64 %conv, %conv22
  store i64 %mul23, i64* %f0f0, align 8
  %35 = load i32, i32* %f0_2, align 4
  %conv24 = sext i32 %35 to i64
  %36 = load i32, i32* %f1, align 4
  %conv25 = sext i32 %36 to i64
  %mul26 = mul i64 %conv24, %conv25
  store i64 %mul26, i64* %f0f1_2, align 8
  %37 = load i32, i32* %f0_2, align 4
  %conv27 = sext i32 %37 to i64
  %38 = load i32, i32* %f2, align 4
  %conv28 = sext i32 %38 to i64
  %mul29 = mul i64 %conv27, %conv28
  store i64 %mul29, i64* %f0f2_2, align 8
  %39 = load i32, i32* %f0_2, align 4
  %conv30 = sext i32 %39 to i64
  %40 = load i32, i32* %f3, align 4
  %conv31 = sext i32 %40 to i64
  %mul32 = mul i64 %conv30, %conv31
  store i64 %mul32, i64* %f0f3_2, align 8
  %41 = load i32, i32* %f0_2, align 4
  %conv33 = sext i32 %41 to i64
  %42 = load i32, i32* %f4, align 4
  %conv34 = sext i32 %42 to i64
  %mul35 = mul i64 %conv33, %conv34
  store i64 %mul35, i64* %f0f4_2, align 8
  %43 = load i32, i32* %f0_2, align 4
  %conv36 = sext i32 %43 to i64
  %44 = load i32, i32* %f5, align 4
  %conv37 = sext i32 %44 to i64
  %mul38 = mul i64 %conv36, %conv37
  store i64 %mul38, i64* %f0f5_2, align 8
  %45 = load i32, i32* %f0_2, align 4
  %conv39 = sext i32 %45 to i64
  %46 = load i32, i32* %f6, align 4
  %conv40 = sext i32 %46 to i64
  %mul41 = mul i64 %conv39, %conv40
  store i64 %mul41, i64* %f0f6_2, align 8
  %47 = load i32, i32* %f0_2, align 4
  %conv42 = sext i32 %47 to i64
  %48 = load i32, i32* %f7, align 4
  %conv43 = sext i32 %48 to i64
  %mul44 = mul i64 %conv42, %conv43
  store i64 %mul44, i64* %f0f7_2, align 8
  %49 = load i32, i32* %f0_2, align 4
  %conv45 = sext i32 %49 to i64
  %50 = load i32, i32* %f8, align 4
  %conv46 = sext i32 %50 to i64
  %mul47 = mul i64 %conv45, %conv46
  store i64 %mul47, i64* %f0f8_2, align 8
  %51 = load i32, i32* %f0_2, align 4
  %conv48 = sext i32 %51 to i64
  %52 = load i32, i32* %f9, align 4
  %conv49 = sext i32 %52 to i64
  %mul50 = mul i64 %conv48, %conv49
  store i64 %mul50, i64* %f0f9_2, align 8
  %53 = load i32, i32* %f1_2, align 4
  %conv51 = sext i32 %53 to i64
  %54 = load i32, i32* %f1, align 4
  %conv52 = sext i32 %54 to i64
  %mul53 = mul i64 %conv51, %conv52
  store i64 %mul53, i64* %f1f1_2, align 8
  %55 = load i32, i32* %f1_2, align 4
  %conv54 = sext i32 %55 to i64
  %56 = load i32, i32* %f2, align 4
  %conv55 = sext i32 %56 to i64
  %mul56 = mul i64 %conv54, %conv55
  store i64 %mul56, i64* %f1f2_2, align 8
  %57 = load i32, i32* %f1_2, align 4
  %conv57 = sext i32 %57 to i64
  %58 = load i32, i32* %f3_2, align 4
  %conv58 = sext i32 %58 to i64
  %mul59 = mul i64 %conv57, %conv58
  store i64 %mul59, i64* %f1f3_4, align 8
  %59 = load i32, i32* %f1_2, align 4
  %conv60 = sext i32 %59 to i64
  %60 = load i32, i32* %f4, align 4
  %conv61 = sext i32 %60 to i64
  %mul62 = mul i64 %conv60, %conv61
  store i64 %mul62, i64* %f1f4_2, align 8
  %61 = load i32, i32* %f1_2, align 4
  %conv63 = sext i32 %61 to i64
  %62 = load i32, i32* %f5_2, align 4
  %conv64 = sext i32 %62 to i64
  %mul65 = mul i64 %conv63, %conv64
  store i64 %mul65, i64* %f1f5_4, align 8
  %63 = load i32, i32* %f1_2, align 4
  %conv66 = sext i32 %63 to i64
  %64 = load i32, i32* %f6, align 4
  %conv67 = sext i32 %64 to i64
  %mul68 = mul i64 %conv66, %conv67
  store i64 %mul68, i64* %f1f6_2, align 8
  %65 = load i32, i32* %f1_2, align 4
  %conv69 = sext i32 %65 to i64
  %66 = load i32, i32* %f7_2, align 4
  %conv70 = sext i32 %66 to i64
  %mul71 = mul i64 %conv69, %conv70
  store i64 %mul71, i64* %f1f7_4, align 8
  %67 = load i32, i32* %f1_2, align 4
  %conv72 = sext i32 %67 to i64
  %68 = load i32, i32* %f8, align 4
  %conv73 = sext i32 %68 to i64
  %mul74 = mul i64 %conv72, %conv73
  store i64 %mul74, i64* %f1f8_2, align 8
  %69 = load i32, i32* %f1_2, align 4
  %conv75 = sext i32 %69 to i64
  %70 = load i32, i32* %f9_38, align 4
  %conv76 = sext i32 %70 to i64
  %mul77 = mul i64 %conv75, %conv76
  store i64 %mul77, i64* %f1f9_76, align 8
  %71 = load i32, i32* %f2, align 4
  %conv78 = sext i32 %71 to i64
  %72 = load i32, i32* %f2, align 4
  %conv79 = sext i32 %72 to i64
  %mul80 = mul i64 %conv78, %conv79
  store i64 %mul80, i64* %f2f2, align 8
  %73 = load i32, i32* %f2_2, align 4
  %conv81 = sext i32 %73 to i64
  %74 = load i32, i32* %f3, align 4
  %conv82 = sext i32 %74 to i64
  %mul83 = mul i64 %conv81, %conv82
  store i64 %mul83, i64* %f2f3_2, align 8
  %75 = load i32, i32* %f2_2, align 4
  %conv84 = sext i32 %75 to i64
  %76 = load i32, i32* %f4, align 4
  %conv85 = sext i32 %76 to i64
  %mul86 = mul i64 %conv84, %conv85
  store i64 %mul86, i64* %f2f4_2, align 8
  %77 = load i32, i32* %f2_2, align 4
  %conv87 = sext i32 %77 to i64
  %78 = load i32, i32* %f5, align 4
  %conv88 = sext i32 %78 to i64
  %mul89 = mul i64 %conv87, %conv88
  store i64 %mul89, i64* %f2f5_2, align 8
  %79 = load i32, i32* %f2_2, align 4
  %conv90 = sext i32 %79 to i64
  %80 = load i32, i32* %f6, align 4
  %conv91 = sext i32 %80 to i64
  %mul92 = mul i64 %conv90, %conv91
  store i64 %mul92, i64* %f2f6_2, align 8
  %81 = load i32, i32* %f2_2, align 4
  %conv93 = sext i32 %81 to i64
  %82 = load i32, i32* %f7, align 4
  %conv94 = sext i32 %82 to i64
  %mul95 = mul i64 %conv93, %conv94
  store i64 %mul95, i64* %f2f7_2, align 8
  %83 = load i32, i32* %f2_2, align 4
  %conv96 = sext i32 %83 to i64
  %84 = load i32, i32* %f8_19, align 4
  %conv97 = sext i32 %84 to i64
  %mul98 = mul i64 %conv96, %conv97
  store i64 %mul98, i64* %f2f8_38, align 8
  %85 = load i32, i32* %f2, align 4
  %conv99 = sext i32 %85 to i64
  %86 = load i32, i32* %f9_38, align 4
  %conv100 = sext i32 %86 to i64
  %mul101 = mul i64 %conv99, %conv100
  store i64 %mul101, i64* %f2f9_38, align 8
  %87 = load i32, i32* %f3_2, align 4
  %conv102 = sext i32 %87 to i64
  %88 = load i32, i32* %f3, align 4
  %conv103 = sext i32 %88 to i64
  %mul104 = mul i64 %conv102, %conv103
  store i64 %mul104, i64* %f3f3_2, align 8
  %89 = load i32, i32* %f3_2, align 4
  %conv105 = sext i32 %89 to i64
  %90 = load i32, i32* %f4, align 4
  %conv106 = sext i32 %90 to i64
  %mul107 = mul i64 %conv105, %conv106
  store i64 %mul107, i64* %f3f4_2, align 8
  %91 = load i32, i32* %f3_2, align 4
  %conv108 = sext i32 %91 to i64
  %92 = load i32, i32* %f5_2, align 4
  %conv109 = sext i32 %92 to i64
  %mul110 = mul i64 %conv108, %conv109
  store i64 %mul110, i64* %f3f5_4, align 8
  %93 = load i32, i32* %f3_2, align 4
  %conv111 = sext i32 %93 to i64
  %94 = load i32, i32* %f6, align 4
  %conv112 = sext i32 %94 to i64
  %mul113 = mul i64 %conv111, %conv112
  store i64 %mul113, i64* %f3f6_2, align 8
  %95 = load i32, i32* %f3_2, align 4
  %conv114 = sext i32 %95 to i64
  %96 = load i32, i32* %f7_38, align 4
  %conv115 = sext i32 %96 to i64
  %mul116 = mul i64 %conv114, %conv115
  store i64 %mul116, i64* %f3f7_76, align 8
  %97 = load i32, i32* %f3_2, align 4
  %conv117 = sext i32 %97 to i64
  %98 = load i32, i32* %f8_19, align 4
  %conv118 = sext i32 %98 to i64
  %mul119 = mul i64 %conv117, %conv118
  store i64 %mul119, i64* %f3f8_38, align 8
  %99 = load i32, i32* %f3_2, align 4
  %conv120 = sext i32 %99 to i64
  %100 = load i32, i32* %f9_38, align 4
  %conv121 = sext i32 %100 to i64
  %mul122 = mul i64 %conv120, %conv121
  store i64 %mul122, i64* %f3f9_76, align 8
  %101 = load i32, i32* %f4, align 4
  %conv123 = sext i32 %101 to i64
  %102 = load i32, i32* %f4, align 4
  %conv124 = sext i32 %102 to i64
  %mul125 = mul i64 %conv123, %conv124
  store i64 %mul125, i64* %f4f4, align 8
  %103 = load i32, i32* %f4_2, align 4
  %conv126 = sext i32 %103 to i64
  %104 = load i32, i32* %f5, align 4
  %conv127 = sext i32 %104 to i64
  %mul128 = mul i64 %conv126, %conv127
  store i64 %mul128, i64* %f4f5_2, align 8
  %105 = load i32, i32* %f4_2, align 4
  %conv129 = sext i32 %105 to i64
  %106 = load i32, i32* %f6_19, align 4
  %conv130 = sext i32 %106 to i64
  %mul131 = mul i64 %conv129, %conv130
  store i64 %mul131, i64* %f4f6_38, align 8
  %107 = load i32, i32* %f4, align 4
  %conv132 = sext i32 %107 to i64
  %108 = load i32, i32* %f7_38, align 4
  %conv133 = sext i32 %108 to i64
  %mul134 = mul i64 %conv132, %conv133
  store i64 %mul134, i64* %f4f7_38, align 8
  %109 = load i32, i32* %f4_2, align 4
  %conv135 = sext i32 %109 to i64
  %110 = load i32, i32* %f8_19, align 4
  %conv136 = sext i32 %110 to i64
  %mul137 = mul i64 %conv135, %conv136
  store i64 %mul137, i64* %f4f8_38, align 8
  %111 = load i32, i32* %f4, align 4
  %conv138 = sext i32 %111 to i64
  %112 = load i32, i32* %f9_38, align 4
  %conv139 = sext i32 %112 to i64
  %mul140 = mul i64 %conv138, %conv139
  store i64 %mul140, i64* %f4f9_38, align 8
  %113 = load i32, i32* %f5, align 4
  %conv141 = sext i32 %113 to i64
  %114 = load i32, i32* %f5_38, align 4
  %conv142 = sext i32 %114 to i64
  %mul143 = mul i64 %conv141, %conv142
  store i64 %mul143, i64* %f5f5_38, align 8
  %115 = load i32, i32* %f5_2, align 4
  %conv144 = sext i32 %115 to i64
  %116 = load i32, i32* %f6_19, align 4
  %conv145 = sext i32 %116 to i64
  %mul146 = mul i64 %conv144, %conv145
  store i64 %mul146, i64* %f5f6_38, align 8
  %117 = load i32, i32* %f5_2, align 4
  %conv147 = sext i32 %117 to i64
  %118 = load i32, i32* %f7_38, align 4
  %conv148 = sext i32 %118 to i64
  %mul149 = mul i64 %conv147, %conv148
  store i64 %mul149, i64* %f5f7_76, align 8
  %119 = load i32, i32* %f5_2, align 4
  %conv150 = sext i32 %119 to i64
  %120 = load i32, i32* %f8_19, align 4
  %conv151 = sext i32 %120 to i64
  %mul152 = mul i64 %conv150, %conv151
  store i64 %mul152, i64* %f5f8_38, align 8
  %121 = load i32, i32* %f5_2, align 4
  %conv153 = sext i32 %121 to i64
  %122 = load i32, i32* %f9_38, align 4
  %conv154 = sext i32 %122 to i64
  %mul155 = mul i64 %conv153, %conv154
  store i64 %mul155, i64* %f5f9_76, align 8
  %123 = load i32, i32* %f6, align 4
  %conv156 = sext i32 %123 to i64
  %124 = load i32, i32* %f6_19, align 4
  %conv157 = sext i32 %124 to i64
  %mul158 = mul i64 %conv156, %conv157
  store i64 %mul158, i64* %f6f6_19, align 8
  %125 = load i32, i32* %f6, align 4
  %conv159 = sext i32 %125 to i64
  %126 = load i32, i32* %f7_38, align 4
  %conv160 = sext i32 %126 to i64
  %mul161 = mul i64 %conv159, %conv160
  store i64 %mul161, i64* %f6f7_38, align 8
  %127 = load i32, i32* %f6_2, align 4
  %conv162 = sext i32 %127 to i64
  %128 = load i32, i32* %f8_19, align 4
  %conv163 = sext i32 %128 to i64
  %mul164 = mul i64 %conv162, %conv163
  store i64 %mul164, i64* %f6f8_38, align 8
  %129 = load i32, i32* %f6, align 4
  %conv165 = sext i32 %129 to i64
  %130 = load i32, i32* %f9_38, align 4
  %conv166 = sext i32 %130 to i64
  %mul167 = mul i64 %conv165, %conv166
  store i64 %mul167, i64* %f6f9_38, align 8
  %131 = load i32, i32* %f7, align 4
  %conv168 = sext i32 %131 to i64
  %132 = load i32, i32* %f7_38, align 4
  %conv169 = sext i32 %132 to i64
  %mul170 = mul i64 %conv168, %conv169
  store i64 %mul170, i64* %f7f7_38, align 8
  %133 = load i32, i32* %f7_2, align 4
  %conv171 = sext i32 %133 to i64
  %134 = load i32, i32* %f8_19, align 4
  %conv172 = sext i32 %134 to i64
  %mul173 = mul i64 %conv171, %conv172
  store i64 %mul173, i64* %f7f8_38, align 8
  %135 = load i32, i32* %f7_2, align 4
  %conv174 = sext i32 %135 to i64
  %136 = load i32, i32* %f9_38, align 4
  %conv175 = sext i32 %136 to i64
  %mul176 = mul i64 %conv174, %conv175
  store i64 %mul176, i64* %f7f9_76, align 8
  %137 = load i32, i32* %f8, align 4
  %conv177 = sext i32 %137 to i64
  %138 = load i32, i32* %f8_19, align 4
  %conv178 = sext i32 %138 to i64
  %mul179 = mul i64 %conv177, %conv178
  store i64 %mul179, i64* %f8f8_19, align 8
  %139 = load i32, i32* %f8, align 4
  %conv180 = sext i32 %139 to i64
  %140 = load i32, i32* %f9_38, align 4
  %conv181 = sext i32 %140 to i64
  %mul182 = mul i64 %conv180, %conv181
  store i64 %mul182, i64* %f8f9_38, align 8
  %141 = load i32, i32* %f9, align 4
  %conv183 = sext i32 %141 to i64
  %142 = load i32, i32* %f9_38, align 4
  %conv184 = sext i32 %142 to i64
  %mul185 = mul i64 %conv183, %conv184
  store i64 %mul185, i64* %f9f9_38, align 8
  %143 = load i64, i64* %f0f0, align 8
  %144 = load i64, i64* %f1f9_76, align 8
  %add = add i64 %143, %144
  %145 = load i64, i64* %f2f8_38, align 8
  %add186 = add i64 %add, %145
  %146 = load i64, i64* %f3f7_76, align 8
  %add187 = add i64 %add186, %146
  %147 = load i64, i64* %f4f6_38, align 8
  %add188 = add i64 %add187, %147
  %148 = load i64, i64* %f5f5_38, align 8
  %add189 = add i64 %add188, %148
  store i64 %add189, i64* %h0, align 8
  %149 = load i64, i64* %f0f1_2, align 8
  %150 = load i64, i64* %f2f9_38, align 8
  %add190 = add i64 %149, %150
  %151 = load i64, i64* %f3f8_38, align 8
  %add191 = add i64 %add190, %151
  %152 = load i64, i64* %f4f7_38, align 8
  %add192 = add i64 %add191, %152
  %153 = load i64, i64* %f5f6_38, align 8
  %add193 = add i64 %add192, %153
  store i64 %add193, i64* %h1, align 8
  %154 = load i64, i64* %f0f2_2, align 8
  %155 = load i64, i64* %f1f1_2, align 8
  %add194 = add i64 %154, %155
  %156 = load i64, i64* %f3f9_76, align 8
  %add195 = add i64 %add194, %156
  %157 = load i64, i64* %f4f8_38, align 8
  %add196 = add i64 %add195, %157
  %158 = load i64, i64* %f5f7_76, align 8
  %add197 = add i64 %add196, %158
  %159 = load i64, i64* %f6f6_19, align 8
  %add198 = add i64 %add197, %159
  store i64 %add198, i64* %h2, align 8
  %160 = load i64, i64* %f0f3_2, align 8
  %161 = load i64, i64* %f1f2_2, align 8
  %add199 = add i64 %160, %161
  %162 = load i64, i64* %f4f9_38, align 8
  %add200 = add i64 %add199, %162
  %163 = load i64, i64* %f5f8_38, align 8
  %add201 = add i64 %add200, %163
  %164 = load i64, i64* %f6f7_38, align 8
  %add202 = add i64 %add201, %164
  store i64 %add202, i64* %h3, align 8
  %165 = load i64, i64* %f0f4_2, align 8
  %166 = load i64, i64* %f1f3_4, align 8
  %add203 = add i64 %165, %166
  %167 = load i64, i64* %f2f2, align 8
  %add204 = add i64 %add203, %167
  %168 = load i64, i64* %f5f9_76, align 8
  %add205 = add i64 %add204, %168
  %169 = load i64, i64* %f6f8_38, align 8
  %add206 = add i64 %add205, %169
  %170 = load i64, i64* %f7f7_38, align 8
  %add207 = add i64 %add206, %170
  store i64 %add207, i64* %h4, align 8
  %171 = load i64, i64* %f0f5_2, align 8
  %172 = load i64, i64* %f1f4_2, align 8
  %add208 = add i64 %171, %172
  %173 = load i64, i64* %f2f3_2, align 8
  %add209 = add i64 %add208, %173
  %174 = load i64, i64* %f6f9_38, align 8
  %add210 = add i64 %add209, %174
  %175 = load i64, i64* %f7f8_38, align 8
  %add211 = add i64 %add210, %175
  store i64 %add211, i64* %h5, align 8
  %176 = load i64, i64* %f0f6_2, align 8
  %177 = load i64, i64* %f1f5_4, align 8
  %add212 = add i64 %176, %177
  %178 = load i64, i64* %f2f4_2, align 8
  %add213 = add i64 %add212, %178
  %179 = load i64, i64* %f3f3_2, align 8
  %add214 = add i64 %add213, %179
  %180 = load i64, i64* %f7f9_76, align 8
  %add215 = add i64 %add214, %180
  %181 = load i64, i64* %f8f8_19, align 8
  %add216 = add i64 %add215, %181
  store i64 %add216, i64* %h6, align 8
  %182 = load i64, i64* %f0f7_2, align 8
  %183 = load i64, i64* %f1f6_2, align 8
  %add217 = add i64 %182, %183
  %184 = load i64, i64* %f2f5_2, align 8
  %add218 = add i64 %add217, %184
  %185 = load i64, i64* %f3f4_2, align 8
  %add219 = add i64 %add218, %185
  %186 = load i64, i64* %f8f9_38, align 8
  %add220 = add i64 %add219, %186
  store i64 %add220, i64* %h7, align 8
  %187 = load i64, i64* %f0f8_2, align 8
  %188 = load i64, i64* %f1f7_4, align 8
  %add221 = add i64 %187, %188
  %189 = load i64, i64* %f2f6_2, align 8
  %add222 = add i64 %add221, %189
  %190 = load i64, i64* %f3f5_4, align 8
  %add223 = add i64 %add222, %190
  %191 = load i64, i64* %f4f4, align 8
  %add224 = add i64 %add223, %191
  %192 = load i64, i64* %f9f9_38, align 8
  %add225 = add i64 %add224, %192
  store i64 %add225, i64* %h8, align 8
  %193 = load i64, i64* %f0f9_2, align 8
  %194 = load i64, i64* %f1f8_2, align 8
  %add226 = add i64 %193, %194
  %195 = load i64, i64* %f2f7_2, align 8
  %add227 = add i64 %add226, %195
  %196 = load i64, i64* %f3f6_2, align 8
  %add228 = add i64 %add227, %196
  %197 = load i64, i64* %f4f5_2, align 8
  %add229 = add i64 %add228, %197
  store i64 %add229, i64* %h9, align 8
  %198 = load i64, i64* %h0, align 8
  %add230 = add i64 %198, 33554432
  %shr = ashr i64 %add230, 26
  store i64 %shr, i64* %carry0, align 8
  %199 = load i64, i64* %carry0, align 8
  %200 = load i64, i64* %h1, align 8
  %add231 = add i64 %200, %199
  store i64 %add231, i64* %h1, align 8
  %201 = load i64, i64* %carry0, align 8
  %mul232 = mul i64 %201, 67108864
  %202 = load i64, i64* %h0, align 8
  %sub = sub i64 %202, %mul232
  store i64 %sub, i64* %h0, align 8
  %203 = load i64, i64* %h4, align 8
  %add233 = add i64 %203, 33554432
  %shr234 = ashr i64 %add233, 26
  store i64 %shr234, i64* %carry4, align 8
  %204 = load i64, i64* %carry4, align 8
  %205 = load i64, i64* %h5, align 8
  %add235 = add i64 %205, %204
  store i64 %add235, i64* %h5, align 8
  %206 = load i64, i64* %carry4, align 8
  %mul236 = mul i64 %206, 67108864
  %207 = load i64, i64* %h4, align 8
  %sub237 = sub i64 %207, %mul236
  store i64 %sub237, i64* %h4, align 8
  %208 = load i64, i64* %h1, align 8
  %add238 = add i64 %208, 16777216
  %shr239 = ashr i64 %add238, 25
  store i64 %shr239, i64* %carry1, align 8
  %209 = load i64, i64* %carry1, align 8
  %210 = load i64, i64* %h2, align 8
  %add240 = add i64 %210, %209
  store i64 %add240, i64* %h2, align 8
  %211 = load i64, i64* %carry1, align 8
  %mul241 = mul i64 %211, 33554432
  %212 = load i64, i64* %h1, align 8
  %sub242 = sub i64 %212, %mul241
  store i64 %sub242, i64* %h1, align 8
  %213 = load i64, i64* %h5, align 8
  %add243 = add i64 %213, 16777216
  %shr244 = ashr i64 %add243, 25
  store i64 %shr244, i64* %carry5, align 8
  %214 = load i64, i64* %carry5, align 8
  %215 = load i64, i64* %h6, align 8
  %add245 = add i64 %215, %214
  store i64 %add245, i64* %h6, align 8
  %216 = load i64, i64* %carry5, align 8
  %mul246 = mul i64 %216, 33554432
  %217 = load i64, i64* %h5, align 8
  %sub247 = sub i64 %217, %mul246
  store i64 %sub247, i64* %h5, align 8
  %218 = load i64, i64* %h2, align 8
  %add248 = add i64 %218, 33554432
  %shr249 = ashr i64 %add248, 26
  store i64 %shr249, i64* %carry2, align 8
  %219 = load i64, i64* %carry2, align 8
  %220 = load i64, i64* %h3, align 8
  %add250 = add i64 %220, %219
  store i64 %add250, i64* %h3, align 8
  %221 = load i64, i64* %carry2, align 8
  %mul251 = mul i64 %221, 67108864
  %222 = load i64, i64* %h2, align 8
  %sub252 = sub i64 %222, %mul251
  store i64 %sub252, i64* %h2, align 8
  %223 = load i64, i64* %h6, align 8
  %add253 = add i64 %223, 33554432
  %shr254 = ashr i64 %add253, 26
  store i64 %shr254, i64* %carry6, align 8
  %224 = load i64, i64* %carry6, align 8
  %225 = load i64, i64* %h7, align 8
  %add255 = add i64 %225, %224
  store i64 %add255, i64* %h7, align 8
  %226 = load i64, i64* %carry6, align 8
  %mul256 = mul i64 %226, 67108864
  %227 = load i64, i64* %h6, align 8
  %sub257 = sub i64 %227, %mul256
  store i64 %sub257, i64* %h6, align 8
  %228 = load i64, i64* %h3, align 8
  %add258 = add i64 %228, 16777216
  %shr259 = ashr i64 %add258, 25
  store i64 %shr259, i64* %carry3, align 8
  %229 = load i64, i64* %carry3, align 8
  %230 = load i64, i64* %h4, align 8
  %add260 = add i64 %230, %229
  store i64 %add260, i64* %h4, align 8
  %231 = load i64, i64* %carry3, align 8
  %mul261 = mul i64 %231, 33554432
  %232 = load i64, i64* %h3, align 8
  %sub262 = sub i64 %232, %mul261
  store i64 %sub262, i64* %h3, align 8
  %233 = load i64, i64* %h7, align 8
  %add263 = add i64 %233, 16777216
  %shr264 = ashr i64 %add263, 25
  store i64 %shr264, i64* %carry7, align 8
  %234 = load i64, i64* %carry7, align 8
  %235 = load i64, i64* %h8, align 8
  %add265 = add i64 %235, %234
  store i64 %add265, i64* %h8, align 8
  %236 = load i64, i64* %carry7, align 8
  %mul266 = mul i64 %236, 33554432
  %237 = load i64, i64* %h7, align 8
  %sub267 = sub i64 %237, %mul266
  store i64 %sub267, i64* %h7, align 8
  %238 = load i64, i64* %h4, align 8
  %add268 = add i64 %238, 33554432
  %shr269 = ashr i64 %add268, 26
  store i64 %shr269, i64* %carry4, align 8
  %239 = load i64, i64* %carry4, align 8
  %240 = load i64, i64* %h5, align 8
  %add270 = add i64 %240, %239
  store i64 %add270, i64* %h5, align 8
  %241 = load i64, i64* %carry4, align 8
  %mul271 = mul i64 %241, 67108864
  %242 = load i64, i64* %h4, align 8
  %sub272 = sub i64 %242, %mul271
  store i64 %sub272, i64* %h4, align 8
  %243 = load i64, i64* %h8, align 8
  %add273 = add i64 %243, 33554432
  %shr274 = ashr i64 %add273, 26
  store i64 %shr274, i64* %carry8, align 8
  %244 = load i64, i64* %carry8, align 8
  %245 = load i64, i64* %h9, align 8
  %add275 = add i64 %245, %244
  store i64 %add275, i64* %h9, align 8
  %246 = load i64, i64* %carry8, align 8
  %mul276 = mul i64 %246, 67108864
  %247 = load i64, i64* %h8, align 8
  %sub277 = sub i64 %247, %mul276
  store i64 %sub277, i64* %h8, align 8
  %248 = load i64, i64* %h9, align 8
  %add278 = add i64 %248, 16777216
  %shr279 = ashr i64 %add278, 25
  store i64 %shr279, i64* %carry9, align 8
  %249 = load i64, i64* %carry9, align 8
  %mul280 = mul i64 %249, 19
  %250 = load i64, i64* %h0, align 8
  %add281 = add i64 %250, %mul280
  store i64 %add281, i64* %h0, align 8
  %251 = load i64, i64* %carry9, align 8
  %mul282 = mul i64 %251, 33554432
  %252 = load i64, i64* %h9, align 8
  %sub283 = sub i64 %252, %mul282
  store i64 %sub283, i64* %h9, align 8
  %253 = load i64, i64* %h0, align 8
  %add284 = add i64 %253, 33554432
  %shr285 = ashr i64 %add284, 26
  store i64 %shr285, i64* %carry0, align 8
  %254 = load i64, i64* %carry0, align 8
  %255 = load i64, i64* %h1, align 8
  %add286 = add i64 %255, %254
  store i64 %add286, i64* %h1, align 8
  %256 = load i64, i64* %carry0, align 8
  %mul287 = mul i64 %256, 67108864
  %257 = load i64, i64* %h0, align 8
  %sub288 = sub i64 %257, %mul287
  store i64 %sub288, i64* %h0, align 8
  %258 = load i64, i64* %h0, align 8
  %conv289 = trunc i64 %258 to i32
  %259 = load i32*, i32** %h.addr, align 4
  %arrayidx290 = getelementptr i32, i32* %259, i32 0
  store i32 %conv289, i32* %arrayidx290, align 4
  %260 = load i64, i64* %h1, align 8
  %conv291 = trunc i64 %260 to i32
  %261 = load i32*, i32** %h.addr, align 4
  %arrayidx292 = getelementptr i32, i32* %261, i32 1
  store i32 %conv291, i32* %arrayidx292, align 4
  %262 = load i64, i64* %h2, align 8
  %conv293 = trunc i64 %262 to i32
  %263 = load i32*, i32** %h.addr, align 4
  %arrayidx294 = getelementptr i32, i32* %263, i32 2
  store i32 %conv293, i32* %arrayidx294, align 4
  %264 = load i64, i64* %h3, align 8
  %conv295 = trunc i64 %264 to i32
  %265 = load i32*, i32** %h.addr, align 4
  %arrayidx296 = getelementptr i32, i32* %265, i32 3
  store i32 %conv295, i32* %arrayidx296, align 4
  %266 = load i64, i64* %h4, align 8
  %conv297 = trunc i64 %266 to i32
  %267 = load i32*, i32** %h.addr, align 4
  %arrayidx298 = getelementptr i32, i32* %267, i32 4
  store i32 %conv297, i32* %arrayidx298, align 4
  %268 = load i64, i64* %h5, align 8
  %conv299 = trunc i64 %268 to i32
  %269 = load i32*, i32** %h.addr, align 4
  %arrayidx300 = getelementptr i32, i32* %269, i32 5
  store i32 %conv299, i32* %arrayidx300, align 4
  %270 = load i64, i64* %h6, align 8
  %conv301 = trunc i64 %270 to i32
  %271 = load i32*, i32** %h.addr, align 4
  %arrayidx302 = getelementptr i32, i32* %271, i32 6
  store i32 %conv301, i32* %arrayidx302, align 4
  %272 = load i64, i64* %h7, align 8
  %conv303 = trunc i64 %272 to i32
  %273 = load i32*, i32** %h.addr, align 4
  %arrayidx304 = getelementptr i32, i32* %273, i32 7
  store i32 %conv303, i32* %arrayidx304, align 4
  %274 = load i64, i64* %h8, align 8
  %conv305 = trunc i64 %274 to i32
  %275 = load i32*, i32** %h.addr, align 4
  %arrayidx306 = getelementptr i32, i32* %275, i32 8
  store i32 %conv305, i32* %arrayidx306, align 4
  %276 = load i64, i64* %h9, align 8
  %conv307 = trunc i64 %276 to i32
  %277 = load i32*, i32** %h.addr, align 4
  %arrayidx308 = getelementptr i32, i32* %277, i32 9
  store i32 %conv307, i32* %arrayidx308, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @fe25519_mul32(i32* %h, i32* %f, i32 %n) #0 {
entry:
  %h.addr = alloca i32*, align 4
  %f.addr = alloca i32*, align 4
  %n.addr = alloca i32, align 4
  %sn = alloca i64, align 8
  %f0 = alloca i32, align 4
  %f1 = alloca i32, align 4
  %f2 = alloca i32, align 4
  %f3 = alloca i32, align 4
  %f4 = alloca i32, align 4
  %f5 = alloca i32, align 4
  %f6 = alloca i32, align 4
  %f7 = alloca i32, align 4
  %f8 = alloca i32, align 4
  %f9 = alloca i32, align 4
  %h0 = alloca i64, align 8
  %h1 = alloca i64, align 8
  %h2 = alloca i64, align 8
  %h3 = alloca i64, align 8
  %h4 = alloca i64, align 8
  %h5 = alloca i64, align 8
  %h6 = alloca i64, align 8
  %h7 = alloca i64, align 8
  %h8 = alloca i64, align 8
  %h9 = alloca i64, align 8
  %carry0 = alloca i64, align 8
  %carry1 = alloca i64, align 8
  %carry2 = alloca i64, align 8
  %carry3 = alloca i64, align 8
  %carry4 = alloca i64, align 8
  %carry5 = alloca i64, align 8
  %carry6 = alloca i64, align 8
  %carry7 = alloca i64, align 8
  %carry8 = alloca i64, align 8
  %carry9 = alloca i64, align 8
  store i32* %h, i32** %h.addr, align 4
  store i32* %f, i32** %f.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %conv = zext i32 %0 to i64
  store i64 %conv, i64* %sn, align 8
  %1 = load i32*, i32** %f.addr, align 4
  %arrayidx = getelementptr i32, i32* %1, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  store i32 %2, i32* %f0, align 4
  %3 = load i32*, i32** %f.addr, align 4
  %arrayidx1 = getelementptr i32, i32* %3, i32 1
  %4 = load i32, i32* %arrayidx1, align 4
  store i32 %4, i32* %f1, align 4
  %5 = load i32*, i32** %f.addr, align 4
  %arrayidx2 = getelementptr i32, i32* %5, i32 2
  %6 = load i32, i32* %arrayidx2, align 4
  store i32 %6, i32* %f2, align 4
  %7 = load i32*, i32** %f.addr, align 4
  %arrayidx3 = getelementptr i32, i32* %7, i32 3
  %8 = load i32, i32* %arrayidx3, align 4
  store i32 %8, i32* %f3, align 4
  %9 = load i32*, i32** %f.addr, align 4
  %arrayidx4 = getelementptr i32, i32* %9, i32 4
  %10 = load i32, i32* %arrayidx4, align 4
  store i32 %10, i32* %f4, align 4
  %11 = load i32*, i32** %f.addr, align 4
  %arrayidx5 = getelementptr i32, i32* %11, i32 5
  %12 = load i32, i32* %arrayidx5, align 4
  store i32 %12, i32* %f5, align 4
  %13 = load i32*, i32** %f.addr, align 4
  %arrayidx6 = getelementptr i32, i32* %13, i32 6
  %14 = load i32, i32* %arrayidx6, align 4
  store i32 %14, i32* %f6, align 4
  %15 = load i32*, i32** %f.addr, align 4
  %arrayidx7 = getelementptr i32, i32* %15, i32 7
  %16 = load i32, i32* %arrayidx7, align 4
  store i32 %16, i32* %f7, align 4
  %17 = load i32*, i32** %f.addr, align 4
  %arrayidx8 = getelementptr i32, i32* %17, i32 8
  %18 = load i32, i32* %arrayidx8, align 4
  store i32 %18, i32* %f8, align 4
  %19 = load i32*, i32** %f.addr, align 4
  %arrayidx9 = getelementptr i32, i32* %19, i32 9
  %20 = load i32, i32* %arrayidx9, align 4
  store i32 %20, i32* %f9, align 4
  %21 = load i32, i32* %f0, align 4
  %conv10 = sext i32 %21 to i64
  %22 = load i64, i64* %sn, align 8
  %mul = mul i64 %conv10, %22
  store i64 %mul, i64* %h0, align 8
  %23 = load i32, i32* %f1, align 4
  %conv11 = sext i32 %23 to i64
  %24 = load i64, i64* %sn, align 8
  %mul12 = mul i64 %conv11, %24
  store i64 %mul12, i64* %h1, align 8
  %25 = load i32, i32* %f2, align 4
  %conv13 = sext i32 %25 to i64
  %26 = load i64, i64* %sn, align 8
  %mul14 = mul i64 %conv13, %26
  store i64 %mul14, i64* %h2, align 8
  %27 = load i32, i32* %f3, align 4
  %conv15 = sext i32 %27 to i64
  %28 = load i64, i64* %sn, align 8
  %mul16 = mul i64 %conv15, %28
  store i64 %mul16, i64* %h3, align 8
  %29 = load i32, i32* %f4, align 4
  %conv17 = sext i32 %29 to i64
  %30 = load i64, i64* %sn, align 8
  %mul18 = mul i64 %conv17, %30
  store i64 %mul18, i64* %h4, align 8
  %31 = load i32, i32* %f5, align 4
  %conv19 = sext i32 %31 to i64
  %32 = load i64, i64* %sn, align 8
  %mul20 = mul i64 %conv19, %32
  store i64 %mul20, i64* %h5, align 8
  %33 = load i32, i32* %f6, align 4
  %conv21 = sext i32 %33 to i64
  %34 = load i64, i64* %sn, align 8
  %mul22 = mul i64 %conv21, %34
  store i64 %mul22, i64* %h6, align 8
  %35 = load i32, i32* %f7, align 4
  %conv23 = sext i32 %35 to i64
  %36 = load i64, i64* %sn, align 8
  %mul24 = mul i64 %conv23, %36
  store i64 %mul24, i64* %h7, align 8
  %37 = load i32, i32* %f8, align 4
  %conv25 = sext i32 %37 to i64
  %38 = load i64, i64* %sn, align 8
  %mul26 = mul i64 %conv25, %38
  store i64 %mul26, i64* %h8, align 8
  %39 = load i32, i32* %f9, align 4
  %conv27 = sext i32 %39 to i64
  %40 = load i64, i64* %sn, align 8
  %mul28 = mul i64 %conv27, %40
  store i64 %mul28, i64* %h9, align 8
  %41 = load i64, i64* %h9, align 8
  %add = add i64 %41, 16777216
  %shr = ashr i64 %add, 25
  store i64 %shr, i64* %carry9, align 8
  %42 = load i64, i64* %carry9, align 8
  %mul29 = mul i64 %42, 19
  %43 = load i64, i64* %h0, align 8
  %add30 = add i64 %43, %mul29
  store i64 %add30, i64* %h0, align 8
  %44 = load i64, i64* %carry9, align 8
  %mul31 = mul i64 %44, 33554432
  %45 = load i64, i64* %h9, align 8
  %sub = sub i64 %45, %mul31
  store i64 %sub, i64* %h9, align 8
  %46 = load i64, i64* %h1, align 8
  %add32 = add i64 %46, 16777216
  %shr33 = ashr i64 %add32, 25
  store i64 %shr33, i64* %carry1, align 8
  %47 = load i64, i64* %carry1, align 8
  %48 = load i64, i64* %h2, align 8
  %add34 = add i64 %48, %47
  store i64 %add34, i64* %h2, align 8
  %49 = load i64, i64* %carry1, align 8
  %mul35 = mul i64 %49, 33554432
  %50 = load i64, i64* %h1, align 8
  %sub36 = sub i64 %50, %mul35
  store i64 %sub36, i64* %h1, align 8
  %51 = load i64, i64* %h3, align 8
  %add37 = add i64 %51, 16777216
  %shr38 = ashr i64 %add37, 25
  store i64 %shr38, i64* %carry3, align 8
  %52 = load i64, i64* %carry3, align 8
  %53 = load i64, i64* %h4, align 8
  %add39 = add i64 %53, %52
  store i64 %add39, i64* %h4, align 8
  %54 = load i64, i64* %carry3, align 8
  %mul40 = mul i64 %54, 33554432
  %55 = load i64, i64* %h3, align 8
  %sub41 = sub i64 %55, %mul40
  store i64 %sub41, i64* %h3, align 8
  %56 = load i64, i64* %h5, align 8
  %add42 = add i64 %56, 16777216
  %shr43 = ashr i64 %add42, 25
  store i64 %shr43, i64* %carry5, align 8
  %57 = load i64, i64* %carry5, align 8
  %58 = load i64, i64* %h6, align 8
  %add44 = add i64 %58, %57
  store i64 %add44, i64* %h6, align 8
  %59 = load i64, i64* %carry5, align 8
  %mul45 = mul i64 %59, 33554432
  %60 = load i64, i64* %h5, align 8
  %sub46 = sub i64 %60, %mul45
  store i64 %sub46, i64* %h5, align 8
  %61 = load i64, i64* %h7, align 8
  %add47 = add i64 %61, 16777216
  %shr48 = ashr i64 %add47, 25
  store i64 %shr48, i64* %carry7, align 8
  %62 = load i64, i64* %carry7, align 8
  %63 = load i64, i64* %h8, align 8
  %add49 = add i64 %63, %62
  store i64 %add49, i64* %h8, align 8
  %64 = load i64, i64* %carry7, align 8
  %mul50 = mul i64 %64, 33554432
  %65 = load i64, i64* %h7, align 8
  %sub51 = sub i64 %65, %mul50
  store i64 %sub51, i64* %h7, align 8
  %66 = load i64, i64* %h0, align 8
  %add52 = add i64 %66, 33554432
  %shr53 = ashr i64 %add52, 26
  store i64 %shr53, i64* %carry0, align 8
  %67 = load i64, i64* %carry0, align 8
  %68 = load i64, i64* %h1, align 8
  %add54 = add i64 %68, %67
  store i64 %add54, i64* %h1, align 8
  %69 = load i64, i64* %carry0, align 8
  %mul55 = mul i64 %69, 67108864
  %70 = load i64, i64* %h0, align 8
  %sub56 = sub i64 %70, %mul55
  store i64 %sub56, i64* %h0, align 8
  %71 = load i64, i64* %h2, align 8
  %add57 = add i64 %71, 33554432
  %shr58 = ashr i64 %add57, 26
  store i64 %shr58, i64* %carry2, align 8
  %72 = load i64, i64* %carry2, align 8
  %73 = load i64, i64* %h3, align 8
  %add59 = add i64 %73, %72
  store i64 %add59, i64* %h3, align 8
  %74 = load i64, i64* %carry2, align 8
  %mul60 = mul i64 %74, 67108864
  %75 = load i64, i64* %h2, align 8
  %sub61 = sub i64 %75, %mul60
  store i64 %sub61, i64* %h2, align 8
  %76 = load i64, i64* %h4, align 8
  %add62 = add i64 %76, 33554432
  %shr63 = ashr i64 %add62, 26
  store i64 %shr63, i64* %carry4, align 8
  %77 = load i64, i64* %carry4, align 8
  %78 = load i64, i64* %h5, align 8
  %add64 = add i64 %78, %77
  store i64 %add64, i64* %h5, align 8
  %79 = load i64, i64* %carry4, align 8
  %mul65 = mul i64 %79, 67108864
  %80 = load i64, i64* %h4, align 8
  %sub66 = sub i64 %80, %mul65
  store i64 %sub66, i64* %h4, align 8
  %81 = load i64, i64* %h6, align 8
  %add67 = add i64 %81, 33554432
  %shr68 = ashr i64 %add67, 26
  store i64 %shr68, i64* %carry6, align 8
  %82 = load i64, i64* %carry6, align 8
  %83 = load i64, i64* %h7, align 8
  %add69 = add i64 %83, %82
  store i64 %add69, i64* %h7, align 8
  %84 = load i64, i64* %carry6, align 8
  %mul70 = mul i64 %84, 67108864
  %85 = load i64, i64* %h6, align 8
  %sub71 = sub i64 %85, %mul70
  store i64 %sub71, i64* %h6, align 8
  %86 = load i64, i64* %h8, align 8
  %add72 = add i64 %86, 33554432
  %shr73 = ashr i64 %add72, 26
  store i64 %shr73, i64* %carry8, align 8
  %87 = load i64, i64* %carry8, align 8
  %88 = load i64, i64* %h9, align 8
  %add74 = add i64 %88, %87
  store i64 %add74, i64* %h9, align 8
  %89 = load i64, i64* %carry8, align 8
  %mul75 = mul i64 %89, 67108864
  %90 = load i64, i64* %h8, align 8
  %sub76 = sub i64 %90, %mul75
  store i64 %sub76, i64* %h8, align 8
  %91 = load i64, i64* %h0, align 8
  %conv77 = trunc i64 %91 to i32
  %92 = load i32*, i32** %h.addr, align 4
  %arrayidx78 = getelementptr i32, i32* %92, i32 0
  store i32 %conv77, i32* %arrayidx78, align 4
  %93 = load i64, i64* %h1, align 8
  %conv79 = trunc i64 %93 to i32
  %94 = load i32*, i32** %h.addr, align 4
  %arrayidx80 = getelementptr i32, i32* %94, i32 1
  store i32 %conv79, i32* %arrayidx80, align 4
  %95 = load i64, i64* %h2, align 8
  %conv81 = trunc i64 %95 to i32
  %96 = load i32*, i32** %h.addr, align 4
  %arrayidx82 = getelementptr i32, i32* %96, i32 2
  store i32 %conv81, i32* %arrayidx82, align 4
  %97 = load i64, i64* %h3, align 8
  %conv83 = trunc i64 %97 to i32
  %98 = load i32*, i32** %h.addr, align 4
  %arrayidx84 = getelementptr i32, i32* %98, i32 3
  store i32 %conv83, i32* %arrayidx84, align 4
  %99 = load i64, i64* %h4, align 8
  %conv85 = trunc i64 %99 to i32
  %100 = load i32*, i32** %h.addr, align 4
  %arrayidx86 = getelementptr i32, i32* %100, i32 4
  store i32 %conv85, i32* %arrayidx86, align 4
  %101 = load i64, i64* %h5, align 8
  %conv87 = trunc i64 %101 to i32
  %102 = load i32*, i32** %h.addr, align 4
  %arrayidx88 = getelementptr i32, i32* %102, i32 5
  store i32 %conv87, i32* %arrayidx88, align 4
  %103 = load i64, i64* %h6, align 8
  %conv89 = trunc i64 %103 to i32
  %104 = load i32*, i32** %h.addr, align 4
  %arrayidx90 = getelementptr i32, i32* %104, i32 6
  store i32 %conv89, i32* %arrayidx90, align 4
  %105 = load i64, i64* %h7, align 8
  %conv91 = trunc i64 %105 to i32
  %106 = load i32*, i32** %h.addr, align 4
  %arrayidx92 = getelementptr i32, i32* %106, i32 7
  store i32 %conv91, i32* %arrayidx92, align 4
  %107 = load i64, i64* %h8, align 8
  %conv93 = trunc i64 %107 to i32
  %108 = load i32*, i32** %h.addr, align 4
  %arrayidx94 = getelementptr i32, i32* %108, i32 8
  store i32 %conv93, i32* %arrayidx94, align 4
  %109 = load i64, i64* %h9, align 8
  %conv95 = trunc i64 %109 to i32
  %110 = load i32*, i32** %h.addr, align 4
  %arrayidx96 = getelementptr i32, i32* %110, i32 9
  store i32 %conv95, i32* %arrayidx96, align 4
  ret void
}

declare void @fe25519_invert(i32*, i32*) #1

declare void @fe25519_tobytes(i8*, i32*) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @edwards_to_montgomery(i32* %montgomeryX, i32* %edwardsY, i32* %edwardsZ) #0 {
entry:
  %montgomeryX.addr = alloca i32*, align 4
  %edwardsY.addr = alloca i32*, align 4
  %edwardsZ.addr = alloca i32*, align 4
  %tempX = alloca [10 x i32], align 16
  %tempZ = alloca [10 x i32], align 16
  store i32* %montgomeryX, i32** %montgomeryX.addr, align 4
  store i32* %edwardsY, i32** %edwardsY.addr, align 4
  store i32* %edwardsZ, i32** %edwardsZ.addr, align 4
  %arraydecay = getelementptr inbounds [10 x i32], [10 x i32]* %tempX, i32 0, i32 0
  %0 = load i32*, i32** %edwardsZ.addr, align 4
  %1 = load i32*, i32** %edwardsY.addr, align 4
  call void @fe25519_add(i32* %arraydecay, i32* %0, i32* %1)
  %arraydecay1 = getelementptr inbounds [10 x i32], [10 x i32]* %tempZ, i32 0, i32 0
  %2 = load i32*, i32** %edwardsZ.addr, align 4
  %3 = load i32*, i32** %edwardsY.addr, align 4
  call void @fe25519_sub(i32* %arraydecay1, i32* %2, i32* %3)
  %arraydecay2 = getelementptr inbounds [10 x i32], [10 x i32]* %tempZ, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [10 x i32], [10 x i32]* %tempZ, i32 0, i32 0
  call void @fe25519_invert(i32* %arraydecay2, i32* %arraydecay3)
  %4 = load i32*, i32** %montgomeryX.addr, align 4
  %arraydecay4 = getelementptr inbounds [10 x i32], [10 x i32]* %tempX, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [10 x i32], [10 x i32]* %tempZ, i32 0, i32 0
  call void @fe25519_mul(i32* %4, i32* %arraydecay4, i32* %arraydecay5)
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
