; ModuleID = 'crypto_auth/hmacsha512256/auth_hmacsha512256.c'
source_filename = "crypto_auth/hmacsha512256/auth_hmacsha512256.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_auth_hmacsha512_state = type { %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state }
%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_statebytes() #0 {
entry:
  ret i32 416
}

; Function Attrs: noinline nounwind optnone
define void @crypto_auth_hmacsha512256_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_init(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* nonnull %key, i32 %keylen) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %1 = load i8*, i8** %key.addr, align 4
  %2 = load i32, i32* %keylen.addr, align 4
  %call = call i32 @crypto_auth_hmacsha512_init(%struct.crypto_auth_hmacsha512_state* %0, i8* %1, i32 %2)
  ret i32 %call
}

declare i32 @crypto_auth_hmacsha512_init(%struct.crypto_auth_hmacsha512_state*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_update(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  %call = call i32 @crypto_auth_hmacsha512_update(%struct.crypto_auth_hmacsha512_state* %0, i8* %1, i64 %2)
  ret i32 %call
}

declare i32 @crypto_auth_hmacsha512_update(%struct.crypto_auth_hmacsha512_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_final(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* nonnull %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %out.addr = alloca i8*, align 4
  %out0 = alloca [64 x i8], align 16
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %out0, i32 0, i32 0
  %call = call i32 @crypto_auth_hmacsha512_final(%struct.crypto_auth_hmacsha512_state* %0, i8* %arraydecay)
  %1 = load i8*, i8** %out.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %out0, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 16 %arraydecay1, i32 32, i1 false)
  ret i32 0
}

declare i32 @crypto_auth_hmacsha512_final(%struct.crypto_auth_hmacsha512_state*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256(i8* nonnull %out, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_auth_hmacsha512_state, align 8
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_auth_hmacsha512256_init(%struct.crypto_auth_hmacsha512_state* %state, i8* %0, i32 32)
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  %call1 = call i32 @crypto_auth_hmacsha512256_update(%struct.crypto_auth_hmacsha512_state* %state, i8* %1, i64 %2)
  %3 = load i8*, i8** %out.addr, align 4
  %call2 = call i32 @crypto_auth_hmacsha512256_final(%struct.crypto_auth_hmacsha512_state* %state, i8* %3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512256_verify(i8* nonnull %h, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %h.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %correct = alloca [32 x i8], align 16
  store i8* %h, i8** %h.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %correct, i32 0, i32 0
  %0 = load i8*, i8** %in.addr, align 4
  %1 = load i64, i64* %inlen.addr, align 8
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_auth_hmacsha512256(i8* %arraydecay, i8* %0, i64 %1, i8* %2)
  %3 = load i8*, i8** %h.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %correct, i32 0, i32 0
  %call2 = call i32 @crypto_verify_32(i8* %3, i8* %arraydecay1)
  %4 = load i8*, i8** %h.addr, align 4
  %arraydecay3 = getelementptr inbounds [32 x i8], [32 x i8]* %correct, i32 0, i32 0
  %cmp = icmp eq i8* %4, %arraydecay3
  %conv = zext i1 %cmp to i32
  %sub = sub i32 0, %conv
  %or = or i32 %call2, %sub
  %arraydecay4 = getelementptr inbounds [32 x i8], [32 x i8]* %correct, i32 0, i32 0
  %5 = load i8*, i8** %h.addr, align 4
  %call5 = call i32 @sodium_memcmp(i8* %arraydecay4, i8* %5, i32 32)
  %or6 = or i32 %or, %call5
  ret i32 %or6
}

declare i32 @crypto_verify_32(i8*, i8*) #1

declare i32 @sodium_memcmp(i8*, i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
