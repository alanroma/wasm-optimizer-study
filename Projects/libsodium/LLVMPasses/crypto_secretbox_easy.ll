; ModuleID = 'crypto_secretbox/crypto_secretbox_easy.c'
source_filename = "crypto_secretbox/crypto_secretbox_easy.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_onetimeauth_poly1305_state = type { [256 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_detached(i8* nonnull %c, i8* nonnull %mac, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block0 = alloca [64 x i8], align 16
  %subkey = alloca [32 x i8], align 16
  %i = alloca i64, align 8
  %mlen0 = alloca i64, align 8
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %0 = load i8*, i8** %n.addr, align 4
  %1 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_core_hsalsa20(i8* %arraydecay, i8* %0, i8* %1, i8* null)
  %2 = load i8*, i8** %c.addr, align 4
  %3 = ptrtoint i8* %2 to i32
  %4 = load i8*, i8** %m.addr, align 4
  %5 = ptrtoint i8* %4 to i32
  %cmp = icmp ugt i32 %3, %5
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %entry
  %6 = load i8*, i8** %c.addr, align 4
  %7 = ptrtoint i8* %6 to i32
  %8 = load i8*, i8** %m.addr, align 4
  %9 = ptrtoint i8* %8 to i32
  %sub = sub i32 %7, %9
  %conv = zext i32 %sub to i64
  %10 = load i64, i64* %mlen.addr, align 8
  %cmp1 = icmp ult i64 %conv, %10
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %entry
  %11 = load i8*, i8** %m.addr, align 4
  %12 = ptrtoint i8* %11 to i32
  %13 = load i8*, i8** %c.addr, align 4
  %14 = ptrtoint i8* %13 to i32
  %cmp3 = icmp ugt i32 %12, %14
  br i1 %cmp3, label %land.lhs.true5, label %if.end

land.lhs.true5:                                   ; preds = %lor.lhs.false
  %15 = load i8*, i8** %m.addr, align 4
  %16 = ptrtoint i8* %15 to i32
  %17 = load i8*, i8** %c.addr, align 4
  %18 = ptrtoint i8* %17 to i32
  %sub6 = sub i32 %16, %18
  %conv7 = zext i32 %sub6 to i64
  %19 = load i64, i64* %mlen.addr, align 8
  %cmp8 = icmp ult i64 %conv7, %19
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true5, %land.lhs.true
  %20 = load i8*, i8** %c.addr, align 4
  %21 = load i8*, i8** %m.addr, align 4
  %22 = load i64, i64* %mlen.addr, align 8
  %conv10 = trunc i64 %22 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %20, i8* align 1 %21, i32 %conv10, i1 false)
  %23 = load i8*, i8** %c.addr, align 4
  store i8* %23, i8** %m.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true5, %lor.lhs.false
  %arraydecay11 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay11, i8 0, i32 32, i1 false)
  %24 = load i64, i64* %mlen.addr, align 8
  store i64 %24, i64* %mlen0, align 8
  %25 = load i64, i64* %mlen0, align 8
  %cmp12 = icmp ugt i64 %25, 32
  br i1 %cmp12, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end
  store i64 32, i64* %mlen0, align 8
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.end
  store i64 0, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end15
  %26 = load i64, i64* %i, align 8
  %27 = load i64, i64* %mlen0, align 8
  %cmp16 = icmp ult i64 %26, %27
  br i1 %cmp16, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %28 = load i8*, i8** %m.addr, align 4
  %29 = load i64, i64* %i, align 8
  %idxprom = trunc i64 %29 to i32
  %arrayidx = getelementptr i8, i8* %28, i32 %idxprom
  %30 = load i8, i8* %arrayidx, align 1
  %31 = load i64, i64* %i, align 8
  %add = add i64 %31, 32
  %idxprom18 = trunc i64 %add to i32
  %arrayidx19 = getelementptr [64 x i8], [64 x i8]* %block0, i32 0, i32 %idxprom18
  store i8 %30, i8* %arrayidx19, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %32 = load i64, i64* %i, align 8
  %inc = add i64 %32, 1
  store i64 %inc, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay20 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %arraydecay21 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %33 = load i64, i64* %mlen0, align 8
  %add22 = add i64 %33, 32
  %34 = load i8*, i8** %n.addr, align 4
  %add.ptr = getelementptr i8, i8* %34, i32 16
  %arraydecay23 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call24 = call i32 @crypto_stream_salsa20_xor(i8* %arraydecay20, i8* %arraydecay21, i64 %add22, i8* %add.ptr, i8* %arraydecay23)
  %arraydecay25 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call26 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay25)
  store i64 0, i64* %i, align 8
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc36, %for.end
  %35 = load i64, i64* %i, align 8
  %36 = load i64, i64* %mlen0, align 8
  %cmp28 = icmp ult i64 %35, %36
  br i1 %cmp28, label %for.body30, label %for.end38

for.body30:                                       ; preds = %for.cond27
  %37 = load i64, i64* %i, align 8
  %add31 = add i64 32, %37
  %idxprom32 = trunc i64 %add31 to i32
  %arrayidx33 = getelementptr [64 x i8], [64 x i8]* %block0, i32 0, i32 %idxprom32
  %38 = load i8, i8* %arrayidx33, align 1
  %39 = load i8*, i8** %c.addr, align 4
  %40 = load i64, i64* %i, align 8
  %idxprom34 = trunc i64 %40 to i32
  %arrayidx35 = getelementptr i8, i8* %39, i32 %idxprom34
  store i8 %38, i8* %arrayidx35, align 1
  br label %for.inc36

for.inc36:                                        ; preds = %for.body30
  %41 = load i64, i64* %i, align 8
  %inc37 = add i64 %41, 1
  store i64 %inc37, i64* %i, align 8
  br label %for.cond27

for.end38:                                        ; preds = %for.cond27
  %arraydecay39 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay39, i32 64)
  %42 = load i64, i64* %mlen.addr, align 8
  %43 = load i64, i64* %mlen0, align 8
  %cmp40 = icmp ugt i64 %42, %43
  br i1 %cmp40, label %if.then42, label %if.end50

if.then42:                                        ; preds = %for.end38
  %44 = load i8*, i8** %c.addr, align 4
  %45 = load i64, i64* %mlen0, align 8
  %idx.ext = trunc i64 %45 to i32
  %add.ptr43 = getelementptr i8, i8* %44, i32 %idx.ext
  %46 = load i8*, i8** %m.addr, align 4
  %47 = load i64, i64* %mlen0, align 8
  %idx.ext44 = trunc i64 %47 to i32
  %add.ptr45 = getelementptr i8, i8* %46, i32 %idx.ext44
  %48 = load i64, i64* %mlen.addr, align 8
  %49 = load i64, i64* %mlen0, align 8
  %sub46 = sub i64 %48, %49
  %50 = load i8*, i8** %n.addr, align 4
  %add.ptr47 = getelementptr i8, i8* %50, i32 16
  %arraydecay48 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call49 = call i32 @crypto_stream_salsa20_xor_ic(i8* %add.ptr43, i8* %add.ptr45, i64 %sub46, i8* %add.ptr47, i64 1, i8* %arraydecay48)
  br label %if.end50

if.end50:                                         ; preds = %if.then42, %for.end38
  %arraydecay51 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay51, i32 32)
  %51 = load i8*, i8** %c.addr, align 4
  %52 = load i64, i64* %mlen.addr, align 8
  %call52 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %51, i64 %52)
  %53 = load i8*, i8** %mac.addr, align 4
  %call53 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %53)
  %54 = bitcast %struct.crypto_onetimeauth_poly1305_state* %state to i8*
  call void @sodium_memzero(i8* %54, i32 256)
  ret i32 0
}

declare i32 @crypto_core_hsalsa20(i8*, i8*, i8*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare i32 @crypto_stream_salsa20_xor(i8*, i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

declare i32 @crypto_stream_salsa20_xor_ic(i8*, i8*, i64, i8*, i64, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state*, i8*, i64) #1

declare i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_easy(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967279
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 16
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i8*, i8** %m.addr, align 4
  %4 = load i64, i64* %mlen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_detached(i8* %add.ptr, i8* %2, i8* %3, i64 %4, i8* %5, i8* %6)
  ret i32 %call
}

; Function Attrs: noreturn
declare void @sodium_misuse() #4

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_open_detached(i8* %m, i8* nonnull %c, i8* nonnull %mac, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %block0 = alloca [64 x i8], align 16
  %subkey = alloca [32 x i8], align 16
  %i = alloca i64, align 8
  %mlen0 = alloca i64, align 8
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %0 = load i8*, i8** %n.addr, align 4
  %1 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_core_hsalsa20(i8* %arraydecay, i8* %0, i8* %1, i8* null)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %2 = load i8*, i8** %n.addr, align 4
  %add.ptr = getelementptr i8, i8* %2, i32 16
  %arraydecay2 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call3 = call i32 @crypto_stream_salsa20(i8* %arraydecay1, i64 32, i8* %add.ptr, i8* %arraydecay2)
  %3 = load i8*, i8** %mac.addr, align 4
  %4 = load i8*, i8** %c.addr, align 4
  %5 = load i64, i64* %clen.addr, align 8
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call5 = call i32 @crypto_onetimeauth_poly1305_verify(i8* %3, i8* %4, i64 %5, i8* %arraydecay4)
  %cmp = icmp ne i32 %call5, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %arraydecay6 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay6, i32 32)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i8*, i8** %m.addr, align 4
  %cmp7 = icmp eq i8* %6, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %7 = load i8*, i8** %c.addr, align 4
  %8 = ptrtoint i8* %7 to i32
  %9 = load i8*, i8** %m.addr, align 4
  %10 = ptrtoint i8* %9 to i32
  %cmp10 = icmp ugt i32 %8, %10
  br i1 %cmp10, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %if.end9
  %11 = load i8*, i8** %c.addr, align 4
  %12 = ptrtoint i8* %11 to i32
  %13 = load i8*, i8** %m.addr, align 4
  %14 = ptrtoint i8* %13 to i32
  %sub = sub i32 %12, %14
  %conv = zext i32 %sub to i64
  %15 = load i64, i64* %clen.addr, align 8
  %cmp11 = icmp ult i64 %conv, %15
  br i1 %cmp11, label %if.then20, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %if.end9
  %16 = load i8*, i8** %m.addr, align 4
  %17 = ptrtoint i8* %16 to i32
  %18 = load i8*, i8** %c.addr, align 4
  %19 = ptrtoint i8* %18 to i32
  %cmp13 = icmp ugt i32 %17, %19
  br i1 %cmp13, label %land.lhs.true15, label %if.end22

land.lhs.true15:                                  ; preds = %lor.lhs.false
  %20 = load i8*, i8** %m.addr, align 4
  %21 = ptrtoint i8* %20 to i32
  %22 = load i8*, i8** %c.addr, align 4
  %23 = ptrtoint i8* %22 to i32
  %sub16 = sub i32 %21, %23
  %conv17 = zext i32 %sub16 to i64
  %24 = load i64, i64* %clen.addr, align 8
  %cmp18 = icmp ult i64 %conv17, %24
  br i1 %cmp18, label %if.then20, label %if.end22

if.then20:                                        ; preds = %land.lhs.true15, %land.lhs.true
  %25 = load i8*, i8** %m.addr, align 4
  %26 = load i8*, i8** %c.addr, align 4
  %27 = load i64, i64* %clen.addr, align 8
  %conv21 = trunc i64 %27 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %25, i8* align 1 %26, i32 %conv21, i1 false)
  %28 = load i8*, i8** %m.addr, align 4
  store i8* %28, i8** %c.addr, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %land.lhs.true15, %lor.lhs.false
  %29 = load i64, i64* %clen.addr, align 8
  store i64 %29, i64* %mlen0, align 8
  %30 = load i64, i64* %mlen0, align 8
  %cmp23 = icmp ugt i64 %30, 32
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end22
  store i64 32, i64* %mlen0, align 8
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end22
  store i64 0, i64* %i, align 8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end26
  %31 = load i64, i64* %i, align 8
  %32 = load i64, i64* %mlen0, align 8
  %cmp27 = icmp ult i64 %31, %32
  br i1 %cmp27, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %33 = load i8*, i8** %c.addr, align 4
  %34 = load i64, i64* %i, align 8
  %idxprom = trunc i64 %34 to i32
  %arrayidx = getelementptr i8, i8* %33, i32 %idxprom
  %35 = load i8, i8* %arrayidx, align 1
  %36 = load i64, i64* %i, align 8
  %add = add i64 32, %36
  %idxprom29 = trunc i64 %add to i32
  %arrayidx30 = getelementptr [64 x i8], [64 x i8]* %block0, i32 0, i32 %idxprom29
  store i8 %35, i8* %arrayidx30, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i64, i64* %i, align 8
  %inc = add i64 %37, 1
  store i64 %inc, i64* %i, align 8
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay31 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %38 = load i64, i64* %mlen0, align 8
  %add33 = add i64 32, %38
  %39 = load i8*, i8** %n.addr, align 4
  %add.ptr34 = getelementptr i8, i8* %39, i32 16
  %arraydecay35 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call36 = call i32 @crypto_stream_salsa20_xor(i8* %arraydecay31, i8* %arraydecay32, i64 %add33, i8* %add.ptr34, i8* %arraydecay35)
  store i64 0, i64* %i, align 8
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc46, %for.end
  %40 = load i64, i64* %i, align 8
  %41 = load i64, i64* %mlen0, align 8
  %cmp38 = icmp ult i64 %40, %41
  br i1 %cmp38, label %for.body40, label %for.end48

for.body40:                                       ; preds = %for.cond37
  %42 = load i64, i64* %i, align 8
  %add41 = add i64 %42, 32
  %idxprom42 = trunc i64 %add41 to i32
  %arrayidx43 = getelementptr [64 x i8], [64 x i8]* %block0, i32 0, i32 %idxprom42
  %43 = load i8, i8* %arrayidx43, align 1
  %44 = load i8*, i8** %m.addr, align 4
  %45 = load i64, i64* %i, align 8
  %idxprom44 = trunc i64 %45 to i32
  %arrayidx45 = getelementptr i8, i8* %44, i32 %idxprom44
  store i8 %43, i8* %arrayidx45, align 1
  br label %for.inc46

for.inc46:                                        ; preds = %for.body40
  %46 = load i64, i64* %i, align 8
  %inc47 = add i64 %46, 1
  store i64 %inc47, i64* %i, align 8
  br label %for.cond37

for.end48:                                        ; preds = %for.cond37
  %47 = load i64, i64* %clen.addr, align 8
  %48 = load i64, i64* %mlen0, align 8
  %cmp49 = icmp ugt i64 %47, %48
  br i1 %cmp49, label %if.then51, label %if.end59

if.then51:                                        ; preds = %for.end48
  %49 = load i8*, i8** %m.addr, align 4
  %50 = load i64, i64* %mlen0, align 8
  %idx.ext = trunc i64 %50 to i32
  %add.ptr52 = getelementptr i8, i8* %49, i32 %idx.ext
  %51 = load i8*, i8** %c.addr, align 4
  %52 = load i64, i64* %mlen0, align 8
  %idx.ext53 = trunc i64 %52 to i32
  %add.ptr54 = getelementptr i8, i8* %51, i32 %idx.ext53
  %53 = load i64, i64* %clen.addr, align 8
  %54 = load i64, i64* %mlen0, align 8
  %sub55 = sub i64 %53, %54
  %55 = load i8*, i8** %n.addr, align 4
  %add.ptr56 = getelementptr i8, i8* %55, i32 16
  %arraydecay57 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call58 = call i32 @crypto_stream_salsa20_xor_ic(i8* %add.ptr52, i8* %add.ptr54, i64 %sub55, i8* %add.ptr56, i64 1, i8* %arraydecay57)
  br label %if.end59

if.end59:                                         ; preds = %if.then51, %for.end48
  %arraydecay60 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay60, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end59, %if.then8, %if.then
  %56 = load i32, i32* %retval, align 4
  ret i32 %56
}

declare i32 @crypto_stream_salsa20(i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_verify(i8*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_open_easy(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ult i64 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %2, i32 16
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %4, 16
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_secretbox_open_detached(i8* %1, i8* %add.ptr, i8* %3, i64 %sub, i8* %5, i8* %6)
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
