; ModuleID = 'crypto_generichash/blake2b/ref/blake2b-compress-ref.c'
source_filename = "crypto_generichash/blake2b/ref/blake2b-compress-ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.blake2b_state = type <{ [8 x i64], [2 x i64], [2 x i64], [256 x i8], i32, i8 }>

@blake2b_IV = internal constant [8 x i64] [i64 7640891576956012808, i64 -4942790177534073029, i64 4354685564936845355, i64 -6534734903238641935, i64 5840696475078001361, i64 -7276294671716946913, i64 2270897969802886507, i64 6620516959819538809], align 64
@blake2b_sigma = internal constant [12 x [16 x i8]] [[16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F", [16 x i8] c"\0E\0A\04\08\09\0F\0D\06\01\0C\00\02\0B\07\05\03", [16 x i8] c"\0B\08\0C\00\05\02\0F\0D\0A\0E\03\06\07\01\09\04", [16 x i8] c"\07\09\03\01\0D\0C\0B\0E\02\06\05\0A\04\00\0F\08", [16 x i8] c"\09\00\05\07\02\04\0A\0F\0E\01\0B\0C\06\08\03\0D", [16 x i8] c"\02\0C\06\0A\00\0B\08\03\04\0D\07\05\0F\0E\01\09", [16 x i8] c"\0C\05\01\0F\0E\0D\04\0A\00\07\06\03\09\02\08\0B", [16 x i8] c"\0D\0B\07\0E\0C\01\03\09\05\00\0F\04\08\06\02\0A", [16 x i8] c"\06\0F\0E\09\0B\03\00\08\0C\02\0D\07\01\04\0A\05", [16 x i8] c"\0A\02\08\04\07\06\01\05\0F\0B\09\0E\03\0C\0D\00", [16 x i8] c"\00\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F", [16 x i8] c"\0E\0A\04\08\09\0F\0D\06\01\0C\00\02\0B\07\05\03"], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @blake2b_compress_ref(%struct.blake2b_state* %S, i8* %block) #0 {
entry:
  %S.addr = alloca %struct.blake2b_state*, align 4
  %block.addr = alloca i8*, align 4
  %m = alloca [16 x i64], align 16
  %v = alloca [16 x i64], align 16
  %i = alloca i32, align 4
  store %struct.blake2b_state* %S, %struct.blake2b_state** %S.addr, align 4
  store i8* %block, i8** %block.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %block.addr, align 4
  %2 = load i32, i32* %i, align 4
  %mul = mul i32 %2, 8
  %add.ptr = getelementptr i8, i8* %1, i32 %mul
  %call = call i64 @load64_le(i8* %add.ptr)
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %3
  store i64 %call, i64* %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc6, %for.end
  %5 = load i32, i32* %i, align 4
  %cmp2 = icmp slt i32 %5, 8
  br i1 %cmp2, label %for.body3, label %for.end8

for.body3:                                        ; preds = %for.cond1
  %6 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr [8 x i64], [8 x i64]* %h, i32 0, i32 %7
  %8 = load i64, i64* %arrayidx4, align 1
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 %9
  store i64 %8, i64* %arrayidx5, align 8
  br label %for.inc6

for.inc6:                                         ; preds = %for.body3
  %10 = load i32, i32* %i, align 4
  %inc7 = add i32 %10, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond1

for.end8:                                         ; preds = %for.cond1
  %11 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 0), align 64
  %arrayidx9 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  store i64 %11, i64* %arrayidx9, align 16
  %12 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 1), align 8
  %arrayidx10 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  store i64 %12, i64* %arrayidx10, align 8
  %13 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 2), align 16
  %arrayidx11 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  store i64 %13, i64* %arrayidx11, align 16
  %14 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 3), align 8
  %arrayidx12 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  store i64 %14, i64* %arrayidx12, align 8
  %15 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %15, i32 0, i32 1
  %arrayidx13 = getelementptr [2 x i64], [2 x i64]* %t, i32 0, i32 0
  %16 = load i64, i64* %arrayidx13, align 1
  %17 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 4), align 32
  %xor = xor i64 %16, %17
  %arrayidx14 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %xor, i64* %arrayidx14, align 16
  %18 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %t15 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %18, i32 0, i32 1
  %arrayidx16 = getelementptr [2 x i64], [2 x i64]* %t15, i32 0, i32 1
  %19 = load i64, i64* %arrayidx16, align 1
  %20 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 5), align 8
  %xor17 = xor i64 %19, %20
  %arrayidx18 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %xor17, i64* %arrayidx18, align 8
  %21 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %f = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %21, i32 0, i32 2
  %arrayidx19 = getelementptr [2 x i64], [2 x i64]* %f, i32 0, i32 0
  %22 = load i64, i64* %arrayidx19, align 1
  %23 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 6), align 16
  %xor20 = xor i64 %22, %23
  %arrayidx21 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %xor20, i64* %arrayidx21, align 16
  %24 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %f22 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %24, i32 0, i32 2
  %arrayidx23 = getelementptr [2 x i64], [2 x i64]* %f22, i32 0, i32 1
  %25 = load i64, i64* %arrayidx23, align 1
  %26 = load i64, i64* getelementptr inbounds ([8 x i64], [8 x i64]* @blake2b_IV, i32 0, i32 7), align 8
  %xor24 = xor i64 %25, %26
  %arrayidx25 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %xor24, i64* %arrayidx25, align 8
  br label %do.body

do.body:                                          ; preds = %for.end8
  br label %do.body26

do.body26:                                        ; preds = %do.body
  %arrayidx27 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %27 = load i64, i64* %arrayidx27, align 16
  %28 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 0), align 16
  %idxprom = zext i8 %28 to i32
  %arrayidx28 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom
  %29 = load i64, i64* %arrayidx28, align 8
  %add = add i64 %27, %29
  %arrayidx29 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %30 = load i64, i64* %arrayidx29, align 16
  %add30 = add i64 %30, %add
  store i64 %add30, i64* %arrayidx29, align 16
  %arrayidx31 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %31 = load i64, i64* %arrayidx31, align 16
  %arrayidx32 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %32 = load i64, i64* %arrayidx32, align 16
  %xor33 = xor i64 %31, %32
  %call34 = call i64 @rotr64(i64 %xor33, i32 32)
  %arrayidx35 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call34, i64* %arrayidx35, align 16
  %arrayidx36 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %33 = load i64, i64* %arrayidx36, align 16
  %arrayidx37 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %34 = load i64, i64* %arrayidx37, align 16
  %add38 = add i64 %34, %33
  store i64 %add38, i64* %arrayidx37, align 16
  %arrayidx39 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %35 = load i64, i64* %arrayidx39, align 16
  %arrayidx40 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %36 = load i64, i64* %arrayidx40, align 16
  %xor41 = xor i64 %35, %36
  %call42 = call i64 @rotr64(i64 %xor41, i32 24)
  %arrayidx43 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call42, i64* %arrayidx43, align 16
  %arrayidx44 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %37 = load i64, i64* %arrayidx44, align 16
  %38 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 1), align 1
  %idxprom45 = zext i8 %38 to i32
  %arrayidx46 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom45
  %39 = load i64, i64* %arrayidx46, align 8
  %add47 = add i64 %37, %39
  %arrayidx48 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %40 = load i64, i64* %arrayidx48, align 16
  %add49 = add i64 %40, %add47
  store i64 %add49, i64* %arrayidx48, align 16
  %arrayidx50 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %41 = load i64, i64* %arrayidx50, align 16
  %arrayidx51 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %42 = load i64, i64* %arrayidx51, align 16
  %xor52 = xor i64 %41, %42
  %call53 = call i64 @rotr64(i64 %xor52, i32 16)
  %arrayidx54 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call53, i64* %arrayidx54, align 16
  %arrayidx55 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %43 = load i64, i64* %arrayidx55, align 16
  %arrayidx56 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %44 = load i64, i64* %arrayidx56, align 16
  %add57 = add i64 %44, %43
  store i64 %add57, i64* %arrayidx56, align 16
  %arrayidx58 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %45 = load i64, i64* %arrayidx58, align 16
  %arrayidx59 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %46 = load i64, i64* %arrayidx59, align 16
  %xor60 = xor i64 %45, %46
  %call61 = call i64 @rotr64(i64 %xor60, i32 63)
  %arrayidx62 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call61, i64* %arrayidx62, align 16
  br label %do.end

do.end:                                           ; preds = %do.body26
  br label %do.body63

do.body63:                                        ; preds = %do.end
  %arrayidx64 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %47 = load i64, i64* %arrayidx64, align 8
  %48 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 2), align 2
  %idxprom65 = zext i8 %48 to i32
  %arrayidx66 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom65
  %49 = load i64, i64* %arrayidx66, align 8
  %add67 = add i64 %47, %49
  %arrayidx68 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %50 = load i64, i64* %arrayidx68, align 8
  %add69 = add i64 %50, %add67
  store i64 %add69, i64* %arrayidx68, align 8
  %arrayidx70 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %51 = load i64, i64* %arrayidx70, align 8
  %arrayidx71 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %52 = load i64, i64* %arrayidx71, align 8
  %xor72 = xor i64 %51, %52
  %call73 = call i64 @rotr64(i64 %xor72, i32 32)
  %arrayidx74 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call73, i64* %arrayidx74, align 8
  %arrayidx75 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %53 = load i64, i64* %arrayidx75, align 8
  %arrayidx76 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %54 = load i64, i64* %arrayidx76, align 8
  %add77 = add i64 %54, %53
  store i64 %add77, i64* %arrayidx76, align 8
  %arrayidx78 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %55 = load i64, i64* %arrayidx78, align 8
  %arrayidx79 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %56 = load i64, i64* %arrayidx79, align 8
  %xor80 = xor i64 %55, %56
  %call81 = call i64 @rotr64(i64 %xor80, i32 24)
  %arrayidx82 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call81, i64* %arrayidx82, align 8
  %arrayidx83 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %57 = load i64, i64* %arrayidx83, align 8
  %58 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 3), align 1
  %idxprom84 = zext i8 %58 to i32
  %arrayidx85 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom84
  %59 = load i64, i64* %arrayidx85, align 8
  %add86 = add i64 %57, %59
  %arrayidx87 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %60 = load i64, i64* %arrayidx87, align 8
  %add88 = add i64 %60, %add86
  store i64 %add88, i64* %arrayidx87, align 8
  %arrayidx89 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %61 = load i64, i64* %arrayidx89, align 8
  %arrayidx90 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %62 = load i64, i64* %arrayidx90, align 8
  %xor91 = xor i64 %61, %62
  %call92 = call i64 @rotr64(i64 %xor91, i32 16)
  %arrayidx93 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call92, i64* %arrayidx93, align 8
  %arrayidx94 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %63 = load i64, i64* %arrayidx94, align 8
  %arrayidx95 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %64 = load i64, i64* %arrayidx95, align 8
  %add96 = add i64 %64, %63
  store i64 %add96, i64* %arrayidx95, align 8
  %arrayidx97 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %65 = load i64, i64* %arrayidx97, align 8
  %arrayidx98 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %66 = load i64, i64* %arrayidx98, align 8
  %xor99 = xor i64 %65, %66
  %call100 = call i64 @rotr64(i64 %xor99, i32 63)
  %arrayidx101 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call100, i64* %arrayidx101, align 8
  br label %do.end102

do.end102:                                        ; preds = %do.body63
  br label %do.body103

do.body103:                                       ; preds = %do.end102
  %arrayidx104 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %67 = load i64, i64* %arrayidx104, align 16
  %68 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 4), align 4
  %idxprom105 = zext i8 %68 to i32
  %arrayidx106 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom105
  %69 = load i64, i64* %arrayidx106, align 8
  %add107 = add i64 %67, %69
  %arrayidx108 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %70 = load i64, i64* %arrayidx108, align 16
  %add109 = add i64 %70, %add107
  store i64 %add109, i64* %arrayidx108, align 16
  %arrayidx110 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %71 = load i64, i64* %arrayidx110, align 16
  %arrayidx111 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %72 = load i64, i64* %arrayidx111, align 16
  %xor112 = xor i64 %71, %72
  %call113 = call i64 @rotr64(i64 %xor112, i32 32)
  %arrayidx114 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call113, i64* %arrayidx114, align 16
  %arrayidx115 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %73 = load i64, i64* %arrayidx115, align 16
  %arrayidx116 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %74 = load i64, i64* %arrayidx116, align 16
  %add117 = add i64 %74, %73
  store i64 %add117, i64* %arrayidx116, align 16
  %arrayidx118 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %75 = load i64, i64* %arrayidx118, align 16
  %arrayidx119 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %76 = load i64, i64* %arrayidx119, align 16
  %xor120 = xor i64 %75, %76
  %call121 = call i64 @rotr64(i64 %xor120, i32 24)
  %arrayidx122 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call121, i64* %arrayidx122, align 16
  %arrayidx123 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %77 = load i64, i64* %arrayidx123, align 16
  %78 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 5), align 1
  %idxprom124 = zext i8 %78 to i32
  %arrayidx125 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom124
  %79 = load i64, i64* %arrayidx125, align 8
  %add126 = add i64 %77, %79
  %arrayidx127 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %80 = load i64, i64* %arrayidx127, align 16
  %add128 = add i64 %80, %add126
  store i64 %add128, i64* %arrayidx127, align 16
  %arrayidx129 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %81 = load i64, i64* %arrayidx129, align 16
  %arrayidx130 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %82 = load i64, i64* %arrayidx130, align 16
  %xor131 = xor i64 %81, %82
  %call132 = call i64 @rotr64(i64 %xor131, i32 16)
  %arrayidx133 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call132, i64* %arrayidx133, align 16
  %arrayidx134 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %83 = load i64, i64* %arrayidx134, align 16
  %arrayidx135 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %84 = load i64, i64* %arrayidx135, align 16
  %add136 = add i64 %84, %83
  store i64 %add136, i64* %arrayidx135, align 16
  %arrayidx137 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %85 = load i64, i64* %arrayidx137, align 16
  %arrayidx138 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %86 = load i64, i64* %arrayidx138, align 16
  %xor139 = xor i64 %85, %86
  %call140 = call i64 @rotr64(i64 %xor139, i32 63)
  %arrayidx141 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call140, i64* %arrayidx141, align 16
  br label %do.end142

do.end142:                                        ; preds = %do.body103
  br label %do.body143

do.body143:                                       ; preds = %do.end142
  %arrayidx144 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %87 = load i64, i64* %arrayidx144, align 8
  %88 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 6), align 2
  %idxprom145 = zext i8 %88 to i32
  %arrayidx146 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom145
  %89 = load i64, i64* %arrayidx146, align 8
  %add147 = add i64 %87, %89
  %arrayidx148 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %90 = load i64, i64* %arrayidx148, align 8
  %add149 = add i64 %90, %add147
  store i64 %add149, i64* %arrayidx148, align 8
  %arrayidx150 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %91 = load i64, i64* %arrayidx150, align 8
  %arrayidx151 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %92 = load i64, i64* %arrayidx151, align 8
  %xor152 = xor i64 %91, %92
  %call153 = call i64 @rotr64(i64 %xor152, i32 32)
  %arrayidx154 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call153, i64* %arrayidx154, align 8
  %arrayidx155 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %93 = load i64, i64* %arrayidx155, align 8
  %arrayidx156 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %94 = load i64, i64* %arrayidx156, align 8
  %add157 = add i64 %94, %93
  store i64 %add157, i64* %arrayidx156, align 8
  %arrayidx158 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %95 = load i64, i64* %arrayidx158, align 8
  %arrayidx159 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %96 = load i64, i64* %arrayidx159, align 8
  %xor160 = xor i64 %95, %96
  %call161 = call i64 @rotr64(i64 %xor160, i32 24)
  %arrayidx162 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call161, i64* %arrayidx162, align 8
  %arrayidx163 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %97 = load i64, i64* %arrayidx163, align 8
  %98 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 7), align 1
  %idxprom164 = zext i8 %98 to i32
  %arrayidx165 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom164
  %99 = load i64, i64* %arrayidx165, align 8
  %add166 = add i64 %97, %99
  %arrayidx167 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %100 = load i64, i64* %arrayidx167, align 8
  %add168 = add i64 %100, %add166
  store i64 %add168, i64* %arrayidx167, align 8
  %arrayidx169 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %101 = load i64, i64* %arrayidx169, align 8
  %arrayidx170 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %102 = load i64, i64* %arrayidx170, align 8
  %xor171 = xor i64 %101, %102
  %call172 = call i64 @rotr64(i64 %xor171, i32 16)
  %arrayidx173 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call172, i64* %arrayidx173, align 8
  %arrayidx174 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %103 = load i64, i64* %arrayidx174, align 8
  %arrayidx175 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %104 = load i64, i64* %arrayidx175, align 8
  %add176 = add i64 %104, %103
  store i64 %add176, i64* %arrayidx175, align 8
  %arrayidx177 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %105 = load i64, i64* %arrayidx177, align 8
  %arrayidx178 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %106 = load i64, i64* %arrayidx178, align 8
  %xor179 = xor i64 %105, %106
  %call180 = call i64 @rotr64(i64 %xor179, i32 63)
  %arrayidx181 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call180, i64* %arrayidx181, align 8
  br label %do.end182

do.end182:                                        ; preds = %do.body143
  br label %do.body183

do.body183:                                       ; preds = %do.end182
  %arrayidx184 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %107 = load i64, i64* %arrayidx184, align 8
  %108 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 8), align 8
  %idxprom185 = zext i8 %108 to i32
  %arrayidx186 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom185
  %109 = load i64, i64* %arrayidx186, align 8
  %add187 = add i64 %107, %109
  %arrayidx188 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %110 = load i64, i64* %arrayidx188, align 16
  %add189 = add i64 %110, %add187
  store i64 %add189, i64* %arrayidx188, align 16
  %arrayidx190 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %111 = load i64, i64* %arrayidx190, align 8
  %arrayidx191 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %112 = load i64, i64* %arrayidx191, align 16
  %xor192 = xor i64 %111, %112
  %call193 = call i64 @rotr64(i64 %xor192, i32 32)
  %arrayidx194 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call193, i64* %arrayidx194, align 8
  %arrayidx195 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %113 = load i64, i64* %arrayidx195, align 8
  %arrayidx196 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %114 = load i64, i64* %arrayidx196, align 16
  %add197 = add i64 %114, %113
  store i64 %add197, i64* %arrayidx196, align 16
  %arrayidx198 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %115 = load i64, i64* %arrayidx198, align 8
  %arrayidx199 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %116 = load i64, i64* %arrayidx199, align 16
  %xor200 = xor i64 %115, %116
  %call201 = call i64 @rotr64(i64 %xor200, i32 24)
  %arrayidx202 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call201, i64* %arrayidx202, align 8
  %arrayidx203 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %117 = load i64, i64* %arrayidx203, align 8
  %118 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 9), align 1
  %idxprom204 = zext i8 %118 to i32
  %arrayidx205 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom204
  %119 = load i64, i64* %arrayidx205, align 8
  %add206 = add i64 %117, %119
  %arrayidx207 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %120 = load i64, i64* %arrayidx207, align 16
  %add208 = add i64 %120, %add206
  store i64 %add208, i64* %arrayidx207, align 16
  %arrayidx209 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %121 = load i64, i64* %arrayidx209, align 8
  %arrayidx210 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %122 = load i64, i64* %arrayidx210, align 16
  %xor211 = xor i64 %121, %122
  %call212 = call i64 @rotr64(i64 %xor211, i32 16)
  %arrayidx213 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call212, i64* %arrayidx213, align 8
  %arrayidx214 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %123 = load i64, i64* %arrayidx214, align 8
  %arrayidx215 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %124 = load i64, i64* %arrayidx215, align 16
  %add216 = add i64 %124, %123
  store i64 %add216, i64* %arrayidx215, align 16
  %arrayidx217 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %125 = load i64, i64* %arrayidx217, align 8
  %arrayidx218 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %126 = load i64, i64* %arrayidx218, align 16
  %xor219 = xor i64 %125, %126
  %call220 = call i64 @rotr64(i64 %xor219, i32 63)
  %arrayidx221 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call220, i64* %arrayidx221, align 8
  br label %do.end222

do.end222:                                        ; preds = %do.body183
  br label %do.body223

do.body223:                                       ; preds = %do.end222
  %arrayidx224 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %127 = load i64, i64* %arrayidx224, align 16
  %128 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 10), align 2
  %idxprom225 = zext i8 %128 to i32
  %arrayidx226 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom225
  %129 = load i64, i64* %arrayidx226, align 8
  %add227 = add i64 %127, %129
  %arrayidx228 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %130 = load i64, i64* %arrayidx228, align 8
  %add229 = add i64 %130, %add227
  store i64 %add229, i64* %arrayidx228, align 8
  %arrayidx230 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %131 = load i64, i64* %arrayidx230, align 16
  %arrayidx231 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %132 = load i64, i64* %arrayidx231, align 8
  %xor232 = xor i64 %131, %132
  %call233 = call i64 @rotr64(i64 %xor232, i32 32)
  %arrayidx234 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call233, i64* %arrayidx234, align 16
  %arrayidx235 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %133 = load i64, i64* %arrayidx235, align 16
  %arrayidx236 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %134 = load i64, i64* %arrayidx236, align 8
  %add237 = add i64 %134, %133
  store i64 %add237, i64* %arrayidx236, align 8
  %arrayidx238 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %135 = load i64, i64* %arrayidx238, align 16
  %arrayidx239 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %136 = load i64, i64* %arrayidx239, align 8
  %xor240 = xor i64 %135, %136
  %call241 = call i64 @rotr64(i64 %xor240, i32 24)
  %arrayidx242 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call241, i64* %arrayidx242, align 16
  %arrayidx243 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %137 = load i64, i64* %arrayidx243, align 16
  %138 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 11), align 1
  %idxprom244 = zext i8 %138 to i32
  %arrayidx245 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom244
  %139 = load i64, i64* %arrayidx245, align 8
  %add246 = add i64 %137, %139
  %arrayidx247 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %140 = load i64, i64* %arrayidx247, align 8
  %add248 = add i64 %140, %add246
  store i64 %add248, i64* %arrayidx247, align 8
  %arrayidx249 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %141 = load i64, i64* %arrayidx249, align 16
  %arrayidx250 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %142 = load i64, i64* %arrayidx250, align 8
  %xor251 = xor i64 %141, %142
  %call252 = call i64 @rotr64(i64 %xor251, i32 16)
  %arrayidx253 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call252, i64* %arrayidx253, align 16
  %arrayidx254 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %143 = load i64, i64* %arrayidx254, align 16
  %arrayidx255 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %144 = load i64, i64* %arrayidx255, align 8
  %add256 = add i64 %144, %143
  store i64 %add256, i64* %arrayidx255, align 8
  %arrayidx257 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %145 = load i64, i64* %arrayidx257, align 16
  %arrayidx258 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %146 = load i64, i64* %arrayidx258, align 8
  %xor259 = xor i64 %145, %146
  %call260 = call i64 @rotr64(i64 %xor259, i32 63)
  %arrayidx261 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call260, i64* %arrayidx261, align 16
  br label %do.end262

do.end262:                                        ; preds = %do.body223
  br label %do.body263

do.body263:                                       ; preds = %do.end262
  %arrayidx264 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %147 = load i64, i64* %arrayidx264, align 8
  %148 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 12), align 4
  %idxprom265 = zext i8 %148 to i32
  %arrayidx266 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom265
  %149 = load i64, i64* %arrayidx266, align 8
  %add267 = add i64 %147, %149
  %arrayidx268 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %150 = load i64, i64* %arrayidx268, align 16
  %add269 = add i64 %150, %add267
  store i64 %add269, i64* %arrayidx268, align 16
  %arrayidx270 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %151 = load i64, i64* %arrayidx270, align 8
  %arrayidx271 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %152 = load i64, i64* %arrayidx271, align 16
  %xor272 = xor i64 %151, %152
  %call273 = call i64 @rotr64(i64 %xor272, i32 32)
  %arrayidx274 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call273, i64* %arrayidx274, align 8
  %arrayidx275 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %153 = load i64, i64* %arrayidx275, align 8
  %arrayidx276 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %154 = load i64, i64* %arrayidx276, align 16
  %add277 = add i64 %154, %153
  store i64 %add277, i64* %arrayidx276, align 16
  %arrayidx278 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %155 = load i64, i64* %arrayidx278, align 8
  %arrayidx279 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %156 = load i64, i64* %arrayidx279, align 16
  %xor280 = xor i64 %155, %156
  %call281 = call i64 @rotr64(i64 %xor280, i32 24)
  %arrayidx282 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call281, i64* %arrayidx282, align 8
  %arrayidx283 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %157 = load i64, i64* %arrayidx283, align 8
  %158 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 13), align 1
  %idxprom284 = zext i8 %158 to i32
  %arrayidx285 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom284
  %159 = load i64, i64* %arrayidx285, align 8
  %add286 = add i64 %157, %159
  %arrayidx287 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %160 = load i64, i64* %arrayidx287, align 16
  %add288 = add i64 %160, %add286
  store i64 %add288, i64* %arrayidx287, align 16
  %arrayidx289 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %161 = load i64, i64* %arrayidx289, align 8
  %arrayidx290 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %162 = load i64, i64* %arrayidx290, align 16
  %xor291 = xor i64 %161, %162
  %call292 = call i64 @rotr64(i64 %xor291, i32 16)
  %arrayidx293 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call292, i64* %arrayidx293, align 8
  %arrayidx294 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %163 = load i64, i64* %arrayidx294, align 8
  %arrayidx295 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %164 = load i64, i64* %arrayidx295, align 16
  %add296 = add i64 %164, %163
  store i64 %add296, i64* %arrayidx295, align 16
  %arrayidx297 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %165 = load i64, i64* %arrayidx297, align 8
  %arrayidx298 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %166 = load i64, i64* %arrayidx298, align 16
  %xor299 = xor i64 %165, %166
  %call300 = call i64 @rotr64(i64 %xor299, i32 63)
  %arrayidx301 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call300, i64* %arrayidx301, align 8
  br label %do.end302

do.end302:                                        ; preds = %do.body263
  br label %do.body303

do.body303:                                       ; preds = %do.end302
  %arrayidx304 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %167 = load i64, i64* %arrayidx304, align 16
  %168 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 14), align 2
  %idxprom305 = zext i8 %168 to i32
  %arrayidx306 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom305
  %169 = load i64, i64* %arrayidx306, align 8
  %add307 = add i64 %167, %169
  %arrayidx308 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %170 = load i64, i64* %arrayidx308, align 8
  %add309 = add i64 %170, %add307
  store i64 %add309, i64* %arrayidx308, align 8
  %arrayidx310 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %171 = load i64, i64* %arrayidx310, align 16
  %arrayidx311 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %172 = load i64, i64* %arrayidx311, align 8
  %xor312 = xor i64 %171, %172
  %call313 = call i64 @rotr64(i64 %xor312, i32 32)
  %arrayidx314 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call313, i64* %arrayidx314, align 16
  %arrayidx315 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %173 = load i64, i64* %arrayidx315, align 16
  %arrayidx316 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %174 = load i64, i64* %arrayidx316, align 8
  %add317 = add i64 %174, %173
  store i64 %add317, i64* %arrayidx316, align 8
  %arrayidx318 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %175 = load i64, i64* %arrayidx318, align 16
  %arrayidx319 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %176 = load i64, i64* %arrayidx319, align 8
  %xor320 = xor i64 %175, %176
  %call321 = call i64 @rotr64(i64 %xor320, i32 24)
  %arrayidx322 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call321, i64* %arrayidx322, align 16
  %arrayidx323 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %177 = load i64, i64* %arrayidx323, align 16
  %178 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 0, i32 15), align 1
  %idxprom324 = zext i8 %178 to i32
  %arrayidx325 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom324
  %179 = load i64, i64* %arrayidx325, align 8
  %add326 = add i64 %177, %179
  %arrayidx327 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %180 = load i64, i64* %arrayidx327, align 8
  %add328 = add i64 %180, %add326
  store i64 %add328, i64* %arrayidx327, align 8
  %arrayidx329 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %181 = load i64, i64* %arrayidx329, align 16
  %arrayidx330 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %182 = load i64, i64* %arrayidx330, align 8
  %xor331 = xor i64 %181, %182
  %call332 = call i64 @rotr64(i64 %xor331, i32 16)
  %arrayidx333 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call332, i64* %arrayidx333, align 16
  %arrayidx334 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %183 = load i64, i64* %arrayidx334, align 16
  %arrayidx335 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %184 = load i64, i64* %arrayidx335, align 8
  %add336 = add i64 %184, %183
  store i64 %add336, i64* %arrayidx335, align 8
  %arrayidx337 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %185 = load i64, i64* %arrayidx337, align 16
  %arrayidx338 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %186 = load i64, i64* %arrayidx338, align 8
  %xor339 = xor i64 %185, %186
  %call340 = call i64 @rotr64(i64 %xor339, i32 63)
  %arrayidx341 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call340, i64* %arrayidx341, align 16
  br label %do.end342

do.end342:                                        ; preds = %do.body303
  br label %do.end343

do.end343:                                        ; preds = %do.end342
  br label %do.body344

do.body344:                                       ; preds = %do.end343
  br label %do.body345

do.body345:                                       ; preds = %do.body344
  %arrayidx346 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %187 = load i64, i64* %arrayidx346, align 16
  %188 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 0), align 16
  %idxprom347 = zext i8 %188 to i32
  %arrayidx348 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom347
  %189 = load i64, i64* %arrayidx348, align 8
  %add349 = add i64 %187, %189
  %arrayidx350 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %190 = load i64, i64* %arrayidx350, align 16
  %add351 = add i64 %190, %add349
  store i64 %add351, i64* %arrayidx350, align 16
  %arrayidx352 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %191 = load i64, i64* %arrayidx352, align 16
  %arrayidx353 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %192 = load i64, i64* %arrayidx353, align 16
  %xor354 = xor i64 %191, %192
  %call355 = call i64 @rotr64(i64 %xor354, i32 32)
  %arrayidx356 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call355, i64* %arrayidx356, align 16
  %arrayidx357 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %193 = load i64, i64* %arrayidx357, align 16
  %arrayidx358 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %194 = load i64, i64* %arrayidx358, align 16
  %add359 = add i64 %194, %193
  store i64 %add359, i64* %arrayidx358, align 16
  %arrayidx360 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %195 = load i64, i64* %arrayidx360, align 16
  %arrayidx361 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %196 = load i64, i64* %arrayidx361, align 16
  %xor362 = xor i64 %195, %196
  %call363 = call i64 @rotr64(i64 %xor362, i32 24)
  %arrayidx364 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call363, i64* %arrayidx364, align 16
  %arrayidx365 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %197 = load i64, i64* %arrayidx365, align 16
  %198 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 1), align 1
  %idxprom366 = zext i8 %198 to i32
  %arrayidx367 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom366
  %199 = load i64, i64* %arrayidx367, align 8
  %add368 = add i64 %197, %199
  %arrayidx369 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %200 = load i64, i64* %arrayidx369, align 16
  %add370 = add i64 %200, %add368
  store i64 %add370, i64* %arrayidx369, align 16
  %arrayidx371 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %201 = load i64, i64* %arrayidx371, align 16
  %arrayidx372 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %202 = load i64, i64* %arrayidx372, align 16
  %xor373 = xor i64 %201, %202
  %call374 = call i64 @rotr64(i64 %xor373, i32 16)
  %arrayidx375 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call374, i64* %arrayidx375, align 16
  %arrayidx376 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %203 = load i64, i64* %arrayidx376, align 16
  %arrayidx377 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %204 = load i64, i64* %arrayidx377, align 16
  %add378 = add i64 %204, %203
  store i64 %add378, i64* %arrayidx377, align 16
  %arrayidx379 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %205 = load i64, i64* %arrayidx379, align 16
  %arrayidx380 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %206 = load i64, i64* %arrayidx380, align 16
  %xor381 = xor i64 %205, %206
  %call382 = call i64 @rotr64(i64 %xor381, i32 63)
  %arrayidx383 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call382, i64* %arrayidx383, align 16
  br label %do.end384

do.end384:                                        ; preds = %do.body345
  br label %do.body385

do.body385:                                       ; preds = %do.end384
  %arrayidx386 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %207 = load i64, i64* %arrayidx386, align 8
  %208 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 2), align 2
  %idxprom387 = zext i8 %208 to i32
  %arrayidx388 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom387
  %209 = load i64, i64* %arrayidx388, align 8
  %add389 = add i64 %207, %209
  %arrayidx390 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %210 = load i64, i64* %arrayidx390, align 8
  %add391 = add i64 %210, %add389
  store i64 %add391, i64* %arrayidx390, align 8
  %arrayidx392 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %211 = load i64, i64* %arrayidx392, align 8
  %arrayidx393 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %212 = load i64, i64* %arrayidx393, align 8
  %xor394 = xor i64 %211, %212
  %call395 = call i64 @rotr64(i64 %xor394, i32 32)
  %arrayidx396 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call395, i64* %arrayidx396, align 8
  %arrayidx397 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %213 = load i64, i64* %arrayidx397, align 8
  %arrayidx398 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %214 = load i64, i64* %arrayidx398, align 8
  %add399 = add i64 %214, %213
  store i64 %add399, i64* %arrayidx398, align 8
  %arrayidx400 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %215 = load i64, i64* %arrayidx400, align 8
  %arrayidx401 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %216 = load i64, i64* %arrayidx401, align 8
  %xor402 = xor i64 %215, %216
  %call403 = call i64 @rotr64(i64 %xor402, i32 24)
  %arrayidx404 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call403, i64* %arrayidx404, align 8
  %arrayidx405 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %217 = load i64, i64* %arrayidx405, align 8
  %218 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 3), align 1
  %idxprom406 = zext i8 %218 to i32
  %arrayidx407 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom406
  %219 = load i64, i64* %arrayidx407, align 8
  %add408 = add i64 %217, %219
  %arrayidx409 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %220 = load i64, i64* %arrayidx409, align 8
  %add410 = add i64 %220, %add408
  store i64 %add410, i64* %arrayidx409, align 8
  %arrayidx411 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %221 = load i64, i64* %arrayidx411, align 8
  %arrayidx412 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %222 = load i64, i64* %arrayidx412, align 8
  %xor413 = xor i64 %221, %222
  %call414 = call i64 @rotr64(i64 %xor413, i32 16)
  %arrayidx415 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call414, i64* %arrayidx415, align 8
  %arrayidx416 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %223 = load i64, i64* %arrayidx416, align 8
  %arrayidx417 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %224 = load i64, i64* %arrayidx417, align 8
  %add418 = add i64 %224, %223
  store i64 %add418, i64* %arrayidx417, align 8
  %arrayidx419 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %225 = load i64, i64* %arrayidx419, align 8
  %arrayidx420 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %226 = load i64, i64* %arrayidx420, align 8
  %xor421 = xor i64 %225, %226
  %call422 = call i64 @rotr64(i64 %xor421, i32 63)
  %arrayidx423 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call422, i64* %arrayidx423, align 8
  br label %do.end424

do.end424:                                        ; preds = %do.body385
  br label %do.body425

do.body425:                                       ; preds = %do.end424
  %arrayidx426 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %227 = load i64, i64* %arrayidx426, align 16
  %228 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 4), align 4
  %idxprom427 = zext i8 %228 to i32
  %arrayidx428 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom427
  %229 = load i64, i64* %arrayidx428, align 8
  %add429 = add i64 %227, %229
  %arrayidx430 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %230 = load i64, i64* %arrayidx430, align 16
  %add431 = add i64 %230, %add429
  store i64 %add431, i64* %arrayidx430, align 16
  %arrayidx432 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %231 = load i64, i64* %arrayidx432, align 16
  %arrayidx433 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %232 = load i64, i64* %arrayidx433, align 16
  %xor434 = xor i64 %231, %232
  %call435 = call i64 @rotr64(i64 %xor434, i32 32)
  %arrayidx436 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call435, i64* %arrayidx436, align 16
  %arrayidx437 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %233 = load i64, i64* %arrayidx437, align 16
  %arrayidx438 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %234 = load i64, i64* %arrayidx438, align 16
  %add439 = add i64 %234, %233
  store i64 %add439, i64* %arrayidx438, align 16
  %arrayidx440 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %235 = load i64, i64* %arrayidx440, align 16
  %arrayidx441 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %236 = load i64, i64* %arrayidx441, align 16
  %xor442 = xor i64 %235, %236
  %call443 = call i64 @rotr64(i64 %xor442, i32 24)
  %arrayidx444 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call443, i64* %arrayidx444, align 16
  %arrayidx445 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %237 = load i64, i64* %arrayidx445, align 16
  %238 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 5), align 1
  %idxprom446 = zext i8 %238 to i32
  %arrayidx447 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom446
  %239 = load i64, i64* %arrayidx447, align 8
  %add448 = add i64 %237, %239
  %arrayidx449 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %240 = load i64, i64* %arrayidx449, align 16
  %add450 = add i64 %240, %add448
  store i64 %add450, i64* %arrayidx449, align 16
  %arrayidx451 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %241 = load i64, i64* %arrayidx451, align 16
  %arrayidx452 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %242 = load i64, i64* %arrayidx452, align 16
  %xor453 = xor i64 %241, %242
  %call454 = call i64 @rotr64(i64 %xor453, i32 16)
  %arrayidx455 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call454, i64* %arrayidx455, align 16
  %arrayidx456 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %243 = load i64, i64* %arrayidx456, align 16
  %arrayidx457 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %244 = load i64, i64* %arrayidx457, align 16
  %add458 = add i64 %244, %243
  store i64 %add458, i64* %arrayidx457, align 16
  %arrayidx459 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %245 = load i64, i64* %arrayidx459, align 16
  %arrayidx460 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %246 = load i64, i64* %arrayidx460, align 16
  %xor461 = xor i64 %245, %246
  %call462 = call i64 @rotr64(i64 %xor461, i32 63)
  %arrayidx463 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call462, i64* %arrayidx463, align 16
  br label %do.end464

do.end464:                                        ; preds = %do.body425
  br label %do.body465

do.body465:                                       ; preds = %do.end464
  %arrayidx466 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %247 = load i64, i64* %arrayidx466, align 8
  %248 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 6), align 2
  %idxprom467 = zext i8 %248 to i32
  %arrayidx468 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom467
  %249 = load i64, i64* %arrayidx468, align 8
  %add469 = add i64 %247, %249
  %arrayidx470 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %250 = load i64, i64* %arrayidx470, align 8
  %add471 = add i64 %250, %add469
  store i64 %add471, i64* %arrayidx470, align 8
  %arrayidx472 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %251 = load i64, i64* %arrayidx472, align 8
  %arrayidx473 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %252 = load i64, i64* %arrayidx473, align 8
  %xor474 = xor i64 %251, %252
  %call475 = call i64 @rotr64(i64 %xor474, i32 32)
  %arrayidx476 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call475, i64* %arrayidx476, align 8
  %arrayidx477 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %253 = load i64, i64* %arrayidx477, align 8
  %arrayidx478 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %254 = load i64, i64* %arrayidx478, align 8
  %add479 = add i64 %254, %253
  store i64 %add479, i64* %arrayidx478, align 8
  %arrayidx480 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %255 = load i64, i64* %arrayidx480, align 8
  %arrayidx481 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %256 = load i64, i64* %arrayidx481, align 8
  %xor482 = xor i64 %255, %256
  %call483 = call i64 @rotr64(i64 %xor482, i32 24)
  %arrayidx484 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call483, i64* %arrayidx484, align 8
  %arrayidx485 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %257 = load i64, i64* %arrayidx485, align 8
  %258 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 7), align 1
  %idxprom486 = zext i8 %258 to i32
  %arrayidx487 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom486
  %259 = load i64, i64* %arrayidx487, align 8
  %add488 = add i64 %257, %259
  %arrayidx489 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %260 = load i64, i64* %arrayidx489, align 8
  %add490 = add i64 %260, %add488
  store i64 %add490, i64* %arrayidx489, align 8
  %arrayidx491 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %261 = load i64, i64* %arrayidx491, align 8
  %arrayidx492 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %262 = load i64, i64* %arrayidx492, align 8
  %xor493 = xor i64 %261, %262
  %call494 = call i64 @rotr64(i64 %xor493, i32 16)
  %arrayidx495 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call494, i64* %arrayidx495, align 8
  %arrayidx496 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %263 = load i64, i64* %arrayidx496, align 8
  %arrayidx497 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %264 = load i64, i64* %arrayidx497, align 8
  %add498 = add i64 %264, %263
  store i64 %add498, i64* %arrayidx497, align 8
  %arrayidx499 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %265 = load i64, i64* %arrayidx499, align 8
  %arrayidx500 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %266 = load i64, i64* %arrayidx500, align 8
  %xor501 = xor i64 %265, %266
  %call502 = call i64 @rotr64(i64 %xor501, i32 63)
  %arrayidx503 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call502, i64* %arrayidx503, align 8
  br label %do.end504

do.end504:                                        ; preds = %do.body465
  br label %do.body505

do.body505:                                       ; preds = %do.end504
  %arrayidx506 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %267 = load i64, i64* %arrayidx506, align 8
  %268 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 8), align 8
  %idxprom507 = zext i8 %268 to i32
  %arrayidx508 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom507
  %269 = load i64, i64* %arrayidx508, align 8
  %add509 = add i64 %267, %269
  %arrayidx510 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %270 = load i64, i64* %arrayidx510, align 16
  %add511 = add i64 %270, %add509
  store i64 %add511, i64* %arrayidx510, align 16
  %arrayidx512 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %271 = load i64, i64* %arrayidx512, align 8
  %arrayidx513 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %272 = load i64, i64* %arrayidx513, align 16
  %xor514 = xor i64 %271, %272
  %call515 = call i64 @rotr64(i64 %xor514, i32 32)
  %arrayidx516 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call515, i64* %arrayidx516, align 8
  %arrayidx517 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %273 = load i64, i64* %arrayidx517, align 8
  %arrayidx518 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %274 = load i64, i64* %arrayidx518, align 16
  %add519 = add i64 %274, %273
  store i64 %add519, i64* %arrayidx518, align 16
  %arrayidx520 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %275 = load i64, i64* %arrayidx520, align 8
  %arrayidx521 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %276 = load i64, i64* %arrayidx521, align 16
  %xor522 = xor i64 %275, %276
  %call523 = call i64 @rotr64(i64 %xor522, i32 24)
  %arrayidx524 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call523, i64* %arrayidx524, align 8
  %arrayidx525 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %277 = load i64, i64* %arrayidx525, align 8
  %278 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 9), align 1
  %idxprom526 = zext i8 %278 to i32
  %arrayidx527 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom526
  %279 = load i64, i64* %arrayidx527, align 8
  %add528 = add i64 %277, %279
  %arrayidx529 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %280 = load i64, i64* %arrayidx529, align 16
  %add530 = add i64 %280, %add528
  store i64 %add530, i64* %arrayidx529, align 16
  %arrayidx531 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %281 = load i64, i64* %arrayidx531, align 8
  %arrayidx532 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %282 = load i64, i64* %arrayidx532, align 16
  %xor533 = xor i64 %281, %282
  %call534 = call i64 @rotr64(i64 %xor533, i32 16)
  %arrayidx535 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call534, i64* %arrayidx535, align 8
  %arrayidx536 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %283 = load i64, i64* %arrayidx536, align 8
  %arrayidx537 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %284 = load i64, i64* %arrayidx537, align 16
  %add538 = add i64 %284, %283
  store i64 %add538, i64* %arrayidx537, align 16
  %arrayidx539 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %285 = load i64, i64* %arrayidx539, align 8
  %arrayidx540 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %286 = load i64, i64* %arrayidx540, align 16
  %xor541 = xor i64 %285, %286
  %call542 = call i64 @rotr64(i64 %xor541, i32 63)
  %arrayidx543 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call542, i64* %arrayidx543, align 8
  br label %do.end544

do.end544:                                        ; preds = %do.body505
  br label %do.body545

do.body545:                                       ; preds = %do.end544
  %arrayidx546 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %287 = load i64, i64* %arrayidx546, align 16
  %288 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 10), align 2
  %idxprom547 = zext i8 %288 to i32
  %arrayidx548 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom547
  %289 = load i64, i64* %arrayidx548, align 8
  %add549 = add i64 %287, %289
  %arrayidx550 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %290 = load i64, i64* %arrayidx550, align 8
  %add551 = add i64 %290, %add549
  store i64 %add551, i64* %arrayidx550, align 8
  %arrayidx552 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %291 = load i64, i64* %arrayidx552, align 16
  %arrayidx553 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %292 = load i64, i64* %arrayidx553, align 8
  %xor554 = xor i64 %291, %292
  %call555 = call i64 @rotr64(i64 %xor554, i32 32)
  %arrayidx556 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call555, i64* %arrayidx556, align 16
  %arrayidx557 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %293 = load i64, i64* %arrayidx557, align 16
  %arrayidx558 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %294 = load i64, i64* %arrayidx558, align 8
  %add559 = add i64 %294, %293
  store i64 %add559, i64* %arrayidx558, align 8
  %arrayidx560 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %295 = load i64, i64* %arrayidx560, align 16
  %arrayidx561 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %296 = load i64, i64* %arrayidx561, align 8
  %xor562 = xor i64 %295, %296
  %call563 = call i64 @rotr64(i64 %xor562, i32 24)
  %arrayidx564 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call563, i64* %arrayidx564, align 16
  %arrayidx565 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %297 = load i64, i64* %arrayidx565, align 16
  %298 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 11), align 1
  %idxprom566 = zext i8 %298 to i32
  %arrayidx567 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom566
  %299 = load i64, i64* %arrayidx567, align 8
  %add568 = add i64 %297, %299
  %arrayidx569 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %300 = load i64, i64* %arrayidx569, align 8
  %add570 = add i64 %300, %add568
  store i64 %add570, i64* %arrayidx569, align 8
  %arrayidx571 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %301 = load i64, i64* %arrayidx571, align 16
  %arrayidx572 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %302 = load i64, i64* %arrayidx572, align 8
  %xor573 = xor i64 %301, %302
  %call574 = call i64 @rotr64(i64 %xor573, i32 16)
  %arrayidx575 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call574, i64* %arrayidx575, align 16
  %arrayidx576 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %303 = load i64, i64* %arrayidx576, align 16
  %arrayidx577 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %304 = load i64, i64* %arrayidx577, align 8
  %add578 = add i64 %304, %303
  store i64 %add578, i64* %arrayidx577, align 8
  %arrayidx579 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %305 = load i64, i64* %arrayidx579, align 16
  %arrayidx580 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %306 = load i64, i64* %arrayidx580, align 8
  %xor581 = xor i64 %305, %306
  %call582 = call i64 @rotr64(i64 %xor581, i32 63)
  %arrayidx583 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call582, i64* %arrayidx583, align 16
  br label %do.end584

do.end584:                                        ; preds = %do.body545
  br label %do.body585

do.body585:                                       ; preds = %do.end584
  %arrayidx586 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %307 = load i64, i64* %arrayidx586, align 8
  %308 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 12), align 4
  %idxprom587 = zext i8 %308 to i32
  %arrayidx588 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom587
  %309 = load i64, i64* %arrayidx588, align 8
  %add589 = add i64 %307, %309
  %arrayidx590 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %310 = load i64, i64* %arrayidx590, align 16
  %add591 = add i64 %310, %add589
  store i64 %add591, i64* %arrayidx590, align 16
  %arrayidx592 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %311 = load i64, i64* %arrayidx592, align 8
  %arrayidx593 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %312 = load i64, i64* %arrayidx593, align 16
  %xor594 = xor i64 %311, %312
  %call595 = call i64 @rotr64(i64 %xor594, i32 32)
  %arrayidx596 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call595, i64* %arrayidx596, align 8
  %arrayidx597 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %313 = load i64, i64* %arrayidx597, align 8
  %arrayidx598 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %314 = load i64, i64* %arrayidx598, align 16
  %add599 = add i64 %314, %313
  store i64 %add599, i64* %arrayidx598, align 16
  %arrayidx600 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %315 = load i64, i64* %arrayidx600, align 8
  %arrayidx601 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %316 = load i64, i64* %arrayidx601, align 16
  %xor602 = xor i64 %315, %316
  %call603 = call i64 @rotr64(i64 %xor602, i32 24)
  %arrayidx604 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call603, i64* %arrayidx604, align 8
  %arrayidx605 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %317 = load i64, i64* %arrayidx605, align 8
  %318 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 13), align 1
  %idxprom606 = zext i8 %318 to i32
  %arrayidx607 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom606
  %319 = load i64, i64* %arrayidx607, align 8
  %add608 = add i64 %317, %319
  %arrayidx609 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %320 = load i64, i64* %arrayidx609, align 16
  %add610 = add i64 %320, %add608
  store i64 %add610, i64* %arrayidx609, align 16
  %arrayidx611 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %321 = load i64, i64* %arrayidx611, align 8
  %arrayidx612 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %322 = load i64, i64* %arrayidx612, align 16
  %xor613 = xor i64 %321, %322
  %call614 = call i64 @rotr64(i64 %xor613, i32 16)
  %arrayidx615 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call614, i64* %arrayidx615, align 8
  %arrayidx616 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %323 = load i64, i64* %arrayidx616, align 8
  %arrayidx617 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %324 = load i64, i64* %arrayidx617, align 16
  %add618 = add i64 %324, %323
  store i64 %add618, i64* %arrayidx617, align 16
  %arrayidx619 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %325 = load i64, i64* %arrayidx619, align 8
  %arrayidx620 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %326 = load i64, i64* %arrayidx620, align 16
  %xor621 = xor i64 %325, %326
  %call622 = call i64 @rotr64(i64 %xor621, i32 63)
  %arrayidx623 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call622, i64* %arrayidx623, align 8
  br label %do.end624

do.end624:                                        ; preds = %do.body585
  br label %do.body625

do.body625:                                       ; preds = %do.end624
  %arrayidx626 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %327 = load i64, i64* %arrayidx626, align 16
  %328 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 14), align 2
  %idxprom627 = zext i8 %328 to i32
  %arrayidx628 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom627
  %329 = load i64, i64* %arrayidx628, align 8
  %add629 = add i64 %327, %329
  %arrayidx630 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %330 = load i64, i64* %arrayidx630, align 8
  %add631 = add i64 %330, %add629
  store i64 %add631, i64* %arrayidx630, align 8
  %arrayidx632 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %331 = load i64, i64* %arrayidx632, align 16
  %arrayidx633 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %332 = load i64, i64* %arrayidx633, align 8
  %xor634 = xor i64 %331, %332
  %call635 = call i64 @rotr64(i64 %xor634, i32 32)
  %arrayidx636 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call635, i64* %arrayidx636, align 16
  %arrayidx637 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %333 = load i64, i64* %arrayidx637, align 16
  %arrayidx638 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %334 = load i64, i64* %arrayidx638, align 8
  %add639 = add i64 %334, %333
  store i64 %add639, i64* %arrayidx638, align 8
  %arrayidx640 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %335 = load i64, i64* %arrayidx640, align 16
  %arrayidx641 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %336 = load i64, i64* %arrayidx641, align 8
  %xor642 = xor i64 %335, %336
  %call643 = call i64 @rotr64(i64 %xor642, i32 24)
  %arrayidx644 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call643, i64* %arrayidx644, align 16
  %arrayidx645 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %337 = load i64, i64* %arrayidx645, align 16
  %338 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 1, i32 15), align 1
  %idxprom646 = zext i8 %338 to i32
  %arrayidx647 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom646
  %339 = load i64, i64* %arrayidx647, align 8
  %add648 = add i64 %337, %339
  %arrayidx649 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %340 = load i64, i64* %arrayidx649, align 8
  %add650 = add i64 %340, %add648
  store i64 %add650, i64* %arrayidx649, align 8
  %arrayidx651 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %341 = load i64, i64* %arrayidx651, align 16
  %arrayidx652 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %342 = load i64, i64* %arrayidx652, align 8
  %xor653 = xor i64 %341, %342
  %call654 = call i64 @rotr64(i64 %xor653, i32 16)
  %arrayidx655 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call654, i64* %arrayidx655, align 16
  %arrayidx656 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %343 = load i64, i64* %arrayidx656, align 16
  %arrayidx657 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %344 = load i64, i64* %arrayidx657, align 8
  %add658 = add i64 %344, %343
  store i64 %add658, i64* %arrayidx657, align 8
  %arrayidx659 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %345 = load i64, i64* %arrayidx659, align 16
  %arrayidx660 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %346 = load i64, i64* %arrayidx660, align 8
  %xor661 = xor i64 %345, %346
  %call662 = call i64 @rotr64(i64 %xor661, i32 63)
  %arrayidx663 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call662, i64* %arrayidx663, align 16
  br label %do.end664

do.end664:                                        ; preds = %do.body625
  br label %do.end665

do.end665:                                        ; preds = %do.end664
  br label %do.body666

do.body666:                                       ; preds = %do.end665
  br label %do.body667

do.body667:                                       ; preds = %do.body666
  %arrayidx668 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %347 = load i64, i64* %arrayidx668, align 16
  %348 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 0), align 16
  %idxprom669 = zext i8 %348 to i32
  %arrayidx670 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom669
  %349 = load i64, i64* %arrayidx670, align 8
  %add671 = add i64 %347, %349
  %arrayidx672 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %350 = load i64, i64* %arrayidx672, align 16
  %add673 = add i64 %350, %add671
  store i64 %add673, i64* %arrayidx672, align 16
  %arrayidx674 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %351 = load i64, i64* %arrayidx674, align 16
  %arrayidx675 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %352 = load i64, i64* %arrayidx675, align 16
  %xor676 = xor i64 %351, %352
  %call677 = call i64 @rotr64(i64 %xor676, i32 32)
  %arrayidx678 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call677, i64* %arrayidx678, align 16
  %arrayidx679 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %353 = load i64, i64* %arrayidx679, align 16
  %arrayidx680 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %354 = load i64, i64* %arrayidx680, align 16
  %add681 = add i64 %354, %353
  store i64 %add681, i64* %arrayidx680, align 16
  %arrayidx682 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %355 = load i64, i64* %arrayidx682, align 16
  %arrayidx683 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %356 = load i64, i64* %arrayidx683, align 16
  %xor684 = xor i64 %355, %356
  %call685 = call i64 @rotr64(i64 %xor684, i32 24)
  %arrayidx686 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call685, i64* %arrayidx686, align 16
  %arrayidx687 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %357 = load i64, i64* %arrayidx687, align 16
  %358 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 1), align 1
  %idxprom688 = zext i8 %358 to i32
  %arrayidx689 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom688
  %359 = load i64, i64* %arrayidx689, align 8
  %add690 = add i64 %357, %359
  %arrayidx691 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %360 = load i64, i64* %arrayidx691, align 16
  %add692 = add i64 %360, %add690
  store i64 %add692, i64* %arrayidx691, align 16
  %arrayidx693 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %361 = load i64, i64* %arrayidx693, align 16
  %arrayidx694 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %362 = load i64, i64* %arrayidx694, align 16
  %xor695 = xor i64 %361, %362
  %call696 = call i64 @rotr64(i64 %xor695, i32 16)
  %arrayidx697 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call696, i64* %arrayidx697, align 16
  %arrayidx698 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %363 = load i64, i64* %arrayidx698, align 16
  %arrayidx699 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %364 = load i64, i64* %arrayidx699, align 16
  %add700 = add i64 %364, %363
  store i64 %add700, i64* %arrayidx699, align 16
  %arrayidx701 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %365 = load i64, i64* %arrayidx701, align 16
  %arrayidx702 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %366 = load i64, i64* %arrayidx702, align 16
  %xor703 = xor i64 %365, %366
  %call704 = call i64 @rotr64(i64 %xor703, i32 63)
  %arrayidx705 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call704, i64* %arrayidx705, align 16
  br label %do.end706

do.end706:                                        ; preds = %do.body667
  br label %do.body707

do.body707:                                       ; preds = %do.end706
  %arrayidx708 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %367 = load i64, i64* %arrayidx708, align 8
  %368 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 2), align 2
  %idxprom709 = zext i8 %368 to i32
  %arrayidx710 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom709
  %369 = load i64, i64* %arrayidx710, align 8
  %add711 = add i64 %367, %369
  %arrayidx712 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %370 = load i64, i64* %arrayidx712, align 8
  %add713 = add i64 %370, %add711
  store i64 %add713, i64* %arrayidx712, align 8
  %arrayidx714 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %371 = load i64, i64* %arrayidx714, align 8
  %arrayidx715 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %372 = load i64, i64* %arrayidx715, align 8
  %xor716 = xor i64 %371, %372
  %call717 = call i64 @rotr64(i64 %xor716, i32 32)
  %arrayidx718 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call717, i64* %arrayidx718, align 8
  %arrayidx719 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %373 = load i64, i64* %arrayidx719, align 8
  %arrayidx720 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %374 = load i64, i64* %arrayidx720, align 8
  %add721 = add i64 %374, %373
  store i64 %add721, i64* %arrayidx720, align 8
  %arrayidx722 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %375 = load i64, i64* %arrayidx722, align 8
  %arrayidx723 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %376 = load i64, i64* %arrayidx723, align 8
  %xor724 = xor i64 %375, %376
  %call725 = call i64 @rotr64(i64 %xor724, i32 24)
  %arrayidx726 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call725, i64* %arrayidx726, align 8
  %arrayidx727 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %377 = load i64, i64* %arrayidx727, align 8
  %378 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 3), align 1
  %idxprom728 = zext i8 %378 to i32
  %arrayidx729 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom728
  %379 = load i64, i64* %arrayidx729, align 8
  %add730 = add i64 %377, %379
  %arrayidx731 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %380 = load i64, i64* %arrayidx731, align 8
  %add732 = add i64 %380, %add730
  store i64 %add732, i64* %arrayidx731, align 8
  %arrayidx733 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %381 = load i64, i64* %arrayidx733, align 8
  %arrayidx734 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %382 = load i64, i64* %arrayidx734, align 8
  %xor735 = xor i64 %381, %382
  %call736 = call i64 @rotr64(i64 %xor735, i32 16)
  %arrayidx737 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call736, i64* %arrayidx737, align 8
  %arrayidx738 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %383 = load i64, i64* %arrayidx738, align 8
  %arrayidx739 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %384 = load i64, i64* %arrayidx739, align 8
  %add740 = add i64 %384, %383
  store i64 %add740, i64* %arrayidx739, align 8
  %arrayidx741 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %385 = load i64, i64* %arrayidx741, align 8
  %arrayidx742 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %386 = load i64, i64* %arrayidx742, align 8
  %xor743 = xor i64 %385, %386
  %call744 = call i64 @rotr64(i64 %xor743, i32 63)
  %arrayidx745 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call744, i64* %arrayidx745, align 8
  br label %do.end746

do.end746:                                        ; preds = %do.body707
  br label %do.body747

do.body747:                                       ; preds = %do.end746
  %arrayidx748 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %387 = load i64, i64* %arrayidx748, align 16
  %388 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 4), align 4
  %idxprom749 = zext i8 %388 to i32
  %arrayidx750 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom749
  %389 = load i64, i64* %arrayidx750, align 8
  %add751 = add i64 %387, %389
  %arrayidx752 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %390 = load i64, i64* %arrayidx752, align 16
  %add753 = add i64 %390, %add751
  store i64 %add753, i64* %arrayidx752, align 16
  %arrayidx754 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %391 = load i64, i64* %arrayidx754, align 16
  %arrayidx755 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %392 = load i64, i64* %arrayidx755, align 16
  %xor756 = xor i64 %391, %392
  %call757 = call i64 @rotr64(i64 %xor756, i32 32)
  %arrayidx758 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call757, i64* %arrayidx758, align 16
  %arrayidx759 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %393 = load i64, i64* %arrayidx759, align 16
  %arrayidx760 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %394 = load i64, i64* %arrayidx760, align 16
  %add761 = add i64 %394, %393
  store i64 %add761, i64* %arrayidx760, align 16
  %arrayidx762 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %395 = load i64, i64* %arrayidx762, align 16
  %arrayidx763 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %396 = load i64, i64* %arrayidx763, align 16
  %xor764 = xor i64 %395, %396
  %call765 = call i64 @rotr64(i64 %xor764, i32 24)
  %arrayidx766 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call765, i64* %arrayidx766, align 16
  %arrayidx767 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %397 = load i64, i64* %arrayidx767, align 16
  %398 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 5), align 1
  %idxprom768 = zext i8 %398 to i32
  %arrayidx769 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom768
  %399 = load i64, i64* %arrayidx769, align 8
  %add770 = add i64 %397, %399
  %arrayidx771 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %400 = load i64, i64* %arrayidx771, align 16
  %add772 = add i64 %400, %add770
  store i64 %add772, i64* %arrayidx771, align 16
  %arrayidx773 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %401 = load i64, i64* %arrayidx773, align 16
  %arrayidx774 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %402 = load i64, i64* %arrayidx774, align 16
  %xor775 = xor i64 %401, %402
  %call776 = call i64 @rotr64(i64 %xor775, i32 16)
  %arrayidx777 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call776, i64* %arrayidx777, align 16
  %arrayidx778 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %403 = load i64, i64* %arrayidx778, align 16
  %arrayidx779 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %404 = load i64, i64* %arrayidx779, align 16
  %add780 = add i64 %404, %403
  store i64 %add780, i64* %arrayidx779, align 16
  %arrayidx781 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %405 = load i64, i64* %arrayidx781, align 16
  %arrayidx782 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %406 = load i64, i64* %arrayidx782, align 16
  %xor783 = xor i64 %405, %406
  %call784 = call i64 @rotr64(i64 %xor783, i32 63)
  %arrayidx785 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call784, i64* %arrayidx785, align 16
  br label %do.end786

do.end786:                                        ; preds = %do.body747
  br label %do.body787

do.body787:                                       ; preds = %do.end786
  %arrayidx788 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %407 = load i64, i64* %arrayidx788, align 8
  %408 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 6), align 2
  %idxprom789 = zext i8 %408 to i32
  %arrayidx790 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom789
  %409 = load i64, i64* %arrayidx790, align 8
  %add791 = add i64 %407, %409
  %arrayidx792 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %410 = load i64, i64* %arrayidx792, align 8
  %add793 = add i64 %410, %add791
  store i64 %add793, i64* %arrayidx792, align 8
  %arrayidx794 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %411 = load i64, i64* %arrayidx794, align 8
  %arrayidx795 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %412 = load i64, i64* %arrayidx795, align 8
  %xor796 = xor i64 %411, %412
  %call797 = call i64 @rotr64(i64 %xor796, i32 32)
  %arrayidx798 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call797, i64* %arrayidx798, align 8
  %arrayidx799 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %413 = load i64, i64* %arrayidx799, align 8
  %arrayidx800 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %414 = load i64, i64* %arrayidx800, align 8
  %add801 = add i64 %414, %413
  store i64 %add801, i64* %arrayidx800, align 8
  %arrayidx802 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %415 = load i64, i64* %arrayidx802, align 8
  %arrayidx803 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %416 = load i64, i64* %arrayidx803, align 8
  %xor804 = xor i64 %415, %416
  %call805 = call i64 @rotr64(i64 %xor804, i32 24)
  %arrayidx806 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call805, i64* %arrayidx806, align 8
  %arrayidx807 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %417 = load i64, i64* %arrayidx807, align 8
  %418 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 7), align 1
  %idxprom808 = zext i8 %418 to i32
  %arrayidx809 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom808
  %419 = load i64, i64* %arrayidx809, align 8
  %add810 = add i64 %417, %419
  %arrayidx811 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %420 = load i64, i64* %arrayidx811, align 8
  %add812 = add i64 %420, %add810
  store i64 %add812, i64* %arrayidx811, align 8
  %arrayidx813 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %421 = load i64, i64* %arrayidx813, align 8
  %arrayidx814 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %422 = load i64, i64* %arrayidx814, align 8
  %xor815 = xor i64 %421, %422
  %call816 = call i64 @rotr64(i64 %xor815, i32 16)
  %arrayidx817 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call816, i64* %arrayidx817, align 8
  %arrayidx818 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %423 = load i64, i64* %arrayidx818, align 8
  %arrayidx819 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %424 = load i64, i64* %arrayidx819, align 8
  %add820 = add i64 %424, %423
  store i64 %add820, i64* %arrayidx819, align 8
  %arrayidx821 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %425 = load i64, i64* %arrayidx821, align 8
  %arrayidx822 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %426 = load i64, i64* %arrayidx822, align 8
  %xor823 = xor i64 %425, %426
  %call824 = call i64 @rotr64(i64 %xor823, i32 63)
  %arrayidx825 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call824, i64* %arrayidx825, align 8
  br label %do.end826

do.end826:                                        ; preds = %do.body787
  br label %do.body827

do.body827:                                       ; preds = %do.end826
  %arrayidx828 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %427 = load i64, i64* %arrayidx828, align 8
  %428 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 8), align 8
  %idxprom829 = zext i8 %428 to i32
  %arrayidx830 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom829
  %429 = load i64, i64* %arrayidx830, align 8
  %add831 = add i64 %427, %429
  %arrayidx832 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %430 = load i64, i64* %arrayidx832, align 16
  %add833 = add i64 %430, %add831
  store i64 %add833, i64* %arrayidx832, align 16
  %arrayidx834 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %431 = load i64, i64* %arrayidx834, align 8
  %arrayidx835 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %432 = load i64, i64* %arrayidx835, align 16
  %xor836 = xor i64 %431, %432
  %call837 = call i64 @rotr64(i64 %xor836, i32 32)
  %arrayidx838 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call837, i64* %arrayidx838, align 8
  %arrayidx839 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %433 = load i64, i64* %arrayidx839, align 8
  %arrayidx840 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %434 = load i64, i64* %arrayidx840, align 16
  %add841 = add i64 %434, %433
  store i64 %add841, i64* %arrayidx840, align 16
  %arrayidx842 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %435 = load i64, i64* %arrayidx842, align 8
  %arrayidx843 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %436 = load i64, i64* %arrayidx843, align 16
  %xor844 = xor i64 %435, %436
  %call845 = call i64 @rotr64(i64 %xor844, i32 24)
  %arrayidx846 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call845, i64* %arrayidx846, align 8
  %arrayidx847 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %437 = load i64, i64* %arrayidx847, align 8
  %438 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 9), align 1
  %idxprom848 = zext i8 %438 to i32
  %arrayidx849 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom848
  %439 = load i64, i64* %arrayidx849, align 8
  %add850 = add i64 %437, %439
  %arrayidx851 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %440 = load i64, i64* %arrayidx851, align 16
  %add852 = add i64 %440, %add850
  store i64 %add852, i64* %arrayidx851, align 16
  %arrayidx853 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %441 = load i64, i64* %arrayidx853, align 8
  %arrayidx854 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %442 = load i64, i64* %arrayidx854, align 16
  %xor855 = xor i64 %441, %442
  %call856 = call i64 @rotr64(i64 %xor855, i32 16)
  %arrayidx857 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call856, i64* %arrayidx857, align 8
  %arrayidx858 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %443 = load i64, i64* %arrayidx858, align 8
  %arrayidx859 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %444 = load i64, i64* %arrayidx859, align 16
  %add860 = add i64 %444, %443
  store i64 %add860, i64* %arrayidx859, align 16
  %arrayidx861 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %445 = load i64, i64* %arrayidx861, align 8
  %arrayidx862 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %446 = load i64, i64* %arrayidx862, align 16
  %xor863 = xor i64 %445, %446
  %call864 = call i64 @rotr64(i64 %xor863, i32 63)
  %arrayidx865 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call864, i64* %arrayidx865, align 8
  br label %do.end866

do.end866:                                        ; preds = %do.body827
  br label %do.body867

do.body867:                                       ; preds = %do.end866
  %arrayidx868 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %447 = load i64, i64* %arrayidx868, align 16
  %448 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 10), align 2
  %idxprom869 = zext i8 %448 to i32
  %arrayidx870 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom869
  %449 = load i64, i64* %arrayidx870, align 8
  %add871 = add i64 %447, %449
  %arrayidx872 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %450 = load i64, i64* %arrayidx872, align 8
  %add873 = add i64 %450, %add871
  store i64 %add873, i64* %arrayidx872, align 8
  %arrayidx874 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %451 = load i64, i64* %arrayidx874, align 16
  %arrayidx875 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %452 = load i64, i64* %arrayidx875, align 8
  %xor876 = xor i64 %451, %452
  %call877 = call i64 @rotr64(i64 %xor876, i32 32)
  %arrayidx878 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call877, i64* %arrayidx878, align 16
  %arrayidx879 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %453 = load i64, i64* %arrayidx879, align 16
  %arrayidx880 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %454 = load i64, i64* %arrayidx880, align 8
  %add881 = add i64 %454, %453
  store i64 %add881, i64* %arrayidx880, align 8
  %arrayidx882 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %455 = load i64, i64* %arrayidx882, align 16
  %arrayidx883 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %456 = load i64, i64* %arrayidx883, align 8
  %xor884 = xor i64 %455, %456
  %call885 = call i64 @rotr64(i64 %xor884, i32 24)
  %arrayidx886 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call885, i64* %arrayidx886, align 16
  %arrayidx887 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %457 = load i64, i64* %arrayidx887, align 16
  %458 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 11), align 1
  %idxprom888 = zext i8 %458 to i32
  %arrayidx889 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom888
  %459 = load i64, i64* %arrayidx889, align 8
  %add890 = add i64 %457, %459
  %arrayidx891 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %460 = load i64, i64* %arrayidx891, align 8
  %add892 = add i64 %460, %add890
  store i64 %add892, i64* %arrayidx891, align 8
  %arrayidx893 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %461 = load i64, i64* %arrayidx893, align 16
  %arrayidx894 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %462 = load i64, i64* %arrayidx894, align 8
  %xor895 = xor i64 %461, %462
  %call896 = call i64 @rotr64(i64 %xor895, i32 16)
  %arrayidx897 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call896, i64* %arrayidx897, align 16
  %arrayidx898 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %463 = load i64, i64* %arrayidx898, align 16
  %arrayidx899 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %464 = load i64, i64* %arrayidx899, align 8
  %add900 = add i64 %464, %463
  store i64 %add900, i64* %arrayidx899, align 8
  %arrayidx901 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %465 = load i64, i64* %arrayidx901, align 16
  %arrayidx902 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %466 = load i64, i64* %arrayidx902, align 8
  %xor903 = xor i64 %465, %466
  %call904 = call i64 @rotr64(i64 %xor903, i32 63)
  %arrayidx905 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call904, i64* %arrayidx905, align 16
  br label %do.end906

do.end906:                                        ; preds = %do.body867
  br label %do.body907

do.body907:                                       ; preds = %do.end906
  %arrayidx908 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %467 = load i64, i64* %arrayidx908, align 8
  %468 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 12), align 4
  %idxprom909 = zext i8 %468 to i32
  %arrayidx910 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom909
  %469 = load i64, i64* %arrayidx910, align 8
  %add911 = add i64 %467, %469
  %arrayidx912 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %470 = load i64, i64* %arrayidx912, align 16
  %add913 = add i64 %470, %add911
  store i64 %add913, i64* %arrayidx912, align 16
  %arrayidx914 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %471 = load i64, i64* %arrayidx914, align 8
  %arrayidx915 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %472 = load i64, i64* %arrayidx915, align 16
  %xor916 = xor i64 %471, %472
  %call917 = call i64 @rotr64(i64 %xor916, i32 32)
  %arrayidx918 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call917, i64* %arrayidx918, align 8
  %arrayidx919 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %473 = load i64, i64* %arrayidx919, align 8
  %arrayidx920 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %474 = load i64, i64* %arrayidx920, align 16
  %add921 = add i64 %474, %473
  store i64 %add921, i64* %arrayidx920, align 16
  %arrayidx922 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %475 = load i64, i64* %arrayidx922, align 8
  %arrayidx923 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %476 = load i64, i64* %arrayidx923, align 16
  %xor924 = xor i64 %475, %476
  %call925 = call i64 @rotr64(i64 %xor924, i32 24)
  %arrayidx926 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call925, i64* %arrayidx926, align 8
  %arrayidx927 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %477 = load i64, i64* %arrayidx927, align 8
  %478 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 13), align 1
  %idxprom928 = zext i8 %478 to i32
  %arrayidx929 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom928
  %479 = load i64, i64* %arrayidx929, align 8
  %add930 = add i64 %477, %479
  %arrayidx931 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %480 = load i64, i64* %arrayidx931, align 16
  %add932 = add i64 %480, %add930
  store i64 %add932, i64* %arrayidx931, align 16
  %arrayidx933 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %481 = load i64, i64* %arrayidx933, align 8
  %arrayidx934 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %482 = load i64, i64* %arrayidx934, align 16
  %xor935 = xor i64 %481, %482
  %call936 = call i64 @rotr64(i64 %xor935, i32 16)
  %arrayidx937 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call936, i64* %arrayidx937, align 8
  %arrayidx938 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %483 = load i64, i64* %arrayidx938, align 8
  %arrayidx939 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %484 = load i64, i64* %arrayidx939, align 16
  %add940 = add i64 %484, %483
  store i64 %add940, i64* %arrayidx939, align 16
  %arrayidx941 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %485 = load i64, i64* %arrayidx941, align 8
  %arrayidx942 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %486 = load i64, i64* %arrayidx942, align 16
  %xor943 = xor i64 %485, %486
  %call944 = call i64 @rotr64(i64 %xor943, i32 63)
  %arrayidx945 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call944, i64* %arrayidx945, align 8
  br label %do.end946

do.end946:                                        ; preds = %do.body907
  br label %do.body947

do.body947:                                       ; preds = %do.end946
  %arrayidx948 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %487 = load i64, i64* %arrayidx948, align 16
  %488 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 14), align 2
  %idxprom949 = zext i8 %488 to i32
  %arrayidx950 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom949
  %489 = load i64, i64* %arrayidx950, align 8
  %add951 = add i64 %487, %489
  %arrayidx952 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %490 = load i64, i64* %arrayidx952, align 8
  %add953 = add i64 %490, %add951
  store i64 %add953, i64* %arrayidx952, align 8
  %arrayidx954 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %491 = load i64, i64* %arrayidx954, align 16
  %arrayidx955 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %492 = load i64, i64* %arrayidx955, align 8
  %xor956 = xor i64 %491, %492
  %call957 = call i64 @rotr64(i64 %xor956, i32 32)
  %arrayidx958 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call957, i64* %arrayidx958, align 16
  %arrayidx959 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %493 = load i64, i64* %arrayidx959, align 16
  %arrayidx960 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %494 = load i64, i64* %arrayidx960, align 8
  %add961 = add i64 %494, %493
  store i64 %add961, i64* %arrayidx960, align 8
  %arrayidx962 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %495 = load i64, i64* %arrayidx962, align 16
  %arrayidx963 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %496 = load i64, i64* %arrayidx963, align 8
  %xor964 = xor i64 %495, %496
  %call965 = call i64 @rotr64(i64 %xor964, i32 24)
  %arrayidx966 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call965, i64* %arrayidx966, align 16
  %arrayidx967 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %497 = load i64, i64* %arrayidx967, align 16
  %498 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 2, i32 15), align 1
  %idxprom968 = zext i8 %498 to i32
  %arrayidx969 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom968
  %499 = load i64, i64* %arrayidx969, align 8
  %add970 = add i64 %497, %499
  %arrayidx971 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %500 = load i64, i64* %arrayidx971, align 8
  %add972 = add i64 %500, %add970
  store i64 %add972, i64* %arrayidx971, align 8
  %arrayidx973 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %501 = load i64, i64* %arrayidx973, align 16
  %arrayidx974 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %502 = load i64, i64* %arrayidx974, align 8
  %xor975 = xor i64 %501, %502
  %call976 = call i64 @rotr64(i64 %xor975, i32 16)
  %arrayidx977 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call976, i64* %arrayidx977, align 16
  %arrayidx978 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %503 = load i64, i64* %arrayidx978, align 16
  %arrayidx979 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %504 = load i64, i64* %arrayidx979, align 8
  %add980 = add i64 %504, %503
  store i64 %add980, i64* %arrayidx979, align 8
  %arrayidx981 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %505 = load i64, i64* %arrayidx981, align 16
  %arrayidx982 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %506 = load i64, i64* %arrayidx982, align 8
  %xor983 = xor i64 %505, %506
  %call984 = call i64 @rotr64(i64 %xor983, i32 63)
  %arrayidx985 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call984, i64* %arrayidx985, align 16
  br label %do.end986

do.end986:                                        ; preds = %do.body947
  br label %do.end987

do.end987:                                        ; preds = %do.end986
  br label %do.body988

do.body988:                                       ; preds = %do.end987
  br label %do.body989

do.body989:                                       ; preds = %do.body988
  %arrayidx990 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %507 = load i64, i64* %arrayidx990, align 16
  %508 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 0), align 16
  %idxprom991 = zext i8 %508 to i32
  %arrayidx992 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom991
  %509 = load i64, i64* %arrayidx992, align 8
  %add993 = add i64 %507, %509
  %arrayidx994 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %510 = load i64, i64* %arrayidx994, align 16
  %add995 = add i64 %510, %add993
  store i64 %add995, i64* %arrayidx994, align 16
  %arrayidx996 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %511 = load i64, i64* %arrayidx996, align 16
  %arrayidx997 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %512 = load i64, i64* %arrayidx997, align 16
  %xor998 = xor i64 %511, %512
  %call999 = call i64 @rotr64(i64 %xor998, i32 32)
  %arrayidx1000 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call999, i64* %arrayidx1000, align 16
  %arrayidx1001 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %513 = load i64, i64* %arrayidx1001, align 16
  %arrayidx1002 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %514 = load i64, i64* %arrayidx1002, align 16
  %add1003 = add i64 %514, %513
  store i64 %add1003, i64* %arrayidx1002, align 16
  %arrayidx1004 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %515 = load i64, i64* %arrayidx1004, align 16
  %arrayidx1005 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %516 = load i64, i64* %arrayidx1005, align 16
  %xor1006 = xor i64 %515, %516
  %call1007 = call i64 @rotr64(i64 %xor1006, i32 24)
  %arrayidx1008 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1007, i64* %arrayidx1008, align 16
  %arrayidx1009 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %517 = load i64, i64* %arrayidx1009, align 16
  %518 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 1), align 1
  %idxprom1010 = zext i8 %518 to i32
  %arrayidx1011 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1010
  %519 = load i64, i64* %arrayidx1011, align 8
  %add1012 = add i64 %517, %519
  %arrayidx1013 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %520 = load i64, i64* %arrayidx1013, align 16
  %add1014 = add i64 %520, %add1012
  store i64 %add1014, i64* %arrayidx1013, align 16
  %arrayidx1015 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %521 = load i64, i64* %arrayidx1015, align 16
  %arrayidx1016 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %522 = load i64, i64* %arrayidx1016, align 16
  %xor1017 = xor i64 %521, %522
  %call1018 = call i64 @rotr64(i64 %xor1017, i32 16)
  %arrayidx1019 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1018, i64* %arrayidx1019, align 16
  %arrayidx1020 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %523 = load i64, i64* %arrayidx1020, align 16
  %arrayidx1021 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %524 = load i64, i64* %arrayidx1021, align 16
  %add1022 = add i64 %524, %523
  store i64 %add1022, i64* %arrayidx1021, align 16
  %arrayidx1023 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %525 = load i64, i64* %arrayidx1023, align 16
  %arrayidx1024 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %526 = load i64, i64* %arrayidx1024, align 16
  %xor1025 = xor i64 %525, %526
  %call1026 = call i64 @rotr64(i64 %xor1025, i32 63)
  %arrayidx1027 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1026, i64* %arrayidx1027, align 16
  br label %do.end1028

do.end1028:                                       ; preds = %do.body989
  br label %do.body1029

do.body1029:                                      ; preds = %do.end1028
  %arrayidx1030 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %527 = load i64, i64* %arrayidx1030, align 8
  %528 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 2), align 2
  %idxprom1031 = zext i8 %528 to i32
  %arrayidx1032 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1031
  %529 = load i64, i64* %arrayidx1032, align 8
  %add1033 = add i64 %527, %529
  %arrayidx1034 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %530 = load i64, i64* %arrayidx1034, align 8
  %add1035 = add i64 %530, %add1033
  store i64 %add1035, i64* %arrayidx1034, align 8
  %arrayidx1036 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %531 = load i64, i64* %arrayidx1036, align 8
  %arrayidx1037 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %532 = load i64, i64* %arrayidx1037, align 8
  %xor1038 = xor i64 %531, %532
  %call1039 = call i64 @rotr64(i64 %xor1038, i32 32)
  %arrayidx1040 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1039, i64* %arrayidx1040, align 8
  %arrayidx1041 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %533 = load i64, i64* %arrayidx1041, align 8
  %arrayidx1042 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %534 = load i64, i64* %arrayidx1042, align 8
  %add1043 = add i64 %534, %533
  store i64 %add1043, i64* %arrayidx1042, align 8
  %arrayidx1044 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %535 = load i64, i64* %arrayidx1044, align 8
  %arrayidx1045 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %536 = load i64, i64* %arrayidx1045, align 8
  %xor1046 = xor i64 %535, %536
  %call1047 = call i64 @rotr64(i64 %xor1046, i32 24)
  %arrayidx1048 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1047, i64* %arrayidx1048, align 8
  %arrayidx1049 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %537 = load i64, i64* %arrayidx1049, align 8
  %538 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 3), align 1
  %idxprom1050 = zext i8 %538 to i32
  %arrayidx1051 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1050
  %539 = load i64, i64* %arrayidx1051, align 8
  %add1052 = add i64 %537, %539
  %arrayidx1053 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %540 = load i64, i64* %arrayidx1053, align 8
  %add1054 = add i64 %540, %add1052
  store i64 %add1054, i64* %arrayidx1053, align 8
  %arrayidx1055 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %541 = load i64, i64* %arrayidx1055, align 8
  %arrayidx1056 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %542 = load i64, i64* %arrayidx1056, align 8
  %xor1057 = xor i64 %541, %542
  %call1058 = call i64 @rotr64(i64 %xor1057, i32 16)
  %arrayidx1059 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1058, i64* %arrayidx1059, align 8
  %arrayidx1060 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %543 = load i64, i64* %arrayidx1060, align 8
  %arrayidx1061 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %544 = load i64, i64* %arrayidx1061, align 8
  %add1062 = add i64 %544, %543
  store i64 %add1062, i64* %arrayidx1061, align 8
  %arrayidx1063 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %545 = load i64, i64* %arrayidx1063, align 8
  %arrayidx1064 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %546 = load i64, i64* %arrayidx1064, align 8
  %xor1065 = xor i64 %545, %546
  %call1066 = call i64 @rotr64(i64 %xor1065, i32 63)
  %arrayidx1067 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1066, i64* %arrayidx1067, align 8
  br label %do.end1068

do.end1068:                                       ; preds = %do.body1029
  br label %do.body1069

do.body1069:                                      ; preds = %do.end1068
  %arrayidx1070 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %547 = load i64, i64* %arrayidx1070, align 16
  %548 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 4), align 4
  %idxprom1071 = zext i8 %548 to i32
  %arrayidx1072 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1071
  %549 = load i64, i64* %arrayidx1072, align 8
  %add1073 = add i64 %547, %549
  %arrayidx1074 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %550 = load i64, i64* %arrayidx1074, align 16
  %add1075 = add i64 %550, %add1073
  store i64 %add1075, i64* %arrayidx1074, align 16
  %arrayidx1076 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %551 = load i64, i64* %arrayidx1076, align 16
  %arrayidx1077 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %552 = load i64, i64* %arrayidx1077, align 16
  %xor1078 = xor i64 %551, %552
  %call1079 = call i64 @rotr64(i64 %xor1078, i32 32)
  %arrayidx1080 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1079, i64* %arrayidx1080, align 16
  %arrayidx1081 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %553 = load i64, i64* %arrayidx1081, align 16
  %arrayidx1082 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %554 = load i64, i64* %arrayidx1082, align 16
  %add1083 = add i64 %554, %553
  store i64 %add1083, i64* %arrayidx1082, align 16
  %arrayidx1084 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %555 = load i64, i64* %arrayidx1084, align 16
  %arrayidx1085 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %556 = load i64, i64* %arrayidx1085, align 16
  %xor1086 = xor i64 %555, %556
  %call1087 = call i64 @rotr64(i64 %xor1086, i32 24)
  %arrayidx1088 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1087, i64* %arrayidx1088, align 16
  %arrayidx1089 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %557 = load i64, i64* %arrayidx1089, align 16
  %558 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 5), align 1
  %idxprom1090 = zext i8 %558 to i32
  %arrayidx1091 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1090
  %559 = load i64, i64* %arrayidx1091, align 8
  %add1092 = add i64 %557, %559
  %arrayidx1093 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %560 = load i64, i64* %arrayidx1093, align 16
  %add1094 = add i64 %560, %add1092
  store i64 %add1094, i64* %arrayidx1093, align 16
  %arrayidx1095 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %561 = load i64, i64* %arrayidx1095, align 16
  %arrayidx1096 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %562 = load i64, i64* %arrayidx1096, align 16
  %xor1097 = xor i64 %561, %562
  %call1098 = call i64 @rotr64(i64 %xor1097, i32 16)
  %arrayidx1099 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1098, i64* %arrayidx1099, align 16
  %arrayidx1100 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %563 = load i64, i64* %arrayidx1100, align 16
  %arrayidx1101 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %564 = load i64, i64* %arrayidx1101, align 16
  %add1102 = add i64 %564, %563
  store i64 %add1102, i64* %arrayidx1101, align 16
  %arrayidx1103 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %565 = load i64, i64* %arrayidx1103, align 16
  %arrayidx1104 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %566 = load i64, i64* %arrayidx1104, align 16
  %xor1105 = xor i64 %565, %566
  %call1106 = call i64 @rotr64(i64 %xor1105, i32 63)
  %arrayidx1107 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1106, i64* %arrayidx1107, align 16
  br label %do.end1108

do.end1108:                                       ; preds = %do.body1069
  br label %do.body1109

do.body1109:                                      ; preds = %do.end1108
  %arrayidx1110 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %567 = load i64, i64* %arrayidx1110, align 8
  %568 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 6), align 2
  %idxprom1111 = zext i8 %568 to i32
  %arrayidx1112 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1111
  %569 = load i64, i64* %arrayidx1112, align 8
  %add1113 = add i64 %567, %569
  %arrayidx1114 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %570 = load i64, i64* %arrayidx1114, align 8
  %add1115 = add i64 %570, %add1113
  store i64 %add1115, i64* %arrayidx1114, align 8
  %arrayidx1116 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %571 = load i64, i64* %arrayidx1116, align 8
  %arrayidx1117 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %572 = load i64, i64* %arrayidx1117, align 8
  %xor1118 = xor i64 %571, %572
  %call1119 = call i64 @rotr64(i64 %xor1118, i32 32)
  %arrayidx1120 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1119, i64* %arrayidx1120, align 8
  %arrayidx1121 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %573 = load i64, i64* %arrayidx1121, align 8
  %arrayidx1122 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %574 = load i64, i64* %arrayidx1122, align 8
  %add1123 = add i64 %574, %573
  store i64 %add1123, i64* %arrayidx1122, align 8
  %arrayidx1124 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %575 = load i64, i64* %arrayidx1124, align 8
  %arrayidx1125 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %576 = load i64, i64* %arrayidx1125, align 8
  %xor1126 = xor i64 %575, %576
  %call1127 = call i64 @rotr64(i64 %xor1126, i32 24)
  %arrayidx1128 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1127, i64* %arrayidx1128, align 8
  %arrayidx1129 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %577 = load i64, i64* %arrayidx1129, align 8
  %578 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 7), align 1
  %idxprom1130 = zext i8 %578 to i32
  %arrayidx1131 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1130
  %579 = load i64, i64* %arrayidx1131, align 8
  %add1132 = add i64 %577, %579
  %arrayidx1133 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %580 = load i64, i64* %arrayidx1133, align 8
  %add1134 = add i64 %580, %add1132
  store i64 %add1134, i64* %arrayidx1133, align 8
  %arrayidx1135 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %581 = load i64, i64* %arrayidx1135, align 8
  %arrayidx1136 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %582 = load i64, i64* %arrayidx1136, align 8
  %xor1137 = xor i64 %581, %582
  %call1138 = call i64 @rotr64(i64 %xor1137, i32 16)
  %arrayidx1139 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1138, i64* %arrayidx1139, align 8
  %arrayidx1140 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %583 = load i64, i64* %arrayidx1140, align 8
  %arrayidx1141 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %584 = load i64, i64* %arrayidx1141, align 8
  %add1142 = add i64 %584, %583
  store i64 %add1142, i64* %arrayidx1141, align 8
  %arrayidx1143 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %585 = load i64, i64* %arrayidx1143, align 8
  %arrayidx1144 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %586 = load i64, i64* %arrayidx1144, align 8
  %xor1145 = xor i64 %585, %586
  %call1146 = call i64 @rotr64(i64 %xor1145, i32 63)
  %arrayidx1147 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1146, i64* %arrayidx1147, align 8
  br label %do.end1148

do.end1148:                                       ; preds = %do.body1109
  br label %do.body1149

do.body1149:                                      ; preds = %do.end1148
  %arrayidx1150 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %587 = load i64, i64* %arrayidx1150, align 8
  %588 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 8), align 8
  %idxprom1151 = zext i8 %588 to i32
  %arrayidx1152 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1151
  %589 = load i64, i64* %arrayidx1152, align 8
  %add1153 = add i64 %587, %589
  %arrayidx1154 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %590 = load i64, i64* %arrayidx1154, align 16
  %add1155 = add i64 %590, %add1153
  store i64 %add1155, i64* %arrayidx1154, align 16
  %arrayidx1156 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %591 = load i64, i64* %arrayidx1156, align 8
  %arrayidx1157 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %592 = load i64, i64* %arrayidx1157, align 16
  %xor1158 = xor i64 %591, %592
  %call1159 = call i64 @rotr64(i64 %xor1158, i32 32)
  %arrayidx1160 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1159, i64* %arrayidx1160, align 8
  %arrayidx1161 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %593 = load i64, i64* %arrayidx1161, align 8
  %arrayidx1162 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %594 = load i64, i64* %arrayidx1162, align 16
  %add1163 = add i64 %594, %593
  store i64 %add1163, i64* %arrayidx1162, align 16
  %arrayidx1164 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %595 = load i64, i64* %arrayidx1164, align 8
  %arrayidx1165 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %596 = load i64, i64* %arrayidx1165, align 16
  %xor1166 = xor i64 %595, %596
  %call1167 = call i64 @rotr64(i64 %xor1166, i32 24)
  %arrayidx1168 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1167, i64* %arrayidx1168, align 8
  %arrayidx1169 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %597 = load i64, i64* %arrayidx1169, align 8
  %598 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 9), align 1
  %idxprom1170 = zext i8 %598 to i32
  %arrayidx1171 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1170
  %599 = load i64, i64* %arrayidx1171, align 8
  %add1172 = add i64 %597, %599
  %arrayidx1173 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %600 = load i64, i64* %arrayidx1173, align 16
  %add1174 = add i64 %600, %add1172
  store i64 %add1174, i64* %arrayidx1173, align 16
  %arrayidx1175 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %601 = load i64, i64* %arrayidx1175, align 8
  %arrayidx1176 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %602 = load i64, i64* %arrayidx1176, align 16
  %xor1177 = xor i64 %601, %602
  %call1178 = call i64 @rotr64(i64 %xor1177, i32 16)
  %arrayidx1179 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1178, i64* %arrayidx1179, align 8
  %arrayidx1180 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %603 = load i64, i64* %arrayidx1180, align 8
  %arrayidx1181 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %604 = load i64, i64* %arrayidx1181, align 16
  %add1182 = add i64 %604, %603
  store i64 %add1182, i64* %arrayidx1181, align 16
  %arrayidx1183 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %605 = load i64, i64* %arrayidx1183, align 8
  %arrayidx1184 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %606 = load i64, i64* %arrayidx1184, align 16
  %xor1185 = xor i64 %605, %606
  %call1186 = call i64 @rotr64(i64 %xor1185, i32 63)
  %arrayidx1187 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1186, i64* %arrayidx1187, align 8
  br label %do.end1188

do.end1188:                                       ; preds = %do.body1149
  br label %do.body1189

do.body1189:                                      ; preds = %do.end1188
  %arrayidx1190 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %607 = load i64, i64* %arrayidx1190, align 16
  %608 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 10), align 2
  %idxprom1191 = zext i8 %608 to i32
  %arrayidx1192 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1191
  %609 = load i64, i64* %arrayidx1192, align 8
  %add1193 = add i64 %607, %609
  %arrayidx1194 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %610 = load i64, i64* %arrayidx1194, align 8
  %add1195 = add i64 %610, %add1193
  store i64 %add1195, i64* %arrayidx1194, align 8
  %arrayidx1196 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %611 = load i64, i64* %arrayidx1196, align 16
  %arrayidx1197 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %612 = load i64, i64* %arrayidx1197, align 8
  %xor1198 = xor i64 %611, %612
  %call1199 = call i64 @rotr64(i64 %xor1198, i32 32)
  %arrayidx1200 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1199, i64* %arrayidx1200, align 16
  %arrayidx1201 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %613 = load i64, i64* %arrayidx1201, align 16
  %arrayidx1202 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %614 = load i64, i64* %arrayidx1202, align 8
  %add1203 = add i64 %614, %613
  store i64 %add1203, i64* %arrayidx1202, align 8
  %arrayidx1204 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %615 = load i64, i64* %arrayidx1204, align 16
  %arrayidx1205 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %616 = load i64, i64* %arrayidx1205, align 8
  %xor1206 = xor i64 %615, %616
  %call1207 = call i64 @rotr64(i64 %xor1206, i32 24)
  %arrayidx1208 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1207, i64* %arrayidx1208, align 16
  %arrayidx1209 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %617 = load i64, i64* %arrayidx1209, align 16
  %618 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 11), align 1
  %idxprom1210 = zext i8 %618 to i32
  %arrayidx1211 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1210
  %619 = load i64, i64* %arrayidx1211, align 8
  %add1212 = add i64 %617, %619
  %arrayidx1213 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %620 = load i64, i64* %arrayidx1213, align 8
  %add1214 = add i64 %620, %add1212
  store i64 %add1214, i64* %arrayidx1213, align 8
  %arrayidx1215 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %621 = load i64, i64* %arrayidx1215, align 16
  %arrayidx1216 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %622 = load i64, i64* %arrayidx1216, align 8
  %xor1217 = xor i64 %621, %622
  %call1218 = call i64 @rotr64(i64 %xor1217, i32 16)
  %arrayidx1219 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1218, i64* %arrayidx1219, align 16
  %arrayidx1220 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %623 = load i64, i64* %arrayidx1220, align 16
  %arrayidx1221 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %624 = load i64, i64* %arrayidx1221, align 8
  %add1222 = add i64 %624, %623
  store i64 %add1222, i64* %arrayidx1221, align 8
  %arrayidx1223 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %625 = load i64, i64* %arrayidx1223, align 16
  %arrayidx1224 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %626 = load i64, i64* %arrayidx1224, align 8
  %xor1225 = xor i64 %625, %626
  %call1226 = call i64 @rotr64(i64 %xor1225, i32 63)
  %arrayidx1227 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1226, i64* %arrayidx1227, align 16
  br label %do.end1228

do.end1228:                                       ; preds = %do.body1189
  br label %do.body1229

do.body1229:                                      ; preds = %do.end1228
  %arrayidx1230 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %627 = load i64, i64* %arrayidx1230, align 8
  %628 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 12), align 4
  %idxprom1231 = zext i8 %628 to i32
  %arrayidx1232 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1231
  %629 = load i64, i64* %arrayidx1232, align 8
  %add1233 = add i64 %627, %629
  %arrayidx1234 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %630 = load i64, i64* %arrayidx1234, align 16
  %add1235 = add i64 %630, %add1233
  store i64 %add1235, i64* %arrayidx1234, align 16
  %arrayidx1236 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %631 = load i64, i64* %arrayidx1236, align 8
  %arrayidx1237 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %632 = load i64, i64* %arrayidx1237, align 16
  %xor1238 = xor i64 %631, %632
  %call1239 = call i64 @rotr64(i64 %xor1238, i32 32)
  %arrayidx1240 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1239, i64* %arrayidx1240, align 8
  %arrayidx1241 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %633 = load i64, i64* %arrayidx1241, align 8
  %arrayidx1242 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %634 = load i64, i64* %arrayidx1242, align 16
  %add1243 = add i64 %634, %633
  store i64 %add1243, i64* %arrayidx1242, align 16
  %arrayidx1244 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %635 = load i64, i64* %arrayidx1244, align 8
  %arrayidx1245 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %636 = load i64, i64* %arrayidx1245, align 16
  %xor1246 = xor i64 %635, %636
  %call1247 = call i64 @rotr64(i64 %xor1246, i32 24)
  %arrayidx1248 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1247, i64* %arrayidx1248, align 8
  %arrayidx1249 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %637 = load i64, i64* %arrayidx1249, align 8
  %638 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 13), align 1
  %idxprom1250 = zext i8 %638 to i32
  %arrayidx1251 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1250
  %639 = load i64, i64* %arrayidx1251, align 8
  %add1252 = add i64 %637, %639
  %arrayidx1253 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %640 = load i64, i64* %arrayidx1253, align 16
  %add1254 = add i64 %640, %add1252
  store i64 %add1254, i64* %arrayidx1253, align 16
  %arrayidx1255 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %641 = load i64, i64* %arrayidx1255, align 8
  %arrayidx1256 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %642 = load i64, i64* %arrayidx1256, align 16
  %xor1257 = xor i64 %641, %642
  %call1258 = call i64 @rotr64(i64 %xor1257, i32 16)
  %arrayidx1259 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1258, i64* %arrayidx1259, align 8
  %arrayidx1260 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %643 = load i64, i64* %arrayidx1260, align 8
  %arrayidx1261 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %644 = load i64, i64* %arrayidx1261, align 16
  %add1262 = add i64 %644, %643
  store i64 %add1262, i64* %arrayidx1261, align 16
  %arrayidx1263 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %645 = load i64, i64* %arrayidx1263, align 8
  %arrayidx1264 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %646 = load i64, i64* %arrayidx1264, align 16
  %xor1265 = xor i64 %645, %646
  %call1266 = call i64 @rotr64(i64 %xor1265, i32 63)
  %arrayidx1267 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1266, i64* %arrayidx1267, align 8
  br label %do.end1268

do.end1268:                                       ; preds = %do.body1229
  br label %do.body1269

do.body1269:                                      ; preds = %do.end1268
  %arrayidx1270 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %647 = load i64, i64* %arrayidx1270, align 16
  %648 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 14), align 2
  %idxprom1271 = zext i8 %648 to i32
  %arrayidx1272 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1271
  %649 = load i64, i64* %arrayidx1272, align 8
  %add1273 = add i64 %647, %649
  %arrayidx1274 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %650 = load i64, i64* %arrayidx1274, align 8
  %add1275 = add i64 %650, %add1273
  store i64 %add1275, i64* %arrayidx1274, align 8
  %arrayidx1276 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %651 = load i64, i64* %arrayidx1276, align 16
  %arrayidx1277 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %652 = load i64, i64* %arrayidx1277, align 8
  %xor1278 = xor i64 %651, %652
  %call1279 = call i64 @rotr64(i64 %xor1278, i32 32)
  %arrayidx1280 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1279, i64* %arrayidx1280, align 16
  %arrayidx1281 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %653 = load i64, i64* %arrayidx1281, align 16
  %arrayidx1282 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %654 = load i64, i64* %arrayidx1282, align 8
  %add1283 = add i64 %654, %653
  store i64 %add1283, i64* %arrayidx1282, align 8
  %arrayidx1284 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %655 = load i64, i64* %arrayidx1284, align 16
  %arrayidx1285 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %656 = load i64, i64* %arrayidx1285, align 8
  %xor1286 = xor i64 %655, %656
  %call1287 = call i64 @rotr64(i64 %xor1286, i32 24)
  %arrayidx1288 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1287, i64* %arrayidx1288, align 16
  %arrayidx1289 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %657 = load i64, i64* %arrayidx1289, align 16
  %658 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 3, i32 15), align 1
  %idxprom1290 = zext i8 %658 to i32
  %arrayidx1291 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1290
  %659 = load i64, i64* %arrayidx1291, align 8
  %add1292 = add i64 %657, %659
  %arrayidx1293 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %660 = load i64, i64* %arrayidx1293, align 8
  %add1294 = add i64 %660, %add1292
  store i64 %add1294, i64* %arrayidx1293, align 8
  %arrayidx1295 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %661 = load i64, i64* %arrayidx1295, align 16
  %arrayidx1296 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %662 = load i64, i64* %arrayidx1296, align 8
  %xor1297 = xor i64 %661, %662
  %call1298 = call i64 @rotr64(i64 %xor1297, i32 16)
  %arrayidx1299 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1298, i64* %arrayidx1299, align 16
  %arrayidx1300 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %663 = load i64, i64* %arrayidx1300, align 16
  %arrayidx1301 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %664 = load i64, i64* %arrayidx1301, align 8
  %add1302 = add i64 %664, %663
  store i64 %add1302, i64* %arrayidx1301, align 8
  %arrayidx1303 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %665 = load i64, i64* %arrayidx1303, align 16
  %arrayidx1304 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %666 = load i64, i64* %arrayidx1304, align 8
  %xor1305 = xor i64 %665, %666
  %call1306 = call i64 @rotr64(i64 %xor1305, i32 63)
  %arrayidx1307 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1306, i64* %arrayidx1307, align 16
  br label %do.end1308

do.end1308:                                       ; preds = %do.body1269
  br label %do.end1309

do.end1309:                                       ; preds = %do.end1308
  br label %do.body1310

do.body1310:                                      ; preds = %do.end1309
  br label %do.body1311

do.body1311:                                      ; preds = %do.body1310
  %arrayidx1312 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %667 = load i64, i64* %arrayidx1312, align 16
  %668 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 0), align 16
  %idxprom1313 = zext i8 %668 to i32
  %arrayidx1314 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1313
  %669 = load i64, i64* %arrayidx1314, align 8
  %add1315 = add i64 %667, %669
  %arrayidx1316 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %670 = load i64, i64* %arrayidx1316, align 16
  %add1317 = add i64 %670, %add1315
  store i64 %add1317, i64* %arrayidx1316, align 16
  %arrayidx1318 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %671 = load i64, i64* %arrayidx1318, align 16
  %arrayidx1319 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %672 = load i64, i64* %arrayidx1319, align 16
  %xor1320 = xor i64 %671, %672
  %call1321 = call i64 @rotr64(i64 %xor1320, i32 32)
  %arrayidx1322 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1321, i64* %arrayidx1322, align 16
  %arrayidx1323 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %673 = load i64, i64* %arrayidx1323, align 16
  %arrayidx1324 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %674 = load i64, i64* %arrayidx1324, align 16
  %add1325 = add i64 %674, %673
  store i64 %add1325, i64* %arrayidx1324, align 16
  %arrayidx1326 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %675 = load i64, i64* %arrayidx1326, align 16
  %arrayidx1327 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %676 = load i64, i64* %arrayidx1327, align 16
  %xor1328 = xor i64 %675, %676
  %call1329 = call i64 @rotr64(i64 %xor1328, i32 24)
  %arrayidx1330 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1329, i64* %arrayidx1330, align 16
  %arrayidx1331 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %677 = load i64, i64* %arrayidx1331, align 16
  %678 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 1), align 1
  %idxprom1332 = zext i8 %678 to i32
  %arrayidx1333 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1332
  %679 = load i64, i64* %arrayidx1333, align 8
  %add1334 = add i64 %677, %679
  %arrayidx1335 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %680 = load i64, i64* %arrayidx1335, align 16
  %add1336 = add i64 %680, %add1334
  store i64 %add1336, i64* %arrayidx1335, align 16
  %arrayidx1337 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %681 = load i64, i64* %arrayidx1337, align 16
  %arrayidx1338 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %682 = load i64, i64* %arrayidx1338, align 16
  %xor1339 = xor i64 %681, %682
  %call1340 = call i64 @rotr64(i64 %xor1339, i32 16)
  %arrayidx1341 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1340, i64* %arrayidx1341, align 16
  %arrayidx1342 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %683 = load i64, i64* %arrayidx1342, align 16
  %arrayidx1343 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %684 = load i64, i64* %arrayidx1343, align 16
  %add1344 = add i64 %684, %683
  store i64 %add1344, i64* %arrayidx1343, align 16
  %arrayidx1345 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %685 = load i64, i64* %arrayidx1345, align 16
  %arrayidx1346 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %686 = load i64, i64* %arrayidx1346, align 16
  %xor1347 = xor i64 %685, %686
  %call1348 = call i64 @rotr64(i64 %xor1347, i32 63)
  %arrayidx1349 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1348, i64* %arrayidx1349, align 16
  br label %do.end1350

do.end1350:                                       ; preds = %do.body1311
  br label %do.body1351

do.body1351:                                      ; preds = %do.end1350
  %arrayidx1352 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %687 = load i64, i64* %arrayidx1352, align 8
  %688 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 2), align 2
  %idxprom1353 = zext i8 %688 to i32
  %arrayidx1354 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1353
  %689 = load i64, i64* %arrayidx1354, align 8
  %add1355 = add i64 %687, %689
  %arrayidx1356 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %690 = load i64, i64* %arrayidx1356, align 8
  %add1357 = add i64 %690, %add1355
  store i64 %add1357, i64* %arrayidx1356, align 8
  %arrayidx1358 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %691 = load i64, i64* %arrayidx1358, align 8
  %arrayidx1359 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %692 = load i64, i64* %arrayidx1359, align 8
  %xor1360 = xor i64 %691, %692
  %call1361 = call i64 @rotr64(i64 %xor1360, i32 32)
  %arrayidx1362 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1361, i64* %arrayidx1362, align 8
  %arrayidx1363 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %693 = load i64, i64* %arrayidx1363, align 8
  %arrayidx1364 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %694 = load i64, i64* %arrayidx1364, align 8
  %add1365 = add i64 %694, %693
  store i64 %add1365, i64* %arrayidx1364, align 8
  %arrayidx1366 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %695 = load i64, i64* %arrayidx1366, align 8
  %arrayidx1367 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %696 = load i64, i64* %arrayidx1367, align 8
  %xor1368 = xor i64 %695, %696
  %call1369 = call i64 @rotr64(i64 %xor1368, i32 24)
  %arrayidx1370 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1369, i64* %arrayidx1370, align 8
  %arrayidx1371 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %697 = load i64, i64* %arrayidx1371, align 8
  %698 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 3), align 1
  %idxprom1372 = zext i8 %698 to i32
  %arrayidx1373 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1372
  %699 = load i64, i64* %arrayidx1373, align 8
  %add1374 = add i64 %697, %699
  %arrayidx1375 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %700 = load i64, i64* %arrayidx1375, align 8
  %add1376 = add i64 %700, %add1374
  store i64 %add1376, i64* %arrayidx1375, align 8
  %arrayidx1377 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %701 = load i64, i64* %arrayidx1377, align 8
  %arrayidx1378 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %702 = load i64, i64* %arrayidx1378, align 8
  %xor1379 = xor i64 %701, %702
  %call1380 = call i64 @rotr64(i64 %xor1379, i32 16)
  %arrayidx1381 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1380, i64* %arrayidx1381, align 8
  %arrayidx1382 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %703 = load i64, i64* %arrayidx1382, align 8
  %arrayidx1383 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %704 = load i64, i64* %arrayidx1383, align 8
  %add1384 = add i64 %704, %703
  store i64 %add1384, i64* %arrayidx1383, align 8
  %arrayidx1385 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %705 = load i64, i64* %arrayidx1385, align 8
  %arrayidx1386 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %706 = load i64, i64* %arrayidx1386, align 8
  %xor1387 = xor i64 %705, %706
  %call1388 = call i64 @rotr64(i64 %xor1387, i32 63)
  %arrayidx1389 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1388, i64* %arrayidx1389, align 8
  br label %do.end1390

do.end1390:                                       ; preds = %do.body1351
  br label %do.body1391

do.body1391:                                      ; preds = %do.end1390
  %arrayidx1392 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %707 = load i64, i64* %arrayidx1392, align 16
  %708 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 4), align 4
  %idxprom1393 = zext i8 %708 to i32
  %arrayidx1394 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1393
  %709 = load i64, i64* %arrayidx1394, align 8
  %add1395 = add i64 %707, %709
  %arrayidx1396 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %710 = load i64, i64* %arrayidx1396, align 16
  %add1397 = add i64 %710, %add1395
  store i64 %add1397, i64* %arrayidx1396, align 16
  %arrayidx1398 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %711 = load i64, i64* %arrayidx1398, align 16
  %arrayidx1399 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %712 = load i64, i64* %arrayidx1399, align 16
  %xor1400 = xor i64 %711, %712
  %call1401 = call i64 @rotr64(i64 %xor1400, i32 32)
  %arrayidx1402 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1401, i64* %arrayidx1402, align 16
  %arrayidx1403 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %713 = load i64, i64* %arrayidx1403, align 16
  %arrayidx1404 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %714 = load i64, i64* %arrayidx1404, align 16
  %add1405 = add i64 %714, %713
  store i64 %add1405, i64* %arrayidx1404, align 16
  %arrayidx1406 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %715 = load i64, i64* %arrayidx1406, align 16
  %arrayidx1407 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %716 = load i64, i64* %arrayidx1407, align 16
  %xor1408 = xor i64 %715, %716
  %call1409 = call i64 @rotr64(i64 %xor1408, i32 24)
  %arrayidx1410 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1409, i64* %arrayidx1410, align 16
  %arrayidx1411 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %717 = load i64, i64* %arrayidx1411, align 16
  %718 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 5), align 1
  %idxprom1412 = zext i8 %718 to i32
  %arrayidx1413 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1412
  %719 = load i64, i64* %arrayidx1413, align 8
  %add1414 = add i64 %717, %719
  %arrayidx1415 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %720 = load i64, i64* %arrayidx1415, align 16
  %add1416 = add i64 %720, %add1414
  store i64 %add1416, i64* %arrayidx1415, align 16
  %arrayidx1417 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %721 = load i64, i64* %arrayidx1417, align 16
  %arrayidx1418 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %722 = load i64, i64* %arrayidx1418, align 16
  %xor1419 = xor i64 %721, %722
  %call1420 = call i64 @rotr64(i64 %xor1419, i32 16)
  %arrayidx1421 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1420, i64* %arrayidx1421, align 16
  %arrayidx1422 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %723 = load i64, i64* %arrayidx1422, align 16
  %arrayidx1423 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %724 = load i64, i64* %arrayidx1423, align 16
  %add1424 = add i64 %724, %723
  store i64 %add1424, i64* %arrayidx1423, align 16
  %arrayidx1425 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %725 = load i64, i64* %arrayidx1425, align 16
  %arrayidx1426 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %726 = load i64, i64* %arrayidx1426, align 16
  %xor1427 = xor i64 %725, %726
  %call1428 = call i64 @rotr64(i64 %xor1427, i32 63)
  %arrayidx1429 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1428, i64* %arrayidx1429, align 16
  br label %do.end1430

do.end1430:                                       ; preds = %do.body1391
  br label %do.body1431

do.body1431:                                      ; preds = %do.end1430
  %arrayidx1432 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %727 = load i64, i64* %arrayidx1432, align 8
  %728 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 6), align 2
  %idxprom1433 = zext i8 %728 to i32
  %arrayidx1434 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1433
  %729 = load i64, i64* %arrayidx1434, align 8
  %add1435 = add i64 %727, %729
  %arrayidx1436 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %730 = load i64, i64* %arrayidx1436, align 8
  %add1437 = add i64 %730, %add1435
  store i64 %add1437, i64* %arrayidx1436, align 8
  %arrayidx1438 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %731 = load i64, i64* %arrayidx1438, align 8
  %arrayidx1439 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %732 = load i64, i64* %arrayidx1439, align 8
  %xor1440 = xor i64 %731, %732
  %call1441 = call i64 @rotr64(i64 %xor1440, i32 32)
  %arrayidx1442 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1441, i64* %arrayidx1442, align 8
  %arrayidx1443 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %733 = load i64, i64* %arrayidx1443, align 8
  %arrayidx1444 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %734 = load i64, i64* %arrayidx1444, align 8
  %add1445 = add i64 %734, %733
  store i64 %add1445, i64* %arrayidx1444, align 8
  %arrayidx1446 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %735 = load i64, i64* %arrayidx1446, align 8
  %arrayidx1447 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %736 = load i64, i64* %arrayidx1447, align 8
  %xor1448 = xor i64 %735, %736
  %call1449 = call i64 @rotr64(i64 %xor1448, i32 24)
  %arrayidx1450 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1449, i64* %arrayidx1450, align 8
  %arrayidx1451 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %737 = load i64, i64* %arrayidx1451, align 8
  %738 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 7), align 1
  %idxprom1452 = zext i8 %738 to i32
  %arrayidx1453 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1452
  %739 = load i64, i64* %arrayidx1453, align 8
  %add1454 = add i64 %737, %739
  %arrayidx1455 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %740 = load i64, i64* %arrayidx1455, align 8
  %add1456 = add i64 %740, %add1454
  store i64 %add1456, i64* %arrayidx1455, align 8
  %arrayidx1457 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %741 = load i64, i64* %arrayidx1457, align 8
  %arrayidx1458 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %742 = load i64, i64* %arrayidx1458, align 8
  %xor1459 = xor i64 %741, %742
  %call1460 = call i64 @rotr64(i64 %xor1459, i32 16)
  %arrayidx1461 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1460, i64* %arrayidx1461, align 8
  %arrayidx1462 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %743 = load i64, i64* %arrayidx1462, align 8
  %arrayidx1463 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %744 = load i64, i64* %arrayidx1463, align 8
  %add1464 = add i64 %744, %743
  store i64 %add1464, i64* %arrayidx1463, align 8
  %arrayidx1465 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %745 = load i64, i64* %arrayidx1465, align 8
  %arrayidx1466 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %746 = load i64, i64* %arrayidx1466, align 8
  %xor1467 = xor i64 %745, %746
  %call1468 = call i64 @rotr64(i64 %xor1467, i32 63)
  %arrayidx1469 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1468, i64* %arrayidx1469, align 8
  br label %do.end1470

do.end1470:                                       ; preds = %do.body1431
  br label %do.body1471

do.body1471:                                      ; preds = %do.end1470
  %arrayidx1472 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %747 = load i64, i64* %arrayidx1472, align 8
  %748 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 8), align 8
  %idxprom1473 = zext i8 %748 to i32
  %arrayidx1474 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1473
  %749 = load i64, i64* %arrayidx1474, align 8
  %add1475 = add i64 %747, %749
  %arrayidx1476 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %750 = load i64, i64* %arrayidx1476, align 16
  %add1477 = add i64 %750, %add1475
  store i64 %add1477, i64* %arrayidx1476, align 16
  %arrayidx1478 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %751 = load i64, i64* %arrayidx1478, align 8
  %arrayidx1479 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %752 = load i64, i64* %arrayidx1479, align 16
  %xor1480 = xor i64 %751, %752
  %call1481 = call i64 @rotr64(i64 %xor1480, i32 32)
  %arrayidx1482 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1481, i64* %arrayidx1482, align 8
  %arrayidx1483 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %753 = load i64, i64* %arrayidx1483, align 8
  %arrayidx1484 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %754 = load i64, i64* %arrayidx1484, align 16
  %add1485 = add i64 %754, %753
  store i64 %add1485, i64* %arrayidx1484, align 16
  %arrayidx1486 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %755 = load i64, i64* %arrayidx1486, align 8
  %arrayidx1487 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %756 = load i64, i64* %arrayidx1487, align 16
  %xor1488 = xor i64 %755, %756
  %call1489 = call i64 @rotr64(i64 %xor1488, i32 24)
  %arrayidx1490 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1489, i64* %arrayidx1490, align 8
  %arrayidx1491 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %757 = load i64, i64* %arrayidx1491, align 8
  %758 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 9), align 1
  %idxprom1492 = zext i8 %758 to i32
  %arrayidx1493 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1492
  %759 = load i64, i64* %arrayidx1493, align 8
  %add1494 = add i64 %757, %759
  %arrayidx1495 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %760 = load i64, i64* %arrayidx1495, align 16
  %add1496 = add i64 %760, %add1494
  store i64 %add1496, i64* %arrayidx1495, align 16
  %arrayidx1497 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %761 = load i64, i64* %arrayidx1497, align 8
  %arrayidx1498 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %762 = load i64, i64* %arrayidx1498, align 16
  %xor1499 = xor i64 %761, %762
  %call1500 = call i64 @rotr64(i64 %xor1499, i32 16)
  %arrayidx1501 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1500, i64* %arrayidx1501, align 8
  %arrayidx1502 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %763 = load i64, i64* %arrayidx1502, align 8
  %arrayidx1503 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %764 = load i64, i64* %arrayidx1503, align 16
  %add1504 = add i64 %764, %763
  store i64 %add1504, i64* %arrayidx1503, align 16
  %arrayidx1505 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %765 = load i64, i64* %arrayidx1505, align 8
  %arrayidx1506 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %766 = load i64, i64* %arrayidx1506, align 16
  %xor1507 = xor i64 %765, %766
  %call1508 = call i64 @rotr64(i64 %xor1507, i32 63)
  %arrayidx1509 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1508, i64* %arrayidx1509, align 8
  br label %do.end1510

do.end1510:                                       ; preds = %do.body1471
  br label %do.body1511

do.body1511:                                      ; preds = %do.end1510
  %arrayidx1512 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %767 = load i64, i64* %arrayidx1512, align 16
  %768 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 10), align 2
  %idxprom1513 = zext i8 %768 to i32
  %arrayidx1514 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1513
  %769 = load i64, i64* %arrayidx1514, align 8
  %add1515 = add i64 %767, %769
  %arrayidx1516 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %770 = load i64, i64* %arrayidx1516, align 8
  %add1517 = add i64 %770, %add1515
  store i64 %add1517, i64* %arrayidx1516, align 8
  %arrayidx1518 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %771 = load i64, i64* %arrayidx1518, align 16
  %arrayidx1519 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %772 = load i64, i64* %arrayidx1519, align 8
  %xor1520 = xor i64 %771, %772
  %call1521 = call i64 @rotr64(i64 %xor1520, i32 32)
  %arrayidx1522 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1521, i64* %arrayidx1522, align 16
  %arrayidx1523 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %773 = load i64, i64* %arrayidx1523, align 16
  %arrayidx1524 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %774 = load i64, i64* %arrayidx1524, align 8
  %add1525 = add i64 %774, %773
  store i64 %add1525, i64* %arrayidx1524, align 8
  %arrayidx1526 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %775 = load i64, i64* %arrayidx1526, align 16
  %arrayidx1527 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %776 = load i64, i64* %arrayidx1527, align 8
  %xor1528 = xor i64 %775, %776
  %call1529 = call i64 @rotr64(i64 %xor1528, i32 24)
  %arrayidx1530 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1529, i64* %arrayidx1530, align 16
  %arrayidx1531 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %777 = load i64, i64* %arrayidx1531, align 16
  %778 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 11), align 1
  %idxprom1532 = zext i8 %778 to i32
  %arrayidx1533 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1532
  %779 = load i64, i64* %arrayidx1533, align 8
  %add1534 = add i64 %777, %779
  %arrayidx1535 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %780 = load i64, i64* %arrayidx1535, align 8
  %add1536 = add i64 %780, %add1534
  store i64 %add1536, i64* %arrayidx1535, align 8
  %arrayidx1537 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %781 = load i64, i64* %arrayidx1537, align 16
  %arrayidx1538 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %782 = load i64, i64* %arrayidx1538, align 8
  %xor1539 = xor i64 %781, %782
  %call1540 = call i64 @rotr64(i64 %xor1539, i32 16)
  %arrayidx1541 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1540, i64* %arrayidx1541, align 16
  %arrayidx1542 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %783 = load i64, i64* %arrayidx1542, align 16
  %arrayidx1543 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %784 = load i64, i64* %arrayidx1543, align 8
  %add1544 = add i64 %784, %783
  store i64 %add1544, i64* %arrayidx1543, align 8
  %arrayidx1545 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %785 = load i64, i64* %arrayidx1545, align 16
  %arrayidx1546 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %786 = load i64, i64* %arrayidx1546, align 8
  %xor1547 = xor i64 %785, %786
  %call1548 = call i64 @rotr64(i64 %xor1547, i32 63)
  %arrayidx1549 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1548, i64* %arrayidx1549, align 16
  br label %do.end1550

do.end1550:                                       ; preds = %do.body1511
  br label %do.body1551

do.body1551:                                      ; preds = %do.end1550
  %arrayidx1552 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %787 = load i64, i64* %arrayidx1552, align 8
  %788 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 12), align 4
  %idxprom1553 = zext i8 %788 to i32
  %arrayidx1554 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1553
  %789 = load i64, i64* %arrayidx1554, align 8
  %add1555 = add i64 %787, %789
  %arrayidx1556 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %790 = load i64, i64* %arrayidx1556, align 16
  %add1557 = add i64 %790, %add1555
  store i64 %add1557, i64* %arrayidx1556, align 16
  %arrayidx1558 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %791 = load i64, i64* %arrayidx1558, align 8
  %arrayidx1559 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %792 = load i64, i64* %arrayidx1559, align 16
  %xor1560 = xor i64 %791, %792
  %call1561 = call i64 @rotr64(i64 %xor1560, i32 32)
  %arrayidx1562 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1561, i64* %arrayidx1562, align 8
  %arrayidx1563 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %793 = load i64, i64* %arrayidx1563, align 8
  %arrayidx1564 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %794 = load i64, i64* %arrayidx1564, align 16
  %add1565 = add i64 %794, %793
  store i64 %add1565, i64* %arrayidx1564, align 16
  %arrayidx1566 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %795 = load i64, i64* %arrayidx1566, align 8
  %arrayidx1567 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %796 = load i64, i64* %arrayidx1567, align 16
  %xor1568 = xor i64 %795, %796
  %call1569 = call i64 @rotr64(i64 %xor1568, i32 24)
  %arrayidx1570 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1569, i64* %arrayidx1570, align 8
  %arrayidx1571 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %797 = load i64, i64* %arrayidx1571, align 8
  %798 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 13), align 1
  %idxprom1572 = zext i8 %798 to i32
  %arrayidx1573 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1572
  %799 = load i64, i64* %arrayidx1573, align 8
  %add1574 = add i64 %797, %799
  %arrayidx1575 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %800 = load i64, i64* %arrayidx1575, align 16
  %add1576 = add i64 %800, %add1574
  store i64 %add1576, i64* %arrayidx1575, align 16
  %arrayidx1577 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %801 = load i64, i64* %arrayidx1577, align 8
  %arrayidx1578 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %802 = load i64, i64* %arrayidx1578, align 16
  %xor1579 = xor i64 %801, %802
  %call1580 = call i64 @rotr64(i64 %xor1579, i32 16)
  %arrayidx1581 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1580, i64* %arrayidx1581, align 8
  %arrayidx1582 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %803 = load i64, i64* %arrayidx1582, align 8
  %arrayidx1583 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %804 = load i64, i64* %arrayidx1583, align 16
  %add1584 = add i64 %804, %803
  store i64 %add1584, i64* %arrayidx1583, align 16
  %arrayidx1585 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %805 = load i64, i64* %arrayidx1585, align 8
  %arrayidx1586 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %806 = load i64, i64* %arrayidx1586, align 16
  %xor1587 = xor i64 %805, %806
  %call1588 = call i64 @rotr64(i64 %xor1587, i32 63)
  %arrayidx1589 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1588, i64* %arrayidx1589, align 8
  br label %do.end1590

do.end1590:                                       ; preds = %do.body1551
  br label %do.body1591

do.body1591:                                      ; preds = %do.end1590
  %arrayidx1592 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %807 = load i64, i64* %arrayidx1592, align 16
  %808 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 14), align 2
  %idxprom1593 = zext i8 %808 to i32
  %arrayidx1594 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1593
  %809 = load i64, i64* %arrayidx1594, align 8
  %add1595 = add i64 %807, %809
  %arrayidx1596 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %810 = load i64, i64* %arrayidx1596, align 8
  %add1597 = add i64 %810, %add1595
  store i64 %add1597, i64* %arrayidx1596, align 8
  %arrayidx1598 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %811 = load i64, i64* %arrayidx1598, align 16
  %arrayidx1599 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %812 = load i64, i64* %arrayidx1599, align 8
  %xor1600 = xor i64 %811, %812
  %call1601 = call i64 @rotr64(i64 %xor1600, i32 32)
  %arrayidx1602 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1601, i64* %arrayidx1602, align 16
  %arrayidx1603 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %813 = load i64, i64* %arrayidx1603, align 16
  %arrayidx1604 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %814 = load i64, i64* %arrayidx1604, align 8
  %add1605 = add i64 %814, %813
  store i64 %add1605, i64* %arrayidx1604, align 8
  %arrayidx1606 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %815 = load i64, i64* %arrayidx1606, align 16
  %arrayidx1607 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %816 = load i64, i64* %arrayidx1607, align 8
  %xor1608 = xor i64 %815, %816
  %call1609 = call i64 @rotr64(i64 %xor1608, i32 24)
  %arrayidx1610 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1609, i64* %arrayidx1610, align 16
  %arrayidx1611 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %817 = load i64, i64* %arrayidx1611, align 16
  %818 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 4, i32 15), align 1
  %idxprom1612 = zext i8 %818 to i32
  %arrayidx1613 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1612
  %819 = load i64, i64* %arrayidx1613, align 8
  %add1614 = add i64 %817, %819
  %arrayidx1615 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %820 = load i64, i64* %arrayidx1615, align 8
  %add1616 = add i64 %820, %add1614
  store i64 %add1616, i64* %arrayidx1615, align 8
  %arrayidx1617 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %821 = load i64, i64* %arrayidx1617, align 16
  %arrayidx1618 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %822 = load i64, i64* %arrayidx1618, align 8
  %xor1619 = xor i64 %821, %822
  %call1620 = call i64 @rotr64(i64 %xor1619, i32 16)
  %arrayidx1621 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1620, i64* %arrayidx1621, align 16
  %arrayidx1622 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %823 = load i64, i64* %arrayidx1622, align 16
  %arrayidx1623 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %824 = load i64, i64* %arrayidx1623, align 8
  %add1624 = add i64 %824, %823
  store i64 %add1624, i64* %arrayidx1623, align 8
  %arrayidx1625 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %825 = load i64, i64* %arrayidx1625, align 16
  %arrayidx1626 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %826 = load i64, i64* %arrayidx1626, align 8
  %xor1627 = xor i64 %825, %826
  %call1628 = call i64 @rotr64(i64 %xor1627, i32 63)
  %arrayidx1629 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1628, i64* %arrayidx1629, align 16
  br label %do.end1630

do.end1630:                                       ; preds = %do.body1591
  br label %do.end1631

do.end1631:                                       ; preds = %do.end1630
  br label %do.body1632

do.body1632:                                      ; preds = %do.end1631
  br label %do.body1633

do.body1633:                                      ; preds = %do.body1632
  %arrayidx1634 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %827 = load i64, i64* %arrayidx1634, align 16
  %828 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 0), align 16
  %idxprom1635 = zext i8 %828 to i32
  %arrayidx1636 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1635
  %829 = load i64, i64* %arrayidx1636, align 8
  %add1637 = add i64 %827, %829
  %arrayidx1638 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %830 = load i64, i64* %arrayidx1638, align 16
  %add1639 = add i64 %830, %add1637
  store i64 %add1639, i64* %arrayidx1638, align 16
  %arrayidx1640 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %831 = load i64, i64* %arrayidx1640, align 16
  %arrayidx1641 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %832 = load i64, i64* %arrayidx1641, align 16
  %xor1642 = xor i64 %831, %832
  %call1643 = call i64 @rotr64(i64 %xor1642, i32 32)
  %arrayidx1644 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1643, i64* %arrayidx1644, align 16
  %arrayidx1645 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %833 = load i64, i64* %arrayidx1645, align 16
  %arrayidx1646 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %834 = load i64, i64* %arrayidx1646, align 16
  %add1647 = add i64 %834, %833
  store i64 %add1647, i64* %arrayidx1646, align 16
  %arrayidx1648 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %835 = load i64, i64* %arrayidx1648, align 16
  %arrayidx1649 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %836 = load i64, i64* %arrayidx1649, align 16
  %xor1650 = xor i64 %835, %836
  %call1651 = call i64 @rotr64(i64 %xor1650, i32 24)
  %arrayidx1652 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1651, i64* %arrayidx1652, align 16
  %arrayidx1653 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %837 = load i64, i64* %arrayidx1653, align 16
  %838 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 1), align 1
  %idxprom1654 = zext i8 %838 to i32
  %arrayidx1655 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1654
  %839 = load i64, i64* %arrayidx1655, align 8
  %add1656 = add i64 %837, %839
  %arrayidx1657 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %840 = load i64, i64* %arrayidx1657, align 16
  %add1658 = add i64 %840, %add1656
  store i64 %add1658, i64* %arrayidx1657, align 16
  %arrayidx1659 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %841 = load i64, i64* %arrayidx1659, align 16
  %arrayidx1660 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %842 = load i64, i64* %arrayidx1660, align 16
  %xor1661 = xor i64 %841, %842
  %call1662 = call i64 @rotr64(i64 %xor1661, i32 16)
  %arrayidx1663 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1662, i64* %arrayidx1663, align 16
  %arrayidx1664 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %843 = load i64, i64* %arrayidx1664, align 16
  %arrayidx1665 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %844 = load i64, i64* %arrayidx1665, align 16
  %add1666 = add i64 %844, %843
  store i64 %add1666, i64* %arrayidx1665, align 16
  %arrayidx1667 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %845 = load i64, i64* %arrayidx1667, align 16
  %arrayidx1668 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %846 = load i64, i64* %arrayidx1668, align 16
  %xor1669 = xor i64 %845, %846
  %call1670 = call i64 @rotr64(i64 %xor1669, i32 63)
  %arrayidx1671 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1670, i64* %arrayidx1671, align 16
  br label %do.end1672

do.end1672:                                       ; preds = %do.body1633
  br label %do.body1673

do.body1673:                                      ; preds = %do.end1672
  %arrayidx1674 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %847 = load i64, i64* %arrayidx1674, align 8
  %848 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 2), align 2
  %idxprom1675 = zext i8 %848 to i32
  %arrayidx1676 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1675
  %849 = load i64, i64* %arrayidx1676, align 8
  %add1677 = add i64 %847, %849
  %arrayidx1678 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %850 = load i64, i64* %arrayidx1678, align 8
  %add1679 = add i64 %850, %add1677
  store i64 %add1679, i64* %arrayidx1678, align 8
  %arrayidx1680 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %851 = load i64, i64* %arrayidx1680, align 8
  %arrayidx1681 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %852 = load i64, i64* %arrayidx1681, align 8
  %xor1682 = xor i64 %851, %852
  %call1683 = call i64 @rotr64(i64 %xor1682, i32 32)
  %arrayidx1684 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1683, i64* %arrayidx1684, align 8
  %arrayidx1685 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %853 = load i64, i64* %arrayidx1685, align 8
  %arrayidx1686 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %854 = load i64, i64* %arrayidx1686, align 8
  %add1687 = add i64 %854, %853
  store i64 %add1687, i64* %arrayidx1686, align 8
  %arrayidx1688 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %855 = load i64, i64* %arrayidx1688, align 8
  %arrayidx1689 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %856 = load i64, i64* %arrayidx1689, align 8
  %xor1690 = xor i64 %855, %856
  %call1691 = call i64 @rotr64(i64 %xor1690, i32 24)
  %arrayidx1692 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1691, i64* %arrayidx1692, align 8
  %arrayidx1693 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %857 = load i64, i64* %arrayidx1693, align 8
  %858 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 3), align 1
  %idxprom1694 = zext i8 %858 to i32
  %arrayidx1695 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1694
  %859 = load i64, i64* %arrayidx1695, align 8
  %add1696 = add i64 %857, %859
  %arrayidx1697 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %860 = load i64, i64* %arrayidx1697, align 8
  %add1698 = add i64 %860, %add1696
  store i64 %add1698, i64* %arrayidx1697, align 8
  %arrayidx1699 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %861 = load i64, i64* %arrayidx1699, align 8
  %arrayidx1700 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %862 = load i64, i64* %arrayidx1700, align 8
  %xor1701 = xor i64 %861, %862
  %call1702 = call i64 @rotr64(i64 %xor1701, i32 16)
  %arrayidx1703 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1702, i64* %arrayidx1703, align 8
  %arrayidx1704 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %863 = load i64, i64* %arrayidx1704, align 8
  %arrayidx1705 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %864 = load i64, i64* %arrayidx1705, align 8
  %add1706 = add i64 %864, %863
  store i64 %add1706, i64* %arrayidx1705, align 8
  %arrayidx1707 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %865 = load i64, i64* %arrayidx1707, align 8
  %arrayidx1708 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %866 = load i64, i64* %arrayidx1708, align 8
  %xor1709 = xor i64 %865, %866
  %call1710 = call i64 @rotr64(i64 %xor1709, i32 63)
  %arrayidx1711 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1710, i64* %arrayidx1711, align 8
  br label %do.end1712

do.end1712:                                       ; preds = %do.body1673
  br label %do.body1713

do.body1713:                                      ; preds = %do.end1712
  %arrayidx1714 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %867 = load i64, i64* %arrayidx1714, align 16
  %868 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 4), align 4
  %idxprom1715 = zext i8 %868 to i32
  %arrayidx1716 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1715
  %869 = load i64, i64* %arrayidx1716, align 8
  %add1717 = add i64 %867, %869
  %arrayidx1718 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %870 = load i64, i64* %arrayidx1718, align 16
  %add1719 = add i64 %870, %add1717
  store i64 %add1719, i64* %arrayidx1718, align 16
  %arrayidx1720 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %871 = load i64, i64* %arrayidx1720, align 16
  %arrayidx1721 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %872 = load i64, i64* %arrayidx1721, align 16
  %xor1722 = xor i64 %871, %872
  %call1723 = call i64 @rotr64(i64 %xor1722, i32 32)
  %arrayidx1724 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1723, i64* %arrayidx1724, align 16
  %arrayidx1725 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %873 = load i64, i64* %arrayidx1725, align 16
  %arrayidx1726 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %874 = load i64, i64* %arrayidx1726, align 16
  %add1727 = add i64 %874, %873
  store i64 %add1727, i64* %arrayidx1726, align 16
  %arrayidx1728 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %875 = load i64, i64* %arrayidx1728, align 16
  %arrayidx1729 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %876 = load i64, i64* %arrayidx1729, align 16
  %xor1730 = xor i64 %875, %876
  %call1731 = call i64 @rotr64(i64 %xor1730, i32 24)
  %arrayidx1732 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1731, i64* %arrayidx1732, align 16
  %arrayidx1733 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %877 = load i64, i64* %arrayidx1733, align 16
  %878 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 5), align 1
  %idxprom1734 = zext i8 %878 to i32
  %arrayidx1735 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1734
  %879 = load i64, i64* %arrayidx1735, align 8
  %add1736 = add i64 %877, %879
  %arrayidx1737 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %880 = load i64, i64* %arrayidx1737, align 16
  %add1738 = add i64 %880, %add1736
  store i64 %add1738, i64* %arrayidx1737, align 16
  %arrayidx1739 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %881 = load i64, i64* %arrayidx1739, align 16
  %arrayidx1740 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %882 = load i64, i64* %arrayidx1740, align 16
  %xor1741 = xor i64 %881, %882
  %call1742 = call i64 @rotr64(i64 %xor1741, i32 16)
  %arrayidx1743 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1742, i64* %arrayidx1743, align 16
  %arrayidx1744 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %883 = load i64, i64* %arrayidx1744, align 16
  %arrayidx1745 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %884 = load i64, i64* %arrayidx1745, align 16
  %add1746 = add i64 %884, %883
  store i64 %add1746, i64* %arrayidx1745, align 16
  %arrayidx1747 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %885 = load i64, i64* %arrayidx1747, align 16
  %arrayidx1748 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %886 = load i64, i64* %arrayidx1748, align 16
  %xor1749 = xor i64 %885, %886
  %call1750 = call i64 @rotr64(i64 %xor1749, i32 63)
  %arrayidx1751 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1750, i64* %arrayidx1751, align 16
  br label %do.end1752

do.end1752:                                       ; preds = %do.body1713
  br label %do.body1753

do.body1753:                                      ; preds = %do.end1752
  %arrayidx1754 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %887 = load i64, i64* %arrayidx1754, align 8
  %888 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 6), align 2
  %idxprom1755 = zext i8 %888 to i32
  %arrayidx1756 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1755
  %889 = load i64, i64* %arrayidx1756, align 8
  %add1757 = add i64 %887, %889
  %arrayidx1758 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %890 = load i64, i64* %arrayidx1758, align 8
  %add1759 = add i64 %890, %add1757
  store i64 %add1759, i64* %arrayidx1758, align 8
  %arrayidx1760 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %891 = load i64, i64* %arrayidx1760, align 8
  %arrayidx1761 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %892 = load i64, i64* %arrayidx1761, align 8
  %xor1762 = xor i64 %891, %892
  %call1763 = call i64 @rotr64(i64 %xor1762, i32 32)
  %arrayidx1764 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1763, i64* %arrayidx1764, align 8
  %arrayidx1765 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %893 = load i64, i64* %arrayidx1765, align 8
  %arrayidx1766 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %894 = load i64, i64* %arrayidx1766, align 8
  %add1767 = add i64 %894, %893
  store i64 %add1767, i64* %arrayidx1766, align 8
  %arrayidx1768 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %895 = load i64, i64* %arrayidx1768, align 8
  %arrayidx1769 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %896 = load i64, i64* %arrayidx1769, align 8
  %xor1770 = xor i64 %895, %896
  %call1771 = call i64 @rotr64(i64 %xor1770, i32 24)
  %arrayidx1772 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1771, i64* %arrayidx1772, align 8
  %arrayidx1773 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %897 = load i64, i64* %arrayidx1773, align 8
  %898 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 7), align 1
  %idxprom1774 = zext i8 %898 to i32
  %arrayidx1775 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1774
  %899 = load i64, i64* %arrayidx1775, align 8
  %add1776 = add i64 %897, %899
  %arrayidx1777 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %900 = load i64, i64* %arrayidx1777, align 8
  %add1778 = add i64 %900, %add1776
  store i64 %add1778, i64* %arrayidx1777, align 8
  %arrayidx1779 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %901 = load i64, i64* %arrayidx1779, align 8
  %arrayidx1780 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %902 = load i64, i64* %arrayidx1780, align 8
  %xor1781 = xor i64 %901, %902
  %call1782 = call i64 @rotr64(i64 %xor1781, i32 16)
  %arrayidx1783 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1782, i64* %arrayidx1783, align 8
  %arrayidx1784 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %903 = load i64, i64* %arrayidx1784, align 8
  %arrayidx1785 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %904 = load i64, i64* %arrayidx1785, align 8
  %add1786 = add i64 %904, %903
  store i64 %add1786, i64* %arrayidx1785, align 8
  %arrayidx1787 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %905 = load i64, i64* %arrayidx1787, align 8
  %arrayidx1788 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %906 = load i64, i64* %arrayidx1788, align 8
  %xor1789 = xor i64 %905, %906
  %call1790 = call i64 @rotr64(i64 %xor1789, i32 63)
  %arrayidx1791 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1790, i64* %arrayidx1791, align 8
  br label %do.end1792

do.end1792:                                       ; preds = %do.body1753
  br label %do.body1793

do.body1793:                                      ; preds = %do.end1792
  %arrayidx1794 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %907 = load i64, i64* %arrayidx1794, align 8
  %908 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 8), align 8
  %idxprom1795 = zext i8 %908 to i32
  %arrayidx1796 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1795
  %909 = load i64, i64* %arrayidx1796, align 8
  %add1797 = add i64 %907, %909
  %arrayidx1798 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %910 = load i64, i64* %arrayidx1798, align 16
  %add1799 = add i64 %910, %add1797
  store i64 %add1799, i64* %arrayidx1798, align 16
  %arrayidx1800 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %911 = load i64, i64* %arrayidx1800, align 8
  %arrayidx1801 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %912 = load i64, i64* %arrayidx1801, align 16
  %xor1802 = xor i64 %911, %912
  %call1803 = call i64 @rotr64(i64 %xor1802, i32 32)
  %arrayidx1804 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1803, i64* %arrayidx1804, align 8
  %arrayidx1805 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %913 = load i64, i64* %arrayidx1805, align 8
  %arrayidx1806 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %914 = load i64, i64* %arrayidx1806, align 16
  %add1807 = add i64 %914, %913
  store i64 %add1807, i64* %arrayidx1806, align 16
  %arrayidx1808 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %915 = load i64, i64* %arrayidx1808, align 8
  %arrayidx1809 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %916 = load i64, i64* %arrayidx1809, align 16
  %xor1810 = xor i64 %915, %916
  %call1811 = call i64 @rotr64(i64 %xor1810, i32 24)
  %arrayidx1812 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1811, i64* %arrayidx1812, align 8
  %arrayidx1813 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %917 = load i64, i64* %arrayidx1813, align 8
  %918 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 9), align 1
  %idxprom1814 = zext i8 %918 to i32
  %arrayidx1815 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1814
  %919 = load i64, i64* %arrayidx1815, align 8
  %add1816 = add i64 %917, %919
  %arrayidx1817 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %920 = load i64, i64* %arrayidx1817, align 16
  %add1818 = add i64 %920, %add1816
  store i64 %add1818, i64* %arrayidx1817, align 16
  %arrayidx1819 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %921 = load i64, i64* %arrayidx1819, align 8
  %arrayidx1820 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %922 = load i64, i64* %arrayidx1820, align 16
  %xor1821 = xor i64 %921, %922
  %call1822 = call i64 @rotr64(i64 %xor1821, i32 16)
  %arrayidx1823 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call1822, i64* %arrayidx1823, align 8
  %arrayidx1824 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %923 = load i64, i64* %arrayidx1824, align 8
  %arrayidx1825 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %924 = load i64, i64* %arrayidx1825, align 16
  %add1826 = add i64 %924, %923
  store i64 %add1826, i64* %arrayidx1825, align 16
  %arrayidx1827 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %925 = load i64, i64* %arrayidx1827, align 8
  %arrayidx1828 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %926 = load i64, i64* %arrayidx1828, align 16
  %xor1829 = xor i64 %925, %926
  %call1830 = call i64 @rotr64(i64 %xor1829, i32 63)
  %arrayidx1831 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call1830, i64* %arrayidx1831, align 8
  br label %do.end1832

do.end1832:                                       ; preds = %do.body1793
  br label %do.body1833

do.body1833:                                      ; preds = %do.end1832
  %arrayidx1834 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %927 = load i64, i64* %arrayidx1834, align 16
  %928 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 10), align 2
  %idxprom1835 = zext i8 %928 to i32
  %arrayidx1836 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1835
  %929 = load i64, i64* %arrayidx1836, align 8
  %add1837 = add i64 %927, %929
  %arrayidx1838 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %930 = load i64, i64* %arrayidx1838, align 8
  %add1839 = add i64 %930, %add1837
  store i64 %add1839, i64* %arrayidx1838, align 8
  %arrayidx1840 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %931 = load i64, i64* %arrayidx1840, align 16
  %arrayidx1841 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %932 = load i64, i64* %arrayidx1841, align 8
  %xor1842 = xor i64 %931, %932
  %call1843 = call i64 @rotr64(i64 %xor1842, i32 32)
  %arrayidx1844 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1843, i64* %arrayidx1844, align 16
  %arrayidx1845 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %933 = load i64, i64* %arrayidx1845, align 16
  %arrayidx1846 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %934 = load i64, i64* %arrayidx1846, align 8
  %add1847 = add i64 %934, %933
  store i64 %add1847, i64* %arrayidx1846, align 8
  %arrayidx1848 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %935 = load i64, i64* %arrayidx1848, align 16
  %arrayidx1849 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %936 = load i64, i64* %arrayidx1849, align 8
  %xor1850 = xor i64 %935, %936
  %call1851 = call i64 @rotr64(i64 %xor1850, i32 24)
  %arrayidx1852 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1851, i64* %arrayidx1852, align 16
  %arrayidx1853 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %937 = load i64, i64* %arrayidx1853, align 16
  %938 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 11), align 1
  %idxprom1854 = zext i8 %938 to i32
  %arrayidx1855 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1854
  %939 = load i64, i64* %arrayidx1855, align 8
  %add1856 = add i64 %937, %939
  %arrayidx1857 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %940 = load i64, i64* %arrayidx1857, align 8
  %add1858 = add i64 %940, %add1856
  store i64 %add1858, i64* %arrayidx1857, align 8
  %arrayidx1859 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %941 = load i64, i64* %arrayidx1859, align 16
  %arrayidx1860 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %942 = load i64, i64* %arrayidx1860, align 8
  %xor1861 = xor i64 %941, %942
  %call1862 = call i64 @rotr64(i64 %xor1861, i32 16)
  %arrayidx1863 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1862, i64* %arrayidx1863, align 16
  %arrayidx1864 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %943 = load i64, i64* %arrayidx1864, align 16
  %arrayidx1865 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %944 = load i64, i64* %arrayidx1865, align 8
  %add1866 = add i64 %944, %943
  store i64 %add1866, i64* %arrayidx1865, align 8
  %arrayidx1867 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %945 = load i64, i64* %arrayidx1867, align 16
  %arrayidx1868 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %946 = load i64, i64* %arrayidx1868, align 8
  %xor1869 = xor i64 %945, %946
  %call1870 = call i64 @rotr64(i64 %xor1869, i32 63)
  %arrayidx1871 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call1870, i64* %arrayidx1871, align 16
  br label %do.end1872

do.end1872:                                       ; preds = %do.body1833
  br label %do.body1873

do.body1873:                                      ; preds = %do.end1872
  %arrayidx1874 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %947 = load i64, i64* %arrayidx1874, align 8
  %948 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 12), align 4
  %idxprom1875 = zext i8 %948 to i32
  %arrayidx1876 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1875
  %949 = load i64, i64* %arrayidx1876, align 8
  %add1877 = add i64 %947, %949
  %arrayidx1878 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %950 = load i64, i64* %arrayidx1878, align 16
  %add1879 = add i64 %950, %add1877
  store i64 %add1879, i64* %arrayidx1878, align 16
  %arrayidx1880 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %951 = load i64, i64* %arrayidx1880, align 8
  %arrayidx1881 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %952 = load i64, i64* %arrayidx1881, align 16
  %xor1882 = xor i64 %951, %952
  %call1883 = call i64 @rotr64(i64 %xor1882, i32 32)
  %arrayidx1884 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1883, i64* %arrayidx1884, align 8
  %arrayidx1885 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %953 = load i64, i64* %arrayidx1885, align 8
  %arrayidx1886 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %954 = load i64, i64* %arrayidx1886, align 16
  %add1887 = add i64 %954, %953
  store i64 %add1887, i64* %arrayidx1886, align 16
  %arrayidx1888 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %955 = load i64, i64* %arrayidx1888, align 8
  %arrayidx1889 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %956 = load i64, i64* %arrayidx1889, align 16
  %xor1890 = xor i64 %955, %956
  %call1891 = call i64 @rotr64(i64 %xor1890, i32 24)
  %arrayidx1892 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1891, i64* %arrayidx1892, align 8
  %arrayidx1893 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %957 = load i64, i64* %arrayidx1893, align 8
  %958 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 13), align 1
  %idxprom1894 = zext i8 %958 to i32
  %arrayidx1895 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1894
  %959 = load i64, i64* %arrayidx1895, align 8
  %add1896 = add i64 %957, %959
  %arrayidx1897 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %960 = load i64, i64* %arrayidx1897, align 16
  %add1898 = add i64 %960, %add1896
  store i64 %add1898, i64* %arrayidx1897, align 16
  %arrayidx1899 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %961 = load i64, i64* %arrayidx1899, align 8
  %arrayidx1900 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %962 = load i64, i64* %arrayidx1900, align 16
  %xor1901 = xor i64 %961, %962
  %call1902 = call i64 @rotr64(i64 %xor1901, i32 16)
  %arrayidx1903 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call1902, i64* %arrayidx1903, align 8
  %arrayidx1904 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %963 = load i64, i64* %arrayidx1904, align 8
  %arrayidx1905 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %964 = load i64, i64* %arrayidx1905, align 16
  %add1906 = add i64 %964, %963
  store i64 %add1906, i64* %arrayidx1905, align 16
  %arrayidx1907 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %965 = load i64, i64* %arrayidx1907, align 8
  %arrayidx1908 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %966 = load i64, i64* %arrayidx1908, align 16
  %xor1909 = xor i64 %965, %966
  %call1910 = call i64 @rotr64(i64 %xor1909, i32 63)
  %arrayidx1911 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call1910, i64* %arrayidx1911, align 8
  br label %do.end1912

do.end1912:                                       ; preds = %do.body1873
  br label %do.body1913

do.body1913:                                      ; preds = %do.end1912
  %arrayidx1914 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %967 = load i64, i64* %arrayidx1914, align 16
  %968 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 14), align 2
  %idxprom1915 = zext i8 %968 to i32
  %arrayidx1916 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1915
  %969 = load i64, i64* %arrayidx1916, align 8
  %add1917 = add i64 %967, %969
  %arrayidx1918 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %970 = load i64, i64* %arrayidx1918, align 8
  %add1919 = add i64 %970, %add1917
  store i64 %add1919, i64* %arrayidx1918, align 8
  %arrayidx1920 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %971 = load i64, i64* %arrayidx1920, align 16
  %arrayidx1921 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %972 = load i64, i64* %arrayidx1921, align 8
  %xor1922 = xor i64 %971, %972
  %call1923 = call i64 @rotr64(i64 %xor1922, i32 32)
  %arrayidx1924 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1923, i64* %arrayidx1924, align 16
  %arrayidx1925 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %973 = load i64, i64* %arrayidx1925, align 16
  %arrayidx1926 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %974 = load i64, i64* %arrayidx1926, align 8
  %add1927 = add i64 %974, %973
  store i64 %add1927, i64* %arrayidx1926, align 8
  %arrayidx1928 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %975 = load i64, i64* %arrayidx1928, align 16
  %arrayidx1929 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %976 = load i64, i64* %arrayidx1929, align 8
  %xor1930 = xor i64 %975, %976
  %call1931 = call i64 @rotr64(i64 %xor1930, i32 24)
  %arrayidx1932 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1931, i64* %arrayidx1932, align 16
  %arrayidx1933 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %977 = load i64, i64* %arrayidx1933, align 16
  %978 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 5, i32 15), align 1
  %idxprom1934 = zext i8 %978 to i32
  %arrayidx1935 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1934
  %979 = load i64, i64* %arrayidx1935, align 8
  %add1936 = add i64 %977, %979
  %arrayidx1937 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %980 = load i64, i64* %arrayidx1937, align 8
  %add1938 = add i64 %980, %add1936
  store i64 %add1938, i64* %arrayidx1937, align 8
  %arrayidx1939 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %981 = load i64, i64* %arrayidx1939, align 16
  %arrayidx1940 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %982 = load i64, i64* %arrayidx1940, align 8
  %xor1941 = xor i64 %981, %982
  %call1942 = call i64 @rotr64(i64 %xor1941, i32 16)
  %arrayidx1943 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call1942, i64* %arrayidx1943, align 16
  %arrayidx1944 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %983 = load i64, i64* %arrayidx1944, align 16
  %arrayidx1945 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %984 = load i64, i64* %arrayidx1945, align 8
  %add1946 = add i64 %984, %983
  store i64 %add1946, i64* %arrayidx1945, align 8
  %arrayidx1947 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %985 = load i64, i64* %arrayidx1947, align 16
  %arrayidx1948 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %986 = load i64, i64* %arrayidx1948, align 8
  %xor1949 = xor i64 %985, %986
  %call1950 = call i64 @rotr64(i64 %xor1949, i32 63)
  %arrayidx1951 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1950, i64* %arrayidx1951, align 16
  br label %do.end1952

do.end1952:                                       ; preds = %do.body1913
  br label %do.end1953

do.end1953:                                       ; preds = %do.end1952
  br label %do.body1954

do.body1954:                                      ; preds = %do.end1953
  br label %do.body1955

do.body1955:                                      ; preds = %do.body1954
  %arrayidx1956 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %987 = load i64, i64* %arrayidx1956, align 16
  %988 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 0), align 16
  %idxprom1957 = zext i8 %988 to i32
  %arrayidx1958 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1957
  %989 = load i64, i64* %arrayidx1958, align 8
  %add1959 = add i64 %987, %989
  %arrayidx1960 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %990 = load i64, i64* %arrayidx1960, align 16
  %add1961 = add i64 %990, %add1959
  store i64 %add1961, i64* %arrayidx1960, align 16
  %arrayidx1962 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %991 = load i64, i64* %arrayidx1962, align 16
  %arrayidx1963 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %992 = load i64, i64* %arrayidx1963, align 16
  %xor1964 = xor i64 %991, %992
  %call1965 = call i64 @rotr64(i64 %xor1964, i32 32)
  %arrayidx1966 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1965, i64* %arrayidx1966, align 16
  %arrayidx1967 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %993 = load i64, i64* %arrayidx1967, align 16
  %arrayidx1968 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %994 = load i64, i64* %arrayidx1968, align 16
  %add1969 = add i64 %994, %993
  store i64 %add1969, i64* %arrayidx1968, align 16
  %arrayidx1970 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %995 = load i64, i64* %arrayidx1970, align 16
  %arrayidx1971 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %996 = load i64, i64* %arrayidx1971, align 16
  %xor1972 = xor i64 %995, %996
  %call1973 = call i64 @rotr64(i64 %xor1972, i32 24)
  %arrayidx1974 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1973, i64* %arrayidx1974, align 16
  %arrayidx1975 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %997 = load i64, i64* %arrayidx1975, align 16
  %998 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 1), align 1
  %idxprom1976 = zext i8 %998 to i32
  %arrayidx1977 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1976
  %999 = load i64, i64* %arrayidx1977, align 8
  %add1978 = add i64 %997, %999
  %arrayidx1979 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1000 = load i64, i64* %arrayidx1979, align 16
  %add1980 = add i64 %1000, %add1978
  store i64 %add1980, i64* %arrayidx1979, align 16
  %arrayidx1981 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1001 = load i64, i64* %arrayidx1981, align 16
  %arrayidx1982 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1002 = load i64, i64* %arrayidx1982, align 16
  %xor1983 = xor i64 %1001, %1002
  %call1984 = call i64 @rotr64(i64 %xor1983, i32 16)
  %arrayidx1985 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call1984, i64* %arrayidx1985, align 16
  %arrayidx1986 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1003 = load i64, i64* %arrayidx1986, align 16
  %arrayidx1987 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1004 = load i64, i64* %arrayidx1987, align 16
  %add1988 = add i64 %1004, %1003
  store i64 %add1988, i64* %arrayidx1987, align 16
  %arrayidx1989 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1005 = load i64, i64* %arrayidx1989, align 16
  %arrayidx1990 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1006 = load i64, i64* %arrayidx1990, align 16
  %xor1991 = xor i64 %1005, %1006
  %call1992 = call i64 @rotr64(i64 %xor1991, i32 63)
  %arrayidx1993 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call1992, i64* %arrayidx1993, align 16
  br label %do.end1994

do.end1994:                                       ; preds = %do.body1955
  br label %do.body1995

do.body1995:                                      ; preds = %do.end1994
  %arrayidx1996 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1007 = load i64, i64* %arrayidx1996, align 8
  %1008 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 2), align 2
  %idxprom1997 = zext i8 %1008 to i32
  %arrayidx1998 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom1997
  %1009 = load i64, i64* %arrayidx1998, align 8
  %add1999 = add i64 %1007, %1009
  %arrayidx2000 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1010 = load i64, i64* %arrayidx2000, align 8
  %add2001 = add i64 %1010, %add1999
  store i64 %add2001, i64* %arrayidx2000, align 8
  %arrayidx2002 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1011 = load i64, i64* %arrayidx2002, align 8
  %arrayidx2003 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1012 = load i64, i64* %arrayidx2003, align 8
  %xor2004 = xor i64 %1011, %1012
  %call2005 = call i64 @rotr64(i64 %xor2004, i32 32)
  %arrayidx2006 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2005, i64* %arrayidx2006, align 8
  %arrayidx2007 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1013 = load i64, i64* %arrayidx2007, align 8
  %arrayidx2008 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1014 = load i64, i64* %arrayidx2008, align 8
  %add2009 = add i64 %1014, %1013
  store i64 %add2009, i64* %arrayidx2008, align 8
  %arrayidx2010 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1015 = load i64, i64* %arrayidx2010, align 8
  %arrayidx2011 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1016 = load i64, i64* %arrayidx2011, align 8
  %xor2012 = xor i64 %1015, %1016
  %call2013 = call i64 @rotr64(i64 %xor2012, i32 24)
  %arrayidx2014 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2013, i64* %arrayidx2014, align 8
  %arrayidx2015 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1017 = load i64, i64* %arrayidx2015, align 8
  %1018 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 3), align 1
  %idxprom2016 = zext i8 %1018 to i32
  %arrayidx2017 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2016
  %1019 = load i64, i64* %arrayidx2017, align 8
  %add2018 = add i64 %1017, %1019
  %arrayidx2019 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1020 = load i64, i64* %arrayidx2019, align 8
  %add2020 = add i64 %1020, %add2018
  store i64 %add2020, i64* %arrayidx2019, align 8
  %arrayidx2021 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1021 = load i64, i64* %arrayidx2021, align 8
  %arrayidx2022 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1022 = load i64, i64* %arrayidx2022, align 8
  %xor2023 = xor i64 %1021, %1022
  %call2024 = call i64 @rotr64(i64 %xor2023, i32 16)
  %arrayidx2025 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2024, i64* %arrayidx2025, align 8
  %arrayidx2026 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1023 = load i64, i64* %arrayidx2026, align 8
  %arrayidx2027 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1024 = load i64, i64* %arrayidx2027, align 8
  %add2028 = add i64 %1024, %1023
  store i64 %add2028, i64* %arrayidx2027, align 8
  %arrayidx2029 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1025 = load i64, i64* %arrayidx2029, align 8
  %arrayidx2030 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1026 = load i64, i64* %arrayidx2030, align 8
  %xor2031 = xor i64 %1025, %1026
  %call2032 = call i64 @rotr64(i64 %xor2031, i32 63)
  %arrayidx2033 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2032, i64* %arrayidx2033, align 8
  br label %do.end2034

do.end2034:                                       ; preds = %do.body1995
  br label %do.body2035

do.body2035:                                      ; preds = %do.end2034
  %arrayidx2036 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1027 = load i64, i64* %arrayidx2036, align 16
  %1028 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 4), align 4
  %idxprom2037 = zext i8 %1028 to i32
  %arrayidx2038 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2037
  %1029 = load i64, i64* %arrayidx2038, align 8
  %add2039 = add i64 %1027, %1029
  %arrayidx2040 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1030 = load i64, i64* %arrayidx2040, align 16
  %add2041 = add i64 %1030, %add2039
  store i64 %add2041, i64* %arrayidx2040, align 16
  %arrayidx2042 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1031 = load i64, i64* %arrayidx2042, align 16
  %arrayidx2043 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1032 = load i64, i64* %arrayidx2043, align 16
  %xor2044 = xor i64 %1031, %1032
  %call2045 = call i64 @rotr64(i64 %xor2044, i32 32)
  %arrayidx2046 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2045, i64* %arrayidx2046, align 16
  %arrayidx2047 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1033 = load i64, i64* %arrayidx2047, align 16
  %arrayidx2048 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1034 = load i64, i64* %arrayidx2048, align 16
  %add2049 = add i64 %1034, %1033
  store i64 %add2049, i64* %arrayidx2048, align 16
  %arrayidx2050 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1035 = load i64, i64* %arrayidx2050, align 16
  %arrayidx2051 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1036 = load i64, i64* %arrayidx2051, align 16
  %xor2052 = xor i64 %1035, %1036
  %call2053 = call i64 @rotr64(i64 %xor2052, i32 24)
  %arrayidx2054 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2053, i64* %arrayidx2054, align 16
  %arrayidx2055 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1037 = load i64, i64* %arrayidx2055, align 16
  %1038 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 5), align 1
  %idxprom2056 = zext i8 %1038 to i32
  %arrayidx2057 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2056
  %1039 = load i64, i64* %arrayidx2057, align 8
  %add2058 = add i64 %1037, %1039
  %arrayidx2059 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1040 = load i64, i64* %arrayidx2059, align 16
  %add2060 = add i64 %1040, %add2058
  store i64 %add2060, i64* %arrayidx2059, align 16
  %arrayidx2061 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1041 = load i64, i64* %arrayidx2061, align 16
  %arrayidx2062 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1042 = load i64, i64* %arrayidx2062, align 16
  %xor2063 = xor i64 %1041, %1042
  %call2064 = call i64 @rotr64(i64 %xor2063, i32 16)
  %arrayidx2065 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2064, i64* %arrayidx2065, align 16
  %arrayidx2066 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1043 = load i64, i64* %arrayidx2066, align 16
  %arrayidx2067 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1044 = load i64, i64* %arrayidx2067, align 16
  %add2068 = add i64 %1044, %1043
  store i64 %add2068, i64* %arrayidx2067, align 16
  %arrayidx2069 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1045 = load i64, i64* %arrayidx2069, align 16
  %arrayidx2070 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1046 = load i64, i64* %arrayidx2070, align 16
  %xor2071 = xor i64 %1045, %1046
  %call2072 = call i64 @rotr64(i64 %xor2071, i32 63)
  %arrayidx2073 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2072, i64* %arrayidx2073, align 16
  br label %do.end2074

do.end2074:                                       ; preds = %do.body2035
  br label %do.body2075

do.body2075:                                      ; preds = %do.end2074
  %arrayidx2076 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1047 = load i64, i64* %arrayidx2076, align 8
  %1048 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 6), align 2
  %idxprom2077 = zext i8 %1048 to i32
  %arrayidx2078 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2077
  %1049 = load i64, i64* %arrayidx2078, align 8
  %add2079 = add i64 %1047, %1049
  %arrayidx2080 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1050 = load i64, i64* %arrayidx2080, align 8
  %add2081 = add i64 %1050, %add2079
  store i64 %add2081, i64* %arrayidx2080, align 8
  %arrayidx2082 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1051 = load i64, i64* %arrayidx2082, align 8
  %arrayidx2083 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1052 = load i64, i64* %arrayidx2083, align 8
  %xor2084 = xor i64 %1051, %1052
  %call2085 = call i64 @rotr64(i64 %xor2084, i32 32)
  %arrayidx2086 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2085, i64* %arrayidx2086, align 8
  %arrayidx2087 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1053 = load i64, i64* %arrayidx2087, align 8
  %arrayidx2088 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1054 = load i64, i64* %arrayidx2088, align 8
  %add2089 = add i64 %1054, %1053
  store i64 %add2089, i64* %arrayidx2088, align 8
  %arrayidx2090 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1055 = load i64, i64* %arrayidx2090, align 8
  %arrayidx2091 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1056 = load i64, i64* %arrayidx2091, align 8
  %xor2092 = xor i64 %1055, %1056
  %call2093 = call i64 @rotr64(i64 %xor2092, i32 24)
  %arrayidx2094 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2093, i64* %arrayidx2094, align 8
  %arrayidx2095 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1057 = load i64, i64* %arrayidx2095, align 8
  %1058 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 7), align 1
  %idxprom2096 = zext i8 %1058 to i32
  %arrayidx2097 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2096
  %1059 = load i64, i64* %arrayidx2097, align 8
  %add2098 = add i64 %1057, %1059
  %arrayidx2099 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1060 = load i64, i64* %arrayidx2099, align 8
  %add2100 = add i64 %1060, %add2098
  store i64 %add2100, i64* %arrayidx2099, align 8
  %arrayidx2101 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1061 = load i64, i64* %arrayidx2101, align 8
  %arrayidx2102 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1062 = load i64, i64* %arrayidx2102, align 8
  %xor2103 = xor i64 %1061, %1062
  %call2104 = call i64 @rotr64(i64 %xor2103, i32 16)
  %arrayidx2105 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2104, i64* %arrayidx2105, align 8
  %arrayidx2106 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1063 = load i64, i64* %arrayidx2106, align 8
  %arrayidx2107 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1064 = load i64, i64* %arrayidx2107, align 8
  %add2108 = add i64 %1064, %1063
  store i64 %add2108, i64* %arrayidx2107, align 8
  %arrayidx2109 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1065 = load i64, i64* %arrayidx2109, align 8
  %arrayidx2110 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1066 = load i64, i64* %arrayidx2110, align 8
  %xor2111 = xor i64 %1065, %1066
  %call2112 = call i64 @rotr64(i64 %xor2111, i32 63)
  %arrayidx2113 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2112, i64* %arrayidx2113, align 8
  br label %do.end2114

do.end2114:                                       ; preds = %do.body2075
  br label %do.body2115

do.body2115:                                      ; preds = %do.end2114
  %arrayidx2116 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1067 = load i64, i64* %arrayidx2116, align 8
  %1068 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 8), align 8
  %idxprom2117 = zext i8 %1068 to i32
  %arrayidx2118 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2117
  %1069 = load i64, i64* %arrayidx2118, align 8
  %add2119 = add i64 %1067, %1069
  %arrayidx2120 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1070 = load i64, i64* %arrayidx2120, align 16
  %add2121 = add i64 %1070, %add2119
  store i64 %add2121, i64* %arrayidx2120, align 16
  %arrayidx2122 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1071 = load i64, i64* %arrayidx2122, align 8
  %arrayidx2123 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1072 = load i64, i64* %arrayidx2123, align 16
  %xor2124 = xor i64 %1071, %1072
  %call2125 = call i64 @rotr64(i64 %xor2124, i32 32)
  %arrayidx2126 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2125, i64* %arrayidx2126, align 8
  %arrayidx2127 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1073 = load i64, i64* %arrayidx2127, align 8
  %arrayidx2128 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1074 = load i64, i64* %arrayidx2128, align 16
  %add2129 = add i64 %1074, %1073
  store i64 %add2129, i64* %arrayidx2128, align 16
  %arrayidx2130 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1075 = load i64, i64* %arrayidx2130, align 8
  %arrayidx2131 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1076 = load i64, i64* %arrayidx2131, align 16
  %xor2132 = xor i64 %1075, %1076
  %call2133 = call i64 @rotr64(i64 %xor2132, i32 24)
  %arrayidx2134 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2133, i64* %arrayidx2134, align 8
  %arrayidx2135 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1077 = load i64, i64* %arrayidx2135, align 8
  %1078 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 9), align 1
  %idxprom2136 = zext i8 %1078 to i32
  %arrayidx2137 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2136
  %1079 = load i64, i64* %arrayidx2137, align 8
  %add2138 = add i64 %1077, %1079
  %arrayidx2139 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1080 = load i64, i64* %arrayidx2139, align 16
  %add2140 = add i64 %1080, %add2138
  store i64 %add2140, i64* %arrayidx2139, align 16
  %arrayidx2141 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1081 = load i64, i64* %arrayidx2141, align 8
  %arrayidx2142 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1082 = load i64, i64* %arrayidx2142, align 16
  %xor2143 = xor i64 %1081, %1082
  %call2144 = call i64 @rotr64(i64 %xor2143, i32 16)
  %arrayidx2145 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2144, i64* %arrayidx2145, align 8
  %arrayidx2146 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1083 = load i64, i64* %arrayidx2146, align 8
  %arrayidx2147 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1084 = load i64, i64* %arrayidx2147, align 16
  %add2148 = add i64 %1084, %1083
  store i64 %add2148, i64* %arrayidx2147, align 16
  %arrayidx2149 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1085 = load i64, i64* %arrayidx2149, align 8
  %arrayidx2150 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1086 = load i64, i64* %arrayidx2150, align 16
  %xor2151 = xor i64 %1085, %1086
  %call2152 = call i64 @rotr64(i64 %xor2151, i32 63)
  %arrayidx2153 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2152, i64* %arrayidx2153, align 8
  br label %do.end2154

do.end2154:                                       ; preds = %do.body2115
  br label %do.body2155

do.body2155:                                      ; preds = %do.end2154
  %arrayidx2156 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1087 = load i64, i64* %arrayidx2156, align 16
  %1088 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 10), align 2
  %idxprom2157 = zext i8 %1088 to i32
  %arrayidx2158 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2157
  %1089 = load i64, i64* %arrayidx2158, align 8
  %add2159 = add i64 %1087, %1089
  %arrayidx2160 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1090 = load i64, i64* %arrayidx2160, align 8
  %add2161 = add i64 %1090, %add2159
  store i64 %add2161, i64* %arrayidx2160, align 8
  %arrayidx2162 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1091 = load i64, i64* %arrayidx2162, align 16
  %arrayidx2163 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1092 = load i64, i64* %arrayidx2163, align 8
  %xor2164 = xor i64 %1091, %1092
  %call2165 = call i64 @rotr64(i64 %xor2164, i32 32)
  %arrayidx2166 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2165, i64* %arrayidx2166, align 16
  %arrayidx2167 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1093 = load i64, i64* %arrayidx2167, align 16
  %arrayidx2168 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1094 = load i64, i64* %arrayidx2168, align 8
  %add2169 = add i64 %1094, %1093
  store i64 %add2169, i64* %arrayidx2168, align 8
  %arrayidx2170 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1095 = load i64, i64* %arrayidx2170, align 16
  %arrayidx2171 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1096 = load i64, i64* %arrayidx2171, align 8
  %xor2172 = xor i64 %1095, %1096
  %call2173 = call i64 @rotr64(i64 %xor2172, i32 24)
  %arrayidx2174 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2173, i64* %arrayidx2174, align 16
  %arrayidx2175 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1097 = load i64, i64* %arrayidx2175, align 16
  %1098 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 11), align 1
  %idxprom2176 = zext i8 %1098 to i32
  %arrayidx2177 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2176
  %1099 = load i64, i64* %arrayidx2177, align 8
  %add2178 = add i64 %1097, %1099
  %arrayidx2179 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1100 = load i64, i64* %arrayidx2179, align 8
  %add2180 = add i64 %1100, %add2178
  store i64 %add2180, i64* %arrayidx2179, align 8
  %arrayidx2181 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1101 = load i64, i64* %arrayidx2181, align 16
  %arrayidx2182 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1102 = load i64, i64* %arrayidx2182, align 8
  %xor2183 = xor i64 %1101, %1102
  %call2184 = call i64 @rotr64(i64 %xor2183, i32 16)
  %arrayidx2185 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2184, i64* %arrayidx2185, align 16
  %arrayidx2186 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1103 = load i64, i64* %arrayidx2186, align 16
  %arrayidx2187 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1104 = load i64, i64* %arrayidx2187, align 8
  %add2188 = add i64 %1104, %1103
  store i64 %add2188, i64* %arrayidx2187, align 8
  %arrayidx2189 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1105 = load i64, i64* %arrayidx2189, align 16
  %arrayidx2190 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1106 = load i64, i64* %arrayidx2190, align 8
  %xor2191 = xor i64 %1105, %1106
  %call2192 = call i64 @rotr64(i64 %xor2191, i32 63)
  %arrayidx2193 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2192, i64* %arrayidx2193, align 16
  br label %do.end2194

do.end2194:                                       ; preds = %do.body2155
  br label %do.body2195

do.body2195:                                      ; preds = %do.end2194
  %arrayidx2196 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1107 = load i64, i64* %arrayidx2196, align 8
  %1108 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 12), align 4
  %idxprom2197 = zext i8 %1108 to i32
  %arrayidx2198 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2197
  %1109 = load i64, i64* %arrayidx2198, align 8
  %add2199 = add i64 %1107, %1109
  %arrayidx2200 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1110 = load i64, i64* %arrayidx2200, align 16
  %add2201 = add i64 %1110, %add2199
  store i64 %add2201, i64* %arrayidx2200, align 16
  %arrayidx2202 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1111 = load i64, i64* %arrayidx2202, align 8
  %arrayidx2203 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1112 = load i64, i64* %arrayidx2203, align 16
  %xor2204 = xor i64 %1111, %1112
  %call2205 = call i64 @rotr64(i64 %xor2204, i32 32)
  %arrayidx2206 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2205, i64* %arrayidx2206, align 8
  %arrayidx2207 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1113 = load i64, i64* %arrayidx2207, align 8
  %arrayidx2208 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1114 = load i64, i64* %arrayidx2208, align 16
  %add2209 = add i64 %1114, %1113
  store i64 %add2209, i64* %arrayidx2208, align 16
  %arrayidx2210 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1115 = load i64, i64* %arrayidx2210, align 8
  %arrayidx2211 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1116 = load i64, i64* %arrayidx2211, align 16
  %xor2212 = xor i64 %1115, %1116
  %call2213 = call i64 @rotr64(i64 %xor2212, i32 24)
  %arrayidx2214 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2213, i64* %arrayidx2214, align 8
  %arrayidx2215 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1117 = load i64, i64* %arrayidx2215, align 8
  %1118 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 13), align 1
  %idxprom2216 = zext i8 %1118 to i32
  %arrayidx2217 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2216
  %1119 = load i64, i64* %arrayidx2217, align 8
  %add2218 = add i64 %1117, %1119
  %arrayidx2219 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1120 = load i64, i64* %arrayidx2219, align 16
  %add2220 = add i64 %1120, %add2218
  store i64 %add2220, i64* %arrayidx2219, align 16
  %arrayidx2221 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1121 = load i64, i64* %arrayidx2221, align 8
  %arrayidx2222 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1122 = load i64, i64* %arrayidx2222, align 16
  %xor2223 = xor i64 %1121, %1122
  %call2224 = call i64 @rotr64(i64 %xor2223, i32 16)
  %arrayidx2225 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2224, i64* %arrayidx2225, align 8
  %arrayidx2226 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1123 = load i64, i64* %arrayidx2226, align 8
  %arrayidx2227 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1124 = load i64, i64* %arrayidx2227, align 16
  %add2228 = add i64 %1124, %1123
  store i64 %add2228, i64* %arrayidx2227, align 16
  %arrayidx2229 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1125 = load i64, i64* %arrayidx2229, align 8
  %arrayidx2230 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1126 = load i64, i64* %arrayidx2230, align 16
  %xor2231 = xor i64 %1125, %1126
  %call2232 = call i64 @rotr64(i64 %xor2231, i32 63)
  %arrayidx2233 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2232, i64* %arrayidx2233, align 8
  br label %do.end2234

do.end2234:                                       ; preds = %do.body2195
  br label %do.body2235

do.body2235:                                      ; preds = %do.end2234
  %arrayidx2236 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1127 = load i64, i64* %arrayidx2236, align 16
  %1128 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 14), align 2
  %idxprom2237 = zext i8 %1128 to i32
  %arrayidx2238 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2237
  %1129 = load i64, i64* %arrayidx2238, align 8
  %add2239 = add i64 %1127, %1129
  %arrayidx2240 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1130 = load i64, i64* %arrayidx2240, align 8
  %add2241 = add i64 %1130, %add2239
  store i64 %add2241, i64* %arrayidx2240, align 8
  %arrayidx2242 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1131 = load i64, i64* %arrayidx2242, align 16
  %arrayidx2243 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1132 = load i64, i64* %arrayidx2243, align 8
  %xor2244 = xor i64 %1131, %1132
  %call2245 = call i64 @rotr64(i64 %xor2244, i32 32)
  %arrayidx2246 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2245, i64* %arrayidx2246, align 16
  %arrayidx2247 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1133 = load i64, i64* %arrayidx2247, align 16
  %arrayidx2248 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1134 = load i64, i64* %arrayidx2248, align 8
  %add2249 = add i64 %1134, %1133
  store i64 %add2249, i64* %arrayidx2248, align 8
  %arrayidx2250 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1135 = load i64, i64* %arrayidx2250, align 16
  %arrayidx2251 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1136 = load i64, i64* %arrayidx2251, align 8
  %xor2252 = xor i64 %1135, %1136
  %call2253 = call i64 @rotr64(i64 %xor2252, i32 24)
  %arrayidx2254 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2253, i64* %arrayidx2254, align 16
  %arrayidx2255 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1137 = load i64, i64* %arrayidx2255, align 16
  %1138 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 6, i32 15), align 1
  %idxprom2256 = zext i8 %1138 to i32
  %arrayidx2257 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2256
  %1139 = load i64, i64* %arrayidx2257, align 8
  %add2258 = add i64 %1137, %1139
  %arrayidx2259 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1140 = load i64, i64* %arrayidx2259, align 8
  %add2260 = add i64 %1140, %add2258
  store i64 %add2260, i64* %arrayidx2259, align 8
  %arrayidx2261 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1141 = load i64, i64* %arrayidx2261, align 16
  %arrayidx2262 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1142 = load i64, i64* %arrayidx2262, align 8
  %xor2263 = xor i64 %1141, %1142
  %call2264 = call i64 @rotr64(i64 %xor2263, i32 16)
  %arrayidx2265 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2264, i64* %arrayidx2265, align 16
  %arrayidx2266 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1143 = load i64, i64* %arrayidx2266, align 16
  %arrayidx2267 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1144 = load i64, i64* %arrayidx2267, align 8
  %add2268 = add i64 %1144, %1143
  store i64 %add2268, i64* %arrayidx2267, align 8
  %arrayidx2269 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1145 = load i64, i64* %arrayidx2269, align 16
  %arrayidx2270 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1146 = load i64, i64* %arrayidx2270, align 8
  %xor2271 = xor i64 %1145, %1146
  %call2272 = call i64 @rotr64(i64 %xor2271, i32 63)
  %arrayidx2273 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2272, i64* %arrayidx2273, align 16
  br label %do.end2274

do.end2274:                                       ; preds = %do.body2235
  br label %do.end2275

do.end2275:                                       ; preds = %do.end2274
  br label %do.body2276

do.body2276:                                      ; preds = %do.end2275
  br label %do.body2277

do.body2277:                                      ; preds = %do.body2276
  %arrayidx2278 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1147 = load i64, i64* %arrayidx2278, align 16
  %1148 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 0), align 16
  %idxprom2279 = zext i8 %1148 to i32
  %arrayidx2280 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2279
  %1149 = load i64, i64* %arrayidx2280, align 8
  %add2281 = add i64 %1147, %1149
  %arrayidx2282 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1150 = load i64, i64* %arrayidx2282, align 16
  %add2283 = add i64 %1150, %add2281
  store i64 %add2283, i64* %arrayidx2282, align 16
  %arrayidx2284 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1151 = load i64, i64* %arrayidx2284, align 16
  %arrayidx2285 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1152 = load i64, i64* %arrayidx2285, align 16
  %xor2286 = xor i64 %1151, %1152
  %call2287 = call i64 @rotr64(i64 %xor2286, i32 32)
  %arrayidx2288 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2287, i64* %arrayidx2288, align 16
  %arrayidx2289 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1153 = load i64, i64* %arrayidx2289, align 16
  %arrayidx2290 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1154 = load i64, i64* %arrayidx2290, align 16
  %add2291 = add i64 %1154, %1153
  store i64 %add2291, i64* %arrayidx2290, align 16
  %arrayidx2292 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1155 = load i64, i64* %arrayidx2292, align 16
  %arrayidx2293 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1156 = load i64, i64* %arrayidx2293, align 16
  %xor2294 = xor i64 %1155, %1156
  %call2295 = call i64 @rotr64(i64 %xor2294, i32 24)
  %arrayidx2296 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2295, i64* %arrayidx2296, align 16
  %arrayidx2297 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1157 = load i64, i64* %arrayidx2297, align 16
  %1158 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 1), align 1
  %idxprom2298 = zext i8 %1158 to i32
  %arrayidx2299 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2298
  %1159 = load i64, i64* %arrayidx2299, align 8
  %add2300 = add i64 %1157, %1159
  %arrayidx2301 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1160 = load i64, i64* %arrayidx2301, align 16
  %add2302 = add i64 %1160, %add2300
  store i64 %add2302, i64* %arrayidx2301, align 16
  %arrayidx2303 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1161 = load i64, i64* %arrayidx2303, align 16
  %arrayidx2304 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1162 = load i64, i64* %arrayidx2304, align 16
  %xor2305 = xor i64 %1161, %1162
  %call2306 = call i64 @rotr64(i64 %xor2305, i32 16)
  %arrayidx2307 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2306, i64* %arrayidx2307, align 16
  %arrayidx2308 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1163 = load i64, i64* %arrayidx2308, align 16
  %arrayidx2309 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1164 = load i64, i64* %arrayidx2309, align 16
  %add2310 = add i64 %1164, %1163
  store i64 %add2310, i64* %arrayidx2309, align 16
  %arrayidx2311 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1165 = load i64, i64* %arrayidx2311, align 16
  %arrayidx2312 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1166 = load i64, i64* %arrayidx2312, align 16
  %xor2313 = xor i64 %1165, %1166
  %call2314 = call i64 @rotr64(i64 %xor2313, i32 63)
  %arrayidx2315 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2314, i64* %arrayidx2315, align 16
  br label %do.end2316

do.end2316:                                       ; preds = %do.body2277
  br label %do.body2317

do.body2317:                                      ; preds = %do.end2316
  %arrayidx2318 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1167 = load i64, i64* %arrayidx2318, align 8
  %1168 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 2), align 2
  %idxprom2319 = zext i8 %1168 to i32
  %arrayidx2320 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2319
  %1169 = load i64, i64* %arrayidx2320, align 8
  %add2321 = add i64 %1167, %1169
  %arrayidx2322 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1170 = load i64, i64* %arrayidx2322, align 8
  %add2323 = add i64 %1170, %add2321
  store i64 %add2323, i64* %arrayidx2322, align 8
  %arrayidx2324 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1171 = load i64, i64* %arrayidx2324, align 8
  %arrayidx2325 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1172 = load i64, i64* %arrayidx2325, align 8
  %xor2326 = xor i64 %1171, %1172
  %call2327 = call i64 @rotr64(i64 %xor2326, i32 32)
  %arrayidx2328 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2327, i64* %arrayidx2328, align 8
  %arrayidx2329 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1173 = load i64, i64* %arrayidx2329, align 8
  %arrayidx2330 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1174 = load i64, i64* %arrayidx2330, align 8
  %add2331 = add i64 %1174, %1173
  store i64 %add2331, i64* %arrayidx2330, align 8
  %arrayidx2332 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1175 = load i64, i64* %arrayidx2332, align 8
  %arrayidx2333 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1176 = load i64, i64* %arrayidx2333, align 8
  %xor2334 = xor i64 %1175, %1176
  %call2335 = call i64 @rotr64(i64 %xor2334, i32 24)
  %arrayidx2336 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2335, i64* %arrayidx2336, align 8
  %arrayidx2337 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1177 = load i64, i64* %arrayidx2337, align 8
  %1178 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 3), align 1
  %idxprom2338 = zext i8 %1178 to i32
  %arrayidx2339 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2338
  %1179 = load i64, i64* %arrayidx2339, align 8
  %add2340 = add i64 %1177, %1179
  %arrayidx2341 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1180 = load i64, i64* %arrayidx2341, align 8
  %add2342 = add i64 %1180, %add2340
  store i64 %add2342, i64* %arrayidx2341, align 8
  %arrayidx2343 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1181 = load i64, i64* %arrayidx2343, align 8
  %arrayidx2344 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1182 = load i64, i64* %arrayidx2344, align 8
  %xor2345 = xor i64 %1181, %1182
  %call2346 = call i64 @rotr64(i64 %xor2345, i32 16)
  %arrayidx2347 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2346, i64* %arrayidx2347, align 8
  %arrayidx2348 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1183 = load i64, i64* %arrayidx2348, align 8
  %arrayidx2349 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1184 = load i64, i64* %arrayidx2349, align 8
  %add2350 = add i64 %1184, %1183
  store i64 %add2350, i64* %arrayidx2349, align 8
  %arrayidx2351 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1185 = load i64, i64* %arrayidx2351, align 8
  %arrayidx2352 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1186 = load i64, i64* %arrayidx2352, align 8
  %xor2353 = xor i64 %1185, %1186
  %call2354 = call i64 @rotr64(i64 %xor2353, i32 63)
  %arrayidx2355 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2354, i64* %arrayidx2355, align 8
  br label %do.end2356

do.end2356:                                       ; preds = %do.body2317
  br label %do.body2357

do.body2357:                                      ; preds = %do.end2356
  %arrayidx2358 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1187 = load i64, i64* %arrayidx2358, align 16
  %1188 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 4), align 4
  %idxprom2359 = zext i8 %1188 to i32
  %arrayidx2360 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2359
  %1189 = load i64, i64* %arrayidx2360, align 8
  %add2361 = add i64 %1187, %1189
  %arrayidx2362 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1190 = load i64, i64* %arrayidx2362, align 16
  %add2363 = add i64 %1190, %add2361
  store i64 %add2363, i64* %arrayidx2362, align 16
  %arrayidx2364 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1191 = load i64, i64* %arrayidx2364, align 16
  %arrayidx2365 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1192 = load i64, i64* %arrayidx2365, align 16
  %xor2366 = xor i64 %1191, %1192
  %call2367 = call i64 @rotr64(i64 %xor2366, i32 32)
  %arrayidx2368 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2367, i64* %arrayidx2368, align 16
  %arrayidx2369 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1193 = load i64, i64* %arrayidx2369, align 16
  %arrayidx2370 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1194 = load i64, i64* %arrayidx2370, align 16
  %add2371 = add i64 %1194, %1193
  store i64 %add2371, i64* %arrayidx2370, align 16
  %arrayidx2372 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1195 = load i64, i64* %arrayidx2372, align 16
  %arrayidx2373 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1196 = load i64, i64* %arrayidx2373, align 16
  %xor2374 = xor i64 %1195, %1196
  %call2375 = call i64 @rotr64(i64 %xor2374, i32 24)
  %arrayidx2376 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2375, i64* %arrayidx2376, align 16
  %arrayidx2377 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1197 = load i64, i64* %arrayidx2377, align 16
  %1198 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 5), align 1
  %idxprom2378 = zext i8 %1198 to i32
  %arrayidx2379 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2378
  %1199 = load i64, i64* %arrayidx2379, align 8
  %add2380 = add i64 %1197, %1199
  %arrayidx2381 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1200 = load i64, i64* %arrayidx2381, align 16
  %add2382 = add i64 %1200, %add2380
  store i64 %add2382, i64* %arrayidx2381, align 16
  %arrayidx2383 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1201 = load i64, i64* %arrayidx2383, align 16
  %arrayidx2384 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1202 = load i64, i64* %arrayidx2384, align 16
  %xor2385 = xor i64 %1201, %1202
  %call2386 = call i64 @rotr64(i64 %xor2385, i32 16)
  %arrayidx2387 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2386, i64* %arrayidx2387, align 16
  %arrayidx2388 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1203 = load i64, i64* %arrayidx2388, align 16
  %arrayidx2389 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1204 = load i64, i64* %arrayidx2389, align 16
  %add2390 = add i64 %1204, %1203
  store i64 %add2390, i64* %arrayidx2389, align 16
  %arrayidx2391 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1205 = load i64, i64* %arrayidx2391, align 16
  %arrayidx2392 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1206 = load i64, i64* %arrayidx2392, align 16
  %xor2393 = xor i64 %1205, %1206
  %call2394 = call i64 @rotr64(i64 %xor2393, i32 63)
  %arrayidx2395 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2394, i64* %arrayidx2395, align 16
  br label %do.end2396

do.end2396:                                       ; preds = %do.body2357
  br label %do.body2397

do.body2397:                                      ; preds = %do.end2396
  %arrayidx2398 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1207 = load i64, i64* %arrayidx2398, align 8
  %1208 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 6), align 2
  %idxprom2399 = zext i8 %1208 to i32
  %arrayidx2400 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2399
  %1209 = load i64, i64* %arrayidx2400, align 8
  %add2401 = add i64 %1207, %1209
  %arrayidx2402 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1210 = load i64, i64* %arrayidx2402, align 8
  %add2403 = add i64 %1210, %add2401
  store i64 %add2403, i64* %arrayidx2402, align 8
  %arrayidx2404 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1211 = load i64, i64* %arrayidx2404, align 8
  %arrayidx2405 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1212 = load i64, i64* %arrayidx2405, align 8
  %xor2406 = xor i64 %1211, %1212
  %call2407 = call i64 @rotr64(i64 %xor2406, i32 32)
  %arrayidx2408 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2407, i64* %arrayidx2408, align 8
  %arrayidx2409 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1213 = load i64, i64* %arrayidx2409, align 8
  %arrayidx2410 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1214 = load i64, i64* %arrayidx2410, align 8
  %add2411 = add i64 %1214, %1213
  store i64 %add2411, i64* %arrayidx2410, align 8
  %arrayidx2412 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1215 = load i64, i64* %arrayidx2412, align 8
  %arrayidx2413 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1216 = load i64, i64* %arrayidx2413, align 8
  %xor2414 = xor i64 %1215, %1216
  %call2415 = call i64 @rotr64(i64 %xor2414, i32 24)
  %arrayidx2416 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2415, i64* %arrayidx2416, align 8
  %arrayidx2417 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1217 = load i64, i64* %arrayidx2417, align 8
  %1218 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 7), align 1
  %idxprom2418 = zext i8 %1218 to i32
  %arrayidx2419 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2418
  %1219 = load i64, i64* %arrayidx2419, align 8
  %add2420 = add i64 %1217, %1219
  %arrayidx2421 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1220 = load i64, i64* %arrayidx2421, align 8
  %add2422 = add i64 %1220, %add2420
  store i64 %add2422, i64* %arrayidx2421, align 8
  %arrayidx2423 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1221 = load i64, i64* %arrayidx2423, align 8
  %arrayidx2424 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1222 = load i64, i64* %arrayidx2424, align 8
  %xor2425 = xor i64 %1221, %1222
  %call2426 = call i64 @rotr64(i64 %xor2425, i32 16)
  %arrayidx2427 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2426, i64* %arrayidx2427, align 8
  %arrayidx2428 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1223 = load i64, i64* %arrayidx2428, align 8
  %arrayidx2429 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1224 = load i64, i64* %arrayidx2429, align 8
  %add2430 = add i64 %1224, %1223
  store i64 %add2430, i64* %arrayidx2429, align 8
  %arrayidx2431 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1225 = load i64, i64* %arrayidx2431, align 8
  %arrayidx2432 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1226 = load i64, i64* %arrayidx2432, align 8
  %xor2433 = xor i64 %1225, %1226
  %call2434 = call i64 @rotr64(i64 %xor2433, i32 63)
  %arrayidx2435 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2434, i64* %arrayidx2435, align 8
  br label %do.end2436

do.end2436:                                       ; preds = %do.body2397
  br label %do.body2437

do.body2437:                                      ; preds = %do.end2436
  %arrayidx2438 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1227 = load i64, i64* %arrayidx2438, align 8
  %1228 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 8), align 8
  %idxprom2439 = zext i8 %1228 to i32
  %arrayidx2440 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2439
  %1229 = load i64, i64* %arrayidx2440, align 8
  %add2441 = add i64 %1227, %1229
  %arrayidx2442 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1230 = load i64, i64* %arrayidx2442, align 16
  %add2443 = add i64 %1230, %add2441
  store i64 %add2443, i64* %arrayidx2442, align 16
  %arrayidx2444 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1231 = load i64, i64* %arrayidx2444, align 8
  %arrayidx2445 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1232 = load i64, i64* %arrayidx2445, align 16
  %xor2446 = xor i64 %1231, %1232
  %call2447 = call i64 @rotr64(i64 %xor2446, i32 32)
  %arrayidx2448 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2447, i64* %arrayidx2448, align 8
  %arrayidx2449 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1233 = load i64, i64* %arrayidx2449, align 8
  %arrayidx2450 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1234 = load i64, i64* %arrayidx2450, align 16
  %add2451 = add i64 %1234, %1233
  store i64 %add2451, i64* %arrayidx2450, align 16
  %arrayidx2452 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1235 = load i64, i64* %arrayidx2452, align 8
  %arrayidx2453 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1236 = load i64, i64* %arrayidx2453, align 16
  %xor2454 = xor i64 %1235, %1236
  %call2455 = call i64 @rotr64(i64 %xor2454, i32 24)
  %arrayidx2456 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2455, i64* %arrayidx2456, align 8
  %arrayidx2457 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1237 = load i64, i64* %arrayidx2457, align 8
  %1238 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 9), align 1
  %idxprom2458 = zext i8 %1238 to i32
  %arrayidx2459 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2458
  %1239 = load i64, i64* %arrayidx2459, align 8
  %add2460 = add i64 %1237, %1239
  %arrayidx2461 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1240 = load i64, i64* %arrayidx2461, align 16
  %add2462 = add i64 %1240, %add2460
  store i64 %add2462, i64* %arrayidx2461, align 16
  %arrayidx2463 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1241 = load i64, i64* %arrayidx2463, align 8
  %arrayidx2464 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1242 = load i64, i64* %arrayidx2464, align 16
  %xor2465 = xor i64 %1241, %1242
  %call2466 = call i64 @rotr64(i64 %xor2465, i32 16)
  %arrayidx2467 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2466, i64* %arrayidx2467, align 8
  %arrayidx2468 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1243 = load i64, i64* %arrayidx2468, align 8
  %arrayidx2469 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1244 = load i64, i64* %arrayidx2469, align 16
  %add2470 = add i64 %1244, %1243
  store i64 %add2470, i64* %arrayidx2469, align 16
  %arrayidx2471 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1245 = load i64, i64* %arrayidx2471, align 8
  %arrayidx2472 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1246 = load i64, i64* %arrayidx2472, align 16
  %xor2473 = xor i64 %1245, %1246
  %call2474 = call i64 @rotr64(i64 %xor2473, i32 63)
  %arrayidx2475 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2474, i64* %arrayidx2475, align 8
  br label %do.end2476

do.end2476:                                       ; preds = %do.body2437
  br label %do.body2477

do.body2477:                                      ; preds = %do.end2476
  %arrayidx2478 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1247 = load i64, i64* %arrayidx2478, align 16
  %1248 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 10), align 2
  %idxprom2479 = zext i8 %1248 to i32
  %arrayidx2480 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2479
  %1249 = load i64, i64* %arrayidx2480, align 8
  %add2481 = add i64 %1247, %1249
  %arrayidx2482 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1250 = load i64, i64* %arrayidx2482, align 8
  %add2483 = add i64 %1250, %add2481
  store i64 %add2483, i64* %arrayidx2482, align 8
  %arrayidx2484 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1251 = load i64, i64* %arrayidx2484, align 16
  %arrayidx2485 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1252 = load i64, i64* %arrayidx2485, align 8
  %xor2486 = xor i64 %1251, %1252
  %call2487 = call i64 @rotr64(i64 %xor2486, i32 32)
  %arrayidx2488 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2487, i64* %arrayidx2488, align 16
  %arrayidx2489 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1253 = load i64, i64* %arrayidx2489, align 16
  %arrayidx2490 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1254 = load i64, i64* %arrayidx2490, align 8
  %add2491 = add i64 %1254, %1253
  store i64 %add2491, i64* %arrayidx2490, align 8
  %arrayidx2492 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1255 = load i64, i64* %arrayidx2492, align 16
  %arrayidx2493 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1256 = load i64, i64* %arrayidx2493, align 8
  %xor2494 = xor i64 %1255, %1256
  %call2495 = call i64 @rotr64(i64 %xor2494, i32 24)
  %arrayidx2496 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2495, i64* %arrayidx2496, align 16
  %arrayidx2497 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1257 = load i64, i64* %arrayidx2497, align 16
  %1258 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 11), align 1
  %idxprom2498 = zext i8 %1258 to i32
  %arrayidx2499 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2498
  %1259 = load i64, i64* %arrayidx2499, align 8
  %add2500 = add i64 %1257, %1259
  %arrayidx2501 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1260 = load i64, i64* %arrayidx2501, align 8
  %add2502 = add i64 %1260, %add2500
  store i64 %add2502, i64* %arrayidx2501, align 8
  %arrayidx2503 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1261 = load i64, i64* %arrayidx2503, align 16
  %arrayidx2504 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1262 = load i64, i64* %arrayidx2504, align 8
  %xor2505 = xor i64 %1261, %1262
  %call2506 = call i64 @rotr64(i64 %xor2505, i32 16)
  %arrayidx2507 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2506, i64* %arrayidx2507, align 16
  %arrayidx2508 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1263 = load i64, i64* %arrayidx2508, align 16
  %arrayidx2509 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1264 = load i64, i64* %arrayidx2509, align 8
  %add2510 = add i64 %1264, %1263
  store i64 %add2510, i64* %arrayidx2509, align 8
  %arrayidx2511 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1265 = load i64, i64* %arrayidx2511, align 16
  %arrayidx2512 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1266 = load i64, i64* %arrayidx2512, align 8
  %xor2513 = xor i64 %1265, %1266
  %call2514 = call i64 @rotr64(i64 %xor2513, i32 63)
  %arrayidx2515 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2514, i64* %arrayidx2515, align 16
  br label %do.end2516

do.end2516:                                       ; preds = %do.body2477
  br label %do.body2517

do.body2517:                                      ; preds = %do.end2516
  %arrayidx2518 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1267 = load i64, i64* %arrayidx2518, align 8
  %1268 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 12), align 4
  %idxprom2519 = zext i8 %1268 to i32
  %arrayidx2520 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2519
  %1269 = load i64, i64* %arrayidx2520, align 8
  %add2521 = add i64 %1267, %1269
  %arrayidx2522 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1270 = load i64, i64* %arrayidx2522, align 16
  %add2523 = add i64 %1270, %add2521
  store i64 %add2523, i64* %arrayidx2522, align 16
  %arrayidx2524 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1271 = load i64, i64* %arrayidx2524, align 8
  %arrayidx2525 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1272 = load i64, i64* %arrayidx2525, align 16
  %xor2526 = xor i64 %1271, %1272
  %call2527 = call i64 @rotr64(i64 %xor2526, i32 32)
  %arrayidx2528 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2527, i64* %arrayidx2528, align 8
  %arrayidx2529 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1273 = load i64, i64* %arrayidx2529, align 8
  %arrayidx2530 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1274 = load i64, i64* %arrayidx2530, align 16
  %add2531 = add i64 %1274, %1273
  store i64 %add2531, i64* %arrayidx2530, align 16
  %arrayidx2532 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1275 = load i64, i64* %arrayidx2532, align 8
  %arrayidx2533 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1276 = load i64, i64* %arrayidx2533, align 16
  %xor2534 = xor i64 %1275, %1276
  %call2535 = call i64 @rotr64(i64 %xor2534, i32 24)
  %arrayidx2536 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2535, i64* %arrayidx2536, align 8
  %arrayidx2537 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1277 = load i64, i64* %arrayidx2537, align 8
  %1278 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 13), align 1
  %idxprom2538 = zext i8 %1278 to i32
  %arrayidx2539 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2538
  %1279 = load i64, i64* %arrayidx2539, align 8
  %add2540 = add i64 %1277, %1279
  %arrayidx2541 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1280 = load i64, i64* %arrayidx2541, align 16
  %add2542 = add i64 %1280, %add2540
  store i64 %add2542, i64* %arrayidx2541, align 16
  %arrayidx2543 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1281 = load i64, i64* %arrayidx2543, align 8
  %arrayidx2544 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1282 = load i64, i64* %arrayidx2544, align 16
  %xor2545 = xor i64 %1281, %1282
  %call2546 = call i64 @rotr64(i64 %xor2545, i32 16)
  %arrayidx2547 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2546, i64* %arrayidx2547, align 8
  %arrayidx2548 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1283 = load i64, i64* %arrayidx2548, align 8
  %arrayidx2549 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1284 = load i64, i64* %arrayidx2549, align 16
  %add2550 = add i64 %1284, %1283
  store i64 %add2550, i64* %arrayidx2549, align 16
  %arrayidx2551 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1285 = load i64, i64* %arrayidx2551, align 8
  %arrayidx2552 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1286 = load i64, i64* %arrayidx2552, align 16
  %xor2553 = xor i64 %1285, %1286
  %call2554 = call i64 @rotr64(i64 %xor2553, i32 63)
  %arrayidx2555 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2554, i64* %arrayidx2555, align 8
  br label %do.end2556

do.end2556:                                       ; preds = %do.body2517
  br label %do.body2557

do.body2557:                                      ; preds = %do.end2556
  %arrayidx2558 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1287 = load i64, i64* %arrayidx2558, align 16
  %1288 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 14), align 2
  %idxprom2559 = zext i8 %1288 to i32
  %arrayidx2560 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2559
  %1289 = load i64, i64* %arrayidx2560, align 8
  %add2561 = add i64 %1287, %1289
  %arrayidx2562 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1290 = load i64, i64* %arrayidx2562, align 8
  %add2563 = add i64 %1290, %add2561
  store i64 %add2563, i64* %arrayidx2562, align 8
  %arrayidx2564 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1291 = load i64, i64* %arrayidx2564, align 16
  %arrayidx2565 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1292 = load i64, i64* %arrayidx2565, align 8
  %xor2566 = xor i64 %1291, %1292
  %call2567 = call i64 @rotr64(i64 %xor2566, i32 32)
  %arrayidx2568 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2567, i64* %arrayidx2568, align 16
  %arrayidx2569 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1293 = load i64, i64* %arrayidx2569, align 16
  %arrayidx2570 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1294 = load i64, i64* %arrayidx2570, align 8
  %add2571 = add i64 %1294, %1293
  store i64 %add2571, i64* %arrayidx2570, align 8
  %arrayidx2572 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1295 = load i64, i64* %arrayidx2572, align 16
  %arrayidx2573 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1296 = load i64, i64* %arrayidx2573, align 8
  %xor2574 = xor i64 %1295, %1296
  %call2575 = call i64 @rotr64(i64 %xor2574, i32 24)
  %arrayidx2576 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2575, i64* %arrayidx2576, align 16
  %arrayidx2577 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1297 = load i64, i64* %arrayidx2577, align 16
  %1298 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 7, i32 15), align 1
  %idxprom2578 = zext i8 %1298 to i32
  %arrayidx2579 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2578
  %1299 = load i64, i64* %arrayidx2579, align 8
  %add2580 = add i64 %1297, %1299
  %arrayidx2581 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1300 = load i64, i64* %arrayidx2581, align 8
  %add2582 = add i64 %1300, %add2580
  store i64 %add2582, i64* %arrayidx2581, align 8
  %arrayidx2583 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1301 = load i64, i64* %arrayidx2583, align 16
  %arrayidx2584 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1302 = load i64, i64* %arrayidx2584, align 8
  %xor2585 = xor i64 %1301, %1302
  %call2586 = call i64 @rotr64(i64 %xor2585, i32 16)
  %arrayidx2587 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2586, i64* %arrayidx2587, align 16
  %arrayidx2588 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1303 = load i64, i64* %arrayidx2588, align 16
  %arrayidx2589 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1304 = load i64, i64* %arrayidx2589, align 8
  %add2590 = add i64 %1304, %1303
  store i64 %add2590, i64* %arrayidx2589, align 8
  %arrayidx2591 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1305 = load i64, i64* %arrayidx2591, align 16
  %arrayidx2592 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1306 = load i64, i64* %arrayidx2592, align 8
  %xor2593 = xor i64 %1305, %1306
  %call2594 = call i64 @rotr64(i64 %xor2593, i32 63)
  %arrayidx2595 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2594, i64* %arrayidx2595, align 16
  br label %do.end2596

do.end2596:                                       ; preds = %do.body2557
  br label %do.end2597

do.end2597:                                       ; preds = %do.end2596
  br label %do.body2598

do.body2598:                                      ; preds = %do.end2597
  br label %do.body2599

do.body2599:                                      ; preds = %do.body2598
  %arrayidx2600 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1307 = load i64, i64* %arrayidx2600, align 16
  %1308 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 0), align 16
  %idxprom2601 = zext i8 %1308 to i32
  %arrayidx2602 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2601
  %1309 = load i64, i64* %arrayidx2602, align 8
  %add2603 = add i64 %1307, %1309
  %arrayidx2604 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1310 = load i64, i64* %arrayidx2604, align 16
  %add2605 = add i64 %1310, %add2603
  store i64 %add2605, i64* %arrayidx2604, align 16
  %arrayidx2606 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1311 = load i64, i64* %arrayidx2606, align 16
  %arrayidx2607 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1312 = load i64, i64* %arrayidx2607, align 16
  %xor2608 = xor i64 %1311, %1312
  %call2609 = call i64 @rotr64(i64 %xor2608, i32 32)
  %arrayidx2610 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2609, i64* %arrayidx2610, align 16
  %arrayidx2611 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1313 = load i64, i64* %arrayidx2611, align 16
  %arrayidx2612 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1314 = load i64, i64* %arrayidx2612, align 16
  %add2613 = add i64 %1314, %1313
  store i64 %add2613, i64* %arrayidx2612, align 16
  %arrayidx2614 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1315 = load i64, i64* %arrayidx2614, align 16
  %arrayidx2615 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1316 = load i64, i64* %arrayidx2615, align 16
  %xor2616 = xor i64 %1315, %1316
  %call2617 = call i64 @rotr64(i64 %xor2616, i32 24)
  %arrayidx2618 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2617, i64* %arrayidx2618, align 16
  %arrayidx2619 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1317 = load i64, i64* %arrayidx2619, align 16
  %1318 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 1), align 1
  %idxprom2620 = zext i8 %1318 to i32
  %arrayidx2621 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2620
  %1319 = load i64, i64* %arrayidx2621, align 8
  %add2622 = add i64 %1317, %1319
  %arrayidx2623 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1320 = load i64, i64* %arrayidx2623, align 16
  %add2624 = add i64 %1320, %add2622
  store i64 %add2624, i64* %arrayidx2623, align 16
  %arrayidx2625 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1321 = load i64, i64* %arrayidx2625, align 16
  %arrayidx2626 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1322 = load i64, i64* %arrayidx2626, align 16
  %xor2627 = xor i64 %1321, %1322
  %call2628 = call i64 @rotr64(i64 %xor2627, i32 16)
  %arrayidx2629 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2628, i64* %arrayidx2629, align 16
  %arrayidx2630 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1323 = load i64, i64* %arrayidx2630, align 16
  %arrayidx2631 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1324 = load i64, i64* %arrayidx2631, align 16
  %add2632 = add i64 %1324, %1323
  store i64 %add2632, i64* %arrayidx2631, align 16
  %arrayidx2633 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1325 = load i64, i64* %arrayidx2633, align 16
  %arrayidx2634 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1326 = load i64, i64* %arrayidx2634, align 16
  %xor2635 = xor i64 %1325, %1326
  %call2636 = call i64 @rotr64(i64 %xor2635, i32 63)
  %arrayidx2637 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2636, i64* %arrayidx2637, align 16
  br label %do.end2638

do.end2638:                                       ; preds = %do.body2599
  br label %do.body2639

do.body2639:                                      ; preds = %do.end2638
  %arrayidx2640 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1327 = load i64, i64* %arrayidx2640, align 8
  %1328 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 2), align 2
  %idxprom2641 = zext i8 %1328 to i32
  %arrayidx2642 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2641
  %1329 = load i64, i64* %arrayidx2642, align 8
  %add2643 = add i64 %1327, %1329
  %arrayidx2644 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1330 = load i64, i64* %arrayidx2644, align 8
  %add2645 = add i64 %1330, %add2643
  store i64 %add2645, i64* %arrayidx2644, align 8
  %arrayidx2646 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1331 = load i64, i64* %arrayidx2646, align 8
  %arrayidx2647 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1332 = load i64, i64* %arrayidx2647, align 8
  %xor2648 = xor i64 %1331, %1332
  %call2649 = call i64 @rotr64(i64 %xor2648, i32 32)
  %arrayidx2650 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2649, i64* %arrayidx2650, align 8
  %arrayidx2651 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1333 = load i64, i64* %arrayidx2651, align 8
  %arrayidx2652 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1334 = load i64, i64* %arrayidx2652, align 8
  %add2653 = add i64 %1334, %1333
  store i64 %add2653, i64* %arrayidx2652, align 8
  %arrayidx2654 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1335 = load i64, i64* %arrayidx2654, align 8
  %arrayidx2655 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1336 = load i64, i64* %arrayidx2655, align 8
  %xor2656 = xor i64 %1335, %1336
  %call2657 = call i64 @rotr64(i64 %xor2656, i32 24)
  %arrayidx2658 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2657, i64* %arrayidx2658, align 8
  %arrayidx2659 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1337 = load i64, i64* %arrayidx2659, align 8
  %1338 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 3), align 1
  %idxprom2660 = zext i8 %1338 to i32
  %arrayidx2661 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2660
  %1339 = load i64, i64* %arrayidx2661, align 8
  %add2662 = add i64 %1337, %1339
  %arrayidx2663 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1340 = load i64, i64* %arrayidx2663, align 8
  %add2664 = add i64 %1340, %add2662
  store i64 %add2664, i64* %arrayidx2663, align 8
  %arrayidx2665 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1341 = load i64, i64* %arrayidx2665, align 8
  %arrayidx2666 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1342 = load i64, i64* %arrayidx2666, align 8
  %xor2667 = xor i64 %1341, %1342
  %call2668 = call i64 @rotr64(i64 %xor2667, i32 16)
  %arrayidx2669 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2668, i64* %arrayidx2669, align 8
  %arrayidx2670 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1343 = load i64, i64* %arrayidx2670, align 8
  %arrayidx2671 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1344 = load i64, i64* %arrayidx2671, align 8
  %add2672 = add i64 %1344, %1343
  store i64 %add2672, i64* %arrayidx2671, align 8
  %arrayidx2673 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1345 = load i64, i64* %arrayidx2673, align 8
  %arrayidx2674 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1346 = load i64, i64* %arrayidx2674, align 8
  %xor2675 = xor i64 %1345, %1346
  %call2676 = call i64 @rotr64(i64 %xor2675, i32 63)
  %arrayidx2677 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2676, i64* %arrayidx2677, align 8
  br label %do.end2678

do.end2678:                                       ; preds = %do.body2639
  br label %do.body2679

do.body2679:                                      ; preds = %do.end2678
  %arrayidx2680 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1347 = load i64, i64* %arrayidx2680, align 16
  %1348 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 4), align 4
  %idxprom2681 = zext i8 %1348 to i32
  %arrayidx2682 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2681
  %1349 = load i64, i64* %arrayidx2682, align 8
  %add2683 = add i64 %1347, %1349
  %arrayidx2684 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1350 = load i64, i64* %arrayidx2684, align 16
  %add2685 = add i64 %1350, %add2683
  store i64 %add2685, i64* %arrayidx2684, align 16
  %arrayidx2686 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1351 = load i64, i64* %arrayidx2686, align 16
  %arrayidx2687 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1352 = load i64, i64* %arrayidx2687, align 16
  %xor2688 = xor i64 %1351, %1352
  %call2689 = call i64 @rotr64(i64 %xor2688, i32 32)
  %arrayidx2690 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2689, i64* %arrayidx2690, align 16
  %arrayidx2691 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1353 = load i64, i64* %arrayidx2691, align 16
  %arrayidx2692 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1354 = load i64, i64* %arrayidx2692, align 16
  %add2693 = add i64 %1354, %1353
  store i64 %add2693, i64* %arrayidx2692, align 16
  %arrayidx2694 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1355 = load i64, i64* %arrayidx2694, align 16
  %arrayidx2695 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1356 = load i64, i64* %arrayidx2695, align 16
  %xor2696 = xor i64 %1355, %1356
  %call2697 = call i64 @rotr64(i64 %xor2696, i32 24)
  %arrayidx2698 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2697, i64* %arrayidx2698, align 16
  %arrayidx2699 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1357 = load i64, i64* %arrayidx2699, align 16
  %1358 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 5), align 1
  %idxprom2700 = zext i8 %1358 to i32
  %arrayidx2701 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2700
  %1359 = load i64, i64* %arrayidx2701, align 8
  %add2702 = add i64 %1357, %1359
  %arrayidx2703 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1360 = load i64, i64* %arrayidx2703, align 16
  %add2704 = add i64 %1360, %add2702
  store i64 %add2704, i64* %arrayidx2703, align 16
  %arrayidx2705 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1361 = load i64, i64* %arrayidx2705, align 16
  %arrayidx2706 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1362 = load i64, i64* %arrayidx2706, align 16
  %xor2707 = xor i64 %1361, %1362
  %call2708 = call i64 @rotr64(i64 %xor2707, i32 16)
  %arrayidx2709 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2708, i64* %arrayidx2709, align 16
  %arrayidx2710 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1363 = load i64, i64* %arrayidx2710, align 16
  %arrayidx2711 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1364 = load i64, i64* %arrayidx2711, align 16
  %add2712 = add i64 %1364, %1363
  store i64 %add2712, i64* %arrayidx2711, align 16
  %arrayidx2713 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1365 = load i64, i64* %arrayidx2713, align 16
  %arrayidx2714 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1366 = load i64, i64* %arrayidx2714, align 16
  %xor2715 = xor i64 %1365, %1366
  %call2716 = call i64 @rotr64(i64 %xor2715, i32 63)
  %arrayidx2717 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2716, i64* %arrayidx2717, align 16
  br label %do.end2718

do.end2718:                                       ; preds = %do.body2679
  br label %do.body2719

do.body2719:                                      ; preds = %do.end2718
  %arrayidx2720 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1367 = load i64, i64* %arrayidx2720, align 8
  %1368 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 6), align 2
  %idxprom2721 = zext i8 %1368 to i32
  %arrayidx2722 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2721
  %1369 = load i64, i64* %arrayidx2722, align 8
  %add2723 = add i64 %1367, %1369
  %arrayidx2724 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1370 = load i64, i64* %arrayidx2724, align 8
  %add2725 = add i64 %1370, %add2723
  store i64 %add2725, i64* %arrayidx2724, align 8
  %arrayidx2726 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1371 = load i64, i64* %arrayidx2726, align 8
  %arrayidx2727 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1372 = load i64, i64* %arrayidx2727, align 8
  %xor2728 = xor i64 %1371, %1372
  %call2729 = call i64 @rotr64(i64 %xor2728, i32 32)
  %arrayidx2730 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2729, i64* %arrayidx2730, align 8
  %arrayidx2731 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1373 = load i64, i64* %arrayidx2731, align 8
  %arrayidx2732 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1374 = load i64, i64* %arrayidx2732, align 8
  %add2733 = add i64 %1374, %1373
  store i64 %add2733, i64* %arrayidx2732, align 8
  %arrayidx2734 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1375 = load i64, i64* %arrayidx2734, align 8
  %arrayidx2735 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1376 = load i64, i64* %arrayidx2735, align 8
  %xor2736 = xor i64 %1375, %1376
  %call2737 = call i64 @rotr64(i64 %xor2736, i32 24)
  %arrayidx2738 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2737, i64* %arrayidx2738, align 8
  %arrayidx2739 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1377 = load i64, i64* %arrayidx2739, align 8
  %1378 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 7), align 1
  %idxprom2740 = zext i8 %1378 to i32
  %arrayidx2741 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2740
  %1379 = load i64, i64* %arrayidx2741, align 8
  %add2742 = add i64 %1377, %1379
  %arrayidx2743 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1380 = load i64, i64* %arrayidx2743, align 8
  %add2744 = add i64 %1380, %add2742
  store i64 %add2744, i64* %arrayidx2743, align 8
  %arrayidx2745 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1381 = load i64, i64* %arrayidx2745, align 8
  %arrayidx2746 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1382 = load i64, i64* %arrayidx2746, align 8
  %xor2747 = xor i64 %1381, %1382
  %call2748 = call i64 @rotr64(i64 %xor2747, i32 16)
  %arrayidx2749 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2748, i64* %arrayidx2749, align 8
  %arrayidx2750 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1383 = load i64, i64* %arrayidx2750, align 8
  %arrayidx2751 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1384 = load i64, i64* %arrayidx2751, align 8
  %add2752 = add i64 %1384, %1383
  store i64 %add2752, i64* %arrayidx2751, align 8
  %arrayidx2753 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1385 = load i64, i64* %arrayidx2753, align 8
  %arrayidx2754 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1386 = load i64, i64* %arrayidx2754, align 8
  %xor2755 = xor i64 %1385, %1386
  %call2756 = call i64 @rotr64(i64 %xor2755, i32 63)
  %arrayidx2757 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2756, i64* %arrayidx2757, align 8
  br label %do.end2758

do.end2758:                                       ; preds = %do.body2719
  br label %do.body2759

do.body2759:                                      ; preds = %do.end2758
  %arrayidx2760 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1387 = load i64, i64* %arrayidx2760, align 8
  %1388 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 8), align 8
  %idxprom2761 = zext i8 %1388 to i32
  %arrayidx2762 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2761
  %1389 = load i64, i64* %arrayidx2762, align 8
  %add2763 = add i64 %1387, %1389
  %arrayidx2764 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1390 = load i64, i64* %arrayidx2764, align 16
  %add2765 = add i64 %1390, %add2763
  store i64 %add2765, i64* %arrayidx2764, align 16
  %arrayidx2766 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1391 = load i64, i64* %arrayidx2766, align 8
  %arrayidx2767 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1392 = load i64, i64* %arrayidx2767, align 16
  %xor2768 = xor i64 %1391, %1392
  %call2769 = call i64 @rotr64(i64 %xor2768, i32 32)
  %arrayidx2770 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2769, i64* %arrayidx2770, align 8
  %arrayidx2771 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1393 = load i64, i64* %arrayidx2771, align 8
  %arrayidx2772 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1394 = load i64, i64* %arrayidx2772, align 16
  %add2773 = add i64 %1394, %1393
  store i64 %add2773, i64* %arrayidx2772, align 16
  %arrayidx2774 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1395 = load i64, i64* %arrayidx2774, align 8
  %arrayidx2775 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1396 = load i64, i64* %arrayidx2775, align 16
  %xor2776 = xor i64 %1395, %1396
  %call2777 = call i64 @rotr64(i64 %xor2776, i32 24)
  %arrayidx2778 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2777, i64* %arrayidx2778, align 8
  %arrayidx2779 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1397 = load i64, i64* %arrayidx2779, align 8
  %1398 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 9), align 1
  %idxprom2780 = zext i8 %1398 to i32
  %arrayidx2781 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2780
  %1399 = load i64, i64* %arrayidx2781, align 8
  %add2782 = add i64 %1397, %1399
  %arrayidx2783 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1400 = load i64, i64* %arrayidx2783, align 16
  %add2784 = add i64 %1400, %add2782
  store i64 %add2784, i64* %arrayidx2783, align 16
  %arrayidx2785 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1401 = load i64, i64* %arrayidx2785, align 8
  %arrayidx2786 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1402 = load i64, i64* %arrayidx2786, align 16
  %xor2787 = xor i64 %1401, %1402
  %call2788 = call i64 @rotr64(i64 %xor2787, i32 16)
  %arrayidx2789 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call2788, i64* %arrayidx2789, align 8
  %arrayidx2790 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1403 = load i64, i64* %arrayidx2790, align 8
  %arrayidx2791 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1404 = load i64, i64* %arrayidx2791, align 16
  %add2792 = add i64 %1404, %1403
  store i64 %add2792, i64* %arrayidx2791, align 16
  %arrayidx2793 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1405 = load i64, i64* %arrayidx2793, align 8
  %arrayidx2794 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1406 = load i64, i64* %arrayidx2794, align 16
  %xor2795 = xor i64 %1405, %1406
  %call2796 = call i64 @rotr64(i64 %xor2795, i32 63)
  %arrayidx2797 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2796, i64* %arrayidx2797, align 8
  br label %do.end2798

do.end2798:                                       ; preds = %do.body2759
  br label %do.body2799

do.body2799:                                      ; preds = %do.end2798
  %arrayidx2800 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1407 = load i64, i64* %arrayidx2800, align 16
  %1408 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 10), align 2
  %idxprom2801 = zext i8 %1408 to i32
  %arrayidx2802 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2801
  %1409 = load i64, i64* %arrayidx2802, align 8
  %add2803 = add i64 %1407, %1409
  %arrayidx2804 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1410 = load i64, i64* %arrayidx2804, align 8
  %add2805 = add i64 %1410, %add2803
  store i64 %add2805, i64* %arrayidx2804, align 8
  %arrayidx2806 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1411 = load i64, i64* %arrayidx2806, align 16
  %arrayidx2807 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1412 = load i64, i64* %arrayidx2807, align 8
  %xor2808 = xor i64 %1411, %1412
  %call2809 = call i64 @rotr64(i64 %xor2808, i32 32)
  %arrayidx2810 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2809, i64* %arrayidx2810, align 16
  %arrayidx2811 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1413 = load i64, i64* %arrayidx2811, align 16
  %arrayidx2812 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1414 = load i64, i64* %arrayidx2812, align 8
  %add2813 = add i64 %1414, %1413
  store i64 %add2813, i64* %arrayidx2812, align 8
  %arrayidx2814 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1415 = load i64, i64* %arrayidx2814, align 16
  %arrayidx2815 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1416 = load i64, i64* %arrayidx2815, align 8
  %xor2816 = xor i64 %1415, %1416
  %call2817 = call i64 @rotr64(i64 %xor2816, i32 24)
  %arrayidx2818 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2817, i64* %arrayidx2818, align 16
  %arrayidx2819 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1417 = load i64, i64* %arrayidx2819, align 16
  %1418 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 11), align 1
  %idxprom2820 = zext i8 %1418 to i32
  %arrayidx2821 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2820
  %1419 = load i64, i64* %arrayidx2821, align 8
  %add2822 = add i64 %1417, %1419
  %arrayidx2823 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1420 = load i64, i64* %arrayidx2823, align 8
  %add2824 = add i64 %1420, %add2822
  store i64 %add2824, i64* %arrayidx2823, align 8
  %arrayidx2825 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1421 = load i64, i64* %arrayidx2825, align 16
  %arrayidx2826 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1422 = load i64, i64* %arrayidx2826, align 8
  %xor2827 = xor i64 %1421, %1422
  %call2828 = call i64 @rotr64(i64 %xor2827, i32 16)
  %arrayidx2829 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2828, i64* %arrayidx2829, align 16
  %arrayidx2830 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1423 = load i64, i64* %arrayidx2830, align 16
  %arrayidx2831 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1424 = load i64, i64* %arrayidx2831, align 8
  %add2832 = add i64 %1424, %1423
  store i64 %add2832, i64* %arrayidx2831, align 8
  %arrayidx2833 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1425 = load i64, i64* %arrayidx2833, align 16
  %arrayidx2834 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1426 = load i64, i64* %arrayidx2834, align 8
  %xor2835 = xor i64 %1425, %1426
  %call2836 = call i64 @rotr64(i64 %xor2835, i32 63)
  %arrayidx2837 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call2836, i64* %arrayidx2837, align 16
  br label %do.end2838

do.end2838:                                       ; preds = %do.body2799
  br label %do.body2839

do.body2839:                                      ; preds = %do.end2838
  %arrayidx2840 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1427 = load i64, i64* %arrayidx2840, align 8
  %1428 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 12), align 4
  %idxprom2841 = zext i8 %1428 to i32
  %arrayidx2842 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2841
  %1429 = load i64, i64* %arrayidx2842, align 8
  %add2843 = add i64 %1427, %1429
  %arrayidx2844 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1430 = load i64, i64* %arrayidx2844, align 16
  %add2845 = add i64 %1430, %add2843
  store i64 %add2845, i64* %arrayidx2844, align 16
  %arrayidx2846 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1431 = load i64, i64* %arrayidx2846, align 8
  %arrayidx2847 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1432 = load i64, i64* %arrayidx2847, align 16
  %xor2848 = xor i64 %1431, %1432
  %call2849 = call i64 @rotr64(i64 %xor2848, i32 32)
  %arrayidx2850 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2849, i64* %arrayidx2850, align 8
  %arrayidx2851 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1433 = load i64, i64* %arrayidx2851, align 8
  %arrayidx2852 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1434 = load i64, i64* %arrayidx2852, align 16
  %add2853 = add i64 %1434, %1433
  store i64 %add2853, i64* %arrayidx2852, align 16
  %arrayidx2854 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1435 = load i64, i64* %arrayidx2854, align 8
  %arrayidx2855 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1436 = load i64, i64* %arrayidx2855, align 16
  %xor2856 = xor i64 %1435, %1436
  %call2857 = call i64 @rotr64(i64 %xor2856, i32 24)
  %arrayidx2858 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2857, i64* %arrayidx2858, align 8
  %arrayidx2859 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1437 = load i64, i64* %arrayidx2859, align 8
  %1438 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 13), align 1
  %idxprom2860 = zext i8 %1438 to i32
  %arrayidx2861 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2860
  %1439 = load i64, i64* %arrayidx2861, align 8
  %add2862 = add i64 %1437, %1439
  %arrayidx2863 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1440 = load i64, i64* %arrayidx2863, align 16
  %add2864 = add i64 %1440, %add2862
  store i64 %add2864, i64* %arrayidx2863, align 16
  %arrayidx2865 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1441 = load i64, i64* %arrayidx2865, align 8
  %arrayidx2866 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1442 = load i64, i64* %arrayidx2866, align 16
  %xor2867 = xor i64 %1441, %1442
  %call2868 = call i64 @rotr64(i64 %xor2867, i32 16)
  %arrayidx2869 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2868, i64* %arrayidx2869, align 8
  %arrayidx2870 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1443 = load i64, i64* %arrayidx2870, align 8
  %arrayidx2871 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1444 = load i64, i64* %arrayidx2871, align 16
  %add2872 = add i64 %1444, %1443
  store i64 %add2872, i64* %arrayidx2871, align 16
  %arrayidx2873 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1445 = load i64, i64* %arrayidx2873, align 8
  %arrayidx2874 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1446 = load i64, i64* %arrayidx2874, align 16
  %xor2875 = xor i64 %1445, %1446
  %call2876 = call i64 @rotr64(i64 %xor2875, i32 63)
  %arrayidx2877 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call2876, i64* %arrayidx2877, align 8
  br label %do.end2878

do.end2878:                                       ; preds = %do.body2839
  br label %do.body2879

do.body2879:                                      ; preds = %do.end2878
  %arrayidx2880 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1447 = load i64, i64* %arrayidx2880, align 16
  %1448 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 14), align 2
  %idxprom2881 = zext i8 %1448 to i32
  %arrayidx2882 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2881
  %1449 = load i64, i64* %arrayidx2882, align 8
  %add2883 = add i64 %1447, %1449
  %arrayidx2884 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1450 = load i64, i64* %arrayidx2884, align 8
  %add2885 = add i64 %1450, %add2883
  store i64 %add2885, i64* %arrayidx2884, align 8
  %arrayidx2886 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1451 = load i64, i64* %arrayidx2886, align 16
  %arrayidx2887 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1452 = load i64, i64* %arrayidx2887, align 8
  %xor2888 = xor i64 %1451, %1452
  %call2889 = call i64 @rotr64(i64 %xor2888, i32 32)
  %arrayidx2890 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2889, i64* %arrayidx2890, align 16
  %arrayidx2891 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1453 = load i64, i64* %arrayidx2891, align 16
  %arrayidx2892 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1454 = load i64, i64* %arrayidx2892, align 8
  %add2893 = add i64 %1454, %1453
  store i64 %add2893, i64* %arrayidx2892, align 8
  %arrayidx2894 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1455 = load i64, i64* %arrayidx2894, align 16
  %arrayidx2895 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1456 = load i64, i64* %arrayidx2895, align 8
  %xor2896 = xor i64 %1455, %1456
  %call2897 = call i64 @rotr64(i64 %xor2896, i32 24)
  %arrayidx2898 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2897, i64* %arrayidx2898, align 16
  %arrayidx2899 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1457 = load i64, i64* %arrayidx2899, align 16
  %1458 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 8, i32 15), align 1
  %idxprom2900 = zext i8 %1458 to i32
  %arrayidx2901 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2900
  %1459 = load i64, i64* %arrayidx2901, align 8
  %add2902 = add i64 %1457, %1459
  %arrayidx2903 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1460 = load i64, i64* %arrayidx2903, align 8
  %add2904 = add i64 %1460, %add2902
  store i64 %add2904, i64* %arrayidx2903, align 8
  %arrayidx2905 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1461 = load i64, i64* %arrayidx2905, align 16
  %arrayidx2906 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1462 = load i64, i64* %arrayidx2906, align 8
  %xor2907 = xor i64 %1461, %1462
  %call2908 = call i64 @rotr64(i64 %xor2907, i32 16)
  %arrayidx2909 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call2908, i64* %arrayidx2909, align 16
  %arrayidx2910 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1463 = load i64, i64* %arrayidx2910, align 16
  %arrayidx2911 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1464 = load i64, i64* %arrayidx2911, align 8
  %add2912 = add i64 %1464, %1463
  store i64 %add2912, i64* %arrayidx2911, align 8
  %arrayidx2913 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1465 = load i64, i64* %arrayidx2913, align 16
  %arrayidx2914 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1466 = load i64, i64* %arrayidx2914, align 8
  %xor2915 = xor i64 %1465, %1466
  %call2916 = call i64 @rotr64(i64 %xor2915, i32 63)
  %arrayidx2917 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2916, i64* %arrayidx2917, align 16
  br label %do.end2918

do.end2918:                                       ; preds = %do.body2879
  br label %do.end2919

do.end2919:                                       ; preds = %do.end2918
  br label %do.body2920

do.body2920:                                      ; preds = %do.end2919
  br label %do.body2921

do.body2921:                                      ; preds = %do.body2920
  %arrayidx2922 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1467 = load i64, i64* %arrayidx2922, align 16
  %1468 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 0), align 16
  %idxprom2923 = zext i8 %1468 to i32
  %arrayidx2924 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2923
  %1469 = load i64, i64* %arrayidx2924, align 8
  %add2925 = add i64 %1467, %1469
  %arrayidx2926 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1470 = load i64, i64* %arrayidx2926, align 16
  %add2927 = add i64 %1470, %add2925
  store i64 %add2927, i64* %arrayidx2926, align 16
  %arrayidx2928 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1471 = load i64, i64* %arrayidx2928, align 16
  %arrayidx2929 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1472 = load i64, i64* %arrayidx2929, align 16
  %xor2930 = xor i64 %1471, %1472
  %call2931 = call i64 @rotr64(i64 %xor2930, i32 32)
  %arrayidx2932 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2931, i64* %arrayidx2932, align 16
  %arrayidx2933 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1473 = load i64, i64* %arrayidx2933, align 16
  %arrayidx2934 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1474 = load i64, i64* %arrayidx2934, align 16
  %add2935 = add i64 %1474, %1473
  store i64 %add2935, i64* %arrayidx2934, align 16
  %arrayidx2936 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1475 = load i64, i64* %arrayidx2936, align 16
  %arrayidx2937 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1476 = load i64, i64* %arrayidx2937, align 16
  %xor2938 = xor i64 %1475, %1476
  %call2939 = call i64 @rotr64(i64 %xor2938, i32 24)
  %arrayidx2940 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2939, i64* %arrayidx2940, align 16
  %arrayidx2941 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1477 = load i64, i64* %arrayidx2941, align 16
  %1478 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 1), align 1
  %idxprom2942 = zext i8 %1478 to i32
  %arrayidx2943 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2942
  %1479 = load i64, i64* %arrayidx2943, align 8
  %add2944 = add i64 %1477, %1479
  %arrayidx2945 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1480 = load i64, i64* %arrayidx2945, align 16
  %add2946 = add i64 %1480, %add2944
  store i64 %add2946, i64* %arrayidx2945, align 16
  %arrayidx2947 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1481 = load i64, i64* %arrayidx2947, align 16
  %arrayidx2948 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1482 = load i64, i64* %arrayidx2948, align 16
  %xor2949 = xor i64 %1481, %1482
  %call2950 = call i64 @rotr64(i64 %xor2949, i32 16)
  %arrayidx2951 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call2950, i64* %arrayidx2951, align 16
  %arrayidx2952 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1483 = load i64, i64* %arrayidx2952, align 16
  %arrayidx2953 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1484 = load i64, i64* %arrayidx2953, align 16
  %add2954 = add i64 %1484, %1483
  store i64 %add2954, i64* %arrayidx2953, align 16
  %arrayidx2955 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1485 = load i64, i64* %arrayidx2955, align 16
  %arrayidx2956 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1486 = load i64, i64* %arrayidx2956, align 16
  %xor2957 = xor i64 %1485, %1486
  %call2958 = call i64 @rotr64(i64 %xor2957, i32 63)
  %arrayidx2959 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call2958, i64* %arrayidx2959, align 16
  br label %do.end2960

do.end2960:                                       ; preds = %do.body2921
  br label %do.body2961

do.body2961:                                      ; preds = %do.end2960
  %arrayidx2962 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1487 = load i64, i64* %arrayidx2962, align 8
  %1488 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 2), align 2
  %idxprom2963 = zext i8 %1488 to i32
  %arrayidx2964 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2963
  %1489 = load i64, i64* %arrayidx2964, align 8
  %add2965 = add i64 %1487, %1489
  %arrayidx2966 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1490 = load i64, i64* %arrayidx2966, align 8
  %add2967 = add i64 %1490, %add2965
  store i64 %add2967, i64* %arrayidx2966, align 8
  %arrayidx2968 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1491 = load i64, i64* %arrayidx2968, align 8
  %arrayidx2969 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1492 = load i64, i64* %arrayidx2969, align 8
  %xor2970 = xor i64 %1491, %1492
  %call2971 = call i64 @rotr64(i64 %xor2970, i32 32)
  %arrayidx2972 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2971, i64* %arrayidx2972, align 8
  %arrayidx2973 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1493 = load i64, i64* %arrayidx2973, align 8
  %arrayidx2974 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1494 = load i64, i64* %arrayidx2974, align 8
  %add2975 = add i64 %1494, %1493
  store i64 %add2975, i64* %arrayidx2974, align 8
  %arrayidx2976 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1495 = load i64, i64* %arrayidx2976, align 8
  %arrayidx2977 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1496 = load i64, i64* %arrayidx2977, align 8
  %xor2978 = xor i64 %1495, %1496
  %call2979 = call i64 @rotr64(i64 %xor2978, i32 24)
  %arrayidx2980 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2979, i64* %arrayidx2980, align 8
  %arrayidx2981 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1497 = load i64, i64* %arrayidx2981, align 8
  %1498 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 3), align 1
  %idxprom2982 = zext i8 %1498 to i32
  %arrayidx2983 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom2982
  %1499 = load i64, i64* %arrayidx2983, align 8
  %add2984 = add i64 %1497, %1499
  %arrayidx2985 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1500 = load i64, i64* %arrayidx2985, align 8
  %add2986 = add i64 %1500, %add2984
  store i64 %add2986, i64* %arrayidx2985, align 8
  %arrayidx2987 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1501 = load i64, i64* %arrayidx2987, align 8
  %arrayidx2988 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1502 = load i64, i64* %arrayidx2988, align 8
  %xor2989 = xor i64 %1501, %1502
  %call2990 = call i64 @rotr64(i64 %xor2989, i32 16)
  %arrayidx2991 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call2990, i64* %arrayidx2991, align 8
  %arrayidx2992 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1503 = load i64, i64* %arrayidx2992, align 8
  %arrayidx2993 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1504 = load i64, i64* %arrayidx2993, align 8
  %add2994 = add i64 %1504, %1503
  store i64 %add2994, i64* %arrayidx2993, align 8
  %arrayidx2995 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1505 = load i64, i64* %arrayidx2995, align 8
  %arrayidx2996 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1506 = load i64, i64* %arrayidx2996, align 8
  %xor2997 = xor i64 %1505, %1506
  %call2998 = call i64 @rotr64(i64 %xor2997, i32 63)
  %arrayidx2999 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call2998, i64* %arrayidx2999, align 8
  br label %do.end3000

do.end3000:                                       ; preds = %do.body2961
  br label %do.body3001

do.body3001:                                      ; preds = %do.end3000
  %arrayidx3002 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1507 = load i64, i64* %arrayidx3002, align 16
  %1508 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 4), align 4
  %idxprom3003 = zext i8 %1508 to i32
  %arrayidx3004 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3003
  %1509 = load i64, i64* %arrayidx3004, align 8
  %add3005 = add i64 %1507, %1509
  %arrayidx3006 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1510 = load i64, i64* %arrayidx3006, align 16
  %add3007 = add i64 %1510, %add3005
  store i64 %add3007, i64* %arrayidx3006, align 16
  %arrayidx3008 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1511 = load i64, i64* %arrayidx3008, align 16
  %arrayidx3009 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1512 = load i64, i64* %arrayidx3009, align 16
  %xor3010 = xor i64 %1511, %1512
  %call3011 = call i64 @rotr64(i64 %xor3010, i32 32)
  %arrayidx3012 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3011, i64* %arrayidx3012, align 16
  %arrayidx3013 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1513 = load i64, i64* %arrayidx3013, align 16
  %arrayidx3014 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1514 = load i64, i64* %arrayidx3014, align 16
  %add3015 = add i64 %1514, %1513
  store i64 %add3015, i64* %arrayidx3014, align 16
  %arrayidx3016 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1515 = load i64, i64* %arrayidx3016, align 16
  %arrayidx3017 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1516 = load i64, i64* %arrayidx3017, align 16
  %xor3018 = xor i64 %1515, %1516
  %call3019 = call i64 @rotr64(i64 %xor3018, i32 24)
  %arrayidx3020 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3019, i64* %arrayidx3020, align 16
  %arrayidx3021 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1517 = load i64, i64* %arrayidx3021, align 16
  %1518 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 5), align 1
  %idxprom3022 = zext i8 %1518 to i32
  %arrayidx3023 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3022
  %1519 = load i64, i64* %arrayidx3023, align 8
  %add3024 = add i64 %1517, %1519
  %arrayidx3025 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1520 = load i64, i64* %arrayidx3025, align 16
  %add3026 = add i64 %1520, %add3024
  store i64 %add3026, i64* %arrayidx3025, align 16
  %arrayidx3027 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1521 = load i64, i64* %arrayidx3027, align 16
  %arrayidx3028 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1522 = load i64, i64* %arrayidx3028, align 16
  %xor3029 = xor i64 %1521, %1522
  %call3030 = call i64 @rotr64(i64 %xor3029, i32 16)
  %arrayidx3031 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3030, i64* %arrayidx3031, align 16
  %arrayidx3032 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1523 = load i64, i64* %arrayidx3032, align 16
  %arrayidx3033 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1524 = load i64, i64* %arrayidx3033, align 16
  %add3034 = add i64 %1524, %1523
  store i64 %add3034, i64* %arrayidx3033, align 16
  %arrayidx3035 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1525 = load i64, i64* %arrayidx3035, align 16
  %arrayidx3036 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1526 = load i64, i64* %arrayidx3036, align 16
  %xor3037 = xor i64 %1525, %1526
  %call3038 = call i64 @rotr64(i64 %xor3037, i32 63)
  %arrayidx3039 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3038, i64* %arrayidx3039, align 16
  br label %do.end3040

do.end3040:                                       ; preds = %do.body3001
  br label %do.body3041

do.body3041:                                      ; preds = %do.end3040
  %arrayidx3042 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1527 = load i64, i64* %arrayidx3042, align 8
  %1528 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 6), align 2
  %idxprom3043 = zext i8 %1528 to i32
  %arrayidx3044 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3043
  %1529 = load i64, i64* %arrayidx3044, align 8
  %add3045 = add i64 %1527, %1529
  %arrayidx3046 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1530 = load i64, i64* %arrayidx3046, align 8
  %add3047 = add i64 %1530, %add3045
  store i64 %add3047, i64* %arrayidx3046, align 8
  %arrayidx3048 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1531 = load i64, i64* %arrayidx3048, align 8
  %arrayidx3049 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1532 = load i64, i64* %arrayidx3049, align 8
  %xor3050 = xor i64 %1531, %1532
  %call3051 = call i64 @rotr64(i64 %xor3050, i32 32)
  %arrayidx3052 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3051, i64* %arrayidx3052, align 8
  %arrayidx3053 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1533 = load i64, i64* %arrayidx3053, align 8
  %arrayidx3054 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1534 = load i64, i64* %arrayidx3054, align 8
  %add3055 = add i64 %1534, %1533
  store i64 %add3055, i64* %arrayidx3054, align 8
  %arrayidx3056 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1535 = load i64, i64* %arrayidx3056, align 8
  %arrayidx3057 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1536 = load i64, i64* %arrayidx3057, align 8
  %xor3058 = xor i64 %1535, %1536
  %call3059 = call i64 @rotr64(i64 %xor3058, i32 24)
  %arrayidx3060 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3059, i64* %arrayidx3060, align 8
  %arrayidx3061 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1537 = load i64, i64* %arrayidx3061, align 8
  %1538 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 7), align 1
  %idxprom3062 = zext i8 %1538 to i32
  %arrayidx3063 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3062
  %1539 = load i64, i64* %arrayidx3063, align 8
  %add3064 = add i64 %1537, %1539
  %arrayidx3065 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1540 = load i64, i64* %arrayidx3065, align 8
  %add3066 = add i64 %1540, %add3064
  store i64 %add3066, i64* %arrayidx3065, align 8
  %arrayidx3067 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1541 = load i64, i64* %arrayidx3067, align 8
  %arrayidx3068 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1542 = load i64, i64* %arrayidx3068, align 8
  %xor3069 = xor i64 %1541, %1542
  %call3070 = call i64 @rotr64(i64 %xor3069, i32 16)
  %arrayidx3071 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3070, i64* %arrayidx3071, align 8
  %arrayidx3072 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1543 = load i64, i64* %arrayidx3072, align 8
  %arrayidx3073 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1544 = load i64, i64* %arrayidx3073, align 8
  %add3074 = add i64 %1544, %1543
  store i64 %add3074, i64* %arrayidx3073, align 8
  %arrayidx3075 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1545 = load i64, i64* %arrayidx3075, align 8
  %arrayidx3076 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1546 = load i64, i64* %arrayidx3076, align 8
  %xor3077 = xor i64 %1545, %1546
  %call3078 = call i64 @rotr64(i64 %xor3077, i32 63)
  %arrayidx3079 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3078, i64* %arrayidx3079, align 8
  br label %do.end3080

do.end3080:                                       ; preds = %do.body3041
  br label %do.body3081

do.body3081:                                      ; preds = %do.end3080
  %arrayidx3082 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1547 = load i64, i64* %arrayidx3082, align 8
  %1548 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 8), align 8
  %idxprom3083 = zext i8 %1548 to i32
  %arrayidx3084 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3083
  %1549 = load i64, i64* %arrayidx3084, align 8
  %add3085 = add i64 %1547, %1549
  %arrayidx3086 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1550 = load i64, i64* %arrayidx3086, align 16
  %add3087 = add i64 %1550, %add3085
  store i64 %add3087, i64* %arrayidx3086, align 16
  %arrayidx3088 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1551 = load i64, i64* %arrayidx3088, align 8
  %arrayidx3089 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1552 = load i64, i64* %arrayidx3089, align 16
  %xor3090 = xor i64 %1551, %1552
  %call3091 = call i64 @rotr64(i64 %xor3090, i32 32)
  %arrayidx3092 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3091, i64* %arrayidx3092, align 8
  %arrayidx3093 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1553 = load i64, i64* %arrayidx3093, align 8
  %arrayidx3094 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1554 = load i64, i64* %arrayidx3094, align 16
  %add3095 = add i64 %1554, %1553
  store i64 %add3095, i64* %arrayidx3094, align 16
  %arrayidx3096 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1555 = load i64, i64* %arrayidx3096, align 8
  %arrayidx3097 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1556 = load i64, i64* %arrayidx3097, align 16
  %xor3098 = xor i64 %1555, %1556
  %call3099 = call i64 @rotr64(i64 %xor3098, i32 24)
  %arrayidx3100 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3099, i64* %arrayidx3100, align 8
  %arrayidx3101 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1557 = load i64, i64* %arrayidx3101, align 8
  %1558 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 9), align 1
  %idxprom3102 = zext i8 %1558 to i32
  %arrayidx3103 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3102
  %1559 = load i64, i64* %arrayidx3103, align 8
  %add3104 = add i64 %1557, %1559
  %arrayidx3105 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1560 = load i64, i64* %arrayidx3105, align 16
  %add3106 = add i64 %1560, %add3104
  store i64 %add3106, i64* %arrayidx3105, align 16
  %arrayidx3107 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1561 = load i64, i64* %arrayidx3107, align 8
  %arrayidx3108 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1562 = load i64, i64* %arrayidx3108, align 16
  %xor3109 = xor i64 %1561, %1562
  %call3110 = call i64 @rotr64(i64 %xor3109, i32 16)
  %arrayidx3111 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3110, i64* %arrayidx3111, align 8
  %arrayidx3112 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1563 = load i64, i64* %arrayidx3112, align 8
  %arrayidx3113 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1564 = load i64, i64* %arrayidx3113, align 16
  %add3114 = add i64 %1564, %1563
  store i64 %add3114, i64* %arrayidx3113, align 16
  %arrayidx3115 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1565 = load i64, i64* %arrayidx3115, align 8
  %arrayidx3116 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1566 = load i64, i64* %arrayidx3116, align 16
  %xor3117 = xor i64 %1565, %1566
  %call3118 = call i64 @rotr64(i64 %xor3117, i32 63)
  %arrayidx3119 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3118, i64* %arrayidx3119, align 8
  br label %do.end3120

do.end3120:                                       ; preds = %do.body3081
  br label %do.body3121

do.body3121:                                      ; preds = %do.end3120
  %arrayidx3122 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1567 = load i64, i64* %arrayidx3122, align 16
  %1568 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 10), align 2
  %idxprom3123 = zext i8 %1568 to i32
  %arrayidx3124 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3123
  %1569 = load i64, i64* %arrayidx3124, align 8
  %add3125 = add i64 %1567, %1569
  %arrayidx3126 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1570 = load i64, i64* %arrayidx3126, align 8
  %add3127 = add i64 %1570, %add3125
  store i64 %add3127, i64* %arrayidx3126, align 8
  %arrayidx3128 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1571 = load i64, i64* %arrayidx3128, align 16
  %arrayidx3129 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1572 = load i64, i64* %arrayidx3129, align 8
  %xor3130 = xor i64 %1571, %1572
  %call3131 = call i64 @rotr64(i64 %xor3130, i32 32)
  %arrayidx3132 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3131, i64* %arrayidx3132, align 16
  %arrayidx3133 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1573 = load i64, i64* %arrayidx3133, align 16
  %arrayidx3134 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1574 = load i64, i64* %arrayidx3134, align 8
  %add3135 = add i64 %1574, %1573
  store i64 %add3135, i64* %arrayidx3134, align 8
  %arrayidx3136 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1575 = load i64, i64* %arrayidx3136, align 16
  %arrayidx3137 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1576 = load i64, i64* %arrayidx3137, align 8
  %xor3138 = xor i64 %1575, %1576
  %call3139 = call i64 @rotr64(i64 %xor3138, i32 24)
  %arrayidx3140 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3139, i64* %arrayidx3140, align 16
  %arrayidx3141 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1577 = load i64, i64* %arrayidx3141, align 16
  %1578 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 11), align 1
  %idxprom3142 = zext i8 %1578 to i32
  %arrayidx3143 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3142
  %1579 = load i64, i64* %arrayidx3143, align 8
  %add3144 = add i64 %1577, %1579
  %arrayidx3145 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1580 = load i64, i64* %arrayidx3145, align 8
  %add3146 = add i64 %1580, %add3144
  store i64 %add3146, i64* %arrayidx3145, align 8
  %arrayidx3147 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1581 = load i64, i64* %arrayidx3147, align 16
  %arrayidx3148 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1582 = load i64, i64* %arrayidx3148, align 8
  %xor3149 = xor i64 %1581, %1582
  %call3150 = call i64 @rotr64(i64 %xor3149, i32 16)
  %arrayidx3151 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3150, i64* %arrayidx3151, align 16
  %arrayidx3152 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1583 = load i64, i64* %arrayidx3152, align 16
  %arrayidx3153 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1584 = load i64, i64* %arrayidx3153, align 8
  %add3154 = add i64 %1584, %1583
  store i64 %add3154, i64* %arrayidx3153, align 8
  %arrayidx3155 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1585 = load i64, i64* %arrayidx3155, align 16
  %arrayidx3156 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1586 = load i64, i64* %arrayidx3156, align 8
  %xor3157 = xor i64 %1585, %1586
  %call3158 = call i64 @rotr64(i64 %xor3157, i32 63)
  %arrayidx3159 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3158, i64* %arrayidx3159, align 16
  br label %do.end3160

do.end3160:                                       ; preds = %do.body3121
  br label %do.body3161

do.body3161:                                      ; preds = %do.end3160
  %arrayidx3162 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1587 = load i64, i64* %arrayidx3162, align 8
  %1588 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 12), align 4
  %idxprom3163 = zext i8 %1588 to i32
  %arrayidx3164 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3163
  %1589 = load i64, i64* %arrayidx3164, align 8
  %add3165 = add i64 %1587, %1589
  %arrayidx3166 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1590 = load i64, i64* %arrayidx3166, align 16
  %add3167 = add i64 %1590, %add3165
  store i64 %add3167, i64* %arrayidx3166, align 16
  %arrayidx3168 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1591 = load i64, i64* %arrayidx3168, align 8
  %arrayidx3169 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1592 = load i64, i64* %arrayidx3169, align 16
  %xor3170 = xor i64 %1591, %1592
  %call3171 = call i64 @rotr64(i64 %xor3170, i32 32)
  %arrayidx3172 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3171, i64* %arrayidx3172, align 8
  %arrayidx3173 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1593 = load i64, i64* %arrayidx3173, align 8
  %arrayidx3174 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1594 = load i64, i64* %arrayidx3174, align 16
  %add3175 = add i64 %1594, %1593
  store i64 %add3175, i64* %arrayidx3174, align 16
  %arrayidx3176 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1595 = load i64, i64* %arrayidx3176, align 8
  %arrayidx3177 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1596 = load i64, i64* %arrayidx3177, align 16
  %xor3178 = xor i64 %1595, %1596
  %call3179 = call i64 @rotr64(i64 %xor3178, i32 24)
  %arrayidx3180 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3179, i64* %arrayidx3180, align 8
  %arrayidx3181 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1597 = load i64, i64* %arrayidx3181, align 8
  %1598 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 13), align 1
  %idxprom3182 = zext i8 %1598 to i32
  %arrayidx3183 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3182
  %1599 = load i64, i64* %arrayidx3183, align 8
  %add3184 = add i64 %1597, %1599
  %arrayidx3185 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1600 = load i64, i64* %arrayidx3185, align 16
  %add3186 = add i64 %1600, %add3184
  store i64 %add3186, i64* %arrayidx3185, align 16
  %arrayidx3187 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1601 = load i64, i64* %arrayidx3187, align 8
  %arrayidx3188 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1602 = load i64, i64* %arrayidx3188, align 16
  %xor3189 = xor i64 %1601, %1602
  %call3190 = call i64 @rotr64(i64 %xor3189, i32 16)
  %arrayidx3191 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3190, i64* %arrayidx3191, align 8
  %arrayidx3192 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1603 = load i64, i64* %arrayidx3192, align 8
  %arrayidx3193 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1604 = load i64, i64* %arrayidx3193, align 16
  %add3194 = add i64 %1604, %1603
  store i64 %add3194, i64* %arrayidx3193, align 16
  %arrayidx3195 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1605 = load i64, i64* %arrayidx3195, align 8
  %arrayidx3196 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1606 = load i64, i64* %arrayidx3196, align 16
  %xor3197 = xor i64 %1605, %1606
  %call3198 = call i64 @rotr64(i64 %xor3197, i32 63)
  %arrayidx3199 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3198, i64* %arrayidx3199, align 8
  br label %do.end3200

do.end3200:                                       ; preds = %do.body3161
  br label %do.body3201

do.body3201:                                      ; preds = %do.end3200
  %arrayidx3202 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1607 = load i64, i64* %arrayidx3202, align 16
  %1608 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 14), align 2
  %idxprom3203 = zext i8 %1608 to i32
  %arrayidx3204 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3203
  %1609 = load i64, i64* %arrayidx3204, align 8
  %add3205 = add i64 %1607, %1609
  %arrayidx3206 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1610 = load i64, i64* %arrayidx3206, align 8
  %add3207 = add i64 %1610, %add3205
  store i64 %add3207, i64* %arrayidx3206, align 8
  %arrayidx3208 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1611 = load i64, i64* %arrayidx3208, align 16
  %arrayidx3209 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1612 = load i64, i64* %arrayidx3209, align 8
  %xor3210 = xor i64 %1611, %1612
  %call3211 = call i64 @rotr64(i64 %xor3210, i32 32)
  %arrayidx3212 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3211, i64* %arrayidx3212, align 16
  %arrayidx3213 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1613 = load i64, i64* %arrayidx3213, align 16
  %arrayidx3214 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1614 = load i64, i64* %arrayidx3214, align 8
  %add3215 = add i64 %1614, %1613
  store i64 %add3215, i64* %arrayidx3214, align 8
  %arrayidx3216 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1615 = load i64, i64* %arrayidx3216, align 16
  %arrayidx3217 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1616 = load i64, i64* %arrayidx3217, align 8
  %xor3218 = xor i64 %1615, %1616
  %call3219 = call i64 @rotr64(i64 %xor3218, i32 24)
  %arrayidx3220 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3219, i64* %arrayidx3220, align 16
  %arrayidx3221 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1617 = load i64, i64* %arrayidx3221, align 16
  %1618 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 9, i32 15), align 1
  %idxprom3222 = zext i8 %1618 to i32
  %arrayidx3223 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3222
  %1619 = load i64, i64* %arrayidx3223, align 8
  %add3224 = add i64 %1617, %1619
  %arrayidx3225 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1620 = load i64, i64* %arrayidx3225, align 8
  %add3226 = add i64 %1620, %add3224
  store i64 %add3226, i64* %arrayidx3225, align 8
  %arrayidx3227 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1621 = load i64, i64* %arrayidx3227, align 16
  %arrayidx3228 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1622 = load i64, i64* %arrayidx3228, align 8
  %xor3229 = xor i64 %1621, %1622
  %call3230 = call i64 @rotr64(i64 %xor3229, i32 16)
  %arrayidx3231 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3230, i64* %arrayidx3231, align 16
  %arrayidx3232 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1623 = load i64, i64* %arrayidx3232, align 16
  %arrayidx3233 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1624 = load i64, i64* %arrayidx3233, align 8
  %add3234 = add i64 %1624, %1623
  store i64 %add3234, i64* %arrayidx3233, align 8
  %arrayidx3235 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1625 = load i64, i64* %arrayidx3235, align 16
  %arrayidx3236 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1626 = load i64, i64* %arrayidx3236, align 8
  %xor3237 = xor i64 %1625, %1626
  %call3238 = call i64 @rotr64(i64 %xor3237, i32 63)
  %arrayidx3239 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3238, i64* %arrayidx3239, align 16
  br label %do.end3240

do.end3240:                                       ; preds = %do.body3201
  br label %do.end3241

do.end3241:                                       ; preds = %do.end3240
  br label %do.body3242

do.body3242:                                      ; preds = %do.end3241
  br label %do.body3243

do.body3243:                                      ; preds = %do.body3242
  %arrayidx3244 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1627 = load i64, i64* %arrayidx3244, align 16
  %1628 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 0), align 16
  %idxprom3245 = zext i8 %1628 to i32
  %arrayidx3246 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3245
  %1629 = load i64, i64* %arrayidx3246, align 8
  %add3247 = add i64 %1627, %1629
  %arrayidx3248 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1630 = load i64, i64* %arrayidx3248, align 16
  %add3249 = add i64 %1630, %add3247
  store i64 %add3249, i64* %arrayidx3248, align 16
  %arrayidx3250 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1631 = load i64, i64* %arrayidx3250, align 16
  %arrayidx3251 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1632 = load i64, i64* %arrayidx3251, align 16
  %xor3252 = xor i64 %1631, %1632
  %call3253 = call i64 @rotr64(i64 %xor3252, i32 32)
  %arrayidx3254 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3253, i64* %arrayidx3254, align 16
  %arrayidx3255 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1633 = load i64, i64* %arrayidx3255, align 16
  %arrayidx3256 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1634 = load i64, i64* %arrayidx3256, align 16
  %add3257 = add i64 %1634, %1633
  store i64 %add3257, i64* %arrayidx3256, align 16
  %arrayidx3258 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1635 = load i64, i64* %arrayidx3258, align 16
  %arrayidx3259 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1636 = load i64, i64* %arrayidx3259, align 16
  %xor3260 = xor i64 %1635, %1636
  %call3261 = call i64 @rotr64(i64 %xor3260, i32 24)
  %arrayidx3262 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3261, i64* %arrayidx3262, align 16
  %arrayidx3263 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1637 = load i64, i64* %arrayidx3263, align 16
  %1638 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 1), align 1
  %idxprom3264 = zext i8 %1638 to i32
  %arrayidx3265 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3264
  %1639 = load i64, i64* %arrayidx3265, align 8
  %add3266 = add i64 %1637, %1639
  %arrayidx3267 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1640 = load i64, i64* %arrayidx3267, align 16
  %add3268 = add i64 %1640, %add3266
  store i64 %add3268, i64* %arrayidx3267, align 16
  %arrayidx3269 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1641 = load i64, i64* %arrayidx3269, align 16
  %arrayidx3270 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1642 = load i64, i64* %arrayidx3270, align 16
  %xor3271 = xor i64 %1641, %1642
  %call3272 = call i64 @rotr64(i64 %xor3271, i32 16)
  %arrayidx3273 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3272, i64* %arrayidx3273, align 16
  %arrayidx3274 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1643 = load i64, i64* %arrayidx3274, align 16
  %arrayidx3275 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1644 = load i64, i64* %arrayidx3275, align 16
  %add3276 = add i64 %1644, %1643
  store i64 %add3276, i64* %arrayidx3275, align 16
  %arrayidx3277 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1645 = load i64, i64* %arrayidx3277, align 16
  %arrayidx3278 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1646 = load i64, i64* %arrayidx3278, align 16
  %xor3279 = xor i64 %1645, %1646
  %call3280 = call i64 @rotr64(i64 %xor3279, i32 63)
  %arrayidx3281 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3280, i64* %arrayidx3281, align 16
  br label %do.end3282

do.end3282:                                       ; preds = %do.body3243
  br label %do.body3283

do.body3283:                                      ; preds = %do.end3282
  %arrayidx3284 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1647 = load i64, i64* %arrayidx3284, align 8
  %1648 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 2), align 2
  %idxprom3285 = zext i8 %1648 to i32
  %arrayidx3286 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3285
  %1649 = load i64, i64* %arrayidx3286, align 8
  %add3287 = add i64 %1647, %1649
  %arrayidx3288 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1650 = load i64, i64* %arrayidx3288, align 8
  %add3289 = add i64 %1650, %add3287
  store i64 %add3289, i64* %arrayidx3288, align 8
  %arrayidx3290 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1651 = load i64, i64* %arrayidx3290, align 8
  %arrayidx3291 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1652 = load i64, i64* %arrayidx3291, align 8
  %xor3292 = xor i64 %1651, %1652
  %call3293 = call i64 @rotr64(i64 %xor3292, i32 32)
  %arrayidx3294 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3293, i64* %arrayidx3294, align 8
  %arrayidx3295 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1653 = load i64, i64* %arrayidx3295, align 8
  %arrayidx3296 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1654 = load i64, i64* %arrayidx3296, align 8
  %add3297 = add i64 %1654, %1653
  store i64 %add3297, i64* %arrayidx3296, align 8
  %arrayidx3298 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1655 = load i64, i64* %arrayidx3298, align 8
  %arrayidx3299 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1656 = load i64, i64* %arrayidx3299, align 8
  %xor3300 = xor i64 %1655, %1656
  %call3301 = call i64 @rotr64(i64 %xor3300, i32 24)
  %arrayidx3302 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3301, i64* %arrayidx3302, align 8
  %arrayidx3303 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1657 = load i64, i64* %arrayidx3303, align 8
  %1658 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 3), align 1
  %idxprom3304 = zext i8 %1658 to i32
  %arrayidx3305 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3304
  %1659 = load i64, i64* %arrayidx3305, align 8
  %add3306 = add i64 %1657, %1659
  %arrayidx3307 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1660 = load i64, i64* %arrayidx3307, align 8
  %add3308 = add i64 %1660, %add3306
  store i64 %add3308, i64* %arrayidx3307, align 8
  %arrayidx3309 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1661 = load i64, i64* %arrayidx3309, align 8
  %arrayidx3310 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1662 = load i64, i64* %arrayidx3310, align 8
  %xor3311 = xor i64 %1661, %1662
  %call3312 = call i64 @rotr64(i64 %xor3311, i32 16)
  %arrayidx3313 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3312, i64* %arrayidx3313, align 8
  %arrayidx3314 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1663 = load i64, i64* %arrayidx3314, align 8
  %arrayidx3315 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1664 = load i64, i64* %arrayidx3315, align 8
  %add3316 = add i64 %1664, %1663
  store i64 %add3316, i64* %arrayidx3315, align 8
  %arrayidx3317 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1665 = load i64, i64* %arrayidx3317, align 8
  %arrayidx3318 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1666 = load i64, i64* %arrayidx3318, align 8
  %xor3319 = xor i64 %1665, %1666
  %call3320 = call i64 @rotr64(i64 %xor3319, i32 63)
  %arrayidx3321 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3320, i64* %arrayidx3321, align 8
  br label %do.end3322

do.end3322:                                       ; preds = %do.body3283
  br label %do.body3323

do.body3323:                                      ; preds = %do.end3322
  %arrayidx3324 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1667 = load i64, i64* %arrayidx3324, align 16
  %1668 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 4), align 4
  %idxprom3325 = zext i8 %1668 to i32
  %arrayidx3326 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3325
  %1669 = load i64, i64* %arrayidx3326, align 8
  %add3327 = add i64 %1667, %1669
  %arrayidx3328 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1670 = load i64, i64* %arrayidx3328, align 16
  %add3329 = add i64 %1670, %add3327
  store i64 %add3329, i64* %arrayidx3328, align 16
  %arrayidx3330 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1671 = load i64, i64* %arrayidx3330, align 16
  %arrayidx3331 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1672 = load i64, i64* %arrayidx3331, align 16
  %xor3332 = xor i64 %1671, %1672
  %call3333 = call i64 @rotr64(i64 %xor3332, i32 32)
  %arrayidx3334 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3333, i64* %arrayidx3334, align 16
  %arrayidx3335 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1673 = load i64, i64* %arrayidx3335, align 16
  %arrayidx3336 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1674 = load i64, i64* %arrayidx3336, align 16
  %add3337 = add i64 %1674, %1673
  store i64 %add3337, i64* %arrayidx3336, align 16
  %arrayidx3338 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1675 = load i64, i64* %arrayidx3338, align 16
  %arrayidx3339 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1676 = load i64, i64* %arrayidx3339, align 16
  %xor3340 = xor i64 %1675, %1676
  %call3341 = call i64 @rotr64(i64 %xor3340, i32 24)
  %arrayidx3342 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3341, i64* %arrayidx3342, align 16
  %arrayidx3343 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1677 = load i64, i64* %arrayidx3343, align 16
  %1678 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 5), align 1
  %idxprom3344 = zext i8 %1678 to i32
  %arrayidx3345 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3344
  %1679 = load i64, i64* %arrayidx3345, align 8
  %add3346 = add i64 %1677, %1679
  %arrayidx3347 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1680 = load i64, i64* %arrayidx3347, align 16
  %add3348 = add i64 %1680, %add3346
  store i64 %add3348, i64* %arrayidx3347, align 16
  %arrayidx3349 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1681 = load i64, i64* %arrayidx3349, align 16
  %arrayidx3350 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1682 = load i64, i64* %arrayidx3350, align 16
  %xor3351 = xor i64 %1681, %1682
  %call3352 = call i64 @rotr64(i64 %xor3351, i32 16)
  %arrayidx3353 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3352, i64* %arrayidx3353, align 16
  %arrayidx3354 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1683 = load i64, i64* %arrayidx3354, align 16
  %arrayidx3355 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1684 = load i64, i64* %arrayidx3355, align 16
  %add3356 = add i64 %1684, %1683
  store i64 %add3356, i64* %arrayidx3355, align 16
  %arrayidx3357 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1685 = load i64, i64* %arrayidx3357, align 16
  %arrayidx3358 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1686 = load i64, i64* %arrayidx3358, align 16
  %xor3359 = xor i64 %1685, %1686
  %call3360 = call i64 @rotr64(i64 %xor3359, i32 63)
  %arrayidx3361 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3360, i64* %arrayidx3361, align 16
  br label %do.end3362

do.end3362:                                       ; preds = %do.body3323
  br label %do.body3363

do.body3363:                                      ; preds = %do.end3362
  %arrayidx3364 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1687 = load i64, i64* %arrayidx3364, align 8
  %1688 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 6), align 2
  %idxprom3365 = zext i8 %1688 to i32
  %arrayidx3366 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3365
  %1689 = load i64, i64* %arrayidx3366, align 8
  %add3367 = add i64 %1687, %1689
  %arrayidx3368 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1690 = load i64, i64* %arrayidx3368, align 8
  %add3369 = add i64 %1690, %add3367
  store i64 %add3369, i64* %arrayidx3368, align 8
  %arrayidx3370 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1691 = load i64, i64* %arrayidx3370, align 8
  %arrayidx3371 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1692 = load i64, i64* %arrayidx3371, align 8
  %xor3372 = xor i64 %1691, %1692
  %call3373 = call i64 @rotr64(i64 %xor3372, i32 32)
  %arrayidx3374 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3373, i64* %arrayidx3374, align 8
  %arrayidx3375 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1693 = load i64, i64* %arrayidx3375, align 8
  %arrayidx3376 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1694 = load i64, i64* %arrayidx3376, align 8
  %add3377 = add i64 %1694, %1693
  store i64 %add3377, i64* %arrayidx3376, align 8
  %arrayidx3378 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1695 = load i64, i64* %arrayidx3378, align 8
  %arrayidx3379 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1696 = load i64, i64* %arrayidx3379, align 8
  %xor3380 = xor i64 %1695, %1696
  %call3381 = call i64 @rotr64(i64 %xor3380, i32 24)
  %arrayidx3382 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3381, i64* %arrayidx3382, align 8
  %arrayidx3383 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1697 = load i64, i64* %arrayidx3383, align 8
  %1698 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 7), align 1
  %idxprom3384 = zext i8 %1698 to i32
  %arrayidx3385 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3384
  %1699 = load i64, i64* %arrayidx3385, align 8
  %add3386 = add i64 %1697, %1699
  %arrayidx3387 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1700 = load i64, i64* %arrayidx3387, align 8
  %add3388 = add i64 %1700, %add3386
  store i64 %add3388, i64* %arrayidx3387, align 8
  %arrayidx3389 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1701 = load i64, i64* %arrayidx3389, align 8
  %arrayidx3390 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1702 = load i64, i64* %arrayidx3390, align 8
  %xor3391 = xor i64 %1701, %1702
  %call3392 = call i64 @rotr64(i64 %xor3391, i32 16)
  %arrayidx3393 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3392, i64* %arrayidx3393, align 8
  %arrayidx3394 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1703 = load i64, i64* %arrayidx3394, align 8
  %arrayidx3395 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1704 = load i64, i64* %arrayidx3395, align 8
  %add3396 = add i64 %1704, %1703
  store i64 %add3396, i64* %arrayidx3395, align 8
  %arrayidx3397 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1705 = load i64, i64* %arrayidx3397, align 8
  %arrayidx3398 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1706 = load i64, i64* %arrayidx3398, align 8
  %xor3399 = xor i64 %1705, %1706
  %call3400 = call i64 @rotr64(i64 %xor3399, i32 63)
  %arrayidx3401 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3400, i64* %arrayidx3401, align 8
  br label %do.end3402

do.end3402:                                       ; preds = %do.body3363
  br label %do.body3403

do.body3403:                                      ; preds = %do.end3402
  %arrayidx3404 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1707 = load i64, i64* %arrayidx3404, align 8
  %1708 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 8), align 8
  %idxprom3405 = zext i8 %1708 to i32
  %arrayidx3406 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3405
  %1709 = load i64, i64* %arrayidx3406, align 8
  %add3407 = add i64 %1707, %1709
  %arrayidx3408 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1710 = load i64, i64* %arrayidx3408, align 16
  %add3409 = add i64 %1710, %add3407
  store i64 %add3409, i64* %arrayidx3408, align 16
  %arrayidx3410 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1711 = load i64, i64* %arrayidx3410, align 8
  %arrayidx3411 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1712 = load i64, i64* %arrayidx3411, align 16
  %xor3412 = xor i64 %1711, %1712
  %call3413 = call i64 @rotr64(i64 %xor3412, i32 32)
  %arrayidx3414 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3413, i64* %arrayidx3414, align 8
  %arrayidx3415 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1713 = load i64, i64* %arrayidx3415, align 8
  %arrayidx3416 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1714 = load i64, i64* %arrayidx3416, align 16
  %add3417 = add i64 %1714, %1713
  store i64 %add3417, i64* %arrayidx3416, align 16
  %arrayidx3418 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1715 = load i64, i64* %arrayidx3418, align 8
  %arrayidx3419 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1716 = load i64, i64* %arrayidx3419, align 16
  %xor3420 = xor i64 %1715, %1716
  %call3421 = call i64 @rotr64(i64 %xor3420, i32 24)
  %arrayidx3422 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3421, i64* %arrayidx3422, align 8
  %arrayidx3423 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1717 = load i64, i64* %arrayidx3423, align 8
  %1718 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 9), align 1
  %idxprom3424 = zext i8 %1718 to i32
  %arrayidx3425 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3424
  %1719 = load i64, i64* %arrayidx3425, align 8
  %add3426 = add i64 %1717, %1719
  %arrayidx3427 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1720 = load i64, i64* %arrayidx3427, align 16
  %add3428 = add i64 %1720, %add3426
  store i64 %add3428, i64* %arrayidx3427, align 16
  %arrayidx3429 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1721 = load i64, i64* %arrayidx3429, align 8
  %arrayidx3430 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1722 = load i64, i64* %arrayidx3430, align 16
  %xor3431 = xor i64 %1721, %1722
  %call3432 = call i64 @rotr64(i64 %xor3431, i32 16)
  %arrayidx3433 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3432, i64* %arrayidx3433, align 8
  %arrayidx3434 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1723 = load i64, i64* %arrayidx3434, align 8
  %arrayidx3435 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1724 = load i64, i64* %arrayidx3435, align 16
  %add3436 = add i64 %1724, %1723
  store i64 %add3436, i64* %arrayidx3435, align 16
  %arrayidx3437 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1725 = load i64, i64* %arrayidx3437, align 8
  %arrayidx3438 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1726 = load i64, i64* %arrayidx3438, align 16
  %xor3439 = xor i64 %1725, %1726
  %call3440 = call i64 @rotr64(i64 %xor3439, i32 63)
  %arrayidx3441 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3440, i64* %arrayidx3441, align 8
  br label %do.end3442

do.end3442:                                       ; preds = %do.body3403
  br label %do.body3443

do.body3443:                                      ; preds = %do.end3442
  %arrayidx3444 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1727 = load i64, i64* %arrayidx3444, align 16
  %1728 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 10), align 2
  %idxprom3445 = zext i8 %1728 to i32
  %arrayidx3446 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3445
  %1729 = load i64, i64* %arrayidx3446, align 8
  %add3447 = add i64 %1727, %1729
  %arrayidx3448 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1730 = load i64, i64* %arrayidx3448, align 8
  %add3449 = add i64 %1730, %add3447
  store i64 %add3449, i64* %arrayidx3448, align 8
  %arrayidx3450 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1731 = load i64, i64* %arrayidx3450, align 16
  %arrayidx3451 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1732 = load i64, i64* %arrayidx3451, align 8
  %xor3452 = xor i64 %1731, %1732
  %call3453 = call i64 @rotr64(i64 %xor3452, i32 32)
  %arrayidx3454 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3453, i64* %arrayidx3454, align 16
  %arrayidx3455 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1733 = load i64, i64* %arrayidx3455, align 16
  %arrayidx3456 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1734 = load i64, i64* %arrayidx3456, align 8
  %add3457 = add i64 %1734, %1733
  store i64 %add3457, i64* %arrayidx3456, align 8
  %arrayidx3458 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1735 = load i64, i64* %arrayidx3458, align 16
  %arrayidx3459 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1736 = load i64, i64* %arrayidx3459, align 8
  %xor3460 = xor i64 %1735, %1736
  %call3461 = call i64 @rotr64(i64 %xor3460, i32 24)
  %arrayidx3462 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3461, i64* %arrayidx3462, align 16
  %arrayidx3463 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1737 = load i64, i64* %arrayidx3463, align 16
  %1738 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 11), align 1
  %idxprom3464 = zext i8 %1738 to i32
  %arrayidx3465 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3464
  %1739 = load i64, i64* %arrayidx3465, align 8
  %add3466 = add i64 %1737, %1739
  %arrayidx3467 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1740 = load i64, i64* %arrayidx3467, align 8
  %add3468 = add i64 %1740, %add3466
  store i64 %add3468, i64* %arrayidx3467, align 8
  %arrayidx3469 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1741 = load i64, i64* %arrayidx3469, align 16
  %arrayidx3470 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1742 = load i64, i64* %arrayidx3470, align 8
  %xor3471 = xor i64 %1741, %1742
  %call3472 = call i64 @rotr64(i64 %xor3471, i32 16)
  %arrayidx3473 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3472, i64* %arrayidx3473, align 16
  %arrayidx3474 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1743 = load i64, i64* %arrayidx3474, align 16
  %arrayidx3475 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1744 = load i64, i64* %arrayidx3475, align 8
  %add3476 = add i64 %1744, %1743
  store i64 %add3476, i64* %arrayidx3475, align 8
  %arrayidx3477 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1745 = load i64, i64* %arrayidx3477, align 16
  %arrayidx3478 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1746 = load i64, i64* %arrayidx3478, align 8
  %xor3479 = xor i64 %1745, %1746
  %call3480 = call i64 @rotr64(i64 %xor3479, i32 63)
  %arrayidx3481 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3480, i64* %arrayidx3481, align 16
  br label %do.end3482

do.end3482:                                       ; preds = %do.body3443
  br label %do.body3483

do.body3483:                                      ; preds = %do.end3482
  %arrayidx3484 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1747 = load i64, i64* %arrayidx3484, align 8
  %1748 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 12), align 4
  %idxprom3485 = zext i8 %1748 to i32
  %arrayidx3486 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3485
  %1749 = load i64, i64* %arrayidx3486, align 8
  %add3487 = add i64 %1747, %1749
  %arrayidx3488 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1750 = load i64, i64* %arrayidx3488, align 16
  %add3489 = add i64 %1750, %add3487
  store i64 %add3489, i64* %arrayidx3488, align 16
  %arrayidx3490 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1751 = load i64, i64* %arrayidx3490, align 8
  %arrayidx3491 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1752 = load i64, i64* %arrayidx3491, align 16
  %xor3492 = xor i64 %1751, %1752
  %call3493 = call i64 @rotr64(i64 %xor3492, i32 32)
  %arrayidx3494 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3493, i64* %arrayidx3494, align 8
  %arrayidx3495 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1753 = load i64, i64* %arrayidx3495, align 8
  %arrayidx3496 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1754 = load i64, i64* %arrayidx3496, align 16
  %add3497 = add i64 %1754, %1753
  store i64 %add3497, i64* %arrayidx3496, align 16
  %arrayidx3498 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1755 = load i64, i64* %arrayidx3498, align 8
  %arrayidx3499 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1756 = load i64, i64* %arrayidx3499, align 16
  %xor3500 = xor i64 %1755, %1756
  %call3501 = call i64 @rotr64(i64 %xor3500, i32 24)
  %arrayidx3502 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3501, i64* %arrayidx3502, align 8
  %arrayidx3503 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1757 = load i64, i64* %arrayidx3503, align 8
  %1758 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 13), align 1
  %idxprom3504 = zext i8 %1758 to i32
  %arrayidx3505 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3504
  %1759 = load i64, i64* %arrayidx3505, align 8
  %add3506 = add i64 %1757, %1759
  %arrayidx3507 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1760 = load i64, i64* %arrayidx3507, align 16
  %add3508 = add i64 %1760, %add3506
  store i64 %add3508, i64* %arrayidx3507, align 16
  %arrayidx3509 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1761 = load i64, i64* %arrayidx3509, align 8
  %arrayidx3510 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1762 = load i64, i64* %arrayidx3510, align 16
  %xor3511 = xor i64 %1761, %1762
  %call3512 = call i64 @rotr64(i64 %xor3511, i32 16)
  %arrayidx3513 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3512, i64* %arrayidx3513, align 8
  %arrayidx3514 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1763 = load i64, i64* %arrayidx3514, align 8
  %arrayidx3515 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1764 = load i64, i64* %arrayidx3515, align 16
  %add3516 = add i64 %1764, %1763
  store i64 %add3516, i64* %arrayidx3515, align 16
  %arrayidx3517 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1765 = load i64, i64* %arrayidx3517, align 8
  %arrayidx3518 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1766 = load i64, i64* %arrayidx3518, align 16
  %xor3519 = xor i64 %1765, %1766
  %call3520 = call i64 @rotr64(i64 %xor3519, i32 63)
  %arrayidx3521 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3520, i64* %arrayidx3521, align 8
  br label %do.end3522

do.end3522:                                       ; preds = %do.body3483
  br label %do.body3523

do.body3523:                                      ; preds = %do.end3522
  %arrayidx3524 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1767 = load i64, i64* %arrayidx3524, align 16
  %1768 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 14), align 2
  %idxprom3525 = zext i8 %1768 to i32
  %arrayidx3526 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3525
  %1769 = load i64, i64* %arrayidx3526, align 8
  %add3527 = add i64 %1767, %1769
  %arrayidx3528 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1770 = load i64, i64* %arrayidx3528, align 8
  %add3529 = add i64 %1770, %add3527
  store i64 %add3529, i64* %arrayidx3528, align 8
  %arrayidx3530 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1771 = load i64, i64* %arrayidx3530, align 16
  %arrayidx3531 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1772 = load i64, i64* %arrayidx3531, align 8
  %xor3532 = xor i64 %1771, %1772
  %call3533 = call i64 @rotr64(i64 %xor3532, i32 32)
  %arrayidx3534 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3533, i64* %arrayidx3534, align 16
  %arrayidx3535 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1773 = load i64, i64* %arrayidx3535, align 16
  %arrayidx3536 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1774 = load i64, i64* %arrayidx3536, align 8
  %add3537 = add i64 %1774, %1773
  store i64 %add3537, i64* %arrayidx3536, align 8
  %arrayidx3538 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1775 = load i64, i64* %arrayidx3538, align 16
  %arrayidx3539 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1776 = load i64, i64* %arrayidx3539, align 8
  %xor3540 = xor i64 %1775, %1776
  %call3541 = call i64 @rotr64(i64 %xor3540, i32 24)
  %arrayidx3542 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3541, i64* %arrayidx3542, align 16
  %arrayidx3543 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1777 = load i64, i64* %arrayidx3543, align 16
  %1778 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 10, i32 15), align 1
  %idxprom3544 = zext i8 %1778 to i32
  %arrayidx3545 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3544
  %1779 = load i64, i64* %arrayidx3545, align 8
  %add3546 = add i64 %1777, %1779
  %arrayidx3547 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1780 = load i64, i64* %arrayidx3547, align 8
  %add3548 = add i64 %1780, %add3546
  store i64 %add3548, i64* %arrayidx3547, align 8
  %arrayidx3549 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1781 = load i64, i64* %arrayidx3549, align 16
  %arrayidx3550 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1782 = load i64, i64* %arrayidx3550, align 8
  %xor3551 = xor i64 %1781, %1782
  %call3552 = call i64 @rotr64(i64 %xor3551, i32 16)
  %arrayidx3553 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3552, i64* %arrayidx3553, align 16
  %arrayidx3554 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1783 = load i64, i64* %arrayidx3554, align 16
  %arrayidx3555 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1784 = load i64, i64* %arrayidx3555, align 8
  %add3556 = add i64 %1784, %1783
  store i64 %add3556, i64* %arrayidx3555, align 8
  %arrayidx3557 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1785 = load i64, i64* %arrayidx3557, align 16
  %arrayidx3558 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1786 = load i64, i64* %arrayidx3558, align 8
  %xor3559 = xor i64 %1785, %1786
  %call3560 = call i64 @rotr64(i64 %xor3559, i32 63)
  %arrayidx3561 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3560, i64* %arrayidx3561, align 16
  br label %do.end3562

do.end3562:                                       ; preds = %do.body3523
  br label %do.end3563

do.end3563:                                       ; preds = %do.end3562
  br label %do.body3564

do.body3564:                                      ; preds = %do.end3563
  br label %do.body3565

do.body3565:                                      ; preds = %do.body3564
  %arrayidx3566 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1787 = load i64, i64* %arrayidx3566, align 16
  %1788 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 0), align 16
  %idxprom3567 = zext i8 %1788 to i32
  %arrayidx3568 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3567
  %1789 = load i64, i64* %arrayidx3568, align 8
  %add3569 = add i64 %1787, %1789
  %arrayidx3570 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1790 = load i64, i64* %arrayidx3570, align 16
  %add3571 = add i64 %1790, %add3569
  store i64 %add3571, i64* %arrayidx3570, align 16
  %arrayidx3572 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1791 = load i64, i64* %arrayidx3572, align 16
  %arrayidx3573 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1792 = load i64, i64* %arrayidx3573, align 16
  %xor3574 = xor i64 %1791, %1792
  %call3575 = call i64 @rotr64(i64 %xor3574, i32 32)
  %arrayidx3576 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3575, i64* %arrayidx3576, align 16
  %arrayidx3577 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1793 = load i64, i64* %arrayidx3577, align 16
  %arrayidx3578 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1794 = load i64, i64* %arrayidx3578, align 16
  %add3579 = add i64 %1794, %1793
  store i64 %add3579, i64* %arrayidx3578, align 16
  %arrayidx3580 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1795 = load i64, i64* %arrayidx3580, align 16
  %arrayidx3581 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1796 = load i64, i64* %arrayidx3581, align 16
  %xor3582 = xor i64 %1795, %1796
  %call3583 = call i64 @rotr64(i64 %xor3582, i32 24)
  %arrayidx3584 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3583, i64* %arrayidx3584, align 16
  %arrayidx3585 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1797 = load i64, i64* %arrayidx3585, align 16
  %1798 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 1), align 1
  %idxprom3586 = zext i8 %1798 to i32
  %arrayidx3587 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3586
  %1799 = load i64, i64* %arrayidx3587, align 8
  %add3588 = add i64 %1797, %1799
  %arrayidx3589 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1800 = load i64, i64* %arrayidx3589, align 16
  %add3590 = add i64 %1800, %add3588
  store i64 %add3590, i64* %arrayidx3589, align 16
  %arrayidx3591 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1801 = load i64, i64* %arrayidx3591, align 16
  %arrayidx3592 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1802 = load i64, i64* %arrayidx3592, align 16
  %xor3593 = xor i64 %1801, %1802
  %call3594 = call i64 @rotr64(i64 %xor3593, i32 16)
  %arrayidx3595 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3594, i64* %arrayidx3595, align 16
  %arrayidx3596 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1803 = load i64, i64* %arrayidx3596, align 16
  %arrayidx3597 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1804 = load i64, i64* %arrayidx3597, align 16
  %add3598 = add i64 %1804, %1803
  store i64 %add3598, i64* %arrayidx3597, align 16
  %arrayidx3599 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1805 = load i64, i64* %arrayidx3599, align 16
  %arrayidx3600 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1806 = load i64, i64* %arrayidx3600, align 16
  %xor3601 = xor i64 %1805, %1806
  %call3602 = call i64 @rotr64(i64 %xor3601, i32 63)
  %arrayidx3603 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3602, i64* %arrayidx3603, align 16
  br label %do.end3604

do.end3604:                                       ; preds = %do.body3565
  br label %do.body3605

do.body3605:                                      ; preds = %do.end3604
  %arrayidx3606 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1807 = load i64, i64* %arrayidx3606, align 8
  %1808 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 2), align 2
  %idxprom3607 = zext i8 %1808 to i32
  %arrayidx3608 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3607
  %1809 = load i64, i64* %arrayidx3608, align 8
  %add3609 = add i64 %1807, %1809
  %arrayidx3610 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1810 = load i64, i64* %arrayidx3610, align 8
  %add3611 = add i64 %1810, %add3609
  store i64 %add3611, i64* %arrayidx3610, align 8
  %arrayidx3612 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1811 = load i64, i64* %arrayidx3612, align 8
  %arrayidx3613 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1812 = load i64, i64* %arrayidx3613, align 8
  %xor3614 = xor i64 %1811, %1812
  %call3615 = call i64 @rotr64(i64 %xor3614, i32 32)
  %arrayidx3616 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3615, i64* %arrayidx3616, align 8
  %arrayidx3617 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1813 = load i64, i64* %arrayidx3617, align 8
  %arrayidx3618 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1814 = load i64, i64* %arrayidx3618, align 8
  %add3619 = add i64 %1814, %1813
  store i64 %add3619, i64* %arrayidx3618, align 8
  %arrayidx3620 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1815 = load i64, i64* %arrayidx3620, align 8
  %arrayidx3621 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1816 = load i64, i64* %arrayidx3621, align 8
  %xor3622 = xor i64 %1815, %1816
  %call3623 = call i64 @rotr64(i64 %xor3622, i32 24)
  %arrayidx3624 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3623, i64* %arrayidx3624, align 8
  %arrayidx3625 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1817 = load i64, i64* %arrayidx3625, align 8
  %1818 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 3), align 1
  %idxprom3626 = zext i8 %1818 to i32
  %arrayidx3627 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3626
  %1819 = load i64, i64* %arrayidx3627, align 8
  %add3628 = add i64 %1817, %1819
  %arrayidx3629 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1820 = load i64, i64* %arrayidx3629, align 8
  %add3630 = add i64 %1820, %add3628
  store i64 %add3630, i64* %arrayidx3629, align 8
  %arrayidx3631 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1821 = load i64, i64* %arrayidx3631, align 8
  %arrayidx3632 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1822 = load i64, i64* %arrayidx3632, align 8
  %xor3633 = xor i64 %1821, %1822
  %call3634 = call i64 @rotr64(i64 %xor3633, i32 16)
  %arrayidx3635 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3634, i64* %arrayidx3635, align 8
  %arrayidx3636 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1823 = load i64, i64* %arrayidx3636, align 8
  %arrayidx3637 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1824 = load i64, i64* %arrayidx3637, align 8
  %add3638 = add i64 %1824, %1823
  store i64 %add3638, i64* %arrayidx3637, align 8
  %arrayidx3639 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1825 = load i64, i64* %arrayidx3639, align 8
  %arrayidx3640 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1826 = load i64, i64* %arrayidx3640, align 8
  %xor3641 = xor i64 %1825, %1826
  %call3642 = call i64 @rotr64(i64 %xor3641, i32 63)
  %arrayidx3643 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3642, i64* %arrayidx3643, align 8
  br label %do.end3644

do.end3644:                                       ; preds = %do.body3605
  br label %do.body3645

do.body3645:                                      ; preds = %do.end3644
  %arrayidx3646 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1827 = load i64, i64* %arrayidx3646, align 16
  %1828 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 4), align 4
  %idxprom3647 = zext i8 %1828 to i32
  %arrayidx3648 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3647
  %1829 = load i64, i64* %arrayidx3648, align 8
  %add3649 = add i64 %1827, %1829
  %arrayidx3650 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1830 = load i64, i64* %arrayidx3650, align 16
  %add3651 = add i64 %1830, %add3649
  store i64 %add3651, i64* %arrayidx3650, align 16
  %arrayidx3652 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1831 = load i64, i64* %arrayidx3652, align 16
  %arrayidx3653 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1832 = load i64, i64* %arrayidx3653, align 16
  %xor3654 = xor i64 %1831, %1832
  %call3655 = call i64 @rotr64(i64 %xor3654, i32 32)
  %arrayidx3656 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3655, i64* %arrayidx3656, align 16
  %arrayidx3657 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1833 = load i64, i64* %arrayidx3657, align 16
  %arrayidx3658 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1834 = load i64, i64* %arrayidx3658, align 16
  %add3659 = add i64 %1834, %1833
  store i64 %add3659, i64* %arrayidx3658, align 16
  %arrayidx3660 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1835 = load i64, i64* %arrayidx3660, align 16
  %arrayidx3661 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1836 = load i64, i64* %arrayidx3661, align 16
  %xor3662 = xor i64 %1835, %1836
  %call3663 = call i64 @rotr64(i64 %xor3662, i32 24)
  %arrayidx3664 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3663, i64* %arrayidx3664, align 16
  %arrayidx3665 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1837 = load i64, i64* %arrayidx3665, align 16
  %1838 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 5), align 1
  %idxprom3666 = zext i8 %1838 to i32
  %arrayidx3667 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3666
  %1839 = load i64, i64* %arrayidx3667, align 8
  %add3668 = add i64 %1837, %1839
  %arrayidx3669 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1840 = load i64, i64* %arrayidx3669, align 16
  %add3670 = add i64 %1840, %add3668
  store i64 %add3670, i64* %arrayidx3669, align 16
  %arrayidx3671 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1841 = load i64, i64* %arrayidx3671, align 16
  %arrayidx3672 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1842 = load i64, i64* %arrayidx3672, align 16
  %xor3673 = xor i64 %1841, %1842
  %call3674 = call i64 @rotr64(i64 %xor3673, i32 16)
  %arrayidx3675 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3674, i64* %arrayidx3675, align 16
  %arrayidx3676 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1843 = load i64, i64* %arrayidx3676, align 16
  %arrayidx3677 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1844 = load i64, i64* %arrayidx3677, align 16
  %add3678 = add i64 %1844, %1843
  store i64 %add3678, i64* %arrayidx3677, align 16
  %arrayidx3679 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1845 = load i64, i64* %arrayidx3679, align 16
  %arrayidx3680 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1846 = load i64, i64* %arrayidx3680, align 16
  %xor3681 = xor i64 %1845, %1846
  %call3682 = call i64 @rotr64(i64 %xor3681, i32 63)
  %arrayidx3683 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3682, i64* %arrayidx3683, align 16
  br label %do.end3684

do.end3684:                                       ; preds = %do.body3645
  br label %do.body3685

do.body3685:                                      ; preds = %do.end3684
  %arrayidx3686 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1847 = load i64, i64* %arrayidx3686, align 8
  %1848 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 6), align 2
  %idxprom3687 = zext i8 %1848 to i32
  %arrayidx3688 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3687
  %1849 = load i64, i64* %arrayidx3688, align 8
  %add3689 = add i64 %1847, %1849
  %arrayidx3690 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1850 = load i64, i64* %arrayidx3690, align 8
  %add3691 = add i64 %1850, %add3689
  store i64 %add3691, i64* %arrayidx3690, align 8
  %arrayidx3692 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1851 = load i64, i64* %arrayidx3692, align 8
  %arrayidx3693 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1852 = load i64, i64* %arrayidx3693, align 8
  %xor3694 = xor i64 %1851, %1852
  %call3695 = call i64 @rotr64(i64 %xor3694, i32 32)
  %arrayidx3696 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3695, i64* %arrayidx3696, align 8
  %arrayidx3697 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1853 = load i64, i64* %arrayidx3697, align 8
  %arrayidx3698 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1854 = load i64, i64* %arrayidx3698, align 8
  %add3699 = add i64 %1854, %1853
  store i64 %add3699, i64* %arrayidx3698, align 8
  %arrayidx3700 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1855 = load i64, i64* %arrayidx3700, align 8
  %arrayidx3701 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1856 = load i64, i64* %arrayidx3701, align 8
  %xor3702 = xor i64 %1855, %1856
  %call3703 = call i64 @rotr64(i64 %xor3702, i32 24)
  %arrayidx3704 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3703, i64* %arrayidx3704, align 8
  %arrayidx3705 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1857 = load i64, i64* %arrayidx3705, align 8
  %1858 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 7), align 1
  %idxprom3706 = zext i8 %1858 to i32
  %arrayidx3707 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3706
  %1859 = load i64, i64* %arrayidx3707, align 8
  %add3708 = add i64 %1857, %1859
  %arrayidx3709 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1860 = load i64, i64* %arrayidx3709, align 8
  %add3710 = add i64 %1860, %add3708
  store i64 %add3710, i64* %arrayidx3709, align 8
  %arrayidx3711 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1861 = load i64, i64* %arrayidx3711, align 8
  %arrayidx3712 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1862 = load i64, i64* %arrayidx3712, align 8
  %xor3713 = xor i64 %1861, %1862
  %call3714 = call i64 @rotr64(i64 %xor3713, i32 16)
  %arrayidx3715 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3714, i64* %arrayidx3715, align 8
  %arrayidx3716 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1863 = load i64, i64* %arrayidx3716, align 8
  %arrayidx3717 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1864 = load i64, i64* %arrayidx3717, align 8
  %add3718 = add i64 %1864, %1863
  store i64 %add3718, i64* %arrayidx3717, align 8
  %arrayidx3719 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1865 = load i64, i64* %arrayidx3719, align 8
  %arrayidx3720 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1866 = load i64, i64* %arrayidx3720, align 8
  %xor3721 = xor i64 %1865, %1866
  %call3722 = call i64 @rotr64(i64 %xor3721, i32 63)
  %arrayidx3723 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3722, i64* %arrayidx3723, align 8
  br label %do.end3724

do.end3724:                                       ; preds = %do.body3685
  br label %do.body3725

do.body3725:                                      ; preds = %do.end3724
  %arrayidx3726 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1867 = load i64, i64* %arrayidx3726, align 8
  %1868 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 8), align 8
  %idxprom3727 = zext i8 %1868 to i32
  %arrayidx3728 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3727
  %1869 = load i64, i64* %arrayidx3728, align 8
  %add3729 = add i64 %1867, %1869
  %arrayidx3730 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1870 = load i64, i64* %arrayidx3730, align 16
  %add3731 = add i64 %1870, %add3729
  store i64 %add3731, i64* %arrayidx3730, align 16
  %arrayidx3732 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1871 = load i64, i64* %arrayidx3732, align 8
  %arrayidx3733 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1872 = load i64, i64* %arrayidx3733, align 16
  %xor3734 = xor i64 %1871, %1872
  %call3735 = call i64 @rotr64(i64 %xor3734, i32 32)
  %arrayidx3736 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3735, i64* %arrayidx3736, align 8
  %arrayidx3737 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1873 = load i64, i64* %arrayidx3737, align 8
  %arrayidx3738 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1874 = load i64, i64* %arrayidx3738, align 16
  %add3739 = add i64 %1874, %1873
  store i64 %add3739, i64* %arrayidx3738, align 16
  %arrayidx3740 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1875 = load i64, i64* %arrayidx3740, align 8
  %arrayidx3741 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1876 = load i64, i64* %arrayidx3741, align 16
  %xor3742 = xor i64 %1875, %1876
  %call3743 = call i64 @rotr64(i64 %xor3742, i32 24)
  %arrayidx3744 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3743, i64* %arrayidx3744, align 8
  %arrayidx3745 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1877 = load i64, i64* %arrayidx3745, align 8
  %1878 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 9), align 1
  %idxprom3746 = zext i8 %1878 to i32
  %arrayidx3747 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3746
  %1879 = load i64, i64* %arrayidx3747, align 8
  %add3748 = add i64 %1877, %1879
  %arrayidx3749 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1880 = load i64, i64* %arrayidx3749, align 16
  %add3750 = add i64 %1880, %add3748
  store i64 %add3750, i64* %arrayidx3749, align 16
  %arrayidx3751 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1881 = load i64, i64* %arrayidx3751, align 8
  %arrayidx3752 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 0
  %1882 = load i64, i64* %arrayidx3752, align 16
  %xor3753 = xor i64 %1881, %1882
  %call3754 = call i64 @rotr64(i64 %xor3753, i32 16)
  %arrayidx3755 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  store i64 %call3754, i64* %arrayidx3755, align 8
  %arrayidx3756 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 15
  %1883 = load i64, i64* %arrayidx3756, align 8
  %arrayidx3757 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1884 = load i64, i64* %arrayidx3757, align 16
  %add3758 = add i64 %1884, %1883
  store i64 %add3758, i64* %arrayidx3757, align 16
  %arrayidx3759 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  %1885 = load i64, i64* %arrayidx3759, align 8
  %arrayidx3760 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 10
  %1886 = load i64, i64* %arrayidx3760, align 16
  %xor3761 = xor i64 %1885, %1886
  %call3762 = call i64 @rotr64(i64 %xor3761, i32 63)
  %arrayidx3763 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 5
  store i64 %call3762, i64* %arrayidx3763, align 8
  br label %do.end3764

do.end3764:                                       ; preds = %do.body3725
  br label %do.body3765

do.body3765:                                      ; preds = %do.end3764
  %arrayidx3766 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1887 = load i64, i64* %arrayidx3766, align 16
  %1888 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 10), align 2
  %idxprom3767 = zext i8 %1888 to i32
  %arrayidx3768 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3767
  %1889 = load i64, i64* %arrayidx3768, align 8
  %add3769 = add i64 %1887, %1889
  %arrayidx3770 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1890 = load i64, i64* %arrayidx3770, align 8
  %add3771 = add i64 %1890, %add3769
  store i64 %add3771, i64* %arrayidx3770, align 8
  %arrayidx3772 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1891 = load i64, i64* %arrayidx3772, align 16
  %arrayidx3773 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1892 = load i64, i64* %arrayidx3773, align 8
  %xor3774 = xor i64 %1891, %1892
  %call3775 = call i64 @rotr64(i64 %xor3774, i32 32)
  %arrayidx3776 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3775, i64* %arrayidx3776, align 16
  %arrayidx3777 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1893 = load i64, i64* %arrayidx3777, align 16
  %arrayidx3778 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1894 = load i64, i64* %arrayidx3778, align 8
  %add3779 = add i64 %1894, %1893
  store i64 %add3779, i64* %arrayidx3778, align 8
  %arrayidx3780 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1895 = load i64, i64* %arrayidx3780, align 16
  %arrayidx3781 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1896 = load i64, i64* %arrayidx3781, align 8
  %xor3782 = xor i64 %1895, %1896
  %call3783 = call i64 @rotr64(i64 %xor3782, i32 24)
  %arrayidx3784 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3783, i64* %arrayidx3784, align 16
  %arrayidx3785 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1897 = load i64, i64* %arrayidx3785, align 16
  %1898 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 11), align 1
  %idxprom3786 = zext i8 %1898 to i32
  %arrayidx3787 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3786
  %1899 = load i64, i64* %arrayidx3787, align 8
  %add3788 = add i64 %1897, %1899
  %arrayidx3789 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1900 = load i64, i64* %arrayidx3789, align 8
  %add3790 = add i64 %1900, %add3788
  store i64 %add3790, i64* %arrayidx3789, align 8
  %arrayidx3791 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1901 = load i64, i64* %arrayidx3791, align 16
  %arrayidx3792 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 1
  %1902 = load i64, i64* %arrayidx3792, align 8
  %xor3793 = xor i64 %1901, %1902
  %call3794 = call i64 @rotr64(i64 %xor3793, i32 16)
  %arrayidx3795 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  store i64 %call3794, i64* %arrayidx3795, align 16
  %arrayidx3796 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 12
  %1903 = load i64, i64* %arrayidx3796, align 16
  %arrayidx3797 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1904 = load i64, i64* %arrayidx3797, align 8
  %add3798 = add i64 %1904, %1903
  store i64 %add3798, i64* %arrayidx3797, align 8
  %arrayidx3799 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  %1905 = load i64, i64* %arrayidx3799, align 16
  %arrayidx3800 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 11
  %1906 = load i64, i64* %arrayidx3800, align 8
  %xor3801 = xor i64 %1905, %1906
  %call3802 = call i64 @rotr64(i64 %xor3801, i32 63)
  %arrayidx3803 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 6
  store i64 %call3802, i64* %arrayidx3803, align 16
  br label %do.end3804

do.end3804:                                       ; preds = %do.body3765
  br label %do.body3805

do.body3805:                                      ; preds = %do.end3804
  %arrayidx3806 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1907 = load i64, i64* %arrayidx3806, align 8
  %1908 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 12), align 4
  %idxprom3807 = zext i8 %1908 to i32
  %arrayidx3808 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3807
  %1909 = load i64, i64* %arrayidx3808, align 8
  %add3809 = add i64 %1907, %1909
  %arrayidx3810 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1910 = load i64, i64* %arrayidx3810, align 16
  %add3811 = add i64 %1910, %add3809
  store i64 %add3811, i64* %arrayidx3810, align 16
  %arrayidx3812 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1911 = load i64, i64* %arrayidx3812, align 8
  %arrayidx3813 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1912 = load i64, i64* %arrayidx3813, align 16
  %xor3814 = xor i64 %1911, %1912
  %call3815 = call i64 @rotr64(i64 %xor3814, i32 32)
  %arrayidx3816 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3815, i64* %arrayidx3816, align 8
  %arrayidx3817 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1913 = load i64, i64* %arrayidx3817, align 8
  %arrayidx3818 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1914 = load i64, i64* %arrayidx3818, align 16
  %add3819 = add i64 %1914, %1913
  store i64 %add3819, i64* %arrayidx3818, align 16
  %arrayidx3820 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1915 = load i64, i64* %arrayidx3820, align 8
  %arrayidx3821 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1916 = load i64, i64* %arrayidx3821, align 16
  %xor3822 = xor i64 %1915, %1916
  %call3823 = call i64 @rotr64(i64 %xor3822, i32 24)
  %arrayidx3824 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3823, i64* %arrayidx3824, align 8
  %arrayidx3825 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1917 = load i64, i64* %arrayidx3825, align 8
  %1918 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 13), align 1
  %idxprom3826 = zext i8 %1918 to i32
  %arrayidx3827 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3826
  %1919 = load i64, i64* %arrayidx3827, align 8
  %add3828 = add i64 %1917, %1919
  %arrayidx3829 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1920 = load i64, i64* %arrayidx3829, align 16
  %add3830 = add i64 %1920, %add3828
  store i64 %add3830, i64* %arrayidx3829, align 16
  %arrayidx3831 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1921 = load i64, i64* %arrayidx3831, align 8
  %arrayidx3832 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 2
  %1922 = load i64, i64* %arrayidx3832, align 16
  %xor3833 = xor i64 %1921, %1922
  %call3834 = call i64 @rotr64(i64 %xor3833, i32 16)
  %arrayidx3835 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  store i64 %call3834, i64* %arrayidx3835, align 8
  %arrayidx3836 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 13
  %1923 = load i64, i64* %arrayidx3836, align 8
  %arrayidx3837 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1924 = load i64, i64* %arrayidx3837, align 16
  %add3838 = add i64 %1924, %1923
  store i64 %add3838, i64* %arrayidx3837, align 16
  %arrayidx3839 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  %1925 = load i64, i64* %arrayidx3839, align 8
  %arrayidx3840 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 8
  %1926 = load i64, i64* %arrayidx3840, align 16
  %xor3841 = xor i64 %1925, %1926
  %call3842 = call i64 @rotr64(i64 %xor3841, i32 63)
  %arrayidx3843 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 7
  store i64 %call3842, i64* %arrayidx3843, align 8
  br label %do.end3844

do.end3844:                                       ; preds = %do.body3805
  br label %do.body3845

do.body3845:                                      ; preds = %do.end3844
  %arrayidx3846 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1927 = load i64, i64* %arrayidx3846, align 16
  %1928 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 14), align 2
  %idxprom3847 = zext i8 %1928 to i32
  %arrayidx3848 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3847
  %1929 = load i64, i64* %arrayidx3848, align 8
  %add3849 = add i64 %1927, %1929
  %arrayidx3850 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1930 = load i64, i64* %arrayidx3850, align 8
  %add3851 = add i64 %1930, %add3849
  store i64 %add3851, i64* %arrayidx3850, align 8
  %arrayidx3852 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1931 = load i64, i64* %arrayidx3852, align 16
  %arrayidx3853 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1932 = load i64, i64* %arrayidx3853, align 8
  %xor3854 = xor i64 %1931, %1932
  %call3855 = call i64 @rotr64(i64 %xor3854, i32 32)
  %arrayidx3856 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3855, i64* %arrayidx3856, align 16
  %arrayidx3857 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1933 = load i64, i64* %arrayidx3857, align 16
  %arrayidx3858 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1934 = load i64, i64* %arrayidx3858, align 8
  %add3859 = add i64 %1934, %1933
  store i64 %add3859, i64* %arrayidx3858, align 8
  %arrayidx3860 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1935 = load i64, i64* %arrayidx3860, align 16
  %arrayidx3861 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1936 = load i64, i64* %arrayidx3861, align 8
  %xor3862 = xor i64 %1935, %1936
  %call3863 = call i64 @rotr64(i64 %xor3862, i32 24)
  %arrayidx3864 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3863, i64* %arrayidx3864, align 16
  %arrayidx3865 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1937 = load i64, i64* %arrayidx3865, align 16
  %1938 = load i8, i8* getelementptr inbounds ([12 x [16 x i8]], [12 x [16 x i8]]* @blake2b_sigma, i32 0, i32 11, i32 15), align 1
  %idxprom3866 = zext i8 %1938 to i32
  %arrayidx3867 = getelementptr [16 x i64], [16 x i64]* %m, i32 0, i32 %idxprom3866
  %1939 = load i64, i64* %arrayidx3867, align 8
  %add3868 = add i64 %1937, %1939
  %arrayidx3869 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1940 = load i64, i64* %arrayidx3869, align 8
  %add3870 = add i64 %1940, %add3868
  store i64 %add3870, i64* %arrayidx3869, align 8
  %arrayidx3871 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1941 = load i64, i64* %arrayidx3871, align 16
  %arrayidx3872 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 3
  %1942 = load i64, i64* %arrayidx3872, align 8
  %xor3873 = xor i64 %1941, %1942
  %call3874 = call i64 @rotr64(i64 %xor3873, i32 16)
  %arrayidx3875 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  store i64 %call3874, i64* %arrayidx3875, align 16
  %arrayidx3876 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 14
  %1943 = load i64, i64* %arrayidx3876, align 16
  %arrayidx3877 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1944 = load i64, i64* %arrayidx3877, align 8
  %add3878 = add i64 %1944, %1943
  store i64 %add3878, i64* %arrayidx3877, align 8
  %arrayidx3879 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  %1945 = load i64, i64* %arrayidx3879, align 16
  %arrayidx3880 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 9
  %1946 = load i64, i64* %arrayidx3880, align 8
  %xor3881 = xor i64 %1945, %1946
  %call3882 = call i64 @rotr64(i64 %xor3881, i32 63)
  %arrayidx3883 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 4
  store i64 %call3882, i64* %arrayidx3883, align 16
  br label %do.end3884

do.end3884:                                       ; preds = %do.body3845
  br label %do.end3885

do.end3885:                                       ; preds = %do.end3884
  store i32 0, i32* %i, align 4
  br label %for.cond3886

for.cond3886:                                     ; preds = %for.inc3898, %do.end3885
  %1947 = load i32, i32* %i, align 4
  %cmp3887 = icmp slt i32 %1947, 8
  br i1 %cmp3887, label %for.body3888, label %for.end3900

for.body3888:                                     ; preds = %for.cond3886
  %1948 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h3889 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %1948, i32 0, i32 0
  %1949 = load i32, i32* %i, align 4
  %arrayidx3890 = getelementptr [8 x i64], [8 x i64]* %h3889, i32 0, i32 %1949
  %1950 = load i64, i64* %arrayidx3890, align 1
  %1951 = load i32, i32* %i, align 4
  %arrayidx3891 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 %1951
  %1952 = load i64, i64* %arrayidx3891, align 8
  %xor3892 = xor i64 %1950, %1952
  %1953 = load i32, i32* %i, align 4
  %add3893 = add i32 %1953, 8
  %arrayidx3894 = getelementptr [16 x i64], [16 x i64]* %v, i32 0, i32 %add3893
  %1954 = load i64, i64* %arrayidx3894, align 8
  %xor3895 = xor i64 %xor3892, %1954
  %1955 = load %struct.blake2b_state*, %struct.blake2b_state** %S.addr, align 4
  %h3896 = getelementptr inbounds %struct.blake2b_state, %struct.blake2b_state* %1955, i32 0, i32 0
  %1956 = load i32, i32* %i, align 4
  %arrayidx3897 = getelementptr [8 x i64], [8 x i64]* %h3896, i32 0, i32 %1956
  store i64 %xor3895, i64* %arrayidx3897, align 1
  br label %for.inc3898

for.inc3898:                                      ; preds = %for.body3888
  %1957 = load i32, i32* %i, align 4
  %inc3899 = add i32 %1957, 1
  store i32 %inc3899, i32* %i, align 4
  br label %for.cond3886

for.end3900:                                      ; preds = %for.cond3886
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal i64 @load64_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i64, align 8
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i64* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 1 %1, i32 8, i1 false)
  %2 = load i64, i64* %w, align 8
  ret i64 %2
}

; Function Attrs: noinline nounwind optnone
define internal i64 @rotr64(i64 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i64, align 8
  %b.addr = alloca i32, align 4
  store i64 %x, i64* %x.addr, align 8
  store i32 %b, i32* %b.addr, align 4
  %0 = load i64, i64* %x.addr, align 8
  %1 = load i32, i32* %b.addr, align 4
  %sh_prom = zext i32 %1 to i64
  %shr = lshr i64 %0, %sh_prom
  %2 = load i64, i64* %x.addr, align 8
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 64, %3
  %sh_prom1 = zext i32 %sub to i64
  %shl = shl i64 %2, %sh_prom1
  %or = or i64 %shr, %shl
  ret i64 %or
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
