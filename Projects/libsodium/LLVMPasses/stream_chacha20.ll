; ModuleID = 'crypto_stream/chacha20/stream_chacha20.c'
source_filename = "crypto_stream/chacha20/stream_chacha20.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_stream_chacha20_implementation = type { i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i8*, i64, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*, i32, i8*)* }

@implementation = internal global %struct.crypto_stream_chacha20_implementation* @crypto_stream_chacha20_ref_implementation, align 4
@crypto_stream_chacha20_ref_implementation = external global %struct.crypto_stream_chacha20_implementation, align 4

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_noncebytes() #0 {
entry:
  ret i32 8
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_messagebytes_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf_noncebytes() #0 {
entry:
  ret i32 12
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf_messagebytes_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20(i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 0
  %2 = load i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i64, i8*, i8*)** %stream, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i64 %4, i8* %5, i8* %6)
  ret i32 %call
}

; Function Attrs: noreturn
declare void @sodium_misuse() #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_xor_ic(i8* nonnull %c, i8* nonnull %m, i64 %mlen, i8* nonnull %n, i64 %ic, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i64 %ic, i64* %ic.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream_xor_ic = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 2
  %2 = load i32 (i8*, i8*, i64, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*, i64, i8*)** %stream_xor_ic, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %7 = load i64, i64* %ic.addr, align 8
  %8 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i8* %4, i64 %5, i8* %6, i64 %7, i8* %8)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_xor(i8* nonnull %c, i8* nonnull %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream_xor_ic = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 2
  %2 = load i32 (i8*, i8*, i64, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*, i64, i8*)** %stream_xor_ic, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %7 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i8* %4, i64 %5, i8* %6, i64 0, i8* %7)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @crypto_stream_chacha20_ietf_ext(i8* %c, i64 %clen, i8* %n, i8* %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream_ietf_ext = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 1
  %2 = load i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i64, i8*, i8*)** %stream_ietf_ext, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i64 %4, i8* %5, i8* %6)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @crypto_stream_chacha20_ietf_ext_xor_ic(i8* %c, i8* %m, i64 %mlen, i8* %n, i32 %ic, i8* %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i32, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i32 %ic, i32* %ic.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream_ietf_ext_xor_ic = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 3
  %2 = load i32 (i8*, i8*, i64, i8*, i32, i8*)*, i32 (i8*, i8*, i64, i8*, i32, i8*)** %stream_ietf_ext_xor_ic, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %7 = load i32, i32* %ic.addr, align 4
  %8 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i8* %4, i64 %5, i8* %6, i32 %7, i8* %8)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf(i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i64, i64* %clen.addr, align 8
  %3 = load i8*, i8** %n.addr, align 4
  %4 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf_ext(i8* %1, i64 %2, i8* %3, i8* %4)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf_xor_ic(i8* nonnull %c, i8* nonnull %m, i64 %mlen, i8* nonnull %n, i32 %ic, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i32, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i32 %ic, i32* %ic.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i32, i32* %ic.addr, align 4
  %conv = zext i32 %0 to i64
  %1 = load i64, i64* %mlen.addr, align 8
  %add = add i64 %1, 63
  %div = udiv i64 %add, 64
  %sub = sub i64 4294967296, %div
  %cmp = icmp ugt i64 %conv, %sub
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i8*, i8** %m.addr, align 4
  %4 = load i64, i64* %mlen.addr, align 8
  %5 = load i8*, i8** %n.addr, align 4
  %6 = load i32, i32* %ic.addr, align 4
  %7 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf_ext_xor_ic(i8* %2, i8* %3, i64 %4, i8* %5, i32 %6, i8* %7)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_stream_chacha20_ietf_xor(i8* nonnull %c, i8* nonnull %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %n.addr, align 4
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf_ext_xor(i8* %1, i8* %2, i64 %3, i8* %4, i8* %5)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @crypto_stream_chacha20_ietf_ext_xor(i8* %c, i8* %m, i64 %mlen, i8* %n, i8* %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  %1 = load %struct.crypto_stream_chacha20_implementation*, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  %stream_ietf_ext_xor_ic = getelementptr inbounds %struct.crypto_stream_chacha20_implementation, %struct.crypto_stream_chacha20_implementation* %1, i32 0, i32 3
  %2 = load i32 (i8*, i8*, i64, i8*, i32, i8*)*, i32 (i8*, i8*, i64, i8*, i32, i8*)** %stream_ietf_ext_xor_ic, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %n.addr, align 4
  %7 = load i8*, i8** %k.addr, align 4
  %call = call i32 %2(i8* %3, i8* %4, i64 %5, i8* %6, i32 0, i8* %7)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define void @crypto_stream_chacha20_ietf_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define void @crypto_stream_chacha20_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_stream_chacha20_pick_best_implementation() #0 {
entry:
  store %struct.crypto_stream_chacha20_implementation* @crypto_stream_chacha20_ref_implementation, %struct.crypto_stream_chacha20_implementation** @implementation, align 4
  ret i32 0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
