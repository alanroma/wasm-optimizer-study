; ModuleID = 'crypto_core/ed25519/core_ristretto255.c'
source_filename = "crypto_core/ed25519/core_ristretto255.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_p1p1 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_cached = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_is_valid_point(i8* nonnull %p) #0 {
entry:
  %retval = alloca i32, align 4
  %p.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %p_p3, i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

declare i32 @ristretto255_frombytes(%struct.ge25519_p3*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_add(i8* nonnull %r, i8* nonnull %p, i8* nonnull %q) #0 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %q.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  %q_p3 = alloca %struct.ge25519_p3, align 4
  %r_p3 = alloca %struct.ge25519_p3, align 4
  %r_p1p1 = alloca %struct.ge25519_p1p1, align 4
  %q_cached = alloca %struct.ge25519_cached, align 4
  store i8* %r, i8** %r.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %q, i8** %q.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %p_p3, i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %q.addr, align 4
  %call1 = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %q_p3, i8* %1)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  call void @ge25519_p3_to_cached(%struct.ge25519_cached* %q_cached, %struct.ge25519_p3* %q_p3)
  call void @ge25519_add(%struct.ge25519_p1p1* %r_p1p1, %struct.ge25519_p3* %p_p3, %struct.ge25519_cached* %q_cached)
  call void @ge25519_p1p1_to_p3(%struct.ge25519_p3* %r_p3, %struct.ge25519_p1p1* %r_p1p1)
  %2 = load i8*, i8** %r.addr, align 4
  call void @ristretto255_p3_tobytes(i8* %2, %struct.ge25519_p3* %r_p3)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare void @ge25519_p3_to_cached(%struct.ge25519_cached*, %struct.ge25519_p3*) #1

declare void @ge25519_add(%struct.ge25519_p1p1*, %struct.ge25519_p3*, %struct.ge25519_cached*) #1

declare void @ge25519_p1p1_to_p3(%struct.ge25519_p3*, %struct.ge25519_p1p1*) #1

declare void @ristretto255_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_sub(i8* nonnull %r, i8* nonnull %p, i8* nonnull %q) #0 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %q.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  %q_p3 = alloca %struct.ge25519_p3, align 4
  %r_p3 = alloca %struct.ge25519_p3, align 4
  %r_p1p1 = alloca %struct.ge25519_p1p1, align 4
  %q_cached = alloca %struct.ge25519_cached, align 4
  store i8* %r, i8** %r.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %q, i8** %q.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %p_p3, i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %q.addr, align 4
  %call1 = call i32 @ristretto255_frombytes(%struct.ge25519_p3* %q_p3, i8* %1)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  call void @ge25519_p3_to_cached(%struct.ge25519_cached* %q_cached, %struct.ge25519_p3* %q_p3)
  call void @ge25519_sub(%struct.ge25519_p1p1* %r_p1p1, %struct.ge25519_p3* %p_p3, %struct.ge25519_cached* %q_cached)
  call void @ge25519_p1p1_to_p3(%struct.ge25519_p3* %r_p3, %struct.ge25519_p1p1* %r_p1p1)
  %2 = load i8*, i8** %r.addr, align 4
  call void @ristretto255_p3_tobytes(i8* %2, %struct.ge25519_p3* %r_p3)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare void @ge25519_sub(%struct.ge25519_p1p1*, %struct.ge25519_p3*, %struct.ge25519_cached*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_from_hash(i8* nonnull %p, i8* nonnull %r) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %r.addr = alloca i8*, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %r, i8** %r.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %1 = load i8*, i8** %r.addr, align 4
  call void @ristretto255_from_hash(i8* %0, i8* %1)
  ret i32 0
}

declare void @ristretto255_from_hash(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_random(i8* nonnull %p) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %h = alloca [64 x i8], align 16
  store i8* %p, i8** %p.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  call void @randombytes_buf(i8* %arraydecay, i32 64)
  %0 = load i8*, i8** %p.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %h, i32 0, i32 0
  %call = call i32 @crypto_core_ristretto255_from_hash(i8* %0, i8* %arraydecay1)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_random(i8* nonnull %r) #0 {
entry:
  %r.addr = alloca i8*, align 4
  store i8* %r, i8** %r.addr, align 4
  %0 = load i8*, i8** %r.addr, align 4
  call void @crypto_core_ed25519_scalar_random(i8* %0)
  ret void
}

declare void @crypto_core_ed25519_scalar_random(i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_scalar_invert(i8* nonnull %recip, i8* nonnull %s) #0 {
entry:
  %recip.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  store i8* %recip, i8** %recip.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %recip.addr, align 4
  %1 = load i8*, i8** %s.addr, align 4
  %call = call i32 @crypto_core_ed25519_scalar_invert(i8* %0, i8* %1)
  ret i32 %call
}

declare i32 @crypto_core_ed25519_scalar_invert(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_negate(i8* nonnull %neg, i8* nonnull %s) #0 {
entry:
  %neg.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  store i8* %neg, i8** %neg.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %neg.addr, align 4
  %1 = load i8*, i8** %s.addr, align 4
  call void @crypto_core_ed25519_scalar_negate(i8* %0, i8* %1)
  ret void
}

declare void @crypto_core_ed25519_scalar_negate(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_complement(i8* nonnull %comp, i8* nonnull %s) #0 {
entry:
  %comp.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  store i8* %comp, i8** %comp.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %comp.addr, align 4
  %1 = load i8*, i8** %s.addr, align 4
  call void @crypto_core_ed25519_scalar_complement(i8* %0, i8* %1)
  ret void
}

declare void @crypto_core_ed25519_scalar_complement(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_add(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %z.addr, align 4
  %1 = load i8*, i8** %x.addr, align 4
  %2 = load i8*, i8** %y.addr, align 4
  call void @crypto_core_ed25519_scalar_add(i8* %0, i8* %1, i8* %2)
  ret void
}

declare void @crypto_core_ed25519_scalar_add(i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_sub(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %z.addr, align 4
  %1 = load i8*, i8** %x.addr, align 4
  %2 = load i8*, i8** %y.addr, align 4
  call void @crypto_core_ed25519_scalar_sub(i8* %0, i8* %1, i8* %2)
  ret void
}

declare void @crypto_core_ed25519_scalar_sub(i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_mul(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %z.addr, align 4
  %1 = load i8*, i8** %x.addr, align 4
  %2 = load i8*, i8** %y.addr, align 4
  call void @sc25519_mul(i8* %0, i8* %1, i8* %2)
  ret void
}

declare void @sc25519_mul(i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ristretto255_scalar_reduce(i8* nonnull %r, i8* nonnull %s) #0 {
entry:
  %r.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  store i8* %r, i8** %r.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %r.addr, align 4
  %1 = load i8*, i8** %s.addr, align 4
  call void @crypto_core_ed25519_scalar_reduce(i8* %0, i8* %1)
  ret void
}

declare void @crypto_core_ed25519_scalar_reduce(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_nonreducedscalarbytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_hashbytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ristretto255_scalarbytes() #0 {
entry:
  ret i32 32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
