; ModuleID = 'crypto_aead/chacha20poly1305/sodium/aead_chacha20poly1305.c'
source_filename = "crypto_aead/chacha20poly1305/sodium/aead_chacha20poly1305.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_onetimeauth_poly1305_state = type { [256 x i8] }

@_pad0 = internal constant [16 x i8] zeroinitializer, align 16

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_encrypt_detached(i8* nonnull %c, i8* nonnull %mac, i64* %maclen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %maclen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block0 = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64* %maclen_p, i64** %maclen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %nsec.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %1 = load i8*, i8** %npub.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20(i8* %arraydecay, i64 64, i8* %1, i8* %2)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call2 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay1)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 64)
  %3 = load i8*, i8** %ad.addr, align 4
  %4 = load i64, i64* %adlen.addr, align 8
  %call4 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %3, i64 %4)
  %arraydecay5 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %5 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay5, i64 %5)
  %arraydecay6 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call7 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay6, i64 8)
  %6 = load i8*, i8** %c.addr, align 4
  %7 = load i8*, i8** %m.addr, align 4
  %8 = load i64, i64* %mlen.addr, align 8
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call8 = call i32 @crypto_stream_chacha20_xor_ic(i8* %6, i8* %7, i64 %8, i8* %9, i64 1, i8* %10)
  %11 = load i8*, i8** %c.addr, align 4
  %12 = load i64, i64* %mlen.addr, align 8
  %call9 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %11, i64 %12)
  %arraydecay10 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %13 = load i64, i64* %mlen.addr, align 8
  call void @store64_le(i8* %arraydecay10, i64 %13)
  %arraydecay11 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call12 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay11, i64 8)
  %14 = load i8*, i8** %mac.addr, align 4
  %call13 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %14)
  %15 = bitcast %struct.crypto_onetimeauth_poly1305_state* %state to i8*
  call void @sodium_memzero(i8* %15, i32 256)
  %16 = load i64*, i64** %maclen_p.addr, align 4
  %cmp = icmp ne i64* %16, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %17 = load i64*, i64** %maclen_p.addr, align 4
  store i64 16, i64* %17, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

declare i32 @crypto_stream_chacha20(i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

declare i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define internal void @store64_le(i8* %dst, i64 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i64, align 8
  store i8* %dst, i8** %dst.addr, align 4
  store i64 %w, i64* %w.addr, align 8
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i64* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

declare i32 @crypto_stream_chacha20_xor_ic(i8*, i8*, i64, i8*, i64, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_encrypt(i8* nonnull %c, i64* %clen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %clen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64* %clen_p, i64** %clen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  store i64 0, i64* %clen, align 8
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967279
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %idx.ext = trunc i64 %3 to i32
  %add.ptr = getelementptr i8, i8* %2, i32 %idx.ext
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %ad.addr, align 4
  %7 = load i64, i64* %adlen.addr, align 8
  %8 = load i8*, i8** %nsec.addr, align 4
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_aead_chacha20poly1305_encrypt_detached(i8* %1, i8* %add.ptr, i64* null, i8* %4, i64 %5, i8* %6, i64 %7, i8* %8, i8* %9, i8* %10)
  store i32 %call, i32* %ret, align 4
  %11 = load i64*, i64** %clen_p.addr, align 4
  %cmp1 = icmp ne i64* %11, null
  br i1 %cmp1, label %if.then2, label %if.end6

if.then2:                                         ; preds = %if.end
  %12 = load i32, i32* %ret, align 4
  %cmp3 = icmp eq i32 %12, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.then2
  %13 = load i64, i64* %mlen.addr, align 8
  %add = add i64 %13, 16
  store i64 %add, i64* %clen, align 8
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.then2
  %14 = load i64, i64* %clen, align 8
  %15 = load i64*, i64** %clen_p.addr, align 4
  store i64 %14, i64* %15, align 8
  br label %if.end6

if.end6:                                          ; preds = %if.end5, %if.end
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noreturn
declare void @sodium_misuse() #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_encrypt_detached(i8* nonnull %c, i8* nonnull %mac, i64* %maclen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %maclen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block0 = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64* %maclen_p, i64** %maclen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %nsec.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %1 = load i8*, i8** %npub.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf(i8* %arraydecay, i64 64, i8* %1, i8* %2)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call2 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay1)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 64)
  %3 = load i8*, i8** %ad.addr, align 4
  %4 = load i64, i64* %adlen.addr, align 8
  %call4 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %3, i64 %4)
  %5 = load i64, i64* %adlen.addr, align 8
  %sub = sub i64 16, %5
  %and = and i64 %sub, 15
  %call5 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and)
  %6 = load i8*, i8** %c.addr, align 4
  %7 = load i8*, i8** %m.addr, align 4
  %8 = load i64, i64* %mlen.addr, align 8
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call6 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %6, i8* %7, i64 %8, i8* %9, i32 1, i8* %10)
  %11 = load i8*, i8** %c.addr, align 4
  %12 = load i64, i64* %mlen.addr, align 8
  %call7 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %11, i64 %12)
  %13 = load i64, i64* %mlen.addr, align 8
  %sub8 = sub i64 16, %13
  %and9 = and i64 %sub8, 15
  %call10 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and9)
  %arraydecay11 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %14 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay11, i64 %14)
  %arraydecay12 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call13 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay12, i64 8)
  %arraydecay14 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %15 = load i64, i64* %mlen.addr, align 8
  call void @store64_le(i8* %arraydecay14, i64 %15)
  %arraydecay15 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call16 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay15, i64 8)
  %16 = load i8*, i8** %mac.addr, align 4
  %call17 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %16)
  %17 = bitcast %struct.crypto_onetimeauth_poly1305_state* %state to i8*
  call void @sodium_memzero(i8* %17, i32 256)
  %18 = load i64*, i64** %maclen_p.addr, align 4
  %cmp = icmp ne i64* %18, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %19 = load i64*, i64** %maclen_p.addr, align 4
  store i64 16, i64* %19, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

declare i32 @crypto_stream_chacha20_ietf(i8*, i64, i8*, i8*) #1

declare i32 @crypto_stream_chacha20_ietf_xor_ic(i8*, i8*, i64, i8*, i32, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_encrypt(i8* nonnull %c, i64* %clen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %clen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64* %clen_p, i64** %clen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  store i64 0, i64* %clen, align 8
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967279
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #5
  unreachable

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %c.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %idx.ext = trunc i64 %3 to i32
  %add.ptr = getelementptr i8, i8* %2, i32 %idx.ext
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %ad.addr, align 4
  %7 = load i64, i64* %adlen.addr, align 8
  %8 = load i8*, i8** %nsec.addr, align 4
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_aead_chacha20poly1305_ietf_encrypt_detached(i8* %1, i8* %add.ptr, i64* null, i8* %4, i64 %5, i8* %6, i64 %7, i8* %8, i8* %9, i8* %10)
  store i32 %call, i32* %ret, align 4
  %11 = load i64*, i64** %clen_p.addr, align 4
  %cmp1 = icmp ne i64* %11, null
  br i1 %cmp1, label %if.then2, label %if.end6

if.then2:                                         ; preds = %if.end
  %12 = load i32, i32* %ret, align 4
  %cmp3 = icmp eq i32 %12, 0
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.then2
  %13 = load i64, i64* %mlen.addr, align 8
  %add = add i64 %13, 16
  store i64 %add, i64* %clen, align 8
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %if.then2
  %14 = load i64, i64* %clen, align 8
  %15 = load i64*, i64** %clen_p.addr, align 4
  store i64 %14, i64* %15, align 8
  br label %if.end6

if.end6:                                          ; preds = %if.end5, %if.end
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_decrypt_detached(i8* %m, i8* %nsec, i8* nonnull %c, i64 %clen, i8* nonnull %mac, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %mac.addr = alloca i8*, align 4
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block0 = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  %computed_mac = alloca [16 x i8], align 16
  %mlen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %nsec.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %1 = load i8*, i8** %npub.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20(i8* %arraydecay, i64 64, i8* %1, i8* %2)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call2 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay1)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 64)
  %3 = load i8*, i8** %ad.addr, align 4
  %4 = load i64, i64* %adlen.addr, align 8
  %call4 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %3, i64 %4)
  %arraydecay5 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %5 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay5, i64 %5)
  %arraydecay6 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call7 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay6, i64 8)
  %6 = load i64, i64* %clen.addr, align 8
  store i64 %6, i64* %mlen, align 8
  %7 = load i8*, i8** %c.addr, align 4
  %8 = load i64, i64* %mlen, align 8
  %call8 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %7, i64 %8)
  %arraydecay9 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %9 = load i64, i64* %mlen, align 8
  call void @store64_le(i8* %arraydecay9, i64 %9)
  %arraydecay10 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call11 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay10, i64 8)
  %arraydecay12 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  %call13 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay12)
  %10 = bitcast %struct.crypto_onetimeauth_poly1305_state* %state to i8*
  call void @sodium_memzero(i8* %10, i32 256)
  %arraydecay14 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  %11 = load i8*, i8** %mac.addr, align 4
  %call15 = call i32 @crypto_verify_16(i8* %arraydecay14, i8* %11)
  store i32 %call15, i32* %ret, align 4
  %arraydecay16 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay16, i32 16)
  %12 = load i8*, i8** %m.addr, align 4
  %cmp = icmp eq i8* %12, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = load i32, i32* %ret, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %14 = load i32, i32* %ret, align 4
  %cmp17 = icmp ne i32 %14, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end
  %15 = load i8*, i8** %m.addr, align 4
  %16 = load i64, i64* %mlen, align 8
  %conv = trunc i64 %16 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %15, i8 0, i32 %conv, i1 false)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end
  %17 = load i8*, i8** %m.addr, align 4
  %18 = load i8*, i8** %c.addr, align 4
  %19 = load i64, i64* %mlen, align 8
  %20 = load i8*, i8** %npub.addr, align 4
  %21 = load i8*, i8** %k.addr, align 4
  %call20 = call i32 @crypto_stream_chacha20_xor_ic(i8* %17, i8* %18, i64 %19, i8* %20, i64 1, i8* %21)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.then18, %if.then
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

declare i32 @crypto_verify_16(i8*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_decrypt(i8* %m, i64* %mlen_p, i8* %nsec, i8* nonnull %c, i64 %clen, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %mlen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  store i64 0, i64* %mlen, align 8
  store i32 -1, i32* %ret, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp uge i64 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i8*, i8** %nsec.addr, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %4, 16
  %5 = load i8*, i8** %c.addr, align 4
  %6 = load i64, i64* %clen.addr, align 8
  %idx.ext = trunc i64 %6 to i32
  %add.ptr = getelementptr i8, i8* %5, i32 %idx.ext
  %add.ptr1 = getelementptr i8, i8* %add.ptr, i32 -16
  %7 = load i8*, i8** %ad.addr, align 4
  %8 = load i64, i64* %adlen.addr, align 8
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_aead_chacha20poly1305_decrypt_detached(i8* %1, i8* %2, i8* %3, i64 %sub, i8* %add.ptr1, i8* %7, i64 %8, i8* %9, i8* %10)
  store i32 %call, i32* %ret, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i64*, i64** %mlen_p.addr, align 4
  %cmp2 = icmp ne i64* %11, null
  br i1 %cmp2, label %if.then3, label %if.end8

if.then3:                                         ; preds = %if.end
  %12 = load i32, i32* %ret, align 4
  %cmp4 = icmp eq i32 %12, 0
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.then3
  %13 = load i64, i64* %clen.addr, align 8
  %sub6 = sub i64 %13, 16
  store i64 %sub6, i64* %mlen, align 8
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.then3
  %14 = load i64, i64* %mlen, align 8
  %15 = load i64*, i64** %mlen_p.addr, align 4
  store i64 %14, i64* %15, align 8
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %if.end
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_decrypt_detached(i8* %m, i8* %nsec, i8* nonnull %c, i64 %clen, i8* nonnull %mac, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %mac.addr = alloca i8*, align 4
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_onetimeauth_poly1305_state, align 16
  %block0 = alloca [64 x i8], align 16
  %slen = alloca [8 x i8], align 1
  %computed_mac = alloca [16 x i8], align 16
  %mlen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %nsec.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %1 = load i8*, i8** %npub.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_chacha20_ietf(i8* %arraydecay, i64 64, i8* %1, i8* %2)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  %call2 = call i32 @crypto_onetimeauth_poly1305_init(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay1)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %block0, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 64)
  %3 = load i8*, i8** %ad.addr, align 4
  %4 = load i64, i64* %adlen.addr, align 8
  %call4 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %3, i64 %4)
  %5 = load i64, i64* %adlen.addr, align 8
  %sub = sub i64 16, %5
  %and = and i64 %sub, 15
  %call5 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and)
  %6 = load i64, i64* %clen.addr, align 8
  store i64 %6, i64* %mlen, align 8
  %7 = load i8*, i8** %c.addr, align 4
  %8 = load i64, i64* %mlen, align 8
  %call6 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %7, i64 %8)
  %9 = load i64, i64* %mlen, align 8
  %sub7 = sub i64 16, %9
  %and8 = and i64 %sub7, 15
  %call9 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_pad0, i32 0, i32 0), i64 %and8)
  %arraydecay10 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %10 = load i64, i64* %adlen.addr, align 8
  call void @store64_le(i8* %arraydecay10, i64 %10)
  %arraydecay11 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call12 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay11, i64 8)
  %arraydecay13 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %11 = load i64, i64* %mlen, align 8
  call void @store64_le(i8* %arraydecay13, i64 %11)
  %arraydecay14 = getelementptr inbounds [8 x i8], [8 x i8]* %slen, i32 0, i32 0
  %call15 = call i32 @crypto_onetimeauth_poly1305_update(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay14, i64 8)
  %arraydecay16 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  %call17 = call i32 @crypto_onetimeauth_poly1305_final(%struct.crypto_onetimeauth_poly1305_state* %state, i8* %arraydecay16)
  %12 = bitcast %struct.crypto_onetimeauth_poly1305_state* %state to i8*
  call void @sodium_memzero(i8* %12, i32 256)
  %arraydecay18 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  %13 = load i8*, i8** %mac.addr, align 4
  %call19 = call i32 @crypto_verify_16(i8* %arraydecay18, i8* %13)
  store i32 %call19, i32* %ret, align 4
  %arraydecay20 = getelementptr inbounds [16 x i8], [16 x i8]* %computed_mac, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay20, i32 16)
  %14 = load i8*, i8** %m.addr, align 4
  %cmp = icmp eq i8* %14, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = load i32, i32* %ret, align 4
  store i32 %15, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %16 = load i32, i32* %ret, align 4
  %cmp21 = icmp ne i32 %16, 0
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end
  %17 = load i8*, i8** %m.addr, align 4
  %18 = load i64, i64* %mlen, align 8
  %conv = trunc i64 %18 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %17, i8 0, i32 %conv, i1 false)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.end
  %19 = load i8*, i8** %m.addr, align 4
  %20 = load i8*, i8** %c.addr, align 4
  %21 = load i64, i64* %mlen, align 8
  %22 = load i8*, i8** %npub.addr, align 4
  %23 = load i8*, i8** %k.addr, align 4
  %call24 = call i32 @crypto_stream_chacha20_ietf_xor_ic(i8* %19, i8* %20, i64 %21, i8* %22, i32 1, i8* %23)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end23, %if.then22, %if.then
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_decrypt(i8* %m, i64* %mlen_p, i8* %nsec, i8* nonnull %c, i64 %clen, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %mlen = alloca i64, align 8
  %ret = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  store i64 0, i64* %mlen, align 8
  store i32 -1, i32* %ret, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp uge i64 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i8*, i8** %nsec.addr, align 4
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %4, 16
  %5 = load i8*, i8** %c.addr, align 4
  %6 = load i64, i64* %clen.addr, align 8
  %idx.ext = trunc i64 %6 to i32
  %add.ptr = getelementptr i8, i8* %5, i32 %idx.ext
  %add.ptr1 = getelementptr i8, i8* %add.ptr, i32 -16
  %7 = load i8*, i8** %ad.addr, align 4
  %8 = load i64, i64* %adlen.addr, align 8
  %9 = load i8*, i8** %npub.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_aead_chacha20poly1305_ietf_decrypt_detached(i8* %1, i8* %2, i8* %3, i64 %sub, i8* %add.ptr1, i8* %7, i64 %8, i8* %9, i8* %10)
  store i32 %call, i32* %ret, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i64*, i64** %mlen_p.addr, align 4
  %cmp2 = icmp ne i64* %11, null
  br i1 %cmp2, label %if.then3, label %if.end8

if.then3:                                         ; preds = %if.end
  %12 = load i32, i32* %ret, align 4
  %cmp4 = icmp eq i32 %12, 0
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.then3
  %13 = load i64, i64* %clen.addr, align 8
  %sub6 = sub i64 %13, 16
  store i64 %sub6, i64* %mlen, align 8
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.then3
  %14 = load i64, i64* %mlen, align 8
  %15 = load i64*, i64** %mlen_p.addr, align 4
  store i64 %14, i64* %15, align 8
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %if.end
  %16 = load i32, i32* %ret, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_npubbytes() #0 {
entry:
  ret i32 12
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_nsecbytes() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_abytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_ietf_messagebytes_max() #0 {
entry:
  ret i32 -17
}

; Function Attrs: noinline nounwind optnone
define void @crypto_aead_chacha20poly1305_ietf_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_npubbytes() #0 {
entry:
  ret i32 8
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_nsecbytes() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_abytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_chacha20poly1305_messagebytes_max() #0 {
entry:
  ret i32 -17
}

; Function Attrs: noinline nounwind optnone
define void @crypto_aead_chacha20poly1305_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
