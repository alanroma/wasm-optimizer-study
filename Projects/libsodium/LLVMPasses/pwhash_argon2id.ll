; ModuleID = 'crypto_pwhash/argon2/pwhash_argon2id.c'
source_filename = "crypto_pwhash/argon2/pwhash_argon2id.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [11 x i8] c"$argon2id$\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_alg_argon2id13() #0 {
entry:
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_bytes_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_passwd_min() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_passwd_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_saltbytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_strbytes() #0 {
entry:
  ret i32 128
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_pwhash_argon2id_strprefix() #0 {
entry:
  ret i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_opslimit_min() #0 {
entry:
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_opslimit_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_memlimit_min() #0 {
entry:
  ret i32 8192
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_memlimit_max() #0 {
entry:
  ret i32 -2147483648
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_opslimit_interactive() #0 {
entry:
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_memlimit_interactive() #0 {
entry:
  ret i32 67108864
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_opslimit_moderate() #0 {
entry:
  ret i32 3
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_memlimit_moderate() #0 {
entry:
  ret i32 268435456
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_opslimit_sensitive() #0 {
entry:
  ret i32 4
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_memlimit_sensitive() #0 {
entry:
  ret i32 1073741824
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id(i8* nonnull %out, i64 %outlen, i8* nonnull %passwd, i64 %passwdlen, i8* nonnull %salt, i64 %opslimit, i32 %memlimit, i32 %alg) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i64, align 8
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %salt.addr = alloca i8*, align 4
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %alg.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i64 %outlen, i64* %outlen.addr, align 8
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i8* %salt, i8** %salt.addr, align 4
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  store i32 %alg, i32* %alg.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i64, i64* %outlen.addr, align 8
  %conv = trunc i64 %1 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 %conv, i1 false)
  %2 = load i64, i64* %outlen.addr, align 8
  %cmp = icmp ugt i64 %2, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i64, i64* %outlen.addr, align 8
  %cmp2 = icmp ult i64 %3, 16
  br i1 %cmp2, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %call5 = call i32* @__errno_location()
  store i32 28, i32* %call5, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %4 = load i64, i64* %passwdlen.addr, align 8
  %cmp7 = icmp ugt i64 %4, 4294967295
  br i1 %cmp7, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %5 = load i64, i64* %opslimit.addr, align 8
  %cmp9 = icmp ugt i64 %5, 4294967295
  br i1 %cmp9, label %if.then15, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %6 = load i32, i32* %memlimit.addr, align 4
  %conv12 = zext i32 %6 to i64
  %cmp13 = icmp ugt i64 %conv12, 2147483648
  br i1 %cmp13, label %if.then15, label %if.end17

if.then15:                                        ; preds = %lor.lhs.false11, %lor.lhs.false, %if.end6
  %call16 = call i32* @__errno_location()
  store i32 22, i32* %call16, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %lor.lhs.false11
  %7 = load i64, i64* %passwdlen.addr, align 8
  %cmp18 = icmp ult i64 %7, 0
  br i1 %cmp18, label %if.then26, label %lor.lhs.false20

lor.lhs.false20:                                  ; preds = %if.end17
  %8 = load i64, i64* %opslimit.addr, align 8
  %cmp21 = icmp ult i64 %8, 1
  br i1 %cmp21, label %if.then26, label %lor.lhs.false23

lor.lhs.false23:                                  ; preds = %lor.lhs.false20
  %9 = load i32, i32* %memlimit.addr, align 4
  %cmp24 = icmp ult i32 %9, 8192
  br i1 %cmp24, label %if.then26, label %if.end28

if.then26:                                        ; preds = %lor.lhs.false23, %lor.lhs.false20, %if.end17
  %call27 = call i32* @__errno_location()
  store i32 28, i32* %call27, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end28:                                         ; preds = %lor.lhs.false23
  %10 = load i32, i32* %alg.addr, align 4
  switch i32 %10, label %sw.default [
    i32 2, label %sw.bb
  ]

sw.bb:                                            ; preds = %if.end28
  %11 = load i64, i64* %opslimit.addr, align 8
  %conv29 = trunc i64 %11 to i32
  %12 = load i32, i32* %memlimit.addr, align 4
  %div = udiv i32 %12, 1024
  %13 = load i8*, i8** %passwd.addr, align 4
  %14 = load i64, i64* %passwdlen.addr, align 8
  %conv30 = trunc i64 %14 to i32
  %15 = load i8*, i8** %salt.addr, align 4
  %16 = load i8*, i8** %out.addr, align 4
  %17 = load i64, i64* %outlen.addr, align 8
  %conv31 = trunc i64 %17 to i32
  %call32 = call i32 @argon2id_hash_raw(i32 %conv29, i32 %div, i32 1, i8* %13, i32 %conv30, i8* %15, i32 16, i8* %16, i32 %conv31)
  %cmp33 = icmp ne i32 %call32, 0
  br i1 %cmp33, label %if.then35, label %if.end36

if.then35:                                        ; preds = %sw.bb
  store i32 -1, i32* %retval, align 4
  br label %return

if.end36:                                         ; preds = %sw.bb
  store i32 0, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %if.end28
  %call37 = call i32* @__errno_location()
  store i32 28, i32* %call37, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %if.end36, %if.then35, %if.then26, %if.then15, %if.then4, %if.then
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

declare i32* @__errno_location() #2

declare i32 @argon2id_hash_raw(i32, i32, i32, i8*, i32, i8*, i32, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_str(i8* nonnull %out, i8* nonnull %passwd, i64 %passwdlen, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %salt = alloca [16 x i8], align 16
  store i8* %out, i8** %out.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  call void @llvm.memset.p0i8.i32(i8* align 1 %0, i8 0, i32 128, i1 false)
  %1 = load i64, i64* %passwdlen.addr, align 8
  %cmp = icmp ugt i64 %1, 4294967295
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i64, i64* %opslimit.addr, align 8
  %cmp1 = icmp ugt i64 %2, 4294967295
  br i1 %cmp1, label %if.then, label %lor.lhs.false2

lor.lhs.false2:                                   ; preds = %lor.lhs.false
  %3 = load i32, i32* %memlimit.addr, align 4
  %conv = zext i32 %3 to i64
  %cmp3 = icmp ugt i64 %conv, 2147483648
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false2, %lor.lhs.false, %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false2
  %4 = load i64, i64* %passwdlen.addr, align 8
  %cmp5 = icmp ult i64 %4, 0
  br i1 %cmp5, label %if.then13, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end
  %5 = load i64, i64* %opslimit.addr, align 8
  %cmp8 = icmp ult i64 %5, 1
  br i1 %cmp8, label %if.then13, label %lor.lhs.false10

lor.lhs.false10:                                  ; preds = %lor.lhs.false7
  %6 = load i32, i32* %memlimit.addr, align 4
  %cmp11 = icmp ult i32 %6, 8192
  br i1 %cmp11, label %if.then13, label %if.end15

if.then13:                                        ; preds = %lor.lhs.false10, %lor.lhs.false7, %if.end
  %call14 = call i32* @__errno_location()
  store i32 28, i32* %call14, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %lor.lhs.false10
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  call void @randombytes_buf(i8* %arraydecay, i32 16)
  %7 = load i64, i64* %opslimit.addr, align 8
  %conv16 = trunc i64 %7 to i32
  %8 = load i32, i32* %memlimit.addr, align 4
  %div = udiv i32 %8, 1024
  %9 = load i8*, i8** %passwd.addr, align 4
  %10 = load i64, i64* %passwdlen.addr, align 8
  %conv17 = trunc i64 %10 to i32
  %arraydecay18 = getelementptr inbounds [16 x i8], [16 x i8]* %salt, i32 0, i32 0
  %11 = load i8*, i8** %out.addr, align 4
  %call19 = call i32 @argon2id_hash_encoded(i32 %conv16, i32 %div, i32 1, i8* %9, i32 %conv17, i8* %arraydecay18, i32 16, i32 32, i8* %11, i32 128)
  %cmp20 = icmp ne i32 %call19, 0
  br i1 %cmp20, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end15
  store i32 -1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.end15
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end23, %if.then22, %if.then13, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare void @randombytes_buf(i8*, i32) #2

declare i32 @argon2id_hash_encoded(i32, i32, i32, i8*, i32, i8*, i32, i32, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_argon2id_str_verify(i8* nonnull %str, i8* nonnull %passwd, i64 %passwdlen) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %verify_ret = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  %0 = load i64, i64* %passwdlen.addr, align 8
  %cmp = icmp ugt i64 %0, 4294967295
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i64, i64* %passwdlen.addr, align 8
  %cmp1 = icmp ult i64 %1, 0
  br i1 %cmp1, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %call3 = call i32* @__errno_location()
  store i32 28, i32* %call3, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %2 = load i8*, i8** %str.addr, align 4
  %3 = load i8*, i8** %passwd.addr, align 4
  %4 = load i64, i64* %passwdlen.addr, align 8
  %conv = trunc i64 %4 to i32
  %call5 = call i32 @argon2id_verify(i8* %2, i8* %3, i32 %conv)
  store i32 %call5, i32* %verify_ret, align 4
  %5 = load i32, i32* %verify_ret, align 4
  %cmp6 = icmp eq i32 %5, 0
  br i1 %cmp6, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end4
  store i32 0, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end4
  %6 = load i32, i32* %verify_ret, align 4
  %cmp10 = icmp eq i32 %6, -35
  br i1 %cmp10, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end9
  %call13 = call i32* @__errno_location()
  store i32 28, i32* %call13, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end14, %if.then8, %if.then2, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

declare i32 @argon2id_verify(i8*, i8*, i32) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
