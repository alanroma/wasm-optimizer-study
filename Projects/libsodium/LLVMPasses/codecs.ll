; ModuleID = 'sodium/codecs.c'
source_filename = "sodium/codecs.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [19 x i8] c"b64_pos <= b64_len\00", align 1
@.str.1 = private unnamed_addr constant [16 x i8] c"sodium/codecs.c\00", align 1
@__func__.sodium_bin2base64 = private unnamed_addr constant [18 x i8] c"sodium_bin2base64\00", align 1

; Function Attrs: noinline nounwind optnone
define i8* @sodium_bin2hex(i8* nonnull %hex, i32 %hex_maxlen, i8* %bin, i32 %bin_len) #0 {
entry:
  %hex.addr = alloca i8*, align 4
  %hex_maxlen.addr = alloca i32, align 4
  %bin.addr = alloca i8*, align 4
  %bin_len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %hex, i8** %hex.addr, align 4
  store i32 %hex_maxlen, i32* %hex_maxlen.addr, align 4
  store i8* %bin, i8** %bin.addr, align 4
  store i32 %bin_len, i32* %bin_len.addr, align 4
  store i32 0, i32* %i, align 4
  %0 = load i32, i32* %bin_len.addr, align 4
  %cmp = icmp uge i32 %0, 2147483647
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %hex_maxlen.addr, align 4
  %2 = load i32, i32* %bin_len.addr, align 4
  %mul = mul i32 %2, 2
  %cmp1 = icmp ule i32 %1, %mul
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %lor.lhs.false
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %bin_len.addr, align 4
  %cmp2 = icmp ult i32 %3, %4
  br i1 %cmp2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i8*, i8** %bin.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %7 to i32
  %and = and i32 %conv, 15
  store i32 %and, i32* %c, align 4
  %8 = load i8*, i8** %bin.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %10 to i32
  %shr = ashr i32 %conv4, 4
  store i32 %shr, i32* %b, align 4
  %11 = load i32, i32* %c, align 4
  %add = add i32 87, %11
  %12 = load i32, i32* %c, align 4
  %sub = sub i32 %12, 10
  %shr5 = lshr i32 %sub, 8
  %and6 = and i32 %shr5, -39
  %add7 = add i32 %add, %and6
  %conv8 = trunc i32 %add7 to i8
  %conv9 = zext i8 %conv8 to i32
  %shl = shl i32 %conv9, 8
  %13 = load i32, i32* %b, align 4
  %add10 = add i32 87, %13
  %14 = load i32, i32* %b, align 4
  %sub11 = sub i32 %14, 10
  %shr12 = lshr i32 %sub11, 8
  %and13 = and i32 %shr12, -39
  %add14 = add i32 %add10, %and13
  %conv15 = trunc i32 %add14 to i8
  %conv16 = zext i8 %conv15 to i32
  %or = or i32 %shl, %conv16
  store i32 %or, i32* %x, align 4
  %15 = load i32, i32* %x, align 4
  %conv17 = trunc i32 %15 to i8
  %16 = load i8*, i8** %hex.addr, align 4
  %17 = load i32, i32* %i, align 4
  %mul18 = mul i32 %17, 2
  %arrayidx19 = getelementptr i8, i8* %16, i32 %mul18
  store i8 %conv17, i8* %arrayidx19, align 1
  %18 = load i32, i32* %x, align 4
  %shr20 = lshr i32 %18, 8
  store i32 %shr20, i32* %x, align 4
  %19 = load i32, i32* %x, align 4
  %conv21 = trunc i32 %19 to i8
  %20 = load i8*, i8** %hex.addr, align 4
  %21 = load i32, i32* %i, align 4
  %mul22 = mul i32 %21, 2
  %add23 = add i32 %mul22, 1
  %arrayidx24 = getelementptr i8, i8* %20, i32 %add23
  store i8 %conv21, i8* %arrayidx24, align 1
  %22 = load i32, i32* %i, align 4
  %inc = add i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = load i8*, i8** %hex.addr, align 4
  %24 = load i32, i32* %i, align 4
  %mul25 = mul i32 %24, 2
  %arrayidx26 = getelementptr i8, i8* %23, i32 %mul25
  store i8 0, i8* %arrayidx26, align 1
  %25 = load i8*, i8** %hex.addr, align 4
  ret i8* %25
}

; Function Attrs: noreturn
declare void @sodium_misuse() #1

; Function Attrs: noinline nounwind optnone
define i32 @sodium_hex2bin(i8* nonnull %bin, i32 %bin_maxlen, i8* %hex, i32 %hex_len, i8* %ignore, i32* %bin_len, i8** %hex_end) #0 {
entry:
  %bin.addr = alloca i8*, align 4
  %bin_maxlen.addr = alloca i32, align 4
  %hex.addr = alloca i8*, align 4
  %hex_len.addr = alloca i32, align 4
  %ignore.addr = alloca i8*, align 4
  %bin_len.addr = alloca i32*, align 4
  %hex_end.addr = alloca i8**, align 4
  %bin_pos = alloca i32, align 4
  %hex_pos = alloca i32, align 4
  %ret = alloca i32, align 4
  %c = alloca i8, align 1
  %c_acc = alloca i8, align 1
  %c_alpha0 = alloca i8, align 1
  %c_alpha = alloca i8, align 1
  %c_num0 = alloca i8, align 1
  %c_num = alloca i8, align 1
  %c_val = alloca i8, align 1
  %state = alloca i8, align 1
  store i8* %bin, i8** %bin.addr, align 4
  store i32 %bin_maxlen, i32* %bin_maxlen.addr, align 4
  store i8* %hex, i8** %hex.addr, align 4
  store i32 %hex_len, i32* %hex_len.addr, align 4
  store i8* %ignore, i8** %ignore.addr, align 4
  store i32* %bin_len, i32** %bin_len.addr, align 4
  store i8** %hex_end, i8*** %hex_end.addr, align 4
  store i32 0, i32* %bin_pos, align 4
  store i32 0, i32* %hex_pos, align 4
  store i32 0, i32* %ret, align 4
  store i8 0, i8* %c_acc, align 1
  store i8 0, i8* %state, align 1
  br label %while.cond

while.cond:                                       ; preds = %if.end54, %if.then27, %entry
  %0 = load i32, i32* %hex_pos, align 4
  %1 = load i32, i32* %hex_len.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i8*, i8** %hex.addr, align 4
  %3 = load i32, i32* %hex_pos, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  store i8 %4, i8* %c, align 1
  %5 = load i8, i8* %c, align 1
  %conv = zext i8 %5 to i32
  %xor = xor i32 %conv, 48
  %conv1 = trunc i32 %xor to i8
  store i8 %conv1, i8* %c_num, align 1
  %6 = load i8, i8* %c_num, align 1
  %conv2 = zext i8 %6 to i32
  %sub = sub i32 %conv2, 10
  %shr = lshr i32 %sub, 8
  %conv3 = trunc i32 %shr to i8
  store i8 %conv3, i8* %c_num0, align 1
  %7 = load i8, i8* %c, align 1
  %conv4 = zext i8 %7 to i32
  %and = and i32 %conv4, -33
  %sub5 = sub i32 %and, 55
  %conv6 = trunc i32 %sub5 to i8
  store i8 %conv6, i8* %c_alpha, align 1
  %8 = load i8, i8* %c_alpha, align 1
  %conv7 = zext i8 %8 to i32
  %sub8 = sub i32 %conv7, 10
  %9 = load i8, i8* %c_alpha, align 1
  %conv9 = zext i8 %9 to i32
  %sub10 = sub i32 %conv9, 16
  %xor11 = xor i32 %sub8, %sub10
  %shr12 = lshr i32 %xor11, 8
  %conv13 = trunc i32 %shr12 to i8
  store i8 %conv13, i8* %c_alpha0, align 1
  %10 = load i8, i8* %c_num0, align 1
  %conv14 = zext i8 %10 to i32
  %11 = load i8, i8* %c_alpha0, align 1
  %conv15 = zext i8 %11 to i32
  %or = or i32 %conv14, %conv15
  %cmp16 = icmp eq i32 %or, 0
  br i1 %cmp16, label %if.then, label %if.end28

if.then:                                          ; preds = %while.body
  %12 = load i8*, i8** %ignore.addr, align 4
  %cmp18 = icmp ne i8* %12, null
  br i1 %cmp18, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %13 = load i8, i8* %state, align 1
  %conv20 = zext i8 %13 to i32
  %cmp21 = icmp eq i32 %conv20, 0
  br i1 %cmp21, label %land.lhs.true23, label %if.end

land.lhs.true23:                                  ; preds = %land.lhs.true
  %14 = load i8*, i8** %ignore.addr, align 4
  %15 = load i8, i8* %c, align 1
  %conv24 = zext i8 %15 to i32
  %call = call i8* @strchr(i8* %14, i32 %conv24)
  %cmp25 = icmp ne i8* %call, null
  br i1 %cmp25, label %if.then27, label %if.end

if.then27:                                        ; preds = %land.lhs.true23
  %16 = load i32, i32* %hex_pos, align 4
  %inc = add i32 %16, 1
  store i32 %inc, i32* %hex_pos, align 4
  br label %while.cond

if.end:                                           ; preds = %land.lhs.true23, %land.lhs.true, %if.then
  br label %while.end

if.end28:                                         ; preds = %while.body
  %17 = load i8, i8* %c_num0, align 1
  %conv29 = zext i8 %17 to i32
  %18 = load i8, i8* %c_num, align 1
  %conv30 = zext i8 %18 to i32
  %and31 = and i32 %conv29, %conv30
  %19 = load i8, i8* %c_alpha0, align 1
  %conv32 = zext i8 %19 to i32
  %20 = load i8, i8* %c_alpha, align 1
  %conv33 = zext i8 %20 to i32
  %and34 = and i32 %conv32, %conv33
  %or35 = or i32 %and31, %and34
  %conv36 = trunc i32 %or35 to i8
  store i8 %conv36, i8* %c_val, align 1
  %21 = load i32, i32* %bin_pos, align 4
  %22 = load i32, i32* %bin_maxlen.addr, align 4
  %cmp37 = icmp uge i32 %21, %22
  br i1 %cmp37, label %if.then39, label %if.end41

if.then39:                                        ; preds = %if.end28
  store i32 -1, i32* %ret, align 4
  %call40 = call i32* @__errno_location()
  store i32 68, i32* %call40, align 4
  br label %while.end

if.end41:                                         ; preds = %if.end28
  %23 = load i8, i8* %state, align 1
  %conv42 = zext i8 %23 to i32
  %cmp43 = icmp eq i32 %conv42, 0
  br i1 %cmp43, label %if.then45, label %if.else

if.then45:                                        ; preds = %if.end41
  %24 = load i8, i8* %c_val, align 1
  %conv46 = zext i8 %24 to i32
  %mul = mul i32 %conv46, 16
  %conv47 = trunc i32 %mul to i8
  store i8 %conv47, i8* %c_acc, align 1
  br label %if.end54

if.else:                                          ; preds = %if.end41
  %25 = load i8, i8* %c_acc, align 1
  %conv48 = zext i8 %25 to i32
  %26 = load i8, i8* %c_val, align 1
  %conv49 = zext i8 %26 to i32
  %or50 = or i32 %conv48, %conv49
  %conv51 = trunc i32 %or50 to i8
  %27 = load i8*, i8** %bin.addr, align 4
  %28 = load i32, i32* %bin_pos, align 4
  %inc52 = add i32 %28, 1
  store i32 %inc52, i32* %bin_pos, align 4
  %arrayidx53 = getelementptr i8, i8* %27, i32 %28
  store i8 %conv51, i8* %arrayidx53, align 1
  br label %if.end54

if.end54:                                         ; preds = %if.else, %if.then45
  %29 = load i8, i8* %state, align 1
  %conv55 = zext i8 %29 to i32
  %neg = xor i32 %conv55, -1
  %conv56 = trunc i32 %neg to i8
  store i8 %conv56, i8* %state, align 1
  %30 = load i32, i32* %hex_pos, align 4
  %inc57 = add i32 %30, 1
  store i32 %inc57, i32* %hex_pos, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then39, %if.end, %while.cond
  %31 = load i8, i8* %state, align 1
  %conv58 = zext i8 %31 to i32
  %cmp59 = icmp ne i32 %conv58, 0
  br i1 %cmp59, label %if.then61, label %if.end63

if.then61:                                        ; preds = %while.end
  %32 = load i32, i32* %hex_pos, align 4
  %dec = add i32 %32, -1
  store i32 %dec, i32* %hex_pos, align 4
  %call62 = call i32* @__errno_location()
  store i32 28, i32* %call62, align 4
  store i32 -1, i32* %ret, align 4
  br label %if.end63

if.end63:                                         ; preds = %if.then61, %while.end
  %33 = load i32, i32* %ret, align 4
  %cmp64 = icmp ne i32 %33, 0
  br i1 %cmp64, label %if.then66, label %if.end67

if.then66:                                        ; preds = %if.end63
  store i32 0, i32* %bin_pos, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then66, %if.end63
  %34 = load i8**, i8*** %hex_end.addr, align 4
  %cmp68 = icmp ne i8** %34, null
  br i1 %cmp68, label %if.then70, label %if.else72

if.then70:                                        ; preds = %if.end67
  %35 = load i8*, i8** %hex.addr, align 4
  %36 = load i32, i32* %hex_pos, align 4
  %arrayidx71 = getelementptr i8, i8* %35, i32 %36
  %37 = load i8**, i8*** %hex_end.addr, align 4
  store i8* %arrayidx71, i8** %37, align 4
  br label %if.end78

if.else72:                                        ; preds = %if.end67
  %38 = load i32, i32* %hex_pos, align 4
  %39 = load i32, i32* %hex_len.addr, align 4
  %cmp73 = icmp ne i32 %38, %39
  br i1 %cmp73, label %if.then75, label %if.end77

if.then75:                                        ; preds = %if.else72
  %call76 = call i32* @__errno_location()
  store i32 28, i32* %call76, align 4
  store i32 -1, i32* %ret, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %if.else72
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.then70
  %40 = load i32*, i32** %bin_len.addr, align 4
  %cmp79 = icmp ne i32* %40, null
  br i1 %cmp79, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.end78
  %41 = load i32, i32* %bin_pos, align 4
  %42 = load i32*, i32** %bin_len.addr, align 4
  store i32 %41, i32* %42, align 4
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %if.end78
  %43 = load i32, i32* %ret, align 4
  ret i32 %43
}

declare i8* @strchr(i8*, i32) #2

declare i32* @__errno_location() #2

; Function Attrs: noinline nounwind optnone
define i32 @sodium_base64_encoded_len(i32 %bin_len, i32 %variant) #0 {
entry:
  %bin_len.addr = alloca i32, align 4
  %variant.addr = alloca i32, align 4
  store i32 %bin_len, i32* %bin_len.addr, align 4
  store i32 %variant, i32* %variant.addr, align 4
  %0 = load i32, i32* %variant.addr, align 4
  call void @sodium_base64_check_variant(i32 %0)
  %1 = load i32, i32* %bin_len.addr, align 4
  %div = udiv i32 %1, 3
  %mul = mul i32 %div, 4
  %2 = load i32, i32* %bin_len.addr, align 4
  %3 = load i32, i32* %bin_len.addr, align 4
  %div1 = udiv i32 %3, 3
  %mul2 = mul i32 %div1, 3
  %sub = sub i32 %2, %mul2
  %4 = load i32, i32* %bin_len.addr, align 4
  %5 = load i32, i32* %bin_len.addr, align 4
  %div3 = udiv i32 %5, 3
  %mul4 = mul i32 %div3, 3
  %sub5 = sub i32 %4, %mul4
  %shr = lshr i32 %sub5, 1
  %or = or i32 %sub, %shr
  %and = and i32 %or, 1
  %6 = load i32, i32* %variant.addr, align 4
  %and6 = and i32 %6, 2
  %shr7 = lshr i32 %and6, 1
  %sub8 = sub i32 %shr7, 1
  %neg = xor i32 %sub8, -1
  %7 = load i32, i32* %bin_len.addr, align 4
  %8 = load i32, i32* %bin_len.addr, align 4
  %div9 = udiv i32 %8, 3
  %mul10 = mul i32 %div9, 3
  %sub11 = sub i32 %7, %mul10
  %sub12 = sub i32 3, %sub11
  %and13 = and i32 %neg, %sub12
  %sub14 = sub i32 4, %and13
  %mul15 = mul i32 %and, %sub14
  %add = add i32 %mul, %mul15
  %add16 = add i32 %add, 1
  ret i32 %add16
}

; Function Attrs: noinline nounwind optnone
define internal void @sodium_base64_check_variant(i32 %variant) #0 {
entry:
  %variant.addr = alloca i32, align 4
  store i32 %variant, i32* %variant.addr, align 4
  %0 = load i32, i32* %variant.addr, align 4
  %and = and i32 %0, -7
  %cmp = icmp ne i32 %and, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

if.end:                                           ; preds = %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define i8* @sodium_bin2base64(i8* nonnull %b64, i32 %b64_maxlen, i8* %bin, i32 %bin_len, i32 %variant) #0 {
entry:
  %b64.addr = alloca i8*, align 4
  %b64_maxlen.addr = alloca i32, align 4
  %bin.addr = alloca i8*, align 4
  %bin_len.addr = alloca i32, align 4
  %variant.addr = alloca i32, align 4
  %acc_len = alloca i32, align 4
  %b64_len = alloca i32, align 4
  %b64_pos = alloca i32, align 4
  %bin_pos = alloca i32, align 4
  %nibbles = alloca i32, align 4
  %remainder = alloca i32, align 4
  %acc = alloca i32, align 4
  store i8* %b64, i8** %b64.addr, align 4
  store i32 %b64_maxlen, i32* %b64_maxlen.addr, align 4
  store i8* %bin, i8** %bin.addr, align 4
  store i32 %bin_len, i32* %bin_len.addr, align 4
  store i32 %variant, i32* %variant.addr, align 4
  store i32 0, i32* %acc_len, align 4
  store i32 0, i32* %b64_pos, align 4
  store i32 0, i32* %bin_pos, align 4
  store i32 0, i32* %acc, align 4
  %0 = load i32, i32* %variant.addr, align 4
  call void @sodium_base64_check_variant(i32 %0)
  %1 = load i32, i32* %bin_len.addr, align 4
  %div = udiv i32 %1, 3
  store i32 %div, i32* %nibbles, align 4
  %2 = load i32, i32* %bin_len.addr, align 4
  %3 = load i32, i32* %nibbles, align 4
  %mul = mul i32 3, %3
  %sub = sub i32 %2, %mul
  store i32 %sub, i32* %remainder, align 4
  %4 = load i32, i32* %nibbles, align 4
  %mul1 = mul i32 %4, 4
  store i32 %mul1, i32* %b64_len, align 4
  %5 = load i32, i32* %remainder, align 4
  %cmp = icmp ne i32 %5, 0
  br i1 %cmp, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %variant.addr, align 4
  %and = and i32 %6, 2
  %cmp2 = icmp eq i32 %and, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %7 = load i32, i32* %b64_len, align 4
  %add = add i32 %7, 4
  store i32 %add, i32* %b64_len, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %8 = load i32, i32* %remainder, align 4
  %shr = lshr i32 %8, 1
  %add4 = add i32 2, %shr
  %9 = load i32, i32* %b64_len, align 4
  %add5 = add i32 %9, %add4
  store i32 %add5, i32* %b64_len, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  %10 = load i32, i32* %b64_maxlen.addr, align 4
  %11 = load i32, i32* %b64_len, align 4
  %cmp7 = icmp ule i32 %10, %11
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  call void @sodium_misuse() #3
  unreachable

if.end9:                                          ; preds = %if.end6
  %12 = load i32, i32* %variant.addr, align 4
  %and10 = and i32 %12, 4
  %cmp11 = icmp ne i32 %and10, 0
  br i1 %cmp11, label %if.then12, label %if.else38

if.then12:                                        ; preds = %if.end9
  br label %while.cond

while.cond:                                       ; preds = %while.end, %if.then12
  %13 = load i32, i32* %bin_pos, align 4
  %14 = load i32, i32* %bin_len.addr, align 4
  %cmp13 = icmp ult i32 %13, %14
  br i1 %cmp13, label %while.body, label %while.end26

while.body:                                       ; preds = %while.cond
  %15 = load i32, i32* %acc, align 4
  %shl = shl i32 %15, 8
  %16 = load i8*, i8** %bin.addr, align 4
  %17 = load i32, i32* %bin_pos, align 4
  %inc = add i32 %17, 1
  store i32 %inc, i32* %bin_pos, align 4
  %arrayidx = getelementptr i8, i8* %16, i32 %17
  %18 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %18 to i32
  %add14 = add i32 %shl, %conv
  store i32 %add14, i32* %acc, align 4
  %19 = load i32, i32* %acc_len, align 4
  %add15 = add i32 %19, 8
  store i32 %add15, i32* %acc_len, align 4
  br label %while.cond16

while.cond16:                                     ; preds = %while.body19, %while.body
  %20 = load i32, i32* %acc_len, align 4
  %cmp17 = icmp uge i32 %20, 6
  br i1 %cmp17, label %while.body19, label %while.end

while.body19:                                     ; preds = %while.cond16
  %21 = load i32, i32* %acc_len, align 4
  %sub20 = sub i32 %21, 6
  store i32 %sub20, i32* %acc_len, align 4
  %22 = load i32, i32* %acc, align 4
  %23 = load i32, i32* %acc_len, align 4
  %shr21 = lshr i32 %22, %23
  %and22 = and i32 %shr21, 63
  %call = call i32 @b64_byte_to_urlsafe_char(i32 %and22)
  %conv23 = trunc i32 %call to i8
  %24 = load i8*, i8** %b64.addr, align 4
  %25 = load i32, i32* %b64_pos, align 4
  %inc24 = add i32 %25, 1
  store i32 %inc24, i32* %b64_pos, align 4
  %arrayidx25 = getelementptr i8, i8* %24, i32 %25
  store i8 %conv23, i8* %arrayidx25, align 1
  br label %while.cond16

while.end:                                        ; preds = %while.cond16
  br label %while.cond

while.end26:                                      ; preds = %while.cond
  %26 = load i32, i32* %acc_len, align 4
  %cmp27 = icmp ugt i32 %26, 0
  br i1 %cmp27, label %if.then29, label %if.end37

if.then29:                                        ; preds = %while.end26
  %27 = load i32, i32* %acc, align 4
  %28 = load i32, i32* %acc_len, align 4
  %sub30 = sub i32 6, %28
  %shl31 = shl i32 %27, %sub30
  %and32 = and i32 %shl31, 63
  %call33 = call i32 @b64_byte_to_urlsafe_char(i32 %and32)
  %conv34 = trunc i32 %call33 to i8
  %29 = load i8*, i8** %b64.addr, align 4
  %30 = load i32, i32* %b64_pos, align 4
  %inc35 = add i32 %30, 1
  store i32 %inc35, i32* %b64_pos, align 4
  %arrayidx36 = getelementptr i8, i8* %29, i32 %30
  store i8 %conv34, i8* %arrayidx36, align 1
  br label %if.end37

if.end37:                                         ; preds = %if.then29, %while.end26
  br label %if.end73

if.else38:                                        ; preds = %if.end9
  br label %while.cond39

while.cond39:                                     ; preds = %while.end60, %if.else38
  %31 = load i32, i32* %bin_pos, align 4
  %32 = load i32, i32* %bin_len.addr, align 4
  %cmp40 = icmp ult i32 %31, %32
  br i1 %cmp40, label %while.body42, label %while.end61

while.body42:                                     ; preds = %while.cond39
  %33 = load i32, i32* %acc, align 4
  %shl43 = shl i32 %33, 8
  %34 = load i8*, i8** %bin.addr, align 4
  %35 = load i32, i32* %bin_pos, align 4
  %inc44 = add i32 %35, 1
  store i32 %inc44, i32* %bin_pos, align 4
  %arrayidx45 = getelementptr i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx45, align 1
  %conv46 = zext i8 %36 to i32
  %add47 = add i32 %shl43, %conv46
  store i32 %add47, i32* %acc, align 4
  %37 = load i32, i32* %acc_len, align 4
  %add48 = add i32 %37, 8
  store i32 %add48, i32* %acc_len, align 4
  br label %while.cond49

while.cond49:                                     ; preds = %while.body52, %while.body42
  %38 = load i32, i32* %acc_len, align 4
  %cmp50 = icmp uge i32 %38, 6
  br i1 %cmp50, label %while.body52, label %while.end60

while.body52:                                     ; preds = %while.cond49
  %39 = load i32, i32* %acc_len, align 4
  %sub53 = sub i32 %39, 6
  store i32 %sub53, i32* %acc_len, align 4
  %40 = load i32, i32* %acc, align 4
  %41 = load i32, i32* %acc_len, align 4
  %shr54 = lshr i32 %40, %41
  %and55 = and i32 %shr54, 63
  %call56 = call i32 @b64_byte_to_char(i32 %and55)
  %conv57 = trunc i32 %call56 to i8
  %42 = load i8*, i8** %b64.addr, align 4
  %43 = load i32, i32* %b64_pos, align 4
  %inc58 = add i32 %43, 1
  store i32 %inc58, i32* %b64_pos, align 4
  %arrayidx59 = getelementptr i8, i8* %42, i32 %43
  store i8 %conv57, i8* %arrayidx59, align 1
  br label %while.cond49

while.end60:                                      ; preds = %while.cond49
  br label %while.cond39

while.end61:                                      ; preds = %while.cond39
  %44 = load i32, i32* %acc_len, align 4
  %cmp62 = icmp ugt i32 %44, 0
  br i1 %cmp62, label %if.then64, label %if.end72

if.then64:                                        ; preds = %while.end61
  %45 = load i32, i32* %acc, align 4
  %46 = load i32, i32* %acc_len, align 4
  %sub65 = sub i32 6, %46
  %shl66 = shl i32 %45, %sub65
  %and67 = and i32 %shl66, 63
  %call68 = call i32 @b64_byte_to_char(i32 %and67)
  %conv69 = trunc i32 %call68 to i8
  %47 = load i8*, i8** %b64.addr, align 4
  %48 = load i32, i32* %b64_pos, align 4
  %inc70 = add i32 %48, 1
  store i32 %inc70, i32* %b64_pos, align 4
  %arrayidx71 = getelementptr i8, i8* %47, i32 %48
  store i8 %conv69, i8* %arrayidx71, align 1
  br label %if.end72

if.end72:                                         ; preds = %if.then64, %while.end61
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %if.end37
  %49 = load i32, i32* %b64_pos, align 4
  %50 = load i32, i32* %b64_len, align 4
  %cmp74 = icmp ule i32 %49, %50
  br i1 %cmp74, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end73
  call void @__assert_fail(i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0), i32 230, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @__func__.sodium_bin2base64, i32 0, i32 0)) #3
  unreachable

51:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %51, %if.end73
  %52 = phi i1 [ true, %if.end73 ], [ false, %51 ]
  %lor.ext = zext i1 %52 to i32
  br label %while.cond76

while.cond76:                                     ; preds = %while.body79, %lor.end
  %53 = load i32, i32* %b64_pos, align 4
  %54 = load i32, i32* %b64_len, align 4
  %cmp77 = icmp ult i32 %53, %54
  br i1 %cmp77, label %while.body79, label %while.end82

while.body79:                                     ; preds = %while.cond76
  %55 = load i8*, i8** %b64.addr, align 4
  %56 = load i32, i32* %b64_pos, align 4
  %inc80 = add i32 %56, 1
  store i32 %inc80, i32* %b64_pos, align 4
  %arrayidx81 = getelementptr i8, i8* %55, i32 %56
  store i8 61, i8* %arrayidx81, align 1
  br label %while.cond76

while.end82:                                      ; preds = %while.cond76
  br label %do.body

do.body:                                          ; preds = %do.cond, %while.end82
  %57 = load i8*, i8** %b64.addr, align 4
  %58 = load i32, i32* %b64_pos, align 4
  %inc83 = add i32 %58, 1
  store i32 %inc83, i32* %b64_pos, align 4
  %arrayidx84 = getelementptr i8, i8* %57, i32 %58
  store i8 0, i8* %arrayidx84, align 1
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %59 = load i32, i32* %b64_pos, align 4
  %60 = load i32, i32* %b64_maxlen.addr, align 4
  %cmp85 = icmp ult i32 %59, %60
  br i1 %cmp85, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %61 = load i8*, i8** %b64.addr, align 4
  ret i8* %61
}

; Function Attrs: noinline nounwind optnone
define internal i32 @b64_byte_to_urlsafe_char(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %sub = sub i32 %0, 26
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %1 = load i32, i32* %x.addr, align 4
  %add = add i32 %1, 65
  %and1 = and i32 %and, %add
  %2 = load i32, i32* %x.addr, align 4
  %sub2 = sub i32 %2, 26
  %shr3 = lshr i32 %sub2, 8
  %and4 = and i32 %shr3, 255
  %xor = xor i32 %and4, 255
  %3 = load i32, i32* %x.addr, align 4
  %sub5 = sub i32 %3, 52
  %shr6 = lshr i32 %sub5, 8
  %and7 = and i32 %shr6, 255
  %and8 = and i32 %xor, %and7
  %4 = load i32, i32* %x.addr, align 4
  %add9 = add i32 %4, 71
  %and10 = and i32 %and8, %add9
  %or = or i32 %and1, %and10
  %5 = load i32, i32* %x.addr, align 4
  %sub11 = sub i32 %5, 52
  %shr12 = lshr i32 %sub11, 8
  %and13 = and i32 %shr12, 255
  %xor14 = xor i32 %and13, 255
  %6 = load i32, i32* %x.addr, align 4
  %sub15 = sub i32 %6, 62
  %shr16 = lshr i32 %sub15, 8
  %and17 = and i32 %shr16, 255
  %and18 = and i32 %xor14, %and17
  %7 = load i32, i32* %x.addr, align 4
  %add19 = add i32 %7, -4
  %and20 = and i32 %and18, %add19
  %or21 = or i32 %or, %and20
  %8 = load i32, i32* %x.addr, align 4
  %xor22 = xor i32 %8, 62
  %sub23 = sub i32 0, %xor22
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor26, 45
  %or28 = or i32 %or21, %and27
  %9 = load i32, i32* %x.addr, align 4
  %xor29 = xor i32 %9, 63
  %sub30 = sub i32 0, %xor29
  %shr31 = lshr i32 %sub30, 8
  %and32 = and i32 %shr31, 255
  %xor33 = xor i32 %and32, 255
  %and34 = and i32 %xor33, 95
  %or35 = or i32 %or28, %and34
  ret i32 %or35
}

; Function Attrs: noinline nounwind optnone
define internal i32 @b64_byte_to_char(i32 %x) #0 {
entry:
  %x.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %sub = sub i32 %0, 26
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %1 = load i32, i32* %x.addr, align 4
  %add = add i32 %1, 65
  %and1 = and i32 %and, %add
  %2 = load i32, i32* %x.addr, align 4
  %sub2 = sub i32 %2, 26
  %shr3 = lshr i32 %sub2, 8
  %and4 = and i32 %shr3, 255
  %xor = xor i32 %and4, 255
  %3 = load i32, i32* %x.addr, align 4
  %sub5 = sub i32 %3, 52
  %shr6 = lshr i32 %sub5, 8
  %and7 = and i32 %shr6, 255
  %and8 = and i32 %xor, %and7
  %4 = load i32, i32* %x.addr, align 4
  %add9 = add i32 %4, 71
  %and10 = and i32 %and8, %add9
  %or = or i32 %and1, %and10
  %5 = load i32, i32* %x.addr, align 4
  %sub11 = sub i32 %5, 52
  %shr12 = lshr i32 %sub11, 8
  %and13 = and i32 %shr12, 255
  %xor14 = xor i32 %and13, 255
  %6 = load i32, i32* %x.addr, align 4
  %sub15 = sub i32 %6, 62
  %shr16 = lshr i32 %sub15, 8
  %and17 = and i32 %shr16, 255
  %and18 = and i32 %xor14, %and17
  %7 = load i32, i32* %x.addr, align 4
  %add19 = add i32 %7, -4
  %and20 = and i32 %and18, %add19
  %or21 = or i32 %or, %and20
  %8 = load i32, i32* %x.addr, align 4
  %xor22 = xor i32 %8, 62
  %sub23 = sub i32 0, %xor22
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor26, 43
  %or28 = or i32 %or21, %and27
  %9 = load i32, i32* %x.addr, align 4
  %xor29 = xor i32 %9, 63
  %sub30 = sub i32 0, %xor29
  %shr31 = lshr i32 %sub30, 8
  %and32 = and i32 %shr31, 255
  %xor33 = xor i32 %and32, 255
  %and34 = and i32 %xor33, 47
  %or35 = or i32 %or28, %and34
  ret i32 %or35
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @sodium_base642bin(i8* nonnull %bin, i32 %bin_maxlen, i8* %b64, i32 %b64_len, i8* %ignore, i32* %bin_len, i8** %b64_end, i32 %variant) #0 {
entry:
  %bin.addr = alloca i8*, align 4
  %bin_maxlen.addr = alloca i32, align 4
  %b64.addr = alloca i8*, align 4
  %b64_len.addr = alloca i32, align 4
  %ignore.addr = alloca i8*, align 4
  %bin_len.addr = alloca i32*, align 4
  %b64_end.addr = alloca i8**, align 4
  %variant.addr = alloca i32, align 4
  %acc_len = alloca i32, align 4
  %b64_pos = alloca i32, align 4
  %bin_pos = alloca i32, align 4
  %is_urlsafe = alloca i32, align 4
  %ret = alloca i32, align 4
  %acc = alloca i32, align 4
  %d = alloca i32, align 4
  %c = alloca i8, align 1
  store i8* %bin, i8** %bin.addr, align 4
  store i32 %bin_maxlen, i32* %bin_maxlen.addr, align 4
  store i8* %b64, i8** %b64.addr, align 4
  store i32 %b64_len, i32* %b64_len.addr, align 4
  store i8* %ignore, i8** %ignore.addr, align 4
  store i32* %bin_len, i32** %bin_len.addr, align 4
  store i8** %b64_end, i8*** %b64_end.addr, align 4
  store i32 %variant, i32* %variant.addr, align 4
  store i32 0, i32* %acc_len, align 4
  store i32 0, i32* %b64_pos, align 4
  store i32 0, i32* %bin_pos, align 4
  store i32 0, i32* %ret, align 4
  store i32 0, i32* %acc, align 4
  %0 = load i32, i32* %variant.addr, align 4
  call void @sodium_base64_check_variant(i32 %0)
  %1 = load i32, i32* %variant.addr, align 4
  %and = and i32 %1, 4
  store i32 %and, i32* %is_urlsafe, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end28, %if.then12, %entry
  %2 = load i32, i32* %b64_pos, align 4
  %3 = load i32, i32* %b64_len.addr, align 4
  %cmp = icmp ult i32 %2, %3
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i8*, i8** %b64.addr, align 4
  %5 = load i32, i32* %b64_pos, align 4
  %arrayidx = getelementptr i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1
  store i8 %6, i8* %c, align 1
  %7 = load i32, i32* %is_urlsafe, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %8 = load i8, i8* %c, align 1
  %conv = sext i8 %8 to i32
  %call = call i32 @b64_urlsafe_char_to_byte(i32 %conv)
  store i32 %call, i32* %d, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %9 = load i8, i8* %c, align 1
  %conv1 = sext i8 %9 to i32
  %call2 = call i32 @b64_char_to_byte(i32 %conv1)
  store i32 %call2, i32* %d, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %10 = load i32, i32* %d, align 4
  %cmp3 = icmp eq i32 %10, 255
  br i1 %cmp3, label %if.then5, label %if.end14

if.then5:                                         ; preds = %if.end
  %11 = load i8*, i8** %ignore.addr, align 4
  %cmp6 = icmp ne i8* %11, null
  br i1 %cmp6, label %land.lhs.true, label %if.end13

land.lhs.true:                                    ; preds = %if.then5
  %12 = load i8*, i8** %ignore.addr, align 4
  %13 = load i8, i8* %c, align 1
  %conv8 = sext i8 %13 to i32
  %call9 = call i8* @strchr(i8* %12, i32 %conv8)
  %cmp10 = icmp ne i8* %call9, null
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %land.lhs.true
  %14 = load i32, i32* %b64_pos, align 4
  %inc = add i32 %14, 1
  store i32 %inc, i32* %b64_pos, align 4
  br label %while.cond

if.end13:                                         ; preds = %land.lhs.true, %if.then5
  br label %while.end

if.end14:                                         ; preds = %if.end
  %15 = load i32, i32* %acc, align 4
  %shl = shl i32 %15, 6
  %16 = load i32, i32* %d, align 4
  %add = add i32 %shl, %16
  store i32 %add, i32* %acc, align 4
  %17 = load i32, i32* %acc_len, align 4
  %add15 = add i32 %17, 6
  store i32 %add15, i32* %acc_len, align 4
  %18 = load i32, i32* %acc_len, align 4
  %cmp16 = icmp uge i32 %18, 8
  br i1 %cmp16, label %if.then18, label %if.end28

if.then18:                                        ; preds = %if.end14
  %19 = load i32, i32* %acc_len, align 4
  %sub = sub i32 %19, 8
  store i32 %sub, i32* %acc_len, align 4
  %20 = load i32, i32* %bin_pos, align 4
  %21 = load i32, i32* %bin_maxlen.addr, align 4
  %cmp19 = icmp uge i32 %20, %21
  br i1 %cmp19, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.then18
  %call22 = call i32* @__errno_location()
  store i32 68, i32* %call22, align 4
  store i32 -1, i32* %ret, align 4
  br label %while.end

if.end23:                                         ; preds = %if.then18
  %22 = load i32, i32* %acc, align 4
  %23 = load i32, i32* %acc_len, align 4
  %shr = lshr i32 %22, %23
  %and24 = and i32 %shr, 255
  %conv25 = trunc i32 %and24 to i8
  %24 = load i8*, i8** %bin.addr, align 4
  %25 = load i32, i32* %bin_pos, align 4
  %inc26 = add i32 %25, 1
  store i32 %inc26, i32* %bin_pos, align 4
  %arrayidx27 = getelementptr i8, i8* %24, i32 %25
  store i8 %conv25, i8* %arrayidx27, align 1
  br label %if.end28

if.end28:                                         ; preds = %if.end23, %if.end14
  %26 = load i32, i32* %b64_pos, align 4
  %inc29 = add i32 %26, 1
  store i32 %inc29, i32* %b64_pos, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then21, %if.end13, %while.cond
  %27 = load i32, i32* %acc_len, align 4
  %cmp30 = icmp ugt i32 %27, 4
  br i1 %cmp30, label %if.then37, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.end
  %28 = load i32, i32* %acc, align 4
  %29 = load i32, i32* %acc_len, align 4
  %shl32 = shl i32 1, %29
  %sub33 = sub i32 %shl32, 1
  %and34 = and i32 %28, %sub33
  %cmp35 = icmp ne i32 %and34, 0
  br i1 %cmp35, label %if.then37, label %if.else38

if.then37:                                        ; preds = %lor.lhs.false, %while.end
  store i32 -1, i32* %ret, align 4
  br label %if.end48

if.else38:                                        ; preds = %lor.lhs.false
  %30 = load i32, i32* %ret, align 4
  %cmp39 = icmp eq i32 %30, 0
  br i1 %cmp39, label %land.lhs.true41, label %if.end47

land.lhs.true41:                                  ; preds = %if.else38
  %31 = load i32, i32* %variant.addr, align 4
  %and42 = and i32 %31, 2
  %cmp43 = icmp eq i32 %and42, 0
  br i1 %cmp43, label %if.then45, label %if.end47

if.then45:                                        ; preds = %land.lhs.true41
  %32 = load i8*, i8** %b64.addr, align 4
  %33 = load i32, i32* %b64_len.addr, align 4
  %34 = load i8*, i8** %ignore.addr, align 4
  %35 = load i32, i32* %acc_len, align 4
  %div = udiv i32 %35, 2
  %call46 = call i32 @_sodium_base642bin_skip_padding(i8* %32, i32 %33, i32* %b64_pos, i8* %34, i32 %div)
  store i32 %call46, i32* %ret, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %land.lhs.true41, %if.else38
  br label %if.end48

if.end48:                                         ; preds = %if.end47, %if.then37
  %36 = load i32, i32* %ret, align 4
  %cmp49 = icmp ne i32 %36, 0
  br i1 %cmp49, label %if.then51, label %if.else52

if.then51:                                        ; preds = %if.end48
  store i32 0, i32* %bin_pos, align 4
  br label %if.end68

if.else52:                                        ; preds = %if.end48
  %37 = load i8*, i8** %ignore.addr, align 4
  %cmp53 = icmp ne i8* %37, null
  br i1 %cmp53, label %if.then55, label %if.end67

if.then55:                                        ; preds = %if.else52
  br label %while.cond56

while.cond56:                                     ; preds = %while.body64, %if.then55
  %38 = load i32, i32* %b64_pos, align 4
  %39 = load i32, i32* %b64_len.addr, align 4
  %cmp57 = icmp ult i32 %38, %39
  br i1 %cmp57, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond56
  %40 = load i8*, i8** %ignore.addr, align 4
  %41 = load i8*, i8** %b64.addr, align 4
  %42 = load i32, i32* %b64_pos, align 4
  %arrayidx59 = getelementptr i8, i8* %41, i32 %42
  %43 = load i8, i8* %arrayidx59, align 1
  %conv60 = sext i8 %43 to i32
  %call61 = call i8* @strchr(i8* %40, i32 %conv60)
  %cmp62 = icmp ne i8* %call61, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond56
  %44 = phi i1 [ false, %while.cond56 ], [ %cmp62, %land.rhs ]
  br i1 %44, label %while.body64, label %while.end66

while.body64:                                     ; preds = %land.end
  %45 = load i32, i32* %b64_pos, align 4
  %inc65 = add i32 %45, 1
  store i32 %inc65, i32* %b64_pos, align 4
  br label %while.cond56

while.end66:                                      ; preds = %land.end
  br label %if.end67

if.end67:                                         ; preds = %while.end66, %if.else52
  br label %if.end68

if.end68:                                         ; preds = %if.end67, %if.then51
  %46 = load i8**, i8*** %b64_end.addr, align 4
  %cmp69 = icmp ne i8** %46, null
  br i1 %cmp69, label %if.then71, label %if.else73

if.then71:                                        ; preds = %if.end68
  %47 = load i8*, i8** %b64.addr, align 4
  %48 = load i32, i32* %b64_pos, align 4
  %arrayidx72 = getelementptr i8, i8* %47, i32 %48
  %49 = load i8**, i8*** %b64_end.addr, align 4
  store i8* %arrayidx72, i8** %49, align 4
  br label %if.end79

if.else73:                                        ; preds = %if.end68
  %50 = load i32, i32* %b64_pos, align 4
  %51 = load i32, i32* %b64_len.addr, align 4
  %cmp74 = icmp ne i32 %50, %51
  br i1 %cmp74, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.else73
  %call77 = call i32* @__errno_location()
  store i32 28, i32* %call77, align 4
  store i32 -1, i32* %ret, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.then76, %if.else73
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.then71
  %52 = load i32*, i32** %bin_len.addr, align 4
  %cmp80 = icmp ne i32* %52, null
  br i1 %cmp80, label %if.then82, label %if.end83

if.then82:                                        ; preds = %if.end79
  %53 = load i32, i32* %bin_pos, align 4
  %54 = load i32*, i32** %bin_len.addr, align 4
  store i32 %53, i32* %54, align 4
  br label %if.end83

if.end83:                                         ; preds = %if.then82, %if.end79
  %55 = load i32, i32* %ret, align 4
  ret i32 %55
}

; Function Attrs: noinline nounwind optnone
define internal i32 @b64_urlsafe_char_to_byte(i32 %c) #0 {
entry:
  %c.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store i32 %c, i32* %c.addr, align 4
  %0 = load i32, i32* %c.addr, align 4
  %sub = sub i32 %0, 65
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %xor = xor i32 %and, 255
  %1 = load i32, i32* %c.addr, align 4
  %sub1 = sub i32 90, %1
  %shr2 = lshr i32 %sub1, 8
  %and3 = and i32 %shr2, 255
  %xor4 = xor i32 %and3, 255
  %and5 = and i32 %xor, %xor4
  %2 = load i32, i32* %c.addr, align 4
  %sub6 = sub i32 %2, 65
  %and7 = and i32 %and5, %sub6
  %3 = load i32, i32* %c.addr, align 4
  %sub8 = sub i32 %3, 97
  %shr9 = lshr i32 %sub8, 8
  %and10 = and i32 %shr9, 255
  %xor11 = xor i32 %and10, 255
  %4 = load i32, i32* %c.addr, align 4
  %sub12 = sub i32 122, %4
  %shr13 = lshr i32 %sub12, 8
  %and14 = and i32 %shr13, 255
  %xor15 = xor i32 %and14, 255
  %and16 = and i32 %xor11, %xor15
  %5 = load i32, i32* %c.addr, align 4
  %sub17 = sub i32 %5, 71
  %and18 = and i32 %and16, %sub17
  %or = or i32 %and7, %and18
  %6 = load i32, i32* %c.addr, align 4
  %sub19 = sub i32 %6, 48
  %shr20 = lshr i32 %sub19, 8
  %and21 = and i32 %shr20, 255
  %xor22 = xor i32 %and21, 255
  %7 = load i32, i32* %c.addr, align 4
  %sub23 = sub i32 57, %7
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor22, %xor26
  %8 = load i32, i32* %c.addr, align 4
  %sub28 = sub i32 %8, -4
  %and29 = and i32 %and27, %sub28
  %or30 = or i32 %or, %and29
  %9 = load i32, i32* %c.addr, align 4
  %xor31 = xor i32 %9, 45
  %sub32 = sub i32 0, %xor31
  %shr33 = lshr i32 %sub32, 8
  %and34 = and i32 %shr33, 255
  %xor35 = xor i32 %and34, 255
  %and36 = and i32 %xor35, 62
  %or37 = or i32 %or30, %and36
  %10 = load i32, i32* %c.addr, align 4
  %xor38 = xor i32 %10, 95
  %sub39 = sub i32 0, %xor38
  %shr40 = lshr i32 %sub39, 8
  %and41 = and i32 %shr40, 255
  %xor42 = xor i32 %and41, 255
  %and43 = and i32 %xor42, 63
  %or44 = or i32 %or37, %and43
  store i32 %or44, i32* %x, align 4
  %11 = load i32, i32* %x, align 4
  %12 = load i32, i32* %x, align 4
  %xor45 = xor i32 %12, 0
  %sub46 = sub i32 0, %xor45
  %shr47 = lshr i32 %sub46, 8
  %and48 = and i32 %shr47, 255
  %xor49 = xor i32 %and48, 255
  %13 = load i32, i32* %c.addr, align 4
  %xor50 = xor i32 %13, 65
  %sub51 = sub i32 0, %xor50
  %shr52 = lshr i32 %sub51, 8
  %and53 = and i32 %shr52, 255
  %xor54 = xor i32 %and53, 255
  %xor55 = xor i32 %xor54, 255
  %and56 = and i32 %xor49, %xor55
  %or57 = or i32 %11, %and56
  ret i32 %or57
}

; Function Attrs: noinline nounwind optnone
define internal i32 @b64_char_to_byte(i32 %c) #0 {
entry:
  %c.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store i32 %c, i32* %c.addr, align 4
  %0 = load i32, i32* %c.addr, align 4
  %sub = sub i32 %0, 65
  %shr = lshr i32 %sub, 8
  %and = and i32 %shr, 255
  %xor = xor i32 %and, 255
  %1 = load i32, i32* %c.addr, align 4
  %sub1 = sub i32 90, %1
  %shr2 = lshr i32 %sub1, 8
  %and3 = and i32 %shr2, 255
  %xor4 = xor i32 %and3, 255
  %and5 = and i32 %xor, %xor4
  %2 = load i32, i32* %c.addr, align 4
  %sub6 = sub i32 %2, 65
  %and7 = and i32 %and5, %sub6
  %3 = load i32, i32* %c.addr, align 4
  %sub8 = sub i32 %3, 97
  %shr9 = lshr i32 %sub8, 8
  %and10 = and i32 %shr9, 255
  %xor11 = xor i32 %and10, 255
  %4 = load i32, i32* %c.addr, align 4
  %sub12 = sub i32 122, %4
  %shr13 = lshr i32 %sub12, 8
  %and14 = and i32 %shr13, 255
  %xor15 = xor i32 %and14, 255
  %and16 = and i32 %xor11, %xor15
  %5 = load i32, i32* %c.addr, align 4
  %sub17 = sub i32 %5, 71
  %and18 = and i32 %and16, %sub17
  %or = or i32 %and7, %and18
  %6 = load i32, i32* %c.addr, align 4
  %sub19 = sub i32 %6, 48
  %shr20 = lshr i32 %sub19, 8
  %and21 = and i32 %shr20, 255
  %xor22 = xor i32 %and21, 255
  %7 = load i32, i32* %c.addr, align 4
  %sub23 = sub i32 57, %7
  %shr24 = lshr i32 %sub23, 8
  %and25 = and i32 %shr24, 255
  %xor26 = xor i32 %and25, 255
  %and27 = and i32 %xor22, %xor26
  %8 = load i32, i32* %c.addr, align 4
  %sub28 = sub i32 %8, -4
  %and29 = and i32 %and27, %sub28
  %or30 = or i32 %or, %and29
  %9 = load i32, i32* %c.addr, align 4
  %xor31 = xor i32 %9, 43
  %sub32 = sub i32 0, %xor31
  %shr33 = lshr i32 %sub32, 8
  %and34 = and i32 %shr33, 255
  %xor35 = xor i32 %and34, 255
  %and36 = and i32 %xor35, 62
  %or37 = or i32 %or30, %and36
  %10 = load i32, i32* %c.addr, align 4
  %xor38 = xor i32 %10, 47
  %sub39 = sub i32 0, %xor38
  %shr40 = lshr i32 %sub39, 8
  %and41 = and i32 %shr40, 255
  %xor42 = xor i32 %and41, 255
  %and43 = and i32 %xor42, 63
  %or44 = or i32 %or37, %and43
  store i32 %or44, i32* %x, align 4
  %11 = load i32, i32* %x, align 4
  %12 = load i32, i32* %x, align 4
  %xor45 = xor i32 %12, 0
  %sub46 = sub i32 0, %xor45
  %shr47 = lshr i32 %sub46, 8
  %and48 = and i32 %shr47, 255
  %xor49 = xor i32 %and48, 255
  %13 = load i32, i32* %c.addr, align 4
  %xor50 = xor i32 %13, 65
  %sub51 = sub i32 0, %xor50
  %shr52 = lshr i32 %sub51, 8
  %and53 = and i32 %shr52, 255
  %xor54 = xor i32 %and53, 255
  %xor55 = xor i32 %xor54, 255
  %and56 = and i32 %xor49, %xor55
  %or57 = or i32 %11, %and56
  ret i32 %or57
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_sodium_base642bin_skip_padding(i8* %b64, i32 %b64_len, i32* %b64_pos_p, i8* %ignore, i32 %padding_len) #0 {
entry:
  %retval = alloca i32, align 4
  %b64.addr = alloca i8*, align 4
  %b64_len.addr = alloca i32, align 4
  %b64_pos_p.addr = alloca i32*, align 4
  %ignore.addr = alloca i8*, align 4
  %padding_len.addr = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %b64, i8** %b64.addr, align 4
  store i32 %b64_len, i32* %b64_len.addr, align 4
  store i32* %b64_pos_p, i32** %b64_pos_p.addr, align 4
  store i8* %ignore, i8** %ignore.addr, align 4
  store i32 %padding_len, i32* %padding_len.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end13, %entry
  %0 = load i32, i32* %padding_len.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32*, i32** %b64_pos_p.addr, align 4
  %2 = load i32, i32* %1, align 4
  %3 = load i32, i32* %b64_len.addr, align 4
  %cmp1 = icmp uge i32 %2, %3
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %call = call i32* @__errno_location()
  store i32 68, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %4 = load i8*, i8** %b64.addr, align 4
  %5 = load i32*, i32** %b64_pos_p.addr, align 4
  %6 = load i32, i32* %5, align 4
  %arrayidx = getelementptr i8, i8* %4, i32 %6
  %7 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %7 to i32
  store i32 %conv, i32* %c, align 4
  %8 = load i32, i32* %c, align 4
  %cmp2 = icmp eq i32 %8, 61
  br i1 %cmp2, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %9 = load i32, i32* %padding_len.addr, align 4
  %dec = add i32 %9, -1
  store i32 %dec, i32* %padding_len.addr, align 4
  br label %if.end13

if.else:                                          ; preds = %if.end
  %10 = load i8*, i8** %ignore.addr, align 4
  %cmp5 = icmp eq i8* %10, null
  br i1 %cmp5, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %11 = load i8*, i8** %ignore.addr, align 4
  %12 = load i32, i32* %c, align 4
  %call7 = call i8* @strchr(i8* %11, i32 %12)
  %cmp8 = icmp eq i8* %call7, null
  br i1 %cmp8, label %if.then10, label %if.end12

if.then10:                                        ; preds = %lor.lhs.false, %if.else
  %call11 = call i32* @__errno_location()
  store i32 28, i32* %call11, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %lor.lhs.false
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.then4
  %13 = load i32*, i32** %b64_pos_p.addr, align 4
  %14 = load i32, i32* %13, align 4
  %inc = add i32 %14, 1
  store i32 %inc, i32* %13, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then10, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
