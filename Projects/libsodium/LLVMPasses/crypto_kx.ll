; ModuleID = 'crypto_kx/crypto_kx.c'
source_filename = "crypto_kx/crypto_kx.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_generichash_blake2b_state = type { [384 x i8] }

@.str = private unnamed_addr constant [14 x i8] c"x25519blake2b\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_seed_keypair(i8* nonnull %pk, i8* nonnull %sk, i8* nonnull %seed) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %seed.addr = alloca i8*, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  store i8* %seed, i8** %seed.addr, align 4
  %0 = load i8*, i8** %sk.addr, align 4
  %1 = load i8*, i8** %seed.addr, align 4
  %call = call i32 @crypto_generichash(i8* %0, i32 32, i8* %1, i64 32, i8* null, i32 0)
  %2 = load i8*, i8** %pk.addr, align 4
  %3 = load i8*, i8** %sk.addr, align 4
  %call1 = call i32 @crypto_scalarmult_base(i8* %2, i8* %3)
  ret i32 %call1
}

declare i32 @crypto_generichash(i8*, i32, i8*, i64, i8*, i32) #1

declare i32 @crypto_scalarmult_base(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_keypair(i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sk.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  %1 = load i8*, i8** %pk.addr, align 4
  %2 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_scalarmult_base(i8* %1, i8* %2)
  ret i32 %call
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_client_session_keys(i8* %rx, i8* %tx, i8* nonnull %client_pk, i8* nonnull %client_sk, i8* nonnull %server_pk) #0 {
entry:
  %retval = alloca i32, align 4
  %rx.addr = alloca i8*, align 4
  %tx.addr = alloca i8*, align 4
  %client_pk.addr = alloca i8*, align 4
  %client_sk.addr = alloca i8*, align 4
  %server_pk.addr = alloca i8*, align 4
  %h = alloca %struct.crypto_generichash_blake2b_state, align 64
  %q = alloca [32 x i8], align 16
  %keys = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  store i8* %rx, i8** %rx.addr, align 4
  store i8* %tx, i8** %tx.addr, align 4
  store i8* %client_pk, i8** %client_pk.addr, align 4
  store i8* %client_sk, i8** %client_sk.addr, align 4
  store i8* %server_pk, i8** %server_pk.addr, align 4
  %0 = load i8*, i8** %rx.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %tx.addr, align 4
  store i8* %1, i8** %rx.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8*, i8** %tx.addr, align 4
  %cmp1 = icmp eq i8* %2, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load i8*, i8** %rx.addr, align 4
  store i8* %3, i8** %tx.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %4 = load i8*, i8** %rx.addr, align 4
  %cmp4 = icmp eq i8* %4, null
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  call void @sodium_misuse() #3
  unreachable

if.end6:                                          ; preds = %if.end3
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  %5 = load i8*, i8** %client_sk.addr, align 4
  %6 = load i8*, i8** %server_pk.addr, align 4
  %call = call i32 @crypto_scalarmult(i8* %arraydecay, i8* %5, i8* %6)
  %cmp7 = icmp ne i32 %call, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %call10 = call i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state* %h, i8* null, i32 0, i32 64)
  %arraydecay11 = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  %call12 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %arraydecay11, i64 32)
  %arraydecay13 = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay13, i32 32)
  %7 = load i8*, i8** %client_pk.addr, align 4
  %call14 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %7, i64 32)
  %8 = load i8*, i8** %server_pk.addr, align 4
  %call15 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %8, i64 32)
  %arraydecay16 = getelementptr inbounds [64 x i8], [64 x i8]* %keys, i32 0, i32 0
  %call17 = call i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state* %h, i8* %arraydecay16, i32 64)
  %9 = bitcast %struct.crypto_generichash_blake2b_state* %h to i8*
  call void @sodium_memzero(i8* %9, i32 384)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end9
  %10 = load i32, i32* %i, align 4
  %cmp18 = icmp slt i32 %10, 32
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [64 x i8], [64 x i8]* %keys, i32 0, i32 %11
  %12 = load i8, i8* %arrayidx, align 1
  %13 = load i8*, i8** %rx.addr, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr i8, i8* %13, i32 %14
  store i8 %12, i8* %arrayidx19, align 1
  %15 = load i32, i32* %i, align 4
  %add = add i32 %15, 32
  %arrayidx20 = getelementptr [64 x i8], [64 x i8]* %keys, i32 0, i32 %add
  %16 = load i8, i8* %arrayidx20, align 1
  %17 = load i8*, i8** %tx.addr, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr i8, i8* %17, i32 %18
  store i8 %16, i8* %arrayidx21, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay22 = getelementptr inbounds [64 x i8], [64 x i8]* %keys, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay22, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then8
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: noreturn
declare void @sodium_misuse() #2

declare i32 @crypto_scalarmult(i8*, i8*, i8*) #1

declare i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state*, i8*, i32, i32) #1

declare i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state*, i8*, i64) #1

declare void @sodium_memzero(i8*, i32) #1

declare i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_server_session_keys(i8* %rx, i8* %tx, i8* nonnull %server_pk, i8* nonnull %server_sk, i8* nonnull %client_pk) #0 {
entry:
  %retval = alloca i32, align 4
  %rx.addr = alloca i8*, align 4
  %tx.addr = alloca i8*, align 4
  %server_pk.addr = alloca i8*, align 4
  %server_sk.addr = alloca i8*, align 4
  %client_pk.addr = alloca i8*, align 4
  %h = alloca %struct.crypto_generichash_blake2b_state, align 64
  %q = alloca [32 x i8], align 16
  %keys = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  store i8* %rx, i8** %rx.addr, align 4
  store i8* %tx, i8** %tx.addr, align 4
  store i8* %server_pk, i8** %server_pk.addr, align 4
  store i8* %server_sk, i8** %server_sk.addr, align 4
  store i8* %client_pk, i8** %client_pk.addr, align 4
  %0 = load i8*, i8** %rx.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %tx.addr, align 4
  store i8* %1, i8** %rx.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8*, i8** %tx.addr, align 4
  %cmp1 = icmp eq i8* %2, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load i8*, i8** %rx.addr, align 4
  store i8* %3, i8** %tx.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %4 = load i8*, i8** %rx.addr, align 4
  %cmp4 = icmp eq i8* %4, null
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  call void @sodium_misuse() #3
  unreachable

if.end6:                                          ; preds = %if.end3
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  %5 = load i8*, i8** %server_sk.addr, align 4
  %6 = load i8*, i8** %client_pk.addr, align 4
  %call = call i32 @crypto_scalarmult(i8* %arraydecay, i8* %5, i8* %6)
  %cmp7 = icmp ne i32 %call, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %call10 = call i32 @crypto_generichash_init(%struct.crypto_generichash_blake2b_state* %h, i8* null, i32 0, i32 64)
  %arraydecay11 = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  %call12 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %arraydecay11, i64 32)
  %arraydecay13 = getelementptr inbounds [32 x i8], [32 x i8]* %q, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay13, i32 32)
  %7 = load i8*, i8** %client_pk.addr, align 4
  %call14 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %7, i64 32)
  %8 = load i8*, i8** %server_pk.addr, align 4
  %call15 = call i32 @crypto_generichash_update(%struct.crypto_generichash_blake2b_state* %h, i8* %8, i64 32)
  %arraydecay16 = getelementptr inbounds [64 x i8], [64 x i8]* %keys, i32 0, i32 0
  %call17 = call i32 @crypto_generichash_final(%struct.crypto_generichash_blake2b_state* %h, i8* %arraydecay16, i32 64)
  %9 = bitcast %struct.crypto_generichash_blake2b_state* %h to i8*
  call void @sodium_memzero(i8* %9, i32 384)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end9
  %10 = load i32, i32* %i, align 4
  %cmp18 = icmp slt i32 %10, 32
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [64 x i8], [64 x i8]* %keys, i32 0, i32 %11
  %12 = load i8, i8* %arrayidx, align 1
  %13 = load i8*, i8** %tx.addr, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr i8, i8* %13, i32 %14
  store i8 %12, i8* %arrayidx19, align 1
  %15 = load i32, i32* %i, align 4
  %add = add i32 %15, 32
  %arrayidx20 = getelementptr [64 x i8], [64 x i8]* %keys, i32 0, i32 %add
  %16 = load i8, i8* %arrayidx20, align 1
  %17 = load i8*, i8** %rx.addr, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr i8, i8* %17, i32 %18
  store i8 %16, i8* %arrayidx21, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay22 = getelementptr inbounds [64 x i8], [64 x i8]* %keys, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay22, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then8
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_publickeybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_secretkeybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_seedbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_kx_sessionkeybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_kx_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str, i32 0, i32 0)
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
