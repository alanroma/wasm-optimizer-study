; ModuleID = 'crypto_pwhash/scryptsalsa208sha256/scrypt_platform.c'
source_filename = "crypto_pwhash/scryptsalsa208sha256/scrypt_platform.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.escrypt_region_t = type { i8*, i8*, i32 }

; Function Attrs: noinline nounwind optnone
define hidden i8* @escrypt_alloc_region(%struct.escrypt_region_t* %region, i32 %size) #0 {
entry:
  %region.addr = alloca %struct.escrypt_region_t*, align 4
  %size.addr = alloca i32, align 4
  %base = alloca i8*, align 4
  %aligned = alloca i8*, align 4
  store %struct.escrypt_region_t* %region, %struct.escrypt_region_t** %region.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %call = call i32 @posix_memalign(i8** %base, i32 64, i32 %0)
  %call1 = call i32* @__errno_location()
  store i32 %call, i32* %call1, align 4
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %base, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load i8*, i8** %base, align 4
  store i8* %1, i8** %aligned, align 4
  %2 = load i8*, i8** %base, align 4
  %3 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %base2 = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %3, i32 0, i32 0
  store i8* %2, i8** %base2, align 4
  %4 = load i8*, i8** %aligned, align 4
  %5 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %aligned3 = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %5, i32 0, i32 1
  store i8* %4, i8** %aligned3, align 4
  %6 = load i8*, i8** %base, align 4
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %7 = load i32, i32* %size.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %7, %cond.true ], [ 0, %cond.false ]
  %8 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %size4 = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %8, i32 0, i32 2
  store i32 %cond, i32* %size4, align 4
  %9 = load i8*, i8** %aligned, align 4
  ret i8* %9
}

declare i32 @posix_memalign(i8**, i32, i32) #1

declare i32* @__errno_location() #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @escrypt_free_region(%struct.escrypt_region_t* %region) #0 {
entry:
  %region.addr = alloca %struct.escrypt_region_t*, align 4
  store %struct.escrypt_region_t* %region, %struct.escrypt_region_t** %region.addr, align 4
  %0 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %base = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %0, i32 0, i32 0
  %1 = load i8*, i8** %base, align 4
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %base1 = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %2, i32 0, i32 0
  %3 = load i8*, i8** %base1, align 4
  call void @free(i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  call void @init_region(%struct.escrypt_region_t* %4)
  ret i32 0
}

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @init_region(%struct.escrypt_region_t* %region) #0 {
entry:
  %region.addr = alloca %struct.escrypt_region_t*, align 4
  store %struct.escrypt_region_t* %region, %struct.escrypt_region_t** %region.addr, align 4
  %0 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %aligned = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %0, i32 0, i32 1
  store i8* null, i8** %aligned, align 4
  %1 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %base = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %1, i32 0, i32 0
  store i8* null, i8** %base, align 4
  %2 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %region.addr, align 4
  %size = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %2, i32 0, i32 2
  store i32 0, i32* %size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @escrypt_init_local(%struct.escrypt_region_t* %local) #0 {
entry:
  %local.addr = alloca %struct.escrypt_region_t*, align 4
  store %struct.escrypt_region_t* %local, %struct.escrypt_region_t** %local.addr, align 4
  %0 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  call void @init_region(%struct.escrypt_region_t* %0)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @escrypt_free_local(%struct.escrypt_region_t* %local) #0 {
entry:
  %local.addr = alloca %struct.escrypt_region_t*, align 4
  store %struct.escrypt_region_t* %local, %struct.escrypt_region_t** %local.addr, align 4
  %0 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %call = call i32 @escrypt_free_region(%struct.escrypt_region_t* %0)
  ret i32 %call
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
