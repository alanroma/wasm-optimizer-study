; ModuleID = 'crypto_auth/hmacsha512/auth_hmacsha512.c'
source_filename = "crypto_auth/hmacsha512/auth_hmacsha512.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_auth_hmacsha512_state = type { %struct.crypto_hash_sha512_state, %struct.crypto_hash_sha512_state }
%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_bytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_statebytes() #0 {
entry:
  ret i32 416
}

; Function Attrs: noinline nounwind optnone
define void @crypto_auth_hmacsha512_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_init(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* nonnull %key, i32 %keylen) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %key.addr = alloca i8*, align 4
  %keylen.addr = alloca i32, align 4
  %pad = alloca [128 x i8], align 16
  %khash = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %key, i8** %key.addr, align 4
  store i32 %keylen, i32* %keylen.addr, align 4
  %0 = load i32, i32* %keylen.addr, align 4
  %cmp = icmp ugt i32 %0, 128
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %1, i32 0, i32 0
  %call = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %ictx)
  %2 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx1 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %2, i32 0, i32 0
  %3 = load i8*, i8** %key.addr, align 4
  %4 = load i32, i32* %keylen.addr, align 4
  %conv = zext i32 %4 to i64
  %call2 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %ictx1, i8* %3, i64 %conv)
  %5 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx3 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %5, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %khash, i32 0, i32 0
  %call4 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %ictx3, i8* %arraydecay)
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %khash, i32 0, i32 0
  store i8* %arraydecay5, i8** %key.addr, align 4
  store i32 64, i32* %keylen.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx6 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %6, i32 0, i32 0
  %call7 = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %ictx6)
  %arraydecay8 = getelementptr inbounds [128 x i8], [128 x i8]* %pad, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay8, i8 54, i32 128, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %keylen.addr, align 4
  %cmp9 = icmp ult i32 %7, %8
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i8*, i8** %key.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1
  %conv11 = zext i8 %11 to i32
  %12 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr [128 x i8], [128 x i8]* %pad, i32 0, i32 %12
  %13 = load i8, i8* %arrayidx12, align 1
  %conv13 = zext i8 %13 to i32
  %xor = xor i32 %conv13, %conv11
  %conv14 = trunc i32 %xor to i8
  store i8 %conv14, i8* %arrayidx12, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx15 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %15, i32 0, i32 0
  %arraydecay16 = getelementptr inbounds [128 x i8], [128 x i8]* %pad, i32 0, i32 0
  %call17 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %ictx15, i8* %arraydecay16, i64 128)
  %16 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %octx = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %16, i32 0, i32 1
  %call18 = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %octx)
  %arraydecay19 = getelementptr inbounds [128 x i8], [128 x i8]* %pad, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay19, i8 92, i32 128, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc30, %for.end
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %keylen.addr, align 4
  %cmp21 = icmp ult i32 %17, %18
  br i1 %cmp21, label %for.body23, label %for.end32

for.body23:                                       ; preds = %for.cond20
  %19 = load i8*, i8** %key.addr, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr i8, i8* %19, i32 %20
  %21 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %21 to i32
  %22 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr [128 x i8], [128 x i8]* %pad, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx26, align 1
  %conv27 = zext i8 %23 to i32
  %xor28 = xor i32 %conv27, %conv25
  %conv29 = trunc i32 %xor28 to i8
  store i8 %conv29, i8* %arrayidx26, align 1
  br label %for.inc30

for.inc30:                                        ; preds = %for.body23
  %24 = load i32, i32* %i, align 4
  %inc31 = add i32 %24, 1
  store i32 %inc31, i32* %i, align 4
  br label %for.cond20

for.end32:                                        ; preds = %for.cond20
  %25 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %octx33 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %25, i32 0, i32 1
  %arraydecay34 = getelementptr inbounds [128 x i8], [128 x i8]* %pad, i32 0, i32 0
  %call35 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %octx33, i8* %arraydecay34, i64 128)
  %arraydecay36 = getelementptr inbounds [128 x i8], [128 x i8]* %pad, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay36, i32 128)
  %arraydecay37 = getelementptr inbounds [64 x i8], [64 x i8]* %khash, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay37, i32 64)
  ret i32 0
}

declare i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state*) #1

declare i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state*, i8*, i64) #1

declare i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_update(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* %in, i64 %inlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  %0 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %0, i32 0, i32 0
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  %call = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %ictx, i8* %1, i64 %2)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_final(%struct.crypto_auth_hmacsha512_state* nonnull %state, i8* nonnull %out) #0 {
entry:
  %state.addr = alloca %struct.crypto_auth_hmacsha512_state*, align 4
  %out.addr = alloca i8*, align 4
  %ihash = alloca [64 x i8], align 16
  store %struct.crypto_auth_hmacsha512_state* %state, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  store i8* %out, i8** %out.addr, align 4
  %0 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %ictx = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %ihash, i32 0, i32 0
  %call = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %ictx, i8* %arraydecay)
  %1 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %octx = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %1, i32 0, i32 1
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %ihash, i32 0, i32 0
  %call2 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %octx, i8* %arraydecay1, i64 64)
  %2 = load %struct.crypto_auth_hmacsha512_state*, %struct.crypto_auth_hmacsha512_state** %state.addr, align 4
  %octx3 = getelementptr inbounds %struct.crypto_auth_hmacsha512_state, %struct.crypto_auth_hmacsha512_state* %2, i32 0, i32 1
  %3 = load i8*, i8** %out.addr, align 4
  %call4 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %octx3, i8* %3)
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %ihash, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay5, i32 64)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512(i8* nonnull %out, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %state = alloca %struct.crypto_auth_hmacsha512_state, align 8
  store i8* %out, i8** %out.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_auth_hmacsha512_init(%struct.crypto_auth_hmacsha512_state* %state, i8* %0, i32 32)
  %1 = load i8*, i8** %in.addr, align 4
  %2 = load i64, i64* %inlen.addr, align 8
  %call1 = call i32 @crypto_auth_hmacsha512_update(%struct.crypto_auth_hmacsha512_state* %state, i8* %1, i64 %2)
  %3 = load i8*, i8** %out.addr, align 4
  %call2 = call i32 @crypto_auth_hmacsha512_final(%struct.crypto_auth_hmacsha512_state* %state, i8* %3)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_auth_hmacsha512_verify(i8* nonnull %h, i8* %in, i64 %inlen, i8* nonnull %k) #0 {
entry:
  %h.addr = alloca i8*, align 4
  %in.addr = alloca i8*, align 4
  %inlen.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %correct = alloca [64 x i8], align 16
  store i8* %h, i8** %h.addr, align 4
  store i8* %in, i8** %in.addr, align 4
  store i64 %inlen, i64* %inlen.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %correct, i32 0, i32 0
  %0 = load i8*, i8** %in.addr, align 4
  %1 = load i64, i64* %inlen.addr, align 8
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_auth_hmacsha512(i8* %arraydecay, i8* %0, i64 %1, i8* %2)
  %3 = load i8*, i8** %h.addr, align 4
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %correct, i32 0, i32 0
  %call2 = call i32 @crypto_verify_64(i8* %3, i8* %arraydecay1)
  %4 = load i8*, i8** %h.addr, align 4
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %correct, i32 0, i32 0
  %cmp = icmp eq i8* %4, %arraydecay3
  %conv = zext i1 %cmp to i32
  %sub = sub i32 0, %conv
  %or = or i32 %call2, %sub
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %correct, i32 0, i32 0
  %5 = load i8*, i8** %h.addr, align 4
  %call5 = call i32 @sodium_memcmp(i8* %arraydecay4, i8* %5, i32 64)
  %or6 = or i32 %or, %call5
  ret i32 %or6
}

declare i32 @crypto_verify_64(i8*, i8*) #1

declare i32 @sodium_memcmp(i8*, i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
