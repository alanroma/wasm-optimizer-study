; ModuleID = 'crypto_pwhash/crypto_pwhash.c'
source_filename = "crypto_pwhash/crypto_pwhash.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@.str = private unnamed_addr constant [11 x i8] c"$argon2id$\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c"$argon2i$\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"argon2i\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_alg_argon2i13() #0 {
entry:
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_alg_argon2id13() #0 {
entry:
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_alg_default() #0 {
entry:
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_bytes_min() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_bytes_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_passwd_min() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_passwd_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_saltbytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_strbytes() #0 {
entry:
  ret i32 128
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_pwhash_strprefix() #0 {
entry:
  ret i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_opslimit_min() #0 {
entry:
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_opslimit_max() #0 {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_memlimit_min() #0 {
entry:
  ret i32 8192
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_memlimit_max() #0 {
entry:
  ret i32 -2147483648
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_opslimit_interactive() #0 {
entry:
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_memlimit_interactive() #0 {
entry:
  ret i32 67108864
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_opslimit_moderate() #0 {
entry:
  ret i32 3
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_memlimit_moderate() #0 {
entry:
  ret i32 268435456
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_opslimit_sensitive() #0 {
entry:
  ret i32 4
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_memlimit_sensitive() #0 {
entry:
  ret i32 1073741824
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash(i8* nonnull %out, i64 %outlen, i8* nonnull %passwd, i64 %passwdlen, i8* nonnull %salt, i64 %opslimit, i32 %memlimit, i32 %alg) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %outlen.addr = alloca i64, align 8
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %salt.addr = alloca i8*, align 4
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %alg.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i64 %outlen, i64* %outlen.addr, align 8
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i8* %salt, i8** %salt.addr, align 4
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  store i32 %alg, i32* %alg.addr, align 4
  %0 = load i32, i32* %alg.addr, align 4
  switch i32 %0, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %out.addr, align 4
  %2 = load i64, i64* %outlen.addr, align 8
  %3 = load i8*, i8** %passwd.addr, align 4
  %4 = load i64, i64* %passwdlen.addr, align 8
  %5 = load i8*, i8** %salt.addr, align 4
  %6 = load i64, i64* %opslimit.addr, align 8
  %7 = load i32, i32* %memlimit.addr, align 4
  %8 = load i32, i32* %alg.addr, align 4
  %call = call i32 @crypto_pwhash_argon2i(i8* %1, i64 %2, i8* %3, i64 %4, i8* %5, i64 %6, i32 %7, i32 %8)
  store i32 %call, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %9 = load i8*, i8** %out.addr, align 4
  %10 = load i64, i64* %outlen.addr, align 8
  %11 = load i8*, i8** %passwd.addr, align 4
  %12 = load i64, i64* %passwdlen.addr, align 8
  %13 = load i8*, i8** %salt.addr, align 4
  %14 = load i64, i64* %opslimit.addr, align 8
  %15 = load i32, i32* %memlimit.addr, align 4
  %16 = load i32, i32* %alg.addr, align 4
  %call2 = call i32 @crypto_pwhash_argon2id(i8* %9, i64 %10, i8* %11, i64 %12, i8* %13, i64 %14, i32 %15, i32 %16)
  store i32 %call2, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %call3 = call i32* @__errno_location()
  store i32 28, i32* %call3, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb1, %sw.bb
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare i32 @crypto_pwhash_argon2i(i8*, i64, i8*, i64, i8*, i64, i32, i32) #1

declare i32 @crypto_pwhash_argon2id(i8*, i64, i8*, i64, i8*, i64, i32, i32) #1

declare i32* @__errno_location() #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_str(i8* nonnull %out, i8* nonnull %passwd, i64 %passwdlen, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %out.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i8*, i8** %out.addr, align 4
  %1 = load i8*, i8** %passwd.addr, align 4
  %2 = load i64, i64* %passwdlen.addr, align 8
  %3 = load i64, i64* %opslimit.addr, align 8
  %4 = load i32, i32* %memlimit.addr, align 4
  %call = call i32 @crypto_pwhash_argon2id_str(i8* %0, i8* %1, i64 %2, i64 %3, i32 %4)
  ret i32 %call
}

declare i32 @crypto_pwhash_argon2id_str(i8*, i8*, i64, i64, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_str_alg(i8* nonnull %out, i8* nonnull %passwd, i64 %passwdlen, i64 %opslimit, i32 %memlimit, i32 %alg) #0 {
entry:
  %retval = alloca i32, align 4
  %out.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  %alg.addr = alloca i32, align 4
  store i8* %out, i8** %out.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  store i32 %alg, i32* %alg.addr, align 4
  %0 = load i32, i32* %alg.addr, align 4
  switch i32 %0, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i8*, i8** %out.addr, align 4
  %2 = load i8*, i8** %passwd.addr, align 4
  %3 = load i64, i64* %passwdlen.addr, align 8
  %4 = load i64, i64* %opslimit.addr, align 8
  %5 = load i32, i32* %memlimit.addr, align 4
  %call = call i32 @crypto_pwhash_argon2i_str(i8* %1, i8* %2, i64 %3, i64 %4, i32 %5)
  store i32 %call, i32* %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry
  %6 = load i8*, i8** %out.addr, align 4
  %7 = load i8*, i8** %passwd.addr, align 4
  %8 = load i64, i64* %passwdlen.addr, align 8
  %9 = load i64, i64* %opslimit.addr, align 8
  %10 = load i32, i32* %memlimit.addr, align 4
  %call2 = call i32 @crypto_pwhash_argon2id_str(i8* %6, i8* %7, i64 %8, i64 %9, i32 %10)
  store i32 %call2, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  call void @sodium_misuse() #3
  unreachable

return:                                           ; preds = %sw.bb1, %sw.bb
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare i32 @crypto_pwhash_argon2i_str(i8*, i8*, i64, i64, i32) #1

; Function Attrs: noreturn
declare void @sodium_misuse() #2

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_str_verify(i8* nonnull %str, i8* nonnull %passwd, i64 %passwdlen) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i64, align 8
  store i8* %str, i8** %str.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i64 %passwdlen, i64* %passwdlen.addr, align 8
  %0 = load i8*, i8** %str.addr, align 4
  %call = call i32 @strncmp(i8* %0, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0), i32 10)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %str.addr, align 4
  %2 = load i8*, i8** %passwd.addr, align 4
  %3 = load i64, i64* %passwdlen.addr, align 8
  %call1 = call i32 @crypto_pwhash_argon2id_str_verify(i8* %1, i8* %2, i64 %3)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i8*, i8** %str.addr, align 4
  %call2 = call i32 @strncmp(i8* %4, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), i32 9)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %5 = load i8*, i8** %str.addr, align 4
  %6 = load i8*, i8** %passwd.addr, align 4
  %7 = load i64, i64* %passwdlen.addr, align 8
  %call5 = call i32 @crypto_pwhash_argon2i_str_verify(i8* %5, i8* %6, i64 %7)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %call7 = call i32* @__errno_location()
  store i32 28, i32* %call7, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then4, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @strncmp(i8*, i8*, i32) #1

declare i32 @crypto_pwhash_argon2id_str_verify(i8*, i8*, i64) #1

declare i32 @crypto_pwhash_argon2i_str_verify(i8*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_str_needs_rehash(i8* nonnull %str, i64 %opslimit, i32 %memlimit) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %opslimit.addr = alloca i64, align 8
  %memlimit.addr = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i64 %opslimit, i64* %opslimit.addr, align 8
  store i32 %memlimit, i32* %memlimit.addr, align 4
  %0 = load i8*, i8** %str.addr, align 4
  %call = call i32 @strncmp(i8* %0, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str, i32 0, i32 0), i32 10)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %str.addr, align 4
  %2 = load i64, i64* %opslimit.addr, align 8
  %3 = load i32, i32* %memlimit.addr, align 4
  %call1 = call i32 @crypto_pwhash_argon2id_str_needs_rehash(i8* %1, i64 %2, i32 %3)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i8*, i8** %str.addr, align 4
  %call2 = call i32 @strncmp(i8* %4, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0), i32 9)
  %cmp3 = icmp eq i32 %call2, 0
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %5 = load i8*, i8** %str.addr, align 4
  %6 = load i64, i64* %opslimit.addr, align 8
  %7 = load i32, i32* %memlimit.addr, align 4
  %call5 = call i32 @crypto_pwhash_argon2i_str_needs_rehash(i8* %5, i64 %6, i32 %7)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %call7 = call i32* @__errno_location()
  store i32 28, i32* %call7, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then4, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @crypto_pwhash_argon2id_str_needs_rehash(i8*, i64, i32) #1

declare i32 @crypto_pwhash_argon2i_str_needs_rehash(i8*, i64, i32) #1

; Function Attrs: noinline nounwind optnone
define i8* @crypto_pwhash_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0)
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
