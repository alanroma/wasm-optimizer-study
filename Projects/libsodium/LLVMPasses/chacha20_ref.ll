; ModuleID = 'crypto_stream/chacha20/ref/chacha20_ref.c'
source_filename = "crypto_stream/chacha20/ref/chacha20_ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_stream_chacha20_implementation = type { i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i8*, i64, i8*, i64, i8*)*, i32 (i8*, i8*, i64, i8*, i32, i8*)* }
%struct.chacha_ctx = type { [16 x i32] }

@crypto_stream_chacha20_ref_implementation = hidden global %struct.crypto_stream_chacha20_implementation { i32 (i8*, i64, i8*, i8*)* @stream_ref, i32 (i8*, i64, i8*, i8*)* @stream_ietf_ext_ref, i32 (i8*, i8*, i64, i8*, i64, i8*)* @stream_ref_xor_ic, i32 (i8*, i8*, i64, i8*, i32, i8*)* @stream_ietf_ext_ref_xor_ic }, align 4

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ref(i8* %c, i64 %clen, i8* %n, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %ctx = alloca %struct.chacha_ctx, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %k.addr, align 4
  call void @chacha_keysetup(%struct.chacha_ctx* %ctx, i8* %1)
  %2 = load i8*, i8** %n.addr, align 4
  call void @chacha_ivsetup(%struct.chacha_ctx* %ctx, i8* %2, i8* null)
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %conv = trunc i64 %4 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %3, i8 0, i32 %conv, i1 false)
  %5 = load i8*, i8** %c.addr, align 4
  %6 = load i8*, i8** %c.addr, align 4
  %7 = load i64, i64* %clen.addr, align 8
  call void @chacha20_encrypt_bytes(%struct.chacha_ctx* %ctx, i8* %5, i8* %6, i64 %7)
  %8 = bitcast %struct.chacha_ctx* %ctx to i8*
  call void @sodium_memzero(i8* %8, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ietf_ext_ref(i8* %c, i64 %clen, i8* %n, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %ctx = alloca %struct.chacha_ctx, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %k.addr, align 4
  call void @chacha_keysetup(%struct.chacha_ctx* %ctx, i8* %1)
  %2 = load i8*, i8** %n.addr, align 4
  call void @chacha_ietf_ivsetup(%struct.chacha_ctx* %ctx, i8* %2, i8* null)
  %3 = load i8*, i8** %c.addr, align 4
  %4 = load i64, i64* %clen.addr, align 8
  %conv = trunc i64 %4 to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %3, i8 0, i32 %conv, i1 false)
  %5 = load i8*, i8** %c.addr, align 4
  %6 = load i8*, i8** %c.addr, align 4
  %7 = load i64, i64* %clen.addr, align 8
  call void @chacha20_encrypt_bytes(%struct.chacha_ctx* %ctx, i8* %5, i8* %6, i64 %7)
  %8 = bitcast %struct.chacha_ctx* %ctx to i8*
  call void @sodium_memzero(i8* %8, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ref_xor_ic(i8* %c, i8* %m, i64 %mlen, i8* %n, i64 %ic, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %ctx = alloca %struct.chacha_ctx, align 4
  %ic_bytes = alloca [8 x i8], align 1
  %ic_high = alloca i32, align 4
  %ic_low = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i64 %ic, i64* %ic.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i64, i64* %ic.addr, align 8
  %shr = lshr i64 %1, 32
  %conv = trunc i64 %shr to i32
  store i32 %conv, i32* %ic_high, align 4
  %2 = load i64, i64* %ic.addr, align 8
  %conv1 = trunc i64 %2 to i32
  store i32 %conv1, i32* %ic_low, align 4
  %arrayidx = getelementptr [8 x i8], [8 x i8]* %ic_bytes, i32 0, i32 0
  %3 = load i32, i32* %ic_low, align 4
  call void @store32_le(i8* %arrayidx, i32 %3)
  %arrayidx2 = getelementptr [8 x i8], [8 x i8]* %ic_bytes, i32 0, i32 4
  %4 = load i32, i32* %ic_high, align 4
  call void @store32_le(i8* %arrayidx2, i32 %4)
  %5 = load i8*, i8** %k.addr, align 4
  call void @chacha_keysetup(%struct.chacha_ctx* %ctx, i8* %5)
  %6 = load i8*, i8** %n.addr, align 4
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %ic_bytes, i32 0, i32 0
  call void @chacha_ivsetup(%struct.chacha_ctx* %ctx, i8* %6, i8* %arraydecay)
  %7 = load i8*, i8** %m.addr, align 4
  %8 = load i8*, i8** %c.addr, align 4
  %9 = load i64, i64* %mlen.addr, align 8
  call void @chacha20_encrypt_bytes(%struct.chacha_ctx* %ctx, i8* %7, i8* %8, i64 %9)
  %10 = bitcast %struct.chacha_ctx* %ctx to i8*
  call void @sodium_memzero(i8* %10, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ietf_ext_ref_xor_ic(i8* %c, i8* %m, i64 %mlen, i8* %n, i32 %ic, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i32, align 4
  %k.addr = alloca i8*, align 4
  %ctx = alloca %struct.chacha_ctx, align 4
  %ic_bytes = alloca [4 x i8], align 1
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i32 %ic, i32* %ic.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %ic_bytes, i32 0, i32 0
  %1 = load i32, i32* %ic.addr, align 4
  call void @store32_le(i8* %arraydecay, i32 %1)
  %2 = load i8*, i8** %k.addr, align 4
  call void @chacha_keysetup(%struct.chacha_ctx* %ctx, i8* %2)
  %3 = load i8*, i8** %n.addr, align 4
  %arraydecay1 = getelementptr inbounds [4 x i8], [4 x i8]* %ic_bytes, i32 0, i32 0
  call void @chacha_ietf_ivsetup(%struct.chacha_ctx* %ctx, i8* %3, i8* %arraydecay1)
  %4 = load i8*, i8** %m.addr, align 4
  %5 = load i8*, i8** %c.addr, align 4
  %6 = load i64, i64* %mlen.addr, align 8
  call void @chacha20_encrypt_bytes(%struct.chacha_ctx* %ctx, i8* %4, i8* %5, i64 %6)
  %7 = bitcast %struct.chacha_ctx* %ctx to i8*
  call void @sodium_memzero(i8* %7, i32 64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define internal void @chacha_keysetup(%struct.chacha_ctx* %ctx, i8* %k) #0 {
entry:
  %ctx.addr = alloca %struct.chacha_ctx*, align 4
  %k.addr = alloca i8*, align 4
  store %struct.chacha_ctx* %ctx, %struct.chacha_ctx** %ctx.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %0, i32 0, i32 0
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %input, i32 0, i32 0
  store i32 1634760805, i32* %arrayidx, align 4
  %1 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input1 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %1, i32 0, i32 0
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %input1, i32 0, i32 1
  store i32 857760878, i32* %arrayidx2, align 4
  %2 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input3 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %2, i32 0, i32 0
  %arrayidx4 = getelementptr [16 x i32], [16 x i32]* %input3, i32 0, i32 2
  store i32 2036477234, i32* %arrayidx4, align 4
  %3 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input5 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %3, i32 0, i32 0
  %arrayidx6 = getelementptr [16 x i32], [16 x i32]* %input5, i32 0, i32 3
  store i32 1797285236, i32* %arrayidx6, align 4
  %4 = load i8*, i8** %k.addr, align 4
  %add.ptr = getelementptr i8, i8* %4, i32 0
  %call = call i32 @load32_le(i8* %add.ptr)
  %5 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input7 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %5, i32 0, i32 0
  %arrayidx8 = getelementptr [16 x i32], [16 x i32]* %input7, i32 0, i32 4
  store i32 %call, i32* %arrayidx8, align 4
  %6 = load i8*, i8** %k.addr, align 4
  %add.ptr9 = getelementptr i8, i8* %6, i32 4
  %call10 = call i32 @load32_le(i8* %add.ptr9)
  %7 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input11 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %7, i32 0, i32 0
  %arrayidx12 = getelementptr [16 x i32], [16 x i32]* %input11, i32 0, i32 5
  store i32 %call10, i32* %arrayidx12, align 4
  %8 = load i8*, i8** %k.addr, align 4
  %add.ptr13 = getelementptr i8, i8* %8, i32 8
  %call14 = call i32 @load32_le(i8* %add.ptr13)
  %9 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input15 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %9, i32 0, i32 0
  %arrayidx16 = getelementptr [16 x i32], [16 x i32]* %input15, i32 0, i32 6
  store i32 %call14, i32* %arrayidx16, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %add.ptr17 = getelementptr i8, i8* %10, i32 12
  %call18 = call i32 @load32_le(i8* %add.ptr17)
  %11 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input19 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %11, i32 0, i32 0
  %arrayidx20 = getelementptr [16 x i32], [16 x i32]* %input19, i32 0, i32 7
  store i32 %call18, i32* %arrayidx20, align 4
  %12 = load i8*, i8** %k.addr, align 4
  %add.ptr21 = getelementptr i8, i8* %12, i32 16
  %call22 = call i32 @load32_le(i8* %add.ptr21)
  %13 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input23 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %13, i32 0, i32 0
  %arrayidx24 = getelementptr [16 x i32], [16 x i32]* %input23, i32 0, i32 8
  store i32 %call22, i32* %arrayidx24, align 4
  %14 = load i8*, i8** %k.addr, align 4
  %add.ptr25 = getelementptr i8, i8* %14, i32 20
  %call26 = call i32 @load32_le(i8* %add.ptr25)
  %15 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input27 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %15, i32 0, i32 0
  %arrayidx28 = getelementptr [16 x i32], [16 x i32]* %input27, i32 0, i32 9
  store i32 %call26, i32* %arrayidx28, align 4
  %16 = load i8*, i8** %k.addr, align 4
  %add.ptr29 = getelementptr i8, i8* %16, i32 24
  %call30 = call i32 @load32_le(i8* %add.ptr29)
  %17 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input31 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %17, i32 0, i32 0
  %arrayidx32 = getelementptr [16 x i32], [16 x i32]* %input31, i32 0, i32 10
  store i32 %call30, i32* %arrayidx32, align 4
  %18 = load i8*, i8** %k.addr, align 4
  %add.ptr33 = getelementptr i8, i8* %18, i32 28
  %call34 = call i32 @load32_le(i8* %add.ptr33)
  %19 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input35 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %19, i32 0, i32 0
  %arrayidx36 = getelementptr [16 x i32], [16 x i32]* %input35, i32 0, i32 11
  store i32 %call34, i32* %arrayidx36, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @chacha_ivsetup(%struct.chacha_ctx* %ctx, i8* %iv, i8* %counter) #0 {
entry:
  %ctx.addr = alloca %struct.chacha_ctx*, align 4
  %iv.addr = alloca i8*, align 4
  %counter.addr = alloca i8*, align 4
  store %struct.chacha_ctx* %ctx, %struct.chacha_ctx** %ctx.addr, align 4
  store i8* %iv, i8** %iv.addr, align 4
  store i8* %counter, i8** %counter.addr, align 4
  %0 = load i8*, i8** %counter.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i8*, i8** %counter.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 0
  %call = call i32 @load32_le(i8* %add.ptr)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %call, %cond.false ]
  %2 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %2, i32 0, i32 0
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %input, i32 0, i32 12
  store i32 %cond, i32* %arrayidx, align 4
  %3 = load i8*, i8** %counter.addr, align 4
  %cmp1 = icmp eq i8* %3, null
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  br label %cond.end6

cond.false3:                                      ; preds = %cond.end
  %4 = load i8*, i8** %counter.addr, align 4
  %add.ptr4 = getelementptr i8, i8* %4, i32 4
  %call5 = call i32 @load32_le(i8* %add.ptr4)
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false3, %cond.true2
  %cond7 = phi i32 [ 0, %cond.true2 ], [ %call5, %cond.false3 ]
  %5 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input8 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %5, i32 0, i32 0
  %arrayidx9 = getelementptr [16 x i32], [16 x i32]* %input8, i32 0, i32 13
  store i32 %cond7, i32* %arrayidx9, align 4
  %6 = load i8*, i8** %iv.addr, align 4
  %add.ptr10 = getelementptr i8, i8* %6, i32 0
  %call11 = call i32 @load32_le(i8* %add.ptr10)
  %7 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input12 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %7, i32 0, i32 0
  %arrayidx13 = getelementptr [16 x i32], [16 x i32]* %input12, i32 0, i32 14
  store i32 %call11, i32* %arrayidx13, align 4
  %8 = load i8*, i8** %iv.addr, align 4
  %add.ptr14 = getelementptr i8, i8* %8, i32 4
  %call15 = call i32 @load32_le(i8* %add.ptr14)
  %9 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input16 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %9, i32 0, i32 0
  %arrayidx17 = getelementptr [16 x i32], [16 x i32]* %input16, i32 0, i32 15
  store i32 %call15, i32* %arrayidx17, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @chacha20_encrypt_bytes(%struct.chacha_ctx* %ctx, i8* %m, i8* %c, i64 %bytes) #0 {
entry:
  %ctx.addr = alloca %struct.chacha_ctx*, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %bytes.addr = alloca i64, align 8
  %x0 = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %x3 = alloca i32, align 4
  %x4 = alloca i32, align 4
  %x5 = alloca i32, align 4
  %x6 = alloca i32, align 4
  %x7 = alloca i32, align 4
  %x8 = alloca i32, align 4
  %x9 = alloca i32, align 4
  %x10 = alloca i32, align 4
  %x11 = alloca i32, align 4
  %x12 = alloca i32, align 4
  %x13 = alloca i32, align 4
  %x14 = alloca i32, align 4
  %x15 = alloca i32, align 4
  %j0 = alloca i32, align 4
  %j1 = alloca i32, align 4
  %j2 = alloca i32, align 4
  %j3 = alloca i32, align 4
  %j4 = alloca i32, align 4
  %j5 = alloca i32, align 4
  %j6 = alloca i32, align 4
  %j7 = alloca i32, align 4
  %j8 = alloca i32, align 4
  %j9 = alloca i32, align 4
  %j10 = alloca i32, align 4
  %j11 = alloca i32, align 4
  %j12 = alloca i32, align 4
  %j13 = alloca i32, align 4
  %j14 = alloca i32, align 4
  %j15 = alloca i32, align 4
  %ctarget = alloca i8*, align 4
  %tmp = alloca [64 x i8], align 16
  %i = alloca i32, align 4
  store %struct.chacha_ctx* %ctx, %struct.chacha_ctx** %ctx.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  store i8* null, i8** %ctarget, align 4
  %0 = load i64, i64* %bytes.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %1, i32 0, i32 0
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %input, i32 0, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  store i32 %2, i32* %j0, align 4
  %3 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input1 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %3, i32 0, i32 0
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %input1, i32 0, i32 1
  %4 = load i32, i32* %arrayidx2, align 4
  store i32 %4, i32* %j1, align 4
  %5 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input3 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %5, i32 0, i32 0
  %arrayidx4 = getelementptr [16 x i32], [16 x i32]* %input3, i32 0, i32 2
  %6 = load i32, i32* %arrayidx4, align 4
  store i32 %6, i32* %j2, align 4
  %7 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input5 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %7, i32 0, i32 0
  %arrayidx6 = getelementptr [16 x i32], [16 x i32]* %input5, i32 0, i32 3
  %8 = load i32, i32* %arrayidx6, align 4
  store i32 %8, i32* %j3, align 4
  %9 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input7 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %9, i32 0, i32 0
  %arrayidx8 = getelementptr [16 x i32], [16 x i32]* %input7, i32 0, i32 4
  %10 = load i32, i32* %arrayidx8, align 4
  store i32 %10, i32* %j4, align 4
  %11 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input9 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %11, i32 0, i32 0
  %arrayidx10 = getelementptr [16 x i32], [16 x i32]* %input9, i32 0, i32 5
  %12 = load i32, i32* %arrayidx10, align 4
  store i32 %12, i32* %j5, align 4
  %13 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input11 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %13, i32 0, i32 0
  %arrayidx12 = getelementptr [16 x i32], [16 x i32]* %input11, i32 0, i32 6
  %14 = load i32, i32* %arrayidx12, align 4
  store i32 %14, i32* %j6, align 4
  %15 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input13 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %15, i32 0, i32 0
  %arrayidx14 = getelementptr [16 x i32], [16 x i32]* %input13, i32 0, i32 7
  %16 = load i32, i32* %arrayidx14, align 4
  store i32 %16, i32* %j7, align 4
  %17 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input15 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %17, i32 0, i32 0
  %arrayidx16 = getelementptr [16 x i32], [16 x i32]* %input15, i32 0, i32 8
  %18 = load i32, i32* %arrayidx16, align 4
  store i32 %18, i32* %j8, align 4
  %19 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input17 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %19, i32 0, i32 0
  %arrayidx18 = getelementptr [16 x i32], [16 x i32]* %input17, i32 0, i32 9
  %20 = load i32, i32* %arrayidx18, align 4
  store i32 %20, i32* %j9, align 4
  %21 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input19 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %21, i32 0, i32 0
  %arrayidx20 = getelementptr [16 x i32], [16 x i32]* %input19, i32 0, i32 10
  %22 = load i32, i32* %arrayidx20, align 4
  store i32 %22, i32* %j10, align 4
  %23 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input21 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %23, i32 0, i32 0
  %arrayidx22 = getelementptr [16 x i32], [16 x i32]* %input21, i32 0, i32 11
  %24 = load i32, i32* %arrayidx22, align 4
  store i32 %24, i32* %j11, align 4
  %25 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input23 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %25, i32 0, i32 0
  %arrayidx24 = getelementptr [16 x i32], [16 x i32]* %input23, i32 0, i32 12
  %26 = load i32, i32* %arrayidx24, align 4
  store i32 %26, i32* %j12, align 4
  %27 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input25 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %27, i32 0, i32 0
  %arrayidx26 = getelementptr [16 x i32], [16 x i32]* %input25, i32 0, i32 13
  %28 = load i32, i32* %arrayidx26, align 4
  store i32 %28, i32* %j13, align 4
  %29 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input27 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %29, i32 0, i32 0
  %arrayidx28 = getelementptr [16 x i32], [16 x i32]* %input27, i32 0, i32 14
  %30 = load i32, i32* %arrayidx28, align 4
  store i32 %30, i32* %j14, align 4
  %31 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input29 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %31, i32 0, i32 0
  %arrayidx30 = getelementptr [16 x i32], [16 x i32]* %input29, i32 0, i32 15
  %32 = load i32, i32* %arrayidx30, align 4
  store i32 %32, i32* %j15, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end244, %if.end
  %33 = load i64, i64* %bytes.addr, align 8
  %cmp = icmp ult i64 %33, 64
  br i1 %cmp, label %if.then31, label %if.end39

if.then31:                                        ; preds = %for.cond
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %tmp, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 64, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc, %if.then31
  %34 = load i32, i32* %i, align 4
  %conv = zext i32 %34 to i64
  %35 = load i64, i64* %bytes.addr, align 8
  %cmp33 = icmp ult i64 %conv, %35
  br i1 %cmp33, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond32
  %36 = load i8*, i8** %m.addr, align 4
  %37 = load i32, i32* %i, align 4
  %arrayidx35 = getelementptr i8, i8* %36, i32 %37
  %38 = load i8, i8* %arrayidx35, align 1
  %39 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr [64 x i8], [64 x i8]* %tmp, i32 0, i32 %39
  store i8 %38, i8* %arrayidx36, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %40 = load i32, i32* %i, align 4
  %inc = add i32 %40, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond32

for.end:                                          ; preds = %for.cond32
  %arraydecay37 = getelementptr inbounds [64 x i8], [64 x i8]* %tmp, i32 0, i32 0
  store i8* %arraydecay37, i8** %m.addr, align 4
  %41 = load i8*, i8** %c.addr, align 4
  store i8* %41, i8** %ctarget, align 4
  %arraydecay38 = getelementptr inbounds [64 x i8], [64 x i8]* %tmp, i32 0, i32 0
  store i8* %arraydecay38, i8** %c.addr, align 4
  br label %if.end39

if.end39:                                         ; preds = %for.end, %for.cond
  %42 = load i32, i32* %j0, align 4
  store i32 %42, i32* %x0, align 4
  %43 = load i32, i32* %j1, align 4
  store i32 %43, i32* %x1, align 4
  %44 = load i32, i32* %j2, align 4
  store i32 %44, i32* %x2, align 4
  %45 = load i32, i32* %j3, align 4
  store i32 %45, i32* %x3, align 4
  %46 = load i32, i32* %j4, align 4
  store i32 %46, i32* %x4, align 4
  %47 = load i32, i32* %j5, align 4
  store i32 %47, i32* %x5, align 4
  %48 = load i32, i32* %j6, align 4
  store i32 %48, i32* %x6, align 4
  %49 = load i32, i32* %j7, align 4
  store i32 %49, i32* %x7, align 4
  %50 = load i32, i32* %j8, align 4
  store i32 %50, i32* %x8, align 4
  %51 = load i32, i32* %j9, align 4
  store i32 %51, i32* %x9, align 4
  %52 = load i32, i32* %j10, align 4
  store i32 %52, i32* %x10, align 4
  %53 = load i32, i32* %j11, align 4
  store i32 %53, i32* %x11, align 4
  %54 = load i32, i32* %j12, align 4
  store i32 %54, i32* %x12, align 4
  %55 = load i32, i32* %j13, align 4
  store i32 %55, i32* %x13, align 4
  %56 = load i32, i32* %j14, align 4
  store i32 %56, i32* %x14, align 4
  %57 = load i32, i32* %j15, align 4
  store i32 %57, i32* %x15, align 4
  store i32 20, i32* %i, align 4
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc137, %if.end39
  %58 = load i32, i32* %i, align 4
  %cmp41 = icmp ugt i32 %58, 0
  br i1 %cmp41, label %for.body43, label %for.end138

for.body43:                                       ; preds = %for.cond40
  %59 = load i32, i32* %x0, align 4
  %60 = load i32, i32* %x4, align 4
  %add = add i32 %59, %60
  store i32 %add, i32* %x0, align 4
  %61 = load i32, i32* %x12, align 4
  %62 = load i32, i32* %x0, align 4
  %xor = xor i32 %61, %62
  %call = call i32 @rotl32(i32 %xor, i32 16)
  store i32 %call, i32* %x12, align 4
  %63 = load i32, i32* %x8, align 4
  %64 = load i32, i32* %x12, align 4
  %add44 = add i32 %63, %64
  store i32 %add44, i32* %x8, align 4
  %65 = load i32, i32* %x4, align 4
  %66 = load i32, i32* %x8, align 4
  %xor45 = xor i32 %65, %66
  %call46 = call i32 @rotl32(i32 %xor45, i32 12)
  store i32 %call46, i32* %x4, align 4
  %67 = load i32, i32* %x0, align 4
  %68 = load i32, i32* %x4, align 4
  %add47 = add i32 %67, %68
  store i32 %add47, i32* %x0, align 4
  %69 = load i32, i32* %x12, align 4
  %70 = load i32, i32* %x0, align 4
  %xor48 = xor i32 %69, %70
  %call49 = call i32 @rotl32(i32 %xor48, i32 8)
  store i32 %call49, i32* %x12, align 4
  %71 = load i32, i32* %x8, align 4
  %72 = load i32, i32* %x12, align 4
  %add50 = add i32 %71, %72
  store i32 %add50, i32* %x8, align 4
  %73 = load i32, i32* %x4, align 4
  %74 = load i32, i32* %x8, align 4
  %xor51 = xor i32 %73, %74
  %call52 = call i32 @rotl32(i32 %xor51, i32 7)
  store i32 %call52, i32* %x4, align 4
  %75 = load i32, i32* %x1, align 4
  %76 = load i32, i32* %x5, align 4
  %add53 = add i32 %75, %76
  store i32 %add53, i32* %x1, align 4
  %77 = load i32, i32* %x13, align 4
  %78 = load i32, i32* %x1, align 4
  %xor54 = xor i32 %77, %78
  %call55 = call i32 @rotl32(i32 %xor54, i32 16)
  store i32 %call55, i32* %x13, align 4
  %79 = load i32, i32* %x9, align 4
  %80 = load i32, i32* %x13, align 4
  %add56 = add i32 %79, %80
  store i32 %add56, i32* %x9, align 4
  %81 = load i32, i32* %x5, align 4
  %82 = load i32, i32* %x9, align 4
  %xor57 = xor i32 %81, %82
  %call58 = call i32 @rotl32(i32 %xor57, i32 12)
  store i32 %call58, i32* %x5, align 4
  %83 = load i32, i32* %x1, align 4
  %84 = load i32, i32* %x5, align 4
  %add59 = add i32 %83, %84
  store i32 %add59, i32* %x1, align 4
  %85 = load i32, i32* %x13, align 4
  %86 = load i32, i32* %x1, align 4
  %xor60 = xor i32 %85, %86
  %call61 = call i32 @rotl32(i32 %xor60, i32 8)
  store i32 %call61, i32* %x13, align 4
  %87 = load i32, i32* %x9, align 4
  %88 = load i32, i32* %x13, align 4
  %add62 = add i32 %87, %88
  store i32 %add62, i32* %x9, align 4
  %89 = load i32, i32* %x5, align 4
  %90 = load i32, i32* %x9, align 4
  %xor63 = xor i32 %89, %90
  %call64 = call i32 @rotl32(i32 %xor63, i32 7)
  store i32 %call64, i32* %x5, align 4
  %91 = load i32, i32* %x2, align 4
  %92 = load i32, i32* %x6, align 4
  %add65 = add i32 %91, %92
  store i32 %add65, i32* %x2, align 4
  %93 = load i32, i32* %x14, align 4
  %94 = load i32, i32* %x2, align 4
  %xor66 = xor i32 %93, %94
  %call67 = call i32 @rotl32(i32 %xor66, i32 16)
  store i32 %call67, i32* %x14, align 4
  %95 = load i32, i32* %x10, align 4
  %96 = load i32, i32* %x14, align 4
  %add68 = add i32 %95, %96
  store i32 %add68, i32* %x10, align 4
  %97 = load i32, i32* %x6, align 4
  %98 = load i32, i32* %x10, align 4
  %xor69 = xor i32 %97, %98
  %call70 = call i32 @rotl32(i32 %xor69, i32 12)
  store i32 %call70, i32* %x6, align 4
  %99 = load i32, i32* %x2, align 4
  %100 = load i32, i32* %x6, align 4
  %add71 = add i32 %99, %100
  store i32 %add71, i32* %x2, align 4
  %101 = load i32, i32* %x14, align 4
  %102 = load i32, i32* %x2, align 4
  %xor72 = xor i32 %101, %102
  %call73 = call i32 @rotl32(i32 %xor72, i32 8)
  store i32 %call73, i32* %x14, align 4
  %103 = load i32, i32* %x10, align 4
  %104 = load i32, i32* %x14, align 4
  %add74 = add i32 %103, %104
  store i32 %add74, i32* %x10, align 4
  %105 = load i32, i32* %x6, align 4
  %106 = load i32, i32* %x10, align 4
  %xor75 = xor i32 %105, %106
  %call76 = call i32 @rotl32(i32 %xor75, i32 7)
  store i32 %call76, i32* %x6, align 4
  %107 = load i32, i32* %x3, align 4
  %108 = load i32, i32* %x7, align 4
  %add77 = add i32 %107, %108
  store i32 %add77, i32* %x3, align 4
  %109 = load i32, i32* %x15, align 4
  %110 = load i32, i32* %x3, align 4
  %xor78 = xor i32 %109, %110
  %call79 = call i32 @rotl32(i32 %xor78, i32 16)
  store i32 %call79, i32* %x15, align 4
  %111 = load i32, i32* %x11, align 4
  %112 = load i32, i32* %x15, align 4
  %add80 = add i32 %111, %112
  store i32 %add80, i32* %x11, align 4
  %113 = load i32, i32* %x7, align 4
  %114 = load i32, i32* %x11, align 4
  %xor81 = xor i32 %113, %114
  %call82 = call i32 @rotl32(i32 %xor81, i32 12)
  store i32 %call82, i32* %x7, align 4
  %115 = load i32, i32* %x3, align 4
  %116 = load i32, i32* %x7, align 4
  %add83 = add i32 %115, %116
  store i32 %add83, i32* %x3, align 4
  %117 = load i32, i32* %x15, align 4
  %118 = load i32, i32* %x3, align 4
  %xor84 = xor i32 %117, %118
  %call85 = call i32 @rotl32(i32 %xor84, i32 8)
  store i32 %call85, i32* %x15, align 4
  %119 = load i32, i32* %x11, align 4
  %120 = load i32, i32* %x15, align 4
  %add86 = add i32 %119, %120
  store i32 %add86, i32* %x11, align 4
  %121 = load i32, i32* %x7, align 4
  %122 = load i32, i32* %x11, align 4
  %xor87 = xor i32 %121, %122
  %call88 = call i32 @rotl32(i32 %xor87, i32 7)
  store i32 %call88, i32* %x7, align 4
  %123 = load i32, i32* %x0, align 4
  %124 = load i32, i32* %x5, align 4
  %add89 = add i32 %123, %124
  store i32 %add89, i32* %x0, align 4
  %125 = load i32, i32* %x15, align 4
  %126 = load i32, i32* %x0, align 4
  %xor90 = xor i32 %125, %126
  %call91 = call i32 @rotl32(i32 %xor90, i32 16)
  store i32 %call91, i32* %x15, align 4
  %127 = load i32, i32* %x10, align 4
  %128 = load i32, i32* %x15, align 4
  %add92 = add i32 %127, %128
  store i32 %add92, i32* %x10, align 4
  %129 = load i32, i32* %x5, align 4
  %130 = load i32, i32* %x10, align 4
  %xor93 = xor i32 %129, %130
  %call94 = call i32 @rotl32(i32 %xor93, i32 12)
  store i32 %call94, i32* %x5, align 4
  %131 = load i32, i32* %x0, align 4
  %132 = load i32, i32* %x5, align 4
  %add95 = add i32 %131, %132
  store i32 %add95, i32* %x0, align 4
  %133 = load i32, i32* %x15, align 4
  %134 = load i32, i32* %x0, align 4
  %xor96 = xor i32 %133, %134
  %call97 = call i32 @rotl32(i32 %xor96, i32 8)
  store i32 %call97, i32* %x15, align 4
  %135 = load i32, i32* %x10, align 4
  %136 = load i32, i32* %x15, align 4
  %add98 = add i32 %135, %136
  store i32 %add98, i32* %x10, align 4
  %137 = load i32, i32* %x5, align 4
  %138 = load i32, i32* %x10, align 4
  %xor99 = xor i32 %137, %138
  %call100 = call i32 @rotl32(i32 %xor99, i32 7)
  store i32 %call100, i32* %x5, align 4
  %139 = load i32, i32* %x1, align 4
  %140 = load i32, i32* %x6, align 4
  %add101 = add i32 %139, %140
  store i32 %add101, i32* %x1, align 4
  %141 = load i32, i32* %x12, align 4
  %142 = load i32, i32* %x1, align 4
  %xor102 = xor i32 %141, %142
  %call103 = call i32 @rotl32(i32 %xor102, i32 16)
  store i32 %call103, i32* %x12, align 4
  %143 = load i32, i32* %x11, align 4
  %144 = load i32, i32* %x12, align 4
  %add104 = add i32 %143, %144
  store i32 %add104, i32* %x11, align 4
  %145 = load i32, i32* %x6, align 4
  %146 = load i32, i32* %x11, align 4
  %xor105 = xor i32 %145, %146
  %call106 = call i32 @rotl32(i32 %xor105, i32 12)
  store i32 %call106, i32* %x6, align 4
  %147 = load i32, i32* %x1, align 4
  %148 = load i32, i32* %x6, align 4
  %add107 = add i32 %147, %148
  store i32 %add107, i32* %x1, align 4
  %149 = load i32, i32* %x12, align 4
  %150 = load i32, i32* %x1, align 4
  %xor108 = xor i32 %149, %150
  %call109 = call i32 @rotl32(i32 %xor108, i32 8)
  store i32 %call109, i32* %x12, align 4
  %151 = load i32, i32* %x11, align 4
  %152 = load i32, i32* %x12, align 4
  %add110 = add i32 %151, %152
  store i32 %add110, i32* %x11, align 4
  %153 = load i32, i32* %x6, align 4
  %154 = load i32, i32* %x11, align 4
  %xor111 = xor i32 %153, %154
  %call112 = call i32 @rotl32(i32 %xor111, i32 7)
  store i32 %call112, i32* %x6, align 4
  %155 = load i32, i32* %x2, align 4
  %156 = load i32, i32* %x7, align 4
  %add113 = add i32 %155, %156
  store i32 %add113, i32* %x2, align 4
  %157 = load i32, i32* %x13, align 4
  %158 = load i32, i32* %x2, align 4
  %xor114 = xor i32 %157, %158
  %call115 = call i32 @rotl32(i32 %xor114, i32 16)
  store i32 %call115, i32* %x13, align 4
  %159 = load i32, i32* %x8, align 4
  %160 = load i32, i32* %x13, align 4
  %add116 = add i32 %159, %160
  store i32 %add116, i32* %x8, align 4
  %161 = load i32, i32* %x7, align 4
  %162 = load i32, i32* %x8, align 4
  %xor117 = xor i32 %161, %162
  %call118 = call i32 @rotl32(i32 %xor117, i32 12)
  store i32 %call118, i32* %x7, align 4
  %163 = load i32, i32* %x2, align 4
  %164 = load i32, i32* %x7, align 4
  %add119 = add i32 %163, %164
  store i32 %add119, i32* %x2, align 4
  %165 = load i32, i32* %x13, align 4
  %166 = load i32, i32* %x2, align 4
  %xor120 = xor i32 %165, %166
  %call121 = call i32 @rotl32(i32 %xor120, i32 8)
  store i32 %call121, i32* %x13, align 4
  %167 = load i32, i32* %x8, align 4
  %168 = load i32, i32* %x13, align 4
  %add122 = add i32 %167, %168
  store i32 %add122, i32* %x8, align 4
  %169 = load i32, i32* %x7, align 4
  %170 = load i32, i32* %x8, align 4
  %xor123 = xor i32 %169, %170
  %call124 = call i32 @rotl32(i32 %xor123, i32 7)
  store i32 %call124, i32* %x7, align 4
  %171 = load i32, i32* %x3, align 4
  %172 = load i32, i32* %x4, align 4
  %add125 = add i32 %171, %172
  store i32 %add125, i32* %x3, align 4
  %173 = load i32, i32* %x14, align 4
  %174 = load i32, i32* %x3, align 4
  %xor126 = xor i32 %173, %174
  %call127 = call i32 @rotl32(i32 %xor126, i32 16)
  store i32 %call127, i32* %x14, align 4
  %175 = load i32, i32* %x9, align 4
  %176 = load i32, i32* %x14, align 4
  %add128 = add i32 %175, %176
  store i32 %add128, i32* %x9, align 4
  %177 = load i32, i32* %x4, align 4
  %178 = load i32, i32* %x9, align 4
  %xor129 = xor i32 %177, %178
  %call130 = call i32 @rotl32(i32 %xor129, i32 12)
  store i32 %call130, i32* %x4, align 4
  %179 = load i32, i32* %x3, align 4
  %180 = load i32, i32* %x4, align 4
  %add131 = add i32 %179, %180
  store i32 %add131, i32* %x3, align 4
  %181 = load i32, i32* %x14, align 4
  %182 = load i32, i32* %x3, align 4
  %xor132 = xor i32 %181, %182
  %call133 = call i32 @rotl32(i32 %xor132, i32 8)
  store i32 %call133, i32* %x14, align 4
  %183 = load i32, i32* %x9, align 4
  %184 = load i32, i32* %x14, align 4
  %add134 = add i32 %183, %184
  store i32 %add134, i32* %x9, align 4
  %185 = load i32, i32* %x4, align 4
  %186 = load i32, i32* %x9, align 4
  %xor135 = xor i32 %185, %186
  %call136 = call i32 @rotl32(i32 %xor135, i32 7)
  store i32 %call136, i32* %x4, align 4
  br label %for.inc137

for.inc137:                                       ; preds = %for.body43
  %187 = load i32, i32* %i, align 4
  %sub = sub i32 %187, 2
  store i32 %sub, i32* %i, align 4
  br label %for.cond40

for.end138:                                       ; preds = %for.cond40
  %188 = load i32, i32* %x0, align 4
  %189 = load i32, i32* %j0, align 4
  %add139 = add i32 %188, %189
  store i32 %add139, i32* %x0, align 4
  %190 = load i32, i32* %x1, align 4
  %191 = load i32, i32* %j1, align 4
  %add140 = add i32 %190, %191
  store i32 %add140, i32* %x1, align 4
  %192 = load i32, i32* %x2, align 4
  %193 = load i32, i32* %j2, align 4
  %add141 = add i32 %192, %193
  store i32 %add141, i32* %x2, align 4
  %194 = load i32, i32* %x3, align 4
  %195 = load i32, i32* %j3, align 4
  %add142 = add i32 %194, %195
  store i32 %add142, i32* %x3, align 4
  %196 = load i32, i32* %x4, align 4
  %197 = load i32, i32* %j4, align 4
  %add143 = add i32 %196, %197
  store i32 %add143, i32* %x4, align 4
  %198 = load i32, i32* %x5, align 4
  %199 = load i32, i32* %j5, align 4
  %add144 = add i32 %198, %199
  store i32 %add144, i32* %x5, align 4
  %200 = load i32, i32* %x6, align 4
  %201 = load i32, i32* %j6, align 4
  %add145 = add i32 %200, %201
  store i32 %add145, i32* %x6, align 4
  %202 = load i32, i32* %x7, align 4
  %203 = load i32, i32* %j7, align 4
  %add146 = add i32 %202, %203
  store i32 %add146, i32* %x7, align 4
  %204 = load i32, i32* %x8, align 4
  %205 = load i32, i32* %j8, align 4
  %add147 = add i32 %204, %205
  store i32 %add147, i32* %x8, align 4
  %206 = load i32, i32* %x9, align 4
  %207 = load i32, i32* %j9, align 4
  %add148 = add i32 %206, %207
  store i32 %add148, i32* %x9, align 4
  %208 = load i32, i32* %x10, align 4
  %209 = load i32, i32* %j10, align 4
  %add149 = add i32 %208, %209
  store i32 %add149, i32* %x10, align 4
  %210 = load i32, i32* %x11, align 4
  %211 = load i32, i32* %j11, align 4
  %add150 = add i32 %210, %211
  store i32 %add150, i32* %x11, align 4
  %212 = load i32, i32* %x12, align 4
  %213 = load i32, i32* %j12, align 4
  %add151 = add i32 %212, %213
  store i32 %add151, i32* %x12, align 4
  %214 = load i32, i32* %x13, align 4
  %215 = load i32, i32* %j13, align 4
  %add152 = add i32 %214, %215
  store i32 %add152, i32* %x13, align 4
  %216 = load i32, i32* %x14, align 4
  %217 = load i32, i32* %j14, align 4
  %add153 = add i32 %216, %217
  store i32 %add153, i32* %x14, align 4
  %218 = load i32, i32* %x15, align 4
  %219 = load i32, i32* %j15, align 4
  %add154 = add i32 %218, %219
  store i32 %add154, i32* %x15, align 4
  %220 = load i32, i32* %x0, align 4
  %221 = load i8*, i8** %m.addr, align 4
  %add.ptr = getelementptr i8, i8* %221, i32 0
  %call155 = call i32 @load32_le(i8* %add.ptr)
  %xor156 = xor i32 %220, %call155
  store i32 %xor156, i32* %x0, align 4
  %222 = load i32, i32* %x1, align 4
  %223 = load i8*, i8** %m.addr, align 4
  %add.ptr157 = getelementptr i8, i8* %223, i32 4
  %call158 = call i32 @load32_le(i8* %add.ptr157)
  %xor159 = xor i32 %222, %call158
  store i32 %xor159, i32* %x1, align 4
  %224 = load i32, i32* %x2, align 4
  %225 = load i8*, i8** %m.addr, align 4
  %add.ptr160 = getelementptr i8, i8* %225, i32 8
  %call161 = call i32 @load32_le(i8* %add.ptr160)
  %xor162 = xor i32 %224, %call161
  store i32 %xor162, i32* %x2, align 4
  %226 = load i32, i32* %x3, align 4
  %227 = load i8*, i8** %m.addr, align 4
  %add.ptr163 = getelementptr i8, i8* %227, i32 12
  %call164 = call i32 @load32_le(i8* %add.ptr163)
  %xor165 = xor i32 %226, %call164
  store i32 %xor165, i32* %x3, align 4
  %228 = load i32, i32* %x4, align 4
  %229 = load i8*, i8** %m.addr, align 4
  %add.ptr166 = getelementptr i8, i8* %229, i32 16
  %call167 = call i32 @load32_le(i8* %add.ptr166)
  %xor168 = xor i32 %228, %call167
  store i32 %xor168, i32* %x4, align 4
  %230 = load i32, i32* %x5, align 4
  %231 = load i8*, i8** %m.addr, align 4
  %add.ptr169 = getelementptr i8, i8* %231, i32 20
  %call170 = call i32 @load32_le(i8* %add.ptr169)
  %xor171 = xor i32 %230, %call170
  store i32 %xor171, i32* %x5, align 4
  %232 = load i32, i32* %x6, align 4
  %233 = load i8*, i8** %m.addr, align 4
  %add.ptr172 = getelementptr i8, i8* %233, i32 24
  %call173 = call i32 @load32_le(i8* %add.ptr172)
  %xor174 = xor i32 %232, %call173
  store i32 %xor174, i32* %x6, align 4
  %234 = load i32, i32* %x7, align 4
  %235 = load i8*, i8** %m.addr, align 4
  %add.ptr175 = getelementptr i8, i8* %235, i32 28
  %call176 = call i32 @load32_le(i8* %add.ptr175)
  %xor177 = xor i32 %234, %call176
  store i32 %xor177, i32* %x7, align 4
  %236 = load i32, i32* %x8, align 4
  %237 = load i8*, i8** %m.addr, align 4
  %add.ptr178 = getelementptr i8, i8* %237, i32 32
  %call179 = call i32 @load32_le(i8* %add.ptr178)
  %xor180 = xor i32 %236, %call179
  store i32 %xor180, i32* %x8, align 4
  %238 = load i32, i32* %x9, align 4
  %239 = load i8*, i8** %m.addr, align 4
  %add.ptr181 = getelementptr i8, i8* %239, i32 36
  %call182 = call i32 @load32_le(i8* %add.ptr181)
  %xor183 = xor i32 %238, %call182
  store i32 %xor183, i32* %x9, align 4
  %240 = load i32, i32* %x10, align 4
  %241 = load i8*, i8** %m.addr, align 4
  %add.ptr184 = getelementptr i8, i8* %241, i32 40
  %call185 = call i32 @load32_le(i8* %add.ptr184)
  %xor186 = xor i32 %240, %call185
  store i32 %xor186, i32* %x10, align 4
  %242 = load i32, i32* %x11, align 4
  %243 = load i8*, i8** %m.addr, align 4
  %add.ptr187 = getelementptr i8, i8* %243, i32 44
  %call188 = call i32 @load32_le(i8* %add.ptr187)
  %xor189 = xor i32 %242, %call188
  store i32 %xor189, i32* %x11, align 4
  %244 = load i32, i32* %x12, align 4
  %245 = load i8*, i8** %m.addr, align 4
  %add.ptr190 = getelementptr i8, i8* %245, i32 48
  %call191 = call i32 @load32_le(i8* %add.ptr190)
  %xor192 = xor i32 %244, %call191
  store i32 %xor192, i32* %x12, align 4
  %246 = load i32, i32* %x13, align 4
  %247 = load i8*, i8** %m.addr, align 4
  %add.ptr193 = getelementptr i8, i8* %247, i32 52
  %call194 = call i32 @load32_le(i8* %add.ptr193)
  %xor195 = xor i32 %246, %call194
  store i32 %xor195, i32* %x13, align 4
  %248 = load i32, i32* %x14, align 4
  %249 = load i8*, i8** %m.addr, align 4
  %add.ptr196 = getelementptr i8, i8* %249, i32 56
  %call197 = call i32 @load32_le(i8* %add.ptr196)
  %xor198 = xor i32 %248, %call197
  store i32 %xor198, i32* %x14, align 4
  %250 = load i32, i32* %x15, align 4
  %251 = load i8*, i8** %m.addr, align 4
  %add.ptr199 = getelementptr i8, i8* %251, i32 60
  %call200 = call i32 @load32_le(i8* %add.ptr199)
  %xor201 = xor i32 %250, %call200
  store i32 %xor201, i32* %x15, align 4
  %252 = load i32, i32* %j12, align 4
  %add202 = add i32 %252, 1
  store i32 %add202, i32* %j12, align 4
  %253 = load i32, i32* %j12, align 4
  %tobool203 = icmp ne i32 %253, 0
  br i1 %tobool203, label %if.end206, label %if.then204

if.then204:                                       ; preds = %for.end138
  %254 = load i32, i32* %j13, align 4
  %add205 = add i32 %254, 1
  store i32 %add205, i32* %j13, align 4
  br label %if.end206

if.end206:                                        ; preds = %if.then204, %for.end138
  %255 = load i8*, i8** %c.addr, align 4
  %add.ptr207 = getelementptr i8, i8* %255, i32 0
  %256 = load i32, i32* %x0, align 4
  call void @store32_le(i8* %add.ptr207, i32 %256)
  %257 = load i8*, i8** %c.addr, align 4
  %add.ptr208 = getelementptr i8, i8* %257, i32 4
  %258 = load i32, i32* %x1, align 4
  call void @store32_le(i8* %add.ptr208, i32 %258)
  %259 = load i8*, i8** %c.addr, align 4
  %add.ptr209 = getelementptr i8, i8* %259, i32 8
  %260 = load i32, i32* %x2, align 4
  call void @store32_le(i8* %add.ptr209, i32 %260)
  %261 = load i8*, i8** %c.addr, align 4
  %add.ptr210 = getelementptr i8, i8* %261, i32 12
  %262 = load i32, i32* %x3, align 4
  call void @store32_le(i8* %add.ptr210, i32 %262)
  %263 = load i8*, i8** %c.addr, align 4
  %add.ptr211 = getelementptr i8, i8* %263, i32 16
  %264 = load i32, i32* %x4, align 4
  call void @store32_le(i8* %add.ptr211, i32 %264)
  %265 = load i8*, i8** %c.addr, align 4
  %add.ptr212 = getelementptr i8, i8* %265, i32 20
  %266 = load i32, i32* %x5, align 4
  call void @store32_le(i8* %add.ptr212, i32 %266)
  %267 = load i8*, i8** %c.addr, align 4
  %add.ptr213 = getelementptr i8, i8* %267, i32 24
  %268 = load i32, i32* %x6, align 4
  call void @store32_le(i8* %add.ptr213, i32 %268)
  %269 = load i8*, i8** %c.addr, align 4
  %add.ptr214 = getelementptr i8, i8* %269, i32 28
  %270 = load i32, i32* %x7, align 4
  call void @store32_le(i8* %add.ptr214, i32 %270)
  %271 = load i8*, i8** %c.addr, align 4
  %add.ptr215 = getelementptr i8, i8* %271, i32 32
  %272 = load i32, i32* %x8, align 4
  call void @store32_le(i8* %add.ptr215, i32 %272)
  %273 = load i8*, i8** %c.addr, align 4
  %add.ptr216 = getelementptr i8, i8* %273, i32 36
  %274 = load i32, i32* %x9, align 4
  call void @store32_le(i8* %add.ptr216, i32 %274)
  %275 = load i8*, i8** %c.addr, align 4
  %add.ptr217 = getelementptr i8, i8* %275, i32 40
  %276 = load i32, i32* %x10, align 4
  call void @store32_le(i8* %add.ptr217, i32 %276)
  %277 = load i8*, i8** %c.addr, align 4
  %add.ptr218 = getelementptr i8, i8* %277, i32 44
  %278 = load i32, i32* %x11, align 4
  call void @store32_le(i8* %add.ptr218, i32 %278)
  %279 = load i8*, i8** %c.addr, align 4
  %add.ptr219 = getelementptr i8, i8* %279, i32 48
  %280 = load i32, i32* %x12, align 4
  call void @store32_le(i8* %add.ptr219, i32 %280)
  %281 = load i8*, i8** %c.addr, align 4
  %add.ptr220 = getelementptr i8, i8* %281, i32 52
  %282 = load i32, i32* %x13, align 4
  call void @store32_le(i8* %add.ptr220, i32 %282)
  %283 = load i8*, i8** %c.addr, align 4
  %add.ptr221 = getelementptr i8, i8* %283, i32 56
  %284 = load i32, i32* %x14, align 4
  call void @store32_le(i8* %add.ptr221, i32 %284)
  %285 = load i8*, i8** %c.addr, align 4
  %add.ptr222 = getelementptr i8, i8* %285, i32 60
  %286 = load i32, i32* %x15, align 4
  call void @store32_le(i8* %add.ptr222, i32 %286)
  %287 = load i64, i64* %bytes.addr, align 8
  %cmp223 = icmp ule i64 %287, 64
  br i1 %cmp223, label %if.then225, label %if.end244

if.then225:                                       ; preds = %if.end206
  %288 = load i64, i64* %bytes.addr, align 8
  %cmp226 = icmp ult i64 %288, 64
  br i1 %cmp226, label %if.then228, label %if.end239

if.then228:                                       ; preds = %if.then225
  store i32 0, i32* %i, align 4
  br label %for.cond229

for.cond229:                                      ; preds = %for.inc236, %if.then228
  %289 = load i32, i32* %i, align 4
  %290 = load i64, i64* %bytes.addr, align 8
  %conv230 = trunc i64 %290 to i32
  %cmp231 = icmp ult i32 %289, %conv230
  br i1 %cmp231, label %for.body233, label %for.end238

for.body233:                                      ; preds = %for.cond229
  %291 = load i8*, i8** %c.addr, align 4
  %292 = load i32, i32* %i, align 4
  %arrayidx234 = getelementptr i8, i8* %291, i32 %292
  %293 = load i8, i8* %arrayidx234, align 1
  %294 = load i8*, i8** %ctarget, align 4
  %295 = load i32, i32* %i, align 4
  %arrayidx235 = getelementptr i8, i8* %294, i32 %295
  store i8 %293, i8* %arrayidx235, align 1
  br label %for.inc236

for.inc236:                                       ; preds = %for.body233
  %296 = load i32, i32* %i, align 4
  %inc237 = add i32 %296, 1
  store i32 %inc237, i32* %i, align 4
  br label %for.cond229

for.end238:                                       ; preds = %for.cond229
  br label %if.end239

if.end239:                                        ; preds = %for.end238, %if.then225
  %297 = load i32, i32* %j12, align 4
  %298 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input240 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %298, i32 0, i32 0
  %arrayidx241 = getelementptr [16 x i32], [16 x i32]* %input240, i32 0, i32 12
  store i32 %297, i32* %arrayidx241, align 4
  %299 = load i32, i32* %j13, align 4
  %300 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input242 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %300, i32 0, i32 0
  %arrayidx243 = getelementptr [16 x i32], [16 x i32]* %input242, i32 0, i32 13
  store i32 %299, i32* %arrayidx243, align 4
  br label %return

if.end244:                                        ; preds = %if.end206
  %301 = load i64, i64* %bytes.addr, align 8
  %sub245 = sub i64 %301, 64
  store i64 %sub245, i64* %bytes.addr, align 8
  %302 = load i8*, i8** %c.addr, align 4
  %add.ptr246 = getelementptr i8, i8* %302, i32 64
  store i8* %add.ptr246, i8** %c.addr, align 4
  %303 = load i8*, i8** %m.addr, align 4
  %add.ptr247 = getelementptr i8, i8* %303, i32 64
  store i8* %add.ptr247, i8** %m.addr, align 4
  br label %for.cond

return:                                           ; preds = %if.end239, %if.then
  ret void
}

declare void @sodium_memzero(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define internal i32 @load32_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i32* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 1 %1, i32 4, i1 false)
  %2 = load i32, i32* %w, align 4
  ret i32 %2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal i32 @rotl32(i32 %x, i32 %b) #0 {
entry:
  %x.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %x, i32* %x.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %shl = shl i32 %0, %1
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %b.addr, align 4
  %sub = sub i32 32, %3
  %shr = lshr i32 %2, %sub
  %or = or i32 %shl, %shr
  ret i32 %or
}

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @chacha_ietf_ivsetup(%struct.chacha_ctx* %ctx, i8* %iv, i8* %counter) #0 {
entry:
  %ctx.addr = alloca %struct.chacha_ctx*, align 4
  %iv.addr = alloca i8*, align 4
  %counter.addr = alloca i8*, align 4
  store %struct.chacha_ctx* %ctx, %struct.chacha_ctx** %ctx.addr, align 4
  store i8* %iv, i8** %iv.addr, align 4
  store i8* %counter, i8** %counter.addr, align 4
  %0 = load i8*, i8** %counter.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i8*, i8** %counter.addr, align 4
  %call = call i32 @load32_le(i8* %1)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %call, %cond.false ]
  %2 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %2, i32 0, i32 0
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %input, i32 0, i32 12
  store i32 %cond, i32* %arrayidx, align 4
  %3 = load i8*, i8** %iv.addr, align 4
  %add.ptr = getelementptr i8, i8* %3, i32 0
  %call1 = call i32 @load32_le(i8* %add.ptr)
  %4 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input2 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %4, i32 0, i32 0
  %arrayidx3 = getelementptr [16 x i32], [16 x i32]* %input2, i32 0, i32 13
  store i32 %call1, i32* %arrayidx3, align 4
  %5 = load i8*, i8** %iv.addr, align 4
  %add.ptr4 = getelementptr i8, i8* %5, i32 4
  %call5 = call i32 @load32_le(i8* %add.ptr4)
  %6 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input6 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %6, i32 0, i32 0
  %arrayidx7 = getelementptr [16 x i32], [16 x i32]* %input6, i32 0, i32 14
  store i32 %call5, i32* %arrayidx7, align 4
  %7 = load i8*, i8** %iv.addr, align 4
  %add.ptr8 = getelementptr i8, i8* %7, i32 8
  %call9 = call i32 @load32_le(i8* %add.ptr8)
  %8 = load %struct.chacha_ctx*, %struct.chacha_ctx** %ctx.addr, align 4
  %input10 = getelementptr inbounds %struct.chacha_ctx, %struct.chacha_ctx* %8, i32 0, i32 0
  %arrayidx11 = getelementptr [16 x i32], [16 x i32]* %input10, i32 0, i32 15
  store i32 %call9, i32* %arrayidx11, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
