; ModuleID = 'crypto_pwhash/scryptsalsa208sha256/crypto_scrypt-common.c'
source_filename = "crypto_pwhash/scryptsalsa208sha256/crypto_scrypt-common.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.escrypt_region_t = type { i8*, i8*, i32 }

@.str = private unnamed_addr constant [65 x i8] c"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i8* @escrypt_parse_setting(i8* %setting, i32* %N_log2_p, i32* %r_p, i32* %p_p) #0 {
entry:
  %retval = alloca i8*, align 4
  %setting.addr = alloca i8*, align 4
  %N_log2_p.addr = alloca i32*, align 4
  %r_p.addr = alloca i32*, align 4
  %p_p.addr = alloca i32*, align 4
  %src = alloca i8*, align 4
  store i8* %setting, i8** %setting.addr, align 4
  store i32* %N_log2_p, i32** %N_log2_p.addr, align 4
  store i32* %r_p, i32** %r_p.addr, align 4
  store i32* %p_p, i32** %p_p.addr, align 4
  %0 = load i8*, i8** %setting.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 36
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i8*, i8** %setting.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %2, i32 1
  %3 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %3 to i32
  %cmp4 = icmp ne i32 %conv3, 55
  br i1 %cmp4, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false
  %4 = load i8*, i8** %setting.addr, align 4
  %arrayidx7 = getelementptr i8, i8* %4, i32 2
  %5 = load i8, i8* %arrayidx7, align 1
  %conv8 = zext i8 %5 to i32
  %cmp9 = icmp ne i32 %conv8, 36
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false, %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  %6 = load i8*, i8** %setting.addr, align 4
  %add.ptr = getelementptr i8, i8* %6, i32 3
  store i8* %add.ptr, i8** %src, align 4
  %7 = load i32*, i32** %N_log2_p.addr, align 4
  %8 = load i8*, i8** %src, align 4
  %9 = load i8, i8* %8, align 1
  %call = call i32 @decode64_one(i32* %7, i8 zeroext %9)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end
  %10 = load i8*, i8** %src, align 4
  %incdec.ptr = getelementptr i8, i8* %10, i32 1
  store i8* %incdec.ptr, i8** %src, align 4
  %11 = load i32*, i32** %r_p.addr, align 4
  %12 = load i8*, i8** %src, align 4
  %call13 = call i8* @decode64_uint32(i32* %11, i32 30, i8* %12)
  store i8* %call13, i8** %src, align 4
  %13 = load i8*, i8** %src, align 4
  %tobool14 = icmp ne i8* %13, null
  br i1 %tobool14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end12
  store i8* null, i8** %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.end12
  %14 = load i32*, i32** %p_p.addr, align 4
  %15 = load i8*, i8** %src, align 4
  %call17 = call i8* @decode64_uint32(i32* %14, i32 30, i8* %15)
  store i8* %call17, i8** %src, align 4
  %16 = load i8*, i8** %src, align 4
  %tobool18 = icmp ne i8* %16, null
  br i1 %tobool18, label %if.end20, label %if.then19

if.then19:                                        ; preds = %if.end16
  store i8* null, i8** %retval, align 4
  br label %return

if.end20:                                         ; preds = %if.end16
  %17 = load i8*, i8** %src, align 4
  store i8* %17, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.then19, %if.then15, %if.then11, %if.then
  %18 = load i8*, i8** %retval, align 4
  ret i8* %18
}

; Function Attrs: noinline nounwind optnone
define internal i32 @decode64_one(i32* %dst, i8 zeroext %src) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i32*, align 4
  %src.addr = alloca i8, align 1
  %ptr = alloca i8*, align 4
  store i32* %dst, i32** %dst.addr, align 4
  store i8 %src, i8* %src.addr, align 1
  %0 = load i8, i8* %src.addr, align 1
  %conv = zext i8 %0 to i32
  %call = call i8* @strchr(i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str, i32 0, i32 0), i32 %conv)
  store i8* %call, i8** %ptr, align 4
  %1 = load i8*, i8** %ptr, align 4
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %ptr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, ptrtoint ([65 x i8]* @.str to i32)
  %3 = load i32*, i32** %dst.addr, align 4
  store i32 %sub.ptr.sub, i32* %3, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32*, i32** %dst.addr, align 4
  store i32 0, i32* %4, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define internal i8* @decode64_uint32(i32* %dst, i32 %dstbits, i8* %src) #0 {
entry:
  %retval = alloca i8*, align 4
  %dst.addr = alloca i32*, align 4
  %dstbits.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %bit = alloca i32, align 4
  %value = alloca i32, align 4
  %one = alloca i32, align 4
  store i32* %dst, i32** %dst.addr, align 4
  store i32 %dstbits, i32* %dstbits.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  store i32 0, i32* %value, align 4
  store i32 0, i32* %bit, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %bit, align 4
  %1 = load i32, i32* %dstbits.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %src.addr, align 4
  %3 = load i8, i8* %2, align 1
  %call = call i32 @decode64_one(i32* %one, i8 zeroext %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32*, i32** %dst.addr, align 4
  store i32 0, i32* %4, align 4
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  %5 = load i8*, i8** %src.addr, align 4
  %incdec.ptr = getelementptr i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %src.addr, align 4
  %6 = load i32, i32* %one, align 4
  %7 = load i32, i32* %bit, align 4
  %shl = shl i32 %6, %7
  %8 = load i32, i32* %value, align 4
  %or = or i32 %8, %shl
  store i32 %or, i32* %value, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %bit, align 4
  %add = add i32 %9, 6
  store i32 %add, i32* %bit, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load i32, i32* %value, align 4
  %11 = load i32*, i32** %dst.addr, align 4
  store i32 %10, i32* %11, align 4
  %12 = load i8*, i8** %src.addr, align 4
  store i8* %12, i8** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %13 = load i8*, i8** %retval, align 4
  ret i8* %13
}

; Function Attrs: noinline nounwind optnone
define hidden i8* @escrypt_r(%struct.escrypt_region_t* %local, i8* %passwd, i32 %passwdlen, i8* %setting, i8* %buf, i32 %buflen) #0 {
entry:
  %retval = alloca i8*, align 4
  %local.addr = alloca %struct.escrypt_region_t*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i32, align 4
  %setting.addr = alloca i8*, align 4
  %buf.addr = alloca i8*, align 4
  %buflen.addr = alloca i32, align 4
  %hash = alloca [32 x i8], align 16
  %escrypt_kdf = alloca i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)*, align 4
  %src = alloca i8*, align 4
  %salt = alloca i8*, align 4
  %dst = alloca i8*, align 4
  %prefixlen = alloca i32, align 4
  %saltlen = alloca i32, align 4
  %need = alloca i32, align 4
  %N = alloca i64, align 8
  %N_log2 = alloca i32, align 4
  %r = alloca i32, align 4
  %p = alloca i32, align 4
  store %struct.escrypt_region_t* %local, %struct.escrypt_region_t** %local.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i32 %passwdlen, i32* %passwdlen.addr, align 4
  store i8* %setting, i8** %setting.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %buflen, i32* %buflen.addr, align 4
  %0 = load i8*, i8** %setting.addr, align 4
  %call = call i8* @escrypt_parse_setting(i8* %0, i32* %N_log2, i32* %r, i32* %p)
  store i8* %call, i8** %src, align 4
  %1 = load i8*, i8** %src, align 4
  %tobool = icmp ne i8* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %N_log2, align 4
  %sh_prom = zext i32 %2 to i64
  %shl = shl i64 1, %sh_prom
  store i64 %shl, i64* %N, align 8
  %3 = load i8*, i8** %src, align 4
  %4 = load i8*, i8** %setting.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %prefixlen, align 4
  %5 = load i8*, i8** %src, align 4
  store i8* %5, i8** %salt, align 4
  %6 = load i8*, i8** %salt, align 4
  %call1 = call i8* @strrchr(i8* %6, i32 36)
  store i8* %call1, i8** %src, align 4
  %7 = load i8*, i8** %src, align 4
  %tobool2 = icmp ne i8* %7, null
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %8 = load i8*, i8** %src, align 4
  %9 = load i8*, i8** %salt, align 4
  %sub.ptr.lhs.cast4 = ptrtoint i8* %8 to i32
  %sub.ptr.rhs.cast5 = ptrtoint i8* %9 to i32
  %sub.ptr.sub6 = sub i32 %sub.ptr.lhs.cast4, %sub.ptr.rhs.cast5
  store i32 %sub.ptr.sub6, i32* %saltlen, align 4
  br label %if.end8

if.else:                                          ; preds = %if.end
  %10 = load i8*, i8** %salt, align 4
  %call7 = call i32 @strlen(i8* %10)
  store i32 %call7, i32* %saltlen, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then3
  %11 = load i32, i32* %prefixlen, align 4
  %12 = load i32, i32* %saltlen, align 4
  %add = add i32 %11, %12
  %add9 = add i32 %add, 1
  %add10 = add i32 %add9, 43
  %add11 = add i32 %add10, 1
  store i32 %add11, i32* %need, align 4
  %13 = load i32, i32* %need, align 4
  %14 = load i32, i32* %buflen.addr, align 4
  %cmp = icmp ugt i32 %13, %14
  br i1 %cmp, label %if.then13, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end8
  %15 = load i32, i32* %need, align 4
  %16 = load i32, i32* %saltlen, align 4
  %cmp12 = icmp ult i32 %15, %16
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %lor.lhs.false, %if.end8
  store i8* null, i8** %retval, align 4
  br label %return

if.end14:                                         ; preds = %lor.lhs.false
  store i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)* @escrypt_kdf_nosse, i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)** %escrypt_kdf, align 4
  %17 = load i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)*, i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)** %escrypt_kdf, align 4
  %18 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %19 = load i8*, i8** %passwd.addr, align 4
  %20 = load i32, i32* %passwdlen.addr, align 4
  %21 = load i8*, i8** %salt, align 4
  %22 = load i32, i32* %saltlen, align 4
  %23 = load i64, i64* %N, align 8
  %24 = load i32, i32* %r, align 4
  %25 = load i32, i32* %p, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %hash, i32 0, i32 0
  %call15 = call i32 %17(%struct.escrypt_region_t* %18, i8* %19, i32 %20, i8* %21, i32 %22, i64 %23, i32 %24, i32 %25, i8* %arraydecay, i32 32)
  %tobool16 = icmp ne i32 %call15, 0
  br i1 %tobool16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end14
  store i8* null, i8** %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end14
  %26 = load i8*, i8** %buf.addr, align 4
  store i8* %26, i8** %dst, align 4
  %27 = load i8*, i8** %dst, align 4
  %28 = load i8*, i8** %setting.addr, align 4
  %29 = load i32, i32* %prefixlen, align 4
  %30 = load i32, i32* %saltlen, align 4
  %add19 = add i32 %29, %30
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %27, i8* align 1 %28, i32 %add19, i1 false)
  %31 = load i32, i32* %prefixlen, align 4
  %32 = load i32, i32* %saltlen, align 4
  %add20 = add i32 %31, %32
  %33 = load i8*, i8** %dst, align 4
  %add.ptr = getelementptr i8, i8* %33, i32 %add20
  store i8* %add.ptr, i8** %dst, align 4
  %34 = load i8*, i8** %dst, align 4
  %incdec.ptr = getelementptr i8, i8* %34, i32 1
  store i8* %incdec.ptr, i8** %dst, align 4
  store i8 36, i8* %34, align 1
  %35 = load i8*, i8** %dst, align 4
  %36 = load i32, i32* %buflen.addr, align 4
  %37 = load i8*, i8** %dst, align 4
  %38 = load i8*, i8** %buf.addr, align 4
  %sub.ptr.lhs.cast21 = ptrtoint i8* %37 to i32
  %sub.ptr.rhs.cast22 = ptrtoint i8* %38 to i32
  %sub.ptr.sub23 = sub i32 %sub.ptr.lhs.cast21, %sub.ptr.rhs.cast22
  %sub = sub i32 %36, %sub.ptr.sub23
  %arraydecay24 = getelementptr inbounds [32 x i8], [32 x i8]* %hash, i32 0, i32 0
  %call25 = call i8* @encode64(i8* %35, i32 %sub, i8* %arraydecay24, i32 32)
  store i8* %call25, i8** %dst, align 4
  %arraydecay26 = getelementptr inbounds [32 x i8], [32 x i8]* %hash, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay26, i32 32)
  %39 = load i8*, i8** %dst, align 4
  %tobool27 = icmp ne i8* %39, null
  br i1 %tobool27, label %lor.lhs.false28, label %if.then31

lor.lhs.false28:                                  ; preds = %if.end18
  %40 = load i8*, i8** %dst, align 4
  %41 = load i8*, i8** %buf.addr, align 4
  %42 = load i32, i32* %buflen.addr, align 4
  %add.ptr29 = getelementptr i8, i8* %41, i32 %42
  %cmp30 = icmp uge i8* %40, %add.ptr29
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %lor.lhs.false28, %if.end18
  store i8* null, i8** %retval, align 4
  br label %return

if.end32:                                         ; preds = %lor.lhs.false28
  %43 = load i8*, i8** %dst, align 4
  store i8 0, i8* %43, align 1
  %44 = load i8*, i8** %buf.addr, align 4
  store i8* %44, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end32, %if.then31, %if.then17, %if.then13, %if.then
  %45 = load i8*, i8** %retval, align 4
  ret i8* %45
}

declare i8* @strrchr(i8*, i32) #1

declare i32 @strlen(i8*) #1

declare i32 @escrypt_kdf_nosse(%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal i8* @encode64(i8* %dst, i32 %dstlen, i8* %src, i32 %srclen) #0 {
entry:
  %retval = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %dstlen.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %srclen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dnext = alloca i8*, align 4
  %value = alloca i32, align 4
  %bits = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %dstlen, i32* %dstlen.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  store i32 %srclen, i32* %srclen.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %srclen.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %value, align 4
  store i32 0, i32* %bits, align 4
  br label %do.body

do.body:                                          ; preds = %land.end, %for.body
  %2 = load i8*, i8** %src.addr, align 4
  %3 = load i32, i32* %i, align 4
  %inc = add i32 %3, 1
  store i32 %inc, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i32, i32* %bits, align 4
  %shl = shl i32 %conv, %5
  %6 = load i32, i32* %value, align 4
  %or = or i32 %6, %shl
  store i32 %or, i32* %value, align 4
  %7 = load i32, i32* %bits, align 4
  %add = add i32 %7, 8
  store i32 %add, i32* %bits, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %8 = load i32, i32* %bits, align 4
  %cmp1 = icmp ult i32 %8, 24
  br i1 %cmp1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %srclen.addr, align 4
  %cmp3 = icmp ult i32 %9, %10
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %11 = phi i1 [ false, %do.cond ], [ %cmp3, %land.rhs ]
  br i1 %11, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %12 = load i8*, i8** %dst.addr, align 4
  %13 = load i32, i32* %dstlen.addr, align 4
  %14 = load i32, i32* %value, align 4
  %15 = load i32, i32* %bits, align 4
  %call = call i8* @encode64_uint32(i8* %12, i32 %13, i32 %14, i32 %15)
  store i8* %call, i8** %dnext, align 4
  %16 = load i8*, i8** %dnext, align 4
  %tobool = icmp ne i8* %16, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %do.end
  %17 = load i8*, i8** %dnext, align 4
  %18 = load i8*, i8** %dst.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %17 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %18 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %19 = load i32, i32* %dstlen.addr, align 4
  %sub = sub i32 %19, %sub.ptr.sub
  store i32 %sub, i32* %dstlen.addr, align 4
  %20 = load i8*, i8** %dnext, align 4
  store i8* %20, i8** %dst.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load i8*, i8** %dst.addr, align 4
  store i8* %21, i8** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %22 = load i8*, i8** %retval, align 4
  ret i8* %22
}

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i8* @escrypt_gensalt_r(i32 %N_log2, i32 %r, i32 %p, i8* %src, i32 %srclen, i8* %buf, i32 %buflen) #0 {
entry:
  %retval = alloca i8*, align 4
  %N_log2.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %p.addr = alloca i32, align 4
  %src.addr = alloca i8*, align 4
  %srclen.addr = alloca i32, align 4
  %buf.addr = alloca i8*, align 4
  %buflen.addr = alloca i32, align 4
  %dst = alloca i8*, align 4
  %prefixlen = alloca i32, align 4
  %saltlen = alloca i32, align 4
  %need = alloca i32, align 4
  store i32 %N_log2, i32* %N_log2.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  store i32 %srclen, i32* %srclen.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %buflen, i32* %buflen.addr, align 4
  store i32 14, i32* %prefixlen, align 4
  %0 = load i32, i32* %srclen.addr, align 4
  %mul = mul i32 %0, 8
  %add = add i32 %mul, 5
  %div = udiv i32 %add, 6
  store i32 %div, i32* %saltlen, align 4
  %1 = load i32, i32* %prefixlen, align 4
  %2 = load i32, i32* %saltlen, align 4
  %add1 = add i32 %1, %2
  %add2 = add i32 %add1, 1
  store i32 %add2, i32* %need, align 4
  %3 = load i32, i32* %need, align 4
  %4 = load i32, i32* %buflen.addr, align 4
  %cmp = icmp ugt i32 %3, %4
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %5 = load i32, i32* %need, align 4
  %6 = load i32, i32* %saltlen, align 4
  %cmp3 = icmp ult i32 %5, %6
  br i1 %cmp3, label %if.then, label %lor.lhs.false4

lor.lhs.false4:                                   ; preds = %lor.lhs.false
  %7 = load i32, i32* %saltlen, align 4
  %8 = load i32, i32* %srclen.addr, align 4
  %cmp5 = icmp ult i32 %7, %8
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false4, %lor.lhs.false, %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false4
  %9 = load i32, i32* %N_log2.addr, align 4
  %cmp6 = icmp ugt i32 %9, 63
  br i1 %cmp6, label %if.then12, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end
  %10 = load i32, i32* %r.addr, align 4
  %conv = zext i32 %10 to i64
  %11 = load i32, i32* %p.addr, align 4
  %conv8 = zext i32 %11 to i64
  %mul9 = mul i64 %conv, %conv8
  %cmp10 = icmp uge i64 %mul9, 1073741824
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %lor.lhs.false7, %if.end
  store i8* null, i8** %retval, align 4
  br label %return

if.end13:                                         ; preds = %lor.lhs.false7
  %12 = load i8*, i8** %buf.addr, align 4
  store i8* %12, i8** %dst, align 4
  %13 = load i8*, i8** %dst, align 4
  %incdec.ptr = getelementptr i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %dst, align 4
  store i8 36, i8* %13, align 1
  %14 = load i8*, i8** %dst, align 4
  %incdec.ptr14 = getelementptr i8, i8* %14, i32 1
  store i8* %incdec.ptr14, i8** %dst, align 4
  store i8 55, i8* %14, align 1
  %15 = load i8*, i8** %dst, align 4
  %incdec.ptr15 = getelementptr i8, i8* %15, i32 1
  store i8* %incdec.ptr15, i8** %dst, align 4
  store i8 36, i8* %15, align 1
  %16 = load i32, i32* %N_log2.addr, align 4
  %arrayidx = getelementptr i8, i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str, i32 0, i32 0), i32 %16
  %17 = load i8, i8* %arrayidx, align 1
  %18 = load i8*, i8** %dst, align 4
  %incdec.ptr16 = getelementptr i8, i8* %18, i32 1
  store i8* %incdec.ptr16, i8** %dst, align 4
  store i8 %17, i8* %18, align 1
  %19 = load i8*, i8** %dst, align 4
  %20 = load i32, i32* %buflen.addr, align 4
  %21 = load i8*, i8** %dst, align 4
  %22 = load i8*, i8** %buf.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %22 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub = sub i32 %20, %sub.ptr.sub
  %23 = load i32, i32* %r.addr, align 4
  %call = call i8* @encode64_uint32(i8* %19, i32 %sub, i32 %23, i32 30)
  store i8* %call, i8** %dst, align 4
  %24 = load i8*, i8** %dst, align 4
  %tobool = icmp ne i8* %24, null
  br i1 %tobool, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.end13
  store i8* null, i8** %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end13
  %25 = load i8*, i8** %dst, align 4
  %26 = load i32, i32* %buflen.addr, align 4
  %27 = load i8*, i8** %dst, align 4
  %28 = load i8*, i8** %buf.addr, align 4
  %sub.ptr.lhs.cast19 = ptrtoint i8* %27 to i32
  %sub.ptr.rhs.cast20 = ptrtoint i8* %28 to i32
  %sub.ptr.sub21 = sub i32 %sub.ptr.lhs.cast19, %sub.ptr.rhs.cast20
  %sub22 = sub i32 %26, %sub.ptr.sub21
  %29 = load i32, i32* %p.addr, align 4
  %call23 = call i8* @encode64_uint32(i8* %25, i32 %sub22, i32 %29, i32 30)
  store i8* %call23, i8** %dst, align 4
  %30 = load i8*, i8** %dst, align 4
  %tobool24 = icmp ne i8* %30, null
  br i1 %tobool24, label %if.end26, label %if.then25

if.then25:                                        ; preds = %if.end18
  store i8* null, i8** %retval, align 4
  br label %return

if.end26:                                         ; preds = %if.end18
  %31 = load i8*, i8** %dst, align 4
  %32 = load i32, i32* %buflen.addr, align 4
  %33 = load i8*, i8** %dst, align 4
  %34 = load i8*, i8** %buf.addr, align 4
  %sub.ptr.lhs.cast27 = ptrtoint i8* %33 to i32
  %sub.ptr.rhs.cast28 = ptrtoint i8* %34 to i32
  %sub.ptr.sub29 = sub i32 %sub.ptr.lhs.cast27, %sub.ptr.rhs.cast28
  %sub30 = sub i32 %32, %sub.ptr.sub29
  %35 = load i8*, i8** %src.addr, align 4
  %36 = load i32, i32* %srclen.addr, align 4
  %call31 = call i8* @encode64(i8* %31, i32 %sub30, i8* %35, i32 %36)
  store i8* %call31, i8** %dst, align 4
  %37 = load i8*, i8** %dst, align 4
  %tobool32 = icmp ne i8* %37, null
  br i1 %tobool32, label %lor.lhs.false33, label %if.then36

lor.lhs.false33:                                  ; preds = %if.end26
  %38 = load i8*, i8** %dst, align 4
  %39 = load i8*, i8** %buf.addr, align 4
  %40 = load i32, i32* %buflen.addr, align 4
  %add.ptr = getelementptr i8, i8* %39, i32 %40
  %cmp34 = icmp uge i8* %38, %add.ptr
  br i1 %cmp34, label %if.then36, label %if.end37

if.then36:                                        ; preds = %lor.lhs.false33, %if.end26
  store i8* null, i8** %retval, align 4
  br label %return

if.end37:                                         ; preds = %lor.lhs.false33
  %41 = load i8*, i8** %dst, align 4
  store i8 0, i8* %41, align 1
  %42 = load i8*, i8** %buf.addr, align 4
  store i8* %42, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end37, %if.then36, %if.then25, %if.then17, %if.then12, %if.then
  %43 = load i8*, i8** %retval, align 4
  ret i8* %43
}

; Function Attrs: noinline nounwind optnone
define internal i8* @encode64_uint32(i8* %dst, i32 %dstlen, i32 %src, i32 %srcbits) #0 {
entry:
  %retval = alloca i8*, align 4
  %dst.addr = alloca i8*, align 4
  %dstlen.addr = alloca i32, align 4
  %src.addr = alloca i32, align 4
  %srcbits.addr = alloca i32, align 4
  %bit = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %dstlen, i32* %dstlen.addr, align 4
  store i32 %src, i32* %src.addr, align 4
  store i32 %srcbits, i32* %srcbits.addr, align 4
  store i32 0, i32* %bit, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %bit, align 4
  %1 = load i32, i32* %srcbits.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %dstlen.addr, align 4
  %cmp1 = icmp ult i32 %2, 1
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  %3 = load i32, i32* %src.addr, align 4
  %and = and i32 %3, 63
  %arrayidx = getelementptr i8, i8* getelementptr inbounds ([65 x i8], [65 x i8]* @.str, i32 0, i32 0), i32 %and
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i8*, i8** %dst.addr, align 4
  %incdec.ptr = getelementptr i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %dst.addr, align 4
  store i8 %4, i8* %5, align 1
  %6 = load i32, i32* %dstlen.addr, align 4
  %dec = add i32 %6, -1
  store i32 %dec, i32* %dstlen.addr, align 4
  %7 = load i32, i32* %src.addr, align 4
  %shr = lshr i32 %7, 6
  store i32 %shr, i32* %src.addr, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %bit, align 4
  %add = add i32 %8, 6
  store i32 %add, i32* %bit, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i8*, i8** %dst.addr, align 4
  store i8* %9, i8** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %10 = load i8*, i8** %retval, align 4
  ret i8* %10
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_pwhash_scryptsalsa208sha256_ll(i8* nonnull %passwd, i32 %passwdlen, i8* nonnull %salt, i32 %saltlen, i64 %N, i32 %r, i32 %p, i8* nonnull %buf, i32 %buflen) #0 {
entry:
  %retval = alloca i32, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %N.addr = alloca i64, align 8
  %r.addr = alloca i32, align 4
  %p.addr = alloca i32, align 4
  %buf.addr = alloca i8*, align 4
  %buflen.addr = alloca i32, align 4
  %escrypt_kdf = alloca i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)*, align 4
  %local = alloca %struct.escrypt_region_t, align 4
  %retval1 = alloca i32, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i32 %passwdlen, i32* %passwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i64 %N, i64* %N.addr, align 8
  store i32 %r, i32* %r.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %buflen, i32* %buflen.addr, align 4
  %call = call i32 @escrypt_init_local(%struct.escrypt_region_t* %local)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)* @escrypt_kdf_nosse, i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)** %escrypt_kdf, align 4
  %0 = load i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)*, i32 (%struct.escrypt_region_t*, i8*, i32, i8*, i32, i64, i32, i32, i8*, i32)** %escrypt_kdf, align 4
  %1 = load i8*, i8** %passwd.addr, align 4
  %2 = load i32, i32* %passwdlen.addr, align 4
  %3 = load i8*, i8** %salt.addr, align 4
  %4 = load i32, i32* %saltlen.addr, align 4
  %5 = load i64, i64* %N.addr, align 8
  %6 = load i32, i32* %r.addr, align 4
  %7 = load i32, i32* %p.addr, align 4
  %8 = load i8*, i8** %buf.addr, align 4
  %9 = load i32, i32* %buflen.addr, align 4
  %call2 = call i32 %0(%struct.escrypt_region_t* %local, i8* %1, i32 %2, i8* %3, i32 %4, i64 %5, i32 %6, i32 %7, i8* %8, i32 %9)
  store i32 %call2, i32* %retval1, align 4
  %call3 = call i32 @escrypt_free_local(%struct.escrypt_region_t* %local)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %10 = load i32, i32* %retval1, align 4
  store i32 %10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

declare i32 @escrypt_init_local(%struct.escrypt_region_t*) #1

declare i32 @escrypt_free_local(%struct.escrypt_region_t*) #1

declare i8* @strchr(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
