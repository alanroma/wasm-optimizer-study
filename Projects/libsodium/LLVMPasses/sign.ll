; ModuleID = 'crypto_sign/ed25519/ref10/sign.c'
source_filename = "crypto_sign/ed25519/ref10/sign.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }
%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

@_crypto_sign_ed25519_ref10_hinit.DOM2PREFIX = internal constant [34 x i8] c"SigEd25519 no Ed25519 collisions\01\00", align 16

; Function Attrs: noinline nounwind optnone
define hidden void @_crypto_sign_ed25519_ref10_hinit(%struct.crypto_hash_sha512_state* %hs, i32 %prehashed) #0 {
entry:
  %hs.addr = alloca %struct.crypto_hash_sha512_state*, align 4
  %prehashed.addr = alloca i32, align 4
  store %struct.crypto_hash_sha512_state* %hs, %struct.crypto_hash_sha512_state** %hs.addr, align 4
  store i32 %prehashed, i32* %prehashed.addr, align 4
  %0 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %hs.addr, align 4
  %call = call i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state* %0)
  %1 = load i32, i32* %prehashed.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.crypto_hash_sha512_state*, %struct.crypto_hash_sha512_state** %hs.addr, align 4
  %call1 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %2, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_crypto_sign_ed25519_ref10_hinit.DOM2PREFIX, i32 0, i32 0), i64 34)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare i32 @crypto_hash_sha512_init(%struct.crypto_hash_sha512_state*) #1

declare i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @_crypto_sign_ed25519_detached(i8* %sig, i64* %siglen_p, i8* %m, i64 %mlen, i8* %sk, i32 %prehashed) #0 {
entry:
  %sig.addr = alloca i8*, align 4
  %siglen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  %prehashed.addr = alloca i32, align 4
  %hs = alloca %struct.crypto_hash_sha512_state, align 8
  %az = alloca [64 x i8], align 16
  %nonce = alloca [64 x i8], align 16
  %hram = alloca [64 x i8], align 16
  %R = alloca %struct.ge25519_p3, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i64* %siglen_p, i64** %siglen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  store i32 %prehashed, i32* %prehashed.addr, align 4
  %0 = load i32, i32* %prehashed.addr, align 4
  call void @_crypto_sign_ed25519_ref10_hinit(%struct.crypto_hash_sha512_state* %hs, i32 %0)
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %az, i32 0, i32 0
  %1 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_hash_sha512(i8* %arraydecay, i8* %1, i64 32)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %az, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay1, i32 32
  %call2 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %add.ptr, i64 32)
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %call3 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %2, i64 %3)
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  %call5 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay4)
  %4 = load i8*, i8** %sig.addr, align 4
  %add.ptr6 = getelementptr i8, i8* %4, i32 32
  %5 = load i8*, i8** %sk.addr, align 4
  %add.ptr7 = getelementptr i8, i8* %5, i32 32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr6, i8* align 1 %add.ptr7, i32 32, i1 false)
  %arraydecay8 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay8)
  %arraydecay9 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @ge25519_scalarmult_base(%struct.ge25519_p3* %R, i8* %arraydecay9)
  %6 = load i8*, i8** %sig.addr, align 4
  call void @ge25519_p3_tobytes(i8* %6, %struct.ge25519_p3* %R)
  %7 = load i32, i32* %prehashed.addr, align 4
  call void @_crypto_sign_ed25519_ref10_hinit(%struct.crypto_hash_sha512_state* %hs, i32 %7)
  %8 = load i8*, i8** %sig.addr, align 4
  %call10 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %8, i64 64)
  %9 = load i8*, i8** %m.addr, align 4
  %10 = load i64, i64* %mlen.addr, align 8
  %call11 = call i32 @crypto_hash_sha512_update(%struct.crypto_hash_sha512_state* %hs, i8* %9, i64 %10)
  %arraydecay12 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  %call13 = call i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state* %hs, i8* %arraydecay12)
  %arraydecay14 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay14)
  %arraydecay15 = getelementptr inbounds [64 x i8], [64 x i8]* %az, i32 0, i32 0
  call void @_crypto_sign_ed25519_clamp(i8* %arraydecay15)
  %11 = load i8*, i8** %sig.addr, align 4
  %add.ptr16 = getelementptr i8, i8* %11, i32 32
  %arraydecay17 = getelementptr inbounds [64 x i8], [64 x i8]* %hram, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [64 x i8], [64 x i8]* %az, i32 0, i32 0
  %arraydecay19 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @sc25519_muladd(i8* %add.ptr16, i8* %arraydecay17, i8* %arraydecay18, i8* %arraydecay19)
  %arraydecay20 = getelementptr inbounds [64 x i8], [64 x i8]* %az, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay20, i32 64)
  %arraydecay21 = getelementptr inbounds [64 x i8], [64 x i8]* %nonce, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay21, i32 64)
  %12 = load i64*, i64** %siglen_p.addr, align 4
  %cmp = icmp ne i64* %12, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %13 = load i64*, i64** %siglen_p.addr, align 4
  store i64 64, i64* %13, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret i32 0
}

declare i32 @crypto_hash_sha512(i8*, i8*, i64) #1

declare i32 @crypto_hash_sha512_final(%struct.crypto_hash_sha512_state*, i8*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

declare void @sc25519_reduce(i8*) #1

declare void @ge25519_scalarmult_base(%struct.ge25519_p3*, i8*) #1

declare void @ge25519_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define internal void @_crypto_sign_ed25519_clamp(i8* %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  %arrayidx = getelementptr i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 248
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1
  %2 = load i8*, i8** %k.addr, align 4
  %arrayidx2 = getelementptr i8, i8* %2, i32 31
  %3 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %3 to i32
  %and4 = and i32 %conv3, 127
  %conv5 = trunc i32 %and4 to i8
  store i8 %conv5, i8* %arrayidx2, align 1
  %4 = load i8*, i8** %k.addr, align 4
  %arrayidx6 = getelementptr i8, i8* %4, i32 31
  %5 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %5 to i32
  %or = or i32 %conv7, 64
  %conv8 = trunc i32 %or to i8
  store i8 %conv8, i8* %arrayidx6, align 1
  ret void
}

declare void @sc25519_muladd(i8*, i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519_detached(i8* nonnull %sig, i64* %siglen_p, i8* %m, i64 %mlen, i8* nonnull %sk) #0 {
entry:
  %sig.addr = alloca i8*, align 4
  %siglen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i64* %siglen_p, i64** %siglen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sig.addr, align 4
  %1 = load i64*, i64** %siglen_p.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @_crypto_sign_ed25519_detached(i8* %0, i64* %1, i8* %2, i64 %3, i8* %4, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_ed25519(i8* nonnull %sm, i64* %smlen_p, i8* %m, i64 %mlen, i8* nonnull %sk) #0 {
entry:
  %retval = alloca i32, align 4
  %sm.addr = alloca i8*, align 4
  %smlen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  %siglen = alloca i64, align 8
  store i8* %sm, i8** %sm.addr, align 4
  store i64* %smlen_p, i64** %smlen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sm.addr, align 4
  %add.ptr = getelementptr i8, i8* %0, i32 64
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %conv = trunc i64 %2 to i32
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %conv, i1 false)
  %3 = load i8*, i8** %sm.addr, align 4
  %4 = load i8*, i8** %sm.addr, align 4
  %add.ptr1 = getelementptr i8, i8* %4, i32 64
  %5 = load i64, i64* %mlen.addr, align 8
  %6 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_detached(i8* %3, i64* %siglen, i8* %add.ptr1, i64 %5, i8* %6)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %7 = load i64, i64* %siglen, align 8
  %cmp3 = icmp ne i64 %7, 64
  br i1 %cmp3, label %if.then, label %if.end9

if.then:                                          ; preds = %lor.lhs.false, %entry
  %8 = load i64*, i64** %smlen_p.addr, align 4
  %cmp5 = icmp ne i64* %8, null
  br i1 %cmp5, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %9 = load i64*, i64** %smlen_p.addr, align 4
  store i64 0, i64* %9, align 8
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %10 = load i8*, i8** %sm.addr, align 4
  %11 = load i64, i64* %mlen.addr, align 8
  %add = add i64 %11, 64
  %conv8 = trunc i64 %add to i32
  call void @llvm.memset.p0i8.i32(i8* align 1 %10, i8 0, i32 %conv8, i1 false)
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %lor.lhs.false
  %12 = load i64*, i64** %smlen_p.addr, align 4
  %cmp10 = icmp ne i64* %12, null
  br i1 %cmp10, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end9
  %13 = load i64, i64* %mlen.addr, align 8
  %14 = load i64, i64* %siglen, align 8
  %add13 = add i64 %13, %14
  %15 = load i64*, i64** %smlen_p.addr, align 4
  store i64 %add13, i64* %15, align 8
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end14, %if.end
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
