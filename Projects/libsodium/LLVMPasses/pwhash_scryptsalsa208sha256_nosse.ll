; ModuleID = 'crypto_pwhash/scryptsalsa208sha256/nosse/pwhash_scryptsalsa208sha256_nosse.c'
source_filename = "crypto_pwhash/scryptsalsa208sha256/nosse/pwhash_scryptsalsa208sha256_nosse.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.escrypt_region_t = type { i8*, i8*, i32 }
%union.escrypt_block_t = type { [8 x i64] }

; Function Attrs: noinline nounwind optnone
define hidden i32 @escrypt_kdf_nosse(%struct.escrypt_region_t* %local, i8* %passwd, i32 %passwdlen, i8* %salt, i32 %saltlen, i64 %N, i32 %_r, i32 %_p, i8* %buf, i32 %buflen) #0 {
entry:
  %retval = alloca i32, align 4
  %local.addr = alloca %struct.escrypt_region_t*, align 4
  %passwd.addr = alloca i8*, align 4
  %passwdlen.addr = alloca i32, align 4
  %salt.addr = alloca i8*, align 4
  %saltlen.addr = alloca i32, align 4
  %N.addr = alloca i64, align 8
  %_r.addr = alloca i32, align 4
  %_p.addr = alloca i32, align 4
  %buf.addr = alloca i8*, align 4
  %buflen.addr = alloca i32, align 4
  %B_size = alloca i32, align 4
  %V_size = alloca i32, align 4
  %XY_size = alloca i32, align 4
  %need = alloca i32, align 4
  %B = alloca i8*, align 4
  %V = alloca i32*, align 4
  %XY = alloca i32*, align 4
  %r = alloca i32, align 4
  %p = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.escrypt_region_t* %local, %struct.escrypt_region_t** %local.addr, align 4
  store i8* %passwd, i8** %passwd.addr, align 4
  store i32 %passwdlen, i32* %passwdlen.addr, align 4
  store i8* %salt, i8** %salt.addr, align 4
  store i32 %saltlen, i32* %saltlen.addr, align 4
  store i64 %N, i64* %N.addr, align 8
  store i32 %_r, i32* %_r.addr, align 4
  store i32 %_p, i32* %_p.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %buflen, i32* %buflen.addr, align 4
  %0 = load i32, i32* %_r.addr, align 4
  store i32 %0, i32* %r, align 4
  %1 = load i32, i32* %_p.addr, align 4
  store i32 %1, i32* %p, align 4
  %2 = load i32, i32* %r, align 4
  %conv = zext i32 %2 to i64
  %3 = load i32, i32* %p, align 4
  %conv1 = zext i32 %3 to i64
  %mul = mul i64 %conv, %conv1
  %cmp = icmp uge i64 %mul, 1073741824
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32* @__errno_location()
  store i32 22, i32* %call, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i64, i64* %N.addr, align 8
  %cmp3 = icmp ugt i64 %4, 4294967295
  br i1 %cmp3, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %call6 = call i32* @__errno_location()
  store i32 22, i32* %call6, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end
  %5 = load i64, i64* %N.addr, align 8
  %6 = load i64, i64* %N.addr, align 8
  %sub = sub i64 %6, 1
  %and = and i64 %5, %sub
  %cmp8 = icmp ne i64 %and, 0
  br i1 %cmp8, label %if.then12, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end7
  %7 = load i64, i64* %N.addr, align 8
  %cmp10 = icmp ult i64 %7, 2
  br i1 %cmp10, label %if.then12, label %if.end14

if.then12:                                        ; preds = %lor.lhs.false, %if.end7
  %call13 = call i32* @__errno_location()
  store i32 28, i32* %call13, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %lor.lhs.false
  %8 = load i32, i32* %r, align 4
  %cmp15 = icmp eq i32 %8, 0
  br i1 %cmp15, label %if.then20, label %lor.lhs.false17

lor.lhs.false17:                                  ; preds = %if.end14
  %9 = load i32, i32* %p, align 4
  %cmp18 = icmp eq i32 %9, 0
  br i1 %cmp18, label %if.then20, label %if.end22

if.then20:                                        ; preds = %lor.lhs.false17, %if.end14
  %call21 = call i32* @__errno_location()
  store i32 28, i32* %call21, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end22:                                         ; preds = %lor.lhs.false17
  %10 = load i32, i32* %r, align 4
  %11 = load i32, i32* %p, align 4
  %div = udiv i32 33554431, %11
  %cmp23 = icmp ugt i32 %10, %div
  br i1 %cmp23, label %if.then33, label %lor.lhs.false25

lor.lhs.false25:                                  ; preds = %if.end22
  %12 = load i32, i32* %r, align 4
  %cmp26 = icmp ugt i32 %12, 16777215
  br i1 %cmp26, label %if.then33, label %lor.lhs.false28

lor.lhs.false28:                                  ; preds = %lor.lhs.false25
  %13 = load i64, i64* %N.addr, align 8
  %14 = load i32, i32* %r, align 4
  %div29 = udiv i32 33554431, %14
  %conv30 = zext i32 %div29 to i64
  %cmp31 = icmp ugt i64 %13, %conv30
  br i1 %cmp31, label %if.then33, label %if.end35

if.then33:                                        ; preds = %lor.lhs.false28, %lor.lhs.false25, %if.end22
  %call34 = call i32* @__errno_location()
  store i32 48, i32* %call34, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end35:                                         ; preds = %lor.lhs.false28
  %15 = load i32, i32* %r, align 4
  %mul36 = mul i32 128, %15
  %16 = load i32, i32* %p, align 4
  %mul37 = mul i32 %mul36, %16
  store i32 %mul37, i32* %B_size, align 4
  %17 = load i32, i32* %r, align 4
  %mul38 = mul i32 128, %17
  %18 = load i64, i64* %N.addr, align 8
  %conv39 = trunc i64 %18 to i32
  %mul40 = mul i32 %mul38, %conv39
  store i32 %mul40, i32* %V_size, align 4
  %19 = load i32, i32* %B_size, align 4
  %20 = load i32, i32* %V_size, align 4
  %add = add i32 %19, %20
  store i32 %add, i32* %need, align 4
  %21 = load i32, i32* %need, align 4
  %22 = load i32, i32* %V_size, align 4
  %cmp41 = icmp ult i32 %21, %22
  br i1 %cmp41, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.end35
  %call44 = call i32* @__errno_location()
  store i32 48, i32* %call44, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end45:                                         ; preds = %if.end35
  %23 = load i32, i32* %r, align 4
  %mul46 = mul i32 256, %23
  %add47 = add i32 %mul46, 64
  store i32 %add47, i32* %XY_size, align 4
  %24 = load i32, i32* %XY_size, align 4
  %25 = load i32, i32* %need, align 4
  %add48 = add i32 %25, %24
  store i32 %add48, i32* %need, align 4
  %26 = load i32, i32* %need, align 4
  %27 = load i32, i32* %XY_size, align 4
  %cmp49 = icmp ult i32 %26, %27
  br i1 %cmp49, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.end45
  %call52 = call i32* @__errno_location()
  store i32 48, i32* %call52, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end53:                                         ; preds = %if.end45
  %28 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %size = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %28, i32 0, i32 2
  %29 = load i32, i32* %size, align 4
  %30 = load i32, i32* %need, align 4
  %cmp54 = icmp ult i32 %29, %30
  br i1 %cmp54, label %if.then56, label %if.end64

if.then56:                                        ; preds = %if.end53
  %31 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %call57 = call i32 @escrypt_free_region(%struct.escrypt_region_t* %31)
  %tobool = icmp ne i32 %call57, 0
  br i1 %tobool, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.then56
  store i32 -1, i32* %retval, align 4
  br label %return

if.end59:                                         ; preds = %if.then56
  %32 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %33 = load i32, i32* %need, align 4
  %call60 = call i8* @escrypt_alloc_region(%struct.escrypt_region_t* %32, i32 %33)
  %tobool61 = icmp ne i8* %call60, null
  br i1 %tobool61, label %if.end63, label %if.then62

if.then62:                                        ; preds = %if.end59
  store i32 -1, i32* %retval, align 4
  br label %return

if.end63:                                         ; preds = %if.end59
  br label %if.end64

if.end64:                                         ; preds = %if.end63, %if.end53
  %34 = load %struct.escrypt_region_t*, %struct.escrypt_region_t** %local.addr, align 4
  %aligned = getelementptr inbounds %struct.escrypt_region_t, %struct.escrypt_region_t* %34, i32 0, i32 1
  %35 = load i8*, i8** %aligned, align 4
  store i8* %35, i8** %B, align 4
  %36 = load i8*, i8** %B, align 4
  %37 = load i32, i32* %B_size, align 4
  %add.ptr = getelementptr i8, i8* %36, i32 %37
  %38 = bitcast i8* %add.ptr to i32*
  store i32* %38, i32** %V, align 4
  %39 = load i32*, i32** %V, align 4
  %40 = bitcast i32* %39 to i8*
  %41 = load i32, i32* %V_size, align 4
  %add.ptr65 = getelementptr i8, i8* %40, i32 %41
  %42 = bitcast i8* %add.ptr65 to i32*
  store i32* %42, i32** %XY, align 4
  %43 = load i8*, i8** %passwd.addr, align 4
  %44 = load i32, i32* %passwdlen.addr, align 4
  %45 = load i8*, i8** %salt.addr, align 4
  %46 = load i32, i32* %saltlen.addr, align 4
  %47 = load i8*, i8** %B, align 4
  %48 = load i32, i32* %B_size, align 4
  call void @escrypt_PBKDF2_SHA256(i8* %43, i32 %44, i8* %45, i32 %46, i64 1, i8* %47, i32 %48)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end64
  %49 = load i32, i32* %i, align 4
  %50 = load i32, i32* %p, align 4
  %cmp66 = icmp ult i32 %49, %50
  br i1 %cmp66, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %51 = load i8*, i8** %B, align 4
  %52 = load i32, i32* %i, align 4
  %mul68 = mul i32 128, %52
  %53 = load i32, i32* %r, align 4
  %mul69 = mul i32 %mul68, %53
  %arrayidx = getelementptr i8, i8* %51, i32 %mul69
  %54 = load i32, i32* %r, align 4
  %55 = load i64, i64* %N.addr, align 8
  %56 = load i32*, i32** %V, align 4
  %57 = load i32*, i32** %XY, align 4
  call void @smix(i8* %arrayidx, i32 %54, i64 %55, i32* %56, i32* %57)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %58 = load i32, i32* %i, align 4
  %inc = add i32 %58, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %59 = load i8*, i8** %passwd.addr, align 4
  %60 = load i32, i32* %passwdlen.addr, align 4
  %61 = load i8*, i8** %B, align 4
  %62 = load i32, i32* %B_size, align 4
  %63 = load i8*, i8** %buf.addr, align 4
  %64 = load i32, i32* %buflen.addr, align 4
  call void @escrypt_PBKDF2_SHA256(i8* %59, i32 %60, i8* %61, i32 %62, i64 1, i8* %63, i32 %64)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then62, %if.then58, %if.then51, %if.then43, %if.then33, %if.then20, %if.then12, %if.then5, %if.then
  %65 = load i32, i32* %retval, align 4
  ret i32 %65
}

declare i32* @__errno_location() #1

declare i32 @escrypt_free_region(%struct.escrypt_region_t*) #1

declare i8* @escrypt_alloc_region(%struct.escrypt_region_t*, i32) #1

declare void @escrypt_PBKDF2_SHA256(i8*, i32, i8*, i32, i64, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @smix(i8* %B, i32 %r, i64 %N, i32* %V, i32* %XY) #0 {
entry:
  %B.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %N.addr = alloca i64, align 8
  %V.addr = alloca i32*, align 4
  %XY.addr = alloca i32*, align 4
  %X = alloca i32*, align 4
  %Y = alloca i32*, align 4
  %Z = alloca i32*, align 4
  %i = alloca i64, align 8
  %j = alloca i64, align 8
  %k = alloca i32, align 4
  store i8* %B, i8** %B.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  store i64 %N, i64* %N.addr, align 8
  store i32* %V, i32** %V.addr, align 4
  store i32* %XY, i32** %XY.addr, align 4
  %0 = load i32*, i32** %XY.addr, align 4
  store i32* %0, i32** %X, align 4
  %1 = load i32*, i32** %XY.addr, align 4
  %2 = load i32, i32* %r.addr, align 4
  %mul = mul i32 32, %2
  %arrayidx = getelementptr i32, i32* %1, i32 %mul
  store i32* %arrayidx, i32** %Y, align 4
  %3 = load i32*, i32** %XY.addr, align 4
  %4 = load i32, i32* %r.addr, align 4
  %mul1 = mul i32 64, %4
  %arrayidx2 = getelementptr i32, i32* %3, i32 %mul1
  store i32* %arrayidx2, i32** %Z, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %k, align 4
  %6 = load i32, i32* %r.addr, align 4
  %mul3 = mul i32 32, %6
  %cmp = icmp ult i32 %5, %mul3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %B.addr, align 4
  %8 = load i32, i32* %k, align 4
  %mul4 = mul i32 4, %8
  %arrayidx5 = getelementptr i8, i8* %7, i32 %mul4
  %call = call i32 @load32_le(i8* %arrayidx5)
  %9 = load i32*, i32** %X, align 4
  %10 = load i32, i32* %k, align 4
  %arrayidx6 = getelementptr i32, i32* %9, i32 %10
  store i32 %call, i32* %arrayidx6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %k, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i64 0, i64* %i, align 8
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc20, %for.end
  %12 = load i64, i64* %i, align 8
  %13 = load i64, i64* %N.addr, align 8
  %cmp8 = icmp ult i64 %12, %13
  br i1 %cmp8, label %for.body9, label %for.end22

for.body9:                                        ; preds = %for.cond7
  %14 = load i32*, i32** %V.addr, align 4
  %15 = load i64, i64* %i, align 8
  %16 = load i32, i32* %r.addr, align 4
  %mul10 = mul i32 32, %16
  %conv = zext i32 %mul10 to i64
  %mul11 = mul i64 %15, %conv
  %idxprom = trunc i64 %mul11 to i32
  %arrayidx12 = getelementptr i32, i32* %14, i32 %idxprom
  %17 = bitcast i32* %arrayidx12 to %union.escrypt_block_t*
  %18 = load i32*, i32** %X, align 4
  %19 = bitcast i32* %18 to %union.escrypt_block_t*
  %20 = load i32, i32* %r.addr, align 4
  %mul13 = mul i32 128, %20
  call void @blkcpy(%union.escrypt_block_t* %17, %union.escrypt_block_t* %19, i32 %mul13)
  %21 = load i32*, i32** %X, align 4
  %22 = load i32*, i32** %Y, align 4
  %23 = load i32*, i32** %Z, align 4
  %24 = load i32, i32* %r.addr, align 4
  call void @blockmix_salsa8(i32* %21, i32* %22, i32* %23, i32 %24)
  %25 = load i32*, i32** %V.addr, align 4
  %26 = load i64, i64* %i, align 8
  %add = add i64 %26, 1
  %27 = load i32, i32* %r.addr, align 4
  %mul14 = mul i32 32, %27
  %conv15 = zext i32 %mul14 to i64
  %mul16 = mul i64 %add, %conv15
  %idxprom17 = trunc i64 %mul16 to i32
  %arrayidx18 = getelementptr i32, i32* %25, i32 %idxprom17
  %28 = bitcast i32* %arrayidx18 to %union.escrypt_block_t*
  %29 = load i32*, i32** %Y, align 4
  %30 = bitcast i32* %29 to %union.escrypt_block_t*
  %31 = load i32, i32* %r.addr, align 4
  %mul19 = mul i32 128, %31
  call void @blkcpy(%union.escrypt_block_t* %28, %union.escrypt_block_t* %30, i32 %mul19)
  %32 = load i32*, i32** %Y, align 4
  %33 = load i32*, i32** %X, align 4
  %34 = load i32*, i32** %Z, align 4
  %35 = load i32, i32* %r.addr, align 4
  call void @blockmix_salsa8(i32* %32, i32* %33, i32* %34, i32 %35)
  br label %for.inc20

for.inc20:                                        ; preds = %for.body9
  %36 = load i64, i64* %i, align 8
  %add21 = add i64 %36, 2
  store i64 %add21, i64* %i, align 8
  br label %for.cond7

for.end22:                                        ; preds = %for.cond7
  store i64 0, i64* %i, align 8
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc43, %for.end22
  %37 = load i64, i64* %i, align 8
  %38 = load i64, i64* %N.addr, align 8
  %cmp24 = icmp ult i64 %37, %38
  br i1 %cmp24, label %for.body26, label %for.end45

for.body26:                                       ; preds = %for.cond23
  %39 = load i32*, i32** %X, align 4
  %40 = bitcast i32* %39 to i8*
  %41 = load i32, i32* %r.addr, align 4
  %call27 = call i64 @integerify(i8* %40, i32 %41)
  %42 = load i64, i64* %N.addr, align 8
  %sub = sub i64 %42, 1
  %and = and i64 %call27, %sub
  store i64 %and, i64* %j, align 8
  %43 = load i32*, i32** %X, align 4
  %44 = bitcast i32* %43 to %union.escrypt_block_t*
  %45 = load i32*, i32** %V.addr, align 4
  %46 = load i64, i64* %j, align 8
  %47 = load i32, i32* %r.addr, align 4
  %mul28 = mul i32 32, %47
  %conv29 = zext i32 %mul28 to i64
  %mul30 = mul i64 %46, %conv29
  %idxprom31 = trunc i64 %mul30 to i32
  %arrayidx32 = getelementptr i32, i32* %45, i32 %idxprom31
  %48 = bitcast i32* %arrayidx32 to %union.escrypt_block_t*
  %49 = load i32, i32* %r.addr, align 4
  %mul33 = mul i32 128, %49
  call void @blkxor(%union.escrypt_block_t* %44, %union.escrypt_block_t* %48, i32 %mul33)
  %50 = load i32*, i32** %X, align 4
  %51 = load i32*, i32** %Y, align 4
  %52 = load i32*, i32** %Z, align 4
  %53 = load i32, i32* %r.addr, align 4
  call void @blockmix_salsa8(i32* %50, i32* %51, i32* %52, i32 %53)
  %54 = load i32*, i32** %Y, align 4
  %55 = bitcast i32* %54 to i8*
  %56 = load i32, i32* %r.addr, align 4
  %call34 = call i64 @integerify(i8* %55, i32 %56)
  %57 = load i64, i64* %N.addr, align 8
  %sub35 = sub i64 %57, 1
  %and36 = and i64 %call34, %sub35
  store i64 %and36, i64* %j, align 8
  %58 = load i32*, i32** %Y, align 4
  %59 = bitcast i32* %58 to %union.escrypt_block_t*
  %60 = load i32*, i32** %V.addr, align 4
  %61 = load i64, i64* %j, align 8
  %62 = load i32, i32* %r.addr, align 4
  %mul37 = mul i32 32, %62
  %conv38 = zext i32 %mul37 to i64
  %mul39 = mul i64 %61, %conv38
  %idxprom40 = trunc i64 %mul39 to i32
  %arrayidx41 = getelementptr i32, i32* %60, i32 %idxprom40
  %63 = bitcast i32* %arrayidx41 to %union.escrypt_block_t*
  %64 = load i32, i32* %r.addr, align 4
  %mul42 = mul i32 128, %64
  call void @blkxor(%union.escrypt_block_t* %59, %union.escrypt_block_t* %63, i32 %mul42)
  %65 = load i32*, i32** %Y, align 4
  %66 = load i32*, i32** %X, align 4
  %67 = load i32*, i32** %Z, align 4
  %68 = load i32, i32* %r.addr, align 4
  call void @blockmix_salsa8(i32* %65, i32* %66, i32* %67, i32 %68)
  br label %for.inc43

for.inc43:                                        ; preds = %for.body26
  %69 = load i64, i64* %i, align 8
  %add44 = add i64 %69, 2
  store i64 %add44, i64* %i, align 8
  br label %for.cond23

for.end45:                                        ; preds = %for.cond23
  store i32 0, i32* %k, align 4
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc54, %for.end45
  %70 = load i32, i32* %k, align 4
  %71 = load i32, i32* %r.addr, align 4
  %mul47 = mul i32 32, %71
  %cmp48 = icmp ult i32 %70, %mul47
  br i1 %cmp48, label %for.body50, label %for.end56

for.body50:                                       ; preds = %for.cond46
  %72 = load i8*, i8** %B.addr, align 4
  %73 = load i32, i32* %k, align 4
  %mul51 = mul i32 4, %73
  %arrayidx52 = getelementptr i8, i8* %72, i32 %mul51
  %74 = load i32*, i32** %X, align 4
  %75 = load i32, i32* %k, align 4
  %arrayidx53 = getelementptr i32, i32* %74, i32 %75
  %76 = load i32, i32* %arrayidx53, align 4
  call void @store32_le(i8* %arrayidx52, i32 %76)
  br label %for.inc54

for.inc54:                                        ; preds = %for.body50
  %77 = load i32, i32* %k, align 4
  %inc55 = add i32 %77, 1
  store i32 %inc55, i32* %k, align 4
  br label %for.cond46

for.end56:                                        ; preds = %for.cond46
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @load32_le(i8* %src) #0 {
entry:
  %src.addr = alloca i8*, align 4
  %w = alloca i32, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = bitcast i32* %w to i8*
  %1 = load i8*, i8** %src.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 1 %1, i32 4, i1 false)
  %2 = load i32, i32* %w, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define internal void @blkcpy(%union.escrypt_block_t* %dest, %union.escrypt_block_t* %src, i32 %len) #0 {
entry:
  %dest.addr = alloca %union.escrypt_block_t*, align 4
  %src.addr = alloca %union.escrypt_block_t*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %L = alloca i32, align 4
  store %union.escrypt_block_t* %dest, %union.escrypt_block_t** %dest.addr, align 4
  store %union.escrypt_block_t* %src, %union.escrypt_block_t** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i32, i32* %len.addr, align 4
  %shr = lshr i32 %0, 2
  store i32 %shr, i32* %L, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %L, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %union.escrypt_block_t*, %union.escrypt_block_t** %src.addr, align 4
  %w = bitcast %union.escrypt_block_t* %3 to [16 x i32]*
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %w, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %6 = load %union.escrypt_block_t*, %union.escrypt_block_t** %dest.addr, align 4
  %w1 = bitcast %union.escrypt_block_t* %6 to [16 x i32]*
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %w1, i32 0, i32 %7
  store i32 %5, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blockmix_salsa8(i32* %Bin, i32* %Bout, i32* %X, i32 %r) #0 {
entry:
  %Bin.addr = alloca i32*, align 4
  %Bout.addr = alloca i32*, align 4
  %X.addr = alloca i32*, align 4
  %r.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32* %Bin, i32** %Bin.addr, align 4
  store i32* %Bout, i32** %Bout.addr, align 4
  store i32* %X, i32** %X.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  %0 = load i32*, i32** %X.addr, align 4
  %1 = bitcast i32* %0 to %union.escrypt_block_t*
  %2 = load i32*, i32** %Bin.addr, align 4
  %3 = load i32, i32* %r.addr, align 4
  %mul = mul i32 2, %3
  %sub = sub i32 %mul, 1
  %mul1 = mul i32 %sub, 16
  %arrayidx = getelementptr i32, i32* %2, i32 %mul1
  %4 = bitcast i32* %arrayidx to %union.escrypt_block_t*
  call void @blkcpy_64(%union.escrypt_block_t* %1, %union.escrypt_block_t* %4)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %r.addr, align 4
  %mul2 = mul i32 2, %6
  %cmp = icmp ult i32 %5, %mul2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32*, i32** %X.addr, align 4
  %8 = bitcast i32* %7 to %union.escrypt_block_t*
  %9 = load i32*, i32** %Bin.addr, align 4
  %10 = load i32, i32* %i, align 4
  %mul3 = mul i32 %10, 16
  %arrayidx4 = getelementptr i32, i32* %9, i32 %mul3
  %11 = bitcast i32* %arrayidx4 to %union.escrypt_block_t*
  call void @blkxor_64(%union.escrypt_block_t* %8, %union.escrypt_block_t* %11)
  %12 = load i32*, i32** %X.addr, align 4
  call void @salsa20_8(i32* %12)
  %13 = load i32*, i32** %Bout.addr, align 4
  %14 = load i32, i32* %i, align 4
  %mul5 = mul i32 %14, 8
  %arrayidx6 = getelementptr i32, i32* %13, i32 %mul5
  %15 = bitcast i32* %arrayidx6 to %union.escrypt_block_t*
  %16 = load i32*, i32** %X.addr, align 4
  %17 = bitcast i32* %16 to %union.escrypt_block_t*
  call void @blkcpy_64(%union.escrypt_block_t* %15, %union.escrypt_block_t* %17)
  %18 = load i32*, i32** %X.addr, align 4
  %19 = bitcast i32* %18 to %union.escrypt_block_t*
  %20 = load i32*, i32** %Bin.addr, align 4
  %21 = load i32, i32* %i, align 4
  %mul7 = mul i32 %21, 16
  %add = add i32 %mul7, 16
  %arrayidx8 = getelementptr i32, i32* %20, i32 %add
  %22 = bitcast i32* %arrayidx8 to %union.escrypt_block_t*
  call void @blkxor_64(%union.escrypt_block_t* %19, %union.escrypt_block_t* %22)
  %23 = load i32*, i32** %X.addr, align 4
  call void @salsa20_8(i32* %23)
  %24 = load i32*, i32** %Bout.addr, align 4
  %25 = load i32, i32* %i, align 4
  %mul9 = mul i32 %25, 8
  %26 = load i32, i32* %r.addr, align 4
  %mul10 = mul i32 %26, 16
  %add11 = add i32 %mul9, %mul10
  %arrayidx12 = getelementptr i32, i32* %24, i32 %add11
  %27 = bitcast i32* %arrayidx12 to %union.escrypt_block_t*
  %28 = load i32*, i32** %X.addr, align 4
  %29 = bitcast i32* %28 to %union.escrypt_block_t*
  call void @blkcpy_64(%union.escrypt_block_t* %27, %union.escrypt_block_t* %29)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4
  %add13 = add i32 %30, 2
  store i32 %add13, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i64 @integerify(i8* %B, i32 %r) #0 {
entry:
  %B.addr = alloca i8*, align 4
  %r.addr = alloca i32, align 4
  %X = alloca i32*, align 4
  store i8* %B, i8** %B.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  %0 = load i8*, i8** %B.addr, align 4
  %1 = bitcast i8* %0 to i32*
  %2 = load i32, i32* %r.addr, align 4
  %mul = mul i32 2, %2
  %sub = sub i32 %mul, 1
  %mul1 = mul i32 %sub, 16
  %add.ptr = getelementptr i32, i32* %1, i32 %mul1
  store i32* %add.ptr, i32** %X, align 4
  %3 = load i32*, i32** %X, align 4
  %arrayidx = getelementptr i32, i32* %3, i32 1
  %4 = load i32, i32* %arrayidx, align 4
  %conv = zext i32 %4 to i64
  %shl = shl i64 %conv, 32
  %5 = load i32*, i32** %X, align 4
  %arrayidx2 = getelementptr i32, i32* %5, i32 0
  %6 = load i32, i32* %arrayidx2, align 4
  %conv3 = zext i32 %6 to i64
  %add = add i64 %shl, %conv3
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define internal void @blkxor(%union.escrypt_block_t* %dest, %union.escrypt_block_t* %src, i32 %len) #0 {
entry:
  %dest.addr = alloca %union.escrypt_block_t*, align 4
  %src.addr = alloca %union.escrypt_block_t*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %L = alloca i32, align 4
  store %union.escrypt_block_t* %dest, %union.escrypt_block_t** %dest.addr, align 4
  store %union.escrypt_block_t* %src, %union.escrypt_block_t** %src.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i32, i32* %len.addr, align 4
  %shr = lshr i32 %0, 2
  store i32 %shr, i32* %L, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %L, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %union.escrypt_block_t*, %union.escrypt_block_t** %src.addr, align 4
  %w = bitcast %union.escrypt_block_t* %3 to [16 x i32]*
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %w, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %6 = load %union.escrypt_block_t*, %union.escrypt_block_t** %dest.addr, align 4
  %w1 = bitcast %union.escrypt_block_t* %6 to [16 x i32]*
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %w1, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx2, align 4
  %xor = xor i32 %8, %5
  store i32 %xor, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @store32_le(i8* %dst, i32 %w) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %w.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i32 %w, i32* %w.addr, align 4
  %0 = load i8*, i8** %dst.addr, align 4
  %1 = bitcast i32* %w.addr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 4 %1, i32 4, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @blkcpy_64(%union.escrypt_block_t* %dest, %union.escrypt_block_t* %src) #0 {
entry:
  %dest.addr = alloca %union.escrypt_block_t*, align 4
  %src.addr = alloca %union.escrypt_block_t*, align 4
  %i = alloca i32, align 4
  store %union.escrypt_block_t* %dest, %union.escrypt_block_t** %dest.addr, align 4
  store %union.escrypt_block_t* %src, %union.escrypt_block_t** %src.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %union.escrypt_block_t*, %union.escrypt_block_t** %src.addr, align 4
  %w = bitcast %union.escrypt_block_t* %1 to [16 x i32]*
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %w, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load %union.escrypt_block_t*, %union.escrypt_block_t** %dest.addr, align 4
  %w1 = bitcast %union.escrypt_block_t* %4 to [16 x i32]*
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %w1, i32 0, i32 %5
  store i32 %3, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @blkxor_64(%union.escrypt_block_t* %dest, %union.escrypt_block_t* %src) #0 {
entry:
  %dest.addr = alloca %union.escrypt_block_t*, align 4
  %src.addr = alloca %union.escrypt_block_t*, align 4
  %i = alloca i32, align 4
  store %union.escrypt_block_t* %dest, %union.escrypt_block_t** %dest.addr, align 4
  store %union.escrypt_block_t* %src, %union.escrypt_block_t** %src.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %union.escrypt_block_t*, %union.escrypt_block_t** %src.addr, align 4
  %w = bitcast %union.escrypt_block_t* %1 to [16 x i32]*
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr [16 x i32], [16 x i32]* %w, i32 0, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load %union.escrypt_block_t*, %union.escrypt_block_t** %dest.addr, align 4
  %w1 = bitcast %union.escrypt_block_t* %4 to [16 x i32]*
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr [16 x i32], [16 x i32]* %w1, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx2, align 4
  %xor = xor i32 %6, %3
  store i32 %xor, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @salsa20_8(i32* %B) #0 {
entry:
  %B.addr = alloca i32*, align 4
  %X = alloca %union.escrypt_block_t, align 8
  %x = alloca i32*, align 4
  %i = alloca i32, align 4
  store i32* %B, i32** %B.addr, align 4
  %w = bitcast %union.escrypt_block_t* %X to [16 x i32]*
  %arraydecay = getelementptr inbounds [16 x i32], [16 x i32]* %w, i32 0, i32 0
  store i32* %arraydecay, i32** %x, align 4
  %0 = load i32*, i32** %B.addr, align 4
  %1 = bitcast i32* %0 to %union.escrypt_block_t*
  call void @blkcpy_64(%union.escrypt_block_t* %X, %union.escrypt_block_t* %1)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %x, align 4
  %arrayidx = getelementptr i32, i32* %3, i32 0
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load i32*, i32** %x, align 4
  %arrayidx1 = getelementptr i32, i32* %5, i32 12
  %6 = load i32, i32* %arrayidx1, align 4
  %add = add i32 %4, %6
  %shl = shl i32 %add, 7
  %7 = load i32*, i32** %x, align 4
  %arrayidx2 = getelementptr i32, i32* %7, i32 0
  %8 = load i32, i32* %arrayidx2, align 4
  %9 = load i32*, i32** %x, align 4
  %arrayidx3 = getelementptr i32, i32* %9, i32 12
  %10 = load i32, i32* %arrayidx3, align 4
  %add4 = add i32 %8, %10
  %shr = lshr i32 %add4, 25
  %or = or i32 %shl, %shr
  %11 = load i32*, i32** %x, align 4
  %arrayidx5 = getelementptr i32, i32* %11, i32 4
  %12 = load i32, i32* %arrayidx5, align 4
  %xor = xor i32 %12, %or
  store i32 %xor, i32* %arrayidx5, align 4
  %13 = load i32*, i32** %x, align 4
  %arrayidx6 = getelementptr i32, i32* %13, i32 4
  %14 = load i32, i32* %arrayidx6, align 4
  %15 = load i32*, i32** %x, align 4
  %arrayidx7 = getelementptr i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx7, align 4
  %add8 = add i32 %14, %16
  %shl9 = shl i32 %add8, 9
  %17 = load i32*, i32** %x, align 4
  %arrayidx10 = getelementptr i32, i32* %17, i32 4
  %18 = load i32, i32* %arrayidx10, align 4
  %19 = load i32*, i32** %x, align 4
  %arrayidx11 = getelementptr i32, i32* %19, i32 0
  %20 = load i32, i32* %arrayidx11, align 4
  %add12 = add i32 %18, %20
  %shr13 = lshr i32 %add12, 23
  %or14 = or i32 %shl9, %shr13
  %21 = load i32*, i32** %x, align 4
  %arrayidx15 = getelementptr i32, i32* %21, i32 8
  %22 = load i32, i32* %arrayidx15, align 4
  %xor16 = xor i32 %22, %or14
  store i32 %xor16, i32* %arrayidx15, align 4
  %23 = load i32*, i32** %x, align 4
  %arrayidx17 = getelementptr i32, i32* %23, i32 8
  %24 = load i32, i32* %arrayidx17, align 4
  %25 = load i32*, i32** %x, align 4
  %arrayidx18 = getelementptr i32, i32* %25, i32 4
  %26 = load i32, i32* %arrayidx18, align 4
  %add19 = add i32 %24, %26
  %shl20 = shl i32 %add19, 13
  %27 = load i32*, i32** %x, align 4
  %arrayidx21 = getelementptr i32, i32* %27, i32 8
  %28 = load i32, i32* %arrayidx21, align 4
  %29 = load i32*, i32** %x, align 4
  %arrayidx22 = getelementptr i32, i32* %29, i32 4
  %30 = load i32, i32* %arrayidx22, align 4
  %add23 = add i32 %28, %30
  %shr24 = lshr i32 %add23, 19
  %or25 = or i32 %shl20, %shr24
  %31 = load i32*, i32** %x, align 4
  %arrayidx26 = getelementptr i32, i32* %31, i32 12
  %32 = load i32, i32* %arrayidx26, align 4
  %xor27 = xor i32 %32, %or25
  store i32 %xor27, i32* %arrayidx26, align 4
  %33 = load i32*, i32** %x, align 4
  %arrayidx28 = getelementptr i32, i32* %33, i32 12
  %34 = load i32, i32* %arrayidx28, align 4
  %35 = load i32*, i32** %x, align 4
  %arrayidx29 = getelementptr i32, i32* %35, i32 8
  %36 = load i32, i32* %arrayidx29, align 4
  %add30 = add i32 %34, %36
  %shl31 = shl i32 %add30, 18
  %37 = load i32*, i32** %x, align 4
  %arrayidx32 = getelementptr i32, i32* %37, i32 12
  %38 = load i32, i32* %arrayidx32, align 4
  %39 = load i32*, i32** %x, align 4
  %arrayidx33 = getelementptr i32, i32* %39, i32 8
  %40 = load i32, i32* %arrayidx33, align 4
  %add34 = add i32 %38, %40
  %shr35 = lshr i32 %add34, 14
  %or36 = or i32 %shl31, %shr35
  %41 = load i32*, i32** %x, align 4
  %arrayidx37 = getelementptr i32, i32* %41, i32 0
  %42 = load i32, i32* %arrayidx37, align 4
  %xor38 = xor i32 %42, %or36
  store i32 %xor38, i32* %arrayidx37, align 4
  %43 = load i32*, i32** %x, align 4
  %arrayidx39 = getelementptr i32, i32* %43, i32 5
  %44 = load i32, i32* %arrayidx39, align 4
  %45 = load i32*, i32** %x, align 4
  %arrayidx40 = getelementptr i32, i32* %45, i32 1
  %46 = load i32, i32* %arrayidx40, align 4
  %add41 = add i32 %44, %46
  %shl42 = shl i32 %add41, 7
  %47 = load i32*, i32** %x, align 4
  %arrayidx43 = getelementptr i32, i32* %47, i32 5
  %48 = load i32, i32* %arrayidx43, align 4
  %49 = load i32*, i32** %x, align 4
  %arrayidx44 = getelementptr i32, i32* %49, i32 1
  %50 = load i32, i32* %arrayidx44, align 4
  %add45 = add i32 %48, %50
  %shr46 = lshr i32 %add45, 25
  %or47 = or i32 %shl42, %shr46
  %51 = load i32*, i32** %x, align 4
  %arrayidx48 = getelementptr i32, i32* %51, i32 9
  %52 = load i32, i32* %arrayidx48, align 4
  %xor49 = xor i32 %52, %or47
  store i32 %xor49, i32* %arrayidx48, align 4
  %53 = load i32*, i32** %x, align 4
  %arrayidx50 = getelementptr i32, i32* %53, i32 9
  %54 = load i32, i32* %arrayidx50, align 4
  %55 = load i32*, i32** %x, align 4
  %arrayidx51 = getelementptr i32, i32* %55, i32 5
  %56 = load i32, i32* %arrayidx51, align 4
  %add52 = add i32 %54, %56
  %shl53 = shl i32 %add52, 9
  %57 = load i32*, i32** %x, align 4
  %arrayidx54 = getelementptr i32, i32* %57, i32 9
  %58 = load i32, i32* %arrayidx54, align 4
  %59 = load i32*, i32** %x, align 4
  %arrayidx55 = getelementptr i32, i32* %59, i32 5
  %60 = load i32, i32* %arrayidx55, align 4
  %add56 = add i32 %58, %60
  %shr57 = lshr i32 %add56, 23
  %or58 = or i32 %shl53, %shr57
  %61 = load i32*, i32** %x, align 4
  %arrayidx59 = getelementptr i32, i32* %61, i32 13
  %62 = load i32, i32* %arrayidx59, align 4
  %xor60 = xor i32 %62, %or58
  store i32 %xor60, i32* %arrayidx59, align 4
  %63 = load i32*, i32** %x, align 4
  %arrayidx61 = getelementptr i32, i32* %63, i32 13
  %64 = load i32, i32* %arrayidx61, align 4
  %65 = load i32*, i32** %x, align 4
  %arrayidx62 = getelementptr i32, i32* %65, i32 9
  %66 = load i32, i32* %arrayidx62, align 4
  %add63 = add i32 %64, %66
  %shl64 = shl i32 %add63, 13
  %67 = load i32*, i32** %x, align 4
  %arrayidx65 = getelementptr i32, i32* %67, i32 13
  %68 = load i32, i32* %arrayidx65, align 4
  %69 = load i32*, i32** %x, align 4
  %arrayidx66 = getelementptr i32, i32* %69, i32 9
  %70 = load i32, i32* %arrayidx66, align 4
  %add67 = add i32 %68, %70
  %shr68 = lshr i32 %add67, 19
  %or69 = or i32 %shl64, %shr68
  %71 = load i32*, i32** %x, align 4
  %arrayidx70 = getelementptr i32, i32* %71, i32 1
  %72 = load i32, i32* %arrayidx70, align 4
  %xor71 = xor i32 %72, %or69
  store i32 %xor71, i32* %arrayidx70, align 4
  %73 = load i32*, i32** %x, align 4
  %arrayidx72 = getelementptr i32, i32* %73, i32 1
  %74 = load i32, i32* %arrayidx72, align 4
  %75 = load i32*, i32** %x, align 4
  %arrayidx73 = getelementptr i32, i32* %75, i32 13
  %76 = load i32, i32* %arrayidx73, align 4
  %add74 = add i32 %74, %76
  %shl75 = shl i32 %add74, 18
  %77 = load i32*, i32** %x, align 4
  %arrayidx76 = getelementptr i32, i32* %77, i32 1
  %78 = load i32, i32* %arrayidx76, align 4
  %79 = load i32*, i32** %x, align 4
  %arrayidx77 = getelementptr i32, i32* %79, i32 13
  %80 = load i32, i32* %arrayidx77, align 4
  %add78 = add i32 %78, %80
  %shr79 = lshr i32 %add78, 14
  %or80 = or i32 %shl75, %shr79
  %81 = load i32*, i32** %x, align 4
  %arrayidx81 = getelementptr i32, i32* %81, i32 5
  %82 = load i32, i32* %arrayidx81, align 4
  %xor82 = xor i32 %82, %or80
  store i32 %xor82, i32* %arrayidx81, align 4
  %83 = load i32*, i32** %x, align 4
  %arrayidx83 = getelementptr i32, i32* %83, i32 10
  %84 = load i32, i32* %arrayidx83, align 4
  %85 = load i32*, i32** %x, align 4
  %arrayidx84 = getelementptr i32, i32* %85, i32 6
  %86 = load i32, i32* %arrayidx84, align 4
  %add85 = add i32 %84, %86
  %shl86 = shl i32 %add85, 7
  %87 = load i32*, i32** %x, align 4
  %arrayidx87 = getelementptr i32, i32* %87, i32 10
  %88 = load i32, i32* %arrayidx87, align 4
  %89 = load i32*, i32** %x, align 4
  %arrayidx88 = getelementptr i32, i32* %89, i32 6
  %90 = load i32, i32* %arrayidx88, align 4
  %add89 = add i32 %88, %90
  %shr90 = lshr i32 %add89, 25
  %or91 = or i32 %shl86, %shr90
  %91 = load i32*, i32** %x, align 4
  %arrayidx92 = getelementptr i32, i32* %91, i32 14
  %92 = load i32, i32* %arrayidx92, align 4
  %xor93 = xor i32 %92, %or91
  store i32 %xor93, i32* %arrayidx92, align 4
  %93 = load i32*, i32** %x, align 4
  %arrayidx94 = getelementptr i32, i32* %93, i32 14
  %94 = load i32, i32* %arrayidx94, align 4
  %95 = load i32*, i32** %x, align 4
  %arrayidx95 = getelementptr i32, i32* %95, i32 10
  %96 = load i32, i32* %arrayidx95, align 4
  %add96 = add i32 %94, %96
  %shl97 = shl i32 %add96, 9
  %97 = load i32*, i32** %x, align 4
  %arrayidx98 = getelementptr i32, i32* %97, i32 14
  %98 = load i32, i32* %arrayidx98, align 4
  %99 = load i32*, i32** %x, align 4
  %arrayidx99 = getelementptr i32, i32* %99, i32 10
  %100 = load i32, i32* %arrayidx99, align 4
  %add100 = add i32 %98, %100
  %shr101 = lshr i32 %add100, 23
  %or102 = or i32 %shl97, %shr101
  %101 = load i32*, i32** %x, align 4
  %arrayidx103 = getelementptr i32, i32* %101, i32 2
  %102 = load i32, i32* %arrayidx103, align 4
  %xor104 = xor i32 %102, %or102
  store i32 %xor104, i32* %arrayidx103, align 4
  %103 = load i32*, i32** %x, align 4
  %arrayidx105 = getelementptr i32, i32* %103, i32 2
  %104 = load i32, i32* %arrayidx105, align 4
  %105 = load i32*, i32** %x, align 4
  %arrayidx106 = getelementptr i32, i32* %105, i32 14
  %106 = load i32, i32* %arrayidx106, align 4
  %add107 = add i32 %104, %106
  %shl108 = shl i32 %add107, 13
  %107 = load i32*, i32** %x, align 4
  %arrayidx109 = getelementptr i32, i32* %107, i32 2
  %108 = load i32, i32* %arrayidx109, align 4
  %109 = load i32*, i32** %x, align 4
  %arrayidx110 = getelementptr i32, i32* %109, i32 14
  %110 = load i32, i32* %arrayidx110, align 4
  %add111 = add i32 %108, %110
  %shr112 = lshr i32 %add111, 19
  %or113 = or i32 %shl108, %shr112
  %111 = load i32*, i32** %x, align 4
  %arrayidx114 = getelementptr i32, i32* %111, i32 6
  %112 = load i32, i32* %arrayidx114, align 4
  %xor115 = xor i32 %112, %or113
  store i32 %xor115, i32* %arrayidx114, align 4
  %113 = load i32*, i32** %x, align 4
  %arrayidx116 = getelementptr i32, i32* %113, i32 6
  %114 = load i32, i32* %arrayidx116, align 4
  %115 = load i32*, i32** %x, align 4
  %arrayidx117 = getelementptr i32, i32* %115, i32 2
  %116 = load i32, i32* %arrayidx117, align 4
  %add118 = add i32 %114, %116
  %shl119 = shl i32 %add118, 18
  %117 = load i32*, i32** %x, align 4
  %arrayidx120 = getelementptr i32, i32* %117, i32 6
  %118 = load i32, i32* %arrayidx120, align 4
  %119 = load i32*, i32** %x, align 4
  %arrayidx121 = getelementptr i32, i32* %119, i32 2
  %120 = load i32, i32* %arrayidx121, align 4
  %add122 = add i32 %118, %120
  %shr123 = lshr i32 %add122, 14
  %or124 = or i32 %shl119, %shr123
  %121 = load i32*, i32** %x, align 4
  %arrayidx125 = getelementptr i32, i32* %121, i32 10
  %122 = load i32, i32* %arrayidx125, align 4
  %xor126 = xor i32 %122, %or124
  store i32 %xor126, i32* %arrayidx125, align 4
  %123 = load i32*, i32** %x, align 4
  %arrayidx127 = getelementptr i32, i32* %123, i32 15
  %124 = load i32, i32* %arrayidx127, align 4
  %125 = load i32*, i32** %x, align 4
  %arrayidx128 = getelementptr i32, i32* %125, i32 11
  %126 = load i32, i32* %arrayidx128, align 4
  %add129 = add i32 %124, %126
  %shl130 = shl i32 %add129, 7
  %127 = load i32*, i32** %x, align 4
  %arrayidx131 = getelementptr i32, i32* %127, i32 15
  %128 = load i32, i32* %arrayidx131, align 4
  %129 = load i32*, i32** %x, align 4
  %arrayidx132 = getelementptr i32, i32* %129, i32 11
  %130 = load i32, i32* %arrayidx132, align 4
  %add133 = add i32 %128, %130
  %shr134 = lshr i32 %add133, 25
  %or135 = or i32 %shl130, %shr134
  %131 = load i32*, i32** %x, align 4
  %arrayidx136 = getelementptr i32, i32* %131, i32 3
  %132 = load i32, i32* %arrayidx136, align 4
  %xor137 = xor i32 %132, %or135
  store i32 %xor137, i32* %arrayidx136, align 4
  %133 = load i32*, i32** %x, align 4
  %arrayidx138 = getelementptr i32, i32* %133, i32 3
  %134 = load i32, i32* %arrayidx138, align 4
  %135 = load i32*, i32** %x, align 4
  %arrayidx139 = getelementptr i32, i32* %135, i32 15
  %136 = load i32, i32* %arrayidx139, align 4
  %add140 = add i32 %134, %136
  %shl141 = shl i32 %add140, 9
  %137 = load i32*, i32** %x, align 4
  %arrayidx142 = getelementptr i32, i32* %137, i32 3
  %138 = load i32, i32* %arrayidx142, align 4
  %139 = load i32*, i32** %x, align 4
  %arrayidx143 = getelementptr i32, i32* %139, i32 15
  %140 = load i32, i32* %arrayidx143, align 4
  %add144 = add i32 %138, %140
  %shr145 = lshr i32 %add144, 23
  %or146 = or i32 %shl141, %shr145
  %141 = load i32*, i32** %x, align 4
  %arrayidx147 = getelementptr i32, i32* %141, i32 7
  %142 = load i32, i32* %arrayidx147, align 4
  %xor148 = xor i32 %142, %or146
  store i32 %xor148, i32* %arrayidx147, align 4
  %143 = load i32*, i32** %x, align 4
  %arrayidx149 = getelementptr i32, i32* %143, i32 7
  %144 = load i32, i32* %arrayidx149, align 4
  %145 = load i32*, i32** %x, align 4
  %arrayidx150 = getelementptr i32, i32* %145, i32 3
  %146 = load i32, i32* %arrayidx150, align 4
  %add151 = add i32 %144, %146
  %shl152 = shl i32 %add151, 13
  %147 = load i32*, i32** %x, align 4
  %arrayidx153 = getelementptr i32, i32* %147, i32 7
  %148 = load i32, i32* %arrayidx153, align 4
  %149 = load i32*, i32** %x, align 4
  %arrayidx154 = getelementptr i32, i32* %149, i32 3
  %150 = load i32, i32* %arrayidx154, align 4
  %add155 = add i32 %148, %150
  %shr156 = lshr i32 %add155, 19
  %or157 = or i32 %shl152, %shr156
  %151 = load i32*, i32** %x, align 4
  %arrayidx158 = getelementptr i32, i32* %151, i32 11
  %152 = load i32, i32* %arrayidx158, align 4
  %xor159 = xor i32 %152, %or157
  store i32 %xor159, i32* %arrayidx158, align 4
  %153 = load i32*, i32** %x, align 4
  %arrayidx160 = getelementptr i32, i32* %153, i32 11
  %154 = load i32, i32* %arrayidx160, align 4
  %155 = load i32*, i32** %x, align 4
  %arrayidx161 = getelementptr i32, i32* %155, i32 7
  %156 = load i32, i32* %arrayidx161, align 4
  %add162 = add i32 %154, %156
  %shl163 = shl i32 %add162, 18
  %157 = load i32*, i32** %x, align 4
  %arrayidx164 = getelementptr i32, i32* %157, i32 11
  %158 = load i32, i32* %arrayidx164, align 4
  %159 = load i32*, i32** %x, align 4
  %arrayidx165 = getelementptr i32, i32* %159, i32 7
  %160 = load i32, i32* %arrayidx165, align 4
  %add166 = add i32 %158, %160
  %shr167 = lshr i32 %add166, 14
  %or168 = or i32 %shl163, %shr167
  %161 = load i32*, i32** %x, align 4
  %arrayidx169 = getelementptr i32, i32* %161, i32 15
  %162 = load i32, i32* %arrayidx169, align 4
  %xor170 = xor i32 %162, %or168
  store i32 %xor170, i32* %arrayidx169, align 4
  %163 = load i32*, i32** %x, align 4
  %arrayidx171 = getelementptr i32, i32* %163, i32 0
  %164 = load i32, i32* %arrayidx171, align 4
  %165 = load i32*, i32** %x, align 4
  %arrayidx172 = getelementptr i32, i32* %165, i32 3
  %166 = load i32, i32* %arrayidx172, align 4
  %add173 = add i32 %164, %166
  %shl174 = shl i32 %add173, 7
  %167 = load i32*, i32** %x, align 4
  %arrayidx175 = getelementptr i32, i32* %167, i32 0
  %168 = load i32, i32* %arrayidx175, align 4
  %169 = load i32*, i32** %x, align 4
  %arrayidx176 = getelementptr i32, i32* %169, i32 3
  %170 = load i32, i32* %arrayidx176, align 4
  %add177 = add i32 %168, %170
  %shr178 = lshr i32 %add177, 25
  %or179 = or i32 %shl174, %shr178
  %171 = load i32*, i32** %x, align 4
  %arrayidx180 = getelementptr i32, i32* %171, i32 1
  %172 = load i32, i32* %arrayidx180, align 4
  %xor181 = xor i32 %172, %or179
  store i32 %xor181, i32* %arrayidx180, align 4
  %173 = load i32*, i32** %x, align 4
  %arrayidx182 = getelementptr i32, i32* %173, i32 1
  %174 = load i32, i32* %arrayidx182, align 4
  %175 = load i32*, i32** %x, align 4
  %arrayidx183 = getelementptr i32, i32* %175, i32 0
  %176 = load i32, i32* %arrayidx183, align 4
  %add184 = add i32 %174, %176
  %shl185 = shl i32 %add184, 9
  %177 = load i32*, i32** %x, align 4
  %arrayidx186 = getelementptr i32, i32* %177, i32 1
  %178 = load i32, i32* %arrayidx186, align 4
  %179 = load i32*, i32** %x, align 4
  %arrayidx187 = getelementptr i32, i32* %179, i32 0
  %180 = load i32, i32* %arrayidx187, align 4
  %add188 = add i32 %178, %180
  %shr189 = lshr i32 %add188, 23
  %or190 = or i32 %shl185, %shr189
  %181 = load i32*, i32** %x, align 4
  %arrayidx191 = getelementptr i32, i32* %181, i32 2
  %182 = load i32, i32* %arrayidx191, align 4
  %xor192 = xor i32 %182, %or190
  store i32 %xor192, i32* %arrayidx191, align 4
  %183 = load i32*, i32** %x, align 4
  %arrayidx193 = getelementptr i32, i32* %183, i32 2
  %184 = load i32, i32* %arrayidx193, align 4
  %185 = load i32*, i32** %x, align 4
  %arrayidx194 = getelementptr i32, i32* %185, i32 1
  %186 = load i32, i32* %arrayidx194, align 4
  %add195 = add i32 %184, %186
  %shl196 = shl i32 %add195, 13
  %187 = load i32*, i32** %x, align 4
  %arrayidx197 = getelementptr i32, i32* %187, i32 2
  %188 = load i32, i32* %arrayidx197, align 4
  %189 = load i32*, i32** %x, align 4
  %arrayidx198 = getelementptr i32, i32* %189, i32 1
  %190 = load i32, i32* %arrayidx198, align 4
  %add199 = add i32 %188, %190
  %shr200 = lshr i32 %add199, 19
  %or201 = or i32 %shl196, %shr200
  %191 = load i32*, i32** %x, align 4
  %arrayidx202 = getelementptr i32, i32* %191, i32 3
  %192 = load i32, i32* %arrayidx202, align 4
  %xor203 = xor i32 %192, %or201
  store i32 %xor203, i32* %arrayidx202, align 4
  %193 = load i32*, i32** %x, align 4
  %arrayidx204 = getelementptr i32, i32* %193, i32 3
  %194 = load i32, i32* %arrayidx204, align 4
  %195 = load i32*, i32** %x, align 4
  %arrayidx205 = getelementptr i32, i32* %195, i32 2
  %196 = load i32, i32* %arrayidx205, align 4
  %add206 = add i32 %194, %196
  %shl207 = shl i32 %add206, 18
  %197 = load i32*, i32** %x, align 4
  %arrayidx208 = getelementptr i32, i32* %197, i32 3
  %198 = load i32, i32* %arrayidx208, align 4
  %199 = load i32*, i32** %x, align 4
  %arrayidx209 = getelementptr i32, i32* %199, i32 2
  %200 = load i32, i32* %arrayidx209, align 4
  %add210 = add i32 %198, %200
  %shr211 = lshr i32 %add210, 14
  %or212 = or i32 %shl207, %shr211
  %201 = load i32*, i32** %x, align 4
  %arrayidx213 = getelementptr i32, i32* %201, i32 0
  %202 = load i32, i32* %arrayidx213, align 4
  %xor214 = xor i32 %202, %or212
  store i32 %xor214, i32* %arrayidx213, align 4
  %203 = load i32*, i32** %x, align 4
  %arrayidx215 = getelementptr i32, i32* %203, i32 5
  %204 = load i32, i32* %arrayidx215, align 4
  %205 = load i32*, i32** %x, align 4
  %arrayidx216 = getelementptr i32, i32* %205, i32 4
  %206 = load i32, i32* %arrayidx216, align 4
  %add217 = add i32 %204, %206
  %shl218 = shl i32 %add217, 7
  %207 = load i32*, i32** %x, align 4
  %arrayidx219 = getelementptr i32, i32* %207, i32 5
  %208 = load i32, i32* %arrayidx219, align 4
  %209 = load i32*, i32** %x, align 4
  %arrayidx220 = getelementptr i32, i32* %209, i32 4
  %210 = load i32, i32* %arrayidx220, align 4
  %add221 = add i32 %208, %210
  %shr222 = lshr i32 %add221, 25
  %or223 = or i32 %shl218, %shr222
  %211 = load i32*, i32** %x, align 4
  %arrayidx224 = getelementptr i32, i32* %211, i32 6
  %212 = load i32, i32* %arrayidx224, align 4
  %xor225 = xor i32 %212, %or223
  store i32 %xor225, i32* %arrayidx224, align 4
  %213 = load i32*, i32** %x, align 4
  %arrayidx226 = getelementptr i32, i32* %213, i32 6
  %214 = load i32, i32* %arrayidx226, align 4
  %215 = load i32*, i32** %x, align 4
  %arrayidx227 = getelementptr i32, i32* %215, i32 5
  %216 = load i32, i32* %arrayidx227, align 4
  %add228 = add i32 %214, %216
  %shl229 = shl i32 %add228, 9
  %217 = load i32*, i32** %x, align 4
  %arrayidx230 = getelementptr i32, i32* %217, i32 6
  %218 = load i32, i32* %arrayidx230, align 4
  %219 = load i32*, i32** %x, align 4
  %arrayidx231 = getelementptr i32, i32* %219, i32 5
  %220 = load i32, i32* %arrayidx231, align 4
  %add232 = add i32 %218, %220
  %shr233 = lshr i32 %add232, 23
  %or234 = or i32 %shl229, %shr233
  %221 = load i32*, i32** %x, align 4
  %arrayidx235 = getelementptr i32, i32* %221, i32 7
  %222 = load i32, i32* %arrayidx235, align 4
  %xor236 = xor i32 %222, %or234
  store i32 %xor236, i32* %arrayidx235, align 4
  %223 = load i32*, i32** %x, align 4
  %arrayidx237 = getelementptr i32, i32* %223, i32 7
  %224 = load i32, i32* %arrayidx237, align 4
  %225 = load i32*, i32** %x, align 4
  %arrayidx238 = getelementptr i32, i32* %225, i32 6
  %226 = load i32, i32* %arrayidx238, align 4
  %add239 = add i32 %224, %226
  %shl240 = shl i32 %add239, 13
  %227 = load i32*, i32** %x, align 4
  %arrayidx241 = getelementptr i32, i32* %227, i32 7
  %228 = load i32, i32* %arrayidx241, align 4
  %229 = load i32*, i32** %x, align 4
  %arrayidx242 = getelementptr i32, i32* %229, i32 6
  %230 = load i32, i32* %arrayidx242, align 4
  %add243 = add i32 %228, %230
  %shr244 = lshr i32 %add243, 19
  %or245 = or i32 %shl240, %shr244
  %231 = load i32*, i32** %x, align 4
  %arrayidx246 = getelementptr i32, i32* %231, i32 4
  %232 = load i32, i32* %arrayidx246, align 4
  %xor247 = xor i32 %232, %or245
  store i32 %xor247, i32* %arrayidx246, align 4
  %233 = load i32*, i32** %x, align 4
  %arrayidx248 = getelementptr i32, i32* %233, i32 4
  %234 = load i32, i32* %arrayidx248, align 4
  %235 = load i32*, i32** %x, align 4
  %arrayidx249 = getelementptr i32, i32* %235, i32 7
  %236 = load i32, i32* %arrayidx249, align 4
  %add250 = add i32 %234, %236
  %shl251 = shl i32 %add250, 18
  %237 = load i32*, i32** %x, align 4
  %arrayidx252 = getelementptr i32, i32* %237, i32 4
  %238 = load i32, i32* %arrayidx252, align 4
  %239 = load i32*, i32** %x, align 4
  %arrayidx253 = getelementptr i32, i32* %239, i32 7
  %240 = load i32, i32* %arrayidx253, align 4
  %add254 = add i32 %238, %240
  %shr255 = lshr i32 %add254, 14
  %or256 = or i32 %shl251, %shr255
  %241 = load i32*, i32** %x, align 4
  %arrayidx257 = getelementptr i32, i32* %241, i32 5
  %242 = load i32, i32* %arrayidx257, align 4
  %xor258 = xor i32 %242, %or256
  store i32 %xor258, i32* %arrayidx257, align 4
  %243 = load i32*, i32** %x, align 4
  %arrayidx259 = getelementptr i32, i32* %243, i32 10
  %244 = load i32, i32* %arrayidx259, align 4
  %245 = load i32*, i32** %x, align 4
  %arrayidx260 = getelementptr i32, i32* %245, i32 9
  %246 = load i32, i32* %arrayidx260, align 4
  %add261 = add i32 %244, %246
  %shl262 = shl i32 %add261, 7
  %247 = load i32*, i32** %x, align 4
  %arrayidx263 = getelementptr i32, i32* %247, i32 10
  %248 = load i32, i32* %arrayidx263, align 4
  %249 = load i32*, i32** %x, align 4
  %arrayidx264 = getelementptr i32, i32* %249, i32 9
  %250 = load i32, i32* %arrayidx264, align 4
  %add265 = add i32 %248, %250
  %shr266 = lshr i32 %add265, 25
  %or267 = or i32 %shl262, %shr266
  %251 = load i32*, i32** %x, align 4
  %arrayidx268 = getelementptr i32, i32* %251, i32 11
  %252 = load i32, i32* %arrayidx268, align 4
  %xor269 = xor i32 %252, %or267
  store i32 %xor269, i32* %arrayidx268, align 4
  %253 = load i32*, i32** %x, align 4
  %arrayidx270 = getelementptr i32, i32* %253, i32 11
  %254 = load i32, i32* %arrayidx270, align 4
  %255 = load i32*, i32** %x, align 4
  %arrayidx271 = getelementptr i32, i32* %255, i32 10
  %256 = load i32, i32* %arrayidx271, align 4
  %add272 = add i32 %254, %256
  %shl273 = shl i32 %add272, 9
  %257 = load i32*, i32** %x, align 4
  %arrayidx274 = getelementptr i32, i32* %257, i32 11
  %258 = load i32, i32* %arrayidx274, align 4
  %259 = load i32*, i32** %x, align 4
  %arrayidx275 = getelementptr i32, i32* %259, i32 10
  %260 = load i32, i32* %arrayidx275, align 4
  %add276 = add i32 %258, %260
  %shr277 = lshr i32 %add276, 23
  %or278 = or i32 %shl273, %shr277
  %261 = load i32*, i32** %x, align 4
  %arrayidx279 = getelementptr i32, i32* %261, i32 8
  %262 = load i32, i32* %arrayidx279, align 4
  %xor280 = xor i32 %262, %or278
  store i32 %xor280, i32* %arrayidx279, align 4
  %263 = load i32*, i32** %x, align 4
  %arrayidx281 = getelementptr i32, i32* %263, i32 8
  %264 = load i32, i32* %arrayidx281, align 4
  %265 = load i32*, i32** %x, align 4
  %arrayidx282 = getelementptr i32, i32* %265, i32 11
  %266 = load i32, i32* %arrayidx282, align 4
  %add283 = add i32 %264, %266
  %shl284 = shl i32 %add283, 13
  %267 = load i32*, i32** %x, align 4
  %arrayidx285 = getelementptr i32, i32* %267, i32 8
  %268 = load i32, i32* %arrayidx285, align 4
  %269 = load i32*, i32** %x, align 4
  %arrayidx286 = getelementptr i32, i32* %269, i32 11
  %270 = load i32, i32* %arrayidx286, align 4
  %add287 = add i32 %268, %270
  %shr288 = lshr i32 %add287, 19
  %or289 = or i32 %shl284, %shr288
  %271 = load i32*, i32** %x, align 4
  %arrayidx290 = getelementptr i32, i32* %271, i32 9
  %272 = load i32, i32* %arrayidx290, align 4
  %xor291 = xor i32 %272, %or289
  store i32 %xor291, i32* %arrayidx290, align 4
  %273 = load i32*, i32** %x, align 4
  %arrayidx292 = getelementptr i32, i32* %273, i32 9
  %274 = load i32, i32* %arrayidx292, align 4
  %275 = load i32*, i32** %x, align 4
  %arrayidx293 = getelementptr i32, i32* %275, i32 8
  %276 = load i32, i32* %arrayidx293, align 4
  %add294 = add i32 %274, %276
  %shl295 = shl i32 %add294, 18
  %277 = load i32*, i32** %x, align 4
  %arrayidx296 = getelementptr i32, i32* %277, i32 9
  %278 = load i32, i32* %arrayidx296, align 4
  %279 = load i32*, i32** %x, align 4
  %arrayidx297 = getelementptr i32, i32* %279, i32 8
  %280 = load i32, i32* %arrayidx297, align 4
  %add298 = add i32 %278, %280
  %shr299 = lshr i32 %add298, 14
  %or300 = or i32 %shl295, %shr299
  %281 = load i32*, i32** %x, align 4
  %arrayidx301 = getelementptr i32, i32* %281, i32 10
  %282 = load i32, i32* %arrayidx301, align 4
  %xor302 = xor i32 %282, %or300
  store i32 %xor302, i32* %arrayidx301, align 4
  %283 = load i32*, i32** %x, align 4
  %arrayidx303 = getelementptr i32, i32* %283, i32 15
  %284 = load i32, i32* %arrayidx303, align 4
  %285 = load i32*, i32** %x, align 4
  %arrayidx304 = getelementptr i32, i32* %285, i32 14
  %286 = load i32, i32* %arrayidx304, align 4
  %add305 = add i32 %284, %286
  %shl306 = shl i32 %add305, 7
  %287 = load i32*, i32** %x, align 4
  %arrayidx307 = getelementptr i32, i32* %287, i32 15
  %288 = load i32, i32* %arrayidx307, align 4
  %289 = load i32*, i32** %x, align 4
  %arrayidx308 = getelementptr i32, i32* %289, i32 14
  %290 = load i32, i32* %arrayidx308, align 4
  %add309 = add i32 %288, %290
  %shr310 = lshr i32 %add309, 25
  %or311 = or i32 %shl306, %shr310
  %291 = load i32*, i32** %x, align 4
  %arrayidx312 = getelementptr i32, i32* %291, i32 12
  %292 = load i32, i32* %arrayidx312, align 4
  %xor313 = xor i32 %292, %or311
  store i32 %xor313, i32* %arrayidx312, align 4
  %293 = load i32*, i32** %x, align 4
  %arrayidx314 = getelementptr i32, i32* %293, i32 12
  %294 = load i32, i32* %arrayidx314, align 4
  %295 = load i32*, i32** %x, align 4
  %arrayidx315 = getelementptr i32, i32* %295, i32 15
  %296 = load i32, i32* %arrayidx315, align 4
  %add316 = add i32 %294, %296
  %shl317 = shl i32 %add316, 9
  %297 = load i32*, i32** %x, align 4
  %arrayidx318 = getelementptr i32, i32* %297, i32 12
  %298 = load i32, i32* %arrayidx318, align 4
  %299 = load i32*, i32** %x, align 4
  %arrayidx319 = getelementptr i32, i32* %299, i32 15
  %300 = load i32, i32* %arrayidx319, align 4
  %add320 = add i32 %298, %300
  %shr321 = lshr i32 %add320, 23
  %or322 = or i32 %shl317, %shr321
  %301 = load i32*, i32** %x, align 4
  %arrayidx323 = getelementptr i32, i32* %301, i32 13
  %302 = load i32, i32* %arrayidx323, align 4
  %xor324 = xor i32 %302, %or322
  store i32 %xor324, i32* %arrayidx323, align 4
  %303 = load i32*, i32** %x, align 4
  %arrayidx325 = getelementptr i32, i32* %303, i32 13
  %304 = load i32, i32* %arrayidx325, align 4
  %305 = load i32*, i32** %x, align 4
  %arrayidx326 = getelementptr i32, i32* %305, i32 12
  %306 = load i32, i32* %arrayidx326, align 4
  %add327 = add i32 %304, %306
  %shl328 = shl i32 %add327, 13
  %307 = load i32*, i32** %x, align 4
  %arrayidx329 = getelementptr i32, i32* %307, i32 13
  %308 = load i32, i32* %arrayidx329, align 4
  %309 = load i32*, i32** %x, align 4
  %arrayidx330 = getelementptr i32, i32* %309, i32 12
  %310 = load i32, i32* %arrayidx330, align 4
  %add331 = add i32 %308, %310
  %shr332 = lshr i32 %add331, 19
  %or333 = or i32 %shl328, %shr332
  %311 = load i32*, i32** %x, align 4
  %arrayidx334 = getelementptr i32, i32* %311, i32 14
  %312 = load i32, i32* %arrayidx334, align 4
  %xor335 = xor i32 %312, %or333
  store i32 %xor335, i32* %arrayidx334, align 4
  %313 = load i32*, i32** %x, align 4
  %arrayidx336 = getelementptr i32, i32* %313, i32 14
  %314 = load i32, i32* %arrayidx336, align 4
  %315 = load i32*, i32** %x, align 4
  %arrayidx337 = getelementptr i32, i32* %315, i32 13
  %316 = load i32, i32* %arrayidx337, align 4
  %add338 = add i32 %314, %316
  %shl339 = shl i32 %add338, 18
  %317 = load i32*, i32** %x, align 4
  %arrayidx340 = getelementptr i32, i32* %317, i32 14
  %318 = load i32, i32* %arrayidx340, align 4
  %319 = load i32*, i32** %x, align 4
  %arrayidx341 = getelementptr i32, i32* %319, i32 13
  %320 = load i32, i32* %arrayidx341, align 4
  %add342 = add i32 %318, %320
  %shr343 = lshr i32 %add342, 14
  %or344 = or i32 %shl339, %shr343
  %321 = load i32*, i32** %x, align 4
  %arrayidx345 = getelementptr i32, i32* %321, i32 15
  %322 = load i32, i32* %arrayidx345, align 4
  %xor346 = xor i32 %322, %or344
  store i32 %xor346, i32* %arrayidx345, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %323 = load i32, i32* %i, align 4
  %add347 = add i32 %323, 2
  store i32 %add347, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond348

for.cond348:                                      ; preds = %for.inc354, %for.end
  %324 = load i32, i32* %i, align 4
  %cmp349 = icmp ult i32 %324, 16
  br i1 %cmp349, label %for.body350, label %for.end355

for.body350:                                      ; preds = %for.cond348
  %325 = load i32*, i32** %x, align 4
  %326 = load i32, i32* %i, align 4
  %arrayidx351 = getelementptr i32, i32* %325, i32 %326
  %327 = load i32, i32* %arrayidx351, align 4
  %328 = load i32*, i32** %B.addr, align 4
  %329 = load i32, i32* %i, align 4
  %arrayidx352 = getelementptr i32, i32* %328, i32 %329
  %330 = load i32, i32* %arrayidx352, align 4
  %add353 = add i32 %330, %327
  store i32 %add353, i32* %arrayidx352, align 4
  br label %for.inc354

for.inc354:                                       ; preds = %for.body350
  %331 = load i32, i32* %i, align 4
  %inc = add i32 %331, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond348

for.end355:                                       ; preds = %for.cond348
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
