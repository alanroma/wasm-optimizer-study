; ModuleID = 'crypto_aead/aes256gcm/aesni/aead_aes256gcm_aesni.c'
source_filename = "crypto_aead/aes256gcm/aesni/aead_aes256gcm_aesni.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_aead_aes256gcm_state_ = type { [512 x i8] }

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_encrypt_detached(i8* nonnull %c, i8* nonnull %mac, i64* %maclen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %maclen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64* %maclen_p, i64** %maclen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

declare i32* @__errno_location() #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_encrypt(i8* nonnull %c, i64* %clen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64* %clen_p, i64** %clen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_decrypt_detached(i8* %m, i8* %nsec, i8* nonnull %c, i64 %clen, i8* nonnull %mac, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %mac.addr = alloca i8*, align 4
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_decrypt(i8* %m, i64* %mlen_p, i8* %nsec, i8* nonnull %c, i64 %clen, i8* %ad, i64 %adlen, i8* nonnull %npub, i8* nonnull %k) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_beforenm(%struct.crypto_aead_aes256gcm_state_* nonnull %ctx_, i8* nonnull %k) #0 {
entry:
  %ctx_.addr = alloca %struct.crypto_aead_aes256gcm_state_*, align 4
  %k.addr = alloca i8*, align 4
  store %struct.crypto_aead_aes256gcm_state_* %ctx_, %struct.crypto_aead_aes256gcm_state_** %ctx_.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_encrypt_detached_afternm(i8* nonnull %c, i8* nonnull %mac, i64* %maclen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, %struct.crypto_aead_aes256gcm_state_* nonnull %ctx_) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %mac.addr = alloca i8*, align 4
  %maclen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %ctx_.addr = alloca %struct.crypto_aead_aes256gcm_state_*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %mac, i8** %mac.addr, align 4
  store i64* %maclen_p, i64** %maclen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store %struct.crypto_aead_aes256gcm_state_* %ctx_, %struct.crypto_aead_aes256gcm_state_** %ctx_.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_encrypt_afternm(i8* nonnull %c, i64* %clen_p, i8* %m, i64 %mlen, i8* %ad, i64 %adlen, i8* %nsec, i8* nonnull %npub, %struct.crypto_aead_aes256gcm_state_* nonnull %ctx_) #0 {
entry:
  %c.addr = alloca i8*, align 4
  %clen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %nsec.addr = alloca i8*, align 4
  %npub.addr = alloca i8*, align 4
  %ctx_.addr = alloca %struct.crypto_aead_aes256gcm_state_*, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64* %clen_p, i64** %clen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %npub, i8** %npub.addr, align 4
  store %struct.crypto_aead_aes256gcm_state_* %ctx_, %struct.crypto_aead_aes256gcm_state_** %ctx_.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_decrypt_detached_afternm(i8* %m, i8* %nsec, i8* nonnull %c, i64 %clen, i8* nonnull %mac, i8* %ad, i64 %adlen, i8* nonnull %npub, %struct.crypto_aead_aes256gcm_state_* nonnull %ctx_) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %mac.addr = alloca i8*, align 4
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %ctx_.addr = alloca %struct.crypto_aead_aes256gcm_state_*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %mac, i8** %mac.addr, align 4
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store %struct.crypto_aead_aes256gcm_state_* %ctx_, %struct.crypto_aead_aes256gcm_state_** %ctx_.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_decrypt_afternm(i8* %m, i64* %mlen_p, i8* %nsec, i8* nonnull %c, i64 %clen, i8* %ad, i64 %adlen, i8* nonnull %npub, %struct.crypto_aead_aes256gcm_state_* nonnull %ctx_) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %nsec.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %ad.addr = alloca i8*, align 4
  %adlen.addr = alloca i64, align 8
  %npub.addr = alloca i8*, align 4
  %ctx_.addr = alloca %struct.crypto_aead_aes256gcm_state_*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %nsec, i8** %nsec.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %ad, i8** %ad.addr, align 4
  store i64 %adlen, i64* %adlen.addr, align 8
  store i8* %npub, i8** %npub.addr, align 4
  store %struct.crypto_aead_aes256gcm_state_* %ctx_, %struct.crypto_aead_aes256gcm_state_** %ctx_.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_is_available() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_nsecbytes() #0 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_npubbytes() #0 {
entry:
  ret i32 12
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_abytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_statebytes() #0 {
entry:
  ret i32 512
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_aead_aes256gcm_messagebytes_max() #0 {
entry:
  ret i32 -17
}

; Function Attrs: noinline nounwind optnone
define void @crypto_aead_aes256gcm_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
