; ModuleID = 'crypto_stream/salsa20/ref/salsa20_ref.c'
source_filename = "crypto_stream/salsa20/ref/salsa20_ref.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_stream_salsa20_implementation = type { i32 (i8*, i64, i8*, i8*)*, i32 (i8*, i8*, i64, i8*, i64, i8*)* }

@crypto_stream_salsa20_ref_implementation = hidden global %struct.crypto_stream_salsa20_implementation { i32 (i8*, i64, i8*, i8*)* @stream_ref, i32 (i8*, i8*, i64, i8*, i64, i8*)* @stream_ref_xor_ic }, align 4

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ref(i8* %c, i64 %clen, i8* %n, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %in = alloca [16 x i8], align 16
  %block = alloca [64 x i8], align 16
  %kcopy = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  %u = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %k.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [32 x i8], [32 x i8]* %kcopy, i32 0, i32 %5
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %7, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %8 = load i8*, i8** %n.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx5, align 1
  %11 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %11
  store i8 %10, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %12 = load i32, i32* %i, align 4
  %inc8 = add i32 %12, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 8, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc14, %for.end9
  %13 = load i32, i32* %i, align 4
  %cmp11 = icmp ult i32 %13, 16
  br i1 %cmp11, label %for.body12, label %for.end16

for.body12:                                       ; preds = %for.cond10
  %14 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %14
  store i8 0, i8* %arrayidx13, align 1
  br label %for.inc14

for.inc14:                                        ; preds = %for.body12
  %15 = load i32, i32* %i, align 4
  %inc15 = add i32 %15, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond10

for.end16:                                        ; preds = %for.cond10
  br label %while.cond

while.cond:                                       ; preds = %for.end27, %for.end16
  %16 = load i64, i64* %clen.addr, align 8
  %cmp17 = icmp uge i64 %16, 64
  br i1 %cmp17, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %17 = load i8*, i8** %c.addr, align 4
  %arraydecay = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call = call i32 @crypto_core_salsa20(i8* %17, i8* %arraydecay, i8* %arraydecay18, i8* null)
  store i32 1, i32* %u, align 4
  store i32 8, i32* %i, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc25, %while.body
  %18 = load i32, i32* %i, align 4
  %cmp20 = icmp ult i32 %18, 16
  br i1 %cmp20, label %for.body21, label %for.end27

for.body21:                                       ; preds = %for.cond19
  %19 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %19
  %20 = load i8, i8* %arrayidx22, align 1
  %conv = zext i8 %20 to i32
  %21 = load i32, i32* %u, align 4
  %add = add i32 %21, %conv
  store i32 %add, i32* %u, align 4
  %22 = load i32, i32* %u, align 4
  %conv23 = trunc i32 %22 to i8
  %23 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %23
  store i8 %conv23, i8* %arrayidx24, align 1
  %24 = load i32, i32* %u, align 4
  %shr = lshr i32 %24, 8
  store i32 %shr, i32* %u, align 4
  br label %for.inc25

for.inc25:                                        ; preds = %for.body21
  %25 = load i32, i32* %i, align 4
  %inc26 = add i32 %25, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond19

for.end27:                                        ; preds = %for.cond19
  %26 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %26, 64
  store i64 %sub, i64* %clen.addr, align 8
  %27 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %27, i32 64
  store i8* %add.ptr, i8** %c.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i64, i64* %clen.addr, align 8
  %tobool28 = icmp ne i64 %28, 0
  br i1 %tobool28, label %if.then29, label %if.end44

if.then29:                                        ; preds = %while.end
  %arraydecay30 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay31 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay32 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call33 = call i32 @crypto_core_salsa20(i8* %arraydecay30, i8* %arraydecay31, i8* %arraydecay32, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc41, %if.then29
  %29 = load i32, i32* %i, align 4
  %30 = load i64, i64* %clen.addr, align 8
  %conv35 = trunc i64 %30 to i32
  %cmp36 = icmp ult i32 %29, %conv35
  br i1 %cmp36, label %for.body38, label %for.end43

for.body38:                                       ; preds = %for.cond34
  %31 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %31
  %32 = load i8, i8* %arrayidx39, align 1
  %33 = load i8*, i8** %c.addr, align 4
  %34 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr i8, i8* %33, i32 %34
  store i8 %32, i8* %arrayidx40, align 1
  br label %for.inc41

for.inc41:                                        ; preds = %for.body38
  %35 = load i32, i32* %i, align 4
  %inc42 = add i32 %35, 1
  store i32 %inc42, i32* %i, align 4
  br label %for.cond34

for.end43:                                        ; preds = %for.cond34
  br label %if.end44

if.end44:                                         ; preds = %for.end43, %while.end
  %arraydecay45 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay45, i32 64)
  %arraydecay46 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay46, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end44, %if.then
  %36 = load i32, i32* %retval, align 4
  ret i32 %36
}

; Function Attrs: noinline nounwind optnone
define internal i32 @stream_ref_xor_ic(i8* %c, i8* %m, i64 %mlen, i8* %n, i64 %ic, i8* %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %ic.addr = alloca i64, align 8
  %k.addr = alloca i8*, align 4
  %in = alloca [16 x i8], align 16
  %block = alloca [64 x i8], align 16
  %kcopy = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  %u = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i64 %ic, i64* %ic.addr, align 8
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 32
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %k.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr [32 x i8], [32 x i8]* %kcopy, i32 0, i32 %5
  store i8 %4, i8* %arrayidx1, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %7, 8
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %8 = load i8*, i8** %n.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %8, i32 %9
  %10 = load i8, i8* %arrayidx5, align 1
  %11 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %11
  store i8 %10, i8* %arrayidx6, align 1
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %12 = load i32, i32* %i, align 4
  %inc8 = add i32 %12, 1
  store i32 %inc8, i32* %i, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  store i32 8, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc14, %for.end9
  %13 = load i32, i32* %i, align 4
  %cmp11 = icmp ult i32 %13, 16
  br i1 %cmp11, label %for.body12, label %for.end16

for.body12:                                       ; preds = %for.cond10
  %14 = load i64, i64* %ic.addr, align 8
  %and = and i64 %14, 255
  %conv = trunc i64 %and to i8
  %15 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %15
  store i8 %conv, i8* %arrayidx13, align 1
  %16 = load i64, i64* %ic.addr, align 8
  %shr = lshr i64 %16, 8
  store i64 %shr, i64* %ic.addr, align 8
  br label %for.inc14

for.inc14:                                        ; preds = %for.body12
  %17 = load i32, i32* %i, align 4
  %inc15 = add i32 %17, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond10

for.end16:                                        ; preds = %for.cond10
  br label %while.cond

while.cond:                                       ; preds = %for.end45, %for.end16
  %18 = load i64, i64* %mlen.addr, align 8
  %cmp17 = icmp uge i64 %18, 64
  br i1 %cmp17, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay19 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay20 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call = call i32 @crypto_core_salsa20(i8* %arraydecay, i8* %arraydecay19, i8* %arraydecay20, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc31, %while.body
  %19 = load i32, i32* %i, align 4
  %cmp22 = icmp ult i32 %19, 64
  br i1 %cmp22, label %for.body24, label %for.end33

for.body24:                                       ; preds = %for.cond21
  %20 = load i8*, i8** %m.addr, align 4
  %21 = load i32, i32* %i, align 4
  %arrayidx25 = getelementptr i8, i8* %20, i32 %21
  %22 = load i8, i8* %arrayidx25, align 1
  %conv26 = zext i8 %22 to i32
  %23 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %23
  %24 = load i8, i8* %arrayidx27, align 1
  %conv28 = zext i8 %24 to i32
  %xor = xor i32 %conv26, %conv28
  %conv29 = trunc i32 %xor to i8
  %25 = load i8*, i8** %c.addr, align 4
  %26 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr i8, i8* %25, i32 %26
  store i8 %conv29, i8* %arrayidx30, align 1
  br label %for.inc31

for.inc31:                                        ; preds = %for.body24
  %27 = load i32, i32* %i, align 4
  %inc32 = add i32 %27, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond21

for.end33:                                        ; preds = %for.cond21
  store i32 1, i32* %u, align 4
  store i32 8, i32* %i, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc43, %for.end33
  %28 = load i32, i32* %i, align 4
  %cmp35 = icmp ult i32 %28, 16
  br i1 %cmp35, label %for.body37, label %for.end45

for.body37:                                       ; preds = %for.cond34
  %29 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %29
  %30 = load i8, i8* %arrayidx38, align 1
  %conv39 = zext i8 %30 to i32
  %31 = load i32, i32* %u, align 4
  %add = add i32 %31, %conv39
  store i32 %add, i32* %u, align 4
  %32 = load i32, i32* %u, align 4
  %conv40 = trunc i32 %32 to i8
  %33 = load i32, i32* %i, align 4
  %arrayidx41 = getelementptr [16 x i8], [16 x i8]* %in, i32 0, i32 %33
  store i8 %conv40, i8* %arrayidx41, align 1
  %34 = load i32, i32* %u, align 4
  %shr42 = lshr i32 %34, 8
  store i32 %shr42, i32* %u, align 4
  br label %for.inc43

for.inc43:                                        ; preds = %for.body37
  %35 = load i32, i32* %i, align 4
  %inc44 = add i32 %35, 1
  store i32 %inc44, i32* %i, align 4
  br label %for.cond34

for.end45:                                        ; preds = %for.cond34
  %36 = load i64, i64* %mlen.addr, align 8
  %sub = sub i64 %36, 64
  store i64 %sub, i64* %mlen.addr, align 8
  %37 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %37, i32 64
  store i8* %add.ptr, i8** %c.addr, align 4
  %38 = load i8*, i8** %m.addr, align 4
  %add.ptr46 = getelementptr i8, i8* %38, i32 64
  store i8* %add.ptr46, i8** %m.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %39 = load i64, i64* %mlen.addr, align 8
  %tobool47 = icmp ne i64 %39, 0
  br i1 %tobool47, label %if.then48, label %if.end68

if.then48:                                        ; preds = %while.end
  %arraydecay49 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  %arraydecay50 = getelementptr inbounds [16 x i8], [16 x i8]* %in, i32 0, i32 0
  %arraydecay51 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  %call52 = call i32 @crypto_core_salsa20(i8* %arraydecay49, i8* %arraydecay50, i8* %arraydecay51, i8* null)
  store i32 0, i32* %i, align 4
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc65, %if.then48
  %40 = load i32, i32* %i, align 4
  %41 = load i64, i64* %mlen.addr, align 8
  %conv54 = trunc i64 %41 to i32
  %cmp55 = icmp ult i32 %40, %conv54
  br i1 %cmp55, label %for.body57, label %for.end67

for.body57:                                       ; preds = %for.cond53
  %42 = load i8*, i8** %m.addr, align 4
  %43 = load i32, i32* %i, align 4
  %arrayidx58 = getelementptr i8, i8* %42, i32 %43
  %44 = load i8, i8* %arrayidx58, align 1
  %conv59 = zext i8 %44 to i32
  %45 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr [64 x i8], [64 x i8]* %block, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx60, align 1
  %conv61 = zext i8 %46 to i32
  %xor62 = xor i32 %conv59, %conv61
  %conv63 = trunc i32 %xor62 to i8
  %47 = load i8*, i8** %c.addr, align 4
  %48 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr i8, i8* %47, i32 %48
  store i8 %conv63, i8* %arrayidx64, align 1
  br label %for.inc65

for.inc65:                                        ; preds = %for.body57
  %49 = load i32, i32* %i, align 4
  %inc66 = add i32 %49, 1
  store i32 %inc66, i32* %i, align 4
  br label %for.cond53

for.end67:                                        ; preds = %for.cond53
  br label %if.end68

if.end68:                                         ; preds = %for.end67, %while.end
  %arraydecay69 = getelementptr inbounds [64 x i8], [64 x i8]* %block, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay69, i32 64)
  %arraydecay70 = getelementptr inbounds [32 x i8], [32 x i8]* %kcopy, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay70, i32 32)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end68, %if.then
  %50 = load i32, i32* %retval, align 4
  ret i32 %50
}

declare i32 @crypto_core_salsa20(i8*, i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
