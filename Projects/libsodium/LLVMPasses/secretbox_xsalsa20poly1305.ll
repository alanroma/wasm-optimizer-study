; ModuleID = 'crypto_secretbox/xsalsa20poly1305/secretbox_xsalsa20poly1305.c'
source_filename = "crypto_secretbox/xsalsa20poly1305/secretbox_xsalsa20poly1305.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305(i8* nonnull %c, i8* %m, i64 %mlen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %c.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %c, i8** %c.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %mlen.addr, align 8
  %cmp = icmp ult i64 %0, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %c.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %n.addr, align 4
  %5 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_xsalsa20_xor(i8* %1, i8* %2, i64 %3, i8* %4, i8* %5)
  %6 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %6, i32 16
  %7 = load i8*, i8** %c.addr, align 4
  %add.ptr1 = getelementptr i8, i8* %7, i32 32
  %8 = load i64, i64* %mlen.addr, align 8
  %sub = sub i64 %8, 32
  %9 = load i8*, i8** %c.addr, align 4
  %call2 = call i32 @crypto_onetimeauth_poly1305(i8* %add.ptr, i8* %add.ptr1, i64 %sub, i8* %9)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4
  %cmp3 = icmp slt i32 %10, 16
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i8*, i8** %c.addr, align 4
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %11, i32 %12
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i32 @crypto_stream_xsalsa20_xor(i8*, i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305(i8*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_open(i8* %m, i8* nonnull %c, i64 %clen, i8* nonnull %n, i8* nonnull %k) #0 {
entry:
  %retval = alloca i32, align 4
  %m.addr = alloca i8*, align 4
  %c.addr = alloca i8*, align 4
  %clen.addr = alloca i64, align 8
  %n.addr = alloca i8*, align 4
  %k.addr = alloca i8*, align 4
  %subkey = alloca [32 x i8], align 16
  %i = alloca i32, align 4
  store i8* %m, i8** %m.addr, align 4
  store i8* %c, i8** %c.addr, align 4
  store i64 %clen, i64* %clen.addr, align 8
  store i8* %n, i8** %n.addr, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i64, i64* %clen.addr, align 8
  %cmp = icmp ult i64 %0, 32
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %1 = load i8*, i8** %n.addr, align 4
  %2 = load i8*, i8** %k.addr, align 4
  %call = call i32 @crypto_stream_xsalsa20(i8* %arraydecay, i64 32, i8* %1, i8* %2)
  %3 = load i8*, i8** %c.addr, align 4
  %add.ptr = getelementptr i8, i8* %3, i32 16
  %4 = load i8*, i8** %c.addr, align 4
  %add.ptr1 = getelementptr i8, i8* %4, i32 32
  %5 = load i64, i64* %clen.addr, align 8
  %sub = sub i64 %5, 32
  %arraydecay2 = getelementptr inbounds [32 x i8], [32 x i8]* %subkey, i32 0, i32 0
  %call3 = call i32 @crypto_onetimeauth_poly1305_verify(i8* %add.ptr, i8* %add.ptr1, i64 %sub, i8* %arraydecay2)
  %cmp4 = icmp ne i32 %call3, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %6 = load i8*, i8** %m.addr, align 4
  %7 = load i8*, i8** %c.addr, align 4
  %8 = load i64, i64* %clen.addr, align 8
  %9 = load i8*, i8** %n.addr, align 4
  %10 = load i8*, i8** %k.addr, align 4
  %call7 = call i32 @crypto_stream_xsalsa20_xor(i8* %6, i8* %7, i64 %8, i8* %9, i8* %10)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %11 = load i32, i32* %i, align 4
  %cmp8 = icmp slt i32 %11, 32
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load i8*, i8** %m.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %12, i32 %13
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then5, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

declare i32 @crypto_stream_xsalsa20(i8*, i64, i8*, i8*) #1

declare i32 @crypto_onetimeauth_poly1305_verify(i8*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_keybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_noncebytes() #0 {
entry:
  ret i32 24
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_zerobytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_boxzerobytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_macbytes() #0 {
entry:
  ret i32 16
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_secretbox_xsalsa20poly1305_messagebytes_max() #0 {
entry:
  ret i32 -17
}

; Function Attrs: noinline nounwind optnone
define void @crypto_secretbox_xsalsa20poly1305_keygen(i8* nonnull %k) #0 {
entry:
  %k.addr = alloca i8*, align 4
  store i8* %k, i8** %k.addr, align 4
  %0 = load i8*, i8** %k.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
