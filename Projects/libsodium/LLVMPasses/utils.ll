; ModuleID = 'sodium/utils.c'
source_filename = "sodium/utils.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@page_size = internal global i32 16384, align 4
@canary = internal global [16 x i8] zeroinitializer, align 16
@.str = private unnamed_addr constant [60 x i8] c"_unprotected_ptr_from_user_ptr(user_ptr) == unprotected_ptr\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"sodium/utils.c\00", align 1
@__func__._sodium_malloc = private unnamed_addr constant [15 x i8] c"_sodium_malloc\00", align 1

; Function Attrs: noinline nounwind optnone
define void @sodium_memzero(i8* %pnt, i32 %len) #0 {
entry:
  %pnt.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %pnt_ = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %pnt, i8** %pnt.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i8*, i8** %pnt.addr, align 4
  store volatile i8* %0, i8** %pnt_, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %len.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load volatile i8*, i8** %pnt_, align 4
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %3, i32 %4
  store volatile i8 0, i8* %arrayidx, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @sodium_stackzero(i32 %len) #0 {
entry:
  %len.addr = alloca i32, align 4
  %saved_stack = alloca i8*, align 4
  %__vla_expr0 = alloca i32, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i32, i32* %len.addr, align 4
  %1 = call i8* @llvm.stacksave()
  store i8* %1, i8** %saved_stack, align 4
  %vla = alloca i8, i32 %0, align 16
  store i32 %0, i32* %__vla_expr0, align 4
  %2 = load i32, i32* %len.addr, align 4
  call void @sodium_memzero(i8* %vla, i32 %2)
  %3 = load i8*, i8** %saved_stack, align 4
  call void @llvm.stackrestore(i8* %3)
  ret void
}

; Function Attrs: nounwind
declare i8* @llvm.stacksave() #1

; Function Attrs: nounwind
declare void @llvm.stackrestore(i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @sodium_memcmp(i8* %b1_, i8* %b2_, i32 %len) #0 {
entry:
  %b1_.addr = alloca i8*, align 4
  %b2_.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %b1 = alloca i8*, align 4
  %b2 = alloca i8*, align 4
  %i = alloca i32, align 4
  %d = alloca i8, align 1
  store i8* %b1_, i8** %b1_.addr, align 4
  store i8* %b2_, i8** %b2_.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i8*, i8** %b1_.addr, align 4
  store volatile i8* %0, i8** %b1, align 4
  %1 = load i8*, i8** %b2_.addr, align 4
  store volatile i8* %1, i8** %b2, align 4
  store volatile i8 0, i8* %d, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %len.addr, align 4
  %cmp = icmp ult i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load volatile i8*, i8** %b1, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %4, i32 %5
  %6 = load volatile i8, i8* %arrayidx, align 1
  %conv = zext i8 %6 to i32
  %7 = load volatile i8*, i8** %b2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %7, i32 %8
  %9 = load volatile i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %9 to i32
  %xor = xor i32 %conv, %conv2
  %10 = load volatile i8, i8* %d, align 1
  %conv3 = zext i8 %10 to i32
  %or = or i32 %conv3, %xor
  %conv4 = trunc i32 %or to i8
  store volatile i8 %conv4, i8* %d, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load volatile i8, i8* %d, align 1
  %conv5 = zext i8 %12 to i32
  %sub = sub i32 %conv5, 1
  %shr = ashr i32 %sub, 8
  %and = and i32 1, %shr
  %sub6 = sub i32 %and, 1
  ret i32 %sub6
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_compare(i8* %b1_, i8* %b2_, i32 %len) #0 {
entry:
  %b1_.addr = alloca i8*, align 4
  %b2_.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %b1 = alloca i8*, align 4
  %b2 = alloca i8*, align 4
  %i = alloca i32, align 4
  %gt = alloca i8, align 1
  %eq = alloca i8, align 1
  %x1 = alloca i16, align 2
  %x2 = alloca i16, align 2
  store i8* %b1_, i8** %b1_.addr, align 4
  store i8* %b2_, i8** %b2_.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i8*, i8** %b1_.addr, align 4
  store volatile i8* %0, i8** %b1, align 4
  %1 = load i8*, i8** %b2_.addr, align 4
  store volatile i8* %1, i8** %b2, align 4
  store volatile i8 0, i8* %gt, align 1
  store volatile i8 1, i8* %eq, align 1
  %2 = load i32, i32* %len.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp ne i32 %3, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i32, i32* %i, align 4
  %dec = add i32 %4, -1
  store i32 %dec, i32* %i, align 4
  %5 = load volatile i8*, i8** %b1, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %5, i32 %6
  %7 = load volatile i8, i8* %arrayidx, align 1
  %conv = zext i8 %7 to i16
  store i16 %conv, i16* %x1, align 2
  %8 = load volatile i8*, i8** %b2, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %8, i32 %9
  %10 = load volatile i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %10 to i16
  store i16 %conv2, i16* %x2, align 2
  %11 = load i16, i16* %x2, align 2
  %conv3 = zext i16 %11 to i32
  %12 = load i16, i16* %x1, align 2
  %conv4 = zext i16 %12 to i32
  %sub = sub i32 %conv3, %conv4
  %shr = ashr i32 %sub, 8
  %13 = load volatile i8, i8* %eq, align 1
  %conv5 = zext i8 %13 to i32
  %and = and i32 %shr, %conv5
  %14 = load volatile i8, i8* %gt, align 1
  %conv6 = zext i8 %14 to i32
  %or = or i32 %conv6, %and
  %conv7 = trunc i32 %or to i8
  store volatile i8 %conv7, i8* %gt, align 1
  %15 = load i16, i16* %x2, align 2
  %conv8 = zext i16 %15 to i32
  %16 = load i16, i16* %x1, align 2
  %conv9 = zext i16 %16 to i32
  %xor = xor i32 %conv8, %conv9
  %sub10 = sub i32 %xor, 1
  %shr11 = ashr i32 %sub10, 8
  %17 = load volatile i8, i8* %eq, align 1
  %conv12 = zext i8 %17 to i32
  %and13 = and i32 %conv12, %shr11
  %conv14 = trunc i32 %and13 to i8
  store volatile i8 %conv14, i8* %eq, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %18 = load volatile i8, i8* %gt, align 1
  %conv15 = zext i8 %18 to i32
  %19 = load volatile i8, i8* %gt, align 1
  %conv16 = zext i8 %19 to i32
  %add = add i32 %conv15, %conv16
  %20 = load volatile i8, i8* %eq, align 1
  %conv17 = zext i8 %20 to i32
  %add18 = add i32 %add, %conv17
  %sub19 = sub i32 %add18, 1
  ret i32 %sub19
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_is_zero(i8* %n, i32 %nlen) #0 {
entry:
  %n.addr = alloca i8*, align 4
  %nlen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %d = alloca i8, align 1
  store i8* %n, i8** %n.addr, align 4
  store i32 %nlen, i32* %nlen.addr, align 4
  store volatile i8 0, i8* %d, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %nlen.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load volatile i8, i8* %d, align 1
  %conv1 = zext i8 %5 to i32
  %or = or i32 %conv1, %conv
  %conv2 = trunc i32 %or to i8
  store volatile i8 %conv2, i8* %d, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load volatile i8, i8* %d, align 1
  %conv3 = zext i8 %7 to i32
  %sub = sub i32 %conv3, 1
  %shr = ashr i32 %sub, 8
  %and = and i32 1, %shr
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define void @sodium_increment(i8* %n, i32 %nlen) #0 {
entry:
  %n.addr = alloca i8*, align 4
  %nlen.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %n, i8** %n.addr, align 4
  store i32 %nlen, i32* %nlen.addr, align 4
  store i32 0, i32* %i, align 4
  store i32 1, i32* %c, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %nlen.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %n.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i32, i32* %c, align 4
  %add = add i32 %5, %conv
  store i32 %add, i32* %c, align 4
  %6 = load i32, i32* %c, align 4
  %conv1 = trunc i32 %6 to i8
  %7 = load i8*, i8** %n.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr i8, i8* %7, i32 %8
  store i8 %conv1, i8* %arrayidx2, align 1
  %9 = load i32, i32* %c, align 4
  %shr = lshr i32 %9, 8
  store i32 %shr, i32* %c, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @sodium_add(i8* %a, i8* %b, i32 %len) #0 {
entry:
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %c = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4
  store i8* %b, i8** %b.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %c, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %a.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %b.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %7 to i32
  %add = add i32 %conv, %conv2
  %8 = load i32, i32* %c, align 4
  %add3 = add i32 %8, %add
  store i32 %add3, i32* %c, align 4
  %9 = load i32, i32* %c, align 4
  %conv4 = trunc i32 %9 to i8
  %10 = load i8*, i8** %a.addr, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %10, i32 %11
  store i8 %conv4, i8* %arrayidx5, align 1
  %12 = load i32, i32* %c, align 4
  %shr = lshr i32 %12, 8
  store i32 %shr, i32* %c, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @sodium_sub(i8* %a, i8* %b, i32 %len) #0 {
entry:
  %a.addr = alloca i8*, align 4
  %b.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  %c = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %a, i8** %a.addr, align 4
  store i8* %b, i8** %b.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %c, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %a.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i32
  %5 = load i8*, i8** %b.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %7 to i32
  %sub = sub i32 %conv, %conv2
  %8 = load i32, i32* %c, align 4
  %sub3 = sub i32 %sub, %8
  store i32 %sub3, i32* %c, align 4
  %9 = load i32, i32* %c, align 4
  %conv4 = trunc i32 %9 to i8
  %10 = load i8*, i8** %a.addr, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr i8, i8* %10, i32 %11
  store i8 %conv4, i8* %arrayidx5, align 1
  %12 = load i32, i32* %c, align 4
  %shr = lshr i32 %12, 8
  %and = and i32 %shr, 1
  store i32 %and, i32* %c, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_sodium_alloc_init() #0 {
entry:
  %page_size_ = alloca i32, align 4
  %call = call i32 @sysconf(i32 30)
  store i32 %call, i32* %page_size_, align 4
  %0 = load i32, i32* %page_size_, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %page_size_, align 4
  store i32 %1, i32* @page_size, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* @page_size, align 4
  %cmp1 = icmp ult i32 %2, 16
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i32, i32* @page_size, align 4
  %cmp2 = icmp ult i32 %3, 4
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  call void @sodium_misuse() #7
  unreachable

if.end4:                                          ; preds = %lor.lhs.false
  call void @randombytes_buf(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @canary, i32 0, i32 0), i32 16)
  ret i32 0
}

declare i32 @sysconf(i32) #2

; Function Attrs: noreturn
declare void @sodium_misuse() #3

declare void @randombytes_buf(i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define i32 @sodium_mlock(i8* nonnull %addr, i32 %len) #0 {
entry:
  %addr.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  store i8* %addr, i8** %addr.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

declare i32* @__errno_location() #2

; Function Attrs: noinline nounwind optnone
define i32 @sodium_munlock(i8* nonnull %addr, i32 %len) #0 {
entry:
  %addr.addr = alloca i8*, align 4
  %len.addr = alloca i32, align 4
  store i8* %addr, i8** %addr.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  %0 = load i8*, i8** %addr.addr, align 4
  %1 = load i32, i32* %len.addr, align 4
  call void @sodium_memzero(i8* %0, i32 %1)
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define noalias i8* @sodium_malloc(i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %call = call noalias i8* @_sodium_malloc(i32 %0)
  store i8* %call, i8** %ptr, align 4
  %cmp = icmp eq i8* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %ptr, align 4
  %2 = load i32, i32* %size.addr, align 4
  call void @llvm.memset.p0i8.i32(i8* align 1 %1, i8 -37, i32 %2, i1 false)
  %3 = load i8*, i8** %ptr, align 4
  store i8* %3, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i8*, i8** %retval, align 4
  ret i8* %4
}

; Function Attrs: noinline nounwind optnone
define internal noalias i8* @_sodium_malloc(i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %user_ptr = alloca i8*, align 4
  %base_ptr = alloca i8*, align 4
  %canary_ptr = alloca i8*, align 4
  %unprotected_ptr = alloca i8*, align 4
  %size_with_canary = alloca i32, align 4
  %total_size = alloca i32, align 4
  %unprotected_size = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i32, i32* @page_size, align 4
  %mul = mul i32 %1, 4
  %sub = sub i32 -1, %mul
  %cmp = icmp uge i32 %0, %sub
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32* @__errno_location()
  store i32 48, i32* %call, align 4
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* @page_size, align 4
  %cmp1 = icmp ule i32 %2, 16
  br i1 %cmp1, label %if.then3, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %3 = load i32, i32* @page_size, align 4
  %cmp2 = icmp ult i32 %3, 4
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %lor.lhs.false, %if.end
  call void @sodium_misuse() #7
  unreachable

if.end4:                                          ; preds = %lor.lhs.false
  %4 = load i32, i32* %size.addr, align 4
  %add = add i32 16, %4
  store i32 %add, i32* %size_with_canary, align 4
  %5 = load i32, i32* %size_with_canary, align 4
  %call5 = call i32 @_page_round(i32 %5)
  store i32 %call5, i32* %unprotected_size, align 4
  %6 = load i32, i32* @page_size, align 4
  %7 = load i32, i32* @page_size, align 4
  %add6 = add i32 %6, %7
  %8 = load i32, i32* %unprotected_size, align 4
  %add7 = add i32 %add6, %8
  %9 = load i32, i32* @page_size, align 4
  %add8 = add i32 %add7, %9
  store i32 %add8, i32* %total_size, align 4
  %10 = load i32, i32* %total_size, align 4
  %call9 = call noalias i8* @_alloc_aligned(i32 %10)
  store i8* %call9, i8** %base_ptr, align 4
  %cmp10 = icmp eq i8* %call9, null
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end4
  store i8* null, i8** %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end4
  %11 = load i8*, i8** %base_ptr, align 4
  %12 = load i32, i32* @page_size, align 4
  %mul13 = mul i32 %12, 2
  %add.ptr = getelementptr i8, i8* %11, i32 %mul13
  store i8* %add.ptr, i8** %unprotected_ptr, align 4
  %13 = load i8*, i8** %base_ptr, align 4
  %14 = load i32, i32* @page_size, align 4
  %add.ptr14 = getelementptr i8, i8* %13, i32 %14
  %15 = load i32, i32* @page_size, align 4
  %call15 = call i32 @_mprotect_noaccess(i8* %add.ptr14, i32 %15)
  %16 = load i8*, i8** %unprotected_ptr, align 4
  %17 = load i32, i32* %unprotected_size, align 4
  %add.ptr16 = getelementptr i8, i8* %16, i32 %17
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr16, i8* align 16 getelementptr inbounds ([16 x i8], [16 x i8]* @canary, i32 0, i32 0), i32 16, i1 false)
  %18 = load i8*, i8** %unprotected_ptr, align 4
  %19 = load i32, i32* %unprotected_size, align 4
  %add.ptr17 = getelementptr i8, i8* %18, i32 %19
  %20 = load i32, i32* @page_size, align 4
  %call18 = call i32 @_mprotect_noaccess(i8* %add.ptr17, i32 %20)
  %21 = load i8*, i8** %unprotected_ptr, align 4
  %22 = load i32, i32* %unprotected_size, align 4
  %call19 = call i32 @sodium_mlock(i8* %21, i32 %22)
  %23 = load i8*, i8** %unprotected_ptr, align 4
  %24 = load i32, i32* %size_with_canary, align 4
  %call20 = call i32 @_page_round(i32 %24)
  %add.ptr21 = getelementptr i8, i8* %23, i32 %call20
  %25 = load i32, i32* %size_with_canary, align 4
  %idx.neg = sub i32 0, %25
  %add.ptr22 = getelementptr i8, i8* %add.ptr21, i32 %idx.neg
  store i8* %add.ptr22, i8** %canary_ptr, align 4
  %26 = load i8*, i8** %canary_ptr, align 4
  %add.ptr23 = getelementptr i8, i8* %26, i32 16
  store i8* %add.ptr23, i8** %user_ptr, align 4
  %27 = load i8*, i8** %canary_ptr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %27, i8* align 16 getelementptr inbounds ([16 x i8], [16 x i8]* @canary, i32 0, i32 0), i32 16, i1 false)
  %28 = load i8*, i8** %base_ptr, align 4
  %29 = bitcast i32* %unprotected_size to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %28, i8* align 4 %29, i32 4, i1 false)
  %30 = load i8*, i8** %base_ptr, align 4
  %31 = load i32, i32* @page_size, align 4
  %call24 = call i32 @_mprotect_readonly(i8* %30, i32 %31)
  %32 = load i8*, i8** %user_ptr, align 4
  %call25 = call i8* @_unprotected_ptr_from_user_ptr(i8* %32)
  %33 = load i8*, i8** %unprotected_ptr, align 4
  %cmp26 = icmp eq i8* %call25, %33
  br i1 %cmp26, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end12
  call void @__assert_fail(i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str, i32 0, i32 0), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0), i32 620, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @__func__._sodium_malloc, i32 0, i32 0)) #7
  unreachable

34:                                               ; No predecessors!
  br label %lor.end

lor.end:                                          ; preds = %34, %if.end12
  %35 = phi i1 [ true, %if.end12 ], [ false, %34 ]
  %lor.ext = zext i1 %35 to i32
  %36 = load i8*, i8** %user_ptr, align 4
  store i8* %36, i8** %retval, align 4
  br label %return

return:                                           ; preds = %lor.end, %if.then11, %if.then
  %37 = load i8*, i8** %retval, align 4
  ret i8* %37
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define noalias i8* @sodium_allocarray(i32 %count, i32 %size) #0 {
entry:
  %retval = alloca i8*, align 4
  %count.addr = alloca i32, align 4
  %size.addr = alloca i32, align 4
  store i32 %count, i32* %count.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %count.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %2 = load i32, i32* %count.addr, align 4
  %div = udiv i32 -1, %2
  %cmp1 = icmp uge i32 %1, %div
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %call = call i32* @__errno_location()
  store i32 48, i32* %call, align 4
  store i8* null, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %count.addr, align 4
  %4 = load i32, i32* %size.addr, align 4
  %mul = mul i32 %3, %4
  %call2 = call noalias i8* @sodium_malloc(i32 %mul)
  store i8* %call2, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i8*, i8** %retval, align 4
  ret i8* %5
}

; Function Attrs: noinline nounwind optnone
define void @sodium_free(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %base_ptr = alloca i8*, align 4
  %canary_ptr = alloca i8*, align 4
  %unprotected_ptr = alloca i8*, align 4
  %total_size = alloca i32, align 4
  %unprotected_size = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4
  %add.ptr = getelementptr i8, i8* %1, i32 -16
  store i8* %add.ptr, i8** %canary_ptr, align 4
  %2 = load i8*, i8** %ptr.addr, align 4
  %call = call i8* @_unprotected_ptr_from_user_ptr(i8* %2)
  store i8* %call, i8** %unprotected_ptr, align 4
  %3 = load i8*, i8** %unprotected_ptr, align 4
  %4 = load i32, i32* @page_size, align 4
  %mul = mul i32 %4, 2
  %idx.neg = sub i32 0, %mul
  %add.ptr1 = getelementptr i8, i8* %3, i32 %idx.neg
  store i8* %add.ptr1, i8** %base_ptr, align 4
  %5 = bitcast i32* %unprotected_size to i8*
  %6 = load i8*, i8** %base_ptr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 1 %6, i32 4, i1 false)
  %7 = load i32, i32* @page_size, align 4
  %8 = load i32, i32* @page_size, align 4
  %add = add i32 %7, %8
  %9 = load i32, i32* %unprotected_size, align 4
  %add2 = add i32 %add, %9
  %10 = load i32, i32* @page_size, align 4
  %add3 = add i32 %add2, %10
  store i32 %add3, i32* %total_size, align 4
  %11 = load i8*, i8** %base_ptr, align 4
  %12 = load i32, i32* %total_size, align 4
  %call4 = call i32 @_mprotect_readwrite(i8* %11, i32 %12)
  %13 = load i8*, i8** %canary_ptr, align 4
  %call5 = call i32 @sodium_memcmp(i8* %13, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @canary, i32 0, i32 0), i32 16)
  %cmp6 = icmp ne i32 %call5, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  call void @_out_of_bounds() #7
  unreachable

if.end8:                                          ; preds = %if.end
  %14 = load i8*, i8** %unprotected_ptr, align 4
  %15 = load i32, i32* %unprotected_size, align 4
  %add.ptr9 = getelementptr i8, i8* %14, i32 %15
  %call10 = call i32 @sodium_memcmp(i8* %add.ptr9, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @canary, i32 0, i32 0), i32 16)
  %cmp11 = icmp ne i32 %call10, 0
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end8
  call void @_out_of_bounds() #7
  unreachable

if.end13:                                         ; preds = %if.end8
  %16 = load i8*, i8** %unprotected_ptr, align 4
  %17 = load i32, i32* %unprotected_size, align 4
  %call14 = call i32 @sodium_munlock(i8* %16, i32 %17)
  %18 = load i8*, i8** %base_ptr, align 4
  %19 = load i32, i32* %total_size, align 4
  call void @_free_aligned(i8* %18, i32 %19)
  br label %return

return:                                           ; preds = %if.end13, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i8* @_unprotected_ptr_from_user_ptr(i8* %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %unprotected_ptr_u = alloca i32, align 4
  %canary_ptr = alloca i8*, align 4
  %page_mask = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %add.ptr = getelementptr i8, i8* %0, i32 -16
  store i8* %add.ptr, i8** %canary_ptr, align 4
  %1 = load i32, i32* @page_size, align 4
  %sub = sub i32 %1, 1
  store i32 %sub, i32* %page_mask, align 4
  %2 = load i8*, i8** %canary_ptr, align 4
  %3 = ptrtoint i8* %2 to i32
  %4 = load i32, i32* %page_mask, align 4
  %neg = xor i32 %4, -1
  %and = and i32 %3, %neg
  store i32 %and, i32* %unprotected_ptr_u, align 4
  %5 = load i32, i32* %unprotected_ptr_u, align 4
  %6 = load i32, i32* @page_size, align 4
  %mul = mul i32 %6, 2
  %cmp = icmp ule i32 %5, %mul
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @sodium_misuse() #7
  unreachable

if.end:                                           ; preds = %entry
  %7 = load i32, i32* %unprotected_ptr_u, align 4
  %8 = inttoptr i32 %7 to i8*
  ret i8* %8
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define internal i32 @_mprotect_readwrite(i8* %ptr, i32 %size) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline noreturn nounwind optnone
define internal void @_out_of_bounds() #6 {
entry:
  call void @abort() #7
  unreachable
}

; Function Attrs: noinline nounwind optnone
define internal void @_free_aligned(i8* %ptr, i32 %size) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @free(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_mprotect_noaccess(i8* nonnull %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %call = call i32 @_sodium_mprotect(i8* %0, i32 (i8*, i32)* @_mprotect_noaccess)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_sodium_mprotect(i8* %ptr, i32 (i8*, i32)* %cb) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %cb.addr = alloca i32 (i8*, i32)*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 (i8*, i32)* %cb, i32 (i8*, i32)** %cb.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %1 = load i32 (i8*, i32)*, i32 (i8*, i32)** %cb.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_mprotect_noaccess(i8* %ptr, i32 %size) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_mprotect_readonly(i8* nonnull %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %call = call i32 @_sodium_mprotect(i8* %0, i32 (i8*, i32)* @_mprotect_readonly)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_mprotect_readonly(i8* %ptr, i32 %size) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %call = call i32* @__errno_location()
  store i32 52, i32* %call, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_mprotect_readwrite(i8* nonnull %ptr) #0 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %call = call i32 @_sodium_mprotect(i8* %0, i32 (i8*, i32)* @_mprotect_readwrite)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_pad(i32* %padded_buflen_p, i8* nonnull %buf, i32 %unpadded_buflen, i32 %blocksize, i32 %max_buflen) #0 {
entry:
  %retval = alloca i32, align 4
  %padded_buflen_p.addr = alloca i32*, align 4
  %buf.addr = alloca i8*, align 4
  %unpadded_buflen.addr = alloca i32, align 4
  %blocksize.addr = alloca i32, align 4
  %max_buflen.addr = alloca i32, align 4
  %tail = alloca i8*, align 4
  %i = alloca i32, align 4
  %xpadlen = alloca i32, align 4
  %xpadded_len = alloca i32, align 4
  %mask = alloca i8, align 1
  %barrier_mask = alloca i8, align 1
  store i32* %padded_buflen_p, i32** %padded_buflen_p.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %unpadded_buflen, i32* %unpadded_buflen.addr, align 4
  store i32 %blocksize, i32* %blocksize.addr, align 4
  store i32 %max_buflen, i32* %max_buflen.addr, align 4
  %0 = load i32, i32* %blocksize.addr, align 4
  %cmp = icmp ule i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %blocksize.addr, align 4
  %sub = sub i32 %1, 1
  store i32 %sub, i32* %xpadlen, align 4
  %2 = load i32, i32* %blocksize.addr, align 4
  %3 = load i32, i32* %blocksize.addr, align 4
  %sub1 = sub i32 %3, 1
  %and = and i32 %2, %sub1
  %cmp2 = icmp eq i32 %and, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %4 = load i32, i32* %unpadded_buflen.addr, align 4
  %5 = load i32, i32* %blocksize.addr, align 4
  %sub4 = sub i32 %5, 1
  %and5 = and i32 %4, %sub4
  %6 = load i32, i32* %xpadlen, align 4
  %sub6 = sub i32 %6, %and5
  store i32 %sub6, i32* %xpadlen, align 4
  br label %if.end8

if.else:                                          ; preds = %if.end
  %7 = load i32, i32* %unpadded_buflen.addr, align 4
  %8 = load i32, i32* %blocksize.addr, align 4
  %rem = urem i32 %7, %8
  %9 = load i32, i32* %xpadlen, align 4
  %sub7 = sub i32 %9, %rem
  store i32 %sub7, i32* %xpadlen, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then3
  %10 = load i32, i32* %unpadded_buflen.addr, align 4
  %sub9 = sub i32 -1, %10
  %11 = load i32, i32* %xpadlen, align 4
  %cmp10 = icmp ule i32 %sub9, %11
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end8
  call void @sodium_misuse() #7
  unreachable

if.end12:                                         ; preds = %if.end8
  %12 = load i32, i32* %unpadded_buflen.addr, align 4
  %13 = load i32, i32* %xpadlen, align 4
  %add = add i32 %12, %13
  store i32 %add, i32* %xpadded_len, align 4
  %14 = load i32, i32* %xpadded_len, align 4
  %15 = load i32, i32* %max_buflen.addr, align 4
  %cmp13 = icmp uge i32 %14, %15
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end12
  store i32 -1, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end12
  %16 = load i8*, i8** %buf.addr, align 4
  %17 = load i32, i32* %xpadded_len, align 4
  %arrayidx = getelementptr i8, i8* %16, i32 %17
  store i8* %arrayidx, i8** %tail, align 4
  %18 = load i32*, i32** %padded_buflen_p.addr, align 4
  %cmp16 = icmp ne i32* %18, null
  br i1 %cmp16, label %if.then17, label %if.end19

if.then17:                                        ; preds = %if.end15
  %19 = load i32, i32* %xpadded_len, align 4
  %add18 = add i32 %19, 1
  %20 = load i32*, i32** %padded_buflen_p.addr, align 4
  store i32 %add18, i32* %20, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then17, %if.end15
  store volatile i8 0, i8* %mask, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end19
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %blocksize.addr, align 4
  %cmp20 = icmp ult i32 %21, %22
  br i1 %cmp20, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %23 = load i32, i32* %i, align 4
  %24 = load i32, i32* %xpadlen, align 4
  %xor = xor i32 %23, %24
  %sub21 = sub i32 %xor, 1
  %shr = lshr i32 %sub21, 24
  %conv = trunc i32 %shr to i8
  store i8 %conv, i8* %barrier_mask, align 1
  %25 = load i8*, i8** %tail, align 4
  %26 = load i32, i32* %i, align 4
  %idx.neg = sub i32 0, %26
  %add.ptr = getelementptr i8, i8* %25, i32 %idx.neg
  %27 = load i8, i8* %add.ptr, align 1
  %conv22 = zext i8 %27 to i32
  %28 = load volatile i8, i8* %mask, align 1
  %conv23 = zext i8 %28 to i32
  %and24 = and i32 %conv22, %conv23
  %29 = load i8, i8* %barrier_mask, align 1
  %conv25 = zext i8 %29 to i32
  %and26 = and i32 128, %conv25
  %or = or i32 %and24, %and26
  %conv27 = trunc i32 %or to i8
  %30 = load i8*, i8** %tail, align 4
  %31 = load i32, i32* %i, align 4
  %idx.neg28 = sub i32 0, %31
  %add.ptr29 = getelementptr i8, i8* %30, i32 %idx.neg28
  store i8 %conv27, i8* %add.ptr29, align 1
  %32 = load i8, i8* %barrier_mask, align 1
  %conv30 = zext i8 %32 to i32
  %33 = load volatile i8, i8* %mask, align 1
  %conv31 = zext i8 %33 to i32
  %or32 = or i32 %conv31, %conv30
  %conv33 = trunc i32 %or32 to i8
  store volatile i8 %conv33, i8* %mask, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %34 = load i32, i32* %i, align 4
  %inc = add i32 %34, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then14, %if.then
  %35 = load i32, i32* %retval, align 4
  ret i32 %35
}

; Function Attrs: noinline nounwind optnone
define i32 @sodium_unpad(i32* %unpadded_buflen_p, i8* nonnull %buf, i32 %padded_buflen, i32 %blocksize) #0 {
entry:
  %retval = alloca i32, align 4
  %unpadded_buflen_p.addr = alloca i32*, align 4
  %buf.addr = alloca i8*, align 4
  %padded_buflen.addr = alloca i32, align 4
  %blocksize.addr = alloca i32, align 4
  %tail = alloca i8*, align 4
  %acc = alloca i8, align 1
  %c = alloca i8, align 1
  %valid = alloca i8, align 1
  %pad_len = alloca i32, align 4
  %i = alloca i32, align 4
  %is_barrier = alloca i32, align 4
  store i32* %unpadded_buflen_p, i32** %unpadded_buflen_p.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %padded_buflen, i32* %padded_buflen.addr, align 4
  store i32 %blocksize, i32* %blocksize.addr, align 4
  store i8 0, i8* %acc, align 1
  store i8 0, i8* %valid, align 1
  store volatile i32 0, i32* %pad_len, align 4
  %0 = load i32, i32* %padded_buflen.addr, align 4
  %1 = load i32, i32* %blocksize.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load i32, i32* %blocksize.addr, align 4
  %cmp1 = icmp ule i32 %2, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load i8*, i8** %buf.addr, align 4
  %4 = load i32, i32* %padded_buflen.addr, align 4
  %sub = sub i32 %4, 1
  %arrayidx = getelementptr i8, i8* %3, i32 %sub
  store i8* %arrayidx, i8** %tail, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %blocksize.addr, align 4
  %cmp2 = icmp ult i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %tail, align 4
  %8 = load i32, i32* %i, align 4
  %idx.neg = sub i32 0, %8
  %add.ptr = getelementptr i8, i8* %7, i32 %idx.neg
  %9 = load i8, i8* %add.ptr, align 1
  store i8 %9, i8* %c, align 1
  %10 = load i8, i8* %acc, align 1
  %conv = zext i8 %10 to i32
  %sub3 = sub i32 %conv, 1
  %11 = load volatile i32, i32* %pad_len, align 4
  %sub4 = sub i32 %11, 1
  %and = and i32 %sub3, %sub4
  %12 = load i8, i8* %c, align 1
  %conv5 = zext i8 %12 to i32
  %xor = xor i32 %conv5, 128
  %sub6 = sub i32 %xor, 1
  %and7 = and i32 %and, %sub6
  %shr = lshr i32 %and7, 8
  %and8 = and i32 %shr, 1
  store i32 %and8, i32* %is_barrier, align 4
  %13 = load i8, i8* %c, align 1
  %conv9 = zext i8 %13 to i32
  %14 = load i8, i8* %acc, align 1
  %conv10 = zext i8 %14 to i32
  %or = or i32 %conv10, %conv9
  %conv11 = trunc i32 %or to i8
  store i8 %conv11, i8* %acc, align 1
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %is_barrier, align 4
  %neg = xor i32 %16, -1
  %add = add i32 1, %neg
  %and12 = and i32 %15, %add
  %17 = load volatile i32, i32* %pad_len, align 4
  %or13 = or i32 %17, %and12
  store volatile i32 %or13, i32* %pad_len, align 4
  %18 = load i32, i32* %is_barrier, align 4
  %conv14 = trunc i32 %18 to i8
  %conv15 = zext i8 %conv14 to i32
  %19 = load i8, i8* %valid, align 1
  %conv16 = zext i8 %19 to i32
  %or17 = or i32 %conv16, %conv15
  %conv18 = trunc i32 %or17 to i8
  store i8 %conv18, i8* %valid, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %21 = load i32, i32* %padded_buflen.addr, align 4
  %sub19 = sub i32 %21, 1
  %22 = load volatile i32, i32* %pad_len, align 4
  %sub20 = sub i32 %sub19, %22
  %23 = load i32*, i32** %unpadded_buflen_p.addr, align 4
  store i32 %sub20, i32* %23, align 4
  %24 = load i8, i8* %valid, align 1
  %conv21 = zext i8 %24 to i32
  %sub22 = sub i32 %conv21, 1
  store i32 %sub22, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_page_round(i32 %size) #0 {
entry:
  %size.addr = alloca i32, align 4
  %page_mask = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* @page_size, align 4
  %sub = sub i32 %0, 1
  store i32 %sub, i32* %page_mask, align 4
  %1 = load i32, i32* %size.addr, align 4
  %2 = load i32, i32* %page_mask, align 4
  %add = add i32 %1, %2
  %3 = load i32, i32* %page_mask, align 4
  %neg = xor i32 %3, -1
  %and = and i32 %add, %neg
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define internal noalias i8* @_alloc_aligned(i32 %size) #0 {
entry:
  %size.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* @page_size, align 4
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32 @posix_memalign(i8** %ptr, i32 %0, i32 %1)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %ptr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i8*, i8** %ptr, align 4
  ret i8* %2
}

; Function Attrs: noreturn
declare void @__assert_fail(i8*, i8*, i32, i8*) #3

declare i32 @posix_memalign(i8**, i32, i32) #2

; Function Attrs: noreturn
declare void @abort() #3

declare void @free(i8*) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { noreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
