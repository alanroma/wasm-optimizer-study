; ModuleID = 'crypto_core/ed25519/core_ed25519.c'
source_filename = "crypto_core/ed25519/core_ed25519.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.ge25519_p3 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_p1p1 = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }
%struct.ge25519_cached = type { [10 x i32], [10 x i32], [10 x i32], [10 x i32] }

@L = internal constant [32 x i8] c"\ED\D3\F5\\\1Ac\12X\D6\9C\F7\A2\DE\F9\DE\14\00\00\00\00\00\00\00\00\00\00\00\00\00\00\00\10", align 16

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_is_valid_point(i8* nonnull %p) #0 {
entry:
  %retval = alloca i32, align 4
  %p.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  store i8* %p, i8** %p.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ge25519_is_canonical(i8* %0)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %p.addr, align 4
  %call1 = call i32 @ge25519_has_small_order(i8* %1)
  %cmp2 = icmp ne i32 %call1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %2 = load i8*, i8** %p.addr, align 4
  %call4 = call i32 @ge25519_frombytes(%struct.ge25519_p3* %p_p3, i8* %2)
  %cmp5 = icmp ne i32 %call4, 0
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %call7 = call i32 @ge25519_is_on_curve(%struct.ge25519_p3* %p_p3)
  %cmp8 = icmp eq i32 %call7, 0
  br i1 %cmp8, label %if.then, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %lor.lhs.false6
  %call10 = call i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3* %p_p3)
  %cmp11 = icmp eq i32 %call10, 0
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false9, %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false9
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare i32 @ge25519_is_canonical(i8*) #1

declare i32 @ge25519_has_small_order(i8*) #1

declare i32 @ge25519_frombytes(%struct.ge25519_p3*, i8*) #1

declare i32 @ge25519_is_on_curve(%struct.ge25519_p3*) #1

declare i32 @ge25519_is_on_main_subgroup(%struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_add(i8* nonnull %r, i8* nonnull %p, i8* nonnull %q) #0 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %q.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  %q_p3 = alloca %struct.ge25519_p3, align 4
  %r_p3 = alloca %struct.ge25519_p3, align 4
  %r_p1p1 = alloca %struct.ge25519_p1p1, align 4
  %q_cached = alloca %struct.ge25519_cached, align 4
  store i8* %r, i8** %r.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %q, i8** %q.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ge25519_frombytes(%struct.ge25519_p3* %p_p3, i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %call1 = call i32 @ge25519_is_on_curve(%struct.ge25519_p3* %p_p3)
  %cmp2 = icmp eq i32 %call1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %1 = load i8*, i8** %q.addr, align 4
  %call4 = call i32 @ge25519_frombytes(%struct.ge25519_p3* %q_p3, i8* %1)
  %cmp5 = icmp ne i32 %call4, 0
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %call7 = call i32 @ge25519_is_on_curve(%struct.ge25519_p3* %q_p3)
  %cmp8 = icmp eq i32 %call7, 0
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  call void @ge25519_p3_to_cached(%struct.ge25519_cached* %q_cached, %struct.ge25519_p3* %q_p3)
  call void @ge25519_add(%struct.ge25519_p1p1* %r_p1p1, %struct.ge25519_p3* %p_p3, %struct.ge25519_cached* %q_cached)
  call void @ge25519_p1p1_to_p3(%struct.ge25519_p3* %r_p3, %struct.ge25519_p1p1* %r_p1p1)
  %2 = load i8*, i8** %r.addr, align 4
  call void @ge25519_p3_tobytes(i8* %2, %struct.ge25519_p3* %r_p3)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare void @ge25519_p3_to_cached(%struct.ge25519_cached*, %struct.ge25519_p3*) #1

declare void @ge25519_add(%struct.ge25519_p1p1*, %struct.ge25519_p3*, %struct.ge25519_cached*) #1

declare void @ge25519_p1p1_to_p3(%struct.ge25519_p3*, %struct.ge25519_p1p1*) #1

declare void @ge25519_p3_tobytes(i8*, %struct.ge25519_p3*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_sub(i8* nonnull %r, i8* nonnull %p, i8* nonnull %q) #0 {
entry:
  %retval = alloca i32, align 4
  %r.addr = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %q.addr = alloca i8*, align 4
  %p_p3 = alloca %struct.ge25519_p3, align 4
  %q_p3 = alloca %struct.ge25519_p3, align 4
  %r_p3 = alloca %struct.ge25519_p3, align 4
  %r_p1p1 = alloca %struct.ge25519_p1p1, align 4
  %q_cached = alloca %struct.ge25519_cached, align 4
  store i8* %r, i8** %r.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %q, i8** %q.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i32 @ge25519_frombytes(%struct.ge25519_p3* %p_p3, i8* %0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %call1 = call i32 @ge25519_is_on_curve(%struct.ge25519_p3* %p_p3)
  %cmp2 = icmp eq i32 %call1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false3

lor.lhs.false3:                                   ; preds = %lor.lhs.false
  %1 = load i8*, i8** %q.addr, align 4
  %call4 = call i32 @ge25519_frombytes(%struct.ge25519_p3* %q_p3, i8* %1)
  %cmp5 = icmp ne i32 %call4, 0
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false3
  %call7 = call i32 @ge25519_is_on_curve(%struct.ge25519_p3* %q_p3)
  %cmp8 = icmp eq i32 %call7, 0
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false3, %lor.lhs.false, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  call void @ge25519_p3_to_cached(%struct.ge25519_cached* %q_cached, %struct.ge25519_p3* %q_p3)
  call void @ge25519_sub(%struct.ge25519_p1p1* %r_p1p1, %struct.ge25519_p3* %p_p3, %struct.ge25519_cached* %q_cached)
  call void @ge25519_p1p1_to_p3(%struct.ge25519_p3* %r_p3, %struct.ge25519_p1p1* %r_p1p1)
  %2 = load i8*, i8** %r.addr, align 4
  call void @ge25519_p3_tobytes(i8* %2, %struct.ge25519_p3* %r_p3)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

declare void @ge25519_sub(%struct.ge25519_p1p1*, %struct.ge25519_p3*, %struct.ge25519_cached*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_from_uniform(i8* nonnull %p, i8* nonnull %r) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %r.addr = alloca i8*, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %r, i8** %r.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %1 = load i8*, i8** %r.addr, align 4
  call void @ge25519_from_uniform(i8* %0, i8* %1)
  ret i32 0
}

declare void @ge25519_from_uniform(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_from_hash(i8* nonnull %p, i8* nonnull %h) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %h.addr = alloca i8*, align 4
  store i8* %p, i8** %p.addr, align 4
  store i8* %h, i8** %h.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %1 = load i8*, i8** %h.addr, align 4
  call void @ge25519_from_hash(i8* %0, i8* %1)
  ret i32 0
}

declare void @ge25519_from_hash(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_random(i8* nonnull %p) #0 {
entry:
  %p.addr = alloca i8*, align 4
  %h = alloca [32 x i8], align 16
  store i8* %p, i8** %p.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %h, i32 0, i32 0
  call void @randombytes_buf(i8* %arraydecay, i32 32)
  %0 = load i8*, i8** %p.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %h, i32 0, i32 0
  %call = call i32 @crypto_core_ed25519_from_uniform(i8* %0, i8* %arraydecay1)
  ret void
}

declare void @randombytes_buf(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_random(i8* nonnull %r) #0 {
entry:
  %r.addr = alloca i8*, align 4
  store i8* %r, i8** %r.addr, align 4
  br label %do.body

do.body:                                          ; preds = %lor.end, %entry
  %0 = load i8*, i8** %r.addr, align 4
  call void @randombytes_buf(i8* %0, i32 32)
  %1 = load i8*, i8** %r.addr, align 4
  %arrayidx = getelementptr i8, i8* %1, i32 31
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 31
  %conv1 = trunc i32 %and to i8
  store i8 %conv1, i8* %arrayidx, align 1
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %3 = load i8*, i8** %r.addr, align 4
  %call = call i32 @sc25519_is_canonical(i8* %3)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %do.cond
  %4 = load i8*, i8** %r.addr, align 4
  %call3 = call i32 @sodium_is_zero(i8* %4, i32 32)
  %tobool = icmp ne i32 %call3, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %do.cond
  %5 = phi i1 [ true, %do.cond ], [ %tobool, %lor.rhs ]
  br i1 %5, label %do.body, label %do.end

do.end:                                           ; preds = %lor.end
  ret void
}

declare i32 @sc25519_is_canonical(i8*) #1

declare i32 @sodium_is_zero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_scalar_invert(i8* nonnull %recip, i8* nonnull %s) #0 {
entry:
  %recip.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  store i8* %recip, i8** %recip.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load i8*, i8** %recip.addr, align 4
  %1 = load i8*, i8** %s.addr, align 4
  call void @sc25519_invert(i8* %0, i8* %1)
  %2 = load i8*, i8** %s.addr, align 4
  %call = call i32 @sodium_is_zero(i8* %2, i32 32)
  %sub = sub i32 0, %call
  ret i32 %sub
}

declare void @sc25519_invert(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_negate(i8* nonnull %neg, i8* nonnull %s) #0 {
entry:
  %neg.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  %t_ = alloca [64 x i8], align 16
  %s_ = alloca [64 x i8], align 16
  store i8* %neg, i8** %neg.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 64, i1 false)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay1, i8 0, i32 64, i1 false)
  %arraydecay2 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay2, i32 32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 16 getelementptr inbounds ([32 x i8], [32 x i8]* @L, i32 0, i32 0), i32 32, i1 false)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  %0 = load i8*, i8** %s.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay3, i8* align 1 %0, i32 32, i1 false)
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  call void @sodium_sub(i8* %arraydecay4, i8* %arraydecay5, i32 64)
  %arraydecay6 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay6)
  %1 = load i8*, i8** %neg.addr, align 4
  %arraydecay7 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 16 %arraydecay7, i32 32, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare void @sodium_sub(i8*, i8*, i32) #1

declare void @sc25519_reduce(i8*) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_complement(i8* nonnull %comp, i8* nonnull %s) #0 {
entry:
  %comp.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  %t_ = alloca [64 x i8], align 16
  %s_ = alloca [64 x i8], align 16
  store i8* %comp, i8** %comp.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 64, i1 false)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay1, i8 0, i32 64, i1 false)
  %arrayidx = getelementptr [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  %0 = load i8, i8* %arrayidx, align 16
  %inc = add i8 %0, 1
  store i8 %inc, i8* %arrayidx, align 16
  %arraydecay2 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  %add.ptr = getelementptr i8, i8* %arraydecay2, i32 32
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 16 getelementptr inbounds ([32 x i8], [32 x i8]* @L, i32 0, i32 0), i32 32, i1 false)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  %1 = load i8*, i8** %s.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay3, i8* align 1 %1, i32 32, i1 false)
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %s_, i32 0, i32 0
  call void @sodium_sub(i8* %arraydecay4, i8* %arraydecay5, i32 64)
  %arraydecay6 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay6)
  %2 = load i8*, i8** %comp.addr, align 4
  %arraydecay7 = getelementptr inbounds [64 x i8], [64 x i8]* %t_, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 16 %arraydecay7, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_add(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  %x_ = alloca [64 x i8], align 16
  %y_ = alloca [64 x i8], align 16
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %x_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 64, i1 false)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %y_, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay1, i8 0, i32 64, i1 false)
  %arraydecay2 = getelementptr inbounds [64 x i8], [64 x i8]* %x_, i32 0, i32 0
  %0 = load i8*, i8** %x.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay2, i8* align 1 %0, i32 32, i1 false)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %y_, i32 0, i32 0
  %1 = load i8*, i8** %y.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay3, i8* align 1 %1, i32 32, i1 false)
  %arraydecay4 = getelementptr inbounds [64 x i8], [64 x i8]* %x_, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [64 x i8], [64 x i8]* %y_, i32 0, i32 0
  call void @sodium_add(i8* %arraydecay4, i8* %arraydecay5, i32 32)
  %2 = load i8*, i8** %z.addr, align 4
  %arraydecay6 = getelementptr inbounds [64 x i8], [64 x i8]* %x_, i32 0, i32 0
  call void @crypto_core_ed25519_scalar_reduce(i8* %2, i8* %arraydecay6)
  ret void
}

declare void @sodium_add(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_reduce(i8* nonnull %r, i8* nonnull %s) #0 {
entry:
  %r.addr = alloca i8*, align 4
  %s.addr = alloca i8*, align 4
  %t = alloca [64 x i8], align 16
  store i8* %r, i8** %r.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %arraydecay = getelementptr inbounds [64 x i8], [64 x i8]* %t, i32 0, i32 0
  %0 = load i8*, i8** %s.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %arraydecay, i8* align 1 %0, i32 64, i1 false)
  %arraydecay1 = getelementptr inbounds [64 x i8], [64 x i8]* %t, i32 0, i32 0
  call void @sc25519_reduce(i8* %arraydecay1)
  %1 = load i8*, i8** %r.addr, align 4
  %arraydecay2 = getelementptr inbounds [64 x i8], [64 x i8]* %t, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %1, i8* align 16 %arraydecay2, i32 32, i1 false)
  %arraydecay3 = getelementptr inbounds [64 x i8], [64 x i8]* %t, i32 0, i32 0
  call void @sodium_memzero(i8* %arraydecay3, i32 64)
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_sub(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  %yn = alloca [32 x i8], align 16
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %yn, i32 0, i32 0
  %0 = load i8*, i8** %y.addr, align 4
  call void @crypto_core_ed25519_scalar_negate(i8* %arraydecay, i8* %0)
  %1 = load i8*, i8** %z.addr, align 4
  %2 = load i8*, i8** %x.addr, align 4
  %arraydecay1 = getelementptr inbounds [32 x i8], [32 x i8]* %yn, i32 0, i32 0
  call void @crypto_core_ed25519_scalar_add(i8* %1, i8* %2, i8* %arraydecay1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define void @crypto_core_ed25519_scalar_mul(i8* nonnull %z, i8* nonnull %x, i8* nonnull %y) #0 {
entry:
  %z.addr = alloca i8*, align 4
  %x.addr = alloca i8*, align 4
  %y.addr = alloca i8*, align 4
  store i8* %z, i8** %z.addr, align 4
  store i8* %x, i8** %x.addr, align 4
  store i8* %y, i8** %y.addr, align 4
  %0 = load i8*, i8** %z.addr, align 4
  %1 = load i8*, i8** %x.addr, align 4
  %2 = load i8*, i8** %y.addr, align 4
  call void @sc25519_mul(i8* %0, i8* %1, i8* %2)
  ret void
}

declare void @sc25519_mul(i8*, i8*, i8*) #1

declare void @sodium_memzero(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_bytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_nonreducedscalarbytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_uniformbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_hashbytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_core_ed25519_scalarbytes() #0 {
entry:
  ret i32 32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
