; ModuleID = 'crypto_sign/crypto_sign.c'
source_filename = "crypto_sign/crypto_sign.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.crypto_sign_ed25519ph_state = type { %struct.crypto_hash_sha512_state }
%struct.crypto_hash_sha512_state = type { [8 x i64], [2 x i64], [128 x i8] }

@.str = private unnamed_addr constant [8 x i8] c"ed25519\00", align 1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_statebytes() #0 {
entry:
  ret i32 208
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_bytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_seedbytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_publickeybytes() #0 {
entry:
  ret i32 32
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_secretkeybytes() #0 {
entry:
  ret i32 64
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_messagebytes_max() #0 {
entry:
  ret i32 -65
}

; Function Attrs: noinline nounwind optnone
define i8* @crypto_sign_primitive() #0 {
entry:
  ret i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_seed_keypair(i8* nonnull %pk, i8* nonnull %sk, i8* nonnull %seed) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  %seed.addr = alloca i8*, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  store i8* %seed, i8** %seed.addr, align 4
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %2 = load i8*, i8** %seed.addr, align 4
  %call = call i32 @crypto_sign_ed25519_seed_keypair(i8* %0, i8* %1, i8* %2)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519_seed_keypair(i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_keypair(i8* nonnull %pk, i8* nonnull %sk) #0 {
entry:
  %pk.addr = alloca i8*, align 4
  %sk.addr = alloca i8*, align 4
  store i8* %pk, i8** %pk.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %pk.addr, align 4
  %1 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_keypair(i8* %0, i8* %1)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519_keypair(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign(i8* nonnull %sm, i64* %smlen_p, i8* %m, i64 %mlen, i8* nonnull %sk) #0 {
entry:
  %sm.addr = alloca i8*, align 4
  %smlen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  store i8* %sm, i8** %sm.addr, align 4
  store i64* %smlen_p, i64** %smlen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sm.addr, align 4
  %1 = load i64*, i64** %smlen_p.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_sign_ed25519(i8* %0, i64* %1, i8* %2, i64 %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519(i8*, i64*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_open(i8* %m, i64* %mlen_p, i8* nonnull %sm, i64 %smlen, i8* nonnull %pk) #0 {
entry:
  %m.addr = alloca i8*, align 4
  %mlen_p.addr = alloca i64*, align 4
  %sm.addr = alloca i8*, align 4
  %smlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64* %mlen_p, i64** %mlen_p.addr, align 4
  store i8* %sm, i8** %sm.addr, align 4
  store i64 %smlen, i64* %smlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load i8*, i8** %m.addr, align 4
  %1 = load i64*, i64** %mlen_p.addr, align 4
  %2 = load i8*, i8** %sm.addr, align 4
  %3 = load i64, i64* %smlen.addr, align 8
  %4 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_open(i8* %0, i64* %1, i8* %2, i64 %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519_open(i8*, i64*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_detached(i8* nonnull %sig, i64* %siglen_p, i8* %m, i64 %mlen, i8* nonnull %sk) #0 {
entry:
  %sig.addr = alloca i8*, align 4
  %siglen_p.addr = alloca i64*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %sk.addr = alloca i8*, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i64* %siglen_p, i64** %siglen_p.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load i8*, i8** %sig.addr, align 4
  %1 = load i64*, i64** %siglen_p.addr, align 4
  %2 = load i8*, i8** %m.addr, align 4
  %3 = load i64, i64* %mlen.addr, align 8
  %4 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_detached(i8* %0, i64* %1, i8* %2, i64 %3, i8* %4)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519_detached(i8*, i64*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_verify_detached(i8* nonnull %sig, i8* %m, i64 %mlen, i8* nonnull %pk) #0 {
entry:
  %sig.addr = alloca i8*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  %pk.addr = alloca i8*, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load i8*, i8** %sig.addr, align 4
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %3 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @crypto_sign_ed25519_verify_detached(i8* %0, i8* %1, i64 %2, i8* %3)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519_verify_detached(i8*, i8*, i64, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_init(%struct.crypto_sign_ed25519ph_state* %state) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %call = call i32 @crypto_sign_ed25519ph_init(%struct.crypto_sign_ed25519ph_state* %0)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519ph_init(%struct.crypto_sign_ed25519ph_state*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_update(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* %m, i64 %mlen) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %m.addr = alloca i8*, align 4
  %mlen.addr = alloca i64, align 8
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %m, i8** %m.addr, align 4
  store i64 %mlen, i64* %mlen.addr, align 8
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %1 = load i8*, i8** %m.addr, align 4
  %2 = load i64, i64* %mlen.addr, align 8
  %call = call i32 @crypto_sign_ed25519ph_update(%struct.crypto_sign_ed25519ph_state* %0, i8* %1, i64 %2)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519ph_update(%struct.crypto_sign_ed25519ph_state*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_final_create(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* nonnull %sig, i64* %siglen_p, i8* nonnull %sk) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %sig.addr = alloca i8*, align 4
  %siglen_p.addr = alloca i64*, align 4
  %sk.addr = alloca i8*, align 4
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i64* %siglen_p, i64** %siglen_p.addr, align 4
  store i8* %sk, i8** %sk.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %1 = load i8*, i8** %sig.addr, align 4
  %2 = load i64*, i64** %siglen_p.addr, align 4
  %3 = load i8*, i8** %sk.addr, align 4
  %call = call i32 @crypto_sign_ed25519ph_final_create(%struct.crypto_sign_ed25519ph_state* %0, i8* %1, i64* %2, i8* %3)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519ph_final_create(%struct.crypto_sign_ed25519ph_state*, i8*, i64*, i8*) #1

; Function Attrs: noinline nounwind optnone
define i32 @crypto_sign_final_verify(%struct.crypto_sign_ed25519ph_state* nonnull %state, i8* nonnull %sig, i8* nonnull %pk) #0 {
entry:
  %state.addr = alloca %struct.crypto_sign_ed25519ph_state*, align 4
  %sig.addr = alloca i8*, align 4
  %pk.addr = alloca i8*, align 4
  store %struct.crypto_sign_ed25519ph_state* %state, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  store i8* %sig, i8** %sig.addr, align 4
  store i8* %pk, i8** %pk.addr, align 4
  %0 = load %struct.crypto_sign_ed25519ph_state*, %struct.crypto_sign_ed25519ph_state** %state.addr, align 4
  %1 = load i8*, i8** %sig.addr, align 4
  %2 = load i8*, i8** %pk.addr, align 4
  %call = call i32 @crypto_sign_ed25519ph_final_verify(%struct.crypto_sign_ed25519ph_state* %0, i8* %1, i8* %2)
  ret i32 %call
}

declare i32 @crypto_sign_ed25519ph_final_verify(%struct.crypto_sign_ed25519ph_state*, i8*, i8*) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
