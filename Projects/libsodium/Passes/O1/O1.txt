[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.000690037 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 9.0979e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00538013 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00392349 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000780946 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00762561 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00387688 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0107348 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00671872 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00180723 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00385573 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00525191 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0119198 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00676342 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00165382 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00527688 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0016548 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00671601 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0068576 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.003957 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000767724 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00695666 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00379332 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00750793 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00666508 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000652054 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.1231e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals...               0.000914546 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00105675 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      5.192e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.123866 seconds.
[PassRunner] (final validation)
