[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0484621 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000165583 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0640205 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0825606 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00746394 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0299231 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0149768 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0357344 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0442056 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.170258 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0805581 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0300818 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0286095 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0331643 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0776739 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0482849 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0115655 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0296272 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0114635 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0477139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.020955 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0419498 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00617037 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0187605 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0237301 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0212601 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.019437 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0426116 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0774708 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0318111 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00831897 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   5.3698e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00414034 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00899014 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      6.1207e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00632757 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0377481 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.26631 seconds.
[PassRunner] (final validation)
