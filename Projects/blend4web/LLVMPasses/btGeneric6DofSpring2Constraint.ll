; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpring2Constraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpring2Constraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btGeneric6DofSpring2Constraint = type { %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor2, [3 x %class.btRotationalLimitMotor2], i32, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, i32 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTranslationalLimitMotor2 = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], [3 x i8], [3 x i8], %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor2 = type { float, float, float, float, float, float, float, i8, float, float, i8, float, i8, float, i8, float, i8, float, float, float, float, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%class.btAlignedObjectArray.5 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btGeneric6DofSpring2ConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], i32 }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN26btTranslationalLimitMotor2C2Ev = comdat any

$_ZN23btRotationalLimitMotor2C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z6btAsinf = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_Z21btAdjustAngleToLimitsfff = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZNK30btGeneric6DofSpring2Constraint7getAxisEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_Z4sqrtf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN30btGeneric6DofSpring2ConstraintD2Ev = comdat any

$_ZN30btGeneric6DofSpring2ConstraintD0Ev = comdat any

$_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btFabsf = comdat any

$_Z16btNormalizeAnglef = comdat any

$_Z6btFmodff = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN30btGeneric6DofSpring2ConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV30btGeneric6DofSpring2Constraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btGeneric6DofSpring2Constraint to i8*), i8* bitcast (%class.btGeneric6DofSpring2Constraint* (%class.btGeneric6DofSpring2Constraint*)* @_ZN30btGeneric6DofSpring2ConstraintD2Ev to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*)* @_ZN30btGeneric6DofSpring2ConstraintD0Ev to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*)* @_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN30btGeneric6DofSpring2Constraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN30btGeneric6DofSpring2Constraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, i32, float, i32)* @_ZN30btGeneric6DofSpring2Constraint8setParamEifi to i8*), i8* bitcast (float (%class.btGeneric6DofSpring2Constraint*, i32, i32)* @_ZNK30btGeneric6DofSpring2Constraint8getParamEii to i8*), i8* bitcast (i32 (%class.btGeneric6DofSpring2Constraint*)* @_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGeneric6DofSpring2Constraint*, i8*, %class.btSerializer*)* @_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer to i8*)] }, align 4
@__const._ZN30btGeneric6DofSpring2Constraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_.cIdx = private unnamed_addr constant [3 x i32] [i32 0, i32 1, i32 2], align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btGeneric6DofSpring2Constraint = hidden constant [33 x i8] c"30btGeneric6DofSpring2Constraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI30btGeneric6DofSpring2Constraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btGeneric6DofSpring2Constraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [35 x i8] c"btGeneric6DofSpring2ConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGeneric6DofSpring2Constraint.cpp, i8* null }]

@_ZN30btGeneric6DofSpring2ConstraintC1ER11btRigidBodyS1_RK11btTransformS4_11RotateOrder = hidden unnamed_addr alias %class.btGeneric6DofSpring2Constraint* (%class.btGeneric6DofSpring2Constraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i32), %class.btGeneric6DofSpring2Constraint* (%class.btGeneric6DofSpring2Constraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i32)* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyS1_RK11btTransformS4_11RotateOrder
@_ZN30btGeneric6DofSpring2ConstraintC1ER11btRigidBodyRK11btTransform11RotateOrder = hidden unnamed_addr alias %class.btGeneric6DofSpring2Constraint* (%class.btGeneric6DofSpring2Constraint*, %class.btRigidBody*, %class.btTransform*, i32), %class.btGeneric6DofSpring2Constraint* (%class.btGeneric6DofSpring2Constraint*, %class.btRigidBody*, %class.btTransform*, i32)* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyRK11btTransform11RotateOrder

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyS1_RK11btTransformS4_11RotateOrder(%class.btGeneric6DofSpring2Constraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i32 %rotOrder) unnamed_addr #2 {
entry:
  %retval = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInA.addr = alloca %class.btTransform*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %rotOrder.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInA, %class.btTransform** %frameInA.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  store i32 %rotOrder, i32* %rotOrder.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btGeneric6DofSpring2Constraint* %this1, %class.btGeneric6DofSpring2Constraint** %retval, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 12, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2)
  %3 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV30btGeneric6DofSpring2Constraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %4 = load %class.btTransform*, %class.btTransform** %frameInA.addr, align 4
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %5 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_jacLinear = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLinear, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call4 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 4
  %array.begin5 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin5, i32 3
  br label %arrayctor.loop7

arrayctor.loop7:                                  ; preds = %arrayctor.loop7, %arrayctor.cont
  %arrayctor.cur8 = phi %class.btJacobianEntry* [ %array.begin5, %arrayctor.cont ], [ %arrayctor.next10, %arrayctor.loop7 ]
  %call9 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur8)
  %arrayctor.next10 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur8, i32 1
  %arrayctor.done11 = icmp eq %class.btJacobianEntry* %arrayctor.next10, %arrayctor.end6
  br i1 %arrayctor.done11, label %arrayctor.cont12, label %arrayctor.loop7

arrayctor.cont12:                                 ; preds = %arrayctor.loop7
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %call13 = call %class.btTranslationalLimitMotor2* @_ZN26btTranslationalLimitMotor2C2Ev(%class.btTranslationalLimitMotor2* %m_linearLimits)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %array.begin14 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 0
  %arrayctor.end15 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %array.begin14, i32 3
  br label %arrayctor.loop16

arrayctor.loop16:                                 ; preds = %arrayctor.loop16, %arrayctor.cont12
  %arrayctor.cur17 = phi %class.btRotationalLimitMotor2* [ %array.begin14, %arrayctor.cont12 ], [ %arrayctor.next19, %arrayctor.loop16 ]
  %call18 = call %class.btRotationalLimitMotor2* @_ZN23btRotationalLimitMotor2C2Ev(%class.btRotationalLimitMotor2* %arrayctor.cur17)
  %arrayctor.next19 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayctor.cur17, i32 1
  %arrayctor.done20 = icmp eq %class.btRotationalLimitMotor2* %arrayctor.next19, %arrayctor.end15
  br i1 %arrayctor.done20, label %arrayctor.cont21, label %arrayctor.loop16

arrayctor.cont21:                                 ; preds = %arrayctor.loop16
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %6 = load i32, i32* %rotOrder.addr, align 4
  store i32 %6, i32* %m_rotateOrder, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call22 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call23 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedAxisAngleDiff)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %array.begin25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 0
  %arrayctor.end26 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin25, i32 3
  br label %arrayctor.loop27

arrayctor.loop27:                                 ; preds = %arrayctor.loop27, %arrayctor.cont21
  %arrayctor.cur28 = phi %class.btVector3* [ %array.begin25, %arrayctor.cont21 ], [ %arrayctor.next30, %arrayctor.loop27 ]
  %call29 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur28)
  %arrayctor.next30 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur28, i32 1
  %arrayctor.done31 = icmp eq %class.btVector3* %arrayctor.next30, %arrayctor.end26
  br i1 %arrayctor.done31, label %arrayctor.cont32, label %arrayctor.loop27

arrayctor.cont32:                                 ; preds = %arrayctor.loop27
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call33 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedLinearDiff)
  %m_flags = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  store i32 0, i32* %m_flags, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  %7 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %retval, align 4
  ret %class.btGeneric6DofSpring2Constraint* %7
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTranslationalLimitMotor2* @_ZN26btTranslationalLimitMotor2C2Ev(%class.btTranslationalLimitMotor2* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btTranslationalLimitMotor2*, align 4
  %this.addr = alloca %class.btTranslationalLimitMotor2*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %i = alloca i32, align 4
  store %class.btTranslationalLimitMotor2* %this, %class.btTranslationalLimitMotor2** %this.addr, align 4
  %this1 = load %class.btTranslationalLimitMotor2*, %class.btTranslationalLimitMotor2** %this.addr, align 4
  store %class.btTranslationalLimitMotor2* %this1, %class.btTranslationalLimitMotor2** %retval, align 4
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lowerLimit)
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_upperLimit)
  %m_bounce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bounce)
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_stopERP)
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_stopCFM)
  %m_motorERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 5
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_motorERP)
  %m_motorCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 6
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_motorCFM)
  %m_servoTarget = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 10
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_servoTarget)
  %m_springStiffness = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 11
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_springStiffness)
  %m_springDamping = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 13
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_springDamping)
  %m_equilibriumPoint = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 15
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_equilibriumPoint)
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 16
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_targetVelocity)
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 17
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_maxMotorForce)
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 18
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentLimitError)
  %m_currentLimitErrorHi = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 19
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentLimitErrorHi)
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 20
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentLinearDiff)
  %m_lowerLimit17 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_lowerLimit17, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %m_upperLimit20 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp21, align 4
  store float 0.000000e+00, float* %ref.tmp22, align 4
  store float 0.000000e+00, float* %ref.tmp23, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_upperLimit20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %m_bounce24 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %ref.tmp25, align 4
  store float 0.000000e+00, float* %ref.tmp26, align 4
  store float 0.000000e+00, float* %ref.tmp27, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_bounce24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27)
  %m_stopERP28 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 3
  store float 0x3FC99999A0000000, float* %ref.tmp29, align 4
  store float 0x3FC99999A0000000, float* %ref.tmp30, align 4
  store float 0x3FC99999A0000000, float* %ref.tmp31, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_stopERP28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %m_stopCFM32 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %ref.tmp33, align 4
  store float 0.000000e+00, float* %ref.tmp34, align 4
  store float 0.000000e+00, float* %ref.tmp35, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_stopCFM32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35)
  %m_motorERP36 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 5
  store float 0x3FECCCCCC0000000, float* %ref.tmp37, align 4
  store float 0x3FECCCCCC0000000, float* %ref.tmp38, align 4
  store float 0x3FECCCCCC0000000, float* %ref.tmp39, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_motorERP36, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39)
  %m_motorCFM40 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %ref.tmp41, align 4
  store float 0.000000e+00, float* %ref.tmp42, align 4
  store float 0.000000e+00, float* %ref.tmp43, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_motorCFM40, float* nonnull align 4 dereferenceable(4) %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %m_currentLimitError44 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %ref.tmp45, align 4
  store float 0.000000e+00, float* %ref.tmp46, align 4
  store float 0.000000e+00, float* %ref.tmp47, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_currentLimitError44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp46, float* nonnull align 4 dereferenceable(4) %ref.tmp47)
  %m_currentLimitErrorHi48 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  store float 0.000000e+00, float* %ref.tmp51, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_currentLimitErrorHi48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  %m_currentLinearDiff52 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %ref.tmp53, align 4
  store float 0.000000e+00, float* %ref.tmp54, align 4
  store float 0.000000e+00, float* %ref.tmp55, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_currentLinearDiff52, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp54, float* nonnull align 4 dereferenceable(4) %ref.tmp55)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 7
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %1
  store i8 0, i8* %arrayidx, align 1
  %m_servoMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 8
  %2 = load i32, i32* %i, align 4
  %arrayidx56 = getelementptr inbounds [3 x i8], [3 x i8]* %m_servoMotor, i32 0, i32 %2
  store i8 0, i8* %arrayidx56, align 1
  %m_enableSpring = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 9
  %3 = load i32, i32* %i, align 4
  %arrayidx57 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring, i32 0, i32 %3
  store i8 0, i8* %arrayidx57, align 1
  %m_servoTarget58 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 10
  %call59 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_servoTarget58)
  %4 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds float, float* %call59, i32 %4
  store float 0.000000e+00, float* %arrayidx60, align 4
  %m_springStiffness61 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 11
  %call62 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springStiffness61)
  %5 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 %5
  store float 0.000000e+00, float* %arrayidx63, align 4
  %m_springStiffnessLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 12
  %6 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springStiffnessLimited, i32 0, i32 %6
  store i8 0, i8* %arrayidx64, align 1
  %m_springDamping65 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 13
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springDamping65)
  %7 = load i32, i32* %i, align 4
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 %7
  store float 0.000000e+00, float* %arrayidx67, align 4
  %m_springDampingLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 14
  %8 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springDampingLimited, i32 0, i32 %8
  store i8 0, i8* %arrayidx68, align 1
  %m_equilibriumPoint69 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 15
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_equilibriumPoint69)
  %9 = load i32, i32* %i, align 4
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 %9
  store float 0.000000e+00, float* %arrayidx71, align 4
  %m_targetVelocity72 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 16
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity72)
  %10 = load i32, i32* %i, align 4
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 %10
  store float 0.000000e+00, float* %arrayidx74, align 4
  %m_maxMotorForce75 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 17
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce75)
  %11 = load i32, i32* %i, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %11
  store float 0.000000e+00, float* %arrayidx77, align 4
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 21
  %12 = load i32, i32* %i, align 4
  %arrayidx78 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %12
  store i32 0, i32* %arrayidx78, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load %class.btTranslationalLimitMotor2*, %class.btTranslationalLimitMotor2** %retval, align 4
  ret %class.btTranslationalLimitMotor2* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRotationalLimitMotor2* @_ZN23btRotationalLimitMotor2C2Ev(%class.btRotationalLimitMotor2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRotationalLimitMotor2*, align 4
  store %class.btRotationalLimitMotor2* %this, %class.btRotationalLimitMotor2** %this.addr, align 4
  %this1 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %this.addr, align 4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 0
  store float 1.000000e+00, float* %m_loLimit, align 4
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 1
  store float -1.000000e+00, float* %m_hiLimit, align 4
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_bounce, align 4
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 3
  store float 0x3FC99999A0000000, float* %m_stopERP, align 4
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_stopCFM, align 4
  %m_motorERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 5
  store float 0x3FECCCCCC0000000, float* %m_motorERP, align 4
  %m_motorCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_motorCFM, align 4
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 7
  store i8 0, i8* %m_enableMotor, align 4
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_targetVelocity, align 4
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 9
  store float 0x3FB99999A0000000, float* %m_maxMotorForce, align 4
  %m_servoMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 10
  store i8 0, i8* %m_servoMotor, align 4
  %m_servoTarget = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 11
  store float 0.000000e+00, float* %m_servoTarget, align 4
  %m_enableSpring = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 12
  store i8 0, i8* %m_enableSpring, align 4
  %m_springStiffness = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_springStiffness, align 4
  %m_springStiffnessLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 14
  store i8 0, i8* %m_springStiffnessLimited, align 4
  %m_springDamping = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 15
  store float 0.000000e+00, float* %m_springDamping, align 4
  %m_springDampingLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 16
  store i8 0, i8* %m_springDampingLimited, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_equilibriumPoint, align 4
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_currentLimitError, align 4
  %m_currentLimitErrorHi = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_currentLimitErrorHi, align 4
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_currentPosition, align 4
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 21
  store i32 0, i32* %m_currentLimit, align 4
  ret %class.btRotationalLimitMotor2* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofSpring2Constraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyRK11btTransform11RotateOrder(%class.btGeneric6DofSpring2Constraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i32 %rotOrder) unnamed_addr #2 {
entry:
  %retval = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %rotOrder.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  store i32 %rotOrder, i32* %rotOrder.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btGeneric6DofSpring2Constraint* %this1, %class.btGeneric6DofSpring2Constraint** %retval, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv()
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call2 = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 12, %class.btRigidBody* nonnull align 4 dereferenceable(676) %call, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV30btGeneric6DofSpring2Constraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_frameInA)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_jacLinear = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLinear, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call5 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 4
  %array.begin6 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin6, i32 3
  br label %arrayctor.loop8

arrayctor.loop8:                                  ; preds = %arrayctor.loop8, %arrayctor.cont
  %arrayctor.cur9 = phi %class.btJacobianEntry* [ %array.begin6, %arrayctor.cont ], [ %arrayctor.next11, %arrayctor.loop8 ]
  %call10 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur9)
  %arrayctor.next11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur9, i32 1
  %arrayctor.done12 = icmp eq %class.btJacobianEntry* %arrayctor.next11, %arrayctor.end7
  br i1 %arrayctor.done12, label %arrayctor.cont13, label %arrayctor.loop8

arrayctor.cont13:                                 ; preds = %arrayctor.loop8
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %call14 = call %class.btTranslationalLimitMotor2* @_ZN26btTranslationalLimitMotor2C2Ev(%class.btTranslationalLimitMotor2* %m_linearLimits)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %array.begin15 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 0
  %arrayctor.end16 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %array.begin15, i32 3
  br label %arrayctor.loop17

arrayctor.loop17:                                 ; preds = %arrayctor.loop17, %arrayctor.cont13
  %arrayctor.cur18 = phi %class.btRotationalLimitMotor2* [ %array.begin15, %arrayctor.cont13 ], [ %arrayctor.next20, %arrayctor.loop17 ]
  %call19 = call %class.btRotationalLimitMotor2* @_ZN23btRotationalLimitMotor2C2Ev(%class.btRotationalLimitMotor2* %arrayctor.cur18)
  %arrayctor.next20 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayctor.cur18, i32 1
  %arrayctor.done21 = icmp eq %class.btRotationalLimitMotor2* %arrayctor.next20, %arrayctor.end16
  br i1 %arrayctor.done21, label %arrayctor.cont22, label %arrayctor.loop17

arrayctor.cont22:                                 ; preds = %arrayctor.loop17
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %4 = load i32, i32* %rotOrder.addr, align 4
  store i32 %4, i32* %m_rotateOrder, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call23 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call24 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedAxisAngleDiff)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %array.begin26 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 0
  %arrayctor.end27 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin26, i32 3
  br label %arrayctor.loop28

arrayctor.loop28:                                 ; preds = %arrayctor.loop28, %arrayctor.cont22
  %arrayctor.cur29 = phi %class.btVector3* [ %array.begin26, %arrayctor.cont22 ], [ %arrayctor.next31, %arrayctor.loop28 ]
  %call30 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur29)
  %arrayctor.next31 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur29, i32 1
  %arrayctor.done32 = icmp eq %class.btVector3* %arrayctor.next31, %arrayctor.end27
  br i1 %arrayctor.done32, label %arrayctor.cont33, label %arrayctor.loop28

arrayctor.cont33:                                 ; preds = %arrayctor.loop28
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call34 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_calculatedLinearDiff)
  %m_flags = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  store i32 0, i32* %m_flags, align 4
  %5 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call35 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %m_frameInB36 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %call35, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB36)
  %m_frameInA37 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %call38 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA37, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  %6 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %retval, align 4
  ret %class.btGeneric6DofSpring2Constraint* %6
}

declare nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv() #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, i32 %index) #2 {
entry:
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %index.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %rem = srem i32 %0, 3
  store i32 %rem, i32* %i, align 4
  %1 = load i32, i32* %index.addr, align 4
  %div = sdiv i32 %1, 3
  store i32 %div, i32* %j, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 %3)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %4 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds float, float* %call1, i32 %4
  %5 = load float, float* %arrayidx, align 4
  ret float %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXYZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 2)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 5)
  %fneg = fneg float %call3
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call4 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, i32 8)
  %call5 = call float @_Z7btAtan2ff(float %fneg, float %call4)
  %5 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %call5, float* %arrayidx, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 2)
  %call8 = call float @_Z6btAsinf(float %call7)
  %7 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %7)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call11 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %8, i32 1)
  %fneg12 = fneg float %call11
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call13 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 0)
  %call14 = call float @_Z7btAtan2ff(float %fneg12, float %call13)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %call14, float* %arrayidx16, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call17 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %11, i32 3)
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call18 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, i32 4)
  %call19 = call float @_Z7btAtan2ff(float %call17, float %call18)
  %fneg20 = fneg float %call19
  %13 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 0
  store float %fneg20, float* %arrayidx22, align 4
  %14 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  store float 0xBFF921FB60000000, float* %arrayidx24, align 4
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float 0.000000e+00, float* %arrayidx26, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else27:                                        ; preds = %entry
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call28 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %16, i32 3)
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call29 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %17, i32 4)
  %call30 = call float @_Z7btAtan2ff(float %call28, float %call29)
  %18 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %18)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 0
  store float %call30, float* %arrayidx32, align 4
  %19 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float 0x3FF921FB60000000, float* %arrayidx34, align 4
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  store float 0.000000e+00, float* %arrayidx36, align 4
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #7
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btAsinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4
  %call = call float @asinf(float %2) #7
  ret float %call
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXZYERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 1)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 7)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call4 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, i32 4)
  %call5 = call float @_Z7btAtan2ff(float %call3, float %call4)
  %5 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %call5, float* %arrayidx, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 2)
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call8 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, i32 0)
  %call9 = call float @_Z7btAtan2ff(float %call7, float %call8)
  %8 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %8)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  store float %call9, float* %arrayidx11, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call12 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 1)
  %fneg = fneg float %call12
  %call13 = call float @_Z6btAsinf(float %fneg)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float %call13, float* %arrayidx15, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call16 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %11, i32 6)
  %fneg17 = fneg float %call16
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call18 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, i32 8)
  %call19 = call float @_Z7btAtan2ff(float %fneg17, float %call18)
  %fneg20 = fneg float %call19
  %13 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 0
  store float %fneg20, float* %arrayidx22, align 4
  %14 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  store float 0.000000e+00, float* %arrayidx24, align 4
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float 0x3FF921FB60000000, float* %arrayidx26, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else27:                                        ; preds = %entry
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call28 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %16, i32 6)
  %fneg29 = fneg float %call28
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call30 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %17, i32 8)
  %call31 = call float @_Z7btAtan2ff(float %fneg29, float %call30)
  %18 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %18)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 0
  store float %call31, float* %arrayidx33, align 4
  %19 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  store float 0.000000e+00, float* %arrayidx35, align 4
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 2
  store float 0xBFF921FB60000000, float* %arrayidx37, align 4
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerYXZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 5)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 5)
  %fneg = fneg float %call3
  %call4 = call float @_Z6btAsinf(float %fneg)
  %4 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %4)
  %arrayidx = getelementptr inbounds float, float* %call5, i32 0
  store float %call4, float* %arrayidx, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call6 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %5, i32 2)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 8)
  %call8 = call float @_Z7btAtan2ff(float %call6, float %call7)
  %7 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %7)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call11 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %8, i32 3)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call12 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 4)
  %call13 = call float @_Z7btAtan2ff(float %call11, float %call12)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float %call13, float* %arrayidx15, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  store float 0x3FF921FB60000000, float* %arrayidx17, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call18 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, i32 1)
  %fneg19 = fneg float %call18
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call20 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, i32 0)
  %call21 = call float @_Z7btAtan2ff(float %fneg19, float %call20)
  %fneg22 = fneg float %call21
  %14 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  store float %fneg22, float* %arrayidx24, align 4
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float 0.000000e+00, float* %arrayidx26, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else27:                                        ; preds = %entry
  %16 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 0
  store float 0xBFF921FB60000000, float* %arrayidx29, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call30 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %17, i32 1)
  %fneg31 = fneg float %call30
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call32 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18, i32 0)
  %call33 = call float @_Z7btAtan2ff(float %fneg31, float %call32)
  %19 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  store float %call33, float* %arrayidx35, align 4
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 2
  store float 0.000000e+00, float* %arrayidx37, align 4
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerYZXERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 3)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 5)
  %fneg = fneg float %call3
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call4 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, i32 4)
  %call5 = call float @_Z7btAtan2ff(float %fneg, float %call4)
  %5 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %call5, float* %arrayidx, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 6)
  %fneg8 = fneg float %call7
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call9 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, i32 0)
  %call10 = call float @_Z7btAtan2ff(float %fneg8, float %call9)
  %8 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %8)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float %call10, float* %arrayidx12, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call13 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 3)
  %call14 = call float @_Z6btAsinf(float %call13)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %call14, float* %arrayidx16, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  store float 0.000000e+00, float* %arrayidx18, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call19 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12, i32 7)
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call20 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, i32 8)
  %call21 = call float @_Z7btAtan2ff(float %call19, float %call20)
  %fneg22 = fneg float %call21
  %14 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  store float %fneg22, float* %arrayidx24, align 4
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float 0xBFF921FB60000000, float* %arrayidx26, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else27:                                        ; preds = %entry
  %16 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 0
  store float 0.000000e+00, float* %arrayidx29, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call30 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %17, i32 7)
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call31 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18, i32 8)
  %call32 = call float @_Z7btAtan2ff(float %call30, float %call31)
  %19 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %call32, float* %arrayidx34, align 4
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  store float 0x3FF921FB60000000, float* %arrayidx36, align 4
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerZXYERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 7)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else27

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 7)
  %call4 = call float @_Z6btAsinf(float %call3)
  %4 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %4)
  %arrayidx = getelementptr inbounds float, float* %call5, i32 0
  store float %call4, float* %arrayidx, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call6 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %5, i32 6)
  %fneg = fneg float %call6
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 8)
  %call8 = call float @_Z7btAtan2ff(float %fneg, float %call7)
  %7 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %7)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call11 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %8, i32 1)
  %fneg12 = fneg float %call11
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call13 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 4)
  %call14 = call float @_Z7btAtan2ff(float %fneg12, float %call13)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %call14, float* %arrayidx16, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  store float 0xBFF921FB60000000, float* %arrayidx18, align 4
  %12 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %12)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  store float 0.000000e+00, float* %arrayidx20, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call21 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, i32 2)
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call22 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %14, i32 0)
  %call23 = call float @_Z7btAtan2ff(float %call21, float %call22)
  %fneg24 = fneg float %call23
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  store float %fneg24, float* %arrayidx26, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else27:                                        ; preds = %entry
  %16 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 0
  store float 0x3FF921FB60000000, float* %arrayidx29, align 4
  %17 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 1
  store float 0.000000e+00, float* %arrayidx31, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call32 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18, i32 2)
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call33 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %19, i32 0)
  %call34 = call float @_Z7btAtan2ff(float %call32, float %call33)
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  store float %call34, float* %arrayidx36, align 4
  br label %if.end

if.end:                                           ; preds = %if.else27
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerZYXERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %mat, %class.btVector3* nonnull align 4 dereferenceable(16) %xyz) #2 {
entry:
  %retval = alloca i1, align 1
  %mat.addr = alloca %class.btMatrix3x3*, align 4
  %xyz.addr = alloca %class.btVector3*, align 4
  %fi = alloca float, align 4
  store %class.btMatrix3x3* %mat, %class.btMatrix3x3** %mat.addr, align 4
  store %class.btVector3* %xyz, %class.btVector3** %xyz.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 6)
  store float %call, float* %fi, align 4
  %1 = load float, float* %fi, align 4
  %cmp = fcmp olt float %1, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.else26

if.then:                                          ; preds = %entry
  %2 = load float, float* %fi, align 4
  %cmp1 = fcmp ogt float %2, -1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call3 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, i32 7)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call4 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, i32 8)
  %call5 = call float @_Z7btAtan2ff(float %call3, float %call4)
  %5 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %call5, float* %arrayidx, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call7 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, i32 6)
  %fneg = fneg float %call7
  %call8 = call float @_Z6btAsinf(float %fneg)
  %7 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %7)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call11 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %8, i32 3)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call12 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9, i32 0)
  %call13 = call float @_Z7btAtan2ff(float %call11, float %call12)
  %10 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float %call13, float* %arrayidx15, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %if.then
  %11 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  store float 0.000000e+00, float* %arrayidx17, align 4
  %12 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %12)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  store float 0x3FF921FB60000000, float* %arrayidx19, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call20 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13, i32 1)
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call21 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %14, i32 2)
  %call22 = call float @_Z7btAtan2ff(float %call20, float %call21)
  %fneg23 = fneg float %call22
  %15 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  store float %fneg23, float* %arrayidx25, align 4
  store i1 false, i1* %retval, align 1
  br label %return

if.else26:                                        ; preds = %entry
  %16 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  store float 0.000000e+00, float* %arrayidx28, align 4
  %17 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 1
  store float 0xBFF921FB60000000, float* %arrayidx30, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call31 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %18, i32 1)
  %fneg32 = fneg float %call31
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %mat.addr, align 4
  %call33 = call float @_ZN30btGeneric6DofSpring2Constraint15btGetMatrixElemERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %19, i32 2)
  %fneg34 = fneg float %call33
  %call35 = call float @_Z7btAtan2ff(float %fneg32, float %fneg34)
  %20 = load %class.btVector3*, %class.btVector3** %xyz.addr, align 4
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 2
  store float %call35, float* %arrayidx37, align 4
  br label %if.end

if.end:                                           ; preds = %if.else26
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.else, %if.then2
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint18calculateAngleInfoEv(%class.btGeneric6DofSpring2Constraint* %this) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %relative_frame = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %axis0 = alloca %class.btVector3, align 4
  %axis2 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %axis037 = alloca %class.btVector3, align 4
  %axis1 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %axis156 = alloca %class.btVector3, align 4
  %axis259 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %ref.tmp70 = alloca %class.btVector3, align 4
  %axis076 = alloca %class.btVector3, align 4
  %axis179 = alloca %class.btVector3, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %axis196 = alloca %class.btVector3, align 4
  %axis299 = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca %class.btVector3, align 4
  %ref.tmp105 = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %axis0116 = alloca %class.btVector3, align 4
  %axis2119 = alloca %class.btVector3, align 4
  %ref.tmp122 = alloca %class.btVector3, align 4
  %ref.tmp125 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %relative_frame, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call2)
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %0 = load i32, i32* %m_rotateOrder, align 4
  switch i32 %0, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb4
    i32 2, label %sw.bb7
    i32 3, label %sw.bb10
    i32 4, label %sw.bb13
    i32 5, label %sw.bb16
  ]

sw.bb:                                            ; preds = %entry
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call3 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXYZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff)
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %m_calculatedAxisAngleDiff5 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call6 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXZYERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff5)
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry
  %m_calculatedAxisAngleDiff8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call9 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerYXZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff8)
  br label %sw.epilog

sw.bb10:                                          ; preds = %entry
  %m_calculatedAxisAngleDiff11 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call12 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerYZXERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff11)
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %m_calculatedAxisAngleDiff14 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call15 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerZXYERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff14)
  br label %sw.epilog

sw.bb16:                                          ; preds = %entry
  %m_calculatedAxisAngleDiff17 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call18 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerZYXERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relative_frame, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedAxisAngleDiff17)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb16, %sw.bb13, %sw.bb10, %sw.bb7, %sw.bb4, %sw.bb
  %m_rotateOrder19 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %1 = load i32, i32* %m_rotateOrder19, align 4
  switch i32 %1, label %sw.default135 [
    i32 0, label %sw.bb20
    i32 1, label %sw.bb36
    i32 2, label %sw.bb55
    i32 3, label %sw.bb75
    i32 4, label %sw.bb95
    i32 5, label %sw.bb115
  ]

sw.bb20:                                          ; preds = %sw.epilog
  %m_calculatedTransformB21 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB21)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis0, %class.btMatrix3x3* %call22, i32 0)
  %m_calculatedTransformA23 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call24 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA23)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis2, %class.btMatrix3x3* %call24, i32 2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp25, %class.btVector3* %axis2, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 1
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  %3 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_calculatedAxis27 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis27, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* %arrayidx28, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2)
  %m_calculatedAxis29 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis29, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx30 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_calculatedAxis32 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis32, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx33)
  %m_calculatedAxis34 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis34, i32 0, i32 2
  %6 = bitcast %class.btVector3* %arrayidx35 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  br label %sw.epilog136

sw.bb36:                                          ; preds = %sw.epilog
  %m_calculatedTransformB38 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call39 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB38)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis037, %class.btMatrix3x3* %call39, i32 0)
  %m_calculatedTransformA40 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call41 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA40)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis1, %class.btMatrix3x3* %call41, i32 1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* %axis037, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1)
  %m_calculatedAxis43 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx44 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis43, i32 0, i32 2
  %8 = bitcast %class.btVector3* %arrayidx44 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_calculatedAxis46 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx47 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis46, i32 0, i32 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* %axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx47)
  %m_calculatedAxis48 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis48, i32 0, i32 0
  %10 = bitcast %class.btVector3* %arrayidx49 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %m_calculatedAxis51 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx52 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis51, i32 0, i32 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* %arrayidx52, %class.btVector3* nonnull align 4 dereferenceable(16) %axis037)
  %m_calculatedAxis53 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx54 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis53, i32 0, i32 1
  %12 = bitcast %class.btVector3* %arrayidx54 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %sw.epilog136

sw.bb55:                                          ; preds = %sw.epilog
  %m_calculatedTransformB57 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call58 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB57)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis156, %class.btMatrix3x3* %call58, i32 1)
  %m_calculatedTransformA60 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call61 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA60)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis259, %class.btMatrix3x3* %call61, i32 2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp62, %class.btVector3* %axis156, %class.btVector3* nonnull align 4 dereferenceable(16) %axis259)
  %m_calculatedAxis63 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx64 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis63, i32 0, i32 0
  %14 = bitcast %class.btVector3* %arrayidx64 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp62 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  %m_calculatedAxis66 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis66, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* %axis259, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx67)
  %m_calculatedAxis68 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis68, i32 0, i32 1
  %16 = bitcast %class.btVector3* %arrayidx69 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %m_calculatedAxis71 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx72 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis71, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp70, %class.btVector3* %arrayidx72, %class.btVector3* nonnull align 4 dereferenceable(16) %axis156)
  %m_calculatedAxis73 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis73, i32 0, i32 2
  %18 = bitcast %class.btVector3* %arrayidx74 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  br label %sw.epilog136

sw.bb75:                                          ; preds = %sw.epilog
  %m_calculatedTransformA77 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call78 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA77)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis076, %class.btMatrix3x3* %call78, i32 0)
  %m_calculatedTransformB80 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call81 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB80)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis179, %class.btMatrix3x3* %call81, i32 1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp82, %class.btVector3* %axis076, %class.btVector3* nonnull align 4 dereferenceable(16) %axis179)
  %m_calculatedAxis83 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx84 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis83, i32 0, i32 2
  %20 = bitcast %class.btVector3* %arrayidx84 to i8*
  %21 = bitcast %class.btVector3* %ref.tmp82 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  %m_calculatedAxis86 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx87 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis86, i32 0, i32 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp85, %class.btVector3* %axis179, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx87)
  %m_calculatedAxis88 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis88, i32 0, i32 0
  %22 = bitcast %class.btVector3* %arrayidx89 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %m_calculatedAxis91 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx92 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis91, i32 0, i32 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* %arrayidx92, %class.btVector3* nonnull align 4 dereferenceable(16) %axis076)
  %m_calculatedAxis93 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx94 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis93, i32 0, i32 1
  %24 = bitcast %class.btVector3* %arrayidx94 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false)
  br label %sw.epilog136

sw.bb95:                                          ; preds = %sw.epilog
  %m_calculatedTransformA97 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call98 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA97)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis196, %class.btMatrix3x3* %call98, i32 1)
  %m_calculatedTransformB100 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call101 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB100)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis299, %class.btMatrix3x3* %call101, i32 2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp102, %class.btVector3* %axis196, %class.btVector3* nonnull align 4 dereferenceable(16) %axis299)
  %m_calculatedAxis103 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx104 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis103, i32 0, i32 0
  %26 = bitcast %class.btVector3* %arrayidx104 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp102 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %m_calculatedAxis106 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis106, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp105, %class.btVector3* %axis299, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx107)
  %m_calculatedAxis108 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx109 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis108, i32 0, i32 1
  %28 = bitcast %class.btVector3* %arrayidx109 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp105 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  %m_calculatedAxis111 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx112 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis111, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp110, %class.btVector3* %arrayidx112, %class.btVector3* nonnull align 4 dereferenceable(16) %axis196)
  %m_calculatedAxis113 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx114 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis113, i32 0, i32 2
  %30 = bitcast %class.btVector3* %arrayidx114 to i8*
  %31 = bitcast %class.btVector3* %ref.tmp110 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  br label %sw.epilog136

sw.bb115:                                         ; preds = %sw.epilog
  %m_calculatedTransformA117 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call118 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA117)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis0116, %class.btMatrix3x3* %call118, i32 0)
  %m_calculatedTransformB120 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call121 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB120)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis2119, %class.btMatrix3x3* %call121, i32 2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp122, %class.btVector3* %axis2119, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0116)
  %m_calculatedAxis123 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis123, i32 0, i32 1
  %32 = bitcast %class.btVector3* %arrayidx124 to i8*
  %33 = bitcast %class.btVector3* %ref.tmp122 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  %m_calculatedAxis126 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx127 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis126, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp125, %class.btVector3* %arrayidx127, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2119)
  %m_calculatedAxis128 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx129 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis128, i32 0, i32 0
  %34 = bitcast %class.btVector3* %arrayidx129 to i8*
  %35 = bitcast %class.btVector3* %ref.tmp125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  %m_calculatedAxis131 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx132 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis131, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* %axis0116, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx132)
  %m_calculatedAxis133 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx134 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis133, i32 0, i32 2
  %36 = bitcast %class.btVector3* %arrayidx134 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp130 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  br label %sw.epilog136

sw.default135:                                    ; preds = %sw.epilog
  br label %sw.epilog136

sw.epilog136:                                     ; preds = %sw.default135, %sw.bb115, %sw.bb95, %sw.bb75, %sw.bb55, %sw.bb36, %sw.bb20
  %m_calculatedAxis137 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx138 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis137, i32 0, i32 0
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx138)
  %m_calculatedAxis140 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx141 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis140, i32 0, i32 1
  %call142 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx141)
  %m_calculatedAxis143 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %arrayidx144 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis143, i32 0, i32 2
  %call145 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx144)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofSpring2Constraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp2 = alloca %class.btTransform, align 4
  %miA = alloca float, align 4
  %miB = alloca float, align 4
  %miS = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInA)
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp2, %class.btTransform* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp2)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateLinearInfoEv(%class.btGeneric6DofSpring2Constraint* %this1)
  call void @_ZN30btGeneric6DofSpring2Constraint18calculateAngleInfoEv(%class.btGeneric6DofSpring2Constraint* %this1)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %call4 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %2)
  %call5 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call4)
  store float %call5, float* %miA, align 4
  %3 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %call6 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %3)
  %call7 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call6)
  store float %call7, float* %miB, align 4
  %4 = load float, float* %miA, align 4
  %cmp = fcmp olt float %4, 0x3E80000000000000
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %5 = load float, float* %miB, align 4
  %cmp8 = fcmp olt float %5, 0x3E80000000000000
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp8, %lor.rhs ]
  %m_hasStaticBody = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 15
  %frombool = zext i1 %6 to i8
  store i8 %frombool, i8* %m_hasStaticBody, align 4
  %7 = load float, float* %miA, align 4
  %8 = load float, float* %miB, align 4
  %add = fadd float %7, %8
  store float %add, float* %miS, align 4
  %9 = load float, float* %miS, align 4
  %cmp9 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp9, label %if.then, label %if.else

if.then:                                          ; preds = %lor.end
  %10 = load float, float* %miB, align 4
  %11 = load float, float* %miS, align 4
  %div = fdiv float %10, %11
  %m_factA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 13
  store float %div, float* %m_factA, align 4
  br label %if.end

if.else:                                          ; preds = %lor.end
  %m_factA10 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 13
  store float 5.000000e-01, float* %m_factA10, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_factA11 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 13
  %12 = load float, float* %m_factA11, align 4
  %sub = fsub float 1.000000e+00, %12
  %m_factB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 14
  store float %sub, float* %m_factB, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19calculateLinearInfoEv(%class.btGeneric6DofSpring2Constraint* %this) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btMatrix3x3, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB)
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %0 = bitcast %class.btVector3* %m_calculatedLinearDiff to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %m_calculatedTransformA5 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA5)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp4, %class.btMatrix3x3* %call6)
  %m_calculatedLinearDiff7 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_calculatedLinearDiff7)
  %m_calculatedLinearDiff8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %2 = bitcast %class.btVector3* %m_calculatedLinearDiff8 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_calculatedLinearDiff9 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff9)
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call10, i32 %5
  %6 = load float, float* %arrayidx, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 20
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLinearDiff)
  %7 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 %7
  store float %6, float* %arrayidx12, align 4
  %m_linearLimits13 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %8 = load i32, i32* %i, align 4
  %m_calculatedLinearDiff14 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff14)
  %9 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %9
  %10 = load float, float* %arrayidx16, align 4
  call void @_ZN26btTranslationalLimitMotor214testLimitValueEif(%class.btTranslationalLimitMotor2* %m_linearLimits13, i32 %8, float %10)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint21testAngularLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %this, i32 %axis_index) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %axis_index.addr = alloca i32, align 4
  %angle = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %axis_index, i32* %axis_index.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %0 = load i32, i32* %axis_index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %angle, align 4
  %2 = load float, float* %angle, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %3 = load i32, i32* %axis_index.addr, align 4
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %3
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 0
  %4 = load float, float* %m_loLimit, align 4
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %5 = load i32, i32* %axis_index.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits3, i32 0, i32 %5
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 1
  %6 = load float, float* %m_hiLimit, align 4
  %call5 = call float @_Z21btAdjustAngleToLimitsfff(float %2, float %4, float %6)
  store float %call5, float* %angle, align 4
  %7 = load float, float* %angle, align 4
  %m_angularLimits6 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %axis_index.addr, align 4
  %arrayidx7 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits6, i32 0, i32 %8
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx7, i32 0, i32 20
  store float %7, float* %m_currentPosition, align 4
  %m_angularLimits8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %axis_index.addr, align 4
  %arrayidx9 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits8, i32 0, i32 %9
  %10 = load float, float* %angle, align 4
  call void @_ZN23btRotationalLimitMotor214testLimitValueEf(%class.btRotationalLimitMotor2* %arrayidx9, float %10)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z21btAdjustAngleToLimitsfff(float %angleInRadians, float %angleLowerLimitInRadians, float %angleUpperLimitInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  %angleLowerLimitInRadians.addr = alloca float, align 4
  %angleUpperLimitInRadians.addr = alloca float, align 4
  %diffLo = alloca float, align 4
  %diffHi = alloca float, align 4
  %diffHi11 = alloca float, align 4
  %diffLo15 = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  store float %angleLowerLimitInRadians, float* %angleLowerLimitInRadians.addr, align 4
  store float %angleUpperLimitInRadians, float* %angleUpperLimitInRadians.addr, align 4
  %0 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %1 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %cmp = fcmp oge float %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  store float %2, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %4 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %cmp1 = fcmp olt float %3, %4
  br i1 %cmp1, label %if.then2, label %if.else8

if.then2:                                         ; preds = %if.else
  %5 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %6 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %5, %6
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  %call3 = call float @_Z6btFabsf(float %call)
  store float %call3, float* %diffLo, align 4
  %7 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %8 = load float, float* %angleInRadians.addr, align 4
  %sub4 = fsub float %7, %8
  %call5 = call float @_Z16btNormalizeAnglef(float %sub4)
  %call6 = call float @_Z6btFabsf(float %call5)
  store float %call6, float* %diffHi, align 4
  %9 = load float, float* %diffLo, align 4
  %10 = load float, float* %diffHi, align 4
  %cmp7 = fcmp olt float %9, %10
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then2
  %11 = load float, float* %angleInRadians.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then2
  %12 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %12, 0x401921FB60000000
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %11, %cond.true ], [ %add, %cond.false ]
  store float %cond, float* %retval, align 4
  br label %return

if.else8:                                         ; preds = %if.else
  %13 = load float, float* %angleInRadians.addr, align 4
  %14 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %cmp9 = fcmp ogt float %13, %14
  br i1 %cmp9, label %if.then10, label %if.else25

if.then10:                                        ; preds = %if.else8
  %15 = load float, float* %angleInRadians.addr, align 4
  %16 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %sub12 = fsub float %15, %16
  %call13 = call float @_Z16btNormalizeAnglef(float %sub12)
  %call14 = call float @_Z6btFabsf(float %call13)
  store float %call14, float* %diffHi11, align 4
  %17 = load float, float* %angleInRadians.addr, align 4
  %18 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %sub16 = fsub float %17, %18
  %call17 = call float @_Z16btNormalizeAnglef(float %sub16)
  %call18 = call float @_Z6btFabsf(float %call17)
  store float %call18, float* %diffLo15, align 4
  %19 = load float, float* %diffLo15, align 4
  %20 = load float, float* %diffHi11, align 4
  %cmp19 = fcmp olt float %19, %20
  br i1 %cmp19, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %if.then10
  %21 = load float, float* %angleInRadians.addr, align 4
  %sub21 = fsub float %21, 0x401921FB60000000
  br label %cond.end23

cond.false22:                                     ; preds = %if.then10
  %22 = load float, float* %angleInRadians.addr, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi float [ %sub21, %cond.true20 ], [ %22, %cond.false22 ]
  store float %cond24, float* %retval, align 4
  br label %return

if.else25:                                        ; preds = %if.else8
  %23 = load float, float* %angleInRadians.addr, align 4
  store float %23, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else25, %cond.end23, %cond.end, %if.then
  %24 = load float, float* %retval, align 4
  ret float %24
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btRotationalLimitMotor214testLimitValueEf(%class.btRotationalLimitMotor2* %this, float %test_value) #1 {
entry:
  %this.addr = alloca %class.btRotationalLimitMotor2*, align 4
  %test_value.addr = alloca float, align 4
  store %class.btRotationalLimitMotor2* %this, %class.btRotationalLimitMotor2** %this.addr, align 4
  store float %test_value, float* %test_value.addr, align 4
  %this1 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %this.addr, align 4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 0
  %0 = load float, float* %m_loLimit, align 4
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 1
  %1 = load float, float* %m_hiLimit, align 4
  %cmp = fcmp ogt float %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 21
  store i32 0, i32* %m_currentLimit, align 4
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_currentLimitError, align 4
  br label %if.end16

if.else:                                          ; preds = %entry
  %m_loLimit2 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 0
  %2 = load float, float* %m_loLimit2, align 4
  %m_hiLimit3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 1
  %3 = load float, float* %m_hiLimit3, align 4
  %cmp4 = fcmp oeq float %2, %3
  br i1 %cmp4, label %if.then5, label %if.else9

if.then5:                                         ; preds = %if.else
  %4 = load float, float* %test_value.addr, align 4
  %m_loLimit6 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 0
  %5 = load float, float* %m_loLimit6, align 4
  %sub = fsub float %4, %5
  %m_currentLimitError7 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 18
  store float %sub, float* %m_currentLimitError7, align 4
  %m_currentLimit8 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 21
  store i32 3, i32* %m_currentLimit8, align 4
  br label %if.end

if.else9:                                         ; preds = %if.else
  %6 = load float, float* %test_value.addr, align 4
  %m_loLimit10 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 0
  %7 = load float, float* %m_loLimit10, align 4
  %sub11 = fsub float %6, %7
  %m_currentLimitError12 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 18
  store float %sub11, float* %m_currentLimitError12, align 4
  %8 = load float, float* %test_value.addr, align 4
  %m_hiLimit13 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 1
  %9 = load float, float* %m_hiLimit13, align 4
  %sub14 = fsub float %8, %9
  %m_currentLimitErrorHi = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 19
  store float %sub14, float* %m_currentLimitErrorHi, align 4
  %m_currentLimit15 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %this1, i32 0, i32 21
  store i32 4, i32* %m_currentLimit15, align 4
  br label %if.end

if.end:                                           ; preds = %if.else9, %if.then5
  br label %if.end16

if.end16:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btGeneric6DofSpring2Constraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsERK11btTransformS2_(%class.btGeneric6DofSpring2Constraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %5 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %5, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 21
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx, align 4
  %cmp3 = icmp eq i32 %8, 4
  br i1 %cmp3, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %9 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows4 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_numConstraintRows4, align 4
  %add = add nsw i32 %10, 2
  store i32 %add, i32* %m_numConstraintRows4, align 4
  br label %if.end12

if.else:                                          ; preds = %for.body
  %m_linearLimits5 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimit6 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits5, i32 0, i32 21
  %11 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit6, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx7, align 4
  %cmp8 = icmp ne i32 %12, 0
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.else
  %13 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %13, i32 0, i32 0
  %14 = load i32, i32* %m_numConstraintRows10, align 4
  %add11 = add nsw i32 %14, 1
  store i32 %add11, i32* %m_numConstraintRows10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.else
  br label %if.end12

if.end12:                                         ; preds = %if.end, %if.then
  %m_linearLimits13 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits13, i32 0, i32 7
  %15 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %15
  %16 = load i8, i8* %arrayidx14, align 1
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %if.then15, label %if.end18

if.then15:                                        ; preds = %if.end12
  %17 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %17, i32 0, i32 0
  %18 = load i32, i32* %m_numConstraintRows16, align 4
  %add17 = add nsw i32 %18, 1
  store i32 %add17, i32* %m_numConstraintRows16, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then15, %if.end12
  %m_linearLimits19 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits19, i32 0, i32 9
  %19 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring, i32 0, i32 %19
  %20 = load i8, i8* %arrayidx20, align 1
  %tobool21 = trunc i8 %20 to i1
  br i1 %tobool21, label %if.then22, label %if.end25

if.then22:                                        ; preds = %if.end18
  %21 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows23 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %21, i32 0, i32 0
  %22 = load i32, i32* %m_numConstraintRows23, align 4
  %add24 = add nsw i32 %22, 1
  store i32 %add24, i32* %m_numConstraintRows23, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then22, %if.end18
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc61, %for.end
  %24 = load i32, i32* %i, align 4
  %cmp27 = icmp slt i32 %24, 3
  br i1 %cmp27, label %for.body28, label %for.end63

for.body28:                                       ; preds = %for.cond26
  %25 = load i32, i32* %i, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint21testAngularLimitMotorEi(%class.btGeneric6DofSpring2Constraint* %this1, i32 %25)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %26 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %26
  %m_currentLimit30 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx29, i32 0, i32 21
  %27 = load i32, i32* %m_currentLimit30, align 4
  %cmp31 = icmp eq i32 %27, 4
  br i1 %cmp31, label %if.then32, label %if.else35

if.then32:                                        ; preds = %for.body28
  %28 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows33 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %28, i32 0, i32 0
  %29 = load i32, i32* %m_numConstraintRows33, align 4
  %add34 = add nsw i32 %29, 2
  store i32 %add34, i32* %m_numConstraintRows33, align 4
  br label %if.end44

if.else35:                                        ; preds = %for.body28
  %m_angularLimits36 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %30 = load i32, i32* %i, align 4
  %arrayidx37 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits36, i32 0, i32 %30
  %m_currentLimit38 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx37, i32 0, i32 21
  %31 = load i32, i32* %m_currentLimit38, align 4
  %cmp39 = icmp ne i32 %31, 0
  br i1 %cmp39, label %if.then40, label %if.end43

if.then40:                                        ; preds = %if.else35
  %32 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows41 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %32, i32 0, i32 0
  %33 = load i32, i32* %m_numConstraintRows41, align 4
  %add42 = add nsw i32 %33, 1
  store i32 %add42, i32* %m_numConstraintRows41, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then40, %if.else35
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.then32
  %m_angularLimits45 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %34 = load i32, i32* %i, align 4
  %arrayidx46 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits45, i32 0, i32 %34
  %m_enableMotor47 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx46, i32 0, i32 7
  %35 = load i8, i8* %m_enableMotor47, align 4
  %tobool48 = trunc i8 %35 to i1
  br i1 %tobool48, label %if.then49, label %if.end52

if.then49:                                        ; preds = %if.end44
  %36 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows50 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %36, i32 0, i32 0
  %37 = load i32, i32* %m_numConstraintRows50, align 4
  %add51 = add nsw i32 %37, 1
  store i32 %add51, i32* %m_numConstraintRows50, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.then49, %if.end44
  %m_angularLimits53 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %38 = load i32, i32* %i, align 4
  %arrayidx54 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits53, i32 0, i32 %38
  %m_enableSpring55 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx54, i32 0, i32 12
  %39 = load i8, i8* %m_enableSpring55, align 4
  %tobool56 = trunc i8 %39 to i1
  br i1 %tobool56, label %if.then57, label %if.end60

if.then57:                                        ; preds = %if.end52
  %40 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows58 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %40, i32 0, i32 0
  %41 = load i32, i32* %m_numConstraintRows58, align 4
  %add59 = add nsw i32 %41, 1
  store i32 %add59, i32* %m_numConstraintRows58, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then57, %if.end52
  br label %for.inc61

for.inc61:                                        ; preds = %if.end60
  %42 = load i32, i32* %i, align 4
  %inc62 = add nsw i32 %42, 1
  store i32 %inc62, i32* %i, align 4
  br label %for.cond26

for.end63:                                        ; preds = %for.cond26
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofSpring2Constraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA = alloca %class.btTransform*, align 4
  %transB = alloca %class.btTransform*, align 4
  %linVelA = alloca %class.btVector3*, align 4
  %linVelB = alloca %class.btVector3*, align 4
  %angVelA = alloca %class.btVector3*, align 4
  %angVelB = alloca %class.btVector3*, align 4
  %row = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  store %class.btTransform* %call, %class.btTransform** %transA, align 4
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  store %class.btTransform* %call2, %class.btTransform** %transB, align 4
  %4 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 8
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %5)
  store %class.btVector3* %call4, %class.btVector3** %linVelA, align 4
  %6 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %6, i32 0, i32 9
  %7 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %7)
  store %class.btVector3* %call6, %class.btVector3** %linVelB, align 4
  %8 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 8
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %9)
  store %class.btVector3* %call8, %class.btVector3** %angVelA, align 4
  %10 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %10, i32 0, i32 9
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %11)
  store %class.btVector3* %call10, %class.btVector3** %angVelB, align 4
  %12 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %13 = load %class.btTransform*, %class.btTransform** %transA, align 4
  %14 = load %class.btTransform*, %class.btTransform** %transB, align 4
  %15 = load %class.btVector3*, %class.btVector3** %linVelA, align 4
  %16 = load %class.btVector3*, %class.btVector3** %linVelB, align 4
  %17 = load %class.btVector3*, %class.btVector3** %angVelA, align 4
  %18 = load %class.btVector3*, %class.btVector3** %angVelB, align 4
  %call11 = call i32 @_ZN30btGeneric6DofSpring2Constraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofSpring2Constraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %12, i32 0, %class.btTransform* nonnull align 4 dereferenceable(64) %13, %class.btTransform* nonnull align 4 dereferenceable(64) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  store i32 %call11, i32* %row, align 4
  %19 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %20 = load i32, i32* %row, align 4
  %21 = load %class.btTransform*, %class.btTransform** %transA, align 4
  %22 = load %class.btTransform*, %class.btTransform** %transB, align 4
  %23 = load %class.btVector3*, %class.btVector3** %linVelA, align 4
  %24 = load %class.btVector3*, %class.btVector3** %linVelB, align 4
  %25 = load %class.btVector3*, %class.btVector3** %angVelA, align 4
  %26 = load %class.btVector3*, %class.btVector3** %angVelB, align 4
  %call12 = call i32 @_ZN30btGeneric6DofSpring2Constraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofSpring2Constraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %19, i32 %20, %class.btTransform* nonnull align 4 dereferenceable(64) %21, %class.btTransform* nonnull align 4 dereferenceable(64) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN30btGeneric6DofSpring2Constraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofSpring2Constraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row_offset, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row_offset.addr = alloca i32, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %row = alloca i32, align 4
  %cIdx = alloca [3 x i32], align 4
  %ii = alloca i32, align 4
  %i = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %flags = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store i32 %row_offset, i32* %row_offset.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %row_offset.addr, align 4
  store i32 %0, i32* %row, align 4
  %1 = bitcast [3 x i32]* %cIdx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast ([3 x i32]* @__const._ZN30btGeneric6DofSpring2Constraint16setAngularLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_.cIdx to i8*), i32 12, i1 false)
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_rotateOrder, align 4
  switch i32 %2, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb4
    i32 2, label %sw.bb8
    i32 3, label %sw.bb12
    i32 4, label %sw.bb16
    i32 5, label %sw.bb20
  ]

sw.bb:                                            ; preds = %entry
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 1, i32* %arrayidx2, align 4
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 2, i32* %arrayidx3, align 4
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 0, i32* %arrayidx5, align 4
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 2, i32* %arrayidx6, align 4
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 1, i32* %arrayidx7, align 4
  br label %sw.epilog

sw.bb8:                                           ; preds = %entry
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 1, i32* %arrayidx9, align 4
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 0, i32* %arrayidx10, align 4
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 2, i32* %arrayidx11, align 4
  br label %sw.epilog

sw.bb12:                                          ; preds = %entry
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 1, i32* %arrayidx13, align 4
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 2, i32* %arrayidx14, align 4
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 0, i32* %arrayidx15, align 4
  br label %sw.epilog

sw.bb16:                                          ; preds = %entry
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 2, i32* %arrayidx17, align 4
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 0, i32* %arrayidx18, align 4
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 1, i32* %arrayidx19, align 4
  br label %sw.epilog

sw.bb20:                                          ; preds = %entry
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 0
  store i32 2, i32* %arrayidx21, align 4
  %arrayidx22 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 1
  store i32 1, i32* %arrayidx22, align 4
  %arrayidx23 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 2
  store i32 0, i32* %arrayidx23, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb20, %sw.bb16, %sw.bb12, %sw.bb8, %sw.bb4, %sw.bb
  store i32 0, i32* %ii, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.epilog
  %3 = load i32, i32* %ii, align 4
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %ii, align 4
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %cIdx, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx24, align 4
  store i32 %5, i32* %i, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %6 = load i32, i32* %i, align 4
  %arrayidx25 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %6
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx25, i32 0, i32 21
  %7 = load i32, i32* %m_currentLimit, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %m_angularLimits26 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits26, i32 0, i32 %8
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx27, i32 0, i32 7
  %9 = load i8, i8* %m_enableMotor, align 4
  %tobool28 = trunc i8 %9 to i1
  br i1 %tobool28, label %if.then, label %lor.lhs.false29

lor.lhs.false29:                                  ; preds = %lor.lhs.false
  %m_angularLimits30 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %10 = load i32, i32* %i, align 4
  %arrayidx31 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits30, i32 0, i32 %10
  %m_enableSpring = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx31, i32 0, i32 12
  %11 = load i8, i8* %m_enableSpring, align 4
  %tobool32 = trunc i8 %11 to i1
  br i1 %tobool32, label %if.then, label %if.end62

if.then:                                          ; preds = %lor.lhs.false29, %lor.lhs.false, %for.body
  %12 = load i32, i32* %i, align 4
  call void @_ZNK30btGeneric6DofSpring2Constraint7getAxisEi(%class.btVector3* sret align 4 %axis, %class.btGeneric6DofSpring2Constraint* %this1, i32 %12)
  %m_flags = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %13 = load i32, i32* %m_flags, align 4
  %14 = load i32, i32* %i, align 4
  %add = add nsw i32 %14, 3
  %mul = mul nsw i32 %add, 4
  %shr = ashr i32 %13, %mul
  store i32 %shr, i32* %flags, align 4
  %15 = load i32, i32* %flags, align 4
  %and = and i32 %15, 1
  %tobool33 = icmp ne i32 %and, 0
  br i1 %tobool33, label %if.end, label %if.then34

if.then34:                                        ; preds = %if.then
  %16 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %16, i32 0, i32 8
  %17 = load float*, float** %cfm, align 4
  %arrayidx35 = getelementptr inbounds float, float* %17, i32 0
  %18 = load float, float* %arrayidx35, align 4
  %m_angularLimits36 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %19 = load i32, i32* %i, align 4
  %arrayidx37 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits36, i32 0, i32 %19
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx37, i32 0, i32 4
  store float %18, float* %m_stopCFM, align 4
  br label %if.end

if.end:                                           ; preds = %if.then34, %if.then
  %20 = load i32, i32* %flags, align 4
  %and38 = and i32 %20, 2
  %tobool39 = icmp ne i32 %and38, 0
  br i1 %tobool39, label %if.end43, label %if.then40

if.then40:                                        ; preds = %if.end
  %21 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %21, i32 0, i32 1
  %22 = load float, float* %erp, align 4
  %m_angularLimits41 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %23 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits41, i32 0, i32 %23
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx42, i32 0, i32 3
  store float %22, float* %m_stopERP, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then40, %if.end
  %24 = load i32, i32* %flags, align 4
  %and44 = and i32 %24, 4
  %tobool45 = icmp ne i32 %and44, 0
  br i1 %tobool45, label %if.end51, label %if.then46

if.then46:                                        ; preds = %if.end43
  %25 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm47 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %25, i32 0, i32 8
  %26 = load float*, float** %cfm47, align 4
  %arrayidx48 = getelementptr inbounds float, float* %26, i32 0
  %27 = load float, float* %arrayidx48, align 4
  %m_angularLimits49 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %28 = load i32, i32* %i, align 4
  %arrayidx50 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits49, i32 0, i32 %28
  %m_motorCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx50, i32 0, i32 6
  store float %27, float* %m_motorCFM, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.then46, %if.end43
  %29 = load i32, i32* %flags, align 4
  %and52 = and i32 %29, 8
  %tobool53 = icmp ne i32 %and52, 0
  br i1 %tobool53, label %if.end58, label %if.then54

if.then54:                                        ; preds = %if.end51
  %30 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp55 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %30, i32 0, i32 1
  %31 = load float, float* %erp55, align 4
  %m_angularLimits56 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %32 = load i32, i32* %i, align 4
  %arrayidx57 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits56, i32 0, i32 %32
  %m_motorERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx57, i32 0, i32 5
  store float %31, float* %m_motorERP, align 4
  br label %if.end58

if.end58:                                         ; preds = %if.then54, %if.end51
  %m_angularLimits59 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %33 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits59, i32 0, i32 %33
  %34 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %35 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %36 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4
  %37 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4
  %38 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %39 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %40 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %41 = load i32, i32* %row, align 4
  %call = call i32 @_ZN30btGeneric6DofSpring2Constraint21get_limit_motor_info2EP23btRotationalLimitMotor2RK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %arrayidx60, %class.btTransform* nonnull align 4 dereferenceable(64) %34, %class.btTransform* nonnull align 4 dereferenceable(64) %35, %class.btVector3* nonnull align 4 dereferenceable(16) %36, %class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %38, %class.btVector3* nonnull align 4 dereferenceable(16) %39, %"struct.btTypedConstraint::btConstraintInfo2"* %40, i32 %41, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, i32 1, i32 0)
  %42 = load i32, i32* %row, align 4
  %add61 = add nsw i32 %42, %call
  store i32 %add61, i32* %row, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.end58, %lor.lhs.false29
  br label %for.inc

for.inc:                                          ; preds = %if.end62
  %43 = load i32, i32* %ii, align 4
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %ii, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = load i32, i32* %row, align 4
  ret i32 %44
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN30btGeneric6DofSpring2Constraint15setLinearLimitsEPN17btTypedConstraint17btConstraintInfo2EiRK11btTransformS5_RK9btVector3S8_S8_S8_(%class.btGeneric6DofSpring2Constraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row.addr = alloca i32, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %limot = alloca %class.btRotationalLimitMotor2, align 4
  %i = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %flags = alloca i32, align 4
  %indx1 = alloca i32, align 4
  %indx2 = alloca i32, align 4
  %rotAllowed = alloca i32, align 4
  %indx1Violated = alloca i8, align 1
  %indx2Violated = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %call = call %class.btRotationalLimitMotor2* @_ZN23btRotationalLimitMotor2C2Ev(%class.btRotationalLimitMotor2* %limot)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 21
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %m_linearLimits2 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits2, i32 0, i32 7
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %3
  %4 = load i8, i8* %arrayidx3, align 1
  %tobool4 = trunc i8 %4 to i1
  br i1 %tobool4, label %if.then, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %lor.lhs.false
  %m_linearLimits6 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits6, i32 0, i32 9
  %5 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring, i32 0, i32 %5
  %6 = load i8, i8* %arrayidx7, align 1
  %tobool8 = trunc i8 %6 to i1
  br i1 %tobool8, label %if.then, label %if.end219

if.then:                                          ; preds = %lor.lhs.false5, %lor.lhs.false, %for.body
  %m_linearLimits9 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_bounce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits9, i32 0, i32 2
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_bounce)
  %7 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 %7
  %8 = load float, float* %arrayidx11, align 4
  %m_bounce12 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 2
  store float %8, float* %m_bounce12, align 4
  %m_linearLimits13 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimit14 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits13, i32 0, i32 21
  %9 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit14, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx15, align 4
  %m_currentLimit16 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 21
  store i32 %10, i32* %m_currentLimit16, align 4
  %m_linearLimits17 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLinearDiff = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits17, i32 0, i32 20
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLinearDiff)
  %11 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %11
  %12 = load float, float* %arrayidx19, align 4
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 20
  store float %12, float* %m_currentPosition, align 4
  %m_linearLimits20 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits20, i32 0, i32 18
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError)
  %13 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 %13
  %14 = load float, float* %arrayidx22, align 4
  %m_currentLimitError23 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 18
  store float %14, float* %m_currentLimitError23, align 4
  %m_linearLimits24 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_currentLimitErrorHi = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits24, i32 0, i32 19
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitErrorHi)
  %15 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %15
  %16 = load float, float* %arrayidx26, align 4
  %m_currentLimitErrorHi27 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 19
  store float %16, float* %m_currentLimitErrorHi27, align 4
  %m_linearLimits28 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor29 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits28, i32 0, i32 7
  %17 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor29, i32 0, i32 %17
  %18 = load i8, i8* %arrayidx30, align 1
  %tobool31 = trunc i8 %18 to i1
  %m_enableMotor32 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 7
  %frombool = zext i1 %tobool31 to i8
  store i8 %frombool, i8* %m_enableMotor32, align 4
  %m_linearLimits33 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits33, i32 0, i32 8
  %19 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [3 x i8], [3 x i8]* %m_servoMotor, i32 0, i32 %19
  %20 = load i8, i8* %arrayidx34, align 1
  %tobool35 = trunc i8 %20 to i1
  %m_servoMotor36 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 10
  %frombool37 = zext i1 %tobool35 to i8
  store i8 %frombool37, i8* %m_servoMotor36, align 4
  %m_linearLimits38 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoTarget = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits38, i32 0, i32 10
  %call39 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_servoTarget)
  %21 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 %21
  %22 = load float, float* %arrayidx40, align 4
  %m_servoTarget41 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 11
  store float %22, float* %m_servoTarget41, align 4
  %m_linearLimits42 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring43 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits42, i32 0, i32 9
  %23 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring43, i32 0, i32 %23
  %24 = load i8, i8* %arrayidx44, align 1
  %tobool45 = trunc i8 %24 to i1
  %m_enableSpring46 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 12
  %frombool47 = zext i1 %tobool45 to i8
  store i8 %frombool47, i8* %m_enableSpring46, align 4
  %m_linearLimits48 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffness = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits48, i32 0, i32 11
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springStiffness)
  %25 = load i32, i32* %i, align 4
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 %25
  %26 = load float, float* %arrayidx50, align 4
  %m_springStiffness51 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 13
  store float %26, float* %m_springStiffness51, align 4
  %m_linearLimits52 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffnessLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits52, i32 0, i32 12
  %27 = load i32, i32* %i, align 4
  %arrayidx53 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springStiffnessLimited, i32 0, i32 %27
  %28 = load i8, i8* %arrayidx53, align 1
  %tobool54 = trunc i8 %28 to i1
  %m_springStiffnessLimited55 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 14
  %frombool56 = zext i1 %tobool54 to i8
  store i8 %frombool56, i8* %m_springStiffnessLimited55, align 4
  %m_linearLimits57 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDamping = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits57, i32 0, i32 13
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springDamping)
  %29 = load i32, i32* %i, align 4
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %29
  %30 = load float, float* %arrayidx59, align 4
  %m_springDamping60 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 15
  store float %30, float* %m_springDamping60, align 4
  %m_linearLimits61 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDampingLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits61, i32 0, i32 14
  %31 = load i32, i32* %i, align 4
  %arrayidx62 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springDampingLimited, i32 0, i32 %31
  %32 = load i8, i8* %arrayidx62, align 1
  %tobool63 = trunc i8 %32 to i1
  %m_springDampingLimited64 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 16
  %frombool65 = zext i1 %tobool63 to i8
  store i8 %frombool65, i8* %m_springDampingLimited64, align 4
  %m_linearLimits66 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits66, i32 0, i32 15
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_equilibriumPoint)
  %33 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %33
  %34 = load float, float* %arrayidx68, align 4
  %m_equilibriumPoint69 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 17
  store float %34, float* %m_equilibriumPoint69, align 4
  %m_linearLimits70 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits70, i32 0, i32 1
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %35 = load i32, i32* %i, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %35
  %36 = load float, float* %arrayidx72, align 4
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 1
  store float %36, float* %m_hiLimit, align 4
  %m_linearLimits73 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits73, i32 0, i32 0
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %37 = load i32, i32* %i, align 4
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 %37
  %38 = load float, float* %arrayidx75, align 4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 0
  store float %38, float* %m_loLimit, align 4
  %m_linearLimits76 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits76, i32 0, i32 17
  %call77 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce)
  %39 = load i32, i32* %i, align 4
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 %39
  %40 = load float, float* %arrayidx78, align 4
  %m_maxMotorForce79 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 9
  store float %40, float* %m_maxMotorForce79, align 4
  %m_linearLimits80 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits80, i32 0, i32 16
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity)
  %41 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 %41
  %42 = load float, float* %arrayidx82, align 4
  %m_targetVelocity83 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 8
  store float %42, float* %m_targetVelocity83, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call84 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  %43 = load i32, i32* %i, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axis, %class.btMatrix3x3* %call84, i32 %43)
  %m_flags = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %44 = load i32, i32* %m_flags, align 4
  %45 = load i32, i32* %i, align 4
  %mul = mul nsw i32 %45, 4
  %shr = ashr i32 %44, %mul
  store i32 %shr, i32* %flags, align 4
  %46 = load i32, i32* %flags, align 4
  %and = and i32 %46, 1
  %tobool85 = icmp ne i32 %and, 0
  br i1 %tobool85, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %m_linearLimits86 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits86, i32 0, i32 4
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopCFM)
  %47 = load i32, i32* %i, align 4
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %47
  %48 = load float, float* %arrayidx88, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %49 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %49, i32 0, i32 8
  %50 = load float*, float** %cfm, align 4
  %arrayidx89 = getelementptr inbounds float, float* %50, i32 0
  %51 = load float, float* %arrayidx89, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %48, %cond.true ], [ %51, %cond.false ]
  %m_stopCFM90 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 4
  store float %cond, float* %m_stopCFM90, align 4
  %52 = load i32, i32* %flags, align 4
  %and91 = and i32 %52, 2
  %tobool92 = icmp ne i32 %and91, 0
  br i1 %tobool92, label %cond.true93, label %cond.false97

cond.true93:                                      ; preds = %cond.end
  %m_linearLimits94 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits94, i32 0, i32 3
  %call95 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopERP)
  %53 = load i32, i32* %i, align 4
  %arrayidx96 = getelementptr inbounds float, float* %call95, i32 %53
  %54 = load float, float* %arrayidx96, align 4
  br label %cond.end98

cond.false97:                                     ; preds = %cond.end
  %55 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %55, i32 0, i32 1
  %56 = load float, float* %erp, align 4
  br label %cond.end98

cond.end98:                                       ; preds = %cond.false97, %cond.true93
  %cond99 = phi float [ %54, %cond.true93 ], [ %56, %cond.false97 ]
  %m_stopERP100 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 3
  store float %cond99, float* %m_stopERP100, align 4
  %57 = load i32, i32* %flags, align 4
  %and101 = and i32 %57, 4
  %tobool102 = icmp ne i32 %and101, 0
  br i1 %tobool102, label %cond.true103, label %cond.false107

cond.true103:                                     ; preds = %cond.end98
  %m_linearLimits104 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits104, i32 0, i32 6
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_motorCFM)
  %58 = load i32, i32* %i, align 4
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 %58
  %59 = load float, float* %arrayidx106, align 4
  br label %cond.end110

cond.false107:                                    ; preds = %cond.end98
  %60 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm108 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %60, i32 0, i32 8
  %61 = load float*, float** %cfm108, align 4
  %arrayidx109 = getelementptr inbounds float, float* %61, i32 0
  %62 = load float, float* %arrayidx109, align 4
  br label %cond.end110

cond.end110:                                      ; preds = %cond.false107, %cond.true103
  %cond111 = phi float [ %59, %cond.true103 ], [ %62, %cond.false107 ]
  %m_motorCFM112 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 6
  store float %cond111, float* %m_motorCFM112, align 4
  %63 = load i32, i32* %flags, align 4
  %and113 = and i32 %63, 8
  %tobool114 = icmp ne i32 %and113, 0
  br i1 %tobool114, label %cond.true115, label %cond.false119

cond.true115:                                     ; preds = %cond.end110
  %m_linearLimits116 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits116, i32 0, i32 5
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_motorERP)
  %64 = load i32, i32* %i, align 4
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 %64
  %65 = load float, float* %arrayidx118, align 4
  br label %cond.end121

cond.false119:                                    ; preds = %cond.end110
  %66 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp120 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %66, i32 0, i32 1
  %67 = load float, float* %erp120, align 4
  br label %cond.end121

cond.end121:                                      ; preds = %cond.false119, %cond.true115
  %cond122 = phi float [ %65, %cond.true115 ], [ %67, %cond.false119 ]
  %m_motorERP123 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %limot, i32 0, i32 5
  store float %cond122, float* %m_motorERP123, align 4
  %68 = load i32, i32* %i, align 4
  %add = add nsw i32 %68, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %indx1, align 4
  %69 = load i32, i32* %i, align 4
  %add124 = add nsw i32 %69, 2
  %rem125 = srem i32 %add124, 3
  store i32 %rem125, i32* %indx2, align 4
  store i32 1, i32* %rotAllowed, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %70 = load i32, i32* %indx1, align 4
  %arrayidx126 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %70
  %m_currentLimit127 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx126, i32 0, i32 21
  %71 = load i32, i32* %m_currentLimit127, align 4
  %cmp128 = icmp eq i32 %71, 1
  br i1 %cmp128, label %lor.end164, label %lor.lhs.false129

lor.lhs.false129:                                 ; preds = %cond.end121
  %m_angularLimits130 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %72 = load i32, i32* %indx1, align 4
  %arrayidx131 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits130, i32 0, i32 %72
  %m_currentLimit132 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx131, i32 0, i32 21
  %73 = load i32, i32* %m_currentLimit132, align 4
  %cmp133 = icmp eq i32 %73, 2
  br i1 %cmp133, label %lor.end164, label %lor.lhs.false134

lor.lhs.false134:                                 ; preds = %lor.lhs.false129
  %m_angularLimits135 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %74 = load i32, i32* %indx1, align 4
  %arrayidx136 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits135, i32 0, i32 %74
  %m_currentLimit137 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx136, i32 0, i32 21
  %75 = load i32, i32* %m_currentLimit137, align 4
  %cmp138 = icmp eq i32 %75, 3
  br i1 %cmp138, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false134
  %m_angularLimits139 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %76 = load i32, i32* %indx1, align 4
  %arrayidx140 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits139, i32 0, i32 %76
  %m_currentLimitError141 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx140, i32 0, i32 18
  %77 = load float, float* %m_currentLimitError141, align 4
  %conv = fpext float %77 to double
  %cmp142 = fcmp olt double %conv, -1.000000e-03
  br i1 %cmp142, label %lor.end164, label %lor.lhs.false143

lor.lhs.false143:                                 ; preds = %land.lhs.true
  %m_angularLimits144 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %78 = load i32, i32* %indx1, align 4
  %arrayidx145 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits144, i32 0, i32 %78
  %m_currentLimitError146 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx145, i32 0, i32 18
  %79 = load float, float* %m_currentLimitError146, align 4
  %conv147 = fpext float %79 to double
  %cmp148 = fcmp ogt double %conv147, 1.000000e-03
  br i1 %cmp148, label %lor.end164, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false143, %lor.lhs.false134
  %m_angularLimits149 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %80 = load i32, i32* %indx1, align 4
  %arrayidx150 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits149, i32 0, i32 %80
  %m_currentLimit151 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx150, i32 0, i32 21
  %81 = load i32, i32* %m_currentLimit151, align 4
  %cmp152 = icmp eq i32 %81, 4
  br i1 %cmp152, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %lor.rhs
  %m_angularLimits153 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %82 = load i32, i32* %indx1, align 4
  %arrayidx154 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits153, i32 0, i32 %82
  %m_currentLimitError155 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx154, i32 0, i32 18
  %83 = load float, float* %m_currentLimitError155, align 4
  %conv156 = fpext float %83 to double
  %cmp157 = fcmp olt double %conv156, -1.000000e-03
  br i1 %cmp157, label %lor.end, label %lor.rhs158

lor.rhs158:                                       ; preds = %land.rhs
  %m_angularLimits159 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %84 = load i32, i32* %indx1, align 4
  %arrayidx160 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits159, i32 0, i32 %84
  %m_currentLimitErrorHi161 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx160, i32 0, i32 19
  %85 = load float, float* %m_currentLimitErrorHi161, align 4
  %conv162 = fpext float %85 to double
  %cmp163 = fcmp ogt double %conv162, 1.000000e-03
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs158, %land.rhs
  %86 = phi i1 [ true, %land.rhs ], [ %cmp163, %lor.rhs158 ]
  br label %land.end

land.end:                                         ; preds = %lor.end, %lor.rhs
  %87 = phi i1 [ false, %lor.rhs ], [ %86, %lor.end ]
  br label %lor.end164

lor.end164:                                       ; preds = %land.end, %lor.lhs.false143, %land.lhs.true, %lor.lhs.false129, %cond.end121
  %88 = phi i1 [ true, %lor.lhs.false143 ], [ true, %land.lhs.true ], [ true, %lor.lhs.false129 ], [ true, %cond.end121 ], [ %87, %land.end ]
  %frombool165 = zext i1 %88 to i8
  store i8 %frombool165, i8* %indx1Violated, align 1
  %m_angularLimits166 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %89 = load i32, i32* %indx2, align 4
  %arrayidx167 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits166, i32 0, i32 %89
  %m_currentLimit168 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx167, i32 0, i32 21
  %90 = load i32, i32* %m_currentLimit168, align 4
  %cmp169 = icmp eq i32 %90, 1
  br i1 %cmp169, label %lor.end211, label %lor.lhs.false170

lor.lhs.false170:                                 ; preds = %lor.end164
  %m_angularLimits171 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %91 = load i32, i32* %indx2, align 4
  %arrayidx172 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits171, i32 0, i32 %91
  %m_currentLimit173 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx172, i32 0, i32 21
  %92 = load i32, i32* %m_currentLimit173, align 4
  %cmp174 = icmp eq i32 %92, 2
  br i1 %cmp174, label %lor.end211, label %lor.lhs.false175

lor.lhs.false175:                                 ; preds = %lor.lhs.false170
  %m_angularLimits176 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %93 = load i32, i32* %indx2, align 4
  %arrayidx177 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits176, i32 0, i32 %93
  %m_currentLimit178 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx177, i32 0, i32 21
  %94 = load i32, i32* %m_currentLimit178, align 4
  %cmp179 = icmp eq i32 %94, 3
  br i1 %cmp179, label %land.lhs.true180, label %lor.rhs192

land.lhs.true180:                                 ; preds = %lor.lhs.false175
  %m_angularLimits181 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %95 = load i32, i32* %indx2, align 4
  %arrayidx182 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits181, i32 0, i32 %95
  %m_currentLimitError183 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx182, i32 0, i32 18
  %96 = load float, float* %m_currentLimitError183, align 4
  %conv184 = fpext float %96 to double
  %cmp185 = fcmp olt double %conv184, -1.000000e-03
  br i1 %cmp185, label %lor.end211, label %lor.lhs.false186

lor.lhs.false186:                                 ; preds = %land.lhs.true180
  %m_angularLimits187 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %97 = load i32, i32* %indx2, align 4
  %arrayidx188 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits187, i32 0, i32 %97
  %m_currentLimitError189 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx188, i32 0, i32 18
  %98 = load float, float* %m_currentLimitError189, align 4
  %conv190 = fpext float %98 to double
  %cmp191 = fcmp ogt double %conv190, 1.000000e-03
  br i1 %cmp191, label %lor.end211, label %lor.rhs192

lor.rhs192:                                       ; preds = %lor.lhs.false186, %lor.lhs.false175
  %m_angularLimits193 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %99 = load i32, i32* %indx2, align 4
  %arrayidx194 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits193, i32 0, i32 %99
  %m_currentLimit195 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx194, i32 0, i32 21
  %100 = load i32, i32* %m_currentLimit195, align 4
  %cmp196 = icmp eq i32 %100, 4
  br i1 %cmp196, label %land.rhs197, label %land.end210

land.rhs197:                                      ; preds = %lor.rhs192
  %m_angularLimits198 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %101 = load i32, i32* %indx2, align 4
  %arrayidx199 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits198, i32 0, i32 %101
  %m_currentLimitError200 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx199, i32 0, i32 18
  %102 = load float, float* %m_currentLimitError200, align 4
  %conv201 = fpext float %102 to double
  %cmp202 = fcmp olt double %conv201, -1.000000e-03
  br i1 %cmp202, label %lor.end209, label %lor.rhs203

lor.rhs203:                                       ; preds = %land.rhs197
  %m_angularLimits204 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %103 = load i32, i32* %indx2, align 4
  %arrayidx205 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits204, i32 0, i32 %103
  %m_currentLimitErrorHi206 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx205, i32 0, i32 19
  %104 = load float, float* %m_currentLimitErrorHi206, align 4
  %conv207 = fpext float %104 to double
  %cmp208 = fcmp ogt double %conv207, 1.000000e-03
  br label %lor.end209

lor.end209:                                       ; preds = %lor.rhs203, %land.rhs197
  %105 = phi i1 [ true, %land.rhs197 ], [ %cmp208, %lor.rhs203 ]
  br label %land.end210

land.end210:                                      ; preds = %lor.end209, %lor.rhs192
  %106 = phi i1 [ false, %lor.rhs192 ], [ %105, %lor.end209 ]
  br label %lor.end211

lor.end211:                                       ; preds = %land.end210, %lor.lhs.false186, %land.lhs.true180, %lor.lhs.false170, %lor.end164
  %107 = phi i1 [ true, %lor.lhs.false186 ], [ true, %land.lhs.true180 ], [ true, %lor.lhs.false170 ], [ true, %lor.end164 ], [ %106, %land.end210 ]
  %frombool212 = zext i1 %107 to i8
  store i8 %frombool212, i8* %indx2Violated, align 1
  %108 = load i8, i8* %indx1Violated, align 1
  %tobool213 = trunc i8 %108 to i1
  br i1 %tobool213, label %land.lhs.true214, label %if.end

land.lhs.true214:                                 ; preds = %lor.end211
  %109 = load i8, i8* %indx2Violated, align 1
  %tobool215 = trunc i8 %109 to i1
  br i1 %tobool215, label %if.then216, label %if.end

if.then216:                                       ; preds = %land.lhs.true214
  store i32 0, i32* %rotAllowed, align 4
  br label %if.end

if.end:                                           ; preds = %if.then216, %land.lhs.true214, %lor.end211
  %110 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %111 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %112 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4
  %113 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4
  %114 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %115 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %116 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %117 = load i32, i32* %row.addr, align 4
  %118 = load i32, i32* %rotAllowed, align 4
  %call217 = call i32 @_ZN30btGeneric6DofSpring2Constraint21get_limit_motor_info2EP23btRotationalLimitMotor2RK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %110, %class.btTransform* nonnull align 4 dereferenceable(64) %111, %class.btVector3* nonnull align 4 dereferenceable(16) %112, %class.btVector3* nonnull align 4 dereferenceable(16) %113, %class.btVector3* nonnull align 4 dereferenceable(16) %114, %class.btVector3* nonnull align 4 dereferenceable(16) %115, %"struct.btTypedConstraint::btConstraintInfo2"* %116, i32 %117, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, i32 0, i32 %118)
  %119 = load i32, i32* %row.addr, align 4
  %add218 = add nsw i32 %119, %call217
  store i32 %add218, i32* %row.addr, align 4
  br label %if.end219

if.end219:                                        ; preds = %if.end, %lor.lhs.false5
  br label %for.inc

for.inc:                                          ; preds = %if.end219
  %120 = load i32, i32* %i, align 4
  %inc = add nsw i32 %120, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %121 = load i32, i32* %row.addr, align 4
  ret i32 %121
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN30btGeneric6DofSpring2Constraint21get_limit_motor_info2EP23btRotationalLimitMotor2RK11btTransformS4_RK9btVector3S7_S7_S7_PN17btTypedConstraint17btConstraintInfo2EiRS5_ii(%class.btGeneric6DofSpring2Constraint* %this, %class.btRotationalLimitMotor2* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %row, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, i32 %rotational, i32 %rotAllowed) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %limot.addr = alloca %class.btRotationalLimitMotor2*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %row.addr = alloca i32, align 4
  %ax1.addr = alloca %class.btVector3*, align 4
  %rotational.addr = alloca i32, align 4
  %rotAllowed.addr = alloca i32, align 4
  %count = alloca i32, align 4
  %srow = alloca i32, align 4
  %vel = alloca float, align 4
  %bounceerror = alloca float, align 4
  %bounceerror35 = alloca float, align 4
  %bounceerror75 = alloca float, align 4
  %bounceerror95 = alloca float, align 4
  %tag_vel = alloca float, align 4
  %mot_fact = alloca float, align 4
  %error = alloca float, align 4
  %targetvelocity = alloca float, align 4
  %tag_vel194 = alloca float, align 4
  %mot_fact196 = alloca float, align 4
  %lowLimit = alloca float, align 4
  %hiLimit = alloca float, align 4
  %error269 = alloca float, align 4
  %dt = alloca float, align 4
  %kd = alloca float, align 4
  %ks = alloca float, align 4
  %vel273 = alloca float, align 4
  %cfm285 = alloca float, align 4
  %mA = alloca float, align 4
  %mB = alloca float, align 4
  %m = alloca float, align 4
  %angularfreq = alloca float, align 4
  %fs = alloca float, align 4
  %fd = alloca float, align 4
  %f = alloca float, align 4
  %minf = alloca float, align 4
  %maxf = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btRotationalLimitMotor2* %limot, %class.btRotationalLimitMotor2** %limot.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store %class.btVector3* %ax1, %class.btVector3** %ax1.addr, align 4
  store i32 %rotational, i32* %rotational.addr, align 4
  store i32 %rotAllowed, i32* %rotAllowed.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 0, i32* %count, align 4
  %0 = load i32, i32* %row.addr, align 4
  %1 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %1, i32 0, i32 6
  %2 = load i32, i32* %rowskip, align 4
  %mul = mul nsw i32 %0, %2
  store i32 %mul, i32* %srow, align 4
  %3 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %3, i32 0, i32 21
  %4 = load i32, i32* %m_currentLimit, align 4
  %cmp = icmp eq i32 %4, 4
  br i1 %cmp, label %if.then, label %if.else122

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %rotational.addr, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %6 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %8 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %sub = fsub float %call, %call2
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %10 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %12 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %sub5 = fsub float %call3, %call4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %sub, %cond.true ], [ %sub5, %cond.false ]
  store float %cond, float* %vel, align 4
  %14 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %15 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %16 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %18 = load i32, i32* %srow, align 4
  %19 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %20 = load i32, i32* %rotational.addr, align 4
  %21 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %14, %class.btTransform* nonnull align 4 dereferenceable(64) %15, %class.btTransform* nonnull align 4 dereferenceable(64) %16, %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19, i32 %20, i32 %21)
  %22 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %22, i32 0, i32 0
  %23 = load float, float* %fps, align 4
  %24 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %24, i32 0, i32 3
  %25 = load float, float* %m_stopERP, align 4
  %mul6 = fmul float %23, %25
  %26 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentLimitError = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %26, i32 0, i32 18
  %27 = load float, float* %m_currentLimitError, align 4
  %mul7 = fmul float %mul6, %27
  %28 = load i32, i32* %rotational.addr, align 4
  %tobool8 = icmp ne i32 %28, 0
  %29 = zext i1 %tobool8 to i64
  %cond9 = select i1 %tobool8, i32 -1, i32 1
  %conv = sitofp i32 %cond9 to float
  %mul10 = fmul float %mul7, %conv
  %30 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %30, i32 0, i32 7
  %31 = load float*, float** %m_constraintError, align 4
  %32 = load i32, i32* %srow, align 4
  %arrayidx = getelementptr inbounds float, float* %31, i32 %32
  store float %mul10, float* %arrayidx, align 4
  %33 = load i32, i32* %rotational.addr, align 4
  %tobool11 = icmp ne i32 %33, 0
  br i1 %tobool11, label %if.then12, label %if.else

if.then12:                                        ; preds = %cond.end
  %34 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError13 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %34, i32 0, i32 7
  %35 = load float*, float** %m_constraintError13, align 4
  %36 = load i32, i32* %srow, align 4
  %arrayidx14 = getelementptr inbounds float, float* %35, i32 %36
  %37 = load float, float* %arrayidx14, align 4
  %38 = load float, float* %vel, align 4
  %39 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP15 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %39, i32 0, i32 3
  %40 = load float, float* %m_stopERP15, align 4
  %mul16 = fmul float %38, %40
  %sub17 = fsub float %37, %mul16
  %cmp18 = fcmp ogt float %sub17, 0.000000e+00
  br i1 %cmp18, label %if.then19, label %if.end27

if.then19:                                        ; preds = %if.then12
  %41 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %41, i32 0, i32 2
  %42 = load float, float* %m_bounce, align 4
  %fneg = fneg float %42
  %43 = load float, float* %vel, align 4
  %mul20 = fmul float %fneg, %43
  store float %mul20, float* %bounceerror, align 4
  %44 = load float, float* %bounceerror, align 4
  %45 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError21 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %45, i32 0, i32 7
  %46 = load float*, float** %m_constraintError21, align 4
  %47 = load i32, i32* %srow, align 4
  %arrayidx22 = getelementptr inbounds float, float* %46, i32 %47
  %48 = load float, float* %arrayidx22, align 4
  %cmp23 = fcmp ogt float %44, %48
  br i1 %cmp23, label %if.then24, label %if.end

if.then24:                                        ; preds = %if.then19
  %49 = load float, float* %bounceerror, align 4
  %50 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError25 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %50, i32 0, i32 7
  %51 = load float*, float** %m_constraintError25, align 4
  %52 = load i32, i32* %srow, align 4
  %arrayidx26 = getelementptr inbounds float, float* %51, i32 %52
  store float %49, float* %arrayidx26, align 4
  br label %if.end

if.end:                                           ; preds = %if.then24, %if.then19
  br label %if.end27

if.end27:                                         ; preds = %if.end, %if.then12
  br label %if.end47

if.else:                                          ; preds = %cond.end
  %53 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError28 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %53, i32 0, i32 7
  %54 = load float*, float** %m_constraintError28, align 4
  %55 = load i32, i32* %srow, align 4
  %arrayidx29 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx29, align 4
  %57 = load float, float* %vel, align 4
  %58 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP30 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %58, i32 0, i32 3
  %59 = load float, float* %m_stopERP30, align 4
  %mul31 = fmul float %57, %59
  %sub32 = fsub float %56, %mul31
  %cmp33 = fcmp olt float %sub32, 0.000000e+00
  br i1 %cmp33, label %if.then34, label %if.end46

if.then34:                                        ; preds = %if.else
  %60 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_bounce36 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %60, i32 0, i32 2
  %61 = load float, float* %m_bounce36, align 4
  %fneg37 = fneg float %61
  %62 = load float, float* %vel, align 4
  %mul38 = fmul float %fneg37, %62
  store float %mul38, float* %bounceerror35, align 4
  %63 = load float, float* %bounceerror35, align 4
  %64 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError39 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %64, i32 0, i32 7
  %65 = load float*, float** %m_constraintError39, align 4
  %66 = load i32, i32* %srow, align 4
  %arrayidx40 = getelementptr inbounds float, float* %65, i32 %66
  %67 = load float, float* %arrayidx40, align 4
  %cmp41 = fcmp olt float %63, %67
  br i1 %cmp41, label %if.then42, label %if.end45

if.then42:                                        ; preds = %if.then34
  %68 = load float, float* %bounceerror35, align 4
  %69 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError43 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %69, i32 0, i32 7
  %70 = load float*, float** %m_constraintError43, align 4
  %71 = load i32, i32* %srow, align 4
  %arrayidx44 = getelementptr inbounds float, float* %70, i32 %71
  store float %68, float* %arrayidx44, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.then42, %if.then34
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.else
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end27
  %72 = load i32, i32* %rotational.addr, align 4
  %tobool48 = icmp ne i32 %72, 0
  %73 = zext i1 %tobool48 to i64
  %cond49 = select i1 %tobool48, float 0.000000e+00, float 0xC7EFFFFFE0000000
  %74 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %74, i32 0, i32 9
  %75 = load float*, float** %m_lowerLimit, align 4
  %76 = load i32, i32* %srow, align 4
  %arrayidx50 = getelementptr inbounds float, float* %75, i32 %76
  store float %cond49, float* %arrayidx50, align 4
  %77 = load i32, i32* %rotational.addr, align 4
  %tobool51 = icmp ne i32 %77, 0
  %78 = zext i1 %tobool51 to i64
  %cond52 = select i1 %tobool51, float 0x47EFFFFFE0000000, float 0.000000e+00
  %79 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %79, i32 0, i32 10
  %80 = load float*, float** %m_upperLimit, align 4
  %81 = load i32, i32* %srow, align 4
  %arrayidx53 = getelementptr inbounds float, float* %80, i32 %81
  store float %cond52, float* %arrayidx53, align 4
  %82 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %82, i32 0, i32 4
  %83 = load float, float* %m_stopCFM, align 4
  %84 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %84, i32 0, i32 8
  %85 = load float*, float** %cfm, align 4
  %86 = load i32, i32* %srow, align 4
  %arrayidx54 = getelementptr inbounds float, float* %85, i32 %86
  store float %83, float* %arrayidx54, align 4
  %87 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip55 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %87, i32 0, i32 6
  %88 = load i32, i32* %rowskip55, align 4
  %89 = load i32, i32* %srow, align 4
  %add = add nsw i32 %89, %88
  store i32 %add, i32* %srow, align 4
  %90 = load i32, i32* %count, align 4
  %inc = add nsw i32 %90, 1
  store i32 %inc, i32* %count, align 4
  %91 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %92 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %93 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %94 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %95 = load i32, i32* %srow, align 4
  %96 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %97 = load i32, i32* %rotational.addr, align 4
  %98 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %91, %class.btTransform* nonnull align 4 dereferenceable(64) %92, %class.btTransform* nonnull align 4 dereferenceable(64) %93, %"struct.btTypedConstraint::btConstraintInfo2"* %94, i32 %95, %class.btVector3* nonnull align 4 dereferenceable(16) %96, i32 %97, i32 %98)
  %99 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps56 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %99, i32 0, i32 0
  %100 = load float, float* %fps56, align 4
  %101 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP57 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %101, i32 0, i32 3
  %102 = load float, float* %m_stopERP57, align 4
  %mul58 = fmul float %100, %102
  %103 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentLimitErrorHi = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %103, i32 0, i32 19
  %104 = load float, float* %m_currentLimitErrorHi, align 4
  %mul59 = fmul float %mul58, %104
  %105 = load i32, i32* %rotational.addr, align 4
  %tobool60 = icmp ne i32 %105, 0
  %106 = zext i1 %tobool60 to i64
  %cond61 = select i1 %tobool60, i32 -1, i32 1
  %conv62 = sitofp i32 %cond61 to float
  %mul63 = fmul float %mul59, %conv62
  %107 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError64 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %107, i32 0, i32 7
  %108 = load float*, float** %m_constraintError64, align 4
  %109 = load i32, i32* %srow, align 4
  %arrayidx65 = getelementptr inbounds float, float* %108, i32 %109
  store float %mul63, float* %arrayidx65, align 4
  %110 = load i32, i32* %rotational.addr, align 4
  %tobool66 = icmp ne i32 %110, 0
  br i1 %tobool66, label %if.then67, label %if.else87

if.then67:                                        ; preds = %if.end47
  %111 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError68 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %111, i32 0, i32 7
  %112 = load float*, float** %m_constraintError68, align 4
  %113 = load i32, i32* %srow, align 4
  %arrayidx69 = getelementptr inbounds float, float* %112, i32 %113
  %114 = load float, float* %arrayidx69, align 4
  %115 = load float, float* %vel, align 4
  %116 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP70 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %116, i32 0, i32 3
  %117 = load float, float* %m_stopERP70, align 4
  %mul71 = fmul float %115, %117
  %sub72 = fsub float %114, %mul71
  %cmp73 = fcmp olt float %sub72, 0.000000e+00
  br i1 %cmp73, label %if.then74, label %if.end86

if.then74:                                        ; preds = %if.then67
  %118 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_bounce76 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %118, i32 0, i32 2
  %119 = load float, float* %m_bounce76, align 4
  %fneg77 = fneg float %119
  %120 = load float, float* %vel, align 4
  %mul78 = fmul float %fneg77, %120
  store float %mul78, float* %bounceerror75, align 4
  %121 = load float, float* %bounceerror75, align 4
  %122 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError79 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %122, i32 0, i32 7
  %123 = load float*, float** %m_constraintError79, align 4
  %124 = load i32, i32* %srow, align 4
  %arrayidx80 = getelementptr inbounds float, float* %123, i32 %124
  %125 = load float, float* %arrayidx80, align 4
  %cmp81 = fcmp olt float %121, %125
  br i1 %cmp81, label %if.then82, label %if.end85

if.then82:                                        ; preds = %if.then74
  %126 = load float, float* %bounceerror75, align 4
  %127 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError83 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %127, i32 0, i32 7
  %128 = load float*, float** %m_constraintError83, align 4
  %129 = load i32, i32* %srow, align 4
  %arrayidx84 = getelementptr inbounds float, float* %128, i32 %129
  store float %126, float* %arrayidx84, align 4
  br label %if.end85

if.end85:                                         ; preds = %if.then82, %if.then74
  br label %if.end86

if.end86:                                         ; preds = %if.end85, %if.then67
  br label %if.end107

if.else87:                                        ; preds = %if.end47
  %130 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError88 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %130, i32 0, i32 7
  %131 = load float*, float** %m_constraintError88, align 4
  %132 = load i32, i32* %srow, align 4
  %arrayidx89 = getelementptr inbounds float, float* %131, i32 %132
  %133 = load float, float* %arrayidx89, align 4
  %134 = load float, float* %vel, align 4
  %135 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP90 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %135, i32 0, i32 3
  %136 = load float, float* %m_stopERP90, align 4
  %mul91 = fmul float %134, %136
  %sub92 = fsub float %133, %mul91
  %cmp93 = fcmp ogt float %sub92, 0.000000e+00
  br i1 %cmp93, label %if.then94, label %if.end106

if.then94:                                        ; preds = %if.else87
  %137 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_bounce96 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %137, i32 0, i32 2
  %138 = load float, float* %m_bounce96, align 4
  %fneg97 = fneg float %138
  %139 = load float, float* %vel, align 4
  %mul98 = fmul float %fneg97, %139
  store float %mul98, float* %bounceerror95, align 4
  %140 = load float, float* %bounceerror95, align 4
  %141 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError99 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %141, i32 0, i32 7
  %142 = load float*, float** %m_constraintError99, align 4
  %143 = load i32, i32* %srow, align 4
  %arrayidx100 = getelementptr inbounds float, float* %142, i32 %143
  %144 = load float, float* %arrayidx100, align 4
  %cmp101 = fcmp ogt float %140, %144
  br i1 %cmp101, label %if.then102, label %if.end105

if.then102:                                       ; preds = %if.then94
  %145 = load float, float* %bounceerror95, align 4
  %146 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError103 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %146, i32 0, i32 7
  %147 = load float*, float** %m_constraintError103, align 4
  %148 = load i32, i32* %srow, align 4
  %arrayidx104 = getelementptr inbounds float, float* %147, i32 %148
  store float %145, float* %arrayidx104, align 4
  br label %if.end105

if.end105:                                        ; preds = %if.then102, %if.then94
  br label %if.end106

if.end106:                                        ; preds = %if.end105, %if.else87
  br label %if.end107

if.end107:                                        ; preds = %if.end106, %if.end86
  %149 = load i32, i32* %rotational.addr, align 4
  %tobool108 = icmp ne i32 %149, 0
  %150 = zext i1 %tobool108 to i64
  %cond109 = select i1 %tobool108, float 0xC7EFFFFFE0000000, float 0.000000e+00
  %151 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit110 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %151, i32 0, i32 9
  %152 = load float*, float** %m_lowerLimit110, align 4
  %153 = load i32, i32* %srow, align 4
  %arrayidx111 = getelementptr inbounds float, float* %152, i32 %153
  store float %cond109, float* %arrayidx111, align 4
  %154 = load i32, i32* %rotational.addr, align 4
  %tobool112 = icmp ne i32 %154, 0
  %155 = zext i1 %tobool112 to i64
  %cond113 = select i1 %tobool112, float 0.000000e+00, float 0x47EFFFFFE0000000
  %156 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit114 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %156, i32 0, i32 10
  %157 = load float*, float** %m_upperLimit114, align 4
  %158 = load i32, i32* %srow, align 4
  %arrayidx115 = getelementptr inbounds float, float* %157, i32 %158
  store float %cond113, float* %arrayidx115, align 4
  %159 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopCFM116 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %159, i32 0, i32 4
  %160 = load float, float* %m_stopCFM116, align 4
  %161 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm117 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %161, i32 0, i32 8
  %162 = load float*, float** %cfm117, align 4
  %163 = load i32, i32* %srow, align 4
  %arrayidx118 = getelementptr inbounds float, float* %162, i32 %163
  store float %160, float* %arrayidx118, align 4
  %164 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip119 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %164, i32 0, i32 6
  %165 = load i32, i32* %rowskip119, align 4
  %166 = load i32, i32* %srow, align 4
  %add120 = add nsw i32 %166, %165
  store i32 %add120, i32* %srow, align 4
  %167 = load i32, i32* %count, align 4
  %inc121 = add nsw i32 %167, 1
  store i32 %inc121, i32* %count, align 4
  br label %if.end148

if.else122:                                       ; preds = %entry
  %168 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentLimit123 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %168, i32 0, i32 21
  %169 = load i32, i32* %m_currentLimit123, align 4
  %cmp124 = icmp eq i32 %169, 3
  br i1 %cmp124, label %if.then125, label %if.end147

if.then125:                                       ; preds = %if.else122
  %170 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %171 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %172 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %173 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %174 = load i32, i32* %srow, align 4
  %175 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %176 = load i32, i32* %rotational.addr, align 4
  %177 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %170, %class.btTransform* nonnull align 4 dereferenceable(64) %171, %class.btTransform* nonnull align 4 dereferenceable(64) %172, %"struct.btTypedConstraint::btConstraintInfo2"* %173, i32 %174, %class.btVector3* nonnull align 4 dereferenceable(16) %175, i32 %176, i32 %177)
  %178 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps126 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %178, i32 0, i32 0
  %179 = load float, float* %fps126, align 4
  %180 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopERP127 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %180, i32 0, i32 3
  %181 = load float, float* %m_stopERP127, align 4
  %mul128 = fmul float %179, %181
  %182 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentLimitError129 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %182, i32 0, i32 18
  %183 = load float, float* %m_currentLimitError129, align 4
  %mul130 = fmul float %mul128, %183
  %184 = load i32, i32* %rotational.addr, align 4
  %tobool131 = icmp ne i32 %184, 0
  %185 = zext i1 %tobool131 to i64
  %cond132 = select i1 %tobool131, i32 -1, i32 1
  %conv133 = sitofp i32 %cond132 to float
  %mul134 = fmul float %mul130, %conv133
  %186 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError135 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %186, i32 0, i32 7
  %187 = load float*, float** %m_constraintError135, align 4
  %188 = load i32, i32* %srow, align 4
  %arrayidx136 = getelementptr inbounds float, float* %187, i32 %188
  store float %mul134, float* %arrayidx136, align 4
  %189 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit137 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %189, i32 0, i32 9
  %190 = load float*, float** %m_lowerLimit137, align 4
  %191 = load i32, i32* %srow, align 4
  %arrayidx138 = getelementptr inbounds float, float* %190, i32 %191
  store float 0xC7EFFFFFE0000000, float* %arrayidx138, align 4
  %192 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit139 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %192, i32 0, i32 10
  %193 = load float*, float** %m_upperLimit139, align 4
  %194 = load i32, i32* %srow, align 4
  %arrayidx140 = getelementptr inbounds float, float* %193, i32 %194
  store float 0x47EFFFFFE0000000, float* %arrayidx140, align 4
  %195 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_stopCFM141 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %195, i32 0, i32 4
  %196 = load float, float* %m_stopCFM141, align 4
  %197 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm142 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %197, i32 0, i32 8
  %198 = load float*, float** %cfm142, align 4
  %199 = load i32, i32* %srow, align 4
  %arrayidx143 = getelementptr inbounds float, float* %198, i32 %199
  store float %196, float* %arrayidx143, align 4
  %200 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip144 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %200, i32 0, i32 6
  %201 = load i32, i32* %rowskip144, align 4
  %202 = load i32, i32* %srow, align 4
  %add145 = add nsw i32 %202, %201
  store i32 %add145, i32* %srow, align 4
  %203 = load i32, i32* %count, align 4
  %inc146 = add nsw i32 %203, 1
  store i32 %inc146, i32* %count, align 4
  br label %if.end147

if.end147:                                        ; preds = %if.then125, %if.else122
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.end107
  %204 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %204, i32 0, i32 7
  %205 = load i8, i8* %m_enableMotor, align 4
  %tobool149 = trunc i8 %205 to i1
  br i1 %tobool149, label %land.lhs.true, label %if.end177

land.lhs.true:                                    ; preds = %if.end148
  %206 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %206, i32 0, i32 10
  %207 = load i8, i8* %m_servoMotor, align 4
  %tobool150 = trunc i8 %207 to i1
  br i1 %tobool150, label %if.end177, label %if.then151

if.then151:                                       ; preds = %land.lhs.true
  %208 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %209 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %210 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %211 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %212 = load i32, i32* %srow, align 4
  %213 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %214 = load i32, i32* %rotational.addr, align 4
  %215 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %208, %class.btTransform* nonnull align 4 dereferenceable(64) %209, %class.btTransform* nonnull align 4 dereferenceable(64) %210, %"struct.btTypedConstraint::btConstraintInfo2"* %211, i32 %212, %class.btVector3* nonnull align 4 dereferenceable(16) %213, i32 %214, i32 %215)
  %216 = load i32, i32* %rotational.addr, align 4
  %tobool152 = icmp ne i32 %216, 0
  br i1 %tobool152, label %cond.true153, label %cond.false154

cond.true153:                                     ; preds = %if.then151
  %217 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %217, i32 0, i32 8
  %218 = load float, float* %m_targetVelocity, align 4
  br label %cond.end157

cond.false154:                                    ; preds = %if.then151
  %219 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_targetVelocity155 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %219, i32 0, i32 8
  %220 = load float, float* %m_targetVelocity155, align 4
  %fneg156 = fneg float %220
  br label %cond.end157

cond.end157:                                      ; preds = %cond.false154, %cond.true153
  %cond158 = phi float [ %218, %cond.true153 ], [ %fneg156, %cond.false154 ]
  store float %cond158, float* %tag_vel, align 4
  %221 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %222 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentPosition = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %222, i32 0, i32 20
  %223 = load float, float* %m_currentPosition, align 4
  %224 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %224, i32 0, i32 0
  %225 = load float, float* %m_loLimit, align 4
  %226 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %226, i32 0, i32 1
  %227 = load float, float* %m_hiLimit, align 4
  %228 = load float, float* %tag_vel, align 4
  %229 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps159 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %229, i32 0, i32 0
  %230 = load float, float* %fps159, align 4
  %231 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_motorERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %231, i32 0, i32 5
  %232 = load float, float* %m_motorERP, align 4
  %mul160 = fmul float %230, %232
  %call161 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %221, float %223, float %225, float %227, float %228, float %mul160)
  store float %call161, float* %mot_fact, align 4
  %233 = load float, float* %mot_fact, align 4
  %234 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_targetVelocity162 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %234, i32 0, i32 8
  %235 = load float, float* %m_targetVelocity162, align 4
  %mul163 = fmul float %233, %235
  %236 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError164 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %236, i32 0, i32 7
  %237 = load float*, float** %m_constraintError164, align 4
  %238 = load i32, i32* %srow, align 4
  %arrayidx165 = getelementptr inbounds float, float* %237, i32 %238
  store float %mul163, float* %arrayidx165, align 4
  %239 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %239, i32 0, i32 9
  %240 = load float, float* %m_maxMotorForce, align 4
  %fneg166 = fneg float %240
  %241 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit167 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %241, i32 0, i32 9
  %242 = load float*, float** %m_lowerLimit167, align 4
  %243 = load i32, i32* %srow, align 4
  %arrayidx168 = getelementptr inbounds float, float* %242, i32 %243
  store float %fneg166, float* %arrayidx168, align 4
  %244 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_maxMotorForce169 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %244, i32 0, i32 9
  %245 = load float, float* %m_maxMotorForce169, align 4
  %246 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit170 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %246, i32 0, i32 10
  %247 = load float*, float** %m_upperLimit170, align 4
  %248 = load i32, i32* %srow, align 4
  %arrayidx171 = getelementptr inbounds float, float* %247, i32 %248
  store float %245, float* %arrayidx171, align 4
  %249 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_motorCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %249, i32 0, i32 6
  %250 = load float, float* %m_motorCFM, align 4
  %251 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm172 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %251, i32 0, i32 8
  %252 = load float*, float** %cfm172, align 4
  %253 = load i32, i32* %srow, align 4
  %arrayidx173 = getelementptr inbounds float, float* %252, i32 %253
  store float %250, float* %arrayidx173, align 4
  %254 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip174 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %254, i32 0, i32 6
  %255 = load i32, i32* %rowskip174, align 4
  %256 = load i32, i32* %srow, align 4
  %add175 = add nsw i32 %256, %255
  store i32 %add175, i32* %srow, align 4
  %257 = load i32, i32* %count, align 4
  %inc176 = add nsw i32 %257, 1
  store i32 %inc176, i32* %count, align 4
  br label %if.end177

if.end177:                                        ; preds = %cond.end157, %land.lhs.true, %if.end148
  %258 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_enableMotor178 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %258, i32 0, i32 7
  %259 = load i8, i8* %m_enableMotor178, align 4
  %tobool179 = trunc i8 %259 to i1
  br i1 %tobool179, label %land.lhs.true180, label %if.end266

land.lhs.true180:                                 ; preds = %if.end177
  %260 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoMotor181 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %260, i32 0, i32 10
  %261 = load i8, i8* %m_servoMotor181, align 4
  %tobool182 = trunc i8 %261 to i1
  br i1 %tobool182, label %if.then183, label %if.end266

if.then183:                                       ; preds = %land.lhs.true180
  %262 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentPosition184 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %262, i32 0, i32 20
  %263 = load float, float* %m_currentPosition184, align 4
  %264 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %264, i32 0, i32 11
  %265 = load float, float* %m_servoTarget, align 4
  %sub185 = fsub float %263, %265
  store float %sub185, float* %error, align 4
  %266 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %267 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %268 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %269 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %270 = load i32, i32* %srow, align 4
  %271 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %272 = load i32, i32* %rotational.addr, align 4
  %273 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %266, %class.btTransform* nonnull align 4 dereferenceable(64) %267, %class.btTransform* nonnull align 4 dereferenceable(64) %268, %"struct.btTypedConstraint::btConstraintInfo2"* %269, i32 %270, %class.btVector3* nonnull align 4 dereferenceable(16) %271, i32 %272, i32 %273)
  %274 = load float, float* %error, align 4
  %cmp186 = fcmp olt float %274, 0.000000e+00
  br i1 %cmp186, label %cond.true187, label %cond.false190

cond.true187:                                     ; preds = %if.then183
  %275 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_targetVelocity188 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %275, i32 0, i32 8
  %276 = load float, float* %m_targetVelocity188, align 4
  %fneg189 = fneg float %276
  br label %cond.end192

cond.false190:                                    ; preds = %if.then183
  %277 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_targetVelocity191 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %277, i32 0, i32 8
  %278 = load float, float* %m_targetVelocity191, align 4
  br label %cond.end192

cond.end192:                                      ; preds = %cond.false190, %cond.true187
  %cond193 = phi float [ %fneg189, %cond.true187 ], [ %278, %cond.false190 ]
  store float %cond193, float* %targetvelocity, align 4
  %279 = load float, float* %targetvelocity, align 4
  %fneg195 = fneg float %279
  store float %fneg195, float* %tag_vel194, align 4
  %280 = load float, float* %error, align 4
  %cmp197 = fcmp une float %280, 0.000000e+00
  br i1 %cmp197, label %if.then198, label %if.else244

if.then198:                                       ; preds = %cond.end192
  %281 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_loLimit199 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %281, i32 0, i32 0
  %282 = load float, float* %m_loLimit199, align 4
  %283 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_hiLimit200 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %283, i32 0, i32 1
  %284 = load float, float* %m_hiLimit200, align 4
  %cmp201 = fcmp ogt float %282, %284
  br i1 %cmp201, label %if.then202, label %if.else215

if.then202:                                       ; preds = %if.then198
  %285 = load float, float* %error, align 4
  %cmp203 = fcmp ogt float %285, 0.000000e+00
  br i1 %cmp203, label %cond.true204, label %cond.false206

cond.true204:                                     ; preds = %if.then202
  %286 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget205 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %286, i32 0, i32 11
  %287 = load float, float* %m_servoTarget205, align 4
  br label %cond.end207

cond.false206:                                    ; preds = %if.then202
  br label %cond.end207

cond.end207:                                      ; preds = %cond.false206, %cond.true204
  %cond208 = phi float [ %287, %cond.true204 ], [ 0xC7EFFFFFE0000000, %cond.false206 ]
  store float %cond208, float* %lowLimit, align 4
  %288 = load float, float* %error, align 4
  %cmp209 = fcmp olt float %288, 0.000000e+00
  br i1 %cmp209, label %cond.true210, label %cond.false212

cond.true210:                                     ; preds = %cond.end207
  %289 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget211 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %289, i32 0, i32 11
  %290 = load float, float* %m_servoTarget211, align 4
  br label %cond.end213

cond.false212:                                    ; preds = %cond.end207
  br label %cond.end213

cond.end213:                                      ; preds = %cond.false212, %cond.true210
  %cond214 = phi float [ %290, %cond.true210 ], [ 0x47EFFFFFE0000000, %cond.false212 ]
  store float %cond214, float* %hiLimit, align 4
  br label %if.end238

if.else215:                                       ; preds = %if.then198
  %291 = load float, float* %error, align 4
  %cmp216 = fcmp ogt float %291, 0.000000e+00
  br i1 %cmp216, label %land.lhs.true217, label %cond.false223

land.lhs.true217:                                 ; preds = %if.else215
  %292 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget218 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %292, i32 0, i32 11
  %293 = load float, float* %m_servoTarget218, align 4
  %294 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_loLimit219 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %294, i32 0, i32 0
  %295 = load float, float* %m_loLimit219, align 4
  %cmp220 = fcmp ogt float %293, %295
  br i1 %cmp220, label %cond.true221, label %cond.false223

cond.true221:                                     ; preds = %land.lhs.true217
  %296 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget222 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %296, i32 0, i32 11
  %297 = load float, float* %m_servoTarget222, align 4
  br label %cond.end225

cond.false223:                                    ; preds = %land.lhs.true217, %if.else215
  %298 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_loLimit224 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %298, i32 0, i32 0
  %299 = load float, float* %m_loLimit224, align 4
  br label %cond.end225

cond.end225:                                      ; preds = %cond.false223, %cond.true221
  %cond226 = phi float [ %297, %cond.true221 ], [ %299, %cond.false223 ]
  store float %cond226, float* %lowLimit, align 4
  %300 = load float, float* %error, align 4
  %cmp227 = fcmp olt float %300, 0.000000e+00
  br i1 %cmp227, label %land.lhs.true228, label %cond.false234

land.lhs.true228:                                 ; preds = %cond.end225
  %301 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget229 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %301, i32 0, i32 11
  %302 = load float, float* %m_servoTarget229, align 4
  %303 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_hiLimit230 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %303, i32 0, i32 1
  %304 = load float, float* %m_hiLimit230, align 4
  %cmp231 = fcmp olt float %302, %304
  br i1 %cmp231, label %cond.true232, label %cond.false234

cond.true232:                                     ; preds = %land.lhs.true228
  %305 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_servoTarget233 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %305, i32 0, i32 11
  %306 = load float, float* %m_servoTarget233, align 4
  br label %cond.end236

cond.false234:                                    ; preds = %land.lhs.true228, %cond.end225
  %307 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_hiLimit235 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %307, i32 0, i32 1
  %308 = load float, float* %m_hiLimit235, align 4
  br label %cond.end236

cond.end236:                                      ; preds = %cond.false234, %cond.true232
  %cond237 = phi float [ %306, %cond.true232 ], [ %308, %cond.false234 ]
  store float %cond237, float* %hiLimit, align 4
  br label %if.end238

if.end238:                                        ; preds = %cond.end236, %cond.end213
  %309 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %310 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentPosition239 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %310, i32 0, i32 20
  %311 = load float, float* %m_currentPosition239, align 4
  %312 = load float, float* %lowLimit, align 4
  %313 = load float, float* %hiLimit, align 4
  %314 = load float, float* %tag_vel194, align 4
  %315 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps240 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %315, i32 0, i32 0
  %316 = load float, float* %fps240, align 4
  %317 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_motorERP241 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %317, i32 0, i32 5
  %318 = load float, float* %m_motorERP241, align 4
  %mul242 = fmul float %316, %318
  %call243 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %309, float %311, float %312, float %313, float %314, float %mul242)
  store float %call243, float* %mot_fact196, align 4
  br label %if.end245

if.else244:                                       ; preds = %cond.end192
  store float 0.000000e+00, float* %mot_fact196, align 4
  br label %if.end245

if.end245:                                        ; preds = %if.else244, %if.end238
  %319 = load float, float* %mot_fact196, align 4
  %320 = load float, float* %targetvelocity, align 4
  %mul246 = fmul float %319, %320
  %321 = load i32, i32* %rotational.addr, align 4
  %tobool247 = icmp ne i32 %321, 0
  %322 = zext i1 %tobool247 to i64
  %cond248 = select i1 %tobool247, i32 -1, i32 1
  %conv249 = sitofp i32 %cond248 to float
  %mul250 = fmul float %mul246, %conv249
  %323 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError251 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %323, i32 0, i32 7
  %324 = load float*, float** %m_constraintError251, align 4
  %325 = load i32, i32* %srow, align 4
  %arrayidx252 = getelementptr inbounds float, float* %324, i32 %325
  store float %mul250, float* %arrayidx252, align 4
  %326 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_maxMotorForce253 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %326, i32 0, i32 9
  %327 = load float, float* %m_maxMotorForce253, align 4
  %fneg254 = fneg float %327
  %328 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit255 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %328, i32 0, i32 9
  %329 = load float*, float** %m_lowerLimit255, align 4
  %330 = load i32, i32* %srow, align 4
  %arrayidx256 = getelementptr inbounds float, float* %329, i32 %330
  store float %fneg254, float* %arrayidx256, align 4
  %331 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_maxMotorForce257 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %331, i32 0, i32 9
  %332 = load float, float* %m_maxMotorForce257, align 4
  %333 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit258 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %333, i32 0, i32 10
  %334 = load float*, float** %m_upperLimit258, align 4
  %335 = load i32, i32* %srow, align 4
  %arrayidx259 = getelementptr inbounds float, float* %334, i32 %335
  store float %332, float* %arrayidx259, align 4
  %336 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_motorCFM260 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %336, i32 0, i32 6
  %337 = load float, float* %m_motorCFM260, align 4
  %338 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm261 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %338, i32 0, i32 8
  %339 = load float*, float** %cfm261, align 4
  %340 = load i32, i32* %srow, align 4
  %arrayidx262 = getelementptr inbounds float, float* %339, i32 %340
  store float %337, float* %arrayidx262, align 4
  %341 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip263 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %341, i32 0, i32 6
  %342 = load i32, i32* %rowskip263, align 4
  %343 = load i32, i32* %srow, align 4
  %add264 = add nsw i32 %343, %342
  store i32 %add264, i32* %srow, align 4
  %344 = load i32, i32* %count, align 4
  %inc265 = add nsw i32 %344, 1
  store i32 %inc265, i32* %count, align 4
  br label %if.end266

if.end266:                                        ; preds = %if.end245, %land.lhs.true180, %if.end177
  %345 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_enableSpring = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %345, i32 0, i32 12
  %346 = load i8, i8* %m_enableSpring, align 4
  %tobool267 = trunc i8 %346 to i1
  br i1 %tobool267, label %if.then268, label %if.end383

if.then268:                                       ; preds = %if.end266
  %347 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_currentPosition270 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %347, i32 0, i32 20
  %348 = load float, float* %m_currentPosition270, align 4
  %349 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %349, i32 0, i32 17
  %350 = load float, float* %m_equilibriumPoint, align 4
  %sub271 = fsub float %348, %350
  store float %sub271, float* %error269, align 4
  %351 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %352 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %353 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %354 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %355 = load i32, i32* %srow, align 4
  %356 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %357 = load i32, i32* %rotational.addr, align 4
  %358 = load i32, i32* %rotAllowed.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this1, %class.btRotationalLimitMotor2* %351, %class.btTransform* nonnull align 4 dereferenceable(64) %352, %class.btTransform* nonnull align 4 dereferenceable(64) %353, %"struct.btTypedConstraint::btConstraintInfo2"* %354, i32 %355, %class.btVector3* nonnull align 4 dereferenceable(16) %356, i32 %357, i32 %358)
  %359 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps272 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %359, i32 0, i32 0
  %360 = load float, float* %fps272, align 4
  %div = fdiv float 1.000000e+00, %360
  store float %div, float* %dt, align 4
  %361 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_springDamping = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %361, i32 0, i32 15
  %362 = load float, float* %m_springDamping, align 4
  store float %362, float* %kd, align 4
  %363 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_springStiffness = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %363, i32 0, i32 13
  %364 = load float, float* %m_springStiffness, align 4
  store float %364, float* %ks, align 4
  %365 = load i32, i32* %rotational.addr, align 4
  %tobool274 = icmp ne i32 %365, 0
  br i1 %tobool274, label %cond.true275, label %cond.false279

cond.true275:                                     ; preds = %if.then268
  %366 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %367 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call276 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %366, %class.btVector3* nonnull align 4 dereferenceable(16) %367)
  %368 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %369 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call277 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %368, %class.btVector3* nonnull align 4 dereferenceable(16) %369)
  %sub278 = fsub float %call276, %call277
  br label %cond.end283

cond.false279:                                    ; preds = %if.then268
  %370 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4
  %371 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call280 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %370, %class.btVector3* nonnull align 4 dereferenceable(16) %371)
  %372 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4
  %373 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call281 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %372, %class.btVector3* nonnull align 4 dereferenceable(16) %373)
  %sub282 = fsub float %call280, %call281
  br label %cond.end283

cond.end283:                                      ; preds = %cond.false279, %cond.true275
  %cond284 = phi float [ %sub278, %cond.true275 ], [ %sub282, %cond.false279 ]
  store float %cond284, float* %vel273, align 4
  store float 0.000000e+00, float* %cfm285, align 4
  %374 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %374, i32 0, i32 8
  %375 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call286 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %375)
  %div287 = fdiv float 1.000000e+00, %call286
  store float %div287, float* %mA, align 4
  %376 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %376, i32 0, i32 9
  %377 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call288 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %377)
  %div289 = fdiv float 1.000000e+00, %call288
  store float %div289, float* %mB, align 4
  %378 = load float, float* %mA, align 4
  %379 = load float, float* %mB, align 4
  %cmp290 = fcmp ogt float %378, %379
  br i1 %cmp290, label %cond.true291, label %cond.false292

cond.true291:                                     ; preds = %cond.end283
  %380 = load float, float* %mB, align 4
  br label %cond.end293

cond.false292:                                    ; preds = %cond.end283
  %381 = load float, float* %mA, align 4
  br label %cond.end293

cond.end293:                                      ; preds = %cond.false292, %cond.true291
  %cond294 = phi float [ %380, %cond.true291 ], [ %381, %cond.false292 ]
  store float %cond294, float* %m, align 4
  %382 = load float, float* %ks, align 4
  %383 = load float, float* %m, align 4
  %div295 = fdiv float %382, %383
  %call296 = call float @_Z4sqrtf(float %div295) #8
  store float %call296, float* %angularfreq, align 4
  %384 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_springStiffnessLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %384, i32 0, i32 14
  %385 = load i8, i8* %m_springStiffnessLimited, align 4
  %tobool297 = trunc i8 %385 to i1
  br i1 %tobool297, label %land.lhs.true298, label %if.end307

land.lhs.true298:                                 ; preds = %cond.end293
  %386 = load float, float* %angularfreq, align 4
  %387 = load float, float* %dt, align 4
  %mul299 = fmul float %386, %387
  %conv300 = fpext float %mul299 to double
  %cmp301 = fcmp olt double 2.500000e-01, %conv300
  br i1 %cmp301, label %if.then302, label %if.end307

if.then302:                                       ; preds = %land.lhs.true298
  %388 = load float, float* %dt, align 4
  %div303 = fdiv float 1.000000e+00, %388
  %389 = load float, float* %dt, align 4
  %div304 = fdiv float %div303, %389
  %div305 = fdiv float %div304, 1.600000e+01
  %390 = load float, float* %m, align 4
  %mul306 = fmul float %div305, %390
  store float %mul306, float* %ks, align 4
  br label %if.end307

if.end307:                                        ; preds = %if.then302, %land.lhs.true298, %cond.end293
  %391 = load %class.btRotationalLimitMotor2*, %class.btRotationalLimitMotor2** %limot.addr, align 4
  %m_springDampingLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %391, i32 0, i32 16
  %392 = load i8, i8* %m_springDampingLimited, align 4
  %tobool308 = trunc i8 %392 to i1
  br i1 %tobool308, label %land.lhs.true309, label %if.end314

land.lhs.true309:                                 ; preds = %if.end307
  %393 = load float, float* %kd, align 4
  %394 = load float, float* %dt, align 4
  %mul310 = fmul float %393, %394
  %395 = load float, float* %m, align 4
  %cmp311 = fcmp ogt float %mul310, %395
  br i1 %cmp311, label %if.then312, label %if.end314

if.then312:                                       ; preds = %land.lhs.true309
  %396 = load float, float* %m, align 4
  %397 = load float, float* %dt, align 4
  %div313 = fdiv float %396, %397
  store float %div313, float* %kd, align 4
  br label %if.end314

if.end314:                                        ; preds = %if.then312, %land.lhs.true309, %if.end307
  %398 = load float, float* %ks, align 4
  %399 = load float, float* %error269, align 4
  %mul315 = fmul float %398, %399
  %400 = load float, float* %dt, align 4
  %mul316 = fmul float %mul315, %400
  store float %mul316, float* %fs, align 4
  %401 = load float, float* %kd, align 4
  %fneg317 = fneg float %401
  %402 = load float, float* %vel273, align 4
  %mul318 = fmul float %fneg317, %402
  %403 = load i32, i32* %rotational.addr, align 4
  %tobool319 = icmp ne i32 %403, 0
  %404 = zext i1 %tobool319 to i64
  %cond320 = select i1 %tobool319, i32 -1, i32 1
  %conv321 = sitofp i32 %cond320 to float
  %mul322 = fmul float %mul318, %conv321
  %405 = load float, float* %dt, align 4
  %mul323 = fmul float %mul322, %405
  store float %mul323, float* %fd, align 4
  %406 = load float, float* %fs, align 4
  %407 = load float, float* %fd, align 4
  %add324 = fadd float %406, %407
  store float %add324, float* %f, align 4
  %408 = load float, float* %vel273, align 4
  %409 = load float, float* %f, align 4
  %410 = load i32, i32* %rotational.addr, align 4
  %tobool325 = icmp ne i32 %410, 0
  %411 = zext i1 %tobool325 to i64
  %cond326 = select i1 %tobool325, i32 -1, i32 1
  %conv327 = sitofp i32 %cond326 to float
  %mul328 = fmul float %409, %conv327
  %add329 = fadd float %408, %mul328
  %412 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError330 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %412, i32 0, i32 7
  %413 = load float*, float** %m_constraintError330, align 4
  %414 = load i32, i32* %srow, align 4
  %arrayidx331 = getelementptr inbounds float, float* %413, i32 %414
  store float %add329, float* %arrayidx331, align 4
  %415 = load float, float* %f, align 4
  %416 = load float, float* %fd, align 4
  %cmp332 = fcmp olt float %415, %416
  br i1 %cmp332, label %cond.true333, label %cond.false334

cond.true333:                                     ; preds = %if.end314
  %417 = load float, float* %f, align 4
  br label %cond.end335

cond.false334:                                    ; preds = %if.end314
  %418 = load float, float* %fd, align 4
  br label %cond.end335

cond.end335:                                      ; preds = %cond.false334, %cond.true333
  %cond336 = phi float [ %417, %cond.true333 ], [ %418, %cond.false334 ]
  store float %cond336, float* %minf, align 4
  %419 = load float, float* %f, align 4
  %420 = load float, float* %fd, align 4
  %cmp337 = fcmp olt float %419, %420
  br i1 %cmp337, label %cond.true338, label %cond.false339

cond.true338:                                     ; preds = %cond.end335
  %421 = load float, float* %fd, align 4
  br label %cond.end340

cond.false339:                                    ; preds = %cond.end335
  %422 = load float, float* %f, align 4
  br label %cond.end340

cond.end340:                                      ; preds = %cond.false339, %cond.true338
  %cond341 = phi float [ %421, %cond.true338 ], [ %422, %cond.false339 ]
  store float %cond341, float* %maxf, align 4
  %423 = load i32, i32* %rotational.addr, align 4
  %tobool342 = icmp ne i32 %423, 0
  br i1 %tobool342, label %if.else358, label %if.then343

if.then343:                                       ; preds = %cond.end340
  %424 = load float, float* %minf, align 4
  %cmp344 = fcmp ogt float %424, 0.000000e+00
  br i1 %cmp344, label %cond.true345, label %cond.false346

cond.true345:                                     ; preds = %if.then343
  br label %cond.end347

cond.false346:                                    ; preds = %if.then343
  %425 = load float, float* %minf, align 4
  br label %cond.end347

cond.end347:                                      ; preds = %cond.false346, %cond.true345
  %cond348 = phi float [ 0.000000e+00, %cond.true345 ], [ %425, %cond.false346 ]
  %426 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit349 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %426, i32 0, i32 9
  %427 = load float*, float** %m_lowerLimit349, align 4
  %428 = load i32, i32* %srow, align 4
  %arrayidx350 = getelementptr inbounds float, float* %427, i32 %428
  store float %cond348, float* %arrayidx350, align 4
  %429 = load float, float* %maxf, align 4
  %cmp351 = fcmp olt float %429, 0.000000e+00
  br i1 %cmp351, label %cond.true352, label %cond.false353

cond.true352:                                     ; preds = %cond.end347
  br label %cond.end354

cond.false353:                                    ; preds = %cond.end347
  %430 = load float, float* %maxf, align 4
  br label %cond.end354

cond.end354:                                      ; preds = %cond.false353, %cond.true352
  %cond355 = phi float [ 0.000000e+00, %cond.true352 ], [ %430, %cond.false353 ]
  %431 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit356 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %431, i32 0, i32 10
  %432 = load float*, float** %m_upperLimit356, align 4
  %433 = load i32, i32* %srow, align 4
  %arrayidx357 = getelementptr inbounds float, float* %432, i32 %433
  store float %cond355, float* %arrayidx357, align 4
  br label %if.end377

if.else358:                                       ; preds = %cond.end340
  %434 = load float, float* %maxf, align 4
  %fneg359 = fneg float %434
  %cmp360 = fcmp ogt float %fneg359, 0.000000e+00
  br i1 %cmp360, label %cond.true361, label %cond.false362

cond.true361:                                     ; preds = %if.else358
  br label %cond.end364

cond.false362:                                    ; preds = %if.else358
  %435 = load float, float* %maxf, align 4
  %fneg363 = fneg float %435
  br label %cond.end364

cond.end364:                                      ; preds = %cond.false362, %cond.true361
  %cond365 = phi float [ 0.000000e+00, %cond.true361 ], [ %fneg363, %cond.false362 ]
  %436 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit366 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %436, i32 0, i32 9
  %437 = load float*, float** %m_lowerLimit366, align 4
  %438 = load i32, i32* %srow, align 4
  %arrayidx367 = getelementptr inbounds float, float* %437, i32 %438
  store float %cond365, float* %arrayidx367, align 4
  %439 = load float, float* %minf, align 4
  %fneg368 = fneg float %439
  %cmp369 = fcmp olt float %fneg368, 0.000000e+00
  br i1 %cmp369, label %cond.true370, label %cond.false371

cond.true370:                                     ; preds = %cond.end364
  br label %cond.end373

cond.false371:                                    ; preds = %cond.end364
  %440 = load float, float* %minf, align 4
  %fneg372 = fneg float %440
  br label %cond.end373

cond.end373:                                      ; preds = %cond.false371, %cond.true370
  %cond374 = phi float [ 0.000000e+00, %cond.true370 ], [ %fneg372, %cond.false371 ]
  %441 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit375 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %441, i32 0, i32 10
  %442 = load float*, float** %m_upperLimit375, align 4
  %443 = load i32, i32* %srow, align 4
  %arrayidx376 = getelementptr inbounds float, float* %442, i32 %443
  store float %cond374, float* %arrayidx376, align 4
  br label %if.end377

if.end377:                                        ; preds = %cond.end373, %cond.end354
  %444 = load float, float* %cfm285, align 4
  %445 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm378 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %445, i32 0, i32 8
  %446 = load float*, float** %cfm378, align 4
  %447 = load i32, i32* %srow, align 4
  %arrayidx379 = getelementptr inbounds float, float* %446, i32 %447
  store float %444, float* %arrayidx379, align 4
  %448 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip380 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %448, i32 0, i32 6
  %449 = load i32, i32* %rowskip380, align 4
  %450 = load i32, i32* %srow, align 4
  %add381 = add nsw i32 %450, %449
  store i32 %add381, i32* %srow, align 4
  %451 = load i32, i32* %count, align 4
  %inc382 = add nsw i32 %451, 1
  store i32 %inc382, i32* %count, align 4
  br label %if.end383

if.end383:                                        ; preds = %if.end377, %if.end266
  %452 = load i32, i32* %count, align 4
  ret i32 %452
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK30btGeneric6DofSpring2Constraint7getAxisEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btGeneric6DofSpring2Constraint* %this, i32 %axis_index) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %axis_index.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %axis_index, i32* %axis_index.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %m_calculatedAxis = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 11
  %0 = load i32, i32* %axis_index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_calculatedAxis, i32 0, i32 %0
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint9setFramesERK11btTransformS2_(%class.btGeneric6DofSpring2Constraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to void (%class.btGeneric6DofSpring2Constraint*)***
  %vtable = load void (%class.btGeneric6DofSpring2Constraint*)**, void (%class.btGeneric6DofSpring2Constraint*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btGeneric6DofSpring2Constraint*)*, void (%class.btGeneric6DofSpring2Constraint*)** %vtable, i64 2
  %3 = load void (%class.btGeneric6DofSpring2Constraint*)*, void (%class.btGeneric6DofSpring2Constraint*)** %vfn, align 4
  call void %3(%class.btGeneric6DofSpring2Constraint* %this1)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN26btTranslationalLimitMotor214testLimitValueEif(%class.btTranslationalLimitMotor2* %this, i32 %limitIndex, float %test_value) #2 {
entry:
  %this.addr = alloca %class.btTranslationalLimitMotor2*, align 4
  %limitIndex.addr = alloca i32, align 4
  %test_value.addr = alloca float, align 4
  %loLimit = alloca float, align 4
  %hiLimit = alloca float, align 4
  store %class.btTranslationalLimitMotor2* %this, %class.btTranslationalLimitMotor2** %this.addr, align 4
  store i32 %limitIndex, i32* %limitIndex.addr, align 4
  store float %test_value, float* %test_value.addr, align 4
  %this1 = load %class.btTranslationalLimitMotor2*, %class.btTranslationalLimitMotor2** %this.addr, align 4
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_lowerLimit)
  %0 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %0
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %loLimit, align 4
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 1
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_upperLimit)
  %2 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %2
  %3 = load float, float* %arrayidx3, align 4
  store float %3, float* %hiLimit, align 4
  %4 = load float, float* %loLimit, align 4
  %5 = load float, float* %hiLimit, align 4
  %cmp = fcmp ogt float %4, %5
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_currentLimitError = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 18
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError)
  %6 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 %6
  store float 0.000000e+00, float* %arrayidx5, align 4
  %m_currentLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 21
  %7 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit, i32 0, i32 %7
  store i32 0, i32* %arrayidx6, align 4
  br label %if.end24

if.else:                                          ; preds = %entry
  %8 = load float, float* %loLimit, align 4
  %9 = load float, float* %hiLimit, align 4
  %cmp7 = fcmp oeq float %8, %9
  br i1 %cmp7, label %if.then8, label %if.else14

if.then8:                                         ; preds = %if.else
  %10 = load float, float* %test_value.addr, align 4
  %11 = load float, float* %loLimit, align 4
  %sub = fsub float %10, %11
  %m_currentLimitError9 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 18
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError9)
  %12 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 %12
  store float %sub, float* %arrayidx11, align 4
  %m_currentLimit12 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 21
  %13 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit12, i32 0, i32 %13
  store i32 3, i32* %arrayidx13, align 4
  br label %if.end

if.else14:                                        ; preds = %if.else
  %14 = load float, float* %test_value.addr, align 4
  %15 = load float, float* %loLimit, align 4
  %sub15 = fsub float %14, %15
  %m_currentLimitError16 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 18
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitError16)
  %16 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 %16
  store float %sub15, float* %arrayidx18, align 4
  %17 = load float, float* %test_value.addr, align 4
  %18 = load float, float* %hiLimit, align 4
  %sub19 = fsub float %17, %18
  %m_currentLimitErrorHi = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 19
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_currentLimitErrorHi)
  %19 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %19
  store float %sub19, float* %arrayidx21, align 4
  %m_currentLimit22 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %this1, i32 0, i32 21
  %20 = load i32, i32* %limitIndex.addr, align 4
  %arrayidx23 = getelementptr inbounds [3 x i32], [3 x i32]* %m_currentLimit22, i32 0, i32 %20
  store i32 4, i32* %arrayidx23, align 4
  br label %if.end

if.end:                                           ; preds = %if.else14, %if.then8
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint15calculateJacobiEP23btRotationalLimitMotor2RK11btTransformS4_PN17btTypedConstraint17btConstraintInfo2EiR9btVector3ii(%class.btGeneric6DofSpring2Constraint* %this, %class.btRotationalLimitMotor2* %limot, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %"struct.btTypedConstraint::btConstraintInfo2"* %info, i32 %srow, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, i32 %rotational, i32 %rotAllowed) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %limot.addr = alloca %class.btRotationalLimitMotor2*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %srow.addr = alloca i32, align 4
  %ax1.addr = alloca %class.btVector3*, align 4
  %rotational.addr = alloca i32, align 4
  %rotAllowed.addr = alloca i32, align 4
  %J1 = alloca float*, align 4
  %J2 = alloca float*, align 4
  %tmpA = alloca %class.btVector3, align 4
  %tmpB = alloca %class.btVector3, align 4
  %relA = alloca %class.btVector3, align 4
  %relB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btRotationalLimitMotor2* %limot, %class.btRotationalLimitMotor2** %limot.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store i32 %srow, i32* %srow.addr, align 4
  store %class.btVector3* %ax1, %class.btVector3** %ax1.addr, align 4
  store i32 %rotational, i32* %rotational.addr, align 4
  store i32 %rotAllowed, i32* %rotAllowed.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %rotational.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %1, i32 0, i32 3
  %2 = load float*, float** %m_J1angularAxis, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %3, i32 0, i32 2
  %4 = load float*, float** %m_J1linearAxis, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %2, %cond.true ], [ %4, %cond.false ]
  store float* %cond, float** %J1, align 4
  %5 = load i32, i32* %rotational.addr, align 4
  %tobool2 = icmp ne i32 %5, 0
  br i1 %tobool2, label %cond.true3, label %cond.false4

cond.true3:                                       ; preds = %cond.end
  %6 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %6, i32 0, i32 5
  %7 = load float*, float** %m_J2angularAxis, align 4
  br label %cond.end5

cond.false4:                                      ; preds = %cond.end
  %8 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %8, i32 0, i32 4
  %9 = load float*, float** %m_J2linearAxis, align 4
  br label %cond.end5

cond.end5:                                        ; preds = %cond.false4, %cond.true3
  %cond6 = phi float* [ %7, %cond.true3 ], [ %9, %cond.false4 ]
  store float* %cond6, float** %J2, align 4
  %10 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %11 = load float, float* %arrayidx, align 4
  %12 = load float*, float** %J1, align 4
  %13 = load i32, i32* %srow.addr, align 4
  %add = add nsw i32 %13, 0
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 %add
  store float %11, float* %arrayidx7, align 4
  %14 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %14)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %15 = load float, float* %arrayidx9, align 4
  %16 = load float*, float** %J1, align 4
  %17 = load i32, i32* %srow.addr, align 4
  %add10 = add nsw i32 %17, 1
  %arrayidx11 = getelementptr inbounds float, float* %16, i32 %add10
  store float %15, float* %arrayidx11, align 4
  %18 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %18)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 2
  %19 = load float, float* %arrayidx13, align 4
  %20 = load float*, float** %J1, align 4
  %21 = load i32, i32* %srow.addr, align 4
  %add14 = add nsw i32 %21, 2
  %arrayidx15 = getelementptr inbounds float, float* %20, i32 %add14
  store float %19, float* %arrayidx15, align 4
  %22 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  %23 = load float, float* %arrayidx17, align 4
  %fneg = fneg float %23
  %24 = load float*, float** %J2, align 4
  %25 = load i32, i32* %srow.addr, align 4
  %add18 = add nsw i32 %25, 0
  %arrayidx19 = getelementptr inbounds float, float* %24, i32 %add18
  store float %fneg, float* %arrayidx19, align 4
  %26 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %26)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %27 = load float, float* %arrayidx21, align 4
  %fneg22 = fneg float %27
  %28 = load float*, float** %J2, align 4
  %29 = load i32, i32* %srow.addr, align 4
  %add23 = add nsw i32 %29, 1
  %arrayidx24 = getelementptr inbounds float, float* %28, i32 %add23
  store float %fneg22, float* %arrayidx24, align 4
  %30 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %31 = load float, float* %arrayidx26, align 4
  %fneg27 = fneg float %31
  %32 = load float*, float** %J2, align 4
  %33 = load i32, i32* %srow.addr, align 4
  %add28 = add nsw i32 %33, 2
  %arrayidx29 = getelementptr inbounds float, float* %32, i32 %add28
  store float %fneg27, float* %arrayidx29, align 4
  %34 = load i32, i32* %rotational.addr, align 4
  %tobool30 = icmp ne i32 %34, 0
  br i1 %tobool30, label %if.end64, label %if.then

if.then:                                          ; preds = %cond.end5
  %call31 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpA)
  %call32 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpB)
  %call33 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relA)
  %call34 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relB)
  %m_calculatedTransformB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 9
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB)
  %35 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %35)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call35, %class.btVector3* nonnull align 4 dereferenceable(16) %call36)
  %36 = bitcast %class.btVector3* %relB to i8*
  %37 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  %m_calculatedTransformA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 8
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA)
  %38 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %38)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %call38, %class.btVector3* nonnull align 4 dereferenceable(16) %call39)
  %39 = bitcast %class.btVector3* %relA to i8*
  %40 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false)
  %41 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %41)
  %42 = bitcast %class.btVector3* %tmpA to i8*
  %43 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false)
  %44 = load %class.btVector3*, %class.btVector3** %ax1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %44)
  %45 = bitcast %class.btVector3* %tmpB to i8*
  %46 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false)
  %m_hasStaticBody = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 15
  %47 = load i8, i8* %m_hasStaticBody, align 4
  %tobool42 = trunc i8 %47 to i1
  br i1 %tobool42, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %48 = load i32, i32* %rotAllowed.addr, align 4
  %tobool43 = icmp ne i32 %48, 0
  br i1 %tobool43, label %if.end, label %if.then44

if.then44:                                        ; preds = %land.lhs.true
  %m_factA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 13
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %m_factA)
  %m_factB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 14
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %m_factB)
  br label %if.end

if.end:                                           ; preds = %if.then44, %land.lhs.true, %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %49 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %49, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %50 = load i32, i32* %i, align 4
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %50
  %51 = load float, float* %arrayidx48, align 4
  %52 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis49 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %52, i32 0, i32 3
  %53 = load float*, float** %m_J1angularAxis49, align 4
  %54 = load i32, i32* %srow.addr, align 4
  %55 = load i32, i32* %i, align 4
  %add50 = add nsw i32 %54, %55
  %arrayidx51 = getelementptr inbounds float, float* %53, i32 %add50
  store float %51, float* %arrayidx51, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %56 = load i32, i32* %i, align 4
  %inc = add nsw i32 %56, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc61, %for.end
  %57 = load i32, i32* %i, align 4
  %cmp53 = icmp slt i32 %57, 3
  br i1 %cmp53, label %for.body54, label %for.end63

for.body54:                                       ; preds = %for.cond52
  %call55 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %58 = load i32, i32* %i, align 4
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 %58
  %59 = load float, float* %arrayidx56, align 4
  %fneg57 = fneg float %59
  %60 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis58 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %60, i32 0, i32 5
  %61 = load float*, float** %m_J2angularAxis58, align 4
  %62 = load i32, i32* %srow.addr, align 4
  %63 = load i32, i32* %i, align 4
  %add59 = add nsw i32 %62, %63
  %arrayidx60 = getelementptr inbounds float, float* %61, i32 %add59
  store float %fneg57, float* %arrayidx60, align 4
  br label %for.inc61

for.inc61:                                        ; preds = %for.body54
  %64 = load i32, i32* %i, align 4
  %inc62 = add nsw i32 %64, 1
  store i32 %inc62, i32* %i, align 4
  br label %for.cond52

for.end63:                                        ; preds = %for.cond52
  br label %if.end64

if.end64:                                         ; preds = %for.end63, %cond.end5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

declare float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint*, float, float, float, float, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4sqrtf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint8setParamEifi(%class.btGeneric6DofSpring2Constraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store float %value, float* %value.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %axis.addr, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %1, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %num.addr, align 4
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 1, label %sw.bb11
    i32 3, label %sw.bb19
  ]

sw.bb:                                            ; preds = %if.then
  %3 = load float, float* %value.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 3
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopERP)
  %4 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %4
  store float %3, float* %arrayidx, align 4
  %5 = load i32, i32* %axis.addr, align 4
  %mul = mul nsw i32 %5, 4
  %shl = shl i32 2, %mul
  %m_flags = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %6 = load i32, i32* %m_flags, align 4
  %or = or i32 %6, %shl
  store i32 %or, i32* %m_flags, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %7 = load float, float* %value.addr, align 4
  %m_linearLimits4 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits4, i32 0, i32 4
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_stopCFM)
  %8 = load i32, i32* %axis.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %8
  store float %7, float* %arrayidx6, align 4
  %9 = load i32, i32* %axis.addr, align 4
  %mul7 = mul nsw i32 %9, 4
  %shl8 = shl i32 1, %mul7
  %m_flags9 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %10 = load i32, i32* %m_flags9, align 4
  %or10 = or i32 %10, %shl8
  store i32 %or10, i32* %m_flags9, align 4
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.then
  %11 = load float, float* %value.addr, align 4
  %m_linearLimits12 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits12, i32 0, i32 5
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_motorERP)
  %12 = load i32, i32* %axis.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %12
  store float %11, float* %arrayidx14, align 4
  %13 = load i32, i32* %axis.addr, align 4
  %mul15 = mul nsw i32 %13, 4
  %shl16 = shl i32 8, %mul15
  %m_flags17 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %14 = load i32, i32* %m_flags17, align 4
  %or18 = or i32 %14, %shl16
  store i32 %or18, i32* %m_flags17, align 4
  br label %sw.epilog

sw.bb19:                                          ; preds = %if.then
  %15 = load float, float* %value.addr, align 4
  %m_linearLimits20 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits20, i32 0, i32 6
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_motorCFM)
  %16 = load i32, i32* %axis.addr, align 4
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 %16
  store float %15, float* %arrayidx22, align 4
  %17 = load i32, i32* %axis.addr, align 4
  %mul23 = mul nsw i32 %17, 4
  %shl24 = shl i32 4, %mul23
  %m_flags25 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %18 = load i32, i32* %m_flags25, align 4
  %or26 = or i32 %18, %shl24
  store i32 %or26, i32* %m_flags25, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb19, %sw.bb11, %sw.bb3, %sw.bb
  br label %if.end68

if.else:                                          ; preds = %land.lhs.true, %entry
  %19 = load i32, i32* %axis.addr, align 4
  %cmp27 = icmp sge i32 %19, 3
  br i1 %cmp27, label %land.lhs.true28, label %if.else67

land.lhs.true28:                                  ; preds = %if.else
  %20 = load i32, i32* %axis.addr, align 4
  %cmp29 = icmp slt i32 %20, 6
  br i1 %cmp29, label %if.then30, label %if.else67

if.then30:                                        ; preds = %land.lhs.true28
  %21 = load i32, i32* %num.addr, align 4
  switch i32 %21, label %sw.default65 [
    i32 2, label %sw.bb31
    i32 4, label %sw.bb38
    i32 1, label %sw.bb47
    i32 3, label %sw.bb56
  ]

sw.bb31:                                          ; preds = %if.then30
  %22 = load float, float* %value.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %23 = load i32, i32* %axis.addr, align 4
  %sub = sub nsw i32 %23, 3
  %arrayidx32 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_stopERP33 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx32, i32 0, i32 3
  store float %22, float* %m_stopERP33, align 4
  %24 = load i32, i32* %axis.addr, align 4
  %mul34 = mul nsw i32 %24, 4
  %shl35 = shl i32 2, %mul34
  %m_flags36 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %25 = load i32, i32* %m_flags36, align 4
  %or37 = or i32 %25, %shl35
  store i32 %or37, i32* %m_flags36, align 4
  br label %sw.epilog66

sw.bb38:                                          ; preds = %if.then30
  %26 = load float, float* %value.addr, align 4
  %m_angularLimits39 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %27 = load i32, i32* %axis.addr, align 4
  %sub40 = sub nsw i32 %27, 3
  %arrayidx41 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits39, i32 0, i32 %sub40
  %m_stopCFM42 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx41, i32 0, i32 4
  store float %26, float* %m_stopCFM42, align 4
  %28 = load i32, i32* %axis.addr, align 4
  %mul43 = mul nsw i32 %28, 4
  %shl44 = shl i32 1, %mul43
  %m_flags45 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %29 = load i32, i32* %m_flags45, align 4
  %or46 = or i32 %29, %shl44
  store i32 %or46, i32* %m_flags45, align 4
  br label %sw.epilog66

sw.bb47:                                          ; preds = %if.then30
  %30 = load float, float* %value.addr, align 4
  %m_angularLimits48 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %31 = load i32, i32* %axis.addr, align 4
  %sub49 = sub nsw i32 %31, 3
  %arrayidx50 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits48, i32 0, i32 %sub49
  %m_motorERP51 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx50, i32 0, i32 5
  store float %30, float* %m_motorERP51, align 4
  %32 = load i32, i32* %axis.addr, align 4
  %mul52 = mul nsw i32 %32, 4
  %shl53 = shl i32 8, %mul52
  %m_flags54 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %33 = load i32, i32* %m_flags54, align 4
  %or55 = or i32 %33, %shl53
  store i32 %or55, i32* %m_flags54, align 4
  br label %sw.epilog66

sw.bb56:                                          ; preds = %if.then30
  %34 = load float, float* %value.addr, align 4
  %m_angularLimits57 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %35 = load i32, i32* %axis.addr, align 4
  %sub58 = sub nsw i32 %35, 3
  %arrayidx59 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits57, i32 0, i32 %sub58
  %m_motorCFM60 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx59, i32 0, i32 6
  store float %34, float* %m_motorCFM60, align 4
  %36 = load i32, i32* %axis.addr, align 4
  %mul61 = mul nsw i32 %36, 4
  %shl62 = shl i32 4, %mul61
  %m_flags63 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 16
  %37 = load i32, i32* %m_flags63, align 4
  %or64 = or i32 %37, %shl62
  store i32 %or64, i32* %m_flags63, align 4
  br label %sw.epilog66

sw.default65:                                     ; preds = %if.then30
  br label %sw.epilog66

sw.epilog66:                                      ; preds = %sw.default65, %sw.bb56, %sw.bb47, %sw.bb38, %sw.bb31
  br label %if.end

if.else67:                                        ; preds = %land.lhs.true28, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else67, %sw.epilog66
  br label %if.end68

if.end68:                                         ; preds = %if.end, %sw.epilog
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZNK30btGeneric6DofSpring2Constraint8getParamEii(%class.btGeneric6DofSpring2Constraint* %this, i32 %num, i32 %axis) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store float 0.000000e+00, float* %retVal, align 4
  %0 = load i32, i32* %axis.addr, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %1, 3
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %num.addr, align 4
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 1, label %sw.bb7
    i32 3, label %sw.bb11
  ]

sw.bb:                                            ; preds = %if.then
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 3
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_stopERP)
  %3 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx, align 4
  store float %4, float* %retVal, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_linearLimits4 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits4, i32 0, i32 4
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_stopCFM)
  %5 = load i32, i32* %axis.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %5
  %6 = load float, float* %arrayidx6, align 4
  store float %6, float* %retVal, align 4
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.then
  %m_linearLimits8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorERP = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits8, i32 0, i32 5
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_motorERP)
  %7 = load i32, i32* %axis.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  store float %8, float* %retVal, align 4
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.then
  %m_linearLimits12 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorCFM = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits12, i32 0, i32 6
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_motorCFM)
  %9 = load i32, i32* %axis.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %9
  %10 = load float, float* %arrayidx14, align 4
  store float %10, float* %retVal, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb11, %sw.bb7, %sw.bb3, %sw.bb
  br label %if.end40

if.else:                                          ; preds = %land.lhs.true, %entry
  %11 = load i32, i32* %axis.addr, align 4
  %cmp15 = icmp sge i32 %11, 3
  br i1 %cmp15, label %land.lhs.true16, label %if.else39

land.lhs.true16:                                  ; preds = %if.else
  %12 = load i32, i32* %axis.addr, align 4
  %cmp17 = icmp slt i32 %12, 6
  br i1 %cmp17, label %if.then18, label %if.else39

if.then18:                                        ; preds = %land.lhs.true16
  %13 = load i32, i32* %num.addr, align 4
  switch i32 %13, label %sw.default37 [
    i32 2, label %sw.bb19
    i32 4, label %sw.bb22
    i32 1, label %sw.bb27
    i32 3, label %sw.bb32
  ]

sw.bb19:                                          ; preds = %if.then18
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %14 = load i32, i32* %axis.addr, align 4
  %sub = sub nsw i32 %14, 3
  %arrayidx20 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_stopERP21 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx20, i32 0, i32 3
  %15 = load float, float* %m_stopERP21, align 4
  store float %15, float* %retVal, align 4
  br label %sw.epilog38

sw.bb22:                                          ; preds = %if.then18
  %m_angularLimits23 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %16 = load i32, i32* %axis.addr, align 4
  %sub24 = sub nsw i32 %16, 3
  %arrayidx25 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits23, i32 0, i32 %sub24
  %m_stopCFM26 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx25, i32 0, i32 4
  %17 = load float, float* %m_stopCFM26, align 4
  store float %17, float* %retVal, align 4
  br label %sw.epilog38

sw.bb27:                                          ; preds = %if.then18
  %m_angularLimits28 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %18 = load i32, i32* %axis.addr, align 4
  %sub29 = sub nsw i32 %18, 3
  %arrayidx30 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits28, i32 0, i32 %sub29
  %m_motorERP31 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx30, i32 0, i32 5
  %19 = load float, float* %m_motorERP31, align 4
  store float %19, float* %retVal, align 4
  br label %sw.epilog38

sw.bb32:                                          ; preds = %if.then18
  %m_angularLimits33 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %20 = load i32, i32* %axis.addr, align 4
  %sub34 = sub nsw i32 %20, 3
  %arrayidx35 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits33, i32 0, i32 %sub34
  %m_motorCFM36 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx35, i32 0, i32 6
  %21 = load float, float* %m_motorCFM36, align 4
  store float %21, float* %retVal, align 4
  br label %sw.epilog38

sw.default37:                                     ; preds = %if.then18
  br label %sw.epilog38

sw.epilog38:                                      ; preds = %sw.default37, %sw.bb32, %sw.bb27, %sw.bb22, %sw.bb19
  br label %if.end

if.else39:                                        ; preds = %land.lhs.true16, %if.else
  br label %if.end

if.end:                                           ; preds = %if.else39, %sw.epilog38
  br label %if.end40

if.end40:                                         ; preds = %if.end, %sw.epilog
  %22 = load float, float* %retVal, align 4
  ret float %22
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint7setAxisERK9btVector3S2_(%class.btGeneric6DofSpring2Constraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %axis1.addr = alloca %class.btVector3*, align 4
  %axis2.addr = alloca %class.btVector3*, align 4
  %zAxis = alloca %class.btVector3, align 4
  %yAxis = alloca %class.btVector3, align 4
  %xAxis = alloca %class.btVector3, align 4
  %frameInW = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp20 = alloca %class.btTransform, align 4
  %ref.tmp23 = alloca %class.btTransform, align 4
  %ref.tmp24 = alloca %class.btTransform, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btVector3* %axis1, %class.btVector3** %axis1.addr, align 4
  store %class.btVector3* %axis2, %class.btVector3** %axis2.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis1.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %zAxis, %class.btVector3* %0)
  %1 = load %class.btVector3*, %class.btVector3** %axis2.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %yAxis, %class.btVector3* %1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %xAxis, %class.btVector3* %yAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %zAxis)
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %frameInW)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %frameInW)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %frameInW)
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 2
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call2, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx15, float* nonnull align 4 dereferenceable(4) %arrayidx17, float* nonnull align 4 dereferenceable(4) %arrayidx19)
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp20, %class.btTransform* %call21)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %ref.tmp20, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %4 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp24, %class.btTransform* %call25)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp23, %class.btTransform* %ref.tmp24, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp23)
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint9setBounceEif(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %bounce) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %bounce.addr = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %bounce, float* %bounce.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %bounce.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_bounce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 2
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_bounce)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %bounce.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_bounce3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 2
  store float %3, float* %m_bounce3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint11enableMotorEib(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, i1 zeroext %onOff) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %onOff.addr = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %frombool = zext i1 %onOff to i8
  store i8 %frombool, i8* %onOff.addr, align 1
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %onOff.addr, align 1
  %tobool = trunc i8 %1 to i1
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 7
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %2
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %arrayidx, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i8, i8* %onOff.addr, align 1
  %tobool3 = trunc i8 %3 to i1
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_enableMotor5 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 7
  %frombool6 = zext i1 %tobool3 to i8
  store i8 %frombool6, i8* %m_enableMotor5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint8setServoEib(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, i1 zeroext %onOff) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %onOff.addr = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %frombool = zext i1 %onOff to i8
  store i8 %frombool, i8* %onOff.addr, align 1
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %onOff.addr, align 1
  %tobool = trunc i8 %1 to i1
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoMotor = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 8
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %m_servoMotor, i32 0, i32 %2
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %arrayidx, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i8, i8* %onOff.addr, align 1
  %tobool3 = trunc i8 %3 to i1
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_servoMotor5 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 10
  %frombool6 = zext i1 %tobool3 to i8
  store i8 %frombool6, i8* %m_servoMotor5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint17setTargetVelocityEif(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %velocity) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %velocity.addr = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %velocity, float* %velocity.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %velocity.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 16
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %velocity.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_targetVelocity3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 8
  store float %3, float* %m_targetVelocity3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint14setServoTargetEif(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %target) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %target.addr = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %target, float* %target.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %target.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoTarget = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 10
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_servoTarget)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %target.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_servoTarget3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 11
  store float %3, float* %m_servoTarget3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint16setMaxMotorForceEif(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %force) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %force.addr = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %force, float* %force.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %force.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 17
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %force.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_maxMotorForce3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 9
  store float %3, float* %m_maxMotorForce3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint12enableSpringEib(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, i1 zeroext %onOff) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %onOff.addr = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %frombool = zext i1 %onOff to i8
  store i8 %frombool, i8* %onOff.addr, align 1
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %onOff.addr, align 1
  %tobool = trunc i8 %1 to i1
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 9
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring, i32 0, i32 %2
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %arrayidx, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i8, i8* %onOff.addr, align 1
  %tobool3 = trunc i8 %3 to i1
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_enableSpring5 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 12
  %frombool6 = zext i1 %tobool3 to i8
  store i8 %frombool6, i8* %m_enableSpring5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint12setStiffnessEifb(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %stiffness, i1 zeroext %limitIfNeeded) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %stiffness.addr = alloca float, align 4
  %limitIfNeeded.addr = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %stiffness, float* %stiffness.addr, align 4
  %frombool = zext i1 %limitIfNeeded to i8
  store i8 %frombool, i8* %limitIfNeeded.addr, align 1
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %stiffness.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffness = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 11
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springStiffness)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  %3 = load i8, i8* %limitIfNeeded.addr, align 1
  %tobool = trunc i8 %3 to i1
  %m_linearLimits2 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffnessLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits2, i32 0, i32 12
  %4 = load i32, i32* %index.addr, align 4
  %arrayidx3 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springStiffnessLimited, i32 0, i32 %4
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %arrayidx3, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load float, float* %stiffness.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %6 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %6, 3
  %arrayidx5 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_springStiffness6 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx5, i32 0, i32 13
  store float %5, float* %m_springStiffness6, align 4
  %7 = load i8, i8* %limitIfNeeded.addr, align 1
  %tobool7 = trunc i8 %7 to i1
  %m_angularLimits8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %index.addr, align 4
  %sub9 = sub nsw i32 %8, 3
  %arrayidx10 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits8, i32 0, i32 %sub9
  %m_springStiffnessLimited11 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx10, i32 0, i32 14
  %frombool12 = zext i1 %tobool7 to i8
  store i8 %frombool12, i8* %m_springStiffnessLimited11, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint10setDampingEifb(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %damping, i1 zeroext %limitIfNeeded) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %damping.addr = alloca float, align 4
  %limitIfNeeded.addr = alloca i8, align 1
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %damping, float* %damping.addr, align 4
  %frombool = zext i1 %limitIfNeeded to i8
  store i8 %frombool, i8* %limitIfNeeded.addr, align 1
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %damping.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDamping = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 13
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_springDamping)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  %3 = load i8, i8* %limitIfNeeded.addr, align 1
  %tobool = trunc i8 %3 to i1
  %m_linearLimits2 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDampingLimited = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits2, i32 0, i32 14
  %4 = load i32, i32* %index.addr, align 4
  %arrayidx3 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springDampingLimited, i32 0, i32 %4
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %arrayidx3, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load float, float* %damping.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %6 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %6, 3
  %arrayidx5 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_springDamping6 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx5, i32 0, i32 15
  store float %5, float* %m_springDamping6, align 4
  %7 = load i8, i8* %limitIfNeeded.addr, align 1
  %tobool7 = trunc i8 %7 to i1
  %m_angularLimits8 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %index.addr, align 4
  %sub9 = sub nsw i32 %8, 3
  %arrayidx10 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits8, i32 0, i32 %sub9
  %m_springDampingLimited11 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx10, i32 0, i32 16
  %frombool12 = zext i1 %tobool7 to i8
  store i8 %frombool12, i8* %m_springDampingLimited11, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19setEquilibriumPointEv(%class.btGeneric6DofSpring2Constraint* %this) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff)
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 15
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_equilibriumPoint)
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %3
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc11, %for.end
  %5 = load i32, i32* %i, align 4
  %cmp5 = icmp slt i32 %5, 3
  br i1 %cmp5, label %for.body6, label %for.end13

for.body6:                                        ; preds = %for.cond4
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %6 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %6
  %7 = load float, float* %arrayidx8, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %8 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %8
  %m_equilibriumPoint10 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx9, i32 0, i32 17
  store float %7, float* %m_equilibriumPoint10, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body6
  %9 = load i32, i32* %i, align 4
  %inc12 = add nsw i32 %9, 1
  store i32 %inc12, i32* %i, align 4
  br label %for.cond4

for.end13:                                        ; preds = %for.cond4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19setEquilibriumPointEi(%class.btGeneric6DofSpring2Constraint* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  call void @_ZN30btGeneric6DofSpring2Constraint19calculateTransformsEv(%class.btGeneric6DofSpring2Constraint* %this1)
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 12
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff)
  %1 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 15
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_equilibriumPoint)
  %3 = load i32, i32* %index.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %3
  store float %2, float* %arrayidx3, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 10
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 %sub
  %5 = load float, float* %arrayidx5, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %6 = load i32, i32* %index.addr, align 4
  %sub6 = sub nsw i32 %6, 3
  %arrayidx7 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub6
  %m_equilibriumPoint8 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx7, i32 0, i32 17
  store float %5, float* %m_equilibriumPoint8, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btGeneric6DofSpring2Constraint19setEquilibriumPointEif(%class.btGeneric6DofSpring2Constraint* %this, i32 %index, float %val) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %index.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float, float* %val.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 15
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_equilibriumPoint)
  %2 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  store float %1, float* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load float, float* %val.addr, align 4
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %4, 3
  %arrayidx2 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %sub
  %m_equilibriumPoint3 = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx2, i32 0, i32 17
  store float %3, float* %m_equilibriumPoint3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintD2Ev(%class.btGeneric6DofSpring2Constraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #8
  ret %class.btGeneric6DofSpring2Constraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2ConstraintD0Ev(%class.btGeneric6DofSpring2Constraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %call = call %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintD2Ev(%class.btGeneric6DofSpring2Constraint* %this1) #8
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to i8*
  call void @_ZN30btGeneric6DofSpring2ConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv(%class.btGeneric6DofSpring2Constraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv(%class.btGeneric6DofSpring2Constraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  ret i32 644
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer(%class.btGeneric6DofSpring2Constraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dof = alloca %struct.btGeneric6DofSpring2ConstraintData*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btGeneric6DofSpring2ConstraintData*
  store %struct.btGeneric6DofSpring2ConstraintData* %1, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %6 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rbAFrame = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInA, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %7 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rbBFrame = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInB, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %8, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %9
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx, i32 0, i32 0
  %10 = load float, float* %m_loLimit, align 4
  %11 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularLowerLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %11, i32 0, i32 23
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %12
  store float %10, float* %arrayidx2, align 4
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits3, i32 0, i32 %13
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 1
  %14 = load float, float* %m_hiLimit, align 4
  %15 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularUpperLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %15, i32 0, i32 22
  %m_floats5 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit, i32 0, i32 0
  %16 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 %16
  store float %14, float* %arrayidx6, align 4
  %m_angularLimits7 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %17 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits7, i32 0, i32 %17
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx8, i32 0, i32 2
  %18 = load float, float* %m_bounce, align 4
  %19 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularBounce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %19, i32 0, i32 24
  %m_floats9 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularBounce, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 %20
  store float %18, float* %arrayidx10, align 4
  %m_angularLimits11 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %21 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits11, i32 0, i32 %21
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx12, i32 0, i32 3
  %22 = load float, float* %m_stopERP, align 4
  %23 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %23, i32 0, i32 25
  %m_floats13 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopERP, i32 0, i32 0
  %24 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 %24
  store float %22, float* %arrayidx14, align 4
  %m_angularLimits15 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %25 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits15, i32 0, i32 %25
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx16, i32 0, i32 4
  %26 = load float, float* %m_stopCFM, align 4
  %27 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %27, i32 0, i32 26
  %m_floats17 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopCFM, i32 0, i32 0
  %28 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 %28
  store float %26, float* %arrayidx18, align 4
  %m_angularLimits19 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %29 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits19, i32 0, i32 %29
  %m_motorERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx20, i32 0, i32 5
  %30 = load float, float* %m_motorERP, align 4
  %31 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %31, i32 0, i32 27
  %m_floats21 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorERP, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 %32
  store float %30, float* %arrayidx22, align 4
  %m_angularLimits23 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %33 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits23, i32 0, i32 %33
  %m_motorCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx24, i32 0, i32 6
  %34 = load float, float* %m_motorCFM, align 4
  %35 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %35, i32 0, i32 28
  %m_floats25 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorCFM, i32 0, i32 0
  %36 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds [4 x float], [4 x float]* %m_floats25, i32 0, i32 %36
  store float %34, float* %arrayidx26, align 4
  %m_angularLimits27 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %37 = load i32, i32* %i, align 4
  %arrayidx28 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits27, i32 0, i32 %37
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx28, i32 0, i32 8
  %38 = load float, float* %m_targetVelocity, align 4
  %39 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularTargetVelocity = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %39, i32 0, i32 29
  %m_floats29 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularTargetVelocity, i32 0, i32 0
  %40 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 %40
  store float %38, float* %arrayidx30, align 4
  %m_angularLimits31 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %41 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits31, i32 0, i32 %41
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx32, i32 0, i32 9
  %42 = load float, float* %m_maxMotorForce, align 4
  %43 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMaxMotorForce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %43, i32 0, i32 30
  %m_floats33 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMaxMotorForce, i32 0, i32 0
  %44 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [4 x float], [4 x float]* %m_floats33, i32 0, i32 %44
  store float %42, float* %arrayidx34, align 4
  %m_angularLimits35 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %45 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits35, i32 0, i32 %45
  %m_servoTarget = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx36, i32 0, i32 11
  %46 = load float, float* %m_servoTarget, align 4
  %47 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoTarget = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %47, i32 0, i32 31
  %m_floats37 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularServoTarget, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds [4 x float], [4 x float]* %m_floats37, i32 0, i32 %48
  store float %46, float* %arrayidx38, align 4
  %m_angularLimits39 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %49 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits39, i32 0, i32 %49
  %m_springStiffness = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx40, i32 0, i32 13
  %50 = load float, float* %m_springStiffness, align 4
  %51 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffness = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %51, i32 0, i32 32
  %m_floats41 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringStiffness, i32 0, i32 0
  %52 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [4 x float], [4 x float]* %m_floats41, i32 0, i32 %52
  store float %50, float* %arrayidx42, align 4
  %m_angularLimits43 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %53 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits43, i32 0, i32 %53
  %m_springDamping = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx44, i32 0, i32 15
  %54 = load float, float* %m_springDamping, align 4
  %55 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDamping = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %55, i32 0, i32 33
  %m_floats45 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringDamping, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx46 = getelementptr inbounds [4 x float], [4 x float]* %m_floats45, i32 0, i32 %56
  store float %54, float* %arrayidx46, align 4
  %m_angularLimits47 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %57 = load i32, i32* %i, align 4
  %arrayidx48 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits47, i32 0, i32 %57
  %m_equilibriumPoint = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx48, i32 0, i32 17
  %58 = load float, float* %m_equilibriumPoint, align 4
  %59 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEquilibriumPoint = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %59, i32 0, i32 34
  %m_floats49 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularEquilibriumPoint, i32 0, i32 0
  %60 = load i32, i32* %i, align 4
  %arrayidx50 = getelementptr inbounds [4 x float], [4 x float]* %m_floats49, i32 0, i32 %60
  store float %58, float* %arrayidx50, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %61 = load i32, i32* %i, align 4
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %62 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularLowerLimit51 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %62, i32 0, i32 23
  %m_floats52 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit51, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [4 x float], [4 x float]* %m_floats52, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx53, align 4
  %63 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularUpperLimit54 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %63, i32 0, i32 22
  %m_floats55 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit54, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [4 x float], [4 x float]* %m_floats55, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx56, align 4
  %64 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularBounce57 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %64, i32 0, i32 24
  %m_floats58 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularBounce57, i32 0, i32 0
  %arrayidx59 = getelementptr inbounds [4 x float], [4 x float]* %m_floats58, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx59, align 4
  %65 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopERP60 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %65, i32 0, i32 25
  %m_floats61 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopERP60, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx62, align 4
  %66 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopCFM63 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %66, i32 0, i32 26
  %m_floats64 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopCFM63, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx65, align 4
  %67 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorERP66 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %67, i32 0, i32 27
  %m_floats67 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorERP66, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [4 x float], [4 x float]* %m_floats67, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx68, align 4
  %68 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorCFM69 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %68, i32 0, i32 28
  %m_floats70 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorCFM69, i32 0, i32 0
  %arrayidx71 = getelementptr inbounds [4 x float], [4 x float]* %m_floats70, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx71, align 4
  %69 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularTargetVelocity72 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %69, i32 0, i32 29
  %m_floats73 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularTargetVelocity72, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [4 x float], [4 x float]* %m_floats73, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx74, align 4
  %70 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMaxMotorForce75 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %70, i32 0, i32 30
  %m_floats76 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMaxMotorForce75, i32 0, i32 0
  %arrayidx77 = getelementptr inbounds [4 x float], [4 x float]* %m_floats76, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx77, align 4
  %71 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoTarget78 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %71, i32 0, i32 31
  %m_floats79 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularServoTarget78, i32 0, i32 0
  %arrayidx80 = getelementptr inbounds [4 x float], [4 x float]* %m_floats79, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx80, align 4
  %72 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffness81 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %72, i32 0, i32 32
  %m_floats82 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringStiffness81, i32 0, i32 0
  %arrayidx83 = getelementptr inbounds [4 x float], [4 x float]* %m_floats82, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx83, align 4
  %73 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDamping84 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %73, i32 0, i32 33
  %m_floats85 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringDamping84, i32 0, i32 0
  %arrayidx86 = getelementptr inbounds [4 x float], [4 x float]* %m_floats85, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx86, align 4
  %74 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEquilibriumPoint87 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %74, i32 0, i32 34
  %m_floats88 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularEquilibriumPoint87, i32 0, i32 0
  %arrayidx89 = getelementptr inbounds [4 x float], [4 x float]* %m_floats88, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx89, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc142, %for.end
  %75 = load i32, i32* %i, align 4
  %cmp91 = icmp slt i32 %75, 4
  br i1 %cmp91, label %for.body92, label %for.end144

for.body92:                                       ; preds = %for.cond90
  %76 = load i32, i32* %i, align 4
  %cmp93 = icmp slt i32 %76, 3
  br i1 %cmp93, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body92
  %m_angularLimits94 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %77 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits94, i32 0, i32 %77
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx95, i32 0, i32 7
  %78 = load i8, i8* %m_enableMotor, align 4
  %tobool = trunc i8 %78 to i1
  %79 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  br label %cond.end

cond.false:                                       ; preds = %for.body92
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond96 = phi i32 [ %cond, %cond.true ], [ 0, %cond.false ]
  %conv = trunc i32 %cond96 to i8
  %80 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEnableMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %80, i32 0, i32 35
  %81 = load i32, i32* %i, align 4
  %arrayidx97 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularEnableMotor, i32 0, i32 %81
  store i8 %conv, i8* %arrayidx97, align 1
  %82 = load i32, i32* %i, align 4
  %cmp98 = icmp slt i32 %82, 3
  br i1 %cmp98, label %cond.true99, label %cond.false104

cond.true99:                                      ; preds = %cond.end
  %m_angularLimits100 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %83 = load i32, i32* %i, align 4
  %arrayidx101 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits100, i32 0, i32 %83
  %m_servoMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx101, i32 0, i32 10
  %84 = load i8, i8* %m_servoMotor, align 4
  %tobool102 = trunc i8 %84 to i1
  %85 = zext i1 %tobool102 to i64
  %cond103 = select i1 %tobool102, i32 1, i32 0
  br label %cond.end105

cond.false104:                                    ; preds = %cond.end
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false104, %cond.true99
  %cond106 = phi i32 [ %cond103, %cond.true99 ], [ 0, %cond.false104 ]
  %conv107 = trunc i32 %cond106 to i8
  %86 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %86, i32 0, i32 36
  %87 = load i32, i32* %i, align 4
  %arrayidx108 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularServoMotor, i32 0, i32 %87
  store i8 %conv107, i8* %arrayidx108, align 1
  %88 = load i32, i32* %i, align 4
  %cmp109 = icmp slt i32 %88, 3
  br i1 %cmp109, label %cond.true110, label %cond.false115

cond.true110:                                     ; preds = %cond.end105
  %m_angularLimits111 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %89 = load i32, i32* %i, align 4
  %arrayidx112 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits111, i32 0, i32 %89
  %m_enableSpring = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx112, i32 0, i32 12
  %90 = load i8, i8* %m_enableSpring, align 4
  %tobool113 = trunc i8 %90 to i1
  %91 = zext i1 %tobool113 to i64
  %cond114 = select i1 %tobool113, i32 1, i32 0
  br label %cond.end116

cond.false115:                                    ; preds = %cond.end105
  br label %cond.end116

cond.end116:                                      ; preds = %cond.false115, %cond.true110
  %cond117 = phi i32 [ %cond114, %cond.true110 ], [ 0, %cond.false115 ]
  %conv118 = trunc i32 %cond117 to i8
  %92 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEnableSpring = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %92, i32 0, i32 37
  %93 = load i32, i32* %i, align 4
  %arrayidx119 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularEnableSpring, i32 0, i32 %93
  store i8 %conv118, i8* %arrayidx119, align 1
  %94 = load i32, i32* %i, align 4
  %cmp120 = icmp slt i32 %94, 3
  br i1 %cmp120, label %cond.true121, label %cond.false126

cond.true121:                                     ; preds = %cond.end116
  %m_angularLimits122 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %95 = load i32, i32* %i, align 4
  %arrayidx123 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits122, i32 0, i32 %95
  %m_springStiffnessLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx123, i32 0, i32 14
  %96 = load i8, i8* %m_springStiffnessLimited, align 4
  %tobool124 = trunc i8 %96 to i1
  %97 = zext i1 %tobool124 to i64
  %cond125 = select i1 %tobool124, i32 1, i32 0
  br label %cond.end127

cond.false126:                                    ; preds = %cond.end116
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false126, %cond.true121
  %cond128 = phi i32 [ %cond125, %cond.true121 ], [ 0, %cond.false126 ]
  %conv129 = trunc i32 %cond128 to i8
  %98 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffnessLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %98, i32 0, i32 38
  %99 = load i32, i32* %i, align 4
  %arrayidx130 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularSpringStiffnessLimited, i32 0, i32 %99
  store i8 %conv129, i8* %arrayidx130, align 1
  %100 = load i32, i32* %i, align 4
  %cmp131 = icmp slt i32 %100, 3
  br i1 %cmp131, label %cond.true132, label %cond.false137

cond.true132:                                     ; preds = %cond.end127
  %m_angularLimits133 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %101 = load i32, i32* %i, align 4
  %arrayidx134 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits133, i32 0, i32 %101
  %m_springDampingLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx134, i32 0, i32 16
  %102 = load i8, i8* %m_springDampingLimited, align 4
  %tobool135 = trunc i8 %102 to i1
  %103 = zext i1 %tobool135 to i64
  %cond136 = select i1 %tobool135, i32 1, i32 0
  br label %cond.end138

cond.false137:                                    ; preds = %cond.end127
  br label %cond.end138

cond.end138:                                      ; preds = %cond.false137, %cond.true132
  %cond139 = phi i32 [ %cond136, %cond.true132 ], [ 0, %cond.false137 ]
  %conv140 = trunc i32 %cond139 to i8
  %104 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDampingLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %104, i32 0, i32 39
  %105 = load i32, i32* %i, align 4
  %arrayidx141 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularSpringDampingLimited, i32 0, i32 %105
  store i8 %conv140, i8* %arrayidx141, align 1
  br label %for.inc142

for.inc142:                                       ; preds = %cond.end138
  %106 = load i32, i32* %i, align 4
  %inc143 = add nsw i32 %106, 1
  store i32 %inc143, i32* %i, align 4
  br label %for.cond90

for.end144:                                       ; preds = %for.cond90
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 0
  %107 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearLowerLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %107, i32 0, i32 4
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_lowerLimit, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearLowerLimit)
  %m_linearLimits145 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits145, i32 0, i32 1
  %108 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearUpperLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %108, i32 0, i32 3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_upperLimit, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearUpperLimit)
  %m_linearLimits146 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_bounce147 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits146, i32 0, i32 2
  %109 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearBounce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %109, i32 0, i32 5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bounce147, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearBounce)
  %m_linearLimits148 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopERP149 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits148, i32 0, i32 3
  %110 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearStopERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %110, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_stopERP149, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearStopERP)
  %m_linearLimits150 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopCFM151 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits150, i32 0, i32 4
  %111 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearStopCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %111, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_stopCFM151, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearStopCFM)
  %m_linearLimits152 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorERP153 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits152, i32 0, i32 5
  %112 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMotorERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %112, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_motorERP153, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMotorERP)
  %m_linearLimits154 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorCFM155 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits154, i32 0, i32 6
  %113 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMotorCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %113, i32 0, i32 9
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_motorCFM155, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMotorCFM)
  %m_linearLimits156 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_targetVelocity157 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits156, i32 0, i32 16
  %114 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearTargetVelocity = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %114, i32 0, i32 10
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_targetVelocity157, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearTargetVelocity)
  %m_linearLimits158 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_maxMotorForce159 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits158, i32 0, i32 17
  %115 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMaxMotorForce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %115, i32 0, i32 11
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_maxMotorForce159, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMaxMotorForce)
  %m_linearLimits160 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoTarget161 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits160, i32 0, i32 10
  %116 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearServoTarget = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %116, i32 0, i32 12
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_servoTarget161, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearServoTarget)
  %m_linearLimits162 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffness163 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits162, i32 0, i32 11
  %117 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringStiffness = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %117, i32 0, i32 13
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_springStiffness163, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearSpringStiffness)
  %m_linearLimits164 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDamping165 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits164, i32 0, i32 13
  %118 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringDamping = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %118, i32 0, i32 14
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_springDamping165, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearSpringDamping)
  %m_linearLimits166 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint167 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits166, i32 0, i32 15
  %119 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEquilibriumPoint = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %119, i32 0, i32 15
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_equilibriumPoint167, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearEquilibriumPoint)
  store i32 0, i32* %i, align 4
  br label %for.cond168

for.cond168:                                      ; preds = %for.inc231, %for.end144
  %120 = load i32, i32* %i, align 4
  %cmp169 = icmp slt i32 %120, 4
  br i1 %cmp169, label %for.body170, label %for.end233

for.body170:                                      ; preds = %for.cond168
  %121 = load i32, i32* %i, align 4
  %cmp171 = icmp slt i32 %121, 3
  br i1 %cmp171, label %cond.true172, label %cond.false178

cond.true172:                                     ; preds = %for.body170
  %m_linearLimits173 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor174 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits173, i32 0, i32 7
  %122 = load i32, i32* %i, align 4
  %arrayidx175 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor174, i32 0, i32 %122
  %123 = load i8, i8* %arrayidx175, align 1
  %tobool176 = trunc i8 %123 to i1
  %124 = zext i1 %tobool176 to i64
  %cond177 = select i1 %tobool176, i32 1, i32 0
  br label %cond.end179

cond.false178:                                    ; preds = %for.body170
  br label %cond.end179

cond.end179:                                      ; preds = %cond.false178, %cond.true172
  %cond180 = phi i32 [ %cond177, %cond.true172 ], [ 0, %cond.false178 ]
  %conv181 = trunc i32 %cond180 to i8
  %125 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEnableMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %125, i32 0, i32 16
  %126 = load i32, i32* %i, align 4
  %arrayidx182 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearEnableMotor, i32 0, i32 %126
  store i8 %conv181, i8* %arrayidx182, align 1
  %127 = load i32, i32* %i, align 4
  %cmp183 = icmp slt i32 %127, 3
  br i1 %cmp183, label %cond.true184, label %cond.false190

cond.true184:                                     ; preds = %cond.end179
  %m_linearLimits185 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoMotor186 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits185, i32 0, i32 8
  %128 = load i32, i32* %i, align 4
  %arrayidx187 = getelementptr inbounds [3 x i8], [3 x i8]* %m_servoMotor186, i32 0, i32 %128
  %129 = load i8, i8* %arrayidx187, align 1
  %tobool188 = trunc i8 %129 to i1
  %130 = zext i1 %tobool188 to i64
  %cond189 = select i1 %tobool188, i32 1, i32 0
  br label %cond.end191

cond.false190:                                    ; preds = %cond.end179
  br label %cond.end191

cond.end191:                                      ; preds = %cond.false190, %cond.true184
  %cond192 = phi i32 [ %cond189, %cond.true184 ], [ 0, %cond.false190 ]
  %conv193 = trunc i32 %cond192 to i8
  %131 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearServoMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %131, i32 0, i32 17
  %132 = load i32, i32* %i, align 4
  %arrayidx194 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearServoMotor, i32 0, i32 %132
  store i8 %conv193, i8* %arrayidx194, align 1
  %133 = load i32, i32* %i, align 4
  %cmp195 = icmp slt i32 %133, 3
  br i1 %cmp195, label %cond.true196, label %cond.false202

cond.true196:                                     ; preds = %cond.end191
  %m_linearLimits197 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring198 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits197, i32 0, i32 9
  %134 = load i32, i32* %i, align 4
  %arrayidx199 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring198, i32 0, i32 %134
  %135 = load i8, i8* %arrayidx199, align 1
  %tobool200 = trunc i8 %135 to i1
  %136 = zext i1 %tobool200 to i64
  %cond201 = select i1 %tobool200, i32 1, i32 0
  br label %cond.end203

cond.false202:                                    ; preds = %cond.end191
  br label %cond.end203

cond.end203:                                      ; preds = %cond.false202, %cond.true196
  %cond204 = phi i32 [ %cond201, %cond.true196 ], [ 0, %cond.false202 ]
  %conv205 = trunc i32 %cond204 to i8
  %137 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEnableSpring = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %137, i32 0, i32 18
  %138 = load i32, i32* %i, align 4
  %arrayidx206 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearEnableSpring, i32 0, i32 %138
  store i8 %conv205, i8* %arrayidx206, align 1
  %139 = load i32, i32* %i, align 4
  %cmp207 = icmp slt i32 %139, 3
  br i1 %cmp207, label %cond.true208, label %cond.false214

cond.true208:                                     ; preds = %cond.end203
  %m_linearLimits209 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffnessLimited210 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits209, i32 0, i32 12
  %140 = load i32, i32* %i, align 4
  %arrayidx211 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springStiffnessLimited210, i32 0, i32 %140
  %141 = load i8, i8* %arrayidx211, align 1
  %tobool212 = trunc i8 %141 to i1
  %142 = zext i1 %tobool212 to i64
  %cond213 = select i1 %tobool212, i32 1, i32 0
  br label %cond.end215

cond.false214:                                    ; preds = %cond.end203
  br label %cond.end215

cond.end215:                                      ; preds = %cond.false214, %cond.true208
  %cond216 = phi i32 [ %cond213, %cond.true208 ], [ 0, %cond.false214 ]
  %conv217 = trunc i32 %cond216 to i8
  %143 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringStiffnessLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %143, i32 0, i32 19
  %144 = load i32, i32* %i, align 4
  %arrayidx218 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearSpringStiffnessLimited, i32 0, i32 %144
  store i8 %conv217, i8* %arrayidx218, align 1
  %145 = load i32, i32* %i, align 4
  %cmp219 = icmp slt i32 %145, 3
  br i1 %cmp219, label %cond.true220, label %cond.false226

cond.true220:                                     ; preds = %cond.end215
  %m_linearLimits221 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDampingLimited222 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits221, i32 0, i32 14
  %146 = load i32, i32* %i, align 4
  %arrayidx223 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springDampingLimited222, i32 0, i32 %146
  %147 = load i8, i8* %arrayidx223, align 1
  %tobool224 = trunc i8 %147 to i1
  %148 = zext i1 %tobool224 to i64
  %cond225 = select i1 %tobool224, i32 1, i32 0
  br label %cond.end227

cond.false226:                                    ; preds = %cond.end215
  br label %cond.end227

cond.end227:                                      ; preds = %cond.false226, %cond.true220
  %cond228 = phi i32 [ %cond225, %cond.true220 ], [ 0, %cond.false226 ]
  %conv229 = trunc i32 %cond228 to i8
  %149 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringDampingLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %149, i32 0, i32 20
  %150 = load i32, i32* %i, align 4
  %arrayidx230 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearSpringDampingLimited, i32 0, i32 %150
  store i8 %conv229, i8* %arrayidx230, align 1
  br label %for.inc231

for.inc231:                                       ; preds = %cond.end227
  %151 = load i32, i32* %i, align 4
  %inc232 = add nsw i32 %151, 1
  store i32 %inc232, i32* %i, align 4
  br label %for.cond168

for.end233:                                       ; preds = %for.cond168
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %152 = load i32, i32* %m_rotateOrder, align 4
  %153 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rotateOrder234 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %153, i32 0, i32 40
  store i32 %152, i32* %m_rotateOrder234, align 4
  ret i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #5

; Function Attrs: nounwind readnone
declare float @asinf(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  %0 = load float, float* %angleInRadians.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4
  %1 = load float, float* %angleInRadians.addr, align 4
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %fmod = frem float %0, %1
  ret float %fmod
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2ConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGeneric6DofSpring2Constraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
