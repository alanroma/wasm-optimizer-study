; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btFixedConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btFixedConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btFixedConstraint = type { %class.btGeneric6DofSpring2Constraint }
%class.btGeneric6DofSpring2Constraint = type { %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor2, [3 x %class.btRotationalLimitMotor2], i32, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, i32 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTranslationalLimitMotor2 = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], [3 x i8], [3 x i8], %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor2 = type { float, float, float, float, float, float, float, i8, float, float, i8, float, i8, float, i8, float, i8, float, float, float, float, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.5 = type opaque
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btGeneric6DofSpring2ConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, [4 x i8], [4 x i8], [4 x i8], [4 x i8], [4 x i8], i32 }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN30btGeneric6DofSpring2Constraint20setAngularLowerLimitERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN30btGeneric6DofSpring2Constraint20setAngularUpperLimitERK9btVector3 = comdat any

$_ZN30btGeneric6DofSpring2Constraint19setLinearLowerLimitERK9btVector3 = comdat any

$_ZN30btGeneric6DofSpring2Constraint19setLinearUpperLimitERK9btVector3 = comdat any

$_ZN30btGeneric6DofSpring2ConstraintD2Ev = comdat any

$_ZN30btGeneric6DofSpring2ConstraintdlEPv = comdat any

$_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer = comdat any

$_Z16btNormalizeAnglef = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z6btFmodff = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV17btFixedConstraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btFixedConstraint to i8*), i8* bitcast (%class.btFixedConstraint* (%class.btFixedConstraint*)* @_ZN17btFixedConstraintD1Ev to i8*), i8* bitcast (void (%class.btFixedConstraint*)* @_ZN17btFixedConstraintD0Ev to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*)* @_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN30btGeneric6DofSpring2Constraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN30btGeneric6DofSpring2Constraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btGeneric6DofSpring2Constraint*, i32, float, i32)* @_ZN30btGeneric6DofSpring2Constraint8setParamEifi to i8*), i8* bitcast (float (%class.btGeneric6DofSpring2Constraint*, i32, i32)* @_ZNK30btGeneric6DofSpring2Constraint8getParamEii to i8*), i8* bitcast (i32 (%class.btGeneric6DofSpring2Constraint*)* @_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGeneric6DofSpring2Constraint*, i8*, %class.btSerializer*)* @_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btFixedConstraint = hidden constant [20 x i8] c"17btFixedConstraint\00", align 1
@_ZTI30btGeneric6DofSpring2Constraint = external constant i8*
@_ZTI17btFixedConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btFixedConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btGeneric6DofSpring2Constraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [35 x i8] c"btGeneric6DofSpring2ConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btFixedConstraint.cpp, i8* null }]

@_ZN17btFixedConstraintC1ER11btRigidBodyS1_RK11btTransformS4_ = hidden unnamed_addr alias %class.btFixedConstraint* (%class.btFixedConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*), %class.btFixedConstraint* (%class.btFixedConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*)* @_ZN17btFixedConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
@_ZN17btFixedConstraintD1Ev = hidden unnamed_addr alias %class.btFixedConstraint* (%class.btFixedConstraint*), %class.btFixedConstraint* (%class.btFixedConstraint*)* @_ZN17btFixedConstraintD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btFixedConstraint* @_ZN17btFixedConstraintC2ER11btRigidBodyS1_RK11btTransformS4_(%class.btFixedConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btFixedConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInA.addr = alloca %class.btTransform*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  store %class.btFixedConstraint* %this, %class.btFixedConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInA, %class.btTransform** %frameInA.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  %this1 = load %class.btFixedConstraint*, %class.btFixedConstraint** %this.addr, align 4
  %0 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %frameInA.addr, align 4
  %4 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %call = call %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyS1_RK11btTransformS4_11RotateOrder(%class.btGeneric6DofSpring2Constraint* %0, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, i32 0)
  %5 = bitcast %class.btFixedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btFixedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4
  %6 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  call void @_ZN30btGeneric6DofSpring2Constraint20setAngularLowerLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %7 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  call void @_ZN30btGeneric6DofSpring2Constraint20setAngularUpperLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %8 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  store float 0.000000e+00, float* %ref.tmp12, align 4
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  call void @_ZN30btGeneric6DofSpring2Constraint19setLinearLowerLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %9 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  call void @_ZN30btGeneric6DofSpring2Constraint19setLinearUpperLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  ret %class.btFixedConstraint* %this1
}

declare %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintC2ER11btRigidBodyS1_RK11btTransformS4_11RotateOrder(%class.btGeneric6DofSpring2Constraint* returned, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), i32) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint20setAngularLowerLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %angularLower) #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %angularLower.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btVector3* %angularLower, %class.btVector3** %angularLower.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.btVector3*, %class.btVector3** %angularLower.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %call2 = call float @_Z16btNormalizeAnglef(float %3)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %4
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx3, i32 0, i32 0
  store float %call2, float* %m_loLimit, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint20setAngularUpperLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %angularUpper) #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %angularUpper.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btVector3* %angularUpper, %class.btVector3** %angularUpper.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.btVector3*, %class.btVector3** %angularUpper.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %call2 = call float @_Z16btNormalizeAnglef(float %3)
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %4
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx3, i32 0, i32 1
  store float %call2, float* %m_hiLimit, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint19setLinearLowerLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearLower) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %linearLower.addr = alloca %class.btVector3*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btVector3* %linearLower, %class.btVector3** %linearLower.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %linearLower.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_lowerLimit to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint19setLinearUpperLimitERK9btVector3(%class.btGeneric6DofSpring2Constraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearUpper) #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %linearUpper.addr = alloca %class.btVector3*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store %class.btVector3* %linearUpper, %class.btVector3** %linearUpper.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %linearUpper.addr, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_upperLimit to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btFixedConstraint* @_ZN17btFixedConstraintD2Ev(%class.btFixedConstraint* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btFixedConstraint*, align 4
  store %class.btFixedConstraint* %this, %class.btFixedConstraint** %this.addr, align 4
  %this1 = load %class.btFixedConstraint*, %class.btFixedConstraint** %this.addr, align 4
  %0 = bitcast %class.btFixedConstraint* %this1 to %class.btGeneric6DofSpring2Constraint*
  %call = call %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintD2Ev(%class.btGeneric6DofSpring2Constraint* %0) #5
  ret %class.btFixedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGeneric6DofSpring2Constraint* @_ZN30btGeneric6DofSpring2ConstraintD2Ev(%class.btGeneric6DofSpring2Constraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #5
  ret %class.btGeneric6DofSpring2Constraint* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN17btFixedConstraintD0Ev(%class.btFixedConstraint* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btFixedConstraint*, align 4
  store %class.btFixedConstraint* %this, %class.btFixedConstraint** %this.addr, align 4
  %this1 = load %class.btFixedConstraint*, %class.btFixedConstraint** %this.addr, align 4
  %call = call %class.btFixedConstraint* @_ZN17btFixedConstraintD1Ev(%class.btFixedConstraint* %this1) #5
  %0 = bitcast %class.btFixedConstraint* %this1 to i8*
  call void @_ZN30btGeneric6DofSpring2ConstraintdlEPv(i8* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2ConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGeneric6DofSpring2Constraint13buildJacobianEv(%class.btGeneric6DofSpring2Constraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

declare void @_ZN30btGeneric6DofSpring2Constraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo1"*) unnamed_addr #3

declare void @_ZN30btGeneric6DofSpring2Constraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofSpring2Constraint*, %"struct.btTypedConstraint::btConstraintInfo2"*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

declare void @_ZN30btGeneric6DofSpring2Constraint8setParamEifi(%class.btGeneric6DofSpring2Constraint*, i32, float, i32) unnamed_addr #3

declare float @_ZNK30btGeneric6DofSpring2Constraint8getParamEii(%class.btGeneric6DofSpring2Constraint*, i32, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK30btGeneric6DofSpring2Constraint28calculateSerializeBufferSizeEv(%class.btGeneric6DofSpring2Constraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  ret i32 644
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK30btGeneric6DofSpring2Constraint9serializeEPvP12btSerializer(%class.btGeneric6DofSpring2Constraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpring2Constraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dof = alloca %struct.btGeneric6DofSpring2ConstraintData*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpring2Constraint* %this, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btGeneric6DofSpring2Constraint*, %class.btGeneric6DofSpring2Constraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btGeneric6DofSpring2ConstraintData*
  store %struct.btGeneric6DofSpring2ConstraintData* %1, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %2 = bitcast %class.btGeneric6DofSpring2Constraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 1
  %6 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rbAFrame = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInA, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 2
  %7 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rbBFrame = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInB, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %8, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits, i32 0, i32 %9
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx, i32 0, i32 0
  %10 = load float, float* %m_loLimit, align 4
  %11 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularLowerLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %11, i32 0, i32 23
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %12
  store float %10, float* %arrayidx2, align 4
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits3, i32 0, i32 %13
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx4, i32 0, i32 1
  %14 = load float, float* %m_hiLimit, align 4
  %15 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularUpperLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %15, i32 0, i32 22
  %m_floats5 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit, i32 0, i32 0
  %16 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 %16
  store float %14, float* %arrayidx6, align 4
  %m_angularLimits7 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %17 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits7, i32 0, i32 %17
  %m_bounce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx8, i32 0, i32 2
  %18 = load float, float* %m_bounce, align 4
  %19 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularBounce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %19, i32 0, i32 24
  %m_floats9 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularBounce, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 %20
  store float %18, float* %arrayidx10, align 4
  %m_angularLimits11 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %21 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits11, i32 0, i32 %21
  %m_stopERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx12, i32 0, i32 3
  %22 = load float, float* %m_stopERP, align 4
  %23 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %23, i32 0, i32 25
  %m_floats13 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopERP, i32 0, i32 0
  %24 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 %24
  store float %22, float* %arrayidx14, align 4
  %m_angularLimits15 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %25 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits15, i32 0, i32 %25
  %m_stopCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx16, i32 0, i32 4
  %26 = load float, float* %m_stopCFM, align 4
  %27 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %27, i32 0, i32 26
  %m_floats17 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopCFM, i32 0, i32 0
  %28 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 %28
  store float %26, float* %arrayidx18, align 4
  %m_angularLimits19 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %29 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits19, i32 0, i32 %29
  %m_motorERP = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx20, i32 0, i32 5
  %30 = load float, float* %m_motorERP, align 4
  %31 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %31, i32 0, i32 27
  %m_floats21 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorERP, i32 0, i32 0
  %32 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 %32
  store float %30, float* %arrayidx22, align 4
  %m_angularLimits23 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %33 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits23, i32 0, i32 %33
  %m_motorCFM = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx24, i32 0, i32 6
  %34 = load float, float* %m_motorCFM, align 4
  %35 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %35, i32 0, i32 28
  %m_floats25 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorCFM, i32 0, i32 0
  %36 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds [4 x float], [4 x float]* %m_floats25, i32 0, i32 %36
  store float %34, float* %arrayidx26, align 4
  %m_angularLimits27 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %37 = load i32, i32* %i, align 4
  %arrayidx28 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits27, i32 0, i32 %37
  %m_targetVelocity = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx28, i32 0, i32 8
  %38 = load float, float* %m_targetVelocity, align 4
  %39 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularTargetVelocity = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %39, i32 0, i32 29
  %m_floats29 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularTargetVelocity, i32 0, i32 0
  %40 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 %40
  store float %38, float* %arrayidx30, align 4
  %m_angularLimits31 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %41 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits31, i32 0, i32 %41
  %m_maxMotorForce = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx32, i32 0, i32 9
  %42 = load float, float* %m_maxMotorForce, align 4
  %43 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMaxMotorForce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %43, i32 0, i32 30
  %m_floats33 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMaxMotorForce, i32 0, i32 0
  %44 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [4 x float], [4 x float]* %m_floats33, i32 0, i32 %44
  store float %42, float* %arrayidx34, align 4
  %m_angularLimits35 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %45 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits35, i32 0, i32 %45
  %m_servoTarget = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx36, i32 0, i32 11
  %46 = load float, float* %m_servoTarget, align 4
  %47 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoTarget = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %47, i32 0, i32 31
  %m_floats37 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularServoTarget, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds [4 x float], [4 x float]* %m_floats37, i32 0, i32 %48
  store float %46, float* %arrayidx38, align 4
  %m_angularLimits39 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %49 = load i32, i32* %i, align 4
  %arrayidx40 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits39, i32 0, i32 %49
  %m_springStiffness = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx40, i32 0, i32 13
  %50 = load float, float* %m_springStiffness, align 4
  %51 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffness = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %51, i32 0, i32 32
  %m_floats41 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringStiffness, i32 0, i32 0
  %52 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [4 x float], [4 x float]* %m_floats41, i32 0, i32 %52
  store float %50, float* %arrayidx42, align 4
  %m_angularLimits43 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %53 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits43, i32 0, i32 %53
  %m_springDamping = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx44, i32 0, i32 15
  %54 = load float, float* %m_springDamping, align 4
  %55 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDamping = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %55, i32 0, i32 33
  %m_floats45 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringDamping, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx46 = getelementptr inbounds [4 x float], [4 x float]* %m_floats45, i32 0, i32 %56
  store float %54, float* %arrayidx46, align 4
  %m_angularLimits47 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %57 = load i32, i32* %i, align 4
  %arrayidx48 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits47, i32 0, i32 %57
  %m_equilibriumPoint = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx48, i32 0, i32 17
  %58 = load float, float* %m_equilibriumPoint, align 4
  %59 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEquilibriumPoint = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %59, i32 0, i32 34
  %m_floats49 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularEquilibriumPoint, i32 0, i32 0
  %60 = load i32, i32* %i, align 4
  %arrayidx50 = getelementptr inbounds [4 x float], [4 x float]* %m_floats49, i32 0, i32 %60
  store float %58, float* %arrayidx50, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %61 = load i32, i32* %i, align 4
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %62 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularLowerLimit51 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %62, i32 0, i32 23
  %m_floats52 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit51, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [4 x float], [4 x float]* %m_floats52, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx53, align 4
  %63 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularUpperLimit54 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %63, i32 0, i32 22
  %m_floats55 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit54, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [4 x float], [4 x float]* %m_floats55, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx56, align 4
  %64 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularBounce57 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %64, i32 0, i32 24
  %m_floats58 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularBounce57, i32 0, i32 0
  %arrayidx59 = getelementptr inbounds [4 x float], [4 x float]* %m_floats58, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx59, align 4
  %65 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopERP60 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %65, i32 0, i32 25
  %m_floats61 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopERP60, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx62, align 4
  %66 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularStopCFM63 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %66, i32 0, i32 26
  %m_floats64 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularStopCFM63, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx65, align 4
  %67 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorERP66 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %67, i32 0, i32 27
  %m_floats67 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorERP66, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [4 x float], [4 x float]* %m_floats67, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx68, align 4
  %68 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMotorCFM69 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %68, i32 0, i32 28
  %m_floats70 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMotorCFM69, i32 0, i32 0
  %arrayidx71 = getelementptr inbounds [4 x float], [4 x float]* %m_floats70, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx71, align 4
  %69 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularTargetVelocity72 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %69, i32 0, i32 29
  %m_floats73 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularTargetVelocity72, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [4 x float], [4 x float]* %m_floats73, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx74, align 4
  %70 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularMaxMotorForce75 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %70, i32 0, i32 30
  %m_floats76 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularMaxMotorForce75, i32 0, i32 0
  %arrayidx77 = getelementptr inbounds [4 x float], [4 x float]* %m_floats76, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx77, align 4
  %71 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoTarget78 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %71, i32 0, i32 31
  %m_floats79 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularServoTarget78, i32 0, i32 0
  %arrayidx80 = getelementptr inbounds [4 x float], [4 x float]* %m_floats79, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx80, align 4
  %72 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffness81 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %72, i32 0, i32 32
  %m_floats82 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringStiffness81, i32 0, i32 0
  %arrayidx83 = getelementptr inbounds [4 x float], [4 x float]* %m_floats82, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx83, align 4
  %73 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDamping84 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %73, i32 0, i32 33
  %m_floats85 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularSpringDamping84, i32 0, i32 0
  %arrayidx86 = getelementptr inbounds [4 x float], [4 x float]* %m_floats85, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx86, align 4
  %74 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEquilibriumPoint87 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %74, i32 0, i32 34
  %m_floats88 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularEquilibriumPoint87, i32 0, i32 0
  %arrayidx89 = getelementptr inbounds [4 x float], [4 x float]* %m_floats88, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx89, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc142, %for.end
  %75 = load i32, i32* %i, align 4
  %cmp91 = icmp slt i32 %75, 4
  br i1 %cmp91, label %for.body92, label %for.end144

for.body92:                                       ; preds = %for.cond90
  %76 = load i32, i32* %i, align 4
  %cmp93 = icmp slt i32 %76, 3
  br i1 %cmp93, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body92
  %m_angularLimits94 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %77 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits94, i32 0, i32 %77
  %m_enableMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx95, i32 0, i32 7
  %78 = load i8, i8* %m_enableMotor, align 4
  %tobool = trunc i8 %78 to i1
  %79 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  br label %cond.end

cond.false:                                       ; preds = %for.body92
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond96 = phi i32 [ %cond, %cond.true ], [ 0, %cond.false ]
  %conv = trunc i32 %cond96 to i8
  %80 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEnableMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %80, i32 0, i32 35
  %81 = load i32, i32* %i, align 4
  %arrayidx97 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularEnableMotor, i32 0, i32 %81
  store i8 %conv, i8* %arrayidx97, align 1
  %82 = load i32, i32* %i, align 4
  %cmp98 = icmp slt i32 %82, 3
  br i1 %cmp98, label %cond.true99, label %cond.false104

cond.true99:                                      ; preds = %cond.end
  %m_angularLimits100 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %83 = load i32, i32* %i, align 4
  %arrayidx101 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits100, i32 0, i32 %83
  %m_servoMotor = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx101, i32 0, i32 10
  %84 = load i8, i8* %m_servoMotor, align 4
  %tobool102 = trunc i8 %84 to i1
  %85 = zext i1 %tobool102 to i64
  %cond103 = select i1 %tobool102, i32 1, i32 0
  br label %cond.end105

cond.false104:                                    ; preds = %cond.end
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false104, %cond.true99
  %cond106 = phi i32 [ %cond103, %cond.true99 ], [ 0, %cond.false104 ]
  %conv107 = trunc i32 %cond106 to i8
  %86 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularServoMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %86, i32 0, i32 36
  %87 = load i32, i32* %i, align 4
  %arrayidx108 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularServoMotor, i32 0, i32 %87
  store i8 %conv107, i8* %arrayidx108, align 1
  %88 = load i32, i32* %i, align 4
  %cmp109 = icmp slt i32 %88, 3
  br i1 %cmp109, label %cond.true110, label %cond.false115

cond.true110:                                     ; preds = %cond.end105
  %m_angularLimits111 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %89 = load i32, i32* %i, align 4
  %arrayidx112 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits111, i32 0, i32 %89
  %m_enableSpring = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx112, i32 0, i32 12
  %90 = load i8, i8* %m_enableSpring, align 4
  %tobool113 = trunc i8 %90 to i1
  %91 = zext i1 %tobool113 to i64
  %cond114 = select i1 %tobool113, i32 1, i32 0
  br label %cond.end116

cond.false115:                                    ; preds = %cond.end105
  br label %cond.end116

cond.end116:                                      ; preds = %cond.false115, %cond.true110
  %cond117 = phi i32 [ %cond114, %cond.true110 ], [ 0, %cond.false115 ]
  %conv118 = trunc i32 %cond117 to i8
  %92 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularEnableSpring = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %92, i32 0, i32 37
  %93 = load i32, i32* %i, align 4
  %arrayidx119 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularEnableSpring, i32 0, i32 %93
  store i8 %conv118, i8* %arrayidx119, align 1
  %94 = load i32, i32* %i, align 4
  %cmp120 = icmp slt i32 %94, 3
  br i1 %cmp120, label %cond.true121, label %cond.false126

cond.true121:                                     ; preds = %cond.end116
  %m_angularLimits122 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %95 = load i32, i32* %i, align 4
  %arrayidx123 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits122, i32 0, i32 %95
  %m_springStiffnessLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx123, i32 0, i32 14
  %96 = load i8, i8* %m_springStiffnessLimited, align 4
  %tobool124 = trunc i8 %96 to i1
  %97 = zext i1 %tobool124 to i64
  %cond125 = select i1 %tobool124, i32 1, i32 0
  br label %cond.end127

cond.false126:                                    ; preds = %cond.end116
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false126, %cond.true121
  %cond128 = phi i32 [ %cond125, %cond.true121 ], [ 0, %cond.false126 ]
  %conv129 = trunc i32 %cond128 to i8
  %98 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringStiffnessLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %98, i32 0, i32 38
  %99 = load i32, i32* %i, align 4
  %arrayidx130 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularSpringStiffnessLimited, i32 0, i32 %99
  store i8 %conv129, i8* %arrayidx130, align 1
  %100 = load i32, i32* %i, align 4
  %cmp131 = icmp slt i32 %100, 3
  br i1 %cmp131, label %cond.true132, label %cond.false137

cond.true132:                                     ; preds = %cond.end127
  %m_angularLimits133 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 6
  %101 = load i32, i32* %i, align 4
  %arrayidx134 = getelementptr inbounds [3 x %class.btRotationalLimitMotor2], [3 x %class.btRotationalLimitMotor2]* %m_angularLimits133, i32 0, i32 %101
  %m_springDampingLimited = getelementptr inbounds %class.btRotationalLimitMotor2, %class.btRotationalLimitMotor2* %arrayidx134, i32 0, i32 16
  %102 = load i8, i8* %m_springDampingLimited, align 4
  %tobool135 = trunc i8 %102 to i1
  %103 = zext i1 %tobool135 to i64
  %cond136 = select i1 %tobool135, i32 1, i32 0
  br label %cond.end138

cond.false137:                                    ; preds = %cond.end127
  br label %cond.end138

cond.end138:                                      ; preds = %cond.false137, %cond.true132
  %cond139 = phi i32 [ %cond136, %cond.true132 ], [ 0, %cond.false137 ]
  %conv140 = trunc i32 %cond139 to i8
  %104 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_angularSpringDampingLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %104, i32 0, i32 39
  %105 = load i32, i32* %i, align 4
  %arrayidx141 = getelementptr inbounds [4 x i8], [4 x i8]* %m_angularSpringDampingLimited, i32 0, i32 %105
  store i8 %conv140, i8* %arrayidx141, align 1
  br label %for.inc142

for.inc142:                                       ; preds = %cond.end138
  %106 = load i32, i32* %i, align 4
  %inc143 = add nsw i32 %106, 1
  store i32 %inc143, i32* %i, align 4
  br label %for.cond90

for.end144:                                       ; preds = %for.cond90
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits, i32 0, i32 0
  %107 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearLowerLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %107, i32 0, i32 4
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_lowerLimit, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearLowerLimit)
  %m_linearLimits145 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits145, i32 0, i32 1
  %108 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearUpperLimit = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %108, i32 0, i32 3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_upperLimit, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearUpperLimit)
  %m_linearLimits146 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_bounce147 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits146, i32 0, i32 2
  %109 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearBounce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %109, i32 0, i32 5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bounce147, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearBounce)
  %m_linearLimits148 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopERP149 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits148, i32 0, i32 3
  %110 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearStopERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %110, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_stopERP149, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearStopERP)
  %m_linearLimits150 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_stopCFM151 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits150, i32 0, i32 4
  %111 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearStopCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %111, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_stopCFM151, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearStopCFM)
  %m_linearLimits152 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorERP153 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits152, i32 0, i32 5
  %112 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMotorERP = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %112, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_motorERP153, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMotorERP)
  %m_linearLimits154 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_motorCFM155 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits154, i32 0, i32 6
  %113 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMotorCFM = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %113, i32 0, i32 9
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_motorCFM155, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMotorCFM)
  %m_linearLimits156 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_targetVelocity157 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits156, i32 0, i32 16
  %114 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearTargetVelocity = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %114, i32 0, i32 10
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_targetVelocity157, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearTargetVelocity)
  %m_linearLimits158 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_maxMotorForce159 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits158, i32 0, i32 17
  %115 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearMaxMotorForce = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %115, i32 0, i32 11
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_maxMotorForce159, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearMaxMotorForce)
  %m_linearLimits160 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoTarget161 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits160, i32 0, i32 10
  %116 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearServoTarget = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %116, i32 0, i32 12
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_servoTarget161, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearServoTarget)
  %m_linearLimits162 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffness163 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits162, i32 0, i32 11
  %117 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringStiffness = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %117, i32 0, i32 13
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_springStiffness163, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearSpringStiffness)
  %m_linearLimits164 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDamping165 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits164, i32 0, i32 13
  %118 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringDamping = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %118, i32 0, i32 14
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_springDamping165, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearSpringDamping)
  %m_linearLimits166 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_equilibriumPoint167 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits166, i32 0, i32 15
  %119 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEquilibriumPoint = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %119, i32 0, i32 15
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_equilibriumPoint167, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearEquilibriumPoint)
  store i32 0, i32* %i, align 4
  br label %for.cond168

for.cond168:                                      ; preds = %for.inc231, %for.end144
  %120 = load i32, i32* %i, align 4
  %cmp169 = icmp slt i32 %120, 4
  br i1 %cmp169, label %for.body170, label %for.end233

for.body170:                                      ; preds = %for.cond168
  %121 = load i32, i32* %i, align 4
  %cmp171 = icmp slt i32 %121, 3
  br i1 %cmp171, label %cond.true172, label %cond.false178

cond.true172:                                     ; preds = %for.body170
  %m_linearLimits173 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableMotor174 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits173, i32 0, i32 7
  %122 = load i32, i32* %i, align 4
  %arrayidx175 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor174, i32 0, i32 %122
  %123 = load i8, i8* %arrayidx175, align 1
  %tobool176 = trunc i8 %123 to i1
  %124 = zext i1 %tobool176 to i64
  %cond177 = select i1 %tobool176, i32 1, i32 0
  br label %cond.end179

cond.false178:                                    ; preds = %for.body170
  br label %cond.end179

cond.end179:                                      ; preds = %cond.false178, %cond.true172
  %cond180 = phi i32 [ %cond177, %cond.true172 ], [ 0, %cond.false178 ]
  %conv181 = trunc i32 %cond180 to i8
  %125 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEnableMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %125, i32 0, i32 16
  %126 = load i32, i32* %i, align 4
  %arrayidx182 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearEnableMotor, i32 0, i32 %126
  store i8 %conv181, i8* %arrayidx182, align 1
  %127 = load i32, i32* %i, align 4
  %cmp183 = icmp slt i32 %127, 3
  br i1 %cmp183, label %cond.true184, label %cond.false190

cond.true184:                                     ; preds = %cond.end179
  %m_linearLimits185 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_servoMotor186 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits185, i32 0, i32 8
  %128 = load i32, i32* %i, align 4
  %arrayidx187 = getelementptr inbounds [3 x i8], [3 x i8]* %m_servoMotor186, i32 0, i32 %128
  %129 = load i8, i8* %arrayidx187, align 1
  %tobool188 = trunc i8 %129 to i1
  %130 = zext i1 %tobool188 to i64
  %cond189 = select i1 %tobool188, i32 1, i32 0
  br label %cond.end191

cond.false190:                                    ; preds = %cond.end179
  br label %cond.end191

cond.end191:                                      ; preds = %cond.false190, %cond.true184
  %cond192 = phi i32 [ %cond189, %cond.true184 ], [ 0, %cond.false190 ]
  %conv193 = trunc i32 %cond192 to i8
  %131 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearServoMotor = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %131, i32 0, i32 17
  %132 = load i32, i32* %i, align 4
  %arrayidx194 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearServoMotor, i32 0, i32 %132
  store i8 %conv193, i8* %arrayidx194, align 1
  %133 = load i32, i32* %i, align 4
  %cmp195 = icmp slt i32 %133, 3
  br i1 %cmp195, label %cond.true196, label %cond.false202

cond.true196:                                     ; preds = %cond.end191
  %m_linearLimits197 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_enableSpring198 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits197, i32 0, i32 9
  %134 = load i32, i32* %i, align 4
  %arrayidx199 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableSpring198, i32 0, i32 %134
  %135 = load i8, i8* %arrayidx199, align 1
  %tobool200 = trunc i8 %135 to i1
  %136 = zext i1 %tobool200 to i64
  %cond201 = select i1 %tobool200, i32 1, i32 0
  br label %cond.end203

cond.false202:                                    ; preds = %cond.end191
  br label %cond.end203

cond.end203:                                      ; preds = %cond.false202, %cond.true196
  %cond204 = phi i32 [ %cond201, %cond.true196 ], [ 0, %cond.false202 ]
  %conv205 = trunc i32 %cond204 to i8
  %137 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearEnableSpring = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %137, i32 0, i32 18
  %138 = load i32, i32* %i, align 4
  %arrayidx206 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearEnableSpring, i32 0, i32 %138
  store i8 %conv205, i8* %arrayidx206, align 1
  %139 = load i32, i32* %i, align 4
  %cmp207 = icmp slt i32 %139, 3
  br i1 %cmp207, label %cond.true208, label %cond.false214

cond.true208:                                     ; preds = %cond.end203
  %m_linearLimits209 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springStiffnessLimited210 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits209, i32 0, i32 12
  %140 = load i32, i32* %i, align 4
  %arrayidx211 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springStiffnessLimited210, i32 0, i32 %140
  %141 = load i8, i8* %arrayidx211, align 1
  %tobool212 = trunc i8 %141 to i1
  %142 = zext i1 %tobool212 to i64
  %cond213 = select i1 %tobool212, i32 1, i32 0
  br label %cond.end215

cond.false214:                                    ; preds = %cond.end203
  br label %cond.end215

cond.end215:                                      ; preds = %cond.false214, %cond.true208
  %cond216 = phi i32 [ %cond213, %cond.true208 ], [ 0, %cond.false214 ]
  %conv217 = trunc i32 %cond216 to i8
  %143 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringStiffnessLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %143, i32 0, i32 19
  %144 = load i32, i32* %i, align 4
  %arrayidx218 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearSpringStiffnessLimited, i32 0, i32 %144
  store i8 %conv217, i8* %arrayidx218, align 1
  %145 = load i32, i32* %i, align 4
  %cmp219 = icmp slt i32 %145, 3
  br i1 %cmp219, label %cond.true220, label %cond.false226

cond.true220:                                     ; preds = %cond.end215
  %m_linearLimits221 = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 5
  %m_springDampingLimited222 = getelementptr inbounds %class.btTranslationalLimitMotor2, %class.btTranslationalLimitMotor2* %m_linearLimits221, i32 0, i32 14
  %146 = load i32, i32* %i, align 4
  %arrayidx223 = getelementptr inbounds [3 x i8], [3 x i8]* %m_springDampingLimited222, i32 0, i32 %146
  %147 = load i8, i8* %arrayidx223, align 1
  %tobool224 = trunc i8 %147 to i1
  %148 = zext i1 %tobool224 to i64
  %cond225 = select i1 %tobool224, i32 1, i32 0
  br label %cond.end227

cond.false226:                                    ; preds = %cond.end215
  br label %cond.end227

cond.end227:                                      ; preds = %cond.false226, %cond.true220
  %cond228 = phi i32 [ %cond225, %cond.true220 ], [ 0, %cond.false226 ]
  %conv229 = trunc i32 %cond228 to i8
  %149 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_linearSpringDampingLimited = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %149, i32 0, i32 20
  %150 = load i32, i32* %i, align 4
  %arrayidx230 = getelementptr inbounds [4 x i8], [4 x i8]* %m_linearSpringDampingLimited, i32 0, i32 %150
  store i8 %conv229, i8* %arrayidx230, align 1
  br label %for.inc231

for.inc231:                                       ; preds = %cond.end227
  %151 = load i32, i32* %i, align 4
  %inc232 = add nsw i32 %151, 1
  store i32 %inc232, i32* %i, align 4
  br label %for.cond168

for.end233:                                       ; preds = %for.cond168
  %m_rotateOrder = getelementptr inbounds %class.btGeneric6DofSpring2Constraint, %class.btGeneric6DofSpring2Constraint* %this1, i32 0, i32 7
  %152 = load i32, i32* %m_rotateOrder, align 4
  %153 = load %struct.btGeneric6DofSpring2ConstraintData*, %struct.btGeneric6DofSpring2ConstraintData** %dof, align 4
  %m_rotateOrder234 = getelementptr inbounds %struct.btGeneric6DofSpring2ConstraintData, %struct.btGeneric6DofSpring2ConstraintData* %153, i32 0, i32 40
  store i32 %152, i32* %m_rotateOrder234, align 4
  ret i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  %0 = load float, float* %angleInRadians.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4
  %1 = load float, float* %angleInRadians.addr, align 4
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %fmod = frem float %0, %1
  ret float %fmod
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btFixedConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
