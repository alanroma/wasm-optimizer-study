; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSoftBodyRigidBodyCollisionConfiguration = type { %class.btDefaultCollisionConfiguration, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btDefaultCollisionConfiguration = type { %class.btCollisionConfiguration, i32, %class.btPoolAllocator*, i8, %class.btPoolAllocator*, i8, %class.btConvexPenetrationDepthSolver*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btCollisionConfiguration = type { i32 (...)** }
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8*, %class.btSpinMutex }
%class.btSpinMutex = type { i32 }
%class.btConvexPenetrationDepthSolver = type opaque
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDefaultCollisionConstructionInfo = type { %class.btPoolAllocator*, %class.btPoolAllocator*, i32, i32, i32, i32 }
%"struct.btSoftSoftCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%"struct.btSoftRigidCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btSoftSoftCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btSoftRigidCollisionAlgorithm = type <{ %class.btCollisionAlgorithm, i8, [3 x i8] }>
%class.btSoftBodyConcaveCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btSoftBodyTriangleCallback }
%class.btSoftBodyTriangleCallback = type { %class.btTriangleCallback, %class.btSoftBody*, %class.btCollisionObject*, %class.btVector3, %class.btVector3, %class.btManifoldResult*, %class.btDispatcher*, %struct.btDispatcherInfo*, float, %class.btHashMap, i32 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btSoftBody = type opaque
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btHashMap = type { %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.10 }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %struct.btTriIndex*, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }
%struct.btTriIndex = type { i32, %class.btCollisionShape* }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %class.btHashKey*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btHashKey = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZNK15btPoolAllocator14getElementSizeEv = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN15btPoolAllocatorD2Ev = comdat any

$_ZN15btPoolAllocatorC2Eii = comdat any

$_ZN17btBroadphaseProxy8isConvexEi = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN11btSpinMutexC2Ev = comdat any

$_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

$_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV41btSoftBodyRigidBodyCollisionConfiguration = hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI41btSoftBodyRigidBodyCollisionConfiguration to i8*), i8* bitcast (%class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev to i8*), i8* bitcast (void (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btSoftBodyRigidBodyCollisionConfiguration*, i32, i32)* @_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btDefaultCollisionConfiguration*, i32, i32)* @_ZN31btDefaultCollisionConfiguration35getClosestPointsAlgorithmCreateFuncEii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS41btSoftBodyRigidBodyCollisionConfiguration = hidden constant [44 x i8] c"41btSoftBodyRigidBodyCollisionConfiguration\00", align 1
@_ZTI31btDefaultCollisionConfiguration = external constant i8*
@_ZTI41btSoftBodyRigidBodyCollisionConfiguration = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([44 x i8], [44 x i8]* @_ZTS41btSoftBodyRigidBodyCollisionConfiguration, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btDefaultCollisionConfiguration to i8*) }, align 4
@_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*)* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*)* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [45 x i8] c"N28btSoftSoftCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([45 x i8], [45 x i8]* @_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4
@_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*)* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*)* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [46 x i8] c"N29btSoftRigidCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [52 x i8] c"N35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([52 x i8], [52 x i8]* @_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [59 x i8] c"N35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([59 x i8], [59 x i8]* @_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSoftBodyRigidBodyCollisionConfiguration.cpp, i8* null }]

@_ZN41btSoftBodyRigidBodyCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = hidden unnamed_addr alias %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*), %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
@_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev = hidden unnamed_addr alias %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*), %class.btSoftBodyRigidBodyCollisionConfiguration* (%class.btSoftBodyRigidBodyCollisionConfiguration*)* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btSoftBodyRigidBodyCollisionConfiguration* returned %this, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %constructionInfo) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %constructionInfo.addr = alloca %struct.btDefaultCollisionConstructionInfo*, align 4
  %mem = alloca i8*, align 4
  %curElemSize = alloca i32, align 4
  %maxSize0 = alloca i32, align 4
  %maxSize1 = alloca i32, align 4
  %maxSize2 = alloca i32, align 4
  %collisionAlgorithmMaxElementSize = alloca i32, align 4
  %mem24 = alloca i8*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  store %struct.btDefaultCollisionConstructionInfo* %constructionInfo, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, %class.btSoftBodyRigidBodyCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %1 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %call = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* %0, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %1)
  %2 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV41btSoftBodyRigidBodyCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call2, i8** %mem, align 4
  %3 = load i8*, i8** %mem, align 4
  %4 = bitcast i8* %3 to %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*
  %call3 = call %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %4)
  %5 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %4 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call4, i8** %mem, align 4
  %6 = load i8*, i8** %mem, align 4
  %7 = bitcast i8* %6 to %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*
  %call5 = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %7)
  %8 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %7 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call6, i8** %mem, align 4
  %9 = load i8*, i8** %mem, align 4
  %10 = bitcast i8* %9 to %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*
  %call7 = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %10)
  %11 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %10 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4
  %m_swappedSoftRigidConvexCreateFunc8 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %12 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc8, align 4
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %12, i32 0, i32 1
  store i8 1, i8* %m_swapped, align 4
  %call9 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call9, i8** %mem, align 4
  %13 = load i8*, i8** %mem, align 4
  %14 = bitcast i8* %13 to %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*
  %call10 = call %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %14)
  %15 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %14 to %struct.btCollisionAlgorithmCreateFunc*
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  store %struct.btCollisionAlgorithmCreateFunc* %15, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4
  %call11 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call11, i8** %mem, align 4
  %16 = load i8*, i8** %mem, align 4
  %17 = bitcast i8* %16 to %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*
  %call12 = call %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %17)
  %18 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %17 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  store %struct.btCollisionAlgorithmCreateFunc* %18, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4
  %m_swappedSoftRigidConcaveCreateFunc13 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %19 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc13, align 4
  %m_swapped14 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %19, i32 0, i32 1
  store i8 1, i8* %m_swapped14, align 4
  %20 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %20, i32 0, i32 5
  %21 = load i8, i8* %m_ownsCollisionAlgorithmPool, align 4
  %tobool = trunc i8 %21 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end28

land.lhs.true:                                    ; preds = %entry
  %22 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %22, i32 0, i32 4
  %23 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4
  %tobool15 = icmp ne %class.btPoolAllocator* %23, null
  br i1 %tobool15, label %if.then, label %if.end28

if.then:                                          ; preds = %land.lhs.true
  %24 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool16 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %24, i32 0, i32 4
  %25 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool16, align 4
  %call17 = call i32 @_ZNK15btPoolAllocator14getElementSizeEv(%class.btPoolAllocator* %25)
  store i32 %call17, i32* %curElemSize, align 4
  store i32 16, i32* %maxSize0, align 4
  store i32 12, i32* %maxSize1, align 4
  store i32 156, i32* %maxSize2, align 4
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %maxSize0, i32* nonnull align 4 dereferenceable(4) %maxSize1)
  %26 = load i32, i32* %call18, align 4
  store i32 %26, i32* %collisionAlgorithmMaxElementSize, align 4
  %call19 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize2)
  %27 = load i32, i32* %call19, align 4
  store i32 %27, i32* %collisionAlgorithmMaxElementSize, align 4
  %28 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4
  %29 = load i32, i32* %curElemSize, align 4
  %cmp = icmp sgt i32 %28, %29
  br i1 %cmp, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then
  %30 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool21 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %30, i32 0, i32 4
  %31 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool21, align 4
  %call22 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %31) #6
  %32 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool23 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %32, i32 0, i32 4
  %33 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool23, align 4
  %34 = bitcast %class.btPoolAllocator* %33 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %34)
  %call25 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  store i8* %call25, i8** %mem24, align 4
  %35 = load i8*, i8** %mem24, align 4
  %36 = bitcast i8* %35 to %class.btPoolAllocator*
  %37 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4
  %38 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_defaultMaxCollisionAlgorithmPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %38, i32 0, i32 3
  %39 = load i32, i32* %m_defaultMaxCollisionAlgorithmPoolSize, align 4
  %call26 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %36, i32 %37, i32 %39)
  %40 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %m_collisionAlgorithmPool27 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %40, i32 0, i32 4
  store %class.btPoolAllocator* %36, %class.btPoolAllocator** %m_collisionAlgorithmPool27, align 4
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then
  br label %if.end28

if.end28:                                         ; preds = %if.end, %land.lhs.true, %entry
  %41 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %retval, align 4
  ret %class.btSoftBodyRigidBodyCollisionConfiguration* %41
}

declare %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* returned, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24)) unnamed_addr #3

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btPoolAllocator14getElementSizeEv(%class.btPoolAllocator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_elemSize, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_pool, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret %class.btPoolAllocator* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* returned %this, i32 %elemSize, i32 %maxElements) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btPoolAllocator*, align 4
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %elemSize.addr = alloca i32, align 4
  %maxElements.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %count = alloca i32, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  store i32 %elemSize, i32* %elemSize.addr, align 4
  store i32 %maxElements, i32* %maxElements.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  store %class.btPoolAllocator* %this1, %class.btPoolAllocator** %retval, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %elemSize.addr, align 4
  store i32 %0, i32* %m_elemSize, align 4
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %1 = load i32, i32* %maxElements.addr, align 4
  store i32 %1, i32* %m_maxElements, align 4
  %m_mutex = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  %call = call %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* %m_mutex)
  %m_elemSize2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_elemSize2, align 4
  %m_maxElements3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_maxElements3, align 4
  %mul = mul nsw i32 %2, %3
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  store i8* %call4, i8** %m_pool, align 4
  %m_pool5 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_pool5, align 4
  store i8* %4, i8** %p, align 4
  %5 = load i8*, i8** %p, align 4
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %5, i8** %m_firstFree, align 4
  %m_maxElements6 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_maxElements6, align 4
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  store i32 %6, i32* %m_freeCount, align 4
  %m_maxElements7 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_maxElements7, align 4
  store i32 %7, i32* %count, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %count, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %count, align 4
  %tobool = icmp ne i32 %dec, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8*, i8** %p, align 4
  %m_elemSize8 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %10 = load i32, i32* %m_elemSize8, align 4
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8*, i8** %p, align 4
  %12 = bitcast i8* %11 to i8**
  store i8* %add.ptr, i8** %12, align 4
  %m_elemSize9 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %13 = load i32, i32* %m_elemSize9, align 4
  %14 = load i8*, i8** %p, align 4
  %add.ptr10 = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr10, i8** %p, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load i8*, i8** %p, align 4
  %16 = bitcast i8* %15 to i8**
  store i8* null, i8** %16, align 4
  %17 = load %class.btPoolAllocator*, %class.btPoolAllocator** %retval, align 4
  ret %class.btPoolAllocator* %17
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV41btSoftBodyRigidBodyCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4
  %2 = bitcast %struct.btCollisionAlgorithmCreateFunc* %1 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %2, align 4
  %vfn = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable, i64 0
  %3 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* %3(%struct.btCollisionAlgorithmCreateFunc* %1) #6
  %m_softSoftCreateFunc2 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %4 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc2, align 4
  %5 = bitcast %struct.btCollisionAlgorithmCreateFunc* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4
  %7 = bitcast %struct.btCollisionAlgorithmCreateFunc* %6 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable3 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %7, align 4
  %vfn4 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable3, i64 0
  %8 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn4, align 4
  %call5 = call %struct.btCollisionAlgorithmCreateFunc* %8(%struct.btCollisionAlgorithmCreateFunc* %6) #6
  %m_softRigidConvexCreateFunc6 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %9 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc6, align 4
  %10 = bitcast %struct.btCollisionAlgorithmCreateFunc* %9 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %10)
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4
  %12 = bitcast %struct.btCollisionAlgorithmCreateFunc* %11 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable7 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %12, align 4
  %vfn8 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable7, i64 0
  %13 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn8, align 4
  %call9 = call %struct.btCollisionAlgorithmCreateFunc* %13(%struct.btCollisionAlgorithmCreateFunc* %11) #6
  %m_swappedSoftRigidConvexCreateFunc10 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc10, align 4
  %15 = bitcast %struct.btCollisionAlgorithmCreateFunc* %14 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %15)
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %16 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4
  %17 = bitcast %struct.btCollisionAlgorithmCreateFunc* %16 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable11 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %17, align 4
  %vfn12 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable11, i64 0
  %18 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn12, align 4
  %call13 = call %struct.btCollisionAlgorithmCreateFunc* %18(%struct.btCollisionAlgorithmCreateFunc* %16) #6
  %m_softRigidConcaveCreateFunc14 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %19 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc14, align 4
  %20 = bitcast %struct.btCollisionAlgorithmCreateFunc* %19 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %20)
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %21 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4
  %22 = bitcast %struct.btCollisionAlgorithmCreateFunc* %21 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable15 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %22, align 4
  %vfn16 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable15, i64 0
  %23 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn16, align 4
  %call17 = call %struct.btCollisionAlgorithmCreateFunc* %23(%struct.btCollisionAlgorithmCreateFunc* %21) #6
  %m_swappedSoftRigidConcaveCreateFunc18 = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %24 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc18, align 4
  %25 = bitcast %struct.btCollisionAlgorithmCreateFunc* %24 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %25)
  %26 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %call19 = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* %26) #6
  ret %class.btSoftBodyRigidBodyCollisionConfiguration* %this1
}

; Function Attrs: nounwind
declare %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %call = call %class.btSoftBodyRigidBodyCollisionConfiguration* @_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev(%class.btSoftBodyRigidBodyCollisionConfiguration* %this1) #6
  %0 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btSoftBodyRigidBodyCollisionConfiguration* %this, i32 %proxyType0, i32 %proxyType1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %this.addr = alloca %class.btSoftBodyRigidBodyCollisionConfiguration*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  store %class.btSoftBodyRigidBodyCollisionConfiguration* %this, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  store i32 %proxyType0, i32* %proxyType0.addr, align 4
  store i32 %proxyType1, i32* %proxyType1.addr, align 4
  %this1 = load %class.btSoftBodyRigidBodyCollisionConfiguration*, %class.btSoftBodyRigidBodyCollisionConfiguration** %this.addr, align 4
  %0 = load i32, i32* %proxyType0.addr, align 4
  %cmp = icmp eq i32 %0, 32
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %proxyType1.addr, align 4
  %cmp2 = icmp eq i32 %1, 32
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_softSoftCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softSoftCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %2, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %proxyType0.addr, align 4
  %cmp3 = icmp eq i32 %3, 32
  br i1 %cmp3, label %land.lhs.true4, label %if.end6

land.lhs.true4:                                   ; preds = %if.end
  %4 = load i32, i32* %proxyType1.addr, align 4
  %call = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %4)
  br i1 %call, label %if.then5, label %if.end6

if.then5:                                         ; preds = %land.lhs.true4
  %m_softRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 2
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConvexCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end6:                                          ; preds = %land.lhs.true4, %if.end
  %6 = load i32, i32* %proxyType0.addr, align 4
  %call7 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %6)
  br i1 %call7, label %land.lhs.true8, label %if.end11

land.lhs.true8:                                   ; preds = %if.end6
  %7 = load i32, i32* %proxyType1.addr, align 4
  %cmp9 = icmp eq i32 %7, 32
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %land.lhs.true8
  %m_swappedSoftRigidConvexCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 3
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConvexCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end11:                                         ; preds = %land.lhs.true8, %if.end6
  %9 = load i32, i32* %proxyType0.addr, align 4
  %cmp12 = icmp eq i32 %9, 32
  br i1 %cmp12, label %land.lhs.true13, label %if.end16

land.lhs.true13:                                  ; preds = %if.end11
  %10 = load i32, i32* %proxyType1.addr, align 4
  %call14 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %10)
  br i1 %call14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true13
  %m_softRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 4
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_softRigidConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end16:                                         ; preds = %land.lhs.true13, %if.end11
  %12 = load i32, i32* %proxyType0.addr, align 4
  %call17 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %12)
  br i1 %call17, label %land.lhs.true18, label %if.end21

land.lhs.true18:                                  ; preds = %if.end16
  %13 = load i32, i32* %proxyType1.addr, align 4
  %cmp19 = icmp eq i32 %13, 32
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %m_swappedSoftRigidConcaveCreateFunc = getelementptr inbounds %class.btSoftBodyRigidBodyCollisionConfiguration, %class.btSoftBodyRigidBodyCollisionConfiguration* %this1, i32 0, i32 5
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedSoftRigidConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %14, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true18, %if.end16
  %15 = bitcast %class.btSoftBodyRigidBodyCollisionConfiguration* %this1 to %class.btDefaultCollisionConfiguration*
  %16 = load i32, i32* %proxyType0.addr, align 4
  %17 = load i32, i32* %proxyType1.addr, align 4
  %call22 = call %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration* %15, i32 %16, i32 %17)
  store %struct.btCollisionAlgorithmCreateFunc* %call22, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20, %if.then15, %if.then10, %if.then5, %if.then
  %18 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp slt i32 %0, 20
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

declare %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration*, i32, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4
  ret %class.btPoolAllocator* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4
  ret %class.btPoolAllocator* %0
}

declare %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration35getClosestPointsAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration*, i32, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %bbsize = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"*, %"struct.btSoftSoftCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store i32 16, i32* %bbsize, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = load i32, i32* %bbsize, align 4
  %3 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %1, i32 %2)
  store i8* %call, i8** %ptr, align 4
  %5 = load i8*, i8** %ptr, align 4
  %6 = bitcast i8* %5 to %class.btSoftSoftCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSoftSoftCollisionAlgorithm* %6, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btSoftSoftCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #6
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

declare %class.btSoftSoftCollisionAlgorithm* @_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSoftSoftCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btCollisionAlgorithm*, align 4
  %this.addr = alloca %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"*, %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 12)
  store i8* %call, i8** %mem, align 4
  %4 = bitcast %"struct.btSoftRigidCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %4, i32 0, i32 1
  %5 = load i8, i8* %m_swapped, align 4
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %6 = load i8*, i8** %mem, align 4
  %7 = bitcast i8* %6 to %class.btSoftRigidCollisionAlgorithm*
  %8 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* %7, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %8, %struct.btCollisionObjectWrapper* %9, %struct.btCollisionObjectWrapper* %10, i1 zeroext false)
  %11 = bitcast %class.btSoftRigidCollisionAlgorithm* %7 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %11, %class.btCollisionAlgorithm** %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %12 = load i8*, i8** %mem, align 4
  %13 = bitcast i8* %12 to %class.btSoftRigidCollisionAlgorithm*
  %14 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call3 = call %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* %13, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %14, %struct.btCollisionObjectWrapper* %15, %struct.btCollisionObjectWrapper* %16, i1 zeroext true)
  %17 = bitcast %class.btSoftRigidCollisionAlgorithm* %13 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %17, %class.btCollisionAlgorithm** %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %18 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %retval, align 4
  ret %class.btCollisionAlgorithm* %18
}

declare %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 156)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btSoftBodyConcaveCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext false)
  %9 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

declare %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btSoftBodyConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 156)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btSoftBodyConcaveCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext true)
  %9 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %this, %class.btSpinMutex** %this.addr, align 4
  %this1 = load %class.btSpinMutex*, %class.btSpinMutex** %this.addr, align 4
  %mLock = getelementptr inbounds %class.btSpinMutex, %class.btSpinMutex* %this1, i32 0, i32 0
  store i32 0, i32* %mLock, align 4
  ret %class.btSpinMutex* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSoftBodyRigidBodyCollisionConfiguration.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
