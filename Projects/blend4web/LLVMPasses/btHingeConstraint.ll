; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btHingeConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btHingeConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btHingeConstraint = type { %class.btTypedConstraint, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTransform, %class.btTransform, float, float, %class.btAngularLimit, float, float, float, float, i8, i8, i8, i8, i8, float, i32, float, float, float, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAngularLimit = type <{ float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btHingeAccumulatedAngleConstraint = type { %class.btHingeConstraint, float }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%class.btAlignedObjectArray.5 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btHingeConstraintFloatData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, i32, i32, i32, float, float, float, float, float, float, float }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN14btAngularLimitC2Ev = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_Z15shortestArcQuatRK9btVector3S1_ = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody22getInvInertiaDiagLocalEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f = comdat any

$_ZN17btHingeConstraint13getRigidBodyAEv = comdat any

$_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_ = comdat any

$_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3 = comdat any

$_ZN17btHingeConstraint13getRigidBodyBEv = comdat any

$_ZN17btHingeConstraint13getSolveLimitEv = comdat any

$_ZN17btHingeConstraint21getEnableAngularMotorEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK14btAngularLimit13getCorrectionEv = comdat any

$_ZNK17btHingeConstraint13getLowerLimitEv = comdat any

$_ZNK17btHingeConstraint13getUpperLimitEv = comdat any

$_ZNK14btAngularLimit19getRelaxationFactorEv = comdat any

$_ZNK14btAngularLimit13getBiasFactorEv = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZNK12btQuaternionngEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN17btHingeConstraintD2Ev = comdat any

$_ZN17btHingeConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK17btHingeConstraint9serializeEPvP12btSerializer = comdat any

$_ZNK17btHingeConstraint8getFlagsEv = comdat any

$_ZN33btHingeAccumulatedAngleConstraintD2Ev = comdat any

$_ZN33btHingeAccumulatedAngleConstraintD0Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_Z6btFabsf = comdat any

$_Z16btNormalizeAnglef = comdat any

$_Z6btFmodff = comdat any

$_ZNK14btAngularLimit7isLimitEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z6btAcosf = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN17btHingeConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK14btAngularLimit11getSoftnessEv = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZN33btHingeAccumulatedAngleConstraintdlEPv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV17btHingeConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btHingeConstraint to i8*), i8* bitcast (%class.btHingeConstraint* (%class.btHingeConstraint*)* @_ZN17btHingeConstraintD2Ev to i8*), i8* bitcast (void (%class.btHingeConstraint*)* @_ZN17btHingeConstraintD0Ev to i8*), i8* bitcast (void (%class.btHingeConstraint*)* @_ZN17btHingeConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btHingeConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btHingeConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btHingeConstraint*, i32, float, i32)* @_ZN17btHingeConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btHingeConstraint*, i32, i32)* @_ZNK17btHingeConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btHingeConstraint*)* @_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btHingeConstraint*, i8*, %class.btSerializer*)* @_ZNK17btHingeConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (i32 (%class.btHingeConstraint*)* @_ZNK17btHingeConstraint8getFlagsEv to i8*)] }, align 4
@_ZL6vHinge = internal global %class.btVector3 zeroinitializer, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btHingeConstraint = hidden constant [20 x i8] c"17btHingeConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI17btHingeConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btHingeConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV33btHingeAccumulatedAngleConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI33btHingeAccumulatedAngleConstraint to i8*), i8* bitcast (%class.btHingeAccumulatedAngleConstraint* (%class.btHingeAccumulatedAngleConstraint*)* @_ZN33btHingeAccumulatedAngleConstraintD2Ev to i8*), i8* bitcast (void (%class.btHingeAccumulatedAngleConstraint*)* @_ZN33btHingeAccumulatedAngleConstraintD0Ev to i8*), i8* bitcast (void (%class.btHingeConstraint*)* @_ZN17btHingeConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btHingeAccumulatedAngleConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN33btHingeAccumulatedAngleConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btHingeConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btHingeConstraint*, i32, float, i32)* @_ZN17btHingeConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btHingeConstraint*, i32, i32)* @_ZNK17btHingeConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btHingeConstraint*)* @_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btHingeConstraint*, i8*, %class.btSerializer*)* @_ZNK17btHingeConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (i32 (%class.btHingeConstraint*)* @_ZNK17btHingeConstraint8getFlagsEv to i8*)] }, align 4
@_ZTS33btHingeAccumulatedAngleConstraint = hidden constant [36 x i8] c"33btHingeAccumulatedAngleConstraint\00", align 1
@_ZTI33btHingeAccumulatedAngleConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([36 x i8], [36 x i8]* @_ZTS33btHingeAccumulatedAngleConstraint, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btHingeConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [27 x i8] c"btHingeConstraintFloatData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btHingeConstraint.cpp, i8* null }]

@_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyRK9btVector3S4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3S4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
@_ZN17btHingeConstraintC1ER11btRigidBodyRK11btTransformb = hidden unnamed_addr alias %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btTransform*, i1), %class.btHingeConstraint* (%class.btHingeConstraint*, %class.btRigidBody*, %class.btTransform*, i1)* @_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK9btVector3S4_S4_S4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInA, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInB, i1 zeroext %useReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  %axisInA.addr = alloca %class.btVector3*, align 4
  %axisInB.addr = alloca %class.btVector3*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %rbAxisA1 = alloca %class.btVector3, align 4
  %rbAxisA2 = alloca %class.btVector3, align 4
  %projection = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %rbAxisB1 = alloca %class.btVector3, align 4
  %rbAxisB2 = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  store %class.btVector3* %axisInA, %class.btVector3** %axisInA.addr, align 4
  store %class.btVector3* %axisInB, %class.btVector3** %axisInB.addr, align 4
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2)
  %3 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call11 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbAFrame)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %4 = load i8, i8* %useReferenceFrameA.addr, align 1
  %tobool = trunc i8 %4 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_normalCFM, align 4
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_normalERP, align 4
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_stopCFM, align 4
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  store float 0.000000e+00, float* %m_stopERP, align 4
  %5 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4
  %m_rbAFrame15 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame15)
  %6 = bitcast %class.btVector3* %call16 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  %call18 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call17)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %rbAxisA1, %class.btMatrix3x3* %call18, i32 0)
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA2)
  %9 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call20 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  store float %call20, float* %projection, align 4
  %10 = load float, float* %projection, align 4
  %cmp = fcmp oge float %10, 0x3FEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %arrayctor.cont10
  %11 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %11)
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call22)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp21, %class.btMatrix3x3* %call23, i32 2)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %12 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call25)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp24, %class.btMatrix3x3* %call26, i32 1)
  %15 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end38

if.else:                                          ; preds = %arrayctor.cont10
  %17 = load float, float* %projection, align 4
  %cmp27 = fcmp ole float %17, 0xBFEFFFFFC0000000
  br i1 %cmp27, label %if.then28, label %if.else35

if.then28:                                        ; preds = %if.else
  %18 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %18)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp29, %class.btMatrix3x3* %call31, i32 2)
  %19 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call33 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %21)
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call33)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp32, %class.btMatrix3x3* %call34, i32 1)
  %22 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  br label %if.end

if.else35:                                        ; preds = %if.else
  %24 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  %25 = bitcast %class.btVector3* %rbAxisA2 to i8*
  %26 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %27 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* %rbAxisA2, %class.btVector3* nonnull align 4 dereferenceable(16) %27)
  %28 = bitcast %class.btVector3* %rbAxisA1 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else35, %if.then28
  br label %if.end38

if.end38:                                         ; preds = %if.end, %if.then
  %m_rbAFrame39 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call40 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame39)
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA1)
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA2)
  %30 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %30)
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA1)
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA2)
  %31 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %31)
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA1)
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA2)
  %32 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %32)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call40, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43, float* nonnull align 4 dereferenceable(4) %call44, float* nonnull align 4 dereferenceable(4) %call45, float* nonnull align 4 dereferenceable(4) %call46, float* nonnull align 4 dereferenceable(4) %call47, float* nonnull align 4 dereferenceable(4) %call48, float* nonnull align 4 dereferenceable(4) %call49)
  %33 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %34 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %33, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %rbAxisB1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  %35 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %rbAxisB2, %class.btVector3* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisB1)
  %36 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4
  %m_rbBFrame50 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame50)
  %37 = bitcast %class.btVector3* %call51 to i8*
  %38 = bitcast %class.btVector3* %36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false)
  %m_rbBFrame52 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call53 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame52)
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB1)
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB2)
  %39 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %39)
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB1)
  %call58 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB2)
  %40 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4
  %call59 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %40)
  %call60 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB1)
  %call61 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB2)
  %41 = load %class.btVector3*, %class.btVector3** %axisInB.addr, align 4
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %41)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call53, float* nonnull align 4 dereferenceable(4) %call54, float* nonnull align 4 dereferenceable(4) %call55, float* nonnull align 4 dereferenceable(4) %call56, float* nonnull align 4 dereferenceable(4) %call57, float* nonnull align 4 dereferenceable(4) %call58, float* nonnull align 4 dereferenceable(4) %call59, float* nonnull align 4 dereferenceable(4) %call60, float* nonnull align 4 dereferenceable(4) %call61, float* nonnull align 4 dereferenceable(4) %call62)
  %m_useReferenceFrameA63 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %42 = load i8, i8* %m_useReferenceFrameA63, align 4
  %tobool64 = trunc i8 %42 to i1
  %43 = zext i1 %tobool64 to i64
  %cond = select i1 %tobool64, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4
  %44 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %44
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  store float 0.000000e+00, float* %m_center, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  store float -1.000000e+00, float* %m_halfRange, align 4
  %m_softness = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 2
  store float 0x3FECCCCCC0000000, float* %m_softness, align 4
  %m_biasFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 3
  store float 0x3FD3333340000000, float* %m_biasFactor, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 4
  store float 1.000000e+00, float* %m_relaxationFactor, align 4
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_correction, align 4
  %m_sign = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_sign, align 4
  %m_solveLimit = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  store i8 0, i8* %m_solveLimit, align 4
  ret %class.btAngularLimit* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %unused = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %s = alloca float, align 4
  %rs = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call, float* %d, align 4
  %4 = load float, float* %d, align 4
  %conv = fpext float %4 to double
  %cmp = fcmp olt double %conv, 0xBFEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %unused)
  %5 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %unused)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %n)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %n)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %n)
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %return

if.end:                                           ; preds = %entry
  %6 = load float, float* %d, align 4
  %add = fadd float 1.000000e+00, %6
  %mul = fmul float %add, 2.000000e+00
  %call7 = call float @_Z6btSqrtf(float %mul)
  store float %call7, float* %s, align 4
  %7 = load float, float* %s, align 4
  %div = fdiv float 1.000000e+00, %7
  store float %div, float* %rs, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %c)
  %8 = load float, float* %call9, align 4
  %9 = load float, float* %rs, align 4
  %mul10 = fmul float %8, %9
  store float %mul10, float* %ref.tmp8, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %c)
  %10 = load float, float* %call12, align 4
  %11 = load float, float* %rs, align 4
  %mul13 = fmul float %10, %11
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %c)
  %12 = load float, float* %call15, align 4
  %13 = load float, float* %rs, align 4
  %mul16 = fmul float %12, %13
  store float %mul16, float* %ref.tmp14, align 4
  %14 = load float, float* %s, align 4
  %mul18 = fmul float %14, 5.000000e-01
  store float %mul18, float* %ref.tmp17, align 4
  %call19 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyRK9btVector3S4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInA, i1 zeroext %useReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %axisInA.addr = alloca %class.btVector3*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %rbAxisA1 = alloca %class.btVector3, align 4
  %rbAxisA2 = alloca %class.btVector3, align 4
  %axisInB = alloca %class.btVector3, align 4
  %rotationArc = alloca %class.btQuaternion, align 4
  %rbAxisB1 = alloca %class.btVector3, align 4
  %rbAxisB2 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4
  store %class.btVector3* %axisInA, %class.btVector3** %axisInA.addr, align 4
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call11 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbAFrame)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rbBFrame)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %3 = load i8, i8* %useReferenceFrameA.addr, align 1
  %tobool = trunc i8 %3 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_normalCFM, align 4
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_normalERP, align 4
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_stopCFM, align 4
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  store float 0.000000e+00, float* %m_stopERP, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA1)
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rbAxisA2)
  %4 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA2)
  %5 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4
  %m_rbAFrame17 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame17)
  %6 = bitcast %class.btVector3* %call18 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_rbAFrame19 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call20 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame19)
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA1)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisA2)
  %8 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %8)
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA1)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisA2)
  %9 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %9)
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisA2)
  %10 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %10)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call20, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %call24, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26, float* nonnull align 4 dereferenceable(4) %call27, float* nonnull align 4 dereferenceable(4) %call28, float* nonnull align 4 dereferenceable(4) %call29)
  %11 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %11)
  %call31 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call30)
  %12 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %axisInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call31, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %13 = load %class.btVector3*, %class.btVector3** %axisInA.addr, align 4
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %axisInB)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %rbAxisB1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotationArc, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisA1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %rbAxisB2, %class.btVector3* %axisInB, %class.btVector3* nonnull align 4 dereferenceable(16) %rbAxisB1)
  %14 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %15 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %call32, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  %m_rbBFrame33 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame33)
  %16 = bitcast %class.btVector3* %call34 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %m_rbBFrame35 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame35)
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB1)
  %call38 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %rbAxisB2)
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %axisInB)
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB1)
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %rbAxisB2)
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %axisInB)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB1)
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %rbAxisB2)
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %axisInB)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call36, float* nonnull align 4 dereferenceable(4) %call37, float* nonnull align 4 dereferenceable(4) %call38, float* nonnull align 4 dereferenceable(4) %call39, float* nonnull align 4 dereferenceable(4) %call40, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43, float* nonnull align 4 dereferenceable(4) %call44, float* nonnull align 4 dereferenceable(4) %call45)
  %m_useReferenceFrameA46 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %18 = load i8, i8* %m_useReferenceFrameA46, align 4
  %tobool47 = trunc i8 %18 to i1
  %19 = zext i1 %tobool47 to i64
  %cond = select i1 %tobool47, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4
  %20 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %20
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %rbBFrame, i1 zeroext %useReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %rbBFrame.addr = alloca %class.btTransform*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4
  store %class.btTransform* %rbBFrame, %class.btTransform** %rbBFrame.addr, align 4
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2)
  %3 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4
  %call11 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %5 = load %class.btTransform*, %class.btTransform** %rbBFrame.addr, align 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %6 = load i8, i8* %useReferenceFrameA.addr, align 1
  %tobool = trunc i8 %6 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_normalCFM, align 4
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_normalERP, align 4
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_stopCFM, align 4
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  store float 0.000000e+00, float* %m_stopERP, align 4
  %m_useReferenceFrameA15 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %7 = load i8, i8* %m_useReferenceFrameA15, align 4
  %tobool16 = trunc i8 %7 to i1
  %8 = zext i1 %tobool16 to i64
  %cond = select i1 %tobool16, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4
  %9 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btHingeConstraint* @_ZN17btHingeConstraintC2ER11btRigidBodyRK11btTransformb(%class.btHingeConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btTransform* nonnull align 4 dereferenceable(64) %rbAFrame, i1 zeroext %useReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btHingeConstraint*, align 4
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbAFrame.addr = alloca %class.btTransform*, align 4
  %useReferenceFrameA.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btTransform* %rbAFrame, %class.btTransform** %rbAFrame.addr, align 4
  %frombool = zext i1 %useReferenceFrameA to i8
  store i8 %frombool, i8* %useReferenceFrameA.addr, align 1
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store %class.btHingeConstraint* %this1, %class.btHingeConstraint** %retval, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* %0, i32 4, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV17btHingeConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %array.begin3 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin3, i32 3
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %class.btJacobianEntry* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %class.btJacobianEntry* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4
  %call11 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %4 = load %class.btTransform*, %class.btTransform** %rbAFrame.addr, align 4
  %call12 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call %class.btAngularLimit* @_ZN14btAngularLimitC2Ev(%class.btAngularLimit* %m_limit)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  store i8 0, i8* %m_angularOnly, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  store i8 0, i8* %m_enableAngularMotor, align 1
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  store i8 0, i8* %m_useSolveConstraintObsolete, align 2
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %5 = load i8, i8* %useReferenceFrameA.addr, align 1
  %tobool = trunc i8 %5 to i1
  %frombool14 = zext i1 %tobool to i8
  store i8 %frombool14, i8* %m_useReferenceFrameA, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  store i32 0, i32* %m_flags, align 4
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_normalCFM, align 4
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_normalERP, align 4
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_stopCFM, align 4
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  store float 0.000000e+00, float* %m_stopERP, align 4
  %6 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %6, i32 0, i32 8
  %7 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %7)
  %m_rbAFrame16 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame16)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %call17)
  %m_rbBFrame18 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame18)
  %8 = bitcast %class.btVector3* %call19 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_useReferenceFrameA20 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %10 = load i8, i8* %m_useReferenceFrameA20, align 4
  %tobool21 = trunc i8 %10 to i1
  %11 = zext i1 %tobool21 to i64
  %cond = select i1 %tobool21, float -1.000000e+00, float 1.000000e+00
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  store float %cond, float* %m_referenceSign, align 4
  %12 = load %class.btHingeConstraint*, %class.btHingeConstraint** %retval, align 4
  ret %class.btHingeConstraint* %12
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint13buildJacobianEv(%class.btHingeConstraint* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %relPos = alloca %class.btVector3, align 4
  %normal = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp19 = alloca %class.btMatrix3x3, align 4
  %ref.tmp23 = alloca %class.btMatrix3x3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %jointAxis0local = alloca %class.btVector3, align 4
  %jointAxis1local = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %jointAxis0 = alloca %class.btVector3, align 4
  %jointAxis1 = alloca %class.btVector3, align 4
  %hingeAxisWorld = alloca %class.btVector3, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btMatrix3x3, align 4
  %ref.tmp66 = alloca %class.btMatrix3x3, align 4
  %ref.tmp77 = alloca %class.btMatrix3x3, align 4
  %ref.tmp81 = alloca %class.btMatrix3x3, align 4
  %ref.tmp92 = alloca %class.btMatrix3x3, align 4
  %ref.tmp96 = alloca %class.btMatrix3x3, align 4
  %axisA = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca %class.btVector3, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end119

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_accMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_accMotorImpulse, align 4
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %2 = load i8, i8* %m_angularOnly, align 4
  %tobool2 = trunc i8 %2 to i1
  br i1 %tobool2, label %if.end43, label %if.then3

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 8
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotAInW, %class.btTransform* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %5 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 9
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivotBInW, %class.btTransform* %call5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relPos, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW)
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then3
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then3 ], [ %arrayctor.next, %arrayctor.loop ]
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %relPos)
  %cmp = fcmp ogt float %call8, 0x3E80000000000000
  br i1 %cmp, label %if.then9, label %if.else

if.then9:                                         ; preds = %arrayctor.cont
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %relPos)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %7 = bitcast %class.btVector3* %arrayidx to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %7, i8* align 4 %8, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %arrayctor.cont
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  store float 1.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  store float 0.000000e+00, float* %ref.tmp13, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then9
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 2
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx14, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %i, align 4
  %cmp17 = icmp slt i32 %9, 3
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_jac = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 1
  %10 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jac, i32 0, i32 %10
  %11 = bitcast %class.btJacobianEntry* %arrayidx18 to i8*
  %12 = bitcast i8* %11 to %class.btJacobianEntry*
  %13 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA20 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 8
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA20, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call21)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp19, %class.btMatrix3x3* %call22)
  %15 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB24 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %15, i32 0, i32 9
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB24, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %16)
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call25)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp23, %class.btMatrix3x3* %call26)
  %17 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA28 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %17, i32 0, i32 8
  %18 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA28, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %18)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  %19 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB31 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %19, i32 0, i32 9
  %20 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB31, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %20)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %21 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %normal, i32 0, i32 %21
  %22 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA34 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %22, i32 0, i32 8
  %23 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA34, align 4
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %23)
  %24 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA36 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %24, i32 0, i32 8
  %25 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA36, align 4
  %call37 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %25)
  %26 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB38 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %26, i32 0, i32 9
  %27 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB38, align 4
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %27)
  %28 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB40 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %28, i32 0, i32 9
  %29 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB40, align 4
  %call41 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %29)
  %call42 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* %12, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp19, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx33, %class.btVector3* nonnull align 4 dereferenceable(16) %call35, float %call37, %class.btVector3* nonnull align 4 dereferenceable(16) %call39, float %call41)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end43

if.end43:                                         ; preds = %for.end, %if.then
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %jointAxis0local)
  %call45 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %jointAxis1local)
  %m_rbAFrame47 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call48 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame47)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp46, %class.btMatrix3x3* %call48, i32 2)
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0local, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1local)
  %call49 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call50 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call49)
  %call51 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call50)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jointAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call51, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0local)
  %call52 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call53 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call52)
  %call54 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call53)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %jointAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call54, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1local)
  %call55 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call56 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call55)
  %call57 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call56)
  %m_rbAFrame59 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call60 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame59)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp58, %class.btMatrix3x3* %call60, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %hingeAxisWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call57, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp58)
  %m_jacAng = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx61 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %31 = bitcast %class.btJacobianEntry* %arrayidx61 to i8*
  %32 = bitcast i8* %31 to %class.btJacobianEntry*
  %33 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA63 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %33, i32 0, i32 8
  %34 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA63, align 4
  %call64 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %34)
  %call65 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call64)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp62, %class.btMatrix3x3* %call65)
  %35 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB67 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %35, i32 0, i32 9
  %36 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB67, align 4
  %call68 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %36)
  %call69 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call68)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp66, %class.btMatrix3x3* %call69)
  %37 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA70 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %37, i32 0, i32 8
  %38 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA70, align 4
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %38)
  %39 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB72 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %39, i32 0, i32 9
  %40 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB72, align 4
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %40)
  %call74 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %32, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp62, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %call73)
  %m_jacAng75 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx76 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng75, i32 0, i32 1
  %41 = bitcast %class.btJacobianEntry* %arrayidx76 to i8*
  %42 = bitcast i8* %41 to %class.btJacobianEntry*
  %43 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA78 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %43, i32 0, i32 8
  %44 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA78, align 4
  %call79 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %44)
  %call80 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call79)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp77, %class.btMatrix3x3* %call80)
  %45 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB82 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %45, i32 0, i32 9
  %46 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB82, align 4
  %call83 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %46)
  %call84 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call83)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp81, %class.btMatrix3x3* %call84)
  %47 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA85 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %47, i32 0, i32 8
  %48 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA85, align 4
  %call86 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %48)
  %49 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB87 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %49, i32 0, i32 9
  %50 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB87, align 4
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %50)
  %call89 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp77, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %call86, %class.btVector3* nonnull align 4 dereferenceable(16) %call88)
  %m_jacAng90 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 2
  %arrayidx91 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng90, i32 0, i32 2
  %51 = bitcast %class.btJacobianEntry* %arrayidx91 to i8*
  %52 = bitcast i8* %51 to %class.btJacobianEntry*
  %53 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA93 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %53, i32 0, i32 8
  %54 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA93, align 4
  %call94 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %54)
  %call95 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call94)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp92, %class.btMatrix3x3* %call95)
  %55 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB97 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %55, i32 0, i32 9
  %56 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB97, align 4
  %call98 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %56)
  %call99 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call98)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp96, %class.btMatrix3x3* %call99)
  %57 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA100 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %57, i32 0, i32 8
  %58 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA100, align 4
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %58)
  %59 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB102 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %59, i32 0, i32 9
  %60 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB102, align 4
  %call103 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %60)
  %call104 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %hingeAxisWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp92, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %call101, %class.btVector3* nonnull align 4 dereferenceable(16) %call103)
  %m_accLimitImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_accLimitImpulse, align 4
  %61 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA105 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %61, i32 0, i32 8
  %62 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA105, align 4
  %call106 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %62)
  %63 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB107 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %63, i32 0, i32 9
  %64 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB107, align 4
  %call108 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %64)
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call106, %class.btTransform* nonnull align 4 dereferenceable(64) %call108)
  %call109 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call110 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call109)
  %call111 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call110)
  %m_rbAFrame113 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call114 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame113)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp112, %class.btMatrix3x3* %call114, i32 2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %axisA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp112)
  %call115 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call116 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call115, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  %call117 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this1)
  %call118 = call float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %call117, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA)
  %add = fadd float %call116, %call118
  %div = fdiv float 1.000000e+00, %add
  %m_kHinge = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 8
  store float %div, float* %m_kHinge, align 4
  br label %if.end119

if.end119:                                        ; preds = %if.end43, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody22getInvInertiaDiagLocalEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  ret %class.btVector3* %m_invInertiaLocal
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f(%class.btJacobianEntry* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, float %massInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB, float %massInvB) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %massInvA.addr = alloca float, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %massInvB.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4
  store float %massInvA, float* %massInvA.addr, align 4
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4
  store float %massInvB, float* %massInvB.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4
  %1 = bitcast %class.btVector3* %m_linearJointAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %m_linearJointAxis6 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_aJ7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_aJ7 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %m_linearJointAxis11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearJointAxis11)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %9 = bitcast %class.btVector3* %m_bJ12 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %12 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %15 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %17 = load float, float* %massInvA.addr, align 4
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %add = fadd float %17, %call21
  %18 = load float, float* %massInvB.addr, align 4
  %add22 = fadd float %add, %18
  %m_1MinvJt23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ24 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ24)
  %add26 = fadd float %add22, %call25
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add26, float* %m_Adiag, align 4
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2ERK9btVector3RK11btMatrix3x3S5_S2_S2_(%class.btJacobianEntry* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %jointAxis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2A, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %world2B, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvA, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaInvB) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  %jointAxis.addr = alloca %class.btVector3*, align 4
  %world2A.addr = alloca %class.btMatrix3x3*, align 4
  %world2B.addr = alloca %class.btMatrix3x3*, align 4
  %inertiaInvA.addr = alloca %class.btVector3*, align 4
  %inertiaInvB.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  store %class.btVector3* %jointAxis, %class.btVector3** %jointAxis.addr, align 4
  store %class.btMatrix3x3* %world2A, %class.btMatrix3x3** %world2A.addr, align 4
  store %class.btMatrix3x3* %world2B, %class.btMatrix3x3** %world2B.addr, align 4
  store %class.btVector3* %inertiaInvA, %class.btVector3** %inertiaInvA.addr, align 4
  store %class.btVector3* %inertiaInvB, %class.btVector3** %inertiaInvB.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_linearJointAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2A.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_aJ9 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_aJ9 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %world2B.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %jointAxis.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %m_bJ12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_bJ12 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %class.btVector3*, %class.btVector3** %inertiaInvA.addr, align 4
  %m_aJ14 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ14)
  %m_0MinvJt15 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %9 = bitcast %class.btVector3* %m_0MinvJt15 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btVector3*, %class.btVector3** %inertiaInvB.addr, align 4
  %m_bJ17 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ17)
  %m_1MinvJt18 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %12 = bitcast %class.btVector3* %m_1MinvJt18 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %m_0MinvJt19 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %m_aJ20 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_0MinvJt19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aJ20)
  %m_1MinvJt22 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %m_bJ23 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call24 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_1MinvJt22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bJ23)
  %add = fadd float %call21, %call24
  %m_Adiag = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 5
  store float %add, float* %m_Adiag, align 4
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  store float %call, float* %m_hingeAngle, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %m_hingeAngle2 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %2 = load float, float* %m_hingeAngle2, align 4
  call void @_ZN14btAngularLimit4testEf(%class.btAngularLimit* %m_limit, float %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btRigidBody32computeAngularImpulseDenominatorERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 9
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %1
}

; Function Attrs: noinline optnone
define hidden float @_ZN33btHingeAccumulatedAngleConstraint24getAccumulatedHingeAngleEv(%class.btHingeAccumulatedAngleConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btHingeAccumulatedAngleConstraint*, align 4
  %hingeAngle = alloca float, align 4
  store %class.btHingeAccumulatedAngleConstraint* %this, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %this1 = load %class.btHingeAccumulatedAngleConstraint*, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeAccumulatedAngleConstraint* %this1 to %class.btHingeConstraint*
  %call = call float @_ZN17btHingeConstraint13getHingeAngleEv(%class.btHingeConstraint* %0)
  store float %call, float* %hingeAngle, align 4
  %m_accumulatedAngle = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  %1 = load float, float* %m_accumulatedAngle, align 4
  %2 = load float, float* %hingeAngle, align 4
  %call2 = call float @_ZL21btShortestAngleUpdateff(float %1, float %2)
  %m_accumulatedAngle3 = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  store float %call2, float* %m_accumulatedAngle3, align 4
  %m_accumulatedAngle4 = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  %3 = load float, float* %m_accumulatedAngle4, align 4
  ret float %3
}

; Function Attrs: noinline optnone
define hidden float @_ZN17btHingeConstraint13getHingeAngleEv(%class.btHingeConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %call3 = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  ret float %call3
}

; Function Attrs: noinline optnone
define internal float @_ZL21btShortestAngleUpdateff(float %accAngle, float %curAngle) #2 {
entry:
  %retval = alloca float, align 4
  %accAngle.addr = alloca float, align 4
  %curAngle.addr = alloca float, align 4
  %tol = alloca float, align 4
  %result = alloca float, align 4
  store float %accAngle, float* %accAngle.addr, align 4
  store float %curAngle, float* %curAngle.addr, align 4
  store float 0x3FD3333340000000, float* %tol, align 4
  %0 = load float, float* %accAngle.addr, align 4
  %1 = load float, float* %curAngle.addr, align 4
  %call = call float @_ZL25btShortestAngularDistanceff(float %0, float %1)
  store float %call, float* %result, align 4
  %2 = load float, float* %result, align 4
  %call1 = call float @_Z6btFabsf(float %2)
  %3 = load float, float* %tol, align 4
  %cmp = fcmp ogt float %call1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %curAngle.addr, align 4
  store float %4, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %5 = load float, float* %accAngle.addr, align 4
  %6 = load float, float* %result, align 4
  %add = fadd float %5, %6
  store float %add, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %7 = load float, float* %retval, align 4
  ret float %7
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN33btHingeAccumulatedAngleConstraint24setAccumulatedHingeAngleEf(%class.btHingeAccumulatedAngleConstraint* %this, float %accAngle) #1 {
entry:
  %this.addr = alloca %class.btHingeAccumulatedAngleConstraint*, align 4
  %accAngle.addr = alloca float, align 4
  store %class.btHingeAccumulatedAngleConstraint* %this, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  store float %accAngle, float* %accAngle.addr, align 4
  %this1 = load %class.btHingeAccumulatedAngleConstraint*, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %0 = load float, float* %accAngle.addr, align 4
  %m_accumulatedAngle = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  store float %0, float* %m_accumulatedAngle, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN33btHingeAccumulatedAngleConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeAccumulatedAngleConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeAccumulatedAngleConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %curHingeAngle = alloca float, align 4
  store %class.btHingeAccumulatedAngleConstraint* %this, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btHingeAccumulatedAngleConstraint*, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeAccumulatedAngleConstraint* %this1 to %class.btHingeConstraint*
  %call = call float @_ZN17btHingeConstraint13getHingeAngleEv(%class.btHingeConstraint* %0)
  store float %call, float* %curHingeAngle, align 4
  %m_accumulatedAngle = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  %1 = load float, float* %m_accumulatedAngle, align 4
  %2 = load float, float* %curHingeAngle, align 4
  %call2 = call float @_ZL21btShortestAngleUpdateff(float %1, float %2)
  %m_accumulatedAngle3 = getelementptr inbounds %class.btHingeAccumulatedAngleConstraint, %class.btHingeAccumulatedAngleConstraint* %this1, i32 0, i32 1
  store float %call2, float* %m_accumulatedAngle3, align 4
  %3 = bitcast %class.btHingeAccumulatedAngleConstraint* %this1 to %class.btHingeConstraint*
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  call void @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeConstraint* %3, %"struct.btTypedConstraint::btConstraintInfo1"* %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  br label %if.end11

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 5, i32* %m_numConstraintRows2, align 4
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 1, i32* %nub3, align 4
  %5 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call4)
  %call5 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %call7 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call7, label %if.then8, label %if.end

if.then8:                                         ; preds = %lor.lhs.false, %if.else
  %9 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_numConstraintRows9, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %m_numConstraintRows9, align 4
  %11 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub10 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %11, i32 0, i32 1
  %12 = load i32, i32* %nub10, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %nub10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then8, %lor.lhs.false
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call zeroext i1 @_ZNK14btAngularLimit7isLimitEv(%class.btAngularLimit* %m_limit)
  %conv = zext i1 %call to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  %0 = load i8, i8* %m_enableAngularMotor, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN17btHingeConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #1 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 14
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 2
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows2, align 4
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 0, i32* %nub3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 15
  %0 = load i8, i8* %m_useOffsetForConstraintFrame, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %4 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 9
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %6 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %6, i32 0, i32 8
  %7 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %7)
  %8 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %8, i32 0, i32 9
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %9)
  call void @_ZN17btHingeConstraint32getInfo2InternalUsingFrameOffsetEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %11 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 8
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %12)
  %13 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %13, i32 0, i32 9
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %14)
  %15 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA11 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %15, i32 0, i32 8
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA11, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %16)
  %17 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB13 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %17, i32 0, i32 9
  %18 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB13, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %18)
  call void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %10, %class.btTransform* nonnull align 4 dereferenceable(64) %call8, %class.btTransform* nonnull align 4 dereferenceable(64) %call10, %class.btVector3* nonnull align 4 dereferenceable(16) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %call14)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint32getInfo2InternalUsingFrameOffsetEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %s = alloca i32, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %ofs = alloca %class.btVector3, align 4
  %miA = alloca float, align 4
  %miB = alloca float, align 4
  %hasStaticBody = alloca i8, align 1
  %miS = alloca float, align 4
  %factA = alloca float, align 4
  %factB = alloca float, align 4
  %ax1A = alloca %class.btVector3, align 4
  %ax1B = alloca %class.btVector3, align 4
  %ax1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %bodyA_trans = alloca %class.btTransform, align 4
  %bodyB_trans = alloca %class.btTransform, align 4
  %s0 = alloca i32, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %nrow = alloca i32, align 4
  %tmpA = alloca %class.btVector3, align 4
  %tmpB = alloca %class.btVector3, align 4
  %relA = alloca %class.btVector3, align 4
  %relB = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %projB = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca float, align 4
  %orthoB = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %projA = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %orthoA = alloca %class.btVector3, align 4
  %totalDist = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %len2 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %normalErp = alloca float, align 4
  %k = alloca float, align 4
  %rhs = alloca float, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %u = alloca %class.btVector3, align 4
  %srow = alloca i32, align 4
  %limit_err = alloca float, align 4
  %limit = alloca i32, align 4
  %powered = alloca i32, align 4
  %lostop = alloca float, align 4
  %histop = alloca float, align 4
  %currERP = alloca float, align 4
  %mot_fact = alloca float, align 4
  %bounce = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc435 = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %0, i32 0, i32 6
  %1 = load i32, i32* %rowskip, align 4
  store i32 %1, i32* %s, align 4
  %2 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %3 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ofs, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %call3 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyAEv(%class.btHingeConstraint* %this1)
  %call4 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call3)
  store float %call4, float* %miA, align 4
  %call5 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btHingeConstraint13getRigidBodyBEv(%class.btHingeConstraint* %this1)
  %call6 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %call5)
  store float %call6, float* %miB, align 4
  %4 = load float, float* %miA, align 4
  %cmp = fcmp olt float %4, 0x3E80000000000000
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %5 = load float, float* %miB, align 4
  %cmp7 = fcmp olt float %5, 0x3E80000000000000
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %6 = phi i1 [ true, %entry ], [ %cmp7, %lor.rhs ]
  %frombool = zext i1 %6 to i8
  store i8 %frombool, i8* %hasStaticBody, align 1
  %7 = load float, float* %miA, align 4
  %8 = load float, float* %miB, align 4
  %add = fadd float %7, %8
  store float %add, float* %miS, align 4
  %9 = load float, float* %miS, align 4
  %cmp8 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %lor.end
  %10 = load float, float* %miB, align 4
  %11 = load float, float* %miS, align 4
  %div = fdiv float %10, %11
  store float %div, float* %factA, align 4
  br label %if.end

if.else:                                          ; preds = %lor.end
  store float 5.000000e-01, float* %factA, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load float, float* %factA, align 4
  %sub = fsub float 1.000000e+00, %12
  store float %sub, float* %factB, align 4
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1A, %class.btMatrix3x3* %call9, i32 2)
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trB)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1B, %class.btMatrix3x3* %call10, i32 2)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1A, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ax1)
  %13 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call13 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyA_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %13)
  %14 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call14 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyB_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %14)
  store i32 0, i32* %s0, align 4
  %15 = load i32, i32* %s, align 4
  store i32 %15, i32* %s1, align 4
  %16 = load i32, i32* %s, align 4
  %mul = mul nsw i32 %16, 2
  store i32 %mul, i32* %s2, align 4
  store i32 2, i32* %nrow, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpA)
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpB)
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relA)
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %relB)
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %q)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyB_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %call22, %class.btVector3* nonnull align 4 dereferenceable(16) %call23)
  %17 = bitcast %class.btVector3* %relB to i8*
  %18 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call25, float* %ref.tmp24, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyA_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %call28)
  %19 = bitcast %class.btVector3* %relA to i8*
  %20 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %call30 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call30, float* %ref.tmp29, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %projA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %totalDist, %class.btVector3* nonnull align 4 dereferenceable(16) %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  %21 = bitcast %class.btVector3* %relA to i8*
  %22 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  %23 = bitcast %class.btVector3* %relB to i8*
  %24 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %25 = bitcast %class.btVector3* %p to i8*
  %26 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %call38 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  store float %call38, float* %len2, align 4
  %27 = load float, float* %len2, align 4
  %cmp39 = fcmp ogt float %27, 0x3E80000000000000
  br i1 %cmp39, label %if.then40, label %if.else44

if.then40:                                        ; preds = %if.end
  %28 = load float, float* %len2, align 4
  %call42 = call float @_Z6btSqrtf(float %28)
  store float %call42, float* %ref.tmp41, align 4
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  br label %if.end47

if.else44:                                        ; preds = %if.end
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp45, %class.btMatrix3x3* %call46, i32 1)
  %29 = bitcast %class.btVector3* %p to i8*
  %30 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  br label %if.end47

if.end47:                                         ; preds = %if.else44, %if.then40
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %31 = bitcast %class.btVector3* %q to i8*
  %32 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %33 = bitcast %class.btVector3* %tmpA to i8*
  %34 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %35 = bitcast %class.btVector3* %tmpB to i8*
  %36 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end47
  %37 = load i32, i32* %i, align 4
  %cmp51 = icmp slt i32 %37, 3
  br i1 %cmp51, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %38 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call52, i32 %38
  %39 = load float, float* %arrayidx, align 4
  %40 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %40, i32 0, i32 3
  %41 = load float*, float** %m_J1angularAxis, align 4
  %42 = load i32, i32* %s0, align 4
  %43 = load i32, i32* %i, align 4
  %add53 = add nsw i32 %42, %43
  %arrayidx54 = getelementptr inbounds float, float* %41, i32 %add53
  store float %39, float* %arrayidx54, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %44 = load i32, i32* %i, align 4
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc62, %for.end
  %45 = load i32, i32* %i, align 4
  %cmp56 = icmp slt i32 %45, 3
  br i1 %cmp56, label %for.body57, label %for.end64

for.body57:                                       ; preds = %for.cond55
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %46 = load i32, i32* %i, align 4
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %46
  %47 = load float, float* %arrayidx59, align 4
  %fneg = fneg float %47
  %48 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %48, i32 0, i32 5
  %49 = load float*, float** %m_J2angularAxis, align 4
  %50 = load i32, i32* %s0, align 4
  %51 = load i32, i32* %i, align 4
  %add60 = add nsw i32 %50, %51
  %arrayidx61 = getelementptr inbounds float, float* %49, i32 %add60
  store float %fneg, float* %arrayidx61, align 4
  br label %for.inc62

for.inc62:                                        ; preds = %for.body57
  %52 = load i32, i32* %i, align 4
  %inc63 = add nsw i32 %52, 1
  store i32 %inc63, i32* %i, align 4
  br label %for.cond55

for.end64:                                        ; preds = %for.cond55
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %53 = bitcast %class.btVector3* %tmpA to i8*
  %54 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %55 = bitcast %class.btVector3* %tmpB to i8*
  %56 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false)
  %57 = load i8, i8* %hasStaticBody, align 1
  %tobool = trunc i8 %57 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end72

land.lhs.true:                                    ; preds = %for.end64
  %call67 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.then69, label %if.end72

if.then69:                                        ; preds = %land.lhs.true
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %factB)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %factA)
  br label %if.end72

if.end72:                                         ; preds = %if.then69, %land.lhs.true, %for.end64
  store i32 0, i32* %i, align 4
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc81, %if.end72
  %58 = load i32, i32* %i, align 4
  %cmp74 = icmp slt i32 %58, 3
  br i1 %cmp74, label %for.body75, label %for.end83

for.body75:                                       ; preds = %for.cond73
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %59 = load i32, i32* %i, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %59
  %60 = load float, float* %arrayidx77, align 4
  %61 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis78 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %61, i32 0, i32 3
  %62 = load float*, float** %m_J1angularAxis78, align 4
  %63 = load i32, i32* %s1, align 4
  %64 = load i32, i32* %i, align 4
  %add79 = add nsw i32 %63, %64
  %arrayidx80 = getelementptr inbounds float, float* %62, i32 %add79
  store float %60, float* %arrayidx80, align 4
  br label %for.inc81

for.inc81:                                        ; preds = %for.body75
  %65 = load i32, i32* %i, align 4
  %inc82 = add nsw i32 %65, 1
  store i32 %inc82, i32* %i, align 4
  br label %for.cond73

for.end83:                                        ; preds = %for.cond73
  store i32 0, i32* %i, align 4
  br label %for.cond84

for.cond84:                                       ; preds = %for.inc93, %for.end83
  %66 = load i32, i32* %i, align 4
  %cmp85 = icmp slt i32 %66, 3
  br i1 %cmp85, label %for.body86, label %for.end95

for.body86:                                       ; preds = %for.cond84
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %67 = load i32, i32* %i, align 4
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %67
  %68 = load float, float* %arrayidx88, align 4
  %fneg89 = fneg float %68
  %69 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis90 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %69, i32 0, i32 5
  %70 = load float*, float** %m_J2angularAxis90, align 4
  %71 = load i32, i32* %s1, align 4
  %72 = load i32, i32* %i, align 4
  %add91 = add nsw i32 %71, %72
  %arrayidx92 = getelementptr inbounds float, float* %70, i32 %add91
  store float %fneg89, float* %arrayidx92, align 4
  br label %for.inc93

for.inc93:                                        ; preds = %for.body86
  %73 = load i32, i32* %i, align 4
  %inc94 = add nsw i32 %73, 1
  store i32 %inc94, i32* %i, align 4
  br label %for.cond84

for.end95:                                        ; preds = %for.cond84
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %74 = bitcast %class.btVector3* %tmpA to i8*
  %75 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp97, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %76 = bitcast %class.btVector3* %tmpB to i8*
  %77 = bitcast %class.btVector3* %ref.tmp97 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 16, i1 false)
  %78 = load i8, i8* %hasStaticBody, align 1
  %tobool98 = trunc i8 %78 to i1
  br i1 %tobool98, label %if.then99, label %if.end102

if.then99:                                        ; preds = %for.end95
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %factB)
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %factA)
  br label %if.end102

if.end102:                                        ; preds = %if.then99, %for.end95
  store i32 0, i32* %i, align 4
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc111, %if.end102
  %79 = load i32, i32* %i, align 4
  %cmp104 = icmp slt i32 %79, 3
  br i1 %cmp104, label %for.body105, label %for.end113

for.body105:                                      ; preds = %for.cond103
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %80 = load i32, i32* %i, align 4
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 %80
  %81 = load float, float* %arrayidx107, align 4
  %82 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis108 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %82, i32 0, i32 3
  %83 = load float*, float** %m_J1angularAxis108, align 4
  %84 = load i32, i32* %s2, align 4
  %85 = load i32, i32* %i, align 4
  %add109 = add nsw i32 %84, %85
  %arrayidx110 = getelementptr inbounds float, float* %83, i32 %add109
  store float %81, float* %arrayidx110, align 4
  br label %for.inc111

for.inc111:                                       ; preds = %for.body105
  %86 = load i32, i32* %i, align 4
  %inc112 = add nsw i32 %86, 1
  store i32 %inc112, i32* %i, align 4
  br label %for.cond103

for.end113:                                       ; preds = %for.cond103
  store i32 0, i32* %i, align 4
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc123, %for.end113
  %87 = load i32, i32* %i, align 4
  %cmp115 = icmp slt i32 %87, 3
  br i1 %cmp115, label %for.body116, label %for.end125

for.body116:                                      ; preds = %for.cond114
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %88 = load i32, i32* %i, align 4
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 %88
  %89 = load float, float* %arrayidx118, align 4
  %fneg119 = fneg float %89
  %90 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis120 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %90, i32 0, i32 5
  %91 = load float*, float** %m_J2angularAxis120, align 4
  %92 = load i32, i32* %s2, align 4
  %93 = load i32, i32* %i, align 4
  %add121 = add nsw i32 %92, %93
  %arrayidx122 = getelementptr inbounds float, float* %91, i32 %add121
  store float %fneg119, float* %arrayidx122, align 4
  br label %for.inc123

for.inc123:                                       ; preds = %for.body116
  %94 = load i32, i32* %i, align 4
  %inc124 = add nsw i32 %94, 1
  store i32 %inc124, i32* %i, align 4
  br label %for.cond114

for.end125:                                       ; preds = %for.cond114
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %95 = load i32, i32* %m_flags, align 4
  %and = and i32 %95, 8
  %tobool126 = icmp ne i32 %and, 0
  br i1 %tobool126, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end125
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %96 = load float, float* %m_normalERP, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.end125
  %97 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %97, i32 0, i32 1
  %98 = load float, float* %erp, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %96, %cond.true ], [ %98, %cond.false ]
  store float %cond, float* %normalErp, align 4
  %99 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %99, i32 0, i32 0
  %100 = load float, float* %fps, align 4
  %101 = load float, float* %normalErp, align 4
  %mul127 = fmul float %100, %101
  store float %mul127, float* %k, align 4
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %102 = load i8, i8* %m_angularOnly, align 4
  %tobool128 = trunc i8 %102 to i1
  br i1 %tobool128, label %if.end208, label %if.then129

if.then129:                                       ; preds = %cond.end
  store i32 0, i32* %i, align 4
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc137, %if.then129
  %103 = load i32, i32* %i, align 4
  %cmp131 = icmp slt i32 %103, 3
  br i1 %cmp131, label %for.body132, label %for.end139

for.body132:                                      ; preds = %for.cond130
  %call133 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %104 = load i32, i32* %i, align 4
  %arrayidx134 = getelementptr inbounds float, float* %call133, i32 %104
  %105 = load float, float* %arrayidx134, align 4
  %106 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %106, i32 0, i32 2
  %107 = load float*, float** %m_J1linearAxis, align 4
  %108 = load i32, i32* %s0, align 4
  %109 = load i32, i32* %i, align 4
  %add135 = add nsw i32 %108, %109
  %arrayidx136 = getelementptr inbounds float, float* %107, i32 %add135
  store float %105, float* %arrayidx136, align 4
  br label %for.inc137

for.inc137:                                       ; preds = %for.body132
  %110 = load i32, i32* %i, align 4
  %inc138 = add nsw i32 %110, 1
  store i32 %inc138, i32* %i, align 4
  br label %for.cond130

for.end139:                                       ; preds = %for.cond130
  store i32 0, i32* %i, align 4
  br label %for.cond140

for.cond140:                                      ; preds = %for.inc148, %for.end139
  %111 = load i32, i32* %i, align 4
  %cmp141 = icmp slt i32 %111, 3
  br i1 %cmp141, label %for.body142, label %for.end150

for.body142:                                      ; preds = %for.cond140
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %112 = load i32, i32* %i, align 4
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 %112
  %113 = load float, float* %arrayidx144, align 4
  %114 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis145 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %114, i32 0, i32 2
  %115 = load float*, float** %m_J1linearAxis145, align 4
  %116 = load i32, i32* %s1, align 4
  %117 = load i32, i32* %i, align 4
  %add146 = add nsw i32 %116, %117
  %arrayidx147 = getelementptr inbounds float, float* %115, i32 %add146
  store float %113, float* %arrayidx147, align 4
  br label %for.inc148

for.inc148:                                       ; preds = %for.body142
  %118 = load i32, i32* %i, align 4
  %inc149 = add nsw i32 %118, 1
  store i32 %inc149, i32* %i, align 4
  br label %for.cond140

for.end150:                                       ; preds = %for.cond140
  store i32 0, i32* %i, align 4
  br label %for.cond151

for.cond151:                                      ; preds = %for.inc159, %for.end150
  %119 = load i32, i32* %i, align 4
  %cmp152 = icmp slt i32 %119, 3
  br i1 %cmp152, label %for.body153, label %for.end161

for.body153:                                      ; preds = %for.cond151
  %call154 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %120 = load i32, i32* %i, align 4
  %arrayidx155 = getelementptr inbounds float, float* %call154, i32 %120
  %121 = load float, float* %arrayidx155, align 4
  %122 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis156 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %122, i32 0, i32 2
  %123 = load float*, float** %m_J1linearAxis156, align 4
  %124 = load i32, i32* %s2, align 4
  %125 = load i32, i32* %i, align 4
  %add157 = add nsw i32 %124, %125
  %arrayidx158 = getelementptr inbounds float, float* %123, i32 %add157
  store float %121, float* %arrayidx158, align 4
  br label %for.inc159

for.inc159:                                       ; preds = %for.body153
  %126 = load i32, i32* %i, align 4
  %inc160 = add nsw i32 %126, 1
  store i32 %inc160, i32* %i, align 4
  br label %for.cond151

for.end161:                                       ; preds = %for.cond151
  store i32 0, i32* %i, align 4
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc170, %for.end161
  %127 = load i32, i32* %i, align 4
  %cmp163 = icmp slt i32 %127, 3
  br i1 %cmp163, label %for.body164, label %for.end172

for.body164:                                      ; preds = %for.cond162
  %call165 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %128 = load i32, i32* %i, align 4
  %arrayidx166 = getelementptr inbounds float, float* %call165, i32 %128
  %129 = load float, float* %arrayidx166, align 4
  %fneg167 = fneg float %129
  %130 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %130, i32 0, i32 4
  %131 = load float*, float** %m_J2linearAxis, align 4
  %132 = load i32, i32* %s0, align 4
  %133 = load i32, i32* %i, align 4
  %add168 = add nsw i32 %132, %133
  %arrayidx169 = getelementptr inbounds float, float* %131, i32 %add168
  store float %fneg167, float* %arrayidx169, align 4
  br label %for.inc170

for.inc170:                                       ; preds = %for.body164
  %134 = load i32, i32* %i, align 4
  %inc171 = add nsw i32 %134, 1
  store i32 %inc171, i32* %i, align 4
  br label %for.cond162

for.end172:                                       ; preds = %for.cond162
  store i32 0, i32* %i, align 4
  br label %for.cond173

for.cond173:                                      ; preds = %for.inc182, %for.end172
  %135 = load i32, i32* %i, align 4
  %cmp174 = icmp slt i32 %135, 3
  br i1 %cmp174, label %for.body175, label %for.end184

for.body175:                                      ; preds = %for.cond173
  %call176 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %136 = load i32, i32* %i, align 4
  %arrayidx177 = getelementptr inbounds float, float* %call176, i32 %136
  %137 = load float, float* %arrayidx177, align 4
  %fneg178 = fneg float %137
  %138 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis179 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %138, i32 0, i32 4
  %139 = load float*, float** %m_J2linearAxis179, align 4
  %140 = load i32, i32* %s1, align 4
  %141 = load i32, i32* %i, align 4
  %add180 = add nsw i32 %140, %141
  %arrayidx181 = getelementptr inbounds float, float* %139, i32 %add180
  store float %fneg178, float* %arrayidx181, align 4
  br label %for.inc182

for.inc182:                                       ; preds = %for.body175
  %142 = load i32, i32* %i, align 4
  %inc183 = add nsw i32 %142, 1
  store i32 %inc183, i32* %i, align 4
  br label %for.cond173

for.end184:                                       ; preds = %for.cond173
  store i32 0, i32* %i, align 4
  br label %for.cond185

for.cond185:                                      ; preds = %for.inc194, %for.end184
  %143 = load i32, i32* %i, align 4
  %cmp186 = icmp slt i32 %143, 3
  br i1 %cmp186, label %for.body187, label %for.end196

for.body187:                                      ; preds = %for.cond185
  %call188 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %144 = load i32, i32* %i, align 4
  %arrayidx189 = getelementptr inbounds float, float* %call188, i32 %144
  %145 = load float, float* %arrayidx189, align 4
  %fneg190 = fneg float %145
  %146 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis191 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %146, i32 0, i32 4
  %147 = load float*, float** %m_J2linearAxis191, align 4
  %148 = load i32, i32* %s2, align 4
  %149 = load i32, i32* %i, align 4
  %add192 = add nsw i32 %148, %149
  %arrayidx193 = getelementptr inbounds float, float* %147, i32 %add192
  store float %fneg190, float* %arrayidx193, align 4
  br label %for.inc194

for.inc194:                                       ; preds = %for.body187
  %150 = load i32, i32* %i, align 4
  %inc195 = add nsw i32 %150, 1
  store i32 %inc195, i32* %i, align 4
  br label %for.cond185

for.end196:                                       ; preds = %for.cond185
  %151 = load float, float* %k, align 4
  %call197 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %p, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul198 = fmul float %151, %call197
  store float %mul198, float* %rhs, align 4
  %152 = load float, float* %rhs, align 4
  %153 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %153, i32 0, i32 7
  %154 = load float*, float** %m_constraintError, align 4
  %155 = load i32, i32* %s0, align 4
  %arrayidx199 = getelementptr inbounds float, float* %154, i32 %155
  store float %152, float* %arrayidx199, align 4
  %156 = load float, float* %k, align 4
  %call200 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %q, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul201 = fmul float %156, %call200
  store float %mul201, float* %rhs, align 4
  %157 = load float, float* %rhs, align 4
  %158 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError202 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %158, i32 0, i32 7
  %159 = load float*, float** %m_constraintError202, align 4
  %160 = load i32, i32* %s1, align 4
  %arrayidx203 = getelementptr inbounds float, float* %159, i32 %160
  store float %157, float* %arrayidx203, align 4
  %161 = load float, float* %k, align 4
  %call204 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul205 = fmul float %161, %call204
  store float %mul205, float* %rhs, align 4
  %162 = load float, float* %rhs, align 4
  %163 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError206 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %163, i32 0, i32 7
  %164 = load float*, float** %m_constraintError206, align 4
  %165 = load i32, i32* %s2, align 4
  %arrayidx207 = getelementptr inbounds float, float* %164, i32 %165
  store float %162, float* %arrayidx207, align 4
  br label %if.end208

if.end208:                                        ; preds = %for.end196, %cond.end
  %166 = load i32, i32* %s, align 4
  %mul209 = mul nsw i32 3, %166
  store i32 %mul209, i32* %s3, align 4
  %167 = load i32, i32* %s, align 4
  %mul210 = mul nsw i32 4, %167
  store i32 %mul210, i32* %s4, align 4
  %call211 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx212 = getelementptr inbounds float, float* %call211, i32 0
  %168 = load float, float* %arrayidx212, align 4
  %169 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis213 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %169, i32 0, i32 3
  %170 = load float*, float** %m_J1angularAxis213, align 4
  %171 = load i32, i32* %s3, align 4
  %add214 = add nsw i32 %171, 0
  %arrayidx215 = getelementptr inbounds float, float* %170, i32 %add214
  store float %168, float* %arrayidx215, align 4
  %call216 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx217 = getelementptr inbounds float, float* %call216, i32 1
  %172 = load float, float* %arrayidx217, align 4
  %173 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis218 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %173, i32 0, i32 3
  %174 = load float*, float** %m_J1angularAxis218, align 4
  %175 = load i32, i32* %s3, align 4
  %add219 = add nsw i32 %175, 1
  %arrayidx220 = getelementptr inbounds float, float* %174, i32 %add219
  store float %172, float* %arrayidx220, align 4
  %call221 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx222 = getelementptr inbounds float, float* %call221, i32 2
  %176 = load float, float* %arrayidx222, align 4
  %177 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis223 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %177, i32 0, i32 3
  %178 = load float*, float** %m_J1angularAxis223, align 4
  %179 = load i32, i32* %s3, align 4
  %add224 = add nsw i32 %179, 2
  %arrayidx225 = getelementptr inbounds float, float* %178, i32 %add224
  store float %176, float* %arrayidx225, align 4
  %call226 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx227 = getelementptr inbounds float, float* %call226, i32 0
  %180 = load float, float* %arrayidx227, align 4
  %181 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis228 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %181, i32 0, i32 3
  %182 = load float*, float** %m_J1angularAxis228, align 4
  %183 = load i32, i32* %s4, align 4
  %add229 = add nsw i32 %183, 0
  %arrayidx230 = getelementptr inbounds float, float* %182, i32 %add229
  store float %180, float* %arrayidx230, align 4
  %call231 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx232 = getelementptr inbounds float, float* %call231, i32 1
  %184 = load float, float* %arrayidx232, align 4
  %185 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis233 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %185, i32 0, i32 3
  %186 = load float*, float** %m_J1angularAxis233, align 4
  %187 = load i32, i32* %s4, align 4
  %add234 = add nsw i32 %187, 1
  %arrayidx235 = getelementptr inbounds float, float* %186, i32 %add234
  store float %184, float* %arrayidx235, align 4
  %call236 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx237 = getelementptr inbounds float, float* %call236, i32 2
  %188 = load float, float* %arrayidx237, align 4
  %189 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis238 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %189, i32 0, i32 3
  %190 = load float*, float** %m_J1angularAxis238, align 4
  %191 = load i32, i32* %s4, align 4
  %add239 = add nsw i32 %191, 2
  %arrayidx240 = getelementptr inbounds float, float* %190, i32 %add239
  store float %188, float* %arrayidx240, align 4
  %call241 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx242 = getelementptr inbounds float, float* %call241, i32 0
  %192 = load float, float* %arrayidx242, align 4
  %fneg243 = fneg float %192
  %193 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis244 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %193, i32 0, i32 5
  %194 = load float*, float** %m_J2angularAxis244, align 4
  %195 = load i32, i32* %s3, align 4
  %add245 = add nsw i32 %195, 0
  %arrayidx246 = getelementptr inbounds float, float* %194, i32 %add245
  store float %fneg243, float* %arrayidx246, align 4
  %call247 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx248 = getelementptr inbounds float, float* %call247, i32 1
  %196 = load float, float* %arrayidx248, align 4
  %fneg249 = fneg float %196
  %197 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis250 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %197, i32 0, i32 5
  %198 = load float*, float** %m_J2angularAxis250, align 4
  %199 = load i32, i32* %s3, align 4
  %add251 = add nsw i32 %199, 1
  %arrayidx252 = getelementptr inbounds float, float* %198, i32 %add251
  store float %fneg249, float* %arrayidx252, align 4
  %call253 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx254 = getelementptr inbounds float, float* %call253, i32 2
  %200 = load float, float* %arrayidx254, align 4
  %fneg255 = fneg float %200
  %201 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis256 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %201, i32 0, i32 5
  %202 = load float*, float** %m_J2angularAxis256, align 4
  %203 = load i32, i32* %s3, align 4
  %add257 = add nsw i32 %203, 2
  %arrayidx258 = getelementptr inbounds float, float* %202, i32 %add257
  store float %fneg255, float* %arrayidx258, align 4
  %call259 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx260 = getelementptr inbounds float, float* %call259, i32 0
  %204 = load float, float* %arrayidx260, align 4
  %fneg261 = fneg float %204
  %205 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis262 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %205, i32 0, i32 5
  %206 = load float*, float** %m_J2angularAxis262, align 4
  %207 = load i32, i32* %s4, align 4
  %add263 = add nsw i32 %207, 0
  %arrayidx264 = getelementptr inbounds float, float* %206, i32 %add263
  store float %fneg261, float* %arrayidx264, align 4
  %call265 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx266 = getelementptr inbounds float, float* %call265, i32 1
  %208 = load float, float* %arrayidx266, align 4
  %fneg267 = fneg float %208
  %209 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis268 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %209, i32 0, i32 5
  %210 = load float*, float** %m_J2angularAxis268, align 4
  %211 = load i32, i32* %s4, align 4
  %add269 = add nsw i32 %211, 1
  %arrayidx270 = getelementptr inbounds float, float* %210, i32 %add269
  store float %fneg267, float* %arrayidx270, align 4
  %call271 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx272 = getelementptr inbounds float, float* %call271, i32 2
  %212 = load float, float* %arrayidx272, align 4
  %fneg273 = fneg float %212
  %213 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis274 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %213, i32 0, i32 5
  %214 = load float*, float** %m_J2angularAxis274, align 4
  %215 = load i32, i32* %s4, align 4
  %add275 = add nsw i32 %215, 2
  %arrayidx276 = getelementptr inbounds float, float* %214, i32 %add275
  store float %fneg273, float* %arrayidx276, align 4
  %216 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps277 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %216, i32 0, i32 0
  %217 = load float, float* %fps277, align 4
  %218 = load float, float* %normalErp, align 4
  %mul278 = fmul float %217, %218
  store float %mul278, float* %k, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %u, %class.btVector3* %ax1A, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B)
  %219 = load float, float* %k, align 4
  %call279 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul280 = fmul float %219, %call279
  %220 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError281 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %220, i32 0, i32 7
  %221 = load float*, float** %m_constraintError281, align 4
  %222 = load i32, i32* %s3, align 4
  %arrayidx282 = getelementptr inbounds float, float* %221, i32 %222
  store float %mul280, float* %arrayidx282, align 4
  %223 = load float, float* %k, align 4
  %call283 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul284 = fmul float %223, %call283
  %224 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError285 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %224, i32 0, i32 7
  %225 = load float*, float** %m_constraintError285, align 4
  %226 = load i32, i32* %s4, align 4
  %arrayidx286 = getelementptr inbounds float, float* %225, i32 %226
  store float %mul284, float* %arrayidx286, align 4
  store i32 4, i32* %nrow, align 4
  store float 0.000000e+00, float* %limit_err, align 4
  store i32 0, i32* %limit, align 4
  %call287 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool288 = icmp ne i32 %call287, 0
  br i1 %tobool288, label %if.then289, label %if.end294

if.then289:                                       ; preds = %if.end208
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call290 = call float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %m_limit)
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %227 = load float, float* %m_referenceSign, align 4
  %mul291 = fmul float %call290, %227
  store float %mul291, float* %limit_err, align 4
  %228 = load float, float* %limit_err, align 4
  %cmp292 = fcmp ogt float %228, 0.000000e+00
  %229 = zext i1 %cmp292 to i64
  %cond293 = select i1 %cmp292, i32 1, i32 2
  store i32 %cond293, i32* %limit, align 4
  br label %if.end294

if.end294:                                        ; preds = %if.then289, %if.end208
  store i32 0, i32* %powered, align 4
  %call295 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call295, label %if.then296, label %if.end297

if.then296:                                       ; preds = %if.end294
  store i32 1, i32* %powered, align 4
  br label %if.end297

if.end297:                                        ; preds = %if.then296, %if.end294
  %230 = load i32, i32* %limit, align 4
  %tobool298 = icmp ne i32 %230, 0
  br i1 %tobool298, label %if.then300, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end297
  %231 = load i32, i32* %powered, align 4
  %tobool299 = icmp ne i32 %231, 0
  br i1 %tobool299, label %if.then300, label %if.end454

if.then300:                                       ; preds = %lor.lhs.false, %if.end297
  %232 = load i32, i32* %nrow, align 4
  %inc301 = add nsw i32 %232, 1
  store i32 %inc301, i32* %nrow, align 4
  %233 = load i32, i32* %nrow, align 4
  %234 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip302 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %234, i32 0, i32 6
  %235 = load i32, i32* %rowskip302, align 4
  %mul303 = mul nsw i32 %233, %235
  store i32 %mul303, i32* %srow, align 4
  %call304 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx305 = getelementptr inbounds float, float* %call304, i32 0
  %236 = load float, float* %arrayidx305, align 4
  %237 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis306 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %237, i32 0, i32 3
  %238 = load float*, float** %m_J1angularAxis306, align 4
  %239 = load i32, i32* %srow, align 4
  %add307 = add nsw i32 %239, 0
  %arrayidx308 = getelementptr inbounds float, float* %238, i32 %add307
  store float %236, float* %arrayidx308, align 4
  %call309 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx310 = getelementptr inbounds float, float* %call309, i32 1
  %240 = load float, float* %arrayidx310, align 4
  %241 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis311 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %241, i32 0, i32 3
  %242 = load float*, float** %m_J1angularAxis311, align 4
  %243 = load i32, i32* %srow, align 4
  %add312 = add nsw i32 %243, 1
  %arrayidx313 = getelementptr inbounds float, float* %242, i32 %add312
  store float %240, float* %arrayidx313, align 4
  %call314 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx315 = getelementptr inbounds float, float* %call314, i32 2
  %244 = load float, float* %arrayidx315, align 4
  %245 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis316 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %245, i32 0, i32 3
  %246 = load float*, float** %m_J1angularAxis316, align 4
  %247 = load i32, i32* %srow, align 4
  %add317 = add nsw i32 %247, 2
  %arrayidx318 = getelementptr inbounds float, float* %246, i32 %add317
  store float %244, float* %arrayidx318, align 4
  %call319 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx320 = getelementptr inbounds float, float* %call319, i32 0
  %248 = load float, float* %arrayidx320, align 4
  %fneg321 = fneg float %248
  %249 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis322 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %249, i32 0, i32 5
  %250 = load float*, float** %m_J2angularAxis322, align 4
  %251 = load i32, i32* %srow, align 4
  %add323 = add nsw i32 %251, 0
  %arrayidx324 = getelementptr inbounds float, float* %250, i32 %add323
  store float %fneg321, float* %arrayidx324, align 4
  %call325 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx326 = getelementptr inbounds float, float* %call325, i32 1
  %252 = load float, float* %arrayidx326, align 4
  %fneg327 = fneg float %252
  %253 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis328 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %253, i32 0, i32 5
  %254 = load float*, float** %m_J2angularAxis328, align 4
  %255 = load i32, i32* %srow, align 4
  %add329 = add nsw i32 %255, 1
  %arrayidx330 = getelementptr inbounds float, float* %254, i32 %add329
  store float %fneg327, float* %arrayidx330, align 4
  %call331 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx332 = getelementptr inbounds float, float* %call331, i32 2
  %256 = load float, float* %arrayidx332, align 4
  %fneg333 = fneg float %256
  %257 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis334 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %257, i32 0, i32 5
  %258 = load float*, float** %m_J2angularAxis334, align 4
  %259 = load i32, i32* %srow, align 4
  %add335 = add nsw i32 %259, 2
  %arrayidx336 = getelementptr inbounds float, float* %258, i32 %add335
  store float %fneg333, float* %arrayidx336, align 4
  %call337 = call float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this1)
  store float %call337, float* %lostop, align 4
  %call338 = call float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this1)
  store float %call338, float* %histop, align 4
  %260 = load i32, i32* %limit, align 4
  %tobool339 = icmp ne i32 %260, 0
  br i1 %tobool339, label %land.lhs.true340, label %if.end343

land.lhs.true340:                                 ; preds = %if.then300
  %261 = load float, float* %lostop, align 4
  %262 = load float, float* %histop, align 4
  %cmp341 = fcmp oeq float %261, %262
  br i1 %cmp341, label %if.then342, label %if.end343

if.then342:                                       ; preds = %land.lhs.true340
  store i32 0, i32* %powered, align 4
  br label %if.end343

if.end343:                                        ; preds = %if.then342, %land.lhs.true340, %if.then300
  %263 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError344 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %263, i32 0, i32 7
  %264 = load float*, float** %m_constraintError344, align 4
  %265 = load i32, i32* %srow, align 4
  %arrayidx345 = getelementptr inbounds float, float* %264, i32 %265
  store float 0.000000e+00, float* %arrayidx345, align 4
  %m_flags346 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %266 = load i32, i32* %m_flags346, align 4
  %and347 = and i32 %266, 2
  %tobool348 = icmp ne i32 %and347, 0
  br i1 %tobool348, label %cond.true349, label %cond.false350

cond.true349:                                     ; preds = %if.end343
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  %267 = load float, float* %m_stopERP, align 4
  br label %cond.end351

cond.false350:                                    ; preds = %if.end343
  %268 = load float, float* %normalErp, align 4
  br label %cond.end351

cond.end351:                                      ; preds = %cond.false350, %cond.true349
  %cond352 = phi float [ %267, %cond.true349 ], [ %268, %cond.false350 ]
  store float %cond352, float* %currERP, align 4
  %269 = load i32, i32* %powered, align 4
  %tobool353 = icmp ne i32 %269, 0
  br i1 %tobool353, label %if.then354, label %if.end375

if.then354:                                       ; preds = %cond.end351
  %m_flags355 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %270 = load i32, i32* %m_flags355, align 4
  %and356 = and i32 %270, 4
  %tobool357 = icmp ne i32 %and356, 0
  br i1 %tobool357, label %if.then358, label %if.end360

if.then358:                                       ; preds = %if.then354
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %271 = load float, float* %m_normalCFM, align 4
  %272 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %272, i32 0, i32 8
  %273 = load float*, float** %cfm, align 4
  %274 = load i32, i32* %srow, align 4
  %arrayidx359 = getelementptr inbounds float, float* %273, i32 %274
  store float %271, float* %arrayidx359, align 4
  br label %if.end360

if.end360:                                        ; preds = %if.then358, %if.then354
  %275 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %276 = load float, float* %m_hingeAngle, align 4
  %277 = load float, float* %lostop, align 4
  %278 = load float, float* %histop, align 4
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %279 = load float, float* %m_motorTargetVelocity, align 4
  %280 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps361 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %280, i32 0, i32 0
  %281 = load float, float* %fps361, align 4
  %282 = load float, float* %currERP, align 4
  %mul362 = fmul float %281, %282
  %call363 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %275, float %276, float %277, float %278, float %279, float %mul362)
  store float %call363, float* %mot_fact, align 4
  %283 = load float, float* %mot_fact, align 4
  %m_motorTargetVelocity364 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %284 = load float, float* %m_motorTargetVelocity364, align 4
  %mul365 = fmul float %283, %284
  %m_referenceSign366 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %285 = load float, float* %m_referenceSign366, align 4
  %mul367 = fmul float %mul365, %285
  %286 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError368 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %286, i32 0, i32 7
  %287 = load float*, float** %m_constraintError368, align 4
  %288 = load i32, i32* %srow, align 4
  %arrayidx369 = getelementptr inbounds float, float* %287, i32 %288
  %289 = load float, float* %arrayidx369, align 4
  %add370 = fadd float %289, %mul367
  store float %add370, float* %arrayidx369, align 4
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %290 = load float, float* %m_maxMotorImpulse, align 4
  %fneg371 = fneg float %290
  %291 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %291, i32 0, i32 9
  %292 = load float*, float** %m_lowerLimit, align 4
  %293 = load i32, i32* %srow, align 4
  %arrayidx372 = getelementptr inbounds float, float* %292, i32 %293
  store float %fneg371, float* %arrayidx372, align 4
  %m_maxMotorImpulse373 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %294 = load float, float* %m_maxMotorImpulse373, align 4
  %295 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %295, i32 0, i32 10
  %296 = load float*, float** %m_upperLimit, align 4
  %297 = load i32, i32* %srow, align 4
  %arrayidx374 = getelementptr inbounds float, float* %296, i32 %297
  store float %294, float* %arrayidx374, align 4
  br label %if.end375

if.end375:                                        ; preds = %if.end360, %cond.end351
  %298 = load i32, i32* %limit, align 4
  %tobool376 = icmp ne i32 %298, 0
  br i1 %tobool376, label %if.then377, label %if.end453

if.then377:                                       ; preds = %if.end375
  %299 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps378 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %299, i32 0, i32 0
  %300 = load float, float* %fps378, align 4
  %301 = load float, float* %currERP, align 4
  %mul379 = fmul float %300, %301
  store float %mul379, float* %k, align 4
  %302 = load float, float* %k, align 4
  %303 = load float, float* %limit_err, align 4
  %mul380 = fmul float %302, %303
  %304 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError381 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %304, i32 0, i32 7
  %305 = load float*, float** %m_constraintError381, align 4
  %306 = load i32, i32* %srow, align 4
  %arrayidx382 = getelementptr inbounds float, float* %305, i32 %306
  %307 = load float, float* %arrayidx382, align 4
  %add383 = fadd float %307, %mul380
  store float %add383, float* %arrayidx382, align 4
  %m_flags384 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %308 = load i32, i32* %m_flags384, align 4
  %and385 = and i32 %308, 1
  %tobool386 = icmp ne i32 %and385, 0
  br i1 %tobool386, label %if.then387, label %if.end390

if.then387:                                       ; preds = %if.then377
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %309 = load float, float* %m_stopCFM, align 4
  %310 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm388 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %310, i32 0, i32 8
  %311 = load float*, float** %cfm388, align 4
  %312 = load i32, i32* %srow, align 4
  %arrayidx389 = getelementptr inbounds float, float* %311, i32 %312
  store float %309, float* %arrayidx389, align 4
  br label %if.end390

if.end390:                                        ; preds = %if.then387, %if.then377
  %313 = load float, float* %lostop, align 4
  %314 = load float, float* %histop, align 4
  %cmp391 = fcmp oeq float %313, %314
  br i1 %cmp391, label %if.then392, label %if.else397

if.then392:                                       ; preds = %if.end390
  %315 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit393 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %315, i32 0, i32 9
  %316 = load float*, float** %m_lowerLimit393, align 4
  %317 = load i32, i32* %srow, align 4
  %arrayidx394 = getelementptr inbounds float, float* %316, i32 %317
  store float 0xC7EFFFFFE0000000, float* %arrayidx394, align 4
  %318 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit395 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %318, i32 0, i32 10
  %319 = load float*, float** %m_upperLimit395, align 4
  %320 = load i32, i32* %srow, align 4
  %arrayidx396 = getelementptr inbounds float, float* %319, i32 %320
  store float 0x47EFFFFFE0000000, float* %arrayidx396, align 4
  br label %if.end410

if.else397:                                       ; preds = %if.end390
  %321 = load i32, i32* %limit, align 4
  %cmp398 = icmp eq i32 %321, 1
  br i1 %cmp398, label %if.then399, label %if.else404

if.then399:                                       ; preds = %if.else397
  %322 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit400 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %322, i32 0, i32 9
  %323 = load float*, float** %m_lowerLimit400, align 4
  %324 = load i32, i32* %srow, align 4
  %arrayidx401 = getelementptr inbounds float, float* %323, i32 %324
  store float 0.000000e+00, float* %arrayidx401, align 4
  %325 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit402 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %325, i32 0, i32 10
  %326 = load float*, float** %m_upperLimit402, align 4
  %327 = load i32, i32* %srow, align 4
  %arrayidx403 = getelementptr inbounds float, float* %326, i32 %327
  store float 0x47EFFFFFE0000000, float* %arrayidx403, align 4
  br label %if.end409

if.else404:                                       ; preds = %if.else397
  %328 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit405 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %328, i32 0, i32 9
  %329 = load float*, float** %m_lowerLimit405, align 4
  %330 = load i32, i32* %srow, align 4
  %arrayidx406 = getelementptr inbounds float, float* %329, i32 %330
  store float 0xC7EFFFFFE0000000, float* %arrayidx406, align 4
  %331 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit407 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %331, i32 0, i32 10
  %332 = load float*, float** %m_upperLimit407, align 4
  %333 = load i32, i32* %srow, align 4
  %arrayidx408 = getelementptr inbounds float, float* %332, i32 %333
  store float 0.000000e+00, float* %arrayidx408, align 4
  br label %if.end409

if.end409:                                        ; preds = %if.else404, %if.then399
  br label %if.end410

if.end410:                                        ; preds = %if.end409, %if.then392
  %m_limit411 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call412 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit411)
  store float %call412, float* %bounce, align 4
  %334 = load float, float* %bounce, align 4
  %cmp413 = fcmp ogt float %334, 0.000000e+00
  br i1 %cmp413, label %if.then414, label %if.end447

if.then414:                                       ; preds = %if.end410
  %335 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %call415 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %335, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call415, float* %vel, align 4
  %336 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %call416 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %336, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %337 = load float, float* %vel, align 4
  %sub417 = fsub float %337, %call416
  store float %sub417, float* %vel, align 4
  %338 = load i32, i32* %limit, align 4
  %cmp418 = icmp eq i32 %338, 1
  br i1 %cmp418, label %if.then419, label %if.else432

if.then419:                                       ; preds = %if.then414
  %339 = load float, float* %vel, align 4
  %cmp420 = fcmp olt float %339, 0.000000e+00
  br i1 %cmp420, label %if.then421, label %if.end431

if.then421:                                       ; preds = %if.then419
  %340 = load float, float* %bounce, align 4
  %fneg422 = fneg float %340
  %341 = load float, float* %vel, align 4
  %mul423 = fmul float %fneg422, %341
  store float %mul423, float* %newc, align 4
  %342 = load float, float* %newc, align 4
  %343 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError424 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %343, i32 0, i32 7
  %344 = load float*, float** %m_constraintError424, align 4
  %345 = load i32, i32* %srow, align 4
  %arrayidx425 = getelementptr inbounds float, float* %344, i32 %345
  %346 = load float, float* %arrayidx425, align 4
  %cmp426 = fcmp ogt float %342, %346
  br i1 %cmp426, label %if.then427, label %if.end430

if.then427:                                       ; preds = %if.then421
  %347 = load float, float* %newc, align 4
  %348 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError428 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %348, i32 0, i32 7
  %349 = load float*, float** %m_constraintError428, align 4
  %350 = load i32, i32* %srow, align 4
  %arrayidx429 = getelementptr inbounds float, float* %349, i32 %350
  store float %347, float* %arrayidx429, align 4
  br label %if.end430

if.end430:                                        ; preds = %if.then427, %if.then421
  br label %if.end431

if.end431:                                        ; preds = %if.end430, %if.then419
  br label %if.end446

if.else432:                                       ; preds = %if.then414
  %351 = load float, float* %vel, align 4
  %cmp433 = fcmp ogt float %351, 0.000000e+00
  br i1 %cmp433, label %if.then434, label %if.end445

if.then434:                                       ; preds = %if.else432
  %352 = load float, float* %bounce, align 4
  %fneg436 = fneg float %352
  %353 = load float, float* %vel, align 4
  %mul437 = fmul float %fneg436, %353
  store float %mul437, float* %newc435, align 4
  %354 = load float, float* %newc435, align 4
  %355 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError438 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %355, i32 0, i32 7
  %356 = load float*, float** %m_constraintError438, align 4
  %357 = load i32, i32* %srow, align 4
  %arrayidx439 = getelementptr inbounds float, float* %356, i32 %357
  %358 = load float, float* %arrayidx439, align 4
  %cmp440 = fcmp olt float %354, %358
  br i1 %cmp440, label %if.then441, label %if.end444

if.then441:                                       ; preds = %if.then434
  %359 = load float, float* %newc435, align 4
  %360 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError442 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %360, i32 0, i32 7
  %361 = load float*, float** %m_constraintError442, align 4
  %362 = load i32, i32* %srow, align 4
  %arrayidx443 = getelementptr inbounds float, float* %361, i32 %362
  store float %359, float* %arrayidx443, align 4
  br label %if.end444

if.end444:                                        ; preds = %if.then441, %if.then434
  br label %if.end445

if.end445:                                        ; preds = %if.end444, %if.else432
  br label %if.end446

if.end446:                                        ; preds = %if.end445, %if.end431
  br label %if.end447

if.end447:                                        ; preds = %if.end446, %if.end410
  %m_limit448 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call449 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit448)
  %363 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError450 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %363, i32 0, i32 7
  %364 = load float*, float** %m_constraintError450, align 4
  %365 = load i32, i32* %srow, align 4
  %arrayidx451 = getelementptr inbounds float, float* %364, i32 %365
  %366 = load float, float* %arrayidx451, align 4
  %mul452 = fmul float %366, %call449
  store float %mul452, float* %arrayidx451, align 4
  br label %if.end453

if.end453:                                        ; preds = %if.end447, %if.end375
  br label %if.end454

if.end454:                                        ; preds = %if.end453, %lor.lhs.false
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %skip = alloca i32, align 4
  %trA = alloca %class.btTransform, align 4
  %trB = alloca %class.btTransform, align 4
  %pivotAInW = alloca %class.btVector3, align 4
  %pivotBInW = alloca %class.btVector3, align 4
  %a1 = alloca %class.btVector3, align 4
  %angular0 = alloca %class.btVector3*, align 4
  %angular1 = alloca %class.btVector3*, align 4
  %angular2 = alloca %class.btVector3*, align 4
  %a1neg = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %angular022 = alloca %class.btVector3*, align 4
  %angular123 = alloca %class.btVector3*, align 4
  %angular226 = alloca %class.btVector3*, align 4
  %normalErp = alloca float, align 4
  %k = alloca float, align 4
  %ax1 = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %s3 = alloca i32, align 4
  %s4 = alloca i32, align 4
  %ax2 = alloca %class.btVector3, align 4
  %u = alloca %class.btVector3, align 4
  %nrow = alloca i32, align 4
  %srow = alloca i32, align 4
  %limit_err = alloca float, align 4
  %limit = alloca i32, align 4
  %powered = alloca i32, align 4
  %lostop = alloca float, align 4
  %histop = alloca float, align 4
  %currERP = alloca float, align 4
  %mot_fact = alloca float, align 4
  %bounce = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc270 = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %0, i32 0, i32 6
  %1 = load i32, i32* %rowskip, align 4
  store i32 %1, i32* %skip, align 4
  %2 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trA, %class.btTransform* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %3 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %trB, %class.btTransform* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trA)
  %4 = bitcast %class.btVector3* %pivotAInW to i8*
  %5 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %trB)
  %6 = bitcast %class.btVector3* %pivotBInW to i8*
  %7 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %8 = load i8, i8* %m_angularOnly, align 4
  %tobool = trunc i8 %8 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %9 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %9, i32 0, i32 2
  %10 = load float*, float** %m_J1linearAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %10, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4
  %11 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %11, i32 0, i32 2
  %12 = load float*, float** %m_J1linearAxis3, align 4
  %13 = load i32, i32* %skip, align 4
  %add = add nsw i32 %13, 1
  %arrayidx4 = getelementptr inbounds float, float* %12, i32 %add
  store float 1.000000e+00, float* %arrayidx4, align 4
  %14 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis5 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %14, i32 0, i32 2
  %15 = load float*, float** %m_J1linearAxis5, align 4
  %16 = load i32, i32* %skip, align 4
  %mul = mul nsw i32 2, %16
  %add6 = add nsw i32 %mul, 2
  %arrayidx7 = getelementptr inbounds float, float* %15, i32 %add6
  store float 1.000000e+00, float* %arrayidx7, align 4
  %17 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %17, i32 0, i32 4
  %18 = load float*, float** %m_J2linearAxis, align 4
  %arrayidx8 = getelementptr inbounds float, float* %18, i32 0
  store float -1.000000e+00, float* %arrayidx8, align 4
  %19 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %19, i32 0, i32 4
  %20 = load float*, float** %m_J2linearAxis9, align 4
  %21 = load i32, i32* %skip, align 4
  %add10 = add nsw i32 %21, 1
  %arrayidx11 = getelementptr inbounds float, float* %20, i32 %add10
  store float -1.000000e+00, float* %arrayidx11, align 4
  %22 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis12 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %22, i32 0, i32 4
  %23 = load float*, float** %m_J2linearAxis12, align 4
  %24 = load i32, i32* %skip, align 4
  %mul13 = mul nsw i32 2, %24
  %add14 = add nsw i32 %mul13, 2
  %arrayidx15 = getelementptr inbounds float, float* %23, i32 %add14
  store float -1.000000e+00, float* %arrayidx15, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %25 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %25)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call16)
  %26 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %26, i32 0, i32 3
  %27 = load float*, float** %m_J1angularAxis, align 4
  %28 = bitcast float* %27 to %class.btVector3*
  store %class.btVector3* %28, %class.btVector3** %angular0, align 4
  %29 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis17 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %29, i32 0, i32 3
  %30 = load float*, float** %m_J1angularAxis17, align 4
  %31 = load i32, i32* %skip, align 4
  %add.ptr = getelementptr inbounds float, float* %30, i32 %31
  %32 = bitcast float* %add.ptr to %class.btVector3*
  store %class.btVector3* %32, %class.btVector3** %angular1, align 4
  %33 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis18 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %33, i32 0, i32 3
  %34 = load float*, float** %m_J1angularAxis18, align 4
  %35 = load i32, i32* %skip, align 4
  %mul19 = mul nsw i32 2, %35
  %add.ptr20 = getelementptr inbounds float, float* %34, i32 %mul19
  %36 = bitcast float* %add.ptr20 to %class.btVector3*
  store %class.btVector3* %36, %class.btVector3** %angular2, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %a1neg, %class.btVector3* nonnull align 4 dereferenceable(16) %a1)
  %37 = load %class.btVector3*, %class.btVector3** %angular0, align 4
  %38 = load %class.btVector3*, %class.btVector3** %angular1, align 4
  %39 = load %class.btVector3*, %class.btVector3** %angular2, align 4
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a1neg, %class.btVector3* %37, %class.btVector3* %38, %class.btVector3* %39)
  %40 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %40)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a2, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBInW, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  %41 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %41, i32 0, i32 5
  %42 = load float*, float** %m_J2angularAxis, align 4
  %43 = bitcast float* %42 to %class.btVector3*
  store %class.btVector3* %43, %class.btVector3** %angular022, align 4
  %44 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis24 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %44, i32 0, i32 5
  %45 = load float*, float** %m_J2angularAxis24, align 4
  %46 = load i32, i32* %skip, align 4
  %add.ptr25 = getelementptr inbounds float, float* %45, i32 %46
  %47 = bitcast float* %add.ptr25 to %class.btVector3*
  store %class.btVector3* %47, %class.btVector3** %angular123, align 4
  %48 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis27 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %48, i32 0, i32 5
  %49 = load float*, float** %m_J2angularAxis27, align 4
  %50 = load i32, i32* %skip, align 4
  %mul28 = mul nsw i32 2, %50
  %add.ptr29 = getelementptr inbounds float, float* %49, i32 %mul28
  %51 = bitcast float* %add.ptr29 to %class.btVector3*
  store %class.btVector3* %51, %class.btVector3** %angular226, align 4
  %52 = load %class.btVector3*, %class.btVector3** %angular022, align 4
  %53 = load %class.btVector3*, %class.btVector3** %angular123, align 4
  %54 = load %class.btVector3*, %class.btVector3** %angular226, align 4
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %a2, %class.btVector3* %52, %class.btVector3* %53, %class.btVector3* %54)
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %55 = load i32, i32* %m_flags, align 4
  %and = and i32 %55, 8
  %tobool30 = icmp ne i32 %and, 0
  br i1 %tobool30, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %56 = load float, float* %m_normalERP, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %57 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %57, i32 0, i32 1
  %58 = load float, float* %erp, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %56, %cond.true ], [ %58, %cond.false ]
  store float %cond, float* %normalErp, align 4
  %59 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %59, i32 0, i32 0
  %60 = load float, float* %fps, align 4
  %61 = load float, float* %normalErp, align 4
  %mul31 = fmul float %60, %61
  store float %mul31, float* %k, align 4
  %m_angularOnly32 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %62 = load i8, i8* %m_angularOnly32, align 4
  %tobool33 = trunc i8 %62 to i1
  br i1 %tobool33, label %if.end42, label %if.then34

if.then34:                                        ; preds = %cond.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then34
  %63 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %63, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %64 = load float, float* %k, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pivotBInW)
  %65 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 %65
  %66 = load float, float* %arrayidx36, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pivotAInW)
  %67 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 %67
  %68 = load float, float* %arrayidx38, align 4
  %sub = fsub float %66, %68
  %mul39 = fmul float %64, %sub
  %69 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %69, i32 0, i32 7
  %70 = load float*, float** %m_constraintError, align 4
  %71 = load i32, i32* %i, align 4
  %72 = load i32, i32* %skip, align 4
  %mul40 = mul nsw i32 %71, %72
  %arrayidx41 = getelementptr inbounds float, float* %70, i32 %mul40
  store float %mul39, float* %arrayidx41, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %73 = load i32, i32* %i, align 4
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end42

if.end42:                                         ; preds = %for.end, %cond.end
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1, %class.btMatrix3x3* %call43, i32 2)
  %call44 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %p, %class.btMatrix3x3* %call44, i32 0)
  %call45 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %q, %class.btMatrix3x3* %call45, i32 1)
  %74 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip46 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %74, i32 0, i32 6
  %75 = load i32, i32* %rowskip46, align 4
  %mul47 = mul nsw i32 3, %75
  store i32 %mul47, i32* %s3, align 4
  %76 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip48 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %76, i32 0, i32 6
  %77 = load i32, i32* %rowskip48, align 4
  %mul49 = mul nsw i32 4, %77
  store i32 %mul49, i32* %s4, align 4
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %78 = load float, float* %arrayidx51, align 4
  %79 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis52 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %79, i32 0, i32 3
  %80 = load float*, float** %m_J1angularAxis52, align 4
  %81 = load i32, i32* %s3, align 4
  %add53 = add nsw i32 %81, 0
  %arrayidx54 = getelementptr inbounds float, float* %80, i32 %add53
  store float %78, float* %arrayidx54, align 4
  %call55 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %82 = load float, float* %arrayidx56, align 4
  %83 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis57 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %83, i32 0, i32 3
  %84 = load float*, float** %m_J1angularAxis57, align 4
  %85 = load i32, i32* %s3, align 4
  %add58 = add nsw i32 %85, 1
  %arrayidx59 = getelementptr inbounds float, float* %84, i32 %add58
  store float %82, float* %arrayidx59, align 4
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %86 = load float, float* %arrayidx61, align 4
  %87 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis62 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %87, i32 0, i32 3
  %88 = load float*, float** %m_J1angularAxis62, align 4
  %89 = load i32, i32* %s3, align 4
  %add63 = add nsw i32 %89, 2
  %arrayidx64 = getelementptr inbounds float, float* %88, i32 %add63
  store float %86, float* %arrayidx64, align 4
  %call65 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx66 = getelementptr inbounds float, float* %call65, i32 0
  %90 = load float, float* %arrayidx66, align 4
  %91 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis67 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %91, i32 0, i32 3
  %92 = load float*, float** %m_J1angularAxis67, align 4
  %93 = load i32, i32* %s4, align 4
  %add68 = add nsw i32 %93, 0
  %arrayidx69 = getelementptr inbounds float, float* %92, i32 %add68
  store float %90, float* %arrayidx69, align 4
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 1
  %94 = load float, float* %arrayidx71, align 4
  %95 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis72 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %95, i32 0, i32 3
  %96 = load float*, float** %m_J1angularAxis72, align 4
  %97 = load i32, i32* %s4, align 4
  %add73 = add nsw i32 %97, 1
  %arrayidx74 = getelementptr inbounds float, float* %96, i32 %add73
  store float %94, float* %arrayidx74, align 4
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 2
  %98 = load float, float* %arrayidx76, align 4
  %99 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis77 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %99, i32 0, i32 3
  %100 = load float*, float** %m_J1angularAxis77, align 4
  %101 = load i32, i32* %s4, align 4
  %add78 = add nsw i32 %101, 2
  %arrayidx79 = getelementptr inbounds float, float* %100, i32 %add78
  store float %98, float* %arrayidx79, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %102 = load float, float* %arrayidx81, align 4
  %fneg = fneg float %102
  %103 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis82 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %103, i32 0, i32 5
  %104 = load float*, float** %m_J2angularAxis82, align 4
  %105 = load i32, i32* %s3, align 4
  %add83 = add nsw i32 %105, 0
  %arrayidx84 = getelementptr inbounds float, float* %104, i32 %add83
  store float %fneg, float* %arrayidx84, align 4
  %call85 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx86 = getelementptr inbounds float, float* %call85, i32 1
  %106 = load float, float* %arrayidx86, align 4
  %fneg87 = fneg float %106
  %107 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis88 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %107, i32 0, i32 5
  %108 = load float*, float** %m_J2angularAxis88, align 4
  %109 = load i32, i32* %s3, align 4
  %add89 = add nsw i32 %109, 1
  %arrayidx90 = getelementptr inbounds float, float* %108, i32 %add89
  store float %fneg87, float* %arrayidx90, align 4
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 2
  %110 = load float, float* %arrayidx92, align 4
  %fneg93 = fneg float %110
  %111 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis94 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %111, i32 0, i32 5
  %112 = load float*, float** %m_J2angularAxis94, align 4
  %113 = load i32, i32* %s3, align 4
  %add95 = add nsw i32 %113, 2
  %arrayidx96 = getelementptr inbounds float, float* %112, i32 %add95
  store float %fneg93, float* %arrayidx96, align 4
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 0
  %114 = load float, float* %arrayidx98, align 4
  %fneg99 = fneg float %114
  %115 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis100 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %115, i32 0, i32 5
  %116 = load float*, float** %m_J2angularAxis100, align 4
  %117 = load i32, i32* %s4, align 4
  %add101 = add nsw i32 %117, 0
  %arrayidx102 = getelementptr inbounds float, float* %116, i32 %add101
  store float %fneg99, float* %arrayidx102, align 4
  %call103 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx104 = getelementptr inbounds float, float* %call103, i32 1
  %118 = load float, float* %arrayidx104, align 4
  %fneg105 = fneg float %118
  %119 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis106 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %119, i32 0, i32 5
  %120 = load float*, float** %m_J2angularAxis106, align 4
  %121 = load i32, i32* %s4, align 4
  %add107 = add nsw i32 %121, 1
  %arrayidx108 = getelementptr inbounds float, float* %120, i32 %add107
  store float %fneg105, float* %arrayidx108, align 4
  %call109 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx110 = getelementptr inbounds float, float* %call109, i32 2
  %122 = load float, float* %arrayidx110, align 4
  %fneg111 = fneg float %122
  %123 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis112 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %123, i32 0, i32 5
  %124 = load float*, float** %m_J2angularAxis112, align 4
  %125 = load i32, i32* %s4, align 4
  %add113 = add nsw i32 %125, 2
  %arrayidx114 = getelementptr inbounds float, float* %124, i32 %add113
  store float %fneg111, float* %arrayidx114, align 4
  %call115 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %trB)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax2, %class.btMatrix3x3* %call115, i32 2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %u, %class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %ax2)
  %126 = load float, float* %k, align 4
  %call116 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul117 = fmul float %126, %call116
  %127 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError118 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %127, i32 0, i32 7
  %128 = load float*, float** %m_constraintError118, align 4
  %129 = load i32, i32* %s3, align 4
  %arrayidx119 = getelementptr inbounds float, float* %128, i32 %129
  store float %mul117, float* %arrayidx119, align 4
  %130 = load float, float* %k, align 4
  %call120 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul121 = fmul float %130, %call120
  %131 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError122 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %131, i32 0, i32 7
  %132 = load float*, float** %m_constraintError122, align 4
  %133 = load i32, i32* %s4, align 4
  %arrayidx123 = getelementptr inbounds float, float* %132, i32 %133
  store float %mul121, float* %arrayidx123, align 4
  store i32 4, i32* %nrow, align 4
  store float 0.000000e+00, float* %limit_err, align 4
  store i32 0, i32* %limit, align 4
  %call124 = call i32 @_ZN17btHingeConstraint13getSolveLimitEv(%class.btHingeConstraint* %this1)
  %tobool125 = icmp ne i32 %call124, 0
  br i1 %tobool125, label %if.then126, label %if.end131

if.then126:                                       ; preds = %if.end42
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call127 = call float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %m_limit)
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %134 = load float, float* %m_referenceSign, align 4
  %mul128 = fmul float %call127, %134
  store float %mul128, float* %limit_err, align 4
  %135 = load float, float* %limit_err, align 4
  %cmp129 = fcmp ogt float %135, 0.000000e+00
  %136 = zext i1 %cmp129 to i64
  %cond130 = select i1 %cmp129, i32 1, i32 2
  store i32 %cond130, i32* %limit, align 4
  br label %if.end131

if.end131:                                        ; preds = %if.then126, %if.end42
  store i32 0, i32* %powered, align 4
  %call132 = call zeroext i1 @_ZN17btHingeConstraint21getEnableAngularMotorEv(%class.btHingeConstraint* %this1)
  br i1 %call132, label %if.then133, label %if.end134

if.then133:                                       ; preds = %if.end131
  store i32 1, i32* %powered, align 4
  br label %if.end134

if.end134:                                        ; preds = %if.then133, %if.end131
  %137 = load i32, i32* %limit, align 4
  %tobool135 = icmp ne i32 %137, 0
  br i1 %tobool135, label %if.then137, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end134
  %138 = load i32, i32* %powered, align 4
  %tobool136 = icmp ne i32 %138, 0
  br i1 %tobool136, label %if.then137, label %if.end289

if.then137:                                       ; preds = %lor.lhs.false, %if.end134
  %139 = load i32, i32* %nrow, align 4
  %inc138 = add nsw i32 %139, 1
  store i32 %inc138, i32* %nrow, align 4
  %140 = load i32, i32* %nrow, align 4
  %141 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip139 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %141, i32 0, i32 6
  %142 = load i32, i32* %rowskip139, align 4
  %mul140 = mul nsw i32 %140, %142
  store i32 %mul140, i32* %srow, align 4
  %call141 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx142 = getelementptr inbounds float, float* %call141, i32 0
  %143 = load float, float* %arrayidx142, align 4
  %144 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis143 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %144, i32 0, i32 3
  %145 = load float*, float** %m_J1angularAxis143, align 4
  %146 = load i32, i32* %srow, align 4
  %add144 = add nsw i32 %146, 0
  %arrayidx145 = getelementptr inbounds float, float* %145, i32 %add144
  store float %143, float* %arrayidx145, align 4
  %call146 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx147 = getelementptr inbounds float, float* %call146, i32 1
  %147 = load float, float* %arrayidx147, align 4
  %148 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis148 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %148, i32 0, i32 3
  %149 = load float*, float** %m_J1angularAxis148, align 4
  %150 = load i32, i32* %srow, align 4
  %add149 = add nsw i32 %150, 1
  %arrayidx150 = getelementptr inbounds float, float* %149, i32 %add149
  store float %147, float* %arrayidx150, align 4
  %call151 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx152 = getelementptr inbounds float, float* %call151, i32 2
  %151 = load float, float* %arrayidx152, align 4
  %152 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis153 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %152, i32 0, i32 3
  %153 = load float*, float** %m_J1angularAxis153, align 4
  %154 = load i32, i32* %srow, align 4
  %add154 = add nsw i32 %154, 2
  %arrayidx155 = getelementptr inbounds float, float* %153, i32 %add154
  store float %151, float* %arrayidx155, align 4
  %call156 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx157 = getelementptr inbounds float, float* %call156, i32 0
  %155 = load float, float* %arrayidx157, align 4
  %fneg158 = fneg float %155
  %156 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis159 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %156, i32 0, i32 5
  %157 = load float*, float** %m_J2angularAxis159, align 4
  %158 = load i32, i32* %srow, align 4
  %add160 = add nsw i32 %158, 0
  %arrayidx161 = getelementptr inbounds float, float* %157, i32 %add160
  store float %fneg158, float* %arrayidx161, align 4
  %call162 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx163 = getelementptr inbounds float, float* %call162, i32 1
  %159 = load float, float* %arrayidx163, align 4
  %fneg164 = fneg float %159
  %160 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis165 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %160, i32 0, i32 5
  %161 = load float*, float** %m_J2angularAxis165, align 4
  %162 = load i32, i32* %srow, align 4
  %add166 = add nsw i32 %162, 1
  %arrayidx167 = getelementptr inbounds float, float* %161, i32 %add166
  store float %fneg164, float* %arrayidx167, align 4
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 2
  %163 = load float, float* %arrayidx169, align 4
  %fneg170 = fneg float %163
  %164 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis171 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %164, i32 0, i32 5
  %165 = load float*, float** %m_J2angularAxis171, align 4
  %166 = load i32, i32* %srow, align 4
  %add172 = add nsw i32 %166, 2
  %arrayidx173 = getelementptr inbounds float, float* %165, i32 %add172
  store float %fneg170, float* %arrayidx173, align 4
  %call174 = call float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this1)
  store float %call174, float* %lostop, align 4
  %call175 = call float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this1)
  store float %call175, float* %histop, align 4
  %167 = load i32, i32* %limit, align 4
  %tobool176 = icmp ne i32 %167, 0
  br i1 %tobool176, label %land.lhs.true, label %if.end179

land.lhs.true:                                    ; preds = %if.then137
  %168 = load float, float* %lostop, align 4
  %169 = load float, float* %histop, align 4
  %cmp177 = fcmp oeq float %168, %169
  br i1 %cmp177, label %if.then178, label %if.end179

if.then178:                                       ; preds = %land.lhs.true
  store i32 0, i32* %powered, align 4
  br label %if.end179

if.end179:                                        ; preds = %if.then178, %land.lhs.true, %if.then137
  %170 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError180 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %170, i32 0, i32 7
  %171 = load float*, float** %m_constraintError180, align 4
  %172 = load i32, i32* %srow, align 4
  %arrayidx181 = getelementptr inbounds float, float* %171, i32 %172
  store float 0.000000e+00, float* %arrayidx181, align 4
  %m_flags182 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %173 = load i32, i32* %m_flags182, align 4
  %and183 = and i32 %173, 2
  %tobool184 = icmp ne i32 %and183, 0
  br i1 %tobool184, label %cond.true185, label %cond.false186

cond.true185:                                     ; preds = %if.end179
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  %174 = load float, float* %m_stopERP, align 4
  br label %cond.end187

cond.false186:                                    ; preds = %if.end179
  %175 = load float, float* %normalErp, align 4
  br label %cond.end187

cond.end187:                                      ; preds = %cond.false186, %cond.true185
  %cond188 = phi float [ %174, %cond.true185 ], [ %175, %cond.false186 ]
  store float %cond188, float* %currERP, align 4
  %176 = load i32, i32* %powered, align 4
  %tobool189 = icmp ne i32 %176, 0
  br i1 %tobool189, label %if.then190, label %if.end211

if.then190:                                       ; preds = %cond.end187
  %m_flags191 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %177 = load i32, i32* %m_flags191, align 4
  %and192 = and i32 %177, 4
  %tobool193 = icmp ne i32 %and192, 0
  br i1 %tobool193, label %if.then194, label %if.end196

if.then194:                                       ; preds = %if.then190
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %178 = load float, float* %m_normalCFM, align 4
  %179 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %179, i32 0, i32 8
  %180 = load float*, float** %cfm, align 4
  %181 = load i32, i32* %srow, align 4
  %arrayidx195 = getelementptr inbounds float, float* %180, i32 %181
  store float %178, float* %arrayidx195, align 4
  br label %if.end196

if.end196:                                        ; preds = %if.then194, %if.then190
  %182 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_hingeAngle = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 10
  %183 = load float, float* %m_hingeAngle, align 4
  %184 = load float, float* %lostop, align 4
  %185 = load float, float* %histop, align 4
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %186 = load float, float* %m_motorTargetVelocity, align 4
  %187 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps197 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %187, i32 0, i32 0
  %188 = load float, float* %fps197, align 4
  %189 = load float, float* %currERP, align 4
  %mul198 = fmul float %188, %189
  %call199 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %182, float %183, float %184, float %185, float %186, float %mul198)
  store float %call199, float* %mot_fact, align 4
  %190 = load float, float* %mot_fact, align 4
  %m_motorTargetVelocity200 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %191 = load float, float* %m_motorTargetVelocity200, align 4
  %mul201 = fmul float %190, %191
  %m_referenceSign202 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %192 = load float, float* %m_referenceSign202, align 4
  %mul203 = fmul float %mul201, %192
  %193 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError204 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %193, i32 0, i32 7
  %194 = load float*, float** %m_constraintError204, align 4
  %195 = load i32, i32* %srow, align 4
  %arrayidx205 = getelementptr inbounds float, float* %194, i32 %195
  %196 = load float, float* %arrayidx205, align 4
  %add206 = fadd float %196, %mul203
  store float %add206, float* %arrayidx205, align 4
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %197 = load float, float* %m_maxMotorImpulse, align 4
  %fneg207 = fneg float %197
  %198 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %198, i32 0, i32 9
  %199 = load float*, float** %m_lowerLimit, align 4
  %200 = load i32, i32* %srow, align 4
  %arrayidx208 = getelementptr inbounds float, float* %199, i32 %200
  store float %fneg207, float* %arrayidx208, align 4
  %m_maxMotorImpulse209 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %201 = load float, float* %m_maxMotorImpulse209, align 4
  %202 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %202, i32 0, i32 10
  %203 = load float*, float** %m_upperLimit, align 4
  %204 = load i32, i32* %srow, align 4
  %arrayidx210 = getelementptr inbounds float, float* %203, i32 %204
  store float %201, float* %arrayidx210, align 4
  br label %if.end211

if.end211:                                        ; preds = %if.end196, %cond.end187
  %205 = load i32, i32* %limit, align 4
  %tobool212 = icmp ne i32 %205, 0
  br i1 %tobool212, label %if.then213, label %if.end288

if.then213:                                       ; preds = %if.end211
  %206 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps214 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %206, i32 0, i32 0
  %207 = load float, float* %fps214, align 4
  %208 = load float, float* %currERP, align 4
  %mul215 = fmul float %207, %208
  store float %mul215, float* %k, align 4
  %209 = load float, float* %k, align 4
  %210 = load float, float* %limit_err, align 4
  %mul216 = fmul float %209, %210
  %211 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError217 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %211, i32 0, i32 7
  %212 = load float*, float** %m_constraintError217, align 4
  %213 = load i32, i32* %srow, align 4
  %arrayidx218 = getelementptr inbounds float, float* %212, i32 %213
  %214 = load float, float* %arrayidx218, align 4
  %add219 = fadd float %214, %mul216
  store float %add219, float* %arrayidx218, align 4
  %m_flags220 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %215 = load i32, i32* %m_flags220, align 4
  %and221 = and i32 %215, 1
  %tobool222 = icmp ne i32 %and221, 0
  br i1 %tobool222, label %if.then223, label %if.end226

if.then223:                                       ; preds = %if.then213
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %216 = load float, float* %m_stopCFM, align 4
  %217 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm224 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %217, i32 0, i32 8
  %218 = load float*, float** %cfm224, align 4
  %219 = load i32, i32* %srow, align 4
  %arrayidx225 = getelementptr inbounds float, float* %218, i32 %219
  store float %216, float* %arrayidx225, align 4
  br label %if.end226

if.end226:                                        ; preds = %if.then223, %if.then213
  %220 = load float, float* %lostop, align 4
  %221 = load float, float* %histop, align 4
  %cmp227 = fcmp oeq float %220, %221
  br i1 %cmp227, label %if.then228, label %if.else

if.then228:                                       ; preds = %if.end226
  %222 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit229 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %222, i32 0, i32 9
  %223 = load float*, float** %m_lowerLimit229, align 4
  %224 = load i32, i32* %srow, align 4
  %arrayidx230 = getelementptr inbounds float, float* %223, i32 %224
  store float 0xC7EFFFFFE0000000, float* %arrayidx230, align 4
  %225 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit231 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %225, i32 0, i32 10
  %226 = load float*, float** %m_upperLimit231, align 4
  %227 = load i32, i32* %srow, align 4
  %arrayidx232 = getelementptr inbounds float, float* %226, i32 %227
  store float 0x47EFFFFFE0000000, float* %arrayidx232, align 4
  br label %if.end245

if.else:                                          ; preds = %if.end226
  %228 = load i32, i32* %limit, align 4
  %cmp233 = icmp eq i32 %228, 1
  br i1 %cmp233, label %if.then234, label %if.else239

if.then234:                                       ; preds = %if.else
  %229 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit235 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %229, i32 0, i32 9
  %230 = load float*, float** %m_lowerLimit235, align 4
  %231 = load i32, i32* %srow, align 4
  %arrayidx236 = getelementptr inbounds float, float* %230, i32 %231
  store float 0.000000e+00, float* %arrayidx236, align 4
  %232 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit237 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %232, i32 0, i32 10
  %233 = load float*, float** %m_upperLimit237, align 4
  %234 = load i32, i32* %srow, align 4
  %arrayidx238 = getelementptr inbounds float, float* %233, i32 %234
  store float 0x47EFFFFFE0000000, float* %arrayidx238, align 4
  br label %if.end244

if.else239:                                       ; preds = %if.else
  %235 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit240 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %235, i32 0, i32 9
  %236 = load float*, float** %m_lowerLimit240, align 4
  %237 = load i32, i32* %srow, align 4
  %arrayidx241 = getelementptr inbounds float, float* %236, i32 %237
  store float 0xC7EFFFFFE0000000, float* %arrayidx241, align 4
  %238 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit242 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %238, i32 0, i32 10
  %239 = load float*, float** %m_upperLimit242, align 4
  %240 = load i32, i32* %srow, align 4
  %arrayidx243 = getelementptr inbounds float, float* %239, i32 %240
  store float 0.000000e+00, float* %arrayidx243, align 4
  br label %if.end244

if.end244:                                        ; preds = %if.else239, %if.then234
  br label %if.end245

if.end245:                                        ; preds = %if.end244, %if.then228
  %m_limit246 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call247 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit246)
  store float %call247, float* %bounce, align 4
  %241 = load float, float* %bounce, align 4
  %cmp248 = fcmp ogt float %241, 0.000000e+00
  br i1 %cmp248, label %if.then249, label %if.end282

if.then249:                                       ; preds = %if.end245
  %242 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %call250 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %242, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call250, float* %vel, align 4
  %243 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  %call251 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %243, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %244 = load float, float* %vel, align 4
  %sub252 = fsub float %244, %call251
  store float %sub252, float* %vel, align 4
  %245 = load i32, i32* %limit, align 4
  %cmp253 = icmp eq i32 %245, 1
  br i1 %cmp253, label %if.then254, label %if.else267

if.then254:                                       ; preds = %if.then249
  %246 = load float, float* %vel, align 4
  %cmp255 = fcmp olt float %246, 0.000000e+00
  br i1 %cmp255, label %if.then256, label %if.end266

if.then256:                                       ; preds = %if.then254
  %247 = load float, float* %bounce, align 4
  %fneg257 = fneg float %247
  %248 = load float, float* %vel, align 4
  %mul258 = fmul float %fneg257, %248
  store float %mul258, float* %newc, align 4
  %249 = load float, float* %newc, align 4
  %250 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError259 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %250, i32 0, i32 7
  %251 = load float*, float** %m_constraintError259, align 4
  %252 = load i32, i32* %srow, align 4
  %arrayidx260 = getelementptr inbounds float, float* %251, i32 %252
  %253 = load float, float* %arrayidx260, align 4
  %cmp261 = fcmp ogt float %249, %253
  br i1 %cmp261, label %if.then262, label %if.end265

if.then262:                                       ; preds = %if.then256
  %254 = load float, float* %newc, align 4
  %255 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError263 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %255, i32 0, i32 7
  %256 = load float*, float** %m_constraintError263, align 4
  %257 = load i32, i32* %srow, align 4
  %arrayidx264 = getelementptr inbounds float, float* %256, i32 %257
  store float %254, float* %arrayidx264, align 4
  br label %if.end265

if.end265:                                        ; preds = %if.then262, %if.then256
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %if.then254
  br label %if.end281

if.else267:                                       ; preds = %if.then249
  %258 = load float, float* %vel, align 4
  %cmp268 = fcmp ogt float %258, 0.000000e+00
  br i1 %cmp268, label %if.then269, label %if.end280

if.then269:                                       ; preds = %if.else267
  %259 = load float, float* %bounce, align 4
  %fneg271 = fneg float %259
  %260 = load float, float* %vel, align 4
  %mul272 = fmul float %fneg271, %260
  store float %mul272, float* %newc270, align 4
  %261 = load float, float* %newc270, align 4
  %262 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError273 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %262, i32 0, i32 7
  %263 = load float*, float** %m_constraintError273, align 4
  %264 = load i32, i32* %srow, align 4
  %arrayidx274 = getelementptr inbounds float, float* %263, i32 %264
  %265 = load float, float* %arrayidx274, align 4
  %cmp275 = fcmp olt float %261, %265
  br i1 %cmp275, label %if.then276, label %if.end279

if.then276:                                       ; preds = %if.then269
  %266 = load float, float* %newc270, align 4
  %267 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError277 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %267, i32 0, i32 7
  %268 = load float*, float** %m_constraintError277, align 4
  %269 = load i32, i32* %srow, align 4
  %arrayidx278 = getelementptr inbounds float, float* %268, i32 %269
  store float %266, float* %arrayidx278, align 4
  br label %if.end279

if.end279:                                        ; preds = %if.then276, %if.then269
  br label %if.end280

if.end280:                                        ; preds = %if.end279, %if.else267
  br label %if.end281

if.end281:                                        ; preds = %if.end280, %if.end266
  br label %if.end282

if.end282:                                        ; preds = %if.end281, %if.end245
  %m_limit283 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call284 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit283)
  %270 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError285 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %270, i32 0, i32 7
  %271 = load float*, float** %m_constraintError285, align 4
  %272 = load i32, i32* %srow, align 4
  %arrayidx286 = getelementptr inbounds float, float* %271, i32 %272
  %273 = load float, float* %arrayidx286, align 4
  %mul287 = fmul float %273, %call284
  store float %mul287, float* %arrayidx286, align 4
  br label %if.end288

if.end288:                                        ; preds = %if.end282, %if.end211
  br label %if.end289

if.end289:                                        ; preds = %if.end288, %lor.lhs.false
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %angVelA.addr = alloca %class.btVector3*, align 4
  %angVelB.addr = alloca %class.btVector3*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %angVelA, %class.btVector3** %angVelA.addr, align 4
  store %class.btVector3* %angVelB, %class.btVector3** %angVelB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZN17btHingeConstraint9testLimitERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %4 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %angVelA.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %angVelB.addr, align 4
  call void @_ZN17btHingeConstraint16getInfo2InternalEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_(%class.btHingeConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %1 = load float, float* %call, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %2 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %3 = load float, float* %call7, align 4
  %fneg8 = fneg float %3
  store float %fneg8, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %2, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %4 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %5 = load float, float* %call10, align 4
  %fneg11 = fneg float %5
  store float %fneg11, float* %ref.tmp9, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp13, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btAngularLimit13getCorrectionEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  %0 = load float, float* %m_correction, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK17btHingeConstraint13getLowerLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK17btHingeConstraint13getUpperLimitEv(%class.btHingeConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %m_limit)
  ret float %call
}

declare float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint*, float, float, float, float, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 4
  %0 = load float, float* %m_relaxationFactor, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_biasFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 3
  %0 = load float, float* %m_biasFactor, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint9setFramesERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %frameA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %frameA.addr = alloca %class.btTransform*, align 4
  %frameB.addr = alloca %class.btTransform*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btTransform* %frameA, %class.btTransform** %frameA.addr, align 4
  store %class.btTransform* %frameB, %class.btTransform** %frameB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %frameA.addr, align 4
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbAFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %1 = load %class.btTransform*, %class.btTransform** %frameB.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_rbBFrame, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to void (%class.btHingeConstraint*)***
  %vtable = load void (%class.btHingeConstraint*)**, void (%class.btHingeConstraint*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btHingeConstraint*)*, void (%class.btHingeConstraint*)** %vtable, i64 2
  %3 = load void (%class.btHingeConstraint*)*, void (%class.btHingeConstraint*)** %vfn, align 4
  call void %3(%class.btHingeConstraint* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN17btHingeConstraint9updateRHSEf(%class.btHingeConstraint* %this, float %timeStep) #1 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %refAxis0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %refAxis1 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call2, i32 0)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %refAxis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %1 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %m_rbAFrame5 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbAFrame5)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp4, %class.btMatrix3x3* %call6, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %refAxis1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %2 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %2)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_rbBFrame)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* %call9, i32 1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %swingAxis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8)
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis0)
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis1)
  %call12 = call float @_Z7btAtan2ff(float %call10, float %call11)
  store float %call12, float* %angle, align 4
  %m_referenceSign = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 11
  %3 = load float, float* %m_referenceSign, align 4
  %4 = load float, float* %angle, align 4
  %mul = fmul float %3, %4
  ret float %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #7
  ret float %call
}

declare void @_ZN14btAngularLimit4testEf(%class.btAngularLimit*, float) #3

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* @_ZL6vHinge, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint14setMotorTargetERK12btQuaternionf(%class.btHingeConstraint* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qAinB, float %dt) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %qAinB.addr = alloca %class.btQuaternion*, align 4
  %dt.addr = alloca float, align 4
  %qConstraint = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca %class.btQuaternion, align 4
  %ref.tmp3 = alloca %class.btQuaternion, align 4
  %ref.tmp4 = alloca %class.btQuaternion, align 4
  %vNoHinge = alloca %class.btVector3, align 4
  %qNoHinge = alloca %class.btQuaternion, align 4
  %qHinge = alloca %class.btQuaternion, align 4
  %ref.tmp6 = alloca %class.btQuaternion, align 4
  %targetAngle = alloca float, align 4
  %ref.tmp9 = alloca %class.btQuaternion, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store %class.btQuaternion* %qAinB, %class.btQuaternion** %qAinB.addr, align 4
  store float %dt, float* %dt.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp3, %class.btTransform* %m_rbBFrame)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp2, %class.btQuaternion* %ref.tmp3)
  %0 = load %class.btQuaternion*, %class.btQuaternion** %qAinB.addr, align 4
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp4, %class.btTransform* %m_rbAFrame)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qConstraint, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qConstraint)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %vNoHinge, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vHinge)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vNoHinge)
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %qNoHinge, %class.btVector3* nonnull align 4 dereferenceable(16) @_ZL6vHinge, %class.btVector3* nonnull align 4 dereferenceable(16) %vNoHinge)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp6, %class.btQuaternion* %qNoHinge)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %qHinge, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btQuaternion* nonnull align 4 dereferenceable(16) %qConstraint)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %qHinge)
  %call8 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qHinge)
  store float %call8, float* %targetAngle, align 4
  %1 = load float, float* %targetAngle, align 4
  %cmp = fcmp ogt float %1, 0x400921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNK12btQuaternionngEv(%class.btQuaternion* sret align 4 %ref.tmp9, %class.btQuaternion* %qHinge)
  %2 = bitcast %class.btQuaternion* %qHinge to i8*
  %3 = bitcast %class.btQuaternion* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %call10 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %qHinge)
  store float %call10, float* %targetAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast %class.btQuaternion* %qHinge to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %4)
  %5 = load float, float* %call11, align 4
  %cmp12 = fcmp olt float %5, 0.000000e+00
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.end
  %6 = load float, float* %targetAngle, align 4
  %fneg = fneg float %6
  store float %fneg, float* %targetAngle, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.end
  %7 = load float, float* %targetAngle, align 4
  %8 = load float, float* %dt.addr, align 4
  call void @_ZN17btHingeConstraint14setMotorTargetEff(%class.btHingeConstraint* %this1, float %7, float %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %1 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btAcosf(float %1)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4
  %2 = load float, float* %s, align 4
  ret float %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternionngEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q2 = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %this1, %class.btQuaternion** %q2, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %4)
  %5 = load float, float* %call3, align 4
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp2, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %7)
  %8 = load float, float* %call6, align 4
  %fneg7 = fneg float %8
  store float %fneg7, float* %ref.tmp5, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %11 = load float, float* %arrayidx, align 4
  %fneg9 = fneg float %11
  store float %fneg9, float* %ref.tmp8, align 4
  %call10 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btHingeConstraint14setMotorTargetEff(%class.btHingeConstraint* %this, float %targetAngle, float %dt) #2 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %targetAngle.addr = alloca float, align 4
  %dt.addr = alloca float, align 4
  %curAngle = alloca float, align 4
  %dAngle = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store float %targetAngle, float* %targetAngle.addr, align 4
  store float %dt, float* %dt.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  call void @_ZNK14btAngularLimit3fitERf(%class.btAngularLimit* %m_limit, float* nonnull align 4 dereferenceable(4) %targetAngle.addr)
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  %call3 = call float @_ZN17btHingeConstraint13getHingeAngleERK11btTransformS2_(%class.btHingeConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  store float %call3, float* %curAngle, align 4
  %4 = load float, float* %targetAngle.addr, align 4
  %5 = load float, float* %curAngle, align 4
  %sub = fsub float %4, %5
  store float %sub, float* %dAngle, align 4
  %6 = load float, float* %dAngle, align 4
  %7 = load float, float* %dt.addr, align 4
  %div = fdiv float %6, %7
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  store float %div, float* %m_motorTargetVelocity, align 4
  ret void
}

declare void @_ZNK14btAngularLimit3fitERf(%class.btAngularLimit*, float* nonnull align 4 dereferenceable(4)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN17btHingeConstraint8setParamEifi(%class.btHingeConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store float %value, float* %value.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load i32, i32* %axis.addr, align 4
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp eq i32 %1, 5
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %2 = load i32, i32* %num.addr, align 4
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb6
    i32 1, label %sw.bb9
  ]

sw.bb:                                            ; preds = %if.then
  %3 = load float, float* %value.addr, align 4
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  store float %3, float* %m_stopERP, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %4 = load i32, i32* %m_flags, align 4
  %or = or i32 %4, 2
  store i32 %or, i32* %m_flags, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %5 = load float, float* %value.addr, align 4
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  store float %5, float* %m_stopCFM, align 4
  %m_flags4 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %6 = load i32, i32* %m_flags4, align 4
  %or5 = or i32 %6, 1
  store i32 %or5, i32* %m_flags4, align 4
  br label %sw.epilog

sw.bb6:                                           ; preds = %if.then
  %7 = load float, float* %value.addr, align 4
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  store float %7, float* %m_normalCFM, align 4
  %m_flags7 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %8 = load i32, i32* %m_flags7, align 4
  %or8 = or i32 %8, 4
  store i32 %or8, i32* %m_flags7, align 4
  br label %sw.epilog

sw.bb9:                                           ; preds = %if.then
  %9 = load float, float* %value.addr, align 4
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  store float %9, float* %m_normalERP, align 4
  %m_flags10 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %10 = load i32, i32* %m_flags10, align 4
  %or11 = or i32 %10, 8
  store i32 %or11, i32* %m_flags10, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb9, %sw.bb6, %sw.bb3, %sw.bb
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK17btHingeConstraint8getParamEii(%class.btHingeConstraint* %this, i32 %num, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  store float 0.000000e+00, float* %retVal, align 4
  %0 = load i32, i32* %axis.addr, align 4
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp eq i32 %1, 5
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %2 = load i32, i32* %num.addr, align 4
  switch i32 %2, label %sw.default [
    i32 2, label %sw.bb
    i32 4, label %sw.bb3
    i32 3, label %sw.bb4
    i32 1, label %sw.bb5
  ]

sw.bb:                                            ; preds = %if.then
  %m_stopERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 22
  %3 = load float, float* %m_stopERP, align 4
  store float %3, float* %retVal, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %if.then
  %m_stopCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 21
  %4 = load float, float* %m_stopCFM, align 4
  store float %4, float* %retVal, align 4
  br label %sw.epilog

sw.bb4:                                           ; preds = %if.then
  %m_normalCFM = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 19
  %5 = load float, float* %m_normalCFM, align 4
  store float %5, float* %retVal, align 4
  br label %sw.epilog

sw.bb5:                                           ; preds = %if.then
  %m_normalERP = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 20
  %6 = load float, float* %m_normalERP, align 4
  store float %6, float* %retVal, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  %7 = load float, float* %retVal, align 4
  ret float %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHingeConstraint* @_ZN17btHingeConstraintD2Ev(%class.btHingeConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #8
  ret %class.btHingeConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btHingeConstraintD0Ev(%class.btHingeConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %call = call %class.btHingeConstraint* @_ZN17btHingeConstraintD2Ev(%class.btHingeConstraint* %this1) #8
  %0 = bitcast %class.btHingeConstraint* %this1 to i8*
  call void @_ZN17btHingeConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btHingeConstraint28calculateSerializeBufferSizeEv(%class.btHingeConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  ret i32 220
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK17btHingeConstraint9serializeEPvP12btSerializer(%class.btHingeConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %hingeData = alloca %struct.btHingeConstraintFloatData*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btHingeConstraintFloatData*
  store %struct.btHingeConstraintFloatData* %1, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %2 = bitcast %class.btHingeConstraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_rbAFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 3
  %6 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_rbAFrame2 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbAFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame2)
  %m_rbBFrame = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 4
  %7 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_rbBFrame3 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_rbBFrame, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame3)
  %m_angularOnly = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 12
  %8 = load i8, i8* %m_angularOnly, align 4
  %tobool = trunc i8 %8 to i1
  %conv = zext i1 %tobool to i32
  %9 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_angularOnly4 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %9, i32 0, i32 4
  store i32 %conv, i32* %m_angularOnly4, align 4
  %m_enableAngularMotor = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 13
  %10 = load i8, i8* %m_enableAngularMotor, align 1
  %tobool5 = trunc i8 %10 to i1
  %conv6 = zext i1 %tobool5 to i32
  %11 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_enableAngularMotor7 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %11, i32 0, i32 5
  store i32 %conv6, i32* %m_enableAngularMotor7, align 4
  %m_maxMotorImpulse = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 6
  %12 = load float, float* %m_maxMotorImpulse, align 4
  %13 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_maxMotorImpulse8 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %13, i32 0, i32 7
  store float %12, float* %m_maxMotorImpulse8, align 4
  %m_motorTargetVelocity = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 5
  %14 = load float, float* %m_motorTargetVelocity, align 4
  %15 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_motorTargetVelocity9 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %15, i32 0, i32 6
  store float %14, float* %m_motorTargetVelocity9, align 4
  %m_useReferenceFrameA = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 16
  %16 = load i8, i8* %m_useReferenceFrameA, align 4
  %tobool10 = trunc i8 %16 to i1
  %conv11 = zext i1 %tobool10 to i32
  %17 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_useReferenceFrameA12 = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %17, i32 0, i32 3
  store i32 %conv11, i32* %m_useReferenceFrameA12, align 4
  %m_limit = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call13 = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %m_limit)
  %18 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %18, i32 0, i32 8
  store float %call13, float* %m_lowerLimit, align 4
  %m_limit14 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call15 = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %m_limit14)
  %19 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_upperLimit = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %19, i32 0, i32 9
  store float %call15, float* %m_upperLimit, align 4
  %m_limit16 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call17 = call float @_ZNK14btAngularLimit11getSoftnessEv(%class.btAngularLimit* %m_limit16)
  %20 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_limitSoftness = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %20, i32 0, i32 10
  store float %call17, float* %m_limitSoftness, align 4
  %m_limit18 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call19 = call float @_ZNK14btAngularLimit13getBiasFactorEv(%class.btAngularLimit* %m_limit18)
  %21 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_biasFactor = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %21, i32 0, i32 11
  store float %call19, float* %m_biasFactor, align 4
  %m_limit20 = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 7
  %call21 = call float @_ZNK14btAngularLimit19getRelaxationFactorEv(%class.btAngularLimit* %m_limit20)
  %22 = load %struct.btHingeConstraintFloatData*, %struct.btHingeConstraintFloatData** %hingeData, align 4
  %m_relaxationFactor = getelementptr inbounds %struct.btHingeConstraintFloatData, %struct.btHingeConstraintFloatData* %22, i32 0, i32 12
  store float %call21, float* %m_relaxationFactor, align 4
  ret i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btHingeConstraint8getFlagsEv(%class.btHingeConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeConstraint*, align 4
  store %class.btHingeConstraint* %this, %class.btHingeConstraint** %this.addr, align 4
  %this1 = load %class.btHingeConstraint*, %class.btHingeConstraint** %this.addr, align 4
  %m_flags = getelementptr inbounds %class.btHingeConstraint, %class.btHingeConstraint* %this1, i32 0, i32 18
  %0 = load i32, i32* %m_flags, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHingeAccumulatedAngleConstraint* @_ZN33btHingeAccumulatedAngleConstraintD2Ev(%class.btHingeAccumulatedAngleConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeAccumulatedAngleConstraint*, align 4
  store %class.btHingeAccumulatedAngleConstraint* %this, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %this1 = load %class.btHingeAccumulatedAngleConstraint*, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %0 = bitcast %class.btHingeAccumulatedAngleConstraint* %this1 to %class.btHingeConstraint*
  %call = call %class.btHingeConstraint* @_ZN17btHingeConstraintD2Ev(%class.btHingeConstraint* %0) #8
  ret %class.btHingeAccumulatedAngleConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN33btHingeAccumulatedAngleConstraintD0Ev(%class.btHingeAccumulatedAngleConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHingeAccumulatedAngleConstraint*, align 4
  store %class.btHingeAccumulatedAngleConstraint* %this, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %this1 = load %class.btHingeAccumulatedAngleConstraint*, %class.btHingeAccumulatedAngleConstraint** %this.addr, align 4
  %call = call %class.btHingeAccumulatedAngleConstraint* @_ZN33btHingeAccumulatedAngleConstraintD2Ev(%class.btHingeAccumulatedAngleConstraint* %this1) #8
  %0 = bitcast %class.btHingeAccumulatedAngleConstraint* %this1 to i8*
  call void @_ZN33btHingeAccumulatedAngleConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define internal float @_ZL25btShortestAngularDistanceff(float %accAngle, float %curAngle) #2 {
entry:
  %accAngle.addr = alloca float, align 4
  %curAngle.addr = alloca float, align 4
  %result = alloca float, align 4
  store float %accAngle, float* %accAngle.addr, align 4
  store float %curAngle, float* %curAngle.addr, align 4
  %0 = load float, float* %curAngle.addr, align 4
  %call = call float @_ZL24btNormalizeAnglePositivef(float %0)
  %1 = load float, float* %accAngle.addr, align 4
  %call1 = call float @_ZL24btNormalizeAnglePositivef(float %1)
  %sub = fsub float %call, %call1
  %call2 = call float @_ZL24btNormalizeAnglePositivef(float %sub)
  %call3 = call float @_Z16btNormalizeAnglef(float %call2)
  store float %call3, float* %result, align 4
  %2 = load float, float* %result, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  %0 = load float, float* %angleInRadians.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4
  %1 = load float, float* %angleInRadians.addr, align 4
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline nounwind optnone
define internal float @_ZL24btNormalizeAnglePositivef(float %angle) #1 {
entry:
  %angle.addr = alloca float, align 4
  store float %angle, float* %angle.addr, align 4
  %0 = load float, float* %angle.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  %add = fadd float %call, 0x401921FB60000000
  %call1 = call float @_Z6btFmodff(float %add, float 0x401921FB60000000)
  ret float %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %fmod = frem float %0, %1
  ret float %fmod
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK14btAngularLimit7isLimitEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_solveLimit = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  %0 = load i8, i8* %m_solveLimit, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

declare float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit*) #3

declare float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btAcosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4
  %call = call float @acosf(float %2) #7
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btHingeConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btAngularLimit11getSoftnessEv(%class.btAngularLimit* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_softness = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 2
  %0 = load float, float* %m_softness, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN33btHingeAccumulatedAngleConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btHingeConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
