; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Character/btKinematicCharacterController.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Character/btKinematicCharacterController.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btKinematicCharacterController = type <{ %class.btCharacterControllerInterface, float, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, float, float, float, float, float, float, float, float, float, float, float, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btQuaternion, %class.btQuaternion, %class.btAlignedObjectArray.13, i8, [3 x i8], %class.btVector3, float, float, i8, i8, i8, i8, float, %class.btVector3, %class.btVector3, i8, i8, i8, i8 }>
%class.btCharacterControllerInterface = type { %class.btActionInterface }
%class.btActionInterface = type { i32 (...)** }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.16, %union.anon.17, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.16 = type { float }
%union.anon.17 = type { float }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPairCachingGhostObject = type { %class.btGhostObject, %class.btHashedOverlappingPairCache* }
%class.btGhostObject = type { %class.btCollisionObject, %class.btAlignedObjectArray.0 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.4, %struct.btOverlapFilterCallback*, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btOverlappingPairCallback* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.7 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon.7 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.0, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btIDebugDraw = type { i32 (...)** }
%class.btKinematicClosestNotMeConvexResultCallback = type { %"struct.btCollisionWorld::ClosestConvexResultCallback", %class.btCollisionObject*, %class.btVector3, float }
%"struct.btCollisionWorld::ClosestConvexResultCallback" = type { %"struct.btCollisionWorld::ConvexResultCallback", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btCollisionObject* }
%"struct.btCollisionWorld::ConvexResultCallback" = type { i32 (...)**, float, i32, i32 }
%"struct.btCollisionWorld::LocalConvexResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, %class.btVector3, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN30btCharacterControllerInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z9btRadiansf = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN30btKinematicCharacterControllerdlEPv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN16btCollisionWorld13getBroadphaseEv = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv = comdat any

$_ZN16btCollisionWorld15getDispatchInfoEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev = comdat any

$_ZNK17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZeqRK11btTransformS1_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_Z5btPowff = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_Z6btFabsf = comdat any

$_Z5btCosf = comdat any

$_Z4fabsf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector3eqERKS_ = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_Z25shortestArcQuatNormalize2R9btVector3S0_ = comdat any

$_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf = comdat any

$_ZN17btActionInterfaceC2Ev = comdat any

$_ZN30btCharacterControllerInterfaceD2Ev = comdat any

$_ZN30btCharacterControllerInterfaceD0Ev = comdat any

$_ZN17btActionInterfaceD2Ev = comdat any

$_ZN17btActionInterfaceD0Ev = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_ = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev = comdat any

$_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackC2Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev = comdat any

$_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld20ConvexResultCallbackD0Ev = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZeqRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_Z15shortestArcQuatRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTS30btCharacterControllerInterface = comdat any

$_ZTS17btActionInterface = comdat any

$_ZTI17btActionInterface = comdat any

$_ZTI30btCharacterControllerInterface = comdat any

$_ZTV30btCharacterControllerInterface = comdat any

$_ZTV17btActionInterface = comdat any

$_ZTV43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTS43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTSN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld20ConvexResultCallbackE = comdat any

$_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTI43btKinematicClosestNotMeConvexResultCallback = comdat any

$_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = comdat any

$_ZTVN16btCollisionWorld20ConvexResultCallbackE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV30btKinematicCharacterController = hidden unnamed_addr constant { [21 x i8*] } { [21 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btKinematicCharacterController to i8*), i8* bitcast (%class.btKinematicCharacterController* (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD1Ev to i8*), i8* bitcast (void (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD0Ev to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)* @_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btIDebugDraw*)* @_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3 to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*, float)* @_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)* @_ZN30btKinematicCharacterController5resetEP16btCollisionWorld to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController4warpERK9btVector3 to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)* @_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)* @_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf to i8*), i8* bitcast (i1 (%class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController7canJumpEv to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController4jumpERK9btVector3 to i8*), i8* bitcast (i1 (%class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController8onGroundEv to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, i1)* @_ZN30btKinematicCharacterController16setUpInterpolateEb to i8*), i8* bitcast (i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN30btKinematicCharacterController14needsCollisionEPK17btCollisionObjectS2_ to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController18setAngularVelocityERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController18getAngularVelocityEv to i8*), i8* bitcast (void (%class.btKinematicCharacterController*, %class.btVector3*)* @_ZN30btKinematicCharacterController17setLinearVelocityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btKinematicCharacterController*)* @_ZNK30btKinematicCharacterController17getLinearVelocityEv to i8*)] }, align 4
@_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection = internal global [3 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection = internal global i32 0, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btKinematicCharacterController = hidden constant [33 x i8] c"30btKinematicCharacterController\00", align 1
@_ZTS30btCharacterControllerInterface = linkonce_odr hidden constant [33 x i8] c"30btCharacterControllerInterface\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS17btActionInterface = linkonce_odr hidden constant [20 x i8] c"17btActionInterface\00", comdat, align 1
@_ZTI17btActionInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btActionInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI30btCharacterControllerInterface = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCharacterControllerInterface, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*) }, comdat, align 4
@_ZTI30btKinematicCharacterController = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btKinematicCharacterController, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCharacterControllerInterface to i8*) }, align 4
@_ZTV30btCharacterControllerInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCharacterControllerInterface to i8*), i8* bitcast (%class.btCharacterControllerInterface* (%class.btCharacterControllerInterface*)* @_ZN30btCharacterControllerInterfaceD2Ev to i8*), i8* bitcast (void (%class.btCharacterControllerInterface*)* @_ZN30btCharacterControllerInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV17btActionInterface = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*), i8* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to i8*), i8* bitcast (void (%class.btActionInterface*)* @_ZN17btActionInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI43btKinematicClosestNotMeConvexResultCallback to i8*), i8* bitcast (%class.btKinematicClosestNotMeConvexResultCallback* (%class.btKinematicClosestNotMeConvexResultCallback*)* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%class.btKinematicClosestNotMeConvexResultCallback*)* @_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%class.btKinematicClosestNotMeConvexResultCallback*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTS43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden constant [46 x i8] c"43btKinematicClosestNotMeConvexResultCallback\00", comdat, align 1
@_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant [50 x i8] c"N16btCollisionWorld27ClosestConvexResultCallbackE\00", comdat, align 1
@_ZTSN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant [43 x i8] c"N16btCollisionWorld20ConvexResultCallbackE\00", comdat, align 1
@_ZTIN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([43 x i8], [43 x i8]* @_ZTSN16btCollisionWorld20ConvexResultCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([50 x i8], [50 x i8]* @_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTI43btKinematicClosestNotMeConvexResultCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([46 x i8], [46 x i8]* @_ZTS43btKinematicClosestNotMeConvexResultCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*) }, comdat, align 4
@_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ClosestConvexResultCallback"* (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ClosestConvexResultCallback"*)* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::LocalConvexResult"*, i1)* @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb to i8*)] }, comdat, align 4
@_ZTVN16btCollisionWorld20ConvexResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld20ConvexResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::ConvexResultCallback"* (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ConvexResultCallback"*)* @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::ConvexResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btKinematicCharacterController.cpp, i8* null }]

@_ZN30btKinematicCharacterControllerC1EP24btPairCachingGhostObjectP13btConvexShapefRK9btVector3 = hidden unnamed_addr alias %class.btKinematicCharacterController* (%class.btKinematicCharacterController*, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, %class.btVector3*), %class.btKinematicCharacterController* (%class.btKinematicCharacterController*, %class.btPairCachingGhostObject*, %class.btConvexShape*, float, %class.btVector3*)* @_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefRK9btVector3
@_ZN30btKinematicCharacterControllerD1Ev = hidden unnamed_addr alias %class.btKinematicCharacterController* (%class.btKinematicCharacterController*), %class.btKinematicCharacterController* (%class.btKinematicCharacterController*)* @_ZN30btKinematicCharacterControllerD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %ref.tmp2, align 4
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %magnitude = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %magnitude, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %magnitude)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerC2EP24btPairCachingGhostObjectP13btConvexShapefRK9btVector3(%class.btKinematicCharacterController* returned %this, %class.btPairCachingGhostObject* %ghostObject, %class.btConvexShape* %convexShape, float %stepHeight, %class.btVector3* nonnull align 4 dereferenceable(16) %up) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %ghostObject.addr = alloca %class.btPairCachingGhostObject*, align 4
  %convexShape.addr = alloca %class.btConvexShape*, align 4
  %stepHeight.addr = alloca float, align 4
  %up.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btPairCachingGhostObject* %ghostObject, %class.btPairCachingGhostObject** %ghostObject.addr, align 4
  store %class.btConvexShape* %convexShape, %class.btConvexShape** %convexShape.addr, align 4
  store float %stepHeight, float* %stepHeight.addr, align 4
  store %class.btVector3* %up, %class.btVector3** %up.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to %class.btCharacterControllerInterface*
  %call = call %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceC2Ev(%class.btCharacterControllerInterface* %0)
  %1 = bitcast %class.btKinematicCharacterController* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [21 x i8*] }, { [21 x i8*] }* @_ZTV30btKinematicCharacterController, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_walkDirection)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalizedDirection)
  %m_AngVel = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_AngVel)
  %m_jumpPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_jumpPosition)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_currentPosition)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_targetPosition)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %call8 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_currentOrientation)
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %call9 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %m_targetOrientation)
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %call10 = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* %m_manifoldArray)
  %m_touchingNormal = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 29
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_touchingNormal)
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_up)
  %m_jumpAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 38
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_jumpAxis)
  %2 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %ghostObject.addr, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  store %class.btPairCachingGhostObject* %2, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %m_up14 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 1.000000e+00, float* %ref.tmp16, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_up14, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %m_jumpAxis17 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 38
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  store float 1.000000e+00, float* %ref.tmp20, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_jumpAxis17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %3 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  call void @_ZN30btKinematicCharacterController5setUpERK9btVector3(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load float, float* %stepHeight.addr, align 4
  call void @_ZN30btKinematicCharacterController13setStepHeightEf(%class.btKinematicCharacterController* %this1, float %4)
  %m_addedMargin = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  store float 0x3F947AE140000000, float* %m_addedMargin, align 4
  %m_walkDirection21 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %ref.tmp22, align 4
  store float 0.000000e+00, float* %ref.tmp23, align 4
  store float 0.000000e+00, float* %ref.tmp24, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_walkDirection21, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %m_AngVel25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %ref.tmp26, align 4
  store float 0.000000e+00, float* %ref.tmp27, align 4
  store float 0.000000e+00, float* %ref.tmp28, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_AngVel25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 34
  store i8 1, i8* %m_useGhostObjectSweepTest, align 2
  %m_turnAngle = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 14
  store float 0.000000e+00, float* %m_turnAngle, align 4
  %5 = load %class.btConvexShape*, %class.btConvexShape** %convexShape.addr, align 4
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  store %class.btConvexShape* %5, %class.btConvexShape** %m_convexShape, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 35
  store i8 1, i8* %m_useWalkDirection, align 1
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  store float 0.000000e+00, float* %m_velocityTimeInterval, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalVelocity, align 4
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_verticalOffset, align 4
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  store float 0x403D666660000000, float* %m_gravity, align 4
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  store float 5.500000e+01, float* %m_fallSpeed, align 4
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  store float 1.000000e+01, float* %m_jumpSpeed, align 4
  %m_jumpSpeed29 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  %6 = load float, float* %m_jumpSpeed29, align 4
  %m_SetjumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 9
  store float %6, float* %m_SetjumpSpeed, align 4
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  store i8 0, i8* %m_wasOnGround, align 4
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  store i8 0, i8* %m_wasJumping, align 1
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 39
  store i8 1, i8* %m_interpolateUp, align 4
  %call30 = call float @_Z9btRadiansf(float 4.500000e+01)
  call void @_ZN30btKinematicCharacterController11setMaxSlopeEf(%class.btKinematicCharacterController* %this1, float %call30)
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  store float 0.000000e+00, float* %m_currentStepOffset, align 4
  %m_maxPenetrationDepth = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float 0x3FC99999A0000000, float* %m_maxPenetrationDepth, align 4
  %full_drop = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 40
  store i8 0, i8* %full_drop, align 1
  %bounce_fix = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 41
  store i8 0, i8* %bounce_fix, align 2
  %m_linearDamping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  store float 0.000000e+00, float* %m_linearDamping, align 4
  %m_angularDamping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  store float 0.000000e+00, float* %m_angularDamping, align 4
  ret %class.btKinematicCharacterController* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceC2Ev(%class.btCharacterControllerInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCharacterControllerInterface*, align 4
  store %class.btCharacterControllerInterface* %this, %class.btCharacterControllerInterface** %this.addr, align 4
  %this1 = load %class.btCharacterControllerInterface*, %class.btCharacterControllerInterface** %this.addr, align 4
  %0 = bitcast %class.btCharacterControllerInterface* %this1 to %class.btActionInterface*
  %call = call %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* %0) #7
  %1 = bitcast %class.btCharacterControllerInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV30btCharacterControllerInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btCharacterControllerInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController5setUpERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %up) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %up.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %up, %class.btVector3** %up.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %0)
  %cmp = fcmp ogt float %call, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %1 = load float, float* %m_gravity, align 4
  %cmp2 = fcmp ogt float %1, 0.000000e+00
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_gravity4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %2 = load float, float* %m_gravity4, align 4
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %3)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  call void @_ZN30btKinematicCharacterController10setGravityERK9btVector3(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %4 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  call void @_ZN30btKinematicCharacterController11setUpVectorERK9btVector3(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController13setStepHeightEf(%class.btKinematicCharacterController* %this, float %h) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %h.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %h, float* %h.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %h.addr, align 4
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  store float %0, float* %m_stepHeight, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController11setMaxSlopeEf(%class.btKinematicCharacterController* %this, float %slopeRadians) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %slopeRadians.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %slopeRadians, float* %slopeRadians.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %slopeRadians.addr, align 4
  %m_maxSlopeRadians = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  store float %0, float* %m_maxSlopeRadians, align 4
  %1 = load float, float* %slopeRadians.addr, align 4
  %call = call float @_Z5btCosf(float %1)
  %m_maxSlopeCosine = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 12
  store float %call, float* %m_maxSlopeCosine, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z9btRadiansf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %mul = fmul float %0, 0x3F91DF46A0000000
  ret float %mul
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerD2Ev(%class.btKinematicCharacterController* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [21 x i8*] }, { [21 x i8*] }* @_ZTV30btKinematicCharacterController, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %call = call %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* %m_manifoldArray) #7
  %1 = bitcast %class.btKinematicCharacterController* %this1 to %class.btCharacterControllerInterface*
  %call2 = call %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceD2Ev(%class.btCharacterControllerInterface* %1) #7
  ret %class.btKinematicCharacterController* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.13* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this1)
  ret %class.btAlignedObjectArray.13* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterControllerD0Ev(%class.btKinematicCharacterController* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btKinematicCharacterController* @_ZN30btKinematicCharacterControllerD1Ev(%class.btKinematicCharacterController* %this1) #7
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i8*
  call void @_ZN30btKinematicCharacterControllerdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btKinematicCharacterControllerdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %0 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  ret %class.btPairCachingGhostObject* %0
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %minAabb = alloca %class.btVector3, align 4
  %maxAabb = alloca %class.btVector3, align 4
  %penetration = alloca i8, align 1
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %collisionPair = alloca %struct.btBroadphasePair*, align 4
  %obj0 = alloca %class.btCollisionObject*, align 4
  %obj1 = alloca %class.btCollisionObject*, align 4
  %j = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %directionSign = alloca float, align 4
  %p = alloca i32, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  %dist = alloca float, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca float, align 4
  %newTrans = alloca %class.btTransform, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minAabb)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %maxAabb)
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %2 = bitcast %class.btPairCachingGhostObject* %1 to %class.btCollisionObject*
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %2)
  %3 = bitcast %class.btConvexShape* %0 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %4 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb)
  %5 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call4 = call %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %5)
  %m_ghostObject5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %6 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject5, align 4
  %7 = bitcast %class.btPairCachingGhostObject* %6 to %class.btCollisionObject*
  %call6 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %7)
  %8 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call7 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %8)
  %9 = bitcast %class.btBroadphaseInterface* %call4 to void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable8 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %9, align 4
  %vfn9 = getelementptr inbounds void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable8, i64 4
  %10 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn9, align 4
  call void %10(%class.btBroadphaseInterface* %call4, %struct.btBroadphaseProxy* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb, %class.btDispatcher* %call7)
  store i8 0, i8* %penetration, align 1
  %11 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call10 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %11)
  %m_ghostObject11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %12 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject11, align 4
  %call12 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %12)
  %13 = bitcast %class.btHashedOverlappingPairCache* %call12 to %class.btOverlappingPairCache*
  %14 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %14)
  %15 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call14 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %15)
  %16 = bitcast %class.btDispatcher* %call10 to void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)***
  %vtable15 = load void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)**, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*** %16, align 4
  %vfn16 = getelementptr inbounds void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)** %vtable15, i64 8
  %17 = load void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)*, void (%class.btDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)** %vfn16, align 4
  call void %17(%class.btDispatcher* %call10, %class.btOverlappingPairCache* %13, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %call13, %class.btDispatcher* %call14)
  %m_ghostObject17 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %18 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject17, align 4
  %19 = bitcast %class.btPairCachingGhostObject* %18 to %class.btCollisionObject*
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %19)
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call18)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %20 = bitcast %class.btVector3* %m_currentPosition to i8*
  %21 = bitcast %class.btVector3* %call19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc76, %entry
  %22 = load i32, i32* %i, align 4
  %m_ghostObject20 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %23 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject20, align 4
  %call21 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %23)
  %24 = bitcast %class.btHashedOverlappingPairCache* %call21 to i32 (%class.btHashedOverlappingPairCache*)***
  %vtable22 = load i32 (%class.btHashedOverlappingPairCache*)**, i32 (%class.btHashedOverlappingPairCache*)*** %24, align 4
  %vfn23 = getelementptr inbounds i32 (%class.btHashedOverlappingPairCache*)*, i32 (%class.btHashedOverlappingPairCache*)** %vtable22, i64 9
  %25 = load i32 (%class.btHashedOverlappingPairCache*)*, i32 (%class.btHashedOverlappingPairCache*)** %vfn23, align 4
  %call24 = call i32 %25(%class.btHashedOverlappingPairCache* %call21)
  %cmp = icmp slt i32 %22, %call24
  br i1 %cmp, label %for.body, label %for.end78

for.body:                                         ; preds = %for.cond
  %m_manifoldArray = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %m_manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_ghostObject25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %26 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject25, align 4
  %call26 = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %26)
  %27 = bitcast %class.btHashedOverlappingPairCache* %call26 to %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)***
  %vtable27 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*** %27, align 4
  %vfn28 = getelementptr inbounds %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vtable27, i64 7
  %28 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vfn28, align 4
  %call29 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* %28(%class.btHashedOverlappingPairCache* %call26)
  %29 = load i32, i32* %i, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.4* %call29, i32 %29)
  store %struct.btBroadphasePair* %call30, %struct.btBroadphasePair** %collisionPair, align 4
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  %31 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %31, i32 0, i32 0
  %32 = load i8*, i8** %m_clientObject, align 4
  %33 = bitcast i8* %32 to %class.btCollisionObject*
  store %class.btCollisionObject* %33, %class.btCollisionObject** %obj0, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_clientObject31 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %35, i32 0, i32 0
  %36 = load i8*, i8** %m_clientObject31, align 4
  %37 = bitcast i8* %36 to %class.btCollisionObject*
  store %class.btCollisionObject* %37, %class.btCollisionObject** %obj1, align 4
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %obj0, align 4
  %tobool = icmp ne %class.btCollisionObject* %38, null
  br i1 %tobool, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %for.body
  %39 = load %class.btCollisionObject*, %class.btCollisionObject** %obj0, align 4
  %call32 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %39)
  br i1 %call32, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %land.lhs.true, %for.body
  %40 = load %class.btCollisionObject*, %class.btCollisionObject** %obj1, align 4
  %tobool33 = icmp ne %class.btCollisionObject* %40, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end

land.lhs.true34:                                  ; preds = %lor.lhs.false
  %41 = load %class.btCollisionObject*, %class.btCollisionObject** %obj1, align 4
  %call35 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %41)
  br i1 %call35, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true34, %land.lhs.true
  br label %for.inc76

if.end:                                           ; preds = %land.lhs.true34, %lor.lhs.false
  %42 = load %class.btCollisionObject*, %class.btCollisionObject** %obj0, align 4
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %obj1, align 4
  %44 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable36 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %44, align 4
  %vfn37 = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable36, i64 14
  %45 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn37, align 4
  %call38 = call zeroext i1 %45(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %42, %class.btCollisionObject* %43)
  br i1 %call38, label %if.end40, label %if.then39

if.then39:                                        ; preds = %if.end
  br label %for.inc76

if.end40:                                         ; preds = %if.end
  %46 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %46, i32 0, i32 2
  %47 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %tobool41 = icmp ne %class.btCollisionAlgorithm* %47, null
  br i1 %tobool41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.end40
  %48 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_algorithm43 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %48, i32 0, i32 2
  %49 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm43, align 4
  %m_manifoldArray44 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %50 = bitcast %class.btCollisionAlgorithm* %49 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)***
  %vtable45 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*** %50, align 4
  %vfn46 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vtable45, i64 4
  %51 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.13*)** %vfn46, align 4
  call void %51(%class.btCollisionAlgorithm* %49, %class.btAlignedObjectArray.13* nonnull align 4 dereferenceable(17) %m_manifoldArray44)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %if.end40
  store i32 0, i32* %j, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc73, %if.end47
  %52 = load i32, i32* %j, align 4
  %m_manifoldArray49 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %call50 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %m_manifoldArray49)
  %cmp51 = icmp slt i32 %52, %call50
  br i1 %cmp51, label %for.body52, label %for.end75

for.body52:                                       ; preds = %for.cond48
  %m_manifoldArray53 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 26
  %53 = load i32, i32* %j, align 4
  %call54 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %m_manifoldArray53, i32 %53)
  %54 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call54, align 4
  store %class.btPersistentManifold* %54, %class.btPersistentManifold** %manifold, align 4
  %55 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call55 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %55)
  %m_ghostObject56 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %56 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject56, align 4
  %57 = bitcast %class.btPairCachingGhostObject* %56 to %class.btCollisionObject*
  %cmp57 = icmp eq %class.btCollisionObject* %call55, %57
  %58 = zext i1 %cmp57 to i64
  %cond = select i1 %cmp57, float -1.000000e+00, float 1.000000e+00
  store float %cond, float* %directionSign, align 4
  store i32 0, i32* %p, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc, %for.body52
  %59 = load i32, i32* %p, align 4
  %60 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call59 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %60)
  %cmp60 = icmp slt i32 %59, %call59
  br i1 %cmp60, label %for.body61, label %for.end

for.body61:                                       ; preds = %for.cond58
  %61 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %62 = load i32, i32* %p, align 4
  %call62 = call nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %61, i32 %62)
  store %class.btManifoldPoint* %call62, %class.btManifoldPoint** %pt, align 4
  %63 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %call63 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %63)
  store float %call63, float* %dist, align 4
  %64 = load float, float* %dist, align 4
  %m_maxPenetrationDepth = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %65 = load float, float* %m_maxPenetrationDepth, align 4
  %fneg = fneg float %65
  %cmp64 = fcmp olt float %64, %fneg
  br i1 %cmp64, label %if.then65, label %if.else

if.then65:                                        ; preds = %for.body61
  %66 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %66, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB, float* nonnull align 4 dereferenceable(4) %directionSign)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %dist)
  store float 0x3FC99999A0000000, float* %ref.tmp69, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %m_currentPosition70 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_currentPosition70, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp66)
  store i8 1, i8* %penetration, align 1
  br label %if.end72

if.else:                                          ; preds = %for.body61
  br label %if.end72

if.end72:                                         ; preds = %if.else, %if.then65
  br label %for.inc

for.inc:                                          ; preds = %if.end72
  %67 = load i32, i32* %p, align 4
  %inc = add nsw i32 %67, 1
  store i32 %inc, i32* %p, align 4
  br label %for.cond58

for.end:                                          ; preds = %for.cond58
  br label %for.inc73

for.inc73:                                        ; preds = %for.end
  %68 = load i32, i32* %j, align 4
  %inc74 = add nsw i32 %68, 1
  store i32 %inc74, i32* %j, align 4
  br label %for.cond48

for.end75:                                        ; preds = %for.cond48
  br label %for.inc76

for.inc76:                                        ; preds = %for.end75, %if.then39, %if.then
  %69 = load i32, i32* %i, align 4
  %inc77 = add nsw i32 %69, 1
  store i32 %inc77, i32* %i, align 4
  br label %for.cond

for.end78:                                        ; preds = %for.cond
  %m_ghostObject79 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %70 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject79, align 4
  %71 = bitcast %class.btPairCachingGhostObject* %70 to %class.btCollisionObject*
  %call80 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %71)
  %call81 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %newTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call80)
  %m_currentPosition82 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %newTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition82)
  %m_ghostObject83 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %72 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject83, align 4
  %73 = bitcast %class.btPairCachingGhostObject* %72 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %73, %class.btTransform* nonnull align 4 dereferenceable(64) %newTrans)
  %74 = load i8, i8* %penetration, align 1
  %tobool84 = trunc i8 %74 to i1
  ret i1 %tobool84
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4
  ret %class.btBroadphaseInterface* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4
  ret %struct.btBroadphaseProxy* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  ret %class.btDispatcher* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPairCachingGhostObject*, align 4
  store %class.btPairCachingGhostObject* %this, %class.btPairCachingGhostObject** %this.addr, align 4
  %this1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %this.addr, align 4
  %m_hashPairCache = getelementptr inbounds %class.btPairCachingGhostObject, %class.btPairCachingGhostObject* %this1, i32 0, i32 1
  %0 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %m_hashPairCache, align 4
  ret %class.btHashedOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 3
  ret %struct.btDispatcherInfo* %m_dispatchInfo
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.13* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.13* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %world) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %world.addr = alloca %class.btCollisionWorld*, align 4
  %stepHeight = alloca float, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %xform = alloca %class.btTransform*, align 4
  %numPenetrationLoops = alloca i32, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %world, %class.btCollisionWorld** %world.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  store float 0.000000e+00, float* %stepHeight, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %0 = load float, float* %m_verticalVelocity, align 4
  %conv = fpext float %0 to double
  %cmp = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %1 = load float, float* %m_stepHeight, align 4
  store float %1, float* %stepHeight, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %m_currentPosition4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up, float* nonnull align 4 dereferenceable(4) %stepHeight)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %m_jumpAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 38
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %2 = load float, float* %m_verticalOffset, align 4
  %cmp8 = fcmp ogt float %2, 0.000000e+00
  br i1 %cmp8, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %m_verticalOffset9 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %3 = load float, float* %m_verticalOffset9, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %3, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_jumpAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %4 = bitcast %class.btVector3* %m_targetPosition to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_targetPosition10 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %6 = bitcast %class.btVector3* %m_currentPosition11 to i8*
  %7 = bitcast %class.btVector3* %m_targetPosition10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_targetPosition12 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition12)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %start, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_currentOrientation)
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %end, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_targetOrientation)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %8 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %9 = bitcast %class.btPairCachingGhostObject* %8 to %class.btCollisionObject*
  %m_up14 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up14)
  %m_maxSlopeCosine = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 12
  %10 = load float, float* %m_maxSlopeCosine, align 4
  %call15 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13, float %10)
  %call16 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %11 = bitcast %class.btPairCachingGhostObject* %call16 to %class.btCollisionObject*
  %call17 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %11)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call17, i32 0, i32 1
  %12 = load i32, i32* %m_collisionFilterGroup, align 4
  %13 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup18 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %13, i32 0, i32 2
  store i32 %12, i32* %m_collisionFilterGroup18, align 4
  %call19 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %14 = bitcast %class.btPairCachingGhostObject* %call19 to %class.btCollisionObject*
  %call20 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %14)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call20, i32 0, i32 2
  %15 = load i32, i32* %m_collisionFilterMask, align 4
  %16 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask21 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %16, i32 0, i32 3
  store i32 %15, i32* %m_collisionFilterMask21, align 4
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 34
  %17 = load i8, i8* %m_useGhostObjectSweepTest, align 2
  %tobool = trunc i8 %17 to i1
  br i1 %tobool, label %if.then22, label %if.else

if.then22:                                        ; preds = %cond.end
  %m_ghostObject23 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %18 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject23, align 4
  %19 = bitcast %class.btPairCachingGhostObject* %18 to %class.btGhostObject*
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %20 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4
  %21 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %22 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %22)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call24, i32 0, i32 9
  %23 = load float, float* %m_allowedCcdPenetration, align 4
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %19, %class.btConvexShape* %20, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %21, float %23)
  br label %if.end28

if.else:                                          ; preds = %cond.end
  %24 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4
  %m_convexShape25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %25 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape25, align 4
  %26 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %27 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %27)
  %m_allowedCcdPenetration27 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call26, i32 0, i32 9
  %28 = load float, float* %m_allowedCcdPenetration27, align 4
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %24, %class.btConvexShape* %25, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %26, float %28)
  br label %if.end28

if.end28:                                         ; preds = %if.else, %if.then22
  %29 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call29 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %29)
  br i1 %call29, label %land.lhs.true, label %if.else77

land.lhs.true:                                    ; preds = %if.end28
  %m_ghostObject30 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %30 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject30, align 4
  %31 = bitcast %class.btPairCachingGhostObject* %30 to %class.btCollisionObject*
  %call31 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %31)
  br i1 %call31, label %land.lhs.true32, label %if.else77

land.lhs.true32:                                  ; preds = %land.lhs.true
  %m_ghostObject33 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %32 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject33, align 4
  %33 = bitcast %class.btPairCachingGhostObject* %32 to %class.btCollisionObject*
  %34 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %34, i32 0, i32 5
  %35 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %36 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %36, align 4
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 14
  %37 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call34 = call zeroext i1 %37(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %33, %class.btCollisionObject* %35)
  br i1 %call34, label %if.then35, label %if.else77

if.then35:                                        ; preds = %land.lhs.true32
  %38 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %38, i32 0, i32 3
  %m_up36 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_hitNormalWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up36)
  %conv38 = fpext float %call37 to double
  %cmp39 = fcmp ogt double %conv38, 0.000000e+00
  br i1 %cmp39, label %if.then40, label %if.end53

if.then40:                                        ; preds = %if.then35
  %39 = load float, float* %stepHeight, align 4
  %40 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %40, i32 0, i32 1
  %41 = load float, float* %m_closestHitFraction, align 4
  %mul = fmul float %39, %41
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  store float %mul, float* %m_currentStepOffset, align 4
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 39
  %42 = load i8, i8* %m_interpolateUp, align 4
  %tobool41 = trunc i8 %42 to i1
  %conv42 = zext i1 %tobool41 to i32
  %cmp43 = icmp eq i32 %conv42, 1
  br i1 %cmp43, label %if.then44, label %if.else49

if.then44:                                        ; preds = %if.then40
  %m_currentPosition45 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_currentPosition46 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition47 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %43 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction48 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %43, i32 0, i32 1
  %44 = load float, float* %m_closestHitFraction48, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition45, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition46, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition47, float %44)
  br label %if.end52

if.else49:                                        ; preds = %if.then40
  %m_targetPosition50 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition51 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %45 = bitcast %class.btVector3* %m_currentPosition51 to i8*
  %46 = bitcast %class.btVector3* %m_targetPosition50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false)
  br label %if.end52

if.end52:                                         ; preds = %if.else49, %if.then44
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then35
  %m_ghostObject54 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %47 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject54, align 4
  %48 = bitcast %class.btPairCachingGhostObject* %47 to %class.btCollisionObject*
  %call55 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %48)
  store %class.btTransform* %call55, %class.btTransform** %xform, align 4
  %49 = load %class.btTransform*, %class.btTransform** %xform, align 4
  %m_currentPosition56 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %49, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition56)
  %m_ghostObject57 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %50 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject57, align 4
  %51 = bitcast %class.btPairCachingGhostObject* %50 to %class.btCollisionObject*
  %52 = load %class.btTransform*, %class.btTransform** %xform, align 4
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %51, %class.btTransform* nonnull align 4 dereferenceable(64) %52)
  store i32 0, i32* %numPenetrationLoops, align 4
  %m_touchingContact = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 0, i8* %m_touchingContact, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end62, %if.end53
  %53 = load %class.btCollisionWorld*, %class.btCollisionWorld** %world.addr, align 4
  %call58 = call zeroext i1 @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %53)
  br i1 %call58, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %54 = load i32, i32* %numPenetrationLoops, align 4
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %numPenetrationLoops, align 4
  %m_touchingContact59 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 1, i8* %m_touchingContact59, align 4
  %55 = load i32, i32* %numPenetrationLoops, align 4
  %cmp60 = icmp sgt i32 %55, 4
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %while.body
  br label %while.end

if.end62:                                         ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %if.then61, %while.cond
  %m_ghostObject63 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %56 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject63, align 4
  %57 = bitcast %class.btPairCachingGhostObject* %56 to %class.btCollisionObject*
  %call64 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %57)
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call64)
  %m_targetPosition66 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %58 = bitcast %class.btVector3* %m_targetPosition66 to i8*
  %59 = bitcast %class.btVector3* %call65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false)
  %m_targetPosition67 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition68 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %60 = bitcast %class.btVector3* %m_currentPosition68 to i8*
  %61 = bitcast %class.btVector3* %m_targetPosition67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false)
  %m_verticalOffset69 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %62 = load float, float* %m_verticalOffset69, align 4
  %cmp70 = fcmp ogt float %62, 0.000000e+00
  br i1 %cmp70, label %if.then71, label %if.end76

if.then71:                                        ; preds = %while.end
  %m_verticalOffset72 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_verticalOffset72, align 4
  %m_verticalVelocity73 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalVelocity73, align 4
  %m_stepHeight74 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %63 = load float, float* %m_stepHeight74, align 4
  %m_currentStepOffset75 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  store float %63, float* %m_currentStepOffset75, align 4
  br label %if.end76

if.end76:                                         ; preds = %if.then71, %while.end
  br label %if.end81

if.else77:                                        ; preds = %land.lhs.true32, %land.lhs.true, %if.end28
  %64 = load float, float* %stepHeight, align 4
  %m_currentStepOffset78 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  store float %64, float* %m_currentStepOffset78, align 4
  %m_targetPosition79 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition80 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %65 = bitcast %class.btVector3* %m_currentPosition80 to i8*
  %66 = bitcast %class.btVector3* %m_targetPosition79 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false)
  br label %if.end81

if.end81:                                         ; preds = %if.else77, %if.end76
  %call82 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* returned %this, %class.btCollisionObject* %me, %class.btVector3* nonnull align 4 dereferenceable(16) %up, float %minSlopeDot) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  %me.addr = alloca %class.btCollisionObject*, align 4
  %up.addr = alloca %class.btVector3*, align 4
  %minSlopeDot.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  store %class.btCollisionObject* %me, %class.btCollisionObject** %me.addr, align 4
  store %class.btVector3* %up, %class.btVector3** %up.addr, align 4
  store float %minSlopeDot, float* %minSlopeDot.addr, align 4
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %call10 = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %1 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV43btKinematicClosestNotMeConvexResultCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_me = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %me.addr, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_me, align 4
  %m_up = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  %4 = bitcast %class.btVector3* %m_up to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_minSlopeDot = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 3
  %6 = load float, float* %minSlopeDot.addr, align 4
  store float %6, float* %m_minSlopeDot, align 4
  ret %class.btKinematicClosestNotMeConvexResultCallback* %this1
}

declare void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject*, %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16), float) #4

declare void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld*, %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16), float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  %0 = load float, float* %m_closestHitFraction, align 4
  %cmp = fcmp olt float %0, 1.000000e+00
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store float %rt, float* %rt.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %rt.addr, align 4
  %sub = fsub float 1.000000e+00, %0
  store float %sub, float* %s, align 4
  %1 = load float, float* %s, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %1, %3
  %4 = load float, float* %rt.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %6 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %4, %6
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4
  %7 = load float, float* %s, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %9 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %7, %9
  %10 = load float, float* %rt.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %12 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %10, %12
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4
  %13 = load float, float* %s, align 4
  %14 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %15 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %13, %15
  %16 = load float, float* %rt.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %18 = load float, float* %arrayidx20, align 4
  %mul21 = fmul float %16, %18
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %0) #7
  ret %class.btKinematicClosestNotMeConvexResultCallback* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btKinematicCharacterController14needsCollisionEPK17btCollisionObjectS2_(%class.btKinematicCharacterController* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %collides = alloca i8, align 1
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call = call %struct.btBroadphaseProxy* @_ZNK17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %0)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call, i32 0, i32 1
  %1 = load i32, i32* %m_collisionFilterGroup, align 4
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call2 = call %struct.btBroadphaseProxy* @_ZNK17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %2)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call2, i32 0, i32 2
  %3 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %1, %3
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %4 = load i8, i8* %collides, align 1
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call3 = call %struct.btBroadphaseProxy* @_ZNK17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %5)
  %m_collisionFilterGroup4 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call3, i32 0, i32 1
  %6 = load i32, i32* %m_collisionFilterGroup4, align 4
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call5 = call %struct.btBroadphaseProxy* @_ZNK17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %7)
  %m_collisionFilterMask6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call5, i32 0, i32 2
  %8 = load i32, i32* %m_collisionFilterMask6, align 4
  %and7 = and i32 %6, %8
  %tobool8 = icmp ne i32 %and7, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %9 = phi i1 [ false, %entry ], [ %tobool8, %land.rhs ]
  %frombool9 = zext i1 %9 to i8
  store i8 %frombool9, i8* %collides, align 1
  %10 = load i8, i8* %collides, align 1
  %tobool10 = trunc i8 %10 to i1
  ret i1 %tobool10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZNK17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4
  ret %struct.btBroadphaseProxy* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormal, float %tangentMag, float %normalMag) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %hitNormal.addr = alloca %class.btVector3*, align 4
  %tangentMag.addr = alloca float, align 4
  %normalMag.addr = alloca float, align 4
  %movementDirection = alloca %class.btVector3, align 4
  %movementLength = alloca float, align 4
  %reflectDir = alloca %class.btVector3, align 4
  %parallelDir = alloca %class.btVector3, align 4
  %perpindicularDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %perpComponent = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %hitNormal, %class.btVector3** %hitNormal.addr, align 4
  store float %tangentMag, float* %tangentMag.addr, align 4
  store float %normalMag, float* %normalMag.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %movementDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %movementDirection)
  store float %call, float* %movementLength, align 4
  %0 = load float, float* %movementLength, align 4
  %cmp = fcmp ogt float %0, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %movementDirection)
  %1 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4
  call void @_ZN30btKinematicCharacterController26computeReflectionDirectionERK9btVector3S2_(%class.btVector3* sret align 4 %reflectDir, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %movementDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %reflectDir)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %parallelDir)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %perpindicularDir)
  %2 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4
  call void @_ZN30btKinematicCharacterController17parallelComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %reflectDir, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btVector3* %parallelDir to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %hitNormal.addr, align 4
  call void @_ZN30btKinematicCharacterController22perpindicularComponentERK9btVector3S2_(%class.btVector3* sret align 4 %ref.tmp6, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %reflectDir, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %6 = bitcast %class.btVector3* %perpindicularDir to i8*
  %7 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_currentPosition7 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %8 = bitcast %class.btVector3* %m_targetPosition8 to i8*
  %9 = bitcast %class.btVector3* %m_currentPosition7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %10 = load float, float* %normalMag.addr, align 4
  %conv = fpext float %10 to double
  %cmp9 = fcmp une double %conv, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  %11 = load float, float* %normalMag.addr, align 4
  %12 = load float, float* %movementLength, align 4
  %mul = fmul float %11, %12
  store float %mul, float* %ref.tmp11, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %perpComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %perpindicularDir, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %m_targetPosition12 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_targetPosition12, %class.btVector3* nonnull align 4 dereferenceable(16) %perpComponent)
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  br label %if.end14

if.else:                                          ; preds = %entry
  br label %if.end14

if.end14:                                         ; preds = %if.else, %if.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %walkMove) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %walkMove.addr = alloca %class.btVector3*, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %fraction = alloca float, align 4
  %distance2 = alloca float, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %maxIter = alloca i32, align 4
  %sweepDirNegative = alloca %class.btVector3, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %margin = alloca float, align 4
  %currentDir = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store %class.btVector3* %walkMove, %class.btVector3** %walkMove.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %0 = load %class.btVector3*, %class.btVector3** %walkMove.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %1 = bitcast %class.btVector3* %m_targetPosition to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  store float 1.000000e+00, float* %fraction, align 4
  %m_currentPosition4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition5)
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp3)
  store float %call6, float* %distance2, align 4
  store i32 10, i32* %maxIter, align 4
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %3 = load float, float* %fraction, align 4
  %cmp = fcmp ogt float %3, 0x3F847AE140000000
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %4 = load i32, i32* %maxIter, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %maxIter, align 4
  %cmp7 = icmp sgt i32 %4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %5 = phi i1 [ false, %while.cond ], [ %cmp7, %land.rhs ]
  br i1 %5, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_currentPosition8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition8)
  %m_targetPosition9 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition9)
  %m_currentPosition10 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %sweepDirNegative, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition11)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %start, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_currentOrientation)
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %end, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_targetOrientation)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %6 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %7 = bitcast %class.btPairCachingGhostObject* %6 to %class.btCollisionObject*
  %call12 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %sweepDirNegative, float 0.000000e+00)
  %call13 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %8 = bitcast %class.btPairCachingGhostObject* %call13 to %class.btCollisionObject*
  %call14 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %8)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call14, i32 0, i32 1
  %9 = load i32, i32* %m_collisionFilterGroup, align 4
  %10 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup15 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %10, i32 0, i32 2
  store i32 %9, i32* %m_collisionFilterGroup15, align 4
  %call16 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %11 = bitcast %class.btPairCachingGhostObject* %call16 to %class.btCollisionObject*
  %call17 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %11)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call17, i32 0, i32 2
  %12 = load i32, i32* %m_collisionFilterMask, align 4
  %13 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask18 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %13, i32 0, i32 3
  store i32 %12, i32* %m_collisionFilterMask18, align 4
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %14 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4
  %15 = bitcast %class.btConvexShape* %14 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %15, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %16 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call19 = call float %16(%class.btConvexShape* %14)
  store float %call19, float* %margin, align 4
  %m_convexShape20 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %17 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape20, align 4
  %18 = load float, float* %margin, align 4
  %m_addedMargin = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 16
  %19 = load float, float* %m_addedMargin, align 4
  %add = fadd float %18, %19
  %20 = bitcast %class.btConvexShape* %17 to void (%class.btConvexShape*, float)***
  %vtable21 = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %20, align 4
  %vfn22 = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable21, i64 11
  %21 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn22, align 4
  call void %21(%class.btConvexShape* %17, float %add)
  %call23 = call zeroext i1 @_ZeqRK11btTransformS1_(%class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end)
  br i1 %call23, label %if.end31, label %if.then

if.then:                                          ; preds = %while.body
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 34
  %22 = load i8, i8* %m_useGhostObjectSweepTest, align 2
  %tobool = trunc i8 %22 to i1
  br i1 %tobool, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then
  %m_ghostObject25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %23 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject25, align 4
  %24 = bitcast %class.btPairCachingGhostObject* %23 to %class.btGhostObject*
  %m_convexShape26 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %25 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape26, align 4
  %26 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %27 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %27)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call27, i32 0, i32 9
  %28 = load float, float* %m_allowedCcdPenetration, align 4
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %24, %class.btConvexShape* %25, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %26, float %28)
  br label %if.end

if.else:                                          ; preds = %if.then
  %29 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %m_convexShape28 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %30 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape28, align 4
  %31 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %32 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %32)
  %m_allowedCcdPenetration30 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call29, i32 0, i32 9
  %33 = load float, float* %m_allowedCcdPenetration30, align 4
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %29, %class.btConvexShape* %30, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %31, float %33)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then24
  br label %if.end31

if.end31:                                         ; preds = %if.end, %while.body
  %m_convexShape32 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %34 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape32, align 4
  %35 = load float, float* %margin, align 4
  %36 = bitcast %class.btConvexShape* %34 to void (%class.btConvexShape*, float)***
  %vtable33 = load void (%class.btConvexShape*, float)**, void (%class.btConvexShape*, float)*** %36, align 4
  %vfn34 = getelementptr inbounds void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vtable33, i64 11
  %37 = load void (%class.btConvexShape*, float)*, void (%class.btConvexShape*, float)** %vfn34, align 4
  call void %37(%class.btConvexShape* %34, float %35)
  %38 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %38, i32 0, i32 1
  %39 = load float, float* %m_closestHitFraction, align 4
  %40 = load float, float* %fraction, align 4
  %sub = fsub float %40, %39
  store float %sub, float* %fraction, align 4
  %41 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call35 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %41)
  br i1 %call35, label %land.lhs.true, label %if.else56

land.lhs.true:                                    ; preds = %if.end31
  %m_ghostObject36 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %42 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject36, align 4
  %43 = bitcast %class.btPairCachingGhostObject* %42 to %class.btCollisionObject*
  %call37 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %43)
  br i1 %call37, label %land.lhs.true38, label %if.else56

land.lhs.true38:                                  ; preds = %land.lhs.true
  %m_ghostObject39 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %44 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject39, align 4
  %45 = bitcast %class.btPairCachingGhostObject* %44 to %class.btCollisionObject*
  %46 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %46, i32 0, i32 5
  %47 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %48 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable40 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %48, align 4
  %vfn41 = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable40, i64 14
  %49 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn41, align 4
  %call42 = call zeroext i1 %49(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %45, %class.btCollisionObject* %47)
  br i1 %call42, label %if.then43, label %if.else56

if.then43:                                        ; preds = %land.lhs.true38
  %50 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %50, i32 0, i32 3
  call void @_ZN30btKinematicCharacterController36updateTargetPositionBasedOnCollisionERK9btVector3ff(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalWorld, float 0.000000e+00, float 1.000000e+00)
  %m_targetPosition44 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition45 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %currentDir, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition44, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition45)
  %call46 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %currentDir)
  store float %call46, float* %distance2, align 4
  %51 = load float, float* %distance2, align 4
  %cmp47 = fcmp ogt float %51, 0x3E80000000000000
  br i1 %cmp47, label %if.then48, label %if.else54

if.then48:                                        ; preds = %if.then43
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %currentDir)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %call50 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %currentDir, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalizedDirection)
  %cmp51 = fcmp ole float %call50, 0.000000e+00
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %if.then48
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %if.then48
  br label %if.end55

if.else54:                                        ; preds = %if.then43
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end55:                                         ; preds = %if.end53
  br label %if.end59

if.else56:                                        ; preds = %land.lhs.true38, %land.lhs.true, %if.end31
  %m_targetPosition57 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition58 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %52 = bitcast %class.btVector3* %m_currentPosition58 to i8*
  %53 = bitcast %class.btVector3* %m_targetPosition57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 16, i1 false)
  br label %if.end59

if.end59:                                         ; preds = %if.else56, %if.end55
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end59, %if.else54, %if.then52
  %call60 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %while.end
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %cleanup, %land.end
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK11btTransformS1_(%class.btTransform* nonnull align 4 dereferenceable(64) %t1, %class.btTransform* nonnull align 4 dereferenceable(64) %t2) #2 comdat {
entry:
  %t1.addr = alloca %class.btTransform*, align 4
  %t2.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %t1, %class.btTransform** %t1.addr, align 4
  store %class.btTransform* %t2, %class.btTransform** %t2.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %t2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  %call2 = call zeroext i1 @_ZeqRK11btMatrix3x3S1_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call1)
  br i1 %call2, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load %class.btTransform*, %class.btTransform** %t1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = load %class.btTransform*, %class.btTransform** %t2.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  %call5 = call zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %call5, %land.rhs ]
  ret i1 %4
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %dt) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dt.addr = alloca float, align 4
  %start = alloca %class.btTransform, align 4
  %end = alloca %class.btTransform, align 4
  %end_double = alloca %class.btTransform, align 4
  %runonce = alloca i8, align 1
  %orig_position = alloca %class.btVector3, align 4
  %downVelocity = alloca float, align 4
  %step_drop = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %callback = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %callback2 = alloca %class.btKinematicClosestNotMeConvexResultCallback, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %downVelocity2 = alloca float, align 4
  %has_hit = alloca i8, align 1
  %stepHeight = alloca float, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp129 = alloca float, align 4
  %fraction = alloca float, align 4
  %ref.tmp207 = alloca %class.btVector3, align 4
  %ref.tmp209 = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store float %dt, float* %dt.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %start)
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end)
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %end_double)
  store i8 0, i8* %runonce, align 1
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %0 = bitcast %class.btVector3* %orig_position to i8*
  %1 = bitcast %class.btVector3* %m_targetPosition to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %2 = load float, float* %m_verticalVelocity, align 4
  %cmp = fcmp olt float %2, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_verticalVelocity4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %3 = load float, float* %m_verticalVelocity4, align 4
  %fneg = fneg float %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ 0.000000e+00, %cond.false ]
  %4 = load float, float* %dt.addr, align 4
  %mul = fmul float %cond, %4
  store float %mul, float* %downVelocity, align 4
  %m_verticalVelocity5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %5 = load float, float* %m_verticalVelocity5, align 4
  %conv = fpext float %5 to double
  %cmp6 = fcmp ogt double %conv, 0.000000e+00
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  br label %return

if.end:                                           ; preds = %cond.end
  %6 = load float, float* %downVelocity, align 4
  %conv7 = fpext float %6 to double
  %cmp8 = fcmp ogt double %conv7, 0.000000e+00
  br i1 %cmp8, label %land.lhs.true, label %if.end14

land.lhs.true:                                    ; preds = %if.end
  %7 = load float, float* %downVelocity, align 4
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %8 = load float, float* %m_fallSpeed, align 4
  %cmp9 = fcmp ogt float %7, %8
  br i1 %cmp9, label %land.lhs.true10, label %if.end14

land.lhs.true10:                                  ; preds = %land.lhs.true
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %9 = load i8, i8* %m_wasOnGround, align 4
  %tobool = trunc i8 %9 to i1
  br i1 %tobool, label %if.then12, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true10
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  %10 = load i8, i8* %m_wasJumping, align 1
  %tobool11 = trunc i8 %10 to i1
  br i1 %tobool11, label %if.end14, label %if.then12

if.then12:                                        ; preds = %lor.lhs.false, %land.lhs.true10
  %m_fallSpeed13 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %11 = load float, float* %m_fallSpeed13, align 4
  store float %11, float* %downVelocity, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %lor.lhs.false, %land.lhs.true, %if.end
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %m_currentStepOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  %12 = load float, float* %m_currentStepOffset, align 4
  %13 = load float, float* %downVelocity, align 4
  %add = fadd float %12, %13
  store float %add, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %step_drop, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_targetPosition15 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition15, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %14 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %15 = bitcast %class.btPairCachingGhostObject* %14 to %class.btCollisionObject*
  %m_up17 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %m_maxSlopeCosine = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 12
  %16 = load float, float* %m_maxSlopeCosine, align 4
  %call18 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback, %class.btCollisionObject* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up17, float %16)
  %call19 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %17 = bitcast %class.btPairCachingGhostObject* %call19 to %class.btCollisionObject*
  %call20 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %17)
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call20, i32 0, i32 1
  %18 = load i32, i32* %m_collisionFilterGroup, align 4
  %19 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup21 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %19, i32 0, i32 2
  store i32 %18, i32* %m_collisionFilterGroup21, align 4
  %call22 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %20 = bitcast %class.btPairCachingGhostObject* %call22 to %class.btCollisionObject*
  %call23 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %20)
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call23, i32 0, i32 2
  %21 = load i32, i32* %m_collisionFilterMask, align 4
  %22 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask24 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %22, i32 0, i32 3
  store i32 %21, i32* %m_collisionFilterMask24, align 4
  %m_ghostObject25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %23 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject25, align 4
  %24 = bitcast %class.btPairCachingGhostObject* %23 to %class.btCollisionObject*
  %m_up26 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %m_maxSlopeCosine27 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 12
  %25 = load float, float* %m_maxSlopeCosine27, align 4
  %call28 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackC2EP17btCollisionObjectRK9btVector3f(%class.btKinematicClosestNotMeConvexResultCallback* %callback2, %class.btCollisionObject* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up26, float %25)
  %call29 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %26 = bitcast %class.btPairCachingGhostObject* %call29 to %class.btCollisionObject*
  %call30 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %26)
  %m_collisionFilterGroup31 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call30, i32 0, i32 1
  %27 = load i32, i32* %m_collisionFilterGroup31, align 4
  %28 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterGroup32 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %28, i32 0, i32 2
  store i32 %27, i32* %m_collisionFilterGroup32, align 4
  %call33 = call %class.btPairCachingGhostObject* @_ZN30btKinematicCharacterController14getGhostObjectEv(%class.btKinematicCharacterController* %this1)
  %29 = bitcast %class.btPairCachingGhostObject* %call33 to %class.btCollisionObject*
  %call34 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %29)
  %m_collisionFilterMask35 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %call34, i32 0, i32 2
  %30 = load i32, i32* %m_collisionFilterMask35, align 4
  %31 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_collisionFilterMask36 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %31, i32 0, i32 3
  store i32 %30, i32* %m_collisionFilterMask36, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.then125, %if.end14
  br label %while.body

while.body:                                       ; preds = %while.cond
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %start)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %end_double)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %start, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition)
  %m_targetPosition37 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition37)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %start, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_currentOrientation)
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %end, %class.btQuaternion* nonnull align 4 dereferenceable(16) %m_targetOrientation)
  %m_targetPosition39 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp38, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition39, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %end_double, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %m_useGhostObjectSweepTest = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 34
  %32 = load i8, i8* %m_useGhostObjectSweepTest, align 2
  %tobool40 = trunc i8 %32 to i1
  br i1 %tobool40, label %if.then41, label %if.else

if.then41:                                        ; preds = %while.body
  %m_ghostObject42 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %33 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject42, align 4
  %34 = bitcast %class.btPairCachingGhostObject* %33 to %class.btGhostObject*
  %m_convexShape = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %35 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape, align 4
  %36 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %37 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %37)
  %m_allowedCcdPenetration = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call43, i32 0, i32 9
  %38 = load float, float* %m_allowedCcdPenetration, align 4
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %34, %class.btConvexShape* %35, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %36, float %38)
  %39 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call44 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %39)
  br i1 %call44, label %if.end53, label %land.lhs.true45

land.lhs.true45:                                  ; preds = %if.then41
  %m_ghostObject46 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %40 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject46, align 4
  %41 = bitcast %class.btPairCachingGhostObject* %40 to %class.btCollisionObject*
  %call47 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %41)
  br i1 %call47, label %if.then48, label %if.end53

if.then48:                                        ; preds = %land.lhs.true45
  %m_ghostObject49 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %42 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject49, align 4
  %43 = bitcast %class.btPairCachingGhostObject* %42 to %class.btGhostObject*
  %m_convexShape50 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %44 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape50, align 4
  %45 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %46 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call51 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %46)
  %m_allowedCcdPenetration52 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call51, i32 0, i32 9
  %47 = load float, float* %m_allowedCcdPenetration52, align 4
  call void @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf(%class.btGhostObject* %43, %class.btConvexShape* %44, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end_double, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %45, float %47)
  br label %if.end53

if.end53:                                         ; preds = %if.then48, %land.lhs.true45, %if.then41
  br label %if.end66

if.else:                                          ; preds = %while.body
  %48 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %m_convexShape54 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %49 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape54, align 4
  %50 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %51 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %51)
  %m_allowedCcdPenetration56 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call55, i32 0, i32 9
  %52 = load float, float* %m_allowedCcdPenetration56, align 4
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %48, %class.btConvexShape* %49, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %50, float %52)
  %53 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call57 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %53)
  br i1 %call57, label %if.end65, label %land.lhs.true58

land.lhs.true58:                                  ; preds = %if.else
  %m_ghostObject59 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %54 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject59, align 4
  %55 = bitcast %class.btPairCachingGhostObject* %54 to %class.btCollisionObject*
  %call60 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %55)
  br i1 %call60, label %if.then61, label %if.end65

if.then61:                                        ; preds = %land.lhs.true58
  %56 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %m_convexShape62 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 3
  %57 = load %class.btConvexShape*, %class.btConvexShape** %m_convexShape62, align 4
  %58 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %59 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %59)
  %m_allowedCcdPenetration64 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %call63, i32 0, i32 9
  %60 = load float, float* %m_allowedCcdPenetration64, align 4
  call void @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf(%class.btCollisionWorld* %56, %class.btConvexShape* %57, %class.btTransform* nonnull align 4 dereferenceable(64) %start, %class.btTransform* nonnull align 4 dereferenceable(64) %end_double, %"struct.btCollisionWorld::ConvexResultCallback"* nonnull align 4 dereferenceable(16) %58, float %60)
  br label %if.end65

if.end65:                                         ; preds = %if.then61, %land.lhs.true58, %if.else
  br label %if.end66

if.end66:                                         ; preds = %if.end65, %if.end53
  %m_verticalVelocity67 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %61 = load float, float* %m_verticalVelocity67, align 4
  %cmp68 = fcmp olt float %61, 0.000000e+00
  br i1 %cmp68, label %cond.true69, label %cond.false72

cond.true69:                                      ; preds = %if.end66
  %m_verticalVelocity70 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %62 = load float, float* %m_verticalVelocity70, align 4
  %fneg71 = fneg float %62
  br label %cond.end73

cond.false72:                                     ; preds = %if.end66
  br label %cond.end73

cond.end73:                                       ; preds = %cond.false72, %cond.true69
  %cond74 = phi float [ %fneg71, %cond.true69 ], [ 0.000000e+00, %cond.false72 ]
  %63 = load float, float* %dt.addr, align 4
  %mul75 = fmul float %cond74, %63
  store float %mul75, float* %downVelocity2, align 4
  %bounce_fix = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 41
  %64 = load i8, i8* %bounce_fix, align 2
  %tobool76 = trunc i8 %64 to i1
  %conv77 = zext i1 %tobool76 to i32
  %cmp78 = icmp eq i32 %conv77, 1
  br i1 %cmp78, label %if.then79, label %if.else88

if.then79:                                        ; preds = %cond.end73
  %65 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call80 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %65)
  br i1 %call80, label %land.lhs.true83, label %lor.lhs.false81

lor.lhs.false81:                                  ; preds = %if.then79
  %66 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call82 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %66)
  br i1 %call82, label %land.lhs.true83, label %land.end

land.lhs.true83:                                  ; preds = %lor.lhs.false81, %if.then79
  %m_ghostObject84 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %67 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject84, align 4
  %68 = bitcast %class.btPairCachingGhostObject* %67 to %class.btCollisionObject*
  %call85 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %68)
  br i1 %call85, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true83
  %m_ghostObject86 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %69 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject86, align 4
  %70 = bitcast %class.btPairCachingGhostObject* %69 to %class.btCollisionObject*
  %71 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %71, i32 0, i32 5
  %72 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %73 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %73, align 4
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 14
  %74 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call87 = call zeroext i1 %74(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %70, %class.btCollisionObject* %72)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true83, %lor.lhs.false81
  %75 = phi i1 [ false, %land.lhs.true83 ], [ false, %lor.lhs.false81 ], [ %call87, %land.rhs ]
  %frombool = zext i1 %75 to i8
  store i8 %frombool, i8* %has_hit, align 1
  br label %if.end101

if.else88:                                        ; preds = %cond.end73
  %76 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call89 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %76)
  br i1 %call89, label %land.lhs.true90, label %land.end99

land.lhs.true90:                                  ; preds = %if.else88
  %m_ghostObject91 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %77 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject91, align 4
  %78 = bitcast %class.btPairCachingGhostObject* %77 to %class.btCollisionObject*
  %call92 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %78)
  br i1 %call92, label %land.rhs93, label %land.end99

land.rhs93:                                       ; preds = %land.lhs.true90
  %m_ghostObject94 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %79 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject94, align 4
  %80 = bitcast %class.btPairCachingGhostObject* %79 to %class.btCollisionObject*
  %81 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback2 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject95 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %81, i32 0, i32 5
  %82 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject95, align 4
  %83 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable96 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %83, align 4
  %vfn97 = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable96, i64 14
  %84 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn97, align 4
  %call98 = call zeroext i1 %84(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %80, %class.btCollisionObject* %82)
  br label %land.end99

land.end99:                                       ; preds = %land.rhs93, %land.lhs.true90, %if.else88
  %85 = phi i1 [ false, %land.lhs.true90 ], [ false, %if.else88 ], [ %call98, %land.rhs93 ]
  %frombool100 = zext i1 %85 to i8
  store i8 %frombool100, i8* %has_hit, align 1
  br label %if.end101

if.end101:                                        ; preds = %land.end99, %land.end
  store float 0.000000e+00, float* %stepHeight, align 4
  %m_verticalVelocity102 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %86 = load float, float* %m_verticalVelocity102, align 4
  %conv103 = fpext float %86 to double
  %cmp104 = fcmp olt double %conv103, 0.000000e+00
  br i1 %cmp104, label %if.then105, label %if.end106

if.then105:                                       ; preds = %if.end101
  %m_stepHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 15
  %87 = load float, float* %m_stepHeight, align 4
  store float %87, float* %stepHeight, align 4
  br label %if.end106

if.end106:                                        ; preds = %if.then105, %if.end101
  %88 = load float, float* %downVelocity2, align 4
  %conv107 = fpext float %88 to double
  %cmp108 = fcmp ogt double %conv107, 0.000000e+00
  br i1 %cmp108, label %land.lhs.true109, label %if.end134

land.lhs.true109:                                 ; preds = %if.end106
  %89 = load float, float* %downVelocity2, align 4
  %90 = load float, float* %stepHeight, align 4
  %cmp110 = fcmp olt float %89, %90
  br i1 %cmp110, label %land.lhs.true111, label %if.end134

land.lhs.true111:                                 ; preds = %land.lhs.true109
  %91 = load i8, i8* %has_hit, align 1
  %tobool112 = trunc i8 %91 to i1
  %conv113 = zext i1 %tobool112 to i32
  %cmp114 = icmp eq i32 %conv113, 1
  br i1 %cmp114, label %land.lhs.true115, label %if.end134

land.lhs.true115:                                 ; preds = %land.lhs.true111
  %92 = load i8, i8* %runonce, align 1
  %tobool116 = trunc i8 %92 to i1
  %conv117 = zext i1 %tobool116 to i32
  %cmp118 = icmp eq i32 %conv117, 0
  br i1 %cmp118, label %land.lhs.true119, label %if.end134

land.lhs.true119:                                 ; preds = %land.lhs.true115
  %m_wasOnGround120 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %93 = load i8, i8* %m_wasOnGround120, align 4
  %tobool121 = trunc i8 %93 to i1
  br i1 %tobool121, label %if.then125, label %lor.lhs.false122

lor.lhs.false122:                                 ; preds = %land.lhs.true119
  %m_wasJumping123 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  %94 = load i8, i8* %m_wasJumping123, align 1
  %tobool124 = trunc i8 %94 to i1
  br i1 %tobool124, label %if.end134, label %if.then125

if.then125:                                       ; preds = %lor.lhs.false122, %land.lhs.true119
  %m_targetPosition126 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %95 = bitcast %class.btVector3* %m_targetPosition126 to i8*
  %96 = bitcast %class.btVector3* %orig_position to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 16, i1 false)
  %97 = load float, float* %stepHeight, align 4
  store float %97, float* %downVelocity, align 4
  %m_up128 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %m_currentStepOffset130 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  %98 = load float, float* %m_currentStepOffset130, align 4
  %99 = load float, float* %downVelocity, align 4
  %add131 = fadd float %98, %99
  store float %add131, float* %ref.tmp129, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %100 = bitcast %class.btVector3* %step_drop to i8*
  %101 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 16, i1 false)
  %m_targetPosition132 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call133 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition132, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  store i8 1, i8* %runonce, align 1
  br label %while.cond

if.end134:                                        ; preds = %lor.lhs.false122, %land.lhs.true115, %land.lhs.true111, %land.lhs.true109, %if.end106
  br label %while.end

while.end:                                        ; preds = %if.end134
  %m_ghostObject135 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %102 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject135, align 4
  %103 = bitcast %class.btPairCachingGhostObject* %102 to %class.btCollisionObject*
  %call136 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %103)
  br i1 %call136, label %land.lhs.true137, label %lor.lhs.false145

land.lhs.true137:                                 ; preds = %while.end
  %104 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call138 = call zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback6hasHitEv(%"struct.btCollisionWorld::ConvexResultCallback"* %104)
  br i1 %call138, label %land.lhs.true139, label %lor.lhs.false145

land.lhs.true139:                                 ; preds = %land.lhs.true137
  %m_ghostObject140 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %105 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject140, align 4
  %106 = bitcast %class.btPairCachingGhostObject* %105 to %class.btCollisionObject*
  %107 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitCollisionObject141 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %107, i32 0, i32 5
  %108 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject141, align 4
  %109 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable142 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*** %109, align 4
  %vfn143 = getelementptr inbounds i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable142, i64 14
  %110 = load i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btKinematicCharacterController*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn143, align 4
  %call144 = call zeroext i1 %110(%class.btKinematicCharacterController* %this1, %class.btCollisionObject* %106, %class.btCollisionObject* %108)
  br i1 %call144, label %if.then149, label %lor.lhs.false145

lor.lhs.false145:                                 ; preds = %land.lhs.true139, %land.lhs.true137, %while.end
  %111 = load i8, i8* %runonce, align 1
  %tobool146 = trunc i8 %111 to i1
  %conv147 = zext i1 %tobool146 to i32
  %cmp148 = icmp eq i32 %conv147, 1
  br i1 %cmp148, label %if.then149, label %if.else179

if.then149:                                       ; preds = %lor.lhs.false145, %land.lhs.true139
  %m_currentPosition150 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %call151 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_currentPosition150)
  %112 = load float, float* %call151, align 4
  %113 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %113, i32 0, i32 4
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_hitPointWorld)
  %114 = load float, float* %call152, align 4
  %sub = fsub float %112, %114
  %div = fdiv float %sub, 2.000000e+00
  store float %div, float* %fraction, align 4
  %bounce_fix153 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 41
  %115 = load i8, i8* %bounce_fix153, align 2
  %tobool154 = trunc i8 %115 to i1
  %conv155 = zext i1 %tobool154 to i32
  %cmp156 = icmp eq i32 %conv155, 1
  br i1 %cmp156, label %if.then157, label %if.else170

if.then157:                                       ; preds = %if.then149
  %full_drop = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 40
  %116 = load i8, i8* %full_drop, align 1
  %tobool158 = trunc i8 %116 to i1
  %conv159 = zext i1 %tobool158 to i32
  %cmp160 = icmp eq i32 %conv159, 1
  br i1 %cmp160, label %if.then161, label %if.else165

if.then161:                                       ; preds = %if.then157
  %m_currentPosition162 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_currentPosition163 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition164 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %117 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %117, i32 0, i32 1
  %118 = load float, float* %m_closestHitFraction, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition162, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition163, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition164, float %118)
  br label %if.end169

if.else165:                                       ; preds = %if.then157
  %m_currentPosition166 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_currentPosition167 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition168 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %119 = load float, float* %fraction, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition166, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition167, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition168, float %119)
  br label %if.end169

if.end169:                                        ; preds = %if.else165, %if.then161
  br label %if.end175

if.else170:                                       ; preds = %if.then149
  %m_currentPosition171 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_currentPosition172 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition173 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %120 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %callback to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction174 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %120, i32 0, i32 1
  %121 = load float, float* %m_closestHitFraction174, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_currentPosition171, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition172, %class.btVector3* nonnull align 4 dereferenceable(16) %m_targetPosition173, float %121)
  br label %if.end175

if.end175:                                        ; preds = %if.else170, %if.end169
  %full_drop176 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 40
  store i8 0, i8* %full_drop176, align 1
  %m_verticalVelocity177 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalVelocity177, align 4
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_verticalOffset, align 4
  %m_wasJumping178 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  store i8 0, i8* %m_wasJumping178, align 1
  br label %if.end218

if.else179:                                       ; preds = %lor.lhs.false145
  %full_drop180 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 40
  store i8 1, i8* %full_drop180, align 1
  %bounce_fix181 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 41
  %122 = load i8, i8* %bounce_fix181, align 2
  %tobool182 = trunc i8 %122 to i1
  %conv183 = zext i1 %tobool182 to i32
  %cmp184 = icmp eq i32 %conv183, 1
  br i1 %cmp184, label %if.then185, label %if.end215

if.then185:                                       ; preds = %if.else179
  %m_verticalVelocity186 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %123 = load float, float* %m_verticalVelocity186, align 4
  %cmp187 = fcmp olt float %123, 0.000000e+00
  br i1 %cmp187, label %cond.true188, label %cond.false191

cond.true188:                                     ; preds = %if.then185
  %m_verticalVelocity189 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %124 = load float, float* %m_verticalVelocity189, align 4
  %fneg190 = fneg float %124
  br label %cond.end192

cond.false191:                                    ; preds = %if.then185
  br label %cond.end192

cond.end192:                                      ; preds = %cond.false191, %cond.true188
  %cond193 = phi float [ %fneg190, %cond.true188 ], [ 0.000000e+00, %cond.false191 ]
  %125 = load float, float* %dt.addr, align 4
  %mul194 = fmul float %cond193, %125
  store float %mul194, float* %downVelocity, align 4
  %126 = load float, float* %downVelocity, align 4
  %m_fallSpeed195 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %127 = load float, float* %m_fallSpeed195, align 4
  %cmp196 = fcmp ogt float %126, %127
  br i1 %cmp196, label %land.lhs.true197, label %if.end214

land.lhs.true197:                                 ; preds = %cond.end192
  %m_wasOnGround198 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %128 = load i8, i8* %m_wasOnGround198, align 4
  %tobool199 = trunc i8 %128 to i1
  br i1 %tobool199, label %if.then203, label %lor.lhs.false200

lor.lhs.false200:                                 ; preds = %land.lhs.true197
  %m_wasJumping201 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  %129 = load i8, i8* %m_wasJumping201, align 1
  %tobool202 = trunc i8 %129 to i1
  br i1 %tobool202, label %if.end214, label %if.then203

if.then203:                                       ; preds = %lor.lhs.false200, %land.lhs.true197
  %m_targetPosition204 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call205 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_targetPosition204, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  %m_fallSpeed206 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %130 = load float, float* %m_fallSpeed206, align 4
  store float %130, float* %downVelocity, align 4
  %m_up208 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %m_currentStepOffset210 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 22
  %131 = load float, float* %m_currentStepOffset210, align 4
  %132 = load float, float* %downVelocity, align 4
  %add211 = fadd float %131, %132
  store float %add211, float* %ref.tmp209, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp207, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up208, float* nonnull align 4 dereferenceable(4) %ref.tmp209)
  %133 = bitcast %class.btVector3* %step_drop to i8*
  %134 = bitcast %class.btVector3* %ref.tmp207 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %133, i8* align 4 %134, i32 16, i1 false)
  %m_targetPosition212 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %call213 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_targetPosition212, %class.btVector3* nonnull align 4 dereferenceable(16) %step_drop)
  br label %if.end214

if.end214:                                        ; preds = %if.then203, %lor.lhs.false200, %cond.end192
  br label %if.end215

if.end215:                                        ; preds = %if.end214, %if.else179
  %m_targetPosition216 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %m_currentPosition217 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %135 = bitcast %class.btVector3* %m_currentPosition217 to i8*
  %136 = bitcast %class.btVector3* %m_targetPosition216 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %135, i8* align 4 %136, i32 16, i1 false)
  br label %if.end218

if.end218:                                        ; preds = %if.end215, %if.end175
  %call219 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* %callback2) #7
  %call220 = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* %callback) #7
  br label %return

return:                                           ; preds = %if.end218, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController16setWalkDirectionERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %walkDirection) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %walkDirection.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %walkDirection, %class.btVector3** %walkDirection.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 35
  store i8 1, i8* %m_useWalkDirection, align 1
  %0 = load %class.btVector3*, %class.btVector3** %walkDirection.addr, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %1 = bitcast %class.btVector3* %m_walkDirection to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_walkDirection2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection2)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %3 = bitcast %class.btVector3* %m_normalizedDirection to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  %cmp = fcmp ogt float %call3, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* %1)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController26setVelocityForTimeIntervalERK9btVector3f(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity, float %timeInterval) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %timeInterval.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  store float %timeInterval, float* %timeInterval.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 35
  store i8 0, i8* %m_useWalkDirection, align 1
  %0 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %1 = bitcast %class.btVector3* %m_walkDirection to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_walkDirection2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZL19getNormalizedVectorRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection2)
  %m_normalizedDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 18
  %3 = bitcast %class.btVector3* %m_normalizedDirection to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load float, float* %timeInterval.addr, align 4
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  %6 = load float, float* %m_velocityTimeInterval, align 4
  %add = fadd float %6, %5
  store float %add, float* %m_velocityTimeInterval, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController18setAngularVelocityERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  %m_AngVel = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %1 = bitcast %class.btVector3* %m_AngVel to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK30btKinematicCharacterController18getAngularVelocityEv(%class.btKinematicCharacterController* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_AngVel = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  ret %class.btVector3* %m_AngVel
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController17setLinearVelocityERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %w = alloca %class.btVector3, align 4
  %c = alloca float, align 4
  %upComponent = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %1 = bitcast %class.btVector3* %m_walkDirection to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_walkDirection2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_walkDirection2)
  %cmp = fcmp ogt float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %w, %class.btVector3* %3)
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %w, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up)
  store float %call3, float* %c, align 4
  %4 = load float, float* %c, align 4
  %cmp4 = fcmp une float %4, 0.000000e+00
  br i1 %cmp4, label %if.then5, label %if.end19

if.then5:                                         ; preds = %if.then
  %m_up6 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %5 = load float, float* %c, align 4
  %call7 = call float @acosf(float %5) #10
  %sub = fsub float 0x3FF921FB60000000, %call7
  %6 = call float @llvm.sin.f32(float %sub)
  %m_walkDirection8 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call9 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_walkDirection8)
  %mul = fmul float %6, %call9
  store float %mul, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %upComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up6, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_walkDirection10 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_walkDirection10, %class.btVector3* nonnull align 4 dereferenceable(16) %upComponent)
  %7 = load float, float* %c, align 4
  %cmp12 = fcmp olt float %7, 0.000000e+00
  %8 = zext i1 %cmp12 to i64
  %cond = select i1 %cmp12, i32 -1, i32 1
  %conv = sitofp i32 %cond to float
  %call13 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %upComponent)
  %mul14 = fmul float %conv, %call13
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float %mul14, float* %m_verticalVelocity, align 4
  %9 = load float, float* %c, align 4
  %cmp15 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp15, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.then5
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  store i8 1, i8* %m_wasJumping, align 1
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %10 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %11 = bitcast %class.btPairCachingGhostObject* %10 to %class.btCollisionObject*
  %call17 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %11)
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call17)
  %m_jumpPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %12 = bitcast %class.btVector3* %m_jumpPosition to i8*
  %13 = bitcast %class.btVector3* %call18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then5
  br label %if.end19

if.end19:                                         ; preds = %if.end, %if.then
  br label %if.end21

if.else:                                          ; preds = %entry
  %m_verticalVelocity20 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalVelocity20, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.else, %if.end19
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #6

; Function Attrs: noinline optnone
define hidden void @_ZNK30btKinematicCharacterController17getLinearVelocityEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %m_verticalVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController5resetEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %cache = alloca %class.btHashedOverlappingPairCache*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_verticalVelocity, align 4
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_verticalOffset, align 4
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  store i8 0, i8* %m_wasOnGround, align 4
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  store i8 0, i8* %m_wasJumping, align 1
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_walkDirection, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  store float 0.000000e+00, float* %m_velocityTimeInterval, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %0 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %call = call %class.btHashedOverlappingPairCache* @_ZN24btPairCachingGhostObject23getOverlappingPairCacheEv(%class.btPairCachingGhostObject* %0)
  store %class.btHashedOverlappingPairCache* %call, %class.btHashedOverlappingPairCache** %cache, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4
  %2 = bitcast %class.btHashedOverlappingPairCache* %1 to %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)***
  %vtable = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vtable, i64 7
  %3 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vfn, align 4
  %call4 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* %3(%class.btHashedOverlappingPairCache* %1)
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.4* %call4)
  %cmp = icmp sgt i32 %call5, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4
  %5 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4
  %6 = bitcast %class.btHashedOverlappingPairCache* %5 to %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)***
  %vtable6 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*** %6, align 4
  %vfn7 = getelementptr inbounds %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vtable6, i64 7
  %7 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vfn7, align 4
  %call8 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* %7(%class.btHashedOverlappingPairCache* %5)
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.4* %call8, i32 0)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call9, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %9 = load %class.btHashedOverlappingPairCache*, %class.btHashedOverlappingPairCache** %cache, align 4
  %10 = bitcast %class.btHashedOverlappingPairCache* %9 to %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)***
  %vtable10 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)**, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*** %10, align 4
  %vfn11 = getelementptr inbounds %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vtable10, i64 7
  %11 = load %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)*, %class.btAlignedObjectArray.4* (%class.btHashedOverlappingPairCache*)** %vfn11, align 4
  %call12 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* %11(%class.btHashedOverlappingPairCache* %9)
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.4* %call12, i32 0)
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %call13, i32 0, i32 1
  %12 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %13 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call14 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %13)
  %14 = bitcast %class.btHashedOverlappingPairCache* %4 to i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable15 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %14, align 4
  %vfn16 = getelementptr inbounds i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable15, i64 3
  %15 = load i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btHashedOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn16, align 4
  %call17 = call i8* %15(%class.btHashedOverlappingPairCache* %4, %struct.btBroadphaseProxy* %8, %struct.btBroadphaseProxy* %12, %class.btDispatcher* %call14)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController4warpERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  %xform = alloca %class.btTransform, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %xform)
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %xform, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %1 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %2 = bitcast %class.btPairCachingGhostObject* %1 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %xform)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController7preStepEP16btCollisionWorld(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %0 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %1 = bitcast %class.btPairCachingGhostObject* %0 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %1)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %2 = bitcast %class.btVector3* %m_currentPosition to i8*
  %3 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_currentPosition3 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %4 = bitcast %class.btVector3* %m_targetPosition to i8*
  %5 = bitcast %class.btVector3* %m_currentPosition3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_ghostObject4 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %6 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject4, align 4
  %7 = bitcast %class.btPairCachingGhostObject* %6 to %class.btCollisionObject*
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %7)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btTransform* %call5)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %8 = bitcast %class.btQuaternion* %m_currentOrientation to i8*
  %9 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_currentOrientation6 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %10 = bitcast %class.btQuaternion* %m_targetOrientation to i8*
  %11 = bitcast %class.btQuaternion* %m_currentOrientation6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController10playerStepEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %dt) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dt.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %xform = alloca %class.btTransform, align 4
  %rot = alloca %class.btQuaternion, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %orn = alloca %class.btQuaternion, align 4
  %ref.tmp18 = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca %class.btQuaternion, align 4
  %ref.tmp36 = alloca float, align 4
  %xform74 = alloca %class.btTransform, align 4
  %dtMoving = alloca float, align 4
  %move = alloca %class.btVector3, align 4
  %numPenetrationLoops = alloca i32, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store float %dt, float* %dt.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_AngVel = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_AngVel)
  %cmp = fcmp ogt float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_angularDamping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 31
  %0 = load float, float* %m_angularDamping, align 4
  %sub = fsub float 1.000000e+00, %0
  %1 = load float, float* %dt.addr, align 4
  %call2 = call float @_Z5btPowff(float %sub, float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_AngVel3 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_AngVel3, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_AngVel5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_AngVel5)
  %cmp7 = fcmp ogt float %call6, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.end28

if.then8:                                         ; preds = %if.end
  %call9 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %2 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %3 = bitcast %class.btPairCachingGhostObject* %2 to %class.btCollisionObject*
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %3)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xform, %class.btTransform* nonnull align 4 dereferenceable(64) %call10)
  %m_AngVel13 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* %m_AngVel13)
  %m_AngVel15 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 19
  %call16 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_AngVel15)
  %4 = load float, float* %dt.addr, align 4
  %mul = fmul float %call16, %4
  store float %mul, float* %ref.tmp14, align 4
  %call17 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rot, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp18, %class.btTransform* %xform)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %orn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rot, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp18)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %xform, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn)
  %m_ghostObject19 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %5 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject19, align 4
  %6 = bitcast %class.btPairCachingGhostObject* %5 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %xform)
  %m_ghostObject20 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %7 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject20, align 4
  %8 = bitcast %class.btPairCachingGhostObject* %7 to %class.btCollisionObject*
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %8)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call21)
  %m_currentPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %9 = bitcast %class.btVector3* %m_currentPosition to i8*
  %10 = bitcast %class.btVector3* %call22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %m_currentPosition23 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  %m_targetPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 23
  %11 = bitcast %class.btVector3* %m_targetPosition to i8*
  %12 = bitcast %class.btVector3* %m_currentPosition23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %m_ghostObject25 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %13 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject25, align 4
  %14 = bitcast %class.btPairCachingGhostObject* %13 to %class.btCollisionObject*
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %14)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp24, %class.btTransform* %call26)
  %m_currentOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %15 = bitcast %class.btQuaternion* %m_currentOrientation to i8*
  %16 = bitcast %class.btQuaternion* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %m_currentOrientation27 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 24
  %m_targetOrientation = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 25
  %17 = bitcast %class.btQuaternion* %m_targetOrientation to i8*
  %18 = bitcast %class.btQuaternion* %m_currentOrientation27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  br label %if.end28

if.end28:                                         ; preds = %if.then8, %if.end
  %m_useWalkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 35
  %19 = load i8, i8* %m_useWalkDirection, align 1
  %tobool = trunc i8 %19 to i1
  br i1 %tobool, label %if.end31, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.end28
  %m_velocityTimeInterval = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  %20 = load float, float* %m_velocityTimeInterval, align 4
  %conv = fpext float %20 to double
  %cmp29 = fcmp ole double %conv, 0.000000e+00
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true
  br label %while.end

if.end31:                                         ; preds = %land.lhs.true, %if.end28
  %21 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*)***
  %vtable = load i1 (%class.btKinematicCharacterController*)**, i1 (%class.btKinematicCharacterController*)*** %21, align 4
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vtable, i64 12
  %22 = load i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vfn, align 4
  %call32 = call zeroext i1 %22(%class.btKinematicCharacterController* %this1)
  %m_wasOnGround = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 32
  %frombool = zext i1 %call32 to i8
  store i8 %frombool, i8* %m_wasOnGround, align 4
  %m_walkDirection = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call33 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_walkDirection)
  %cmp34 = fcmp ogt float %call33, 0.000000e+00
  br i1 %cmp34, label %if.then35, label %if.end41

if.then35:                                        ; preds = %if.end31
  %m_linearDamping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  %23 = load float, float* %m_linearDamping, align 4
  %sub37 = fsub float 1.000000e+00, %23
  %24 = load float, float* %dt.addr, align 4
  %call38 = call float @_Z5btPowff(float %sub37, float %24)
  store float %call38, float* %ref.tmp36, align 4
  %m_walkDirection39 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_walkDirection39, float* nonnull align 4 dereferenceable(4) %ref.tmp36)
  br label %if.end41

if.end41:                                         ; preds = %if.then35, %if.end31
  %m_linearDamping42 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 30
  %25 = load float, float* %m_linearDamping42, align 4
  %sub43 = fsub float 1.000000e+00, %25
  %26 = load float, float* %dt.addr, align 4
  %call44 = call float @_Z5btPowff(float %sub43, float %26)
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %27 = load float, float* %m_verticalVelocity, align 4
  %mul45 = fmul float %27, %call44
  store float %mul45, float* %m_verticalVelocity, align 4
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %28 = load float, float* %m_gravity, align 4
  %29 = load float, float* %dt.addr, align 4
  %mul46 = fmul float %28, %29
  %m_verticalVelocity47 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %30 = load float, float* %m_verticalVelocity47, align 4
  %sub48 = fsub float %30, %mul46
  store float %sub48, float* %m_verticalVelocity47, align 4
  %m_verticalVelocity49 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %31 = load float, float* %m_verticalVelocity49, align 4
  %conv50 = fpext float %31 to double
  %cmp51 = fcmp ogt double %conv50, 0.000000e+00
  br i1 %cmp51, label %land.lhs.true52, label %if.end58

land.lhs.true52:                                  ; preds = %if.end41
  %m_verticalVelocity53 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %32 = load float, float* %m_verticalVelocity53, align 4
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  %33 = load float, float* %m_jumpSpeed, align 4
  %cmp54 = fcmp ogt float %32, %33
  br i1 %cmp54, label %if.then55, label %if.end58

if.then55:                                        ; preds = %land.lhs.true52
  %m_jumpSpeed56 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  %34 = load float, float* %m_jumpSpeed56, align 4
  %m_verticalVelocity57 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float %34, float* %m_verticalVelocity57, align 4
  br label %if.end58

if.end58:                                         ; preds = %if.then55, %land.lhs.true52, %if.end41
  %m_verticalVelocity59 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %35 = load float, float* %m_verticalVelocity59, align 4
  %conv60 = fpext float %35 to double
  %cmp61 = fcmp olt double %conv60, 0.000000e+00
  br i1 %cmp61, label %land.lhs.true62, label %if.end71

land.lhs.true62:                                  ; preds = %if.end58
  %m_verticalVelocity63 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %36 = load float, float* %m_verticalVelocity63, align 4
  %call64 = call float @_Z6btFabsf(float %36)
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %37 = load float, float* %m_fallSpeed, align 4
  %call65 = call float @_Z6btFabsf(float %37)
  %cmp66 = fcmp ogt float %call64, %call65
  br i1 %cmp66, label %if.then67, label %if.end71

if.then67:                                        ; preds = %land.lhs.true62
  %m_fallSpeed68 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  %38 = load float, float* %m_fallSpeed68, align 4
  %call69 = call float @_Z6btFabsf(float %38)
  %fneg = fneg float %call69
  %m_verticalVelocity70 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float %fneg, float* %m_verticalVelocity70, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.then67, %land.lhs.true62, %if.end58
  %m_verticalVelocity72 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %39 = load float, float* %m_verticalVelocity72, align 4
  %40 = load float, float* %dt.addr, align 4
  %mul73 = fmul float %39, %40
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  store float %mul73, float* %m_verticalOffset, align 4
  %call75 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform74)
  %m_ghostObject76 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %41 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject76, align 4
  %42 = bitcast %class.btPairCachingGhostObject* %41 to %class.btCollisionObject*
  %call77 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %42)
  %call78 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xform74, %class.btTransform* nonnull align 4 dereferenceable(64) %call77)
  %43 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  call void @_ZN30btKinematicCharacterController6stepUpEP16btCollisionWorld(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %43)
  %m_useWalkDirection79 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 35
  %44 = load i8, i8* %m_useWalkDirection79, align 1
  %tobool80 = trunc i8 %44 to i1
  br i1 %tobool80, label %if.then81, label %if.else

if.then81:                                        ; preds = %if.end71
  %45 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %m_walkDirection82 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection82)
  br label %if.end89

if.else:                                          ; preds = %if.end71
  %46 = load float, float* %dt.addr, align 4
  %m_velocityTimeInterval83 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  %47 = load float, float* %m_velocityTimeInterval83, align 4
  %cmp84 = fcmp olt float %46, %47
  br i1 %cmp84, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %48 = load float, float* %dt.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_velocityTimeInterval85 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  %49 = load float, float* %m_velocityTimeInterval85, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %48, %cond.true ], [ %49, %cond.false ]
  store float %cond, float* %dtMoving, align 4
  %50 = load float, float* %dt.addr, align 4
  %m_velocityTimeInterval86 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 36
  %51 = load float, float* %m_velocityTimeInterval86, align 4
  %sub87 = fsub float %51, %50
  store float %sub87, float* %m_velocityTimeInterval86, align 4
  %m_walkDirection88 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 17
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %move, %class.btVector3* nonnull align 4 dereferenceable(16) %m_walkDirection88, float* nonnull align 4 dereferenceable(4) %dtMoving)
  %52 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  call void @_ZN30btKinematicCharacterController20stepForwardAndStrafeEP16btCollisionWorldRK9btVector3(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %move)
  br label %if.end89

if.end89:                                         ; preds = %cond.end, %if.then81
  %53 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %54 = load float, float* %dt.addr, align 4
  call void @_ZN30btKinematicCharacterController8stepDownEP16btCollisionWorldf(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %53, float %54)
  %m_currentPosition90 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 21
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %xform74, %class.btVector3* nonnull align 4 dereferenceable(16) %m_currentPosition90)
  %m_ghostObject91 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %55 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject91, align 4
  %56 = bitcast %class.btPairCachingGhostObject* %55 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %56, %class.btTransform* nonnull align 4 dereferenceable(64) %xform74)
  store i32 0, i32* %numPenetrationLoops, align 4
  %m_touchingContact = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 0, i8* %m_touchingContact, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end96, %if.end89
  %57 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call92 = call zeroext i1 @_ZN30btKinematicCharacterController22recoverFromPenetrationEP16btCollisionWorld(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %57)
  br i1 %call92, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %58 = load i32, i32* %numPenetrationLoops, align 4
  %inc = add nsw i32 %58, 1
  store i32 %inc, i32* %numPenetrationLoops, align 4
  %m_touchingContact93 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 27
  store i8 1, i8* %m_touchingContact93, align 4
  %59 = load i32, i32* %numPenetrationLoops, align 4
  %cmp94 = icmp sgt i32 %59, 4
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %while.body
  br label %while.end

if.end96:                                         ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %if.then30, %if.then95, %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btPowff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %2 = call float @llvm.pow.f32(float %0, float %1)
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController12setFallSpeedEf(%class.btKinematicCharacterController* %this, float %fallSpeed) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %fallSpeed.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %fallSpeed, float* %fallSpeed.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %fallSpeed.addr, align 4
  %m_fallSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 7
  store float %0, float* %m_fallSpeed, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController12setJumpSpeedEf(%class.btKinematicCharacterController* %this, float %jumpSpeed) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %jumpSpeed.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %jumpSpeed, float* %jumpSpeed.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %jumpSpeed.addr, align 4
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  store float %0, float* %m_jumpSpeed, align 4
  %m_jumpSpeed2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  %1 = load float, float* %m_jumpSpeed2, align 4
  %m_SetjumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 9
  store float %1, float* %m_SetjumpSpeed, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController16setMaxJumpHeightEf(%class.btKinematicCharacterController* %this, float %maxJumpHeight) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %maxJumpHeight.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %maxJumpHeight, float* %maxJumpHeight.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %maxJumpHeight.addr, align 4
  %m_maxJumpHeight = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 10
  store float %0, float* %m_maxJumpHeight, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK30btKinematicCharacterController7canJumpEv(%class.btKinematicCharacterController* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = bitcast %class.btKinematicCharacterController* %this1 to i1 (%class.btKinematicCharacterController*)***
  %vtable = load i1 (%class.btKinematicCharacterController*)**, i1 (%class.btKinematicCharacterController*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vtable, i64 12
  %1 = load i1 (%class.btKinematicCharacterController*)*, i1 (%class.btKinematicCharacterController*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btKinematicCharacterController* %this1)
  ret i1 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController4jumpERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %0)
  %cmp = fcmp oeq float %call, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_SetjumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 9
  %1 = load float, float* %m_SetjumpSpeed, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %1, %cond.true ], [ %call2, %cond.false ]
  %m_jumpSpeed = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  store float %cond, float* %m_jumpSpeed, align 4
  %m_jumpSpeed3 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 8
  %3 = load float, float* %m_jumpSpeed3, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  store float %3, float* %m_verticalVelocity, align 4
  %m_wasJumping = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 33
  store i8 1, i8* %m_wasJumping, align 1
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %4)
  %cmp5 = fcmp oeq float %call4, 0.000000e+00
  br i1 %cmp5, label %cond.true6, label %cond.false7

cond.true6:                                       ; preds = %cond.end
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  %6 = bitcast %class.btVector3* %m_up to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %cond.end8

cond.false7:                                      ; preds = %cond.end
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %7)
  br label %cond.end8

cond.end8:                                        ; preds = %cond.false7, %cond.true6
  %m_jumpAxis = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 38
  %8 = bitcast %class.btVector3* %m_jumpAxis to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %10 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %11 = bitcast %class.btPairCachingGhostObject* %10 to %class.btCollisionObject*
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %11)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call9)
  %m_jumpPosition = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 20
  %12 = bitcast %class.btVector3* %m_jumpPosition to i8*
  %13 = bitcast %class.btVector3* %call10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController10setGravityERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %gravity) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %gravity.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %gravity, %class.btVector3** %gravity.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %0)
  %cmp = fcmp ogt float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZN30btKinematicCharacterController11setUpVectorERK9btVector3(%class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4
  %call2 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %2)
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  store float %call2, float* %m_gravity, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN30btKinematicCharacterController11setUpVectorERK9btVector3(%class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %up) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %up.addr = alloca %class.btVector3*, align 4
  %u = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %rot = alloca %class.btQuaternion, align 4
  %xform = alloca %class.btTransform, align 4
  %orn = alloca %class.btQuaternion, align 4
  %ref.tmp20 = alloca %class.btQuaternion, align 4
  %ref.tmp21 = alloca %class.btQuaternion, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %up, %class.btVector3** %up.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %0 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  %call = call zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %m_up, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_up2 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %1 = bitcast %class.btVector3* %u to i8*
  %2 = bitcast %class.btVector3* %m_up2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  %call3 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %3)
  %cmp = fcmp ogt float %call3, 0.000000e+00
  br i1 %cmp, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %4 = load %class.btVector3*, %class.btVector3** %up.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %4)
  %m_up5 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %5 = bitcast %class.btVector3* %m_up5 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %if.end12

if.else:                                          ; preds = %if.end
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %m_up11 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  %7 = bitcast %class.btVector3* %m_up11 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then4
  %m_ghostObject = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %9 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject, align 4
  %tobool = icmp ne %class.btPairCachingGhostObject* %9, null
  br i1 %tobool, label %if.end14, label %if.then13

if.then13:                                        ; preds = %if.end12
  br label %return

if.end14:                                         ; preds = %if.end12
  %m_up15 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  call void @_ZNK30btKinematicCharacterController11getRotationER9btVector3S1_(%class.btQuaternion* sret align 4 %rot, %class.btKinematicCharacterController* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up15, %class.btVector3* nonnull align 4 dereferenceable(16) %u)
  %call16 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xform)
  %m_ghostObject17 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %10 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject17, align 4
  %11 = bitcast %class.btPairCachingGhostObject* %10 to %class.btCollisionObject*
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %11)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xform, %class.btTransform* nonnull align 4 dereferenceable(64) %call18)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp20, %class.btQuaternion* %rot)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp21, %class.btTransform* %xform)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %orn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp20, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp21)
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %xform, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn)
  %m_ghostObject22 = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 2
  %12 = load %class.btPairCachingGhostObject*, %class.btPairCachingGhostObject** %m_ghostObject22, align 4
  %13 = bitcast %class.btPairCachingGhostObject* %12 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %13, %class.btTransform* nonnull align 4 dereferenceable(64) %xform)
  br label %return

return:                                           ; preds = %if.end14, %if.then13, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK30btKinematicCharacterController10getGravityEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_gravity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 13
  %0 = load float, float* %m_gravity, align 4
  %fneg = fneg float %0
  store float %fneg, float* %ref.tmp, align 4
  %m_up = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 37
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_up)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK30btKinematicCharacterController11getMaxSlopeEv(%class.btKinematicCharacterController* %this) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_maxSlopeRadians = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 11
  %0 = load float, float* %m_maxSlopeRadians, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController22setMaxPenetrationDepthEf(%class.btKinematicCharacterController* %this, float %d) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %d.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store float %d, float* %d.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load float, float* %d.addr, align 4
  %m_maxPenetrationDepth = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  store float %0, float* %m_maxPenetrationDepth, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK30btKinematicCharacterController22getMaxPenetrationDepthEv(%class.btKinematicCharacterController* %this) #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_maxPenetrationDepth = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 4
  %0 = load float, float* %m_maxPenetrationDepth, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK30btKinematicCharacterController8onGroundEv(%class.btKinematicCharacterController* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %m_verticalVelocity = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 5
  %0 = load float, float* %m_verticalVelocity, align 4
  %call = call float @_Z4fabsf(float %0) #7
  %cmp = fcmp olt float %call, 0x3E80000000000000
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_verticalOffset = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 6
  %1 = load float, float* %m_verticalOffset, align 4
  %call2 = call float @_Z4fabsf(float %1) #7
  %cmp3 = fcmp olt float %call2, 0x3E80000000000000
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden %class.btVector3* @_ZN30btKinematicCharacterController19getUpAxisDirectionsEv() #2 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection) #7
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  call void @__cxa_guard_release(i32* @_ZGVZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection) #7
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN30btKinematicCharacterController19getUpAxisDirectionsEvE16sUpAxisDirection, i32 0, i32 0)
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #7

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController9debugDrawEP12btIDebugDraw(%class.btKinematicCharacterController* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btKinematicCharacterController16setUpInterpolateEb(%class.btKinematicCharacterController* %this, i1 zeroext %value) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %value.addr = alloca i8, align 1
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  %frombool = zext i1 %value to i8
  store i8 %frombool, i8* %value.addr, align 1
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load i8, i8* %value.addr, align 1
  %tobool = trunc i8 %0 to i1
  %m_interpolateUp = getelementptr inbounds %class.btKinematicCharacterController, %class.btKinematicCharacterController* %this1, i32 0, i32 39
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_interpolateUp, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 3
  %2 = load float, float* %arrayidx3, align 4
  %cmp = fcmp oeq float %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp oeq float %3, %5
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 1
  %8 = load float, float* %arrayidx13, align 4
  %cmp14 = fcmp oeq float %6, %8
  br i1 %cmp14, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true9
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 0
  %11 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp oeq float %9, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true9, %land.lhs.true, %entry
  %12 = phi i1 [ false, %land.lhs.true9 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp19, %land.rhs ]
  ret i1 %12
}

; Function Attrs: noinline optnone
define hidden void @_ZNK30btKinematicCharacterController11getRotationER9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btKinematicCharacterController* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %0)
  %cmp = fcmp oeq float %call, 0.000000e+00
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %1)
  %cmp3 = fcmp oeq float %call2, 0.000000e+00
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %call4 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_Z25shortestArcQuatNormalize2R9btVector3S0_(%class.btQuaternion* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z25shortestArcQuatNormalize2R9btVector3S0_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %0)
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %1)
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN30btKinematicCharacterController12updateActionEP16btCollisionWorldf(%class.btKinematicCharacterController* %this, %class.btCollisionWorld* %collisionWorld, float %deltaTime) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btKinematicCharacterController*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %deltaTime.addr = alloca float, align 4
  store %class.btKinematicCharacterController* %this, %class.btKinematicCharacterController** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store float %deltaTime, float* %deltaTime.addr, align 4
  %this1 = load %class.btKinematicCharacterController*, %class.btKinematicCharacterController** %this.addr, align 4
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %1 = bitcast %class.btKinematicCharacterController* %this1 to void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)***
  %vtable = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)**, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)** %vtable, i64 8
  %2 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*)** %vfn, align 4
  call void %2(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %0)
  %3 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %4 = load float, float* %deltaTime.addr, align 4
  %5 = bitcast %class.btKinematicCharacterController* %this1 to void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)***
  %vtable2 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)**, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*** %5, align 4
  %vfn3 = getelementptr inbounds void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)** %vtable2, i64 9
  %6 = load void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)*, void (%class.btKinematicCharacterController*, %class.btCollisionWorld*, float)** %vfn3, align 4
  call void %6(%class.btKinematicCharacterController* %this1, %class.btCollisionWorld* %3, float %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  %0 = bitcast %class.btActionInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV17btActionInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCharacterControllerInterface* @_ZN30btCharacterControllerInterfaceD2Ev(%class.btCharacterControllerInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCharacterControllerInterface*, align 4
  store %class.btCharacterControllerInterface* %this, %class.btCharacterControllerInterface** %this.addr, align 4
  %this1 = load %class.btCharacterControllerInterface*, %class.btCharacterControllerInterface** %this.addr, align 4
  %0 = bitcast %class.btCharacterControllerInterface* %this1 to %class.btActionInterface*
  %call = call %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* %0) #7
  ret %class.btCharacterControllerInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btCharacterControllerInterfaceD0Ev(%class.btCharacterControllerInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCharacterControllerInterface*, align 4
  store %class.btCharacterControllerInterface* %this, %class.btCharacterControllerInterface** %this.addr, align 4
  %this1 = load %class.btCharacterControllerInterface*, %class.btCharacterControllerInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btActionInterfaceD0Ev(%class.btActionInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestConvexResultCallback"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %convexFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %convexToWorld) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexFromWorld.addr = alloca %class.btVector3*, align 4
  %convexToWorld.addr = alloca %class.btVector3*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  store %class.btVector3* %convexFromWorld, %class.btVector3** %convexFromWorld.addr, align 4
  store %class.btVector3* %convexToWorld, %class.btVector3** %convexToWorld.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %0)
  %1 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld27ClosestConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_convexFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %convexFromWorld.addr, align 4
  %3 = bitcast %class.btVector3* %m_convexFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_convexToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %convexToWorld.addr, align 4
  %6 = bitcast %class.btVector3* %m_convexToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalWorld)
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointWorld)
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_hitCollisionObject, align 4
  ret %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN43btKinematicClosestNotMeConvexResultCallbackD0Ev(%class.btKinematicClosestNotMeConvexResultCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %call = call %class.btKinematicClosestNotMeConvexResultCallback* @_ZN43btKinematicClosestNotMeConvexResultCallbackD2Ev(%class.btKinematicClosestNotMeConvexResultCallback* %this1) #7
  %0 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::ConvexResultCallback"* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 1
  %1 = load i32, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_collisionFilterMask, align 4
  %and = and i32 %1, %2
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1
  %3 = load i8, i8* %collides, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_collisionFilterGroup2 = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  %4 = load i32, i32* %m_collisionFilterGroup2, align 4
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %m_collisionFilterMask3 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 2
  %6 = load i32, i32* %m_collisionFilterMask3, align 4
  %and4 = and i32 %4, %6
  %tobool5 = icmp ne i32 %and4, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %tobool5, %land.rhs ]
  %frombool6 = zext i1 %7 to i8
  store i8 %frombool6, i8* %collides, align 1
  %8 = load i8, i8* %collides, align 1
  %tobool7 = trunc i8 %8 to i1
  ret i1 %tobool7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN43btKinematicClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb(%class.btKinematicClosestNotMeConvexResultCallback* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #2 comdat {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btKinematicClosestNotMeConvexResultCallback*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %hitNormalWorld = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %dotUp = alloca float, align 4
  store %class.btKinematicClosestNotMeConvexResultCallback* %this, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1
  %this1 = load %class.btKinematicClosestNotMeConvexResultCallback*, %class.btKinematicClosestNotMeConvexResultCallback** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 0
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %m_me = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %m_me, align 4
  %cmp = icmp eq %class.btCollisionObject* %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject2, align 4
  %call = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %4)
  br i1 %call, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hitNormalWorld)
  %5 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end4
  %6 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %6, i32 0, i32 2
  %7 = bitcast %class.btVector3* %hitNormalWorld to i8*
  %8 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %if.end11

if.else:                                          ; preds = %if.end4
  %9 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject7 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %9, i32 0, i32 0
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject7, align 4
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %10)
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call8)
  %11 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal10 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %11, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal10)
  %12 = bitcast %class.btVector3* %hitNormalWorld to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %if.end11

if.end11:                                         ; preds = %if.else, %if.then6
  %m_up = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 2
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_up, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormalWorld)
  store float %call12, float* %dotUp, align 4
  %14 = load float, float* %dotUp, align 4
  %m_minSlopeDot = getelementptr inbounds %class.btKinematicClosestNotMeConvexResultCallback, %class.btKinematicClosestNotMeConvexResultCallback* %this1, i32 0, i32 3
  %15 = load float, float* %m_minSlopeDot, align 4
  %cmp13 = fcmp olt float %14, %15
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end11
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end11
  %16 = bitcast %class.btKinematicClosestNotMeConvexResultCallback* %this1 to %"struct.btCollisionWorld::ClosestConvexResultCallback"*
  %17 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %18 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool16 = trunc i8 %18 to i1
  %call17 = call float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %16, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %17, i1 zeroext %tobool16)
  store float %call17, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end15, %if.then14, %if.then3, %if.then
  %19 = load float, float* %retval, align 4
  ret float %19
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackC2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ConvexResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld20ConvexResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_closestHitFraction, align 4
  %m_collisionFilterGroup = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 2
  store i32 1, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %this1, i32 0, i32 3
  store i32 -1, i32* %m_collisionFilterMask, align 4
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %call = call %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %0) #7
  ret %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld27ClosestConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestConvexResultCallback"* @_ZN16btCollisionWorld27ClosestConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1) #7
  %0 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb(%"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::LocalConvexResult"* nonnull align 4 dereferenceable(44) %convexResult, i1 zeroext %normalInWorldSpace) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestConvexResultCallback"*, align 4
  %convexResult.addr = alloca %"struct.btCollisionWorld::LocalConvexResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  store %"struct.btCollisionWorld::LocalConvexResult"* %convexResult, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1
  %this1 = load %"struct.btCollisionWorld::ClosestConvexResultCallback"*, %"struct.btCollisionWorld::ClosestConvexResultCallback"** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %0, i32 0, i32 4
  %1 = load float, float* %m_hitFraction, align 4
  %2 = bitcast %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1 to %"struct.btCollisionWorld::ConvexResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::ConvexResultCallback", %"struct.btCollisionWorld::ConvexResultCallback"* %2, i32 0, i32 1
  store float %1, float* %m_closestHitFraction, align 4
  %3 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitCollisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject, align 4
  %m_hitCollisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_hitCollisionObject2, align 4
  %5 = load i8, i8* %normalInWorldSpace.addr, align 1
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %6, i32 0, i32 2
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %7 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  %8 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_hitCollisionObject3 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 5
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %m_hitCollisionObject3, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %9)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  %10 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitNormalLocal5 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %10, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal5)
  %m_hitNormalWorld6 = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 3
  %11 = bitcast %class.btVector3* %m_hitNormalWorld6 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %13 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitPointLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %13, i32 0, i32 3
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestConvexResultCallback", %"struct.btCollisionWorld::ClosestConvexResultCallback"* %this1, i32 0, i32 4
  %14 = bitcast %class.btVector3* %m_hitPointWorld to i8*
  %15 = bitcast %class.btVector3* %m_hitPointLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  %16 = load %"struct.btCollisionWorld::LocalConvexResult"*, %"struct.btCollisionWorld::LocalConvexResult"** %convexResult.addr, align 4
  %m_hitFraction7 = getelementptr inbounds %"struct.btCollisionWorld::LocalConvexResult", %"struct.btCollisionWorld::LocalConvexResult"* %16, i32 0, i32 4
  %17 = load float, float* %m_hitFraction7, align 4
  ret float %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::ConvexResultCallback"* @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev(%"struct.btCollisionWorld::ConvexResultCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  ret %"struct.btCollisionWorld::ConvexResultCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld20ConvexResultCallbackD0Ev(%"struct.btCollisionWorld::ConvexResultCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ConvexResultCallback"*, align 4
  store %"struct.btCollisionWorld::ConvexResultCallback"* %this, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  %this1 = load %"struct.btCollisionWorld::ConvexResultCallback"*, %"struct.btCollisionWorld::ConvexResultCallback"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK11btMatrix3x3S1_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %3 = load float, float* %arrayidx4, align 4
  %cmp = fcmp oeq float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %5 = load float, float* %arrayidx7, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 1)
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %7 = load float, float* %arrayidx10, align 4
  %cmp11 = fcmp oeq float %5, %7
  br i1 %cmp11, label %land.lhs.true12, label %land.end

land.lhs.true12:                                  ; preds = %land.lhs.true
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 2)
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %9 = load float, float* %arrayidx15, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 2)
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  %11 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp oeq float %9, %11
  br i1 %cmp19, label %land.lhs.true20, label %land.end

land.lhs.true20:                                  ; preds = %land.lhs.true12
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 0)
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %13 = load float, float* %arrayidx23, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 0)
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %15 = load float, float* %arrayidx26, align 4
  %cmp27 = fcmp oeq float %13, %15
  br i1 %cmp27, label %land.lhs.true28, label %land.end

land.lhs.true28:                                  ; preds = %land.lhs.true20
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 1)
  %call30 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 1
  %17 = load float, float* %arrayidx31, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call33 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call32)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  %19 = load float, float* %arrayidx34, align 4
  %cmp35 = fcmp oeq float %17, %19
  br i1 %cmp35, label %land.lhs.true36, label %land.end

land.lhs.true36:                                  ; preds = %land.lhs.true28
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %20, i32 2)
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %21 = load float, float* %arrayidx39, align 4
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %22, i32 2)
  %call41 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call40)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 1
  %23 = load float, float* %arrayidx42, align 4
  %cmp43 = fcmp oeq float %21, %23
  br i1 %cmp43, label %land.lhs.true44, label %land.end

land.lhs.true44:                                  ; preds = %land.lhs.true36
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 0)
  %call46 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call45)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 2
  %25 = load float, float* %arrayidx47, align 4
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %26, i32 0)
  %call49 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call48)
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 2
  %27 = load float, float* %arrayidx50, align 4
  %cmp51 = fcmp oeq float %25, %27
  br i1 %cmp51, label %land.lhs.true52, label %land.end

land.lhs.true52:                                  ; preds = %land.lhs.true44
  %28 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %28, i32 1)
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call53)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %29 = load float, float* %arrayidx55, align 4
  %30 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %30, i32 1)
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 2
  %31 = load float, float* %arrayidx58, align 4
  %cmp59 = fcmp oeq float %29, %31
  br i1 %cmp59, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true52
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call60 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 2)
  %call61 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call60)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 2
  %33 = load float, float* %arrayidx62, align 4
  %34 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %34, i32 2)
  %call64 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call63)
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 2
  %35 = load float, float* %arrayidx65, align 4
  %cmp66 = fcmp oeq float %33, %35
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true52, %land.lhs.true44, %land.lhs.true36, %land.lhs.true28, %land.lhs.true20, %land.lhs.true12, %land.lhs.true, %entry
  %36 = phi i1 [ false, %land.lhs.true52 ], [ false, %land.lhs.true44 ], [ false, %land.lhs.true36 ], [ false, %land.lhs.true28 ], [ false, %land.lhs.true20 ], [ false, %land.lhs.true12 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp66, %land.rhs ]
  ret i1 %36
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15shortestArcQuatRK9btVector3S1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1) #2 comdat {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %c = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %unused = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %s = alloca float, align 4
  %rs = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call, float* %d, align 4
  %4 = load float, float* %d, align 4
  %conv = fpext float %4 to double
  %cmp = fcmp olt double %conv, 0xBFEFFFFFC0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %unused)
  %5 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %unused)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %n)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %n)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %n)
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %return

if.end:                                           ; preds = %entry
  %6 = load float, float* %d, align 4
  %add = fadd float 1.000000e+00, %6
  %mul = fmul float %add, 2.000000e+00
  %call7 = call float @_Z6btSqrtf(float %mul)
  store float %call7, float* %s, align 4
  %7 = load float, float* %s, align 4
  %div = fdiv float 1.000000e+00, %7
  store float %div, float* %rs, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %c)
  %8 = load float, float* %call9, align 4
  %9 = load float, float* %rs, align 4
  %mul10 = fmul float %8, %9
  store float %mul10, float* %ref.tmp8, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %c)
  %10 = load float, float* %call12, align 4
  %11 = load float, float* %rs, align 4
  %mul13 = fmul float %10, %11
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %c)
  %12 = load float, float* %call15, align 4
  %13 = load float, float* %rs, align 4
  %mul16 = fmul float %12, %13
  store float %mul16, float* %ref.tmp14, align 4
  %14 = load float, float* %s, align 4
  %mul18 = fmul float %14, 5.000000e-01
  store float %mul18, float* %ref.tmp17, align 4
  %call19 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.14* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  ret %class.btAlignedAllocator.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.13* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.14* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.13* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.13* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.13* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.13* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.13* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.13* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.13* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.13*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.13* %this, %class.btAlignedObjectArray.13** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.13*, %class.btAlignedObjectArray.13** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.13* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.14* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.14*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.14* %this, %class.btAlignedAllocator.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.14*, %class.btAlignedAllocator.14** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btKinematicCharacterController.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind readnone }
attributes #11 = { noreturn nounwind }
attributes #12 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
