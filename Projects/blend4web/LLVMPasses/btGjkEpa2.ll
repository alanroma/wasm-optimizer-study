; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpa2.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpa2.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%"struct.btGjkEpaSolver2::sResults" = type { i32, [2 x %class.btVector3], %class.btVector3, float }
%"struct.gjkepa2_impl::MinkowskiDiff" = type { [2 x %class.btConvexShape*], %class.btMatrix3x3, %class.btTransform, { i32, i32 } }
%"struct.gjkepa2_impl::GJK" = type { %"struct.gjkepa2_impl::MinkowskiDiff", %class.btVector3, float, [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"*], i32, i32, %"struct.gjkepa2_impl::GJK::sSimplex"*, i32 }
%"struct.gjkepa2_impl::GJK::sSimplex" = type { [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x float], i32 }
%"struct.gjkepa2_impl::GJK::sSV" = type { %class.btVector3, %class.btVector3 }
%"struct.gjkepa2_impl::EPA" = type { i32, %"struct.gjkepa2_impl::GJK::sSimplex", %class.btVector3, float, [128 x %"struct.gjkepa2_impl::GJK::sSV"], [256 x %"struct.gjkepa2_impl::EPA::sFace"], i32, %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList" }
%"struct.gjkepa2_impl::EPA::sFace" = type { %class.btVector3, float, [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x i8], i8 }
%"struct.gjkepa2_impl::EPA::sList" = type { %"struct.gjkepa2_impl::EPA::sFace"*, i32 }
%"struct.gjkepa2_impl::EPA::sHorizon" = type { %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"*, i32 }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN12gjkepa2_impl13MinkowskiDiffC2Ev = comdat any

$_ZN12gjkepa2_impl3GJKC2Ev = comdat any

$_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN12gjkepa2_impl3EPAC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3 = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN11btTransformC2ERK12btQuaternionRK9btVector3 = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN13btSphereShapeD2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x314transposeTimesERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btTransform12inverseTimesERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN12gjkepa2_impl3GJK3sSVC2Ev = comdat any

$_ZN12gjkepa2_impl3GJK10InitializeEv = comdat any

$_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3 = comdat any

$_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3 = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3 = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3 = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_ = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN12gjkepa2_impl3EPA5sFaceC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA5sListC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA10InitializeEv = comdat any

$_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE = comdat any

$_ZN12gjkepa2_impl3GJK13EncloseOriginEv = comdat any

$_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE = comdat any

$_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_ = comdat any

$_Z6btSwapIfEvRT_S1_ = comdat any

$_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b = comdat any

$_ZN12gjkepa2_impl3EPA8findbestEv = comdat any

$_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j = comdat any

$_ZN12gjkepa2_impl3EPA8sHorizonC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN21btConvexInternalShapeD2Ev = comdat any

$_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3 = comdat any

$_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3 = comdat any

$_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3 = comdat any

$_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3 = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3 = linkonce_odr hidden constant [3 x i32] [i32 2, i32 0, i32 1], comdat, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGjkEpa2.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN15btGjkEpaSolver220StackSizeRequirementEv() #1 {
entry:
  ret i32 18892
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #2 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %w1 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %i = alloca i32, align 4
  %p = alloca float, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca float, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %2 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  %4 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %4, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext false)
  %call1 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  %5 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  %call2 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store i32 %call2, i32* %gjk_status, align 4
  %6 = load i32, i32* %gjk_status, align 4
  %cmp = icmp eq i32 %6, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w1, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %i, align 4
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %8 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %8, i32 0, i32 2
  %9 = load i32, i32* %rank, align 4
  %cmp10 = icmp ult i32 %7, %9
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplex11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %10 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex11, align 4
  %p12 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %10, i32 0, i32 1
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p12, i32 0, i32 %11
  %12 = load float, float* %arrayidx, align 4
  store float %12, float* %p, align 4
  %m_simplex15 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %13 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex15, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %13, i32 0, i32 0
  %14 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %14
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx16, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp14, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %p)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %m_simplex21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %16 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex21, align 4
  %c22 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %16, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %arrayidx23 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c22, i32 0, i32 %17
  %18 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx23, align 4
  %d24 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %18, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %d24)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp19, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20, i32 1)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %p)
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp26, %class.btTransform* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %21 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %21, i32 0, i32 1
  %arrayidx27 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %22 = bitcast %class.btVector3* %arrayidx27 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %24 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp28, %class.btTransform* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %25 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses29 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %25, i32 0, i32 1
  %arrayidx30 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses29, i32 0, i32 1
  %26 = bitcast %class.btVector3* %arrayidx30 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %28 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %28, i32 0, i32 2
  %29 = bitcast %class.btVector3* %normal to i8*
  %30 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  %31 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal32 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %31, i32 0, i32 2
  %call33 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %normal32)
  %32 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %distance = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %32, i32 0, i32 3
  store float %call33, float* %distance, align 4
  %33 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %distance35 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %33, i32 0, i32 3
  %34 = load float, float* %distance35, align 4
  %cmp36 = fcmp ogt float %34, 0x3F1A36E2E0000000
  br i1 %cmp36, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %35 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %distance37 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %35, i32 0, i32 3
  %36 = load float, float* %distance37, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %36, %cond.true ], [ 1.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp34, align 4
  %37 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal38 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %37, i32 0, i32 2
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %normal38, float* nonnull align 4 dereferenceable(4) %ref.tmp34)
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  %38 = load i32, i32* %gjk_status, align 4
  %cmp40 = icmp eq i32 %38, 1
  %39 = zext i1 %cmp40 to i64
  %cond41 = select i1 %cmp40, i32 1, i32 2
  %40 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %40, i32 0, i32 0
  store i32 %cond41, i32* %status, align 4
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.else, %cond.end
  %41 = load i1, i1* %retval, align 1
  ret i1 %41
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_toshape1)
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_toshape0)
  ret %"struct.gjkepa2_impl::MinkowskiDiff"* %this1
}

; Function Attrs: noinline optnone
define internal void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext %withmargins) #2 {
entry:
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %withmargins.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca %class.btMatrix3x3, align 4
  %ref.tmp13 = alloca %class.btTransform, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %frombool = zext i1 %withmargins to i8
  store i8 %frombool, i8* %withmargins.addr, align 1
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %0 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %0, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 1
  %1 = bitcast %class.btVector3* %arrayidx to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses4 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %3, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses4, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx5 to i8*
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %6, i32 0, i32 0
  store i32 0, i32* %status, align 4
  %7 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %8 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %8, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 0
  store %class.btConvexShape* %7, %class.btConvexShape** %arrayidx6, align 4
  %9 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4
  %10 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %m_shapes7 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %10, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes7, i32 0, i32 1
  store %class.btConvexShape* %9, %class.btConvexShape** %arrayidx8, align 4
  %11 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %11)
  %12 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %12)
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp9, %class.btMatrix3x3* %call10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call11)
  %13 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %13, i32 0, i32 1
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_toshape1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp9)
  %14 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %15 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  call void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* sret align 4 %ref.tmp13, %class.btTransform* %14, %class.btTransform* nonnull align 4 dereferenceable(64) %15)
  %16 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %16, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_toshape0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp13)
  %17 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4
  %18 = load i8, i8* %withmargins.addr, align 1
  %tobool = trunc i8 %18 to i1
  call void @_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb(%"struct.gjkepa2_impl::MinkowskiDiff"* %17, i1 zeroext %tobool)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK"** %retval, align 4
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape)
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_ray)
  %m_simplices = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_store = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %array.begin = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"struct.gjkepa2_impl::GJK::sSV"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call3 = call %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  call void @_ZN12gjkepa2_impl3GJK10InitializeEv(%"struct.gjkepa2_impl::GJK"* %this1)
  %0 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %retval, align 4
  ret %"struct.gjkepa2_impl::GJK"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shapearg, %class.btVector3* nonnull align 4 dereferenceable(16) %guess) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %shapearg.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %iterations = alloca i32, align 4
  %sqdist = alloca float, align 4
  %alpha = alloca float, align 4
  %lastw = alloca [4 x %class.btVector3], align 16
  %clastw = alloca i32, align 4
  %sqrl = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %next = alloca i32, align 4
  %cs = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %ns = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %rl = alloca float, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %w50 = alloca %class.btVector3*, align 4
  %found = alloca i8, align 1
  %i = alloca i32, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %omega = alloca float, align 4
  %weights = alloca [4 x float], align 16
  %mask = alloca i32, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %i125 = alloca i32, align 4
  %ni = alloca i32, align 4
  %ref.tmp143 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %shapearg, %"struct.gjkepa2_impl::MinkowskiDiff"** %shapearg.addr, align 4
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store i32 0, i32* %iterations, align 4
  store float 0.000000e+00, float* %sqdist, align 4
  store float 0.000000e+00, float* %alpha, align 4
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 0, i32* %clastw, align 4
  %m_store = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store, i32 0, i32 0
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4
  %m_store3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store3, i32 0, i32 1
  %m_free5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx6 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free5, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx4, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4
  %m_store7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store7, i32 0, i32 2
  %m_free9 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx10 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free9, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx8, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx10, align 4
  %m_store11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store11, i32 0, i32 3
  %m_free13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx14 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free13, i32 0, i32 3
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx12, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx14, align 4
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  store i32 4, i32* %m_nfree, align 4
  %m_current = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 0, i32* %m_current, align 4
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 0, i32* %m_status, align 4
  %0 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shapearg.addr, align 4
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %call15 = call nonnull align 4 dereferenceable(128) %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_(%"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %0)
  %m_distance = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance, align 4
  %m_simplices = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx16 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices, i32 0, i32 0
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx16, i32 0, i32 2
  store i32 0, i32* %rank, align 4
  %1 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_ray to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_ray17 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call18 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_ray17)
  store float %call18, float* %sqrl, align 4
  %m_simplices19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx20 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices19, i32 0, i32 0
  %4 = load float, float* %sqrl, align 4
  %cmp = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %m_ray21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray21)
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  store float 1.000000e+00, float* %ref.tmp22, align 4
  store float 0.000000e+00, float* %ref.tmp23, align 4
  store float 0.000000e+00, float* %ref.tmp24, align 4
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %m_simplices26 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx27 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices26, i32 0, i32 0
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx27, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 0
  store float 1.000000e+00, float* %arrayidx28, align 4
  %m_simplices29 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx30 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices29, i32 0, i32 0
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx30, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 0
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx31, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %5, i32 0, i32 1
  %m_ray32 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %6 = bitcast %class.btVector3* %m_ray32 to i8*
  %7 = bitcast %class.btVector3* %w to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load float, float* %sqrl, align 4
  store float %8, float* %sqdist, align 4
  %m_ray33 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %arrayidx34 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 3
  %9 = bitcast %class.btVector3* %arrayidx34 to i8*
  %10 = bitcast %class.btVector3* %m_ray33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %9, i8* align 4 %10, i32 16, i1 false)
  %arrayidx35 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 2
  %11 = bitcast %class.btVector3* %arrayidx35 to i8*
  %12 = bitcast %class.btVector3* %arrayidx34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %11, i8* align 4 %12, i32 16, i1 false)
  %arrayidx36 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 1
  %13 = bitcast %class.btVector3* %arrayidx36 to i8*
  %14 = bitcast %class.btVector3* %arrayidx35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %13, i8* align 4 %14, i32 16, i1 false)
  %arrayidx37 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 0
  %15 = bitcast %class.btVector3* %arrayidx37 to i8*
  %16 = bitcast %class.btVector3* %arrayidx36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %15, i8* align 4 %16, i32 16, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %cond.end
  %m_current38 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %17 = load i32, i32* %m_current38, align 4
  %sub = sub i32 1, %17
  store i32 %sub, i32* %next, align 4
  %m_simplices39 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current40 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %18 = load i32, i32* %m_current40, align 4
  %arrayidx41 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices39, i32 0, i32 %18
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx41, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %m_simplices42 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %19 = load i32, i32* %next, align 4
  %arrayidx43 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices42, i32 0, i32 %19
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx43, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %m_ray44 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call45 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_ray44)
  store float %call45, float* %rl, align 4
  %20 = load float, float* %rl, align 4
  %cmp46 = fcmp olt float %20, 0x3F1A36E2E0000000
  br i1 %cmp46, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  %m_status47 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 1, i32* %m_status47, align 4
  br label %do.end

if.end:                                           ; preds = %do.body
  %21 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %m_ray49 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray49)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp48)
  %22 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c51 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %22, i32 0, i32 0
  %23 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %rank52 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %23, i32 0, i32 2
  %24 = load i32, i32* %rank52, align 4
  %sub53 = sub i32 %24, 1
  %arrayidx54 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c51, i32 0, i32 %sub53
  %25 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx54, align 4
  %w55 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %25, i32 0, i32 1
  store %class.btVector3* %w55, %class.btVector3** %w50, align 4
  store i8 0, i8* %found, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %26 = load i32, i32* %i, align 4
  %cmp56 = icmp ult i32 %26, 4
  br i1 %cmp56, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %27 = load %class.btVector3*, %class.btVector3** %w50, align 4
  %28 = load i32, i32* %i, align 4
  %arrayidx58 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 %28
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp57, %class.btVector3* nonnull align 4 dereferenceable(16) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx58)
  %call59 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp57)
  %cmp60 = fcmp olt float %call59, 0x3F1A36E2E0000000
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %for.body
  store i8 1, i8* %found, align 1
  br label %for.end

if.end62:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end62
  %29 = load i32, i32* %i, align 4
  %inc = add i32 %29, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then61, %for.cond
  %30 = load i8, i8* %found, align 1
  %tobool = trunc i8 %30 to i1
  br i1 %tobool, label %if.then63, label %if.else

if.then63:                                        ; preds = %for.end
  %m_simplices64 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current65 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %31 = load i32, i32* %m_current65, align 4
  %arrayidx66 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices64, i32 0, i32 %31
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx66)
  br label %do.end

if.else:                                          ; preds = %for.end
  %32 = load %class.btVector3*, %class.btVector3** %w50, align 4
  %33 = load i32, i32* %clastw, align 4
  %add = add i32 %33, 1
  %and = and i32 %add, 3
  store i32 %and, i32* %clastw, align 4
  %arrayidx67 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 %and
  %34 = bitcast %class.btVector3* %arrayidx67 to i8*
  %35 = bitcast %class.btVector3* %32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %34, i8* align 4 %35, i32 16, i1 false)
  br label %if.end68

if.end68:                                         ; preds = %if.else
  %m_ray69 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %36 = load %class.btVector3*, %class.btVector3** %w50, align 4
  %call70 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_ray69, %class.btVector3* nonnull align 4 dereferenceable(16) %36)
  %37 = load float, float* %rl, align 4
  %div = fdiv float %call70, %37
  store float %div, float* %omega, align 4
  %call71 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %omega, float* nonnull align 4 dereferenceable(4) %alpha)
  %38 = load float, float* %call71, align 4
  store float %38, float* %alpha, align 4
  %39 = load float, float* %rl, align 4
  %40 = load float, float* %alpha, align 4
  %sub72 = fsub float %39, %40
  %41 = load float, float* %rl, align 4
  %mul = fmul float 0x3F1A36E2E0000000, %41
  %sub73 = fsub float %sub72, %mul
  %cmp74 = fcmp ole float %sub73, 0.000000e+00
  br i1 %cmp74, label %if.then75, label %if.end79

if.then75:                                        ; preds = %if.end68
  %m_simplices76 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current77 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %42 = load i32, i32* %m_current77, align 4
  %arrayidx78 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices76, i32 0, i32 %42
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx78)
  br label %do.end

if.end79:                                         ; preds = %if.end68
  store i32 0, i32* %mask, align 4
  %43 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %rank80 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %43, i32 0, i32 2
  %44 = load i32, i32* %rank80, align 4
  switch i32 %44, label %sw.epilog [
    i32 2, label %sw.bb
    i32 3, label %sw.bb88
    i32 4, label %sw.bb100
  ]

sw.bb:                                            ; preds = %if.end79
  %45 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c81 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %45, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c81, i32 0, i32 0
  %46 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx82, align 4
  %w83 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %46, i32 0, i32 1
  %47 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c84 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %47, i32 0, i32 0
  %arrayidx85 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c84, i32 0, i32 1
  %48 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx85, align 4
  %w86 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %48, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call87 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w83, %class.btVector3* nonnull align 4 dereferenceable(16) %w86, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call87, float* %sqdist, align 4
  br label %sw.epilog

sw.bb88:                                          ; preds = %if.end79
  %49 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c89 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %49, i32 0, i32 0
  %arrayidx90 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c89, i32 0, i32 0
  %50 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx90, align 4
  %w91 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %50, i32 0, i32 1
  %51 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c92 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %51, i32 0, i32 0
  %arrayidx93 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c92, i32 0, i32 1
  %52 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx93, align 4
  %w94 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %52, i32 0, i32 1
  %53 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c95 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %53, i32 0, i32 0
  %arrayidx96 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c95, i32 0, i32 2
  %54 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx96, align 4
  %w97 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %54, i32 0, i32 1
  %arraydecay98 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call99 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w91, %class.btVector3* nonnull align 4 dereferenceable(16) %w94, %class.btVector3* nonnull align 4 dereferenceable(16) %w97, float* %arraydecay98, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call99, float* %sqdist, align 4
  br label %sw.epilog

sw.bb100:                                         ; preds = %if.end79
  %55 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c101 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %55, i32 0, i32 0
  %arrayidx102 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c101, i32 0, i32 0
  %56 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx102, align 4
  %w103 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %56, i32 0, i32 1
  %57 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c104 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %57, i32 0, i32 0
  %arrayidx105 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c104, i32 0, i32 1
  %58 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx105, align 4
  %w106 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %58, i32 0, i32 1
  %59 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c107 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %59, i32 0, i32 0
  %arrayidx108 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c107, i32 0, i32 2
  %60 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx108, align 4
  %w109 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %60, i32 0, i32 1
  %61 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c110 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %61, i32 0, i32 0
  %arrayidx111 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c110, i32 0, i32 3
  %62 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx111, align 4
  %w112 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %62, i32 0, i32 1
  %arraydecay113 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call114 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w103, %class.btVector3* nonnull align 4 dereferenceable(16) %w106, %class.btVector3* nonnull align 4 dereferenceable(16) %w109, %class.btVector3* nonnull align 4 dereferenceable(16) %w112, float* %arraydecay113, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call114, float* %sqdist, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end79, %sw.bb100, %sw.bb88, %sw.bb
  %63 = load float, float* %sqdist, align 4
  %cmp115 = fcmp oge float %63, 0.000000e+00
  br i1 %cmp115, label %if.then116, label %if.else165

if.then116:                                       ; preds = %sw.epilog
  %64 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %rank117 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %64, i32 0, i32 2
  store i32 0, i32* %rank117, align 4
  store float 0.000000e+00, float* %ref.tmp119, align 4
  store float 0.000000e+00, float* %ref.tmp120, align 4
  store float 0.000000e+00, float* %ref.tmp121, align 4
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %m_ray123 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %65 = bitcast %class.btVector3* %m_ray123 to i8*
  %66 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false)
  %67 = load i32, i32* %next, align 4
  %m_current124 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 %67, i32* %m_current124, align 4
  store i32 0, i32* %i125, align 4
  %68 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %rank126 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %68, i32 0, i32 2
  %69 = load i32, i32* %rank126, align 4
  store i32 %69, i32* %ni, align 4
  br label %for.cond127

for.cond127:                                      ; preds = %for.inc158, %if.then116
  %70 = load i32, i32* %i125, align 4
  %71 = load i32, i32* %ni, align 4
  %cmp128 = icmp ult i32 %70, %71
  br i1 %cmp128, label %for.body129, label %for.end160

for.body129:                                      ; preds = %for.cond127
  %72 = load i32, i32* %mask, align 4
  %73 = load i32, i32* %i125, align 4
  %shl = shl i32 1, %73
  %and130 = and i32 %72, %shl
  %tobool131 = icmp ne i32 %and130, 0
  br i1 %tobool131, label %if.then132, label %if.else150

if.then132:                                       ; preds = %for.body129
  %74 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c133 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %74, i32 0, i32 0
  %75 = load i32, i32* %i125, align 4
  %arrayidx134 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c133, i32 0, i32 %75
  %76 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx134, align 4
  %77 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %c135 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %77, i32 0, i32 0
  %78 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %rank136 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %78, i32 0, i32 2
  %79 = load i32, i32* %rank136, align 4
  %arrayidx137 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c135, i32 0, i32 %79
  store %"struct.gjkepa2_impl::GJK::sSV"* %76, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx137, align 4
  %80 = load i32, i32* %i125, align 4
  %arrayidx138 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 %80
  %81 = load float, float* %arrayidx138, align 4
  %82 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %p139 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %82, i32 0, i32 1
  %83 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4
  %rank140 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %83, i32 0, i32 2
  %84 = load i32, i32* %rank140, align 4
  %inc141 = add i32 %84, 1
  store i32 %inc141, i32* %rank140, align 4
  %arrayidx142 = getelementptr inbounds [4 x float], [4 x float]* %p139, i32 0, i32 %84
  store float %81, float* %arrayidx142, align 4
  %85 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c144 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %85, i32 0, i32 0
  %86 = load i32, i32* %i125, align 4
  %arrayidx145 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c144, i32 0, i32 %86
  %87 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx145, align 4
  %w146 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %87, i32 0, i32 1
  %88 = load i32, i32* %i125, align 4
  %arrayidx147 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 %88
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp143, %class.btVector3* nonnull align 4 dereferenceable(16) %w146, float* nonnull align 4 dereferenceable(4) %arrayidx147)
  %m_ray148 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call149 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_ray148, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp143)
  br label %if.end157

if.else150:                                       ; preds = %for.body129
  %89 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4
  %c151 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %89, i32 0, i32 0
  %90 = load i32, i32* %i125, align 4
  %arrayidx152 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c151, i32 0, i32 %90
  %91 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx152, align 4
  %m_free153 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree154 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %92 = load i32, i32* %m_nfree154, align 4
  %inc155 = add i32 %92, 1
  store i32 %inc155, i32* %m_nfree154, align 4
  %arrayidx156 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free153, i32 0, i32 %92
  store %"struct.gjkepa2_impl::GJK::sSV"* %91, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx156, align 4
  br label %if.end157

if.end157:                                        ; preds = %if.else150, %if.then132
  br label %for.inc158

for.inc158:                                       ; preds = %if.end157
  %93 = load i32, i32* %i125, align 4
  %inc159 = add i32 %93, 1
  store i32 %inc159, i32* %i125, align 4
  br label %for.cond127

for.end160:                                       ; preds = %for.cond127
  %94 = load i32, i32* %mask, align 4
  %cmp161 = icmp eq i32 %94, 15
  br i1 %cmp161, label %if.then162, label %if.end164

if.then162:                                       ; preds = %for.end160
  %m_status163 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 1, i32* %m_status163, align 4
  br label %if.end164

if.end164:                                        ; preds = %if.then162, %for.end160
  br label %if.end169

if.else165:                                       ; preds = %sw.epilog
  %m_simplices166 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current167 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %95 = load i32, i32* %m_current167, align 4
  %arrayidx168 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices166, i32 0, i32 %95
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx168)
  br label %do.end

if.end169:                                        ; preds = %if.end164
  %96 = load i32, i32* %iterations, align 4
  %inc170 = add i32 %96, 1
  store i32 %inc170, i32* %iterations, align 4
  %cmp171 = icmp ult i32 %inc170, 128
  br i1 %cmp171, label %cond.true172, label %cond.false174

cond.true172:                                     ; preds = %if.end169
  %m_status173 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %97 = load i32, i32* %m_status173, align 4
  br label %cond.end175

cond.false174:                                    ; preds = %if.end169
  br label %cond.end175

cond.end175:                                      ; preds = %cond.false174, %cond.true172
  %cond = phi i32 [ %97, %cond.true172 ], [ 2, %cond.false174 ]
  %m_status176 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 %cond, i32* %m_status176, align 4
  br label %do.cond

do.cond:                                          ; preds = %cond.end175
  %m_status177 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %98 = load i32, i32* %m_status177, align 4
  %cmp178 = icmp eq i32 %98, 0
  br i1 %cmp178, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %if.else165, %if.then75, %if.then63, %if.then
  %m_simplices179 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current180 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %99 = load i32, i32* %m_current180, align 4
  %arrayidx181 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices179, i32 0, i32 %99
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx181, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4
  %m_status182 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %100 = load i32, i32* %m_status182, align 4
  switch i32 %100, label %sw.default [
    i32 0, label %sw.bb183
    i32 1, label %sw.bb187
  ]

sw.bb183:                                         ; preds = %do.end
  %m_ray184 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call185 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_ray184)
  %m_distance186 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float %call185, float* %m_distance186, align 4
  br label %sw.epilog189

sw.bb187:                                         ; preds = %do.end
  %m_distance188 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance188, align 4
  br label %sw.epilog189

sw.default:                                       ; preds = %do.end
  br label %sw.epilog189

sw.epilog189:                                     ; preds = %sw.default, %sw.bb187, %sw.bb183
  %m_status190 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %101 = load i32, i32* %m_status190, align 4
  ret i32 %101
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %index.addr = alloca i32, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  br label %return

if.else:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  br label %return

return:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results, i1 zeroext %usemargins) #2 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %usemargins.addr = alloca i8, align 1
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %epa = alloca %"struct.gjkepa2_impl::EPA", align 4
  %epa_status = alloca i32, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %frombool = zext i1 %usemargins to i8
  store i8 %frombool, i8* %usemargins.addr, align 1
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %2 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  %4 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %5 = load i8, i8* %usemargins.addr, align 1
  %tobool = trunc i8 %5 to i1
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %4, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext %tobool)
  %call1 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  %6 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %call2 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store i32 %call2, i32* %gjk_status, align 4
  %7 = load i32, i32* %gjk_status, align 4
  switch i32 %7, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb28
  ]

sw.bb:                                            ; preds = %entry
  %call3 = call %"struct.gjkepa2_impl::EPA"* @_ZN12gjkepa2_impl3EPAC2Ev(%"struct.gjkepa2_impl::EPA"* %epa)
  %8 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %call5 = call i32 @_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3(%"struct.gjkepa2_impl::EPA"* %epa, %"struct.gjkepa2_impl::GJK"* nonnull align 4 dereferenceable(380) %gjk, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  store i32 %call5, i32* %epa_status, align 4
  %9 = load i32, i32* %epa_status, align 4
  %cmp = icmp ne i32 %9, 9
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result, i32 0, i32 2
  %11 = load i32, i32* %rank, align 4
  %cmp10 = icmp ult i32 %10, %11
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_result13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result13, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %12
  %13 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %13, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp12, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  %m_result14 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result14, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 %14
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %arrayidx15)
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %16, i32 0, i32 0
  store i32 1, i32* %status, align 4
  %17 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btTransform* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %18 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %18, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %19 = bitcast %class.btVector3* %arrayidx18 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 2
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %m_depth)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, %class.btTransform* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %22 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses22 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %22, i32 0, i32 1
  %arrayidx23 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses22, i32 0, i32 1
  %23 = bitcast %class.btVector3* %arrayidx23 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %m_normal25 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal25)
  %25 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %25, i32 0, i32 2
  %26 = bitcast %class.btVector3* %normal to i8*
  %27 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %m_depth26 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 3
  %28 = load float, float* %m_depth26, align 4
  %fneg = fneg float %28
  %29 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %distance = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %29, i32 0, i32 3
  store float %fneg, float* %distance, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %sw.bb
  %30 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %status27 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %30, i32 0, i32 0
  store i32 3, i32* %status27, align 4
  br label %if.end

if.end:                                           ; preds = %if.else
  br label %sw.epilog

sw.bb28:                                          ; preds = %entry
  %31 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %status29 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %31, i32 0, i32 0
  store i32 2, i32* %status29, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb28, %if.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %sw.epilog, %for.end
  %32 = load i1, i1* %retval, align 1
  ret i1 %32
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA"* @_ZN12gjkepa2_impl3EPAC2Ev(%"struct.gjkepa2_impl::EPA"* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store %"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA"** %retval, align 4
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normal)
  %m_sv_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 4
  %array.begin = getelementptr inbounds [128 x %"struct.gjkepa2_impl::GJK::sSV"], [128 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_sv_store, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"struct.gjkepa2_impl::GJK::sSV"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_fc_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 5
  %array.begin3 = getelementptr inbounds [256 x %"struct.gjkepa2_impl::EPA::sFace"], [256 x %"struct.gjkepa2_impl::EPA::sFace"]* %m_fc_store, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %array.begin3, i32 256
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %"struct.gjkepa2_impl::EPA::sFace"* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA5sFaceC2Ev(%"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %call11 = call %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* %m_hull)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %call12 = call %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* %m_stock)
  call void @_ZN12gjkepa2_impl3EPA10InitializeEv(%"struct.gjkepa2_impl::EPA"* %this1)
  %0 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %retval, align 4
  ret %"struct.gjkepa2_impl::EPA"* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::GJK"* nonnull align 4 dereferenceable(380) %gjk, %class.btVector3* nonnull align 4 dereferenceable(16) %guess) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %gjk.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %simplex = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %f = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %tetra = alloca [4 x %"struct.gjkepa2_impl::EPA::sFace"*], align 16
  %best = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %outer = alloca %"struct.gjkepa2_impl::EPA::sFace", align 4
  %pass = alloca i32, align 4
  %iterations = alloca i32, align 4
  %horizon = alloca %"struct.gjkepa2_impl::EPA::sHorizon", align 4
  %w84 = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %valid = alloca i8, align 1
  %wdist = alloca float, align 4
  %j = alloca i32, align 4
  %projection = alloca %class.btVector3, align 4
  %ref.tmp146 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca %class.btVector3, align 4
  %ref.tmp151 = alloca %class.btVector3, align 4
  %ref.tmp159 = alloca %class.btVector3, align 4
  %ref.tmp160 = alloca %class.btVector3, align 4
  %ref.tmp164 = alloca %class.btVector3, align 4
  %ref.tmp172 = alloca %class.btVector3, align 4
  %ref.tmp173 = alloca %class.btVector3, align 4
  %ref.tmp177 = alloca %class.btVector3, align 4
  %sum = alloca float, align 4
  %ref.tmp210 = alloca %class.btVector3, align 4
  %nl = alloca float, align 4
  %ref.tmp216 = alloca %class.btVector3, align 4
  %ref.tmp220 = alloca %class.btVector3, align 4
  %ref.tmp221 = alloca float, align 4
  %ref.tmp222 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %0, i32 0, i32 8
  %1 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %1, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %2 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %2, i32 0, i32 2
  %3 = load i32, i32* %rank, align 4
  %cmp = icmp ugt i32 %3, 1
  br i1 %cmp, label %land.lhs.true, label %if.end208

land.lhs.true:                                    ; preds = %entry
  %4 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4
  %call = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %4)
  br i1 %call, label %if.then, label %if.end208

if.then:                                          ; preds = %land.lhs.true
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull, i32 0, i32 0
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %5, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_hull2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull2, i32 0, i32 0
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %6, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  %m_hull4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull4, %"struct.gjkepa2_impl::EPA::sFace"* %7)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %8)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 0, i32* %m_status, align 4
  %m_nextsv = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nextsv, align 4
  %9 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4
  %10 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %10, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 0
  %11 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %11, i32 0, i32 1
  %12 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %12, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 3
  %13 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4
  %w7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %13, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w7)
  %14 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c9 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %14, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c9, i32 0, i32 1
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx10, align 4
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 1
  %16 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c12 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c12, i32 0, i32 3
  %17 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx13, align 4
  %w14 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %17, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %w11, %class.btVector3* nonnull align 4 dereferenceable(16) %w14)
  %18 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c16 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %18, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c16, i32 0, i32 2
  %19 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx17, align 4
  %w18 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %19, i32 0, i32 1
  %20 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %20, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c19, i32 0, i32 3
  %21 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx20, align 4
  %w21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %21, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %w18, %class.btVector3* nonnull align 4 dereferenceable(16) %w21)
  %call22 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %cmp23 = fcmp olt float %call22, 0.000000e+00
  br i1 %cmp23, label %if.then24, label %if.end

if.then24:                                        ; preds = %while.end
  %22 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c25 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %22, i32 0, i32 0
  %arrayidx26 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c25, i32 0, i32 0
  %23 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c27 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %23, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c27, i32 0, i32 1
  call void @_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_(%"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %arrayidx26, %"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %arrayidx28)
  %24 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %24, i32 0, i32 1
  %arrayidx29 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 0
  %25 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %p30 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %25, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [4 x float], [4 x float]* %p30, i32 0, i32 1
  call void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %arrayidx29, float* nonnull align 4 dereferenceable(4) %arrayidx31)
  br label %if.end

if.end:                                           ; preds = %if.then24, %while.end
  %arrayinit.begin = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %26 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c32 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %26, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c32, i32 0, i32 0
  %27 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx33, align 4
  %28 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c34 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %28, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c34, i32 0, i32 1
  %29 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx35, align 4
  %30 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c36 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %30, i32 0, i32 0
  %arrayidx37 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c36, i32 0, i32 2
  %31 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx37, align 4
  %call38 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %27, %"struct.gjkepa2_impl::GJK::sSV"* %29, %"struct.gjkepa2_impl::GJK::sSV"* %31, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call38, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.begin, i32 1
  %32 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c39 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %32, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c39, i32 0, i32 1
  %33 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx40, align 4
  %34 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c41 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %34, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c41, i32 0, i32 0
  %35 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx42, align 4
  %36 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c43 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %36, i32 0, i32 0
  %arrayidx44 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c43, i32 0, i32 3
  %37 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx44, align 4
  %call45 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %33, %"struct.gjkepa2_impl::GJK::sSV"* %35, %"struct.gjkepa2_impl::GJK::sSV"* %37, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call45, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element, align 4
  %arrayinit.element46 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element, i32 1
  %38 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c47 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %38, i32 0, i32 0
  %arrayidx48 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c47, i32 0, i32 2
  %39 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx48, align 4
  %40 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c49 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %40, i32 0, i32 0
  %arrayidx50 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c49, i32 0, i32 1
  %41 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx50, align 4
  %42 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c51 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %42, i32 0, i32 0
  %arrayidx52 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c51, i32 0, i32 3
  %43 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx52, align 4
  %call53 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %39, %"struct.gjkepa2_impl::GJK::sSV"* %41, %"struct.gjkepa2_impl::GJK::sSV"* %43, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call53, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element46, align 4
  %arrayinit.element54 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element46, i32 1
  %44 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c55 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %44, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c55, i32 0, i32 0
  %45 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx56, align 4
  %46 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c57 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %46, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c57, i32 0, i32 2
  %47 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx58, align 4
  %48 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c59 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %48, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c59, i32 0, i32 3
  %49 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx60, align 4
  %call61 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %45, %"struct.gjkepa2_impl::GJK::sSV"* %47, %"struct.gjkepa2_impl::GJK::sSV"* %49, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call61, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element54, align 4
  %m_hull62 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull62, i32 0, i32 1
  %50 = load i32, i32* %count, align 4
  %cmp63 = icmp eq i32 %50, 4
  br i1 %cmp63, label %if.then64, label %if.end207

if.then64:                                        ; preds = %if.end
  %call65 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this1)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call65, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %51 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %52 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  %53 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 56, i1 false)
  store i32 0, i32* %pass, align 4
  store i32 0, i32* %iterations, align 4
  %arrayidx66 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %54 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx66, align 16
  %arrayidx67 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %55 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx67, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %54, i32 0, %"struct.gjkepa2_impl::EPA::sFace"* %55, i32 0)
  %arrayidx68 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %56 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx68, align 16
  %arrayidx69 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %57 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx69, align 8
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %56, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %57, i32 0)
  %arrayidx70 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %58 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx70, align 16
  %arrayidx71 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %59 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx71, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %58, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %59, i32 0)
  %arrayidx72 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %60 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx72, align 4
  %arrayidx73 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %61 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx73, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %60, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %61, i32 2)
  %arrayidx74 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %62 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx74, align 4
  %arrayidx75 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %63 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx75, align 8
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %62, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %63, i32 1)
  %arrayidx76 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %64 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx76, align 8
  %arrayidx77 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %65 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx77, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %64, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %65, i32 1)
  %m_status78 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 0, i32* %m_status78, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc123, %if.then64
  %66 = load i32, i32* %iterations, align 4
  %cmp79 = icmp ult i32 %66, 255
  br i1 %cmp79, label %for.body, label %for.end125

for.body:                                         ; preds = %for.cond
  %m_nextsv80 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  %67 = load i32, i32* %m_nextsv80, align 4
  %cmp81 = icmp ult i32 %67, 128
  br i1 %cmp81, label %if.then82, label %if.else120

if.then82:                                        ; preds = %for.body
  %call83 = call %"struct.gjkepa2_impl::EPA::sHorizon"* @_ZN12gjkepa2_impl3EPA8sHorizonC2Ev(%"struct.gjkepa2_impl::EPA::sHorizon"* %horizon)
  %m_sv_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 4
  %m_nextsv85 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  %68 = load i32, i32* %m_nextsv85, align 4
  %inc = add i32 %68, 1
  store i32 %inc, i32* %m_nextsv85, align 4
  %arrayidx86 = getelementptr inbounds [128 x %"struct.gjkepa2_impl::GJK::sSV"], [128 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_sv_store, i32 0, i32 %68
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx86, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4
  store i8 1, i8* %valid, align 1
  %69 = load i32, i32* %pass, align 4
  %inc87 = add i32 %69, 1
  store i32 %inc87, i32* %pass, align 4
  %conv = trunc i32 %inc87 to i8
  %70 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %pass88 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %70, i32 0, i32 6
  store i8 %conv, i8* %pass88, align 1
  %71 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4
  %72 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %72, i32 0, i32 0
  %73 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4
  call void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %71, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %73)
  %74 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %n89 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %74, i32 0, i32 0
  %75 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4
  %w90 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %75, i32 0, i32 1
  %call91 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n89, %class.btVector3* nonnull align 4 dereferenceable(16) %w90)
  %76 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %76, i32 0, i32 1
  %77 = load float, float* %d, align 4
  %sub = fsub float %call91, %77
  store float %sub, float* %wdist, align 4
  %78 = load float, float* %wdist, align 4
  %cmp92 = fcmp ogt float %78, 0x3F1A36E2E0000000
  br i1 %cmp92, label %if.then93, label %if.else117

if.then93:                                        ; preds = %if.then82
  store i32 0, i32* %j, align 4
  br label %for.cond94

for.cond94:                                       ; preds = %for.inc, %if.then93
  %79 = load i32, i32* %j, align 4
  %cmp95 = icmp ult i32 %79, 3
  br i1 %cmp95, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond94
  %80 = load i8, i8* %valid, align 1
  %tobool96 = trunc i8 %80 to i1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond94
  %81 = phi i1 [ false, %for.cond94 ], [ %tobool96, %land.rhs ]
  br i1 %81, label %for.body97, label %for.end

for.body97:                                       ; preds = %land.end
  %82 = load i32, i32* %pass, align 4
  %83 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4
  %84 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %f98 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %84, i32 0, i32 3
  %85 = load i32, i32* %j, align 4
  %arrayidx99 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f98, i32 0, i32 %85
  %86 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx99, align 4
  %87 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %e = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %87, i32 0, i32 5
  %88 = load i32, i32* %j, align 4
  %arrayidx100 = getelementptr inbounds [3 x i8], [3 x i8]* %e, i32 0, i32 %88
  %89 = load i8, i8* %arrayidx100, align 1
  %conv101 = zext i8 %89 to i32
  %call102 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %82, %"struct.gjkepa2_impl::GJK::sSV"* %83, %"struct.gjkepa2_impl::EPA::sFace"* %86, i32 %conv101, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %horizon)
  %conv103 = zext i1 %call102 to i32
  %90 = load i8, i8* %valid, align 1
  %tobool104 = trunc i8 %90 to i1
  %conv105 = zext i1 %tobool104 to i32
  %and = and i32 %conv105, %conv103
  %tobool106 = icmp ne i32 %and, 0
  %frombool = zext i1 %tobool106 to i8
  store i8 %frombool, i8* %valid, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body97
  %91 = load i32, i32* %j, align 4
  %inc107 = add i32 %91, 1
  store i32 %inc107, i32* %j, align 4
  br label %for.cond94

for.end:                                          ; preds = %land.end
  %92 = load i8, i8* %valid, align 1
  %tobool108 = trunc i8 %92 to i1
  br i1 %tobool108, label %land.lhs.true109, label %if.else

land.lhs.true109:                                 ; preds = %for.end
  %nf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 2
  %93 = load i32, i32* %nf, align 4
  %cmp110 = icmp uge i32 %93, 3
  br i1 %cmp110, label %if.then111, label %if.else

if.then111:                                       ; preds = %land.lhs.true109
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 0
  %94 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 1
  %95 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %94, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %95, i32 2)
  %m_hull112 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %96 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull112, %"struct.gjkepa2_impl::EPA::sFace"* %96)
  %m_stock113 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %97 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock113, %"struct.gjkepa2_impl::EPA::sFace"* %97)
  %call114 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this1)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call114, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %98 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4
  %99 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  %100 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %98 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %99, i8* align 4 %100, i32 56, i1 false)
  br label %if.end116

if.else:                                          ; preds = %land.lhs.true109, %for.end
  %m_status115 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 4, i32* %m_status115, align 4
  br label %for.end125

if.end116:                                        ; preds = %if.then111
  br label %if.end119

if.else117:                                       ; preds = %if.then82
  %m_status118 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 7, i32* %m_status118, align 4
  br label %for.end125

if.end119:                                        ; preds = %if.end116
  br label %if.end122

if.else120:                                       ; preds = %for.body
  %m_status121 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 6, i32* %m_status121, align 4
  br label %for.end125

if.end122:                                        ; preds = %if.end119
  br label %for.inc123

for.inc123:                                       ; preds = %if.end122
  %101 = load i32, i32* %iterations, align 4
  %inc124 = add i32 %101, 1
  store i32 %inc124, i32* %iterations, align 4
  br label %for.cond

for.end125:                                       ; preds = %if.else120, %if.else117, %if.else, %for.cond
  %n126 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 0
  %d127 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projection, %class.btVector3* nonnull align 4 dereferenceable(16) %n126, float* nonnull align 4 dereferenceable(4) %d127)
  %n128 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 0
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %102 = bitcast %class.btVector3* %m_normal to i8*
  %103 = bitcast %class.btVector3* %n128 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 4 %103, i32 16, i1 false)
  %d129 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 1
  %104 = load float, float* %d129, align 4
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float %104, float* %m_depth, align 4
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %rank130 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result, i32 0, i32 2
  store i32 3, i32* %rank130, align 4
  %c131 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx132 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c131, i32 0, i32 0
  %105 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx132, align 4
  %m_result133 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c134 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result133, i32 0, i32 0
  %arrayidx135 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c134, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %105, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx135, align 4
  %c136 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx137 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c136, i32 0, i32 1
  %106 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx137, align 4
  %m_result138 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c139 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result138, i32 0, i32 0
  %arrayidx140 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c139, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %106, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx140, align 4
  %c141 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx142 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c141, i32 0, i32 2
  %107 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx142, align 4
  %m_result143 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c144 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result143, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c144, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %107, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx145, align 4
  %c148 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx149 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c148, i32 0, i32 1
  %108 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx149, align 4
  %w150 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %108, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp147, %class.btVector3* nonnull align 4 dereferenceable(16) %w150, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %c152 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx153 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c152, i32 0, i32 2
  %109 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx153, align 4
  %w154 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %109, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp151, %class.btVector3* nonnull align 4 dereferenceable(16) %w154, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp146, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp147, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp151)
  %call155 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp146)
  %m_result156 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p157 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result156, i32 0, i32 1
  %arrayidx158 = getelementptr inbounds [4 x float], [4 x float]* %p157, i32 0, i32 0
  store float %call155, float* %arrayidx158, align 4
  %c161 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx162 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c161, i32 0, i32 2
  %110 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx162, align 4
  %w163 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %110, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp160, %class.btVector3* nonnull align 4 dereferenceable(16) %w163, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %c165 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx166 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c165, i32 0, i32 0
  %111 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx166, align 4
  %w167 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %111, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp164, %class.btVector3* nonnull align 4 dereferenceable(16) %w167, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp159, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp160, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp164)
  %call168 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp159)
  %m_result169 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p170 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result169, i32 0, i32 1
  %arrayidx171 = getelementptr inbounds [4 x float], [4 x float]* %p170, i32 0, i32 1
  store float %call168, float* %arrayidx171, align 4
  %c174 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx175 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c174, i32 0, i32 0
  %112 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx175, align 4
  %w176 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %112, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp173, %class.btVector3* nonnull align 4 dereferenceable(16) %w176, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %c178 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx179 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c178, i32 0, i32 1
  %113 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx179, align 4
  %w180 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %113, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp177, %class.btVector3* nonnull align 4 dereferenceable(16) %w180, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp172, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp173, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp177)
  %call181 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp172)
  %m_result182 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p183 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result182, i32 0, i32 1
  %arrayidx184 = getelementptr inbounds [4 x float], [4 x float]* %p183, i32 0, i32 2
  store float %call181, float* %arrayidx184, align 4
  %m_result185 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p186 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result185, i32 0, i32 1
  %arrayidx187 = getelementptr inbounds [4 x float], [4 x float]* %p186, i32 0, i32 0
  %114 = load float, float* %arrayidx187, align 4
  %m_result188 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p189 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result188, i32 0, i32 1
  %arrayidx190 = getelementptr inbounds [4 x float], [4 x float]* %p189, i32 0, i32 1
  %115 = load float, float* %arrayidx190, align 4
  %add = fadd float %114, %115
  %m_result191 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p192 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result191, i32 0, i32 1
  %arrayidx193 = getelementptr inbounds [4 x float], [4 x float]* %p192, i32 0, i32 2
  %116 = load float, float* %arrayidx193, align 4
  %add194 = fadd float %add, %116
  store float %add194, float* %sum, align 4
  %117 = load float, float* %sum, align 4
  %m_result195 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p196 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result195, i32 0, i32 1
  %arrayidx197 = getelementptr inbounds [4 x float], [4 x float]* %p196, i32 0, i32 0
  %118 = load float, float* %arrayidx197, align 4
  %div = fdiv float %118, %117
  store float %div, float* %arrayidx197, align 4
  %119 = load float, float* %sum, align 4
  %m_result198 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p199 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result198, i32 0, i32 1
  %arrayidx200 = getelementptr inbounds [4 x float], [4 x float]* %p199, i32 0, i32 1
  %120 = load float, float* %arrayidx200, align 4
  %div201 = fdiv float %120, %119
  store float %div201, float* %arrayidx200, align 4
  %121 = load float, float* %sum, align 4
  %m_result202 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p203 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result202, i32 0, i32 1
  %arrayidx204 = getelementptr inbounds [4 x float], [4 x float]* %p203, i32 0, i32 2
  %122 = load float, float* %arrayidx204, align 4
  %div205 = fdiv float %122, %121
  store float %div205, float* %arrayidx204, align 4
  %m_status206 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  %123 = load i32, i32* %m_status206, align 4
  store i32 %123, i32* %retval, align 4
  br label %return

if.end207:                                        ; preds = %if.end
  br label %if.end208

if.end208:                                        ; preds = %if.end207, %land.lhs.true, %entry
  %m_status209 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 8, i32* %m_status209, align 4
  %124 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp210, %class.btVector3* nonnull align 4 dereferenceable(16) %124)
  %m_normal211 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %125 = bitcast %class.btVector3* %m_normal211 to i8*
  %126 = bitcast %class.btVector3* %ref.tmp210 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %125, i8* align 4 %126, i32 16, i1 false)
  %m_normal212 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %call213 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_normal212)
  store float %call213, float* %nl, align 4
  %127 = load float, float* %nl, align 4
  %cmp214 = fcmp ogt float %127, 0.000000e+00
  br i1 %cmp214, label %if.then215, label %if.else219

if.then215:                                       ; preds = %if.end208
  %m_normal217 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp216, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal217, float* nonnull align 4 dereferenceable(4) %nl)
  %m_normal218 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %128 = bitcast %class.btVector3* %m_normal218 to i8*
  %129 = bitcast %class.btVector3* %ref.tmp216 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %128, i8* align 4 %129, i32 16, i1 false)
  br label %if.end226

if.else219:                                       ; preds = %if.end208
  store float 1.000000e+00, float* %ref.tmp221, align 4
  store float 0.000000e+00, float* %ref.tmp222, align 4
  store float 0.000000e+00, float* %ref.tmp223, align 4
  %call224 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp220, float* nonnull align 4 dereferenceable(4) %ref.tmp221, float* nonnull align 4 dereferenceable(4) %ref.tmp222, float* nonnull align 4 dereferenceable(4) %ref.tmp223)
  %m_normal225 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %130 = bitcast %class.btVector3* %m_normal225 to i8*
  %131 = bitcast %class.btVector3* %ref.tmp220 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %130, i8* align 4 %131, i32 16, i1 false)
  br label %if.end226

if.end226:                                        ; preds = %if.else219, %if.then215
  %m_depth227 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %m_depth227, align 4
  %m_result228 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %rank229 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result228, i32 0, i32 2
  store i32 1, i32* %rank229, align 4
  %132 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4
  %c230 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %132, i32 0, i32 0
  %arrayidx231 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c230, i32 0, i32 0
  %133 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx231, align 4
  %m_result232 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c233 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result232, i32 0, i32 0
  %arrayidx234 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c233, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %133, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx234, align 4
  %m_result235 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p236 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result235, i32 0, i32 1
  %arrayidx237 = getelementptr inbounds [4 x float], [4 x float]* %p236, i32 0, i32 0
  store float 1.000000e+00, float* %arrayidx237, align 4
  %m_status238 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  %134 = load i32, i32* %m_status238, align 4
  store i32 %134, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end226, %for.end125
  %135 = load i32, i32* %retval, align 4
  ret i32 %135
}

; Function Attrs: noinline optnone
define hidden float @_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE(%class.btVector3* nonnull align 4 dereferenceable(16) %position, float %margin, %class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #2 {
entry:
  %retval = alloca float, align 4
  %position.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %shape1 = alloca %class.btSphereShape, align 4
  %wtrs1 = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %w1 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %i = alloca i32, align 4
  %p = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %delta = alloca %class.btVector3, align 4
  %margin48 = alloca float, align 4
  %length = alloca float, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %delta62 = alloca %class.btVector3, align 4
  %length67 = alloca float, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  store %class.btVector3* %position, %class.btVector3** %position.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %0 = load float, float* %margin.addr, align 4
  %call1 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %shape1, float %0)
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %1 = load %class.btVector3*, %class.btVector3** %position.addr, align 4
  %call7 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %wtrs1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %4 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %5 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btConvexShape* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %5, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext false)
  %call8 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  store float 1.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  store float 1.000000e+00, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %call14 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  store i32 %call14, i32* %gjk_status, align 4
  %6 = load i32, i32* %gjk_status, align 4
  %cmp = icmp eq i32 %6, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  store float 0.000000e+00, float* %ref.tmp19, align 4
  store float 0.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w1, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %i, align 4
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %8 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %8, i32 0, i32 2
  %9 = load i32, i32* %rank, align 4
  %cmp23 = icmp ult i32 %7, %9
  br i1 %cmp23, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_simplex24 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %10 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex24, align 4
  %p25 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %10, i32 0, i32 1
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p25, i32 0, i32 %11
  %12 = load float, float* %arrayidx, align 4
  store float %12, float* %p, align 4
  %m_simplex28 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %13 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex28, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %13, i32 0, i32 0
  %14 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %14
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx29, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp27, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %p)
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %m_simplex34 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %16 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex34, align 4
  %c35 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %16, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c35, i32 0, i32 %17
  %18 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx36, align 4
  %d37 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %18, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %d37)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp32, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, i32 1)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %p)
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp39, %class.btTransform* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %21 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %21, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %22 = bitcast %class.btVector3* %arrayidx40 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %24 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btTransform* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %25 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses42 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %25, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses42, i32 0, i32 1
  %26 = bitcast %class.btVector3* %arrayidx43 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %28 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses44 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %28, i32 0, i32 1
  %arrayidx45 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses44, i32 0, i32 1
  %29 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses46 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %29, i32 0, i32 1
  %arrayidx47 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses46, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %delta, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx47)
  %30 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %call49 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %30)
  %31 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %call50 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %31)
  %add = fadd float %call49, %call50
  store float %add, float* %margin48, align 4
  %call51 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %delta)
  store float %call51, float* %length, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp52, %class.btVector3* nonnull align 4 dereferenceable(16) %delta, float* nonnull align 4 dereferenceable(4) %length)
  %32 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %32, i32 0, i32 2
  %33 = bitcast %class.btVector3* %normal to i8*
  %34 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  %35 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal54 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %35, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp53, %class.btVector3* nonnull align 4 dereferenceable(16) %normal54, float* nonnull align 4 dereferenceable(4) %margin48)
  %36 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses55 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %36, i32 0, i32 1
  %arrayidx56 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses55, i32 0, i32 0
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %37 = load float, float* %length, align 4
  %38 = load float, float* %margin48, align 4
  %sub = fsub float %37, %38
  store float %sub, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %entry
  %39 = load i32, i32* %gjk_status, align 4
  %cmp58 = icmp eq i32 %39, 1
  br i1 %cmp58, label %if.then59, label %if.end74

if.then59:                                        ; preds = %if.else
  %40 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %41 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %42 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 1
  %43 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %call60 = call zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %40, %class.btTransform* nonnull align 4 dereferenceable(64) %41, %class.btConvexShape* %42, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %43, i1 zeroext true)
  br i1 %call60, label %if.then61, label %if.end73

if.then61:                                        ; preds = %if.then59
  %44 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses63 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %44, i32 0, i32 1
  %arrayidx64 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses63, i32 0, i32 0
  %45 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %witnesses65 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %45, i32 0, i32 1
  %arrayidx66 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses65, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %delta62, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx64, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx66)
  %call68 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %delta62)
  store float %call68, float* %length67, align 4
  %46 = load float, float* %length67, align 4
  %cmp69 = fcmp oge float %46, 0x3E80000000000000
  br i1 %cmp69, label %if.then70, label %if.end

if.then70:                                        ; preds = %if.then61
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %delta62, float* nonnull align 4 dereferenceable(4) %length67)
  %47 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %normal72 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %47, i32 0, i32 2
  %48 = bitcast %class.btVector3* %normal72 to i8*
  %49 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then70, %if.then61
  %50 = load float, float* %length67, align 4
  %fneg = fneg float %50
  store float %fneg, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end73:                                         ; preds = %if.then59
  br label %if.end74

if.end74:                                         ; preds = %if.end73, %if.else
  br label %if.end75

if.end75:                                         ; preds = %if.end74
  store float 0x47EFFFFFE0000000, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end75, %if.end, %for.end
  %call76 = call %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* %shape1) #8
  %51 = load float, float* %retval, align 4
  ret float %51
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  store float %radius, float* %radius.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

declare float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* %0) #8
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #2 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %2 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  %5 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %call = call zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %5)
  br i1 %call, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %6 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4
  %7 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4
  %8 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4
  %9 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4
  %11 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4
  %call1 = call zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %7, %class.btConvexShape* %8, %class.btTransform* nonnull align 4 dereferenceable(64) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %11, i1 zeroext false)
  store i1 %call1, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.else, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call2)
  %3 = load float, float* %call3, align 4
  %mul = fmul float %1, %3
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 1)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call7)
  %6 = load float, float* %call8, align 4
  %mul9 = fmul float %4, %6
  %add = fadd float %mul, %mul9
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx11)
  %7 = load float, float* %call12, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 2)
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call13)
  %9 = load float, float* %call14, align 4
  %mul15 = fmul float %7, %9
  %add16 = fadd float %add, %mul15
  store float %add16, float* %ref.tmp, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx19)
  %10 = load float, float* %call20, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call21)
  %12 = load float, float* %call22, align 4
  %mul23 = fmul float %10, %12
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx25)
  %13 = load float, float* %call26, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %15 = load float, float* %call28, align 4
  %mul29 = fmul float %13, %15
  %add30 = fadd float %mul23, %mul29
  %m_el31 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el31, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx32)
  %16 = load float, float* %call33, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 2)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call34)
  %18 = load float, float* %call35, align 4
  %mul36 = fmul float %16, %18
  %add37 = fadd float %add30, %mul36
  store float %add37, float* %ref.tmp17, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 0
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx40)
  %19 = load float, float* %call41, align 4
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %20, i32 0)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call42)
  %21 = load float, float* %call43, align 4
  %mul44 = fmul float %19, %21
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx46)
  %22 = load float, float* %call47, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call48)
  %24 = load float, float* %call49, align 4
  %mul50 = fmul float %22, %24
  %add51 = fadd float %mul44, %mul50
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 2
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %25 = load float, float* %call54, align 4
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %26, i32 2)
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call55)
  %27 = load float, float* %call56, align 4
  %mul57 = fmul float %25, %27
  %add58 = fadd float %add51, %mul57
  store float %add58, float* %ref.tmp38, align 4
  %m_el60 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el60, i32 0, i32 0
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx61)
  %28 = load float, float* %call62, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 0)
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call63)
  %30 = load float, float* %call64, align 4
  %mul65 = fmul float %28, %30
  %m_el66 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el66, i32 0, i32 1
  %call68 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx67)
  %31 = load float, float* %call68, align 4
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 1)
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call69)
  %33 = load float, float* %call70, align 4
  %mul71 = fmul float %31, %33
  %add72 = fadd float %mul65, %mul71
  %m_el73 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el73, i32 0, i32 2
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx74)
  %34 = load float, float* %call75, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call76)
  %36 = load float, float* %call77, align 4
  %mul78 = fmul float %34, %36
  %add79 = fadd float %add72, %mul78
  store float %add79, float* %ref.tmp59, align 4
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 0
  %call83 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx82)
  %37 = load float, float* %call83, align 4
  %38 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %38, i32 0)
  %call85 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call84)
  %39 = load float, float* %call85, align 4
  %mul86 = fmul float %37, %39
  %m_el87 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx88 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el87, i32 0, i32 1
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx88)
  %40 = load float, float* %call89, align 4
  %41 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %41, i32 1)
  %call91 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call90)
  %42 = load float, float* %call91, align 4
  %mul92 = fmul float %40, %42
  %add93 = fadd float %mul86, %mul92
  %m_el94 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el94, i32 0, i32 2
  %call96 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx95)
  %43 = load float, float* %call96, align 4
  %44 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %44, i32 2)
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call97)
  %45 = load float, float* %call98, align 4
  %mul99 = fmul float %43, %45
  %add100 = fadd float %add93, %mul99
  store float %add100, float* %ref.tmp80, align 4
  %m_el102 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el102, i32 0, i32 0
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx103)
  %46 = load float, float* %call104, align 4
  %47 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %47, i32 0)
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call105)
  %48 = load float, float* %call106, align 4
  %mul107 = fmul float %46, %48
  %m_el108 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el108, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx109)
  %49 = load float, float* %call110, align 4
  %50 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %50, i32 1)
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call111)
  %51 = load float, float* %call112, align 4
  %mul113 = fmul float %49, %51
  %add114 = fadd float %mul107, %mul113
  %m_el115 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el115, i32 0, i32 2
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx116)
  %52 = load float, float* %call117, align 4
  %53 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %53, i32 2)
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call118)
  %54 = load float, float* %call119, align 4
  %mul120 = fmul float %52, %54
  %add121 = fadd float %add114, %mul120
  store float %add121, float* %ref.tmp101, align 4
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 0
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx124)
  %55 = load float, float* %call125, align 4
  %56 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call126 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %56, i32 0)
  %call127 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call126)
  %57 = load float, float* %call127, align 4
  %mul128 = fmul float %55, %57
  %m_el129 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx130 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el129, i32 0, i32 1
  %call131 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx130)
  %58 = load float, float* %call131, align 4
  %59 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %59, i32 1)
  %call133 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call132)
  %60 = load float, float* %call133, align 4
  %mul134 = fmul float %58, %60
  %add135 = fadd float %mul128, %mul134
  %m_el136 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el136, i32 0, i32 2
  %call138 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx137)
  %61 = load float, float* %call138, align 4
  %62 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %62, i32 2)
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call139)
  %63 = load float, float* %call140, align 4
  %mul141 = fmul float %61, %63
  %add142 = fadd float %add135, %mul141
  store float %add142, float* %ref.tmp122, align 4
  %m_el144 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el144, i32 0, i32 0
  %call146 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx145)
  %64 = load float, float* %call146, align 4
  %65 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %65, i32 0)
  %call148 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call147)
  %66 = load float, float* %call148, align 4
  %mul149 = fmul float %64, %66
  %m_el150 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx151 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el150, i32 0, i32 1
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx151)
  %67 = load float, float* %call152, align 4
  %68 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call153 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %68, i32 1)
  %call154 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call153)
  %69 = load float, float* %call154, align 4
  %mul155 = fmul float %67, %69
  %add156 = fadd float %mul149, %mul155
  %m_el157 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx158 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el157, i32 0, i32 2
  %call159 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx158)
  %70 = load float, float* %call159, align 4
  %71 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %71, i32 2)
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call160)
  %72 = load float, float* %call161, align 4
  %mul162 = fmul float %70, %72
  %add163 = fadd float %add156, %mul162
  store float %add163, float* %ref.tmp143, align 4
  %m_el165 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx166 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el165, i32 0, i32 0
  %call167 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx166)
  %73 = load float, float* %call167, align 4
  %74 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call168 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %74, i32 0)
  %call169 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call168)
  %75 = load float, float* %call169, align 4
  %mul170 = fmul float %73, %75
  %m_el171 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el171, i32 0, i32 1
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx172)
  %76 = load float, float* %call173, align 4
  %77 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call174 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %77, i32 1)
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call174)
  %78 = load float, float* %call175, align 4
  %mul176 = fmul float %76, %78
  %add177 = fadd float %mul170, %mul176
  %m_el178 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el178, i32 0, i32 2
  %call180 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx179)
  %79 = load float, float* %call180, align 4
  %80 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call181 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %80, i32 2)
  %call182 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call181)
  %81 = load float, float* %call182, align 4
  %mul183 = fmul float %79, %81
  %add184 = fadd float %add177, %mul183
  store float %add184, float* %ref.tmp164, align 4
  %call185 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp164)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis4)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb(%"struct.gjkepa2_impl::MinkowskiDiff"* %this, i1 zeroext %enable) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %enable.addr = alloca i8, align 1
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %frombool = zext i1 %enable to i8
  store i8 %frombool, i8* %enable.addr, align 1
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = load i8, i8* %enable.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } { i32 ptrtoint (void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3 to i32), i32 0 }, { i32, i32 }* %Ls, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %Ls2 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } { i32 ptrtoint (void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3 to i32), i32 0 }, { i32, i32 }* %Ls2, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

declare void @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

declare void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %this, %"struct.gjkepa2_impl::GJK::sSV"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %this.addr, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %d)
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %w)
  ret %"struct.gjkepa2_impl::GJK::sSV"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK10InitializeEv(%"struct.gjkepa2_impl::GJK"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %0 = bitcast %class.btVector3* %m_ray to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nfree, align 4
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 2, i32* %m_status, align 4
  %m_current = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 0, i32* %m_current, align 4
  %m_distance = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(128) %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_(%"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %0) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %0, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %1 = bitcast [2 x %class.btConvexShape*]* %m_shapes to i8*
  %2 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4
  %m_shapes2 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %2, i32 0, i32 0
  %3 = bitcast [2 x %class.btConvexShape*]* %m_shapes2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 8, i1 false)
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %4 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4
  %m_toshape13 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %4, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_toshape1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_toshape13)
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %5 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4
  %m_toshape04 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %5, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_toshape0, %class.btTransform* nonnull align 4 dereferenceable(64) %m_toshape04)
  %6 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %6, i32 0, i32 3
  %7 = load { i32, i32 }, { i32, i32 }* %Ls, align 4
  %Ls6 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } %7, { i32, i32 }* %Ls6, align 4
  ret %"struct.gjkepa2_impl::MinkowskiDiff"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %simplex, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %simplex.addr = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %simplex, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 1
  %1 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %1, i32 0, i32 2
  %2 = load i32, i32* %rank, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 %2
  store float 0.000000e+00, float* %arrayidx, align 4
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_nfree, align 4
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_nfree, align 4
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 %dec
  %4 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4
  %5 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %5, i32 0, i32 0
  %6 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %rank3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %6, i32 0, i32 2
  %7 = load i32, i32* %rank3, align 4
  %arrayidx4 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %7
  store %"struct.gjkepa2_impl::GJK::sSV"* %4, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx4, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %9 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %9, i32 0, i32 0
  %10 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %rank6 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %10, i32 0, i32 2
  %11 = load i32, i32* %rank6, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %rank6, align 4
  %arrayidx7 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 %11
  %12 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx7, align 4
  call void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %12)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %simplex) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %simplex.addr = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %simplex, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 0
  %1 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %1, i32 0, i32 2
  %2 = load i32, i32* %rank, align 4
  %dec = add i32 %2, -1
  store i32 %dec, i32* %rank, align 4
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %dec
  %3 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %4 = load i32, i32* %m_nfree, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %m_nfree, align 4
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 %4
  store %"struct.gjkepa2_impl::GJK::sSV"* %3, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #2 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %d = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %t = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store float* %w, float** %w.addr, align 4
  store i32* %m, i32** %m.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %d)
  store float %call, float* %l, align 4
  %2 = load float, float* %l, align 4
  %cmp = fcmp ogt float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load float, float* %l, align 4
  %cmp1 = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %4 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  %fneg = fneg float %call2
  %5 = load float, float* %l, align 4
  %div = fdiv float %fneg, %5
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %t, align 4
  %6 = load float, float* %t, align 4
  %cmp3 = fcmp oge float %6, 1.000000e+00
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %cond.end
  %7 = load float*, float** %w.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %7, i32 0
  store float 0.000000e+00, float* %arrayidx, align 4
  %8 = load float*, float** %w.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 1
  store float 1.000000e+00, float* %arrayidx5, align 4
  %9 = load i32*, i32** %m.addr, align 4
  store i32 2, i32* %9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %10)
  store float %call6, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %cond.end
  %11 = load float, float* %t, align 4
  %cmp7 = fcmp ole float %11, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.else12

if.then8:                                         ; preds = %if.else
  %12 = load float*, float** %w.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %12, i32 0
  store float 1.000000e+00, float* %arrayidx9, align 4
  %13 = load float*, float** %w.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %13, i32 1
  store float 0.000000e+00, float* %arrayidx10, align 4
  %14 = load i32*, i32** %m.addr, align 4
  store i32 1, i32* %14, align 4
  %15 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %15)
  store float %call11, float* %retval, align 4
  br label %return

if.else12:                                        ; preds = %if.else
  %16 = load float, float* %t, align 4
  %17 = load float*, float** %w.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %17, i32 1
  store float %16, float* %arrayidx13, align 4
  %sub = fsub float 1.000000e+00, %16
  %18 = load float*, float** %w.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %18, i32 0
  store float %sub, float* %arrayidx14, align 4
  %19 = load i32*, i32** %m.addr, align 4
  store i32 3, i32* %19, align 4
  %20 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %d, float* nonnull align 4 dereferenceable(4) %t)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %call16 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call16, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store float -1.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.else12, %if.then8, %if.then4
  %21 = load float, float* %retval, align 4
  ret float %21
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #2 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %vt = alloca [3 x %class.btVector3*], align 4
  %dl = alloca [3 x %class.btVector3], align 16
  %n = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %mindist = alloca float, align 4
  %subw = alloca [2 x float], align 4
  %subm = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %subd = alloca float, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store float* %w, float** %w.addr, align 4
  store i32* %m, i32** %m.addr, align 4
  %arrayinit.begin = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %0, %class.btVector3** %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.begin, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %arrayinit.element, align 4
  %arrayinit.element1 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %2, %class.btVector3** %arrayinit.element1, align 4
  %arrayinit.begin2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.begin2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %arrayinit.element3 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin2, i32 1
  %5 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element3, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %arrayinit.element4 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element3, i32 1
  %7 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element4, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n)
  store float %call, float* %l, align 4
  %9 = load float, float* %l, align 4
  %cmp = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end56

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %mindist, align 4
  %10 = bitcast [2 x float]* %subw to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 8, i1 false)
  store i32 0, i32* %subm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4
  %cmp6 = icmp ult i32 %11, 3
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %12
  %13 = load %class.btVector3*, %class.btVector3** %arrayidx7, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %14
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %call9 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %cmp10 = fcmp ogt float %call9, 0.000000e+00
  br i1 %cmp10, label %if.then11, label %if.end32

if.then11:                                        ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx12, align 4
  store i32 %16, i32* %j, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %17
  %18 = load %class.btVector3*, %class.btVector3** %arrayidx13, align 4
  %19 = load i32, i32* %j, align 4
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %19
  %20 = load %class.btVector3*, %class.btVector3** %arrayidx14, align 4
  %arraydecay = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 0
  %call15 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %20, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %subm)
  store float %call15, float* %subd, align 4
  %21 = load float, float* %mindist, align 4
  %cmp16 = fcmp olt float %21, 0.000000e+00
  br i1 %cmp16, label %if.then18, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then11
  %22 = load float, float* %subd, align 4
  %23 = load float, float* %mindist, align 4
  %cmp17 = fcmp olt float %22, %23
  br i1 %cmp17, label %if.then18, label %if.end

if.then18:                                        ; preds = %lor.lhs.false, %if.then11
  %24 = load float, float* %subd, align 4
  store float %24, float* %mindist, align 4
  %25 = load i32, i32* %subm, align 4
  %and = and i32 %25, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then18
  %26 = load i32, i32* %i, align 4
  %shl = shl i32 1, %26
  br label %cond.end

cond.false:                                       ; preds = %if.then18
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shl, %cond.true ], [ 0, %cond.false ]
  %27 = load i32, i32* %subm, align 4
  %and19 = and i32 %27, 2
  %tobool20 = icmp ne i32 %and19, 0
  br i1 %tobool20, label %cond.true21, label %cond.false23

cond.true21:                                      ; preds = %cond.end
  %28 = load i32, i32* %j, align 4
  %shl22 = shl i32 1, %28
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true21
  %cond25 = phi i32 [ %shl22, %cond.true21 ], [ 0, %cond.false23 ]
  %add = add nsw i32 %cond, %cond25
  %29 = load i32*, i32** %m.addr, align 4
  store i32 %add, i32* %29, align 4
  %arrayidx26 = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 0
  %30 = load float, float* %arrayidx26, align 4
  %31 = load float*, float** %w.addr, align 4
  %32 = load i32, i32* %i, align 4
  %arrayidx27 = getelementptr inbounds float, float* %31, i32 %32
  store float %30, float* %arrayidx27, align 4
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 1
  %33 = load float, float* %arrayidx28, align 4
  %34 = load float*, float** %w.addr, align 4
  %35 = load i32, i32* %j, align 4
  %arrayidx29 = getelementptr inbounds float, float* %34, i32 %35
  store float %33, float* %arrayidx29, align 4
  %36 = load float*, float** %w.addr, align 4
  %37 = load i32, i32* %j, align 4
  %arrayidx30 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx30, align 4
  %arrayidx31 = getelementptr inbounds float, float* %36, i32 %38
  store float 0.000000e+00, float* %arrayidx31, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end24, %lor.lhs.false
  br label %if.end32

if.end32:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %39 = load i32, i32* %i, align 4
  %inc = add i32 %39, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %40 = load float, float* %mindist, align 4
  %cmp33 = fcmp olt float %40, 0.000000e+00
  br i1 %cmp33, label %if.then34, label %if.end55

if.then34:                                        ; preds = %for.end
  %41 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call35 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %41, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  store float %call35, float* %d, align 4
  %42 = load float, float* %l, align 4
  %call36 = call float @_Z6btSqrtf(float %42)
  store float %call36, float* %s, align 4
  %43 = load float, float* %d, align 4
  %44 = load float, float* %l, align 4
  %div = fdiv float %43, %44
  store float %div, float* %ref.tmp37, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %p, %class.btVector3* nonnull align 4 dereferenceable(16) %n, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %call38 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  store float %call38, float* %mindist, align 4
  %45 = load i32*, i32** %m.addr, align 4
  store i32 7, i32* %45, align 4
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  %46 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %46, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %call42 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp39)
  %47 = load float, float* %s, align 4
  %div43 = fdiv float %call42, %47
  %48 = load float*, float** %w.addr, align 4
  %arrayidx44 = getelementptr inbounds float, float* %48, i32 0
  store float %div43, float* %arrayidx44, align 4
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 2
  %49 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47)
  %call48 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp45)
  %50 = load float, float* %s, align 4
  %div49 = fdiv float %call48, %50
  %51 = load float*, float** %w.addr, align 4
  %arrayidx50 = getelementptr inbounds float, float* %51, i32 1
  store float %div49, float* %arrayidx50, align 4
  %52 = load float*, float** %w.addr, align 4
  %arrayidx51 = getelementptr inbounds float, float* %52, i32 0
  %53 = load float, float* %arrayidx51, align 4
  %54 = load float*, float** %w.addr, align 4
  %arrayidx52 = getelementptr inbounds float, float* %54, i32 1
  %55 = load float, float* %arrayidx52, align 4
  %add53 = fadd float %53, %55
  %sub = fsub float 1.000000e+00, %add53
  %56 = load float*, float** %w.addr, align 4
  %arrayidx54 = getelementptr inbounds float, float* %56, i32 2
  store float %sub, float* %arrayidx54, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then34, %for.end
  %57 = load float, float* %mindist, align 4
  store float %57, float* %retval, align 4
  br label %return

if.end56:                                         ; preds = %entry
  store float -1.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end56, %if.end55
  %58 = load float, float* %retval, align 4
  ret float %58
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #2 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %vt = alloca [4 x %class.btVector3*], align 16
  %dl = alloca [3 x %class.btVector3], align 16
  %vl = alloca float, align 4
  %ng = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %mindist = alloca float, align 4
  %subw = alloca [3 x float], align 4
  %subm = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %s = alloca float, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %subd = alloca float, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  store float* %w, float** %w.addr, align 4
  store i32* %m, i32** %m.addr, align 4
  %arrayinit.begin = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %0, %class.btVector3** %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.begin, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %arrayinit.element, align 4
  %arrayinit.element1 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %2, %class.btVector3** %arrayinit.element1, align 4
  %arrayinit.element2 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element1, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  store %class.btVector3* %3, %class.btVector3** %arrayinit.element2, align 4
  %arrayinit.begin3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.begin3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %arrayinit.element4 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin3, i32 1
  %6 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element4, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %arrayinit.element5 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element4, i32 1
  %8 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element5, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 2
  %call = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  store float %call, float* %vl, align 4
  %10 = load float, float* %vl, align 4
  %11 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %12 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %14 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %15 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call10 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %mul = fmul float %10, %call10
  %cmp = fcmp ole float %mul, 0.000000e+00
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %ng, align 1
  %16 = load i8, i8* %ng, align 1
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end66

land.lhs.true:                                    ; preds = %entry
  %17 = load float, float* %vl, align 4
  %call11 = call float @_Z6btFabsf(float %17)
  %cmp12 = fcmp ogt float %call11, 0.000000e+00
  br i1 %cmp12, label %if.then, label %if.end66

if.then:                                          ; preds = %land.lhs.true
  store float -1.000000e+00, float* %mindist, align 4
  %18 = bitcast [3 x float]* %subw to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %18, i8 0, i32 12, i1 false)
  store i32 0, i32* %subm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %19 = load i32, i32* %i, align 4
  %cmp13 = icmp ult i32 %19, 3
  br i1 %cmp13, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx14, align 4
  store i32 %21, i32* %j, align 4
  %22 = load float, float* %vl, align 4
  %23 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %24
  %25 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %25
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx17)
  %call18 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %mul19 = fmul float %22, %call18
  store float %mul19, float* %s, align 4
  %26 = load float, float* %s, align 4
  %cmp20 = fcmp ogt float %26, 0.000000e+00
  br i1 %cmp20, label %if.then21, label %if.end48

if.then21:                                        ; preds = %for.body
  %27 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 %27
  %28 = load %class.btVector3*, %class.btVector3** %arrayidx22, align 4
  %29 = load i32, i32* %j, align 4
  %arrayidx23 = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 %29
  %30 = load %class.btVector3*, %class.btVector3** %arrayidx23, align 4
  %31 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 0
  %call24 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %28, %class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %subm)
  store float %call24, float* %subd, align 4
  %32 = load float, float* %mindist, align 4
  %cmp25 = fcmp olt float %32, 0.000000e+00
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then21
  %33 = load float, float* %subd, align 4
  %34 = load float, float* %mindist, align 4
  %cmp26 = fcmp olt float %33, %34
  br i1 %cmp26, label %if.then27, label %if.end

if.then27:                                        ; preds = %lor.lhs.false, %if.then21
  %35 = load float, float* %subd, align 4
  store float %35, float* %mindist, align 4
  %36 = load i32, i32* %subm, align 4
  %and = and i32 %36, 1
  %tobool28 = icmp ne i32 %and, 0
  br i1 %tobool28, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then27
  %37 = load i32, i32* %i, align 4
  %shl = shl i32 1, %37
  br label %cond.end

cond.false:                                       ; preds = %if.then27
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shl, %cond.true ], [ 0, %cond.false ]
  %38 = load i32, i32* %subm, align 4
  %and29 = and i32 %38, 2
  %tobool30 = icmp ne i32 %and29, 0
  br i1 %tobool30, label %cond.true31, label %cond.false33

cond.true31:                                      ; preds = %cond.end
  %39 = load i32, i32* %j, align 4
  %shl32 = shl i32 1, %39
  br label %cond.end34

cond.false33:                                     ; preds = %cond.end
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false33, %cond.true31
  %cond35 = phi i32 [ %shl32, %cond.true31 ], [ 0, %cond.false33 ]
  %add = add nsw i32 %cond, %cond35
  %40 = load i32, i32* %subm, align 4
  %and36 = and i32 %40, 4
  %tobool37 = icmp ne i32 %and36, 0
  %41 = zext i1 %tobool37 to i64
  %cond38 = select i1 %tobool37, i32 8, i32 0
  %add39 = add nsw i32 %add, %cond38
  %42 = load i32*, i32** %m.addr, align 4
  store i32 %add39, i32* %42, align 4
  %arrayidx40 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 0
  %43 = load float, float* %arrayidx40, align 4
  %44 = load float*, float** %w.addr, align 4
  %45 = load i32, i32* %i, align 4
  %arrayidx41 = getelementptr inbounds float, float* %44, i32 %45
  store float %43, float* %arrayidx41, align 4
  %arrayidx42 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 1
  %46 = load float, float* %arrayidx42, align 4
  %47 = load float*, float** %w.addr, align 4
  %48 = load i32, i32* %j, align 4
  %arrayidx43 = getelementptr inbounds float, float* %47, i32 %48
  store float %46, float* %arrayidx43, align 4
  %49 = load float*, float** %w.addr, align 4
  %50 = load i32, i32* %j, align 4
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx44, align 4
  %arrayidx45 = getelementptr inbounds float, float* %49, i32 %51
  store float 0.000000e+00, float* %arrayidx45, align 4
  %arrayidx46 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 2
  %52 = load float, float* %arrayidx46, align 4
  %53 = load float*, float** %w.addr, align 4
  %arrayidx47 = getelementptr inbounds float, float* %53, i32 3
  store float %52, float* %arrayidx47, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end34, %lor.lhs.false
  br label %if.end48

if.end48:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end48
  %54 = load i32, i32* %i, align 4
  %inc = add i32 %54, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %55 = load float, float* %mindist, align 4
  %cmp49 = fcmp olt float %55, 0.000000e+00
  br i1 %cmp49, label %if.then50, label %if.end65

if.then50:                                        ; preds = %for.end
  store float 0.000000e+00, float* %mindist, align 4
  %56 = load i32*, i32** %m.addr, align 4
  store i32 15, i32* %56, align 4
  %57 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %58 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %59 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call51 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %57, %class.btVector3* nonnull align 4 dereferenceable(16) %58, %class.btVector3* nonnull align 4 dereferenceable(16) %59)
  %60 = load float, float* %vl, align 4
  %div = fdiv float %call51, %60
  %61 = load float*, float** %w.addr, align 4
  %arrayidx52 = getelementptr inbounds float, float* %61, i32 0
  store float %div, float* %arrayidx52, align 4
  %62 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %63 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %64 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call53 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %62, %class.btVector3* nonnull align 4 dereferenceable(16) %63, %class.btVector3* nonnull align 4 dereferenceable(16) %64)
  %65 = load float, float* %vl, align 4
  %div54 = fdiv float %call53, %65
  %66 = load float*, float** %w.addr, align 4
  %arrayidx55 = getelementptr inbounds float, float* %66, i32 1
  store float %div54, float* %arrayidx55, align 4
  %67 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %68 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %69 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call56 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %67, %class.btVector3* nonnull align 4 dereferenceable(16) %68, %class.btVector3* nonnull align 4 dereferenceable(16) %69)
  %70 = load float, float* %vl, align 4
  %div57 = fdiv float %call56, %70
  %71 = load float*, float** %w.addr, align 4
  %arrayidx58 = getelementptr inbounds float, float* %71, i32 2
  store float %div57, float* %arrayidx58, align 4
  %72 = load float*, float** %w.addr, align 4
  %arrayidx59 = getelementptr inbounds float, float* %72, i32 0
  %73 = load float, float* %arrayidx59, align 4
  %74 = load float*, float** %w.addr, align 4
  %arrayidx60 = getelementptr inbounds float, float* %74, i32 1
  %75 = load float, float* %arrayidx60, align 4
  %add61 = fadd float %73, %75
  %76 = load float*, float** %w.addr, align 4
  %arrayidx62 = getelementptr inbounds float, float* %76, i32 2
  %77 = load float, float* %arrayidx62, align 4
  %add63 = fadd float %add61, %77
  %sub = fsub float 1.000000e+00, %add63
  %78 = load float*, float** %w.addr, align 4
  %arrayidx64 = getelementptr inbounds float, float* %78, i32 3
  store float %sub, float* %arrayidx64, align 4
  br label %if.end65

if.end65:                                         ; preds = %if.then50, %for.end
  %79 = load float, float* %mindist, align 4
  store float %79, float* %retval, align 4
  br label %return

if.end66:                                         ; preds = %land.lhs.true, %entry
  store float -1.000000e+00, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end66, %if.end65
  %80 = load float, float* %retval, align 4
  ret float %80
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %sv) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %sv.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %sv, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %1)
  store float %call, float* %ref.tmp2, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %2 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4
  %d3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %2, i32 0, i32 0
  %3 = bitcast %class.btVector3* %d3 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4
  %d5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %5, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d5)
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %6, i32 0, i32 1
  %7 = bitcast %class.btVector3* %w to i8*
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 0
  %0 = load %class.btConvexShape*, %class.btConvexShape** %arrayidx, align 4
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  %1 = load { i32, i32 }, { i32, i32 }* %Ls, align 4
  %memptr.adj = extractvalue { i32, i32 } %1, 1
  %memptr.adj.shifted = ashr i32 %memptr.adj, 1
  %2 = bitcast %class.btConvexShape* %0 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 %memptr.adj.shifted
  %this.adjusted = bitcast i8* %3 to %class.btConvexShape*
  %memptr.ptr = extractvalue { i32, i32 } %1, 0
  %4 = and i32 %memptr.adj, 1
  %memptr.isvirtual = icmp ne i32 %4, 0
  br i1 %memptr.isvirtual, label %memptr.virtual, label %memptr.nonvirtual

memptr.virtual:                                   ; preds = %entry
  %5 = bitcast %class.btConvexShape* %this.adjusted to i8**
  %vtable = load i8*, i8** %5, align 4
  %6 = getelementptr i8, i8* %vtable, i32 %memptr.ptr, !nosanitize !2
  %7 = bitcast i8* %6 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, !nosanitize !2
  %memptr.virtualfn = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %7, align 4, !nosanitize !2
  br label %memptr.end

memptr.nonvirtual:                                ; preds = %entry
  %memptr.nonvirtualfn = inttoptr i32 %memptr.ptr to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*
  br label %memptr.end

memptr.end:                                       ; preds = %memptr.nonvirtual, %memptr.virtual
  %8 = phi void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* [ %memptr.virtualfn, %memptr.virtual ], [ %memptr.nonvirtualfn, %memptr.nonvirtual ]
  %9 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void %8(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %this.adjusted, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 1
  %0 = load %class.btConvexShape*, %class.btConvexShape** %arrayidx, align 4
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  %1 = load { i32, i32 }, { i32, i32 }* %Ls, align 4
  %memptr.adj = extractvalue { i32, i32 } %1, 1
  %memptr.adj.shifted = ashr i32 %memptr.adj, 1
  %2 = bitcast %class.btConvexShape* %0 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 %memptr.adj.shifted
  %this.adjusted = bitcast i8* %3 to %class.btConvexShape*
  %memptr.ptr = extractvalue { i32, i32 } %1, 0
  %4 = and i32 %memptr.adj, 1
  %memptr.isvirtual = icmp ne i32 %4, 0
  br i1 %memptr.isvirtual, label %memptr.virtual, label %memptr.nonvirtual

memptr.virtual:                                   ; preds = %entry
  %5 = bitcast %class.btConvexShape* %this.adjusted to i8**
  %vtable = load i8*, i8** %5, align 4
  %6 = getelementptr i8, i8* %vtable, i32 %memptr.ptr, !nosanitize !2
  %7 = bitcast i8* %6 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, !nosanitize !2
  %memptr.virtualfn = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %7, align 4, !nosanitize !2
  br label %memptr.end

memptr.nonvirtual:                                ; preds = %entry
  %memptr.nonvirtualfn = inttoptr i32 %memptr.ptr to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*
  br label %memptr.end

memptr.end:                                       ; preds = %memptr.nonvirtual, %memptr.virtual
  %8 = phi void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* [ %memptr.virtualfn, %memptr.virtual ], [ %memptr.nonvirtualfn, %memptr.nonvirtual ]
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %9 = load %class.btVector3*, %class.btVector3** %d.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_toshape1, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  call void %8(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %this.adjusted, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %m_toshape0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) #1 comdat {
entry:
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %mul = fmul float %1, %3
  %4 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %mul3 = fmul float %mul, %5
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %6)
  %7 = load float, float* %call4, align 4
  %8 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call5, align 4
  %mul6 = fmul float %7, %9
  %10 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %10)
  %11 = load float, float* %call7, align 4
  %mul8 = fmul float %mul6, %11
  %add = fadd float %mul3, %mul8
  %12 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %12)
  %13 = load float, float* %call9, align 4
  %14 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %14)
  %15 = load float, float* %call10, align 4
  %mul11 = fmul float %13, %15
  %16 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %16)
  %17 = load float, float* %call12, align 4
  %mul13 = fmul float %mul11, %17
  %sub = fsub float %add, %mul13
  %18 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call14, align 4
  %20 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %20)
  %21 = load float, float* %call15, align 4
  %mul16 = fmul float %19, %21
  %22 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %22)
  %23 = load float, float* %call17, align 4
  %mul18 = fmul float %mul16, %23
  %sub19 = fsub float %sub, %mul18
  %24 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %24)
  %25 = load float, float* %call20, align 4
  %26 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %26)
  %27 = load float, float* %call21, align 4
  %mul22 = fmul float %25, %27
  %28 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call23, align 4
  %mul24 = fmul float %mul22, %29
  %add25 = fadd float %sub19, %mul24
  %30 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call26, align 4
  %32 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %32)
  %33 = load float, float* %call27, align 4
  %mul28 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %mul28, %35
  %sub31 = fsub float %add25, %mul30
  ret float %sub31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA5sFaceC2Ev(%"struct.gjkepa2_impl::EPA::sFace"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %this, %"struct.gjkepa2_impl::EPA::sFace"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %this.addr, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  ret %"struct.gjkepa2_impl::EPA::sFace"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %this, %"struct.gjkepa2_impl::EPA::sList"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %this.addr, align 4
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %this1, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %this1, i32 0, i32 1
  store i32 0, i32* %count, align 4
  ret %"struct.gjkepa2_impl::EPA::sList"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA10InitializeEv(%"struct.gjkepa2_impl::EPA"* %this) #2 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %i = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 9, i32* %m_status, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %m_normal to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %m_depth, align 4
  %m_nextsv = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nextsv, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 256
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %m_fc_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 5
  %3 = load i32, i32* %i, align 4
  %sub = sub i32 256, %3
  %sub5 = sub i32 %sub, 1
  %arrayidx = getelementptr inbounds [256 x %"struct.gjkepa2_impl::EPA::sFace"], [256 x %"struct.gjkepa2_impl::EPA::sFace"]* %m_fc_store, i32 0, i32 %sub5
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %list, %"struct.gjkepa2_impl::EPA::sFace"* %face) #1 comdat {
entry:
  %list.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %list, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4
  %1 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %1, i32 0, i32 0
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l1 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %3, i32 0, i32 4
  %arrayidx2 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l1, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %2, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx2, align 4
  %4 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %4, i32 0, i32 0
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %7 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %7, i32 0, i32 0
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root4, align 4
  %l5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 4
  %arrayidx6 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l5, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %6, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %10 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %10, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %root7, align 4
  %11 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %11, i32 0, i32 1
  %12 = load i32, i32* %count, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %count, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %i = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %d = alloca %class.btVector3, align 4
  %i21 = alloca i32, align 4
  %axis25 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp78 = alloca %class.btVector3, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp103 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 2
  %1 = load i32, i32* %rank, align 4
  switch i32 %1, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb14
    i32 3, label %sw.bb50
    i32 4, label %sw.bb84
  ]

sw.bb:                                            ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb
  %2 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %2, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis)
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call4, i32 %3
  store float 1.000000e+00, float* %arrayidx, align 4
  %m_simplex5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %4 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex5, align 4
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %axis)
  %call6 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %m_simplex7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %5 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex7, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %5)
  %m_simplex8 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %6 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex8, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %axis)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call10 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.end
  %m_simplex13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %7 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex13, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %7)
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %m_simplex15 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %9 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex15, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %9, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 1
  %10 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx16, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %10, i32 0, i32 1
  %m_simplex17 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %11 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex17, align 4
  %c18 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %11, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c18, i32 0, i32 0
  %12 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx19, align 4
  %w20 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %12, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w20)
  store i32 0, i32* %i21, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc47, %sw.bb14
  %13 = load i32, i32* %i21, align 4
  %cmp23 = icmp ult i32 %13, 3
  br i1 %cmp23, label %for.body24, label %for.end49

for.body24:                                       ; preds = %for.cond22
  store float 0.000000e+00, float* %ref.tmp26, align 4
  store float 0.000000e+00, float* %ref.tmp27, align 4
  store float 0.000000e+00, float* %ref.tmp28, align 4
  %call29 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axis25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis25)
  %14 = load i32, i32* %i21, align 4
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 %14
  store float 1.000000e+00, float* %arrayidx31, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %p, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %class.btVector3* nonnull align 4 dereferenceable(16) %axis25)
  %call32 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  %cmp33 = fcmp ogt float %call32, 0.000000e+00
  br i1 %cmp33, label %if.then34, label %if.end46

if.then34:                                        ; preds = %for.body24
  %m_simplex35 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %15 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex35, align 4
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %call36 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.then34
  store i1 true, i1* %retval, align 1
  br label %return

if.end38:                                         ; preds = %if.then34
  %m_simplex39 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %16 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex39, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %16)
  %m_simplex40 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %17 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex40, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %call42 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.end38
  store i1 true, i1* %retval, align 1
  br label %return

if.end44:                                         ; preds = %if.end38
  %m_simplex45 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %18 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex45, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %18)
  br label %if.end46

if.end46:                                         ; preds = %if.end44, %for.body24
  br label %for.inc47

for.inc47:                                        ; preds = %if.end46
  %19 = load i32, i32* %i21, align 4
  %inc48 = add i32 %19, 1
  store i32 %inc48, i32* %i21, align 4
  br label %for.cond22

for.end49:                                        ; preds = %for.cond22
  br label %sw.epilog

sw.bb50:                                          ; preds = %entry
  %m_simplex52 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %20 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex52, align 4
  %c53 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %20, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c53, i32 0, i32 1
  %21 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx54, align 4
  %w55 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %21, i32 0, i32 1
  %m_simplex56 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %22 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex56, align 4
  %c57 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %22, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c57, i32 0, i32 0
  %23 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx58, align 4
  %w59 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %23, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %w55, %class.btVector3* nonnull align 4 dereferenceable(16) %w59)
  %m_simplex61 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %24 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex61, align 4
  %c62 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %24, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c62, i32 0, i32 2
  %25 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx63, align 4
  %w64 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %25, i32 0, i32 1
  %m_simplex65 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %26 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex65, align 4
  %c66 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %26, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c66, i32 0, i32 0
  %27 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx67, align 4
  %w68 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %27, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %w64, %class.btVector3* nonnull align 4 dereferenceable(16) %w68)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp60)
  %call69 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n)
  %cmp70 = fcmp ogt float %call69, 0.000000e+00
  br i1 %cmp70, label %if.then71, label %if.end83

if.then71:                                        ; preds = %sw.bb50
  %m_simplex72 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %28 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex72, align 4
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %28, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %call73 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call73, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.then71
  store i1 true, i1* %retval, align 1
  br label %return

if.end75:                                         ; preds = %if.then71
  %m_simplex76 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %29 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex76, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %29)
  %m_simplex77 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %30 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex77, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp78, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp78)
  %call79 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %if.end75
  store i1 true, i1* %retval, align 1
  br label %return

if.end81:                                         ; preds = %if.end75
  %m_simplex82 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %31 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex82, align 4
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %31)
  br label %if.end83

if.end83:                                         ; preds = %if.end81, %sw.bb50
  br label %sw.epilog

sw.bb84:                                          ; preds = %entry
  %m_simplex86 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %32 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex86, align 4
  %c87 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %32, i32 0, i32 0
  %arrayidx88 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c87, i32 0, i32 0
  %33 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx88, align 4
  %w89 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %33, i32 0, i32 1
  %m_simplex90 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %34 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex90, align 4
  %c91 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %34, i32 0, i32 0
  %arrayidx92 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c91, i32 0, i32 3
  %35 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx92, align 4
  %w93 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %35, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp85, %class.btVector3* nonnull align 4 dereferenceable(16) %w89, %class.btVector3* nonnull align 4 dereferenceable(16) %w93)
  %m_simplex95 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %36 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex95, align 4
  %c96 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %36, i32 0, i32 0
  %arrayidx97 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c96, i32 0, i32 1
  %37 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx97, align 4
  %w98 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %37, i32 0, i32 1
  %m_simplex99 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %38 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex99, align 4
  %c100 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %38, i32 0, i32 0
  %arrayidx101 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c100, i32 0, i32 3
  %39 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx101, align 4
  %w102 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %39, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %w98, %class.btVector3* nonnull align 4 dereferenceable(16) %w102)
  %m_simplex104 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %40 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex104, align 4
  %c105 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %40, i32 0, i32 0
  %arrayidx106 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c105, i32 0, i32 2
  %41 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx106, align 4
  %w107 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %41, i32 0, i32 1
  %m_simplex108 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %42 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex108, align 4
  %c109 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %42, i32 0, i32 0
  %arrayidx110 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c109, i32 0, i32 3
  %43 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx110, align 4
  %w111 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %43, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %w107, %class.btVector3* nonnull align 4 dereferenceable(16) %w111)
  %call112 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp103)
  %call113 = call float @_Z6btFabsf(float %call112)
  %cmp114 = fcmp ogt float %call113, 0.000000e+00
  br i1 %cmp114, label %if.then115, label %if.end116

if.then115:                                       ; preds = %sw.bb84
  store i1 true, i1* %retval, align 1
  br label %return

if.end116:                                        ; preds = %sw.bb84
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %if.end116, %if.end83, %for.end49, %for.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %sw.epilog, %if.then115, %if.then80, %if.then74, %if.then43, %if.then37, %if.then11, %if.then
  %44 = load i1, i1* %retval, align 1
  ret i1 %44
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %list, %"struct.gjkepa2_impl::EPA::sFace"* %face) #1 comdat {
entry:
  %list.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %list, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 1
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l1 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %2, i32 0, i32 4
  %arrayidx2 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l1, i32 0, i32 0
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx2, align 4
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l3, i32 0, i32 1
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx4, align 4
  %l5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 4
  %arrayidx6 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l5, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %3, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %6, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l7, i32 0, i32 0
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx8, align 4
  %tobool9 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %7, null
  br i1 %tobool9, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l11 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l11, i32 0, i32 1
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx12, align 4
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 4
  %arrayidx14 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l13, i32 0, i32 0
  %11 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx14, align 4
  %l15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %11, i32 0, i32 4
  %arrayidx16 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l15, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx16, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then10, %if.end
  %12 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %13 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %13, i32 0, i32 0
  %14 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  %cmp = icmp eq %"struct.gjkepa2_impl::EPA::sFace"* %12, %14
  br i1 %cmp, label %if.then18, label %if.end22

if.then18:                                        ; preds = %if.end17
  %15 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %l19 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %15, i32 0, i32 4
  %arrayidx20 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l19, i32 0, i32 1
  %16 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx20, align 4
  %17 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %root21 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %17, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %16, %"struct.gjkepa2_impl::EPA::sFace"** %root21, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then18, %if.end17
  %18 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %18, i32 0, i32 1
  %19 = load i32, i32* %count, align 4
  %dec = add i32 %19, -1
  store i32 %dec, i32* %count, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_(%"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %a, %"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"**, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"**, align 4
  %tmp = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"** %a, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"** %b, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4
  %1 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %0, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %1, %"struct.gjkepa2_impl::GJK::sSV"** %tmp, align 4
  %2 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4
  %3 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %2, align 4
  %4 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %3, %"struct.gjkepa2_impl::GJK::sSV"** %4, align 4
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %tmp, align 4
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %5, %"struct.gjkepa2_impl::GJK::sSV"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %tmp = alloca float, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  store float %1, float* %tmp, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %4 = load float*, float** %a.addr, align 4
  store float %3, float* %4, align 4
  %5 = load float, float* %tmp, align 4
  %6 = load float*, float** %b.addr, align 4
  store float %5, float* %6, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"* %c, i1 zeroext %forced) #2 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %c.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %forced.addr = alloca i8, align 1
  %face = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %v = alloca i8, align 1
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %c, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4
  %frombool = zext i1 %forced to i8
  store i8 %frombool, i8* %forced.addr, align 1
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock, i32 0, i32 0
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %0, null
  br i1 %tobool, label %if.then, label %if.end43

if.then:                                          ; preds = %entry
  %m_stock2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock2, i32 0, i32 0
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %1, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %m_stock4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock4, %"struct.gjkepa2_impl::EPA::sFace"* %2)
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull, %"struct.gjkepa2_impl::EPA::sFace"* %3)
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %pass = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 6
  store i8 0, i8* %pass, align 1
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %6, i32 0, i32 2
  %arrayidx = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %5, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4
  %7 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %c6 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 2
  %arrayidx7 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c6, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %7, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx7, align 4
  %9 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %c8 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 2
  %arrayidx9 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c8, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %9, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx9, align 4
  %11 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %11, i32 0, i32 1
  %12 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %12, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w11)
  %13 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4
  %w13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %13, i32 0, i32 1
  %14 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w14 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %14, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %w13, %class.btVector3* nonnull align 4 dereferenceable(16) %w14)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %15 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %15, i32 0, i32 0
  %16 = bitcast %class.btVector3* %n to i8*
  %17 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %18 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %n15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %18, i32 0, i32 0
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %n15)
  store float %call, float* %l, align 4
  %19 = load float, float* %l, align 4
  %cmp = fcmp ogt float %19, 0x3F1A36E2E0000000
  %frombool16 = zext i1 %cmp to i8
  store i8 %frombool16, i8* %v, align 1
  %20 = load i8, i8* %v, align 1
  %tobool17 = trunc i8 %20 to i1
  br i1 %tobool17, label %if.then18, label %if.else38

if.then18:                                        ; preds = %if.then
  %21 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %22 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %23 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %24 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %24, i32 0, i32 1
  %call19 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %21, %"struct.gjkepa2_impl::GJK::sSV"* %22, %"struct.gjkepa2_impl::GJK::sSV"* %23, float* nonnull align 4 dereferenceable(4) %d)
  br i1 %call19, label %if.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then18
  %25 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %26 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %27 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4
  %28 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %d20 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %28, i32 0, i32 1
  %call21 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %25, %"struct.gjkepa2_impl::GJK::sSV"* %26, %"struct.gjkepa2_impl::GJK::sSV"* %27, float* nonnull align 4 dereferenceable(4) %d20)
  br i1 %call21, label %if.end, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false
  %29 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %30 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4
  %31 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %32 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %d23 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %32, i32 0, i32 1
  %call24 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %29, %"struct.gjkepa2_impl::GJK::sSV"* %30, %"struct.gjkepa2_impl::GJK::sSV"* %31, float* nonnull align 4 dereferenceable(4) %d23)
  br i1 %call24, label %if.end, label %if.then25

if.then25:                                        ; preds = %lor.lhs.false22
  %33 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w26 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %33, i32 0, i32 1
  %34 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %n27 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %34, i32 0, i32 0
  %call28 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w26, %class.btVector3* nonnull align 4 dereferenceable(16) %n27)
  %35 = load float, float* %l, align 4
  %div = fdiv float %call28, %35
  %36 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %d29 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %36, i32 0, i32 1
  store float %div, float* %d29, align 4
  br label %if.end

if.end:                                           ; preds = %if.then25, %lor.lhs.false22, %lor.lhs.false, %if.then18
  %37 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %n30 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %37, i32 0, i32 0
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %n30, float* nonnull align 4 dereferenceable(4) %l)
  %38 = load i8, i8* %forced.addr, align 1
  %tobool32 = trunc i8 %38 to i1
  br i1 %tobool32, label %if.then36, label %lor.lhs.false33

lor.lhs.false33:                                  ; preds = %if.end
  %39 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  %d34 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %39, i32 0, i32 1
  %40 = load float, float* %d34, align 4
  %cmp35 = fcmp oge float %40, 0xBEE4F8B580000000
  br i1 %cmp35, label %if.then36, label %if.else

if.then36:                                        ; preds = %lor.lhs.false33, %if.end
  %41 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %41, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  br label %return

if.else:                                          ; preds = %lor.lhs.false33
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 3, i32* %m_status, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else
  br label %if.end40

if.else38:                                        ; preds = %if.then
  %m_status39 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 2, i32* %m_status39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.else38, %if.end37
  %m_hull41 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %42 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull41, %"struct.gjkepa2_impl::EPA::sFace"* %42)
  %m_stock42 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %43 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock42, %"struct.gjkepa2_impl::EPA::sFace"* %43)
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  br label %return

if.end43:                                         ; preds = %entry
  %m_stock44 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root45 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock44, i32 0, i32 0
  %44 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root45, align 4
  %tobool46 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %44, null
  %45 = zext i1 %tobool46 to i64
  %cond = select i1 %tobool46, i32 6, i32 5
  %m_status47 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 %cond, i32* %m_status47, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end43, %if.end40, %if.then36
  %46 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  ret %"struct.gjkepa2_impl::EPA::sFace"* %46
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %minf = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %mind = alloca float, align 4
  %f = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %sqd = alloca float, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull, i32 0, i32 0
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %0, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %1, i32 0, i32 1
  %2 = load float, float* %d, align 4
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  %d2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %3, i32 0, i32 1
  %4 = load float, float* %d2, align 4
  %mul = fmul float %2, %4
  store float %mul, float* %mind, align 4
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 1
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %6, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %7, null
  br i1 %tobool, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  %d3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 1
  %9 = load float, float* %d3, align 4
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  %d4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 1
  %11 = load float, float* %d4, align 4
  %mul5 = fmul float %9, %11
  store float %mul5, float* %sqd, align 4
  %12 = load float, float* %sqd, align 4
  %13 = load float, float* %mind, align 4
  %cmp = fcmp olt float %12, %13
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %14 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %14, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  %15 = load float, float* %sqd, align 4
  store float %15, float* %mind, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  %l6 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %16, i32 0, i32 4
  %arrayidx7 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l6, i32 0, i32 1
  %17 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx7, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %17, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4
  ret %"struct.gjkepa2_impl::EPA::sFace"* %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %fa, i32 %ea, %"struct.gjkepa2_impl::EPA::sFace"* %fb, i32 %eb) #1 comdat {
entry:
  %fa.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ea.addr = alloca i32, align 4
  %fb.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %eb.addr = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %fa, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4
  store i32 %ea, i32* %ea.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %fb, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4
  store i32 %eb, i32* %eb.addr, align 4
  %0 = load i32, i32* %eb.addr, align 4
  %conv = trunc i32 %0 to i8
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4
  %e = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %1, i32 0, i32 5
  %2 = load i32, i32* %ea.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %e, i32 0, i32 %2
  store i8 %conv, i8* %arrayidx, align 1
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4
  %f = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 3
  %5 = load i32, i32* %ea.addr, align 4
  %arrayidx1 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f, i32 0, i32 %5
  store %"struct.gjkepa2_impl::EPA::sFace"* %3, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx1, align 4
  %6 = load i32, i32* %ea.addr, align 4
  %conv2 = trunc i32 %6 to i8
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4
  %e3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %7, i32 0, i32 5
  %8 = load i32, i32* %eb.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x i8], [3 x i8]* %e3, i32 0, i32 %8
  store i8 %conv2, i8* %arrayidx4, align 1
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4
  %f5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 3
  %11 = load i32, i32* %eb.addr, align 4
  %arrayidx6 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f5, i32 0, i32 %11
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sHorizon"* @_ZN12gjkepa2_impl3EPA8sHorizonC2Ev(%"struct.gjkepa2_impl::EPA::sHorizon"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sHorizon"*, align 4
  store %"struct.gjkepa2_impl::EPA::sHorizon"* %this, %"struct.gjkepa2_impl::EPA::sHorizon"** %this.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %this.addr, align 4
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4
  %nf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 2
  store i32 0, i32* %nf, align 4
  ret %"struct.gjkepa2_impl::EPA::sHorizon"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this, i32 %pass, %"struct.gjkepa2_impl::GJK::sSV"* %w, %"struct.gjkepa2_impl::EPA::sFace"* %f, i32 %e, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %horizon) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %pass.addr = alloca i32, align 4
  %w.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %f.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %e.addr = alloca i32, align 4
  %horizon.addr = alloca %"struct.gjkepa2_impl::EPA::sHorizon"*, align 4
  %e1 = alloca i32, align 4
  %nf = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %e2 = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store i32 %pass, i32* %pass.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %w, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %f, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  store i32 %e, i32* %e.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %pass2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 6
  %1 = load i8, i8* %pass2, align 1
  %conv = zext i8 %1 to i32
  %2 = load i32, i32* %pass.addr, align 4
  %cmp = icmp ne i32 %conv, %2
  br i1 %cmp, label %if.then, label %if.end36

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %e.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  store i32 %4, i32* %e1, align 4
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 0
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4
  %w3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %6, i32 0, i32 1
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %w3)
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %7, i32 0, i32 1
  %8 = load float, float* %d, align 4
  %sub = fsub float %call, %8
  %cmp4 = fcmp olt float %sub, 0xBEE4F8B580000000
  br i1 %cmp4, label %if.then5, label %if.else17

if.then5:                                         ; preds = %if.then
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %c = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %9, i32 0, i32 2
  %10 = load i32, i32* %e1, align 4
  %arrayidx6 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %10
  %11 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4
  %12 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %c7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %12, i32 0, i32 2
  %13 = load i32, i32* %e.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c7, i32 0, i32 %13
  %14 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx8, align 4
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4
  %call9 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %11, %"struct.gjkepa2_impl::GJK::sSV"* %14, %"struct.gjkepa2_impl::GJK::sSV"* %15, i1 zeroext false)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call9, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  %16 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %16, null
  br i1 %tobool, label %if.then10, label %if.end16

if.then10:                                        ; preds = %if.then5
  %17 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  %18 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %19 = load i32, i32* %e.addr, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %17, i32 0, %"struct.gjkepa2_impl::EPA::sFace"* %18, i32 %19)
  %20 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %20, i32 0, i32 0
  %21 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4
  %tobool11 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %21, null
  br i1 %tobool11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then10
  %22 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %cf13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %22, i32 0, i32 0
  %23 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf13, align 4
  %24 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %23, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %24, i32 2)
  br label %if.end

if.else:                                          ; preds = %if.then10
  %25 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  %26 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %26, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %25, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then12
  %27 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4
  %28 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %cf14 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %28, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %27, %"struct.gjkepa2_impl::EPA::sFace"** %cf14, align 4
  %29 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %nf15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %29, i32 0, i32 2
  %30 = load i32, i32* %nf15, align 4
  %inc = add i32 %30, 1
  store i32 %inc, i32* %nf15, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %if.then5
  br label %if.end35

if.else17:                                        ; preds = %if.then
  %31 = load i32, i32* %e.addr, align 4
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx18, align 4
  store i32 %32, i32* %e2, align 4
  %33 = load i32, i32* %pass.addr, align 4
  %conv19 = trunc i32 %33 to i8
  %34 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %pass20 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %34, i32 0, i32 6
  store i8 %conv19, i8* %pass20, align 1
  %35 = load i32, i32* %pass.addr, align 4
  %36 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4
  %37 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %f21 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %37, i32 0, i32 3
  %38 = load i32, i32* %e1, align 4
  %arrayidx22 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f21, i32 0, i32 %38
  %39 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx22, align 4
  %40 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %e23 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %40, i32 0, i32 5
  %41 = load i32, i32* %e1, align 4
  %arrayidx24 = getelementptr inbounds [3 x i8], [3 x i8]* %e23, i32 0, i32 %41
  %42 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %42 to i32
  %43 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %call26 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %35, %"struct.gjkepa2_impl::GJK::sSV"* %36, %"struct.gjkepa2_impl::EPA::sFace"* %39, i32 %conv25, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %43)
  br i1 %call26, label %land.lhs.true, label %if.end34

land.lhs.true:                                    ; preds = %if.else17
  %44 = load i32, i32* %pass.addr, align 4
  %45 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4
  %46 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %f27 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %46, i32 0, i32 3
  %47 = load i32, i32* %e2, align 4
  %arrayidx28 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f27, i32 0, i32 %47
  %48 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx28, align 4
  %49 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  %e29 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %49, i32 0, i32 5
  %50 = load i32, i32* %e2, align 4
  %arrayidx30 = getelementptr inbounds [3 x i8], [3 x i8]* %e29, i32 0, i32 %50
  %51 = load i8, i8* %arrayidx30, align 1
  %conv31 = zext i8 %51 to i32
  %52 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4
  %call32 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %44, %"struct.gjkepa2_impl::GJK::sSV"* %45, %"struct.gjkepa2_impl::EPA::sFace"* %48, i32 %conv31, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %52)
  br i1 %call32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %land.lhs.true
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %53 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull, %"struct.gjkepa2_impl::EPA::sFace"* %53)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %54 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %54)
  store i1 true, i1* %retval, align 1
  br label %return

if.end34:                                         ; preds = %land.lhs.true, %if.else17
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.end16
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end36, %if.then33, %if.end
  %55 = load i1, i1* %retval, align 1
  ret i1 %55
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"* %b, float* nonnull align 4 dereferenceable(4) %dist) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %dist.addr = alloca float*, align 4
  %ba = alloca %class.btVector3, align 4
  %n_ab = alloca %class.btVector3, align 4
  %a_dot_nab = alloca float, align 4
  %ba_l2 = alloca float, align 4
  %a_dot_ba = alloca float, align 4
  %b_dot_ba = alloca float, align 4
  %a_dot_b = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  store float* %dist, float** %dist.addr, align 4
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %0, i32 0, i32 1
  %1 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w2 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ba, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w2)
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %2, i32 0, i32 0
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n_ab, %class.btVector3* nonnull align 4 dereferenceable(16) %ba, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %3 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %3, i32 0, i32 1
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w3, %class.btVector3* nonnull align 4 dereferenceable(16) %n_ab)
  store float %call, float* %a_dot_nab, align 4
  %4 = load float, float* %a_dot_nab, align 4
  %cmp = fcmp olt float %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end30

if.then:                                          ; preds = %entry
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ba)
  store float %call4, float* %ba_l2, align 4
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %5, i32 0, i32 1
  %call6 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w5, %class.btVector3* nonnull align 4 dereferenceable(16) %ba)
  store float %call6, float* %a_dot_ba, align 4
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %6, i32 0, i32 1
  %call8 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w7, %class.btVector3* nonnull align 4 dereferenceable(16) %ba)
  store float %call8, float* %b_dot_ba, align 4
  %7 = load float, float* %a_dot_ba, align 4
  %cmp9 = fcmp ogt float %7, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then
  %8 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %8, i32 0, i32 1
  %call12 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %w11)
  %9 = load float*, float** %dist.addr, align 4
  store float %call12, float* %9, align 4
  br label %if.end29

if.else:                                          ; preds = %if.then
  %10 = load float, float* %b_dot_ba, align 4
  %cmp13 = fcmp olt float %10, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.else17

if.then14:                                        ; preds = %if.else
  %11 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w15 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %11, i32 0, i32 1
  %call16 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %w15)
  %12 = load float*, float** %dist.addr, align 4
  store float %call16, float* %12, align 4
  br label %if.end

if.else17:                                        ; preds = %if.else
  %13 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w18 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %13, i32 0, i32 1
  %14 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %14, i32 0, i32 1
  %call20 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w18, %class.btVector3* nonnull align 4 dereferenceable(16) %w19)
  store float %call20, float* %a_dot_b, align 4
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4
  %w21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 1
  %call22 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %w21)
  %16 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4
  %w23 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %16, i32 0, i32 1
  %call24 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %w23)
  %mul = fmul float %call22, %call24
  %17 = load float, float* %a_dot_b, align 4
  %18 = load float, float* %a_dot_b, align 4
  %mul25 = fmul float %17, %18
  %sub = fsub float %mul, %mul25
  %19 = load float, float* %ba_l2, align 4
  %div = fdiv float %sub, %19
  store float %div, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp26, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %20 = load float, float* %call27, align 4
  %call28 = call float @_Z6btSqrtf(float %20)
  %21 = load float*, float** %dist.addr, align 4
  store float %call28, float* %21, align 4
  br label %if.end

if.end:                                           ; preds = %if.else17, %if.then14
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then10
  store i1 true, i1* %retval, align 1
  br label %return

if.end30:                                         ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end30, %if.end29
  %22 = load i1, i1* %retval, align 1
  ret i1 %22
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %class.btConvexInternalShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #8
  ret %class.btConvexInternalShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #7

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGjkEpa2.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{}
