; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btNNCGConstraintSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btNNCGConstraintSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btNNCGConstraintSolver = type <{ %class.btSequentialImpulseConstraintSolver, float, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22, i8, [3 x i8] }>
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.18, i32, i32, %class.btAlignedObjectArray.14, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.3, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.12, i32, i32, i32, i32 }
%union.anon.12 = type { i8* }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%class.btPersistentManifold = type opaque
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.6, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.6 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btIDebugDraw = type opaque
%class.btDispatcher = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZN22btNNCGConstraintSolverD2Ev = comdat any

$_ZN22btNNCGConstraintSolverD0Ev = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK22btNNCGConstraintSolver13getSolverTypeEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN22btNNCGConstraintSolverdlEPv = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV22btNNCGConstraintSolver = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btNNCGConstraintSolver to i8*), i8* bitcast (%class.btNNCGConstraintSolver* (%class.btNNCGConstraintSolver*)* @_ZN22btNNCGConstraintSolverD2Ev to i8*), i8* bitcast (void (%class.btNNCGConstraintSolver*)* @_ZN22btNNCGConstraintSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btNNCGConstraintSolver*)* @_ZNK22btNNCGConstraintSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btNNCGConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN22btNNCGConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btNNCGConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN22btNNCGConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btNNCGConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN22btNNCGConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btNNCGConstraintSolver = hidden constant [25 x i8] c"22btNNCGConstraintSolver\00", align 1
@_ZTI35btSequentialImpulseConstraintSolver = external constant i8*
@_ZTI22btNNCGConstraintSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btNNCGConstraintSolver, i32 0, i32 0), i8* bitcast (i8** @_ZTI35btSequentialImpulseConstraintSolver to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btNNCGConstraintSolver.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN22btNNCGConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btNNCGConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %val = alloca float, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodies.addr, align 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %4 = load i32, i32* %numManifolds.addr, align 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %6 = load i32, i32* %numConstraints.addr, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %7, %class.btIDebugDraw* %8)
  store float %call, float* %val, align 4
  %m_pNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 2
  %9 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %9, i32 0, i32 3
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pNC, i32 %call2)
  %m_pC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 3
  %10 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %10, i32 0, i32 2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pC, i32 %call3)
  %m_pCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 4
  %11 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %11, i32 0, i32 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pCF, i32 %call4)
  %m_pCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 5
  %12 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %12, i32 0, i32 5
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pCRF, i32 %call5)
  %m_deltafNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 6
  %13 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool6 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %13, i32 0, i32 3
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool6)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafNC, i32 %call7)
  %m_deltafC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 7
  %14 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool8 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %14, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool8)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafC, i32 %call9)
  %m_deltafCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 8
  %15 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %15, i32 0, i32 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool10)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafCF, i32 %call11)
  %m_deltafCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 9
  %16 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool12 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %16, i32 0, i32 5
  %call13 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool12)
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafCRF, i32 %call13)
  %17 = load float, float* %val, align 4
  ret float %17
}

declare float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden float @_ZN22btNNCGConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btNNCGConstraintSolver* %this, i32 %iteration, %class.btCollisionObject** %0, i32 %1, %class.btPersistentManifold** %2, i32 %3, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %4) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  %iteration.addr = alloca i32, align 4
  %.addr = alloca %class.btCollisionObject**, align 4
  %.addr1 = alloca i32, align 4
  %.addr2 = alloca %class.btPersistentManifold**, align 4
  %.addr3 = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr4 = alloca %class.btIDebugDraw*, align 4
  %numNonContactPool = alloca i32, align 4
  %numConstraintPool = alloca i32, align 4
  %numFrictionPool = alloca i32, align 4
  %j = alloca i32, align 4
  %tmp = alloca i32, align 4
  %swapi = alloca i32, align 4
  %j18 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %swapi24 = alloca i32, align 4
  %j36 = alloca i32, align 4
  %tmp40 = alloca i32, align 4
  %swapi42 = alloca i32, align 4
  %deltaflengthsqr = alloca float, align 4
  %j59 = alloca i32, align 4
  %constraint = alloca %struct.btSolverConstraint*, align 4
  %deltaf = alloca float, align 4
  %j81 = alloca i32, align 4
  %constraint87 = alloca %struct.btSolverConstraint*, align 4
  %deltaf95 = alloca float, align 4
  %j116 = alloca i32, align 4
  %beta = alloca float, align 4
  %j133 = alloca i32, align 4
  %j145 = alloca i32, align 4
  %constraint151 = alloca %struct.btSolverConstraint*, align 4
  %additionaldeltaimpulse = alloca float, align 4
  %body1 = alloca %struct.btSolverBody*, align 4
  %body2 = alloca %struct.btSolverBody*, align 4
  %c = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp179 = alloca %class.btVector3, align 4
  %j196 = alloca i32, align 4
  %bodyAid = alloca i32, align 4
  %bodyBid = alloca i32, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %numPoolConstraints = alloca i32, align 4
  %multiplier = alloca i32, align 4
  %c229 = alloca i32, align 4
  %totalImpulse = alloca float, align 4
  %solveManifold = alloca %struct.btSolverConstraint*, align 4
  %deltaf237 = alloca float, align 4
  %applyFriction = alloca i8, align 1
  %solveManifold251 = alloca %struct.btSolverConstraint*, align 4
  %deltaf262 = alloca float, align 4
  %solveManifold283 = alloca %struct.btSolverConstraint*, align 4
  %deltaf299 = alloca float, align 4
  %numPoolConstraints325 = alloca i32, align 4
  %j328 = alloca i32, align 4
  %solveManifold332 = alloca %struct.btSolverConstraint*, align 4
  %deltaf337 = alloca float, align 4
  %numFrictionPoolConstraints = alloca i32, align 4
  %solveManifold357 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse362 = alloca float, align 4
  %deltaf375 = alloca float, align 4
  %numRollingFrictionPoolConstraints = alloca i32, align 4
  %rollingFrictionConstraint = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse400 = alloca float, align 4
  %rollingFrictionMagnitude = alloca float, align 4
  %deltaf417 = alloca float, align 4
  %j441 = alloca i32, align 4
  %bodyAid448 = alloca i32, align 4
  %bodyBid453 = alloca i32, align 4
  %bodyA458 = alloca %struct.btSolverBody*, align 4
  %bodyB461 = alloca %struct.btSolverBody*, align 4
  %numPoolConstraints472 = alloca i32, align 4
  %j475 = alloca i32, align 4
  %solveManifold479 = alloca %struct.btSolverConstraint*, align 4
  %deltaf484 = alloca float, align 4
  %numFrictionPoolConstraints499 = alloca i32, align 4
  %j502 = alloca i32, align 4
  %solveManifold506 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse511 = alloca float, align 4
  %deltaf525 = alloca float, align 4
  %numRollingFrictionPoolConstraints544 = alloca i32, align 4
  %j547 = alloca i32, align 4
  %rollingFrictionConstraint551 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse554 = alloca float, align 4
  %rollingFrictionMagnitude561 = alloca float, align 4
  %deltaf572 = alloca float, align 4
  %j598 = alloca i32, align 4
  %j611 = alloca i32, align 4
  %j623 = alloca i32, align 4
  %j642 = alloca i32, align 4
  %beta656 = alloca float, align 4
  %j667 = alloca i32, align 4
  %j678 = alloca i32, align 4
  %j689 = alloca i32, align 4
  %j704 = alloca i32, align 4
  %j717 = alloca i32, align 4
  %constraint723 = alloca %struct.btSolverConstraint*, align 4
  %additionaldeltaimpulse731 = alloca float, align 4
  %body1746 = alloca %struct.btSolverBody*, align 4
  %body2750 = alloca %struct.btSolverBody*, align 4
  %c754 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp755 = alloca %class.btVector3, align 4
  %ref.tmp759 = alloca %class.btVector3, align 4
  %j767 = alloca i32, align 4
  %constraint773 = alloca %struct.btSolverConstraint*, align 4
  %additionaldeltaimpulse781 = alloca float, align 4
  %body1796 = alloca %struct.btSolverBody*, align 4
  %body2800 = alloca %struct.btSolverBody*, align 4
  %c804 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp805 = alloca %class.btVector3, align 4
  %ref.tmp809 = alloca %class.btVector3, align 4
  %j817 = alloca i32, align 4
  %constraint823 = alloca %struct.btSolverConstraint*, align 4
  %additionaldeltaimpulse831 = alloca float, align 4
  %body1846 = alloca %struct.btSolverBody*, align 4
  %body2850 = alloca %struct.btSolverBody*, align 4
  %c854 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp855 = alloca %class.btVector3, align 4
  %ref.tmp859 = alloca %class.btVector3, align 4
  %j875 = alloca i32, align 4
  %constraint881 = alloca %struct.btSolverConstraint*, align 4
  %additionaldeltaimpulse887 = alloca float, align 4
  %body1902 = alloca %struct.btSolverBody*, align 4
  %body2906 = alloca %struct.btSolverBody*, align 4
  %c910 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp911 = alloca %class.btVector3, align 4
  %ref.tmp915 = alloca %class.btVector3, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  store i32 %iteration, i32* %iteration.addr, align 4
  store %class.btCollisionObject** %0, %class.btCollisionObject*** %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %.addr2, align 4
  store i32 %3, i32* %.addr3, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %4, %class.btIDebugDraw** %.addr4, align 4
  %this5 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  %5 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %5, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  store i32 %call, i32* %numNonContactPool, align 4
  %6 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %6, i32 0, i32 2
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call6, i32* %numConstraintPool, align 4
  %7 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %7, i32 0, i32 4
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  store i32 %call7, i32* %numFrictionPool, align 4
  %8 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %9 = bitcast %struct.btContactSolverInfo* %8 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 16
  %10 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %10, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end54

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %j, align 4
  %12 = load i32, i32* %numNonContactPool, align 4
  %cmp = icmp slt i32 %11, %12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %13, i32 0, i32 7
  %14 = load i32, i32* %j, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool, i32 %14)
  %15 = load i32, i32* %call8, align 4
  store i32 %15, i32* %tmp, align 4
  %16 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %17 = load i32, i32* %j, align 4
  %add = add nsw i32 %17, 1
  %call9 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %16, i32 %add)
  store i32 %call9, i32* %swapi, align 4
  %18 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %18, i32 0, i32 7
  %19 = load i32, i32* %swapi, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool10, i32 %19)
  %20 = load i32, i32* %call11, align 4
  %21 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool12 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %21, i32 0, i32 7
  %22 = load i32, i32* %j, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool12, i32 %22)
  store i32 %20, i32* %call13, align 4
  %23 = load i32, i32* %tmp, align 4
  %24 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool14 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %24, i32 0, i32 7
  %25 = load i32, i32* %swapi, align 4
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool14, i32 %25)
  store i32 %23, i32* %call15, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %j, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %27 = load i32, i32* %iteration.addr, align 4
  %28 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %29 = bitcast %struct.btContactSolverInfo* %28 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %29, i32 0, i32 5
  %30 = load i32, i32* %m_numIterations, align 4
  %cmp16 = icmp slt i32 %27, %30
  br i1 %cmp16, label %if.then17, label %if.end

if.then17:                                        ; preds = %for.end
  store i32 0, i32* %j18, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc33, %if.then17
  %31 = load i32, i32* %j18, align 4
  %32 = load i32, i32* %numConstraintPool, align 4
  %cmp20 = icmp slt i32 %31, %32
  br i1 %cmp20, label %for.body21, label %for.end35

for.body21:                                       ; preds = %for.cond19
  %33 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %33, i32 0, i32 6
  %34 = load i32, i32* %j18, align 4
  %call23 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool, i32 %34)
  %35 = load i32, i32* %call23, align 4
  store i32 %35, i32* %tmp22, align 4
  %36 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %37 = load i32, i32* %j18, align 4
  %add25 = add nsw i32 %37, 1
  %call26 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %36, i32 %add25)
  store i32 %call26, i32* %swapi24, align 4
  %38 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool27 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %38, i32 0, i32 6
  %39 = load i32, i32* %swapi24, align 4
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool27, i32 %39)
  %40 = load i32, i32* %call28, align 4
  %41 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool29 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %41, i32 0, i32 6
  %42 = load i32, i32* %j18, align 4
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool29, i32 %42)
  store i32 %40, i32* %call30, align 4
  %43 = load i32, i32* %tmp22, align 4
  %44 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool31 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %44, i32 0, i32 6
  %45 = load i32, i32* %swapi24, align 4
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool31, i32 %45)
  store i32 %43, i32* %call32, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body21
  %46 = load i32, i32* %j18, align 4
  %inc34 = add nsw i32 %46, 1
  store i32 %inc34, i32* %j18, align 4
  br label %for.cond19

for.end35:                                        ; preds = %for.cond19
  store i32 0, i32* %j36, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc51, %for.end35
  %47 = load i32, i32* %j36, align 4
  %48 = load i32, i32* %numFrictionPool, align 4
  %cmp38 = icmp slt i32 %47, %48
  br i1 %cmp38, label %for.body39, label %for.end53

for.body39:                                       ; preds = %for.cond37
  %49 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %49, i32 0, i32 8
  %50 = load i32, i32* %j36, align 4
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool, i32 %50)
  %51 = load i32, i32* %call41, align 4
  store i32 %51, i32* %tmp40, align 4
  %52 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %53 = load i32, i32* %j36, align 4
  %add43 = add nsw i32 %53, 1
  %call44 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %52, i32 %add43)
  store i32 %call44, i32* %swapi42, align 4
  %54 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool45 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %54, i32 0, i32 8
  %55 = load i32, i32* %swapi42, align 4
  %call46 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool45, i32 %55)
  %56 = load i32, i32* %call46, align 4
  %57 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool47 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %57, i32 0, i32 8
  %58 = load i32, i32* %j36, align 4
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool47, i32 %58)
  store i32 %56, i32* %call48, align 4
  %59 = load i32, i32* %tmp40, align 4
  %60 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool49 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %60, i32 0, i32 8
  %61 = load i32, i32* %swapi42, align 4
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool49, i32 %61)
  store i32 %59, i32* %call50, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %for.body39
  %62 = load i32, i32* %j36, align 4
  %inc52 = add nsw i32 %62, 1
  store i32 %inc52, i32* %j36, align 4
  br label %for.cond37

for.end53:                                        ; preds = %for.cond37
  br label %if.end

if.end:                                           ; preds = %for.end53, %for.end
  br label %if.end54

if.end54:                                         ; preds = %if.end, %entry
  store float 0.000000e+00, float* %deltaflengthsqr, align 4
  %63 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %64 = bitcast %struct.btContactSolverInfo* %63 to %struct.btContactSolverInfoData*
  %m_solverMode55 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %64, i32 0, i32 16
  %65 = load i32, i32* %m_solverMode55, align 4
  %and56 = and i32 %65, 256
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.else

if.then58:                                        ; preds = %if.end54
  store i32 0, i32* %j59, align 4
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc78, %if.then58
  %66 = load i32, i32* %j59, align 4
  %67 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool61 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %67, i32 0, i32 3
  %call62 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool61)
  %cmp63 = icmp slt i32 %66, %call62
  br i1 %cmp63, label %for.body64, label %for.end80

for.body64:                                       ; preds = %for.cond60
  %68 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool65 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %68, i32 0, i32 3
  %69 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool66 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %69, i32 0, i32 7
  %70 = load i32, i32* %j59, align 4
  %call67 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool66, i32 %70)
  %71 = load i32, i32* %call67, align 4
  %call68 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool65, i32 %71)
  store %struct.btSolverConstraint* %call68, %struct.btSolverConstraint** %constraint, align 4
  %72 = load i32, i32* %iteration.addr, align 4
  %73 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_overrideNumSolverIterations = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %73, i32 0, i32 16
  %74 = load i32, i32* %m_overrideNumSolverIterations, align 4
  %cmp69 = icmp slt i32 %72, %74
  br i1 %cmp69, label %if.then70, label %if.end77

if.then70:                                        ; preds = %for.body64
  %75 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %76 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %76, i32 0, i32 1
  %77 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %77, i32 0, i32 18
  %78 = load i32, i32* %m_solverBodyIdA, align 4
  %call71 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %78)
  %79 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool72 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %79, i32 0, i32 1
  %80 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %80, i32 0, i32 19
  %81 = load i32, i32* %m_solverBodyIdB, align 4
  %call73 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool72, i32 %81)
  %82 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %call74 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %75, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call71, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call73, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %82)
  store float %call74, float* %deltaf, align 4
  %83 = load float, float* %deltaf, align 4
  %m_deltafNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %84 = load i32, i32* %j59, align 4
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC, i32 %84)
  store float %83, float* %call75, align 4
  %85 = load float, float* %deltaf, align 4
  %86 = load float, float* %deltaf, align 4
  %mul = fmul float %85, %86
  %87 = load float, float* %deltaflengthsqr, align 4
  %add76 = fadd float %87, %mul
  store float %add76, float* %deltaflengthsqr, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then70, %for.body64
  br label %for.inc78

for.inc78:                                        ; preds = %if.end77
  %88 = load i32, i32* %j59, align 4
  %inc79 = add nsw i32 %88, 1
  store i32 %inc79, i32* %j59, align 4
  br label %for.cond60

for.end80:                                        ; preds = %for.cond60
  br label %if.end111

if.else:                                          ; preds = %if.end54
  store i32 0, i32* %j81, align 4
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc108, %if.else
  %89 = load i32, i32* %j81, align 4
  %90 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool83 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %90, i32 0, i32 3
  %call84 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool83)
  %cmp85 = icmp slt i32 %89, %call84
  br i1 %cmp85, label %for.body86, label %for.end110

for.body86:                                       ; preds = %for.cond82
  %91 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool88 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %91, i32 0, i32 3
  %92 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool89 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %92, i32 0, i32 7
  %93 = load i32, i32* %j81, align 4
  %call90 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool89, i32 %93)
  %94 = load i32, i32* %call90, align 4
  %call91 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool88, i32 %94)
  store %struct.btSolverConstraint* %call91, %struct.btSolverConstraint** %constraint87, align 4
  %95 = load i32, i32* %iteration.addr, align 4
  %96 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint87, align 4
  %m_overrideNumSolverIterations92 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %96, i32 0, i32 16
  %97 = load i32, i32* %m_overrideNumSolverIterations92, align 4
  %cmp93 = icmp slt i32 %95, %97
  br i1 %cmp93, label %if.then94, label %if.end107

if.then94:                                        ; preds = %for.body86
  %98 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %99 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool96 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %99, i32 0, i32 1
  %100 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint87, align 4
  %m_solverBodyIdA97 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %100, i32 0, i32 18
  %101 = load i32, i32* %m_solverBodyIdA97, align 4
  %call98 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool96, i32 %101)
  %102 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool99 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %102, i32 0, i32 1
  %103 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint87, align 4
  %m_solverBodyIdB100 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %103, i32 0, i32 19
  %104 = load i32, i32* %m_solverBodyIdB100, align 4
  %call101 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool99, i32 %104)
  %105 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint87, align 4
  %call102 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %98, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call98, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call101, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %105)
  store float %call102, float* %deltaf95, align 4
  %106 = load float, float* %deltaf95, align 4
  %m_deltafNC103 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %107 = load i32, i32* %j81, align 4
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC103, i32 %107)
  store float %106, float* %call104, align 4
  %108 = load float, float* %deltaf95, align 4
  %109 = load float, float* %deltaf95, align 4
  %mul105 = fmul float %108, %109
  %110 = load float, float* %deltaflengthsqr, align 4
  %add106 = fadd float %110, %mul105
  store float %add106, float* %deltaflengthsqr, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.then94, %for.body86
  br label %for.inc108

for.inc108:                                       ; preds = %if.end107
  %111 = load i32, i32* %j81, align 4
  %inc109 = add nsw i32 %111, 1
  store i32 %inc109, i32* %j81, align 4
  br label %for.cond82

for.end110:                                       ; preds = %for.cond82
  br label %if.end111

if.end111:                                        ; preds = %for.end110, %for.end80
  %m_onlyForNoneContact = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 10
  %112 = load i8, i8* %m_onlyForNoneContact, align 4
  %tobool112 = trunc i8 %112 to i1
  br i1 %tobool112, label %if.then113, label %if.end188

if.then113:                                       ; preds = %if.end111
  %113 = load i32, i32* %iteration.addr, align 4
  %cmp114 = icmp eq i32 %113, 0
  br i1 %cmp114, label %if.then115, label %if.else128

if.then115:                                       ; preds = %if.then113
  store i32 0, i32* %j116, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc125, %if.then115
  %114 = load i32, i32* %j116, align 4
  %115 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool118 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %115, i32 0, i32 3
  %call119 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool118)
  %cmp120 = icmp slt i32 %114, %call119
  br i1 %cmp120, label %for.body121, label %for.end127

for.body121:                                      ; preds = %for.cond117
  %m_deltafNC122 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %116 = load i32, i32* %j116, align 4
  %call123 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC122, i32 %116)
  %117 = load float, float* %call123, align 4
  %m_pNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %118 = load i32, i32* %j116, align 4
  %call124 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC, i32 %118)
  store float %117, float* %call124, align 4
  br label %for.inc125

for.inc125:                                       ; preds = %for.body121
  %119 = load i32, i32* %j116, align 4
  %inc126 = add nsw i32 %119, 1
  store i32 %inc126, i32* %j116, align 4
  br label %for.cond117

for.end127:                                       ; preds = %for.cond117
  br label %if.end186

if.else128:                                       ; preds = %if.then113
  %m_deltafLengthSqrPrev = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  %120 = load float, float* %m_deltafLengthSqrPrev, align 4
  %cmp129 = fcmp ogt float %120, 0.000000e+00
  br i1 %cmp129, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else128
  %121 = load float, float* %deltaflengthsqr, align 4
  %m_deltafLengthSqrPrev130 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  %122 = load float, float* %m_deltafLengthSqrPrev130, align 4
  %div = fdiv float %121, %122
  br label %cond.end

cond.false:                                       ; preds = %if.else128
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div, %cond.true ], [ 2.000000e+00, %cond.false ]
  store float %cond, float* %beta, align 4
  %123 = load float, float* %beta, align 4
  %cmp131 = fcmp ogt float %123, 1.000000e+00
  br i1 %cmp131, label %if.then132, label %if.else144

if.then132:                                       ; preds = %cond.end
  store i32 0, i32* %j133, align 4
  br label %for.cond134

for.cond134:                                      ; preds = %for.inc141, %if.then132
  %124 = load i32, i32* %j133, align 4
  %125 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool135 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %125, i32 0, i32 3
  %call136 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool135)
  %cmp137 = icmp slt i32 %124, %call136
  br i1 %cmp137, label %for.body138, label %for.end143

for.body138:                                      ; preds = %for.cond134
  %m_pNC139 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %126 = load i32, i32* %j133, align 4
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC139, i32 %126)
  store float 0.000000e+00, float* %call140, align 4
  br label %for.inc141

for.inc141:                                       ; preds = %for.body138
  %127 = load i32, i32* %j133, align 4
  %inc142 = add nsw i32 %127, 1
  store i32 %inc142, i32* %j133, align 4
  br label %for.cond134

for.end143:                                       ; preds = %for.cond134
  br label %if.end185

if.else144:                                       ; preds = %cond.end
  store i32 0, i32* %j145, align 4
  br label %for.cond146

for.cond146:                                      ; preds = %for.inc182, %if.else144
  %128 = load i32, i32* %j145, align 4
  %129 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool147 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %129, i32 0, i32 3
  %call148 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool147)
  %cmp149 = icmp slt i32 %128, %call148
  br i1 %cmp149, label %for.body150, label %for.end184

for.body150:                                      ; preds = %for.cond146
  %130 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool152 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %130, i32 0, i32 3
  %131 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool153 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %131, i32 0, i32 7
  %132 = load i32, i32* %j145, align 4
  %call154 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool153, i32 %132)
  %133 = load i32, i32* %call154, align 4
  %call155 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool152, i32 %133)
  store %struct.btSolverConstraint* %call155, %struct.btSolverConstraint** %constraint151, align 4
  %134 = load i32, i32* %iteration.addr, align 4
  %135 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  %m_overrideNumSolverIterations156 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %135, i32 0, i32 16
  %136 = load i32, i32* %m_overrideNumSolverIterations156, align 4
  %cmp157 = icmp slt i32 %134, %136
  br i1 %cmp157, label %if.then158, label %if.end181

if.then158:                                       ; preds = %for.body150
  %137 = load float, float* %beta, align 4
  %m_pNC159 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %138 = load i32, i32* %j145, align 4
  %call160 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC159, i32 %138)
  %139 = load float, float* %call160, align 4
  %mul161 = fmul float %137, %139
  store float %mul161, float* %additionaldeltaimpulse, align 4
  %140 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %140, i32 0, i32 7
  %141 = load float, float* %m_appliedImpulse, align 4
  %142 = load float, float* %additionaldeltaimpulse, align 4
  %add162 = fadd float %141, %142
  %143 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  %m_appliedImpulse163 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %143, i32 0, i32 7
  store float %add162, float* %m_appliedImpulse163, align 4
  %144 = load float, float* %beta, align 4
  %m_pNC164 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %145 = load i32, i32* %j145, align 4
  %call165 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC164, i32 %145)
  %146 = load float, float* %call165, align 4
  %mul166 = fmul float %144, %146
  %m_deltafNC167 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %147 = load i32, i32* %j145, align 4
  %call168 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC167, i32 %147)
  %148 = load float, float* %call168, align 4
  %add169 = fadd float %mul166, %148
  %m_pNC170 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %149 = load i32, i32* %j145, align 4
  %call171 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC170, i32 %149)
  store float %add169, float* %call171, align 4
  %150 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool172 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %150, i32 0, i32 1
  %151 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  %m_solverBodyIdA173 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %151, i32 0, i32 18
  %152 = load i32, i32* %m_solverBodyIdA173, align 4
  %call174 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool172, i32 %152)
  store %struct.btSolverBody* %call174, %struct.btSolverBody** %body1, align 4
  %153 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool175 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %153, i32 0, i32 1
  %154 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  %m_solverBodyIdB176 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %154, i32 0, i32 19
  %155 = load i32, i32* %m_solverBodyIdB176, align 4
  %call177 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool175, i32 %155)
  store %struct.btSolverBody* %call177, %struct.btSolverBody** %body2, align 4
  %156 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint151, align 4
  store %struct.btSolverConstraint* %156, %struct.btSolverConstraint** %c, align 4
  %157 = load %struct.btSolverBody*, %struct.btSolverBody** %body1, align 4
  %158 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %158, i32 0, i32 1
  %159 = load %struct.btSolverBody*, %struct.btSolverBody** %body1, align 4
  %call178 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %159)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call178)
  %160 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %160, i32 0, i32 4
  %161 = load float, float* %additionaldeltaimpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %157, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %161)
  %162 = load %struct.btSolverBody*, %struct.btSolverBody** %body2, align 4
  %163 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %163, i32 0, i32 3
  %164 = load %struct.btSolverBody*, %struct.btSolverBody** %body2, align 4
  %call180 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %164)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp179, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call180)
  %165 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %165, i32 0, i32 5
  %166 = load float, float* %additionaldeltaimpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %162, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp179, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %166)
  br label %if.end181

if.end181:                                        ; preds = %if.then158, %for.body150
  br label %for.inc182

for.inc182:                                       ; preds = %if.end181
  %167 = load i32, i32* %j145, align 4
  %inc183 = add nsw i32 %167, 1
  store i32 %inc183, i32* %j145, align 4
  br label %for.cond146

for.end184:                                       ; preds = %for.cond146
  br label %if.end185

if.end185:                                        ; preds = %for.end184, %for.end143
  br label %if.end186

if.end186:                                        ; preds = %if.end185, %for.end127
  %168 = load float, float* %deltaflengthsqr, align 4
  %m_deltafLengthSqrPrev187 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  store float %168, float* %m_deltafLengthSqrPrev187, align 4
  br label %if.end188

if.end188:                                        ; preds = %if.end186, %if.end111
  %169 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %170 = bitcast %struct.btContactSolverInfo* %169 to %struct.btContactSolverInfoData*
  %m_solverMode189 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %170, i32 0, i32 16
  %171 = load i32, i32* %m_solverMode189, align 4
  %and190 = and i32 %171, 256
  %tobool191 = icmp ne i32 %and190, 0
  br i1 %tobool191, label %if.then192, label %if.else437

if.then192:                                       ; preds = %if.end188
  %172 = load i32, i32* %iteration.addr, align 4
  %173 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %174 = bitcast %struct.btContactSolverInfo* %173 to %struct.btContactSolverInfoData*
  %m_numIterations193 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %174, i32 0, i32 5
  %175 = load i32, i32* %m_numIterations193, align 4
  %cmp194 = icmp slt i32 %172, %175
  br i1 %cmp194, label %if.then195, label %if.end436

if.then195:                                       ; preds = %if.then192
  store i32 0, i32* %j196, align 4
  br label %for.cond197

for.cond197:                                      ; preds = %for.inc216, %if.then195
  %176 = load i32, i32* %j196, align 4
  %177 = load i32, i32* %numConstraints.addr, align 4
  %cmp198 = icmp slt i32 %176, %177
  br i1 %cmp198, label %for.body199, label %for.end218

for.body199:                                      ; preds = %for.cond197
  %178 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %179 = load i32, i32* %j196, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %178, i32 %179
  %180 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  %call200 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %180)
  br i1 %call200, label %if.then201, label %if.end215

if.then201:                                       ; preds = %for.body199
  %181 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %182 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %183 = load i32, i32* %j196, align 4
  %arrayidx202 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %182, i32 %183
  %184 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx202, align 4
  %call203 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %184)
  %185 = bitcast %class.btRigidBody* %call203 to %class.btCollisionObject*
  %186 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %187 = bitcast %struct.btContactSolverInfo* %186 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %187, i32 0, i32 3
  %188 = load float, float* %m_timeStep, align 4
  %call204 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %181, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %185, float %188)
  store i32 %call204, i32* %bodyAid, align 4
  %189 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %190 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %191 = load i32, i32* %j196, align 4
  %arrayidx205 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %190, i32 %191
  %192 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx205, align 4
  %call206 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %192)
  %193 = bitcast %class.btRigidBody* %call206 to %class.btCollisionObject*
  %194 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %195 = bitcast %struct.btContactSolverInfo* %194 to %struct.btContactSolverInfoData*
  %m_timeStep207 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %195, i32 0, i32 3
  %196 = load float, float* %m_timeStep207, align 4
  %call208 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %189, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %193, float %196)
  store i32 %call208, i32* %bodyBid, align 4
  %197 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool209 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %197, i32 0, i32 1
  %198 = load i32, i32* %bodyAid, align 4
  %call210 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool209, i32 %198)
  store %struct.btSolverBody* %call210, %struct.btSolverBody** %bodyA, align 4
  %199 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool211 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %199, i32 0, i32 1
  %200 = load i32, i32* %bodyBid, align 4
  %call212 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool211, i32 %200)
  store %struct.btSolverBody* %call212, %struct.btSolverBody** %bodyB, align 4
  %201 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %202 = load i32, i32* %j196, align 4
  %arrayidx213 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %201, i32 %202
  %203 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx213, align 4
  %204 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %205 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %206 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %207 = bitcast %struct.btContactSolverInfo* %206 to %struct.btContactSolverInfoData*
  %m_timeStep214 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %207, i32 0, i32 3
  %208 = load float, float* %m_timeStep214, align 4
  %209 = bitcast %class.btTypedConstraint* %203 to void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)***
  %vtable = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)**, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*** %209, align 4
  %vfn = getelementptr inbounds void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vtable, i64 6
  %210 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vfn, align 4
  call void %210(%class.btTypedConstraint* %203, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %204, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %205, float %208)
  br label %if.end215

if.end215:                                        ; preds = %if.then201, %for.body199
  br label %for.inc216

for.inc216:                                       ; preds = %if.end215
  %211 = load i32, i32* %j196, align 4
  %inc217 = add nsw i32 %211, 1
  store i32 %inc217, i32* %j196, align 4
  br label %for.cond197

for.end218:                                       ; preds = %for.cond197
  %212 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %213 = bitcast %struct.btContactSolverInfo* %212 to %struct.btContactSolverInfoData*
  %m_solverMode219 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %213, i32 0, i32 16
  %214 = load i32, i32* %m_solverMode219, align 4
  %and220 = and i32 %214, 512
  %tobool221 = icmp ne i32 %and220, 0
  br i1 %tobool221, label %if.then222, label %if.else324

if.then222:                                       ; preds = %for.end218
  %215 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool223 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %215, i32 0, i32 2
  %call224 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool223)
  store i32 %call224, i32* %numPoolConstraints, align 4
  %216 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %217 = bitcast %struct.btContactSolverInfo* %216 to %struct.btContactSolverInfoData*
  %m_solverMode225 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %217, i32 0, i32 16
  %218 = load i32, i32* %m_solverMode225, align 4
  %and226 = and i32 %218, 16
  %tobool227 = icmp ne i32 %and226, 0
  %219 = zext i1 %tobool227 to i64
  %cond228 = select i1 %tobool227, i32 2, i32 1
  store i32 %cond228, i32* %multiplier, align 4
  store i32 0, i32* %c229, align 4
  br label %for.cond230

for.cond230:                                      ; preds = %for.inc321, %if.then222
  %220 = load i32, i32* %c229, align 4
  %221 = load i32, i32* %numPoolConstraints, align 4
  %cmp231 = icmp slt i32 %220, %221
  br i1 %cmp231, label %for.body232, label %for.end323

for.body232:                                      ; preds = %for.cond230
  store float 0.000000e+00, float* %totalImpulse, align 4
  %222 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool233 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %222, i32 0, i32 2
  %223 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool234 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %223, i32 0, i32 6
  %224 = load i32, i32* %c229, align 4
  %call235 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool234, i32 %224)
  %225 = load i32, i32* %call235, align 4
  %call236 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool233, i32 %225)
  store %struct.btSolverConstraint* %call236, %struct.btSolverConstraint** %solveManifold, align 4
  %226 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %227 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool238 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %227, i32 0, i32 1
  %228 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdA239 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %228, i32 0, i32 18
  %229 = load i32, i32* %m_solverBodyIdA239, align 4
  %call240 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool238, i32 %229)
  %230 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool241 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %230, i32 0, i32 1
  %231 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdB242 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %231, i32 0, i32 19
  %232 = load i32, i32* %m_solverBodyIdB242, align 4
  %call243 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool241, i32 %232)
  %233 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %call244 = call float @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %226, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call240, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call243, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %233)
  store float %call244, float* %deltaf237, align 4
  %234 = load float, float* %deltaf237, align 4
  %m_deltafC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 7
  %235 = load i32, i32* %c229, align 4
  %call245 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafC, i32 %235)
  store float %234, float* %call245, align 4
  %236 = load float, float* %deltaf237, align 4
  %237 = load float, float* %deltaf237, align 4
  %mul246 = fmul float %236, %237
  %238 = load float, float* %deltaflengthsqr, align 4
  %add247 = fadd float %238, %mul246
  store float %add247, float* %deltaflengthsqr, align 4
  %239 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_appliedImpulse248 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %239, i32 0, i32 7
  %240 = load float, float* %m_appliedImpulse248, align 4
  store float %240, float* %totalImpulse, align 4
  store i8 1, i8* %applyFriction, align 1
  %241 = load i8, i8* %applyFriction, align 1
  %tobool249 = trunc i8 %241 to i1
  br i1 %tobool249, label %if.then250, label %if.end320

if.then250:                                       ; preds = %for.body232
  %242 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool252 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %242, i32 0, i32 4
  %243 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool253 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %243, i32 0, i32 8
  %244 = load i32, i32* %c229, align 4
  %245 = load i32, i32* %multiplier, align 4
  %mul254 = mul nsw i32 %244, %245
  %call255 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool253, i32 %mul254)
  %246 = load i32, i32* %call255, align 4
  %call256 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool252, i32 %246)
  store %struct.btSolverConstraint* %call256, %struct.btSolverConstraint** %solveManifold251, align 4
  %247 = load float, float* %totalImpulse, align 4
  %cmp257 = fcmp ogt float %247, 0.000000e+00
  br i1 %cmp257, label %if.then258, label %if.else274

if.then258:                                       ; preds = %if.then250
  %248 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_friction = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %248, i32 0, i32 8
  %249 = load float, float* %m_friction, align 4
  %250 = load float, float* %totalImpulse, align 4
  %mul259 = fmul float %249, %250
  %fneg = fneg float %mul259
  %251 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %251, i32 0, i32 12
  store float %fneg, float* %m_lowerLimit, align 4
  %252 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_friction260 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %252, i32 0, i32 8
  %253 = load float, float* %m_friction260, align 4
  %254 = load float, float* %totalImpulse, align 4
  %mul261 = fmul float %253, %254
  %255 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %255, i32 0, i32 13
  store float %mul261, float* %m_upperLimit, align 4
  %256 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %257 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool263 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %257, i32 0, i32 1
  %258 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_solverBodyIdA264 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %258, i32 0, i32 18
  %259 = load i32, i32* %m_solverBodyIdA264, align 4
  %call265 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool263, i32 %259)
  %260 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool266 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %260, i32 0, i32 1
  %261 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %m_solverBodyIdB267 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %261, i32 0, i32 19
  %262 = load i32, i32* %m_solverBodyIdB267, align 4
  %call268 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool266, i32 %262)
  %263 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold251, align 4
  %call269 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %256, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call265, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call268, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %263)
  store float %call269, float* %deltaf262, align 4
  %264 = load float, float* %deltaf262, align 4
  %m_deltafCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %265 = load i32, i32* %c229, align 4
  %266 = load i32, i32* %multiplier, align 4
  %mul270 = mul nsw i32 %265, %266
  %call271 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF, i32 %mul270)
  store float %264, float* %call271, align 4
  %267 = load float, float* %deltaf262, align 4
  %268 = load float, float* %deltaf262, align 4
  %mul272 = fmul float %267, %268
  %269 = load float, float* %deltaflengthsqr, align 4
  %add273 = fadd float %269, %mul272
  store float %add273, float* %deltaflengthsqr, align 4
  br label %if.end278

if.else274:                                       ; preds = %if.then250
  %m_deltafCF275 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %270 = load i32, i32* %c229, align 4
  %271 = load i32, i32* %multiplier, align 4
  %mul276 = mul nsw i32 %270, %271
  %call277 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF275, i32 %mul276)
  store float 0.000000e+00, float* %call277, align 4
  br label %if.end278

if.end278:                                        ; preds = %if.else274, %if.then258
  %272 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %273 = bitcast %struct.btContactSolverInfo* %272 to %struct.btContactSolverInfoData*
  %m_solverMode279 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %273, i32 0, i32 16
  %274 = load i32, i32* %m_solverMode279, align 4
  %and280 = and i32 %274, 16
  %tobool281 = icmp ne i32 %and280, 0
  br i1 %tobool281, label %if.then282, label %if.end319

if.then282:                                       ; preds = %if.end278
  %275 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool284 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %275, i32 0, i32 4
  %276 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool285 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %276, i32 0, i32 8
  %277 = load i32, i32* %c229, align 4
  %278 = load i32, i32* %multiplier, align 4
  %mul286 = mul nsw i32 %277, %278
  %add287 = add nsw i32 %mul286, 1
  %call288 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool285, i32 %add287)
  %279 = load i32, i32* %call288, align 4
  %call289 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool284, i32 %279)
  store %struct.btSolverConstraint* %call289, %struct.btSolverConstraint** %solveManifold283, align 4
  %280 = load float, float* %totalImpulse, align 4
  %cmp290 = fcmp ogt float %280, 0.000000e+00
  br i1 %cmp290, label %if.then291, label %if.else313

if.then291:                                       ; preds = %if.then282
  %281 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_friction292 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %281, i32 0, i32 8
  %282 = load float, float* %m_friction292, align 4
  %283 = load float, float* %totalImpulse, align 4
  %mul293 = fmul float %282, %283
  %fneg294 = fneg float %mul293
  %284 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_lowerLimit295 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %284, i32 0, i32 12
  store float %fneg294, float* %m_lowerLimit295, align 4
  %285 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_friction296 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %285, i32 0, i32 8
  %286 = load float, float* %m_friction296, align 4
  %287 = load float, float* %totalImpulse, align 4
  %mul297 = fmul float %286, %287
  %288 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_upperLimit298 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %288, i32 0, i32 13
  store float %mul297, float* %m_upperLimit298, align 4
  %289 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %290 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool300 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %290, i32 0, i32 1
  %291 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_solverBodyIdA301 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %291, i32 0, i32 18
  %292 = load i32, i32* %m_solverBodyIdA301, align 4
  %call302 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool300, i32 %292)
  %293 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool303 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %293, i32 0, i32 1
  %294 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %m_solverBodyIdB304 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %294, i32 0, i32 19
  %295 = load i32, i32* %m_solverBodyIdB304, align 4
  %call305 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool303, i32 %295)
  %296 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold283, align 4
  %call306 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %289, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call302, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call305, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %296)
  store float %call306, float* %deltaf299, align 4
  %297 = load float, float* %deltaf299, align 4
  %m_deltafCF307 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %298 = load i32, i32* %c229, align 4
  %299 = load i32, i32* %multiplier, align 4
  %mul308 = mul nsw i32 %298, %299
  %add309 = add nsw i32 %mul308, 1
  %call310 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF307, i32 %add309)
  store float %297, float* %call310, align 4
  %300 = load float, float* %deltaf299, align 4
  %301 = load float, float* %deltaf299, align 4
  %mul311 = fmul float %300, %301
  %302 = load float, float* %deltaflengthsqr, align 4
  %add312 = fadd float %302, %mul311
  store float %add312, float* %deltaflengthsqr, align 4
  br label %if.end318

if.else313:                                       ; preds = %if.then282
  %m_deltafCF314 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %303 = load i32, i32* %c229, align 4
  %304 = load i32, i32* %multiplier, align 4
  %mul315 = mul nsw i32 %303, %304
  %add316 = add nsw i32 %mul315, 1
  %call317 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF314, i32 %add316)
  store float 0.000000e+00, float* %call317, align 4
  br label %if.end318

if.end318:                                        ; preds = %if.else313, %if.then291
  br label %if.end319

if.end319:                                        ; preds = %if.end318, %if.end278
  br label %if.end320

if.end320:                                        ; preds = %if.end319, %for.body232
  br label %for.inc321

for.inc321:                                       ; preds = %if.end320
  %305 = load i32, i32* %c229, align 4
  %inc322 = add nsw i32 %305, 1
  store i32 %inc322, i32* %c229, align 4
  br label %for.cond230

for.end323:                                       ; preds = %for.cond230
  br label %if.end435

if.else324:                                       ; preds = %for.end218
  %306 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool326 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %306, i32 0, i32 2
  %call327 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool326)
  store i32 %call327, i32* %numPoolConstraints325, align 4
  store i32 0, i32* %j328, align 4
  br label %for.cond329

for.cond329:                                      ; preds = %for.inc349, %if.else324
  %307 = load i32, i32* %j328, align 4
  %308 = load i32, i32* %numPoolConstraints325, align 4
  %cmp330 = icmp slt i32 %307, %308
  br i1 %cmp330, label %for.body331, label %for.end351

for.body331:                                      ; preds = %for.cond329
  %309 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool333 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %309, i32 0, i32 2
  %310 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool334 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %310, i32 0, i32 6
  %311 = load i32, i32* %j328, align 4
  %call335 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool334, i32 %311)
  %312 = load i32, i32* %call335, align 4
  %call336 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool333, i32 %312)
  store %struct.btSolverConstraint* %call336, %struct.btSolverConstraint** %solveManifold332, align 4
  %313 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %314 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool338 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %314, i32 0, i32 1
  %315 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold332, align 4
  %m_solverBodyIdA339 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %315, i32 0, i32 18
  %316 = load i32, i32* %m_solverBodyIdA339, align 4
  %call340 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool338, i32 %316)
  %317 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool341 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %317, i32 0, i32 1
  %318 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold332, align 4
  %m_solverBodyIdB342 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %318, i32 0, i32 19
  %319 = load i32, i32* %m_solverBodyIdB342, align 4
  %call343 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool341, i32 %319)
  %320 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold332, align 4
  %call344 = call float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %313, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call340, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call343, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %320)
  store float %call344, float* %deltaf337, align 4
  %321 = load float, float* %deltaf337, align 4
  %m_deltafC345 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 7
  %322 = load i32, i32* %j328, align 4
  %call346 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafC345, i32 %322)
  store float %321, float* %call346, align 4
  %323 = load float, float* %deltaf337, align 4
  %324 = load float, float* %deltaf337, align 4
  %mul347 = fmul float %323, %324
  %325 = load float, float* %deltaflengthsqr, align 4
  %add348 = fadd float %325, %mul347
  store float %add348, float* %deltaflengthsqr, align 4
  br label %for.inc349

for.inc349:                                       ; preds = %for.body331
  %326 = load i32, i32* %j328, align 4
  %inc350 = add nsw i32 %326, 1
  store i32 %inc350, i32* %j328, align 4
  br label %for.cond329

for.end351:                                       ; preds = %for.cond329
  %327 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool352 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %327, i32 0, i32 4
  %call353 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool352)
  store i32 %call353, i32* %numFrictionPoolConstraints, align 4
  store i32 0, i32* %j328, align 4
  br label %for.cond354

for.cond354:                                      ; preds = %for.inc391, %for.end351
  %328 = load i32, i32* %j328, align 4
  %329 = load i32, i32* %numFrictionPoolConstraints, align 4
  %cmp355 = icmp slt i32 %328, %329
  br i1 %cmp355, label %for.body356, label %for.end393

for.body356:                                      ; preds = %for.cond354
  %330 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool358 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %330, i32 0, i32 4
  %331 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool359 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %331, i32 0, i32 8
  %332 = load i32, i32* %j328, align 4
  %call360 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool359, i32 %332)
  %333 = load i32, i32* %call360, align 4
  %call361 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool358, i32 %333)
  store %struct.btSolverConstraint* %call361, %struct.btSolverConstraint** %solveManifold357, align 4
  %334 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool363 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %334, i32 0, i32 2
  %335 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %335, i32 0, i32 17
  %336 = load i32, i32* %m_frictionIndex, align 4
  %call364 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool363, i32 %336)
  %m_appliedImpulse365 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call364, i32 0, i32 7
  %337 = load float, float* %m_appliedImpulse365, align 4
  store float %337, float* %totalImpulse362, align 4
  %338 = load float, float* %totalImpulse362, align 4
  %cmp366 = fcmp ogt float %338, 0.000000e+00
  br i1 %cmp366, label %if.then367, label %if.else387

if.then367:                                       ; preds = %for.body356
  %339 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_friction368 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %339, i32 0, i32 8
  %340 = load float, float* %m_friction368, align 4
  %341 = load float, float* %totalImpulse362, align 4
  %mul369 = fmul float %340, %341
  %fneg370 = fneg float %mul369
  %342 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_lowerLimit371 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %342, i32 0, i32 12
  store float %fneg370, float* %m_lowerLimit371, align 4
  %343 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_friction372 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %343, i32 0, i32 8
  %344 = load float, float* %m_friction372, align 4
  %345 = load float, float* %totalImpulse362, align 4
  %mul373 = fmul float %344, %345
  %346 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_upperLimit374 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %346, i32 0, i32 13
  store float %mul373, float* %m_upperLimit374, align 4
  %347 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %348 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool376 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %348, i32 0, i32 1
  %349 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_solverBodyIdA377 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %349, i32 0, i32 18
  %350 = load i32, i32* %m_solverBodyIdA377, align 4
  %call378 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool376, i32 %350)
  %351 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool379 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %351, i32 0, i32 1
  %352 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %m_solverBodyIdB380 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %352, i32 0, i32 19
  %353 = load i32, i32* %m_solverBodyIdB380, align 4
  %call381 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool379, i32 %353)
  %354 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold357, align 4
  %call382 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %347, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call378, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call381, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %354)
  store float %call382, float* %deltaf375, align 4
  %355 = load float, float* %deltaf375, align 4
  %m_deltafCF383 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %356 = load i32, i32* %j328, align 4
  %call384 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF383, i32 %356)
  store float %355, float* %call384, align 4
  %357 = load float, float* %deltaf375, align 4
  %358 = load float, float* %deltaf375, align 4
  %mul385 = fmul float %357, %358
  %359 = load float, float* %deltaflengthsqr, align 4
  %add386 = fadd float %359, %mul385
  store float %add386, float* %deltaflengthsqr, align 4
  br label %if.end390

if.else387:                                       ; preds = %for.body356
  %m_deltafCF388 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %360 = load i32, i32* %j328, align 4
  %call389 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF388, i32 %360)
  store float 0.000000e+00, float* %call389, align 4
  br label %if.end390

if.end390:                                        ; preds = %if.else387, %if.then367
  br label %for.inc391

for.inc391:                                       ; preds = %if.end390
  %361 = load i32, i32* %j328, align 4
  %inc392 = add nsw i32 %361, 1
  store i32 %inc392, i32* %j328, align 4
  br label %for.cond354

for.end393:                                       ; preds = %for.cond354
  %362 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %362, i32 0, i32 5
  %call394 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool)
  store i32 %call394, i32* %numRollingFrictionPoolConstraints, align 4
  store i32 0, i32* %j328, align 4
  br label %for.cond395

for.cond395:                                      ; preds = %for.inc432, %for.end393
  %363 = load i32, i32* %j328, align 4
  %364 = load i32, i32* %numRollingFrictionPoolConstraints, align 4
  %cmp396 = icmp slt i32 %363, %364
  br i1 %cmp396, label %for.body397, label %for.end434

for.body397:                                      ; preds = %for.cond395
  %365 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool398 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %365, i32 0, i32 5
  %366 = load i32, i32* %j328, align 4
  %call399 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool398, i32 %366)
  store %struct.btSolverConstraint* %call399, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %367 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool401 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %367, i32 0, i32 2
  %368 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_frictionIndex402 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %368, i32 0, i32 17
  %369 = load i32, i32* %m_frictionIndex402, align 4
  %call403 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool401, i32 %369)
  %m_appliedImpulse404 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call403, i32 0, i32 7
  %370 = load float, float* %m_appliedImpulse404, align 4
  store float %370, float* %totalImpulse400, align 4
  %371 = load float, float* %totalImpulse400, align 4
  %cmp405 = fcmp ogt float %371, 0.000000e+00
  br i1 %cmp405, label %if.then406, label %if.else428

if.then406:                                       ; preds = %for.body397
  %372 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction407 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %372, i32 0, i32 8
  %373 = load float, float* %m_friction407, align 4
  %374 = load float, float* %totalImpulse400, align 4
  %mul408 = fmul float %373, %374
  store float %mul408, float* %rollingFrictionMagnitude, align 4
  %375 = load float, float* %rollingFrictionMagnitude, align 4
  %376 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction409 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %376, i32 0, i32 8
  %377 = load float, float* %m_friction409, align 4
  %cmp410 = fcmp ogt float %375, %377
  br i1 %cmp410, label %if.then411, label %if.end413

if.then411:                                       ; preds = %if.then406
  %378 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction412 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %378, i32 0, i32 8
  %379 = load float, float* %m_friction412, align 4
  store float %379, float* %rollingFrictionMagnitude, align 4
  br label %if.end413

if.end413:                                        ; preds = %if.then411, %if.then406
  %380 = load float, float* %rollingFrictionMagnitude, align 4
  %fneg414 = fneg float %380
  %381 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_lowerLimit415 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %381, i32 0, i32 12
  store float %fneg414, float* %m_lowerLimit415, align 4
  %382 = load float, float* %rollingFrictionMagnitude, align 4
  %383 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_upperLimit416 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %383, i32 0, i32 13
  store float %382, float* %m_upperLimit416, align 4
  %384 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %385 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool418 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %385, i32 0, i32 1
  %386 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_solverBodyIdA419 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %386, i32 0, i32 18
  %387 = load i32, i32* %m_solverBodyIdA419, align 4
  %call420 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool418, i32 %387)
  %388 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool421 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %388, i32 0, i32 1
  %389 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_solverBodyIdB422 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %389, i32 0, i32 19
  %390 = load i32, i32* %m_solverBodyIdB422, align 4
  %call423 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool421, i32 %390)
  %391 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %call424 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %384, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call420, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call423, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %391)
  store float %call424, float* %deltaf417, align 4
  %392 = load float, float* %deltaf417, align 4
  %m_deltafCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %393 = load i32, i32* %j328, align 4
  %call425 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF, i32 %393)
  store float %392, float* %call425, align 4
  %394 = load float, float* %deltaf417, align 4
  %395 = load float, float* %deltaf417, align 4
  %mul426 = fmul float %394, %395
  %396 = load float, float* %deltaflengthsqr, align 4
  %add427 = fadd float %396, %mul426
  store float %add427, float* %deltaflengthsqr, align 4
  br label %if.end431

if.else428:                                       ; preds = %for.body397
  %m_deltafCRF429 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %397 = load i32, i32* %j328, align 4
  %call430 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF429, i32 %397)
  store float 0.000000e+00, float* %call430, align 4
  br label %if.end431

if.end431:                                        ; preds = %if.else428, %if.end413
  br label %for.inc432

for.inc432:                                       ; preds = %if.end431
  %398 = load i32, i32* %j328, align 4
  %inc433 = add nsw i32 %398, 1
  store i32 %inc433, i32* %j328, align 4
  br label %for.cond395

for.end434:                                       ; preds = %for.cond395
  br label %if.end435

if.end435:                                        ; preds = %for.end434, %for.end323
  br label %if.end436

if.end436:                                        ; preds = %if.end435, %if.then192
  br label %if.end592

if.else437:                                       ; preds = %if.end188
  %399 = load i32, i32* %iteration.addr, align 4
  %400 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %401 = bitcast %struct.btContactSolverInfo* %400 to %struct.btContactSolverInfoData*
  %m_numIterations438 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %401, i32 0, i32 5
  %402 = load i32, i32* %m_numIterations438, align 4
  %cmp439 = icmp slt i32 %399, %402
  br i1 %cmp439, label %if.then440, label %if.end591

if.then440:                                       ; preds = %if.else437
  store i32 0, i32* %j441, align 4
  br label %for.cond442

for.cond442:                                      ; preds = %for.inc469, %if.then440
  %403 = load i32, i32* %j441, align 4
  %404 = load i32, i32* %numConstraints.addr, align 4
  %cmp443 = icmp slt i32 %403, %404
  br i1 %cmp443, label %for.body444, label %for.end471

for.body444:                                      ; preds = %for.cond442
  %405 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %406 = load i32, i32* %j441, align 4
  %arrayidx445 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %405, i32 %406
  %407 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx445, align 4
  %call446 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %407)
  br i1 %call446, label %if.then447, label %if.end468

if.then447:                                       ; preds = %for.body444
  %408 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %409 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %410 = load i32, i32* %j441, align 4
  %arrayidx449 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %409, i32 %410
  %411 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx449, align 4
  %call450 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %411)
  %412 = bitcast %class.btRigidBody* %call450 to %class.btCollisionObject*
  %413 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %414 = bitcast %struct.btContactSolverInfo* %413 to %struct.btContactSolverInfoData*
  %m_timeStep451 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %414, i32 0, i32 3
  %415 = load float, float* %m_timeStep451, align 4
  %call452 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %408, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %412, float %415)
  store i32 %call452, i32* %bodyAid448, align 4
  %416 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %417 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %418 = load i32, i32* %j441, align 4
  %arrayidx454 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %417, i32 %418
  %419 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx454, align 4
  %call455 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %419)
  %420 = bitcast %class.btRigidBody* %call455 to %class.btCollisionObject*
  %421 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %422 = bitcast %struct.btContactSolverInfo* %421 to %struct.btContactSolverInfoData*
  %m_timeStep456 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %422, i32 0, i32 3
  %423 = load float, float* %m_timeStep456, align 4
  %call457 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %416, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %420, float %423)
  store i32 %call457, i32* %bodyBid453, align 4
  %424 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool459 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %424, i32 0, i32 1
  %425 = load i32, i32* %bodyAid448, align 4
  %call460 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool459, i32 %425)
  store %struct.btSolverBody* %call460, %struct.btSolverBody** %bodyA458, align 4
  %426 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool462 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %426, i32 0, i32 1
  %427 = load i32, i32* %bodyBid453, align 4
  %call463 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool462, i32 %427)
  store %struct.btSolverBody* %call463, %struct.btSolverBody** %bodyB461, align 4
  %428 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %429 = load i32, i32* %j441, align 4
  %arrayidx464 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %428, i32 %429
  %430 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx464, align 4
  %431 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA458, align 4
  %432 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB461, align 4
  %433 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %434 = bitcast %struct.btContactSolverInfo* %433 to %struct.btContactSolverInfoData*
  %m_timeStep465 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %434, i32 0, i32 3
  %435 = load float, float* %m_timeStep465, align 4
  %436 = bitcast %class.btTypedConstraint* %430 to void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)***
  %vtable466 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)**, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*** %436, align 4
  %vfn467 = getelementptr inbounds void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vtable466, i64 6
  %437 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vfn467, align 4
  call void %437(%class.btTypedConstraint* %430, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %431, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %432, float %435)
  br label %if.end468

if.end468:                                        ; preds = %if.then447, %for.body444
  br label %for.inc469

for.inc469:                                       ; preds = %if.end468
  %438 = load i32, i32* %j441, align 4
  %inc470 = add nsw i32 %438, 1
  store i32 %inc470, i32* %j441, align 4
  br label %for.cond442

for.end471:                                       ; preds = %for.cond442
  %439 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool473 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %439, i32 0, i32 2
  %call474 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool473)
  store i32 %call474, i32* %numPoolConstraints472, align 4
  store i32 0, i32* %j475, align 4
  br label %for.cond476

for.cond476:                                      ; preds = %for.inc496, %for.end471
  %440 = load i32, i32* %j475, align 4
  %441 = load i32, i32* %numPoolConstraints472, align 4
  %cmp477 = icmp slt i32 %440, %441
  br i1 %cmp477, label %for.body478, label %for.end498

for.body478:                                      ; preds = %for.cond476
  %442 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool480 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %442, i32 0, i32 2
  %443 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool481 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %443, i32 0, i32 6
  %444 = load i32, i32* %j475, align 4
  %call482 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool481, i32 %444)
  %445 = load i32, i32* %call482, align 4
  %call483 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool480, i32 %445)
  store %struct.btSolverConstraint* %call483, %struct.btSolverConstraint** %solveManifold479, align 4
  %446 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %447 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool485 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %447, i32 0, i32 1
  %448 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold479, align 4
  %m_solverBodyIdA486 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %448, i32 0, i32 18
  %449 = load i32, i32* %m_solverBodyIdA486, align 4
  %call487 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool485, i32 %449)
  %450 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool488 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %450, i32 0, i32 1
  %451 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold479, align 4
  %m_solverBodyIdB489 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %451, i32 0, i32 19
  %452 = load i32, i32* %m_solverBodyIdB489, align 4
  %call490 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool488, i32 %452)
  %453 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold479, align 4
  %call491 = call float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %446, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call487, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call490, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %453)
  store float %call491, float* %deltaf484, align 4
  %454 = load float, float* %deltaf484, align 4
  %m_deltafC492 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 7
  %455 = load i32, i32* %j475, align 4
  %call493 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafC492, i32 %455)
  store float %454, float* %call493, align 4
  %456 = load float, float* %deltaf484, align 4
  %457 = load float, float* %deltaf484, align 4
  %mul494 = fmul float %456, %457
  %458 = load float, float* %deltaflengthsqr, align 4
  %add495 = fadd float %458, %mul494
  store float %add495, float* %deltaflengthsqr, align 4
  br label %for.inc496

for.inc496:                                       ; preds = %for.body478
  %459 = load i32, i32* %j475, align 4
  %inc497 = add nsw i32 %459, 1
  store i32 %inc497, i32* %j475, align 4
  br label %for.cond476

for.end498:                                       ; preds = %for.cond476
  %460 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool500 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %460, i32 0, i32 4
  %call501 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool500)
  store i32 %call501, i32* %numFrictionPoolConstraints499, align 4
  store i32 0, i32* %j502, align 4
  br label %for.cond503

for.cond503:                                      ; preds = %for.inc541, %for.end498
  %461 = load i32, i32* %j502, align 4
  %462 = load i32, i32* %numFrictionPoolConstraints499, align 4
  %cmp504 = icmp slt i32 %461, %462
  br i1 %cmp504, label %for.body505, label %for.end543

for.body505:                                      ; preds = %for.cond503
  %463 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool507 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %463, i32 0, i32 4
  %464 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool508 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %464, i32 0, i32 8
  %465 = load i32, i32* %j502, align 4
  %call509 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool508, i32 %465)
  %466 = load i32, i32* %call509, align 4
  %call510 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool507, i32 %466)
  store %struct.btSolverConstraint* %call510, %struct.btSolverConstraint** %solveManifold506, align 4
  %467 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool512 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %467, i32 0, i32 2
  %468 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_frictionIndex513 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %468, i32 0, i32 17
  %469 = load i32, i32* %m_frictionIndex513, align 4
  %call514 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool512, i32 %469)
  %m_appliedImpulse515 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call514, i32 0, i32 7
  %470 = load float, float* %m_appliedImpulse515, align 4
  store float %470, float* %totalImpulse511, align 4
  %471 = load float, float* %totalImpulse511, align 4
  %cmp516 = fcmp ogt float %471, 0.000000e+00
  br i1 %cmp516, label %if.then517, label %if.else537

if.then517:                                       ; preds = %for.body505
  %472 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_friction518 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %472, i32 0, i32 8
  %473 = load float, float* %m_friction518, align 4
  %474 = load float, float* %totalImpulse511, align 4
  %mul519 = fmul float %473, %474
  %fneg520 = fneg float %mul519
  %475 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_lowerLimit521 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %475, i32 0, i32 12
  store float %fneg520, float* %m_lowerLimit521, align 4
  %476 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_friction522 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %476, i32 0, i32 8
  %477 = load float, float* %m_friction522, align 4
  %478 = load float, float* %totalImpulse511, align 4
  %mul523 = fmul float %477, %478
  %479 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_upperLimit524 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %479, i32 0, i32 13
  store float %mul523, float* %m_upperLimit524, align 4
  %480 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %481 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool526 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %481, i32 0, i32 1
  %482 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_solverBodyIdA527 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %482, i32 0, i32 18
  %483 = load i32, i32* %m_solverBodyIdA527, align 4
  %call528 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool526, i32 %483)
  %484 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool529 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %484, i32 0, i32 1
  %485 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %m_solverBodyIdB530 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %485, i32 0, i32 19
  %486 = load i32, i32* %m_solverBodyIdB530, align 4
  %call531 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool529, i32 %486)
  %487 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold506, align 4
  %call532 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %480, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call528, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call531, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %487)
  store float %call532, float* %deltaf525, align 4
  %488 = load float, float* %deltaf525, align 4
  %m_deltafCF533 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %489 = load i32, i32* %j502, align 4
  %call534 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF533, i32 %489)
  store float %488, float* %call534, align 4
  %490 = load float, float* %deltaf525, align 4
  %491 = load float, float* %deltaf525, align 4
  %mul535 = fmul float %490, %491
  %492 = load float, float* %deltaflengthsqr, align 4
  %add536 = fadd float %492, %mul535
  store float %add536, float* %deltaflengthsqr, align 4
  br label %if.end540

if.else537:                                       ; preds = %for.body505
  %m_deltafCF538 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %493 = load i32, i32* %j502, align 4
  %call539 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF538, i32 %493)
  store float 0.000000e+00, float* %call539, align 4
  br label %if.end540

if.end540:                                        ; preds = %if.else537, %if.then517
  br label %for.inc541

for.inc541:                                       ; preds = %if.end540
  %494 = load i32, i32* %j502, align 4
  %inc542 = add nsw i32 %494, 1
  store i32 %inc542, i32* %j502, align 4
  br label %for.cond503

for.end543:                                       ; preds = %for.cond503
  %495 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool545 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %495, i32 0, i32 5
  %call546 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool545)
  store i32 %call546, i32* %numRollingFrictionPoolConstraints544, align 4
  store i32 0, i32* %j547, align 4
  br label %for.cond548

for.cond548:                                      ; preds = %for.inc588, %for.end543
  %496 = load i32, i32* %j547, align 4
  %497 = load i32, i32* %numRollingFrictionPoolConstraints544, align 4
  %cmp549 = icmp slt i32 %496, %497
  br i1 %cmp549, label %for.body550, label %for.end590

for.body550:                                      ; preds = %for.cond548
  %498 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool552 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %498, i32 0, i32 5
  %499 = load i32, i32* %j547, align 4
  %call553 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool552, i32 %499)
  store %struct.btSolverConstraint* %call553, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %500 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool555 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %500, i32 0, i32 2
  %501 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_frictionIndex556 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %501, i32 0, i32 17
  %502 = load i32, i32* %m_frictionIndex556, align 4
  %call557 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool555, i32 %502)
  %m_appliedImpulse558 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call557, i32 0, i32 7
  %503 = load float, float* %m_appliedImpulse558, align 4
  store float %503, float* %totalImpulse554, align 4
  %504 = load float, float* %totalImpulse554, align 4
  %cmp559 = fcmp ogt float %504, 0.000000e+00
  br i1 %cmp559, label %if.then560, label %if.else584

if.then560:                                       ; preds = %for.body550
  %505 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_friction562 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %505, i32 0, i32 8
  %506 = load float, float* %m_friction562, align 4
  %507 = load float, float* %totalImpulse554, align 4
  %mul563 = fmul float %506, %507
  store float %mul563, float* %rollingFrictionMagnitude561, align 4
  %508 = load float, float* %rollingFrictionMagnitude561, align 4
  %509 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_friction564 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %509, i32 0, i32 8
  %510 = load float, float* %m_friction564, align 4
  %cmp565 = fcmp ogt float %508, %510
  br i1 %cmp565, label %if.then566, label %if.end568

if.then566:                                       ; preds = %if.then560
  %511 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_friction567 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %511, i32 0, i32 8
  %512 = load float, float* %m_friction567, align 4
  store float %512, float* %rollingFrictionMagnitude561, align 4
  br label %if.end568

if.end568:                                        ; preds = %if.then566, %if.then560
  %513 = load float, float* %rollingFrictionMagnitude561, align 4
  %fneg569 = fneg float %513
  %514 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_lowerLimit570 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %514, i32 0, i32 12
  store float %fneg569, float* %m_lowerLimit570, align 4
  %515 = load float, float* %rollingFrictionMagnitude561, align 4
  %516 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_upperLimit571 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %516, i32 0, i32 13
  store float %515, float* %m_upperLimit571, align 4
  %517 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %518 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool573 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %518, i32 0, i32 1
  %519 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_solverBodyIdA574 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %519, i32 0, i32 18
  %520 = load i32, i32* %m_solverBodyIdA574, align 4
  %call575 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool573, i32 %520)
  %521 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool576 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %521, i32 0, i32 1
  %522 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %m_solverBodyIdB577 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %522, i32 0, i32 19
  %523 = load i32, i32* %m_solverBodyIdB577, align 4
  %call578 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool576, i32 %523)
  %524 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint551, align 4
  %call579 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %517, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call575, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call578, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %524)
  store float %call579, float* %deltaf572, align 4
  %525 = load float, float* %deltaf572, align 4
  %m_deltafCRF580 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %526 = load i32, i32* %j547, align 4
  %call581 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF580, i32 %526)
  store float %525, float* %call581, align 4
  %527 = load float, float* %deltaf572, align 4
  %528 = load float, float* %deltaf572, align 4
  %mul582 = fmul float %527, %528
  %529 = load float, float* %deltaflengthsqr, align 4
  %add583 = fadd float %529, %mul582
  store float %add583, float* %deltaflengthsqr, align 4
  br label %if.end587

if.else584:                                       ; preds = %for.body550
  %m_deltafCRF585 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %530 = load i32, i32* %j547, align 4
  %call586 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF585, i32 %530)
  store float 0.000000e+00, float* %call586, align 4
  br label %if.end587

if.end587:                                        ; preds = %if.else584, %if.end568
  br label %for.inc588

for.inc588:                                       ; preds = %if.end587
  %531 = load i32, i32* %j547, align 4
  %inc589 = add nsw i32 %531, 1
  store i32 %inc589, i32* %j547, align 4
  br label %for.cond548

for.end590:                                       ; preds = %for.cond548
  br label %if.end591

if.end591:                                        ; preds = %for.end590, %if.else437
  br label %if.end592

if.end592:                                        ; preds = %if.end591, %if.end436
  %m_onlyForNoneContact593 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 10
  %532 = load i8, i8* %m_onlyForNoneContact593, align 4
  %tobool594 = trunc i8 %532 to i1
  br i1 %tobool594, label %if.end927, label %if.then595

if.then595:                                       ; preds = %if.end592
  %533 = load i32, i32* %iteration.addr, align 4
  %cmp596 = icmp eq i32 %533, 0
  br i1 %cmp596, label %if.then597, label %if.else655

if.then597:                                       ; preds = %if.then595
  store i32 0, i32* %j598, align 4
  br label %for.cond599

for.cond599:                                      ; preds = %for.inc608, %if.then597
  %534 = load i32, i32* %j598, align 4
  %535 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool600 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %535, i32 0, i32 3
  %call601 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool600)
  %cmp602 = icmp slt i32 %534, %call601
  br i1 %cmp602, label %for.body603, label %for.end610

for.body603:                                      ; preds = %for.cond599
  %m_deltafNC604 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %536 = load i32, i32* %j598, align 4
  %call605 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC604, i32 %536)
  %537 = load float, float* %call605, align 4
  %m_pNC606 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %538 = load i32, i32* %j598, align 4
  %call607 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC606, i32 %538)
  store float %537, float* %call607, align 4
  br label %for.inc608

for.inc608:                                       ; preds = %for.body603
  %539 = load i32, i32* %j598, align 4
  %inc609 = add nsw i32 %539, 1
  store i32 %inc609, i32* %j598, align 4
  br label %for.cond599

for.end610:                                       ; preds = %for.cond599
  store i32 0, i32* %j611, align 4
  br label %for.cond612

for.cond612:                                      ; preds = %for.inc620, %for.end610
  %540 = load i32, i32* %j611, align 4
  %541 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool613 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %541, i32 0, i32 2
  %call614 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool613)
  %cmp615 = icmp slt i32 %540, %call614
  br i1 %cmp615, label %for.body616, label %for.end622

for.body616:                                      ; preds = %for.cond612
  %m_deltafC617 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 7
  %542 = load i32, i32* %j611, align 4
  %call618 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafC617, i32 %542)
  %543 = load float, float* %call618, align 4
  %m_pC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 3
  %544 = load i32, i32* %j611, align 4
  %call619 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pC, i32 %544)
  store float %543, float* %call619, align 4
  br label %for.inc620

for.inc620:                                       ; preds = %for.body616
  %545 = load i32, i32* %j611, align 4
  %inc621 = add nsw i32 %545, 1
  store i32 %inc621, i32* %j611, align 4
  br label %for.cond612

for.end622:                                       ; preds = %for.cond612
  store i32 0, i32* %j623, align 4
  br label %for.cond624

for.cond624:                                      ; preds = %for.inc632, %for.end622
  %546 = load i32, i32* %j623, align 4
  %547 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool625 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %547, i32 0, i32 4
  %call626 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool625)
  %cmp627 = icmp slt i32 %546, %call626
  br i1 %cmp627, label %for.body628, label %for.end634

for.body628:                                      ; preds = %for.cond624
  %m_deltafCF629 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %548 = load i32, i32* %j623, align 4
  %call630 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF629, i32 %548)
  %549 = load float, float* %call630, align 4
  %m_pCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 4
  %550 = load i32, i32* %j623, align 4
  %call631 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCF, i32 %550)
  store float %549, float* %call631, align 4
  br label %for.inc632

for.inc632:                                       ; preds = %for.body628
  %551 = load i32, i32* %j623, align 4
  %inc633 = add nsw i32 %551, 1
  store i32 %inc633, i32* %j623, align 4
  br label %for.cond624

for.end634:                                       ; preds = %for.cond624
  %552 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %553 = bitcast %struct.btContactSolverInfo* %552 to %struct.btContactSolverInfoData*
  %m_solverMode635 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %553, i32 0, i32 16
  %554 = load i32, i32* %m_solverMode635, align 4
  %and636 = and i32 %554, 256
  %cmp637 = icmp eq i32 %and636, 0
  br i1 %cmp637, label %if.then641, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end634
  %555 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %556 = bitcast %struct.btContactSolverInfo* %555 to %struct.btContactSolverInfoData*
  %m_solverMode638 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %556, i32 0, i32 16
  %557 = load i32, i32* %m_solverMode638, align 4
  %and639 = and i32 %557, 512
  %cmp640 = icmp eq i32 %and639, 0
  br i1 %cmp640, label %if.then641, label %if.end654

if.then641:                                       ; preds = %lor.lhs.false, %for.end634
  store i32 0, i32* %j642, align 4
  br label %for.cond643

for.cond643:                                      ; preds = %for.inc651, %if.then641
  %558 = load i32, i32* %j642, align 4
  %559 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool644 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %559, i32 0, i32 5
  %call645 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool644)
  %cmp646 = icmp slt i32 %558, %call645
  br i1 %cmp646, label %for.body647, label %for.end653

for.body647:                                      ; preds = %for.cond643
  %m_deltafCRF648 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %560 = load i32, i32* %j642, align 4
  %call649 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF648, i32 %560)
  %561 = load float, float* %call649, align 4
  %m_pCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 5
  %562 = load i32, i32* %j642, align 4
  %call650 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCRF, i32 %562)
  store float %561, float* %call650, align 4
  br label %for.inc651

for.inc651:                                       ; preds = %for.body647
  %563 = load i32, i32* %j642, align 4
  %inc652 = add nsw i32 %563, 1
  store i32 %inc652, i32* %j642, align 4
  br label %for.cond643

for.end653:                                       ; preds = %for.cond643
  br label %if.end654

if.end654:                                        ; preds = %for.end653, %lor.lhs.false
  br label %if.end925

if.else655:                                       ; preds = %if.then595
  %m_deltafLengthSqrPrev657 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  %564 = load float, float* %m_deltafLengthSqrPrev657, align 4
  %cmp658 = fcmp ogt float %564, 0.000000e+00
  br i1 %cmp658, label %cond.true659, label %cond.false662

cond.true659:                                     ; preds = %if.else655
  %565 = load float, float* %deltaflengthsqr, align 4
  %m_deltafLengthSqrPrev660 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  %566 = load float, float* %m_deltafLengthSqrPrev660, align 4
  %div661 = fdiv float %565, %566
  br label %cond.end663

cond.false662:                                    ; preds = %if.else655
  br label %cond.end663

cond.end663:                                      ; preds = %cond.false662, %cond.true659
  %cond664 = phi float [ %div661, %cond.true659 ], [ 2.000000e+00, %cond.false662 ]
  store float %cond664, float* %beta656, align 4
  %567 = load float, float* %beta656, align 4
  %cmp665 = fcmp ogt float %567, 1.000000e+00
  br i1 %cmp665, label %if.then666, label %if.else716

if.then666:                                       ; preds = %cond.end663
  store i32 0, i32* %j667, align 4
  br label %for.cond668

for.cond668:                                      ; preds = %for.inc675, %if.then666
  %568 = load i32, i32* %j667, align 4
  %569 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool669 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %569, i32 0, i32 3
  %call670 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool669)
  %cmp671 = icmp slt i32 %568, %call670
  br i1 %cmp671, label %for.body672, label %for.end677

for.body672:                                      ; preds = %for.cond668
  %m_pNC673 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %570 = load i32, i32* %j667, align 4
  %call674 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC673, i32 %570)
  store float 0.000000e+00, float* %call674, align 4
  br label %for.inc675

for.inc675:                                       ; preds = %for.body672
  %571 = load i32, i32* %j667, align 4
  %inc676 = add nsw i32 %571, 1
  store i32 %inc676, i32* %j667, align 4
  br label %for.cond668

for.end677:                                       ; preds = %for.cond668
  store i32 0, i32* %j678, align 4
  br label %for.cond679

for.cond679:                                      ; preds = %for.inc686, %for.end677
  %572 = load i32, i32* %j678, align 4
  %573 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool680 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %573, i32 0, i32 2
  %call681 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool680)
  %cmp682 = icmp slt i32 %572, %call681
  br i1 %cmp682, label %for.body683, label %for.end688

for.body683:                                      ; preds = %for.cond679
  %m_pC684 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 3
  %574 = load i32, i32* %j678, align 4
  %call685 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pC684, i32 %574)
  store float 0.000000e+00, float* %call685, align 4
  br label %for.inc686

for.inc686:                                       ; preds = %for.body683
  %575 = load i32, i32* %j678, align 4
  %inc687 = add nsw i32 %575, 1
  store i32 %inc687, i32* %j678, align 4
  br label %for.cond679

for.end688:                                       ; preds = %for.cond679
  store i32 0, i32* %j689, align 4
  br label %for.cond690

for.cond690:                                      ; preds = %for.inc697, %for.end688
  %576 = load i32, i32* %j689, align 4
  %577 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool691 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %577, i32 0, i32 4
  %call692 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool691)
  %cmp693 = icmp slt i32 %576, %call692
  br i1 %cmp693, label %for.body694, label %for.end699

for.body694:                                      ; preds = %for.cond690
  %m_pCF695 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 4
  %578 = load i32, i32* %j689, align 4
  %call696 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCF695, i32 %578)
  store float 0.000000e+00, float* %call696, align 4
  br label %for.inc697

for.inc697:                                       ; preds = %for.body694
  %579 = load i32, i32* %j689, align 4
  %inc698 = add nsw i32 %579, 1
  store i32 %inc698, i32* %j689, align 4
  br label %for.cond690

for.end699:                                       ; preds = %for.cond690
  %580 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %581 = bitcast %struct.btContactSolverInfo* %580 to %struct.btContactSolverInfoData*
  %m_solverMode700 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %581, i32 0, i32 16
  %582 = load i32, i32* %m_solverMode700, align 4
  %and701 = and i32 %582, 512
  %cmp702 = icmp eq i32 %and701, 0
  br i1 %cmp702, label %if.then703, label %if.end715

if.then703:                                       ; preds = %for.end699
  store i32 0, i32* %j704, align 4
  br label %for.cond705

for.cond705:                                      ; preds = %for.inc712, %if.then703
  %583 = load i32, i32* %j704, align 4
  %584 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool706 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %584, i32 0, i32 5
  %call707 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool706)
  %cmp708 = icmp slt i32 %583, %call707
  br i1 %cmp708, label %for.body709, label %for.end714

for.body709:                                      ; preds = %for.cond705
  %m_pCRF710 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 5
  %585 = load i32, i32* %j704, align 4
  %call711 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCRF710, i32 %585)
  store float 0.000000e+00, float* %call711, align 4
  br label %for.inc712

for.inc712:                                       ; preds = %for.body709
  %586 = load i32, i32* %j704, align 4
  %inc713 = add nsw i32 %586, 1
  store i32 %inc713, i32* %j704, align 4
  br label %for.cond705

for.end714:                                       ; preds = %for.cond705
  br label %if.end715

if.end715:                                        ; preds = %for.end714, %for.end699
  br label %if.end924

if.else716:                                       ; preds = %cond.end663
  store i32 0, i32* %j717, align 4
  br label %for.cond718

for.cond718:                                      ; preds = %for.inc764, %if.else716
  %587 = load i32, i32* %j717, align 4
  %588 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool719 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %588, i32 0, i32 3
  %call720 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool719)
  %cmp721 = icmp slt i32 %587, %call720
  br i1 %cmp721, label %for.body722, label %for.end766

for.body722:                                      ; preds = %for.cond718
  %589 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverNonContactConstraintPool724 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %589, i32 0, i32 3
  %590 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderNonContactConstraintPool725 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %590, i32 0, i32 7
  %591 = load i32, i32* %j717, align 4
  %call726 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool725, i32 %591)
  %592 = load i32, i32* %call726, align 4
  %call727 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool724, i32 %592)
  store %struct.btSolverConstraint* %call727, %struct.btSolverConstraint** %constraint723, align 4
  %593 = load i32, i32* %iteration.addr, align 4
  %594 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  %m_overrideNumSolverIterations728 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %594, i32 0, i32 16
  %595 = load i32, i32* %m_overrideNumSolverIterations728, align 4
  %cmp729 = icmp slt i32 %593, %595
  br i1 %cmp729, label %if.then730, label %if.end763

if.then730:                                       ; preds = %for.body722
  %596 = load float, float* %beta656, align 4
  %m_pNC732 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %597 = load i32, i32* %j717, align 4
  %call733 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC732, i32 %597)
  %598 = load float, float* %call733, align 4
  %mul734 = fmul float %596, %598
  store float %mul734, float* %additionaldeltaimpulse731, align 4
  %599 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  %m_appliedImpulse735 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %599, i32 0, i32 7
  %600 = load float, float* %m_appliedImpulse735, align 4
  %601 = load float, float* %additionaldeltaimpulse731, align 4
  %add736 = fadd float %600, %601
  %602 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  %m_appliedImpulse737 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %602, i32 0, i32 7
  store float %add736, float* %m_appliedImpulse737, align 4
  %603 = load float, float* %beta656, align 4
  %m_pNC738 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %604 = load i32, i32* %j717, align 4
  %call739 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC738, i32 %604)
  %605 = load float, float* %call739, align 4
  %mul740 = fmul float %603, %605
  %m_deltafNC741 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 6
  %606 = load i32, i32* %j717, align 4
  %call742 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafNC741, i32 %606)
  %607 = load float, float* %call742, align 4
  %add743 = fadd float %mul740, %607
  %m_pNC744 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 2
  %608 = load i32, i32* %j717, align 4
  %call745 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pNC744, i32 %608)
  store float %add743, float* %call745, align 4
  %609 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool747 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %609, i32 0, i32 1
  %610 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  %m_solverBodyIdA748 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %610, i32 0, i32 18
  %611 = load i32, i32* %m_solverBodyIdA748, align 4
  %call749 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool747, i32 %611)
  store %struct.btSolverBody* %call749, %struct.btSolverBody** %body1746, align 4
  %612 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool751 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %612, i32 0, i32 1
  %613 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  %m_solverBodyIdB752 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %613, i32 0, i32 19
  %614 = load i32, i32* %m_solverBodyIdB752, align 4
  %call753 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool751, i32 %614)
  store %struct.btSolverBody* %call753, %struct.btSolverBody** %body2750, align 4
  %615 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint723, align 4
  store %struct.btSolverConstraint* %615, %struct.btSolverConstraint** %c754, align 4
  %616 = load %struct.btSolverBody*, %struct.btSolverBody** %body1746, align 4
  %617 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c754, align 4
  %m_contactNormal1756 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %617, i32 0, i32 1
  %618 = load %struct.btSolverBody*, %struct.btSolverBody** %body1746, align 4
  %call757 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %618)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp755, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1756, %class.btVector3* nonnull align 4 dereferenceable(16) %call757)
  %619 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c754, align 4
  %m_angularComponentA758 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %619, i32 0, i32 4
  %620 = load float, float* %additionaldeltaimpulse731, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %616, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp755, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA758, float %620)
  %621 = load %struct.btSolverBody*, %struct.btSolverBody** %body2750, align 4
  %622 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c754, align 4
  %m_contactNormal2760 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %622, i32 0, i32 3
  %623 = load %struct.btSolverBody*, %struct.btSolverBody** %body2750, align 4
  %call761 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %623)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp759, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2760, %class.btVector3* nonnull align 4 dereferenceable(16) %call761)
  %624 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c754, align 4
  %m_angularComponentB762 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %624, i32 0, i32 5
  %625 = load float, float* %additionaldeltaimpulse731, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %621, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp759, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB762, float %625)
  br label %if.end763

if.end763:                                        ; preds = %if.then730, %for.body722
  br label %for.inc764

for.inc764:                                       ; preds = %if.end763
  %626 = load i32, i32* %j717, align 4
  %inc765 = add nsw i32 %626, 1
  store i32 %inc765, i32* %j717, align 4
  br label %for.cond718

for.end766:                                       ; preds = %for.cond718
  store i32 0, i32* %j767, align 4
  br label %for.cond768

for.cond768:                                      ; preds = %for.inc814, %for.end766
  %627 = load i32, i32* %j767, align 4
  %628 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool769 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %628, i32 0, i32 2
  %call770 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool769)
  %cmp771 = icmp slt i32 %627, %call770
  br i1 %cmp771, label %for.body772, label %for.end816

for.body772:                                      ; preds = %for.cond768
  %629 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactConstraintPool774 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %629, i32 0, i32 2
  %630 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderTmpConstraintPool775 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %630, i32 0, i32 6
  %631 = load i32, i32* %j767, align 4
  %call776 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool775, i32 %631)
  %632 = load i32, i32* %call776, align 4
  %call777 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool774, i32 %632)
  store %struct.btSolverConstraint* %call777, %struct.btSolverConstraint** %constraint773, align 4
  %633 = load i32, i32* %iteration.addr, align 4
  %634 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %635 = bitcast %struct.btContactSolverInfo* %634 to %struct.btContactSolverInfoData*
  %m_numIterations778 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %635, i32 0, i32 5
  %636 = load i32, i32* %m_numIterations778, align 4
  %cmp779 = icmp slt i32 %633, %636
  br i1 %cmp779, label %if.then780, label %if.end813

if.then780:                                       ; preds = %for.body772
  %637 = load float, float* %beta656, align 4
  %m_pC782 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 3
  %638 = load i32, i32* %j767, align 4
  %call783 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pC782, i32 %638)
  %639 = load float, float* %call783, align 4
  %mul784 = fmul float %637, %639
  store float %mul784, float* %additionaldeltaimpulse781, align 4
  %640 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint773, align 4
  %m_appliedImpulse785 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %640, i32 0, i32 7
  %641 = load float, float* %m_appliedImpulse785, align 4
  %642 = load float, float* %additionaldeltaimpulse781, align 4
  %add786 = fadd float %641, %642
  %643 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint773, align 4
  %m_appliedImpulse787 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %643, i32 0, i32 7
  store float %add786, float* %m_appliedImpulse787, align 4
  %644 = load float, float* %beta656, align 4
  %m_pC788 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 3
  %645 = load i32, i32* %j767, align 4
  %call789 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pC788, i32 %645)
  %646 = load float, float* %call789, align 4
  %mul790 = fmul float %644, %646
  %m_deltafC791 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 7
  %647 = load i32, i32* %j767, align 4
  %call792 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafC791, i32 %647)
  %648 = load float, float* %call792, align 4
  %add793 = fadd float %mul790, %648
  %m_pC794 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 3
  %649 = load i32, i32* %j767, align 4
  %call795 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pC794, i32 %649)
  store float %add793, float* %call795, align 4
  %650 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool797 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %650, i32 0, i32 1
  %651 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint773, align 4
  %m_solverBodyIdA798 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %651, i32 0, i32 18
  %652 = load i32, i32* %m_solverBodyIdA798, align 4
  %call799 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool797, i32 %652)
  store %struct.btSolverBody* %call799, %struct.btSolverBody** %body1796, align 4
  %653 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool801 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %653, i32 0, i32 1
  %654 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint773, align 4
  %m_solverBodyIdB802 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %654, i32 0, i32 19
  %655 = load i32, i32* %m_solverBodyIdB802, align 4
  %call803 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool801, i32 %655)
  store %struct.btSolverBody* %call803, %struct.btSolverBody** %body2800, align 4
  %656 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint773, align 4
  store %struct.btSolverConstraint* %656, %struct.btSolverConstraint** %c804, align 4
  %657 = load %struct.btSolverBody*, %struct.btSolverBody** %body1796, align 4
  %658 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c804, align 4
  %m_contactNormal1806 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %658, i32 0, i32 1
  %659 = load %struct.btSolverBody*, %struct.btSolverBody** %body1796, align 4
  %call807 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %659)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp805, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1806, %class.btVector3* nonnull align 4 dereferenceable(16) %call807)
  %660 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c804, align 4
  %m_angularComponentA808 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %660, i32 0, i32 4
  %661 = load float, float* %additionaldeltaimpulse781, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %657, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp805, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA808, float %661)
  %662 = load %struct.btSolverBody*, %struct.btSolverBody** %body2800, align 4
  %663 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c804, align 4
  %m_contactNormal2810 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %663, i32 0, i32 3
  %664 = load %struct.btSolverBody*, %struct.btSolverBody** %body2800, align 4
  %call811 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %664)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp809, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2810, %class.btVector3* nonnull align 4 dereferenceable(16) %call811)
  %665 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c804, align 4
  %m_angularComponentB812 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %665, i32 0, i32 5
  %666 = load float, float* %additionaldeltaimpulse781, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %662, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp809, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB812, float %666)
  br label %if.end813

if.end813:                                        ; preds = %if.then780, %for.body772
  br label %for.inc814

for.inc814:                                       ; preds = %if.end813
  %667 = load i32, i32* %j767, align 4
  %inc815 = add nsw i32 %667, 1
  store i32 %inc815, i32* %j767, align 4
  br label %for.cond768

for.end816:                                       ; preds = %for.cond768
  store i32 0, i32* %j817, align 4
  br label %for.cond818

for.cond818:                                      ; preds = %for.inc864, %for.end816
  %668 = load i32, i32* %j817, align 4
  %669 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool819 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %669, i32 0, i32 4
  %call820 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool819)
  %cmp821 = icmp slt i32 %668, %call820
  br i1 %cmp821, label %for.body822, label %for.end866

for.body822:                                      ; preds = %for.cond818
  %670 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactFrictionConstraintPool824 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %670, i32 0, i32 4
  %671 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_orderFrictionConstraintPool825 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %671, i32 0, i32 8
  %672 = load i32, i32* %j817, align 4
  %call826 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool825, i32 %672)
  %673 = load i32, i32* %call826, align 4
  %call827 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool824, i32 %673)
  store %struct.btSolverConstraint* %call827, %struct.btSolverConstraint** %constraint823, align 4
  %674 = load i32, i32* %iteration.addr, align 4
  %675 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %676 = bitcast %struct.btContactSolverInfo* %675 to %struct.btContactSolverInfoData*
  %m_numIterations828 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %676, i32 0, i32 5
  %677 = load i32, i32* %m_numIterations828, align 4
  %cmp829 = icmp slt i32 %674, %677
  br i1 %cmp829, label %if.then830, label %if.end863

if.then830:                                       ; preds = %for.body822
  %678 = load float, float* %beta656, align 4
  %m_pCF832 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 4
  %679 = load i32, i32* %j817, align 4
  %call833 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCF832, i32 %679)
  %680 = load float, float* %call833, align 4
  %mul834 = fmul float %678, %680
  store float %mul834, float* %additionaldeltaimpulse831, align 4
  %681 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint823, align 4
  %m_appliedImpulse835 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %681, i32 0, i32 7
  %682 = load float, float* %m_appliedImpulse835, align 4
  %683 = load float, float* %additionaldeltaimpulse831, align 4
  %add836 = fadd float %682, %683
  %684 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint823, align 4
  %m_appliedImpulse837 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %684, i32 0, i32 7
  store float %add836, float* %m_appliedImpulse837, align 4
  %685 = load float, float* %beta656, align 4
  %m_pCF838 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 4
  %686 = load i32, i32* %j817, align 4
  %call839 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCF838, i32 %686)
  %687 = load float, float* %call839, align 4
  %mul840 = fmul float %685, %687
  %m_deltafCF841 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 8
  %688 = load i32, i32* %j817, align 4
  %call842 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCF841, i32 %688)
  %689 = load float, float* %call842, align 4
  %add843 = fadd float %mul840, %689
  %m_pCF844 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 4
  %690 = load i32, i32* %j817, align 4
  %call845 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCF844, i32 %690)
  store float %add843, float* %call845, align 4
  %691 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool847 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %691, i32 0, i32 1
  %692 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint823, align 4
  %m_solverBodyIdA848 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %692, i32 0, i32 18
  %693 = load i32, i32* %m_solverBodyIdA848, align 4
  %call849 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool847, i32 %693)
  store %struct.btSolverBody* %call849, %struct.btSolverBody** %body1846, align 4
  %694 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool851 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %694, i32 0, i32 1
  %695 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint823, align 4
  %m_solverBodyIdB852 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %695, i32 0, i32 19
  %696 = load i32, i32* %m_solverBodyIdB852, align 4
  %call853 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool851, i32 %696)
  store %struct.btSolverBody* %call853, %struct.btSolverBody** %body2850, align 4
  %697 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint823, align 4
  store %struct.btSolverConstraint* %697, %struct.btSolverConstraint** %c854, align 4
  %698 = load %struct.btSolverBody*, %struct.btSolverBody** %body1846, align 4
  %699 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c854, align 4
  %m_contactNormal1856 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %699, i32 0, i32 1
  %700 = load %struct.btSolverBody*, %struct.btSolverBody** %body1846, align 4
  %call857 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %700)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp855, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1856, %class.btVector3* nonnull align 4 dereferenceable(16) %call857)
  %701 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c854, align 4
  %m_angularComponentA858 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %701, i32 0, i32 4
  %702 = load float, float* %additionaldeltaimpulse831, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %698, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp855, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA858, float %702)
  %703 = load %struct.btSolverBody*, %struct.btSolverBody** %body2850, align 4
  %704 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c854, align 4
  %m_contactNormal2860 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %704, i32 0, i32 3
  %705 = load %struct.btSolverBody*, %struct.btSolverBody** %body2850, align 4
  %call861 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %705)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp859, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2860, %class.btVector3* nonnull align 4 dereferenceable(16) %call861)
  %706 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c854, align 4
  %m_angularComponentB862 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %706, i32 0, i32 5
  %707 = load float, float* %additionaldeltaimpulse831, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %703, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp859, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB862, float %707)
  br label %if.end863

if.end863:                                        ; preds = %if.then830, %for.body822
  br label %for.inc864

for.inc864:                                       ; preds = %if.end863
  %708 = load i32, i32* %j817, align 4
  %inc865 = add nsw i32 %708, 1
  store i32 %inc865, i32* %j817, align 4
  br label %for.cond818

for.end866:                                       ; preds = %for.cond818
  %709 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %710 = bitcast %struct.btContactSolverInfo* %709 to %struct.btContactSolverInfoData*
  %m_solverMode867 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %710, i32 0, i32 16
  %711 = load i32, i32* %m_solverMode867, align 4
  %and868 = and i32 %711, 256
  %cmp869 = icmp eq i32 %and868, 0
  br i1 %cmp869, label %if.then874, label %lor.lhs.false870

lor.lhs.false870:                                 ; preds = %for.end866
  %712 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %713 = bitcast %struct.btContactSolverInfo* %712 to %struct.btContactSolverInfoData*
  %m_solverMode871 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %713, i32 0, i32 16
  %714 = load i32, i32* %m_solverMode871, align 4
  %and872 = and i32 %714, 512
  %cmp873 = icmp eq i32 %and872, 0
  br i1 %cmp873, label %if.then874, label %if.end923

if.then874:                                       ; preds = %lor.lhs.false870, %for.end866
  store i32 0, i32* %j875, align 4
  br label %for.cond876

for.cond876:                                      ; preds = %for.inc920, %if.then874
  %715 = load i32, i32* %j875, align 4
  %716 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool877 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %716, i32 0, i32 5
  %call878 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool877)
  %cmp879 = icmp slt i32 %715, %call878
  br i1 %cmp879, label %for.body880, label %for.end922

for.body880:                                      ; preds = %for.cond876
  %717 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverContactRollingFrictionConstraintPool882 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %717, i32 0, i32 5
  %718 = load i32, i32* %j875, align 4
  %call883 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool882, i32 %718)
  store %struct.btSolverConstraint* %call883, %struct.btSolverConstraint** %constraint881, align 4
  %719 = load i32, i32* %iteration.addr, align 4
  %720 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %721 = bitcast %struct.btContactSolverInfo* %720 to %struct.btContactSolverInfoData*
  %m_numIterations884 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %721, i32 0, i32 5
  %722 = load i32, i32* %m_numIterations884, align 4
  %cmp885 = icmp slt i32 %719, %722
  br i1 %cmp885, label %if.then886, label %if.end919

if.then886:                                       ; preds = %for.body880
  %723 = load float, float* %beta656, align 4
  %m_pCRF888 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 5
  %724 = load i32, i32* %j875, align 4
  %call889 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCRF888, i32 %724)
  %725 = load float, float* %call889, align 4
  %mul890 = fmul float %723, %725
  store float %mul890, float* %additionaldeltaimpulse887, align 4
  %726 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint881, align 4
  %m_appliedImpulse891 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %726, i32 0, i32 7
  %727 = load float, float* %m_appliedImpulse891, align 4
  %728 = load float, float* %additionaldeltaimpulse887, align 4
  %add892 = fadd float %727, %728
  %729 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint881, align 4
  %m_appliedImpulse893 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %729, i32 0, i32 7
  store float %add892, float* %m_appliedImpulse893, align 4
  %730 = load float, float* %beta656, align 4
  %m_pCRF894 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 5
  %731 = load i32, i32* %j875, align 4
  %call895 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCRF894, i32 %731)
  %732 = load float, float* %call895, align 4
  %mul896 = fmul float %730, %732
  %m_deltafCRF897 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 9
  %733 = load i32, i32* %j875, align 4
  %call898 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_deltafCRF897, i32 %733)
  %734 = load float, float* %call898, align 4
  %add899 = fadd float %mul896, %734
  %m_pCRF900 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 5
  %735 = load i32, i32* %j875, align 4
  %call901 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %m_pCRF900, i32 %735)
  store float %add899, float* %call901, align 4
  %736 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool903 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %736, i32 0, i32 1
  %737 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint881, align 4
  %m_solverBodyIdA904 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %737, i32 0, i32 18
  %738 = load i32, i32* %m_solverBodyIdA904, align 4
  %call905 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool903, i32 %738)
  store %struct.btSolverBody* %call905, %struct.btSolverBody** %body1902, align 4
  %739 = bitcast %class.btNNCGConstraintSolver* %this5 to %class.btSequentialImpulseConstraintSolver*
  %m_tmpSolverBodyPool907 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %739, i32 0, i32 1
  %740 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint881, align 4
  %m_solverBodyIdB908 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %740, i32 0, i32 19
  %741 = load i32, i32* %m_solverBodyIdB908, align 4
  %call909 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool907, i32 %741)
  store %struct.btSolverBody* %call909, %struct.btSolverBody** %body2906, align 4
  %742 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint881, align 4
  store %struct.btSolverConstraint* %742, %struct.btSolverConstraint** %c910, align 4
  %743 = load %struct.btSolverBody*, %struct.btSolverBody** %body1902, align 4
  %744 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c910, align 4
  %m_contactNormal1912 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %744, i32 0, i32 1
  %745 = load %struct.btSolverBody*, %struct.btSolverBody** %body1902, align 4
  %call913 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %745)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp911, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1912, %class.btVector3* nonnull align 4 dereferenceable(16) %call913)
  %746 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c910, align 4
  %m_angularComponentA914 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %746, i32 0, i32 4
  %747 = load float, float* %additionaldeltaimpulse887, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %743, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp911, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA914, float %747)
  %748 = load %struct.btSolverBody*, %struct.btSolverBody** %body2906, align 4
  %749 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c910, align 4
  %m_contactNormal2916 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %749, i32 0, i32 3
  %750 = load %struct.btSolverBody*, %struct.btSolverBody** %body2906, align 4
  %call917 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %750)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp915, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2916, %class.btVector3* nonnull align 4 dereferenceable(16) %call917)
  %751 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c910, align 4
  %m_angularComponentB918 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %751, i32 0, i32 5
  %752 = load float, float* %additionaldeltaimpulse887, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %748, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp915, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB918, float %752)
  br label %if.end919

if.end919:                                        ; preds = %if.then886, %for.body880
  br label %for.inc920

for.inc920:                                       ; preds = %if.end919
  %753 = load i32, i32* %j875, align 4
  %inc921 = add nsw i32 %753, 1
  store i32 %inc921, i32* %j875, align 4
  br label %for.cond876

for.end922:                                       ; preds = %for.cond876
  br label %if.end923

if.end923:                                        ; preds = %for.end922, %lor.lhs.false870
  br label %if.end924

if.end924:                                        ; preds = %if.end923, %if.end715
  br label %if.end925

if.end925:                                        ; preds = %if.end924, %if.end654
  %754 = load float, float* %deltaflengthsqr, align 4
  %m_deltafLengthSqrPrev926 = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this5, i32 0, i32 1
  store float %754, float* %m_deltafLengthSqrPrev926, align 4
  br label %if.end927

if.end927:                                        ; preds = %if.end925, %if.end592
  %755 = load float, float* %deltaflengthsqr, align 4
  ret float %755
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

declare i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 %1
  ret %struct.btSolverConstraint* %arrayidx
}

declare float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver*, %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverConstraint* nonnull align 4 dereferenceable(152)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

declare float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver*, %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverConstraint* nonnull align 4 dereferenceable(152)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

declare i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject* nonnull align 4 dereferenceable(324), float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

declare float @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver*, %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverConstraint* nonnull align 4 dereferenceable(152)) #3

declare float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver*, %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverBody* nonnull align 4 dereferenceable(244), %struct.btSolverConstraint* nonnull align 4 dereferenceable(152)) #3

; Function Attrs: noinline optnone
define hidden float @_ZN22btNNCGConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btNNCGConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  %m_pNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pNC, i32 0)
  %m_pC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pC, i32 0)
  %m_pCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 4
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pCF, i32 0)
  %m_pCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_pCRF, i32 0)
  %m_deltafNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 6
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafNC, i32 0)
  %m_deltafC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 7
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafC, i32 0)
  %m_deltafCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafCF, i32 0)
  %m_deltafCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 9
  call void @_ZN20btAlignedObjectArrayIfE18resizeNoInitializeEi(%class.btAlignedObjectArray.22* %m_deltafCRF, i32 0)
  %0 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodies.addr, align 4
  %3 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %3)
  ret float %call
}

declare float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btNNCGConstraintSolver* @_ZN22btNNCGConstraintSolverD2Ev(%class.btNNCGConstraintSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  %this1 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btNNCGConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTV22btNNCGConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_deltafCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 9
  %call = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_deltafCRF) #5
  %m_deltafCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 8
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_deltafCF) #5
  %m_deltafC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 7
  %call3 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_deltafC) #5
  %m_deltafNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 6
  %call4 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_deltafNC) #5
  %m_pCRF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 5
  %call5 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_pCRF) #5
  %m_pCF = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 4
  %call6 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_pCF) #5
  %m_pC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 3
  %call7 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_pC) #5
  %m_pNC = getelementptr inbounds %class.btNNCGConstraintSolver, %class.btNNCGConstraintSolver* %this1, i32 0, i32 2
  %call8 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* %m_pNC) #5
  %1 = bitcast %class.btNNCGConstraintSolver* %this1 to %class.btSequentialImpulseConstraintSolver*
  %call9 = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* %1) #5
  ret %class.btNNCGConstraintSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btNNCGConstraintSolverD0Ev(%class.btNNCGConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  %this1 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  %call = call %class.btNNCGConstraintSolver* @_ZN22btNNCGConstraintSolverD2Ev(%class.btNNCGConstraintSolver* %this1) #5
  %0 = bitcast %class.btNNCGConstraintSolver* %this1 to i8*
  call void @_ZN22btNNCGConstraintSolverdlEPv(i8* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*, %class.btDispatcher*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %0, %class.btIDebugDraw* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

declare void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btNNCGConstraintSolver13getSolverTypeEv(%class.btNNCGConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNNCGConstraintSolver*, align 4
  store %class.btNNCGConstraintSolver* %this, %class.btNNCGConstraintSolver** %this.addr, align 4
  %this1 = load %class.btNNCGConstraintSolver*, %class.btNNCGConstraintSolver** %this.addr, align 4
  ret i32 4
}

declare void @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

declare void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

declare float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), %class.btIDebugDraw*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: nounwind
declare %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.23* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.23* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btNNCGConstraintSolverdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.22* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.22* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.23* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.22* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.23* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btNNCGConstraintSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
