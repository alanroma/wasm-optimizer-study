; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCompoundCompoundCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCompoundCompoundCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btCompoundCompoundCollisionAlgorithm = type { %class.btCompoundCollisionAlgorithm, %class.btHashedSimplePairCache*, %class.btAlignedObjectArray.15, i32, i32 }
%class.btCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btAlignedObjectArray, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.11, i8, %class.btPersistentManifold*, i8, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.11 = type <{ %class.btAlignedAllocator.12, [3 x i8], i32, i32, %class.btCollisionAlgorithm**, i8, [3 x i8] }>
%class.btAlignedAllocator.12 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.4, %union.anon.5, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.4 = type { float }
%union.anon.5 = type { float }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.6, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }
%class.btHashedSimplePairCache = type { i32 (...)**, %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20 }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, %struct.btSimplePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%struct.btSimplePair = type { i32, i32, %union.anon.18 }
%union.anon.18 = type { i8* }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.24, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.24 = type <{ %class.btAlignedAllocator.25, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.25 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.28 }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCompoundCompoundLeafCallback = type { %"struct.btDbvt::ICollide", i32, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btDispatcher*, %struct.btDispatcherInfo*, %class.btManifoldResult*, %class.btHashedSimplePairCache*, %class.btPersistentManifold* }
%"struct.btDbvt::ICollide" = type { i32 (...)** }
%class.CProfileSample = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK15btCompoundShape17getUpdateRevisionEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairED2Ev = comdat any

$_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairEixEi = comdat any

$_ZNK15btCompoundShape18getDynamicAabbTreeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE20initializeFromBufferEPvii = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK15btCompoundShape13getChildShapeEi = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK15btCompoundShape17getChildTransformEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_ = comdat any

$_ZN12btSimplePairC2Eii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv = comdat any

$_ZN30btCompoundCompoundLeafCallbackD2Ev = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN30btCompoundCompoundLeafCallbackD0Ev = comdat any

$_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_ = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZNK16btManifoldResult12getBody0WrapEv = comdat any

$_ZNK16btManifoldResult12getBody1WrapEv = comdat any

$_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev = comdat any

$_ZN6btDbvt6sStkNNC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE20initializeFromBufferEPvii = comdat any

$_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

$_ZNK12btDbvtAabbMm4MinsEv = comdat any

$_ZNK12btDbvtAabbMm4MaxsEv = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_Z9IntersectRK12btDbvtAabbMmS1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_ = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_ = comdat any

$_ZTV30btCompoundCompoundLeafCallback = comdat any

$_ZTS30btCompoundCompoundLeafCallback = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI30btCompoundCompoundLeafCallback = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gCompoundCompoundChildShapePairCallback = hidden global i1 (%class.btCollisionShape*, %class.btCollisionShape*)* null, align 4
@_ZTV36btCompoundCompoundCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI36btCompoundCompoundCollisionAlgorithm to i8*), i8* bitcast (%class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN36btCompoundCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btCompoundCompoundCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN36btCompoundCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*, %class.btAlignedObjectArray.1*)* @_ZN36btCompoundCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS36btCompoundCompoundCollisionAlgorithm = hidden constant [39 x i8] c"36btCompoundCompoundCollisionAlgorithm\00", align 1
@_ZTI28btCompoundCollisionAlgorithm = external constant i8*
@_ZTI36btCompoundCompoundCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btCompoundCompoundCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI28btCompoundCollisionAlgorithm to i8*) }, align 4
@_ZTV30btCompoundCompoundLeafCallback = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCompoundCompoundLeafCallback to i8*), i8* bitcast (%struct.btCompoundCompoundLeafCallback* (%struct.btCompoundCompoundLeafCallback*)* @_ZN30btCompoundCompoundLeafCallbackD2Ev to i8*), i8* bitcast (void (%struct.btCompoundCompoundLeafCallback*)* @_ZN30btCompoundCompoundLeafCallbackD0Ev to i8*), i8* bitcast (void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS30btCompoundCompoundLeafCallback = linkonce_odr hidden constant [33 x i8] c"30btCompoundCompoundLeafCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI30btCompoundCompoundLeafCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCompoundCompoundLeafCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@.str = private unnamed_addr constant [40 x i8] c"btCompoundCompoundLeafCallback::Process\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCompoundCompoundCollisionAlgorithm.cpp, i8* null }]

@_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b = hidden unnamed_addr alias %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN36btCompoundCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b
@_ZN36btCompoundCompoundCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*), %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  %ptr = alloca i8*, align 4
  %col0ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCompoundCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %4 = load i8, i8* %isSwapped.addr, align 1
  %tobool = trunc i8 %4 to i1
  %call = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3, i1 zeroext %tobool)
  %5 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV36btCompoundCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray.15* %m_removePairs)
  %call3 = call i8* @_Z22btAlignedAllocInternalmi(i32 64, i32 16)
  store i8* %call3, i8** %ptr, align 4
  %6 = load i8*, i8** %ptr, align 4
  %7 = bitcast i8* %6 to %class.btHashedSimplePairCache*
  %call4 = call %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheC1Ev(%class.btHashedSimplePairCache* %7)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btHashedSimplePairCache* %7, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %9, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %call5 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %10)
  %11 = bitcast %class.btCollisionShape* %call5 to %class.btCompoundShape*
  store %class.btCompoundShape* %11, %class.btCompoundShape** %compoundShape0, align 4
  %12 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %call6 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %12)
  %m_compoundShapeRevision0 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  store i32 %call6, i32* %m_compoundShapeRevision0, align 4
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %call7 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %13)
  %14 = bitcast %class.btCollisionShape* %call7 to %class.btCompoundShape*
  store %class.btCompoundShape* %14, %class.btCompoundShape** %compoundShape1, align 4
  %15 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %call8 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %15)
  %m_compoundShapeRevision1 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  store i32 %call8, i32* %m_compoundShapeRevision1, align 4
  ret %class.btCompoundCompoundCollisionAlgorithm* %this1
}

declare %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray.15* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.16* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator.16* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray.15* %this1)
  ret %class.btAlignedObjectArray.15* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

declare %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheC1Ev(%class.btHashedSimplePairCache* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmD2Ev(%class.btCompoundCompoundCollisionAlgorithm* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV36btCompoundCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  call void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this1)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %2 = bitcast %class.btHashedSimplePairCache* %1 to %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)***
  %vtable = load %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)**, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)** %vtable, i64 0
  %3 = load %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)** %vfn, align 4
  %call = call %class.btHashedSimplePairCache* %3(%class.btHashedSimplePairCache* %1) #8
  %m_childCollisionAlgorithmCache2 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %4 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache2, align 4
  %5 = bitcast %class.btHashedSimplePairCache* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray.15* %m_removePairs) #8
  %6 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCompoundCollisionAlgorithm*
  %call4 = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD2Ev(%class.btCompoundCollisionAlgorithm* %6) #8
  ret %class.btCompoundCompoundCollisionAlgorithm* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this) #2 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %pairs = alloca %class.btAlignedObjectArray.15*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %0 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.15* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %0)
  store %class.btAlignedObjectArray.15* %call, %class.btAlignedObjectArray.15** %pairs, align 4
  %1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %1)
  store i32 %call2, i32* %numChildren, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %numChildren, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %4, i32 %5)
  %6 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call3, i32 0, i32 2
  %m_userPointer = bitcast %union.anon.18* %6 to i8**
  %7 = load i8*, i8** %m_userPointer, align 4
  %tobool = icmp ne i8* %7, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %9 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %8, i32 %9)
  %10 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call4, i32 0, i32 2
  %m_userPointer5 = bitcast %union.anon.18* %10 to i8**
  %11 = load i8*, i8** %m_userPointer5, align 4
  %12 = bitcast i8* %11 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %12, %class.btCollisionAlgorithm** %algo, align 4
  %13 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4
  %14 = bitcast %class.btCollisionAlgorithm* %13 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %14, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %15 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call6 = call %class.btCollisionAlgorithm* %15(%class.btCollisionAlgorithm* %13) #8
  %16 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %16, i32 0, i32 1
  %17 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %18 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4
  %19 = bitcast %class.btCollisionAlgorithm* %18 to i8*
  %20 = bitcast %class.btDispatcher* %17 to void (%class.btDispatcher*, i8*)***
  %vtable7 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %20, align 4
  %vfn8 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable7, i64 15
  %21 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn8, align 4
  call void %21(%class.btDispatcher* %17, i8* %19)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_childCollisionAlgorithmCache9 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %23 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache9, align 4
  call void @_ZN23btHashedSimplePairCache14removeAllPairsEv(%class.btHashedSimplePairCache* %23)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.15* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray.15* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray.15* %this1)
  ret %class.btAlignedObjectArray.15* %this1
}

; Function Attrs: nounwind
declare %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmD2Ev(%class.btCompoundCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN36btCompoundCompoundCollisionAlgorithmD0Ev(%class.btCompoundCompoundCollisionAlgorithm* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmD1Ev(%class.btCompoundCompoundCollisionAlgorithm* %this1) #8
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btCompoundCompoundCollisionAlgorithm* %this, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %i = alloca i32, align 4
  %pairs = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.1* %manifoldArray, %class.btAlignedObjectArray.1** %manifoldArray.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %0 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.15* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %0)
  store %class.btAlignedObjectArray.15* %call, %class.btAlignedObjectArray.15** %pairs, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %2)
  %cmp = icmp slt i32 %1, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %4 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %3, i32 %4)
  %5 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call3, i32 0, i32 2
  %m_userPointer = bitcast %union.anon.18* %5 to i8**
  %6 = load i8*, i8** %m_userPointer, align 4
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %7 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %8 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %7, i32 %8)
  %9 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call4, i32 0, i32 2
  %m_userPointer5 = bitcast %union.anon.18* %9 to i8**
  %10 = load i8*, i8** %m_userPointer5, align 4
  %11 = bitcast i8* %10 to %class.btCollisionAlgorithm*
  %12 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %manifoldArray.addr, align 4
  %13 = bitcast %class.btCollisionAlgorithm* %11 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*** %13, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vtable, i64 4
  %14 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vfn, align 4
  call void %14(%class.btCollisionAlgorithm* %11, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.15* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.15* %m_overlappingPairArray
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %0, i32 %1
  ret %struct.btSimplePair* %arrayidx
}

declare void @_ZN23btHashedSimplePairCache14removeAllPairsEv(%class.btHashedSimplePairCache*) #3

; Function Attrs: noinline optnone
define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %col0ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  %tree0 = alloca %struct.btDbvt*, align 4
  %tree1 = alloca %struct.btDbvt*, align 4
  %i = alloca i32, align 4
  %manifoldArray = alloca %class.btAlignedObjectArray.1, align 4
  %localManifolds = alloca [4 x %class.btPersistentManifold], align 16
  %pairs = alloca %class.btAlignedObjectArray.15*, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  %m = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %callback = alloca %struct.btCompoundCompoundLeafCallback, align 4
  %xform = alloca %class.btTransform, align 4
  %ref.tmp43 = alloca %class.btTransform, align 4
  %pairs47 = alloca %class.btAlignedObjectArray.15*, align 4
  %i50 = alloca i32, align 4
  %manifoldArray51 = alloca %class.btAlignedObjectArray.1, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %algo65 = alloca %class.btCollisionAlgorithm*, align 4
  %orgTrans0 = alloca %class.btTransform, align 4
  %childShape0 = alloca %class.btCollisionShape*, align 4
  %newChildWorldTrans0 = alloca %class.btTransform, align 4
  %orgInterpolationTrans0 = alloca %class.btTransform, align 4
  %childTrans0 = alloca %class.btTransform*, align 4
  %ref.tmp80 = alloca %class.btTransform, align 4
  %thresholdVec = alloca %class.btVector3, align 4
  %orgInterpolationTrans1 = alloca %class.btTransform, align 4
  %childShape1 = alloca %class.btCollisionShape*, align 4
  %orgTrans1 = alloca %class.btTransform, align 4
  %newChildWorldTrans1 = alloca %class.btTransform, align 4
  %childTrans1 = alloca %class.btTransform*, align 4
  %ref.tmp102 = alloca %class.btTransform, align 4
  %ref.tmp116 = alloca %struct.btSimplePair, align 4
  %i127 = alloca i32, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %2)
  %3 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %3, %class.btCompoundShape** %compoundShape0, align 4
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %call2 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %4)
  %5 = bitcast %class.btCollisionShape* %call2 to %class.btCompoundShape*
  store %class.btCompoundShape* %5, %class.btCompoundShape** %compoundShape1, align 4
  %6 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %call3 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %6)
  store %struct.btDbvt* %call3, %struct.btDbvt** %tree0, align 4
  %7 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %call4 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %7)
  store %struct.btDbvt* %call4, %struct.btDbvt** %tree1, align 4
  %8 = load %struct.btDbvt*, %struct.btDbvt** %tree0, align 4
  %tobool = icmp ne %struct.btDbvt* %8, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %9 = load %struct.btDbvt*, %struct.btDbvt** %tree1, align 4
  %tobool5 = icmp ne %struct.btDbvt* %9, null
  br i1 %tobool5, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  %10 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCompoundCollisionAlgorithm*
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %13 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %14 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm* %10, %struct.btCollisionObjectWrapper* %11, %struct.btCollisionObjectWrapper* %12, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %13, %class.btManifoldResult* %14)
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %15 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %call6 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %15)
  %m_compoundShapeRevision0 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %16 = load i32, i32* %m_compoundShapeRevision0, align 4
  %cmp = icmp ne i32 %call6, %16
  br i1 %cmp, label %if.then10, label %lor.lhs.false7

lor.lhs.false7:                                   ; preds = %if.end
  %17 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %call8 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %17)
  %m_compoundShapeRevision1 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  %18 = load i32, i32* %m_compoundShapeRevision1, align 4
  %cmp9 = icmp ne i32 %call8, %18
  br i1 %cmp9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %lor.lhs.false7, %if.end
  call void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this1)
  %19 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %call11 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %19)
  %m_compoundShapeRevision012 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  store i32 %call11, i32* %m_compoundShapeRevision012, align 4
  %20 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %call13 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %20)
  %m_compoundShapeRevision114 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  store i32 %call13, i32* %m_compoundShapeRevision114, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then10, %lor.lhs.false7
  %call16 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.1* %manifoldArray)
  %array.begin = getelementptr inbounds [4 x %class.btPersistentManifold], [4 x %class.btPersistentManifold]* %localManifolds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.end15
  %arrayctor.cur = phi %class.btPersistentManifold* [ %array.begin, %if.end15 ], [ %arrayctor.next, %arrayctor.loop ]
  %call17 = call %class.btPersistentManifold* @_ZN20btPersistentManifoldC1Ev(%class.btPersistentManifold* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btPersistentManifold* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %21 = bitcast [4 x %class.btPersistentManifold]* %localManifolds to i8*
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE20initializeFromBufferEPvii(%class.btAlignedObjectArray.1* %manifoldArray, i8* %21, i32 0, i32 4)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %22 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %call18 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.15* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %22)
  store %class.btAlignedObjectArray.15* %call18, %class.btAlignedObjectArray.15** %pairs, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc37, %arrayctor.cont
  %23 = load i32, i32* %i, align 4
  %24 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %call19 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %24)
  %cmp20 = icmp slt i32 %23, %call19
  br i1 %cmp20, label %for.body, label %for.end39

for.body:                                         ; preds = %for.cond
  %25 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %26 = load i32, i32* %i, align 4
  %call21 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %25, i32 %26)
  %27 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call21, i32 0, i32 2
  %m_userPointer = bitcast %union.anon.18* %27 to i8**
  %28 = load i8*, i8** %m_userPointer, align 4
  %tobool22 = icmp ne i8* %28, null
  br i1 %tobool22, label %if.then23, label %if.end36

if.then23:                                        ; preds = %for.body
  %29 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs, align 4
  %30 = load i32, i32* %i, align 4
  %call24 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %29, i32 %30)
  %31 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call24, i32 0, i32 2
  %m_userPointer25 = bitcast %union.anon.18* %31 to i8**
  %32 = load i8*, i8** %m_userPointer25, align 4
  %33 = bitcast i8* %32 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %33, %class.btCollisionAlgorithm** %algo, align 4
  %34 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4
  %35 = bitcast %class.btCollisionAlgorithm* %34 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*** %35, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vtable, i64 4
  %36 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.1*)** %vfn, align 4
  call void %36(%class.btCollisionAlgorithm* %34, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %manifoldArray)
  store i32 0, i32* %m, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc, %if.then23
  %37 = load i32, i32* %m, align 4
  %call27 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %manifoldArray)
  %cmp28 = icmp slt i32 %37, %call27
  br i1 %cmp28, label %for.body29, label %for.end

for.body29:                                       ; preds = %for.cond26
  %38 = load i32, i32* %m, align 4
  %call30 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %manifoldArray, i32 %38)
  %39 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call30, align 4
  %call31 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %39)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.then33, label %if.end35

if.then33:                                        ; preds = %for.body29
  %40 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %41 = load i32, i32* %m, align 4
  %call34 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %manifoldArray, i32 %41)
  %42 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call34, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %40, %class.btPersistentManifold* %42)
  %43 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %43)
  %44 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %44, %class.btPersistentManifold* null)
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %for.body29
  br label %for.inc

for.inc:                                          ; preds = %if.end35
  %45 = load i32, i32* %m, align 4
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %m, align 4
  br label %for.cond26

for.end:                                          ; preds = %for.cond26
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end36

if.end36:                                         ; preds = %for.end, %for.body
  br label %for.inc37

for.inc37:                                        ; preds = %if.end36
  %46 = load i32, i32* %i, align 4
  %inc38 = add nsw i32 %46, 1
  store i32 %inc38, i32* %i, align 4
  br label %for.cond

for.end39:                                        ; preds = %for.cond
  %call40 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.1* %manifoldArray) #8
  %47 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %48 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %49 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %49, i32 0, i32 1
  %50 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %51 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %52 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_childCollisionAlgorithmCache41 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %53 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache41, align 4
  %54 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCompoundCollisionAlgorithm*
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCollisionAlgorithm, %class.btCompoundCollisionAlgorithm* %54, i32 0, i32 5
  %55 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4
  %call42 = call %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold(%struct.btCompoundCompoundLeafCallback* %callback, %struct.btCollisionObjectWrapper* %47, %struct.btCollisionObjectWrapper* %48, %class.btDispatcher* %50, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %51, %class.btManifoldResult* %52, %class.btHashedSimplePairCache* %53, %class.btPersistentManifold* %55)
  %56 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %call44 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %56)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp43, %class.btTransform* %call44)
  %57 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %call45 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %57)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %xform, %class.btTransform* %ref.tmp43, %class.btTransform* nonnull align 4 dereferenceable(64) %call45)
  %58 = load %struct.btDbvt*, %struct.btDbvt** %tree0, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %58, i32 0, i32 0
  %59 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %60 = load %struct.btDbvt*, %struct.btDbvt** %tree1, align 4
  %m_root46 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %60, i32 0, i32 0
  %61 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root46, align 4
  %62 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %62, i32 0, i32 8
  %63 = load float, float* %m_closestPointDistanceThreshold, align 4
  call void @_ZL11MycollideTTPK10btDbvtNodeS1_RK11btTransformP30btCompoundCompoundLeafCallbackf(%struct.btDbvtNode* %59, %struct.btDbvtNode* %61, %class.btTransform* nonnull align 4 dereferenceable(64) %xform, %struct.btCompoundCompoundLeafCallback* %callback, float %63)
  %m_childCollisionAlgorithmCache48 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %64 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache48, align 4
  %call49 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.15* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %64)
  store %class.btAlignedObjectArray.15* %call49, %class.btAlignedObjectArray.15** %pairs47, align 4
  %call52 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.1* %manifoldArray51)
  %call53 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %call54 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %call55 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %call56 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  store i32 0, i32* %i50, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc124, %for.end39
  %65 = load i32, i32* %i50, align 4
  %66 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %66)
  %cmp59 = icmp slt i32 %65, %call58
  br i1 %cmp59, label %for.body60, label %for.end126

for.body60:                                       ; preds = %for.cond57
  %67 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %68 = load i32, i32* %i50, align 4
  %call61 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %67, i32 %68)
  %69 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call61, i32 0, i32 2
  %m_userPointer62 = bitcast %union.anon.18* %69 to i8**
  %70 = load i8*, i8** %m_userPointer62, align 4
  %tobool63 = icmp ne i8* %70, null
  br i1 %tobool63, label %if.then64, label %if.end123

if.then64:                                        ; preds = %for.body60
  %71 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %72 = load i32, i32* %i50, align 4
  %call66 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %71, i32 %72)
  %73 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call66, i32 0, i32 2
  %m_userPointer67 = bitcast %union.anon.18* %73 to i8**
  %74 = load i8*, i8** %m_userPointer67, align 4
  %75 = bitcast i8* %74 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %75, %class.btCollisionAlgorithm** %algo65, align 4
  %call68 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans0)
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape0, align 4
  %call69 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans0)
  %call70 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgInterpolationTrans0)
  %76 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %77 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %78 = load i32, i32* %i50, align 4
  %call71 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %77, i32 %78)
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call71, i32 0, i32 0
  %79 = load i32, i32* %m_indexA, align 4
  %call72 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %76, i32 %79)
  store %class.btCollisionShape* %call72, %class.btCollisionShape** %childShape0, align 4
  %80 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %call73 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %80)
  %call74 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call73)
  %81 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4
  %call75 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %81)
  %call76 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgInterpolationTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call75)
  %82 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %83 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %84 = load i32, i32* %i50, align 4
  %call77 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %83, i32 %84)
  %m_indexA78 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call77, i32 0, i32 0
  %85 = load i32, i32* %m_indexA78, align 4
  %call79 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %82, i32 %85)
  store %class.btTransform* %call79, %class.btTransform** %childTrans0, align 4
  %86 = load %class.btTransform*, %class.btTransform** %childTrans0, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp80, %class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %86)
  %call81 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp80)
  %87 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4
  %88 = bitcast %class.btCollisionShape* %87 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable82 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %88, align 4
  %vfn83 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable82, i64 2
  %89 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn83, align 4
  call void %89(%class.btCollisionShape* %87, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %90 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold84 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %90, i32 0, i32 8
  %91 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold85 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %91, i32 0, i32 8
  %92 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold86 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %92, i32 0, i32 8
  %call87 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %thresholdVec, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold84, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold85, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold86)
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %call90 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgInterpolationTrans1)
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape1, align 4
  %call91 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans1)
  %call92 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans1)
  %93 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %94 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %95 = load i32, i32* %i50, align 4
  %call93 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %94, i32 %95)
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call93, i32 0, i32 1
  %96 = load i32, i32* %m_indexB, align 4
  %call94 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %93, i32 %96)
  store %class.btCollisionShape* %call94, %class.btCollisionShape** %childShape1, align 4
  %97 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %call95 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %97)
  %call96 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call95)
  %98 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4
  %call97 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %98)
  %call98 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgInterpolationTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call97)
  %99 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %100 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %101 = load i32, i32* %i50, align 4
  %call99 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %100, i32 %101)
  %m_indexB100 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call99, i32 0, i32 1
  %102 = load i32, i32* %m_indexB100, align 4
  %call101 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %99, i32 %102)
  store %class.btTransform* %call101, %class.btTransform** %childTrans1, align 4
  %103 = load %class.btTransform*, %class.btTransform** %childTrans1, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp102, %class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %103)
  %call103 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp102)
  %104 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4
  %105 = bitcast %class.btCollisionShape* %104 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable104 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %105, align 4
  %vfn105 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable104, i64 2
  %106 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn105, align 4
  call void %106(%class.btCollisionShape* %104, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %call106 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %call107 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %call108 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call108, label %if.end122, label %if.then109

if.then109:                                       ; preds = %if.then64
  %107 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo65, align 4
  %108 = bitcast %class.btCollisionAlgorithm* %107 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable110 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %108, align 4
  %vfn111 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable110, i64 0
  %109 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn111, align 4
  %call112 = call %class.btCollisionAlgorithm* %109(%class.btCollisionAlgorithm* %107) #8
  %110 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher113 = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %110, i32 0, i32 1
  %111 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher113, align 4
  %112 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo65, align 4
  %113 = bitcast %class.btCollisionAlgorithm* %112 to i8*
  %114 = bitcast %class.btDispatcher* %111 to void (%class.btDispatcher*, i8*)***
  %vtable114 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %114, align 4
  %vfn115 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable114, i64 15
  %115 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn115, align 4
  call void %115(%class.btDispatcher* %111, i8* %113)
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %116 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %117 = load i32, i32* %i50, align 4
  %call117 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %116, i32 %117)
  %m_indexA118 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call117, i32 0, i32 0
  %118 = load i32, i32* %m_indexA118, align 4
  %119 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %pairs47, align 4
  %120 = load i32, i32* %i50, align 4
  %call119 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %119, i32 %120)
  %m_indexB120 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call119, i32 0, i32 1
  %121 = load i32, i32* %m_indexB120, align 4
  %call121 = call %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* %ref.tmp116, i32 %118, i32 %121)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_(%class.btAlignedObjectArray.15* %m_removePairs, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %ref.tmp116)
  br label %if.end122

if.end122:                                        ; preds = %if.then109, %if.then64
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %for.body60
  br label %for.inc124

for.inc124:                                       ; preds = %if.end123
  %122 = load i32, i32* %i50, align 4
  %inc125 = add nsw i32 %122, 1
  store i32 %inc125, i32* %i50, align 4
  br label %for.cond57

for.end126:                                       ; preds = %for.cond57
  store i32 0, i32* %i127, align 4
  br label %for.cond128

for.cond128:                                      ; preds = %for.inc143, %for.end126
  %123 = load i32, i32* %i127, align 4
  %m_removePairs129 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call130 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %m_removePairs129)
  %cmp131 = icmp slt i32 %123, %call130
  br i1 %cmp131, label %for.body132, label %for.end145

for.body132:                                      ; preds = %for.cond128
  %m_childCollisionAlgorithmCache133 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %124 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache133, align 4
  %m_removePairs134 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %125 = load i32, i32* %i127, align 4
  %call135 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %m_removePairs134, i32 %125)
  %m_indexA136 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call135, i32 0, i32 0
  %126 = load i32, i32* %m_indexA136, align 4
  %m_removePairs137 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %127 = load i32, i32* %i127, align 4
  %call138 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray.15* %m_removePairs137, i32 %127)
  %m_indexB139 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call138, i32 0, i32 1
  %128 = load i32, i32* %m_indexB139, align 4
  %129 = bitcast %class.btHashedSimplePairCache* %124 to i8* (%class.btHashedSimplePairCache*, i32, i32)***
  %vtable140 = load i8* (%class.btHashedSimplePairCache*, i32, i32)**, i8* (%class.btHashedSimplePairCache*, i32, i32)*** %129, align 4
  %vfn141 = getelementptr inbounds i8* (%class.btHashedSimplePairCache*, i32, i32)*, i8* (%class.btHashedSimplePairCache*, i32, i32)** %vtable140, i64 2
  %130 = load i8* (%class.btHashedSimplePairCache*, i32, i32)*, i8* (%class.btHashedSimplePairCache*, i32, i32)** %vfn141, align 4
  %call142 = call i8* %130(%class.btHashedSimplePairCache* %124, i32 %126, i32 %128)
  br label %for.inc143

for.inc143:                                       ; preds = %for.body132
  %131 = load i32, i32* %i127, align 4
  %inc144 = add nsw i32 %131, 1
  store i32 %inc144, i32* %i127, align 4
  br label %for.cond128

for.end145:                                       ; preds = %for.cond128
  %m_removePairs146 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray.15* %m_removePairs146)
  %call147 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.1* %manifoldArray51) #8
  %call148 = call %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackD2Ev(%struct.btCompoundCompoundLeafCallback* %callback) #8
  br label %return

return:                                           ; preds = %for.end145, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  ret %struct.btDbvt* %0
}

declare void @_ZN28btCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40), %class.btManifoldResult*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

declare %class.btPersistentManifold* @_ZN20btPersistentManifoldC1Ev(%class.btPersistentManifold* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE20initializeFromBufferEPvii(%class.btAlignedObjectArray.1* %this, i8* %buffer, i32 %size, i32 %capacity) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %capacity, i32* %capacity.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = bitcast i8* %0 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %1, %class.btPersistentManifold*** %m_data, align 4
  %2 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  %3 = load i32, i32* %capacity.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end20

if.end:                                           ; preds = %entry
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1
  %3 = load i8, i8* %isSwapped, align 1
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.then, %if.else, %if.then6
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold(%struct.btCompoundCompoundLeafCallback* returned %this, %struct.btCollisionObjectWrapper* %compound1ObjWrap, %struct.btCollisionObjectWrapper* %compound0ObjWrap, %class.btDispatcher* %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut, %class.btHashedSimplePairCache* %childAlgorithmsCache, %class.btPersistentManifold* %sharedManifold) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %compound1ObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %compound0ObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %childAlgorithmsCache.addr = alloca %class.btHashedSimplePairCache*, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %compound1ObjWrap, %struct.btCollisionObjectWrapper** %compound1ObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %compound0ObjWrap, %struct.btCollisionObjectWrapper** %compound0ObjWrap.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  store %class.btHashedSimplePairCache* %childAlgorithmsCache, %class.btHashedSimplePairCache** %childAlgorithmsCache.addr, align 4
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #8
  %1 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV30btCompoundCompoundLeafCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_numOverlapPairs = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 1
  store i32 0, i32* %m_numOverlapPairs, align 4
  %m_compound0ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compound1ObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap, align 4
  %m_compound1ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compound0ObjWrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap, align 4
  %m_dispatcher = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 4
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 5
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %struct.btDispatcherInfo* %5, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  %m_resultOut = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %6 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  store %class.btManifoldResult* %6, %class.btManifoldResult** %m_resultOut, align 4
  %m_childCollisionAlgorithmCache = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %7 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %childAlgorithmsCache.addr, align 4
  store %class.btHashedSimplePairCache* %7, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 8
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %m_sharedManifold, align 4
  ret %struct.btCompoundCompoundLeafCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL11MycollideTTPK10btDbvtNodeS1_RK11btTransformP30btCompoundCompoundLeafCallbackf(%struct.btDbvtNode* %root0, %struct.btDbvtNode* %root1, %class.btTransform* nonnull align 4 dereferenceable(64) %xform, %struct.btCompoundCompoundLeafCallback* %callback, float %distanceThreshold) #2 {
entry:
  %root0.addr = alloca %struct.btDbvtNode*, align 4
  %root1.addr = alloca %struct.btDbvtNode*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  %callback.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %distanceThreshold.addr = alloca float, align 4
  %depth = alloca i32, align 4
  %treshold = alloca i32, align 4
  %stkStack = alloca %class.btAlignedObjectArray.28, align 4
  %localStack = alloca [128 x %"struct.btDbvt::sStkNN"], align 16
  %ref.tmp = alloca %"struct.btDbvt::sStkNN", align 4
  %p = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp11 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp20 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp27 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp37 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp47 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp57 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp65 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp78 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp86 = alloca %"struct.btDbvt::sStkNN", align 4
  store %struct.btDbvtNode* %root0, %struct.btDbvtNode** %root0.addr, align 4
  store %struct.btDbvtNode* %root1, %struct.btDbvtNode** %root1.addr, align 4
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4
  store %struct.btCompoundCompoundLeafCallback* %callback, %struct.btCompoundCompoundLeafCallback** %callback.addr, align 4
  store float %distanceThreshold, float* %distanceThreshold.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end102

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4
  %tobool1 = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool1, label %if.then, label %if.end102

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %depth, align 4
  store i32 124, i32* %treshold, align 4
  %call = call %class.btAlignedObjectArray.28* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray.28* %stkStack)
  %array.begin = getelementptr inbounds [128 x %"struct.btDbvt::sStkNN"], [128 x %"struct.btDbvt::sStkNN"]* %localStack, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then
  %arrayctor.cur = phi %"struct.btDbvt::sStkNN"* [ %array.begin, %if.then ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.btDbvt::sStkNN"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast [128 x %"struct.btDbvt::sStkNN"]* %localStack to i8*
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE20initializeFromBufferEPvii(%class.btAlignedObjectArray.28* %stkStack, i8* %2, i32 128, i32 128)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4
  %call3 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp, %struct.btDbvtNode* %3, %struct.btDbvtNode* %4)
  %call4 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 0)
  %5 = bitcast %"struct.btDbvt::sStkNN"* %call4 to i8*
  %6 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 8, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %arrayctor.cont
  %7 = load i32, i32* %depth, align 4
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %depth, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %dec)
  %8 = bitcast %"struct.btDbvt::sStkNN"* %p to i8*
  %9 = bitcast %"struct.btDbvt::sStkNN"* %call5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 8, i1 false)
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 0
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b, align 4
  %volume6 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %11, i32 0, i32 0
  %12 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4
  %13 = load float, float* %distanceThreshold.addr, align 4
  %call7 = call zeroext i1 @_ZL11MyIntersectRK12btDbvtAabbMmS1_RK11btTransformf(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume6, %class.btTransform* nonnull align 4 dereferenceable(64) %12, float %13)
  br i1 %call7, label %if.then8, label %if.end99

if.then8:                                         ; preds = %do.body
  %14 = load i32, i32* %depth, align 4
  %15 = load i32, i32* %treshold, align 4
  %cmp = icmp sgt i32 %14, %15
  br i1 %cmp, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.then8
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %stkStack)
  %mul = mul nsw i32 %call10, 2
  %call12 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %ref.tmp11)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray.28* %stkStack, i32 %mul, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %ref.tmp11)
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %stkStack)
  %sub = sub nsw i32 %call13, 4
  store i32 %sub, i32* %treshold, align 4
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.then8
  %a14 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a14, align 4
  %call15 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %16)
  br i1 %call15, label %if.then16, label %if.else74

if.then16:                                        ; preds = %if.end
  %b17 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b17, align 4
  %call18 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %17)
  br i1 %call18, label %if.then19, label %if.else

if.then19:                                        ; preds = %if.then16
  %a21 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %18 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a21, align 4
  %19 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %18, i32 0, i32 2
  %childs = bitcast %union.anon.0* %19 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %b22 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b22, align 4
  %22 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %21, i32 0, i32 2
  %childs23 = bitcast %union.anon.0* %22 to [2 x %struct.btDbvtNode*]*
  %arrayidx24 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs23, i32 0, i32 0
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx24, align 4
  %call25 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp20, %struct.btDbvtNode* %20, %struct.btDbvtNode* %23)
  %24 = load i32, i32* %depth, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %depth, align 4
  %call26 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %24)
  %25 = bitcast %"struct.btDbvt::sStkNN"* %call26 to i8*
  %26 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 8, i1 false)
  %a28 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a28, align 4
  %28 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %27, i32 0, i32 2
  %childs29 = bitcast %union.anon.0* %28 to [2 x %struct.btDbvtNode*]*
  %arrayidx30 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs29, i32 0, i32 1
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx30, align 4
  %b31 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b31, align 4
  %31 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 2
  %childs32 = bitcast %union.anon.0* %31 to [2 x %struct.btDbvtNode*]*
  %arrayidx33 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs32, i32 0, i32 0
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx33, align 4
  %call34 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp27, %struct.btDbvtNode* %29, %struct.btDbvtNode* %32)
  %33 = load i32, i32* %depth, align 4
  %inc35 = add nsw i32 %33, 1
  store i32 %inc35, i32* %depth, align 4
  %call36 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %33)
  %34 = bitcast %"struct.btDbvt::sStkNN"* %call36 to i8*
  %35 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 8, i1 false)
  %a38 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %36 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a38, align 4
  %37 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %36, i32 0, i32 2
  %childs39 = bitcast %union.anon.0* %37 to [2 x %struct.btDbvtNode*]*
  %arrayidx40 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs39, i32 0, i32 0
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx40, align 4
  %b41 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b41, align 4
  %40 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %39, i32 0, i32 2
  %childs42 = bitcast %union.anon.0* %40 to [2 x %struct.btDbvtNode*]*
  %arrayidx43 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs42, i32 0, i32 1
  %41 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx43, align 4
  %call44 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp37, %struct.btDbvtNode* %38, %struct.btDbvtNode* %41)
  %42 = load i32, i32* %depth, align 4
  %inc45 = add nsw i32 %42, 1
  store i32 %inc45, i32* %depth, align 4
  %call46 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %42)
  %43 = bitcast %"struct.btDbvt::sStkNN"* %call46 to i8*
  %44 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 8, i1 false)
  %a48 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %45 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a48, align 4
  %46 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %45, i32 0, i32 2
  %childs49 = bitcast %union.anon.0* %46 to [2 x %struct.btDbvtNode*]*
  %arrayidx50 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs49, i32 0, i32 1
  %47 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx50, align 4
  %b51 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %48 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b51, align 4
  %49 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %48, i32 0, i32 2
  %childs52 = bitcast %union.anon.0* %49 to [2 x %struct.btDbvtNode*]*
  %arrayidx53 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs52, i32 0, i32 1
  %50 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx53, align 4
  %call54 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp47, %struct.btDbvtNode* %47, %struct.btDbvtNode* %50)
  %51 = load i32, i32* %depth, align 4
  %inc55 = add nsw i32 %51, 1
  store i32 %inc55, i32* %depth, align 4
  %call56 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %51)
  %52 = bitcast %"struct.btDbvt::sStkNN"* %call56 to i8*
  %53 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp47 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 8, i1 false)
  br label %if.end73

if.else:                                          ; preds = %if.then16
  %a58 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a58, align 4
  %55 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %54, i32 0, i32 2
  %childs59 = bitcast %union.anon.0* %55 to [2 x %struct.btDbvtNode*]*
  %arrayidx60 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs59, i32 0, i32 0
  %56 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx60, align 4
  %b61 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %57 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b61, align 4
  %call62 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp57, %struct.btDbvtNode* %56, %struct.btDbvtNode* %57)
  %58 = load i32, i32* %depth, align 4
  %inc63 = add nsw i32 %58, 1
  store i32 %inc63, i32* %depth, align 4
  %call64 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %58)
  %59 = bitcast %"struct.btDbvt::sStkNN"* %call64 to i8*
  %60 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 8, i1 false)
  %a66 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %61 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a66, align 4
  %62 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %61, i32 0, i32 2
  %childs67 = bitcast %union.anon.0* %62 to [2 x %struct.btDbvtNode*]*
  %arrayidx68 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs67, i32 0, i32 1
  %63 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx68, align 4
  %b69 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %64 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b69, align 4
  %call70 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp65, %struct.btDbvtNode* %63, %struct.btDbvtNode* %64)
  %65 = load i32, i32* %depth, align 4
  %inc71 = add nsw i32 %65, 1
  store i32 %inc71, i32* %depth, align 4
  %call72 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %65)
  %66 = bitcast %"struct.btDbvt::sStkNN"* %call72 to i8*
  %67 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %66, i8* align 4 %67, i32 8, i1 false)
  br label %if.end73

if.end73:                                         ; preds = %if.else, %if.then19
  br label %if.end98

if.else74:                                        ; preds = %if.end
  %b75 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %68 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b75, align 4
  %call76 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %68)
  br i1 %call76, label %if.then77, label %if.else94

if.then77:                                        ; preds = %if.else74
  %a79 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %69 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a79, align 4
  %b80 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %70 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b80, align 4
  %71 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %70, i32 0, i32 2
  %childs81 = bitcast %union.anon.0* %71 to [2 x %struct.btDbvtNode*]*
  %arrayidx82 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs81, i32 0, i32 0
  %72 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx82, align 4
  %call83 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp78, %struct.btDbvtNode* %69, %struct.btDbvtNode* %72)
  %73 = load i32, i32* %depth, align 4
  %inc84 = add nsw i32 %73, 1
  store i32 %inc84, i32* %depth, align 4
  %call85 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %73)
  %74 = bitcast %"struct.btDbvt::sStkNN"* %call85 to i8*
  %75 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp78 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 8, i1 false)
  %a87 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %76 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a87, align 4
  %b88 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %77 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b88, align 4
  %78 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %77, i32 0, i32 2
  %childs89 = bitcast %union.anon.0* %78 to [2 x %struct.btDbvtNode*]*
  %arrayidx90 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs89, i32 0, i32 1
  %79 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx90, align 4
  %call91 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp86, %struct.btDbvtNode* %76, %struct.btDbvtNode* %79)
  %80 = load i32, i32* %depth, align 4
  %inc92 = add nsw i32 %80, 1
  store i32 %inc92, i32* %depth, align 4
  %call93 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %stkStack, i32 %80)
  %81 = bitcast %"struct.btDbvt::sStkNN"* %call93 to i8*
  %82 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 8, i1 false)
  br label %if.end97

if.else94:                                        ; preds = %if.else74
  %83 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %callback.addr, align 4
  %a95 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %84 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a95, align 4
  %b96 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %85 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b96, align 4
  %86 = bitcast %struct.btCompoundCompoundLeafCallback* %83 to void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)***
  %vtable = load void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)**, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*** %86, align 4
  %vfn = getelementptr inbounds void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vtable, i64 2
  %87 = load void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vfn, align 4
  call void %87(%struct.btCompoundCompoundLeafCallback* %83, %struct.btDbvtNode* %84, %struct.btDbvtNode* %85)
  br label %if.end97

if.end97:                                         ; preds = %if.else94, %if.then77
  br label %if.end98

if.end98:                                         ; preds = %if.end97, %if.end73
  br label %if.end99

if.end99:                                         ; preds = %if.end98, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end99
  %88 = load i32, i32* %depth, align 4
  %tobool100 = icmp ne i32 %88, 0
  br i1 %tobool100, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %call101 = call %class.btAlignedObjectArray.28* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray.28* %stkStack) #8
  br label %if.end102

if.end102:                                        ; preds = %do.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.24* %m_children, i32 %0)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.24* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4
  store i8 1, i8* %overlap, align 1
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1
  %27 = load i8, i8* %overlap, align 1
  %tobool31 = trunc i8 %27 to i1
  ret i1 %tobool31
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_(%class.btAlignedObjectArray.15* %this, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %_Val.addr = alloca %struct.btSimplePair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store %struct.btSimplePair* %_Val, %struct.btSimplePair** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray.15* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray.15* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray.15* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %1 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %1, i32 %2
  %3 = bitcast %struct.btSimplePair* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btSimplePair*
  %5 = load %struct.btSimplePair*, %struct.btSimplePair** %_Val.addr, align 4
  %6 = bitcast %struct.btSimplePair* %4 to i8*
  %7 = bitcast %struct.btSimplePair* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 12, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* returned %this, i32 %indexA, i32 %indexB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btSimplePair*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  store %struct.btSimplePair* %this, %struct.btSimplePair** %this.addr, align 4
  store i32 %indexA, i32* %indexA.addr, align 4
  store i32 %indexB, i32* %indexB.addr, align 4
  %this1 = load %struct.btSimplePair*, %struct.btSimplePair** %this.addr, align 4
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 0
  %0 = load i32, i32* %indexA.addr, align 4
  store i32 %0, i32* %m_indexA, align 4
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 1
  %1 = load i32, i32* %indexB.addr, align 4
  store i32 %1, i32* %m_indexB, align 4
  %2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 2
  %m_userPointer = bitcast %union.anon.18* %2 to i8**
  store i8* null, i8** %m_userPointer, align 4
  ret %struct.btSimplePair* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray.15* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray.15* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray.15* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray.15* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackD2Ev(%struct.btCompoundCompoundLeafCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %0) #8
  ret %struct.btCompoundCompoundLeafCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN36btCompoundCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  ret float 0.000000e+00
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btCompoundCompoundLeafCallbackD0Ev(%struct.btCompoundCompoundLeafCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %call = call %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackD2Ev(%struct.btCompoundCompoundLeafCallback* %this1) #8
  %0 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_(%struct.btCompoundCompoundLeafCallback* %this, %struct.btDbvtNode* %leaf0, %struct.btDbvtNode* %leaf1) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %leaf0.addr = alloca %struct.btDbvtNode*, align 4
  %leaf1.addr = alloca %struct.btDbvtNode*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %childIndex0 = alloca i32, align 4
  %childIndex1 = alloca i32, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  %childShape0 = alloca %class.btCollisionShape*, align 4
  %childShape1 = alloca %class.btCollisionShape*, align 4
  %orgTrans0 = alloca %class.btTransform, align 4
  %childTrans0 = alloca %class.btTransform*, align 4
  %newChildWorldTrans0 = alloca %class.btTransform, align 4
  %orgTrans1 = alloca %class.btTransform, align 4
  %childTrans1 = alloca %class.btTransform*, align 4
  %newChildWorldTrans1 = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %thresholdVec = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %compoundWrap0 = alloca %struct.btCollisionObjectWrapper, align 4
  %compoundWrap1 = alloca %struct.btCollisionObjectWrapper, align 4
  %pair = alloca %struct.btSimplePair*, align 4
  %colAlgo = alloca %class.btCollisionAlgorithm*, align 4
  %tmpWrap0 = alloca %struct.btCollisionObjectWrapper*, align 4
  %tmpWrap1 = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  store %struct.btDbvtNode* %leaf0, %struct.btDbvtNode** %leaf0.addr, align 4
  store %struct.btDbvtNode* %leaf1, %struct.btDbvtNode** %leaf1.addr, align 4
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str, i32 0, i32 0))
  %m_numOverlapPairs = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numOverlapPairs, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_numOverlapPairs, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf0.addr, align 4
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %dataAsInt = bitcast %union.anon.0* %2 to i32*
  %3 = load i32, i32* %dataAsInt, align 4
  store i32 %3, i32* %childIndex0, align 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf1.addr, align 4
  %5 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %4, i32 0, i32 2
  %dataAsInt2 = bitcast %union.anon.0* %5 to i32*
  %6 = load i32, i32* %dataAsInt2, align 4
  store i32 %6, i32* %childIndex1, align 4
  %m_compound0ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap, align 4
  %call3 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %7)
  %8 = bitcast %class.btCollisionShape* %call3 to %class.btCompoundShape*
  store %class.btCompoundShape* %8, %class.btCompoundShape** %compoundShape0, align 4
  %m_compound1ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap, align 4
  %call4 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btCollisionShape* %call4 to %class.btCompoundShape*
  store %class.btCompoundShape* %10, %class.btCompoundShape** %compoundShape1, align 4
  %11 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %12 = load i32, i32* %childIndex0, align 4
  %call5 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %11, i32 %12)
  store %class.btCollisionShape* %call5, %class.btCollisionShape** %childShape0, align 4
  %13 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %14 = load i32, i32* %childIndex1, align 4
  %call6 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %13, i32 %14)
  store %class.btCollisionShape* %call6, %class.btCollisionShape** %childShape1, align 4
  %m_compound0ColObjWrap7 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap7, align 4
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %15)
  %call9 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call8)
  %16 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4
  %17 = load i32, i32* %childIndex0, align 4
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %16, i32 %17)
  store %class.btTransform* %call10, %class.btTransform** %childTrans0, align 4
  %18 = load %class.btTransform*, %class.btTransform** %childTrans0, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans0, %class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %18)
  %m_compound1ColObjWrap11 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap11, align 4
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %19)
  %call13 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  %20 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4
  %21 = load i32, i32* %childIndex1, align 4
  %call14 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %20, i32 %21)
  store %class.btTransform* %call14, %class.btTransform** %childTrans1, align 4
  %22 = load %class.btTransform*, %class.btTransform** %childTrans1, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans1, %class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %22)
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  %23 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4
  %24 = bitcast %class.btCollisionShape* %23 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %24, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %25 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %25(%class.btCollisionShape* %23, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %26 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4
  %27 = bitcast %class.btCollisionShape* %26 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable19 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %27, align 4
  %vfn20 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable19, i64 2
  %28 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn20, align 4
  call void %28(%class.btCollisionShape* %26, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %m_resultOut = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %29 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %29, i32 0, i32 8
  %m_resultOut21 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %30 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut21, align 4
  %m_closestPointDistanceThreshold22 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %30, i32 0, i32 8
  %m_resultOut23 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %31 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut23, align 4
  %m_closestPointDistanceThreshold24 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %31, i32 0, i32 8
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %thresholdVec, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold22, float* nonnull align 4 dereferenceable(4) %m_closestPointDistanceThreshold24)
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %thresholdVec)
  %32 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundCompoundChildShapePairCallback, align 4
  %tobool = icmp ne i1 (%class.btCollisionShape*, %class.btCollisionShape*)* %32, null
  br i1 %tobool, label %if.then, label %if.end30

if.then:                                          ; preds = %entry
  %33 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundCompoundChildShapePairCallback, align 4
  %34 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4
  %35 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4
  %call28 = call zeroext i1 %33(%class.btCollisionShape* %34, %class.btCollisionShape* %35)
  br i1 %call28, label %if.end, label %if.then29

if.then29:                                        ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  br label %if.end30

if.end30:                                         ; preds = %if.end, %entry
  %call31 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call31, label %if.then32, label %if.end79

if.then32:                                        ; preds = %if.end30
  %m_compound0ColObjWrap33 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %36 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap33, align 4
  %37 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4
  %m_compound0ColObjWrap34 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %38 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap34, align 4
  %call35 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %38)
  %39 = load i32, i32* %childIndex0, align 4
  %call36 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %36, %class.btCollisionShape* %37, %class.btCollisionObject* %call35, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, i32 -1, i32 %39)
  %m_compound1ColObjWrap37 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %40 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap37, align 4
  %41 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4
  %m_compound1ColObjWrap38 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %42 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap38, align 4
  %call39 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %42)
  %43 = load i32, i32* %childIndex1, align 4
  %call40 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap1, %struct.btCollisionObjectWrapper* %40, %class.btCollisionShape* %41, %class.btCollisionObject* %call39, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, i32 -1, i32 %43)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %44 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4
  %45 = load i32, i32* %childIndex0, align 4
  %46 = load i32, i32* %childIndex1, align 4
  %call41 = call %struct.btSimplePair* @_ZN23btHashedSimplePairCache8findPairEii(%class.btHashedSimplePairCache* %44, i32 %45, i32 %46)
  store %struct.btSimplePair* %call41, %struct.btSimplePair** %pair, align 4
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %colAlgo, align 4
  %m_resultOut42 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %47 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut42, align 4
  %m_closestPointDistanceThreshold43 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %47, i32 0, i32 8
  %48 = load float, float* %m_closestPointDistanceThreshold43, align 4
  %cmp = fcmp ogt float %48, 0.000000e+00
  br i1 %cmp, label %if.then44, label %if.else

if.then44:                                        ; preds = %if.then32
  %m_dispatcher = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 4
  %49 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %50 = bitcast %class.btDispatcher* %49 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable45 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %50, align 4
  %vfn46 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable45, i64 2
  %51 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn46, align 4
  %call47 = call %class.btCollisionAlgorithm* %51(%class.btDispatcher* %49, %struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %compoundWrap1, %class.btPersistentManifold* null, i32 2)
  store %class.btCollisionAlgorithm* %call47, %class.btCollisionAlgorithm** %colAlgo, align 4
  br label %if.end61

if.else:                                          ; preds = %if.then32
  %52 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4
  %tobool48 = icmp ne %struct.btSimplePair* %52, null
  br i1 %tobool48, label %if.then49, label %if.else50

if.then49:                                        ; preds = %if.else
  %53 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4
  %54 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %53, i32 0, i32 2
  %m_userPointer = bitcast %union.anon.18* %54 to i8**
  %55 = load i8*, i8** %m_userPointer, align 4
  %56 = bitcast i8* %55 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %56, %class.btCollisionAlgorithm** %colAlgo, align 4
  br label %if.end60

if.else50:                                        ; preds = %if.else
  %m_dispatcher51 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 4
  %57 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher51, align 4
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 8
  %58 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4
  %59 = bitcast %class.btDispatcher* %57 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable52 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %59, align 4
  %vfn53 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable52, i64 2
  %60 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn53, align 4
  %call54 = call %class.btCollisionAlgorithm* %60(%class.btDispatcher* %57, %struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %compoundWrap1, %class.btPersistentManifold* %58, i32 1)
  store %class.btCollisionAlgorithm* %call54, %class.btCollisionAlgorithm** %colAlgo, align 4
  %m_childCollisionAlgorithmCache55 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %61 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache55, align 4
  %62 = load i32, i32* %childIndex0, align 4
  %63 = load i32, i32* %childIndex1, align 4
  %64 = bitcast %class.btHashedSimplePairCache* %61 to %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)***
  %vtable56 = load %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)**, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*** %64, align 4
  %vfn57 = getelementptr inbounds %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)** %vtable56, i64 3
  %65 = load %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)** %vfn57, align 4
  %call58 = call %struct.btSimplePair* %65(%class.btHashedSimplePairCache* %61, i32 %62, i32 %63)
  store %struct.btSimplePair* %call58, %struct.btSimplePair** %pair, align 4
  %66 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4
  %67 = bitcast %class.btCollisionAlgorithm* %66 to i8*
  %68 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4
  %69 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %68, i32 0, i32 2
  %m_userPointer59 = bitcast %union.anon.18* %69 to i8**
  store i8* %67, i8** %m_userPointer59, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.else50, %if.then49
  br label %if.end61

if.end61:                                         ; preds = %if.end60, %if.then44
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4
  %m_resultOut62 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %70 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut62, align 4
  %call63 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %70)
  store %struct.btCollisionObjectWrapper* %call63, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4
  %m_resultOut64 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %71 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut64, align 4
  %call65 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %71)
  store %struct.btCollisionObjectWrapper* %call65, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4
  %m_resultOut66 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %72 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut66, align 4
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %72, %struct.btCollisionObjectWrapper* %compoundWrap0)
  %m_resultOut67 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %73 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut67, align 4
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %73, %struct.btCollisionObjectWrapper* %compoundWrap1)
  %m_resultOut68 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %74 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut68, align 4
  %75 = load i32, i32* %childIndex0, align 4
  %76 = bitcast %class.btManifoldResult* %74 to void (%class.btManifoldResult*, i32, i32)***
  %vtable69 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %76, align 4
  %vfn70 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable69, i64 2
  %77 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn70, align 4
  call void %77(%class.btManifoldResult* %74, i32 -1, i32 %75)
  %m_resultOut71 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %78 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut71, align 4
  %79 = load i32, i32* %childIndex1, align 4
  %80 = bitcast %class.btManifoldResult* %78 to void (%class.btManifoldResult*, i32, i32)***
  %vtable72 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %80, align 4
  %vfn73 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable72, i64 3
  %81 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn73, align 4
  call void %81(%class.btManifoldResult* %78, i32 -1, i32 %79)
  %82 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 5
  %83 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  %m_resultOut74 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %84 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut74, align 4
  %85 = bitcast %class.btCollisionAlgorithm* %82 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable75 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %85, align 4
  %vfn76 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable75, i64 2
  %86 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn76, align 4
  call void %86(%class.btCollisionAlgorithm* %82, %struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %compoundWrap1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %83, %class.btManifoldResult* %84)
  %m_resultOut77 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %87 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut77, align 4
  %88 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %87, %struct.btCollisionObjectWrapper* %88)
  %m_resultOut78 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %89 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut78, align 4
  %90 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %89, %struct.btCollisionObjectWrapper* %90)
  br label %if.end79

if.end79:                                         ; preds = %if.end61, %if.end30
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end79, %if.then29
  %call80 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store float %0, float* %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #8
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4
  store i32 %4, i32* %m_partId, align 4
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4
  store i32 %5, i32* %m_index, align 4
  ret %struct.btCollisionObjectWrapper* %this1
}

declare %struct.btSimplePair* @_ZN23btHashedSimplePairCache8findPairEii(%class.btHashedSimplePairCache*, i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj0Wrap) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj1Wrap) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  ret void
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.28* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray.28* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.29* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator.29* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.28* %this1)
  ret %class.btAlignedObjectArray.28* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE20initializeFromBufferEPvii(%class.btAlignedObjectArray.28* %this, i8* %buffer, i32 %size, i32 %capacity) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %capacity, i32* %capacity.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray.28* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = bitcast i8* %0 to %"struct.btDbvt::sStkNN"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* %1, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %2 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  %3 = load i32, i32* %capacity.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* returned %this, %struct.btDbvtNode* %na, %struct.btDbvtNode* %nb) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %na.addr = alloca %struct.btDbvtNode*, align 4
  %nb.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  store %struct.btDbvtNode* %na, %struct.btDbvtNode** %na.addr, align 4
  store %struct.btDbvtNode* %nb, %struct.btDbvtNode** %nb.addr, align 4
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %na.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %a, align 4
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %nb.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %b, align 4
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.28* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %0, i32 %1
  ret %"struct.btDbvt::sStkNN"* %arrayidx
}

; Function Attrs: noinline optnone
define internal zeroext i1 @_ZL11MyIntersectRK12btDbvtAabbMmS1_RK11btTransformf(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b, %class.btTransform* nonnull align 4 dereferenceable(64) %xform, float %distanceThreshold) #2 {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  %distanceThreshold.addr = alloca float, align 4
  %newmin = alloca %class.btVector3, align 4
  %newmax = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %newb = alloca %struct.btDbvtAabbMm, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4
  store float %distanceThreshold, float* %distanceThreshold.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newmin)
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newmax)
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %0)
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %1)
  %2 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, float 0.000000e+00, %class.btTransform* nonnull align 4 dereferenceable(64) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %newmin, %class.btVector3* nonnull align 4 dereferenceable(16) %newmax)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %newmin, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr, float* nonnull align 4 dereferenceable(4) %distanceThreshold.addr)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %newmax, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %newb, %class.btVector3* nonnull align 4 dereferenceable(16) %newmin, %class.btVector3* nonnull align 4 dereferenceable(16) %newmax)
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %call9 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %newb)
  ret i1 %call9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray.28* %this, i32 %newsize, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"struct.btDbvt::sStkNN"* %fillData, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %5 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray.28* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %14 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %14, i32 %15
  %16 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"struct.btDbvt::sStkNN"*
  %18 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4
  %19 = bitcast %"struct.btDbvt::sStkNN"* %17 to i8*
  %20 = bitcast %"struct.btDbvt::sStkNN"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 8, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.28* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray.28* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray.28* %this1)
  ret %class.btAlignedObjectArray.28* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.29* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator.29* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.29*, align 4
  store %class.btAlignedAllocator.29* %this, %class.btAlignedAllocator.29** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.29*, %class.btAlignedAllocator.29** %this.addr, align 4
  ret %class.btAlignedAllocator.29* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.28* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray.28* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.28* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.28* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.28* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.28* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.28* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %tobool = icmp ne %"struct.btDbvt::sStkNN"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %2 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.29* %m_allocator, %"struct.btDbvt::sStkNN"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.29* %this, %"struct.btDbvt::sStkNN"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.29*, align 4
  %ptr.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedAllocator.29* %this, %class.btAlignedAllocator.29** %this.addr, align 4
  store %"struct.btDbvt::sStkNN"* %ptr, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.29*, %class.btAlignedAllocator.29** %this.addr, align 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %1 = bitcast %"struct.btDbvt::sStkNN"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #2 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4
  store float 5.000000e-01, float* %ref.tmp, align 4
  %0 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  store float 5.000000e-01, float* %ref.tmp4, align 4
  %2 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %5 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %6 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %9 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  ret %class.btVector3* %mi
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  ret %class.btVector3* %mx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #2 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 1
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ole float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %5 = load float, float* %call3, align 4
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi4)
  %7 = load float, float* %call5, align 4
  %cmp6 = fcmp oge float %5, %7
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi8)
  %9 = load float, float* %call9, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx10)
  %11 = load float, float* %call11, align 4
  %cmp12 = fcmp ole float %9, %11
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true7
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %13 = load float, float* %call15, align 4
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 0
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi16)
  %15 = load float, float* %call17, align 4
  %cmp18 = fcmp oge float %13, %15
  br i1 %cmp18, label %land.lhs.true19, label %land.end

land.lhs.true19:                                  ; preds = %land.lhs.true13
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi20)
  %17 = load float, float* %call21, align 4
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4
  %cmp24 = fcmp ole float %17, %19
  br i1 %cmp24, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 0
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi27)
  %23 = load float, float* %call28, align 4
  %cmp29 = fcmp oge float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true19, %land.lhs.true13, %land.lhs.true7, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true19 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp29, %land.rhs ]
  ret i1 %24
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray.28* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray.28* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray.28* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btDbvt::sStkNN"*
  store %"struct.btDbvt::sStkNN"* %2, %"struct.btDbvt::sStkNN"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %this1)
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray.28* %this1, i32 0, i32 %call3, %"struct.btDbvt::sStkNN"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.28* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.28* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.28* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* %4, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray.28* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray.28* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.29* %m_allocator, i32 %1, %"struct.btDbvt::sStkNN"** null)
  %2 = bitcast %"struct.btDbvt::sStkNN"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray.28* %this, i32 %start, i32 %end, %"struct.btDbvt::sStkNN"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btDbvt::sStkNN"* %dest, %"struct.btDbvt::sStkNN"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %3, i32 %4
  %5 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btDbvt::sStkNN"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %7 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %7, i32 %8
  %9 = bitcast %"struct.btDbvt::sStkNN"* %6 to i8*
  %10 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.29* %this, i32 %n, %"struct.btDbvt::sStkNN"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.29*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btDbvt::sStkNN"**, align 4
  store %class.btAlignedAllocator.29* %this, %class.btAlignedAllocator.29** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btDbvt::sStkNN"** %hint, %"struct.btDbvt::sStkNN"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.29*, %class.btAlignedAllocator.29** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btDbvt::sStkNN"*
  ret %"struct.btDbvt::sStkNN"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.24* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.24*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.24* %this, %class.btAlignedObjectArray.24** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.24*, %class.btAlignedObjectArray.24** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.24, %class.btAlignedObjectArray.24* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.16* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator.16* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  ret %class.btAlignedAllocator.16* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray.15* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray.15* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray.15* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray.15* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btSimplePair*
  store %struct.btSimplePair* %2, %struct.btSimplePair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  %3 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray.15* %this1, i32 0, i32 %call3, %struct.btSimplePair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray.15* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray.15* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray.15* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %struct.btSimplePair* %4, %struct.btSimplePair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray.15* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray.15* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.16* %m_allocator, i32 %1, %struct.btSimplePair** null)
  %2 = bitcast %struct.btSimplePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray.15* %this, i32 %start, i32 %end, %struct.btSimplePair* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSimplePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btSimplePair* %dest, %struct.btSimplePair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btSimplePair*, %struct.btSimplePair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %3, i32 %4
  %5 = bitcast %struct.btSimplePair* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btSimplePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %7 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %7, i32 %8
  %9 = bitcast %struct.btSimplePair* %6 to i8*
  %10 = bitcast %struct.btSimplePair* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 12, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray.15* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %3 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray.15* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.15*, align 4
  store %class.btAlignedObjectArray.15* %this, %class.btAlignedObjectArray.15** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.15*, %class.btAlignedObjectArray.15** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4
  %tobool = icmp ne %struct.btSimplePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  %2 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.16* %m_allocator, %struct.btSimplePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.16* %this, i32 %n, %struct.btSimplePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSimplePair**, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btSimplePair** %hint, %struct.btSimplePair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 12, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSimplePair*
  ret %struct.btSimplePair* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.16* %this, %struct.btSimplePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.16*, align 4
  %ptr.addr = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedAllocator.16* %this, %class.btAlignedAllocator.16** %this.addr, align 4
  store %struct.btSimplePair* %ptr, %struct.btSimplePair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.16*, %class.btAlignedAllocator.16** %this.addr, align 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %ptr.addr, align 4
  %1 = bitcast %struct.btSimplePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCompoundCompoundCollisionAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
