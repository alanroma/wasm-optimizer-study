; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btContinuousConvexCollision = type { %class.btConvexCast, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btStaticPlaneShape* }
%class.btConvexCast = type { i32 (...)** }
%class.btStaticPlaneShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btVector3 }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexPenetrationDepthSolver = type opaque
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btPointCollector = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btVector3, %class.btVector3, float, i8, [3 x i8] }>
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btIDebugDraw = type { i32 (...)** }
%"struct.btConvexCast::CastResult" = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, float, %class.btIDebugDraw*, float }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN12btConvexCastC2Ev = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN17btGjkPairDetectorD2Ev = comdat any

$_ZNK18btStaticPlaneShape14getPlaneNormalEv = comdat any

$_ZNK18btStaticPlaneShape16getPlaneConstantEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN16btPointCollectorC2Ev = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZNK11btTransform12inverseTimesERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN16btPointCollectorD2Ev = comdat any

$_ZN27btContinuousConvexCollisionD2Ev = comdat any

$_ZN27btContinuousConvexCollisionD0Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_Z6btAcosf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN16btPointCollectorD0Ev = comdat any

$_ZN16btPointCollector20setShapeIdentifiersAEii = comdat any

$_ZN16btPointCollector20setShapeIdentifiersBEii = comdat any

$_ZN16btPointCollector15addContactPointERK9btVector3S2_f = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK11btMatrix3x314transposeTimesERKS_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZTV16btPointCollector = comdat any

$_ZTS16btPointCollector = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTI16btPointCollector = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV27btContinuousConvexCollision = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27btContinuousConvexCollision to i8*), i8* bitcast (%class.btContinuousConvexCollision* (%class.btContinuousConvexCollision*)* @_ZN27btContinuousConvexCollisionD2Ev to i8*), i8* bitcast (void (%class.btContinuousConvexCollision*)* @_ZN27btContinuousConvexCollisionD0Ev to i8*), i8* bitcast (i1 (%class.btContinuousConvexCollision*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %"struct.btConvexCast::CastResult"*)* @_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS27btContinuousConvexCollision = hidden constant [30 x i8] c"27btContinuousConvexCollision\00", align 1
@_ZTI12btConvexCast = external constant i8*
@_ZTI27btContinuousConvexCollision = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27btContinuousConvexCollision, i32 0, i32 0), i8* bitcast (i8** @_ZTI12btConvexCast to i8*) }, align 4
@_ZTV12btConvexCast = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTV16btPointCollector = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btPointCollector to i8*), i8* bitcast (%struct.btPointCollector* (%struct.btPointCollector*)* @_ZN16btPointCollectorD2Ev to i8*), i8* bitcast (void (%struct.btPointCollector*)* @_ZN16btPointCollectorD0Ev to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)* @_ZN16btPointCollector15addContactPointERK9btVector3S2_f to i8*)] }, comdat, align 4
@_ZTS16btPointCollector = linkonce_odr hidden constant [19 x i8] c"16btPointCollector\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTI16btPointCollector = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btPointCollector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, comdat, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btContinuousConvexCollision.cpp, i8* null }]

@_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %class.btContinuousConvexCollision* (%class.btContinuousConvexCollision*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*), %class.btContinuousConvexCollision* (%class.btContinuousConvexCollision*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*)* @_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
@_ZN27btContinuousConvexCollisionC1EPK13btConvexShapePK18btStaticPlaneShape = hidden unnamed_addr alias %class.btContinuousConvexCollision* (%class.btContinuousConvexCollision*, %class.btConvexShape*, %class.btStaticPlaneShape*), %class.btContinuousConvexCollision* (%class.btContinuousConvexCollision*, %class.btConvexShape*, %class.btStaticPlaneShape*)* @_ZN27btContinuousConvexCollisionC2EPK13btConvexShapePK18btStaticPlaneShape

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btContinuousConvexCollision* @_ZN27btContinuousConvexCollisionC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btContinuousConvexCollision* returned %this, %class.btConvexShape* %convexA, %class.btConvexShape* %convexB, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %convexB.addr = alloca %class.btConvexShape*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %penetrationDepthSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %convexB, %class.btConvexShape** %convexB.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %0 = bitcast %class.btContinuousConvexCollision* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* %0) #10
  %1 = bitcast %class.btContinuousConvexCollision* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV27btContinuousConvexCollision, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_simplexSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 1
  %2 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btVoronoiSimplexSolver* %2, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  %m_penetrationDepthSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 2
  %3 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %3, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %m_convexA = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %4 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %4, %class.btConvexShape** %m_convexA, align 4
  %m_convexB1 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %5 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  store %class.btConvexShape* %5, %class.btConvexShape** %m_convexB1, align 4
  %m_planeShape = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 5
  store %class.btStaticPlaneShape* null, %class.btStaticPlaneShape** %m_planeShape, align 4
  ret %class.btContinuousConvexCollision* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexCast*, align 4
  store %class.btConvexCast* %this, %class.btConvexCast** %this.addr, align 4
  %this1 = load %class.btConvexCast*, %class.btConvexCast** %this.addr, align 4
  %0 = bitcast %class.btConvexCast* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV12btConvexCast, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btConvexCast* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btContinuousConvexCollision* @_ZN27btContinuousConvexCollisionC2EPK13btConvexShapePK18btStaticPlaneShape(%class.btContinuousConvexCollision* returned %this, %class.btConvexShape* %convexA, %class.btStaticPlaneShape* %plane) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %plane.addr = alloca %class.btStaticPlaneShape*, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4
  store %class.btStaticPlaneShape* %plane, %class.btStaticPlaneShape** %plane.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %0 = bitcast %class.btContinuousConvexCollision* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* %0) #10
  %1 = bitcast %class.btContinuousConvexCollision* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV27btContinuousConvexCollision, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_simplexSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 1
  store %class.btVoronoiSimplexSolver* null, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  %m_penetrationDepthSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 2
  store %class.btConvexPenetrationDepthSolver* null, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %m_convexA = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %2 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %2, %class.btConvexShape** %m_convexA, align 4
  %m_convexB1 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  store %class.btConvexShape* null, %class.btConvexShape** %m_convexB1, align 4
  %m_planeShape = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 5
  %3 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %plane.addr, align 4
  store %class.btStaticPlaneShape* %3, %class.btStaticPlaneShape** %m_planeShape, align 4
  ret %class.btContinuousConvexCollision* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btContinuousConvexCollision20computeClosestPointsERK11btTransformS2_R16btPointCollector(%class.btContinuousConvexCollision* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %struct.btPointCollector* nonnull align 4 dereferenceable(41) %pointCollector) #2 {
entry:
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %pointCollector.addr = alloca %struct.btPointCollector*, align 4
  %gjk = alloca %class.btGjkPairDetector, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %convexShape = alloca %class.btConvexShape*, align 4
  %planeShape = alloca %class.btStaticPlaneShape*, align 4
  %planeNormal = alloca %class.btVector3*, align 4
  %planeConstant = alloca float*, align 4
  %convexWorldTransform = alloca %class.btTransform, align 4
  %convexInPlaneTrans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp23 = alloca %class.btTransform, align 4
  %planeInConvex = alloca %class.btTransform, align 4
  %ref.tmp26 = alloca %class.btTransform, align 4
  %ref.tmp27 = alloca %class.btTransform, align 4
  %vtx = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %vtxInPlane = alloca %class.btVector3, align 4
  %distance = alloca float, align 4
  %vtxInPlaneProjected = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %vtxInPlaneWorld = alloca %class.btVector3, align 4
  %normalOnSurfaceB = alloca %class.btVector3, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %struct.btPointCollector* %pointCollector, %struct.btPointCollector** %pointCollector.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %m_convexB1 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %0 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB1, align 4
  %tobool = icmp ne %class.btConvexShape* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_simplexSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 1
  %1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %1)
  %m_convexA = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %2 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA, align 4
  %m_convexB12 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %3 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB12, align 4
  %m_convexA3 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %4 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA3, align 4
  %5 = bitcast %class.btConvexShape* %4 to %class.btCollisionShape*
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %5)
  %m_convexB14 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %6 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB14, align 4
  %7 = bitcast %class.btConvexShape* %6 to %class.btCollisionShape*
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %7)
  %m_convexA6 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %8 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA6, align 4
  %9 = bitcast %class.btConvexShape* %8 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %9, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %10 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call7 = call float %10(%class.btConvexShape* %8)
  %m_convexB18 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %11 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB18, align 4
  %12 = bitcast %class.btConvexShape* %11 to float (%class.btConvexShape*)***
  %vtable9 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %12, align 4
  %vfn10 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable9, i64 12
  %13 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn10, align 4
  %call11 = call float %13(%class.btConvexShape* %11)
  %m_simplexSolver12 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 1
  %14 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver12, align 4
  %m_penetrationDepthSolver = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 2
  %15 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %call13 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %gjk, %class.btConvexShape* %2, %class.btConvexShape* %3, i32 %call, i32 %call5, float %call7, float %call11, %class.btVoronoiSimplexSolver* %14, %class.btConvexPenetrationDepthSolver* %15)
  %call14 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %16 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %16)
  %17 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %17)
  %18 = load %struct.btPointCollector*, %struct.btPointCollector** %pointCollector.addr, align 4
  %19 = bitcast %struct.btPointCollector* %18 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjk, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %19, %class.btIDebugDraw* null, i1 zeroext false)
  %call17 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* %gjk) #10
  br label %if.end

if.else:                                          ; preds = %entry
  %m_convexA18 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %20 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA18, align 4
  store %class.btConvexShape* %20, %class.btConvexShape** %convexShape, align 4
  %m_planeShape = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 5
  %21 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %m_planeShape, align 4
  store %class.btStaticPlaneShape* %21, %class.btStaticPlaneShape** %planeShape, align 4
  %22 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %planeShape, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK18btStaticPlaneShape14getPlaneNormalEv(%class.btStaticPlaneShape* %22)
  store %class.btVector3* %call19, %class.btVector3** %planeNormal, align 4
  %23 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %planeShape, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK18btStaticPlaneShape16getPlaneConstantEv(%class.btStaticPlaneShape* %23)
  store float* %call20, float** %planeConstant, align 4
  %24 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call21 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %convexWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %24)
  %call22 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %convexInPlaneTrans)
  %25 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp23, %class.btTransform* %25)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %ref.tmp23, %class.btTransform* nonnull align 4 dereferenceable(64) %convexWorldTransform)
  %call24 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %convexInPlaneTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %call25 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %planeInConvex)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp27, %class.btTransform* %convexWorldTransform)
  %26 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp26, %class.btTransform* %ref.tmp27, %class.btTransform* nonnull align 4 dereferenceable(64) %26)
  %call28 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %planeInConvex, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp26)
  %27 = load %class.btConvexShape*, %class.btConvexShape** %convexShape, align 4
  %call30 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %planeInConvex)
  %28 = load %class.btVector3*, %class.btVector3** %planeNormal, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %28)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp29, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %29 = bitcast %class.btConvexShape* %27 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable32 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %29, align 4
  %vfn33 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable32, i64 16
  %30 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn33, align 4
  call void %30(%class.btVector3* sret align 4 %vtx, %class.btConvexShape* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %vtxInPlane, %class.btTransform* %convexInPlaneTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  %31 = load %class.btVector3*, %class.btVector3** %planeNormal, align 4
  %call34 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %vtxInPlane)
  %32 = load float*, float** %planeConstant, align 4
  %33 = load float, float* %32, align 4
  %sub = fsub float %call34, %33
  store float %sub, float* %distance, align 4
  %34 = load %class.btVector3*, %class.btVector3** %planeNormal, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp35, float* nonnull align 4 dereferenceable(4) %distance, %class.btVector3* nonnull align 4 dereferenceable(16) %34)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vtxInPlaneProjected, %class.btVector3* nonnull align 4 dereferenceable(16) %vtxInPlane, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35)
  %35 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %vtxInPlaneWorld, %class.btTransform* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %vtxInPlaneProjected)
  %36 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %36)
  %37 = load %class.btVector3*, %class.btVector3** %planeNormal, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %normalOnSurfaceB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call36, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  %38 = load %struct.btPointCollector*, %struct.btPointCollector** %pointCollector.addr, align 4
  %39 = load float, float* %distance, align 4
  %40 = bitcast %struct.btPointCollector* %38 to void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)***
  %vtable37 = load void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)**, void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)*** %40, align 4
  %vfn38 = getelementptr inbounds void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)*, void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)** %vtable37, i64 4
  %41 = load void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)*, void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)** %vfn38, align 4
  call void %41(%struct.btPointCollector* %38, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnSurfaceB, %class.btVector3* nonnull align 4 dereferenceable(16) %vtxInPlaneWorld, float %39)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #10
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK18btStaticPlaneShape14getPlaneNormalEv(%class.btStaticPlaneShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStaticPlaneShape*, align 4
  store %class.btStaticPlaneShape* %this, %class.btStaticPlaneShape** %this.addr, align 4
  %this1 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %this.addr, align 4
  %m_planeNormal = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %this1, i32 0, i32 3
  ret %class.btVector3* %m_planeNormal
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK18btStaticPlaneShape16getPlaneConstantEv(%class.btStaticPlaneShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStaticPlaneShape*, align 4
  store %class.btStaticPlaneShape* %this, %class.btStaticPlaneShape** %this.addr, align 4
  %this1 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %this.addr, align 4
  %m_planeConstant = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %this1, i32 0, i32 4
  ret float* %m_planeConstant
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN27btContinuousConvexCollision16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btContinuousConvexCollision* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %fromA, %class.btTransform* nonnull align 4 dereferenceable(64) %toA, %class.btTransform* nonnull align 4 dereferenceable(64) %fromB, %class.btTransform* nonnull align 4 dereferenceable(64) %toB, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %result) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  %fromA.addr = alloca %class.btTransform*, align 4
  %toA.addr = alloca %class.btTransform*, align 4
  %fromB.addr = alloca %class.btTransform*, align 4
  %toB.addr = alloca %class.btTransform*, align 4
  %result.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %linVelA = alloca %class.btVector3, align 4
  %angVelA = alloca %class.btVector3, align 4
  %linVelB = alloca %class.btVector3, align 4
  %angVelB = alloca %class.btVector3, align 4
  %boundingRadiusA = alloca float, align 4
  %boundingRadiusB = alloca float, align 4
  %maxAngularProjectedVelocity = alloca float, align 4
  %relLinVel = alloca %class.btVector3, align 4
  %relLinVelocLength = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %lambda = alloca float, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %maxIter = alloca i32, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %hasResult = alloca i8, align 1
  %c = alloca %class.btVector3, align 4
  %lastLambda = alloca float, align 4
  %numIter = alloca i32, align 4
  %radius = alloca float, align 4
  %pointCollector1 = alloca %struct.btPointCollector, align 4
  %dist = alloca float, align 4
  %projectedLinearVelocity = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %dLambda = alloca float, align 4
  %interpolatedTransA = alloca %class.btTransform, align 4
  %interpolatedTransB = alloca %class.btTransform, align 4
  %relativeTrans = alloca %class.btTransform, align 4
  %ref.tmp65 = alloca %class.btTransform, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %ref.tmp73 = alloca float, align 4
  %ref.tmp74 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %pointCollector = alloca %struct.btPointCollector, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  store %class.btTransform* %fromA, %class.btTransform** %fromA.addr, align 4
  store %class.btTransform* %toA, %class.btTransform** %toA.addr, align 4
  store %class.btTransform* %fromB, %class.btTransform** %fromB.addr, align 4
  store %class.btTransform* %toB, %class.btTransform** %toB.addr, align 4
  store %"struct.btConvexCast::CastResult"* %result, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelA)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelA)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelB)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVelB)
  %0 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %toA.addr, align 4
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, float 1.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA)
  %2 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %toB.addr, align 4
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, float 1.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB)
  %m_convexA = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 3
  %4 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA, align 4
  %5 = bitcast %class.btConvexShape* %4 to %class.btCollisionShape*
  %6 = bitcast %class.btCollisionShape* %5 to float (%class.btCollisionShape*)***
  %vtable = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %6, align 4
  %vfn = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable, i64 4
  %7 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn, align 4
  %call5 = call float %7(%class.btCollisionShape* %5)
  store float %call5, float* %boundingRadiusA, align 4
  %m_convexB1 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %8 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB1, align 4
  %tobool = icmp ne %class.btConvexShape* %8, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_convexB16 = getelementptr inbounds %class.btContinuousConvexCollision, %class.btContinuousConvexCollision* %this1, i32 0, i32 4
  %9 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB16, align 4
  %10 = bitcast %class.btConvexShape* %9 to %class.btCollisionShape*
  %11 = bitcast %class.btCollisionShape* %10 to float (%class.btCollisionShape*)***
  %vtable7 = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %11, align 4
  %vfn8 = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable7, i64 4
  %12 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn8, align 4
  %call9 = call float %12(%class.btCollisionShape* %10)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %call9, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %boundingRadiusB, align 4
  %call10 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %angVelA)
  %13 = load float, float* %boundingRadiusA, align 4
  %mul = fmul float %call10, %13
  %call11 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %angVelB)
  %14 = load float, float* %boundingRadiusB, align 4
  %mul12 = fmul float %call11, %14
  %add = fadd float %mul, %mul12
  store float %add, float* %maxAngularProjectedVelocity, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relLinVel, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA)
  %call13 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp)
  store float %call13, float* %relLinVelocLength, align 4
  %15 = load float, float* %relLinVelocLength, align 4
  %16 = load float, float* %maxAngularProjectedVelocity, align 4
  %add14 = fadd float %15, %16
  %cmp = fcmp oeq float %add14, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %cond.end
  store float 0.000000e+00, float* %lambda, align 4
  store float 1.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %v, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  store i32 64, i32* %maxIter, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  store float 0.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  store float 0.000000e+00, float* %ref.tmp22, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %n, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  store i8 0, i8* %hasResult, align 1
  %call23 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %c)
  %17 = load float, float* %lambda, align 4
  store float %17, float* %lastLambda, align 4
  store i32 0, i32* %numIter, align 4
  store float 0x3F50624DE0000000, float* %radius, align 4
  %call24 = call %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* %pointCollector1)
  %18 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %19 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  call void @_ZN27btContinuousConvexCollision20computeClosestPointsERK11btTransformS2_R16btPointCollector(%class.btContinuousConvexCollision* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %18, %class.btTransform* nonnull align 4 dereferenceable(64) %19, %struct.btPointCollector* nonnull align 4 dereferenceable(41) %pointCollector1)
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector1, i32 0, i32 4
  %20 = load i8, i8* %m_hasResult, align 4
  %tobool25 = trunc i8 %20 to i1
  %frombool = zext i1 %tobool25 to i8
  store i8 %frombool, i8* %hasResult, align 1
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector1, i32 0, i32 2
  %21 = bitcast %class.btVector3* %c to i8*
  %22 = bitcast %class.btVector3* %m_pointInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %23 = load i8, i8* %hasResult, align 1
  %tobool26 = trunc i8 %23 to i1
  br i1 %tobool26, label %if.then27, label %if.end100

if.then27:                                        ; preds = %if.end
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector1, i32 0, i32 3
  %24 = load float, float* %m_distance, align 4
  %25 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_allowedPenetration = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %25, i32 0, i32 7
  %26 = load float, float* %m_allowedPenetration, align 4
  %add28 = fadd float %24, %26
  store float %add28, float* %dist, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector1, i32 0, i32 1
  %27 = bitcast %class.btVector3* %n to i8*
  %28 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %call29 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relLinVel, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  store float %call29, float* %projectedLinearVelocity, align 4
  %29 = load float, float* %projectedLinearVelocity, align 4
  %30 = load float, float* %maxAngularProjectedVelocity, align 4
  %add30 = fadd float %29, %30
  %cmp31 = fcmp ole float %add30, 0x3E80000000000000
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.then27
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end33:                                         ; preds = %if.then27
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %if.end33
  %31 = load float, float* %dist, align 4
  %32 = load float, float* %radius, align 4
  %cmp34 = fcmp ogt float %31, %32
  br i1 %cmp34, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_debugDrawer = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %33, i32 0, i32 6
  %34 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %tobool35 = icmp ne %class.btIDebugDraw* %34, null
  br i1 %tobool35, label %if.then36, label %if.end45

if.then36:                                        ; preds = %while.body
  %35 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_debugDrawer37 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %35, i32 0, i32 6
  %36 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer37, align 4
  store float 1.000000e+00, float* %ref.tmp39, align 4
  store float 1.000000e+00, float* %ref.tmp40, align 4
  store float 1.000000e+00, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %37 = bitcast %class.btIDebugDraw* %36 to void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)***
  %vtable43 = load void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*** %37, align 4
  %vfn44 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)** %vtable43, i64 7
  %38 = load void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)** %vfn44, align 4
  call void %38(%class.btIDebugDraw* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %c, float 0x3FC99999A0000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  br label %if.end45

if.end45:                                         ; preds = %if.then36, %while.body
  store float 0.000000e+00, float* %dLambda, align 4
  %call46 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relLinVel, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  store float %call46, float* %projectedLinearVelocity, align 4
  %39 = load float, float* %projectedLinearVelocity, align 4
  %40 = load float, float* %maxAngularProjectedVelocity, align 4
  %add47 = fadd float %39, %40
  %cmp48 = fcmp ole float %add47, 0x3E80000000000000
  br i1 %cmp48, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.end45
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end50:                                         ; preds = %if.end45
  %41 = load float, float* %dist, align 4
  %42 = load float, float* %projectedLinearVelocity, align 4
  %43 = load float, float* %maxAngularProjectedVelocity, align 4
  %add51 = fadd float %42, %43
  %div = fdiv float %41, %add51
  store float %div, float* %dLambda, align 4
  %44 = load float, float* %lambda, align 4
  %45 = load float, float* %dLambda, align 4
  %add52 = fadd float %44, %45
  store float %add52, float* %lambda, align 4
  %46 = load float, float* %lambda, align 4
  %cmp53 = fcmp ogt float %46, 1.000000e+00
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %if.end50
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end55:                                         ; preds = %if.end50
  %47 = load float, float* %lambda, align 4
  %cmp56 = fcmp olt float %47, 0.000000e+00
  br i1 %cmp56, label %if.then57, label %if.end58

if.then57:                                        ; preds = %if.end55
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end58:                                         ; preds = %if.end55
  %48 = load float, float* %lambda, align 4
  %49 = load float, float* %lastLambda, align 4
  %cmp59 = fcmp ole float %48, %49
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end58
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end61:                                         ; preds = %if.end58
  %50 = load float, float* %lambda, align 4
  store float %50, float* %lastLambda, align 4
  %call62 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %interpolatedTransA)
  %call63 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %interpolatedTransB)
  %call64 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %relativeTrans)
  %51 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %52 = load float, float* %lambda, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %51, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelA, float %52, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransA)
  %53 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  %54 = load float, float* %lambda, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %53, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, %class.btVector3* nonnull align 4 dereferenceable(16) %angVelB, float %54, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransB)
  call void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* sret align 4 %ref.tmp65, %class.btTransform* %interpolatedTransB, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransA)
  %call66 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %relativeTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp65)
  %55 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_debugDrawer67 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %55, i32 0, i32 6
  %56 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer67, align 4
  %tobool68 = icmp ne %class.btIDebugDraw* %56, null
  br i1 %tobool68, label %if.then69, label %if.end79

if.then69:                                        ; preds = %if.end61
  %57 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_debugDrawer70 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %57, i32 0, i32 6
  %58 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer70, align 4
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %interpolatedTransA)
  store float 1.000000e+00, float* %ref.tmp73, align 4
  store float 0.000000e+00, float* %ref.tmp74, align 4
  store float 0.000000e+00, float* %ref.tmp75, align 4
  %call76 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73, float* nonnull align 4 dereferenceable(4) %ref.tmp74, float* nonnull align 4 dereferenceable(4) %ref.tmp75)
  %59 = bitcast %class.btIDebugDraw* %58 to void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)***
  %vtable77 = load void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*** %59, align 4
  %vfn78 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)** %vtable77, i64 7
  %60 = load void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, float, %class.btVector3*)** %vfn78, align 4
  call void %60(%class.btIDebugDraw* %58, %class.btVector3* nonnull align 4 dereferenceable(16) %call71, float 0x3FC99999A0000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp72)
  br label %if.end79

if.end79:                                         ; preds = %if.then69, %if.end61
  %61 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %62 = load float, float* %lambda, align 4
  %63 = bitcast %"struct.btConvexCast::CastResult"* %61 to void (%"struct.btConvexCast::CastResult"*, float)***
  %vtable80 = load void (%"struct.btConvexCast::CastResult"*, float)**, void (%"struct.btConvexCast::CastResult"*, float)*** %63, align 4
  %vfn81 = getelementptr inbounds void (%"struct.btConvexCast::CastResult"*, float)*, void (%"struct.btConvexCast::CastResult"*, float)** %vtable80, i64 0
  %64 = load void (%"struct.btConvexCast::CastResult"*, float)*, void (%"struct.btConvexCast::CastResult"*, float)** %vfn81, align 4
  call void %64(%"struct.btConvexCast::CastResult"* %61, float %62)
  %call82 = call %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* %pointCollector)
  call void @_ZN27btContinuousConvexCollision20computeClosestPointsERK11btTransformS2_R16btPointCollector(%class.btContinuousConvexCollision* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %interpolatedTransB, %struct.btPointCollector* nonnull align 4 dereferenceable(41) %pointCollector)
  %m_hasResult83 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 4
  %65 = load i8, i8* %m_hasResult83, align 4
  %tobool84 = trunc i8 %65 to i1
  br i1 %tobool84, label %if.then85, label %if.else

if.then85:                                        ; preds = %if.end79
  %m_distance86 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 3
  %66 = load float, float* %m_distance86, align 4
  %67 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_allowedPenetration87 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %67, i32 0, i32 7
  %68 = load float, float* %m_allowedPenetration87, align 4
  %add88 = fadd float %66, %68
  store float %add88, float* %dist, align 4
  %m_pointInWorld89 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 2
  %69 = bitcast %class.btVector3* %c to i8*
  %70 = bitcast %class.btVector3* %m_pointInWorld89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 16, i1 false)
  %m_normalOnBInWorld90 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 1
  %71 = bitcast %class.btVector3* %n to i8*
  %72 = bitcast %class.btVector3* %m_normalOnBInWorld90 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 16, i1 false)
  br label %if.end93

if.else:                                          ; preds = %if.end79
  %73 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %74 = load i32, i32* %numIter, align 4
  %75 = bitcast %"struct.btConvexCast::CastResult"* %73 to void (%"struct.btConvexCast::CastResult"*, i32, i32)***
  %vtable91 = load void (%"struct.btConvexCast::CastResult"*, i32, i32)**, void (%"struct.btConvexCast::CastResult"*, i32, i32)*** %75, align 4
  %vfn92 = getelementptr inbounds void (%"struct.btConvexCast::CastResult"*, i32, i32)*, void (%"struct.btConvexCast::CastResult"*, i32, i32)** %vtable91, i64 2
  %76 = load void (%"struct.btConvexCast::CastResult"*, i32, i32)*, void (%"struct.btConvexCast::CastResult"*, i32, i32)** %vfn92, align 4
  call void %76(%"struct.btConvexCast::CastResult"* %73, i32 -1, i32 %74)
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end93:                                         ; preds = %if.then85
  %77 = load i32, i32* %numIter, align 4
  %inc = add nsw i32 %77, 1
  store i32 %inc, i32* %numIter, align 4
  %78 = load i32, i32* %numIter, align 4
  %79 = load i32, i32* %maxIter, align 4
  %cmp94 = icmp sgt i32 %78, %79
  br i1 %cmp94, label %if.then95, label %if.end98

if.then95:                                        ; preds = %if.end93
  %80 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %81 = load i32, i32* %numIter, align 4
  %82 = bitcast %"struct.btConvexCast::CastResult"* %80 to void (%"struct.btConvexCast::CastResult"*, i32, i32)***
  %vtable96 = load void (%"struct.btConvexCast::CastResult"*, i32, i32)**, void (%"struct.btConvexCast::CastResult"*, i32, i32)*** %82, align 4
  %vfn97 = getelementptr inbounds void (%"struct.btConvexCast::CastResult"*, i32, i32)*, void (%"struct.btConvexCast::CastResult"*, i32, i32)** %vtable96, i64 2
  %83 = load void (%"struct.btConvexCast::CastResult"*, i32, i32)*, void (%"struct.btConvexCast::CastResult"*, i32, i32)** %vfn97, align 4
  call void %83(%"struct.btConvexCast::CastResult"* %80, i32 -2, i32 %81)
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end98:                                         ; preds = %if.end93
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end98, %if.then95, %if.else
  %call99 = call %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* %pointCollector) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup101 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %84 = load float, float* %lambda, align 4
  %85 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %85, i32 0, i32 5
  store float %84, float* %m_fraction, align 4
  %86 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_normal = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %86, i32 0, i32 3
  %87 = bitcast %class.btVector3* %m_normal to i8*
  %88 = bitcast %class.btVector3* %n to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 16, i1 false)
  %89 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_hitPoint = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %89, i32 0, i32 4
  %90 = bitcast %class.btVector3* %m_hitPoint to i8*
  %91 = bitcast %class.btVector3* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %90, i8* align 4 %91, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

if.end100:                                        ; preds = %if.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup101

cleanup101:                                       ; preds = %if.end100, %while.end, %cleanup, %if.then60, %if.then57, %if.then54, %if.then49, %if.then32
  %call102 = call %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* %pointCollector1) #10
  br label %return

return:                                           ; preds = %cleanup101, %if.then
  %92 = load i1, i1* %retval, align 1
  ret i1 %92
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %2 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4
  %3 = bitcast %class.btVector3* %2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %5 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %6 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btTransform* nonnull align 4 dereferenceable(64) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %7 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = bitcast %struct.btPointCollector* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #10
  %1 = bitcast %struct.btPointCollector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btPointCollector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalOnBInWorld)
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pointInWorld)
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float 0x43ABC16D60000000, float* %m_distance, align 4
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 0, i8* %m_hasResult, align 4
  ret %struct.btPointCollector* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %3 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call3, float* %fAngle, align 4
  %4 = load float, float* %fAngle, align 4
  %5 = load float, float* %timeStep.addr, align 4
  %mul = fmul float %4, %5
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %timeStep.addr, align 4
  %div = fdiv float 0x3FE921FB60000000, %6
  store float %div, float* %fAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load float, float* %fAngle, align 4
  %cmp4 = fcmp olt float %7, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %8 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %9 = load float, float* %timeStep.addr, align 4
  %mul8 = fmul float 5.000000e-01, %9
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load float, float* %timeStep.addr, align 4
  %mul9 = fmul float %10, %11
  %12 = load float, float* %timeStep.addr, align 4
  %mul10 = fmul float %mul9, %12
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %13 = load float, float* %fAngle, align 4
  %mul12 = fmul float %mul11, %13
  %14 = load float, float* %fAngle, align 4
  %mul13 = fmul float %mul12, %14
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %8, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast %class.btVector3* %axis to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %18 = load float, float* %fAngle, align 4
  %mul16 = fmul float 5.000000e-01, %18
  %19 = load float, float* %timeStep.addr, align 4
  %mul17 = fmul float %mul16, %19
  %call18 = call float @_Z5btSinf(float %mul17)
  %20 = load float, float* %fAngle, align 4
  %div19 = fdiv float %call18, %20
  store float %div19, float* %ref.tmp15, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %21 = bitcast %class.btVector3* %axis to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %23 = load float, float* %fAngle, align 4
  %24 = load float, float* %timeStep.addr, align 4
  %mul25 = fmul float %23, %24
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %25 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %25)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %26 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis4)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = bitcast %struct.btPointCollector* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #10
  ret %struct.btPointCollector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btContinuousConvexCollision* @_ZN27btContinuousConvexCollisionD2Ev(%class.btContinuousConvexCollision* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %0 = bitcast %class.btContinuousConvexCollision* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* %0) #10
  ret %class.btContinuousConvexCollision* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27btContinuousConvexCollisionD0Ev(%class.btContinuousConvexCollision* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btContinuousConvexCollision*, align 4
  store %class.btContinuousConvexCollision* %this, %class.btContinuousConvexCollision** %this.addr, align 4
  %this1 = load %class.btContinuousConvexCollision*, %class.btContinuousConvexCollision** %this.addr, align 4
  %call = call %class.btContinuousConvexCollision* @_ZN27btContinuousConvexCollisionD2Ev(%class.btContinuousConvexCollision* %this1) #10
  %0 = bitcast %class.btContinuousConvexCollision* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %angle, float** %angle.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %2 = load float*, float** %angle.addr, align 4
  store float %call4, float* %2, align 4
  %3 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %5)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %6 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %9)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4
  %10 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %10)
  store float %call11, float* %len, align 4
  %11 = load float, float* %len, align 4
  %cmp = fcmp olt float %11, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load float, float* %len, align 4
  %call18 = call float @_Z6btSqrtf(float %15)
  store float %call18, float* %ref.tmp17, align 4
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %1 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btAcosf(float %1)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4
  %2 = load float, float* %s, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btAcosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4
  %call = call float @acosf(float %2) #12
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollectorD0Ev(%struct.btPointCollector* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %call = call %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* %this1) #10
  %0 = bitcast %struct.btPointCollector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersAEii(%struct.btPointCollector* %this, i32 %partId0, i32 %index0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersBEii(%struct.btPointCollector* %this, i32 %partId1, i32 %index1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector15addContactPointERK9btVector3S2_f(%struct.btPointCollector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4
  store float %depth, float* %depth.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = load float, float* %depth.addr, align 4
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  %1 = load float, float* %m_distance, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 1, i8* %m_hasResult, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_pointInWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load float, float* %depth.addr, align 4
  %m_distance2 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float %8, float* %m_distance2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #13
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call2)
  %3 = load float, float* %call3, align 4
  %mul = fmul float %1, %3
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 1)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call7)
  %6 = load float, float* %call8, align 4
  %mul9 = fmul float %4, %6
  %add = fadd float %mul, %mul9
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx11)
  %7 = load float, float* %call12, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 2)
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call13)
  %9 = load float, float* %call14, align 4
  %mul15 = fmul float %7, %9
  %add16 = fadd float %add, %mul15
  store float %add16, float* %ref.tmp, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx19)
  %10 = load float, float* %call20, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call21)
  %12 = load float, float* %call22, align 4
  %mul23 = fmul float %10, %12
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx25)
  %13 = load float, float* %call26, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %15 = load float, float* %call28, align 4
  %mul29 = fmul float %13, %15
  %add30 = fadd float %mul23, %mul29
  %m_el31 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el31, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx32)
  %16 = load float, float* %call33, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 2)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call34)
  %18 = load float, float* %call35, align 4
  %mul36 = fmul float %16, %18
  %add37 = fadd float %add30, %mul36
  store float %add37, float* %ref.tmp17, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 0
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx40)
  %19 = load float, float* %call41, align 4
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %20, i32 0)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call42)
  %21 = load float, float* %call43, align 4
  %mul44 = fmul float %19, %21
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx46)
  %22 = load float, float* %call47, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call48)
  %24 = load float, float* %call49, align 4
  %mul50 = fmul float %22, %24
  %add51 = fadd float %mul44, %mul50
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 2
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %25 = load float, float* %call54, align 4
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %26, i32 2)
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call55)
  %27 = load float, float* %call56, align 4
  %mul57 = fmul float %25, %27
  %add58 = fadd float %add51, %mul57
  store float %add58, float* %ref.tmp38, align 4
  %m_el60 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el60, i32 0, i32 0
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx61)
  %28 = load float, float* %call62, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 0)
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call63)
  %30 = load float, float* %call64, align 4
  %mul65 = fmul float %28, %30
  %m_el66 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el66, i32 0, i32 1
  %call68 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx67)
  %31 = load float, float* %call68, align 4
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 1)
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call69)
  %33 = load float, float* %call70, align 4
  %mul71 = fmul float %31, %33
  %add72 = fadd float %mul65, %mul71
  %m_el73 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el73, i32 0, i32 2
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx74)
  %34 = load float, float* %call75, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call76)
  %36 = load float, float* %call77, align 4
  %mul78 = fmul float %34, %36
  %add79 = fadd float %add72, %mul78
  store float %add79, float* %ref.tmp59, align 4
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 0
  %call83 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx82)
  %37 = load float, float* %call83, align 4
  %38 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %38, i32 0)
  %call85 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call84)
  %39 = load float, float* %call85, align 4
  %mul86 = fmul float %37, %39
  %m_el87 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx88 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el87, i32 0, i32 1
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx88)
  %40 = load float, float* %call89, align 4
  %41 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %41, i32 1)
  %call91 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call90)
  %42 = load float, float* %call91, align 4
  %mul92 = fmul float %40, %42
  %add93 = fadd float %mul86, %mul92
  %m_el94 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el94, i32 0, i32 2
  %call96 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx95)
  %43 = load float, float* %call96, align 4
  %44 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %44, i32 2)
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call97)
  %45 = load float, float* %call98, align 4
  %mul99 = fmul float %43, %45
  %add100 = fadd float %add93, %mul99
  store float %add100, float* %ref.tmp80, align 4
  %m_el102 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el102, i32 0, i32 0
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx103)
  %46 = load float, float* %call104, align 4
  %47 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %47, i32 0)
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call105)
  %48 = load float, float* %call106, align 4
  %mul107 = fmul float %46, %48
  %m_el108 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el108, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx109)
  %49 = load float, float* %call110, align 4
  %50 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %50, i32 1)
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call111)
  %51 = load float, float* %call112, align 4
  %mul113 = fmul float %49, %51
  %add114 = fadd float %mul107, %mul113
  %m_el115 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el115, i32 0, i32 2
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx116)
  %52 = load float, float* %call117, align 4
  %53 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %53, i32 2)
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call118)
  %54 = load float, float* %call119, align 4
  %mul120 = fmul float %52, %54
  %add121 = fadd float %add114, %mul120
  store float %add121, float* %ref.tmp101, align 4
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 0
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx124)
  %55 = load float, float* %call125, align 4
  %56 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call126 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %56, i32 0)
  %call127 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call126)
  %57 = load float, float* %call127, align 4
  %mul128 = fmul float %55, %57
  %m_el129 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx130 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el129, i32 0, i32 1
  %call131 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx130)
  %58 = load float, float* %call131, align 4
  %59 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %59, i32 1)
  %call133 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call132)
  %60 = load float, float* %call133, align 4
  %mul134 = fmul float %58, %60
  %add135 = fadd float %mul128, %mul134
  %m_el136 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el136, i32 0, i32 2
  %call138 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx137)
  %61 = load float, float* %call138, align 4
  %62 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %62, i32 2)
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call139)
  %63 = load float, float* %call140, align 4
  %mul141 = fmul float %61, %63
  %add142 = fadd float %add135, %mul141
  store float %add142, float* %ref.tmp122, align 4
  %m_el144 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el144, i32 0, i32 0
  %call146 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx145)
  %64 = load float, float* %call146, align 4
  %65 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %65, i32 0)
  %call148 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call147)
  %66 = load float, float* %call148, align 4
  %mul149 = fmul float %64, %66
  %m_el150 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx151 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el150, i32 0, i32 1
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx151)
  %67 = load float, float* %call152, align 4
  %68 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call153 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %68, i32 1)
  %call154 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call153)
  %69 = load float, float* %call154, align 4
  %mul155 = fmul float %67, %69
  %add156 = fadd float %mul149, %mul155
  %m_el157 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx158 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el157, i32 0, i32 2
  %call159 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx158)
  %70 = load float, float* %call159, align 4
  %71 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %71, i32 2)
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call160)
  %72 = load float, float* %call161, align 4
  %mul162 = fmul float %70, %72
  %add163 = fadd float %add156, %mul162
  store float %add163, float* %ref.tmp143, align 4
  %m_el165 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx166 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el165, i32 0, i32 0
  %call167 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx166)
  %73 = load float, float* %call167, align 4
  %74 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call168 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %74, i32 0)
  %call169 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call168)
  %75 = load float, float* %call169, align 4
  %mul170 = fmul float %73, %75
  %m_el171 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el171, i32 0, i32 1
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx172)
  %76 = load float, float* %call173, align 4
  %77 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call174 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %77, i32 1)
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call174)
  %78 = load float, float* %call175, align 4
  %mul176 = fmul float %76, %78
  %add177 = fadd float %mul170, %mul176
  %m_el178 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el178, i32 0, i32 2
  %call180 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx179)
  %79 = load float, float* %call180, align 4
  %80 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call181 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %80, i32 2)
  %call182 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call181)
  %81 = load float, float* %call182, align 4
  %mul183 = fmul float %79, %81
  %add184 = fadd float %add177, %mul183
  store float %add184, float* %ref.tmp164, align 4
  %call185 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp164)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: nounwind
declare %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned) unnamed_addr #9

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btContinuousConvexCollision.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind }
attributes #11 = { builtin nounwind }
attributes #12 = { nounwind readnone }
attributes #13 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
