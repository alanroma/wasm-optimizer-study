; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btBoxBoxDetector.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btBoxBoxDetector.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.btBoxBoxDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btBoxShape*, %class.btBoxShape* }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btBoxShape = type { %class.btPolyhedralConvexShape }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btConvexPolyhedron = type opaque
%struct.dContactGeom = type opaque
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btIDebugDraw = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z6btFabsf = comdat any

$_Z7btAtan2ff = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZngRK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK10btBoxShape24getHalfExtentsWithMarginEv = comdat any

$_ZN16btBoxBoxDetectorD2Ev = comdat any

$_ZN16btBoxBoxDetectorD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD0Ev = comdat any

$_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZTS36btDiscreteCollisionDetectorInterface = comdat any

$_ZTI36btDiscreteCollisionDetectorInterface = comdat any

$_ZTV36btDiscreteCollisionDetectorInterface = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV16btBoxBoxDetector = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btBoxBoxDetector to i8*), i8* bitcast (%struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*)* @_ZN16btBoxBoxDetectorD2Ev to i8*), i8* bitcast (void (%struct.btBoxBoxDetector*)* @_ZN16btBoxBoxDetectorD0Ev to i8*), i8* bitcast (void (%struct.btBoxBoxDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btIDebugDraw*, i1)* @_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btBoxBoxDetector = hidden constant [19 x i8] c"16btBoxBoxDetector\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant [39 x i8] c"36btDiscreteCollisionDetectorInterface\00", comdat, align 1
@_ZTI36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btDiscreteCollisionDetectorInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI16btBoxBoxDetector = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btBoxBoxDetector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*) }, align 4
@_ZTV36btDiscreteCollisionDetectorInterface = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btBoxBoxDetector.cpp, i8* null }]

@_ZN16btBoxBoxDetectorC1EPK10btBoxShapeS2_ = hidden unnamed_addr alias %struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*, %class.btBoxShape*, %class.btBoxShape*), %struct.btBoxBoxDetector* (%struct.btBoxBoxDetector*, %class.btBoxShape*, %class.btBoxShape*)* @_ZN16btBoxBoxDetectorC2EPK10btBoxShapeS2_

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.btBoxBoxDetector* @_ZN16btBoxBoxDetectorC2EPK10btBoxShapeS2_(%struct.btBoxBoxDetector* returned %this, %class.btBoxShape* %box1, %class.btBoxShape* %box2) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  %box1.addr = alloca %class.btBoxShape*, align 4
  %box2.addr = alloca %class.btBoxShape*, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4
  store %class.btBoxShape* %box1, %class.btBoxShape** %box1.addr, align 4
  store %class.btBoxShape* %box2, %class.btBoxShape** %box2.addr, align 4
  %this1 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %0 = bitcast %struct.btBoxBoxDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  %1 = bitcast %struct.btBoxBoxDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV16btBoxBoxDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_box1 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this1, i32 0, i32 1
  %2 = load %class.btBoxShape*, %class.btBoxShape** %box1.addr, align 4
  store %class.btBoxShape* %2, %class.btBoxShape** %m_box1, align 4
  %m_box2 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this1, i32 0, i32 2
  %3 = load %class.btBoxShape*, %class.btBoxShape** %box2.addr, align 4
  store %class.btBoxShape* %3, %class.btBoxShape** %m_box2, align 4
  ret %struct.btBoxBoxDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %0 = bitcast %struct.btDiscreteCollisionDetectorInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36btDiscreteCollisionDetectorInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_(%class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %ua, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %ub, float* %alpha, float* %beta) #2 {
entry:
  %pa.addr = alloca %class.btVector3*, align 4
  %ua.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  %ub.addr = alloca %class.btVector3*, align 4
  %alpha.addr = alloca float*, align 4
  %beta.addr = alloca float*, align 4
  %p = alloca %class.btVector3, align 4
  %uaub = alloca float, align 4
  %q1 = alloca float, align 4
  %q2 = alloca float, align 4
  %d = alloca float, align 4
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4
  store %class.btVector3* %ua, %class.btVector3** %ua.addr, align 4
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4
  store %class.btVector3* %ub, %class.btVector3** %ub.addr, align 4
  store float* %alpha, float** %alpha.addr, align 4
  store float* %beta, float** %beta.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %0 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx3, align 4
  %sub = fsub float %1, %3
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  store float %sub, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %7 = load float, float* %arrayidx9, align 4
  %sub10 = fsub float %5, %7
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float %sub10, float* %arrayidx12, align 4
  %8 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %9 = load float, float* %arrayidx14, align 4
  %10 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %11 = load float, float* %arrayidx16, align 4
  %sub17 = fsub float %9, %11
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  store float %sub17, float* %arrayidx19, align 4
  %12 = load %class.btVector3*, %class.btVector3** %ua.addr, align 4
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %13 = load %class.btVector3*, %class.btVector3** %ub.addr, align 4
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %call22 = call float @_ZL4dDOTPKfS0_(float* %call20, float* %call21)
  store float %call22, float* %uaub, align 4
  %14 = load %class.btVector3*, %class.btVector3** %ua.addr, align 4
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call25 = call float @_ZL4dDOTPKfS0_(float* %call23, float* %call24)
  store float %call25, float* %q1, align 4
  %15 = load %class.btVector3*, %class.btVector3** %ub.addr, align 4
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call28 = call float @_ZL4dDOTPKfS0_(float* %call26, float* %call27)
  %fneg = fneg float %call28
  store float %fneg, float* %q2, align 4
  %16 = load float, float* %uaub, align 4
  %17 = load float, float* %uaub, align 4
  %mul = fmul float %16, %17
  %sub29 = fsub float 1.000000e+00, %mul
  store float %sub29, float* %d, align 4
  %18 = load float, float* %d, align 4
  %cmp = fcmp ole float %18, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %19 = load float*, float** %alpha.addr, align 4
  store float 0.000000e+00, float* %19, align 4
  %20 = load float*, float** %beta.addr, align 4
  store float 0.000000e+00, float* %20, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %21 = load float, float* %d, align 4
  %div = fdiv float 1.000000e+00, %21
  store float %div, float* %d, align 4
  %22 = load float, float* %q1, align 4
  %23 = load float, float* %uaub, align 4
  %24 = load float, float* %q2, align 4
  %mul30 = fmul float %23, %24
  %add = fadd float %22, %mul30
  %25 = load float, float* %d, align 4
  %mul31 = fmul float %add, %25
  %26 = load float*, float** %alpha.addr, align 4
  store float %mul31, float* %26, align 4
  %27 = load float, float* %uaub, align 4
  %28 = load float, float* %q1, align 4
  %mul32 = fmul float %27, %28
  %29 = load float, float* %q2, align 4
  %add33 = fadd float %mul32, %29
  %30 = load float, float* %d, align 4
  %mul34 = fmul float %add33, %30
  %31 = load float*, float** %beta.addr, align 4
  store float %mul34, float* %31, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define internal float @_ZL4dDOTPKfS0_(float* %a, float* %b) #1 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float*, float** %b.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx5, align 4
  %10 = load float*, float** %b.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: noinline optnone
define hidden void @_Z11cullPoints2iPfiiPi(i32 %n, float* %p, i32 %m, i32 %i0, i32* %iret) #2 {
entry:
  %n.addr = alloca i32, align 4
  %p.addr = alloca float*, align 4
  %m.addr = alloca i32, align 4
  %i0.addr = alloca i32, align 4
  %iret.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %a = alloca float, align 4
  %cx = alloca float, align 4
  %cy = alloca float, align 4
  %q = alloca float, align 4
  %A = alloca [8 x float], align 16
  %avail = alloca [8 x i32], align 16
  %maxdiff = alloca float, align 4
  %diff = alloca float, align 4
  store i32 %n, i32* %n.addr, align 4
  store float* %p, float** %p.addr, align 4
  store i32 %m, i32* %m.addr, align 4
  store i32 %i0, i32* %i0.addr, align 4
  store i32* %iret, i32** %iret.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load float*, float** %p.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float, float* %arrayidx, align 4
  store float %2, float* %cx, align 4
  %3 = load float*, float** %p.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %3, i32 1
  %4 = load float, float* %arrayidx1, align 4
  store float %4, float* %cy, align 4
  br label %if.end78

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %n.addr, align 4
  %cmp2 = icmp eq i32 %5, 2
  br i1 %cmp2, label %if.then3, label %if.else10

if.then3:                                         ; preds = %if.else
  %6 = load float*, float** %p.addr, align 4
  %arrayidx4 = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx4, align 4
  %8 = load float*, float** %p.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx5, align 4
  %add = fadd float %7, %9
  %mul = fmul float 5.000000e-01, %add
  store float %mul, float* %cx, align 4
  %10 = load float*, float** %p.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 1
  %11 = load float, float* %arrayidx6, align 4
  %12 = load float*, float** %p.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 3
  %13 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %11, %13
  %mul9 = fmul float 5.000000e-01, %add8
  store float %mul9, float* %cy, align 4
  br label %if.end77

if.else10:                                        ; preds = %if.else
  store float 0.000000e+00, float* %a, align 4
  store float 0.000000e+00, float* %cx, align 4
  store float 0.000000e+00, float* %cy, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else10
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %15, 1
  %cmp11 = icmp slt i32 %14, %sub
  br i1 %cmp11, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load float*, float** %p.addr, align 4
  %17 = load i32, i32* %i, align 4
  %mul12 = mul nsw i32 %17, 2
  %arrayidx13 = getelementptr inbounds float, float* %16, i32 %mul12
  %18 = load float, float* %arrayidx13, align 4
  %19 = load float*, float** %p.addr, align 4
  %20 = load i32, i32* %i, align 4
  %mul14 = mul nsw i32 %20, 2
  %add15 = add nsw i32 %mul14, 3
  %arrayidx16 = getelementptr inbounds float, float* %19, i32 %add15
  %21 = load float, float* %arrayidx16, align 4
  %mul17 = fmul float %18, %21
  %22 = load float*, float** %p.addr, align 4
  %23 = load i32, i32* %i, align 4
  %mul18 = mul nsw i32 %23, 2
  %add19 = add nsw i32 %mul18, 2
  %arrayidx20 = getelementptr inbounds float, float* %22, i32 %add19
  %24 = load float, float* %arrayidx20, align 4
  %25 = load float*, float** %p.addr, align 4
  %26 = load i32, i32* %i, align 4
  %mul21 = mul nsw i32 %26, 2
  %add22 = add nsw i32 %mul21, 1
  %arrayidx23 = getelementptr inbounds float, float* %25, i32 %add22
  %27 = load float, float* %arrayidx23, align 4
  %mul24 = fmul float %24, %27
  %sub25 = fsub float %mul17, %mul24
  store float %sub25, float* %q, align 4
  %28 = load float, float* %q, align 4
  %29 = load float, float* %a, align 4
  %add26 = fadd float %29, %28
  store float %add26, float* %a, align 4
  %30 = load float, float* %q, align 4
  %31 = load float*, float** %p.addr, align 4
  %32 = load i32, i32* %i, align 4
  %mul27 = mul nsw i32 %32, 2
  %arrayidx28 = getelementptr inbounds float, float* %31, i32 %mul27
  %33 = load float, float* %arrayidx28, align 4
  %34 = load float*, float** %p.addr, align 4
  %35 = load i32, i32* %i, align 4
  %mul29 = mul nsw i32 %35, 2
  %add30 = add nsw i32 %mul29, 2
  %arrayidx31 = getelementptr inbounds float, float* %34, i32 %add30
  %36 = load float, float* %arrayidx31, align 4
  %add32 = fadd float %33, %36
  %mul33 = fmul float %30, %add32
  %37 = load float, float* %cx, align 4
  %add34 = fadd float %37, %mul33
  store float %add34, float* %cx, align 4
  %38 = load float, float* %q, align 4
  %39 = load float*, float** %p.addr, align 4
  %40 = load i32, i32* %i, align 4
  %mul35 = mul nsw i32 %40, 2
  %add36 = add nsw i32 %mul35, 1
  %arrayidx37 = getelementptr inbounds float, float* %39, i32 %add36
  %41 = load float, float* %arrayidx37, align 4
  %42 = load float*, float** %p.addr, align 4
  %43 = load i32, i32* %i, align 4
  %mul38 = mul nsw i32 %43, 2
  %add39 = add nsw i32 %mul38, 3
  %arrayidx40 = getelementptr inbounds float, float* %42, i32 %add39
  %44 = load float, float* %arrayidx40, align 4
  %add41 = fadd float %41, %44
  %mul42 = fmul float %38, %add41
  %45 = load float, float* %cy, align 4
  %add43 = fadd float %45, %mul42
  store float %add43, float* %cy, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %46 = load i32, i32* %i, align 4
  %inc = add nsw i32 %46, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %47 = load float*, float** %p.addr, align 4
  %48 = load i32, i32* %n.addr, align 4
  %mul44 = mul nsw i32 %48, 2
  %sub45 = sub nsw i32 %mul44, 2
  %arrayidx46 = getelementptr inbounds float, float* %47, i32 %sub45
  %49 = load float, float* %arrayidx46, align 4
  %50 = load float*, float** %p.addr, align 4
  %arrayidx47 = getelementptr inbounds float, float* %50, i32 1
  %51 = load float, float* %arrayidx47, align 4
  %mul48 = fmul float %49, %51
  %52 = load float*, float** %p.addr, align 4
  %arrayidx49 = getelementptr inbounds float, float* %52, i32 0
  %53 = load float, float* %arrayidx49, align 4
  %54 = load float*, float** %p.addr, align 4
  %55 = load i32, i32* %n.addr, align 4
  %mul50 = mul nsw i32 %55, 2
  %sub51 = sub nsw i32 %mul50, 1
  %arrayidx52 = getelementptr inbounds float, float* %54, i32 %sub51
  %56 = load float, float* %arrayidx52, align 4
  %mul53 = fmul float %53, %56
  %sub54 = fsub float %mul48, %mul53
  store float %sub54, float* %q, align 4
  %57 = load float, float* %a, align 4
  %58 = load float, float* %q, align 4
  %add55 = fadd float %57, %58
  %call = call float @_Z6btFabsf(float %add55)
  %cmp56 = fcmp ogt float %call, 0x3E80000000000000
  br i1 %cmp56, label %if.then57, label %if.else60

if.then57:                                        ; preds = %for.end
  %59 = load float, float* %a, align 4
  %60 = load float, float* %q, align 4
  %add58 = fadd float %59, %60
  %mul59 = fmul float 3.000000e+00, %add58
  %div = fdiv float 1.000000e+00, %mul59
  store float %div, float* %a, align 4
  br label %if.end

if.else60:                                        ; preds = %for.end
  store float 0x43ABC16D60000000, float* %a, align 4
  br label %if.end

if.end:                                           ; preds = %if.else60, %if.then57
  %61 = load float, float* %a, align 4
  %62 = load float, float* %cx, align 4
  %63 = load float, float* %q, align 4
  %64 = load float*, float** %p.addr, align 4
  %65 = load i32, i32* %n.addr, align 4
  %mul61 = mul nsw i32 %65, 2
  %sub62 = sub nsw i32 %mul61, 2
  %arrayidx63 = getelementptr inbounds float, float* %64, i32 %sub62
  %66 = load float, float* %arrayidx63, align 4
  %67 = load float*, float** %p.addr, align 4
  %arrayidx64 = getelementptr inbounds float, float* %67, i32 0
  %68 = load float, float* %arrayidx64, align 4
  %add65 = fadd float %66, %68
  %mul66 = fmul float %63, %add65
  %add67 = fadd float %62, %mul66
  %mul68 = fmul float %61, %add67
  store float %mul68, float* %cx, align 4
  %69 = load float, float* %a, align 4
  %70 = load float, float* %cy, align 4
  %71 = load float, float* %q, align 4
  %72 = load float*, float** %p.addr, align 4
  %73 = load i32, i32* %n.addr, align 4
  %mul69 = mul nsw i32 %73, 2
  %sub70 = sub nsw i32 %mul69, 1
  %arrayidx71 = getelementptr inbounds float, float* %72, i32 %sub70
  %74 = load float, float* %arrayidx71, align 4
  %75 = load float*, float** %p.addr, align 4
  %arrayidx72 = getelementptr inbounds float, float* %75, i32 1
  %76 = load float, float* %arrayidx72, align 4
  %add73 = fadd float %74, %76
  %mul74 = fmul float %71, %add73
  %add75 = fadd float %70, %mul74
  %mul76 = fmul float %69, %add75
  store float %mul76, float* %cy, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.end, %if.then3
  br label %if.end78

if.end78:                                         ; preds = %if.end77, %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc91, %if.end78
  %77 = load i32, i32* %i, align 4
  %78 = load i32, i32* %n.addr, align 4
  %cmp80 = icmp slt i32 %77, %78
  br i1 %cmp80, label %for.body81, label %for.end93

for.body81:                                       ; preds = %for.cond79
  %79 = load float*, float** %p.addr, align 4
  %80 = load i32, i32* %i, align 4
  %mul82 = mul nsw i32 %80, 2
  %add83 = add nsw i32 %mul82, 1
  %arrayidx84 = getelementptr inbounds float, float* %79, i32 %add83
  %81 = load float, float* %arrayidx84, align 4
  %82 = load float, float* %cy, align 4
  %sub85 = fsub float %81, %82
  %83 = load float*, float** %p.addr, align 4
  %84 = load i32, i32* %i, align 4
  %mul86 = mul nsw i32 %84, 2
  %arrayidx87 = getelementptr inbounds float, float* %83, i32 %mul86
  %85 = load float, float* %arrayidx87, align 4
  %86 = load float, float* %cx, align 4
  %sub88 = fsub float %85, %86
  %call89 = call float @_Z7btAtan2ff(float %sub85, float %sub88)
  %87 = load i32, i32* %i, align 4
  %arrayidx90 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %87
  store float %call89, float* %arrayidx90, align 4
  br label %for.inc91

for.inc91:                                        ; preds = %for.body81
  %88 = load i32, i32* %i, align 4
  %inc92 = add nsw i32 %88, 1
  store i32 %inc92, i32* %i, align 4
  br label %for.cond79

for.end93:                                        ; preds = %for.cond79
  store i32 0, i32* %i, align 4
  br label %for.cond94

for.cond94:                                       ; preds = %for.inc98, %for.end93
  %89 = load i32, i32* %i, align 4
  %90 = load i32, i32* %n.addr, align 4
  %cmp95 = icmp slt i32 %89, %90
  br i1 %cmp95, label %for.body96, label %for.end100

for.body96:                                       ; preds = %for.cond94
  %91 = load i32, i32* %i, align 4
  %arrayidx97 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %91
  store i32 1, i32* %arrayidx97, align 4
  br label %for.inc98

for.inc98:                                        ; preds = %for.body96
  %92 = load i32, i32* %i, align 4
  %inc99 = add nsw i32 %92, 1
  store i32 %inc99, i32* %i, align 4
  br label %for.cond94

for.end100:                                       ; preds = %for.cond94
  %93 = load i32, i32* %i0.addr, align 4
  %arrayidx101 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %93
  store i32 0, i32* %arrayidx101, align 4
  %94 = load i32, i32* %i0.addr, align 4
  %95 = load i32*, i32** %iret.addr, align 4
  %arrayidx102 = getelementptr inbounds i32, i32* %95, i32 0
  store i32 %94, i32* %arrayidx102, align 4
  %96 = load i32*, i32** %iret.addr, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %96, i32 1
  store i32* %incdec.ptr, i32** %iret.addr, align 4
  store i32 1, i32* %j, align 4
  br label %for.cond103

for.cond103:                                      ; preds = %for.inc136, %for.end100
  %97 = load i32, i32* %j, align 4
  %98 = load i32, i32* %m.addr, align 4
  %cmp104 = icmp slt i32 %97, %98
  br i1 %cmp104, label %for.body105, label %for.end138

for.body105:                                      ; preds = %for.cond103
  %99 = load i32, i32* %j, align 4
  %conv = sitofp i32 %99 to float
  %100 = load i32, i32* %m.addr, align 4
  %conv106 = sitofp i32 %100 to float
  %div107 = fdiv float 0x401921FB60000000, %conv106
  %mul108 = fmul float %conv, %div107
  %101 = load i32, i32* %i0.addr, align 4
  %arrayidx109 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %101
  %102 = load float, float* %arrayidx109, align 4
  %add110 = fadd float %mul108, %102
  store float %add110, float* %a, align 4
  %103 = load float, float* %a, align 4
  %cmp111 = fcmp ogt float %103, 0x400921FB60000000
  br i1 %cmp111, label %if.then112, label %if.end114

if.then112:                                       ; preds = %for.body105
  %104 = load float, float* %a, align 4
  %sub113 = fsub float %104, 0x401921FB60000000
  store float %sub113, float* %a, align 4
  br label %if.end114

if.end114:                                        ; preds = %if.then112, %for.body105
  store float 1.000000e+09, float* %maxdiff, align 4
  %105 = load i32, i32* %i0.addr, align 4
  %106 = load i32*, i32** %iret.addr, align 4
  store i32 %105, i32* %106, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc131, %if.end114
  %107 = load i32, i32* %i, align 4
  %108 = load i32, i32* %n.addr, align 4
  %cmp116 = icmp slt i32 %107, %108
  br i1 %cmp116, label %for.body117, label %for.end133

for.body117:                                      ; preds = %for.cond115
  %109 = load i32, i32* %i, align 4
  %arrayidx118 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %109
  %110 = load i32, i32* %arrayidx118, align 4
  %tobool = icmp ne i32 %110, 0
  br i1 %tobool, label %if.then119, label %if.end130

if.then119:                                       ; preds = %for.body117
  %111 = load i32, i32* %i, align 4
  %arrayidx120 = getelementptr inbounds [8 x float], [8 x float]* %A, i32 0, i32 %111
  %112 = load float, float* %arrayidx120, align 4
  %113 = load float, float* %a, align 4
  %sub121 = fsub float %112, %113
  %call122 = call float @_Z6btFabsf(float %sub121)
  store float %call122, float* %diff, align 4
  %114 = load float, float* %diff, align 4
  %cmp123 = fcmp ogt float %114, 0x400921FB60000000
  br i1 %cmp123, label %if.then124, label %if.end126

if.then124:                                       ; preds = %if.then119
  %115 = load float, float* %diff, align 4
  %sub125 = fsub float 0x401921FB60000000, %115
  store float %sub125, float* %diff, align 4
  br label %if.end126

if.end126:                                        ; preds = %if.then124, %if.then119
  %116 = load float, float* %diff, align 4
  %117 = load float, float* %maxdiff, align 4
  %cmp127 = fcmp olt float %116, %117
  br i1 %cmp127, label %if.then128, label %if.end129

if.then128:                                       ; preds = %if.end126
  %118 = load float, float* %diff, align 4
  store float %118, float* %maxdiff, align 4
  %119 = load i32, i32* %i, align 4
  %120 = load i32*, i32** %iret.addr, align 4
  store i32 %119, i32* %120, align 4
  br label %if.end129

if.end129:                                        ; preds = %if.then128, %if.end126
  br label %if.end130

if.end130:                                        ; preds = %if.end129, %for.body117
  br label %for.inc131

for.inc131:                                       ; preds = %if.end130
  %121 = load i32, i32* %i, align 4
  %inc132 = add nsw i32 %121, 1
  store i32 %inc132, i32* %i, align 4
  br label %for.cond115

for.end133:                                       ; preds = %for.cond115
  %122 = load i32*, i32** %iret.addr, align 4
  %123 = load i32, i32* %122, align 4
  %arrayidx134 = getelementptr inbounds [8 x i32], [8 x i32]* %avail, i32 0, i32 %123
  store i32 0, i32* %arrayidx134, align 4
  %124 = load i32*, i32** %iret.addr, align 4
  %incdec.ptr135 = getelementptr inbounds i32, i32* %124, i32 1
  store i32* %incdec.ptr135, i32** %iret.addr, align 4
  br label %for.inc136

for.inc136:                                       ; preds = %for.end133
  %125 = load i32, i32* %j, align 4
  %inc137 = add nsw i32 %125, 1
  store i32 %inc137, i32* %j, align 4
  br label %for.cond103

for.end138:                                       ; preds = %for.cond103
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #9
  ret float %call
}

; Function Attrs: noinline optnone
define hidden i32 @_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %p1, float* %R1, %class.btVector3* nonnull align 4 dereferenceable(16) %side1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2, float* %R2, %class.btVector3* nonnull align 4 dereferenceable(16) %side2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %depth, i32* %return_code, i32 %maxc, %struct.dContactGeom* %0, i32 %1, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output) #2 {
entry:
  %retval = alloca i32, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %R1.addr = alloca float*, align 4
  %side1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  %R2.addr = alloca float*, align 4
  %side2.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float*, align 4
  %return_code.addr = alloca i32*, align 4
  %maxc.addr = alloca i32, align 4
  %.addr = alloca %struct.dContactGeom*, align 4
  %.addr1 = alloca i32, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %fudge_factor = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %pp = alloca %class.btVector3, align 4
  %normalC = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %normalR = alloca float*, align 4
  %A = alloca [3 x float], align 4
  %B = alloca [3 x float], align 4
  %R11 = alloca float, align 4
  %R12 = alloca float, align 4
  %R13 = alloca float, align 4
  %R21 = alloca float, align 4
  %R22 = alloca float, align 4
  %R23 = alloca float, align 4
  %R31 = alloca float, align 4
  %R32 = alloca float, align 4
  %R33 = alloca float, align 4
  %Q11 = alloca float, align 4
  %Q12 = alloca float, align 4
  %Q13 = alloca float, align 4
  %Q21 = alloca float, align 4
  %Q22 = alloca float, align 4
  %Q23 = alloca float, align 4
  %Q31 = alloca float, align 4
  %Q32 = alloca float, align 4
  %Q33 = alloca float, align 4
  %s = alloca float, align 4
  %s2 = alloca float, align 4
  %l = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %invert_normal = alloca i32, align 4
  %code = alloca i32, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %fudge2 = alloca float, align 4
  %pa = alloca %class.btVector3, align 4
  %sign = alloca float, align 4
  %pb = alloca %class.btVector3, align 4
  %alpha = alloca float, align 4
  %beta = alloca float, align 4
  %ua = alloca %class.btVector3, align 4
  %ub = alloca %class.btVector3, align 4
  %pointInWorld = alloca %class.btVector3, align 4
  %ref.tmp917 = alloca %class.btVector3, align 4
  %Ra = alloca float*, align 4
  %Rb = alloca float*, align 4
  %pa920 = alloca float*, align 4
  %pb921 = alloca float*, align 4
  %Sa = alloca float*, align 4
  %Sb = alloca float*, align 4
  %normal2 = alloca %class.btVector3, align 4
  %nr = alloca %class.btVector3, align 4
  %anr = alloca %class.btVector3, align 4
  %lanr = alloca i32, align 4
  %a1 = alloca i32, align 4
  %a2 = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %codeN = alloca i32, align 4
  %code1 = alloca i32, align 4
  %code2 = alloca i32, align 4
  %quad = alloca [8 x float], align 16
  %c1 = alloca float, align 4
  %c2 = alloca float, align 4
  %m11 = alloca float, align 4
  %m12 = alloca float, align 4
  %m21 = alloca float, align 4
  %m22 = alloca float, align 4
  %k1 = alloca float, align 4
  %k2 = alloca float, align 4
  %k3 = alloca float, align 4
  %k4 = alloca float, align 4
  %rect = alloca [2 x float], align 4
  %ret = alloca [16 x float], align 16
  %n = alloca i32, align 4
  %point = alloca [24 x float], align 16
  %dep = alloca [8 x float], align 16
  %det1 = alloca float, align 4
  %cnum = alloca i32, align 4
  %k11147 = alloca float, align 4
  %k21158 = alloca float, align 4
  %pointInWorld1233 = alloca %class.btVector3, align 4
  %ref.tmp1248 = alloca %class.btVector3, align 4
  %pointInWorld1260 = alloca %class.btVector3, align 4
  %ref.tmp1280 = alloca %class.btVector3, align 4
  %i1 = alloca i32, align 4
  %maxdepth = alloca float, align 4
  %iret = alloca [8 x i32], align 16
  %posInWorld = alloca %class.btVector3, align 4
  %ref.tmp1324 = alloca %class.btVector3, align 4
  %ref.tmp1331 = alloca %class.btVector3, align 4
  %ref.tmp1332 = alloca %class.btVector3, align 4
  %ref.tmp1333 = alloca %class.btVector3, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4
  store float* %R1, float** %R1.addr, align 4
  store %class.btVector3* %side1, %class.btVector3** %side1.addr, align 4
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4
  store float* %R2, float** %R2.addr, align 4
  store %class.btVector3* %side2, %class.btVector3** %side2.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store float* %depth, float** %depth.addr, align 4
  store i32* %return_code, i32** %return_code.addr, align 4
  store i32 %maxc, i32* %maxc.addr, align 4
  store %struct.dContactGeom* %0, %struct.dContactGeom** %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  store float 0x3FF0CCCCC0000000, float* %fudge_factor, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pp)
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalC, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  store float* null, float** %normalR, align 4
  %2 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast %class.btVector3* %p to i8*
  %5 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load float*, float** %R1.addr, align 4
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call8 = call float @_ZL6dDOT41PKfS0_(float* %6, float* %call7)
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx = getelementptr inbounds float, float* %call9, i32 0
  store float %call8, float* %arrayidx, align 4
  %7 = load float*, float** %R1.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %7, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call11 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr, float* %call10)
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  store float %call11, float* %arrayidx13, align 4
  %8 = load float*, float** %R1.addr, align 4
  %add.ptr14 = getelementptr inbounds float, float* %8, i32 2
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call16 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr14, float* %call15)
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  store float %call16, float* %arrayidx18, align 4
  %9 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 0
  %10 = load float, float* %arrayidx20, align 4
  %mul = fmul float %10, 5.000000e-01
  %arrayidx21 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float %mul, float* %arrayidx21, align 4
  %11 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4
  %call22 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %12 = load float, float* %arrayidx23, align 4
  %mul24 = fmul float %12, 5.000000e-01
  %arrayidx25 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  store float %mul24, float* %arrayidx25, align 4
  %13 = load %class.btVector3*, %class.btVector3** %side1.addr, align 4
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %13)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %14 = load float, float* %arrayidx27, align 4
  %mul28 = fmul float %14, 5.000000e-01
  %arrayidx29 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  store float %mul28, float* %arrayidx29, align 4
  %15 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4
  %call30 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 0
  %16 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %16, 5.000000e-01
  %arrayidx33 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float %mul32, float* %arrayidx33, align 4
  %17 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %17)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %18 = load float, float* %arrayidx35, align 4
  %mul36 = fmul float %18, 5.000000e-01
  %arrayidx37 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  store float %mul36, float* %arrayidx37, align 4
  %19 = load %class.btVector3*, %class.btVector3** %side2.addr, align 4
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %19)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %20 = load float, float* %arrayidx39, align 4
  %mul40 = fmul float %20, 5.000000e-01
  %arrayidx41 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  store float %mul40, float* %arrayidx41, align 4
  %21 = load float*, float** %R1.addr, align 4
  %add.ptr42 = getelementptr inbounds float, float* %21, i32 0
  %22 = load float*, float** %R2.addr, align 4
  %add.ptr43 = getelementptr inbounds float, float* %22, i32 0
  %call44 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr42, float* %add.ptr43)
  store float %call44, float* %R11, align 4
  %23 = load float*, float** %R1.addr, align 4
  %add.ptr45 = getelementptr inbounds float, float* %23, i32 0
  %24 = load float*, float** %R2.addr, align 4
  %add.ptr46 = getelementptr inbounds float, float* %24, i32 1
  %call47 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr45, float* %add.ptr46)
  store float %call47, float* %R12, align 4
  %25 = load float*, float** %R1.addr, align 4
  %add.ptr48 = getelementptr inbounds float, float* %25, i32 0
  %26 = load float*, float** %R2.addr, align 4
  %add.ptr49 = getelementptr inbounds float, float* %26, i32 2
  %call50 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr48, float* %add.ptr49)
  store float %call50, float* %R13, align 4
  %27 = load float*, float** %R1.addr, align 4
  %add.ptr51 = getelementptr inbounds float, float* %27, i32 1
  %28 = load float*, float** %R2.addr, align 4
  %add.ptr52 = getelementptr inbounds float, float* %28, i32 0
  %call53 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr51, float* %add.ptr52)
  store float %call53, float* %R21, align 4
  %29 = load float*, float** %R1.addr, align 4
  %add.ptr54 = getelementptr inbounds float, float* %29, i32 1
  %30 = load float*, float** %R2.addr, align 4
  %add.ptr55 = getelementptr inbounds float, float* %30, i32 1
  %call56 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr54, float* %add.ptr55)
  store float %call56, float* %R22, align 4
  %31 = load float*, float** %R1.addr, align 4
  %add.ptr57 = getelementptr inbounds float, float* %31, i32 1
  %32 = load float*, float** %R2.addr, align 4
  %add.ptr58 = getelementptr inbounds float, float* %32, i32 2
  %call59 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr57, float* %add.ptr58)
  store float %call59, float* %R23, align 4
  %33 = load float*, float** %R1.addr, align 4
  %add.ptr60 = getelementptr inbounds float, float* %33, i32 2
  %34 = load float*, float** %R2.addr, align 4
  %add.ptr61 = getelementptr inbounds float, float* %34, i32 0
  %call62 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr60, float* %add.ptr61)
  store float %call62, float* %R31, align 4
  %35 = load float*, float** %R1.addr, align 4
  %add.ptr63 = getelementptr inbounds float, float* %35, i32 2
  %36 = load float*, float** %R2.addr, align 4
  %add.ptr64 = getelementptr inbounds float, float* %36, i32 1
  %call65 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr63, float* %add.ptr64)
  store float %call65, float* %R32, align 4
  %37 = load float*, float** %R1.addr, align 4
  %add.ptr66 = getelementptr inbounds float, float* %37, i32 2
  %38 = load float*, float** %R2.addr, align 4
  %add.ptr67 = getelementptr inbounds float, float* %38, i32 2
  %call68 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr66, float* %add.ptr67)
  store float %call68, float* %R33, align 4
  %39 = load float, float* %R11, align 4
  %call69 = call float @_Z6btFabsf(float %39)
  store float %call69, float* %Q11, align 4
  %40 = load float, float* %R12, align 4
  %call70 = call float @_Z6btFabsf(float %40)
  store float %call70, float* %Q12, align 4
  %41 = load float, float* %R13, align 4
  %call71 = call float @_Z6btFabsf(float %41)
  store float %call71, float* %Q13, align 4
  %42 = load float, float* %R21, align 4
  %call72 = call float @_Z6btFabsf(float %42)
  store float %call72, float* %Q21, align 4
  %43 = load float, float* %R22, align 4
  %call73 = call float @_Z6btFabsf(float %43)
  store float %call73, float* %Q22, align 4
  %44 = load float, float* %R23, align 4
  %call74 = call float @_Z6btFabsf(float %44)
  store float %call74, float* %Q23, align 4
  %45 = load float, float* %R31, align 4
  %call75 = call float @_Z6btFabsf(float %45)
  store float %call75, float* %Q31, align 4
  %46 = load float, float* %R32, align 4
  %call76 = call float @_Z6btFabsf(float %46)
  store float %call76, float* %Q32, align 4
  %47 = load float, float* %R33, align 4
  %call77 = call float @_Z6btFabsf(float %47)
  store float %call77, float* %Q33, align 4
  store float 0xC7EFFFFFE0000000, float* %s, align 4
  store i32 0, i32* %invert_normal, align 4
  store i32 0, i32* %code, align 4
  %call78 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 0
  %48 = load float, float* %arrayidx79, align 4
  %call80 = call float @_Z6btFabsf(float %48)
  %arrayidx81 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %49 = load float, float* %arrayidx81, align 4
  %arrayidx82 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %50 = load float, float* %arrayidx82, align 4
  %51 = load float, float* %Q11, align 4
  %mul83 = fmul float %50, %51
  %add = fadd float %49, %mul83
  %arrayidx84 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %52 = load float, float* %arrayidx84, align 4
  %53 = load float, float* %Q12, align 4
  %mul85 = fmul float %52, %53
  %add86 = fadd float %add, %mul85
  %arrayidx87 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %54 = load float, float* %arrayidx87, align 4
  %55 = load float, float* %Q13, align 4
  %mul88 = fmul float %54, %55
  %add89 = fadd float %add86, %mul88
  %sub = fsub float %call80, %add89
  store float %sub, float* %s2, align 4
  %56 = load float, float* %s2, align 4
  %cmp = fcmp ogt float %56, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %57 = load float, float* %s2, align 4
  %58 = load float, float* %s, align 4
  %cmp90 = fcmp ogt float %57, %58
  br i1 %cmp90, label %if.then91, label %if.end96

if.then91:                                        ; preds = %if.end
  %59 = load float, float* %s2, align 4
  store float %59, float* %s, align 4
  %60 = load float*, float** %R1.addr, align 4
  %add.ptr92 = getelementptr inbounds float, float* %60, i32 0
  store float* %add.ptr92, float** %normalR, align 4
  %call93 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 0
  %61 = load float, float* %arrayidx94, align 4
  %cmp95 = fcmp olt float %61, 0.000000e+00
  %conv = zext i1 %cmp95 to i32
  store i32 %conv, i32* %invert_normal, align 4
  store i32 1, i32* %code, align 4
  br label %if.end96

if.end96:                                         ; preds = %if.then91, %if.end
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 1
  %62 = load float, float* %arrayidx98, align 4
  %call99 = call float @_Z6btFabsf(float %62)
  %arrayidx100 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %63 = load float, float* %arrayidx100, align 4
  %arrayidx101 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %64 = load float, float* %arrayidx101, align 4
  %65 = load float, float* %Q21, align 4
  %mul102 = fmul float %64, %65
  %add103 = fadd float %63, %mul102
  %arrayidx104 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %66 = load float, float* %arrayidx104, align 4
  %67 = load float, float* %Q22, align 4
  %mul105 = fmul float %66, %67
  %add106 = fadd float %add103, %mul105
  %arrayidx107 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %68 = load float, float* %arrayidx107, align 4
  %69 = load float, float* %Q23, align 4
  %mul108 = fmul float %68, %69
  %add109 = fadd float %add106, %mul108
  %sub110 = fsub float %call99, %add109
  store float %sub110, float* %s2, align 4
  %70 = load float, float* %s2, align 4
  %cmp111 = fcmp ogt float %70, 0.000000e+00
  br i1 %cmp111, label %if.then112, label %if.end113

if.then112:                                       ; preds = %if.end96
  store i32 0, i32* %retval, align 4
  br label %return

if.end113:                                        ; preds = %if.end96
  %71 = load float, float* %s2, align 4
  %72 = load float, float* %s, align 4
  %cmp114 = fcmp ogt float %71, %72
  br i1 %cmp114, label %if.then115, label %if.end121

if.then115:                                       ; preds = %if.end113
  %73 = load float, float* %s2, align 4
  store float %73, float* %s, align 4
  %74 = load float*, float** %R1.addr, align 4
  %add.ptr116 = getelementptr inbounds float, float* %74, i32 1
  store float* %add.ptr116, float** %normalR, align 4
  %call117 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 1
  %75 = load float, float* %arrayidx118, align 4
  %cmp119 = fcmp olt float %75, 0.000000e+00
  %conv120 = zext i1 %cmp119 to i32
  store i32 %conv120, i32* %invert_normal, align 4
  store i32 2, i32* %code, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then115, %if.end113
  %call122 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 2
  %76 = load float, float* %arrayidx123, align 4
  %call124 = call float @_Z6btFabsf(float %76)
  %arrayidx125 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %77 = load float, float* %arrayidx125, align 4
  %arrayidx126 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %78 = load float, float* %arrayidx126, align 4
  %79 = load float, float* %Q31, align 4
  %mul127 = fmul float %78, %79
  %add128 = fadd float %77, %mul127
  %arrayidx129 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %80 = load float, float* %arrayidx129, align 4
  %81 = load float, float* %Q32, align 4
  %mul130 = fmul float %80, %81
  %add131 = fadd float %add128, %mul130
  %arrayidx132 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %82 = load float, float* %arrayidx132, align 4
  %83 = load float, float* %Q33, align 4
  %mul133 = fmul float %82, %83
  %add134 = fadd float %add131, %mul133
  %sub135 = fsub float %call124, %add134
  store float %sub135, float* %s2, align 4
  %84 = load float, float* %s2, align 4
  %cmp136 = fcmp ogt float %84, 0.000000e+00
  br i1 %cmp136, label %if.then137, label %if.end138

if.then137:                                       ; preds = %if.end121
  store i32 0, i32* %retval, align 4
  br label %return

if.end138:                                        ; preds = %if.end121
  %85 = load float, float* %s2, align 4
  %86 = load float, float* %s, align 4
  %cmp139 = fcmp ogt float %85, %86
  br i1 %cmp139, label %if.then140, label %if.end146

if.then140:                                       ; preds = %if.end138
  %87 = load float, float* %s2, align 4
  store float %87, float* %s, align 4
  %88 = load float*, float** %R1.addr, align 4
  %add.ptr141 = getelementptr inbounds float, float* %88, i32 2
  store float* %add.ptr141, float** %normalR, align 4
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 2
  %89 = load float, float* %arrayidx143, align 4
  %cmp144 = fcmp olt float %89, 0.000000e+00
  %conv145 = zext i1 %cmp144 to i32
  store i32 %conv145, i32* %invert_normal, align 4
  store i32 3, i32* %code, align 4
  br label %if.end146

if.end146:                                        ; preds = %if.then140, %if.end138
  %90 = load float*, float** %R2.addr, align 4
  %add.ptr147 = getelementptr inbounds float, float* %90, i32 0
  %call148 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call149 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr147, float* %call148)
  %call150 = call float @_Z6btFabsf(float %call149)
  %arrayidx151 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %91 = load float, float* %arrayidx151, align 4
  %92 = load float, float* %Q11, align 4
  %mul152 = fmul float %91, %92
  %arrayidx153 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %93 = load float, float* %arrayidx153, align 4
  %94 = load float, float* %Q21, align 4
  %mul154 = fmul float %93, %94
  %add155 = fadd float %mul152, %mul154
  %arrayidx156 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %95 = load float, float* %arrayidx156, align 4
  %96 = load float, float* %Q31, align 4
  %mul157 = fmul float %95, %96
  %add158 = fadd float %add155, %mul157
  %arrayidx159 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %97 = load float, float* %arrayidx159, align 4
  %add160 = fadd float %add158, %97
  %sub161 = fsub float %call150, %add160
  store float %sub161, float* %s2, align 4
  %98 = load float, float* %s2, align 4
  %cmp162 = fcmp ogt float %98, 0.000000e+00
  br i1 %cmp162, label %if.then163, label %if.end164

if.then163:                                       ; preds = %if.end146
  store i32 0, i32* %retval, align 4
  br label %return

if.end164:                                        ; preds = %if.end146
  %99 = load float, float* %s2, align 4
  %100 = load float, float* %s, align 4
  %cmp165 = fcmp ogt float %99, %100
  br i1 %cmp165, label %if.then166, label %if.end173

if.then166:                                       ; preds = %if.end164
  %101 = load float, float* %s2, align 4
  store float %101, float* %s, align 4
  %102 = load float*, float** %R2.addr, align 4
  %add.ptr167 = getelementptr inbounds float, float* %102, i32 0
  store float* %add.ptr167, float** %normalR, align 4
  %103 = load float*, float** %R2.addr, align 4
  %add.ptr168 = getelementptr inbounds float, float* %103, i32 0
  %call169 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call170 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr168, float* %call169)
  %cmp171 = fcmp olt float %call170, 0.000000e+00
  %conv172 = zext i1 %cmp171 to i32
  store i32 %conv172, i32* %invert_normal, align 4
  store i32 4, i32* %code, align 4
  br label %if.end173

if.end173:                                        ; preds = %if.then166, %if.end164
  %104 = load float*, float** %R2.addr, align 4
  %add.ptr174 = getelementptr inbounds float, float* %104, i32 1
  %call175 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call176 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr174, float* %call175)
  %call177 = call float @_Z6btFabsf(float %call176)
  %arrayidx178 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %105 = load float, float* %arrayidx178, align 4
  %106 = load float, float* %Q12, align 4
  %mul179 = fmul float %105, %106
  %arrayidx180 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %107 = load float, float* %arrayidx180, align 4
  %108 = load float, float* %Q22, align 4
  %mul181 = fmul float %107, %108
  %add182 = fadd float %mul179, %mul181
  %arrayidx183 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %109 = load float, float* %arrayidx183, align 4
  %110 = load float, float* %Q32, align 4
  %mul184 = fmul float %109, %110
  %add185 = fadd float %add182, %mul184
  %arrayidx186 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %111 = load float, float* %arrayidx186, align 4
  %add187 = fadd float %add185, %111
  %sub188 = fsub float %call177, %add187
  store float %sub188, float* %s2, align 4
  %112 = load float, float* %s2, align 4
  %cmp189 = fcmp ogt float %112, 0.000000e+00
  br i1 %cmp189, label %if.then190, label %if.end191

if.then190:                                       ; preds = %if.end173
  store i32 0, i32* %retval, align 4
  br label %return

if.end191:                                        ; preds = %if.end173
  %113 = load float, float* %s2, align 4
  %114 = load float, float* %s, align 4
  %cmp192 = fcmp ogt float %113, %114
  br i1 %cmp192, label %if.then193, label %if.end200

if.then193:                                       ; preds = %if.end191
  %115 = load float, float* %s2, align 4
  store float %115, float* %s, align 4
  %116 = load float*, float** %R2.addr, align 4
  %add.ptr194 = getelementptr inbounds float, float* %116, i32 1
  store float* %add.ptr194, float** %normalR, align 4
  %117 = load float*, float** %R2.addr, align 4
  %add.ptr195 = getelementptr inbounds float, float* %117, i32 1
  %call196 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call197 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr195, float* %call196)
  %cmp198 = fcmp olt float %call197, 0.000000e+00
  %conv199 = zext i1 %cmp198 to i32
  store i32 %conv199, i32* %invert_normal, align 4
  store i32 5, i32* %code, align 4
  br label %if.end200

if.end200:                                        ; preds = %if.then193, %if.end191
  %118 = load float*, float** %R2.addr, align 4
  %add.ptr201 = getelementptr inbounds float, float* %118, i32 2
  %call202 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call203 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr201, float* %call202)
  %call204 = call float @_Z6btFabsf(float %call203)
  %arrayidx205 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %119 = load float, float* %arrayidx205, align 4
  %120 = load float, float* %Q13, align 4
  %mul206 = fmul float %119, %120
  %arrayidx207 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %121 = load float, float* %arrayidx207, align 4
  %122 = load float, float* %Q23, align 4
  %mul208 = fmul float %121, %122
  %add209 = fadd float %mul206, %mul208
  %arrayidx210 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %123 = load float, float* %arrayidx210, align 4
  %124 = load float, float* %Q33, align 4
  %mul211 = fmul float %123, %124
  %add212 = fadd float %add209, %mul211
  %arrayidx213 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %125 = load float, float* %arrayidx213, align 4
  %add214 = fadd float %add212, %125
  %sub215 = fsub float %call204, %add214
  store float %sub215, float* %s2, align 4
  %126 = load float, float* %s2, align 4
  %cmp216 = fcmp ogt float %126, 0.000000e+00
  br i1 %cmp216, label %if.then217, label %if.end218

if.then217:                                       ; preds = %if.end200
  store i32 0, i32* %retval, align 4
  br label %return

if.end218:                                        ; preds = %if.end200
  %127 = load float, float* %s2, align 4
  %128 = load float, float* %s, align 4
  %cmp219 = fcmp ogt float %127, %128
  br i1 %cmp219, label %if.then220, label %if.end227

if.then220:                                       ; preds = %if.end218
  %129 = load float, float* %s2, align 4
  store float %129, float* %s, align 4
  %130 = load float*, float** %R2.addr, align 4
  %add.ptr221 = getelementptr inbounds float, float* %130, i32 2
  store float* %add.ptr221, float** %normalR, align 4
  %131 = load float*, float** %R2.addr, align 4
  %add.ptr222 = getelementptr inbounds float, float* %131, i32 2
  %call223 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %call224 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr222, float* %call223)
  %cmp225 = fcmp olt float %call224, 0.000000e+00
  %conv226 = zext i1 %cmp225 to i32
  store i32 %conv226, i32* %invert_normal, align 4
  store i32 6, i32* %code, align 4
  br label %if.end227

if.end227:                                        ; preds = %if.then220, %if.end218
  store float 0x3EE4F8B580000000, float* %fudge2, align 4
  %132 = load float, float* %fudge2, align 4
  %133 = load float, float* %Q11, align 4
  %add228 = fadd float %133, %132
  store float %add228, float* %Q11, align 4
  %134 = load float, float* %fudge2, align 4
  %135 = load float, float* %Q12, align 4
  %add229 = fadd float %135, %134
  store float %add229, float* %Q12, align 4
  %136 = load float, float* %fudge2, align 4
  %137 = load float, float* %Q13, align 4
  %add230 = fadd float %137, %136
  store float %add230, float* %Q13, align 4
  %138 = load float, float* %fudge2, align 4
  %139 = load float, float* %Q21, align 4
  %add231 = fadd float %139, %138
  store float %add231, float* %Q21, align 4
  %140 = load float, float* %fudge2, align 4
  %141 = load float, float* %Q22, align 4
  %add232 = fadd float %141, %140
  store float %add232, float* %Q22, align 4
  %142 = load float, float* %fudge2, align 4
  %143 = load float, float* %Q23, align 4
  %add233 = fadd float %143, %142
  store float %add233, float* %Q23, align 4
  %144 = load float, float* %fudge2, align 4
  %145 = load float, float* %Q31, align 4
  %add234 = fadd float %145, %144
  store float %add234, float* %Q31, align 4
  %146 = load float, float* %fudge2, align 4
  %147 = load float, float* %Q32, align 4
  %add235 = fadd float %147, %146
  store float %add235, float* %Q32, align 4
  %148 = load float, float* %fudge2, align 4
  %149 = load float, float* %Q33, align 4
  %add236 = fadd float %149, %148
  store float %add236, float* %Q33, align 4
  %call237 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx238 = getelementptr inbounds float, float* %call237, i32 2
  %150 = load float, float* %arrayidx238, align 4
  %151 = load float, float* %R21, align 4
  %mul239 = fmul float %150, %151
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 1
  %152 = load float, float* %arrayidx241, align 4
  %153 = load float, float* %R31, align 4
  %mul242 = fmul float %152, %153
  %sub243 = fsub float %mul239, %mul242
  %call244 = call float @_Z6btFabsf(float %sub243)
  %arrayidx245 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %154 = load float, float* %arrayidx245, align 4
  %155 = load float, float* %Q31, align 4
  %mul246 = fmul float %154, %155
  %arrayidx247 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %156 = load float, float* %arrayidx247, align 4
  %157 = load float, float* %Q21, align 4
  %mul248 = fmul float %156, %157
  %add249 = fadd float %mul246, %mul248
  %arrayidx250 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %158 = load float, float* %arrayidx250, align 4
  %159 = load float, float* %Q13, align 4
  %mul251 = fmul float %158, %159
  %add252 = fadd float %add249, %mul251
  %arrayidx253 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %160 = load float, float* %arrayidx253, align 4
  %161 = load float, float* %Q12, align 4
  %mul254 = fmul float %160, %161
  %add255 = fadd float %add252, %mul254
  %sub256 = fsub float %call244, %add255
  store float %sub256, float* %s2, align 4
  %162 = load float, float* %s2, align 4
  %cmp257 = fcmp ogt float %162, 0x3E80000000000000
  br i1 %cmp257, label %if.then258, label %if.end259

if.then258:                                       ; preds = %if.end227
  store i32 0, i32* %retval, align 4
  br label %return

if.end259:                                        ; preds = %if.end227
  %163 = load float, float* %R31, align 4
  %fneg = fneg float %163
  %164 = load float, float* %R31, align 4
  %fneg260 = fneg float %164
  %mul261 = fmul float %fneg, %fneg260
  %add262 = fadd float 0.000000e+00, %mul261
  %165 = load float, float* %R21, align 4
  %166 = load float, float* %R21, align 4
  %mul263 = fmul float %165, %166
  %add264 = fadd float %add262, %mul263
  %call265 = call float @_Z6btSqrtf(float %add264)
  store float %call265, float* %l, align 4
  %167 = load float, float* %l, align 4
  %cmp266 = fcmp ogt float %167, 0x3E80000000000000
  br i1 %cmp266, label %if.then267, label %if.end291

if.then267:                                       ; preds = %if.end259
  %168 = load float, float* %l, align 4
  %169 = load float, float* %s2, align 4
  %div = fdiv float %169, %168
  store float %div, float* %s2, align 4
  %170 = load float, float* %s2, align 4
  %mul268 = fmul float %170, 0x3FF0CCCCC0000000
  %171 = load float, float* %s, align 4
  %cmp269 = fcmp ogt float %mul268, %171
  br i1 %cmp269, label %if.then270, label %if.end290

if.then270:                                       ; preds = %if.then267
  %172 = load float, float* %s2, align 4
  store float %172, float* %s, align 4
  store float* null, float** %normalR, align 4
  %173 = load float, float* %l, align 4
  %div271 = fdiv float 0.000000e+00, %173
  %call272 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx273 = getelementptr inbounds float, float* %call272, i32 0
  store float %div271, float* %arrayidx273, align 4
  %174 = load float, float* %R31, align 4
  %fneg274 = fneg float %174
  %175 = load float, float* %l, align 4
  %div275 = fdiv float %fneg274, %175
  %call276 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx277 = getelementptr inbounds float, float* %call276, i32 1
  store float %div275, float* %arrayidx277, align 4
  %176 = load float, float* %R21, align 4
  %177 = load float, float* %l, align 4
  %div278 = fdiv float %176, %177
  %call279 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx280 = getelementptr inbounds float, float* %call279, i32 2
  store float %div278, float* %arrayidx280, align 4
  %call281 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx282 = getelementptr inbounds float, float* %call281, i32 2
  %178 = load float, float* %arrayidx282, align 4
  %179 = load float, float* %R21, align 4
  %mul283 = fmul float %178, %179
  %call284 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx285 = getelementptr inbounds float, float* %call284, i32 1
  %180 = load float, float* %arrayidx285, align 4
  %181 = load float, float* %R31, align 4
  %mul286 = fmul float %180, %181
  %sub287 = fsub float %mul283, %mul286
  %cmp288 = fcmp olt float %sub287, 0.000000e+00
  %conv289 = zext i1 %cmp288 to i32
  store i32 %conv289, i32* %invert_normal, align 4
  store i32 7, i32* %code, align 4
  br label %if.end290

if.end290:                                        ; preds = %if.then270, %if.then267
  br label %if.end291

if.end291:                                        ; preds = %if.end290, %if.end259
  %call292 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx293 = getelementptr inbounds float, float* %call292, i32 2
  %182 = load float, float* %arrayidx293, align 4
  %183 = load float, float* %R22, align 4
  %mul294 = fmul float %182, %183
  %call295 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx296 = getelementptr inbounds float, float* %call295, i32 1
  %184 = load float, float* %arrayidx296, align 4
  %185 = load float, float* %R32, align 4
  %mul297 = fmul float %184, %185
  %sub298 = fsub float %mul294, %mul297
  %call299 = call float @_Z6btFabsf(float %sub298)
  %arrayidx300 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %186 = load float, float* %arrayidx300, align 4
  %187 = load float, float* %Q32, align 4
  %mul301 = fmul float %186, %187
  %arrayidx302 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %188 = load float, float* %arrayidx302, align 4
  %189 = load float, float* %Q22, align 4
  %mul303 = fmul float %188, %189
  %add304 = fadd float %mul301, %mul303
  %arrayidx305 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %190 = load float, float* %arrayidx305, align 4
  %191 = load float, float* %Q13, align 4
  %mul306 = fmul float %190, %191
  %add307 = fadd float %add304, %mul306
  %arrayidx308 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %192 = load float, float* %arrayidx308, align 4
  %193 = load float, float* %Q11, align 4
  %mul309 = fmul float %192, %193
  %add310 = fadd float %add307, %mul309
  %sub311 = fsub float %call299, %add310
  store float %sub311, float* %s2, align 4
  %194 = load float, float* %s2, align 4
  %cmp312 = fcmp ogt float %194, 0x3E80000000000000
  br i1 %cmp312, label %if.then313, label %if.end314

if.then313:                                       ; preds = %if.end291
  store i32 0, i32* %retval, align 4
  br label %return

if.end314:                                        ; preds = %if.end291
  %195 = load float, float* %R32, align 4
  %fneg315 = fneg float %195
  %196 = load float, float* %R32, align 4
  %fneg316 = fneg float %196
  %mul317 = fmul float %fneg315, %fneg316
  %add318 = fadd float 0.000000e+00, %mul317
  %197 = load float, float* %R22, align 4
  %198 = load float, float* %R22, align 4
  %mul319 = fmul float %197, %198
  %add320 = fadd float %add318, %mul319
  %call321 = call float @_Z6btSqrtf(float %add320)
  store float %call321, float* %l, align 4
  %199 = load float, float* %l, align 4
  %cmp322 = fcmp ogt float %199, 0x3E80000000000000
  br i1 %cmp322, label %if.then323, label %if.end348

if.then323:                                       ; preds = %if.end314
  %200 = load float, float* %l, align 4
  %201 = load float, float* %s2, align 4
  %div324 = fdiv float %201, %200
  store float %div324, float* %s2, align 4
  %202 = load float, float* %s2, align 4
  %mul325 = fmul float %202, 0x3FF0CCCCC0000000
  %203 = load float, float* %s, align 4
  %cmp326 = fcmp ogt float %mul325, %203
  br i1 %cmp326, label %if.then327, label %if.end347

if.then327:                                       ; preds = %if.then323
  %204 = load float, float* %s2, align 4
  store float %204, float* %s, align 4
  store float* null, float** %normalR, align 4
  %205 = load float, float* %l, align 4
  %div328 = fdiv float 0.000000e+00, %205
  %call329 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx330 = getelementptr inbounds float, float* %call329, i32 0
  store float %div328, float* %arrayidx330, align 4
  %206 = load float, float* %R32, align 4
  %fneg331 = fneg float %206
  %207 = load float, float* %l, align 4
  %div332 = fdiv float %fneg331, %207
  %call333 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx334 = getelementptr inbounds float, float* %call333, i32 1
  store float %div332, float* %arrayidx334, align 4
  %208 = load float, float* %R22, align 4
  %209 = load float, float* %l, align 4
  %div335 = fdiv float %208, %209
  %call336 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx337 = getelementptr inbounds float, float* %call336, i32 2
  store float %div335, float* %arrayidx337, align 4
  %call338 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx339 = getelementptr inbounds float, float* %call338, i32 2
  %210 = load float, float* %arrayidx339, align 4
  %211 = load float, float* %R22, align 4
  %mul340 = fmul float %210, %211
  %call341 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx342 = getelementptr inbounds float, float* %call341, i32 1
  %212 = load float, float* %arrayidx342, align 4
  %213 = load float, float* %R32, align 4
  %mul343 = fmul float %212, %213
  %sub344 = fsub float %mul340, %mul343
  %cmp345 = fcmp olt float %sub344, 0.000000e+00
  %conv346 = zext i1 %cmp345 to i32
  store i32 %conv346, i32* %invert_normal, align 4
  store i32 8, i32* %code, align 4
  br label %if.end347

if.end347:                                        ; preds = %if.then327, %if.then323
  br label %if.end348

if.end348:                                        ; preds = %if.end347, %if.end314
  %call349 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx350 = getelementptr inbounds float, float* %call349, i32 2
  %214 = load float, float* %arrayidx350, align 4
  %215 = load float, float* %R23, align 4
  %mul351 = fmul float %214, %215
  %call352 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx353 = getelementptr inbounds float, float* %call352, i32 1
  %216 = load float, float* %arrayidx353, align 4
  %217 = load float, float* %R33, align 4
  %mul354 = fmul float %216, %217
  %sub355 = fsub float %mul351, %mul354
  %call356 = call float @_Z6btFabsf(float %sub355)
  %arrayidx357 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %218 = load float, float* %arrayidx357, align 4
  %219 = load float, float* %Q33, align 4
  %mul358 = fmul float %218, %219
  %arrayidx359 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %220 = load float, float* %arrayidx359, align 4
  %221 = load float, float* %Q23, align 4
  %mul360 = fmul float %220, %221
  %add361 = fadd float %mul358, %mul360
  %arrayidx362 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %222 = load float, float* %arrayidx362, align 4
  %223 = load float, float* %Q12, align 4
  %mul363 = fmul float %222, %223
  %add364 = fadd float %add361, %mul363
  %arrayidx365 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %224 = load float, float* %arrayidx365, align 4
  %225 = load float, float* %Q11, align 4
  %mul366 = fmul float %224, %225
  %add367 = fadd float %add364, %mul366
  %sub368 = fsub float %call356, %add367
  store float %sub368, float* %s2, align 4
  %226 = load float, float* %s2, align 4
  %cmp369 = fcmp ogt float %226, 0x3E80000000000000
  br i1 %cmp369, label %if.then370, label %if.end371

if.then370:                                       ; preds = %if.end348
  store i32 0, i32* %retval, align 4
  br label %return

if.end371:                                        ; preds = %if.end348
  %227 = load float, float* %R33, align 4
  %fneg372 = fneg float %227
  %228 = load float, float* %R33, align 4
  %fneg373 = fneg float %228
  %mul374 = fmul float %fneg372, %fneg373
  %add375 = fadd float 0.000000e+00, %mul374
  %229 = load float, float* %R23, align 4
  %230 = load float, float* %R23, align 4
  %mul376 = fmul float %229, %230
  %add377 = fadd float %add375, %mul376
  %call378 = call float @_Z6btSqrtf(float %add377)
  store float %call378, float* %l, align 4
  %231 = load float, float* %l, align 4
  %cmp379 = fcmp ogt float %231, 0x3E80000000000000
  br i1 %cmp379, label %if.then380, label %if.end405

if.then380:                                       ; preds = %if.end371
  %232 = load float, float* %l, align 4
  %233 = load float, float* %s2, align 4
  %div381 = fdiv float %233, %232
  store float %div381, float* %s2, align 4
  %234 = load float, float* %s2, align 4
  %mul382 = fmul float %234, 0x3FF0CCCCC0000000
  %235 = load float, float* %s, align 4
  %cmp383 = fcmp ogt float %mul382, %235
  br i1 %cmp383, label %if.then384, label %if.end404

if.then384:                                       ; preds = %if.then380
  %236 = load float, float* %s2, align 4
  store float %236, float* %s, align 4
  store float* null, float** %normalR, align 4
  %237 = load float, float* %l, align 4
  %div385 = fdiv float 0.000000e+00, %237
  %call386 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx387 = getelementptr inbounds float, float* %call386, i32 0
  store float %div385, float* %arrayidx387, align 4
  %238 = load float, float* %R33, align 4
  %fneg388 = fneg float %238
  %239 = load float, float* %l, align 4
  %div389 = fdiv float %fneg388, %239
  %call390 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx391 = getelementptr inbounds float, float* %call390, i32 1
  store float %div389, float* %arrayidx391, align 4
  %240 = load float, float* %R23, align 4
  %241 = load float, float* %l, align 4
  %div392 = fdiv float %240, %241
  %call393 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx394 = getelementptr inbounds float, float* %call393, i32 2
  store float %div392, float* %arrayidx394, align 4
  %call395 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx396 = getelementptr inbounds float, float* %call395, i32 2
  %242 = load float, float* %arrayidx396, align 4
  %243 = load float, float* %R23, align 4
  %mul397 = fmul float %242, %243
  %call398 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx399 = getelementptr inbounds float, float* %call398, i32 1
  %244 = load float, float* %arrayidx399, align 4
  %245 = load float, float* %R33, align 4
  %mul400 = fmul float %244, %245
  %sub401 = fsub float %mul397, %mul400
  %cmp402 = fcmp olt float %sub401, 0.000000e+00
  %conv403 = zext i1 %cmp402 to i32
  store i32 %conv403, i32* %invert_normal, align 4
  store i32 9, i32* %code, align 4
  br label %if.end404

if.end404:                                        ; preds = %if.then384, %if.then380
  br label %if.end405

if.end405:                                        ; preds = %if.end404, %if.end371
  %call406 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx407 = getelementptr inbounds float, float* %call406, i32 0
  %246 = load float, float* %arrayidx407, align 4
  %247 = load float, float* %R31, align 4
  %mul408 = fmul float %246, %247
  %call409 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx410 = getelementptr inbounds float, float* %call409, i32 2
  %248 = load float, float* %arrayidx410, align 4
  %249 = load float, float* %R11, align 4
  %mul411 = fmul float %248, %249
  %sub412 = fsub float %mul408, %mul411
  %call413 = call float @_Z6btFabsf(float %sub412)
  %arrayidx414 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %250 = load float, float* %arrayidx414, align 4
  %251 = load float, float* %Q31, align 4
  %mul415 = fmul float %250, %251
  %arrayidx416 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %252 = load float, float* %arrayidx416, align 4
  %253 = load float, float* %Q11, align 4
  %mul417 = fmul float %252, %253
  %add418 = fadd float %mul415, %mul417
  %arrayidx419 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %254 = load float, float* %arrayidx419, align 4
  %255 = load float, float* %Q23, align 4
  %mul420 = fmul float %254, %255
  %add421 = fadd float %add418, %mul420
  %arrayidx422 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %256 = load float, float* %arrayidx422, align 4
  %257 = load float, float* %Q22, align 4
  %mul423 = fmul float %256, %257
  %add424 = fadd float %add421, %mul423
  %sub425 = fsub float %call413, %add424
  store float %sub425, float* %s2, align 4
  %258 = load float, float* %s2, align 4
  %cmp426 = fcmp ogt float %258, 0x3E80000000000000
  br i1 %cmp426, label %if.then427, label %if.end428

if.then427:                                       ; preds = %if.end405
  store i32 0, i32* %retval, align 4
  br label %return

if.end428:                                        ; preds = %if.end405
  %259 = load float, float* %R31, align 4
  %260 = load float, float* %R31, align 4
  %mul429 = fmul float %259, %260
  %add430 = fadd float %mul429, 0.000000e+00
  %261 = load float, float* %R11, align 4
  %fneg431 = fneg float %261
  %262 = load float, float* %R11, align 4
  %fneg432 = fneg float %262
  %mul433 = fmul float %fneg431, %fneg432
  %add434 = fadd float %add430, %mul433
  %call435 = call float @_Z6btSqrtf(float %add434)
  store float %call435, float* %l, align 4
  %263 = load float, float* %l, align 4
  %cmp436 = fcmp ogt float %263, 0x3E80000000000000
  br i1 %cmp436, label %if.then437, label %if.end462

if.then437:                                       ; preds = %if.end428
  %264 = load float, float* %l, align 4
  %265 = load float, float* %s2, align 4
  %div438 = fdiv float %265, %264
  store float %div438, float* %s2, align 4
  %266 = load float, float* %s2, align 4
  %mul439 = fmul float %266, 0x3FF0CCCCC0000000
  %267 = load float, float* %s, align 4
  %cmp440 = fcmp ogt float %mul439, %267
  br i1 %cmp440, label %if.then441, label %if.end461

if.then441:                                       ; preds = %if.then437
  %268 = load float, float* %s2, align 4
  store float %268, float* %s, align 4
  store float* null, float** %normalR, align 4
  %269 = load float, float* %R31, align 4
  %270 = load float, float* %l, align 4
  %div442 = fdiv float %269, %270
  %call443 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx444 = getelementptr inbounds float, float* %call443, i32 0
  store float %div442, float* %arrayidx444, align 4
  %271 = load float, float* %l, align 4
  %div445 = fdiv float 0.000000e+00, %271
  %call446 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx447 = getelementptr inbounds float, float* %call446, i32 1
  store float %div445, float* %arrayidx447, align 4
  %272 = load float, float* %R11, align 4
  %fneg448 = fneg float %272
  %273 = load float, float* %l, align 4
  %div449 = fdiv float %fneg448, %273
  %call450 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx451 = getelementptr inbounds float, float* %call450, i32 2
  store float %div449, float* %arrayidx451, align 4
  %call452 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx453 = getelementptr inbounds float, float* %call452, i32 0
  %274 = load float, float* %arrayidx453, align 4
  %275 = load float, float* %R31, align 4
  %mul454 = fmul float %274, %275
  %call455 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx456 = getelementptr inbounds float, float* %call455, i32 2
  %276 = load float, float* %arrayidx456, align 4
  %277 = load float, float* %R11, align 4
  %mul457 = fmul float %276, %277
  %sub458 = fsub float %mul454, %mul457
  %cmp459 = fcmp olt float %sub458, 0.000000e+00
  %conv460 = zext i1 %cmp459 to i32
  store i32 %conv460, i32* %invert_normal, align 4
  store i32 10, i32* %code, align 4
  br label %if.end461

if.end461:                                        ; preds = %if.then441, %if.then437
  br label %if.end462

if.end462:                                        ; preds = %if.end461, %if.end428
  %call463 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx464 = getelementptr inbounds float, float* %call463, i32 0
  %278 = load float, float* %arrayidx464, align 4
  %279 = load float, float* %R32, align 4
  %mul465 = fmul float %278, %279
  %call466 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx467 = getelementptr inbounds float, float* %call466, i32 2
  %280 = load float, float* %arrayidx467, align 4
  %281 = load float, float* %R12, align 4
  %mul468 = fmul float %280, %281
  %sub469 = fsub float %mul465, %mul468
  %call470 = call float @_Z6btFabsf(float %sub469)
  %arrayidx471 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %282 = load float, float* %arrayidx471, align 4
  %283 = load float, float* %Q32, align 4
  %mul472 = fmul float %282, %283
  %arrayidx473 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %284 = load float, float* %arrayidx473, align 4
  %285 = load float, float* %Q12, align 4
  %mul474 = fmul float %284, %285
  %add475 = fadd float %mul472, %mul474
  %arrayidx476 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %286 = load float, float* %arrayidx476, align 4
  %287 = load float, float* %Q23, align 4
  %mul477 = fmul float %286, %287
  %add478 = fadd float %add475, %mul477
  %arrayidx479 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %288 = load float, float* %arrayidx479, align 4
  %289 = load float, float* %Q21, align 4
  %mul480 = fmul float %288, %289
  %add481 = fadd float %add478, %mul480
  %sub482 = fsub float %call470, %add481
  store float %sub482, float* %s2, align 4
  %290 = load float, float* %s2, align 4
  %cmp483 = fcmp ogt float %290, 0x3E80000000000000
  br i1 %cmp483, label %if.then484, label %if.end485

if.then484:                                       ; preds = %if.end462
  store i32 0, i32* %retval, align 4
  br label %return

if.end485:                                        ; preds = %if.end462
  %291 = load float, float* %R32, align 4
  %292 = load float, float* %R32, align 4
  %mul486 = fmul float %291, %292
  %add487 = fadd float %mul486, 0.000000e+00
  %293 = load float, float* %R12, align 4
  %fneg488 = fneg float %293
  %294 = load float, float* %R12, align 4
  %fneg489 = fneg float %294
  %mul490 = fmul float %fneg488, %fneg489
  %add491 = fadd float %add487, %mul490
  %call492 = call float @_Z6btSqrtf(float %add491)
  store float %call492, float* %l, align 4
  %295 = load float, float* %l, align 4
  %cmp493 = fcmp ogt float %295, 0x3E80000000000000
  br i1 %cmp493, label %if.then494, label %if.end519

if.then494:                                       ; preds = %if.end485
  %296 = load float, float* %l, align 4
  %297 = load float, float* %s2, align 4
  %div495 = fdiv float %297, %296
  store float %div495, float* %s2, align 4
  %298 = load float, float* %s2, align 4
  %mul496 = fmul float %298, 0x3FF0CCCCC0000000
  %299 = load float, float* %s, align 4
  %cmp497 = fcmp ogt float %mul496, %299
  br i1 %cmp497, label %if.then498, label %if.end518

if.then498:                                       ; preds = %if.then494
  %300 = load float, float* %s2, align 4
  store float %300, float* %s, align 4
  store float* null, float** %normalR, align 4
  %301 = load float, float* %R32, align 4
  %302 = load float, float* %l, align 4
  %div499 = fdiv float %301, %302
  %call500 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx501 = getelementptr inbounds float, float* %call500, i32 0
  store float %div499, float* %arrayidx501, align 4
  %303 = load float, float* %l, align 4
  %div502 = fdiv float 0.000000e+00, %303
  %call503 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx504 = getelementptr inbounds float, float* %call503, i32 1
  store float %div502, float* %arrayidx504, align 4
  %304 = load float, float* %R12, align 4
  %fneg505 = fneg float %304
  %305 = load float, float* %l, align 4
  %div506 = fdiv float %fneg505, %305
  %call507 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx508 = getelementptr inbounds float, float* %call507, i32 2
  store float %div506, float* %arrayidx508, align 4
  %call509 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx510 = getelementptr inbounds float, float* %call509, i32 0
  %306 = load float, float* %arrayidx510, align 4
  %307 = load float, float* %R32, align 4
  %mul511 = fmul float %306, %307
  %call512 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx513 = getelementptr inbounds float, float* %call512, i32 2
  %308 = load float, float* %arrayidx513, align 4
  %309 = load float, float* %R12, align 4
  %mul514 = fmul float %308, %309
  %sub515 = fsub float %mul511, %mul514
  %cmp516 = fcmp olt float %sub515, 0.000000e+00
  %conv517 = zext i1 %cmp516 to i32
  store i32 %conv517, i32* %invert_normal, align 4
  store i32 11, i32* %code, align 4
  br label %if.end518

if.end518:                                        ; preds = %if.then498, %if.then494
  br label %if.end519

if.end519:                                        ; preds = %if.end518, %if.end485
  %call520 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx521 = getelementptr inbounds float, float* %call520, i32 0
  %310 = load float, float* %arrayidx521, align 4
  %311 = load float, float* %R33, align 4
  %mul522 = fmul float %310, %311
  %call523 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx524 = getelementptr inbounds float, float* %call523, i32 2
  %312 = load float, float* %arrayidx524, align 4
  %313 = load float, float* %R13, align 4
  %mul525 = fmul float %312, %313
  %sub526 = fsub float %mul522, %mul525
  %call527 = call float @_Z6btFabsf(float %sub526)
  %arrayidx528 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %314 = load float, float* %arrayidx528, align 4
  %315 = load float, float* %Q33, align 4
  %mul529 = fmul float %314, %315
  %arrayidx530 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 2
  %316 = load float, float* %arrayidx530, align 4
  %317 = load float, float* %Q13, align 4
  %mul531 = fmul float %316, %317
  %add532 = fadd float %mul529, %mul531
  %arrayidx533 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %318 = load float, float* %arrayidx533, align 4
  %319 = load float, float* %Q22, align 4
  %mul534 = fmul float %318, %319
  %add535 = fadd float %add532, %mul534
  %arrayidx536 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %320 = load float, float* %arrayidx536, align 4
  %321 = load float, float* %Q21, align 4
  %mul537 = fmul float %320, %321
  %add538 = fadd float %add535, %mul537
  %sub539 = fsub float %call527, %add538
  store float %sub539, float* %s2, align 4
  %322 = load float, float* %s2, align 4
  %cmp540 = fcmp ogt float %322, 0x3E80000000000000
  br i1 %cmp540, label %if.then541, label %if.end542

if.then541:                                       ; preds = %if.end519
  store i32 0, i32* %retval, align 4
  br label %return

if.end542:                                        ; preds = %if.end519
  %323 = load float, float* %R33, align 4
  %324 = load float, float* %R33, align 4
  %mul543 = fmul float %323, %324
  %add544 = fadd float %mul543, 0.000000e+00
  %325 = load float, float* %R13, align 4
  %fneg545 = fneg float %325
  %326 = load float, float* %R13, align 4
  %fneg546 = fneg float %326
  %mul547 = fmul float %fneg545, %fneg546
  %add548 = fadd float %add544, %mul547
  %call549 = call float @_Z6btSqrtf(float %add548)
  store float %call549, float* %l, align 4
  %327 = load float, float* %l, align 4
  %cmp550 = fcmp ogt float %327, 0x3E80000000000000
  br i1 %cmp550, label %if.then551, label %if.end576

if.then551:                                       ; preds = %if.end542
  %328 = load float, float* %l, align 4
  %329 = load float, float* %s2, align 4
  %div552 = fdiv float %329, %328
  store float %div552, float* %s2, align 4
  %330 = load float, float* %s2, align 4
  %mul553 = fmul float %330, 0x3FF0CCCCC0000000
  %331 = load float, float* %s, align 4
  %cmp554 = fcmp ogt float %mul553, %331
  br i1 %cmp554, label %if.then555, label %if.end575

if.then555:                                       ; preds = %if.then551
  %332 = load float, float* %s2, align 4
  store float %332, float* %s, align 4
  store float* null, float** %normalR, align 4
  %333 = load float, float* %R33, align 4
  %334 = load float, float* %l, align 4
  %div556 = fdiv float %333, %334
  %call557 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx558 = getelementptr inbounds float, float* %call557, i32 0
  store float %div556, float* %arrayidx558, align 4
  %335 = load float, float* %l, align 4
  %div559 = fdiv float 0.000000e+00, %335
  %call560 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx561 = getelementptr inbounds float, float* %call560, i32 1
  store float %div559, float* %arrayidx561, align 4
  %336 = load float, float* %R13, align 4
  %fneg562 = fneg float %336
  %337 = load float, float* %l, align 4
  %div563 = fdiv float %fneg562, %337
  %call564 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx565 = getelementptr inbounds float, float* %call564, i32 2
  store float %div563, float* %arrayidx565, align 4
  %call566 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx567 = getelementptr inbounds float, float* %call566, i32 0
  %338 = load float, float* %arrayidx567, align 4
  %339 = load float, float* %R33, align 4
  %mul568 = fmul float %338, %339
  %call569 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx570 = getelementptr inbounds float, float* %call569, i32 2
  %340 = load float, float* %arrayidx570, align 4
  %341 = load float, float* %R13, align 4
  %mul571 = fmul float %340, %341
  %sub572 = fsub float %mul568, %mul571
  %cmp573 = fcmp olt float %sub572, 0.000000e+00
  %conv574 = zext i1 %cmp573 to i32
  store i32 %conv574, i32* %invert_normal, align 4
  store i32 12, i32* %code, align 4
  br label %if.end575

if.end575:                                        ; preds = %if.then555, %if.then551
  br label %if.end576

if.end576:                                        ; preds = %if.end575, %if.end542
  %call577 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx578 = getelementptr inbounds float, float* %call577, i32 1
  %342 = load float, float* %arrayidx578, align 4
  %343 = load float, float* %R11, align 4
  %mul579 = fmul float %342, %343
  %call580 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx581 = getelementptr inbounds float, float* %call580, i32 0
  %344 = load float, float* %arrayidx581, align 4
  %345 = load float, float* %R21, align 4
  %mul582 = fmul float %344, %345
  %sub583 = fsub float %mul579, %mul582
  %call584 = call float @_Z6btFabsf(float %sub583)
  %arrayidx585 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %346 = load float, float* %arrayidx585, align 4
  %347 = load float, float* %Q21, align 4
  %mul586 = fmul float %346, %347
  %arrayidx587 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %348 = load float, float* %arrayidx587, align 4
  %349 = load float, float* %Q11, align 4
  %mul588 = fmul float %348, %349
  %add589 = fadd float %mul586, %mul588
  %arrayidx590 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %350 = load float, float* %arrayidx590, align 4
  %351 = load float, float* %Q33, align 4
  %mul591 = fmul float %350, %351
  %add592 = fadd float %add589, %mul591
  %arrayidx593 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %352 = load float, float* %arrayidx593, align 4
  %353 = load float, float* %Q32, align 4
  %mul594 = fmul float %352, %353
  %add595 = fadd float %add592, %mul594
  %sub596 = fsub float %call584, %add595
  store float %sub596, float* %s2, align 4
  %354 = load float, float* %s2, align 4
  %cmp597 = fcmp ogt float %354, 0x3E80000000000000
  br i1 %cmp597, label %if.then598, label %if.end599

if.then598:                                       ; preds = %if.end576
  store i32 0, i32* %retval, align 4
  br label %return

if.end599:                                        ; preds = %if.end576
  %355 = load float, float* %R21, align 4
  %fneg600 = fneg float %355
  %356 = load float, float* %R21, align 4
  %fneg601 = fneg float %356
  %mul602 = fmul float %fneg600, %fneg601
  %357 = load float, float* %R11, align 4
  %358 = load float, float* %R11, align 4
  %mul603 = fmul float %357, %358
  %add604 = fadd float %mul602, %mul603
  %add605 = fadd float %add604, 0.000000e+00
  %call606 = call float @_Z6btSqrtf(float %add605)
  store float %call606, float* %l, align 4
  %359 = load float, float* %l, align 4
  %cmp607 = fcmp ogt float %359, 0x3E80000000000000
  br i1 %cmp607, label %if.then608, label %if.end633

if.then608:                                       ; preds = %if.end599
  %360 = load float, float* %l, align 4
  %361 = load float, float* %s2, align 4
  %div609 = fdiv float %361, %360
  store float %div609, float* %s2, align 4
  %362 = load float, float* %s2, align 4
  %mul610 = fmul float %362, 0x3FF0CCCCC0000000
  %363 = load float, float* %s, align 4
  %cmp611 = fcmp ogt float %mul610, %363
  br i1 %cmp611, label %if.then612, label %if.end632

if.then612:                                       ; preds = %if.then608
  %364 = load float, float* %s2, align 4
  store float %364, float* %s, align 4
  store float* null, float** %normalR, align 4
  %365 = load float, float* %R21, align 4
  %fneg613 = fneg float %365
  %366 = load float, float* %l, align 4
  %div614 = fdiv float %fneg613, %366
  %call615 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx616 = getelementptr inbounds float, float* %call615, i32 0
  store float %div614, float* %arrayidx616, align 4
  %367 = load float, float* %R11, align 4
  %368 = load float, float* %l, align 4
  %div617 = fdiv float %367, %368
  %call618 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx619 = getelementptr inbounds float, float* %call618, i32 1
  store float %div617, float* %arrayidx619, align 4
  %369 = load float, float* %l, align 4
  %div620 = fdiv float 0.000000e+00, %369
  %call621 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx622 = getelementptr inbounds float, float* %call621, i32 2
  store float %div620, float* %arrayidx622, align 4
  %call623 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx624 = getelementptr inbounds float, float* %call623, i32 1
  %370 = load float, float* %arrayidx624, align 4
  %371 = load float, float* %R11, align 4
  %mul625 = fmul float %370, %371
  %call626 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx627 = getelementptr inbounds float, float* %call626, i32 0
  %372 = load float, float* %arrayidx627, align 4
  %373 = load float, float* %R21, align 4
  %mul628 = fmul float %372, %373
  %sub629 = fsub float %mul625, %mul628
  %cmp630 = fcmp olt float %sub629, 0.000000e+00
  %conv631 = zext i1 %cmp630 to i32
  store i32 %conv631, i32* %invert_normal, align 4
  store i32 13, i32* %code, align 4
  br label %if.end632

if.end632:                                        ; preds = %if.then612, %if.then608
  br label %if.end633

if.end633:                                        ; preds = %if.end632, %if.end599
  %call634 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx635 = getelementptr inbounds float, float* %call634, i32 1
  %374 = load float, float* %arrayidx635, align 4
  %375 = load float, float* %R12, align 4
  %mul636 = fmul float %374, %375
  %call637 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx638 = getelementptr inbounds float, float* %call637, i32 0
  %376 = load float, float* %arrayidx638, align 4
  %377 = load float, float* %R22, align 4
  %mul639 = fmul float %376, %377
  %sub640 = fsub float %mul636, %mul639
  %call641 = call float @_Z6btFabsf(float %sub640)
  %arrayidx642 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %378 = load float, float* %arrayidx642, align 4
  %379 = load float, float* %Q22, align 4
  %mul643 = fmul float %378, %379
  %arrayidx644 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %380 = load float, float* %arrayidx644, align 4
  %381 = load float, float* %Q12, align 4
  %mul645 = fmul float %380, %381
  %add646 = fadd float %mul643, %mul645
  %arrayidx647 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %382 = load float, float* %arrayidx647, align 4
  %383 = load float, float* %Q33, align 4
  %mul648 = fmul float %382, %383
  %add649 = fadd float %add646, %mul648
  %arrayidx650 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 2
  %384 = load float, float* %arrayidx650, align 4
  %385 = load float, float* %Q31, align 4
  %mul651 = fmul float %384, %385
  %add652 = fadd float %add649, %mul651
  %sub653 = fsub float %call641, %add652
  store float %sub653, float* %s2, align 4
  %386 = load float, float* %s2, align 4
  %cmp654 = fcmp ogt float %386, 0x3E80000000000000
  br i1 %cmp654, label %if.then655, label %if.end656

if.then655:                                       ; preds = %if.end633
  store i32 0, i32* %retval, align 4
  br label %return

if.end656:                                        ; preds = %if.end633
  %387 = load float, float* %R22, align 4
  %fneg657 = fneg float %387
  %388 = load float, float* %R22, align 4
  %fneg658 = fneg float %388
  %mul659 = fmul float %fneg657, %fneg658
  %389 = load float, float* %R12, align 4
  %390 = load float, float* %R12, align 4
  %mul660 = fmul float %389, %390
  %add661 = fadd float %mul659, %mul660
  %add662 = fadd float %add661, 0.000000e+00
  %call663 = call float @_Z6btSqrtf(float %add662)
  store float %call663, float* %l, align 4
  %391 = load float, float* %l, align 4
  %cmp664 = fcmp ogt float %391, 0x3E80000000000000
  br i1 %cmp664, label %if.then665, label %if.end690

if.then665:                                       ; preds = %if.end656
  %392 = load float, float* %l, align 4
  %393 = load float, float* %s2, align 4
  %div666 = fdiv float %393, %392
  store float %div666, float* %s2, align 4
  %394 = load float, float* %s2, align 4
  %mul667 = fmul float %394, 0x3FF0CCCCC0000000
  %395 = load float, float* %s, align 4
  %cmp668 = fcmp ogt float %mul667, %395
  br i1 %cmp668, label %if.then669, label %if.end689

if.then669:                                       ; preds = %if.then665
  %396 = load float, float* %s2, align 4
  store float %396, float* %s, align 4
  store float* null, float** %normalR, align 4
  %397 = load float, float* %R22, align 4
  %fneg670 = fneg float %397
  %398 = load float, float* %l, align 4
  %div671 = fdiv float %fneg670, %398
  %call672 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx673 = getelementptr inbounds float, float* %call672, i32 0
  store float %div671, float* %arrayidx673, align 4
  %399 = load float, float* %R12, align 4
  %400 = load float, float* %l, align 4
  %div674 = fdiv float %399, %400
  %call675 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx676 = getelementptr inbounds float, float* %call675, i32 1
  store float %div674, float* %arrayidx676, align 4
  %401 = load float, float* %l, align 4
  %div677 = fdiv float 0.000000e+00, %401
  %call678 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx679 = getelementptr inbounds float, float* %call678, i32 2
  store float %div677, float* %arrayidx679, align 4
  %call680 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx681 = getelementptr inbounds float, float* %call680, i32 1
  %402 = load float, float* %arrayidx681, align 4
  %403 = load float, float* %R12, align 4
  %mul682 = fmul float %402, %403
  %call683 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx684 = getelementptr inbounds float, float* %call683, i32 0
  %404 = load float, float* %arrayidx684, align 4
  %405 = load float, float* %R22, align 4
  %mul685 = fmul float %404, %405
  %sub686 = fsub float %mul682, %mul685
  %cmp687 = fcmp olt float %sub686, 0.000000e+00
  %conv688 = zext i1 %cmp687 to i32
  store i32 %conv688, i32* %invert_normal, align 4
  store i32 14, i32* %code, align 4
  br label %if.end689

if.end689:                                        ; preds = %if.then669, %if.then665
  br label %if.end690

if.end690:                                        ; preds = %if.end689, %if.end656
  %call691 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx692 = getelementptr inbounds float, float* %call691, i32 1
  %406 = load float, float* %arrayidx692, align 4
  %407 = load float, float* %R13, align 4
  %mul693 = fmul float %406, %407
  %call694 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx695 = getelementptr inbounds float, float* %call694, i32 0
  %408 = load float, float* %arrayidx695, align 4
  %409 = load float, float* %R23, align 4
  %mul696 = fmul float %408, %409
  %sub697 = fsub float %mul693, %mul696
  %call698 = call float @_Z6btFabsf(float %sub697)
  %arrayidx699 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  %410 = load float, float* %arrayidx699, align 4
  %411 = load float, float* %Q23, align 4
  %mul700 = fmul float %410, %411
  %arrayidx701 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 1
  %412 = load float, float* %arrayidx701, align 4
  %413 = load float, float* %Q13, align 4
  %mul702 = fmul float %412, %413
  %add703 = fadd float %mul700, %mul702
  %arrayidx704 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  %414 = load float, float* %arrayidx704, align 4
  %415 = load float, float* %Q32, align 4
  %mul705 = fmul float %414, %415
  %add706 = fadd float %add703, %mul705
  %arrayidx707 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 1
  %416 = load float, float* %arrayidx707, align 4
  %417 = load float, float* %Q31, align 4
  %mul708 = fmul float %416, %417
  %add709 = fadd float %add706, %mul708
  %sub710 = fsub float %call698, %add709
  store float %sub710, float* %s2, align 4
  %418 = load float, float* %s2, align 4
  %cmp711 = fcmp ogt float %418, 0x3E80000000000000
  br i1 %cmp711, label %if.then712, label %if.end713

if.then712:                                       ; preds = %if.end690
  store i32 0, i32* %retval, align 4
  br label %return

if.end713:                                        ; preds = %if.end690
  %419 = load float, float* %R23, align 4
  %fneg714 = fneg float %419
  %420 = load float, float* %R23, align 4
  %fneg715 = fneg float %420
  %mul716 = fmul float %fneg714, %fneg715
  %421 = load float, float* %R13, align 4
  %422 = load float, float* %R13, align 4
  %mul717 = fmul float %421, %422
  %add718 = fadd float %mul716, %mul717
  %add719 = fadd float %add718, 0.000000e+00
  %call720 = call float @_Z6btSqrtf(float %add719)
  store float %call720, float* %l, align 4
  %423 = load float, float* %l, align 4
  %cmp721 = fcmp ogt float %423, 0x3E80000000000000
  br i1 %cmp721, label %if.then722, label %if.end747

if.then722:                                       ; preds = %if.end713
  %424 = load float, float* %l, align 4
  %425 = load float, float* %s2, align 4
  %div723 = fdiv float %425, %424
  store float %div723, float* %s2, align 4
  %426 = load float, float* %s2, align 4
  %mul724 = fmul float %426, 0x3FF0CCCCC0000000
  %427 = load float, float* %s, align 4
  %cmp725 = fcmp ogt float %mul724, %427
  br i1 %cmp725, label %if.then726, label %if.end746

if.then726:                                       ; preds = %if.then722
  %428 = load float, float* %s2, align 4
  store float %428, float* %s, align 4
  store float* null, float** %normalR, align 4
  %429 = load float, float* %R23, align 4
  %fneg727 = fneg float %429
  %430 = load float, float* %l, align 4
  %div728 = fdiv float %fneg727, %430
  %call729 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx730 = getelementptr inbounds float, float* %call729, i32 0
  store float %div728, float* %arrayidx730, align 4
  %431 = load float, float* %R13, align 4
  %432 = load float, float* %l, align 4
  %div731 = fdiv float %431, %432
  %call732 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx733 = getelementptr inbounds float, float* %call732, i32 1
  store float %div731, float* %arrayidx733, align 4
  %433 = load float, float* %l, align 4
  %div734 = fdiv float 0.000000e+00, %433
  %call735 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %arrayidx736 = getelementptr inbounds float, float* %call735, i32 2
  store float %div734, float* %arrayidx736, align 4
  %call737 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx738 = getelementptr inbounds float, float* %call737, i32 1
  %434 = load float, float* %arrayidx738, align 4
  %435 = load float, float* %R13, align 4
  %mul739 = fmul float %434, %435
  %call740 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pp)
  %arrayidx741 = getelementptr inbounds float, float* %call740, i32 0
  %436 = load float, float* %arrayidx741, align 4
  %437 = load float, float* %R23, align 4
  %mul742 = fmul float %436, %437
  %sub743 = fsub float %mul739, %mul742
  %cmp744 = fcmp olt float %sub743, 0.000000e+00
  %conv745 = zext i1 %cmp744 to i32
  store i32 %conv745, i32* %invert_normal, align 4
  store i32 15, i32* %code, align 4
  br label %if.end746

if.end746:                                        ; preds = %if.then726, %if.then722
  br label %if.end747

if.end747:                                        ; preds = %if.end746, %if.end713
  %438 = load i32, i32* %code, align 4
  %tobool = icmp ne i32 %438, 0
  br i1 %tobool, label %if.end749, label %if.then748

if.then748:                                       ; preds = %if.end747
  store i32 0, i32* %retval, align 4
  br label %return

if.end749:                                        ; preds = %if.end747
  %439 = load float*, float** %normalR, align 4
  %tobool750 = icmp ne float* %439, null
  br i1 %tobool750, label %if.then751, label %if.else

if.then751:                                       ; preds = %if.end749
  %440 = load float*, float** %normalR, align 4
  %arrayidx752 = getelementptr inbounds float, float* %440, i32 0
  %441 = load float, float* %arrayidx752, align 4
  %442 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call753 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %442)
  %arrayidx754 = getelementptr inbounds float, float* %call753, i32 0
  store float %441, float* %arrayidx754, align 4
  %443 = load float*, float** %normalR, align 4
  %arrayidx755 = getelementptr inbounds float, float* %443, i32 4
  %444 = load float, float* %arrayidx755, align 4
  %445 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call756 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %445)
  %arrayidx757 = getelementptr inbounds float, float* %call756, i32 1
  store float %444, float* %arrayidx757, align 4
  %446 = load float*, float** %normalR, align 4
  %arrayidx758 = getelementptr inbounds float, float* %446, i32 8
  %447 = load float, float* %arrayidx758, align 4
  %448 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call759 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %448)
  %arrayidx760 = getelementptr inbounds float, float* %call759, i32 2
  store float %447, float* %arrayidx760, align 4
  br label %if.end775

if.else:                                          ; preds = %if.end749
  %449 = load float*, float** %R1.addr, align 4
  %call761 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call762 = call float @_ZL4dDOTPKfS0_(float* %449, float* %call761)
  %450 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call763 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %450)
  %arrayidx764 = getelementptr inbounds float, float* %call763, i32 0
  store float %call762, float* %arrayidx764, align 4
  %451 = load float*, float** %R1.addr, align 4
  %add.ptr765 = getelementptr inbounds float, float* %451, i32 4
  %call766 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call767 = call float @_ZL4dDOTPKfS0_(float* %add.ptr765, float* %call766)
  %452 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call768 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %452)
  %arrayidx769 = getelementptr inbounds float, float* %call768, i32 1
  store float %call767, float* %arrayidx769, align 4
  %453 = load float*, float** %R1.addr, align 4
  %add.ptr770 = getelementptr inbounds float, float* %453, i32 8
  %call771 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normalC)
  %call772 = call float @_ZL4dDOTPKfS0_(float* %add.ptr770, float* %call771)
  %454 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call773 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %454)
  %arrayidx774 = getelementptr inbounds float, float* %call773, i32 2
  store float %call772, float* %arrayidx774, align 4
  br label %if.end775

if.end775:                                        ; preds = %if.else, %if.then751
  %455 = load i32, i32* %invert_normal, align 4
  %tobool776 = icmp ne i32 %455, 0
  br i1 %tobool776, label %if.then777, label %if.end793

if.then777:                                       ; preds = %if.end775
  %456 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call778 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %456)
  %arrayidx779 = getelementptr inbounds float, float* %call778, i32 0
  %457 = load float, float* %arrayidx779, align 4
  %fneg780 = fneg float %457
  %458 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call781 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %458)
  %arrayidx782 = getelementptr inbounds float, float* %call781, i32 0
  store float %fneg780, float* %arrayidx782, align 4
  %459 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call783 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %459)
  %arrayidx784 = getelementptr inbounds float, float* %call783, i32 1
  %460 = load float, float* %arrayidx784, align 4
  %fneg785 = fneg float %460
  %461 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call786 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %461)
  %arrayidx787 = getelementptr inbounds float, float* %call786, i32 1
  store float %fneg785, float* %arrayidx787, align 4
  %462 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call788 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %462)
  %arrayidx789 = getelementptr inbounds float, float* %call788, i32 2
  %463 = load float, float* %arrayidx789, align 4
  %fneg790 = fneg float %463
  %464 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call791 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %464)
  %arrayidx792 = getelementptr inbounds float, float* %call791, i32 2
  store float %fneg790, float* %arrayidx792, align 4
  br label %if.end793

if.end793:                                        ; preds = %if.then777, %if.end775
  %465 = load float, float* %s, align 4
  %fneg794 = fneg float %465
  %466 = load float*, float** %depth.addr, align 4
  store float %fneg794, float* %466, align 4
  %467 = load i32, i32* %code, align 4
  %cmp795 = icmp sgt i32 %467, 6
  br i1 %cmp795, label %if.then796, label %if.end919

if.then796:                                       ; preds = %if.end793
  %call797 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then796
  %468 = load i32, i32* %i, align 4
  %cmp798 = icmp slt i32 %468, 3
  br i1 %cmp798, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %469 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %call799 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %469)
  %470 = load i32, i32* %i, align 4
  %arrayidx800 = getelementptr inbounds float, float* %call799, i32 %470
  %471 = load float, float* %arrayidx800, align 4
  %call801 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %472 = load i32, i32* %i, align 4
  %arrayidx802 = getelementptr inbounds float, float* %call801, i32 %472
  store float %471, float* %arrayidx802, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %473 = load i32, i32* %i, align 4
  %inc = add nsw i32 %473, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond803

for.cond803:                                      ; preds = %for.inc825, %for.end
  %474 = load i32, i32* %j, align 4
  %cmp804 = icmp slt i32 %474, 3
  br i1 %cmp804, label %for.body805, label %for.end827

for.body805:                                      ; preds = %for.cond803
  %475 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call806 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %475)
  %476 = load float*, float** %R1.addr, align 4
  %477 = load i32, i32* %j, align 4
  %add.ptr807 = getelementptr inbounds float, float* %476, i32 %477
  %call808 = call float @_ZL6dDOT14PKfS0_(float* %call806, float* %add.ptr807)
  %cmp809 = fcmp ogt float %call808, 0.000000e+00
  %478 = zext i1 %cmp809 to i64
  %cond = select i1 %cmp809, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %sign, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond810

for.cond810:                                      ; preds = %for.inc822, %for.body805
  %479 = load i32, i32* %i, align 4
  %cmp811 = icmp slt i32 %479, 3
  br i1 %cmp811, label %for.body812, label %for.end824

for.body812:                                      ; preds = %for.cond810
  %480 = load float, float* %sign, align 4
  %481 = load i32, i32* %j, align 4
  %arrayidx813 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 %481
  %482 = load float, float* %arrayidx813, align 4
  %mul814 = fmul float %480, %482
  %483 = load float*, float** %R1.addr, align 4
  %484 = load i32, i32* %i, align 4
  %mul815 = mul nsw i32 %484, 4
  %485 = load i32, i32* %j, align 4
  %add816 = add nsw i32 %mul815, %485
  %arrayidx817 = getelementptr inbounds float, float* %483, i32 %add816
  %486 = load float, float* %arrayidx817, align 4
  %mul818 = fmul float %mul814, %486
  %call819 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %487 = load i32, i32* %i, align 4
  %arrayidx820 = getelementptr inbounds float, float* %call819, i32 %487
  %488 = load float, float* %arrayidx820, align 4
  %add821 = fadd float %488, %mul818
  store float %add821, float* %arrayidx820, align 4
  br label %for.inc822

for.inc822:                                       ; preds = %for.body812
  %489 = load i32, i32* %i, align 4
  %inc823 = add nsw i32 %489, 1
  store i32 %inc823, i32* %i, align 4
  br label %for.cond810

for.end824:                                       ; preds = %for.cond810
  br label %for.inc825

for.inc825:                                       ; preds = %for.end824
  %490 = load i32, i32* %j, align 4
  %inc826 = add nsw i32 %490, 1
  store i32 %inc826, i32* %j, align 4
  br label %for.cond803

for.end827:                                       ; preds = %for.cond803
  %call828 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  store i32 0, i32* %i, align 4
  br label %for.cond829

for.cond829:                                      ; preds = %for.inc836, %for.end827
  %491 = load i32, i32* %i, align 4
  %cmp830 = icmp slt i32 %491, 3
  br i1 %cmp830, label %for.body831, label %for.end838

for.body831:                                      ; preds = %for.cond829
  %492 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %call832 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %492)
  %493 = load i32, i32* %i, align 4
  %arrayidx833 = getelementptr inbounds float, float* %call832, i32 %493
  %494 = load float, float* %arrayidx833, align 4
  %call834 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %495 = load i32, i32* %i, align 4
  %arrayidx835 = getelementptr inbounds float, float* %call834, i32 %495
  store float %494, float* %arrayidx835, align 4
  br label %for.inc836

for.inc836:                                       ; preds = %for.body831
  %496 = load i32, i32* %i, align 4
  %inc837 = add nsw i32 %496, 1
  store i32 %inc837, i32* %i, align 4
  br label %for.cond829

for.end838:                                       ; preds = %for.cond829
  store i32 0, i32* %j, align 4
  br label %for.cond839

for.cond839:                                      ; preds = %for.inc862, %for.end838
  %497 = load i32, i32* %j, align 4
  %cmp840 = icmp slt i32 %497, 3
  br i1 %cmp840, label %for.body841, label %for.end864

for.body841:                                      ; preds = %for.cond839
  %498 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call842 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %498)
  %499 = load float*, float** %R2.addr, align 4
  %500 = load i32, i32* %j, align 4
  %add.ptr843 = getelementptr inbounds float, float* %499, i32 %500
  %call844 = call float @_ZL6dDOT14PKfS0_(float* %call842, float* %add.ptr843)
  %cmp845 = fcmp ogt float %call844, 0.000000e+00
  %501 = zext i1 %cmp845 to i64
  %cond846 = select i1 %cmp845, float -1.000000e+00, float 1.000000e+00
  store float %cond846, float* %sign, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond847

for.cond847:                                      ; preds = %for.inc859, %for.body841
  %502 = load i32, i32* %i, align 4
  %cmp848 = icmp slt i32 %502, 3
  br i1 %cmp848, label %for.body849, label %for.end861

for.body849:                                      ; preds = %for.cond847
  %503 = load float, float* %sign, align 4
  %504 = load i32, i32* %j, align 4
  %arrayidx850 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 %504
  %505 = load float, float* %arrayidx850, align 4
  %mul851 = fmul float %503, %505
  %506 = load float*, float** %R2.addr, align 4
  %507 = load i32, i32* %i, align 4
  %mul852 = mul nsw i32 %507, 4
  %508 = load i32, i32* %j, align 4
  %add853 = add nsw i32 %mul852, %508
  %arrayidx854 = getelementptr inbounds float, float* %506, i32 %add853
  %509 = load float, float* %arrayidx854, align 4
  %mul855 = fmul float %mul851, %509
  %call856 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %510 = load i32, i32* %i, align 4
  %arrayidx857 = getelementptr inbounds float, float* %call856, i32 %510
  %511 = load float, float* %arrayidx857, align 4
  %add858 = fadd float %511, %mul855
  store float %add858, float* %arrayidx857, align 4
  br label %for.inc859

for.inc859:                                       ; preds = %for.body849
  %512 = load i32, i32* %i, align 4
  %inc860 = add nsw i32 %512, 1
  store i32 %inc860, i32* %i, align 4
  br label %for.cond847

for.end861:                                       ; preds = %for.cond847
  br label %for.inc862

for.inc862:                                       ; preds = %for.end861
  %513 = load i32, i32* %j, align 4
  %inc863 = add nsw i32 %513, 1
  store i32 %inc863, i32* %j, align 4
  br label %for.cond839

for.end864:                                       ; preds = %for.cond839
  %call865 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ua)
  %call866 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ub)
  store i32 0, i32* %i, align 4
  br label %for.cond867

for.cond867:                                      ; preds = %for.inc877, %for.end864
  %514 = load i32, i32* %i, align 4
  %cmp868 = icmp slt i32 %514, 3
  br i1 %cmp868, label %for.body869, label %for.end879

for.body869:                                      ; preds = %for.cond867
  %515 = load float*, float** %R1.addr, align 4
  %516 = load i32, i32* %code, align 4
  %sub870 = sub nsw i32 %516, 7
  %div871 = sdiv i32 %sub870, 3
  %517 = load i32, i32* %i, align 4
  %mul872 = mul nsw i32 %517, 4
  %add873 = add nsw i32 %div871, %mul872
  %arrayidx874 = getelementptr inbounds float, float* %515, i32 %add873
  %518 = load float, float* %arrayidx874, align 4
  %call875 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ua)
  %519 = load i32, i32* %i, align 4
  %arrayidx876 = getelementptr inbounds float, float* %call875, i32 %519
  store float %518, float* %arrayidx876, align 4
  br label %for.inc877

for.inc877:                                       ; preds = %for.body869
  %520 = load i32, i32* %i, align 4
  %inc878 = add nsw i32 %520, 1
  store i32 %inc878, i32* %i, align 4
  br label %for.cond867

for.end879:                                       ; preds = %for.cond867
  store i32 0, i32* %i, align 4
  br label %for.cond880

for.cond880:                                      ; preds = %for.inc889, %for.end879
  %521 = load i32, i32* %i, align 4
  %cmp881 = icmp slt i32 %521, 3
  br i1 %cmp881, label %for.body882, label %for.end891

for.body882:                                      ; preds = %for.cond880
  %522 = load float*, float** %R2.addr, align 4
  %523 = load i32, i32* %code, align 4
  %sub883 = sub nsw i32 %523, 7
  %rem = srem i32 %sub883, 3
  %524 = load i32, i32* %i, align 4
  %mul884 = mul nsw i32 %524, 4
  %add885 = add nsw i32 %rem, %mul884
  %arrayidx886 = getelementptr inbounds float, float* %522, i32 %add885
  %525 = load float, float* %arrayidx886, align 4
  %call887 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ub)
  %526 = load i32, i32* %i, align 4
  %arrayidx888 = getelementptr inbounds float, float* %call887, i32 %526
  store float %525, float* %arrayidx888, align 4
  br label %for.inc889

for.inc889:                                       ; preds = %for.body882
  %527 = load i32, i32* %i, align 4
  %inc890 = add nsw i32 %527, 1
  store i32 %inc890, i32* %i, align 4
  br label %for.cond880

for.end891:                                       ; preds = %for.cond880
  call void @_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_(%class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %ua, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %ub, float* %alpha, float* %beta)
  store i32 0, i32* %i, align 4
  br label %for.cond892

for.cond892:                                      ; preds = %for.inc901, %for.end891
  %528 = load i32, i32* %i, align 4
  %cmp893 = icmp slt i32 %528, 3
  br i1 %cmp893, label %for.body894, label %for.end903

for.body894:                                      ; preds = %for.cond892
  %call895 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ua)
  %529 = load i32, i32* %i, align 4
  %arrayidx896 = getelementptr inbounds float, float* %call895, i32 %529
  %530 = load float, float* %arrayidx896, align 4
  %531 = load float, float* %alpha, align 4
  %mul897 = fmul float %530, %531
  %call898 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pa)
  %532 = load i32, i32* %i, align 4
  %arrayidx899 = getelementptr inbounds float, float* %call898, i32 %532
  %533 = load float, float* %arrayidx899, align 4
  %add900 = fadd float %533, %mul897
  store float %add900, float* %arrayidx899, align 4
  br label %for.inc901

for.inc901:                                       ; preds = %for.body894
  %534 = load i32, i32* %i, align 4
  %inc902 = add nsw i32 %534, 1
  store i32 %inc902, i32* %i, align 4
  br label %for.cond892

for.end903:                                       ; preds = %for.cond892
  store i32 0, i32* %i, align 4
  br label %for.cond904

for.cond904:                                      ; preds = %for.inc913, %for.end903
  %535 = load i32, i32* %i, align 4
  %cmp905 = icmp slt i32 %535, 3
  br i1 %cmp905, label %for.body906, label %for.end915

for.body906:                                      ; preds = %for.cond904
  %call907 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ub)
  %536 = load i32, i32* %i, align 4
  %arrayidx908 = getelementptr inbounds float, float* %call907, i32 %536
  %537 = load float, float* %arrayidx908, align 4
  %538 = load float, float* %beta, align 4
  %mul909 = fmul float %537, %538
  %call910 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pb)
  %539 = load i32, i32* %i, align 4
  %arrayidx911 = getelementptr inbounds float, float* %call910, i32 %539
  %540 = load float, float* %arrayidx911, align 4
  %add912 = fadd float %540, %mul909
  store float %add912, float* %arrayidx911, align 4
  br label %for.inc913

for.inc913:                                       ; preds = %for.body906
  %541 = load i32, i32* %i, align 4
  %inc914 = add nsw i32 %541, 1
  store i32 %inc914, i32* %i, align 4
  br label %for.cond904

for.end915:                                       ; preds = %for.cond904
  %call916 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld)
  %542 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %543 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp917, %class.btVector3* nonnull align 4 dereferenceable(16) %543)
  %544 = load float*, float** %depth.addr, align 4
  %545 = load float, float* %544, align 4
  %fneg918 = fneg float %545
  %546 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %542 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %546, align 4
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %547 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %547(%"struct.btDiscreteCollisionDetectorInterface::Result"* %542, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp917, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, float %fneg918)
  %548 = load i32, i32* %code, align 4
  %549 = load i32*, i32** %return_code.addr, align 4
  store i32 %548, i32* %549, align 4
  store i32 1, i32* %retval, align 4
  br label %return

if.end919:                                        ; preds = %if.end793
  %550 = load i32, i32* %code, align 4
  %cmp922 = icmp sle i32 %550, 3
  br i1 %cmp922, label %if.then923, label %if.else927

if.then923:                                       ; preds = %if.end919
  %551 = load float*, float** %R1.addr, align 4
  store float* %551, float** %Ra, align 4
  %552 = load float*, float** %R2.addr, align 4
  store float* %552, float** %Rb, align 4
  %553 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %call924 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %553)
  store float* %call924, float** %pa920, align 4
  %554 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %call925 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %554)
  store float* %call925, float** %pb921, align 4
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float* %arraydecay, float** %Sa, align 4
  %arraydecay926 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float* %arraydecay926, float** %Sb, align 4
  br label %if.end932

if.else927:                                       ; preds = %if.end919
  %555 = load float*, float** %R2.addr, align 4
  store float* %555, float** %Ra, align 4
  %556 = load float*, float** %R1.addr, align 4
  store float* %556, float** %Rb, align 4
  %557 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %call928 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %557)
  store float* %call928, float** %pa920, align 4
  %558 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %call929 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %558)
  store float* %call929, float** %pb921, align 4
  %arraydecay930 = getelementptr inbounds [3 x float], [3 x float]* %B, i32 0, i32 0
  store float* %arraydecay930, float** %Sa, align 4
  %arraydecay931 = getelementptr inbounds [3 x float], [3 x float]* %A, i32 0, i32 0
  store float* %arraydecay931, float** %Sb, align 4
  br label %if.end932

if.end932:                                        ; preds = %if.else927, %if.then923
  %call933 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal2)
  %call934 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nr)
  %call935 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %anr)
  %559 = load i32, i32* %code, align 4
  %cmp936 = icmp sle i32 %559, 3
  br i1 %cmp936, label %if.then937, label %if.else950

if.then937:                                       ; preds = %if.end932
  %560 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call938 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %560)
  %arrayidx939 = getelementptr inbounds float, float* %call938, i32 0
  %561 = load float, float* %arrayidx939, align 4
  %call940 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx941 = getelementptr inbounds float, float* %call940, i32 0
  store float %561, float* %arrayidx941, align 4
  %562 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call942 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %562)
  %arrayidx943 = getelementptr inbounds float, float* %call942, i32 1
  %563 = load float, float* %arrayidx943, align 4
  %call944 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx945 = getelementptr inbounds float, float* %call944, i32 1
  store float %563, float* %arrayidx945, align 4
  %564 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call946 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %564)
  %arrayidx947 = getelementptr inbounds float, float* %call946, i32 2
  %565 = load float, float* %arrayidx947, align 4
  %call948 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx949 = getelementptr inbounds float, float* %call948, i32 2
  store float %565, float* %arrayidx949, align 4
  br label %if.end966

if.else950:                                       ; preds = %if.end932
  %566 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call951 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %566)
  %arrayidx952 = getelementptr inbounds float, float* %call951, i32 0
  %567 = load float, float* %arrayidx952, align 4
  %fneg953 = fneg float %567
  %call954 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx955 = getelementptr inbounds float, float* %call954, i32 0
  store float %fneg953, float* %arrayidx955, align 4
  %568 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call956 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %568)
  %arrayidx957 = getelementptr inbounds float, float* %call956, i32 1
  %569 = load float, float* %arrayidx957, align 4
  %fneg958 = fneg float %569
  %call959 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx960 = getelementptr inbounds float, float* %call959, i32 1
  store float %fneg958, float* %arrayidx960, align 4
  %570 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call961 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %570)
  %arrayidx962 = getelementptr inbounds float, float* %call961, i32 2
  %571 = load float, float* %arrayidx962, align 4
  %fneg963 = fneg float %571
  %call964 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arrayidx965 = getelementptr inbounds float, float* %call964, i32 2
  store float %fneg963, float* %arrayidx965, align 4
  br label %if.end966

if.end966:                                        ; preds = %if.else950, %if.then937
  %572 = load float*, float** %Rb, align 4
  %call967 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call968 = call float @_ZL6dDOT41PKfS0_(float* %572, float* %call967)
  %call969 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx970 = getelementptr inbounds float, float* %call969, i32 0
  store float %call968, float* %arrayidx970, align 4
  %573 = load float*, float** %Rb, align 4
  %add.ptr971 = getelementptr inbounds float, float* %573, i32 1
  %call972 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call973 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr971, float* %call972)
  %call974 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx975 = getelementptr inbounds float, float* %call974, i32 1
  store float %call973, float* %arrayidx975, align 4
  %574 = load float*, float** %Rb, align 4
  %add.ptr976 = getelementptr inbounds float, float* %574, i32 2
  %call977 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %call978 = call float @_ZL6dDOT41PKfS0_(float* %add.ptr976, float* %call977)
  %call979 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx980 = getelementptr inbounds float, float* %call979, i32 2
  store float %call978, float* %arrayidx980, align 4
  %call981 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx982 = getelementptr inbounds float, float* %call981, i32 0
  %575 = load float, float* %arrayidx982, align 4
  %call983 = call float @_Z6btFabsf(float %575)
  %call984 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx985 = getelementptr inbounds float, float* %call984, i32 0
  store float %call983, float* %arrayidx985, align 4
  %call986 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx987 = getelementptr inbounds float, float* %call986, i32 1
  %576 = load float, float* %arrayidx987, align 4
  %call988 = call float @_Z6btFabsf(float %576)
  %call989 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx990 = getelementptr inbounds float, float* %call989, i32 1
  store float %call988, float* %arrayidx990, align 4
  %call991 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %arrayidx992 = getelementptr inbounds float, float* %call991, i32 2
  %577 = load float, float* %arrayidx992, align 4
  %call993 = call float @_Z6btFabsf(float %577)
  %call994 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx995 = getelementptr inbounds float, float* %call994, i32 2
  store float %call993, float* %arrayidx995, align 4
  %call996 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx997 = getelementptr inbounds float, float* %call996, i32 1
  %578 = load float, float* %arrayidx997, align 4
  %call998 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx999 = getelementptr inbounds float, float* %call998, i32 0
  %579 = load float, float* %arrayidx999, align 4
  %cmp1000 = fcmp ogt float %578, %579
  br i1 %cmp1000, label %if.then1001, label %if.else1010

if.then1001:                                      ; preds = %if.end966
  %call1002 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1003 = getelementptr inbounds float, float* %call1002, i32 1
  %580 = load float, float* %arrayidx1003, align 4
  %call1004 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1005 = getelementptr inbounds float, float* %call1004, i32 2
  %581 = load float, float* %arrayidx1005, align 4
  %cmp1006 = fcmp ogt float %580, %581
  br i1 %cmp1006, label %if.then1007, label %if.else1008

if.then1007:                                      ; preds = %if.then1001
  store i32 0, i32* %a1, align 4
  store i32 1, i32* %lanr, align 4
  store i32 2, i32* %a2, align 4
  br label %if.end1009

if.else1008:                                      ; preds = %if.then1001
  store i32 0, i32* %a1, align 4
  store i32 1, i32* %a2, align 4
  store i32 2, i32* %lanr, align 4
  br label %if.end1009

if.end1009:                                       ; preds = %if.else1008, %if.then1007
  br label %if.end1019

if.else1010:                                      ; preds = %if.end966
  %call1011 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1012 = getelementptr inbounds float, float* %call1011, i32 0
  %582 = load float, float* %arrayidx1012, align 4
  %call1013 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %anr)
  %arrayidx1014 = getelementptr inbounds float, float* %call1013, i32 2
  %583 = load float, float* %arrayidx1014, align 4
  %cmp1015 = fcmp ogt float %582, %583
  br i1 %cmp1015, label %if.then1016, label %if.else1017

if.then1016:                                      ; preds = %if.else1010
  store i32 0, i32* %lanr, align 4
  store i32 1, i32* %a1, align 4
  store i32 2, i32* %a2, align 4
  br label %if.end1018

if.else1017:                                      ; preds = %if.else1010
  store i32 0, i32* %a1, align 4
  store i32 1, i32* %a2, align 4
  store i32 2, i32* %lanr, align 4
  br label %if.end1018

if.end1018:                                       ; preds = %if.else1017, %if.then1016
  br label %if.end1019

if.end1019:                                       ; preds = %if.end1018, %if.end1009
  %call1020 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  %call1021 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %nr)
  %584 = load i32, i32* %lanr, align 4
  %arrayidx1022 = getelementptr inbounds float, float* %call1021, i32 %584
  %585 = load float, float* %arrayidx1022, align 4
  %cmp1023 = fcmp olt float %585, 0.000000e+00
  br i1 %cmp1023, label %if.then1024, label %if.else1042

if.then1024:                                      ; preds = %if.end1019
  store i32 0, i32* %i, align 4
  br label %for.cond1025

for.cond1025:                                     ; preds = %for.inc1039, %if.then1024
  %586 = load i32, i32* %i, align 4
  %cmp1026 = icmp slt i32 %586, 3
  br i1 %cmp1026, label %for.body1027, label %for.end1041

for.body1027:                                     ; preds = %for.cond1025
  %587 = load float*, float** %pb921, align 4
  %588 = load i32, i32* %i, align 4
  %arrayidx1028 = getelementptr inbounds float, float* %587, i32 %588
  %589 = load float, float* %arrayidx1028, align 4
  %590 = load float*, float** %pa920, align 4
  %591 = load i32, i32* %i, align 4
  %arrayidx1029 = getelementptr inbounds float, float* %590, i32 %591
  %592 = load float, float* %arrayidx1029, align 4
  %sub1030 = fsub float %589, %592
  %593 = load float*, float** %Sb, align 4
  %594 = load i32, i32* %lanr, align 4
  %arrayidx1031 = getelementptr inbounds float, float* %593, i32 %594
  %595 = load float, float* %arrayidx1031, align 4
  %596 = load float*, float** %Rb, align 4
  %597 = load i32, i32* %i, align 4
  %mul1032 = mul nsw i32 %597, 4
  %598 = load i32, i32* %lanr, align 4
  %add1033 = add nsw i32 %mul1032, %598
  %arrayidx1034 = getelementptr inbounds float, float* %596, i32 %add1033
  %599 = load float, float* %arrayidx1034, align 4
  %mul1035 = fmul float %595, %599
  %add1036 = fadd float %sub1030, %mul1035
  %call1037 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %600 = load i32, i32* %i, align 4
  %arrayidx1038 = getelementptr inbounds float, float* %call1037, i32 %600
  store float %add1036, float* %arrayidx1038, align 4
  br label %for.inc1039

for.inc1039:                                      ; preds = %for.body1027
  %601 = load i32, i32* %i, align 4
  %inc1040 = add nsw i32 %601, 1
  store i32 %inc1040, i32* %i, align 4
  br label %for.cond1025

for.end1041:                                      ; preds = %for.cond1025
  br label %if.end1060

if.else1042:                                      ; preds = %if.end1019
  store i32 0, i32* %i, align 4
  br label %for.cond1043

for.cond1043:                                     ; preds = %for.inc1057, %if.else1042
  %602 = load i32, i32* %i, align 4
  %cmp1044 = icmp slt i32 %602, 3
  br i1 %cmp1044, label %for.body1045, label %for.end1059

for.body1045:                                     ; preds = %for.cond1043
  %603 = load float*, float** %pb921, align 4
  %604 = load i32, i32* %i, align 4
  %arrayidx1046 = getelementptr inbounds float, float* %603, i32 %604
  %605 = load float, float* %arrayidx1046, align 4
  %606 = load float*, float** %pa920, align 4
  %607 = load i32, i32* %i, align 4
  %arrayidx1047 = getelementptr inbounds float, float* %606, i32 %607
  %608 = load float, float* %arrayidx1047, align 4
  %sub1048 = fsub float %605, %608
  %609 = load float*, float** %Sb, align 4
  %610 = load i32, i32* %lanr, align 4
  %arrayidx1049 = getelementptr inbounds float, float* %609, i32 %610
  %611 = load float, float* %arrayidx1049, align 4
  %612 = load float*, float** %Rb, align 4
  %613 = load i32, i32* %i, align 4
  %mul1050 = mul nsw i32 %613, 4
  %614 = load i32, i32* %lanr, align 4
  %add1051 = add nsw i32 %mul1050, %614
  %arrayidx1052 = getelementptr inbounds float, float* %612, i32 %add1051
  %615 = load float, float* %arrayidx1052, align 4
  %mul1053 = fmul float %611, %615
  %sub1054 = fsub float %sub1048, %mul1053
  %call1055 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %616 = load i32, i32* %i, align 4
  %arrayidx1056 = getelementptr inbounds float, float* %call1055, i32 %616
  store float %sub1054, float* %arrayidx1056, align 4
  br label %for.inc1057

for.inc1057:                                      ; preds = %for.body1045
  %617 = load i32, i32* %i, align 4
  %inc1058 = add nsw i32 %617, 1
  store i32 %inc1058, i32* %i, align 4
  br label %for.cond1043

for.end1059:                                      ; preds = %for.cond1043
  br label %if.end1060

if.end1060:                                       ; preds = %for.end1059, %for.end1041
  %618 = load i32, i32* %code, align 4
  %cmp1061 = icmp sle i32 %618, 3
  br i1 %cmp1061, label %if.then1062, label %if.else1064

if.then1062:                                      ; preds = %if.end1060
  %619 = load i32, i32* %code, align 4
  %sub1063 = sub nsw i32 %619, 1
  store i32 %sub1063, i32* %codeN, align 4
  br label %if.end1066

if.else1064:                                      ; preds = %if.end1060
  %620 = load i32, i32* %code, align 4
  %sub1065 = sub nsw i32 %620, 4
  store i32 %sub1065, i32* %codeN, align 4
  br label %if.end1066

if.end1066:                                       ; preds = %if.else1064, %if.then1062
  %621 = load i32, i32* %codeN, align 4
  %cmp1067 = icmp eq i32 %621, 0
  br i1 %cmp1067, label %if.then1068, label %if.else1069

if.then1068:                                      ; preds = %if.end1066
  store i32 1, i32* %code1, align 4
  store i32 2, i32* %code2, align 4
  br label %if.end1074

if.else1069:                                      ; preds = %if.end1066
  %622 = load i32, i32* %codeN, align 4
  %cmp1070 = icmp eq i32 %622, 1
  br i1 %cmp1070, label %if.then1071, label %if.else1072

if.then1071:                                      ; preds = %if.else1069
  store i32 0, i32* %code1, align 4
  store i32 2, i32* %code2, align 4
  br label %if.end1073

if.else1072:                                      ; preds = %if.else1069
  store i32 0, i32* %code1, align 4
  store i32 1, i32* %code2, align 4
  br label %if.end1073

if.end1073:                                       ; preds = %if.else1072, %if.then1071
  br label %if.end1074

if.end1074:                                       ; preds = %if.end1073, %if.then1068
  %call1075 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %623 = load float*, float** %Ra, align 4
  %624 = load i32, i32* %code1, align 4
  %add.ptr1076 = getelementptr inbounds float, float* %623, i32 %624
  %call1077 = call float @_ZL6dDOT14PKfS0_(float* %call1075, float* %add.ptr1076)
  store float %call1077, float* %c1, align 4
  %call1078 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %625 = load float*, float** %Ra, align 4
  %626 = load i32, i32* %code2, align 4
  %add.ptr1079 = getelementptr inbounds float, float* %625, i32 %626
  %call1080 = call float @_ZL6dDOT14PKfS0_(float* %call1078, float* %add.ptr1079)
  store float %call1080, float* %c2, align 4
  %627 = load float*, float** %Ra, align 4
  %628 = load i32, i32* %code1, align 4
  %add.ptr1081 = getelementptr inbounds float, float* %627, i32 %628
  %629 = load float*, float** %Rb, align 4
  %630 = load i32, i32* %a1, align 4
  %add.ptr1082 = getelementptr inbounds float, float* %629, i32 %630
  %call1083 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1081, float* %add.ptr1082)
  store float %call1083, float* %m11, align 4
  %631 = load float*, float** %Ra, align 4
  %632 = load i32, i32* %code1, align 4
  %add.ptr1084 = getelementptr inbounds float, float* %631, i32 %632
  %633 = load float*, float** %Rb, align 4
  %634 = load i32, i32* %a2, align 4
  %add.ptr1085 = getelementptr inbounds float, float* %633, i32 %634
  %call1086 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1084, float* %add.ptr1085)
  store float %call1086, float* %m12, align 4
  %635 = load float*, float** %Ra, align 4
  %636 = load i32, i32* %code2, align 4
  %add.ptr1087 = getelementptr inbounds float, float* %635, i32 %636
  %637 = load float*, float** %Rb, align 4
  %638 = load i32, i32* %a1, align 4
  %add.ptr1088 = getelementptr inbounds float, float* %637, i32 %638
  %call1089 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1087, float* %add.ptr1088)
  store float %call1089, float* %m21, align 4
  %639 = load float*, float** %Ra, align 4
  %640 = load i32, i32* %code2, align 4
  %add.ptr1090 = getelementptr inbounds float, float* %639, i32 %640
  %641 = load float*, float** %Rb, align 4
  %642 = load i32, i32* %a2, align 4
  %add.ptr1091 = getelementptr inbounds float, float* %641, i32 %642
  %call1092 = call float @_ZL6dDOT44PKfS0_(float* %add.ptr1090, float* %add.ptr1091)
  store float %call1092, float* %m22, align 4
  %643 = load float, float* %m11, align 4
  %644 = load float*, float** %Sb, align 4
  %645 = load i32, i32* %a1, align 4
  %arrayidx1093 = getelementptr inbounds float, float* %644, i32 %645
  %646 = load float, float* %arrayidx1093, align 4
  %mul1094 = fmul float %643, %646
  store float %mul1094, float* %k1, align 4
  %647 = load float, float* %m21, align 4
  %648 = load float*, float** %Sb, align 4
  %649 = load i32, i32* %a1, align 4
  %arrayidx1095 = getelementptr inbounds float, float* %648, i32 %649
  %650 = load float, float* %arrayidx1095, align 4
  %mul1096 = fmul float %647, %650
  store float %mul1096, float* %k2, align 4
  %651 = load float, float* %m12, align 4
  %652 = load float*, float** %Sb, align 4
  %653 = load i32, i32* %a2, align 4
  %arrayidx1097 = getelementptr inbounds float, float* %652, i32 %653
  %654 = load float, float* %arrayidx1097, align 4
  %mul1098 = fmul float %651, %654
  store float %mul1098, float* %k3, align 4
  %655 = load float, float* %m22, align 4
  %656 = load float*, float** %Sb, align 4
  %657 = load i32, i32* %a2, align 4
  %arrayidx1099 = getelementptr inbounds float, float* %656, i32 %657
  %658 = load float, float* %arrayidx1099, align 4
  %mul1100 = fmul float %655, %658
  store float %mul1100, float* %k4, align 4
  %659 = load float, float* %c1, align 4
  %660 = load float, float* %k1, align 4
  %sub1101 = fsub float %659, %660
  %661 = load float, float* %k3, align 4
  %sub1102 = fsub float %sub1101, %661
  %arrayidx1103 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 0
  store float %sub1102, float* %arrayidx1103, align 16
  %662 = load float, float* %c2, align 4
  %663 = load float, float* %k2, align 4
  %sub1104 = fsub float %662, %663
  %664 = load float, float* %k4, align 4
  %sub1105 = fsub float %sub1104, %664
  %arrayidx1106 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 1
  store float %sub1105, float* %arrayidx1106, align 4
  %665 = load float, float* %c1, align 4
  %666 = load float, float* %k1, align 4
  %sub1107 = fsub float %665, %666
  %667 = load float, float* %k3, align 4
  %add1108 = fadd float %sub1107, %667
  %arrayidx1109 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 2
  store float %add1108, float* %arrayidx1109, align 8
  %668 = load float, float* %c2, align 4
  %669 = load float, float* %k2, align 4
  %sub1110 = fsub float %668, %669
  %670 = load float, float* %k4, align 4
  %add1111 = fadd float %sub1110, %670
  %arrayidx1112 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 3
  store float %add1111, float* %arrayidx1112, align 4
  %671 = load float, float* %c1, align 4
  %672 = load float, float* %k1, align 4
  %add1113 = fadd float %671, %672
  %673 = load float, float* %k3, align 4
  %add1114 = fadd float %add1113, %673
  %arrayidx1115 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 4
  store float %add1114, float* %arrayidx1115, align 16
  %674 = load float, float* %c2, align 4
  %675 = load float, float* %k2, align 4
  %add1116 = fadd float %674, %675
  %676 = load float, float* %k4, align 4
  %add1117 = fadd float %add1116, %676
  %arrayidx1118 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 5
  store float %add1117, float* %arrayidx1118, align 4
  %677 = load float, float* %c1, align 4
  %678 = load float, float* %k1, align 4
  %add1119 = fadd float %677, %678
  %679 = load float, float* %k3, align 4
  %sub1120 = fsub float %add1119, %679
  %arrayidx1121 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 6
  store float %sub1120, float* %arrayidx1121, align 8
  %680 = load float, float* %c2, align 4
  %681 = load float, float* %k2, align 4
  %add1122 = fadd float %680, %681
  %682 = load float, float* %k4, align 4
  %sub1123 = fsub float %add1122, %682
  %arrayidx1124 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 7
  store float %sub1123, float* %arrayidx1124, align 4
  %683 = load float*, float** %Sa, align 4
  %684 = load i32, i32* %code1, align 4
  %arrayidx1125 = getelementptr inbounds float, float* %683, i32 %684
  %685 = load float, float* %arrayidx1125, align 4
  %arrayidx1126 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 0
  store float %685, float* %arrayidx1126, align 4
  %686 = load float*, float** %Sa, align 4
  %687 = load i32, i32* %code2, align 4
  %arrayidx1127 = getelementptr inbounds float, float* %686, i32 %687
  %688 = load float, float* %arrayidx1127, align 4
  %arrayidx1128 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 1
  store float %688, float* %arrayidx1128, align 4
  %arraydecay1129 = getelementptr inbounds [2 x float], [2 x float]* %rect, i32 0, i32 0
  %arraydecay1130 = getelementptr inbounds [8 x float], [8 x float]* %quad, i32 0, i32 0
  %arraydecay1131 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 0
  %call1132 = call i32 @_ZL18intersectRectQuad2PfS_S_(float* %arraydecay1129, float* %arraydecay1130, float* %arraydecay1131)
  store i32 %call1132, i32* %n, align 4
  %689 = load i32, i32* %n, align 4
  %cmp1133 = icmp slt i32 %689, 1
  br i1 %cmp1133, label %if.then1134, label %if.end1135

if.then1134:                                      ; preds = %if.end1074
  store i32 0, i32* %retval, align 4
  br label %return

if.end1135:                                       ; preds = %if.end1074
  %690 = load float, float* %m11, align 4
  %691 = load float, float* %m22, align 4
  %mul1136 = fmul float %690, %691
  %692 = load float, float* %m12, align 4
  %693 = load float, float* %m21, align 4
  %mul1137 = fmul float %692, %693
  %sub1138 = fsub float %mul1136, %mul1137
  %div1139 = fdiv float 1.000000e+00, %sub1138
  store float %div1139, float* %det1, align 4
  %694 = load float, float* %det1, align 4
  %695 = load float, float* %m11, align 4
  %mul1140 = fmul float %695, %694
  store float %mul1140, float* %m11, align 4
  %696 = load float, float* %det1, align 4
  %697 = load float, float* %m12, align 4
  %mul1141 = fmul float %697, %696
  store float %mul1141, float* %m12, align 4
  %698 = load float, float* %det1, align 4
  %699 = load float, float* %m21, align 4
  %mul1142 = fmul float %699, %698
  store float %mul1142, float* %m21, align 4
  %700 = load float, float* %det1, align 4
  %701 = load float, float* %m22, align 4
  %mul1143 = fmul float %701, %700
  store float %mul1143, float* %m22, align 4
  store i32 0, i32* %cnum, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond1144

for.cond1144:                                     ; preds = %for.inc1214, %if.end1135
  %702 = load i32, i32* %j, align 4
  %703 = load i32, i32* %n, align 4
  %cmp1145 = icmp slt i32 %702, %703
  br i1 %cmp1145, label %for.body1146, label %for.end1216

for.body1146:                                     ; preds = %for.cond1144
  %704 = load float, float* %m22, align 4
  %705 = load i32, i32* %j, align 4
  %mul1148 = mul nsw i32 %705, 2
  %arrayidx1149 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1148
  %706 = load float, float* %arrayidx1149, align 4
  %707 = load float, float* %c1, align 4
  %sub1150 = fsub float %706, %707
  %mul1151 = fmul float %704, %sub1150
  %708 = load float, float* %m12, align 4
  %709 = load i32, i32* %j, align 4
  %mul1152 = mul nsw i32 %709, 2
  %add1153 = add nsw i32 %mul1152, 1
  %arrayidx1154 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1153
  %710 = load float, float* %arrayidx1154, align 4
  %711 = load float, float* %c2, align 4
  %sub1155 = fsub float %710, %711
  %mul1156 = fmul float %708, %sub1155
  %sub1157 = fsub float %mul1151, %mul1156
  store float %sub1157, float* %k11147, align 4
  %712 = load float, float* %m21, align 4
  %fneg1159 = fneg float %712
  %713 = load i32, i32* %j, align 4
  %mul1160 = mul nsw i32 %713, 2
  %arrayidx1161 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1160
  %714 = load float, float* %arrayidx1161, align 4
  %715 = load float, float* %c1, align 4
  %sub1162 = fsub float %714, %715
  %mul1163 = fmul float %fneg1159, %sub1162
  %716 = load float, float* %m11, align 4
  %717 = load i32, i32* %j, align 4
  %mul1164 = mul nsw i32 %717, 2
  %add1165 = add nsw i32 %mul1164, 1
  %arrayidx1166 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1165
  %718 = load float, float* %arrayidx1166, align 4
  %719 = load float, float* %c2, align 4
  %sub1167 = fsub float %718, %719
  %mul1168 = fmul float %716, %sub1167
  %add1169 = fadd float %mul1163, %mul1168
  store float %add1169, float* %k21158, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond1170

for.cond1170:                                     ; preds = %for.inc1188, %for.body1146
  %720 = load i32, i32* %i, align 4
  %cmp1171 = icmp slt i32 %720, 3
  br i1 %cmp1171, label %for.body1172, label %for.end1190

for.body1172:                                     ; preds = %for.cond1170
  %call1173 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %721 = load i32, i32* %i, align 4
  %arrayidx1174 = getelementptr inbounds float, float* %call1173, i32 %721
  %722 = load float, float* %arrayidx1174, align 4
  %723 = load float, float* %k11147, align 4
  %724 = load float*, float** %Rb, align 4
  %725 = load i32, i32* %i, align 4
  %mul1175 = mul nsw i32 %725, 4
  %726 = load i32, i32* %a1, align 4
  %add1176 = add nsw i32 %mul1175, %726
  %arrayidx1177 = getelementptr inbounds float, float* %724, i32 %add1176
  %727 = load float, float* %arrayidx1177, align 4
  %mul1178 = fmul float %723, %727
  %add1179 = fadd float %722, %mul1178
  %728 = load float, float* %k21158, align 4
  %729 = load float*, float** %Rb, align 4
  %730 = load i32, i32* %i, align 4
  %mul1180 = mul nsw i32 %730, 4
  %731 = load i32, i32* %a2, align 4
  %add1181 = add nsw i32 %mul1180, %731
  %arrayidx1182 = getelementptr inbounds float, float* %729, i32 %add1181
  %732 = load float, float* %arrayidx1182, align 4
  %mul1183 = fmul float %728, %732
  %add1184 = fadd float %add1179, %mul1183
  %733 = load i32, i32* %cnum, align 4
  %mul1185 = mul nsw i32 %733, 3
  %734 = load i32, i32* %i, align 4
  %add1186 = add nsw i32 %mul1185, %734
  %arrayidx1187 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1186
  store float %add1184, float* %arrayidx1187, align 4
  br label %for.inc1188

for.inc1188:                                      ; preds = %for.body1172
  %735 = load i32, i32* %i, align 4
  %inc1189 = add nsw i32 %735, 1
  store i32 %inc1189, i32* %i, align 4
  br label %for.cond1170

for.end1190:                                      ; preds = %for.cond1170
  %736 = load float*, float** %Sa, align 4
  %737 = load i32, i32* %codeN, align 4
  %arrayidx1191 = getelementptr inbounds float, float* %736, i32 %737
  %738 = load float, float* %arrayidx1191, align 4
  %call1192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal2)
  %arraydecay1193 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 0
  %739 = load i32, i32* %cnum, align 4
  %mul1194 = mul nsw i32 %739, 3
  %add.ptr1195 = getelementptr inbounds float, float* %arraydecay1193, i32 %mul1194
  %call1196 = call float @_ZL4dDOTPKfS0_(float* %call1192, float* %add.ptr1195)
  %sub1197 = fsub float %738, %call1196
  %740 = load i32, i32* %cnum, align 4
  %arrayidx1198 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %740
  store float %sub1197, float* %arrayidx1198, align 4
  %741 = load i32, i32* %cnum, align 4
  %arrayidx1199 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %741
  %742 = load float, float* %arrayidx1199, align 4
  %cmp1200 = fcmp oge float %742, 0.000000e+00
  br i1 %cmp1200, label %if.then1201, label %if.end1213

if.then1201:                                      ; preds = %for.end1190
  %743 = load i32, i32* %j, align 4
  %mul1202 = mul nsw i32 %743, 2
  %arrayidx1203 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1202
  %744 = load float, float* %arrayidx1203, align 4
  %745 = load i32, i32* %cnum, align 4
  %mul1204 = mul nsw i32 %745, 2
  %arrayidx1205 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %mul1204
  store float %744, float* %arrayidx1205, align 4
  %746 = load i32, i32* %j, align 4
  %mul1206 = mul nsw i32 %746, 2
  %add1207 = add nsw i32 %mul1206, 1
  %arrayidx1208 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1207
  %747 = load float, float* %arrayidx1208, align 4
  %748 = load i32, i32* %cnum, align 4
  %mul1209 = mul nsw i32 %748, 2
  %add1210 = add nsw i32 %mul1209, 1
  %arrayidx1211 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 %add1210
  store float %747, float* %arrayidx1211, align 4
  %749 = load i32, i32* %cnum, align 4
  %inc1212 = add nsw i32 %749, 1
  store i32 %inc1212, i32* %cnum, align 4
  br label %if.end1213

if.end1213:                                       ; preds = %if.then1201, %for.end1190
  br label %for.inc1214

for.inc1214:                                      ; preds = %if.end1213
  %750 = load i32, i32* %j, align 4
  %inc1215 = add nsw i32 %750, 1
  store i32 %inc1215, i32* %j, align 4
  br label %for.cond1144

for.end1216:                                      ; preds = %for.cond1144
  %751 = load i32, i32* %cnum, align 4
  %cmp1217 = icmp slt i32 %751, 1
  br i1 %cmp1217, label %if.then1218, label %if.end1219

if.then1218:                                      ; preds = %for.end1216
  store i32 0, i32* %retval, align 4
  br label %return

if.end1219:                                       ; preds = %for.end1216
  %752 = load i32, i32* %maxc.addr, align 4
  %753 = load i32, i32* %cnum, align 4
  %cmp1220 = icmp sgt i32 %752, %753
  br i1 %cmp1220, label %if.then1221, label %if.end1222

if.then1221:                                      ; preds = %if.end1219
  %754 = load i32, i32* %cnum, align 4
  store i32 %754, i32* %maxc.addr, align 4
  br label %if.end1222

if.end1222:                                       ; preds = %if.then1221, %if.end1219
  %755 = load i32, i32* %maxc.addr, align 4
  %cmp1223 = icmp slt i32 %755, 1
  br i1 %cmp1223, label %if.then1224, label %if.end1225

if.then1224:                                      ; preds = %if.end1222
  store i32 1, i32* %maxc.addr, align 4
  br label %if.end1225

if.end1225:                                       ; preds = %if.then1224, %if.end1222
  %756 = load i32, i32* %cnum, align 4
  %757 = load i32, i32* %maxc.addr, align 4
  %cmp1226 = icmp sle i32 %756, %757
  br i1 %cmp1226, label %if.then1227, label %if.else1289

if.then1227:                                      ; preds = %if.end1225
  %758 = load i32, i32* %code, align 4
  %cmp1228 = icmp slt i32 %758, 4
  br i1 %cmp1228, label %if.then1229, label %if.else1256

if.then1229:                                      ; preds = %if.then1227
  store i32 0, i32* %j, align 4
  br label %for.cond1230

for.cond1230:                                     ; preds = %for.inc1253, %if.then1229
  %759 = load i32, i32* %j, align 4
  %760 = load i32, i32* %cnum, align 4
  %cmp1231 = icmp slt i32 %759, %760
  br i1 %cmp1231, label %for.body1232, label %for.end1255

for.body1232:                                     ; preds = %for.cond1230
  %call1234 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld1233)
  store i32 0, i32* %i, align 4
  br label %for.cond1235

for.cond1235:                                     ; preds = %for.inc1245, %for.body1232
  %761 = load i32, i32* %i, align 4
  %cmp1236 = icmp slt i32 %761, 3
  br i1 %cmp1236, label %for.body1237, label %for.end1247

for.body1237:                                     ; preds = %for.cond1235
  %762 = load i32, i32* %j, align 4
  %mul1238 = mul nsw i32 %762, 3
  %763 = load i32, i32* %i, align 4
  %add1239 = add nsw i32 %mul1238, %763
  %arrayidx1240 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1239
  %764 = load float, float* %arrayidx1240, align 4
  %765 = load float*, float** %pa920, align 4
  %766 = load i32, i32* %i, align 4
  %arrayidx1241 = getelementptr inbounds float, float* %765, i32 %766
  %767 = load float, float* %arrayidx1241, align 4
  %add1242 = fadd float %764, %767
  %call1243 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pointInWorld1233)
  %768 = load i32, i32* %i, align 4
  %arrayidx1244 = getelementptr inbounds float, float* %call1243, i32 %768
  store float %add1242, float* %arrayidx1244, align 4
  br label %for.inc1245

for.inc1245:                                      ; preds = %for.body1237
  %769 = load i32, i32* %i, align 4
  %inc1246 = add nsw i32 %769, 1
  store i32 %inc1246, i32* %i, align 4
  br label %for.cond1235

for.end1247:                                      ; preds = %for.cond1235
  %770 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %771 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1248, %class.btVector3* nonnull align 4 dereferenceable(16) %771)
  %772 = load i32, i32* %j, align 4
  %arrayidx1249 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %772
  %773 = load float, float* %arrayidx1249, align 4
  %fneg1250 = fneg float %773
  %774 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %770 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1251 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %774, align 4
  %vfn1252 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1251, i64 4
  %775 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1252, align 4
  call void %775(%"struct.btDiscreteCollisionDetectorInterface::Result"* %770, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1248, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld1233, float %fneg1250)
  br label %for.inc1253

for.inc1253:                                      ; preds = %for.end1247
  %776 = load i32, i32* %j, align 4
  %inc1254 = add nsw i32 %776, 1
  store i32 %inc1254, i32* %j, align 4
  br label %for.cond1230

for.end1255:                                      ; preds = %for.cond1230
  br label %if.end1288

if.else1256:                                      ; preds = %if.then1227
  store i32 0, i32* %j, align 4
  br label %for.cond1257

for.cond1257:                                     ; preds = %for.inc1285, %if.else1256
  %777 = load i32, i32* %j, align 4
  %778 = load i32, i32* %cnum, align 4
  %cmp1258 = icmp slt i32 %777, %778
  br i1 %cmp1258, label %for.body1259, label %for.end1287

for.body1259:                                     ; preds = %for.cond1257
  %call1261 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointInWorld1260)
  store i32 0, i32* %i, align 4
  br label %for.cond1262

for.cond1262:                                     ; preds = %for.inc1277, %for.body1259
  %779 = load i32, i32* %i, align 4
  %cmp1263 = icmp slt i32 %779, 3
  br i1 %cmp1263, label %for.body1264, label %for.end1279

for.body1264:                                     ; preds = %for.cond1262
  %780 = load i32, i32* %j, align 4
  %mul1265 = mul nsw i32 %780, 3
  %781 = load i32, i32* %i, align 4
  %add1266 = add nsw i32 %mul1265, %781
  %arrayidx1267 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1266
  %782 = load float, float* %arrayidx1267, align 4
  %783 = load float*, float** %pa920, align 4
  %784 = load i32, i32* %i, align 4
  %arrayidx1268 = getelementptr inbounds float, float* %783, i32 %784
  %785 = load float, float* %arrayidx1268, align 4
  %add1269 = fadd float %782, %785
  %786 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call1270 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %786)
  %787 = load i32, i32* %i, align 4
  %arrayidx1271 = getelementptr inbounds float, float* %call1270, i32 %787
  %788 = load float, float* %arrayidx1271, align 4
  %789 = load i32, i32* %j, align 4
  %arrayidx1272 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %789
  %790 = load float, float* %arrayidx1272, align 4
  %mul1273 = fmul float %788, %790
  %sub1274 = fsub float %add1269, %mul1273
  %call1275 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pointInWorld1260)
  %791 = load i32, i32* %i, align 4
  %arrayidx1276 = getelementptr inbounds float, float* %call1275, i32 %791
  store float %sub1274, float* %arrayidx1276, align 4
  br label %for.inc1277

for.inc1277:                                      ; preds = %for.body1264
  %792 = load i32, i32* %i, align 4
  %inc1278 = add nsw i32 %792, 1
  store i32 %inc1278, i32* %i, align 4
  br label %for.cond1262

for.end1279:                                      ; preds = %for.cond1262
  %793 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %794 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1280, %class.btVector3* nonnull align 4 dereferenceable(16) %794)
  %795 = load i32, i32* %j, align 4
  %arrayidx1281 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %795
  %796 = load float, float* %arrayidx1281, align 4
  %fneg1282 = fneg float %796
  %797 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %793 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1283 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %797, align 4
  %vfn1284 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1283, i64 4
  %798 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1284, align 4
  call void %798(%"struct.btDiscreteCollisionDetectorInterface::Result"* %793, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1280, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld1260, float %fneg1282)
  br label %for.inc1285

for.inc1285:                                      ; preds = %for.end1279
  %799 = load i32, i32* %j, align 4
  %inc1286 = add nsw i32 %799, 1
  store i32 %inc1286, i32* %j, align 4
  br label %for.cond1257

for.end1287:                                      ; preds = %for.cond1257
  br label %if.end1288

if.end1288:                                       ; preds = %for.end1287, %for.end1255
  br label %if.end1345

if.else1289:                                      ; preds = %if.end1225
  store i32 0, i32* %i1, align 4
  %arrayidx1290 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 0
  %800 = load float, float* %arrayidx1290, align 16
  store float %800, float* %maxdepth, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond1291

for.cond1291:                                     ; preds = %for.inc1299, %if.else1289
  %801 = load i32, i32* %i, align 4
  %802 = load i32, i32* %cnum, align 4
  %cmp1292 = icmp slt i32 %801, %802
  br i1 %cmp1292, label %for.body1293, label %for.end1301

for.body1293:                                     ; preds = %for.cond1291
  %803 = load i32, i32* %i, align 4
  %arrayidx1294 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %803
  %804 = load float, float* %arrayidx1294, align 4
  %805 = load float, float* %maxdepth, align 4
  %cmp1295 = fcmp ogt float %804, %805
  br i1 %cmp1295, label %if.then1296, label %if.end1298

if.then1296:                                      ; preds = %for.body1293
  %806 = load i32, i32* %i, align 4
  %arrayidx1297 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %806
  %807 = load float, float* %arrayidx1297, align 4
  store float %807, float* %maxdepth, align 4
  %808 = load i32, i32* %i, align 4
  store i32 %808, i32* %i1, align 4
  br label %if.end1298

if.end1298:                                       ; preds = %if.then1296, %for.body1293
  br label %for.inc1299

for.inc1299:                                      ; preds = %if.end1298
  %809 = load i32, i32* %i, align 4
  %inc1300 = add nsw i32 %809, 1
  store i32 %inc1300, i32* %i, align 4
  br label %for.cond1291

for.end1301:                                      ; preds = %for.cond1291
  %810 = load i32, i32* %cnum, align 4
  %arraydecay1302 = getelementptr inbounds [16 x float], [16 x float]* %ret, i32 0, i32 0
  %811 = load i32, i32* %maxc.addr, align 4
  %812 = load i32, i32* %i1, align 4
  %arraydecay1303 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 0
  call void @_Z11cullPoints2iPfiiPi(i32 %810, float* %arraydecay1302, i32 %811, i32 %812, i32* %arraydecay1303)
  store i32 0, i32* %j, align 4
  br label %for.cond1304

for.cond1304:                                     ; preds = %for.inc1342, %for.end1301
  %813 = load i32, i32* %j, align 4
  %814 = load i32, i32* %maxc.addr, align 4
  %cmp1305 = icmp slt i32 %813, %814
  br i1 %cmp1305, label %for.body1306, label %for.end1344

for.body1306:                                     ; preds = %for.cond1304
  %call1307 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %posInWorld)
  store i32 0, i32* %i, align 4
  br label %for.cond1308

for.cond1308:                                     ; preds = %for.inc1319, %for.body1306
  %815 = load i32, i32* %i, align 4
  %cmp1309 = icmp slt i32 %815, 3
  br i1 %cmp1309, label %for.body1310, label %for.end1321

for.body1310:                                     ; preds = %for.cond1308
  %816 = load i32, i32* %j, align 4
  %arrayidx1311 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %816
  %817 = load i32, i32* %arrayidx1311, align 4
  %mul1312 = mul nsw i32 %817, 3
  %818 = load i32, i32* %i, align 4
  %add1313 = add nsw i32 %mul1312, %818
  %arrayidx1314 = getelementptr inbounds [24 x float], [24 x float]* %point, i32 0, i32 %add1313
  %819 = load float, float* %arrayidx1314, align 4
  %820 = load float*, float** %pa920, align 4
  %821 = load i32, i32* %i, align 4
  %arrayidx1315 = getelementptr inbounds float, float* %820, i32 %821
  %822 = load float, float* %arrayidx1315, align 4
  %add1316 = fadd float %819, %822
  %call1317 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %posInWorld)
  %823 = load i32, i32* %i, align 4
  %arrayidx1318 = getelementptr inbounds float, float* %call1317, i32 %823
  store float %add1316, float* %arrayidx1318, align 4
  br label %for.inc1319

for.inc1319:                                      ; preds = %for.body1310
  %824 = load i32, i32* %i, align 4
  %inc1320 = add nsw i32 %824, 1
  store i32 %inc1320, i32* %i, align 4
  br label %for.cond1308

for.end1321:                                      ; preds = %for.cond1308
  %825 = load i32, i32* %code, align 4
  %cmp1322 = icmp slt i32 %825, 4
  br i1 %cmp1322, label %if.then1323, label %if.else1330

if.then1323:                                      ; preds = %for.end1321
  %826 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %827 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1324, %class.btVector3* nonnull align 4 dereferenceable(16) %827)
  %828 = load i32, i32* %j, align 4
  %arrayidx1325 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %828
  %829 = load i32, i32* %arrayidx1325, align 4
  %arrayidx1326 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %829
  %830 = load float, float* %arrayidx1326, align 4
  %fneg1327 = fneg float %830
  %831 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %826 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1328 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %831, align 4
  %vfn1329 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1328, i64 4
  %832 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1329, align 4
  call void %832(%"struct.btDiscreteCollisionDetectorInterface::Result"* %826, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1324, %class.btVector3* nonnull align 4 dereferenceable(16) %posInWorld, float %fneg1327)
  br label %if.end1341

if.else1330:                                      ; preds = %for.end1321
  %833 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %834 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp1331, %class.btVector3* nonnull align 4 dereferenceable(16) %834)
  %835 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %836 = load i32, i32* %j, align 4
  %arrayidx1334 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %836
  %837 = load i32, i32* %arrayidx1334, align 4
  %arrayidx1335 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %837
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1333, %class.btVector3* nonnull align 4 dereferenceable(16) %835, float* nonnull align 4 dereferenceable(4) %arrayidx1335)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1332, %class.btVector3* nonnull align 4 dereferenceable(16) %posInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1333)
  %838 = load i32, i32* %j, align 4
  %arrayidx1336 = getelementptr inbounds [8 x i32], [8 x i32]* %iret, i32 0, i32 %838
  %839 = load i32, i32* %arrayidx1336, align 4
  %arrayidx1337 = getelementptr inbounds [8 x float], [8 x float]* %dep, i32 0, i32 %839
  %840 = load float, float* %arrayidx1337, align 4
  %fneg1338 = fneg float %840
  %841 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %833 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable1339 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %841, align 4
  %vfn1340 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable1339, i64 4
  %842 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn1340, align 4
  call void %842(%"struct.btDiscreteCollisionDetectorInterface::Result"* %833, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1331, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1332, float %fneg1338)
  br label %if.end1341

if.end1341:                                       ; preds = %if.else1330, %if.then1323
  br label %for.inc1342

for.inc1342:                                      ; preds = %if.end1341
  %843 = load i32, i32* %j, align 4
  %inc1343 = add nsw i32 %843, 1
  store i32 %inc1343, i32* %j, align 4
  br label %for.cond1304

for.end1344:                                      ; preds = %for.cond1304
  %844 = load i32, i32* %maxc.addr, align 4
  store i32 %844, i32* %cnum, align 4
  br label %if.end1345

if.end1345:                                       ; preds = %for.end1344, %if.end1288
  %845 = load i32, i32* %code, align 4
  %846 = load i32*, i32** %return_code.addr, align 4
  store i32 %845, i32* %846, align 4
  %847 = load i32, i32* %cnum, align 4
  store i32 %847, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end1345, %if.then1218, %if.then1134, %for.end915, %if.then748, %if.then712, %if.then655, %if.then598, %if.then541, %if.then484, %if.then427, %if.then370, %if.then313, %if.then258, %if.then217, %if.then190, %if.then163, %if.then137, %if.then112, %if.then
  %848 = load i32, i32* %retval, align 4
  ret i32 %848
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal float @_ZL6dDOT41PKfS0_(float* %a, float* %b) #1 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 4
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float*, float** %b.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 8
  %9 = load float, float* %arrayidx5, align 4
  %10 = load float*, float** %b.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: noinline nounwind optnone
define internal float @_ZL6dDOT44PKfS0_(float* %a, float* %b) #1 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 4
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float*, float** %b.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 4
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 8
  %9 = load float, float* %arrayidx5, align 4
  %10 = load float*, float** %b.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 8
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define internal float @_ZL6dDOT14PKfS0_(float* %a, float* %b) #1 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4
  %mul = fmul float %1, %3
  %4 = load float*, float** %a.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float*, float** %b.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 4
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %8 = load float*, float** %a.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 2
  %9 = load float, float* %arrayidx5, align 4
  %10 = load float*, float** %b.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 8
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %9, %11
  %add8 = fadd float %add, %mul7
  ret float %add8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZL18intersectRectQuad2PfS_S_(float* %h, float* %p, float* %ret) #1 {
entry:
  %h.addr = alloca float*, align 4
  %p.addr = alloca float*, align 4
  %ret.addr = alloca float*, align 4
  %nq = alloca i32, align 4
  %nr = alloca i32, align 4
  %buffer = alloca [16 x float], align 16
  %q = alloca float*, align 4
  %r = alloca float*, align 4
  %dir = alloca i32, align 4
  %sign = alloca i32, align 4
  %pq = alloca float*, align 4
  %pr = alloca float*, align 4
  %i = alloca i32, align 4
  %nextq = alloca float*, align 4
  store float* %h, float** %h.addr, align 4
  store float* %p, float** %p.addr, align 4
  store float* %ret, float** %ret.addr, align 4
  store i32 4, i32* %nq, align 4
  store i32 0, i32* %nr, align 4
  %0 = load float*, float** %p.addr, align 4
  store float* %0, float** %q, align 4
  %1 = load float*, float** %ret.addr, align 4
  store float* %1, float** %r, align 4
  store i32 0, i32* %dir, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc68, %entry
  %2 = load i32, i32* %dir, align 4
  %cmp = icmp sle i32 %2, 1
  br i1 %cmp, label %for.body, label %for.end70

for.body:                                         ; preds = %for.cond
  store i32 -1, i32* %sign, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc65, %for.body
  %3 = load i32, i32* %sign, align 4
  %cmp2 = icmp sle i32 %3, 1
  br i1 %cmp2, label %for.body3, label %for.end67

for.body3:                                        ; preds = %for.cond1
  %4 = load float*, float** %q, align 4
  store float* %4, float** %pq, align 4
  %5 = load float*, float** %r, align 4
  store float* %5, float** %pr, align 4
  store i32 0, i32* %nr, align 4
  %6 = load i32, i32* %nq, align 4
  store i32 %6, i32* %i, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body3
  %7 = load i32, i32* %i, align 4
  %cmp5 = icmp sgt i32 %7, 0
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %8 = load i32, i32* %sign, align 4
  %conv = sitofp i32 %8 to float
  %9 = load float*, float** %pq, align 4
  %10 = load i32, i32* %dir, align 4
  %arrayidx = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx, align 4
  %mul = fmul float %conv, %11
  %12 = load float*, float** %h.addr, align 4
  %13 = load i32, i32* %dir, align 4
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 %13
  %14 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %mul, %14
  br i1 %cmp8, label %if.then, label %if.end14

if.then:                                          ; preds = %for.body6
  %15 = load float*, float** %pq, align 4
  %arrayidx9 = getelementptr inbounds float, float* %15, i32 0
  %16 = load float, float* %arrayidx9, align 4
  %17 = load float*, float** %pr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %17, i32 0
  store float %16, float* %arrayidx10, align 4
  %18 = load float*, float** %pq, align 4
  %arrayidx11 = getelementptr inbounds float, float* %18, i32 1
  %19 = load float, float* %arrayidx11, align 4
  %20 = load float*, float** %pr, align 4
  %arrayidx12 = getelementptr inbounds float, float* %20, i32 1
  store float %19, float* %arrayidx12, align 4
  %21 = load float*, float** %pr, align 4
  %add.ptr = getelementptr inbounds float, float* %21, i32 2
  store float* %add.ptr, float** %pr, align 4
  %22 = load i32, i32* %nr, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %nr, align 4
  %23 = load i32, i32* %nr, align 4
  %and = and i32 %23, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then
  %24 = load float*, float** %r, align 4
  store float* %24, float** %q, align 4
  br label %done

if.end:                                           ; preds = %if.then
  br label %if.end14

if.end14:                                         ; preds = %if.end, %for.body6
  %25 = load i32, i32* %i, align 4
  %cmp15 = icmp sgt i32 %25, 1
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end14
  %26 = load float*, float** %pq, align 4
  %add.ptr16 = getelementptr inbounds float, float* %26, i32 2
  br label %cond.end

cond.false:                                       ; preds = %if.end14
  %27 = load float*, float** %q, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %add.ptr16, %cond.true ], [ %27, %cond.false ]
  store float* %cond, float** %nextq, align 4
  %28 = load i32, i32* %sign, align 4
  %conv17 = sitofp i32 %28 to float
  %29 = load float*, float** %pq, align 4
  %30 = load i32, i32* %dir, align 4
  %arrayidx18 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %conv17, %31
  %32 = load float*, float** %h.addr, align 4
  %33 = load i32, i32* %dir, align 4
  %arrayidx20 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx20, align 4
  %cmp21 = fcmp olt float %mul19, %34
  %conv22 = zext i1 %cmp21 to i32
  %35 = load i32, i32* %sign, align 4
  %conv23 = sitofp i32 %35 to float
  %36 = load float*, float** %nextq, align 4
  %37 = load i32, i32* %dir, align 4
  %arrayidx24 = getelementptr inbounds float, float* %36, i32 %37
  %38 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %conv23, %38
  %39 = load float*, float** %h.addr, align 4
  %40 = load i32, i32* %dir, align 4
  %arrayidx26 = getelementptr inbounds float, float* %39, i32 %40
  %41 = load float, float* %arrayidx26, align 4
  %cmp27 = fcmp olt float %mul25, %41
  %conv28 = zext i1 %cmp27 to i32
  %xor = xor i32 %conv22, %conv28
  %tobool29 = icmp ne i32 %xor, 0
  br i1 %tobool29, label %if.then30, label %if.end58

if.then30:                                        ; preds = %cond.end
  %42 = load float*, float** %pq, align 4
  %43 = load i32, i32* %dir, align 4
  %sub = sub nsw i32 1, %43
  %arrayidx31 = getelementptr inbounds float, float* %42, i32 %sub
  %44 = load float, float* %arrayidx31, align 4
  %45 = load float*, float** %nextq, align 4
  %46 = load i32, i32* %dir, align 4
  %sub32 = sub nsw i32 1, %46
  %arrayidx33 = getelementptr inbounds float, float* %45, i32 %sub32
  %47 = load float, float* %arrayidx33, align 4
  %48 = load float*, float** %pq, align 4
  %49 = load i32, i32* %dir, align 4
  %sub34 = sub nsw i32 1, %49
  %arrayidx35 = getelementptr inbounds float, float* %48, i32 %sub34
  %50 = load float, float* %arrayidx35, align 4
  %sub36 = fsub float %47, %50
  %51 = load float*, float** %nextq, align 4
  %52 = load i32, i32* %dir, align 4
  %arrayidx37 = getelementptr inbounds float, float* %51, i32 %52
  %53 = load float, float* %arrayidx37, align 4
  %54 = load float*, float** %pq, align 4
  %55 = load i32, i32* %dir, align 4
  %arrayidx38 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx38, align 4
  %sub39 = fsub float %53, %56
  %div = fdiv float %sub36, %sub39
  %57 = load i32, i32* %sign, align 4
  %conv40 = sitofp i32 %57 to float
  %58 = load float*, float** %h.addr, align 4
  %59 = load i32, i32* %dir, align 4
  %arrayidx41 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx41, align 4
  %mul42 = fmul float %conv40, %60
  %61 = load float*, float** %pq, align 4
  %62 = load i32, i32* %dir, align 4
  %arrayidx43 = getelementptr inbounds float, float* %61, i32 %62
  %63 = load float, float* %arrayidx43, align 4
  %sub44 = fsub float %mul42, %63
  %mul45 = fmul float %div, %sub44
  %add = fadd float %44, %mul45
  %64 = load float*, float** %pr, align 4
  %65 = load i32, i32* %dir, align 4
  %sub46 = sub nsw i32 1, %65
  %arrayidx47 = getelementptr inbounds float, float* %64, i32 %sub46
  store float %add, float* %arrayidx47, align 4
  %66 = load i32, i32* %sign, align 4
  %conv48 = sitofp i32 %66 to float
  %67 = load float*, float** %h.addr, align 4
  %68 = load i32, i32* %dir, align 4
  %arrayidx49 = getelementptr inbounds float, float* %67, i32 %68
  %69 = load float, float* %arrayidx49, align 4
  %mul50 = fmul float %conv48, %69
  %70 = load float*, float** %pr, align 4
  %71 = load i32, i32* %dir, align 4
  %arrayidx51 = getelementptr inbounds float, float* %70, i32 %71
  store float %mul50, float* %arrayidx51, align 4
  %72 = load float*, float** %pr, align 4
  %add.ptr52 = getelementptr inbounds float, float* %72, i32 2
  store float* %add.ptr52, float** %pr, align 4
  %73 = load i32, i32* %nr, align 4
  %inc53 = add nsw i32 %73, 1
  store i32 %inc53, i32* %nr, align 4
  %74 = load i32, i32* %nr, align 4
  %and54 = and i32 %74, 8
  %tobool55 = icmp ne i32 %and54, 0
  br i1 %tobool55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %if.then30
  %75 = load float*, float** %r, align 4
  store float* %75, float** %q, align 4
  br label %done

if.end57:                                         ; preds = %if.then30
  br label %if.end58

if.end58:                                         ; preds = %if.end57, %cond.end
  %76 = load float*, float** %pq, align 4
  %add.ptr59 = getelementptr inbounds float, float* %76, i32 2
  store float* %add.ptr59, float** %pq, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end58
  %77 = load i32, i32* %i, align 4
  %dec = add nsw i32 %77, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %78 = load float*, float** %r, align 4
  store float* %78, float** %q, align 4
  %79 = load float*, float** %q, align 4
  %80 = load float*, float** %ret.addr, align 4
  %cmp60 = icmp eq float* %79, %80
  br i1 %cmp60, label %cond.true61, label %cond.false62

cond.true61:                                      ; preds = %for.end
  %arraydecay = getelementptr inbounds [16 x float], [16 x float]* %buffer, i32 0, i32 0
  br label %cond.end63

cond.false62:                                     ; preds = %for.end
  %81 = load float*, float** %ret.addr, align 4
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false62, %cond.true61
  %cond64 = phi float* [ %arraydecay, %cond.true61 ], [ %81, %cond.false62 ]
  store float* %cond64, float** %r, align 4
  %82 = load i32, i32* %nr, align 4
  store i32 %82, i32* %nq, align 4
  br label %for.inc65

for.inc65:                                        ; preds = %cond.end63
  %83 = load i32, i32* %sign, align 4
  %add66 = add nsw i32 %83, 2
  store i32 %add66, i32* %sign, align 4
  br label %for.cond1

for.end67:                                        ; preds = %for.cond1
  br label %for.inc68

for.inc68:                                        ; preds = %for.end67
  %84 = load i32, i32* %dir, align 4
  %inc69 = add nsw i32 %84, 1
  store i32 %inc69, i32* %dir, align 4
  br label %for.cond

for.end70:                                        ; preds = %for.cond
  br label %done

done:                                             ; preds = %for.end70, %if.then56, %if.then13
  %85 = load float*, float** %q, align 4
  %86 = load float*, float** %ret.addr, align 4
  %cmp71 = icmp ne float* %85, %86
  br i1 %cmp71, label %if.then72, label %if.end75

if.then72:                                        ; preds = %done
  %87 = load float*, float** %ret.addr, align 4
  %88 = bitcast float* %87 to i8*
  %89 = load float*, float** %q, align 4
  %90 = bitcast float* %89 to i8*
  %91 = load i32, i32* %nr, align 4
  %mul73 = mul nsw i32 %91, 2
  %mul74 = mul i32 %mul73, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %88, i8* align 4 %90, i32 %mul74, i1 false)
  br label %if.end75

if.end75:                                         ; preds = %if.then72, %done
  %92 = load i32, i32* %nr, align 4
  ret i32 %92
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%struct.btBoxBoxDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %0, i1 zeroext %1) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %.addr = alloca %class.btIDebugDraw*, align 4
  %.addr1 = alloca i8, align 1
  %transformA = alloca %class.btTransform*, align 4
  %transformB = alloca %class.btTransform*, align 4
  %skip = alloca i32, align 4
  %contact = alloca %struct.dContactGeom*, align 4
  %R1 = alloca [12 x float], align 16
  %R2 = alloca [12 x float], align 16
  %j = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  %depth = alloca float, align 4
  %return_code = alloca i32, align 4
  %maxc = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp43 = alloca %class.btVector3, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %.addr, align 4
  %frombool = zext i1 %1 to i8
  store i8 %frombool, i8* %.addr1, align 1
  %this2 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %2 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %2, i32 0, i32 0
  store %class.btTransform* %m_transformA, %class.btTransform** %transformA, align 4
  %3 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %3, i32 0, i32 1
  store %class.btTransform* %m_transformB, %class.btTransform** %transformB, align 4
  store i32 0, i32* %skip, align 4
  store %struct.dContactGeom* null, %struct.dContactGeom** %contact, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btTransform*, %class.btTransform** %transformA, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %6 = load i32, i32* %j, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call, i32 %6)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call3)
  %7 = load float, float* %call4, align 4
  %8 = load i32, i32* %j, align 4
  %mul = mul nsw i32 4, %8
  %add = add nsw i32 0, %mul
  %arrayidx = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add
  store float %7, float* %arrayidx, align 4
  %9 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %9)
  %10 = load i32, i32* %j, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call5, i32 %10)
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call6)
  %11 = load float, float* %call7, align 4
  %12 = load i32, i32* %j, align 4
  %mul8 = mul nsw i32 4, %12
  %add9 = add nsw i32 0, %mul8
  %arrayidx10 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add9
  store float %11, float* %arrayidx10, align 4
  %13 = load %class.btTransform*, %class.btTransform** %transformA, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %13)
  %14 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call11, i32 %14)
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call12)
  %15 = load float, float* %call13, align 4
  %16 = load i32, i32* %j, align 4
  %mul14 = mul nsw i32 4, %16
  %add15 = add nsw i32 1, %mul14
  %arrayidx16 = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add15
  store float %15, float* %arrayidx16, align 4
  %17 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call17 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  %18 = load i32, i32* %j, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call17, i32 %18)
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call18)
  %19 = load float, float* %call19, align 4
  %20 = load i32, i32* %j, align 4
  %mul20 = mul nsw i32 4, %20
  %add21 = add nsw i32 1, %mul20
  %arrayidx22 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add21
  store float %19, float* %arrayidx22, align 4
  %21 = load %class.btTransform*, %class.btTransform** %transformA, align 4
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %21)
  %22 = load i32, i32* %j, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call23, i32 %22)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call24)
  %23 = load float, float* %call25, align 4
  %24 = load i32, i32* %j, align 4
  %mul26 = mul nsw i32 4, %24
  %add27 = add nsw i32 2, %mul26
  %arrayidx28 = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 %add27
  store float %23, float* %arrayidx28, align 4
  %25 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call29 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %25)
  %26 = load i32, i32* %j, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call29, i32 %26)
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call30)
  %27 = load float, float* %call31, align 4
  %28 = load i32, i32* %j, align 4
  %mul32 = mul nsw i32 4, %28
  %add33 = add nsw i32 2, %mul32
  %arrayidx34 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 %add33
  store float %27, float* %arrayidx34, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %j, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call35 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  store i32 4, i32* %maxc, align 4
  %30 = load %class.btTransform*, %class.btTransform** %transformA, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %30)
  %arraydecay = getelementptr inbounds [12 x float], [12 x float]* %R1, i32 0, i32 0
  store float 2.000000e+00, float* %ref.tmp37, align 4
  %m_box1 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this2, i32 0, i32 1
  %31 = load %class.btBoxShape*, %class.btBoxShape** %m_box1, align 4
  call void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp38, %class.btBoxShape* %31)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %32 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %32)
  %arraydecay40 = getelementptr inbounds [12 x float], [12 x float]* %R2, i32 0, i32 0
  store float 2.000000e+00, float* %ref.tmp42, align 4
  %m_box2 = getelementptr inbounds %struct.btBoxBoxDetector, %struct.btBoxBoxDetector* %this2, i32 0, i32 2
  %33 = load %class.btBoxShape*, %class.btBoxShape** %m_box2, align 4
  call void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp43, %class.btBoxShape* %33)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp43)
  %34 = load i32, i32* %maxc, align 4
  %35 = load %struct.dContactGeom*, %struct.dContactGeom** %contact, align 4
  %36 = load i32, i32* %skip, align 4
  %37 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %call44 = call i32 @_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %call36, float* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call39, float* %arraydecay40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* %depth, i32* %return_code, i32 %34, %struct.dContactGeom* %35, i32 %36, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %37)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK10btBoxShape24getHalfExtentsWithMarginEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btBoxShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btBoxShape*, align 4
  %margin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btBoxShape* %this, %class.btBoxShape** %this.addr, align 4
  %this1 = load %class.btBoxShape*, %class.btBoxShape** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv(%class.btBoxShape* %this1)
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %2 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %3 = bitcast %class.btConvexInternalShape* %2 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %3, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %4 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call2 = call float %4(%class.btConvexInternalShape* %2)
  store float %call2, float* %ref.tmp, align 4
  %5 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %6 = bitcast %class.btConvexInternalShape* %5 to float (%class.btConvexInternalShape*)***
  %vtable4 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %6, align 4
  %vfn5 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable4, i64 12
  %7 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn5, align 4
  %call6 = call float %7(%class.btConvexInternalShape* %5)
  store float %call6, float* %ref.tmp3, align 4
  %8 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %9 = bitcast %class.btConvexInternalShape* %8 to float (%class.btConvexInternalShape*)***
  %vtable8 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %9, align 4
  %vfn9 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable8, i64 12
  %10 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn9, align 4
  %call10 = call float %10(%class.btConvexInternalShape* %8)
  store float %call10, float* %ref.tmp7, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %margin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBoxBoxDetector* @_ZN16btBoxBoxDetectorD2Ev(%struct.btBoxBoxDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4
  %this1 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %0 = bitcast %struct.btBoxBoxDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  ret %struct.btBoxBoxDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btBoxBoxDetectorD0Ev(%struct.btBoxBoxDetector* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBoxBoxDetector*, align 4
  store %struct.btBoxBoxDetector* %this, %struct.btBoxBoxDetector** %this.addr, align 4
  %this1 = load %struct.btBoxBoxDetector*, %struct.btBoxBoxDetector** %this.addr, align 4
  %call = call %struct.btBoxBoxDetector* @_ZN16btBoxBoxDetectorD2Ev(%struct.btBoxBoxDetector* %this1) #8
  %0 = bitcast %struct.btBoxBoxDetector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev(%struct.btDiscreteCollisionDetectorInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK10btBoxShape27getHalfExtentsWithoutMarginEv(%class.btBoxShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBoxShape*, align 4
  store %class.btBoxShape* %this, %class.btBoxShape** %this.addr, align 4
  %this1 = load %class.btBoxShape*, %class.btBoxShape** %this.addr, align 4
  %0 = bitcast %class.btBoxShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  ret %class.btVector3* %m_implicitShapeDimensions
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btBoxBoxDetector.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
