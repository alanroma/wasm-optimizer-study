; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btPersistentManifold.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btPersistentManifold.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btCollisionObject = type opaque
%class.btVector4 = type { %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN13btTypedObjectC2Ei = comdat any

$_ZN15btManifoldPointC2Ev = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector4C2ERKfS1_S1_S1_ = comdat any

$_ZNK9btVector412closestAxis4Ev = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK20btPersistentManifold20validContactDistanceERK15btManifoldPoint = comdat any

$_ZN20btPersistentManifold18removeContactPointEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_ZNK9btVector49absolute4Ev = comdat any

$_ZNK9btVector48maxAxis4Ev = comdat any

$_Z6btFabsf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gContactBreakingThreshold = hidden global float 0x3F947AE140000000, align 4
@gContactDestroyedCallback = hidden global i1 (i8*)* null, align 4
@gContactProcessedCallback = hidden global i1 (%class.btManifoldPoint*, i8*, i8*)* null, align 4
@gContactCalcArea3Points = hidden global i8 1, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btPersistentManifold.cpp, i8* null }]

@_ZN20btPersistentManifoldC1Ev = hidden unnamed_addr alias %class.btPersistentManifold* (%class.btPersistentManifold*), %class.btPersistentManifold* (%class.btPersistentManifold*)* @_ZN20btPersistentManifoldC2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btPersistentManifold* @_ZN20btPersistentManifoldC2Ev(%class.btPersistentManifold* returned %this) unnamed_addr #2 {
entry:
  %retval = alloca %class.btPersistentManifold*, align 4
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  store %class.btPersistentManifold* %this1, %class.btPersistentManifold** %retval, align 4
  %0 = bitcast %class.btPersistentManifold* %this1 to %struct.btTypedObject*
  %call = call %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* %0, i32 1025)
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btManifoldPoint* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btManifoldPoint* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_body0, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_body1, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  store i32 0, i32* %m_cachedPoints, align 4
  %m_index1a = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 9
  store i32 0, i32* %m_index1a, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %retval, align 4
  ret %class.btPersistentManifold* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* returned %this, i32 %objectType) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btTypedObject*, align 4
  %objectType.addr = alloca i32, align 4
  store %struct.btTypedObject* %this, %struct.btTypedObject** %this.addr, align 4
  store i32 %objectType, i32* %objectType.addr, align 4
  %this1 = load %struct.btTypedObject*, %struct.btTypedObject** %this.addr, align 4
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %this1, i32 0, i32 0
  %0 = load i32, i32* %objectType.addr, align 4
  store i32 %0, i32* %m_objectType, align 4
  ret %struct.btTypedObject* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointA)
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointB)
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnB)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnA)
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalWorldOnB)
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 14
  store i8* null, i8** %m_userPersistentData, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 15
  store i32 0, i32* %m_contactPointFlags, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_contactMotion1, align 4
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_contactMotion2, align 4
  %0 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 21
  %m_contactCFM = bitcast %union.anon.0* %0 to float*
  store float 0.000000e+00, float* %m_contactCFM, align 4
  %1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  %m_contactERP = bitcast %union.anon.1* %1 to float*
  store float 0.000000e+00, float* %m_contactERP, align 4
  %m_frictionCFM = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 23
  store float 0.000000e+00, float* %m_frictionCFM, align 4
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 24
  store i32 0, i32* %m_lifeTime, align 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 25
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir1)
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 26
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir2)
  ret %class.btManifoldPoint* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %pt) #2 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %pt.addr = alloca %class.btManifoldPoint*, align 4
  %oldPtr = alloca i8*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btManifoldPoint* %pt, %class.btManifoldPoint** %pt.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %0 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %0, i32 0, i32 14
  %1 = load i8*, i8** %m_userPersistentData, align 4
  store i8* %1, i8** %oldPtr, align 4
  %2 = load i8*, i8** %oldPtr, align 4
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %3 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_userPersistentData2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %3, i32 0, i32 14
  %4 = load i8*, i8** %m_userPersistentData2, align 4
  %tobool3 = icmp ne i8* %4, null
  br i1 %tobool3, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %5 = load i1 (i8*)*, i1 (i8*)** @gContactDestroyedCallback, align 4
  %tobool4 = icmp ne i1 (i8*)* %5, null
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %land.lhs.true
  %6 = load i1 (i8*)*, i1 (i8*)** @gContactDestroyedCallback, align 4
  %7 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_userPersistentData6 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %7, i32 0, i32 14
  %8 = load i8*, i8** %m_userPersistentData6, align 4
  %call = call zeroext i1 %6(i8* %8)
  %9 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_userPersistentData7 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %9, i32 0, i32 14
  store i8* null, i8** %m_userPersistentData7, align 4
  br label %if.end

if.end:                                           ; preds = %if.then5, %land.lhs.true, %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %pt) #2 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %pt.addr = alloca %class.btManifoldPoint*, align 4
  %maxPenetrationIndex = alloca i32, align 4
  %maxPenetration = alloca float, align 4
  %i = alloca i32, align 4
  %res0 = alloca float, align 4
  %res1 = alloca float, align 4
  %res2 = alloca float, align 4
  %res3 = alloca float, align 4
  %a0 = alloca %class.btVector3, align 4
  %b0 = alloca %class.btVector3, align 4
  %cross = alloca %class.btVector3, align 4
  %a1 = alloca %class.btVector3, align 4
  %b1 = alloca %class.btVector3, align 4
  %cross33 = alloca %class.btVector3, align 4
  %a2 = alloca %class.btVector3, align 4
  %b2 = alloca %class.btVector3, align 4
  %cross48 = alloca %class.btVector3, align 4
  %a3 = alloca %class.btVector3, align 4
  %b3 = alloca %class.btVector3, align 4
  %cross63 = alloca %class.btVector3, align 4
  %maxvec = alloca %class.btVector4, align 4
  %biggestarea = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btManifoldPoint* %pt, %class.btManifoldPoint** %pt.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  store i32 -1, i32* %maxPenetrationIndex, align 4
  %0 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %call = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %0)
  store float %call, float* %maxPenetration, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %2
  %call2 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %arrayidx)
  %3 = load float, float* %maxPenetration, align 4
  %cmp3 = fcmp olt float %call2, %3
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  store i32 %4, i32* %maxPenetrationIndex, align 4
  %m_pointCache4 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache4, i32 0, i32 %5
  %call6 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %arrayidx5)
  store float %call6, float* %maxPenetration, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store float 0.000000e+00, float* %res0, align 4
  store float 0.000000e+00, float* %res1, align 4
  store float 0.000000e+00, float* %res2, align 4
  store float 0.000000e+00, float* %res3, align 4
  %7 = load i8, i8* @gContactCalcArea3Points, align 1
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %if.then7, label %if.else

if.then7:                                         ; preds = %for.end
  %8 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp8 = icmp ne i32 %8, 0
  br i1 %cmp8, label %if.then9, label %if.end20

if.then9:                                         ; preds = %if.then7
  %9 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %9, i32 0, i32 0
  %m_pointCache10 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache10, i32 0, i32 1
  %m_localPointA12 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx11, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA12)
  %m_pointCache13 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache13, i32 0, i32 3
  %m_localPointA15 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx14, i32 0, i32 0
  %m_pointCache16 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx17 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache16, i32 0, i32 2
  %m_localPointA18 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx17, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %b0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA18)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %cross, %class.btVector3* %a0, %class.btVector3* nonnull align 4 dereferenceable(16) %b0)
  %call19 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %cross)
  store float %call19, float* %res0, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then9, %if.then7
  %10 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp21 = icmp ne i32 %10, 1
  br i1 %cmp21, label %if.then22, label %if.end35

if.then22:                                        ; preds = %if.end20
  %11 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA23 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %11, i32 0, i32 0
  %m_pointCache24 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx25 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache24, i32 0, i32 0
  %m_localPointA26 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx25, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA26)
  %m_pointCache27 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache27, i32 0, i32 3
  %m_localPointA29 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx28, i32 0, i32 0
  %m_pointCache30 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache30, i32 0, i32 2
  %m_localPointA32 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx31, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %b1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA29, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA32)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %cross33, %class.btVector3* %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %b1)
  %call34 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %cross33)
  store float %call34, float* %res1, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.then22, %if.end20
  %12 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp36 = icmp ne i32 %12, 2
  br i1 %cmp36, label %if.then37, label %if.end50

if.then37:                                        ; preds = %if.end35
  %13 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA38 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %13, i32 0, i32 0
  %m_pointCache39 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache39, i32 0, i32 0
  %m_localPointA41 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx40, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA38, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA41)
  %m_pointCache42 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache42, i32 0, i32 3
  %m_localPointA44 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx43, i32 0, i32 0
  %m_pointCache45 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx46 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache45, i32 0, i32 1
  %m_localPointA47 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx46, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %b2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA44, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA47)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %cross48, %class.btVector3* %a2, %class.btVector3* nonnull align 4 dereferenceable(16) %b2)
  %call49 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %cross48)
  store float %call49, float* %res2, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.then37, %if.end35
  %14 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp51 = icmp ne i32 %14, 3
  br i1 %cmp51, label %if.then52, label %if.end65

if.then52:                                        ; preds = %if.end50
  %15 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA53 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %15, i32 0, i32 0
  %m_pointCache54 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx55 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache54, i32 0, i32 0
  %m_localPointA56 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx55, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %a3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA53, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA56)
  %m_pointCache57 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx58 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache57, i32 0, i32 2
  %m_localPointA59 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx58, i32 0, i32 0
  %m_pointCache60 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx61 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache60, i32 0, i32 1
  %m_localPointA62 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx61, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %b3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA59, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA62)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %cross63, %class.btVector3* %a3, %class.btVector3* nonnull align 4 dereferenceable(16) %b3)
  %call64 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %cross63)
  store float %call64, float* %res3, align 4
  br label %if.end65

if.end65:                                         ; preds = %if.then52, %if.end50
  br label %if.end122

if.else:                                          ; preds = %for.end
  %16 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp66 = icmp ne i32 %16, 0
  br i1 %cmp66, label %if.then67, label %if.end79

if.then67:                                        ; preds = %if.else
  %17 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA68 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %17, i32 0, i32 0
  %m_pointCache69 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx70 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache69, i32 0, i32 1
  %m_localPointA71 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx70, i32 0, i32 0
  %m_pointCache72 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx73 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache72, i32 0, i32 2
  %m_localPointA74 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx73, i32 0, i32 0
  %m_pointCache75 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx76 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache75, i32 0, i32 3
  %m_localPointA77 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx76, i32 0, i32 0
  %call78 = call float @_ZL15calcArea4PointsRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA68, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA71, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA74, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA77)
  store float %call78, float* %res0, align 4
  br label %if.end79

if.end79:                                         ; preds = %if.then67, %if.else
  %18 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp80 = icmp ne i32 %18, 1
  br i1 %cmp80, label %if.then81, label %if.end93

if.then81:                                        ; preds = %if.end79
  %19 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA82 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %19, i32 0, i32 0
  %m_pointCache83 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx84 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache83, i32 0, i32 0
  %m_localPointA85 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx84, i32 0, i32 0
  %m_pointCache86 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx87 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache86, i32 0, i32 2
  %m_localPointA88 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx87, i32 0, i32 0
  %m_pointCache89 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx90 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache89, i32 0, i32 3
  %m_localPointA91 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx90, i32 0, i32 0
  %call92 = call float @_ZL15calcArea4PointsRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA82, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA85, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA88, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA91)
  store float %call92, float* %res1, align 4
  br label %if.end93

if.end93:                                         ; preds = %if.then81, %if.end79
  %20 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp94 = icmp ne i32 %20, 2
  br i1 %cmp94, label %if.then95, label %if.end107

if.then95:                                        ; preds = %if.end93
  %21 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA96 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %21, i32 0, i32 0
  %m_pointCache97 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx98 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache97, i32 0, i32 0
  %m_localPointA99 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx98, i32 0, i32 0
  %m_pointCache100 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx101 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache100, i32 0, i32 1
  %m_localPointA102 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx101, i32 0, i32 0
  %m_pointCache103 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx104 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache103, i32 0, i32 3
  %m_localPointA105 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx104, i32 0, i32 0
  %call106 = call float @_ZL15calcArea4PointsRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA96, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA99, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA102, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA105)
  store float %call106, float* %res2, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.then95, %if.end93
  %22 = load i32, i32* %maxPenetrationIndex, align 4
  %cmp108 = icmp ne i32 %22, 3
  br i1 %cmp108, label %if.then109, label %if.end121

if.then109:                                       ; preds = %if.end107
  %23 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_localPointA110 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %23, i32 0, i32 0
  %m_pointCache111 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx112 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache111, i32 0, i32 0
  %m_localPointA113 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx112, i32 0, i32 0
  %m_pointCache114 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx115 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache114, i32 0, i32 1
  %m_localPointA116 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx115, i32 0, i32 0
  %m_pointCache117 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %arrayidx118 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache117, i32 0, i32 2
  %m_localPointA119 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx118, i32 0, i32 0
  %call120 = call float @_ZL15calcArea4PointsRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA110, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA113, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA116, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA119)
  store float %call120, float* %res3, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then109, %if.end107
  br label %if.end122

if.end122:                                        ; preds = %if.end121, %if.end65
  %call123 = call %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* %maxvec, float* nonnull align 4 dereferenceable(4) %res0, float* nonnull align 4 dereferenceable(4) %res1, float* nonnull align 4 dereferenceable(4) %res2, float* nonnull align 4 dereferenceable(4) %res3)
  %call124 = call i32 @_ZNK9btVector412closestAxis4Ev(%class.btVector4* %maxvec)
  store i32 %call124, i32* %biggestarea, align 4
  %24 = load i32, i32* %biggestarea, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define internal float @_ZL15calcArea4PointsRK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2, %class.btVector3* nonnull align 4 dereferenceable(16) %p3) #2 {
entry:
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  %p3.addr = alloca %class.btVector3*, align 4
  %a = alloca [3 x %class.btVector3], align 16
  %b = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %tmp0 = alloca %class.btVector3, align 4
  %tmp1 = alloca %class.btVector3, align 4
  %tmp2 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4
  store %class.btVector3* %p3, %class.btVector3** %p3.addr, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %array.begin1 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 0
  %arrayctor.end2 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin1, i32 3
  br label %arrayctor.loop3

arrayctor.loop3:                                  ; preds = %arrayctor.loop3, %arrayctor.cont
  %arrayctor.cur4 = phi %class.btVector3* [ %array.begin1, %arrayctor.cont ], [ %arrayctor.next6, %arrayctor.loop3 ]
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur4)
  %arrayctor.next6 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur4, i32 1
  %arrayctor.done7 = icmp eq %class.btVector3* %arrayctor.next6, %arrayctor.end2
  br i1 %arrayctor.done7, label %arrayctor.cont8, label %arrayctor.loop3

arrayctor.cont8:                                  ; preds = %arrayctor.loop3
  %0 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 0
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 1
  %6 = bitcast %class.btVector3* %arrayidx10 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %p3.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx12 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %p3.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 0
  %14 = bitcast %class.btVector3* %arrayidx14 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %14, i8* align 4 %15, i32 16, i1 false)
  %16 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %p3.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 1
  %18 = bitcast %class.btVector3* %arrayidx16 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %18, i8* align 4 %19, i32 16, i1 false)
  %20 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %21 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21)
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 2
  %22 = bitcast %class.btVector3* %arrayidx18 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %22, i8* align 4 %23, i32 16, i1 false)
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 0
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %tmp0, %class.btVector3* %arrayidx19, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %tmp1, %class.btVector3* %arrayidx21, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx22)
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %a, i32 0, i32 2
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %b, i32 0, i32 2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %tmp2, %class.btVector3* %arrayidx23, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  %call26 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %tmp0)
  store float %call26, float* %ref.tmp25, align 4
  %call28 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %tmp1)
  store float %call28, float* %ref.tmp27, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27)
  %call31 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %tmp2)
  store float %call31, float* %ref.tmp30, align 4
  %call32 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %call29, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  %24 = load float, float* %call32, align 4
  ret float %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3)
  %4 = load float*, float** %_w.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  store float %5, float* %arrayidx, align 4
  ret %class.btVector4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btVector412closestAxis4Ev(%class.btVector4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %ref.tmp = alloca %class.btVector4, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  call void @_ZNK9btVector49absolute4Ev(%class.btVector4* sret align 4 %ref.tmp, %class.btVector4* %this1)
  %call = call i32 @_ZNK9btVector48maxAxis4Ev(%class.btVector4* %ref.tmp)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK20btPersistentManifold13getCacheEntryERK15btManifoldPoint(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %newPoint) #2 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %newPoint.addr = alloca %class.btManifoldPoint*, align 4
  %shortestDist = alloca float, align 4
  %size = alloca i32, align 4
  %nearestPoint = alloca i32, align 4
  %i = alloca i32, align 4
  %mp = alloca %class.btManifoldPoint*, align 4
  %diffA = alloca %class.btVector3, align 4
  %distToManiPoint = alloca float, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btManifoldPoint* %newPoint, %class.btManifoldPoint** %newPoint.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %call = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this1)
  %call2 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this1)
  %mul = fmul float %call, %call2
  store float %mul, float* %shortestDist, align 4
  %call3 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this1)
  store i32 %call3, i32* %size, align 4
  store i32 -1, i32* %nearestPoint, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %size, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %2
  store %class.btManifoldPoint* %arrayidx, %class.btManifoldPoint** %mp, align 4
  %3 = load %class.btManifoldPoint*, %class.btManifoldPoint** %mp, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %3, i32 0, i32 0
  %4 = load %class.btManifoldPoint*, %class.btManifoldPoint** %newPoint.addr, align 4
  %m_localPointA4 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %4, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diffA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA4)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %diffA, %class.btVector3* nonnull align 4 dereferenceable(16) %diffA)
  store float %call5, float* %distToManiPoint, align 4
  %5 = load float, float* %distToManiPoint, align 4
  %6 = load float, float* %shortestDist, align 4
  %cmp6 = fcmp olt float %5, %6
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %7 = load float, float* %distToManiPoint, align 4
  store float %7, float* %shortestDist, align 4
  %8 = load i32, i32* %i, align 4
  store i32 %8, i32* %nearestPoint, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load i32, i32* %nearestPoint, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this) #1 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_contactBreakingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 5
  %0 = load float, float* %m_contactBreakingThreshold, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN20btPersistentManifold16addManifoldPointERK15btManifoldPointb(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %newPoint, i1 zeroext %isPredictive) #2 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %newPoint.addr = alloca %class.btManifoldPoint*, align 4
  %isPredictive.addr = alloca i8, align 1
  %insertIndex = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btManifoldPoint* %newPoint, %class.btManifoldPoint** %newPoint.addr, align 4
  %frombool = zext i1 %isPredictive to i8
  store i8 %frombool, i8* %isPredictive.addr, align 1
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %0 = load i8, i8* %isPredictive.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this1)
  store i32 %call, i32* %insertIndex, align 4
  %1 = load i32, i32* %insertIndex, align 4
  %cmp = icmp eq i32 %1, 4
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %2 = load %class.btManifoldPoint*, %class.btManifoldPoint** %newPoint.addr, align 4
  %call3 = call i32 @_ZN20btPersistentManifold16sortCachedPointsERK15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %2)
  store i32 %call3, i32* %insertIndex, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %3 = load i32, i32* %insertIndex, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %3
  call void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %arrayidx)
  br label %if.end4

if.else:                                          ; preds = %if.end
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %4 = load i32, i32* %m_cachedPoints, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %m_cachedPoints, align 4
  br label %if.end4

if.end4:                                          ; preds = %if.else, %if.then2
  %5 = load i32, i32* %insertIndex, align 4
  %cmp5 = icmp slt i32 %5, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 0, i32* %insertIndex, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end4
  %6 = load %class.btManifoldPoint*, %class.btManifoldPoint** %newPoint.addr, align 4
  %m_pointCache8 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %7 = load i32, i32* %insertIndex, align 4
  %arrayidx9 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache8, i32 0, i32 %7
  %8 = bitcast %class.btManifoldPoint* %arrayidx9 to i8*
  %9 = bitcast %class.btManifoldPoint* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 192, i1 false)
  %10 = load i32, i32* %insertIndex, align 4
  ret i32 %10
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trA, %class.btTransform* nonnull align 4 dereferenceable(64) %trB) #2 {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %trA.addr = alloca %class.btTransform*, align 4
  %trB.addr = alloca %class.btTransform*, align 4
  %i = alloca i32, align 4
  %manifoldPoint = alloca %class.btManifoldPoint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %distance2d = alloca float, align 4
  %projectedDifference = alloca %class.btVector3, align 4
  %projectedPoint = alloca %class.btVector3, align 4
  %manifoldPoint14 = alloca %class.btManifoldPoint*, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btTransform* %trA, %class.btTransform** %trA.addr, align 4
  store %class.btTransform* %trB, %class.btTransform** %trB.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this1)
  %sub = sub nsw i32 %call, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %1
  store %class.btManifoldPoint* %arrayidx, %class.btManifoldPoint** %manifoldPoint, align 4
  %2 = load %class.btTransform*, %class.btTransform** %trA.addr, align 4
  %3 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %3, i32 0, i32 0
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointA)
  %4 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %4, i32 0, i32 3
  %5 = bitcast %class.btVector3* %m_positionWorldOnA to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btTransform*, %class.btTransform** %trB.addr, align 4
  %8 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %8, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btTransform* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB)
  %9 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %9, i32 0, i32 2
  %10 = bitcast %class.btVector3* %m_positionWorldOnB to i8*
  %11 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_positionWorldOnA4 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %12, i32 0, i32 3
  %13 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_positionWorldOnB5 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %13, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB5)
  %14 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %14, i32 0, i32 4
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB)
  %15 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %15, i32 0, i32 5
  store float %call6, float* %m_distance1, align 4
  %16 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint, align 4
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %16, i32 0, i32 24
  %17 = load i32, i32* %m_lifeTime, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %m_lifeTime, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4
  %dec = add nsw i32 %18, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %projectedDifference)
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %projectedPoint)
  %call9 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this1)
  %sub10 = sub nsw i32 %call9, 1
  store i32 %sub10, i32* %i, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc35, %for.end
  %19 = load i32, i32* %i, align 4
  %cmp12 = icmp sge i32 %19, 0
  br i1 %cmp12, label %for.body13, label %for.end37

for.body13:                                       ; preds = %for.cond11
  %m_pointCache15 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %20 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache15, i32 0, i32 %20
  store %class.btManifoldPoint* %arrayidx16, %class.btManifoldPoint** %manifoldPoint14, align 4
  %21 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %call17 = call zeroext i1 @_ZNK20btPersistentManifold20validContactDistanceERK15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %21)
  br i1 %call17, label %if.else, label %if.then

if.then:                                          ; preds = %for.body13
  %22 = load i32, i32* %i, align 4
  call void @_ZN20btPersistentManifold18removeContactPointEi(%class.btPersistentManifold* %this1, i32 %22)
  br label %if.end34

if.else:                                          ; preds = %for.body13
  %23 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %m_positionWorldOnA19 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %23, i32 0, i32 3
  %24 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %m_normalWorldOnB21 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %24, i32 0, i32 4
  %25 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %m_distance122 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %25, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB21, float* nonnull align 4 dereferenceable(4) %m_distance122)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %26 = bitcast %class.btVector3* %projectedPoint to i8*
  %27 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %28 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %m_positionWorldOnB24 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %28, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB24, %class.btVector3* nonnull align 4 dereferenceable(16) %projectedPoint)
  %29 = bitcast %class.btVector3* %projectedDifference to i8*
  %30 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %projectedDifference, %class.btVector3* nonnull align 4 dereferenceable(16) %projectedDifference)
  store float %call25, float* %distance2d, align 4
  %31 = load float, float* %distance2d, align 4
  %call26 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this1)
  %call27 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this1)
  %mul = fmul float %call26, %call27
  %cmp28 = fcmp ogt float %31, %mul
  br i1 %cmp28, label %if.then29, label %if.else30

if.then29:                                        ; preds = %if.else
  %32 = load i32, i32* %i, align 4
  call void @_ZN20btPersistentManifold18removeContactPointEi(%class.btPersistentManifold* %this1, i32 %32)
  br label %if.end33

if.else30:                                        ; preds = %if.else
  %33 = load i1 (%class.btManifoldPoint*, i8*, i8*)*, i1 (%class.btManifoldPoint*, i8*, i8*)** @gContactProcessedCallback, align 4
  %tobool = icmp ne i1 (%class.btManifoldPoint*, i8*, i8*)* %33, null
  br i1 %tobool, label %if.then31, label %if.end

if.then31:                                        ; preds = %if.else30
  %34 = load i1 (%class.btManifoldPoint*, i8*, i8*)*, i1 (%class.btManifoldPoint*, i8*, i8*)** @gContactProcessedCallback, align 4
  %35 = load %class.btManifoldPoint*, %class.btManifoldPoint** %manifoldPoint14, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %36 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  %37 = bitcast %class.btCollisionObject* %36 to i8*
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  %39 = bitcast %class.btCollisionObject* %38 to i8*
  %call32 = call zeroext i1 %34(%class.btManifoldPoint* nonnull align 4 dereferenceable(192) %35, i8* %37, i8* %39)
  br label %if.end

if.end:                                           ; preds = %if.then31, %if.else30
  br label %if.end33

if.end33:                                         ; preds = %if.end, %if.then29
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then
  br label %for.inc35

for.inc35:                                        ; preds = %if.end34
  %40 = load i32, i32* %i, align 4
  %dec36 = add nsw i32 %40, -1
  store i32 %dec36, i32* %i, align 4
  br label %for.cond11

for.end37:                                        ; preds = %for.cond11
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btPersistentManifold20validContactDistanceERK15btManifoldPoint(%class.btPersistentManifold* %this, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %pt) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %pt.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btManifoldPoint* %pt, %class.btManifoldPoint** %pt.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %0 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %0, i32 0, i32 5
  %1 = load float, float* %m_distance1, align 4
  %call = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %this1)
  %cmp = fcmp ole float %1, %call
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btPersistentManifold18removeContactPointEi(%class.btPersistentManifold* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  %lastUsedIndex = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  call void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %arrayidx)
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this1)
  %sub = sub nsw i32 %call, 1
  store i32 %sub, i32* %lastUsedIndex, align 4
  %1 = load i32, i32* %index.addr, align 4
  %2 = load i32, i32* %lastUsedIndex, align 4
  %cmp = icmp ne i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_pointCache2 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %3 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx3 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache2, i32 0, i32 %3
  %m_pointCache4 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %4 = load i32, i32* %index.addr, align 4
  %arrayidx5 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache4, i32 0, i32 %4
  %5 = bitcast %class.btManifoldPoint* %arrayidx5 to i8*
  %6 = bitcast %class.btManifoldPoint* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 192, i1 false)
  %m_pointCache6 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %7 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx7 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache6, i32 0, i32 %7
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx7, i32 0, i32 14
  store i8* null, i8** %m_userPersistentData, align 4
  %m_pointCache8 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %8 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx9 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache8, i32 0, i32 %8
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx9, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_pointCache10 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %9 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx11 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache10, i32 0, i32 %9
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx11, i32 0, i32 15
  store i32 0, i32* %m_contactPointFlags, align 4
  %m_pointCache12 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %10 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx13 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache12, i32 0, i32 %10
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx13, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4
  %m_pointCache14 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %11 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx15 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache14, i32 0, i32 %11
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx15, i32 0, i32 18
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4
  %m_pointCache16 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %12 = load i32, i32* %lastUsedIndex, align 4
  %arrayidx17 = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache16, i32 0, i32 %12
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayidx17, i32 0, i32 24
  store i32 0, i32* %m_lifeTime, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %13 = load i32, i32* %m_cachedPoints, align 4
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* %m_cachedPoints, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector49absolute4Ev(%class.btVector4* noalias sret align 4 %agg.result, %class.btVector4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btFabsf(float %1)
  store float %call, float* %ref.tmp, align 4
  %2 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats3 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %call5 = call float @_Z6btFabsf(float %3)
  store float %call5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %call9 = call float @_Z6btFabsf(float %5)
  store float %call9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %7 = load float, float* %arrayidx12, align 4
  %call13 = call float @_Z6btFabsf(float %7)
  store float %call13, float* %ref.tmp10, align 4
  %call14 = call %class.btVector4* @_ZN9btVector4C2ERKfS1_S1_S1_(%class.btVector4* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector48maxAxis4Ev(%class.btVector4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %maxIndex = alloca i32, align 4
  %maxVal = alloca float, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  store i32 -1, i32* %maxIndex, align 4
  store float 0xC3ABC16D60000000, float* %maxVal, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float, float* %maxVal, align 4
  %cmp = fcmp ogt float %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %maxIndex, align 4
  %3 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %4 = load float, float* %arrayidx3, align 4
  store float %4, float* %maxVal, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %6 = load float, float* %arrayidx5, align 4
  %7 = load float, float* %maxVal, align 4
  %cmp6 = fcmp ogt float %6, %7
  br i1 %cmp6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end
  store i32 1, i32* %maxIndex, align 4
  %8 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 1
  %9 = load float, float* %arrayidx9, align 4
  store float %9, float* %maxVal, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.end
  %10 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %11 = load float, float* %arrayidx12, align 4
  %12 = load float, float* %maxVal, align 4
  %cmp13 = fcmp ogt float %11, %12
  br i1 %cmp13, label %if.then14, label %if.end17

if.then14:                                        ; preds = %if.end10
  store i32 2, i32* %maxIndex, align 4
  %13 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 2
  %14 = load float, float* %arrayidx16, align 4
  store float %14, float* %maxVal, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then14, %if.end10
  %15 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats18 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %m_floats18, i32 0, i32 3
  %16 = load float, float* %arrayidx19, align 4
  %17 = load float, float* %maxVal, align 4
  %cmp20 = fcmp ogt float %16, %17
  br i1 %cmp20, label %if.then21, label %if.end24

if.then21:                                        ; preds = %if.end17
  store i32 3, i32* %maxIndex, align 4
  %18 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 3
  %19 = load float, float* %arrayidx23, align 4
  store float %19, float* %maxVal, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then21, %if.end17
  %20 = load i32, i32* %maxIndex, align 4
  ret i32 %20
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btPersistentManifold.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
