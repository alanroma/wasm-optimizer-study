; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btMinkowskiPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btIDebugDraw = type { i32 (...)** }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%struct.btIntermediateResult = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btVector3, %class.btVector3, float, i8, [3 x i8] }>
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK16btCollisionShape10isConvex2dEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN17btGjkPairDetectorD2Ev = comdat any

$_ZN33btMinkowskiPenetrationDepthSolverD2Ev = comdat any

$_ZN33btMinkowskiPenetrationDepthSolverD0Ev = comdat any

$_ZN17btBroadphaseProxy10isConvex2dEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections = internal global [62 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections = internal global i32 0, align 4
@_ZTV33btMinkowskiPenetrationDepthSolver = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI33btMinkowskiPenetrationDepthSolver to i8*), i8* bitcast (%class.btMinkowskiPenetrationDepthSolver* (%class.btMinkowskiPenetrationDepthSolver*)* @_ZN33btMinkowskiPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btMinkowskiPenetrationDepthSolver*)* @_ZN33btMinkowskiPenetrationDepthSolverD0Ev to i8*), i8* bitcast (i1 (%class.btMinkowskiPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)* @_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS33btMinkowskiPenetrationDepthSolver = hidden constant [36 x i8] c"33btMinkowskiPenetrationDepthSolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTI33btMinkowskiPenetrationDepthSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([36 x i8], [36 x i8]* @_ZTS33btMinkowskiPenetrationDepthSolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*) }, align 4
@_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult to i8*), i8* bitcast (%struct.btIntermediateResult* (%struct.btIntermediateResult*)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD2Ev to i8*), i8* bitcast (void (%struct.btIntermediateResult*)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD0Ev to i8*), i8* bitcast (void (%struct.btIntermediateResult*, i32, i32)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btIntermediateResult*, i32, i32)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btIntermediateResult*, %class.btVector3*, %class.btVector3*, float)* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult15addContactPointERKS8_SE_f to i8*)] }, align 4
@_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal constant [171 x i8] c"ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult\00", align 1
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTIZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([171 x i8], [171 x i8]* @_ZTSZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw(%class.btMinkowskiPenetrationDepthSolver* %this, %class.btVoronoiSimplexSolver* nonnull align 4 dereferenceable(357) %simplexSolver, %class.btConvexShape* %convexA, %class.btConvexShape* %convexB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btIDebugDraw* %debugDraw) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %convexB.addr = alloca %class.btConvexShape*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %check2d = alloca i8, align 1
  %minProj = alloca float, align 4
  %minNorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %minA = alloca %class.btVector3, align 4
  %minB = alloca %class.btVector3, align 4
  %seperatingAxisInA = alloca %class.btVector3, align 4
  %seperatingAxisInB = alloca %class.btVector3, align 4
  %pInA = alloca %class.btVector3, align 4
  %qInB = alloca %class.btVector3, align 4
  %pWorld = alloca %class.btVector3, align 4
  %qWorld = alloca %class.btVector3, align 4
  %w = alloca %class.btVector3, align 4
  %supportVerticesABatch = alloca [62 x %class.btVector3], align 16
  %supportVerticesBBatch = alloca [62 x %class.btVector3], align 16
  %seperatingAxisInABatch = alloca [62 x %class.btVector3], align 16
  %seperatingAxisInBBatch = alloca [62 x %class.btVector3], align 16
  %i = alloca i32, align 4
  %numSampleDirections = alloca i32, align 4
  %norm = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %numPDA = alloca i32, align 4
  %i49 = alloca i32, align 4
  %norm53 = alloca %class.btVector3, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %ref.tmp62 = alloca %class.btVector3, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %numPDB = alloca i32, align 4
  %i77 = alloca i32, align 4
  %norm81 = alloca %class.btVector3, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca %class.btVector3, align 4
  %norm111 = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp135 = alloca %class.btVector3, align 4
  %delta = alloca float, align 4
  %ref.tmp144 = alloca %class.btVector3, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp148 = alloca %class.btVector3, align 4
  %ref.tmp149 = alloca float, align 4
  %extraSeparation = alloca float, align 4
  %gjkdet = alloca %class.btGjkPairDetector, align 4
  %offsetDist = alloca float, align 4
  %offset = alloca %class.btVector3, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %newOrg = alloca %class.btVector3, align 4
  %displacedTrans = alloca %class.btTransform, align 4
  %res = alloca %struct.btIntermediateResult, align 4
  %ref.tmp166 = alloca %class.btVector3, align 4
  %correctedMinNorm = alloca float, align 4
  %penetration_relaxation = alloca float, align 4
  %ref.tmp170 = alloca %class.btVector3, align 4
  %ref.tmp171 = alloca %class.btVector3, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %convexB, %class.btConvexShape** %convexB.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %2 = bitcast %class.btConvexShape* %1 to %class.btCollisionShape*
  %call = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %2)
  br i1 %call, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %3 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %4 = bitcast %class.btConvexShape* %3 to %class.btCollisionShape*
  %call2 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %4)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %5 = phi i1 [ false, %entry ], [ %call2, %land.rhs ]
  %frombool = zext i1 %5 to i8
  store i8 %frombool, i8* %check2d, align 1
  store float 0x43ABC16D60000000, float* %minProj, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minA)
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minB)
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %seperatingAxisInA)
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %seperatingAxisInB)
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pInA)
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %qInB)
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pWorld)
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %qWorld)
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %w)
  %array.begin = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 62
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %land.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %land.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %array.begin16 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 0
  %arrayctor.end17 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin16, i32 62
  br label %arrayctor.loop18

arrayctor.loop18:                                 ; preds = %arrayctor.loop18, %arrayctor.cont
  %arrayctor.cur19 = phi %class.btVector3* [ %array.begin16, %arrayctor.cont ], [ %arrayctor.next21, %arrayctor.loop18 ]
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur19)
  %arrayctor.next21 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur19, i32 1
  %arrayctor.done22 = icmp eq %class.btVector3* %arrayctor.next21, %arrayctor.end17
  br i1 %arrayctor.done22, label %arrayctor.cont23, label %arrayctor.loop18

arrayctor.cont23:                                 ; preds = %arrayctor.loop18
  %array.begin24 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 0
  %arrayctor.end25 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin24, i32 62
  br label %arrayctor.loop26

arrayctor.loop26:                                 ; preds = %arrayctor.loop26, %arrayctor.cont23
  %arrayctor.cur27 = phi %class.btVector3* [ %array.begin24, %arrayctor.cont23 ], [ %arrayctor.next29, %arrayctor.loop26 ]
  %call28 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur27)
  %arrayctor.next29 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur27, i32 1
  %arrayctor.done30 = icmp eq %class.btVector3* %arrayctor.next29, %arrayctor.end25
  br i1 %arrayctor.done30, label %arrayctor.cont31, label %arrayctor.loop26

arrayctor.cont31:                                 ; preds = %arrayctor.loop26
  %array.begin32 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 0
  %arrayctor.end33 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin32, i32 62
  br label %arrayctor.loop34

arrayctor.loop34:                                 ; preds = %arrayctor.loop34, %arrayctor.cont31
  %arrayctor.cur35 = phi %class.btVector3* [ %array.begin32, %arrayctor.cont31 ], [ %arrayctor.next37, %arrayctor.loop34 ]
  %call36 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur35)
  %arrayctor.next37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur35, i32 1
  %arrayctor.done38 = icmp eq %class.btVector3* %arrayctor.next37, %arrayctor.end33
  br i1 %arrayctor.done38, label %arrayctor.cont39, label %arrayctor.loop34

arrayctor.cont39:                                 ; preds = %arrayctor.loop34
  store i32 42, i32* %numSampleDirections, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont39
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %numSampleDirections, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call40 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %8 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call40, i32 %8
  %9 = bitcast %class.btVector3* %norm to i8*
  %10 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %norm)
  %11 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %11)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call43)
  %12 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %12
  %13 = bitcast %class.btVector3* %arrayidx44 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %13, i8* align 4 %14, i32 16, i1 false)
  %15 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %15)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %norm, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call46)
  %16 = load i32, i32* %i, align 4
  %arrayidx47 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %16
  %17 = bitcast %class.btVector3* %arrayidx47 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %17, i8* align 4 %18, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %21 = bitcast %class.btConvexShape* %20 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %21, align 4
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %22 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call48 = call i32 %22(%class.btConvexShape* %20)
  store i32 %call48, i32* %numPDA, align 4
  %23 = load i32, i32* %numPDA, align 4
  %tobool = icmp ne i32 %23, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 0, i32* %i49, align 4
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc69, %if.then
  %24 = load i32, i32* %i49, align 4
  %25 = load i32, i32* %numPDA, align 4
  %cmp51 = icmp slt i32 %24, %25
  br i1 %cmp51, label %for.body52, label %for.end71

for.body52:                                       ; preds = %for.cond50
  %call54 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm53)
  %26 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %27 = load i32, i32* %i49, align 4
  %28 = bitcast %class.btConvexShape* %26 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable55 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %28, align 4
  %vfn56 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable55, i64 22
  %29 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn56, align 4
  call void %29(%class.btConvexShape* %26, i32 %27, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %30 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call58 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %30)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp57, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call58, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %31 = bitcast %class.btVector3* %norm53 to i8*
  %32 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  %call59 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %33 = load i32, i32* %numSampleDirections, align 4
  %arrayidx60 = getelementptr inbounds %class.btVector3, %class.btVector3* %call59, i32 %33
  %34 = bitcast %class.btVector3* %arrayidx60 to i8*
  %35 = bitcast %class.btVector3* %norm53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp62, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53)
  %36 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %36)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp61, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp62, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call63)
  %37 = load i32, i32* %numSampleDirections, align 4
  %arrayidx64 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %37
  %38 = bitcast %class.btVector3* %arrayidx64 to i8*
  %39 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %38, i8* align 4 %39, i32 16, i1 false)
  %40 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call66 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %40)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* nonnull align 4 dereferenceable(16) %norm53, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call66)
  %41 = load i32, i32* %numSampleDirections, align 4
  %arrayidx67 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %41
  %42 = bitcast %class.btVector3* %arrayidx67 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %42, i8* align 4 %43, i32 16, i1 false)
  %44 = load i32, i32* %numSampleDirections, align 4
  %inc68 = add nsw i32 %44, 1
  store i32 %inc68, i32* %numSampleDirections, align 4
  br label %for.inc69

for.inc69:                                        ; preds = %for.body52
  %45 = load i32, i32* %i49, align 4
  %inc70 = add nsw i32 %45, 1
  store i32 %inc70, i32* %i49, align 4
  br label %for.cond50

for.end71:                                        ; preds = %for.cond50
  br label %if.end

if.end:                                           ; preds = %for.end71, %for.end
  %46 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %47 = bitcast %class.btConvexShape* %46 to i32 (%class.btConvexShape*)***
  %vtable72 = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %47, align 4
  %vfn73 = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable72, i64 21
  %48 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn73, align 4
  %call74 = call i32 %48(%class.btConvexShape* %46)
  store i32 %call74, i32* %numPDB, align 4
  %49 = load i32, i32* %numPDB, align 4
  %tobool75 = icmp ne i32 %49, 0
  br i1 %tobool75, label %if.then76, label %if.end100

if.then76:                                        ; preds = %if.end
  store i32 0, i32* %i77, align 4
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc97, %if.then76
  %50 = load i32, i32* %i77, align 4
  %51 = load i32, i32* %numPDB, align 4
  %cmp79 = icmp slt i32 %50, %51
  br i1 %cmp79, label %for.body80, label %for.end99

for.body80:                                       ; preds = %for.cond78
  %call82 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm81)
  %52 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %53 = load i32, i32* %i77, align 4
  %54 = bitcast %class.btConvexShape* %52 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable83 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %54, align 4
  %vfn84 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable83, i64 22
  %55 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn84, align 4
  call void %55(%class.btConvexShape* %52, i32 %53, %class.btVector3* nonnull align 4 dereferenceable(16) %norm81)
  %56 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call86 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %56)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp85, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call86, %class.btVector3* nonnull align 4 dereferenceable(16) %norm81)
  %57 = bitcast %class.btVector3* %norm81 to i8*
  %58 = bitcast %class.btVector3* %ref.tmp85 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false)
  %call87 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %59 = load i32, i32* %numSampleDirections, align 4
  %arrayidx88 = getelementptr inbounds %class.btVector3, %class.btVector3* %call87, i32 %59
  %60 = bitcast %class.btVector3* %arrayidx88 to i8*
  %61 = bitcast %class.btVector3* %norm81 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %norm81)
  %62 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call91 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %62)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call91)
  %63 = load i32, i32* %numSampleDirections, align 4
  %arrayidx92 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %63
  %64 = bitcast %class.btVector3* %arrayidx92 to i8*
  %65 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %64, i8* align 4 %65, i32 16, i1 false)
  %66 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call94 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %66)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp93, %class.btVector3* nonnull align 4 dereferenceable(16) %norm81, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call94)
  %67 = load i32, i32* %numSampleDirections, align 4
  %arrayidx95 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %67
  %68 = bitcast %class.btVector3* %arrayidx95 to i8*
  %69 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %68, i8* align 4 %69, i32 16, i1 false)
  %70 = load i32, i32* %numSampleDirections, align 4
  %inc96 = add nsw i32 %70, 1
  store i32 %inc96, i32* %numSampleDirections, align 4
  br label %for.inc97

for.inc97:                                        ; preds = %for.body80
  %71 = load i32, i32* %i77, align 4
  %inc98 = add nsw i32 %71, 1
  store i32 %inc98, i32* %i77, align 4
  br label %for.cond78

for.end99:                                        ; preds = %for.cond78
  br label %if.end100

if.end100:                                        ; preds = %for.end99, %if.end
  %72 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %arraydecay = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 0
  %arraydecay101 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 0
  %73 = load i32, i32* %numSampleDirections, align 4
  %74 = bitcast %class.btConvexShape* %72 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable102 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %74, align 4
  %vfn103 = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable102, i64 19
  %75 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn103, align 4
  call void %75(%class.btConvexShape* %72, %class.btVector3* %arraydecay, %class.btVector3* %arraydecay101, i32 %73)
  %76 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %arraydecay104 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 0
  %arraydecay105 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 0
  %77 = load i32, i32* %numSampleDirections, align 4
  %78 = bitcast %class.btConvexShape* %76 to void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable106 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %78, align 4
  %vfn107 = getelementptr inbounds void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable106, i64 19
  %79 = load void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn107, align 4
  call void %79(%class.btConvexShape* %76, %class.btVector3* %arraydecay104, %class.btVector3* %arraydecay105, i32 %77)
  store i32 0, i32* %i, align 4
  br label %for.cond108

for.cond108:                                      ; preds = %for.inc141, %if.end100
  %80 = load i32, i32* %i, align 4
  %81 = load i32, i32* %numSampleDirections, align 4
  %cmp109 = icmp slt i32 %80, %81
  br i1 %cmp109, label %for.body110, label %for.end143

for.body110:                                      ; preds = %for.cond108
  %call112 = call %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv()
  %82 = load i32, i32* %i, align 4
  %arrayidx113 = getelementptr inbounds %class.btVector3, %class.btVector3* %call112, i32 %82
  %83 = bitcast %class.btVector3* %norm111 to i8*
  %84 = bitcast %class.btVector3* %arrayidx113 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %83, i8* align 4 %84, i32 16, i1 false)
  %85 = load i8, i8* %check2d, align 1
  %tobool114 = trunc i8 %85 to i1
  br i1 %tobool114, label %if.then115, label %if.end118

if.then115:                                       ; preds = %for.body110
  %call116 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %norm111)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 2
  store float 0.000000e+00, float* %arrayidx117, align 4
  br label %if.end118

if.end118:                                        ; preds = %if.then115, %for.body110
  %call119 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %norm111)
  %conv = fpext float %call119 to double
  %cmp120 = fcmp ogt double %conv, 1.000000e-02
  br i1 %cmp120, label %if.then121, label %if.end140

if.then121:                                       ; preds = %if.end118
  %86 = load i32, i32* %i, align 4
  %arrayidx122 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInABatch, i32 0, i32 %86
  %87 = bitcast %class.btVector3* %seperatingAxisInA to i8*
  %88 = bitcast %class.btVector3* %arrayidx122 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 16 %88, i32 16, i1 false)
  %89 = load i32, i32* %i, align 4
  %arrayidx123 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %seperatingAxisInBBatch, i32 0, i32 %89
  %90 = bitcast %class.btVector3* %seperatingAxisInB to i8*
  %91 = bitcast %class.btVector3* %arrayidx123 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %90, i8* align 16 %91, i32 16, i1 false)
  %92 = load i32, i32* %i, align 4
  %arrayidx124 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesABatch, i32 0, i32 %92
  %93 = bitcast %class.btVector3* %pInA to i8*
  %94 = bitcast %class.btVector3* %arrayidx124 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 16 %94, i32 16, i1 false)
  %95 = load i32, i32* %i, align 4
  %arrayidx125 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportVerticesBBatch, i32 0, i32 %95
  %96 = bitcast %class.btVector3* %qInB to i8*
  %97 = bitcast %class.btVector3* %arrayidx125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %96, i8* align 16 %97, i32 16, i1 false)
  %98 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp126, %class.btTransform* %98, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA)
  %99 = bitcast %class.btVector3* %pWorld to i8*
  %100 = bitcast %class.btVector3* %ref.tmp126 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %99, i8* align 4 %100, i32 16, i1 false)
  %101 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp127, %class.btTransform* %101, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB)
  %102 = bitcast %class.btVector3* %qWorld to i8*
  %103 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 4 %103, i32 16, i1 false)
  %104 = load i8, i8* %check2d, align 1
  %tobool128 = trunc i8 %104 to i1
  br i1 %tobool128, label %if.then129, label %if.end134

if.then129:                                       ; preds = %if.then121
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pWorld)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 2
  store float 0.000000e+00, float* %arrayidx131, align 4
  %call132 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %qWorld)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 2
  store float 0.000000e+00, float* %arrayidx133, align 4
  br label %if.end134

if.end134:                                        ; preds = %if.then129, %if.then121
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp135, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld)
  %105 = bitcast %class.btVector3* %w to i8*
  %106 = bitcast %class.btVector3* %ref.tmp135 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %106, i32 16, i1 false)
  %call136 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %norm111, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  store float %call136, float* %delta, align 4
  %107 = load float, float* %delta, align 4
  %108 = load float, float* %minProj, align 4
  %cmp137 = fcmp olt float %107, %108
  br i1 %cmp137, label %if.then138, label %if.end139

if.then138:                                       ; preds = %if.end134
  %109 = load float, float* %delta, align 4
  store float %109, float* %minProj, align 4
  %110 = bitcast %class.btVector3* %minNorm to i8*
  %111 = bitcast %class.btVector3* %norm111 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %110, i8* align 4 %111, i32 16, i1 false)
  %112 = bitcast %class.btVector3* %minA to i8*
  %113 = bitcast %class.btVector3* %pWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %113, i32 16, i1 false)
  %114 = bitcast %class.btVector3* %minB to i8*
  %115 = bitcast %class.btVector3* %qWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false)
  br label %if.end139

if.end139:                                        ; preds = %if.then138, %if.end134
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %if.end118
  br label %for.inc141

for.inc141:                                       ; preds = %if.end140
  %116 = load i32, i32* %i, align 4
  %inc142 = add nsw i32 %116, 1
  store i32 %inc142, i32* %i, align 4
  br label %for.cond108

for.end143:                                       ; preds = %for.cond108
  %117 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %call146 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %117)
  store float %call146, float* %ref.tmp145, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp144, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %minA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp144)
  %118 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %call150 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %118)
  store float %call150, float* %ref.tmp149, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp148, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  %call151 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %minB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp148)
  %119 = load float, float* %minProj, align 4
  %cmp152 = fcmp olt float %119, 0.000000e+00
  br i1 %cmp152, label %if.then153, label %if.end154

if.then153:                                       ; preds = %for.end143
  store i1 false, i1* %retval, align 1
  br label %return

if.end154:                                        ; preds = %for.end143
  store float 5.000000e-01, float* %extraSeparation, align 4
  %120 = load float, float* %extraSeparation, align 4
  %121 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %call155 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %121)
  %122 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %call156 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %122)
  %add = fadd float %call155, %call156
  %add157 = fadd float %120, %add
  %123 = load float, float* %minProj, align 4
  %add158 = fadd float %123, %add157
  store float %add158, float* %minProj, align 4
  %124 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  %125 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  %126 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  %call159 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %gjkdet, %class.btConvexShape* %124, %class.btConvexShape* %125, %class.btVoronoiSimplexSolver* %126, %class.btConvexPenetrationDepthSolver* null)
  %127 = load float, float* %minProj, align 4
  store float %127, float* %offsetDist, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %offset, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %offsetDist)
  %call160 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %128 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call161 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %128)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %newOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %call161, %class.btVector3* nonnull align 4 dereferenceable(16) %offset)
  %129 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call162 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %displacedTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %129)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %displacedTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %newOrg)
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call163 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %displacedTrans)
  %130 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call164 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %130)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  %call165 = call %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultC2Ev(%struct.btIntermediateResult* %res)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp166, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm)
  call void @_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3(%class.btGjkPairDetector* %gjkdet, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp166)
  %131 = bitcast %struct.btIntermediateResult* %res to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %132 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkdet, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %131, %class.btIDebugDraw* %132, i1 zeroext false)
  %133 = load float, float* %minProj, align 4
  %m_depth = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 3
  %134 = load float, float* %m_depth, align 4
  %sub = fsub float %133, %134
  store float %sub, float* %correctedMinNorm, align 4
  store float 1.000000e+00, float* %penetration_relaxation, align 4
  %call167 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %minNorm, float* nonnull align 4 dereferenceable(4) %penetration_relaxation)
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 4
  %135 = load i8, i8* %m_hasResult, align 4
  %tobool168 = trunc i8 %135 to i1
  br i1 %tobool168, label %if.then169, label %if.end173

if.then169:                                       ; preds = %if.end154
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp171, %class.btVector3* nonnull align 4 dereferenceable(16) %minNorm, float* nonnull align 4 dereferenceable(4) %correctedMinNorm)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp170, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pointInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp171)
  %136 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4
  %137 = bitcast %class.btVector3* %136 to i8*
  %138 = bitcast %class.btVector3* %ref.tmp170 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %137, i8* align 4 %138, i32 16, i1 false)
  %m_pointInWorld172 = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 2
  %139 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4
  %140 = bitcast %class.btVector3* %139 to i8*
  %141 = bitcast %class.btVector3* %m_pointInWorld172 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %140, i8* align 4 %141, i32 16, i1 false)
  %142 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %143 = bitcast %class.btVector3* %142 to i8*
  %144 = bitcast %class.btVector3* %minNorm to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %143, i8* align 4 %144, i32 16, i1 false)
  br label %if.end173

if.end173:                                        ; preds = %if.then169, %if.end154
  %m_hasResult174 = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %res, i32 0, i32 4
  %145 = load i8, i8* %m_hasResult174, align 4
  %tobool175 = trunc i8 %145 to i1
  store i1 %tobool175, i1* %retval, align 1
  %call176 = call %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD2Ev(%struct.btIntermediateResult* %res) #5
  %call177 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* %gjkdet) #5
  br label %return

return:                                           ; preds = %if.end173, %if.then153
  %146 = load i1, i1* %retval, align 1
  ret i1 %146
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btVector3* @_ZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEv() #2 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %ref.tmp85 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp92 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp99 = alloca float, align 4
  %ref.tmp100 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp109 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %ref.tmp117 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca float, align 4
  %ref.tmp132 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp136 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp152 = alloca float, align 4
  %ref.tmp153 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp160 = alloca float, align 4
  %ref.tmp161 = alloca float, align 4
  %ref.tmp163 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %ref.tmp165 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections) #5
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float -0.000000e+00, float* %ref.tmp1, align 4
  store float -1.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  store float 0x3FE727CC00000000, float* %ref.tmp3, align 4
  store float 0xBFE0D2BD40000000, float* %ref.tmp4, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0xBFD1B05740000000, float* %ref.tmp7, align 4
  store float 0xBFEB388440000000, float* %ref.tmp8, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  store float 0xBFEC9F2340000000, float* %ref.tmp11, align 4
  store float -0.000000e+00, float* %ref.tmp12, align 4
  store float 0xBFDC9F2FE0000000, float* %ref.tmp13, align 4
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  store float 0xBFD1B05740000000, float* %ref.tmp15, align 4
  store float 0x3FEB388440000000, float* %ref.tmp16, align 4
  store float 0xBFDC9F40A0000000, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  store float 0x3FE727CC00000000, float* %ref.tmp19, align 4
  store float 0x3FE0D2BD40000000, float* %ref.tmp20, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp21, align 4
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  store float 0x3FD1B05740000000, float* %ref.tmp23, align 4
  store float 0xBFEB388440000000, float* %ref.tmp24, align 4
  store float 0x3FDC9F40A0000000, float* %ref.tmp25, align 4
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 6), float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  store float 0xBFE727CC00000000, float* %ref.tmp27, align 4
  store float 0xBFE0D2BD40000000, float* %ref.tmp28, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp29, align 4
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 7), float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  store float 0xBFE727CC00000000, float* %ref.tmp31, align 4
  store float 0x3FE0D2BD40000000, float* %ref.tmp32, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp33, align 4
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 8), float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  store float 0x3FD1B05740000000, float* %ref.tmp35, align 4
  store float 0x3FEB388440000000, float* %ref.tmp36, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp37, align 4
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 9), float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  store float 0x3FEC9F2340000000, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 0x3FDC9F2FE0000000, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 10), float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  store float -0.000000e+00, float* %ref.tmp43, align 4
  store float 0.000000e+00, float* %ref.tmp44, align 4
  store float 1.000000e+00, float* %ref.tmp45, align 4
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 11), float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  store float 0x3FDB387E00000000, float* %ref.tmp47, align 4
  store float 0xBFD3C6D620000000, float* %ref.tmp48, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp49, align 4
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 12), float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  store float 0xBFC4CB5BC0000000, float* %ref.tmp51, align 4
  store float 0xBFDFFFEB00000000, float* %ref.tmp52, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp53, align 4
  %call54 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 13), float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  store float 0x3FD0D2D880000000, float* %ref.tmp55, align 4
  store float 0xBFE9E36D20000000, float* %ref.tmp56, align 4
  store float 0xBFE0D2D880000000, float* %ref.tmp57, align 4
  %call58 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 14), float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  store float 0x3FDB387E00000000, float* %ref.tmp59, align 4
  store float 0x3FD3C6D620000000, float* %ref.tmp60, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp61, align 4
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 15), float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  store float 0x3FEB388220000000, float* %ref.tmp63, align 4
  store float -0.000000e+00, float* %ref.tmp64, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp65, align 4
  %call66 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 16), float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  store float 0xBFE0D2C7C0000000, float* %ref.tmp67, align 4
  store float -0.000000e+00, float* %ref.tmp68, align 4
  store float 0xBFEB388A80000000, float* %ref.tmp69, align 4
  %call70 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 17), float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  store float 0xBFE605A700000000, float* %ref.tmp71, align 4
  store float 0xBFDFFFF360000000, float* %ref.tmp72, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp73, align 4
  %call74 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 18), float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  store float 0xBFC4CB5BC0000000, float* %ref.tmp75, align 4
  store float 0x3FDFFFEB00000000, float* %ref.tmp76, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp77, align 4
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 19), float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  store float 0xBFE605A700000000, float* %ref.tmp79, align 4
  store float 0x3FDFFFF360000000, float* %ref.tmp80, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp81, align 4
  %call82 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 20), float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp81)
  store float 0x3FD0D2D880000000, float* %ref.tmp83, align 4
  store float 0x3FE9E36D20000000, float* %ref.tmp84, align 4
  store float 0xBFE0D2D880000000, float* %ref.tmp85, align 4
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 21), float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84, float* nonnull align 4 dereferenceable(4) %ref.tmp85)
  store float 0x3FEE6F1120000000, float* %ref.tmp87, align 4
  store float 0x3FD3C6DE80000000, float* %ref.tmp88, align 4
  store float 0.000000e+00, float* %ref.tmp89, align 4
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 22), float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  store float 0x3FEE6F1120000000, float* %ref.tmp91, align 4
  store float 0xBFD3C6DE80000000, float* %ref.tmp92, align 4
  store float 0.000000e+00, float* %ref.tmp93, align 4
  %call94 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 23), float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp92, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  store float 0x3FE2CF24A0000000, float* %ref.tmp95, align 4
  store float 0xBFE9E377A0000000, float* %ref.tmp96, align 4
  store float 0.000000e+00, float* %ref.tmp97, align 4
  %call98 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 24), float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  store float 0.000000e+00, float* %ref.tmp99, align 4
  store float -1.000000e+00, float* %ref.tmp100, align 4
  store float 0.000000e+00, float* %ref.tmp101, align 4
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 25), float* nonnull align 4 dereferenceable(4) %ref.tmp99, float* nonnull align 4 dereferenceable(4) %ref.tmp100, float* nonnull align 4 dereferenceable(4) %ref.tmp101)
  store float 0xBFE2CF24A0000000, float* %ref.tmp103, align 4
  store float 0xBFE9E377A0000000, float* %ref.tmp104, align 4
  store float 0.000000e+00, float* %ref.tmp105, align 4
  %call106 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 26), float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  store float 0xBFEE6F1120000000, float* %ref.tmp107, align 4
  store float 0xBFD3C6DE80000000, float* %ref.tmp108, align 4
  store float -0.000000e+00, float* %ref.tmp109, align 4
  %call110 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 27), float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp109)
  store float 0xBFEE6F1120000000, float* %ref.tmp111, align 4
  store float 0x3FD3C6DE80000000, float* %ref.tmp112, align 4
  store float -0.000000e+00, float* %ref.tmp113, align 4
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 28), float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  store float 0xBFE2CF24A0000000, float* %ref.tmp115, align 4
  store float 0x3FE9E377A0000000, float* %ref.tmp116, align 4
  store float -0.000000e+00, float* %ref.tmp117, align 4
  %call118 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 29), float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116, float* nonnull align 4 dereferenceable(4) %ref.tmp117)
  store float -0.000000e+00, float* %ref.tmp119, align 4
  store float 1.000000e+00, float* %ref.tmp120, align 4
  store float -0.000000e+00, float* %ref.tmp121, align 4
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 30), float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  store float 0x3FE2CF24A0000000, float* %ref.tmp123, align 4
  store float 0x3FE9E377A0000000, float* %ref.tmp124, align 4
  store float -0.000000e+00, float* %ref.tmp125, align 4
  %call126 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 31), float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125)
  store float 0x3FE605A700000000, float* %ref.tmp127, align 4
  store float 0xBFDFFFF360000000, float* %ref.tmp128, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp129, align 4
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 32), float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  store float 0xBFD0D2D880000000, float* %ref.tmp131, align 4
  store float 0xBFE9E36D20000000, float* %ref.tmp132, align 4
  store float 0x3FE0D2D880000000, float* %ref.tmp133, align 4
  %call134 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 33), float* nonnull align 4 dereferenceable(4) %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133)
  store float 0xBFEB388220000000, float* %ref.tmp135, align 4
  store float 0.000000e+00, float* %ref.tmp136, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp137, align 4
  %call138 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 34), float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp136, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  store float 0xBFD0D2D880000000, float* %ref.tmp139, align 4
  store float 0x3FE9E36D20000000, float* %ref.tmp140, align 4
  store float 0x3FE0D2D880000000, float* %ref.tmp141, align 4
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 35), float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  store float 0x3FE605A700000000, float* %ref.tmp143, align 4
  store float 0x3FDFFFF360000000, float* %ref.tmp144, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp145, align 4
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 36), float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  store float 0x3FE0D2C7C0000000, float* %ref.tmp147, align 4
  store float 0.000000e+00, float* %ref.tmp148, align 4
  store float 0x3FEB388A80000000, float* %ref.tmp149, align 4
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 37), float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  store float 0x3FC4CB5BC0000000, float* %ref.tmp151, align 4
  store float 0xBFDFFFEB00000000, float* %ref.tmp152, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp153, align 4
  %call154 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 38), float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp152, float* nonnull align 4 dereferenceable(4) %ref.tmp153)
  store float 0xBFDB387E00000000, float* %ref.tmp155, align 4
  store float 0xBFD3C6D620000000, float* %ref.tmp156, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp157, align 4
  %call158 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 39), float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157)
  store float 0xBFDB387E00000000, float* %ref.tmp159, align 4
  store float 0x3FD3C6D620000000, float* %ref.tmp160, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp161, align 4
  %call162 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 40), float* nonnull align 4 dereferenceable(4) %ref.tmp159, float* nonnull align 4 dereferenceable(4) %ref.tmp160, float* nonnull align 4 dereferenceable(4) %ref.tmp161)
  store float 0x3FC4CB5BC0000000, float* %ref.tmp163, align 4
  store float 0x3FDFFFEB00000000, float* %ref.tmp164, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp165, align 4
  %call166 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 41), float* nonnull align 4 dereferenceable(4) %ref.tmp163, float* nonnull align 4 dereferenceable(4) %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165)
  br label %arrayinit.body

arrayinit.body:                                   ; preds = %arrayinit.body, %init
  %arrayinit.cur = phi %class.btVector3* [ getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 42), %init ], [ %arrayinit.next, %arrayinit.body ]
  %call167 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayinit.cur)
  %arrayinit.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.cur, i32 1
  %arrayinit.done = icmp eq %class.btVector3* %arrayinit.next, getelementptr inbounds (%class.btVector3, %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0), i32 62)
  br i1 %arrayinit.done, label %arrayinit.end, label %arrayinit.body

arrayinit.end:                                    ; preds = %arrayinit.body
  call void @__cxa_guard_release(i32* @_ZGVZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections) #5
  br label %init.end

init.end:                                         ; preds = %arrayinit.end, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN33btMinkowskiPenetrationDepthSolver24getPenetrationDirectionsEvE22sPenetrationDirections, i32 0, i32 0)
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

declare float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define internal %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultC2Ev(%struct.btIntermediateResult* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %0 = bitcast %struct.btIntermediateResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #5
  %1 = bitcast %struct.btIntermediateResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawE20btIntermediateResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalOnBInWorld)
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pointInWorld)
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 4
  store i8 0, i8* %m_hasResult, align 4
  ret %struct.btIntermediateResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btGjkPairDetector23setCachedSeperatingAxisERK9btVector3(%class.btGjkPairDetector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxis) #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %seperatingAxis.addr = alloca %class.btVector3*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %class.btVector3* %seperatingAxis, %class.btVector3** %seperatingAxis.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %seperatingAxis.addr, align 4
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_cachedSeparatingAxis to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD2Ev(%struct.btIntermediateResult* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %0 = bitcast %struct.btIntermediateResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #5
  ret %struct.btIntermediateResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #5
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #5

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverD2Ev(%class.btMinkowskiPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* %0) #5
  ret %class.btMinkowskiPenetrationDepthSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN33btMinkowskiPenetrationDepthSolverD0Ev(%class.btMinkowskiPenetrationDepthSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %call = call %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverD2Ev(%class.btMinkowskiPenetrationDepthSolver* %this1) #5
  %0 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp eq i32 %0, 17
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4
  %cmp1 = icmp eq i32 %1, 18
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp1, %lor.rhs ]
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD0Ev(%struct.btIntermediateResult* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %call = call %struct.btIntermediateResult* @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResultD2Ev(%struct.btIntermediateResult* %this1) #5
  %0 = bitcast %struct.btIntermediateResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersAEii(%struct.btIntermediateResult* %this, i32 %partId0, i32 %index0) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult20setShapeIdentifiersBEii(%struct.btIntermediateResult* %this, i32 %partId1, i32 %index1) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN33btMinkowskiPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDrawEN20btIntermediateResult15addContactPointERKS8_SE_f(%struct.btIntermediateResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btIntermediateResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btIntermediateResult* %this, %struct.btIntermediateResult** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4
  store float %depth, float* %depth.addr, align 4
  %this1 = load %struct.btIntermediateResult*, %struct.btIntermediateResult** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  %m_pointInWorld = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 2
  %4 = bitcast %class.btVector3* %m_pointInWorld to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load float, float* %depth.addr, align 4
  %m_depth = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 3
  store float %6, float* %m_depth, align 4
  %m_hasResult = getelementptr inbounds %struct.btIntermediateResult, %struct.btIntermediateResult* %this1, i32 0, i32 4
  store i8 1, i8* %m_hasResult, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMinkowskiPenetrationDepthSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { builtin nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
