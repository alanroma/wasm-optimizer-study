; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexArray.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexArray.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN23btStridingMeshInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN13btIndexedMeshC2Ev = comdat any

$_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshED2Ev = comdat any

$_ZN26btTriangleIndexVertexArraydlEPv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshEixEi = comdat any

$_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv = comdat any

$_ZN26btTriangleIndexVertexArray19preallocateVerticesEi = comdat any

$_ZN26btTriangleIndexVertexArray18preallocateIndicesEi = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi = comdat any

$_ZN13btIndexedMeshnwEmPv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_ = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE5clearEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV26btTriangleIndexVertexArray = hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI26btTriangleIndexVertexArray to i8*), i8* bitcast (%class.btTriangleIndexVertexArray* (%class.btTriangleIndexVertexArray*)* @_ZN26btTriangleIndexVertexArrayD1Ev to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*)* @_ZN26btTriangleIndexVertexArrayD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi to i8*), i8* bitcast (i32 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi to i8*), i8* bitcast (i1 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS26btTriangleIndexVertexArray = hidden constant [29 x i8] c"26btTriangleIndexVertexArray\00", align 1
@_ZTI23btStridingMeshInterface = external constant i8*
@_ZTI26btTriangleIndexVertexArray = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTS26btTriangleIndexVertexArray, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btStridingMeshInterface to i8*) }, align 4
@_ZTV23btStridingMeshInterface = external unnamed_addr constant { [17 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btTriangleIndexVertexArray.cpp, i8* null }]

@_ZN26btTriangleIndexVertexArrayC1EiPiiiPfi = hidden unnamed_addr alias %class.btTriangleIndexVertexArray* (%class.btTriangleIndexVertexArray*, i32, i32*, i32, i32, float*, i32), %class.btTriangleIndexVertexArray* (%class.btTriangleIndexVertexArray*, i32, i32*, i32, i32, float*, i32)* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
@_ZN26btTriangleIndexVertexArrayD1Ev = hidden unnamed_addr alias %class.btTriangleIndexVertexArray* (%class.btTriangleIndexVertexArray*), %class.btTriangleIndexVertexArray* (%class.btTriangleIndexVertexArray*)* @_ZN26btTriangleIndexVertexArrayD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi(%class.btTriangleIndexVertexArray* returned %this, i32 %numTriangles, i32* %triangleIndexBase, i32 %triangleIndexStride, i32 %numVertices, float* %vertexBase, i32 %vertexStride) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numTriangles.addr = alloca i32, align 4
  %triangleIndexBase.addr = alloca i32*, align 4
  %triangleIndexStride.addr = alloca i32, align 4
  %numVertices.addr = alloca i32, align 4
  %vertexBase.addr = alloca float*, align 4
  %vertexStride.addr = alloca i32, align 4
  %mesh = alloca %struct.btIndexedMesh, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %numTriangles, i32* %numTriangles.addr, align 4
  store i32* %triangleIndexBase, i32** %triangleIndexBase.addr, align 4
  store i32 %triangleIndexStride, i32* %triangleIndexStride.addr, align 4
  store i32 %numVertices, i32* %numVertices.addr, align 4
  store float* %vertexBase, float** %vertexBase.addr, align 4
  store i32 %vertexStride, i32* %vertexStride.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexArray* %this1 to %class.btStridingMeshInterface*
  %call = call %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceC2Ev(%class.btStridingMeshInterface* %0)
  %1 = bitcast %class.btTriangleIndexVertexArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV26btTriangleIndexVertexArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev(%class.btAlignedObjectArray* %m_indexedMeshes)
  %m_hasAabb = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_hasAabb, align 4
  %m_aabbMin = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  %call5 = call %struct.btIndexedMesh* @_ZN13btIndexedMeshC2Ev(%struct.btIndexedMesh* %mesh)
  %2 = load i32, i32* %numTriangles.addr, align 4
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 0
  store i32 %2, i32* %m_numTriangles, align 4
  %3 = load i32*, i32** %triangleIndexBase.addr, align 4
  %4 = bitcast i32* %3 to i8*
  %m_triangleIndexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 1
  store i8* %4, i8** %m_triangleIndexBase, align 4
  %5 = load i32, i32* %triangleIndexStride.addr, align 4
  %m_triangleIndexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 2
  store i32 %5, i32* %m_triangleIndexStride, align 4
  %6 = load i32, i32* %numVertices.addr, align 4
  %m_numVertices = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 3
  store i32 %6, i32* %m_numVertices, align 4
  %7 = load float*, float** %vertexBase.addr, align 4
  %8 = bitcast float* %7 to i8*
  %m_vertexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 4
  store i8* %8, i8** %m_vertexBase, align 4
  %9 = load i32, i32* %vertexStride.addr, align 4
  %m_vertexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %mesh, i32 0, i32 5
  store i32 %9, i32* %m_vertexStride, align 4
  call void @_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType(%class.btTriangleIndexVertexArray* %this1, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %mesh, i32 2)
  ret %class.btTriangleIndexVertexArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceC2Ev(%class.btStridingMeshInterface* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = bitcast %class.btStridingMeshInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV23btStridingMeshInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_scaling, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret %class.btStridingMeshInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btIndexedMesh* @_ZN13btIndexedMeshC2Ev(%struct.btIndexedMesh* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btIndexedMesh*, align 4
  store %struct.btIndexedMesh* %this, %struct.btIndexedMesh** %this.addr, align 4
  %this1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %this.addr, align 4
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %this1, i32 0, i32 6
  store i32 2, i32* %m_indexType, align 4
  %m_vertexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %this1, i32 0, i32 7
  store i32 0, i32* %m_vertexType, align 4
  ret %struct.btIndexedMesh* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray14addIndexedMeshERK13btIndexedMesh14PHY_ScalarType(%class.btTriangleIndexVertexArray* %this, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %mesh, i32 %indexType) #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %mesh.addr = alloca %struct.btIndexedMesh*, align 4
  %indexType.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store %struct.btIndexedMesh* %mesh, %struct.btIndexedMesh** %mesh.addr, align 4
  store i32 %indexType, i32* %indexType.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_(%class.btAlignedObjectArray* %m_indexedMeshes, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %0)
  %1 = load i32, i32* %indexType.addr, align 4
  %m_indexedMeshes2 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %m_indexedMeshes3 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %m_indexedMeshes3)
  %sub = sub nsw i32 %call, 1
  %call4 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes2, i32 %sub)
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call4, i32 0, i32 6
  store i32 %1, i32* %m_indexType, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV26btTriangleIndexVertexArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshED2Ev(%class.btAlignedObjectArray* %m_indexedMeshes) #6
  %1 = bitcast %class.btTriangleIndexVertexArray* %this1 to %class.btStridingMeshInterface*
  %call2 = call %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceD2Ev(%class.btStridingMeshInterface* %1) #6
  ret %class.btTriangleIndexVertexArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceD2Ev(%class.btStridingMeshInterface* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN26btTriangleIndexVertexArrayD0Ev(%class.btTriangleIndexVertexArray* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %call = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD1Ev(%class.btTriangleIndexVertexArray* %this1) #6
  %0 = bitcast %class.btTriangleIndexVertexArray* %this1 to i8*
  call void @_ZN26btTriangleIndexVertexArraydlEPv(i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArraydlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexArray* %this, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %vertexStride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %subpart) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %vertexbase.addr = alloca i8**, align 4
  %numverts.addr = alloca i32*, align 4
  %type.addr = alloca i32*, align 4
  %vertexStride.addr = alloca i32*, align 4
  %indexbase.addr = alloca i8**, align 4
  %indexstride.addr = alloca i32*, align 4
  %numfaces.addr = alloca i32*, align 4
  %indicestype.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mesh = alloca %struct.btIndexedMesh*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i8** %vertexbase, i8*** %vertexbase.addr, align 4
  store i32* %numverts, i32** %numverts.addr, align 4
  store i32* %type, i32** %type.addr, align 4
  store i32* %vertexStride, i32** %vertexStride.addr, align 4
  store i8** %indexbase, i8*** %indexbase.addr, align 4
  store i32* %indexstride, i32** %indexstride.addr, align 4
  store i32* %numfaces, i32** %numfaces.addr, align 4
  store i32* %indicestype, i32** %indicestype.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %0 = load i32, i32* %subpart.addr, align 4
  %call = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 %0)
  store %struct.btIndexedMesh* %call, %struct.btIndexedMesh** %mesh, align 4
  %1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_numVertices = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %1, i32 0, i32 3
  %2 = load i32, i32* %m_numVertices, align 4
  %3 = load i32*, i32** %numverts.addr, align 4
  store i32 %2, i32* %3, align 4
  %4 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %4, i32 0, i32 4
  %5 = load i8*, i8** %m_vertexBase, align 4
  %6 = load i8**, i8*** %vertexbase.addr, align 4
  store i8* %5, i8** %6, align 4
  %7 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %7, i32 0, i32 7
  %8 = load i32, i32* %m_vertexType, align 4
  %9 = load i32*, i32** %type.addr, align 4
  store i32 %8, i32* %9, align 4
  %10 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %10, i32 0, i32 5
  %11 = load i32, i32* %m_vertexStride, align 4
  %12 = load i32*, i32** %vertexStride.addr, align 4
  store i32 %11, i32* %12, align 4
  %13 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %13, i32 0, i32 0
  %14 = load i32, i32* %m_numTriangles, align 4
  %15 = load i32*, i32** %numfaces.addr, align 4
  store i32 %14, i32* %15, align 4
  %16 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_triangleIndexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %16, i32 0, i32 1
  %17 = load i8*, i8** %m_triangleIndexBase, align 4
  %18 = load i8**, i8*** %indexbase.addr, align 4
  store i8* %17, i8** %18, align 4
  %19 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_triangleIndexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %19, i32 0, i32 2
  %20 = load i32, i32* %m_triangleIndexStride, align 4
  %21 = load i32*, i32** %indexstride.addr, align 4
  store i32 %20, i32* %21, align 4
  %22 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %22, i32 0, i32 6
  %23 = load i32, i32* %m_indexType, align 4
  %24 = load i32*, i32** %indicestype.addr, align 4
  store i32 %23, i32* %24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %0, i32 %1
  ret %struct.btIndexedMesh* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexArray* %this, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %vertexStride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %subpart) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %vertexbase.addr = alloca i8**, align 4
  %numverts.addr = alloca i32*, align 4
  %type.addr = alloca i32*, align 4
  %vertexStride.addr = alloca i32*, align 4
  %indexbase.addr = alloca i8**, align 4
  %indexstride.addr = alloca i32*, align 4
  %numfaces.addr = alloca i32*, align 4
  %indicestype.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mesh = alloca %struct.btIndexedMesh*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i8** %vertexbase, i8*** %vertexbase.addr, align 4
  store i32* %numverts, i32** %numverts.addr, align 4
  store i32* %type, i32** %type.addr, align 4
  store i32* %vertexStride, i32** %vertexStride.addr, align 4
  store i8** %indexbase, i8*** %indexbase.addr, align 4
  store i32* %indexstride, i32** %indexstride.addr, align 4
  store i32* %numfaces, i32** %numfaces.addr, align 4
  store i32* %indicestype, i32** %indicestype.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %0 = load i32, i32* %subpart.addr, align 4
  %call = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZNK20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 %0)
  store %struct.btIndexedMesh* %call, %struct.btIndexedMesh** %mesh, align 4
  %1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_numVertices = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %1, i32 0, i32 3
  %2 = load i32, i32* %m_numVertices, align 4
  %3 = load i32*, i32** %numverts.addr, align 4
  store i32 %2, i32* %3, align 4
  %4 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %4, i32 0, i32 4
  %5 = load i8*, i8** %m_vertexBase, align 4
  %6 = load i8**, i8*** %vertexbase.addr, align 4
  store i8* %5, i8** %6, align 4
  %7 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %7, i32 0, i32 7
  %8 = load i32, i32* %m_vertexType, align 4
  %9 = load i32*, i32** %type.addr, align 4
  store i32 %8, i32* %9, align 4
  %10 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_vertexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %10, i32 0, i32 5
  %11 = load i32, i32* %m_vertexStride, align 4
  %12 = load i32*, i32** %vertexStride.addr, align 4
  store i32 %11, i32* %12, align 4
  %13 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %13, i32 0, i32 0
  %14 = load i32, i32* %m_numTriangles, align 4
  %15 = load i32*, i32** %numfaces.addr, align 4
  store i32 %14, i32* %15, align 4
  %16 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_triangleIndexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %16, i32 0, i32 1
  %17 = load i8*, i8** %m_triangleIndexBase, align 4
  %18 = load i8**, i8*** %indexbase.addr, align 4
  store i8* %17, i8** %18, align 4
  %19 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_triangleIndexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %19, i32 0, i32 2
  %20 = load i32, i32* %m_triangleIndexStride, align 4
  %21 = load i32*, i32** %indexstride.addr, align 4
  store i32 %20, i32* %21, align 4
  %22 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %mesh, align 4
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %22, i32 0, i32 6
  %23 = load i32, i32* %m_indexType, align 4
  %24 = load i32*, i32** %indicestype.addr, align 4
  store i32 %23, i32* %24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZNK20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %0, i32 %1
  ret %struct.btIndexedMesh* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv(%class.btTriangleIndexVertexArray* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_hasAabb = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_hasAabb, align 4
  %cmp = icmp eq i32 %0, 1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_(%class.btTriangleIndexVertexArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %m_aabbMin = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 4
  %1 = bitcast %class.btVector3* %m_aabbMin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %m_aabbMax = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 5
  %4 = bitcast %class.btVector3* %m_aabbMax to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_hasAabb = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 3
  store i32 1, i32* %m_hasAabb, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_(%class.btTriangleIndexVertexArray* %this, %class.btVector3* %aabbMin, %class.btVector3* %aabbMax) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_aabbMin = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_aabbMax = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 5
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

declare void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv(%class.btTriangleIndexVertexArray* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %m_indexedMeshes)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi(%class.btTriangleIndexVertexArray* %this, i32 %numverts) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numverts.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %numverts, i32* %numverts.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi(%class.btTriangleIndexVertexArray* %this, i32 %numindices) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numindices.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %numindices, i32* %numindices.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

declare i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface*, i8*, %class.btSerializer*) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btIndexedMesh*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btIndexedMesh* %_Val, %struct.btIndexedMesh** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %1, i32 %2
  %3 = bitcast %struct.btIndexedMesh* %arrayidx to i8*
  %call5 = call i8* @_ZN13btIndexedMeshnwEmPv(i32 32, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btIndexedMesh*
  %5 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %_Val.addr, align 4
  %6 = bitcast %struct.btIndexedMesh* %4 to i8*
  %7 = bitcast %struct.btIndexedMesh* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 32, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btIndexedMesh*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btIndexedMesh*
  store %struct.btIndexedMesh* %2, %struct.btIndexedMesh** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btIndexedMesh* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* %4, %struct.btIndexedMesh** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN13btIndexedMeshnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btIndexedMesh* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btIndexedMesh** null)
  %2 = bitcast %struct.btIndexedMesh* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btIndexedMesh* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btIndexedMesh*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btIndexedMesh* %dest, %struct.btIndexedMesh** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %3, i32 %4
  %5 = bitcast %struct.btIndexedMesh* %arrayidx to i8*
  %call = call i8* @_ZN13btIndexedMeshnwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call to %struct.btIndexedMesh*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %7, i32 %8
  %9 = bitcast %struct.btIndexedMesh* %6 to i8*
  %10 = bitcast %struct.btIndexedMesh* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %tobool = icmp ne %struct.btIndexedMesh* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btIndexedMesh* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* null, %struct.btIndexedMesh** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btIndexedMesh* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btIndexedMesh** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btIndexedMesh**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btIndexedMesh** %hint, %struct.btIndexedMesh*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btIndexedMesh*
  ret %struct.btIndexedMesh* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btIndexedMesh* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btIndexedMesh*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btIndexedMesh* %ptr, %struct.btIndexedMesh** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %ptr.addr, align 4
  %1 = bitcast %struct.btIndexedMesh* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* null, %struct.btIndexedMesh** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btTriangleIndexVertexArray.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
