; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btAxisSweep3.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btAxisSweep3.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btAxisSweep3 = type { %class.btAxisSweep3Internal }
%class.btAxisSweep3Internal = type { %class.btBroadphaseInterface, i16, i16, %class.btVector3, %class.btVector3, %class.btVector3, i16, i16, %"class.btAxisSweep3Internal<unsigned short>::Handle"*, i16, [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x i8*], %class.btOverlappingPairCache*, %class.btOverlappingPairCallback*, i8, i32, %struct.btDbvtBroadphase*, %class.btOverlappingPairCache* }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%"class.btAxisSweep3Internal<unsigned short>::Handle" = type { %struct.btBroadphaseProxy, [3 x i16], [3 x i16], %struct.btBroadphaseProxy* }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%"class.btAxisSweep3Internal<unsigned short>::Edge" = type { i16, i16 }
%class.btOverlappingPairCallback = type { i32 (...)** }
%struct.btDbvtBroadphase = type { %class.btBroadphaseInterface, [2 x %struct.btDbvt], [3 x %struct.btDbvtProxy*], %class.btOverlappingPairCache*, float, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i8, i8, i8, i8, %class.btAlignedObjectArray.1 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%struct.btDbvtProxy = type { %struct.btBroadphaseProxy, %struct.btDbvtNode*, [2 x %struct.btDbvtProxy*], i32 }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btAlignedObjectArray.4*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.bt32BitAxisSweep3 = type { %class.btAxisSweep3Internal.9 }
%class.btAxisSweep3Internal.9 = type { %class.btBroadphaseInterface, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, %"class.btAxisSweep3Internal<unsigned int>::Handle"*, i32, [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x i8*], %class.btOverlappingPairCache*, %class.btOverlappingPairCallback*, i8, i32, %struct.btDbvtBroadphase*, %class.btOverlappingPairCache* }
%"class.btAxisSweep3Internal<unsigned int>::Handle" = type { %struct.btBroadphaseProxy, [3 x i32], [3 x i32], %struct.btBroadphaseProxy* }
%"class.btAxisSweep3Internal<unsigned int>::Edge" = type { i32, i32 }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.10, %struct.btOverlapFilterCallback*, %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.15, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.13 }
%class.btCollisionAlgorithm = type opaque
%union.anon.13 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%class.btNullPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.10 }
%class.btDispatcher = type opaque
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%class.btBroadphasePairSortPredicate = type { i8 }
%struct.btOverlapCallback = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb = comdat any

$_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb = comdat any

$_ZN12btAxisSweep3D2Ev = comdat any

$_ZN12btAxisSweep3D0Ev = comdat any

$_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPviiP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ = comdat any

$_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ = comdat any

$_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback = comdat any

$_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ = comdat any

$_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE10printStatsEv = comdat any

$_ZN17bt32BitAxisSweep3D2Ev = comdat any

$_ZN17bt32BitAxisSweep3D0Ev = comdat any

$_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPviiP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ = comdat any

$_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ = comdat any

$_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback = comdat any

$_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ = comdat any

$_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE10printStatsEv = comdat any

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN28btHashedOverlappingPairCachenwEmPv = comdat any

$_ZN15btNullPairCacheC2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAxisSweep3InternalItE6HandlenaEm = comdat any

$_ZN20btAxisSweep3InternalItE6HandleC2Ev = comdat any

$_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt = comdat any

$_ZN20btAxisSweep3InternalItED2Ev = comdat any

$_ZN20btAxisSweep3InternalItED0Ev = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN22btOverlappingPairCacheC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev = comdat any

$_ZN15btNullPairCacheD2Ev = comdat any

$_ZN15btNullPairCacheD0Ev = comdat any

$_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher = comdat any

$_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN15btNullPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN15btNullPairCache23getOverlappingPairArrayEv = comdat any

$_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher = comdat any

$_ZNK15btNullPairCache22getNumOverlappingPairsEv = comdat any

$_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher = comdat any

$_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN15btNullPairCache18hasDeferredRemovalEv = comdat any

$_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher = comdat any

$_ZN25btOverlappingPairCallbackC2Ev = comdat any

$_ZN22btOverlappingPairCacheD2Ev = comdat any

$_ZN22btOverlappingPairCacheD0Ev = comdat any

$_ZN25btOverlappingPairCallbackD2Ev = comdat any

$_ZN25btOverlappingPairCallbackD0Ev = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN17btBroadphaseProxyC2Ev = comdat any

$_ZN20btAxisSweep3InternalItE6HandledaEPv = comdat any

$_ZN20btAxisSweep3InternalItEdlEPv = comdat any

$_ZN20btAxisSweep3InternalIjE6HandlenaEm = comdat any

$_ZN20btAxisSweep3InternalIjE6HandleC2Ev = comdat any

$_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj = comdat any

$_ZN20btAxisSweep3InternalIjED2Ev = comdat any

$_ZN20btAxisSweep3InternalIjED0Ev = comdat any

$_ZN20btAxisSweep3InternalIjE6HandledaEPv = comdat any

$_ZN20btAxisSweep3InternalIjEdlEPv = comdat any

$_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PviiP12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalItE9getHandleEt = comdat any

$_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i = comdat any

$_ZN20btAxisSweep3InternalItE11allocHandleEv = comdat any

$_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv = comdat any

$_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv = comdat any

$_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii = comdat any

$_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE10freeHandleEt = comdat any

$_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PviiP12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalIjE9getHandleEj = comdat any

$_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i = comdat any

$_ZN20btAxisSweep3InternalIjE11allocHandleEv = comdat any

$_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb = comdat any

$_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv = comdat any

$_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv = comdat any

$_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii = comdat any

$_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE10freeHandleEj = comdat any

$_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_ = comdat any

$_ZTV12btAxisSweep3 = comdat any

$_ZTV17bt32BitAxisSweep3 = comdat any

$_ZTS12btAxisSweep3 = comdat any

$_ZTS20btAxisSweep3InternalItE = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTI20btAxisSweep3InternalItE = comdat any

$_ZTI12btAxisSweep3 = comdat any

$_ZTS17bt32BitAxisSweep3 = comdat any

$_ZTS20btAxisSweep3InternalIjE = comdat any

$_ZTI20btAxisSweep3InternalIjE = comdat any

$_ZTI17bt32BitAxisSweep3 = comdat any

$_ZTV20btAxisSweep3InternalItE = comdat any

$_ZTV21btBroadphaseInterface = comdat any

$_ZTV15btNullPairCache = comdat any

$_ZTS15btNullPairCache = comdat any

$_ZTS22btOverlappingPairCache = comdat any

$_ZTS25btOverlappingPairCallback = comdat any

$_ZTI25btOverlappingPairCallback = comdat any

$_ZTI22btOverlappingPairCache = comdat any

$_ZTI15btNullPairCache = comdat any

$_ZTV22btOverlappingPairCache = comdat any

$_ZTV25btOverlappingPairCallback = comdat any

$_ZTV20btAxisSweep3InternalIjE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV12btAxisSweep3 = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btAxisSweep3 to i8*), i8* bitcast (%class.btAxisSweep3* (%class.btAxisSweep3*)* @_ZN12btAxisSweep3D2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3*)* @_ZN12btAxisSweep3D0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPviiP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE10printStatsEv to i8*)] }, comdat, align 4
@_ZTV17bt32BitAxisSweep3 = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17bt32BitAxisSweep3 to i8*), i8* bitcast (%class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*)* @_ZN17bt32BitAxisSweep3D2Ev to i8*), i8* bitcast (void (%class.bt32BitAxisSweep3*)* @_ZN17bt32BitAxisSweep3D0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPviiP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.9*)* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjE10printStatsEv to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS12btAxisSweep3 = linkonce_odr hidden constant [15 x i8] c"12btAxisSweep3\00", comdat, align 1
@_ZTS20btAxisSweep3InternalItE = linkonce_odr hidden constant [26 x i8] c"20btAxisSweep3InternalItE\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI20btAxisSweep3InternalItE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS20btAxisSweep3InternalItE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, comdat, align 4
@_ZTI12btAxisSweep3 = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btAxisSweep3, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalItE to i8*) }, comdat, align 4
@_ZTS17bt32BitAxisSweep3 = linkonce_odr hidden constant [20 x i8] c"17bt32BitAxisSweep3\00", comdat, align 1
@_ZTS20btAxisSweep3InternalIjE = linkonce_odr hidden constant [26 x i8] c"20btAxisSweep3InternalIjE\00", comdat, align 1
@_ZTI20btAxisSweep3InternalIjE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS20btAxisSweep3InternalIjE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, comdat, align 4
@_ZTI17bt32BitAxisSweep3 = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17bt32BitAxisSweep3, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalIjE to i8*) }, comdat, align 4
@_ZTV20btAxisSweep3InternalItE = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalItE to i8*), i8* bitcast (%class.btAxisSweep3Internal* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPviiP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE10printStatsEv to i8*)] }, comdat, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV15btNullPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btNullPairCache to i8*), i8* bitcast (%class.btNullPairCache* (%class.btNullPairCache*)* @_ZN15btNullPairCacheD2Ev to i8*), i8* bitcast (void (%class.btNullPairCache*)* @_ZN15btNullPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*)* @_ZN15btNullPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*)* @_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray.10* (%class.btNullPairCache*)* @_ZN15btNullPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btNullPairCache*)* @_ZNK15btNullPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btOverlapFilterCallback*)* @_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btNullPairCache*)* @_ZN15btNullPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %class.btOverlappingPairCallback*)* @_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btNullPairCache*, %class.btDispatcher*)* @_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, comdat, align 4
@_ZTS15btNullPairCache = linkonce_odr hidden constant [18 x i8] c"15btNullPairCache\00", comdat, align 1
@_ZTS22btOverlappingPairCache = linkonce_odr hidden constant [25 x i8] c"22btOverlappingPairCache\00", comdat, align 1
@_ZTS25btOverlappingPairCallback = linkonce_odr hidden constant [28 x i8] c"25btOverlappingPairCallback\00", comdat, align 1
@_ZTI25btOverlappingPairCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btOverlappingPairCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI22btOverlappingPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*) }, comdat, align 4
@_ZTI15btNullPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btNullPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, comdat, align 4
@_ZTV22btOverlappingPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV25btOverlappingPairCallback = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV20btAxisSweep3InternalIjE = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalIjE to i8*), i8* bitcast (%class.btAxisSweep3Internal.9* (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjED2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjED0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPviiP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.9*)* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.9*)* @_ZN20btAxisSweep3InternalIjE10printStatsEv to i8*)] }, comdat, align 4
@gOverlappingPairs = external global i32, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btAxisSweep3.cpp, i8* null }]

@_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb = hidden unnamed_addr alias %class.btAxisSweep3* (%class.btAxisSweep3*, %class.btVector3*, %class.btVector3*, i16, %class.btOverlappingPairCache*, i1), %class.btAxisSweep3* (%class.btAxisSweep3*, %class.btVector3*, %class.btVector3*, i16, %class.btOverlappingPairCache*, i1)* @_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
@_ZN17bt32BitAxisSweep3C1ERK9btVector3S2_jP22btOverlappingPairCacheb = hidden unnamed_addr alias %class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*, %class.btVector3*, %class.btVector3*, i32, %class.btOverlappingPairCache*, i1), %class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*, %class.btVector3*, %class.btVector3*, i32, %class.btOverlappingPairCache*, i1)* @_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btAxisSweep3* @_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb(%class.btAxisSweep3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i16 zeroext %maxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btAxisSweep3*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %maxHandles.addr = alloca i16, align 2
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  store %class.btAxisSweep3* %this, %class.btAxisSweep3** %this.addr, align 4
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4
  store i16 %maxHandles, i16* %maxHandles.addr, align 2
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1
  %this1 = load %class.btAxisSweep3*, %class.btAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.btAxisSweep3* %this1 to %class.btAxisSweep3Internal*
  %1 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4
  %3 = load i16, i16* %maxHandles.addr, align 2
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %5 = load i8, i8* %disableRaycastAccelerator.addr, align 1
  %tobool = trunc i8 %5 to i1
  %call = call %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb(%class.btAxisSweep3Internal* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i16 zeroext -2, i16 zeroext -1, i16 zeroext %3, %class.btOverlappingPairCache* %4, i1 zeroext %tobool)
  %6 = bitcast %class.btAxisSweep3* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV12btAxisSweep3, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4
  ret %class.btAxisSweep3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb(%class.btAxisSweep3Internal* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i16 zeroext %handleMask, i16 zeroext %handleSentinel, i16 zeroext %userMaxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %handleMask.addr = alloca i16, align 2
  %handleSentinel.addr = alloca i16, align 2
  %userMaxHandles.addr = alloca i16, align 2
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  %maxHandles = alloca i16, align 2
  %ptr = alloca i8*, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %maxInt = alloca i16, align 2
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %i = alloca i16, align 2
  %i49 = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4
  store i16 %handleMask, i16* %handleMask.addr, align 2
  store i16 %handleSentinel, i16* %handleSentinel.addr, align 2
  store i16 %userMaxHandles, i16* %userMaxHandles.addr, align 2
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btAxisSweep3Internal* %this1, %class.btAxisSweep3Internal** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #9
  %1 = bitcast %class.btAxisSweep3Internal* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalItE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %2 = load i16, i16* %handleMask.addr, align 2
  store i16 %2, i16* %m_bpHandleMask, align 4
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %3 = load i16, i16* %handleSentinel.addr, align 2
  store i16 %3, i16* %m_handleSentinel, align 2
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMin)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMax)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_quantize)
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btOverlappingPairCache* %4, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  store i8 0, i8* %m_ownsPairCache, align 4
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* null, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %5 = load i16, i16* %userMaxHandles.addr, align 2
  %conv = zext i16 %5 to i32
  %add = add nsw i32 %conv, 1
  %conv5 = trunc i32 %add to i16
  store i16 %conv5, i16* %maxHandles, align 2
  %m_pairCache6 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache6, align 4
  %tobool = icmp ne %class.btOverlappingPairCache* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 72, i32 16)
  store i8* %call7, i8** %ptr, align 4
  %7 = load i8*, i8** %ptr, align 4
  %call8 = call i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 72, i8* %7)
  %8 = bitcast i8* %call8 to %class.btHashedOverlappingPairCache*
  %call9 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %8)
  %9 = bitcast %class.btHashedOverlappingPairCache* %8 to %class.btOverlappingPairCache*
  %m_pairCache10 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  store %class.btOverlappingPairCache* %9, %class.btOverlappingPairCache** %m_pairCache10, align 4
  %m_ownsPairCache11 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  store i8 1, i8* %m_ownsPairCache11, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i8, i8* %disableRaycastAccelerator.addr, align 1
  %tobool12 = trunc i8 %10 to i1
  br i1 %tobool12, label %if.end21, label %if.then13

if.then13:                                        ; preds = %if.end
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  %11 = bitcast i8* %call14 to %class.btNullPairCache*
  %12 = bitcast %class.btNullPairCache* %11 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 24, i1 false)
  %call15 = call %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* %11)
  %13 = bitcast %class.btNullPairCache* %11 to %class.btOverlappingPairCache*
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  store %class.btOverlappingPairCache* %13, %class.btOverlappingPairCache** %m_nullPairCache, align 4
  %call16 = call i8* @_Z22btAlignedAllocInternalmi(i32 176, i32 16)
  %14 = bitcast i8* %call16 to %struct.btDbvtBroadphase*
  %m_nullPairCache17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %15 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache17, align 4
  %call18 = call %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* %14, %class.btOverlappingPairCache* %15)
  %m_raycastAccelerator19 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* %14, %struct.btDbvtBroadphase** %m_raycastAccelerator19, align 4
  %m_raycastAccelerator20 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %16 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator20, align 4
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %16, i32 0, i32 18
  store i8 1, i8* %m_deferedcollide, align 1
  br label %if.end21

if.end21:                                         ; preds = %if.then13, %if.end
  %17 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4
  %m_worldAabbMin22 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %18 = bitcast %class.btVector3* %m_worldAabbMin22 to i8*
  %19 = bitcast %class.btVector3* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %20 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4
  %m_worldAabbMax23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %21 = bitcast %class.btVector3* %m_worldAabbMax23 to i8*
  %22 = bitcast %class.btVector3* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %m_worldAabbMax24 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %m_worldAabbMin25 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMax24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin25)
  %m_handleSentinel26 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %23 = load i16, i16* %m_handleSentinel26, align 2
  store i16 %23, i16* %maxInt, align 2
  %24 = load i16, i16* %maxInt, align 2
  %conv29 = uitofp i16 %24 to float
  store float %conv29, float* %ref.tmp28, align 4
  %25 = load i16, i16* %maxInt, align 2
  %conv31 = uitofp i16 %25 to float
  store float %conv31, float* %ref.tmp30, align 4
  %26 = load i16, i16* %maxInt, align 2
  %conv33 = uitofp i16 %26 to float
  store float %conv33, float* %ref.tmp32, align 4
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_quantize35 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  %27 = bitcast %class.btVector3* %m_quantize35 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load i16, i16* %maxHandles, align 2
  %conv36 = zext i16 %29 to i32
  %30 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %conv36, i32 64)
  %31 = extractvalue { i32, i1 } %30, 1
  %32 = extractvalue { i32, i1 } %30, 0
  %33 = select i1 %31, i32 -1, i32 %32
  %call37 = call i8* @_ZN20btAxisSweep3InternalItE6HandlenaEm(i32 %33)
  %34 = bitcast i8* %call37 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  %isempty = icmp eq i32 %conv36, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end21
  %arrayctor.end = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %34, i32 %conv36
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %"class.btAxisSweep3Internal<unsigned short>::Handle"* [ %34, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call38 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZN20btAxisSweep3InternalItE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end21, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %34, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %35 = load i16, i16* %maxHandles, align 2
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  store i16 %35, i16* %m_maxHandles, align 2
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  store i16 0, i16* %m_numHandles, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 1, i16* %m_firstFreeHandle, align 4
  %m_firstFreeHandle39 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %36 = load i16, i16* %m_firstFreeHandle39, align 4
  store i16 %36, i16* %i, align 2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %37 = load i16, i16* %i, align 2
  %conv40 = zext i16 %37 to i32
  %38 = load i16, i16* %maxHandles, align 2
  %conv41 = zext i16 %38 to i32
  %cmp = icmp slt i32 %conv40, %conv41
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles42 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %39 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles42, align 4
  %40 = load i16, i16* %i, align 2
  %idxprom = zext i16 %40 to i32
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %39, i32 %idxprom
  %41 = load i16, i16* %i, align 2
  %conv43 = zext i16 %41 to i32
  %add44 = add nsw i32 %conv43, 1
  %conv45 = trunc i32 %add44 to i16
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i16 zeroext %conv45)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i16, i16* %i, align 2
  %inc = add i16 %42, 1
  store i16 %inc, i16* %i, align 2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles46 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %43 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles46, align 4
  %44 = load i16, i16* %maxHandles, align 2
  %conv47 = zext i16 %44 to i32
  %sub = sub nsw i32 %conv47, 1
  %arrayidx48 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %43, i32 %sub
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx48, i16 zeroext 0)
  store i32 0, i32* %i49, align 4
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc62, %for.end
  %45 = load i32, i32* %i49, align 4
  %cmp51 = icmp slt i32 %45, 3
  br i1 %cmp51, label %for.body52, label %for.end64

for.body52:                                       ; preds = %for.cond50
  %46 = load i16, i16* %maxHandles, align 2
  %conv53 = zext i16 %46 to i32
  %mul = mul i32 4, %conv53
  %mul54 = mul i32 %mul, 2
  %call55 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul54, i32 16)
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %47 = load i32, i32* %i49, align 4
  %arrayidx56 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %47
  store i8* %call55, i8** %arrayidx56, align 4
  %48 = load i16, i16* %maxHandles, align 2
  %conv57 = zext i16 %48 to i32
  %mul58 = mul nsw i32 %conv57, 2
  %49 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %mul58, i32 4)
  %50 = extractvalue { i32, i1 } %49, 1
  %51 = extractvalue { i32, i1 } %49, 0
  %52 = select i1 %50, i32 -1, i32 %51
  %m_pEdgesRawPtr59 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %53 = load i32, i32* %i49, align 4
  %arrayidx60 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr59, i32 0, i32 %53
  %54 = load i8*, i8** %arrayidx60, align 4
  %55 = bitcast i8* %54 to %"class.btAxisSweep3Internal<unsigned short>::Edge"*
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %56 = load i32, i32* %i49, align 4
  %arrayidx61 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %56
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %55, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx61, align 4
  br label %for.inc62

for.inc62:                                        ; preds = %for.body52
  %57 = load i32, i32* %i49, align 4
  %inc63 = add nsw i32 %57, 1
  store i32 %inc63, i32* %i49, align 4
  br label %for.cond50

for.end64:                                        ; preds = %for.cond50
  %m_pHandles65 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %58 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles65, align 4
  %arrayidx66 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %58, i32 0
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx66 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %59, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc91, %for.end64
  %60 = load i32, i32* %axis, align 4
  %cmp68 = icmp slt i32 %60, 3
  br i1 %cmp68, label %for.body69, label %for.end93

for.body69:                                       ; preds = %for.cond67
  %m_pHandles70 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %61 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles70, align 4
  %arrayidx71 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %61, i32 0
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx71, i32 0, i32 1
  %62 = load i32, i32* %axis, align 4
  %arrayidx72 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %62
  store i16 0, i16* %arrayidx72, align 2
  %m_pHandles73 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %63 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles73, align 4
  %arrayidx74 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %63, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx74, i32 0, i32 2
  %64 = load i32, i32* %axis, align 4
  %arrayidx75 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %64
  store i16 1, i16* %arrayidx75, align 2
  %m_pEdges76 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %65 = load i32, i32* %axis, align 4
  %arrayidx77 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges76, i32 0, i32 %65
  %66 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx77, align 4
  %arrayidx78 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %66, i32 0
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx78, i32 0, i32 0
  store i16 0, i16* %m_pos, align 2
  %m_pEdges79 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %67 = load i32, i32* %axis, align 4
  %arrayidx80 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges79, i32 0, i32 %67
  %68 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx80, align 4
  %arrayidx81 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %68, i32 0
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx81, i32 0, i32 1
  store i16 0, i16* %m_handle, align 2
  %m_handleSentinel82 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %69 = load i16, i16* %m_handleSentinel82, align 2
  %m_pEdges83 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %70 = load i32, i32* %axis, align 4
  %arrayidx84 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges83, i32 0, i32 %70
  %71 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx84, align 4
  %arrayidx85 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %71, i32 1
  %m_pos86 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx85, i32 0, i32 0
  store i16 %69, i16* %m_pos86, align 2
  %m_pEdges87 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %72 = load i32, i32* %axis, align 4
  %arrayidx88 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges87, i32 0, i32 %72
  %73 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx88, align 4
  %arrayidx89 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %73, i32 1
  %m_handle90 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx89, i32 0, i32 1
  store i16 0, i16* %m_handle90, align 2
  br label %for.inc91

for.inc91:                                        ; preds = %for.body69
  %74 = load i32, i32* %axis, align 4
  %inc92 = add nsw i32 %74, 1
  store i32 %inc92, i32* %axis, align 4
  br label %for.cond67

for.end93:                                        ; preds = %for.cond67
  %75 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %retval, align 4
  ret %class.btAxisSweep3Internal* %75
}

; Function Attrs: noinline optnone
define hidden %class.bt32BitAxisSweep3* @_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb(%class.bt32BitAxisSweep3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i32 %maxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.bt32BitAxisSweep3*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %maxHandles.addr = alloca i32, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  store %class.bt32BitAxisSweep3* %this, %class.bt32BitAxisSweep3** %this.addr, align 4
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4
  store i32 %maxHandles, i32* %maxHandles.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1
  %this1 = load %class.bt32BitAxisSweep3*, %class.bt32BitAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.bt32BitAxisSweep3* %this1 to %class.btAxisSweep3Internal.9*
  %1 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4
  %3 = load i32, i32* %maxHandles.addr, align 4
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %5 = load i8, i8* %disableRaycastAccelerator.addr, align 1
  %tobool = trunc i8 %5 to i1
  %call = call %class.btAxisSweep3Internal.9* @_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb(%class.btAxisSweep3Internal.9* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 -2, i32 2147483647, i32 %3, %class.btOverlappingPairCache* %4, i1 zeroext %tobool)
  %6 = bitcast %class.bt32BitAxisSweep3* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV17bt32BitAxisSweep3, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4
  ret %class.bt32BitAxisSweep3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAxisSweep3Internal.9* @_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb(%class.btAxisSweep3Internal.9* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i32 %handleMask, i32 %handleSentinel, i32 %userMaxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal.9*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %handleMask.addr = alloca i32, align 4
  %handleSentinel.addr = alloca i32, align 4
  %userMaxHandles.addr = alloca i32, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  %maxHandles = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %maxInt = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %i = alloca i32, align 4
  %i41 = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4
  store i32 %handleMask, i32* %handleMask.addr, align 4
  store i32 %handleSentinel, i32* %handleSentinel.addr, align 4
  store i32 %userMaxHandles, i32* %userMaxHandles.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btAxisSweep3Internal.9* %this1, %class.btAxisSweep3Internal.9** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal.9* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #9
  %1 = bitcast %class.btAxisSweep3Internal.9* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalIjE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %2 = load i32, i32* %handleMask.addr, align 4
  store i32 %2, i32* %m_bpHandleMask, align 4
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %3 = load i32, i32* %handleSentinel.addr, align 4
  store i32 %3, i32* %m_handleSentinel, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMin)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMax)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_quantize)
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %class.btOverlappingPairCache* %4, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 14
  store i8 0, i8* %m_ownsPairCache, align 4
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* null, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %5 = load i32, i32* %userMaxHandles.addr, align 4
  %add = add i32 %5, 1
  store i32 %add, i32* %maxHandles, align 4
  %m_pairCache5 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache5, align 4
  %tobool = icmp ne %class.btOverlappingPairCache* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 72, i32 16)
  store i8* %call6, i8** %ptr, align 4
  %7 = load i8*, i8** %ptr, align 4
  %call7 = call i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 72, i8* %7)
  %8 = bitcast i8* %call7 to %class.btHashedOverlappingPairCache*
  %call8 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %8)
  %9 = bitcast %class.btHashedOverlappingPairCache* %8 to %class.btOverlappingPairCache*
  %m_pairCache9 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  store %class.btOverlappingPairCache* %9, %class.btOverlappingPairCache** %m_pairCache9, align 4
  %m_ownsPairCache10 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 14
  store i8 1, i8* %m_ownsPairCache10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i8, i8* %disableRaycastAccelerator.addr, align 1
  %tobool11 = trunc i8 %10 to i1
  br i1 %tobool11, label %if.end20, label %if.then12

if.then12:                                        ; preds = %if.end
  %call13 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  %11 = bitcast i8* %call13 to %class.btNullPairCache*
  %12 = bitcast %class.btNullPairCache* %11 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 24, i1 false)
  %call14 = call %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* %11)
  %13 = bitcast %class.btNullPairCache* %11 to %class.btOverlappingPairCache*
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 17
  store %class.btOverlappingPairCache* %13, %class.btOverlappingPairCache** %m_nullPairCache, align 4
  %call15 = call i8* @_Z22btAlignedAllocInternalmi(i32 176, i32 16)
  %14 = bitcast i8* %call15 to %struct.btDbvtBroadphase*
  %m_nullPairCache16 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 17
  %15 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache16, align 4
  %call17 = call %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* %14, %class.btOverlappingPairCache* %15)
  %m_raycastAccelerator18 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* %14, %struct.btDbvtBroadphase** %m_raycastAccelerator18, align 4
  %m_raycastAccelerator19 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %16 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator19, align 4
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %16, i32 0, i32 18
  store i8 1, i8* %m_deferedcollide, align 1
  br label %if.end20

if.end20:                                         ; preds = %if.then12, %if.end
  %17 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4
  %m_worldAabbMin21 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 3
  %18 = bitcast %class.btVector3* %m_worldAabbMin21 to i8*
  %19 = bitcast %class.btVector3* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %20 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4
  %m_worldAabbMax22 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 4
  %21 = bitcast %class.btVector3* %m_worldAabbMax22 to i8*
  %22 = bitcast %class.btVector3* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %m_worldAabbMax23 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 4
  %m_worldAabbMin24 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMax23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin24)
  %m_handleSentinel25 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %23 = load i32, i32* %m_handleSentinel25, align 4
  store i32 %23, i32* %maxInt, align 4
  %24 = load i32, i32* %maxInt, align 4
  %conv = uitofp i32 %24 to float
  store float %conv, float* %ref.tmp27, align 4
  %25 = load i32, i32* %maxInt, align 4
  %conv29 = uitofp i32 %25 to float
  store float %conv29, float* %ref.tmp28, align 4
  %26 = load i32, i32* %maxInt, align 4
  %conv31 = uitofp i32 %26 to float
  store float %conv31, float* %ref.tmp30, align 4
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_quantize33 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 5
  %27 = bitcast %class.btVector3* %m_quantize33 to i8*
  %28 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load i32, i32* %maxHandles, align 4
  %30 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %29, i32 76)
  %31 = extractvalue { i32, i1 } %30, 1
  %32 = extractvalue { i32, i1 } %30, 0
  %33 = select i1 %31, i32 -1, i32 %32
  %call34 = call i8* @_ZN20btAxisSweep3InternalIjE6HandlenaEm(i32 %33)
  %34 = bitcast i8* %call34 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  %isempty = icmp eq i32 %29, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end20
  %arrayctor.end = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %34, i32 %29
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %"class.btAxisSweep3Internal<unsigned int>::Handle"* [ %34, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call35 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZN20btAxisSweep3InternalIjE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end20, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %34, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %35 = load i32, i32* %maxHandles, align 4
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 7
  store i32 %35, i32* %m_maxHandles, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  store i32 0, i32* %m_numHandles, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  store i32 1, i32* %m_firstFreeHandle, align 4
  %m_firstFreeHandle36 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  %36 = load i32, i32* %m_firstFreeHandle36, align 4
  store i32 %36, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %37 = load i32, i32* %i, align 4
  %38 = load i32, i32* %maxHandles, align 4
  %cmp = icmp ult i32 %37, %38
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles37 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %39 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles37, align 4
  %40 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %39, i32 %40
  %41 = load i32, i32* %i, align 4
  %add38 = add i32 %41, 1
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 %add38)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %42 = load i32, i32* %i, align 4
  %inc = add i32 %42, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles39 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %43 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles39, align 4
  %44 = load i32, i32* %maxHandles, align 4
  %sub = sub i32 %44, 1
  %arrayidx40 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %43, i32 %sub
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx40, i32 0)
  store i32 0, i32* %i41, align 4
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc52, %for.end
  %45 = load i32, i32* %i41, align 4
  %cmp43 = icmp slt i32 %45, 3
  br i1 %cmp43, label %for.body44, label %for.end54

for.body44:                                       ; preds = %for.cond42
  %46 = load i32, i32* %maxHandles, align 4
  %mul = mul i32 8, %46
  %mul45 = mul i32 %mul, 2
  %call46 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul45, i32 16)
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 11
  %47 = load i32, i32* %i41, align 4
  %arrayidx47 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %47
  store i8* %call46, i8** %arrayidx47, align 4
  %48 = load i32, i32* %maxHandles, align 4
  %mul48 = mul i32 %48, 2
  %49 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %mul48, i32 8)
  %50 = extractvalue { i32, i1 } %49, 1
  %51 = extractvalue { i32, i1 } %49, 0
  %52 = select i1 %50, i32 -1, i32 %51
  %m_pEdgesRawPtr49 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 11
  %53 = load i32, i32* %i41, align 4
  %arrayidx50 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr49, i32 0, i32 %53
  %54 = load i8*, i8** %arrayidx50, align 4
  %55 = bitcast i8* %54 to %"class.btAxisSweep3Internal<unsigned int>::Edge"*
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %56 = load i32, i32* %i41, align 4
  %arrayidx51 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %56
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %55, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx51, align 4
  br label %for.inc52

for.inc52:                                        ; preds = %for.body44
  %57 = load i32, i32* %i41, align 4
  %inc53 = add nsw i32 %57, 1
  store i32 %inc53, i32* %i41, align 4
  br label %for.cond42

for.end54:                                        ; preds = %for.cond42
  %m_pHandles55 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %58 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles55, align 4
  %arrayidx56 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %58, i32 0
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx56 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %59, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc81, %for.end54
  %60 = load i32, i32* %axis, align 4
  %cmp58 = icmp slt i32 %60, 3
  br i1 %cmp58, label %for.body59, label %for.end83

for.body59:                                       ; preds = %for.cond57
  %m_pHandles60 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %61 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles60, align 4
  %arrayidx61 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %61, i32 0
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx61, i32 0, i32 1
  %62 = load i32, i32* %axis, align 4
  %arrayidx62 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %62
  store i32 0, i32* %arrayidx62, align 4
  %m_pHandles63 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %63 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles63, align 4
  %arrayidx64 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %63, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx64, i32 0, i32 2
  %64 = load i32, i32* %axis, align 4
  %arrayidx65 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %64
  store i32 1, i32* %arrayidx65, align 4
  %m_pEdges66 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %65 = load i32, i32* %axis, align 4
  %arrayidx67 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges66, i32 0, i32 %65
  %66 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx67, align 4
  %arrayidx68 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %66, i32 0
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx68, i32 0, i32 0
  store i32 0, i32* %m_pos, align 4
  %m_pEdges69 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %67 = load i32, i32* %axis, align 4
  %arrayidx70 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges69, i32 0, i32 %67
  %68 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx70, align 4
  %arrayidx71 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %68, i32 0
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx71, i32 0, i32 1
  store i32 0, i32* %m_handle, align 4
  %m_handleSentinel72 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %69 = load i32, i32* %m_handleSentinel72, align 4
  %m_pEdges73 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %70 = load i32, i32* %axis, align 4
  %arrayidx74 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges73, i32 0, i32 %70
  %71 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx74, align 4
  %arrayidx75 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %71, i32 1
  %m_pos76 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx75, i32 0, i32 0
  store i32 %69, i32* %m_pos76, align 4
  %m_pEdges77 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %72 = load i32, i32* %axis, align 4
  %arrayidx78 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges77, i32 0, i32 %72
  %73 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx78, align 4
  %arrayidx79 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %73, i32 1
  %m_handle80 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx79, i32 0, i32 1
  store i32 0, i32* %m_handle80, align 4
  br label %for.inc81

for.inc81:                                        ; preds = %for.body59
  %74 = load i32, i32* %axis, align 4
  %inc82 = add nsw i32 %74, 1
  store i32 %inc82, i32* %axis, align 4
  br label %for.cond57

for.end83:                                        ; preds = %for.cond57
  %75 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %retval, align 4
  ret %class.btAxisSweep3Internal.9* %75
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAxisSweep3* @_ZN12btAxisSweep3D2Ev(%class.btAxisSweep3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3*, align 4
  store %class.btAxisSweep3* %this, %class.btAxisSweep3** %this.addr, align 4
  %this1 = load %class.btAxisSweep3*, %class.btAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.btAxisSweep3* %this1 to %class.btAxisSweep3Internal*
  %call = call %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItED2Ev(%class.btAxisSweep3Internal* %0) #9
  ret %class.btAxisSweep3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btAxisSweep3D0Ev(%class.btAxisSweep3* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3*, align 4
  store %class.btAxisSweep3* %this, %class.btAxisSweep3** %this.addr, align 4
  %this1 = load %class.btAxisSweep3*, %class.btAxisSweep3** %this.addr, align 4
  %call = call %class.btAxisSweep3* @_ZN12btAxisSweep3D2Ev(%class.btAxisSweep3* %this1) #9
  %0 = bitcast %class.btAxisSweep3* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPviiP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handleId = alloca i16, align 2
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %rayProxy = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %shapeType, i32* %shapeType.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %2 = load i8*, i8** %userPtr.addr, align 4
  %3 = load i32, i32* %collisionFilterGroup.addr, align 4
  %4 = load i32, i32* %collisionFilterMask.addr, align 4
  %5 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call zeroext i16 @_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PviiP12btDispatcher(%class.btAxisSweep3Internal* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i8* %2, i32 %3, i32 %4, %class.btDispatcher* %5)
  store i16 %call, i16* %handleId, align 2
  %6 = load i16, i16* %handleId, align 2
  %call2 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %6)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %7, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %8 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %11 = load i32, i32* %shapeType.addr, align 4
  %12 = load i8*, i8** %userPtr.addr, align 4
  %13 = load i32, i32* %collisionFilterGroup.addr, align 4
  %14 = load i32, i32* %collisionFilterMask.addr, align 4
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %16 = bitcast %struct.btDbvtBroadphase* %8 to %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)***
  %vtable = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)**, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*** %16, align 4
  %vfn = getelementptr inbounds %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)** %vtable, i64 2
  %17 = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)** %vfn, align 4
  %call4 = call %struct.btBroadphaseProxy* %17(%struct.btDbvtBroadphase* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, i32 %11, i8* %12, i32 %13, i32 %14, %class.btDispatcher* %15)
  store %struct.btBroadphaseProxy* %call4, %struct.btBroadphaseProxy** %rayProxy, align 4
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %rayProxy, align 4
  %19 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %19, i32 0, i32 3
  store %struct.btBroadphaseProxy* %18, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20 to %struct.btBroadphaseProxy*
  ret %struct.btBroadphaseProxy* %21
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %2 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %3 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %4, i32 0, i32 3
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %7 = bitcast %struct.btDbvtBroadphase* %3 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %7, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %8 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %3, %struct.btBroadphaseProxy* %5, %class.btDispatcher* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %10 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %9 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 3
  %11 = load i32, i32* %m_uniqueId, align 4
  %conv = trunc i32 %11 to i16
  %12 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher(%class.btAxisSweep3Internal* %this1, i16 zeroext %conv, %class.btDispatcher* %12)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %3 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = bitcast %class.btVector3* %m_aabbMin to i8*
  %6 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 5
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  %11 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %12 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId, align 4
  %conv = trunc i32 %14 to i16
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %16 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %17 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal* %this1, i16 zeroext %conv, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btDispatcher* %17)
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %18 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %18, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20, i32 0, i32 3
  %21 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  %22 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %23 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %24 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %25 = bitcast %struct.btDbvtBroadphase* %19 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %25, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable, i64 4
  %26 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn, align 4
  call void %26(%struct.btDbvtBroadphase* %19, %struct.btBroadphaseProxy* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btDispatcher* %24)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %2 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %3 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %2 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %7 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 5
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %axis = alloca i16, align 2
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %7 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %7, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %8 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  br label %if.end17

if.else:                                          ; preds = %entry
  store i16 0, i16* %axis, align 2
  store i16 1, i16* %i, align 2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %9 = load i16, i16* %i, align 2
  %conv = zext i16 %9 to i32
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %10 = load i16, i16* %m_numHandles, align 4
  %conv3 = zext i16 %10 to i32
  %mul = mul nsw i32 %conv3, 2
  %add = add nsw i32 %mul, 1
  %cmp = icmp slt i32 %conv, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %11 = load i16, i16* %axis, align 2
  %idxprom = zext i16 %11 to i32
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %13 = load i16, i16* %i, align 2
  %idxprom4 = zext i16 %13 to i32
  %arrayidx5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %12, i32 %idxprom4
  %call = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx5)
  %tobool6 = icmp ne i16 %call, 0
  br i1 %tobool6, label %if.then7, label %if.end

if.then7:                                         ; preds = %for.body
  %14 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %15 = bitcast %struct.btBroadphaseRayCallback* %14 to %struct.btBroadphaseAabbCallback*
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %16 = load i16, i16* %axis, align 2
  %idxprom9 = zext i16 %16 to i32
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges8, i32 0, i32 %idxprom9
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx10, align 4
  %18 = load i16, i16* %i, align 2
  %idxprom11 = zext i16 %18 to i32
  %arrayidx12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %17, i32 %idxprom11
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx12, i32 0, i32 1
  %19 = load i16, i16* %m_handle, align 2
  %call13 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %19)
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call13 to %struct.btBroadphaseProxy*
  %21 = bitcast %struct.btBroadphaseAabbCallback* %15 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable14 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %21, align 4
  %vfn15 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable14, i64 2
  %22 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn15, align 4
  %call16 = call zeroext i1 %22(%struct.btBroadphaseAabbCallback* %15, %struct.btBroadphaseProxy* %20)
  br label %if.end

if.end:                                           ; preds = %if.then7, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i16, i16* %i, align 2
  %inc = add i16 %23, 1
  store i16 %inc, i16* %i, align 2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end17

if.end17:                                         ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %axis = alloca i16, align 2
  %i = alloca i16, align 2
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %5 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*** %5, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vtable, i64 7
  %6 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vfn, align 4
  call void %6(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %4)
  br label %if.end20

if.else:                                          ; preds = %entry
  store i16 0, i16* %axis, align 2
  store i16 1, i16* %i, align 2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %7 = load i16, i16* %i, align 2
  %conv = zext i16 %7 to i32
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %8 = load i16, i16* %m_numHandles, align 4
  %conv3 = zext i16 %8 to i32
  %mul = mul nsw i32 %conv3, 2
  %add = add nsw i32 %mul, 1
  %cmp = icmp slt i32 %conv, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %9 = load i16, i16* %axis, align 2
  %idxprom = zext i16 %9 to i32
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %11 = load i16, i16* %i, align 2
  %idxprom4 = zext i16 %11 to i32
  %arrayidx5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %10, i32 %idxprom4
  %call = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx5)
  %tobool6 = icmp ne i16 %call, 0
  br i1 %tobool6, label %if.then7, label %if.end19

if.then7:                                         ; preds = %for.body
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %12 = load i16, i16* %axis, align 2
  %idxprom9 = zext i16 %12 to i32
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges8, i32 0, i32 %idxprom9
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx10, align 4
  %14 = load i16, i16* %i, align 2
  %idxprom11 = zext i16 %14 to i32
  %arrayidx12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 %idxprom11
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx12, i32 0, i32 1
  %15 = load i16, i16* %m_handle, align 2
  %call13 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %15)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call13, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %19 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %19, i32 0, i32 4
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 5
  %call14 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then7
  %22 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4
  %24 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %23 to %struct.btBroadphaseProxy*
  %25 = bitcast %struct.btBroadphaseAabbCallback* %22 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable16 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %25, align 4
  %vfn17 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable16, i64 2
  %26 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn17, align 4
  %call18 = call zeroext i1 %26(%struct.btBroadphaseAabbCallback* %22, %struct.btBroadphaseProxy* %24)
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.then7
  br label %if.end19

if.end19:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %27 = load i16, i16* %i, align 2
  %inc = add i16 %27, 1
  store i16 %inc, i16* %i, align 2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end20

if.end20:                                         ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.10*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp7 = alloca %struct.btBroadphasePair, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp33 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp37 = alloca %struct.btBroadphasePair, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %1 = bitcast %class.btOverlappingPairCache* %0 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %2 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %2(%class.btOverlappingPairCache* %0)
  br i1 %call, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  %m_pairCache2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)***
  %vtable3 = load %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*** %4, align 4
  %vfn4 = getelementptr inbounds %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)** %vtable3, i64 7
  %5 = load %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* %5(%class.btOverlappingPairCache* %3)
  store %class.btAlignedObjectArray.10* %call5, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %6 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.10* %6, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %7 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %8 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %8)
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %9 = load i32, i32* %m_invalidPair, align 4
  %sub = sub nsw i32 %call6, %9
  %call8 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp7)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.10* %7, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %m_invalidPair9 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair9, align 4
  %call10 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4
  %11 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %11)
  %cmp = icmp slt i32 %10, %call11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %13 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %12, i32 %13)
  store %struct.btBroadphasePair* %call12, %struct.btBroadphasePair** %pair, align 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %call13 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %14, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call13 to i8
  store i8 %frombool, i8* %isDuplicate, align 1
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %16 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %17 = bitcast %struct.btBroadphasePair* %15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  store i8 0, i8* %needsRemoval, align 1
  %18 = load i8, i8* %isDuplicate, align 1
  %tobool = trunc i8 %18 to i1
  br i1 %tobool, label %if.else21, label %if.then14

if.then14:                                        ; preds = %for.body
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy015 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 0
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy015, align 4
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy116 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %21, i32 0, i32 1
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy116, align 4
  %call17 = call zeroext i1 @_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal* %this1, %struct.btBroadphaseProxy* %20, %struct.btBroadphaseProxy* %22)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %hasOverlap, align 1
  %23 = load i8, i8* %hasOverlap, align 1
  %tobool19 = trunc i8 %23 to i1
  br i1 %tobool19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.then14
  store i8 0, i8* %needsRemoval, align 1
  br label %if.end

if.else:                                          ; preds = %if.then14
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then20
  br label %if.end22

if.else21:                                        ; preds = %for.body
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end22

if.end22:                                         ; preds = %if.else21, %if.end
  %24 = load i8, i8* %needsRemoval, align 1
  %tobool23 = trunc i8 %24 to i1
  br i1 %tobool23, label %if.then24, label %if.end31

if.then24:                                        ; preds = %if.end22
  %m_pairCache25 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %25 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %27 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %28 = bitcast %class.btOverlappingPairCache* %25 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable26 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %28, align 4
  %vfn27 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable26, i64 8
  %29 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn27, align 4
  call void %29(%class.btOverlappingPairCache* %25, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %26, %class.btDispatcher* %27)
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy028 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy028, align 4
  %31 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy129 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %31, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy129, align 4
  %m_invalidPair30 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %32 = load i32, i32* %m_invalidPair30, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %m_invalidPair30, align 4
  %33 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %33, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then24, %if.end22
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %34 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %34, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.10* %35, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp33)
  %36 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %37 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call34 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %37)
  %m_invalidPair35 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %38 = load i32, i32* %m_invalidPair35, align 4
  %sub36 = sub nsw i32 %call34, %38
  %call38 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp37)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.10* %36, i32 %sub36, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %m_invalidPair39 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair39, align 4
  br label %if.end40

if.end40:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_worldAabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_worldAabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btDispatcher* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %1 = load i16, i16* %m_numHandles, align 4
  %conv = zext i16 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 1, i16* %m_firstFreeHandle, align 4
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %2 = load i16, i16* %m_firstFreeHandle2, align 4
  store i16 %2, i16* %i, align 2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i16, i16* %i, align 2
  %conv3 = zext i16 %3 to i32
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  %4 = load i16, i16* %m_maxHandles, align 2
  %conv4 = zext i16 %4 to i32
  %cmp5 = icmp slt i32 %conv3, %conv4
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %6 = load i16, i16* %i, align 2
  %idxprom = zext i16 %6 to i32
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %5, i32 %idxprom
  %7 = load i16, i16* %i, align 2
  %conv6 = zext i16 %7 to i32
  %add = add nsw i32 %conv6, 1
  %conv7 = trunc i32 %add to i16
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i16 zeroext %conv7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i16, i16* %i, align 2
  %inc = add i16 %8, 1
  store i16 %inc, i16* %i, align 2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles8, align 4
  %m_maxHandles9 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  %10 = load i16, i16* %m_maxHandles9, align 2
  %conv10 = zext i16 %10 to i32
  %sub = sub nsw i32 %conv10, 1
  %arrayidx11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %9, i32 %sub
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx11, i16 zeroext 0)
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE10printStatsEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.bt32BitAxisSweep3* @_ZN17bt32BitAxisSweep3D2Ev(%class.bt32BitAxisSweep3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.bt32BitAxisSweep3*, align 4
  store %class.bt32BitAxisSweep3* %this, %class.bt32BitAxisSweep3** %this.addr, align 4
  %this1 = load %class.bt32BitAxisSweep3*, %class.bt32BitAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.bt32BitAxisSweep3* %this1 to %class.btAxisSweep3Internal.9*
  %call = call %class.btAxisSweep3Internal.9* @_ZN20btAxisSweep3InternalIjED2Ev(%class.btAxisSweep3Internal.9* %0) #9
  ret %class.bt32BitAxisSweep3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17bt32BitAxisSweep3D0Ev(%class.bt32BitAxisSweep3* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.bt32BitAxisSweep3*, align 4
  store %class.bt32BitAxisSweep3* %this, %class.bt32BitAxisSweep3** %this.addr, align 4
  %this1 = load %class.bt32BitAxisSweep3*, %class.bt32BitAxisSweep3** %this.addr, align 4
  %call = call %class.bt32BitAxisSweep3* @_ZN17bt32BitAxisSweep3D2Ev(%class.bt32BitAxisSweep3* %this1) #9
  %0 = bitcast %class.bt32BitAxisSweep3* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPviiP12btDispatcher(%class.btAxisSweep3Internal.9* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handleId = alloca i32, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %rayProxy = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %shapeType, i32* %shapeType.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %2 = load i8*, i8** %userPtr.addr, align 4
  %3 = load i32, i32* %collisionFilterGroup.addr, align 4
  %4 = load i32, i32* %collisionFilterMask.addr, align 4
  %5 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call = call i32 @_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PviiP12btDispatcher(%class.btAxisSweep3Internal.9* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i8* %2, i32 %3, i32 %4, %class.btDispatcher* %5)
  store i32 %call, i32* %handleId, align 4
  %6 = load i32, i32* %handleId, align 4
  %call2 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %6)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %7, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %8 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %11 = load i32, i32* %shapeType.addr, align 4
  %12 = load i8*, i8** %userPtr.addr, align 4
  %13 = load i32, i32* %collisionFilterGroup.addr, align 4
  %14 = load i32, i32* %collisionFilterMask.addr, align 4
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %16 = bitcast %struct.btDbvtBroadphase* %8 to %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)***
  %vtable = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)**, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*** %16, align 4
  %vfn = getelementptr inbounds %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)** %vtable, i64 2
  %17 = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)** %vfn, align 4
  %call4 = call %struct.btBroadphaseProxy* %17(%struct.btDbvtBroadphase* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, i32 %11, i8* %12, i32 %13, i32 %14, %class.btDispatcher* %15)
  store %struct.btBroadphaseProxy* %call4, %struct.btBroadphaseProxy** %rayProxy, align 4
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %rayProxy, align 4
  %19 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %19, i32 0, i32 3
  store %struct.btBroadphaseProxy* %18, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20 to %struct.btBroadphaseProxy*
  ret %struct.btBroadphaseProxy* %21
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btAxisSweep3Internal.9* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %2 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %3 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %4, i32 0, i32 3
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %7 = bitcast %struct.btDbvtBroadphase* %3 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %7, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %8 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %3, %struct.btBroadphaseProxy* %5, %class.btDispatcher* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %10 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %9 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 3
  %11 = load i32, i32* %m_uniqueId, align 4
  %12 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher(%class.btAxisSweep3Internal.9* %this1, i32 %11, %class.btDispatcher* %12)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher(%class.btAxisSweep3Internal.9* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %3 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = bitcast %class.btVector3* %m_aabbMin to i8*
  %6 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 5
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  %11 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %12 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %16 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %17 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal.9* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btDispatcher* %17)
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %18 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %18, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20, i32 0, i32 3
  %21 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4
  %22 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %23 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %24 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %25 = bitcast %struct.btDbvtBroadphase* %19 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %25, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable, i64 4
  %26 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn, align 4
  call void %26(%struct.btDbvtBroadphase* %19, %struct.btBroadphaseProxy* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btDispatcher* %24)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_(%class.btAxisSweep3Internal.9* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %2 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %3 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %2 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %7 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 5
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_(%class.btAxisSweep3Internal.9* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %axis = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %7 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %7, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %8 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  br label %if.end13

if.else:                                          ; preds = %entry
  store i32 0, i32* %axis, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %9 = load i32, i32* %i, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %10 = load i32, i32* %m_numHandles, align 4
  %mul = mul i32 %10, 2
  %add = add i32 %mul, 1
  %cmp = icmp ult i32 %9, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %11 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %11
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %12, i32 %13
  %call = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx3)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %for.body
  %14 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %15 = bitcast %struct.btBroadphaseRayCallback* %14 to %struct.btBroadphaseAabbCallback*
  %m_pEdges6 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %16 = load i32, i32* %axis, align 4
  %arrayidx7 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges6, i32 0, i32 %16
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx7, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %17, i32 %18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx8, i32 0, i32 1
  %19 = load i32, i32* %m_handle, align 4
  %call9 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %19)
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call9 to %struct.btBroadphaseProxy*
  %21 = bitcast %struct.btBroadphaseAabbCallback* %15 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable10 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %21, align 4
  %vfn11 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable10, i64 2
  %22 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn11, align 4
  %call12 = call zeroext i1 %22(%struct.btBroadphaseAabbCallback* %15, %struct.btBroadphaseProxy* %20)
  br label %if.end

if.end:                                           ; preds = %if.then5, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc = add i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end13

if.end13:                                         ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback(%class.btAxisSweep3Internal.9* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %axis = alloca i32, align 4
  %i = alloca i32, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %5 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*** %5, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vtable, i64 7
  %6 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vfn, align 4
  call void %6(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %4)
  br label %if.end16

if.else:                                          ; preds = %entry
  store i32 0, i32* %axis, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %7 = load i32, i32* %i, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %8 = load i32, i32* %m_numHandles, align 4
  %mul = mul i32 %8, 2
  %add = add i32 %mul, 1
  %cmp = icmp ult i32 %7, %add
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %9 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %9
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %10, i32 %11
  %call = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx3)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end15

if.then5:                                         ; preds = %for.body
  %m_pEdges6 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %12 = load i32, i32* %axis, align 4
  %arrayidx7 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges6, i32 0, i32 %12
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx7, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 %14
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx8, i32 0, i32 1
  %15 = load i32, i32* %m_handle, align 4
  %call9 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %15)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call9, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %19 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %19, i32 0, i32 4
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 5
  %call10 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then5
  %22 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4
  %24 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %23 to %struct.btBroadphaseProxy*
  %25 = bitcast %struct.btBroadphaseAabbCallback* %22 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable12 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %25, align 4
  %vfn13 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable12, i64 2
  %26 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn13, align 4
  %call14 = call zeroext i1 %26(%struct.btBroadphaseAabbCallback* %22, %struct.btBroadphaseProxy* %24)
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then5
  br label %if.end15

if.end15:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end15
  %27 = load i32, i32* %i, align 4
  %inc = add i32 %27, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.end16:                                         ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher(%class.btAxisSweep3Internal.9* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.10*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp7 = alloca %struct.btBroadphasePair, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp33 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp37 = alloca %struct.btBroadphasePair, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %1 = bitcast %class.btOverlappingPairCache* %0 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %2 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %2(%class.btOverlappingPairCache* %0)
  br i1 %call, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  %m_pairCache2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)***
  %vtable3 = load %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*** %4, align 4
  %vfn4 = getelementptr inbounds %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)** %vtable3, i64 7
  %5 = load %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.10* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* %5(%class.btOverlappingPairCache* %3)
  store %class.btAlignedObjectArray.10* %call5, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %6 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.10* %6, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %7 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %8 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %8)
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  %9 = load i32, i32* %m_invalidPair, align 4
  %sub = sub nsw i32 %call6, %9
  %call8 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp7)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.10* %7, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %m_invalidPair9 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair9, align 4
  %call10 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %10 = load i32, i32* %i, align 4
  %11 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %11)
  %cmp = icmp slt i32 %10, %call11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %13 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %12, i32 %13)
  store %struct.btBroadphasePair* %call12, %struct.btBroadphasePair** %pair, align 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %call13 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %14, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call13 to i8
  store i8 %frombool, i8* %isDuplicate, align 1
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %16 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %17 = bitcast %struct.btBroadphasePair* %15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  store i8 0, i8* %needsRemoval, align 1
  %18 = load i8, i8* %isDuplicate, align 1
  %tobool = trunc i8 %18 to i1
  br i1 %tobool, label %if.else21, label %if.then14

if.then14:                                        ; preds = %for.body
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy015 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 0
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy015, align 4
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy116 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %21, i32 0, i32 1
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy116, align 4
  %call17 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal.9* %this1, %struct.btBroadphaseProxy* %20, %struct.btBroadphaseProxy* %22)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %hasOverlap, align 1
  %23 = load i8, i8* %hasOverlap, align 1
  %tobool19 = trunc i8 %23 to i1
  br i1 %tobool19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.then14
  store i8 0, i8* %needsRemoval, align 1
  br label %if.end

if.else:                                          ; preds = %if.then14
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then20
  br label %if.end22

if.else21:                                        ; preds = %for.body
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end22

if.end22:                                         ; preds = %if.else21, %if.end
  %24 = load i8, i8* %needsRemoval, align 1
  %tobool23 = trunc i8 %24 to i1
  br i1 %tobool23, label %if.then24, label %if.end31

if.then24:                                        ; preds = %if.end22
  %m_pairCache25 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %25 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %27 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %28 = bitcast %class.btOverlappingPairCache* %25 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable26 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %28, align 4
  %vfn27 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable26, i64 8
  %29 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn27, align 4
  call void %29(%class.btOverlappingPairCache* %25, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %26, %class.btDispatcher* %27)
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy028 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy028, align 4
  %31 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy129 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %31, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy129, align 4
  %m_invalidPair30 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  %32 = load i32, i32* %m_invalidPair30, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %m_invalidPair30, align 4
  %33 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %33, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then24, %if.end22
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %34 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %34, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.10* %35, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp33)
  %36 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %37 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %overlappingPairArray, align 4
  %call34 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %37)
  %m_invalidPair35 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  %38 = load i32, i32* %m_invalidPair35, align 4
  %sub36 = sub nsw i32 %call34, %38
  %call38 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp37)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.10* %36, i32 %sub36, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %m_invalidPair39 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair39, align 4
  br label %if.end40

if.end40:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal.9* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal.9* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_(%class.btAxisSweep3Internal.9* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_worldAabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_worldAabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher(%class.btAxisSweep3Internal.9* %this, %class.btDispatcher* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_numHandles, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  store i32 1, i32* %m_firstFreeHandle, align 4
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  %2 = load i32, i32* %m_firstFreeHandle2, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 7
  %4 = load i32, i32* %m_maxHandles, align 4
  %cmp3 = icmp ult i32 %3, %4
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %5, i32 %6
  %7 = load i32, i32* %i, align 4
  %add = add i32 %7, 1
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 %add)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles4 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles4, align 4
  %m_maxHandles5 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 7
  %10 = load i32, i32* %m_maxHandles5, align 4
  %sub = sub i32 %10, 1
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %9, i32 %sub
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx6, i32 0)
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE10printStatsEv(%class.btAxisSweep3Internal.9* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %0 = bitcast %class.btNullPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #9
  %1 = bitcast %class.btNullPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV15btNullPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray.10* %m_overlappingPairArray)
  ret %class.btNullPairCache* %this1
}

declare %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* returned, %class.btOverlappingPairCache*) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %div = fdiv float %1, %3
  store float %div, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %div8 = fdiv float %5, %7
  store float %div8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %div14 = fdiv float %9, %11
  store float %div14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAxisSweep3InternalItE6HandlenaEm(i32 %sizeInBytes) #2 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4
  %0 = load i32, i32* %sizeInBytes.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZN20btAxisSweep3InternalItE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned short>::Handle"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, i16 zeroext %next) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %next.addr = alloca i16, align 2
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  store i16 %next, i16* %next.addr, align 2
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %0 = load i16, i16* %next.addr, align 2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 0
  store i16 %0, i16* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItED2Ev(%class.btAxisSweep3Internal* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btAxisSweep3Internal* %this1, %class.btAxisSweep3Internal** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalItE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache, align 4
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #9
  %m_nullPairCache2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache2, align 4
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4
  %8 = bitcast %struct.btDbvtBroadphase* %7 to %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)***
  %vtable4 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)**, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*** %8, align 4
  %vfn5 = getelementptr inbounds %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vtable4, i64 0
  %9 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vfn5, align 4
  %call6 = call %struct.btDbvtBroadphase* %9(%struct.btDbvtBroadphase* %7) #9
  %m_raycastAccelerator7 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator7, align 4
  %11 = bitcast %struct.btDbvtBroadphase* %10 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %12 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %12, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %13
  %14 = load i8*, i8** %arrayidx, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %14)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %16 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %isnull = icmp eq %"class.btAxisSweep3Internal<unsigned short>::Handle"* %16, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.end
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %16 to i8*
  call void @_ZN20btAxisSweep3InternalItE6HandledaEPv(i8* %17) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.end
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  %18 = load i8, i8* %m_ownsPairCache, align 4
  %tobool8 = trunc i8 %18 to i1
  br i1 %tobool8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %delete.end
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %19 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %20 = bitcast %class.btOverlappingPairCache* %19 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable10 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %20, align 4
  %vfn11 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable10, i64 0
  %21 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn11, align 4
  %call12 = call %class.btOverlappingPairCache* %21(%class.btOverlappingPairCache* %19) #9
  %m_pairCache13 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %22 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache13, align 4
  %23 = bitcast %class.btOverlappingPairCache* %22 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %23)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %delete.end
  %24 = bitcast %class.btAxisSweep3Internal* %this1 to %class.btBroadphaseInterface*
  %call15 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %24) #9
  %25 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %retval, align 4
  ret %class.btAxisSweep3Internal* %25
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItED0Ev(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %call = call %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItED2Ev(%class.btAxisSweep3Internal* %this1) #9
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* %0) #9
  %1 = bitcast %class.btOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV22btOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btOverlappingPairCache* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btNullPairCache* @_ZN15btNullPairCacheD2Ev(%class.btNullPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %0 = bitcast %class.btNullPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV15btNullPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray.10* %m_overlappingPairArray) #9
  %1 = bitcast %class.btNullPairCache* %this1 to %class.btOverlappingPairCache*
  %call2 = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheD2Ev(%class.btOverlappingPairCache* %1) #9
  ret %class.btNullPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCacheD0Ev(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %call = call %class.btNullPairCache* @_ZN15btNullPairCacheD2Ev(%class.btNullPairCache* %this1) #9
  %0 = bitcast %class.btNullPairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret %struct.btBroadphasePair* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1, %class.btDispatcher* %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  %.addr2 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4
  store %class.btDispatcher* %2, %class.btDispatcher** %.addr2, align 4
  %this3 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i8* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache26getOverlappingPairArrayPtrEv(%class.btNullPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv(%class.btNullPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN15btNullPairCache23getOverlappingPairArrayEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.10* %m_overlappingPairArray
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphasePair*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphasePair* %0, %struct.btBroadphasePair** %.addr, align 4
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btNullPairCache22getNumOverlappingPairsEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btNullPairCache* %this, %struct.btOverlapFilterCallback* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btNullPairCache* %this, %struct.btOverlapCallback* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btOverlapCallback*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btOverlapCallback* %0, %struct.btOverlapCallback** %.addr, align 4
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret %struct.btBroadphasePair* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN15btNullPairCache18hasDeferredRemovalEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btNullPairCache* %this, %class.btOverlappingPairCallback* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher(%class.btNullPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV25btOverlappingPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheD2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* %0) #9
  ret %class.btOverlappingPairCache* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btOverlappingPairCacheD0Ev(%class.btOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btOverlappingPairCallbackD0Ev(%class.btOverlappingPairCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator.11* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  ret %class.btAlignedAllocator.11* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.10* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.11* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.11* %this, %struct.btBroadphasePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE6HandledaEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAxisSweep3InternalIjE6HandlenaEm(i32 %sizeInBytes) #2 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4
  %0 = load i32, i32* %sizeInBytes.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZN20btAxisSweep3InternalIjE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned int>::Handle"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, i32 %next) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %next.addr = alloca i32, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  store i32 %next, i32* %next.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %0 = load i32, i32* %next.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 0
  store i32 %0, i32* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAxisSweep3Internal.9* @_ZN20btAxisSweep3InternalIjED2Ev(%class.btAxisSweep3Internal.9* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal.9*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btAxisSweep3Internal.9* %this1, %class.btAxisSweep3Internal.9** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal.9* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalIjE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4
  %tobool = icmp ne %struct.btDbvtBroadphase* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 17
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache, align 4
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #9
  %m_nullPairCache2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 17
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache2, align 4
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4
  %8 = bitcast %struct.btDbvtBroadphase* %7 to %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)***
  %vtable4 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)**, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*** %8, align 4
  %vfn5 = getelementptr inbounds %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vtable4, i64 0
  %9 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vfn5, align 4
  %call6 = call %struct.btDbvtBroadphase* %9(%struct.btDbvtBroadphase* %7) #9
  %m_raycastAccelerator7 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator7, align 4
  %11 = bitcast %struct.btDbvtBroadphase* %10 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %12 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %12, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 11
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %13
  %14 = load i8*, i8** %arrayidx, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %14)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %16 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %isnull = icmp eq %"class.btAxisSweep3Internal<unsigned int>::Handle"* %16, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.end
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %16 to i8*
  call void @_ZN20btAxisSweep3InternalIjE6HandledaEPv(i8* %17) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.end
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 14
  %18 = load i8, i8* %m_ownsPairCache, align 4
  %tobool8 = trunc i8 %18 to i1
  br i1 %tobool8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %delete.end
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %19 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %20 = bitcast %class.btOverlappingPairCache* %19 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable10 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %20, align 4
  %vfn11 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable10, i64 0
  %21 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn11, align 4
  %call12 = call %class.btOverlappingPairCache* %21(%class.btOverlappingPairCache* %19) #9
  %m_pairCache13 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %22 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache13, align 4
  %23 = bitcast %class.btOverlappingPairCache* %22 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %23)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %delete.end
  %24 = bitcast %class.btAxisSweep3Internal.9* %this1 to %class.btBroadphaseInterface*
  %call15 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %24) #9
  %25 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %retval, align 4
  ret %class.btAxisSweep3Internal.9* %25
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjED0Ev(%class.btAxisSweep3Internal.9* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %call = call %class.btAxisSweep3Internal.9* @_ZN20btAxisSweep3InternalIjED2Ev(%class.btAxisSweep3Internal.9* %this1) #9
  %0 = bitcast %class.btAxisSweep3Internal.9* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE6HandledaEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i16 @_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PviiP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %pOwner, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pOwner.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %min = alloca [3 x i16], align 2
  %max = alloca [3 x i16], align 2
  %handle = alloca i16, align 2
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %limit = alloca i16, align 2
  %axis = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i8* %pOwner, i8** %pOwner.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %0, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 1)
  %call = call zeroext i16 @_ZN20btAxisSweep3InternalItE11allocHandleEv(%class.btAxisSweep3Internal* %this1)
  store i16 %call, i16* %handle, align 2
  %2 = load i16, i16* %handle, align 2
  %call3 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %2)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call3, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %3 = load i16, i16* %handle, align 2
  %conv = zext i16 %3 to i32
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %4 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 3
  store i32 %conv, i32* %m_uniqueId, align 4
  %6 = load i8*, i8** %pOwner.addr, align 4
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %7 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 0
  store i8* %6, i8** %m_clientObject, align 4
  %9 = load i32, i32* %collisionFilterGroup.addr, align 4
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %11 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %10 to %struct.btBroadphaseProxy*
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  store i32 %9, i32* %m_collisionFilterGroup, align 4
  %12 = load i32, i32* %collisionFilterMask.addr, align 4
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %13 to %struct.btBroadphaseProxy*
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 2
  store i32 %12, i32* %m_collisionFilterMask, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %15 = load i16, i16* %m_numHandles, align 4
  %conv4 = zext i16 %15 to i32
  %mul = mul nsw i32 %conv4, 2
  %conv5 = trunc i32 %mul to i16
  store i16 %conv5, i16* %limit, align 2
  store i16 0, i16* %axis, align 2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i16, i16* %axis, align 2
  %conv6 = zext i16 %16 to i32
  %cmp = icmp slt i32 %conv6, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %17, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i32 0, i32 2
  %18 = load i16, i16* %axis, align 2
  %idxprom = zext i16 %18 to i32
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %idxprom
  %19 = load i16, i16* %arrayidx7, align 2
  %conv8 = zext i16 %19 to i32
  %add = add nsw i32 %conv8, 2
  %conv9 = trunc i32 %add to i16
  store i16 %conv9, i16* %arrayidx7, align 2
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %20 = load i16, i16* %axis, align 2
  %idxprom10 = zext i16 %20 to i32
  %arrayidx11 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom10
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx11, align 4
  %22 = load i16, i16* %limit, align 2
  %conv12 = zext i16 %22 to i32
  %sub = sub nsw i32 %conv12, 1
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %21, i32 %sub
  %m_pEdges14 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %23 = load i16, i16* %axis, align 2
  %idxprom15 = zext i16 %23 to i32
  %arrayidx16 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges14, i32 0, i32 %idxprom15
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx16, align 4
  %25 = load i16, i16* %limit, align 2
  %conv17 = zext i16 %25 to i32
  %add18 = add nsw i32 %conv17, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %24, i32 %add18
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx19 to i8*
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %26, i8* align 2 %27, i32 4, i1 false)
  %28 = load i16, i16* %axis, align 2
  %idxprom20 = zext i16 %28 to i32
  %arrayidx21 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %idxprom20
  %29 = load i16, i16* %arrayidx21, align 2
  %m_pEdges22 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %30 = load i16, i16* %axis, align 2
  %idxprom23 = zext i16 %30 to i32
  %arrayidx24 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges22, i32 0, i32 %idxprom23
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx24, align 4
  %32 = load i16, i16* %limit, align 2
  %conv25 = zext i16 %32 to i32
  %sub26 = sub nsw i32 %conv25, 1
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %31, i32 %sub26
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx27, i32 0, i32 0
  store i16 %29, i16* %m_pos, align 2
  %33 = load i16, i16* %handle, align 2
  %m_pEdges28 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %34 = load i16, i16* %axis, align 2
  %idxprom29 = zext i16 %34 to i32
  %arrayidx30 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges28, i32 0, i32 %idxprom29
  %35 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx30, align 4
  %36 = load i16, i16* %limit, align 2
  %conv31 = zext i16 %36 to i32
  %sub32 = sub nsw i32 %conv31, 1
  %arrayidx33 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %35, i32 %sub32
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx33, i32 0, i32 1
  store i16 %33, i16* %m_handle, align 2
  %37 = load i16, i16* %axis, align 2
  %idxprom34 = zext i16 %37 to i32
  %arrayidx35 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %idxprom34
  %38 = load i16, i16* %arrayidx35, align 2
  %m_pEdges36 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %39 = load i16, i16* %axis, align 2
  %idxprom37 = zext i16 %39 to i32
  %arrayidx38 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges36, i32 0, i32 %idxprom37
  %40 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx38, align 4
  %41 = load i16, i16* %limit, align 2
  %idxprom39 = zext i16 %41 to i32
  %arrayidx40 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %40, i32 %idxprom39
  %m_pos41 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx40, i32 0, i32 0
  store i16 %38, i16* %m_pos41, align 2
  %42 = load i16, i16* %handle, align 2
  %m_pEdges42 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %43 = load i16, i16* %axis, align 2
  %idxprom43 = zext i16 %43 to i32
  %arrayidx44 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges42, i32 0, i32 %idxprom43
  %44 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx44, align 4
  %45 = load i16, i16* %limit, align 2
  %idxprom45 = zext i16 %45 to i32
  %arrayidx46 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %44, i32 %idxprom45
  %m_handle47 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx46, i32 0, i32 1
  store i16 %42, i16* %m_handle47, align 2
  %46 = load i16, i16* %limit, align 2
  %conv48 = zext i16 %46 to i32
  %sub49 = sub nsw i32 %conv48, 1
  %conv50 = trunc i32 %sub49 to i16
  %47 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %47, i32 0, i32 1
  %48 = load i16, i16* %axis, align 2
  %idxprom51 = zext i16 %48 to i32
  %arrayidx52 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %idxprom51
  store i16 %conv50, i16* %arrayidx52, align 2
  %49 = load i16, i16* %limit, align 2
  %50 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges53 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %50, i32 0, i32 2
  %51 = load i16, i16* %axis, align 2
  %idxprom54 = zext i16 %51 to i32
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges53, i32 0, i32 %idxprom54
  store i16 %49, i16* %arrayidx55, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i16, i16* %axis, align 2
  %inc = add i16 %52, 1
  store i16 %inc, i16* %axis, align 2
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges56 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %53, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges56, i32 0, i32 0
  %54 = load i16, i16* %arrayidx57, align 4
  %55 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 0, i16 zeroext %54, %class.btDispatcher* %55, i1 zeroext false)
  %56 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges58 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %56, i32 0, i32 2
  %arrayidx59 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges58, i32 0, i32 0
  %57 = load i16, i16* %arrayidx59, align 2
  %58 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 0, i16 zeroext %57, %class.btDispatcher* %58, i1 zeroext false)
  %59 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges60 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %59, i32 0, i32 1
  %arrayidx61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges60, i32 0, i32 1
  %60 = load i16, i16* %arrayidx61, align 2
  %61 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 1, i16 zeroext %60, %class.btDispatcher* %61, i1 zeroext false)
  %62 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges62 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %62, i32 0, i32 2
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges62, i32 0, i32 1
  %63 = load i16, i16* %arrayidx63, align 2
  %64 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 1, i16 zeroext %63, %class.btDispatcher* %64, i1 zeroext false)
  %65 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges64 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %65, i32 0, i32 1
  %arrayidx65 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges64, i32 0, i32 2
  %66 = load i16, i16* %arrayidx65, align 4
  %67 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 2, i16 zeroext %66, %class.btDispatcher* %67, i1 zeroext true)
  %68 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges66 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %68, i32 0, i32 2
  %arrayidx67 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges66, i32 0, i32 2
  %69 = load i16, i16* %arrayidx67, align 2
  %70 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 2, i16 zeroext %69, %class.btDispatcher* %70, i1 zeroext true)
  %71 = load i16, i16* %handle, align 2
  ret i16 %71
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this, i16 zeroext %index) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %index.addr = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i16 %index, i16* %index.addr, align 2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %0 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %1 = load i16, i16* %index.addr, align 2
  %conv = zext i16 %1 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %0, i32 %conv
  ret %"class.btAxisSweep3Internal<unsigned short>::Handle"* %add.ptr
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i16* %out, i16** %out.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store i32 %isMax, i32* %isMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_quantize)
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  %cmp = fcmp ole float %1, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %isMax.addr, align 4
  %conv = trunc i32 %2 to i16
  br label %cond.end22

cond.false:                                       ; preds = %entry
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx3, align 4
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %4 = load i16, i16* %m_handleSentinel, align 2
  %conv4 = zext i16 %4 to i32
  %conv5 = sitofp i32 %conv4 to float
  %cmp6 = fcmp oge float %3, %conv5
  br i1 %cmp6, label %cond.true7, label %cond.false12

cond.true7:                                       ; preds = %cond.false
  %m_handleSentinel8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %5 = load i16, i16* %m_handleSentinel8, align 2
  %conv9 = zext i16 %5 to i32
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %6 = load i16, i16* %m_bpHandleMask, align 4
  %conv10 = zext i16 %6 to i32
  %and = and i32 %conv9, %conv10
  %7 = load i32, i32* %isMax.addr, align 4
  %or = or i32 %and, %7
  %conv11 = trunc i32 %or to i16
  br label %cond.end

cond.false12:                                     ; preds = %cond.false
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 0
  %8 = load float, float* %arrayidx14, align 4
  %conv15 = fptoui float %8 to i16
  %conv16 = zext i16 %conv15 to i32
  %m_bpHandleMask17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %9 = load i16, i16* %m_bpHandleMask17, align 4
  %conv18 = zext i16 %9 to i32
  %and19 = and i32 %conv16, %conv18
  %10 = load i32, i32* %isMax.addr, align 4
  %or20 = or i32 %and19, %10
  %conv21 = trunc i32 %or20 to i16
  br label %cond.end

cond.end:                                         ; preds = %cond.false12, %cond.true7
  %cond = phi i16 [ %conv11, %cond.true7 ], [ %conv21, %cond.false12 ]
  br label %cond.end22

cond.end22:                                       ; preds = %cond.end, %cond.true
  %cond23 = phi i16 [ %conv, %cond.true ], [ %cond, %cond.end ]
  %11 = load i16*, i16** %out.addr, align 4
  %arrayidx24 = getelementptr inbounds i16, i16* %11, i32 0
  store i16 %cond23, i16* %arrayidx24, align 2
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %12 = load float, float* %arrayidx26, align 4
  %cmp27 = fcmp ole float %12, 0.000000e+00
  br i1 %cmp27, label %cond.true28, label %cond.false30

cond.true28:                                      ; preds = %cond.end22
  %13 = load i32, i32* %isMax.addr, align 4
  %conv29 = trunc i32 %13 to i16
  br label %cond.end57

cond.false30:                                     ; preds = %cond.end22
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %14 = load float, float* %arrayidx32, align 4
  %m_handleSentinel33 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %15 = load i16, i16* %m_handleSentinel33, align 2
  %conv34 = zext i16 %15 to i32
  %conv35 = sitofp i32 %conv34 to float
  %cmp36 = fcmp oge float %14, %conv35
  br i1 %cmp36, label %cond.true37, label %cond.false45

cond.true37:                                      ; preds = %cond.false30
  %m_handleSentinel38 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %16 = load i16, i16* %m_handleSentinel38, align 2
  %conv39 = zext i16 %16 to i32
  %m_bpHandleMask40 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %17 = load i16, i16* %m_bpHandleMask40, align 4
  %conv41 = zext i16 %17 to i32
  %and42 = and i32 %conv39, %conv41
  %18 = load i32, i32* %isMax.addr, align 4
  %or43 = or i32 %and42, %18
  %conv44 = trunc i32 %or43 to i16
  br label %cond.end55

cond.false45:                                     ; preds = %cond.false30
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 1
  %19 = load float, float* %arrayidx47, align 4
  %conv48 = fptoui float %19 to i16
  %conv49 = zext i16 %conv48 to i32
  %m_bpHandleMask50 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %20 = load i16, i16* %m_bpHandleMask50, align 4
  %conv51 = zext i16 %20 to i32
  %and52 = and i32 %conv49, %conv51
  %21 = load i32, i32* %isMax.addr, align 4
  %or53 = or i32 %and52, %21
  %conv54 = trunc i32 %or53 to i16
  br label %cond.end55

cond.end55:                                       ; preds = %cond.false45, %cond.true37
  %cond56 = phi i16 [ %conv44, %cond.true37 ], [ %conv54, %cond.false45 ]
  br label %cond.end57

cond.end57:                                       ; preds = %cond.end55, %cond.true28
  %cond58 = phi i16 [ %conv29, %cond.true28 ], [ %cond56, %cond.end55 ]
  %22 = load i16*, i16** %out.addr, align 4
  %arrayidx59 = getelementptr inbounds i16, i16* %22, i32 1
  store i16 %cond58, i16* %arrayidx59, align 2
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %23 = load float, float* %arrayidx61, align 4
  %cmp62 = fcmp ole float %23, 0.000000e+00
  br i1 %cmp62, label %cond.true63, label %cond.false65

cond.true63:                                      ; preds = %cond.end57
  %24 = load i32, i32* %isMax.addr, align 4
  %conv64 = trunc i32 %24 to i16
  br label %cond.end92

cond.false65:                                     ; preds = %cond.end57
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %25 = load float, float* %arrayidx67, align 4
  %m_handleSentinel68 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %26 = load i16, i16* %m_handleSentinel68, align 2
  %conv69 = zext i16 %26 to i32
  %conv70 = sitofp i32 %conv69 to float
  %cmp71 = fcmp oge float %25, %conv70
  br i1 %cmp71, label %cond.true72, label %cond.false80

cond.true72:                                      ; preds = %cond.false65
  %m_handleSentinel73 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %27 = load i16, i16* %m_handleSentinel73, align 2
  %conv74 = zext i16 %27 to i32
  %m_bpHandleMask75 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %28 = load i16, i16* %m_bpHandleMask75, align 4
  %conv76 = zext i16 %28 to i32
  %and77 = and i32 %conv74, %conv76
  %29 = load i32, i32* %isMax.addr, align 4
  %or78 = or i32 %and77, %29
  %conv79 = trunc i32 %or78 to i16
  br label %cond.end90

cond.false80:                                     ; preds = %cond.false65
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %30 = load float, float* %arrayidx82, align 4
  %conv83 = fptoui float %30 to i16
  %conv84 = zext i16 %conv83 to i32
  %m_bpHandleMask85 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %31 = load i16, i16* %m_bpHandleMask85, align 4
  %conv86 = zext i16 %31 to i32
  %and87 = and i32 %conv84, %conv86
  %32 = load i32, i32* %isMax.addr, align 4
  %or88 = or i32 %and87, %32
  %conv89 = trunc i32 %or88 to i16
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false80, %cond.true72
  %cond91 = phi i16 [ %conv79, %cond.true72 ], [ %conv89, %cond.false80 ]
  br label %cond.end92

cond.end92:                                       ; preds = %cond.end90, %cond.true63
  %cond93 = phi i16 [ %conv64, %cond.true63 ], [ %cond91, %cond.end90 ]
  %33 = load i16*, i16** %out.addr, align 4
  %arrayidx94 = getelementptr inbounds i16, i16* %33, i32 2
  store i16 %cond93, i16* %arrayidx94, align 2
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i16 @_ZN20btAxisSweep3InternalItE11allocHandleEv(%class.btAxisSweep3Internal* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %0 = load i16, i16* %m_firstFreeHandle, align 4
  store i16 %0, i16* %handle, align 2
  %1 = load i16, i16* %handle, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %1)
  %call2 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %call)
  %m_firstFreeHandle3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 %call2, i16* %m_firstFreeHandle3, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %2 = load i16, i16* %m_numHandles, align 4
  %inc = add i16 %2, 1
  store i16 %inc, i16* %m_numHandles, align 4
  %3 = load i16, i16* %handle, align 2
  ret i16 %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i16 %edge, i16* %edge.addr, align 2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %3 = load i16, i16* %edge.addr, align 2
  %conv = zext i16 %3 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %2, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %4, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %5, i32 0, i32 1
  %6 = load i16, i16* %m_handle, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %6)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end25, %entry
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %7, i32 0, i32 0
  %8 = load i16, i16* %m_pos, align 2
  %conv3 = zext i16 %8 to i32
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %9, i32 0, i32 0
  %10 = load i16, i16* %m_pos4, align 2
  %conv5 = zext i16 %10 to i32
  %cmp = icmp slt i32 %conv3, %conv5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %m_handle6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %11, i32 0, i32 1
  %12 = load i16, i16* %m_handle6, align 2
  %call7 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %12)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call7, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %call8 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %13)
  %tobool = icmp ne i16 %call8, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %14 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %14
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %15 = load i32, i32* %axis1, align 4
  %shl9 = shl i32 1, %15
  %and10 = and i32 %shl9, 3
  store i32 %and10, i32* %axis2, align 4
  %16 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool11 = trunc i8 %16 to i1
  br i1 %tobool11, label %land.lhs.true, label %if.end21

land.lhs.true:                                    ; preds = %if.then
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %19 = load i32, i32* %axis1, align 4
  %20 = load i32, i32* %axis2, align 4
  %call12 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %17, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18, i32 %19, i32 %20)
  br i1 %call12, label %if.then13, label %if.end21

if.then13:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %21 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %22 = bitcast %class.btOverlappingPairCache* %21 to %class.btOverlappingPairCallback*
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %24 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %23 to %struct.btBroadphaseProxy*
  %25 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %25 to %struct.btBroadphaseProxy*
  %27 = bitcast %class.btOverlappingPairCallback* %22 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %27, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %28 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call14 = call %struct.btBroadphasePair* %28(%class.btOverlappingPairCallback* %22, %struct.btBroadphaseProxy* %24, %struct.btBroadphaseProxy* %26)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %29 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool15 = icmp ne %class.btOverlappingPairCallback* %29, null
  br i1 %tobool15, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.then13
  %m_userPairCallback17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %30 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback17, align 4
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %34 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %33 to %struct.btBroadphaseProxy*
  %35 = bitcast %class.btOverlappingPairCallback* %30 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable18 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %35, align 4
  %vfn19 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable18, i64 2
  %36 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn19, align 4
  %call20 = call %struct.btBroadphasePair* %36(%class.btOverlappingPairCallback* %30, %struct.btBroadphaseProxy* %32, %struct.btBroadphaseProxy* %34)
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then13
  br label %if.end21

if.end21:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %37 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %37, i32 0, i32 2
  %38 = load i32, i32* %axis.addr, align 4
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %38
  %39 = load i16, i16* %arrayidx22, align 2
  %inc = add i16 %39, 1
  store i16 %inc, i16* %arrayidx22, align 2
  br label %if.end25

if.else:                                          ; preds = %while.body
  %40 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %40, i32 0, i32 1
  %41 = load i32, i32* %axis.addr, align 4
  %arrayidx23 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %41
  %42 = load i16, i16* %arrayidx23, align 2
  %inc24 = add i16 %42, 1
  store i16 %inc24, i16* %arrayidx23, align 2
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.end21
  %43 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %m_minEdges26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %43, i32 0, i32 1
  %44 = load i32, i32* %axis.addr, align 4
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges26, i32 0, i32 %44
  %45 = load i16, i16* %arrayidx27, align 2
  %dec = add i16 %45, -1
  store i16 %dec, i16* %arrayidx27, align 2
  %46 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %48 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %47, i8* align 2 %48, i32 4, i1 false)
  %49 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %50 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %51 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %50 to i8*
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %51, i8* align 2 %52, i32 4, i1 false)
  %53 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %53 to i8*
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %54, i8* align 2 %55, i32 4, i1 false)
  %56 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %56, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %incdec.ptr28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %57, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr28, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i16 %edge, i16* %edge.addr, align 2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %0 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %0
  %1 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %2 = load i16, i16* %edge.addr, align 2
  %conv = zext i16 %2 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %1, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %3, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %4, i32 0, i32 1
  %5 = load i16, i16* %m_handle, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %5)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %6, i32 0, i32 0
  %7 = load i16, i16* %m_pos, align 2
  %conv3 = zext i16 %7 to i32
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %8, i32 0, i32 0
  %9 = load i16, i16* %m_pos4, align 2
  %conv5 = zext i16 %9 to i32
  %cmp = icmp slt i32 %conv3, %conv5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %m_handle6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %10, i32 0, i32 1
  %11 = load i16, i16* %m_handle6, align 2
  %call7 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %11)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call7, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %call8 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %12)
  %tobool = icmp ne i16 %call8, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 0, i32 1
  %14 = load i16, i16* %m_handle9, align 2
  %call10 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %14)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %15, i32 0, i32 1
  %16 = load i16, i16* %m_handle11, align 2
  %call12 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %16)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %17 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %17
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %18 = load i32, i32* %axis1, align 4
  %shl13 = shl i32 1, %18
  %and14 = and i32 %shl13, 3
  store i32 %and14, i32* %axis2, align 4
  %19 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool15 = trunc i8 %19 to i1
  br i1 %tobool15, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %22 = load i32, i32* %axis1, align 4
  %23 = load i32, i32* %axis2, align 4
  %call16 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %21, i32 %22, i32 %23)
  br i1 %call16, label %if.then17, label %if.end25

if.then17:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %24 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %25 = bitcast %class.btOverlappingPairCache* %24 to %class.btOverlappingPairCallback*
  %26 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %26 to %struct.btBroadphaseProxy*
  %28 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %29 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %28 to %struct.btBroadphaseProxy*
  %30 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %31 = bitcast %class.btOverlappingPairCallback* %25 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %31, align 4
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %32 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call18 = call i8* %32(%class.btOverlappingPairCallback* %25, %struct.btBroadphaseProxy* %27, %struct.btBroadphaseProxy* %29, %class.btDispatcher* %30)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %33 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %33, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then17
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %34 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4
  %35 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %36 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %35 to %struct.btBroadphaseProxy*
  %37 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %40 = bitcast %class.btOverlappingPairCallback* %34 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable22 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %40, align 4
  %vfn23 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable22, i64 3
  %41 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn23, align 4
  %call24 = call i8* %41(%class.btOverlappingPairCallback* %34, %struct.btBroadphaseProxy* %36, %struct.btBroadphaseProxy* %38, %class.btDispatcher* %39)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then17
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %42 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %42, i32 0, i32 1
  %43 = load i32, i32* %axis.addr, align 4
  %arrayidx26 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %43
  %44 = load i16, i16* %arrayidx26, align 2
  %inc = add i16 %44, 1
  store i16 %inc, i16* %arrayidx26, align 2
  br label %if.end29

if.else:                                          ; preds = %while.body
  %45 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %45, i32 0, i32 2
  %46 = load i32, i32* %axis.addr, align 4
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %46
  %47 = load i16, i16* %arrayidx27, align 2
  %inc28 = add i16 %47, 1
  store i16 %inc28, i16* %arrayidx27, align 2
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %48 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %m_maxEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %48, i32 0, i32 2
  %49 = load i32, i32* %axis.addr, align 4
  %arrayidx31 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges30, i32 0, i32 %49
  %50 = load i16, i16* %arrayidx31, align 2
  %dec = add i16 %50, -1
  store i16 %dec, i16* %arrayidx31, align 2
  %51 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %53 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %52, i8* align 2 %53, i32 4, i1 false)
  %54 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %55 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %55 to i8*
  %57 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %56, i8* align 2 %57, i32 4, i1 false)
  %58 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %58 to i8*
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %59, i8* align 2 %60, i32 4, i1 false)
  %61 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %61, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %62 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %62, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 0
  %0 = load i16, i16* %arrayidx, align 4
  ret i16 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %this, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %this.addr, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_pos, align 2
  %conv = zext i16 %0 to i32
  %and = and i32 %conv, 1
  %conv2 = trunc i32 %and to i16
  ret i16 %conv2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleB, i32 %axis0, i32 %axis1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %pHandleA.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleB.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis0.addr = alloca i32, align 4
  %axis1.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleB, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4
  store i32 %axis0, i32* %axis0.addr, align 4
  store i32 %axis1, i32* %axis1.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %0, i32 0, i32 2
  %1 = load i32, i32* %axis0.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %1
  %2 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %2 to i32
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %3, i32 0, i32 1
  %4 = load i32, i32* %axis0.addr, align 4
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %4
  %5 = load i16, i16* %arrayidx2, align 2
  %conv3 = zext i16 %5 to i32
  %cmp = icmp slt i32 %conv, %conv3
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4
  %m_maxEdges4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %6, i32 0, i32 2
  %7 = load i32, i32* %axis0.addr, align 4
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges4, i32 0, i32 %7
  %8 = load i16, i16* %arrayidx5, align 2
  %conv6 = zext i16 %8 to i32
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4
  %m_minEdges7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %9, i32 0, i32 1
  %10 = load i32, i32* %axis0.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges7, i32 0, i32 %10
  %11 = load i16, i16* %arrayidx8, align 2
  %conv9 = zext i16 %11 to i32
  %cmp10 = icmp slt i32 %conv6, %conv9
  br i1 %cmp10, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4
  %m_maxEdges12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %12, i32 0, i32 2
  %13 = load i32, i32* %axis1.addr, align 4
  %arrayidx13 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges12, i32 0, i32 %13
  %14 = load i16, i16* %arrayidx13, align 2
  %conv14 = zext i16 %14 to i32
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4
  %m_minEdges15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %15, i32 0, i32 1
  %16 = load i32, i32* %axis1.addr, align 4
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges15, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx16, align 2
  %conv17 = zext i16 %17 to i32
  %cmp18 = icmp slt i32 %conv14, %conv17
  br i1 %cmp18, label %if.then, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false11
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4
  %m_maxEdges20 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18, i32 0, i32 2
  %19 = load i32, i32* %axis1.addr, align 4
  %arrayidx21 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges20, i32 0, i32 %19
  %20 = load i16, i16* %arrayidx21, align 2
  %conv22 = zext i16 %20 to i32
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4
  %m_minEdges23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %21, i32 0, i32 1
  %22 = load i32, i32* %axis1.addr, align 4
  %arrayidx24 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges23, i32 0, i32 %22
  %23 = load i16, i16* %arrayidx24, align 2
  %conv25 = zext i16 %23 to i32
  %cmp26 = icmp slt i32 %conv22, %conv25
  br i1 %cmp26, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false19, %lor.lhs.false11, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false19
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher(%class.btAxisSweep3Internal* %this, i16 zeroext %handle, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  %pEdges = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %max = alloca i16, align 2
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i16 %handle, i16* %handle.addr, align 2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load i16, i16* %handle.addr, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %0)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %2 = bitcast %class.btOverlappingPairCache* %1 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %2, align 4
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %3 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call zeroext i1 %3(%class.btOverlappingPairCache* %1)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_pairCache3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache3, align 4
  %5 = bitcast %class.btOverlappingPairCache* %4 to %class.btOverlappingPairCallback*
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %6 to %struct.btBroadphaseProxy*
  %8 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %9 = bitcast %class.btOverlappingPairCallback* %5 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable4 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %9, align 4
  %vfn5 = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable4, i64 4
  %10 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn5, align 4
  call void %10(%class.btOverlappingPairCallback* %5, %struct.btBroadphaseProxy* %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %11 = load i16, i16* %m_numHandles, align 4
  %conv = zext i16 %11 to i32
  %mul = mul nsw i32 %conv, 2
  store i32 %mul, i32* %limit, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %12 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %12, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %13, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i32 0, i32 2
  %14 = load i32, i32* %axis, align 4
  %arrayidx6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %14
  %15 = load i16, i16* %arrayidx6, align 2
  %conv7 = zext i16 %15 to i32
  %sub = sub nsw i32 %conv7, 2
  %conv8 = trunc i32 %sub to i16
  store i16 %conv8, i16* %arrayidx6, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %axis, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc27, %for.end
  %17 = load i32, i32* %axis, align 4
  %cmp10 = icmp slt i32 %17, 3
  br i1 %cmp10, label %for.body11, label %for.end29

for.body11:                                       ; preds = %for.cond9
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %18 = load i32, i32* %axis, align 4
  %arrayidx12 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %18
  %19 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx12, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %19, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20, i32 0, i32 2
  %21 = load i32, i32* %axis, align 4
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges13, i32 0, i32 %21
  %22 = load i16, i16* %arrayidx14, align 2
  store i16 %22, i16* %max, align 2
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %23 = load i16, i16* %m_handleSentinel, align 2
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4
  %25 = load i16, i16* %max, align 2
  %idxprom = zext i16 %25 to i32
  %arrayidx15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %24, i32 %idxprom
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx15, i32 0, i32 0
  store i16 %23, i16* %m_pos, align 2
  %26 = load i32, i32* %axis, align 4
  %27 = load i16, i16* %max, align 2
  %28 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %26, i16 zeroext %27, %class.btDispatcher* %28, i1 zeroext false)
  %29 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %29, i32 0, i32 1
  %30 = load i32, i32* %axis, align 4
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %30
  %31 = load i16, i16* %arrayidx16, align 2
  store i16 %31, i16* %i, align 2
  %m_handleSentinel17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %32 = load i16, i16* %m_handleSentinel17, align 2
  %33 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4
  %34 = load i16, i16* %i, align 2
  %idxprom18 = zext i16 %34 to i32
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %33, i32 %idxprom18
  %m_pos20 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx19, i32 0, i32 0
  store i16 %32, i16* %m_pos20, align 2
  %35 = load i32, i32* %axis, align 4
  %36 = load i16, i16* %i, align 2
  %37 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %35, i16 zeroext %36, %class.btDispatcher* %37, i1 zeroext false)
  %38 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4
  %39 = load i32, i32* %limit, align 4
  %sub21 = sub nsw i32 %39, 1
  %arrayidx22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %38, i32 %sub21
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx22, i32 0, i32 1
  store i16 0, i16* %m_handle, align 2
  %m_handleSentinel23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %40 = load i16, i16* %m_handleSentinel23, align 2
  %41 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4
  %42 = load i32, i32* %limit, align 4
  %sub24 = sub nsw i32 %42, 1
  %arrayidx25 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %41, i32 %sub24
  %m_pos26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx25, i32 0, i32 0
  store i16 %40, i16* %m_pos26, align 2
  br label %for.inc27

for.inc27:                                        ; preds = %for.body11
  %43 = load i32, i32* %axis, align 4
  %inc28 = add nsw i32 %43, 1
  store i32 %inc28, i32* %axis, align 4
  br label %for.cond9

for.end29:                                        ; preds = %for.cond9
  %44 = load i16, i16* %handle.addr, align 2
  call void @_ZN20btAxisSweep3InternalItE10freeHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i16 %edge, i16* %edge.addr, align 2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %3 = load i16, i16* %edge.addr, align 2
  %conv = zext i16 %3 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %2, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %4, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %5, i32 0, i32 1
  %6 = load i16, i16* %m_handle, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %6)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end31, %entry
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %7, i32 0, i32 1
  %8 = load i16, i16* %m_handle3, align 2
  %tobool = icmp ne i16 %8, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %9, i32 0, i32 0
  %10 = load i16, i16* %m_pos, align 2
  %conv4 = zext i16 %10 to i32
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_pos5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %11, i32 0, i32 0
  %12 = load i16, i16* %m_pos5, align 2
  %conv6 = zext i16 %12 to i32
  %cmp = icmp sge i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %13 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %13, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %14, i32 0, i32 1
  %15 = load i16, i16* %m_handle7, align 2
  %call8 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %15)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %16 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %16
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %17 = load i32, i32* %axis1, align 4
  %shl9 = shl i32 1, %17
  %and10 = and i32 %shl9, 3
  store i32 %and10, i32* %axis2, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %call11 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %18)
  %tobool12 = icmp ne i16 %call11, 0
  br i1 %tobool12, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %19 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool13 = trunc i8 %19 to i1
  br i1 %tobool13, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.then
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %22 = load i32, i32* %axis1, align 4
  %23 = load i32, i32* %axis2, align 4
  %call14 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %21, i32 %22, i32 %23)
  br i1 %call14, label %if.then15, label %if.end27

if.then15:                                        ; preds = %land.lhs.true
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %24, i32 0, i32 1
  %25 = load i16, i16* %m_handle16, align 2
  %call17 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %25)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call17, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %26 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle18 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %26, i32 0, i32 1
  %27 = load i16, i16* %m_handle18, align 2
  %call19 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %27)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call19, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %28 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %29 = bitcast %class.btOverlappingPairCache* %28 to %class.btOverlappingPairCallback*
  %30 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %31 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %30 to %struct.btBroadphaseProxy*
  %32 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %33 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %32 to %struct.btBroadphaseProxy*
  %34 = bitcast %class.btOverlappingPairCallback* %29 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %34, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %35 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call20 = call %struct.btBroadphasePair* %35(%class.btOverlappingPairCallback* %29, %struct.btBroadphaseProxy* %31, %struct.btBroadphaseProxy* %33)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool21 = icmp ne %class.btOverlappingPairCallback* %36, null
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then15
  %m_userPairCallback23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %37 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback23, align 4
  %38 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = bitcast %class.btOverlappingPairCallback* %37 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable24 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %42, align 4
  %vfn25 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable24, i64 2
  %43 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn25, align 4
  %call26 = call %struct.btBroadphasePair* %43(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41)
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then15
  br label %if.end27

if.end27:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %44 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %44, i32 0, i32 1
  %45 = load i32, i32* %axis.addr, align 4
  %arrayidx28 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %45
  %46 = load i16, i16* %arrayidx28, align 2
  %dec = add i16 %46, -1
  store i16 %dec, i16* %arrayidx28, align 2
  br label %if.end31

if.else:                                          ; preds = %while.body
  %47 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %47, i32 0, i32 2
  %48 = load i32, i32* %axis.addr, align 4
  %arrayidx29 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %48
  %49 = load i16, i16* %arrayidx29, align 2
  %dec30 = add i16 %49, -1
  store i16 %dec30, i16* %arrayidx29, align 2
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.end27
  %50 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %m_maxEdges32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %50, i32 0, i32 2
  %51 = load i32, i32* %axis.addr, align 4
  %arrayidx33 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges32, i32 0, i32 %51
  %52 = load i16, i16* %arrayidx33, align 2
  %inc = add i16 %52, 1
  store i16 %inc, i16* %arrayidx33, align 2
  %53 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %54, i8* align 2 %55, i32 4, i1 false)
  %56 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %58 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %57 to i8*
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %58, i8* align 2 %59, i32 4, i1 false)
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %61 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %60 to i8*
  %62 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %61, i8* align 2 %62, i32 4, i1 false)
  %63 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %63, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %64 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %incdec.ptr34 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %64, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr34, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i16 %edge, i16* %edge.addr, align 2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %0 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %0
  %1 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4
  %2 = load i16, i16* %edge.addr, align 2
  %conv = zext i16 %2 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %1, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %3, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %4, i32 0, i32 1
  %5 = load i16, i16* %m_handle, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %5)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end31, %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %6, i32 0, i32 1
  %7 = load i16, i16* %m_handle3, align 2
  %tobool = icmp ne i16 %7, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %8, i32 0, i32 0
  %9 = load i16, i16* %m_pos, align 2
  %conv4 = zext i16 %9 to i32
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_pos5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %10, i32 0, i32 0
  %11 = load i16, i16* %m_pos5, align 2
  %conv6 = zext i16 %11 to i32
  %cmp = icmp sge i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %12 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %12, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 0, i32 1
  %14 = load i16, i16* %m_handle7, align 2
  %call8 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %14)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %call9 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %15)
  %tobool10 = icmp ne i16 %call9, 0
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %16 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %16, i32 0, i32 1
  %17 = load i16, i16* %m_handle11, align 2
  %call12 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %17)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %m_handle13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %18, i32 0, i32 1
  %19 = load i16, i16* %m_handle13, align 2
  %call14 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %19)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call14, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %20 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %20
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %21 = load i32, i32* %axis1, align 4
  %shl15 = shl i32 1, %21
  %and16 = and i32 %shl15, 3
  store i32 %and16, i32* %axis2, align 4
  %22 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool17 = trunc i8 %22 to i1
  br i1 %tobool17, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.then
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %25 = load i32, i32* %axis1, align 4
  %26 = load i32, i32* %axis2, align 4
  %call18 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %23, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %24, i32 %25, i32 %26)
  br i1 %call18, label %if.then19, label %if.end27

if.then19:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %27 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %28 = bitcast %class.btOverlappingPairCache* %27 to %class.btOverlappingPairCallback*
  %29 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %29 to %struct.btBroadphaseProxy*
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %34 = bitcast %class.btOverlappingPairCallback* %28 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %34, align 4
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %35 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call20 = call i8* %35(%class.btOverlappingPairCallback* %28, %struct.btBroadphaseProxy* %30, %struct.btBroadphaseProxy* %32, %class.btDispatcher* %33)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool21 = icmp ne %class.btOverlappingPairCallback* %36, null
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then19
  %m_userPairCallback23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %37 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback23, align 4
  %38 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %43 = bitcast %class.btOverlappingPairCallback* %37 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable24 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %43, align 4
  %vfn25 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable24, i64 3
  %44 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn25, align 4
  %call26 = call i8* %44(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41, %class.btDispatcher* %42)
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then19
  br label %if.end27

if.end27:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %45 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %45, i32 0, i32 2
  %46 = load i32, i32* %axis.addr, align 4
  %arrayidx28 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %46
  %47 = load i16, i16* %arrayidx28, align 2
  %dec = add i16 %47, -1
  store i16 %dec, i16* %arrayidx28, align 2
  br label %if.end31

if.else:                                          ; preds = %while.body
  %48 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %48, i32 0, i32 1
  %49 = load i32, i32* %axis.addr, align 4
  %arrayidx29 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %49
  %50 = load i16, i16* %arrayidx29, align 2
  %dec30 = add i16 %50, -1
  store i16 %dec30, i16* %arrayidx29, align 2
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.end27
  %51 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4
  %m_minEdges32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %51, i32 0, i32 1
  %52 = load i32, i32* %axis.addr, align 4
  %arrayidx33 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges32, i32 0, i32 %52
  %53 = load i16, i16* %arrayidx33, align 2
  %inc = add i16 %53, 1
  store i16 %inc, i16* %arrayidx33, align 2
  %54 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %55, i8* align 2 %56, i32 4, i1 false)
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %58 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %58 to i8*
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %59, i8* align 2 %60, i32 4, i1 false)
  %61 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %62 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %61 to i8*
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %62, i8* align 2 %63, i32 4, i1 false)
  %64 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %64, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4
  %65 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  %incdec.ptr34 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %65, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr34, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE10freeHandleEt(%class.btAxisSweep3Internal* %this, i16 zeroext %handle) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i16 %handle, i16* %handle.addr, align 2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load i16, i16* %handle.addr, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %0)
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %1 = load i16, i16* %m_firstFreeHandle, align 4
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, i16 zeroext %1)
  %2 = load i16, i16* %handle.addr, align 2
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 %2, i16* %m_firstFreeHandle2, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %3 = load i16, i16* %m_numHandles, align 4
  %dec = add i16 %3, -1
  store i16 %dec, i16* %m_numHandles, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal* %this, i16 zeroext %handle, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %min = alloca [3 x i16], align 2
  %max = alloca [3 x i16], align 2
  %axis = alloca i32, align 4
  %emin = alloca i16, align 2
  %emax = alloca i16, align 2
  %dmin = alloca i32, align 4
  %dmax = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store i16 %handle, i16* %handle.addr, align 2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load i16, i16* %handle.addr, align 2
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %0)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %4, i32 0, i32 1
  %5 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %5
  %6 = load i16, i16* %arrayidx, align 2
  store i16 %6, i16* %emin, align 2
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %7, i32 0, i32 2
  %8 = load i32, i32* %axis, align 4
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %8
  %9 = load i16, i16* %arrayidx3, align 2
  store i16 %9, i16* %emax, align 2
  %10 = load i32, i32* %axis, align 4
  %arrayidx4 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %10
  %11 = load i16, i16* %arrayidx4, align 2
  %conv = zext i16 %11 to i32
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %12 = load i32, i32* %axis, align 4
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %12
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx5, align 4
  %14 = load i16, i16* %emin, align 2
  %idxprom = zext i16 %14 to i32
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 %idxprom
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx6, i32 0, i32 0
  %15 = load i16, i16* %m_pos, align 2
  %conv7 = zext i16 %15 to i32
  %sub = sub nsw i32 %conv, %conv7
  store i32 %sub, i32* %dmin, align 4
  %16 = load i32, i32* %axis, align 4
  %arrayidx8 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx8, align 2
  %conv9 = zext i16 %17 to i32
  %m_pEdges10 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %18 = load i32, i32* %axis, align 4
  %arrayidx11 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges10, i32 0, i32 %18
  %19 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx11, align 4
  %20 = load i16, i16* %emax, align 2
  %idxprom12 = zext i16 %20 to i32
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %19, i32 %idxprom12
  %m_pos14 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx13, i32 0, i32 0
  %21 = load i16, i16* %m_pos14, align 2
  %conv15 = zext i16 %21 to i32
  %sub16 = sub nsw i32 %conv9, %conv15
  store i32 %sub16, i32* %dmax, align 4
  %22 = load i32, i32* %axis, align 4
  %arrayidx17 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %22
  %23 = load i16, i16* %arrayidx17, align 2
  %m_pEdges18 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %24 = load i32, i32* %axis, align 4
  %arrayidx19 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges18, i32 0, i32 %24
  %25 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx19, align 4
  %26 = load i16, i16* %emin, align 2
  %idxprom20 = zext i16 %26 to i32
  %arrayidx21 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %25, i32 %idxprom20
  %m_pos22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx21, i32 0, i32 0
  store i16 %23, i16* %m_pos22, align 2
  %27 = load i32, i32* %axis, align 4
  %arrayidx23 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %27
  %28 = load i16, i16* %arrayidx23, align 2
  %m_pEdges24 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %29 = load i32, i32* %axis, align 4
  %arrayidx25 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges24, i32 0, i32 %29
  %30 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx25, align 4
  %31 = load i16, i16* %emax, align 2
  %idxprom26 = zext i16 %31 to i32
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %30, i32 %idxprom26
  %m_pos28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx27, i32 0, i32 0
  store i16 %28, i16* %m_pos28, align 2
  %32 = load i32, i32* %dmin, align 4
  %cmp29 = icmp slt i32 %32, 0
  br i1 %cmp29, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %33 = load i32, i32* %axis, align 4
  %34 = load i16, i16* %emin, align 2
  %35 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %33, i16 zeroext %34, %class.btDispatcher* %35, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %36 = load i32, i32* %dmax, align 4
  %cmp30 = icmp sgt i32 %36, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end
  %37 = load i32, i32* %axis, align 4
  %38 = load i16, i16* %emax, align 2
  %39 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %37, i16 zeroext %38, %class.btDispatcher* %39, i1 zeroext true)
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end
  %40 = load i32, i32* %dmin, align 4
  %cmp33 = icmp sgt i32 %40, 0
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end32
  %41 = load i32, i32* %axis, align 4
  %42 = load i16, i16* %emin, align 2
  %43 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %41, i16 zeroext %42, %class.btDispatcher* %43, i1 zeroext true)
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end32
  %44 = load i32, i32* %dmax, align 4
  %cmp36 = icmp slt i32 %44, 0
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end35
  %45 = load i32, i32* %axis, align 4
  %46 = load i16, i16* %emax, align 2
  %47 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %45, i16 zeroext %46, %class.btDispatcher* %47, i1 zeroext true)
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %if.end35
  br label %for.inc

for.inc:                                          ; preds = %if.end38
  %48 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4
  store i8 1, i8* %overlap, align 1
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1
  %27 = load i8, i8* %overlap, align 1
  %tobool31 = trunc i8 %27 to i1
  ret i1 %tobool31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.10* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.10* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.10* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end15

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %14, i32 %15
  %16 = bitcast %struct.btBroadphasePair* %arrayidx10 to i8*
  %call11 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btBroadphasePair*
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4
  %call12 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %17, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6

for.end15:                                        ; preds = %for.cond6
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.13* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pHandleA = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleB = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %3 = bitcast %struct.btBroadphaseProxy* %2 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %3, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %5, i32 0, i32 2
  %6 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %6
  %7 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %7 to i32
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %8, i32 0, i32 1
  %9 = load i32, i32* %axis, align 4
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %9
  %10 = load i16, i16* %arrayidx2, align 2
  %conv3 = zext i16 %10 to i32
  %cmp4 = icmp slt i32 %conv, %conv3
  br i1 %cmp4, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4
  %m_maxEdges5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %11, i32 0, i32 2
  %12 = load i32, i32* %axis, align 4
  %arrayidx6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges5, i32 0, i32 %12
  %13 = load i16, i16* %arrayidx6, align 2
  %conv7 = zext i16 %13 to i32
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4
  %m_minEdges8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %14, i32 0, i32 1
  %15 = load i32, i32* %axis, align 4
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges8, i32 0, i32 %15
  %16 = load i16, i16* %arrayidx9, align 2
  %conv10 = zext i16 %16 to i32
  %cmp11 = icmp slt i32 %conv7, %conv10
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.10* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 %7
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %5, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %9 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 %11
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %9, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %12 = load i32, i32* %j, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.10* %this1, i32 %15, i32 %16)
  %17 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %17, 1
  store i32 %inc11, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %dec12 = add nsw i32 %18, -1
  store i32 %dec12, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sle i32 %19, %20
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i32, i32* %lo.addr, align 4
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %23 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %24 = load i32, i32* %lo.addr, align 4
  %25 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.10* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %23, i32 %24, i32 %25)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %hi.addr, align 4
  %cmp17 = icmp slt i32 %26, %27
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %28 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.10* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.13* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon.13* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4
  store i8* %9, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %tobool = icmp ne %struct.btBroadphaseProxy* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 0, i32 0
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %6, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 3
  %9 = load i32, i32* %m_uniqueId7, align 4
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %9, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %11, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 1
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId14, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %14, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %16, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 1
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 3
  %19 = load i32, i32* %m_uniqueId22, align 4
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %19, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4
  %20 = load i32, i32* %uidA0, align 4
  %21 = load i32, i32* %uidB0, align 4
  %cmp = icmp sgt i32 %20, %21
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 0
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 0
  %25 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %23, %25
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %26 = load i32, i32* %uidA1, align 4
  %27 = load i32, i32* %uidB1, align 4
  %cmp29 = icmp sgt i32 %26, %27
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  %31 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %29, %31
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 1
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 2
  %37 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 2
  %39 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %37, %39
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %40 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %41 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %40, %land.end ]
  ret i1 %41
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.10* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %7 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %10 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %11 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.10* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.10* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %2, %struct.btBroadphasePair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %4, %struct.btBroadphasePair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.10* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.11* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.10* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 %8
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.11* %this, i32 %n, %struct.btBroadphasePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PviiP12btDispatcher(%class.btAxisSweep3Internal.9* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %pOwner, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pOwner.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %min = alloca [3 x i32], align 4
  %max = alloca [3 x i32], align 4
  %handle = alloca i32, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i8* %pOwner, i8** %pOwner.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.9* %this1, i32* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %0, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.9* %this1, i32* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 1)
  %call = call i32 @_ZN20btAxisSweep3InternalIjE11allocHandleEv(%class.btAxisSweep3Internal.9* %this1)
  store i32 %call, i32* %handle, align 4
  %2 = load i32, i32* %handle, align 4
  %call3 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %2)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call3, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %3 = load i32, i32* %handle, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %4 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 3
  store i32 %3, i32* %m_uniqueId, align 4
  %6 = load i8*, i8** %pOwner.addr, align 4
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %7 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 0
  store i8* %6, i8** %m_clientObject, align 4
  %9 = load i32, i32* %collisionFilterGroup.addr, align 4
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %11 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %10 to %struct.btBroadphaseProxy*
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 1
  store i32 %9, i32* %m_collisionFilterGroup, align 4
  %12 = load i32, i32* %collisionFilterMask.addr, align 4
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %13 to %struct.btBroadphaseProxy*
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 2
  store i32 %12, i32* %m_collisionFilterMask, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %15 = load i32, i32* %m_numHandles, align 4
  %mul = mul i32 %15, 2
  store i32 %mul, i32* %limit, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %16 = load i32, i32* %axis, align 4
  %cmp = icmp ult i32 %16, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %17, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 0, i32 2
  %18 = load i32, i32* %axis, align 4
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx4, align 4
  %add = add i32 %19, 2
  store i32 %add, i32* %arrayidx4, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %20 = load i32, i32* %axis, align 4
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %20
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx5, align 4
  %22 = load i32, i32* %limit, align 4
  %sub = sub i32 %22, 1
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %21, i32 %sub
  %m_pEdges7 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %23 = load i32, i32* %axis, align 4
  %arrayidx8 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges7, i32 0, i32 %23
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx8, align 4
  %25 = load i32, i32* %limit, align 4
  %add9 = add i32 %25, 1
  %arrayidx10 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %24, i32 %add9
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx10 to i8*
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 8, i1 false)
  %28 = load i32, i32* %axis, align 4
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx11, align 4
  %m_pEdges12 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %30 = load i32, i32* %axis, align 4
  %arrayidx13 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges12, i32 0, i32 %30
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx13, align 4
  %32 = load i32, i32* %limit, align 4
  %sub14 = sub i32 %32, 1
  %arrayidx15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %31, i32 %sub14
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx15, i32 0, i32 0
  store i32 %29, i32* %m_pos, align 4
  %33 = load i32, i32* %handle, align 4
  %m_pEdges16 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %34 = load i32, i32* %axis, align 4
  %arrayidx17 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges16, i32 0, i32 %34
  %35 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx17, align 4
  %36 = load i32, i32* %limit, align 4
  %sub18 = sub i32 %36, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %35, i32 %sub18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx19, i32 0, i32 1
  store i32 %33, i32* %m_handle, align 4
  %37 = load i32, i32* %axis, align 4
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx20, align 4
  %m_pEdges21 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %39 = load i32, i32* %axis, align 4
  %arrayidx22 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges21, i32 0, i32 %39
  %40 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx22, align 4
  %41 = load i32, i32* %limit, align 4
  %arrayidx23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %40, i32 %41
  %m_pos24 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx23, i32 0, i32 0
  store i32 %38, i32* %m_pos24, align 4
  %42 = load i32, i32* %handle, align 4
  %m_pEdges25 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %43 = load i32, i32* %axis, align 4
  %arrayidx26 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges25, i32 0, i32 %43
  %44 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx26, align 4
  %45 = load i32, i32* %limit, align 4
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %44, i32 %45
  %m_handle28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx27, i32 0, i32 1
  store i32 %42, i32* %m_handle28, align 4
  %46 = load i32, i32* %limit, align 4
  %sub29 = sub i32 %46, 1
  %47 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %47, i32 0, i32 1
  %48 = load i32, i32* %axis, align 4
  %arrayidx30 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %48
  store i32 %sub29, i32* %arrayidx30, align 4
  %49 = load i32, i32* %limit, align 4
  %50 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges31 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %50, i32 0, i32 2
  %51 = load i32, i32* %axis, align 4
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges31, i32 0, i32 %51
  store i32 %49, i32* %arrayidx32, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %52 = load i32, i32* %axis, align 4
  %inc = add i32 %52, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %53 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges33 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %53, i32 0, i32 1
  %arrayidx34 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges33, i32 0, i32 0
  %54 = load i32, i32* %arrayidx34, align 4
  %55 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 0, i32 %54, %class.btDispatcher* %55, i1 zeroext false)
  %56 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges35 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %56, i32 0, i32 2
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges35, i32 0, i32 0
  %57 = load i32, i32* %arrayidx36, align 4
  %58 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 0, i32 %57, %class.btDispatcher* %58, i1 zeroext false)
  %59 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges37 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %59, i32 0, i32 1
  %arrayidx38 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges37, i32 0, i32 1
  %60 = load i32, i32* %arrayidx38, align 4
  %61 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 1, i32 %60, %class.btDispatcher* %61, i1 zeroext false)
  %62 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges39 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %62, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges39, i32 0, i32 1
  %63 = load i32, i32* %arrayidx40, align 4
  %64 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 1, i32 %63, %class.btDispatcher* %64, i1 zeroext false)
  %65 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges41 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %65, i32 0, i32 1
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges41, i32 0, i32 2
  %66 = load i32, i32* %arrayidx42, align 4
  %67 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 2, i32 %66, %class.btDispatcher* %67, i1 zeroext true)
  %68 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges43 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %68, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges43, i32 0, i32 2
  %69 = load i32, i32* %arrayidx44, align 4
  %70 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 2, i32 %69, %class.btDispatcher* %70, i1 zeroext true)
  %71 = load i32, i32* %handle, align 4
  ret i32 %71
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %0 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %1 = load i32, i32* %index.addr, align 4
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %0, i32 %1
  ret %"class.btAxisSweep3Internal<unsigned int>::Handle"* %add.ptr
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.9* %this, i32* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %out.addr = alloca i32*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32* %out, i32** %out.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store i32 %isMax, i32* %isMax.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_quantize)
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  %cmp = fcmp ole float %1, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %isMax.addr, align 4
  br label %cond.end14

cond.false:                                       ; preds = %entry
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx3, align 4
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %4 = load i32, i32* %m_handleSentinel, align 4
  %conv = uitofp i32 %4 to float
  %cmp4 = fcmp oge float %3, %conv
  br i1 %cmp4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.false
  %m_handleSentinel6 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %5 = load i32, i32* %m_handleSentinel6, align 4
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_bpHandleMask, align 4
  %and = and i32 %5, %6
  %7 = load i32, i32* %isMax.addr, align 4
  %or = or i32 %and, %7
  br label %cond.end

cond.false7:                                      ; preds = %cond.false
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %8 = load float, float* %arrayidx9, align 4
  %conv10 = fptoui float %8 to i32
  %m_bpHandleMask11 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %9 = load i32, i32* %m_bpHandleMask11, align 4
  %and12 = and i32 %conv10, %9
  %10 = load i32, i32* %isMax.addr, align 4
  %or13 = or i32 %and12, %10
  br label %cond.end

cond.end:                                         ; preds = %cond.false7, %cond.true5
  %cond = phi i32 [ %or, %cond.true5 ], [ %or13, %cond.false7 ]
  br label %cond.end14

cond.end14:                                       ; preds = %cond.end, %cond.true
  %cond15 = phi i32 [ %2, %cond.true ], [ %cond, %cond.end ]
  %11 = load i32*, i32** %out.addr, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %11, i32 0
  store i32 %cond15, i32* %arrayidx16, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %12 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp ole float %12, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end14
  %13 = load i32, i32* %isMax.addr, align 4
  br label %cond.end41

cond.false21:                                     ; preds = %cond.end14
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %14 = load float, float* %arrayidx23, align 4
  %m_handleSentinel24 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %15 = load i32, i32* %m_handleSentinel24, align 4
  %conv25 = uitofp i32 %15 to float
  %cmp26 = fcmp oge float %14, %conv25
  br i1 %cmp26, label %cond.true27, label %cond.false32

cond.true27:                                      ; preds = %cond.false21
  %m_handleSentinel28 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %16 = load i32, i32* %m_handleSentinel28, align 4
  %m_bpHandleMask29 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %17 = load i32, i32* %m_bpHandleMask29, align 4
  %and30 = and i32 %16, %17
  %18 = load i32, i32* %isMax.addr, align 4
  %or31 = or i32 %and30, %18
  br label %cond.end39

cond.false32:                                     ; preds = %cond.false21
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  %19 = load float, float* %arrayidx34, align 4
  %conv35 = fptoui float %19 to i32
  %m_bpHandleMask36 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %20 = load i32, i32* %m_bpHandleMask36, align 4
  %and37 = and i32 %conv35, %20
  %21 = load i32, i32* %isMax.addr, align 4
  %or38 = or i32 %and37, %21
  br label %cond.end39

cond.end39:                                       ; preds = %cond.false32, %cond.true27
  %cond40 = phi i32 [ %or31, %cond.true27 ], [ %or38, %cond.false32 ]
  br label %cond.end41

cond.end41:                                       ; preds = %cond.end39, %cond.true20
  %cond42 = phi i32 [ %13, %cond.true20 ], [ %cond40, %cond.end39 ]
  %22 = load i32*, i32** %out.addr, align 4
  %arrayidx43 = getelementptr inbounds i32, i32* %22, i32 1
  store i32 %cond42, i32* %arrayidx43, align 4
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 2
  %23 = load float, float* %arrayidx45, align 4
  %cmp46 = fcmp ole float %23, 0.000000e+00
  br i1 %cmp46, label %cond.true47, label %cond.false48

cond.true47:                                      ; preds = %cond.end41
  %24 = load i32, i32* %isMax.addr, align 4
  br label %cond.end68

cond.false48:                                     ; preds = %cond.end41
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 2
  %25 = load float, float* %arrayidx50, align 4
  %m_handleSentinel51 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %26 = load i32, i32* %m_handleSentinel51, align 4
  %conv52 = uitofp i32 %26 to float
  %cmp53 = fcmp oge float %25, %conv52
  br i1 %cmp53, label %cond.true54, label %cond.false59

cond.true54:                                      ; preds = %cond.false48
  %m_handleSentinel55 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %27 = load i32, i32* %m_handleSentinel55, align 4
  %m_bpHandleMask56 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %28 = load i32, i32* %m_bpHandleMask56, align 4
  %and57 = and i32 %27, %28
  %29 = load i32, i32* %isMax.addr, align 4
  %or58 = or i32 %and57, %29
  br label %cond.end66

cond.false59:                                     ; preds = %cond.false48
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %30 = load float, float* %arrayidx61, align 4
  %conv62 = fptoui float %30 to i32
  %m_bpHandleMask63 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 1
  %31 = load i32, i32* %m_bpHandleMask63, align 4
  %and64 = and i32 %conv62, %31
  %32 = load i32, i32* %isMax.addr, align 4
  %or65 = or i32 %and64, %32
  br label %cond.end66

cond.end66:                                       ; preds = %cond.false59, %cond.true54
  %cond67 = phi i32 [ %or58, %cond.true54 ], [ %or65, %cond.false59 ]
  br label %cond.end68

cond.end68:                                       ; preds = %cond.end66, %cond.true47
  %cond69 = phi i32 [ %24, %cond.true47 ], [ %cond67, %cond.end66 ]
  %33 = load i32*, i32** %out.addr, align 4
  %arrayidx70 = getelementptr inbounds i32, i32* %33, i32 2
  store i32 %cond69, i32* %arrayidx70, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN20btAxisSweep3InternalIjE11allocHandleEv(%class.btAxisSweep3Internal.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %handle = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  %0 = load i32, i32* %m_firstFreeHandle, align 4
  store i32 %0, i32* %handle, align 4
  %1 = load i32, i32* %handle, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %1)
  %call2 = call i32 @_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %call)
  %m_firstFreeHandle3 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  store i32 %call2, i32* %m_firstFreeHandle3, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %2 = load i32, i32* %m_numHandles, align 4
  %inc = add i32 %2, 1
  store i32 %inc, i32* %m_numHandles, align 4
  %3 = load i32, i32* %handle, align 4
  ret i32 %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this, i32 %axis, i32 %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i32 %edge, i32* %edge.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %3 = load i32, i32* %edge.addr, align 4
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %2, i32 %3
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %4, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %5, i32 0, i32 1
  %6 = load i32, i32* %m_handle, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %6)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end23, %entry
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %7, i32 0, i32 0
  %8 = load i32, i32* %m_pos, align 4
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %m_pos3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_pos3, align 4
  %cmp = icmp ult i32 %8, %10
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %m_handle4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %11, i32 0, i32 1
  %12 = load i32, i32* %m_handle4, align 4
  %call5 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %12)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call5, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %call6 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %13)
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %14 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %14
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %15 = load i32, i32* %axis1, align 4
  %shl7 = shl i32 1, %15
  %and8 = and i32 %shl7, 3
  store i32 %and8, i32* %axis2, align 4
  %16 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool9 = trunc i8 %16 to i1
  br i1 %tobool9, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %if.then
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %19 = load i32, i32* %axis1, align 4
  %20 = load i32, i32* %axis2, align 4
  %call10 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.9* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %17, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18, i32 %19, i32 %20)
  br i1 %call10, label %if.then11, label %if.end19

if.then11:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %21 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %22 = bitcast %class.btOverlappingPairCache* %21 to %class.btOverlappingPairCallback*
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %24 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %23 to %struct.btBroadphaseProxy*
  %25 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %25 to %struct.btBroadphaseProxy*
  %27 = bitcast %class.btOverlappingPairCallback* %22 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %27, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %28 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call12 = call %struct.btBroadphasePair* %28(%class.btOverlappingPairCallback* %22, %struct.btBroadphaseProxy* %24, %struct.btBroadphaseProxy* %26)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %29 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool13 = icmp ne %class.btOverlappingPairCallback* %29, null
  br i1 %tobool13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then11
  %m_userPairCallback15 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %30 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback15, align 4
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %34 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %33 to %struct.btBroadphaseProxy*
  %35 = bitcast %class.btOverlappingPairCallback* %30 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable16 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %35, align 4
  %vfn17 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable16, i64 2
  %36 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn17, align 4
  %call18 = call %struct.btBroadphasePair* %36(%class.btOverlappingPairCallback* %30, %struct.btBroadphaseProxy* %32, %struct.btBroadphaseProxy* %34)
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then11
  br label %if.end19

if.end19:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %37 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %37, i32 0, i32 2
  %38 = load i32, i32* %axis.addr, align 4
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx20, align 4
  %inc = add i32 %39, 1
  store i32 %inc, i32* %arrayidx20, align 4
  br label %if.end23

if.else:                                          ; preds = %while.body
  %40 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %40, i32 0, i32 1
  %41 = load i32, i32* %axis.addr, align 4
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx21, align 4
  %inc22 = add i32 %42, 1
  store i32 %inc22, i32* %arrayidx21, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.else, %if.end19
  %43 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %m_minEdges24 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %43, i32 0, i32 1
  %44 = load i32, i32* %axis.addr, align 4
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges24, i32 0, i32 %44
  %45 = load i32, i32* %arrayidx25, align 4
  %dec = add i32 %45, -1
  store i32 %dec, i32* %arrayidx25, align 4
  %46 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %48 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 8, i1 false)
  %49 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %50 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %51 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %50 to i8*
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 8, i1 false)
  %53 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %53 to i8*
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 8, i1 false)
  %56 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %56, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %incdec.ptr26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %57, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr26, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this, i32 %axis, i32 %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i32 %edge, i32* %edge.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %0 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %0
  %1 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %2 = load i32, i32* %edge.addr, align 4
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %1, i32 %2
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %3, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %4, i32 0, i32 1
  %5 = load i32, i32* %m_handle, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %5)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end27, %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %6, i32 0, i32 0
  %7 = load i32, i32* %m_pos, align 4
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %m_pos3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %8, i32 0, i32 0
  %9 = load i32, i32* %m_pos3, align 4
  %cmp = icmp ult i32 %7, %9
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %m_handle4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %10, i32 0, i32 1
  %11 = load i32, i32* %m_handle4, align 4
  %call5 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %11)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call5, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %call6 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %12)
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 0, i32 1
  %14 = load i32, i32* %m_handle7, align 4
  %call8 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %14)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %15, i32 0, i32 1
  %16 = load i32, i32* %m_handle9, align 4
  %call10 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %16)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %17 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %17
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %18 = load i32, i32* %axis1, align 4
  %shl11 = shl i32 1, %18
  %and12 = and i32 %shl11, 3
  store i32 %and12, i32* %axis2, align 4
  %19 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool13 = trunc i8 %19 to i1
  br i1 %tobool13, label %land.lhs.true, label %if.end23

land.lhs.true:                                    ; preds = %if.then
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %22 = load i32, i32* %axis1, align 4
  %23 = load i32, i32* %axis2, align 4
  %call14 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.9* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %21, i32 %22, i32 %23)
  br i1 %call14, label %if.then15, label %if.end23

if.then15:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %24 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %25 = bitcast %class.btOverlappingPairCache* %24 to %class.btOverlappingPairCallback*
  %26 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %26 to %struct.btBroadphaseProxy*
  %28 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %29 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %28 to %struct.btBroadphaseProxy*
  %30 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %31 = bitcast %class.btOverlappingPairCallback* %25 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %31, align 4
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %32 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call16 = call i8* %32(%class.btOverlappingPairCallback* %25, %struct.btBroadphaseProxy* %27, %struct.btBroadphaseProxy* %29, %class.btDispatcher* %30)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %33 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool17 = icmp ne %class.btOverlappingPairCallback* %33, null
  br i1 %tobool17, label %if.then18, label %if.end

if.then18:                                        ; preds = %if.then15
  %m_userPairCallback19 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %34 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback19, align 4
  %35 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %36 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %35 to %struct.btBroadphaseProxy*
  %37 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %40 = bitcast %class.btOverlappingPairCallback* %34 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable20 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %40, align 4
  %vfn21 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable20, i64 3
  %41 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn21, align 4
  %call22 = call i8* %41(%class.btOverlappingPairCallback* %34, %struct.btBroadphaseProxy* %36, %struct.btBroadphaseProxy* %38, %class.btDispatcher* %39)
  br label %if.end

if.end:                                           ; preds = %if.then18, %if.then15
  br label %if.end23

if.end23:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %42 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %42, i32 0, i32 1
  %43 = load i32, i32* %axis.addr, align 4
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx24, align 4
  %inc = add i32 %44, 1
  store i32 %inc, i32* %arrayidx24, align 4
  br label %if.end27

if.else:                                          ; preds = %while.body
  %45 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %45, i32 0, i32 2
  %46 = load i32, i32* %axis.addr, align 4
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx25, align 4
  %inc26 = add i32 %47, 1
  store i32 %inc26, i32* %arrayidx25, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.else, %if.end23
  %48 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %m_maxEdges28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %48, i32 0, i32 2
  %49 = load i32, i32* %axis.addr, align 4
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges28, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx29, align 4
  %dec = add i32 %50, -1
  store i32 %dec, i32* %arrayidx29, align 4
  %51 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %53 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 8, i1 false)
  %54 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %55 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %55 to i8*
  %57 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 8, i1 false)
  %58 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %58 to i8*
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 8, i1 false)
  %61 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %61, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %62 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  %incdec.ptr30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %62, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr30, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 0
  %0 = load i32, i32* %arrayidx, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %this, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %this.addr, align 4
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %this.addr, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_pos, align 4
  %and = and i32 %0, 1
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.9* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleB, i32 %axis0, i32 %axis1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %pHandleA.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleB.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis0.addr = alloca i32, align 4
  %axis1.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleB, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4
  store i32 %axis0, i32* %axis0.addr, align 4
  store i32 %axis1, i32* %axis1.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %0, i32 0, i32 2
  %1 = load i32, i32* %axis0.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %3, i32 0, i32 1
  %4 = load i32, i32* %axis0.addr, align 4
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx2, align 4
  %cmp = icmp ult i32 %2, %5
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4
  %m_maxEdges3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %6, i32 0, i32 2
  %7 = load i32, i32* %axis0.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges3, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx4, align 4
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4
  %m_minEdges5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %9, i32 0, i32 1
  %10 = load i32, i32* %axis0.addr, align 4
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges5, i32 0, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4
  %cmp7 = icmp ult i32 %8, %11
  br i1 %cmp7, label %if.then, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4
  %m_maxEdges9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %12, i32 0, i32 2
  %13 = load i32, i32* %axis1.addr, align 4
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges9, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx10, align 4
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4
  %m_minEdges11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %15, i32 0, i32 1
  %16 = load i32, i32* %axis1.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges11, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx12, align 4
  %cmp13 = icmp ult i32 %14, %17
  br i1 %cmp13, label %if.then, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %lor.lhs.false8
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4
  %m_maxEdges15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18, i32 0, i32 2
  %19 = load i32, i32* %axis1.addr, align 4
  %arrayidx16 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges15, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx16, align 4
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4
  %m_minEdges17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %21, i32 0, i32 1
  %22 = load i32, i32* %axis1.addr, align 4
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges17, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx18, align 4
  %cmp19 = icmp ult i32 %20, %23
  br i1 %cmp19, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false14, %lor.lhs.false8, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false14
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher(%class.btAxisSweep3Internal.9* %this, i32 %handle, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %handle.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  %pEdges = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %max = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %handle, i32* %handle.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load i32, i32* %handle.addr, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %0)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %2 = bitcast %class.btOverlappingPairCache* %1 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %2, align 4
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %3 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call zeroext i1 %3(%class.btOverlappingPairCache* %1)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_pairCache3 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache3, align 4
  %5 = bitcast %class.btOverlappingPairCache* %4 to %class.btOverlappingPairCallback*
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %6 to %struct.btBroadphaseProxy*
  %8 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %9 = bitcast %class.btOverlappingPairCallback* %5 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable4 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %9, align 4
  %vfn5 = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable4, i64 4
  %10 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn5, align 4
  call void %10(%class.btOverlappingPairCallback* %5, %struct.btBroadphaseProxy* %7, %class.btDispatcher* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %11 = load i32, i32* %m_numHandles, align 4
  %mul = mul i32 %11, 2
  store i32 %mul, i32* %limit, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %12 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %12, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 8
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %13, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 0, i32 2
  %14 = load i32, i32* %axis, align 4
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx6, align 4
  %sub = sub i32 %15, 2
  store i32 %sub, i32* %arrayidx6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %axis, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc24, %for.end
  %17 = load i32, i32* %axis, align 4
  %cmp8 = icmp slt i32 %17, 3
  br i1 %cmp8, label %for.body9, label %for.end26

for.body9:                                        ; preds = %for.cond7
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %18 = load i32, i32* %axis, align 4
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %18
  %19 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx10, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %19, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20, i32 0, i32 2
  %21 = load i32, i32* %axis, align 4
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges11, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx12, align 4
  store i32 %22, i32* %max, align 4
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %23 = load i32, i32* %m_handleSentinel, align 4
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4
  %25 = load i32, i32* %max, align 4
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %24, i32 %25
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx13, i32 0, i32 0
  store i32 %23, i32* %m_pos, align 4
  %26 = load i32, i32* %axis, align 4
  %27 = load i32, i32* %max, align 4
  %28 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %26, i32 %27, %class.btDispatcher* %28, i1 zeroext false)
  %29 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %29, i32 0, i32 1
  %30 = load i32, i32* %axis, align 4
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %30
  %31 = load i32, i32* %arrayidx14, align 4
  store i32 %31, i32* %i, align 4
  %m_handleSentinel15 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %32 = load i32, i32* %m_handleSentinel15, align 4
  %33 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4
  %34 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %33, i32 %34
  %m_pos17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx16, i32 0, i32 0
  store i32 %32, i32* %m_pos17, align 4
  %35 = load i32, i32* %axis, align 4
  %36 = load i32, i32* %i, align 4
  %37 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %35, i32 %36, %class.btDispatcher* %37, i1 zeroext false)
  %38 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4
  %39 = load i32, i32* %limit, align 4
  %sub18 = sub nsw i32 %39, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %38, i32 %sub18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx19, i32 0, i32 1
  store i32 0, i32* %m_handle, align 4
  %m_handleSentinel20 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 2
  %40 = load i32, i32* %m_handleSentinel20, align 4
  %41 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4
  %42 = load i32, i32* %limit, align 4
  %sub21 = sub nsw i32 %42, 1
  %arrayidx22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %41, i32 %sub21
  %m_pos23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx22, i32 0, i32 0
  store i32 %40, i32* %m_pos23, align 4
  br label %for.inc24

for.inc24:                                        ; preds = %for.body9
  %43 = load i32, i32* %axis, align 4
  %inc25 = add nsw i32 %43, 1
  store i32 %inc25, i32* %axis, align 4
  br label %for.cond7

for.end26:                                        ; preds = %for.cond7
  %44 = load i32, i32* %handle.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE10freeHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this, i32 %axis, i32 %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i32 %edge, i32* %edge.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %3 = load i32, i32* %edge.addr, align 4
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %2, i32 %3
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %4, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %5, i32 0, i32 1
  %6 = load i32, i32* %m_handle, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %6)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %7, i32 0, i32 1
  %8 = load i32, i32* %m_handle3, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_pos, align 4
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %11, i32 0, i32 0
  %12 = load i32, i32* %m_pos4, align 4
  %cmp = icmp uge i32 %10, %12
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %13 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %13, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %14, i32 0, i32 1
  %15 = load i32, i32* %m_handle5, align 4
  %call6 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %15)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call6, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %16 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %16
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %17 = load i32, i32* %axis1, align 4
  %shl7 = shl i32 1, %17
  %and8 = and i32 %shl7, 3
  store i32 %and8, i32* %axis2, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %call9 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %18)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %19 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool11 = trunc i8 %19 to i1
  br i1 %tobool11, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %22 = load i32, i32* %axis1, align 4
  %23 = load i32, i32* %axis2, align 4
  %call12 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.9* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %21, i32 %22, i32 %23)
  br i1 %call12, label %if.then13, label %if.end25

if.then13:                                        ; preds = %land.lhs.true
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle14 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %24, i32 0, i32 1
  %25 = load i32, i32* %m_handle14, align 4
  %call15 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %25)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call15, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %26 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %26, i32 0, i32 1
  %27 = load i32, i32* %m_handle16, align 4
  %call17 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %27)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call17, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %28 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %29 = bitcast %class.btOverlappingPairCache* %28 to %class.btOverlappingPairCallback*
  %30 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %31 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %30 to %struct.btBroadphaseProxy*
  %32 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %33 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %32 to %struct.btBroadphaseProxy*
  %34 = bitcast %class.btOverlappingPairCallback* %29 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %34, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %35 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call18 = call %struct.btBroadphasePair* %35(%class.btOverlappingPairCallback* %29, %struct.btBroadphaseProxy* %31, %struct.btBroadphaseProxy* %33)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %36, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then13
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %37 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4
  %38 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = bitcast %class.btOverlappingPairCallback* %37 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable22 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %42, align 4
  %vfn23 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable22, i64 2
  %43 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn23, align 4
  %call24 = call %struct.btBroadphasePair* %43(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then13
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %44 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %44, i32 0, i32 1
  %45 = load i32, i32* %axis.addr, align 4
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx26, align 4
  %dec = add i32 %46, -1
  store i32 %dec, i32* %arrayidx26, align 4
  br label %if.end29

if.else:                                          ; preds = %while.body
  %47 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %47, i32 0, i32 2
  %48 = load i32, i32* %axis.addr, align 4
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %48
  %49 = load i32, i32* %arrayidx27, align 4
  %dec28 = add i32 %49, -1
  store i32 %dec28, i32* %arrayidx27, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %50 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %m_maxEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %50, i32 0, i32 2
  %51 = load i32, i32* %axis.addr, align 4
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges30, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx31, align 4
  %inc = add i32 %52, 1
  store i32 %inc, i32* %arrayidx31, align 4
  %53 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 8, i1 false)
  %56 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %58 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %57 to i8*
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 8, i1 false)
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %61 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %60 to i8*
  %62 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 8, i1 false)
  %63 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %63, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %64 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %64, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this, i32 %axis, i32 %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  store i32 %edge, i32* %edge.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %0 = load i32, i32* %axis.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %0
  %1 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4
  %2 = load i32, i32* %edge.addr, align 4
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %1, i32 %2
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %3, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %4, i32 0, i32 1
  %5 = load i32, i32* %m_handle, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %5)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %6, i32 0, i32 1
  %7 = load i32, i32* %m_handle3, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %8, i32 0, i32 0
  %9 = load i32, i32* %m_pos, align 4
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %10, i32 0, i32 0
  %11 = load i32, i32* %m_pos4, align 4
  %cmp = icmp uge i32 %9, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %12 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %12, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 0, i32 1
  %14 = load i32, i32* %m_handle5, align 4
  %call6 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %14)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call6, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %call7 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %15)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %16 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %16, i32 0, i32 1
  %17 = load i32, i32* %m_handle9, align 4
  %call10 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %17)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %18, i32 0, i32 1
  %19 = load i32, i32* %m_handle11, align 4
  %call12 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %19)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %20 = load i32, i32* %axis.addr, align 4
  %shl = shl i32 1, %20
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4
  %21 = load i32, i32* %axis1, align 4
  %shl13 = shl i32 1, %21
  %and14 = and i32 %shl13, 3
  store i32 %and14, i32* %axis2, align 4
  %22 = load i8, i8* %updateOverlaps.addr, align 1
  %tobool15 = trunc i8 %22 to i1
  br i1 %tobool15, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %25 = load i32, i32* %axis1, align 4
  %26 = load i32, i32* %axis2, align 4
  %call16 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.9* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %23, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %24, i32 %25, i32 %26)
  br i1 %call16, label %if.then17, label %if.end25

if.then17:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 12
  %27 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %28 = bitcast %class.btOverlappingPairCache* %27 to %class.btOverlappingPairCallback*
  %29 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %29 to %struct.btBroadphaseProxy*
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %34 = bitcast %class.btOverlappingPairCallback* %28 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %34, align 4
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %35 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call18 = call i8* %35(%class.btOverlappingPairCallback* %28, %struct.btBroadphaseProxy* %30, %struct.btBroadphaseProxy* %32, %class.btDispatcher* %33)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %36, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then17
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 13
  %37 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4
  %38 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %43 = bitcast %class.btOverlappingPairCallback* %37 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable22 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %43, align 4
  %vfn23 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable22, i64 3
  %44 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn23, align 4
  %call24 = call i8* %44(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41, %class.btDispatcher* %42)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then17
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %45 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %45, i32 0, i32 2
  %46 = load i32, i32* %axis.addr, align 4
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx26, align 4
  %dec = add i32 %47, -1
  store i32 %dec, i32* %arrayidx26, align 4
  br label %if.end29

if.else:                                          ; preds = %while.body
  %48 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %48, i32 0, i32 1
  %49 = load i32, i32* %axis.addr, align 4
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx27, align 4
  %dec28 = add i32 %50, -1
  store i32 %dec28, i32* %arrayidx27, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %51 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4
  %m_minEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %51, i32 0, i32 1
  %52 = load i32, i32* %axis.addr, align 4
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges30, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx31, align 4
  %inc = add i32 %53, 1
  store i32 %inc, i32* %arrayidx31, align 4
  %54 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 8, i1 false)
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %58 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %58 to i8*
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 8, i1 false)
  %61 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %62 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %61 to i8*
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 8, i1 false)
  %64 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %64, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4
  %65 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %65, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE10freeHandleEj(%class.btAxisSweep3Internal.9* %this, i32 %handle) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %handle.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %handle, i32* %handle.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load i32, i32* %handle.addr, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %0)
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  %1 = load i32, i32* %m_firstFreeHandle, align 4
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, i32 %1)
  %2 = load i32, i32* %handle.addr, align 4
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 9
  store i32 %2, i32* %m_firstFreeHandle2, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_numHandles, align 4
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_numHandles, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal.9* %this, i32 %handle, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %handle.addr = alloca i32, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %min = alloca [3 x i32], align 4
  %max = alloca [3 x i32], align 4
  %axis = alloca i32, align 4
  %emin = alloca i32, align 4
  %emax = alloca i32, align 4
  %dmin = alloca i32, align 4
  %dmax = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store i32 %handle, i32* %handle.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load i32, i32* %handle.addr, align 4
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.9* %this1, i32 %0)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.9* %this1, i32* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.9* %this1, i32* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %4, i32 0, i32 1
  %5 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  store i32 %6, i32* %emin, align 4
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %7, i32 0, i32 2
  %8 = load i32, i32* %axis, align 4
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %8
  %9 = load i32, i32* %arrayidx3, align 4
  store i32 %9, i32* %emax, align 4
  %10 = load i32, i32* %axis, align 4
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %10
  %11 = load i32, i32* %arrayidx4, align 4
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %12 = load i32, i32* %axis, align 4
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %12
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx5, align 4
  %14 = load i32, i32* %emin, align 4
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 %14
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx6, i32 0, i32 0
  %15 = load i32, i32* %m_pos, align 4
  %sub = sub nsw i32 %11, %15
  store i32 %sub, i32* %dmin, align 4
  %16 = load i32, i32* %axis, align 4
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx7, align 4
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %18 = load i32, i32* %axis, align 4
  %arrayidx9 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges8, i32 0, i32 %18
  %19 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx9, align 4
  %20 = load i32, i32* %emax, align 4
  %arrayidx10 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %19, i32 %20
  %m_pos11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx10, i32 0, i32 0
  %21 = load i32, i32* %m_pos11, align 4
  %sub12 = sub nsw i32 %17, %21
  store i32 %sub12, i32* %dmax, align 4
  %22 = load i32, i32* %axis, align 4
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx13, align 4
  %m_pEdges14 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %24 = load i32, i32* %axis, align 4
  %arrayidx15 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges14, i32 0, i32 %24
  %25 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx15, align 4
  %26 = load i32, i32* %emin, align 4
  %arrayidx16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %25, i32 %26
  %m_pos17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx16, i32 0, i32 0
  store i32 %23, i32* %m_pos17, align 4
  %27 = load i32, i32* %axis, align 4
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx18, align 4
  %m_pEdges19 = getelementptr inbounds %class.btAxisSweep3Internal.9, %class.btAxisSweep3Internal.9* %this1, i32 0, i32 10
  %29 = load i32, i32* %axis, align 4
  %arrayidx20 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges19, i32 0, i32 %29
  %30 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx20, align 4
  %31 = load i32, i32* %emax, align 4
  %arrayidx21 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %30, i32 %31
  %m_pos22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx21, i32 0, i32 0
  store i32 %28, i32* %m_pos22, align 4
  %32 = load i32, i32* %dmin, align 4
  %cmp23 = icmp slt i32 %32, 0
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %33 = load i32, i32* %axis, align 4
  %34 = load i32, i32* %emin, align 4
  %35 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %33, i32 %34, %class.btDispatcher* %35, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %36 = load i32, i32* %dmax, align 4
  %cmp24 = icmp sgt i32 %36, 0
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end
  %37 = load i32, i32* %axis, align 4
  %38 = load i32, i32* %emax, align 4
  %39 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %37, i32 %38, %class.btDispatcher* %39, i1 zeroext true)
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end
  %40 = load i32, i32* %dmin, align 4
  %cmp27 = icmp sgt i32 %40, 0
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.end26
  %41 = load i32, i32* %axis, align 4
  %42 = load i32, i32* %emin, align 4
  %43 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %41, i32 %42, %class.btDispatcher* %43, i1 zeroext true)
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.end26
  %44 = load i32, i32* %dmax, align 4
  %cmp30 = icmp slt i32 %44, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end29
  %45 = load i32, i32* %axis, align 4
  %46 = load i32, i32* %emax, align 4
  %47 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.9* %this1, i32 %45, i32 %46, %class.btDispatcher* %47, i1 zeroext true)
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end29
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %48 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal.9* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal.9*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pHandleA = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleB = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal.9* %this, %class.btAxisSweep3Internal.9** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btAxisSweep3Internal.9*, %class.btAxisSweep3Internal.9** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %1, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %3 = bitcast %struct.btBroadphaseProxy* %2 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %3, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4
  store i32 0, i32* %axis, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %axis, align 4
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %5, i32 0, i32 2
  %6 = load i32, i32* %axis, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx, align 4
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %8, i32 0, i32 1
  %9 = load i32, i32* %axis, align 4
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4
  %cmp3 = icmp ult i32 %7, %10
  br i1 %cmp3, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4
  %m_maxEdges4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %11, i32 0, i32 2
  %12 = load i32, i32* %axis, align 4
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges4, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx5, align 4
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4
  %m_minEdges6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %14, i32 0, i32 1
  %15 = load i32, i32* %axis, align 4
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges6, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx7, align 4
  %cmp8 = icmp ult i32 %13, %16
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %axis, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %axis, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btAxisSweep3.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { noreturn nounwind }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
