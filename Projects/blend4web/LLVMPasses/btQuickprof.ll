; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btQuickprof.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btQuickprof.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btClock = type { %struct.btClockData* }
%struct.btClockData = type { %struct.timeval }
%struct.timeval = type { i32, i32 }
%class.CProfileNode = type { i8*, i32, float, i32, i32, %class.CProfileNode*, %class.CProfileNode*, %class.CProfileNode*, i8* }
%class.CProfileIterator = type { %class.CProfileNode*, %class.CProfileNode* }
%class.CProfileSample = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_Z17Profile_Get_TicksPm = comdat any

$_Z21Profile_Get_Tick_Ratev = comdat any

$_ZN12CProfileNode9Get_ChildEv = comdat any

$_ZN12CProfileNode11Get_SiblingEv = comdat any

$_ZN12CProfileNode10Get_ParentEv = comdat any

$_ZN12CProfileNode8Get_NameEv = comdat any

$_ZN16CProfileIterator7Is_RootEv = comdat any

$_ZN16CProfileIterator29Get_Current_Parent_Total_TimeEv = comdat any

$_ZN15CProfileManager27Get_Frame_Count_Since_ResetEv = comdat any

$_ZN16CProfileIterator23Get_Current_Parent_NameEv = comdat any

$_ZN16CProfileIterator22Get_Current_Total_TimeEv = comdat any

$_ZN16CProfileIterator16Get_Current_NameEv = comdat any

$_ZN16CProfileIterator23Get_Current_Total_CallsEv = comdat any

$_ZN15CProfileManager16Release_IteratorEP16CProfileIterator = comdat any

$_ZN12CProfileNode14Get_Total_TimeEv = comdat any

$_ZN12CProfileNode15Get_Total_CallsEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN7btClock14getTimeSecondsEvE23microseconds_to_seconds = internal constant float 0x3EB0C6F7A0000000, align 4
@_ZL13gProfileClock = internal global %class.btClock zeroinitializer, align 4
@__dso_handle = external hidden global i8
@gRoots = hidden global [64 x %class.CProfileNode] zeroinitializer, align 16
@.str = private unnamed_addr constant [5 x i8] c"Root\00", align 1
@gCurrentNodes = hidden global [64 x %class.CProfileNode*] [%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 0), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 36) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 72) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 108) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 144) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 180) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 216) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 252) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 288) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 324) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 360) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 396) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 432) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 468) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 504) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 540) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 576) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 612) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 648) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 684) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 720) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 756) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 792) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 828) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 864) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 900) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 936) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 972) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1008) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1044) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1080) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1116) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1152) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1188) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1224) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1260) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1296) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1332) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1368) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1404) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1440) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1476) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1512) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1548) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1584) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1620) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1656) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1692) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1728) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1764) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1800) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1836) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1872) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1908) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1944) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 1980) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2016) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2052) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2088) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2124) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2160) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2196) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2232) to %class.CProfileNode*), %class.CProfileNode* bitcast (i8* getelementptr (i8, i8* bitcast ([64 x %class.CProfileNode]* @gRoots to i8*), i64 2268) to %class.CProfileNode*)], align 16
@_ZN15CProfileManager12FrameCounterE = hidden global i32 0, align 4
@_ZN15CProfileManager9ResetTimeE = hidden global i32 0, align 4
@.str.4 = private unnamed_addr constant [2 x i8] c".\00", align 1
@.str.5 = private unnamed_addr constant [36 x i8] c"----------------------------------\0A\00", align 1
@.str.6 = private unnamed_addr constant [49 x i8] c"Profiling: %s (total running time: %.3f ms) ---\0A\00", align 1
@.str.7 = private unnamed_addr constant [50 x i8] c"%d -- %s (%.2f %%) :: %.3f ms / frame (%d calls)\0A\00", align 1
@.str.8 = private unnamed_addr constant [25 x i8] c"%s (%.3f %%) :: %.3f ms\0A\00", align 1
@.str.9 = private unnamed_addr constant [13 x i8] c"Unaccounted:\00", align 1
@_ZZ33btQuickprofGetCurrentThreadIndex2vE14gThreadCounter = internal global i32 0, align 4
@_ZL13bts_enterFunc = internal global void (i8*)* @_Z25btEnterProfileZoneDefaultPKc, align 4
@_ZL13bts_leaveFunc = internal global void ()* @_Z25btLeaveProfileZoneDefaultv, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btQuickprof.cpp, i8* null }]

@_ZN7btClockC1Ev = hidden unnamed_addr alias %class.btClock* (%class.btClock*), %class.btClock* (%class.btClock*)* @_ZN7btClockC2Ev
@_ZN7btClockD1Ev = hidden unnamed_addr alias %class.btClock* (%class.btClock*), %class.btClock* (%class.btClock*)* @_ZN7btClockD2Ev
@_ZN7btClockC1ERKS_ = hidden unnamed_addr alias %class.btClock* (%class.btClock*, %class.btClock*), %class.btClock* (%class.btClock*, %class.btClock*)* @_ZN7btClockC2ERKS_
@_ZN12CProfileNodeC1EPKcPS_ = hidden unnamed_addr alias %class.CProfileNode* (%class.CProfileNode*, i8*, %class.CProfileNode*), %class.CProfileNode* (%class.CProfileNode*, i8*, %class.CProfileNode*)* @_ZN12CProfileNodeC2EPKcPS_
@_ZN12CProfileNodeD1Ev = hidden unnamed_addr alias %class.CProfileNode* (%class.CProfileNode*), %class.CProfileNode* (%class.CProfileNode*)* @_ZN12CProfileNodeD2Ev
@_ZN16CProfileIteratorC1EP12CProfileNode = hidden unnamed_addr alias %class.CProfileIterator* (%class.CProfileIterator*, %class.CProfileNode*), %class.CProfileIterator* (%class.CProfileIterator*, %class.CProfileNode*)* @_ZN16CProfileIteratorC2EP12CProfileNode
@_ZN14CProfileSampleC1EPKc = hidden unnamed_addr alias %class.CProfileSample* (%class.CProfileSample*, i8*), %class.CProfileSample* (%class.CProfileSample*, i8*)* @_ZN14CProfileSampleC2EPKc
@_ZN14CProfileSampleD1Ev = hidden unnamed_addr alias %class.CProfileSample* (%class.CProfileSample*), %class.CProfileSample* (%class.CProfileSample*)* @_ZN14CProfileSampleD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btClock* @_ZN7btClockC2Ev(%class.btClock* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 8) #8
  %0 = bitcast i8* %call to %struct.btClockData*
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  store %struct.btClockData* %0, %struct.btClockData** %m_data, align 4
  call void @_ZN7btClock5resetEv(%class.btClock* %this1)
  ret %class.btClock* %this1
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN7btClock5resetEv(%class.btClock* %this) #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %0 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %mStartTime = getelementptr inbounds %struct.btClockData, %struct.btClockData* %0, i32 0, i32 0
  %call = call i32 @gettimeofday(%struct.timeval* %mStartTime, i8* null)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btClock* @_ZN7btClockD2Ev(%class.btClock* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btClock*, align 4
  %this.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  store %class.btClock* %this1, %class.btClock** %retval, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %0 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %isnull = icmp eq %struct.btClockData* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %struct.btClockData* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  %2 = load %class.btClock*, %class.btClock** %retval, align 4
  ret %class.btClock* %2
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define hidden %class.btClock* @_ZN7btClockC2ERKS_(%class.btClock* returned %this, %class.btClock* nonnull align 4 dereferenceable(4) %other) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  %other.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  store %class.btClock* %other, %class.btClock** %other.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 8) #8
  %0 = bitcast i8* %call to %struct.btClockData*
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  store %struct.btClockData* %0, %struct.btClockData** %m_data, align 4
  %1 = load %class.btClock*, %class.btClock** %other.addr, align 4
  %m_data2 = getelementptr inbounds %class.btClock, %class.btClock* %1, i32 0, i32 0
  %2 = load %struct.btClockData*, %struct.btClockData** %m_data2, align 4
  %m_data3 = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %3 = load %struct.btClockData*, %struct.btClockData** %m_data3, align 4
  %4 = bitcast %struct.btClockData* %3 to i8*
  %5 = bitcast %struct.btClockData* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 8, i1 false)
  ret %class.btClock* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define hidden nonnull align 4 dereferenceable(4) %class.btClock* @_ZN7btClockaSERKS_(%class.btClock* %this, %class.btClock* nonnull align 4 dereferenceable(4) %other) #1 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  %other.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  store %class.btClock* %other, %class.btClock** %other.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %0 = load %class.btClock*, %class.btClock** %other.addr, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %0, i32 0, i32 0
  %1 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %m_data2 = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %2 = load %struct.btClockData*, %struct.btClockData** %m_data2, align 4
  %3 = bitcast %struct.btClockData* %2 to i8*
  %4 = bitcast %struct.btClockData* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 8, i1 false)
  ret %class.btClock* %this1
}

declare i32 @gettimeofday(%struct.timeval*, i8*) #6

; Function Attrs: noinline optnone
define hidden i64 @_ZN7btClock19getTimeMillisecondsEv(%class.btClock* %this) #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  %currentTime = alloca %struct.timeval, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call i32 @gettimeofday(%struct.timeval* %currentTime, i8* null)
  %tv_sec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 0
  %0 = load i32, i32* %tv_sec, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %1 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %mStartTime = getelementptr inbounds %struct.btClockData, %struct.btClockData* %1, i32 0, i32 0
  %tv_sec2 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime, i32 0, i32 0
  %2 = load i32, i32* %tv_sec2, align 4
  %sub = sub nsw i32 %0, %2
  %mul = mul nsw i32 %sub, 1000
  %tv_usec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 1
  %3 = load i32, i32* %tv_usec, align 4
  %m_data3 = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %4 = load %struct.btClockData*, %struct.btClockData** %m_data3, align 4
  %mStartTime4 = getelementptr inbounds %struct.btClockData, %struct.btClockData* %4, i32 0, i32 0
  %tv_usec5 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime4, i32 0, i32 1
  %5 = load i32, i32* %tv_usec5, align 4
  %sub6 = sub nsw i32 %3, %5
  %div = sdiv i32 %sub6, 1000
  %add = add nsw i32 %mul, %div
  %conv = sext i32 %add to i64
  ret i64 %conv
}

; Function Attrs: noinline optnone
define hidden i64 @_ZN7btClock19getTimeMicrosecondsEv(%class.btClock* %this) #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  %currentTime = alloca %struct.timeval, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call i32 @gettimeofday(%struct.timeval* %currentTime, i8* null)
  %tv_sec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 0
  %0 = load i32, i32* %tv_sec, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %1 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %mStartTime = getelementptr inbounds %struct.btClockData, %struct.btClockData* %1, i32 0, i32 0
  %tv_sec2 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime, i32 0, i32 0
  %2 = load i32, i32* %tv_sec2, align 4
  %sub = sub nsw i32 %0, %2
  %mul = mul nsw i32 %sub, 1000000
  %tv_usec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 1
  %3 = load i32, i32* %tv_usec, align 4
  %m_data3 = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %4 = load %struct.btClockData*, %struct.btClockData** %m_data3, align 4
  %mStartTime4 = getelementptr inbounds %struct.btClockData, %struct.btClockData* %4, i32 0, i32 0
  %tv_usec5 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime4, i32 0, i32 1
  %5 = load i32, i32* %tv_usec5, align 4
  %sub6 = sub nsw i32 %3, %5
  %add = add nsw i32 %mul, %sub6
  %conv = sext i32 %add to i64
  ret i64 %conv
}

; Function Attrs: noinline optnone
define hidden i64 @_ZN7btClock18getTimeNanosecondsEv(%class.btClock* %this) #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  %currentTime = alloca %struct.timeval, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call i32 @gettimeofday(%struct.timeval* %currentTime, i8* null)
  %tv_sec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 0
  %0 = load i32, i32* %tv_sec, align 4
  %m_data = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %1 = load %struct.btClockData*, %struct.btClockData** %m_data, align 4
  %mStartTime = getelementptr inbounds %struct.btClockData, %struct.btClockData* %1, i32 0, i32 0
  %tv_sec2 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime, i32 0, i32 0
  %2 = load i32, i32* %tv_sec2, align 4
  %sub = sub nsw i32 %0, %2
  %conv = sitofp i32 %sub to double
  %mul = fmul double %conv, 1.000000e+09
  %tv_usec = getelementptr inbounds %struct.timeval, %struct.timeval* %currentTime, i32 0, i32 1
  %3 = load i32, i32* %tv_usec, align 4
  %m_data3 = getelementptr inbounds %class.btClock, %class.btClock* %this1, i32 0, i32 0
  %4 = load %struct.btClockData*, %struct.btClockData** %m_data3, align 4
  %mStartTime4 = getelementptr inbounds %struct.btClockData, %struct.btClockData* %4, i32 0, i32 0
  %tv_usec5 = getelementptr inbounds %struct.timeval, %struct.timeval* %mStartTime4, i32 0, i32 1
  %5 = load i32, i32* %tv_usec5, align 4
  %sub6 = sub nsw i32 %3, %5
  %mul7 = mul nsw i32 %sub6, 1000
  %conv8 = sitofp i32 %mul7 to double
  %add = fadd double %mul, %conv8
  %conv9 = fptoui double %add to i64
  ret i64 %conv9
}

; Function Attrs: noinline optnone
define hidden float @_ZN7btClock14getTimeSecondsEv(%class.btClock* %this) #2 {
entry:
  %this.addr = alloca %class.btClock*, align 4
  store %class.btClock* %this, %class.btClock** %this.addr, align 4
  %this1 = load %class.btClock*, %class.btClock** %this.addr, align 4
  %call = call i64 @_ZN7btClock19getTimeMicrosecondsEv(%class.btClock* %this1)
  %conv = uitofp i64 %call to float
  %mul = fmul float %conv, 0x3EB0C6F7A0000000
  ret float %mul
}

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %call = call %class.btClock* @_ZN7btClockC1Ev(%class.btClock* @_ZL13gProfileClock)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #7
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %class.btClock* @_ZN7btClockD1Ev(%class.btClock* @_ZL13gProfileClock) #7
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #7

; Function Attrs: noinline optnone
define hidden %class.CProfileNode* @_ZN12CProfileNodeC2EPKcPS_(%class.CProfileNode* returned %this, i8* %name, %class.CProfileNode* %parent) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  %name.addr = alloca i8*, align 4
  %parent.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  store i8* %name, i8** %name.addr, align 4
  store %class.CProfileNode* %parent, %class.CProfileNode** %parent.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Name = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 0
  %0 = load i8*, i8** %name.addr, align 4
  store i8* %0, i8** %Name, align 4
  %TotalCalls = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 1
  store i32 0, i32* %TotalCalls, align 4
  %TotalTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %TotalTime, align 4
  %StartTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 3
  store i32 0, i32* %StartTime, align 4
  %RecursionCounter = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 4
  store i32 0, i32* %RecursionCounter, align 4
  %Parent = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 5
  %1 = load %class.CProfileNode*, %class.CProfileNode** %parent.addr, align 4
  store %class.CProfileNode* %1, %class.CProfileNode** %Parent, align 4
  %Child = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  store %class.CProfileNode* null, %class.CProfileNode** %Child, align 4
  %Sibling = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  store %class.CProfileNode* null, %class.CProfileNode** %Sibling, align 4
  %m_userPtr = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 8
  store i8* null, i8** %m_userPtr, align 4
  call void @_ZN12CProfileNode5ResetEv(%class.CProfileNode* %this1)
  ret %class.CProfileNode* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN12CProfileNode5ResetEv(%class.CProfileNode* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %TotalCalls = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 1
  store i32 0, i32* %TotalCalls, align 4
  %TotalTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %TotalTime, align 4
  %Child = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Child, align 4
  %tobool = icmp ne %class.CProfileNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %Child2 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %1 = load %class.CProfileNode*, %class.CProfileNode** %Child2, align 4
  call void @_ZN12CProfileNode5ResetEv(%class.CProfileNode* %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %Sibling = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  %2 = load %class.CProfileNode*, %class.CProfileNode** %Sibling, align 4
  %tobool3 = icmp ne %class.CProfileNode* %2, null
  br i1 %tobool3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %Sibling5 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  %3 = load %class.CProfileNode*, %class.CProfileNode** %Sibling5, align 4
  call void @_ZN12CProfileNode5ResetEv(%class.CProfileNode* %3)
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN12CProfileNode13CleanupMemoryEv(%class.CProfileNode* %this) #1 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Child = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Child, align 4
  %isnull = icmp eq %class.CProfileNode* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %class.CProfileNode* @_ZN12CProfileNodeD1Ev(%class.CProfileNode* %0) #7
  %1 = bitcast %class.CProfileNode* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  %Child2 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  store %class.CProfileNode* null, %class.CProfileNode** %Child2, align 4
  %Sibling = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  %2 = load %class.CProfileNode*, %class.CProfileNode** %Sibling, align 4
  %isnull3 = icmp eq %class.CProfileNode* %2, null
  br i1 %isnull3, label %delete.end6, label %delete.notnull4

delete.notnull4:                                  ; preds = %delete.end
  %call5 = call %class.CProfileNode* @_ZN12CProfileNodeD1Ev(%class.CProfileNode* %2) #7
  %3 = bitcast %class.CProfileNode* %2 to i8*
  call void @_ZdlPv(i8* %3) #9
  br label %delete.end6

delete.end6:                                      ; preds = %delete.notnull4, %delete.end
  %Sibling7 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  store %class.CProfileNode* null, %class.CProfileNode** %Sibling7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.CProfileNode* @_ZN12CProfileNodeD2Ev(%class.CProfileNode* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  call void @_ZN12CProfileNode13CleanupMemoryEv(%class.CProfileNode* %this1)
  ret %class.CProfileNode* %this1
}

; Function Attrs: noinline optnone
define hidden %class.CProfileNode* @_ZN12CProfileNode12Get_Sub_NodeEPKc(%class.CProfileNode* %this, i8* %name) #2 {
entry:
  %retval = alloca %class.CProfileNode*, align 4
  %this.addr = alloca %class.CProfileNode*, align 4
  %name.addr = alloca i8*, align 4
  %child = alloca %class.CProfileNode*, align 4
  %node = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  store i8* %name, i8** %name.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Child = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Child, align 4
  store %class.CProfileNode* %0, %class.CProfileNode** %child, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %1 = load %class.CProfileNode*, %class.CProfileNode** %child, align 4
  %tobool = icmp ne %class.CProfileNode* %1, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %class.CProfileNode*, %class.CProfileNode** %child, align 4
  %Name = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %2, i32 0, i32 0
  %3 = load i8*, i8** %Name, align 4
  %4 = load i8*, i8** %name.addr, align 4
  %cmp = icmp eq i8* %3, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %5 = load %class.CProfileNode*, %class.CProfileNode** %child, align 4
  store %class.CProfileNode* %5, %class.CProfileNode** %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %6 = load %class.CProfileNode*, %class.CProfileNode** %child, align 4
  %Sibling = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %6, i32 0, i32 7
  %7 = load %class.CProfileNode*, %class.CProfileNode** %Sibling, align 4
  store %class.CProfileNode* %7, %class.CProfileNode** %child, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %call = call noalias nonnull i8* @_Znwm(i32 36) #8
  %8 = bitcast i8* %call to %class.CProfileNode*
  %9 = load i8*, i8** %name.addr, align 4
  %call2 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* %8, i8* %9, %class.CProfileNode* %this1)
  store %class.CProfileNode* %8, %class.CProfileNode** %node, align 4
  %Child3 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %10 = load %class.CProfileNode*, %class.CProfileNode** %Child3, align 4
  %11 = load %class.CProfileNode*, %class.CProfileNode** %node, align 4
  %Sibling4 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %11, i32 0, i32 7
  store %class.CProfileNode* %10, %class.CProfileNode** %Sibling4, align 4
  %12 = load %class.CProfileNode*, %class.CProfileNode** %node, align 4
  %Child5 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  store %class.CProfileNode* %12, %class.CProfileNode** %Child5, align 4
  %13 = load %class.CProfileNode*, %class.CProfileNode** %node, align 4
  store %class.CProfileNode* %13, %class.CProfileNode** %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %14 = load %class.CProfileNode*, %class.CProfileNode** %retval, align 4
  ret %class.CProfileNode* %14
}

; Function Attrs: noinline optnone
define hidden void @_ZN12CProfileNode4CallEv(%class.CProfileNode* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %TotalCalls = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 1
  %0 = load i32, i32* %TotalCalls, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %TotalCalls, align 4
  %RecursionCounter = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 4
  %1 = load i32, i32* %RecursionCounter, align 4
  %inc2 = add nsw i32 %1, 1
  store i32 %inc2, i32* %RecursionCounter, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %StartTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 3
  call void @_Z17Profile_Get_TicksPm(i32* %StartTime)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z17Profile_Get_TicksPm(i32* %ticks) #2 comdat {
entry:
  %ticks.addr = alloca i32*, align 4
  store i32* %ticks, i32** %ticks.addr, align 4
  %call = call i64 @_ZN7btClock19getTimeMicrosecondsEv(%class.btClock* @_ZL13gProfileClock)
  %conv = trunc i64 %call to i32
  %0 = load i32*, i32** %ticks.addr, align 4
  store i32 %conv, i32* %0, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN12CProfileNode6ReturnEv(%class.CProfileNode* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  %time = alloca i32, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %RecursionCounter = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 4
  %0 = load i32, i32* %RecursionCounter, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %RecursionCounter, align 4
  %cmp = icmp eq i32 %dec, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %TotalCalls = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 1
  %1 = load i32, i32* %TotalCalls, align 4
  %cmp2 = icmp ne i32 %1, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  call void @_Z17Profile_Get_TicksPm(i32* %time)
  %StartTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 3
  %2 = load i32, i32* %StartTime, align 4
  %3 = load i32, i32* %time, align 4
  %sub = sub i32 %3, %2
  store i32 %sub, i32* %time, align 4
  %4 = load i32, i32* %time, align 4
  %conv = uitofp i32 %4 to float
  %call = call float @_Z21Profile_Get_Tick_Ratev()
  %div = fdiv float %conv, %call
  %TotalTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 2
  %5 = load float, float* %TotalTime, align 4
  %add = fadd float %5, %div
  store float %add, float* %TotalTime, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %RecursionCounter3 = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 4
  %6 = load i32, i32* %RecursionCounter3, align 4
  %cmp4 = icmp eq i32 %6, 0
  ret i1 %cmp4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z21Profile_Get_Tick_Ratev() #1 comdat {
entry:
  ret float 1.000000e+03
}

; Function Attrs: noinline optnone
define hidden %class.CProfileIterator* @_ZN16CProfileIteratorC2EP12CProfileNode(%class.CProfileIterator* returned %this, %class.CProfileNode* %start) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  %start.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  store %class.CProfileNode* %start, %class.CProfileNode** %start.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %0 = load %class.CProfileNode*, %class.CProfileNode** %start.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  store %class.CProfileNode* %0, %class.CProfileNode** %CurrentParent, align 4
  %CurrentParent2 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %1 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent2, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %1)
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call, %class.CProfileNode** %CurrentChild, align 4
  ret %class.CProfileIterator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Child = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 6
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Child, align 4
  ret %class.CProfileNode* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN16CProfileIterator5FirstEv(%class.CProfileIterator* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %0)
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call, %class.CProfileNode** %CurrentChild, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16CProfileIterator4NextEv(%class.CProfileIterator* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode11Get_SiblingEv(%class.CProfileNode* %0)
  %CurrentChild2 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call, %class.CProfileNode** %CurrentChild2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.CProfileNode* @_ZN12CProfileNode11Get_SiblingEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Sibling = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 7
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Sibling, align 4
  ret %class.CProfileNode* %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN16CProfileIterator7Is_DoneEv(%class.CProfileIterator* %this) #1 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild, align 4
  %cmp = icmp eq %class.CProfileNode* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define hidden void @_ZN16CProfileIterator11Enter_ChildEi(%class.CProfileIterator* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  %index.addr = alloca i32, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %0)
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call, %class.CProfileNode** %CurrentChild, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %CurrentChild2 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %1 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild2, align 4
  %cmp = icmp ne %class.CProfileNode* %1, null
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load i32, i32* %index.addr, align 4
  %cmp3 = icmp ne i32 %2, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %3 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %3, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %4 = load i32, i32* %index.addr, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %index.addr, align 4
  %CurrentChild4 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %5 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild4, align 4
  %call5 = call %class.CProfileNode* @_ZN12CProfileNode11Get_SiblingEv(%class.CProfileNode* %5)
  %CurrentChild6 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call5, %class.CProfileNode** %CurrentChild6, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %CurrentChild7 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %6 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild7, align 4
  %cmp8 = icmp ne %class.CProfileNode* %6, null
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %CurrentChild9 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %7 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild9, align 4
  %CurrentParent10 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  store %class.CProfileNode* %7, %class.CProfileNode** %CurrentParent10, align 4
  %CurrentParent11 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %8 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent11, align 4
  %call12 = call %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %8)
  %CurrentChild13 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call12, %class.CProfileNode** %CurrentChild13, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16CProfileIterator12Enter_ParentEv(%class.CProfileIterator* %this) #2 {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode10Get_ParentEv(%class.CProfileNode* %0)
  %cmp = icmp ne %class.CProfileNode* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %CurrentParent2 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %1 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent2, align 4
  %call3 = call %class.CProfileNode* @_ZN12CProfileNode10Get_ParentEv(%class.CProfileNode* %1)
  %CurrentParent4 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  store %class.CProfileNode* %call3, %class.CProfileNode** %CurrentParent4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %CurrentParent5 = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %2 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent5, align 4
  %call6 = call %class.CProfileNode* @_ZN12CProfileNode9Get_ChildEv(%class.CProfileNode* %2)
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  store %class.CProfileNode* %call6, %class.CProfileNode** %CurrentChild, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.CProfileNode* @_ZN12CProfileNode10Get_ParentEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Parent = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 5
  %0 = load %class.CProfileNode*, %class.CProfileNode** %Parent, align 4
  ret %class.CProfileNode* %0
}

; Function Attrs: noinline
define internal void @__cxx_global_var_init.2() #0 {
entry:
  %arrayinit.endOfInit = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 0), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 1), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call1 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 1), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 2), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call2 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 2), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 3), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call3 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 3), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 4), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call4 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 4), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 5), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call5 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 5), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 6), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call6 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 6), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 7), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call7 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 7), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 8), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call8 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 8), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 9), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call9 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 9), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 10), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call10 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 10), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 11), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call11 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 11), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 12), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call12 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 12), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 13), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call13 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 13), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 14), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call14 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 14), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 15), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call15 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 15), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 16), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call16 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 16), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 17), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call17 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 17), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 18), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call18 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 18), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 19), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call19 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 19), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 20), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call20 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 20), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 21), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call21 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 21), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 22), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call22 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 22), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 23), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call23 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 23), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 24), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call24 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 24), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 25), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call25 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 25), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 26), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call26 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 26), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 27), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call27 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 27), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 28), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call28 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 28), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 29), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call29 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 29), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 30), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call30 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 30), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 31), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call31 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 31), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 32), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call32 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 32), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 33), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call33 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 33), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 34), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call34 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 34), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 35), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call35 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 35), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 36), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call36 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 36), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 37), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call37 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 37), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 38), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call38 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 38), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 39), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call39 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 39), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 40), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call40 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 40), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 41), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call41 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 41), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 42), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call42 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 42), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 43), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call43 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 43), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 44), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call44 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 44), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 45), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call45 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 45), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 46), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call46 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 46), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 47), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call47 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 47), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 48), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call48 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 48), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 49), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call49 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 49), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 50), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call50 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 50), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 51), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call51 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 51), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 52), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call52 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 52), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 53), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call53 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 53), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 54), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call54 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 54), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 55), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call55 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 55), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 56), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call56 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 56), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 57), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call57 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 57), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 58), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call58 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 58), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 59), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call59 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 59), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 60), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call60 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 60), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 61), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call61 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 61), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 62), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call62 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 62), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  store %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 63), %class.CProfileNode** %arrayinit.endOfInit, align 4
  %call63 = call %class.CProfileNode* @_ZN12CProfileNodeC1EPKcPS_(%class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 63), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str, i32 0, i32 0), %class.CProfileNode* null)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.3, i8* null, i8* @__dso_handle) #7
  ret void
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor.3(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %entry
  %arraydestroy.elementPast = phi %class.CProfileNode* [ getelementptr inbounds (%class.CProfileNode, %class.CProfileNode* getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 0), i32 64), %entry ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %arraydestroy.elementPast, i32 -1
  %call = call %class.CProfileNode* @_ZN12CProfileNodeD1Ev(%class.CProfileNode* %arraydestroy.element) #7
  %arraydestroy.done = icmp eq %class.CProfileNode* %arraydestroy.element, getelementptr inbounds ([64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 0)
  br i1 %arraydestroy.done, label %arraydestroy.done1, label %arraydestroy.body

arraydestroy.done1:                               ; preds = %arraydestroy.body
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.CProfileIterator* @_ZN15CProfileManager12Get_IteratorEv() #2 {
entry:
  %retval = alloca %class.CProfileIterator*, align 4
  %threadIndex = alloca i32, align 4
  %call = call i32 @_Z33btQuickprofGetCurrentThreadIndex2v()
  store i32 %call, i32* %threadIndex, align 4
  %0 = load i32, i32* %threadIndex, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %class.CProfileIterator* null, %class.CProfileIterator** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call1 = call noalias nonnull i8* @_Znwm(i32 8) #8
  %1 = bitcast i8* %call1 to %class.CProfileIterator*
  %2 = load i32, i32* %threadIndex, align 4
  %arrayidx = getelementptr inbounds [64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 %2
  %call2 = call %class.CProfileIterator* @_ZN16CProfileIteratorC1EP12CProfileNode(%class.CProfileIterator* %1, %class.CProfileNode* %arrayidx)
  store %class.CProfileIterator* %1, %class.CProfileIterator** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.CProfileIterator*, %class.CProfileIterator** %retval, align 4
  ret %class.CProfileIterator* %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_Z33btQuickprofGetCurrentThreadIndex2v() #1 {
entry:
  %kNullIndex = alloca i32, align 4
  %sThreadIndex = alloca i32, align 4
  store i32 -1, i32* %kNullIndex, align 4
  store i32 0, i32* %sThreadIndex, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN15CProfileManager13CleanupMemoryEv() #1 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 64
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 %1
  call void @_ZN12CProfileNode13CleanupMemoryEv(%class.CProfileNode* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %name) #2 {
entry:
  %name.addr = alloca i8*, align 4
  %threadIndex = alloca i32, align 4
  store i8* %name, i8** %name.addr, align 4
  %call = call i32 @_Z33btQuickprofGetCurrentThreadIndex2v()
  store i32 %call, i32* %threadIndex, align 4
  %0 = load i32, i32* %threadIndex, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %name.addr, align 4
  %2 = load i32, i32* %threadIndex, align 4
  %arrayidx = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %2
  %3 = load %class.CProfileNode*, %class.CProfileNode** %arrayidx, align 4
  %call1 = call i8* @_ZN12CProfileNode8Get_NameEv(%class.CProfileNode* %3)
  %cmp2 = icmp ne i8* %1, %call1
  br i1 %cmp2, label %if.then3, label %if.end7

if.then3:                                         ; preds = %if.end
  %4 = load i32, i32* %threadIndex, align 4
  %arrayidx4 = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %4
  %5 = load %class.CProfileNode*, %class.CProfileNode** %arrayidx4, align 4
  %6 = load i8*, i8** %name.addr, align 4
  %call5 = call %class.CProfileNode* @_ZN12CProfileNode12Get_Sub_NodeEPKc(%class.CProfileNode* %5, i8* %6)
  %7 = load i32, i32* %threadIndex, align 4
  %arrayidx6 = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %7
  store %class.CProfileNode* %call5, %class.CProfileNode** %arrayidx6, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then3, %if.end
  %8 = load i32, i32* %threadIndex, align 4
  %arrayidx8 = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %8
  %9 = load %class.CProfileNode*, %class.CProfileNode** %arrayidx8, align 4
  call void @_ZN12CProfileNode4CallEv(%class.CProfileNode* %9)
  br label %return

return:                                           ; preds = %if.end7, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN12CProfileNode8Get_NameEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %Name = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 0
  %0 = load i8*, i8** %Name, align 4
  ret i8* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN15CProfileManager12Stop_ProfileEv() #2 {
entry:
  %threadIndex = alloca i32, align 4
  %call = call i32 @_Z33btQuickprofGetCurrentThreadIndex2v()
  store i32 %call, i32* %threadIndex, align 4
  %0 = load i32, i32* %threadIndex, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end6

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %threadIndex, align 4
  %arrayidx = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %1
  %2 = load %class.CProfileNode*, %class.CProfileNode** %arrayidx, align 4
  %call1 = call zeroext i1 @_ZN12CProfileNode6ReturnEv(%class.CProfileNode* %2)
  br i1 %call1, label %if.then2, label %if.end6

if.then2:                                         ; preds = %if.end
  %3 = load i32, i32* %threadIndex, align 4
  %arrayidx3 = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %3
  %4 = load %class.CProfileNode*, %class.CProfileNode** %arrayidx3, align 4
  %call4 = call %class.CProfileNode* @_ZN12CProfileNode10Get_ParentEv(%class.CProfileNode* %4)
  %5 = load i32, i32* %threadIndex, align 4
  %arrayidx5 = getelementptr inbounds [64 x %class.CProfileNode*], [64 x %class.CProfileNode*]* @gCurrentNodes, i32 0, i32 %5
  store %class.CProfileNode* %call4, %class.CProfileNode** %arrayidx5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then, %if.then2, %if.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15CProfileManager5ResetEv() #2 {
entry:
  %threadIndex = alloca i32, align 4
  call void @_ZN7btClock5resetEv(%class.btClock* @_ZL13gProfileClock)
  %call = call i32 @_Z33btQuickprofGetCurrentThreadIndex2v()
  store i32 %call, i32* %threadIndex, align 4
  %0 = load i32, i32* %threadIndex, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %threadIndex, align 4
  %arrayidx = getelementptr inbounds [64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 %1
  call void @_ZN12CProfileNode5ResetEv(%class.CProfileNode* %arrayidx)
  %2 = load i32, i32* %threadIndex, align 4
  %arrayidx1 = getelementptr inbounds [64 x %class.CProfileNode], [64 x %class.CProfileNode]* @gRoots, i32 0, i32 %2
  call void @_ZN12CProfileNode4CallEv(%class.CProfileNode* %arrayidx1)
  store i32 0, i32* @_ZN15CProfileManager12FrameCounterE, align 4
  call void @_Z17Profile_Get_TicksPm(i32* @_ZN15CProfileManager9ResetTimeE)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN15CProfileManager23Increment_Frame_CounterEv() #1 {
entry:
  %0 = load i32, i32* @_ZN15CProfileManager12FrameCounterE, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @_ZN15CProfileManager12FrameCounterE, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN15CProfileManager20Get_Time_Since_ResetEv() #2 {
entry:
  %time = alloca i32, align 4
  call void @_Z17Profile_Get_TicksPm(i32* %time)
  %0 = load i32, i32* @_ZN15CProfileManager9ResetTimeE, align 4
  %1 = load i32, i32* %time, align 4
  %sub = sub i32 %1, %0
  store i32 %sub, i32* %time, align 4
  %2 = load i32, i32* %time, align 4
  %conv = uitofp i32 %2 to float
  %call = call float @_Z21Profile_Get_Tick_Ratev()
  %div = fdiv float %conv, %call
  ret float %div
}

; Function Attrs: noinline optnone
define hidden void @_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori(%class.CProfileIterator* %profileIterator, i32 %spacing) #2 {
entry:
  %profileIterator.addr = alloca %class.CProfileIterator*, align 4
  %spacing.addr = alloca i32, align 4
  %accumulated_time = alloca float, align 4
  %parent_time = alloca float, align 4
  %i = alloca i32, align 4
  %frames_since_reset = alloca i32, align 4
  %totalTime = alloca float, align 4
  %numChildren = alloca i32, align 4
  %current_total_time = alloca float, align 4
  %fraction = alloca float, align 4
  %i26 = alloca i32, align 4
  store %class.CProfileIterator* %profileIterator, %class.CProfileIterator** %profileIterator.addr, align 4
  store i32 %spacing, i32* %spacing.addr, align 4
  %0 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  call void @_ZN16CProfileIterator5FirstEv(%class.CProfileIterator* %0)
  %1 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call = call zeroext i1 @_ZN16CProfileIterator7Is_DoneEv(%class.CProfileIterator* %1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end72

if.end:                                           ; preds = %entry
  store float 0.000000e+00, float* %accumulated_time, align 4
  %2 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call1 = call zeroext i1 @_ZN16CProfileIterator7Is_RootEv(%class.CProfileIterator* %2)
  br i1 %call1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %call2 = call float @_ZN15CProfileManager20Get_Time_Since_ResetEv()
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %3 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call3 = call float @_ZN16CProfileIterator29Get_Current_Parent_Total_TimeEv(%class.CProfileIterator* %3)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %call2, %cond.true ], [ %call3, %cond.false ]
  store float %cond, float* %parent_time, align 4
  %call4 = call i32 @_ZN15CProfileManager27Get_Frame_Count_Since_ResetEv()
  store i32 %call4, i32* %frames_since_reset, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %spacing.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.5, i32 0, i32 0))
  store i32 0, i32* %i, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc11, %for.end
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %spacing.addr, align 4
  %cmp8 = icmp slt i32 %7, %8
  br i1 %cmp8, label %for.body9, label %for.end13

for.body9:                                        ; preds = %for.cond7
  %call10 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %for.inc11

for.inc11:                                        ; preds = %for.body9
  %9 = load i32, i32* %i, align 4
  %inc12 = add nsw i32 %9, 1
  store i32 %inc12, i32* %i, align 4
  br label %for.cond7

for.end13:                                        ; preds = %for.cond7
  %10 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call14 = call i8* @_ZN16CProfileIterator23Get_Current_Parent_NameEv(%class.CProfileIterator* %10)
  %11 = load float, float* %parent_time, align 4
  %conv = fpext float %11 to double
  %call15 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.6, i32 0, i32 0), i8* %call14, double %conv)
  store float 0.000000e+00, float* %totalTime, align 4
  store i32 0, i32* %numChildren, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc42, %for.end13
  %12 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call17 = call zeroext i1 @_ZN16CProfileIterator7Is_DoneEv(%class.CProfileIterator* %12)
  %lnot = xor i1 %call17, true
  br i1 %lnot, label %for.body18, label %for.end44

for.body18:                                       ; preds = %for.cond16
  %13 = load i32, i32* %numChildren, align 4
  %inc19 = add nsw i32 %13, 1
  store i32 %inc19, i32* %numChildren, align 4
  %14 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call20 = call float @_ZN16CProfileIterator22Get_Current_Total_TimeEv(%class.CProfileIterator* %14)
  store float %call20, float* %current_total_time, align 4
  %15 = load float, float* %current_total_time, align 4
  %16 = load float, float* %accumulated_time, align 4
  %add = fadd float %16, %15
  store float %add, float* %accumulated_time, align 4
  %17 = load float, float* %parent_time, align 4
  %cmp21 = fcmp ogt float %17, 0x3E80000000000000
  br i1 %cmp21, label %cond.true22, label %cond.false23

cond.true22:                                      ; preds = %for.body18
  %18 = load float, float* %current_total_time, align 4
  %19 = load float, float* %parent_time, align 4
  %div = fdiv float %18, %19
  %mul = fmul float %div, 1.000000e+02
  br label %cond.end24

cond.false23:                                     ; preds = %for.body18
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true22
  %cond25 = phi float [ %mul, %cond.true22 ], [ 0.000000e+00, %cond.false23 ]
  store float %cond25, float* %fraction, align 4
  store i32 0, i32* %i26, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc31, %cond.end24
  %20 = load i32, i32* %i26, align 4
  %21 = load i32, i32* %spacing.addr, align 4
  %cmp28 = icmp slt i32 %20, %21
  br i1 %cmp28, label %for.body29, label %for.end33

for.body29:                                       ; preds = %for.cond27
  %call30 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %for.inc31

for.inc31:                                        ; preds = %for.body29
  %22 = load i32, i32* %i26, align 4
  %inc32 = add nsw i32 %22, 1
  store i32 %inc32, i32* %i26, align 4
  br label %for.cond27

for.end33:                                        ; preds = %for.cond27
  %23 = load i32, i32* %i, align 4
  %24 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call34 = call i8* @_ZN16CProfileIterator16Get_Current_NameEv(%class.CProfileIterator* %24)
  %25 = load float, float* %fraction, align 4
  %conv35 = fpext float %25 to double
  %26 = load float, float* %current_total_time, align 4
  %conv36 = fpext float %26 to double
  %27 = load i32, i32* %frames_since_reset, align 4
  %conv37 = sitofp i32 %27 to double
  %div38 = fdiv double %conv36, %conv37
  %28 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %call39 = call i32 @_ZN16CProfileIterator23Get_Current_Total_CallsEv(%class.CProfileIterator* %28)
  %call40 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.7, i32 0, i32 0), i32 %23, i8* %call34, double %conv35, double %div38, i32 %call39)
  %29 = load float, float* %current_total_time, align 4
  %30 = load float, float* %totalTime, align 4
  %add41 = fadd float %30, %29
  store float %add41, float* %totalTime, align 4
  br label %for.inc42

for.inc42:                                        ; preds = %for.end33
  %31 = load i32, i32* %i, align 4
  %inc43 = add nsw i32 %31, 1
  store i32 %inc43, i32* %i, align 4
  %32 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  call void @_ZN16CProfileIterator4NextEv(%class.CProfileIterator* %32)
  br label %for.cond16

for.end44:                                        ; preds = %for.cond16
  %33 = load float, float* %parent_time, align 4
  %34 = load float, float* %accumulated_time, align 4
  %cmp45 = fcmp olt float %33, %34
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %for.end44
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %for.end44
  store i32 0, i32* %i, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc52, %if.end47
  %35 = load i32, i32* %i, align 4
  %36 = load i32, i32* %spacing.addr, align 4
  %cmp49 = icmp slt i32 %35, %36
  br i1 %cmp49, label %for.body50, label %for.end54

for.body50:                                       ; preds = %for.cond48
  %call51 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  br label %for.inc52

for.inc52:                                        ; preds = %for.body50
  %37 = load i32, i32* %i, align 4
  %inc53 = add nsw i32 %37, 1
  store i32 %inc53, i32* %i, align 4
  br label %for.cond48

for.end54:                                        ; preds = %for.cond48
  %38 = load float, float* %parent_time, align 4
  %cmp55 = fcmp ogt float %38, 0x3E80000000000000
  br i1 %cmp55, label %cond.true56, label %cond.false59

cond.true56:                                      ; preds = %for.end54
  %39 = load float, float* %parent_time, align 4
  %40 = load float, float* %accumulated_time, align 4
  %sub = fsub float %39, %40
  %41 = load float, float* %parent_time, align 4
  %div57 = fdiv float %sub, %41
  %mul58 = fmul float %div57, 1.000000e+02
  br label %cond.end60

cond.false59:                                     ; preds = %for.end54
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false59, %cond.true56
  %cond61 = phi float [ %mul58, %cond.true56 ], [ 0.000000e+00, %cond.false59 ]
  %conv62 = fpext float %cond61 to double
  %42 = load float, float* %parent_time, align 4
  %43 = load float, float* %accumulated_time, align 4
  %sub63 = fsub float %42, %43
  %conv64 = fpext float %sub63 to double
  %call65 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.9, i32 0, i32 0), double %conv62, double %conv64)
  store i32 0, i32* %i, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc70, %cond.end60
  %44 = load i32, i32* %i, align 4
  %45 = load i32, i32* %numChildren, align 4
  %cmp67 = icmp slt i32 %44, %45
  br i1 %cmp67, label %for.body68, label %for.end72

for.body68:                                       ; preds = %for.cond66
  %46 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %47 = load i32, i32* %i, align 4
  call void @_ZN16CProfileIterator11Enter_ChildEi(%class.CProfileIterator* %46, i32 %47)
  %48 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  %49 = load i32, i32* %spacing.addr, align 4
  %add69 = add nsw i32 %49, 3
  call void @_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori(%class.CProfileIterator* %48, i32 %add69)
  %50 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator.addr, align 4
  call void @_ZN16CProfileIterator12Enter_ParentEv(%class.CProfileIterator* %50)
  br label %for.inc70

for.inc70:                                        ; preds = %for.body68
  %51 = load i32, i32* %i, align 4
  %inc71 = add nsw i32 %51, 1
  store i32 %inc71, i32* %i, align 4
  br label %for.cond66

for.end72:                                        ; preds = %if.then, %for.cond66
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN16CProfileIterator7Is_RootEv(%class.CProfileIterator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call %class.CProfileNode* @_ZN12CProfileNode10Get_ParentEv(%class.CProfileNode* %0)
  %cmp = icmp eq %class.CProfileNode* %call, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN16CProfileIterator29Get_Current_Parent_Total_TimeEv(%class.CProfileIterator* %this) #2 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call float @_ZN12CProfileNode14Get_Total_TimeEv(%class.CProfileNode* %0)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN15CProfileManager27Get_Frame_Count_Since_ResetEv() #1 comdat {
entry:
  %0 = load i32, i32* @_ZN15CProfileManager12FrameCounterE, align 4
  ret i32 %0
}

declare i32 @printf(i8*, ...) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16CProfileIterator23Get_Current_Parent_NameEv(%class.CProfileIterator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentParent = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 0
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentParent, align 4
  %call = call i8* @_ZN12CProfileNode8Get_NameEv(%class.CProfileNode* %0)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN16CProfileIterator22Get_Current_Total_TimeEv(%class.CProfileIterator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild, align 4
  %call = call float @_ZN12CProfileNode14Get_Total_TimeEv(%class.CProfileNode* %0)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16CProfileIterator16Get_Current_NameEv(%class.CProfileIterator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild, align 4
  %call = call i8* @_ZN12CProfileNode8Get_NameEv(%class.CProfileNode* %0)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN16CProfileIterator23Get_Current_Total_CallsEv(%class.CProfileIterator* %this) #2 comdat {
entry:
  %this.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %this, %class.CProfileIterator** %this.addr, align 4
  %this1 = load %class.CProfileIterator*, %class.CProfileIterator** %this.addr, align 4
  %CurrentChild = getelementptr inbounds %class.CProfileIterator, %class.CProfileIterator* %this1, i32 0, i32 1
  %0 = load %class.CProfileNode*, %class.CProfileNode** %CurrentChild, align 4
  %call = call i32 @_ZN12CProfileNode15Get_Total_CallsEv(%class.CProfileNode* %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN15CProfileManager7dumpAllEv() #2 {
entry:
  %profileIterator = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* null, %class.CProfileIterator** %profileIterator, align 4
  %call = call %class.CProfileIterator* @_ZN15CProfileManager12Get_IteratorEv()
  store %class.CProfileIterator* %call, %class.CProfileIterator** %profileIterator, align 4
  %0 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator, align 4
  call void @_ZN15CProfileManager13dumpRecursiveEP16CProfileIteratori(%class.CProfileIterator* %0, i32 0)
  %1 = load %class.CProfileIterator*, %class.CProfileIterator** %profileIterator, align 4
  call void @_ZN15CProfileManager16Release_IteratorEP16CProfileIterator(%class.CProfileIterator* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15CProfileManager16Release_IteratorEP16CProfileIterator(%class.CProfileIterator* %iterator) #1 comdat {
entry:
  %iterator.addr = alloca %class.CProfileIterator*, align 4
  store %class.CProfileIterator* %iterator, %class.CProfileIterator** %iterator.addr, align 4
  %0 = load %class.CProfileIterator*, %class.CProfileIterator** %iterator.addr, align 4
  %isnull = icmp eq %class.CProfileIterator* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %class.CProfileIterator* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z25btEnterProfileZoneDefaultPKc(i8* %name) #2 {
entry:
  %name.addr = alloca i8*, align 4
  store i8* %name, i8** %name.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z25btLeaveProfileZoneDefaultv() #2 {
entry:
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z18btEnterProfileZonePKc(i8* %name) #2 {
entry:
  %name.addr = alloca i8*, align 4
  store i8* %name, i8** %name.addr, align 4
  %0 = load void (i8*)*, void (i8*)** @_ZL13bts_enterFunc, align 4
  %1 = load i8*, i8** %name.addr, align 4
  call void %0(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z18btLeaveProfileZonev() #2 {
entry:
  %0 = load void ()*, void ()** @_ZL13bts_leaveFunc, align 4
  call void %0()
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void (i8*)* @_Z32btGetCurrentEnterProfileZoneFuncv() #1 {
entry:
  %0 = load void (i8*)*, void (i8*)** @_ZL13bts_enterFunc, align 4
  ret void (i8*)* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void ()* @_Z32btGetCurrentLeaveProfileZoneFuncv() #1 {
entry:
  %0 = load void ()*, void ()** @_ZL13bts_leaveFunc, align 4
  ret void ()* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z31btSetCustomEnterProfileZoneFuncPFvPKcE(void (i8*)* %enterFunc) #1 {
entry:
  %enterFunc.addr = alloca void (i8*)*, align 4
  store void (i8*)* %enterFunc, void (i8*)** %enterFunc.addr, align 4
  %0 = load void (i8*)*, void (i8*)** %enterFunc.addr, align 4
  store void (i8*)* %0, void (i8*)** @_ZL13bts_enterFunc, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z31btSetCustomLeaveProfileZoneFuncPFvvE(void ()* %leaveFunc) #1 {
entry:
  %leaveFunc.addr = alloca void ()*, align 4
  store void ()* %leaveFunc, void ()** %leaveFunc.addr, align 4
  %0 = load void ()*, void ()** %leaveFunc.addr, align 4
  store void ()* %0, void ()** @_ZL13bts_leaveFunc, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4
  store i8* %name, i8** %name.addr, align 4
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4
  call void @_Z18btEnterProfileZonePKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_Z18btLeaveProfileZonev()
  ret %class.CProfileSample* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN12CProfileNode14Get_Total_TimeEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %TotalTime = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 2
  %0 = load float, float* %TotalTime, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN12CProfileNode15Get_Total_CallsEv(%class.CProfileNode* %this) #1 comdat {
entry:
  %this.addr = alloca %class.CProfileNode*, align 4
  store %class.CProfileNode* %this, %class.CProfileNode** %this.addr, align 4
  %this1 = load %class.CProfileNode*, %class.CProfileNode** %this.addr, align 4
  %TotalCalls = getelementptr inbounds %class.CProfileNode, %class.CProfileNode* %this1, i32 0, i32 1
  %0 = load i32, i32* %TotalCalls, align 4
  ret i32 %0
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btQuickprof.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  call void @__cxx_global_var_init.2()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
