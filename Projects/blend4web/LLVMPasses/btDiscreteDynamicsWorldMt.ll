; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btDiscreteDynamicsWorldMt.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btDiscreteDynamicsWorldMt = type { %class.btDiscreteDynamicsWorld, %struct.InplaceSolverIslandCallbackMt* }
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.4, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.19, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.23, i32, i8, [3 x i8], %class.btAlignedObjectArray.13, %class.btSpinMutex }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray.9 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.7, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.7 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.4, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%class.btActionInterface = type { i32 (...)** }
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.16, %union.anon.17, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.16 = type { float }
%union.anon.17 = type { float }
%class.btSpinMutex = type { i32 }
%struct.InplaceSolverIslandCallbackMt = type { %"struct.btSimulationIslandManagerMt::IslandCallback", %struct.btContactSolverInfo*, %class.btConstraintSolver*, %class.btIDebugDraw*, %class.btDispatcher* }
%"struct.btSimulationIslandManagerMt::IslandCallback" = type { i32 (...)** }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type { i32 (...)** }
%class.btCollisionConfiguration = type opaque
%class.btSimulationIslandManagerMt = type { %class.btSimulationIslandManager.base, [3 x i8], %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27, %"struct.btSimulationIslandManagerMt::Island"*, i32, i32, void (%class.btAlignedObjectArray.27*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)* }
%class.btSimulationIslandManager.base = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray, i8 }>
%class.btAlignedObjectArray.27 = type <{ %class.btAlignedAllocator.28, [3 x i8], i32, i32, %"struct.btSimulationIslandManagerMt::Island"**, i8, [3 x i8] }>
%class.btAlignedAllocator.28 = type { i8 }
%"struct.btSimulationIslandManagerMt::Island" = type <{ %class.btAlignedObjectArray, %class.btAlignedObjectArray.13, %class.btAlignedObjectArray.4, i32, i8, [3 x i8] }>
%class.btStackAlloc = type opaque
%class.CProfileSample = type { i8 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }
%class.btSerializer = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN29InplaceSolverIslandCallbackMtC2EP18btConstraintSolverP12btStackAllocP12btDispatcher = comdat any

$_ZN27btSimulationIslandManagerMt25setMinimumSolverBatchSizeEi = comdat any

$_ZN25btDiscreteDynamicsWorldMtdlEPv = comdat any

$_ZN29InplaceSolverIslandCallbackMt5setupEP19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv = comdat any

$_ZNK16btCollisionWorld22getNumCollisionObjectsEv = comdat any

$_ZN16btCollisionWorld13getDispatcherEv = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZN27btSimulationIslandManagerMt14IslandCallbackC2Ev = comdat any

$_ZN29InplaceSolverIslandCallbackMtD2Ev = comdat any

$_ZN29InplaceSolverIslandCallbackMtD0Ev = comdat any

$_ZN29InplaceSolverIslandCallbackMt13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintii = comdat any

$_ZN27btSimulationIslandManagerMt14IslandCallbackD2Ev = comdat any

$_ZN27btSimulationIslandManagerMt14IslandCallbackD0Ev = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZTV29InplaceSolverIslandCallbackMt = comdat any

$_ZTS29InplaceSolverIslandCallbackMt = comdat any

$_ZTSN27btSimulationIslandManagerMt14IslandCallbackE = comdat any

$_ZTIN27btSimulationIslandManagerMt14IslandCallbackE = comdat any

$_ZTI29InplaceSolverIslandCallbackMt = comdat any

$_ZTVN27btSimulationIslandManagerMt14IslandCallbackE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV25btDiscreteDynamicsWorldMt = hidden unnamed_addr constant { [49 x i8*] } { [49 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btDiscreteDynamicsWorldMt to i8*), i8* bitcast (%class.btDiscreteDynamicsWorldMt* (%class.btDiscreteDynamicsWorldMt*)* @_ZN25btDiscreteDynamicsWorldMtD1Ev to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorldMt*)* @_ZN25btDiscreteDynamicsWorldMtD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*)* @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btSerializer*)* @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorldMt*, %struct.btContactSolverInfo*)* @_ZN25btDiscreteDynamicsWorldMt16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*)] }, align 4
@.str = private unnamed_addr constant [17 x i8] c"solveConstraints\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS25btDiscreteDynamicsWorldMt = hidden constant [28 x i8] c"25btDiscreteDynamicsWorldMt\00", align 1
@_ZTI23btDiscreteDynamicsWorld = external constant i8*
@_ZTI25btDiscreteDynamicsWorldMt = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btDiscreteDynamicsWorldMt, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btDiscreteDynamicsWorld to i8*) }, align 4
@_ZTV29InplaceSolverIslandCallbackMt = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI29InplaceSolverIslandCallbackMt to i8*), i8* bitcast (%struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)* @_ZN29InplaceSolverIslandCallbackMtD2Ev to i8*), i8* bitcast (void (%struct.InplaceSolverIslandCallbackMt*)* @_ZN29InplaceSolverIslandCallbackMtD0Ev to i8*), i8* bitcast (void (%struct.InplaceSolverIslandCallbackMt*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)* @_ZN29InplaceSolverIslandCallbackMt13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintii to i8*)] }, comdat, align 4
@_ZTS29InplaceSolverIslandCallbackMt = linkonce_odr hidden constant [32 x i8] c"29InplaceSolverIslandCallbackMt\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN27btSimulationIslandManagerMt14IslandCallbackE = linkonce_odr hidden constant [48 x i8] c"N27btSimulationIslandManagerMt14IslandCallbackE\00", comdat, align 1
@_ZTIN27btSimulationIslandManagerMt14IslandCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN27btSimulationIslandManagerMt14IslandCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTI29InplaceSolverIslandCallbackMt = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @_ZTS29InplaceSolverIslandCallbackMt, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN27btSimulationIslandManagerMt14IslandCallbackE to i8*) }, comdat, align 4
@_ZTVN27btSimulationIslandManagerMt14IslandCallbackE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN27btSimulationIslandManagerMt14IslandCallbackE to i8*), i8* bitcast (%"struct.btSimulationIslandManagerMt::IslandCallback"* (%"struct.btSimulationIslandManagerMt::IslandCallback"*)* @_ZN27btSimulationIslandManagerMt14IslandCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btSimulationIslandManagerMt::IslandCallback"*)* @_ZN27btSimulationIslandManagerMt14IslandCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDiscreteDynamicsWorldMt.cpp, i8* null }]

@_ZN25btDiscreteDynamicsWorldMtC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = hidden unnamed_addr alias %class.btDiscreteDynamicsWorldMt* (%class.btDiscreteDynamicsWorldMt*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*), %class.btDiscreteDynamicsWorldMt* (%class.btDiscreteDynamicsWorldMt*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*)* @_ZN25btDiscreteDynamicsWorldMtC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
@_ZN25btDiscreteDynamicsWorldMtD1Ev = hidden unnamed_addr alias %class.btDiscreteDynamicsWorldMt* (%class.btDiscreteDynamicsWorldMt*), %class.btDiscreteDynamicsWorldMt* (%class.btDiscreteDynamicsWorldMt*)* @_ZN25btDiscreteDynamicsWorldMtD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btDiscreteDynamicsWorldMt* @_ZN25btDiscreteDynamicsWorldMtC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorldMt* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #2 {
entry:
  %retval = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  %this.addr = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %mem = alloca i8*, align 4
  %mem6 = alloca i8*, align 4
  %im = alloca %class.btSimulationIslandManagerMt*, align 4
  store %class.btDiscreteDynamicsWorldMt* %this, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4
  store %class.btConstraintSolver* %constraintSolver, %class.btConstraintSolver** %constraintSolver.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  store %class.btDiscreteDynamicsWorldMt* %this1, %class.btDiscreteDynamicsWorldMt** %retval, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4
  %3 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver.addr, align 4
  %4 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btConstraintSolver* %3, %class.btCollisionConfiguration* %4)
  %5 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV25btDiscreteDynamicsWorldMt, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4
  %6 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_ownsIslandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %6, i32 0, i32 10
  %7 = load i8, i8* %m_ownsIslandManager, align 4
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %8, i32 0, i32 4
  %9 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  %10 = bitcast %class.btSimulationIslandManager* %9 to %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)***
  %vtable = load %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)**, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*** %10, align 4
  %vfn = getelementptr inbounds %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)** %vtable, i64 0
  %11 = load %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)*, %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)** %vfn, align 4
  %call2 = call %class.btSimulationIslandManager* %11(%class.btSimulationIslandManager* %9) #7
  %12 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager3 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %12, i32 0, i32 4
  %13 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager3, align 4
  %14 = bitcast %class.btSimulationIslandManager* %13 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 20, i32 16)
  store i8* %call4, i8** %mem, align 4
  %15 = load i8*, i8** %mem, align 4
  %16 = bitcast i8* %15 to %struct.InplaceSolverIslandCallbackMt*
  %17 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %17, i32 0, i32 3
  %18 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %19 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %call5 = call %struct.InplaceSolverIslandCallbackMt* @_ZN29InplaceSolverIslandCallbackMtC2EP18btConstraintSolverP12btStackAllocP12btDispatcher(%struct.InplaceSolverIslandCallbackMt* %16, %class.btConstraintSolver* %18, %class.btStackAlloc* null, %class.btDispatcher* %19)
  %m_solverIslandCallbackMt = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  store %struct.InplaceSolverIslandCallbackMt* %16, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt, align 4
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 164, i32 16)
  store i8* %call7, i8** %mem6, align 4
  %20 = load i8*, i8** %mem6, align 4
  %21 = bitcast i8* %20 to %class.btSimulationIslandManagerMt*
  %call8 = call %class.btSimulationIslandManagerMt* @_ZN27btSimulationIslandManagerMtC1Ev(%class.btSimulationIslandManagerMt* %21)
  store %class.btSimulationIslandManagerMt* %21, %class.btSimulationIslandManagerMt** %im, align 4
  %22 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %im, align 4
  %23 = bitcast %class.btSimulationIslandManagerMt* %22 to %class.btSimulationIslandManager*
  %24 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager9 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %24, i32 0, i32 4
  store %class.btSimulationIslandManager* %23, %class.btSimulationIslandManager** %m_islandManager9, align 4
  %25 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %im, align 4
  %26 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDynamicsWorld*
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %26, i32 0, i32 4
  %27 = bitcast %struct.btContactSolverInfo* %m_solverInfo to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %27, i32 0, i32 18
  %28 = load i32, i32* %m_minimumSolverBatchSize, align 4
  call void @_ZN27btSimulationIslandManagerMt25setMinimumSolverBatchSizeEi(%class.btSimulationIslandManagerMt* %25, i32 %28)
  %29 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %retval, align 4
  ret %class.btDiscreteDynamicsWorldMt* %29
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #3

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.InplaceSolverIslandCallbackMt* @_ZN29InplaceSolverIslandCallbackMtC2EP18btConstraintSolverP12btStackAllocP12btDispatcher(%struct.InplaceSolverIslandCallbackMt* returned %this, %class.btConstraintSolver* %solver, %class.btStackAlloc* %stackAlloc, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallbackMt*, align 4
  %solver.addr = alloca %class.btConstraintSolver*, align 4
  %stackAlloc.addr = alloca %class.btStackAlloc*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.InplaceSolverIslandCallbackMt* %this, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  store %class.btConstraintSolver* %solver, %class.btConstraintSolver** %solver.addr, align 4
  store %class.btStackAlloc* %stackAlloc, %class.btStackAlloc** %stackAlloc.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %0 = bitcast %struct.InplaceSolverIslandCallbackMt* %this1 to %"struct.btSimulationIslandManagerMt::IslandCallback"*
  %call = call %"struct.btSimulationIslandManagerMt::IslandCallback"* @_ZN27btSimulationIslandManagerMt14IslandCallbackC2Ev(%"struct.btSimulationIslandManagerMt::IslandCallback"* %0) #7
  %1 = bitcast %struct.InplaceSolverIslandCallbackMt* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV29InplaceSolverIslandCallbackMt, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* null, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 2
  %2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %solver.addr, align 4
  store %class.btConstraintSolver* %2, %class.btConstraintSolver** %m_solver, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 3
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 4
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btDispatcher* %3, %class.btDispatcher** %m_dispatcher, align 4
  ret %struct.InplaceSolverIslandCallbackMt* %this1
}

declare %class.btSimulationIslandManagerMt* @_ZN27btSimulationIslandManagerMtC1Ev(%class.btSimulationIslandManagerMt* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27btSimulationIslandManagerMt25setMinimumSolverBatchSizeEi(%class.btSimulationIslandManagerMt* %this, i32 %sz) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %sz.addr = alloca i32, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store i32 %sz, i32* %sz.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = load i32, i32* %sz.addr, align 4
  %m_minimumSolverBatchSize = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 7
  store i32 %0, i32* %m_minimumSolverBatchSize, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btDiscreteDynamicsWorldMt* @_ZN25btDiscreteDynamicsWorldMtD2Ev(%class.btDiscreteDynamicsWorldMt* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  %this.addr = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  store %class.btDiscreteDynamicsWorldMt* %this, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  store %class.btDiscreteDynamicsWorldMt* %this1, %class.btDiscreteDynamicsWorldMt** %retval, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV25btDiscreteDynamicsWorldMt, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_solverIslandCallbackMt = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  %1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt, align 4
  %tobool = icmp ne %struct.InplaceSolverIslandCallbackMt* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_solverIslandCallbackMt2 = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  %2 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt2, align 4
  %3 = bitcast %struct.InplaceSolverIslandCallbackMt* %2 to %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)***
  %vtable = load %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)**, %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)*** %3, align 4
  %vfn = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)*, %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)** %vtable, i64 0
  %4 = load %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)*, %struct.InplaceSolverIslandCallbackMt* (%struct.InplaceSolverIslandCallbackMt*)** %vfn, align 4
  %call = call %struct.InplaceSolverIslandCallbackMt* %4(%struct.InplaceSolverIslandCallbackMt* %2) #7
  %m_solverIslandCallbackMt3 = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  %5 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt3, align 4
  %6 = bitcast %struct.InplaceSolverIslandCallbackMt* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_ownsConstraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %7, i32 0, i32 11
  %8 = load i8, i8* %m_ownsConstraintSolver, align 1
  %tobool4 = trunc i8 %8 to i1
  br i1 %tobool4, label %if.then5, label %if.end10

if.then5:                                         ; preds = %if.end
  %9 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %9, i32 0, i32 3
  %10 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %11 = bitcast %class.btConstraintSolver* %10 to %class.btConstraintSolver* (%class.btConstraintSolver*)***
  %vtable6 = load %class.btConstraintSolver* (%class.btConstraintSolver*)**, %class.btConstraintSolver* (%class.btConstraintSolver*)*** %11, align 4
  %vfn7 = getelementptr inbounds %class.btConstraintSolver* (%class.btConstraintSolver*)*, %class.btConstraintSolver* (%class.btConstraintSolver*)** %vtable6, i64 0
  %12 = load %class.btConstraintSolver* (%class.btConstraintSolver*)*, %class.btConstraintSolver* (%class.btConstraintSolver*)** %vfn7, align 4
  %call8 = call %class.btConstraintSolver* %12(%class.btConstraintSolver* %10) #7
  %13 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver9 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %13, i32 0, i32 3
  %14 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver9, align 4
  %15 = bitcast %class.btConstraintSolver* %14 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %15)
  br label %if.end10

if.end10:                                         ; preds = %if.then5, %if.end
  %16 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %call11 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* %16) #7
  %17 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %retval, align 4
  ret %class.btDiscreteDynamicsWorldMt* %17
}

; Function Attrs: nounwind
declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN25btDiscreteDynamicsWorldMtD0Ev(%class.btDiscreteDynamicsWorldMt* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  store %class.btDiscreteDynamicsWorldMt* %this, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  %call = call %class.btDiscreteDynamicsWorldMt* @_ZN25btDiscreteDynamicsWorldMtD1Ev(%class.btDiscreteDynamicsWorldMt* %this1) #7
  %0 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to i8*
  call void @_ZN25btDiscreteDynamicsWorldMtdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN25btDiscreteDynamicsWorldMtdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN25btDiscreteDynamicsWorldMt16solveConstraintsER19btContactSolverInfo(%class.btDiscreteDynamicsWorldMt* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %solverInfo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorldMt*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %im = alloca %class.btSimulationIslandManagerMt*, align 4
  store %class.btDiscreteDynamicsWorldMt* %this, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorldMt*, %class.btDiscreteDynamicsWorldMt** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0))
  %m_solverIslandCallbackMt = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  %0 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt, align 4
  %1 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %2 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btCollisionWorld*
  %3 = bitcast %class.btCollisionWorld* %2 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %4 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call2 = call %class.btIDebugDraw* %4(%class.btCollisionWorld* %2)
  call void @_ZN29InplaceSolverIslandCallbackMt5setupEP19btContactSolverInfoP12btIDebugDraw(%struct.InplaceSolverIslandCallbackMt* %0, %struct.btContactSolverInfo* %1, %class.btIDebugDraw* %call2)
  %5 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %5, i32 0, i32 3
  %6 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4
  %7 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %call3 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %7)
  %call4 = call i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %call3)
  %8 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %call5 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %8)
  %call6 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call5)
  %9 = bitcast %class.btDispatcher* %call6 to i32 (%class.btDispatcher*)***
  %vtable7 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %9, align 4
  %vfn8 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable7, i64 9
  %10 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn8, align 4
  %call9 = call i32 %10(%class.btDispatcher* %call6)
  %11 = bitcast %class.btConstraintSolver* %6 to void (%class.btConstraintSolver*, i32, i32)***
  %vtable10 = load void (%class.btConstraintSolver*, i32, i32)**, void (%class.btConstraintSolver*, i32, i32)*** %11, align 4
  %vfn11 = getelementptr inbounds void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vtable10, i64 2
  %12 = load void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vfn11, align 4
  call void %12(%class.btConstraintSolver* %6, i32 %call4, i32 %call9)
  %13 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_islandManager = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %13, i32 0, i32 4
  %14 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %m_islandManager, align 4
  %15 = bitcast %class.btSimulationIslandManager* %14 to %class.btSimulationIslandManagerMt*
  store %class.btSimulationIslandManagerMt* %15, %class.btSimulationIslandManagerMt** %im, align 4
  %16 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %im, align 4
  %17 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %call12 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %17)
  %call13 = call %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %call12)
  %18 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %call14 = call %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %18)
  %19 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraints = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %19, i32 0, i32 5
  %m_solverIslandCallbackMt15 = getelementptr inbounds %class.btDiscreteDynamicsWorldMt, %class.btDiscreteDynamicsWorldMt* %this1, i32 0, i32 1
  %20 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %m_solverIslandCallbackMt15, align 4
  %21 = bitcast %struct.InplaceSolverIslandCallbackMt* %20 to %"struct.btSimulationIslandManagerMt::IslandCallback"*
  %22 = bitcast %class.btSimulationIslandManagerMt* %16 to void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)***
  %vtable16 = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)**, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)*** %22, align 4
  %vfn17 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)** %vtable16, i64 10
  %23 = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.4*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)** %vfn17, align 4
  call void %23(%class.btSimulationIslandManagerMt* %16, %class.btDispatcher* %call13, %class.btCollisionWorld* %call14, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %m_constraints, %"struct.btSimulationIslandManagerMt::IslandCallback"* %21)
  %24 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btDiscreteDynamicsWorld*
  %m_constraintSolver18 = getelementptr inbounds %class.btDiscreteDynamicsWorld, %class.btDiscreteDynamicsWorld* %24, i32 0, i32 3
  %25 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver18, align 4
  %26 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %27 = bitcast %class.btDiscreteDynamicsWorldMt* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %27, i32 0, i32 5
  %28 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %29 = bitcast %class.btConstraintSolver* %25 to void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable19 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %29, align 4
  %vfn20 = getelementptr inbounds void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable19, i64 4
  %30 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn20, align 4
  call void %30(%class.btConstraintSolver* %25, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %26, %class.btIDebugDraw* %28)
  %call21 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #7
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN29InplaceSolverIslandCallbackMt5setupEP19btContactSolverInfoP12btIDebugDraw(%struct.InplaceSolverIslandCallbackMt* %this, %struct.btContactSolverInfo* %solverInfo, %class.btIDebugDraw* %debugDrawer) #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallbackMt*, align 4
  %solverInfo.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %struct.InplaceSolverIslandCallbackMt* %this, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  store %struct.btContactSolverInfo* %solverInfo, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %solverInfo.addr, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 1
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %1 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 3
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionWorld* @_ZN23btDiscreteDynamicsWorld17getCollisionWorldEv(%class.btDiscreteDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDiscreteDynamicsWorld* %this1 to %class.btCollisionWorld*
  ret %class.btCollisionWorld* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDispatcher* @_ZN16btCollisionWorld13getDispatcherEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 2
  %0 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  ret %class.btDispatcher* %0
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #3

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret %class.btIDebugDraw* %0
}

declare void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE(%class.btCollisionWorld*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24)) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*) unnamed_addr #3

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld9serializeEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) unnamed_addr #3

declare i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld*, float, i32, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1 zeroext) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* sret align 4, %class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*) unnamed_addr #3

declare %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

declare %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv(%class.btDiscreteDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret i32 2
}

declare void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %numTasks, i32* %numTasks.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManagerMt::IslandCallback"* @_ZN27btSimulationIslandManagerMt14IslandCallbackC2Ev(%"struct.btSimulationIslandManagerMt::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManagerMt::IslandCallback"* %this, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btSimulationIslandManagerMt::IslandCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN27btSimulationIslandManagerMt14IslandCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btSimulationIslandManagerMt::IslandCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.InplaceSolverIslandCallbackMt* @_ZN29InplaceSolverIslandCallbackMtD2Ev(%struct.InplaceSolverIslandCallbackMt* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallbackMt*, align 4
  store %struct.InplaceSolverIslandCallbackMt* %this, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %0 = bitcast %struct.InplaceSolverIslandCallbackMt* %this1 to %"struct.btSimulationIslandManagerMt::IslandCallback"*
  %call = call %"struct.btSimulationIslandManagerMt::IslandCallback"* @_ZN27btSimulationIslandManagerMt14IslandCallbackD2Ev(%"struct.btSimulationIslandManagerMt::IslandCallback"* %0) #7
  ret %struct.InplaceSolverIslandCallbackMt* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN29InplaceSolverIslandCallbackMtD0Ev(%struct.InplaceSolverIslandCallbackMt* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallbackMt*, align 4
  store %struct.InplaceSolverIslandCallbackMt* %this, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %call = call %struct.InplaceSolverIslandCallbackMt* @_ZN29InplaceSolverIslandCallbackMtD2Ev(%struct.InplaceSolverIslandCallbackMt* %this1) #7
  %0 = bitcast %struct.InplaceSolverIslandCallbackMt* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN29InplaceSolverIslandCallbackMt13processIslandEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintii(%struct.InplaceSolverIslandCallbackMt* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifolds, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, i32 %islandId) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.InplaceSolverIslandCallbackMt*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifolds.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %islandId.addr = alloca i32, align 4
  store %struct.InplaceSolverIslandCallbackMt* %this, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifolds, %class.btPersistentManifold*** %manifolds.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store i32 %islandId, i32* %islandId.addr, align 4
  %this1 = load %struct.InplaceSolverIslandCallbackMt*, %struct.InplaceSolverIslandCallbackMt** %this.addr, align 4
  %m_solver = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 2
  %0 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_solver, align 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodies.addr, align 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds.addr, align 4
  %4 = load i32, i32* %numManifolds.addr, align 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %6 = load i32, i32* %numConstraints.addr, align 4
  %m_solverInfo = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 1
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %m_solverInfo, align 4
  %m_debugDrawer = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 3
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_dispatcher = getelementptr inbounds %struct.InplaceSolverIslandCallbackMt, %struct.InplaceSolverIslandCallbackMt* %this1, i32 0, i32 4
  %9 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %10 = bitcast %class.btConstraintSolver* %0 to float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %10, align 4
  %vfn = getelementptr inbounds float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable, i64 3
  %11 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn, align 4
  %call = call float %11(%class.btConstraintSolver* %0, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %7, %class.btIDebugDraw* %8, %class.btDispatcher* %9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManagerMt::IslandCallback"* @_ZN27btSimulationIslandManagerMt14IslandCallbackD2Ev(%"struct.btSimulationIslandManagerMt::IslandCallback"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManagerMt::IslandCallback"* %this, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  ret %"struct.btSimulationIslandManagerMt::IslandCallback"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN27btSimulationIslandManagerMt14IslandCallbackD0Ev(%"struct.btSimulationIslandManagerMt::IslandCallback"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::IslandCallback"*, align 4
  store %"struct.btSimulationIslandManagerMt::IslandCallback"* %this, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

declare void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld*, float) #3

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDiscreteDynamicsWorldMt.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
