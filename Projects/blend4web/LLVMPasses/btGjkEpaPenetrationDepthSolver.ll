; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btGjkEpaPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btIDebugDraw = type opaque
%"struct.btGjkEpaSolver2::sResults" = type { i32, [2 x %class.btVector3], %class.btVector3, float }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN15btGjkEpaSolver28sResultsC2Ev = comdat any

$_ZN30btGjkEpaPenetrationDepthSolverD2Ev = comdat any

$_ZN30btGjkEpaPenetrationDepthSolverD0Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV30btGjkEpaPenetrationDepthSolver = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btGjkEpaPenetrationDepthSolver to i8*), i8* bitcast (%class.btGjkEpaPenetrationDepthSolver* (%class.btGjkEpaPenetrationDepthSolver*)* @_ZN30btGjkEpaPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btGjkEpaPenetrationDepthSolver*)* @_ZN30btGjkEpaPenetrationDepthSolverD0Ev to i8*), i8* bitcast (i1 (%class.btGjkEpaPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)* @_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btGjkEpaPenetrationDepthSolver = hidden constant [33 x i8] c"30btGjkEpaPenetrationDepthSolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTI30btGjkEpaPenetrationDepthSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btGjkEpaPenetrationDepthSolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGjkEpaPenetrationDepthSolver.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN30btGjkEpaPenetrationDepthSolver12calcPenDepthER22btVoronoiSimplexSolverPK13btConvexShapeS4_RK11btTransformS7_R9btVector3S9_S9_P12btIDebugDraw(%class.btGjkEpaPenetrationDepthSolver* %this, %class.btVoronoiSimplexSolver* nonnull align 4 dereferenceable(357) %simplexSolver, %class.btConvexShape* %pConvexA, %class.btConvexShape* %pConvexB, %class.btTransform* nonnull align 4 dereferenceable(64) %transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %transformB, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %wWitnessOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %wWitnessOnB, %class.btIDebugDraw* %debugDraw) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %pConvexA.addr = alloca %class.btConvexShape*, align 4
  %pConvexB.addr = alloca %class.btConvexShape*, align 4
  %transformA.addr = alloca %class.btTransform*, align 4
  %transformB.addr = alloca %class.btTransform*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %wWitnessOnA.addr = alloca %class.btVector3*, align 4
  %wWitnessOnB.addr = alloca %class.btVector3*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %guessVector = alloca %class.btVector3, align 4
  %results = alloca %"struct.btGjkEpaSolver2::sResults", align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btConvexShape* %pConvexA, %class.btConvexShape** %pConvexA.addr, align 4
  store %class.btConvexShape* %pConvexB, %class.btConvexShape** %pConvexB.addr, align 4
  store %class.btTransform* %transformA, %class.btTransform** %transformA.addr, align 4
  store %class.btTransform* %transformB, %class.btTransform** %transformB.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btVector3* %wWitnessOnA, %class.btVector3** %wWitnessOnA.addr, align 4
  store %class.btVector3* %wWitnessOnB, %class.btVector3** %wWitnessOnB.addr, align 4
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  %2 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %guessVector, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %call3 = call %"struct.btGjkEpaSolver2::sResults"* @_ZN15btGjkEpaSolver28sResultsC2Ev(%"struct.btGjkEpaSolver2::sResults"* %results)
  %4 = load %class.btConvexShape*, %class.btConvexShape** %pConvexA.addr, align 4
  %5 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %6 = load %class.btConvexShape*, %class.btConvexShape** %pConvexB.addr, align 4
  %7 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call4 = call zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btConvexShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %guessVector, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results, i1 zeroext true)
  br i1 %call4, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %8 = load %class.btVector3*, %class.btVector3** %wWitnessOnA.addr, align 4
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %witnesses5 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses5, i32 0, i32 1
  %11 = load %class.btVector3*, %class.btVector3** %wWitnessOnB.addr, align 4
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 2
  %14 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %15 = bitcast %class.btVector3* %14 to i8*
  %16 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  %17 = load %class.btConvexShape*, %class.btConvexShape** %pConvexA.addr, align 4
  %18 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %19 = load %class.btConvexShape*, %class.btConvexShape** %pConvexB.addr, align 4
  %20 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call7 = call zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %17, %class.btTransform* nonnull align 4 dereferenceable(64) %18, %class.btConvexShape* %19, %class.btTransform* nonnull align 4 dereferenceable(64) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %guessVector, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results)
  br i1 %call7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.else
  %witnesses9 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 1
  %arrayidx10 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses9, i32 0, i32 0
  %21 = load %class.btVector3*, %class.btVector3** %wWitnessOnA.addr, align 4
  %22 = bitcast %class.btVector3* %21 to i8*
  %23 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %witnesses11 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses11, i32 0, i32 1
  %24 = load %class.btVector3*, %class.btVector3** %wWitnessOnB.addr, align 4
  %25 = bitcast %class.btVector3* %24 to i8*
  %26 = bitcast %class.btVector3* %arrayidx12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %normal13 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %results, i32 0, i32 2
  %27 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %normal13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end14

if.end14:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end14, %if.then8, %if.then
  %30 = load i1, i1* %retval, align 1
  ret i1 %30
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btGjkEpaSolver2::sResults"* @_ZN15btGjkEpaSolver28sResultsC2Ev(%"struct.btGjkEpaSolver2::sResults"* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %this.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %this, %"struct.btGjkEpaSolver2::sResults"** %this.addr, align 4
  %this1 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %this.addr, align 4
  store %"struct.btGjkEpaSolver2::sResults"* %this1, %"struct.btGjkEpaSolver2::sResults"** %retval, align 4
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  %0 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %retval, align 4
  ret %"struct.btGjkEpaSolver2::sResults"* %0
}

declare zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56), i1 zeroext) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

declare zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverD2Ev(%class.btGjkEpaPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* %0) #6
  ret %class.btGjkEpaPenetrationDepthSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btGjkEpaPenetrationDepthSolverD0Ev(%class.btGjkEpaPenetrationDepthSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %call = call %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverD2Ev(%class.btGjkEpaPenetrationDepthSolver* %this1) #6
  %0 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGjkEpaPenetrationDepthSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
