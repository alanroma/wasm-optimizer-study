; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btLemkeAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btLemkeAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.btVectorX = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btLemkeAlgorithm = type { i32 (...)**, %struct.btMatrixX, %struct.btVectorX, i32, i32, i32 }
%struct.btMatrixX = type { i32, i32, i32, i32, i32, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btAlignedObjectArray.3*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.btVectorX*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.CProfileSample = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVectorXIfE4sizeEv = comdat any

$_ZN9btVectorXIfEC2Ei = comdat any

$_ZN9btVectorXIfE7setZeroEv = comdat any

$_ZN9btMatrixXIfEC2Eii = comdat any

$_ZN9btMatrixXIfE11setIdentityEv = comdat any

$_ZN9btMatrixXIfE8negativeEv = comdat any

$_ZN9btMatrixXIfE12setSubMatrixEiiiiRKS0_ = comdat any

$_ZN9btMatrixXIfE12setSubMatrixEiiiif = comdat any

$_ZN9btMatrixXIfE12setSubMatrixEiiiiRK9btVectorXIfE = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK9btMatrixXIfEclEii = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN9btVectorXIfEixEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN9btMatrixXIfED2Ev = comdat any

$_ZN9btVectorXIfED2Ev = comdat any

$_ZNK9btMatrixXIfE4rowsEv = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE9push_backERKS1_ = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv = comdat any

$_ZNK9btVectorXIfE4nrm2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEED2Ev = comdat any

$_Z4fabsf = comdat any

$_ZNK9btVectorXIfEixEi = comdat any

$_ZNK9btMatrixXIfE4colsEv = comdat any

$_ZN9btMatrixXIfE7setElemEiif = comdat any

$_ZN9btMatrixXIfE7mulElemEiif = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIS_IiEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIS_IiEE4initEv = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK9btVectorXIfE4rowsEv = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_Z9btSetZeroIfEvPT_i = comdat any

$_ZN20btAlignedObjectArrayIS_IiEEC2Ev = comdat any

$_ZN9btMatrixXIfE6resizeEii = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev = comdat any

$_ZN9btMatrixXIfE7setZeroEv = comdat any

$_ZNK9btVectorXIfE4colsEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZN18btAlignedAllocatorI9btVectorXIfELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVectorXIfELj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayI9btVectorXIfEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE9allocSizeEi = comdat any

$_ZN9btVectorXIfEC2ERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVectorXIfEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVectorXIfEE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorI9btVectorXIfELj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIfEC2ERKS0_ = comdat any

$_Z6btFabsf = comdat any

$_Z4sqrtf = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZ9btMachEpsvE10calculated = internal global i8 0, align 1
@_ZZ9btMachEpsvE7machEps = internal global float 1.000000e+00, align 4
@_ZZ9btEpsRootvE7epsroot = internal global float 0.000000e+00, align 4
@_ZZ9btEpsRootvE17alreadyCalculated = internal global i8 0, align 1
@.str = private unnamed_addr constant [17 x i8] c"m_storage.resize\00", align 1
@.str.1 = private unnamed_addr constant [10 x i8] c"storage=0\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btLemkeAlgorithm.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden float @_Z9btMachEpsv() #1 {
entry:
  %0 = load i8, i8* @_ZZ9btMachEpsvE10calculated, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %1 = load float, float* @_ZZ9btMachEpsvE7machEps, align 4
  %div = fdiv float %1, 2.000000e+00
  store float %div, float* @_ZZ9btMachEpsvE7machEps, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %2 = load float, float* @_ZZ9btMachEpsvE7machEps, align 4
  %div1 = fdiv float %2, 2.000000e+00
  %conv = fpext float %div1 to double
  %add = fadd double 1.000000e+00, %conv
  %conv2 = fptrunc double %add to float
  %cmp = fcmp une float %conv2, 1.000000e+00
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  store i8 1, i8* @_ZZ9btMachEpsvE10calculated, align 1
  br label %if.end

if.end:                                           ; preds = %do.end, %entry
  %3 = load float, float* @_ZZ9btMachEpsvE7machEps, align 4
  ret float %3
}

; Function Attrs: noinline optnone
define hidden float @_Z9btEpsRootv() #2 {
entry:
  %0 = load i8, i8* @_ZZ9btEpsRootvE17alreadyCalculated, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call = call float @_Z9btMachEpsv()
  %call1 = call float @_Z6btSqrtf(float %call)
  store float %call1, float* @_ZZ9btEpsRootvE7epsroot, align 4
  store i8 1, i8* @_ZZ9btEpsRootvE17alreadyCalculated, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* @_ZZ9btEpsRootvE7epsroot, align 4
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btLemkeAlgorithm5solveEj(%struct.btVectorX* noalias sret align 4 %agg.result, %class.btLemkeAlgorithm* %this, i32 %maxloops) #2 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %maxloops.addr = alloca i32, align 4
  %dim = alloca i32, align 4
  %nrvo = alloca i1, align 1
  %ident = alloca %struct.btMatrixX, align 4
  %mNeg = alloca %struct.btMatrixX, align 4
  %A = alloca %struct.btMatrixX, align 4
  %basis = alloca %class.btAlignedObjectArray.3, align 4
  %i = alloca i32, align 4
  %pivotRowIndex = alloca i32, align 4
  %minValue = alloca float, align 4
  %greaterZero = alloca i8, align 1
  %i20 = alloca i32, align 4
  %v = alloca float, align 4
  %z0Row = alloca i32, align 4
  %pivotColIndex = alloca i32, align 4
  %pivotColIndexOld = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i66 = alloca i32, align 4
  %0 = bitcast %struct.btVectorX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store i32 %maxloops, i32* %maxloops.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  %steps = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 3
  store i32 0, i32* %steps, align 4
  %m_q = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 2
  %call = call i32 @_ZNK9btVectorXIfE4sizeEv(%struct.btVectorX* %m_q)
  store i32 %call, i32* %dim, align 4
  store i1 false, i1* %nrvo, align 1
  %1 = load i32, i32* %dim, align 4
  %mul = mul nsw i32 2, %1
  %call2 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ei(%struct.btVectorX* %agg.result, i32 %mul)
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %agg.result)
  %2 = load i32, i32* %dim, align 4
  %3 = load i32, i32* %dim, align 4
  %call3 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %ident, i32 %2, i32 %3)
  call void @_ZN9btMatrixXIfE11setIdentityEv(%struct.btMatrixX* %ident)
  %m_M = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 1
  call void @_ZN9btMatrixXIfE8negativeEv(%struct.btMatrixX* sret align 4 %mNeg, %struct.btMatrixX* %m_M)
  %4 = load i32, i32* %dim, align 4
  %5 = load i32, i32* %dim, align 4
  %mul4 = mul nsw i32 2, %5
  %add = add nsw i32 %mul4, 2
  %call5 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %A, i32 %4, i32 %add)
  %6 = load i32, i32* %dim, align 4
  %sub = sub nsw i32 %6, 1
  %7 = load i32, i32* %dim, align 4
  %sub6 = sub nsw i32 %7, 1
  call void @_ZN9btMatrixXIfE12setSubMatrixEiiiiRKS0_(%struct.btMatrixX* %A, i32 0, i32 0, i32 %sub, i32 %sub6, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %ident)
  %8 = load i32, i32* %dim, align 4
  %9 = load i32, i32* %dim, align 4
  %sub7 = sub nsw i32 %9, 1
  %10 = load i32, i32* %dim, align 4
  %mul8 = mul nsw i32 2, %10
  %sub9 = sub nsw i32 %mul8, 1
  call void @_ZN9btMatrixXIfE12setSubMatrixEiiiiRKS0_(%struct.btMatrixX* %A, i32 0, i32 %8, i32 %sub7, i32 %sub9, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %mNeg)
  %11 = load i32, i32* %dim, align 4
  %mul10 = mul nsw i32 2, %11
  %12 = load i32, i32* %dim, align 4
  %sub11 = sub nsw i32 %12, 1
  %13 = load i32, i32* %dim, align 4
  %mul12 = mul nsw i32 2, %13
  call void @_ZN9btMatrixXIfE12setSubMatrixEiiiif(%struct.btMatrixX* %A, i32 0, i32 %mul10, i32 %sub11, i32 %mul12, float -1.000000e+00)
  %14 = load i32, i32* %dim, align 4
  %mul13 = mul nsw i32 2, %14
  %add14 = add nsw i32 %mul13, 1
  %15 = load i32, i32* %dim, align 4
  %sub15 = sub nsw i32 %15, 1
  %16 = load i32, i32* %dim, align 4
  %mul16 = mul nsw i32 2, %16
  %add17 = add nsw i32 %mul16, 1
  %m_q18 = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 2
  call void @_ZN9btMatrixXIfE12setSubMatrixEiiiiRK9btVectorXIfE(%struct.btMatrixX* %A, i32 0, i32 %add14, i32 %sub15, i32 %add17, %struct.btVectorX* nonnull align 4 dereferenceable(20) %m_q18)
  %call19 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %basis)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %dim, align 4
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %basis, i32* nonnull align 4 dereferenceable(4) %i)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 -1, i32* %pivotRowIndex, align 4
  store float 0x46293E5940000000, float* %minValue, align 4
  store i8 1, i8* %greaterZero, align 1
  store i32 0, i32* %i20, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc31, %for.end
  %20 = load i32, i32* %i20, align 4
  %21 = load i32, i32* %dim, align 4
  %cmp22 = icmp slt i32 %20, %21
  br i1 %cmp22, label %for.body23, label %for.end33

for.body23:                                       ; preds = %for.cond21
  %22 = load i32, i32* %i20, align 4
  %23 = load i32, i32* %dim, align 4
  %mul24 = mul nsw i32 2, %23
  %add25 = add nsw i32 %mul24, 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %A, i32 %22, i32 %add25)
  %24 = load float, float* %call26, align 4
  store float %24, float* %v, align 4
  %25 = load float, float* %v, align 4
  %26 = load float, float* %minValue, align 4
  %cmp27 = fcmp olt float %25, %26
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %for.body23
  %27 = load float, float* %v, align 4
  store float %27, float* %minValue, align 4
  %28 = load i32, i32* %i20, align 4
  store i32 %28, i32* %pivotRowIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body23
  %29 = load float, float* %v, align 4
  %cmp28 = fcmp olt float %29, 0.000000e+00
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  store i8 0, i8* %greaterZero, align 1
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end
  br label %for.inc31

for.inc31:                                        ; preds = %if.end30
  %30 = load i32, i32* %i20, align 4
  %inc32 = add nsw i32 %30, 1
  store i32 %inc32, i32* %i20, align 4
  br label %for.cond21

for.end33:                                        ; preds = %for.cond21
  %31 = load i32, i32* %pivotRowIndex, align 4
  store i32 %31, i32* %z0Row, align 4
  %32 = load i32, i32* %dim, align 4
  %mul34 = mul nsw i32 2, %32
  store i32 %mul34, i32* %pivotColIndex, align 4
  %33 = load i8, i8* %greaterZero, align 1
  %tobool = trunc i8 %33 to i1
  br i1 %tobool, label %if.end65, label %if.then35

if.then35:                                        ; preds = %for.end33
  %34 = load i32, i32* %maxloops.addr, align 4
  %cmp36 = icmp eq i32 %34, 0
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.then35
  store i32 100, i32* %maxloops.addr, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %if.then35
  %steps39 = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 3
  store i32 0, i32* %steps39, align 4
  br label %for.cond40

for.cond40:                                       ; preds = %for.inc58, %if.end38
  %steps41 = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 3
  %35 = load i32, i32* %steps41, align 4
  %36 = load i32, i32* %maxloops.addr, align 4
  %cmp42 = icmp ult i32 %35, %36
  br i1 %cmp42, label %for.body43, label %for.end61

for.body43:                                       ; preds = %for.cond40
  %37 = load i32, i32* %pivotRowIndex, align 4
  %38 = load i32, i32* %pivotColIndex, align 4
  call void @_ZN16btLemkeAlgorithm26GaussJordanEliminationStepER9btMatrixXIfEiiRK20btAlignedObjectArrayIiE(%class.btLemkeAlgorithm* %this1, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %A, i32 %37, i32 %38, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %basis)
  %39 = load i32, i32* %pivotColIndex, align 4
  store i32 %39, i32* %pivotColIndexOld, align 4
  %40 = load i32, i32* %pivotRowIndex, align 4
  %call44 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %40)
  %41 = load i32, i32* %call44, align 4
  %42 = load i32, i32* %dim, align 4
  %cmp45 = icmp slt i32 %41, %42
  br i1 %cmp45, label %if.then46, label %if.else

if.then46:                                        ; preds = %for.body43
  %43 = load i32, i32* %pivotRowIndex, align 4
  %call47 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %43)
  %44 = load i32, i32* %call47, align 4
  %45 = load i32, i32* %dim, align 4
  %add48 = add nsw i32 %44, %45
  store i32 %add48, i32* %pivotColIndex, align 4
  br label %if.end51

if.else:                                          ; preds = %for.body43
  %46 = load i32, i32* %pivotRowIndex, align 4
  %call49 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %46)
  %47 = load i32, i32* %call49, align 4
  %48 = load i32, i32* %dim, align 4
  %sub50 = sub nsw i32 %47, %48
  store i32 %sub50, i32* %pivotColIndex, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.else, %if.then46
  %49 = load i32, i32* %pivotColIndexOld, align 4
  %50 = load i32, i32* %pivotRowIndex, align 4
  %call52 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %50)
  store i32 %49, i32* %call52, align 4
  %call53 = call i32 @_ZN16btLemkeAlgorithm24findLexicographicMinimumERK9btMatrixXIfERKi(%class.btLemkeAlgorithm* %this1, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %A, i32* nonnull align 4 dereferenceable(4) %pivotColIndex)
  store i32 %call53, i32* %pivotRowIndex, align 4
  %51 = load i32, i32* %z0Row, align 4
  %52 = load i32, i32* %pivotRowIndex, align 4
  %cmp54 = icmp eq i32 %51, %52
  br i1 %cmp54, label %if.then55, label %if.end57

if.then55:                                        ; preds = %if.end51
  %53 = load i32, i32* %pivotRowIndex, align 4
  %54 = load i32, i32* %pivotColIndex, align 4
  call void @_ZN16btLemkeAlgorithm26GaussJordanEliminationStepER9btMatrixXIfEiiRK20btAlignedObjectArrayIiE(%class.btLemkeAlgorithm* %this1, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %A, i32 %53, i32 %54, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %basis)
  %55 = load i32, i32* %pivotColIndex, align 4
  %56 = load i32, i32* %pivotRowIndex, align 4
  %call56 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %56)
  store i32 %55, i32* %call56, align 4
  br label %for.end61

if.end57:                                         ; preds = %if.end51
  br label %for.inc58

for.inc58:                                        ; preds = %if.end57
  %steps59 = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 3
  %57 = load i32, i32* %steps59, align 4
  %inc60 = add i32 %57, 1
  store i32 %inc60, i32* %steps59, align 4
  br label %for.cond40

for.end61:                                        ; preds = %if.then55, %for.cond40
  %call62 = call zeroext i1 @_ZN16btLemkeAlgorithm10validBasisERK20btAlignedObjectArrayIiE(%class.btLemkeAlgorithm* %this1, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %basis)
  br i1 %call62, label %if.end64, label %if.then63

if.then63:                                        ; preds = %for.end61
  %info = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 5
  store i32 -1, i32* %info, align 4
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end64:                                         ; preds = %for.end61
  br label %if.end65

if.end65:                                         ; preds = %if.end64, %for.end33
  store i32 0, i32* %i66, align 4
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc76, %if.end65
  %58 = load i32, i32* %i66, align 4
  %call68 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %basis)
  %cmp69 = icmp slt i32 %58, %call68
  br i1 %cmp69, label %for.body70, label %for.end78

for.body70:                                       ; preds = %for.cond67
  %59 = load i32, i32* %i66, align 4
  %60 = load i32, i32* %dim, align 4
  %mul71 = mul nsw i32 2, %60
  %add72 = add nsw i32 %mul71, 1
  %call73 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %A, i32 %59, i32 %add72)
  %61 = load float, float* %call73, align 4
  %62 = load i32, i32* %i66, align 4
  %call74 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %basis, i32 %62)
  %63 = load i32, i32* %call74, align 4
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %agg.result, i32 %63)
  store float %61, float* %call75, align 4
  br label %for.inc76

for.inc76:                                        ; preds = %for.body70
  %64 = load i32, i32* %i66, align 4
  %inc77 = add nsw i32 %64, 1
  store i32 %inc77, i32* %i66, align 4
  br label %for.cond67

for.end78:                                        ; preds = %for.cond67
  %info79 = getelementptr inbounds %class.btLemkeAlgorithm, %class.btLemkeAlgorithm* %this1, i32 0, i32 5
  store i32 0, i32* %info79, align 4
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end78, %if.then63
  %call80 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %basis) #6
  %call82 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %A) #6
  %call84 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %mNeg) #6
  %call86 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %ident) #6
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %cleanup
  %call88 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %agg.result) #6
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %cleanup
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btVectorXIfE4sizeEv(%struct.btVectorX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %call = call i32 @_ZNK9btVectorXIfE4rowsEv(%struct.btVectorX* %this1)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfEC2Ei(%struct.btVectorX* returned %this, i32 %numRows) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %numRows.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store i32 %numRows, i32* %numRows.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray* %m_storage)
  %m_storage2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %numRows.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %m_storage2, i32 %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %struct.btVectorX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %m_storage)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_storage2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage2, i32 0)
  %m_storage4 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %m_storage4)
  call void @_Z9btSetZeroIfEvPT_i(float* %call3, i32 %call5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* returned %this, i32 %rows, i32 %cols) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rows, i32* %rows.addr, align 4
  store i32 %cols, i32* %cols.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %rows.addr, align 4
  store i32 %0, i32* %m_rows, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %1 = load i32, i32* %cols.addr, align 4
  store i32 %1, i32* %m_cols, align 4
  %m_operations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 2
  store i32 0, i32* %m_operations, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  store i32 0, i32* %m_resizeOperations, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  store i32 0, i32* %m_setElemOperations, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray* %m_storage)
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.0* %m_rowNonZeroElements1)
  %2 = load i32, i32* %rows.addr, align 4
  %3 = load i32, i32* %cols.addr, align 4
  call void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this1, i32 %2, i32 %3)
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE11setIdentityEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  call void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %this1)
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %row, align 4
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %row, align 4
  %2 = load i32, i32* %row, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %1, i32 %2, float 1.000000e+00)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %row, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %row, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE8negativeEv(%struct.btMatrixX* noalias sret align 4 %agg.result, %struct.btMatrixX* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %struct.btMatrixX*, align 4
  %nrvo = alloca i1, align 1
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %v = alloca float, align 4
  %0 = bitcast %struct.btMatrixX* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i1 false, i1* %nrvo, align 1
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %call2 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %this1)
  %call3 = call %struct.btMatrixX* @_ZN9btMatrixXIfEC2Eii(%struct.btMatrixX* %agg.result, i32 %call, i32 %call2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc10, %entry
  %1 = load i32, i32* %i, align 4
  %call4 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this1)
  %cmp = icmp slt i32 %1, %call4
  br i1 %cmp, label %for.body, label %for.end12

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %call6 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %this1)
  %cmp7 = icmp slt i32 %2, %call6
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond5
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %j, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this1, i32 %3, i32 %4)
  %5 = load float, float* %call9, align 4
  store float %5, float* %v, align 4
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %j, align 4
  %8 = load float, float* %v, align 4
  %fneg = fneg float %8
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %agg.result, i32 %6, i32 %7, float %fneg)
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %9 = load i32, i32* %j, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond5

for.end:                                          ; preds = %for.cond5
  br label %for.inc10

for.inc10:                                        ; preds = %for.end
  %10 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %10, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond

for.end12:                                        ; preds = %for.cond
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end12
  %call13 = call %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* %agg.result) #6
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end12
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE12setSubMatrixEiiiiRKS0_(%struct.btMatrixX* %this, i32 %rowstart, i32 %colstart, i32 %rowend, i32 %colend, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %block) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rowstart.addr = alloca i32, align 4
  %colstart.addr = alloca i32, align 4
  %rowend.addr = alloca i32, align 4
  %colend.addr = alloca i32, align 4
  %block.addr = alloca %struct.btMatrixX*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rowstart, i32* %rowstart.addr, align 4
  store i32 %colstart, i32* %colstart.addr, align 4
  store i32 %rowend, i32* %rowend.addr, align 4
  store i32 %colend, i32* %colend.addr, align 4
  store %struct.btMatrixX* %block, %struct.btMatrixX** %block.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %0 = load i32, i32* %row, align 4
  %1 = load %struct.btMatrixX*, %struct.btMatrixX** %block.addr, align 4
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end10

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %2 = load i32, i32* %col, align 4
  %3 = load %struct.btMatrixX*, %struct.btMatrixX** %block.addr, align 4
  %call3 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %3)
  %cmp4 = icmp slt i32 %2, %call3
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond2
  %4 = load i32, i32* %rowstart.addr, align 4
  %5 = load i32, i32* %row, align 4
  %add = add nsw i32 %4, %5
  %6 = load i32, i32* %colstart.addr, align 4
  %7 = load i32, i32* %col, align 4
  %add6 = add nsw i32 %6, %7
  %8 = load %struct.btMatrixX*, %struct.btMatrixX** %block.addr, align 4
  %9 = load i32, i32* %row, align 4
  %10 = load i32, i32* %col, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %8, i32 %9, i32 %10)
  %11 = load float, float* %call7, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %add, i32 %add6, float %11)
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %12 = load i32, i32* %col, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %col, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %13 = load i32, i32* %row, align 4
  %inc9 = add nsw i32 %13, 1
  store i32 %inc9, i32* %row, align 4
  br label %for.cond

for.end10:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE12setSubMatrixEiiiif(%struct.btMatrixX* %this, i32 %rowstart, i32 %colstart, i32 %rowend, i32 %colend, float %value) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rowstart.addr = alloca i32, align 4
  %colstart.addr = alloca i32, align 4
  %rowend.addr = alloca i32, align 4
  %colend.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %numRows = alloca i32, align 4
  %numCols = alloca i32, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rowstart, i32* %rowstart.addr, align 4
  store i32 %colstart, i32* %colstart.addr, align 4
  store i32 %rowend, i32* %rowend.addr, align 4
  store i32 %colend, i32* %colend.addr, align 4
  store float %value, float* %value.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %0 = load i32, i32* %rowend.addr, align 4
  %add = add nsw i32 %0, 1
  %1 = load i32, i32* %rowstart.addr, align 4
  %sub = sub nsw i32 %add, %1
  store i32 %sub, i32* %numRows, align 4
  %2 = load i32, i32* %colend.addr, align 4
  %add2 = add nsw i32 %2, 1
  %3 = load i32, i32* %colstart.addr, align 4
  %sub3 = sub nsw i32 %add2, %3
  store i32 %sub3, i32* %numCols, align 4
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc9, %entry
  %4 = load i32, i32* %row, align 4
  %5 = load i32, i32* %numRows, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end11

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %col, align 4
  %7 = load i32, i32* %numCols, align 4
  %cmp5 = icmp slt i32 %6, %7
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %8 = load i32, i32* %rowstart.addr, align 4
  %9 = load i32, i32* %row, align 4
  %add7 = add nsw i32 %8, %9
  %10 = load i32, i32* %colstart.addr, align 4
  %11 = load i32, i32* %col, align 4
  %add8 = add nsw i32 %10, %11
  %12 = load float, float* %value.addr, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %add7, i32 %add8, float %12)
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %13 = load i32, i32* %col, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %col, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc9

for.inc9:                                         ; preds = %for.end
  %14 = load i32, i32* %row, align 4
  %inc10 = add nsw i32 %14, 1
  store i32 %inc10, i32* %row, align 4
  br label %for.cond

for.end11:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE12setSubMatrixEiiiiRK9btVectorXIfE(%struct.btMatrixX* %this, i32 %rowstart, i32 %colstart, i32 %rowend, i32 %colend, %struct.btVectorX* nonnull align 4 dereferenceable(20) %block) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rowstart.addr = alloca i32, align 4
  %colstart.addr = alloca i32, align 4
  %rowend.addr = alloca i32, align 4
  %colend.addr = alloca i32, align 4
  %block.addr = alloca %struct.btVectorX*, align 4
  %row = alloca i32, align 4
  %col = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rowstart, i32* %rowstart.addr, align 4
  store i32 %colstart, i32* %colstart.addr, align 4
  store i32 %rowend, i32* %rowend.addr, align 4
  store i32 %colend, i32* %colend.addr, align 4
  store %struct.btVectorX* %block, %struct.btVectorX** %block.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc8, %entry
  %0 = load i32, i32* %row, align 4
  %1 = load %struct.btVectorX*, %struct.btVectorX** %block.addr, align 4
  %call = call i32 @_ZNK9btVectorXIfE4rowsEv(%struct.btVectorX* %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end10

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %col, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %2 = load i32, i32* %col, align 4
  %3 = load %struct.btVectorX*, %struct.btVectorX** %block.addr, align 4
  %call3 = call i32 @_ZNK9btVectorXIfE4colsEv(%struct.btVectorX* %3)
  %cmp4 = icmp slt i32 %2, %call3
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond2
  %4 = load i32, i32* %rowstart.addr, align 4
  %5 = load i32, i32* %row, align 4
  %add = add nsw i32 %4, %5
  %6 = load i32, i32* %colstart.addr, align 4
  %7 = load i32, i32* %col, align 4
  %add6 = add nsw i32 %6, %7
  %8 = load %struct.btVectorX*, %struct.btVectorX** %block.addr, align 4
  %9 = load i32, i32* %row, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %8, i32 %9)
  %10 = load float, float* %call7, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this1, i32 %add, i32 %add6, float %10)
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %11 = load i32, i32* %col, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %col, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc8

for.inc8:                                         ; preds = %for.end
  %12 = load i32, i32* %row, align 4
  %inc9 = add nsw i32 %12, 1
  store i32 %inc9, i32* %row, align 4
  br label %for.cond

for.end10:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %this, i32 %row, i32 %col) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %0 = load i32, i32* %col.addr, align 4
  %1 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %2 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 %add)
  ret float* %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btLemkeAlgorithm26GaussJordanEliminationStepER9btMatrixXIfEiiRK20btAlignedObjectArrayIiE(%class.btLemkeAlgorithm* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %A, i32 %pivotRowIndex, i32 %pivotColumnIndex, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %basis) #2 {
entry:
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %A.addr = alloca %struct.btMatrixX*, align 4
  %pivotRowIndex.addr = alloca i32, align 4
  %pivotColumnIndex.addr = alloca i32, align 4
  %basis.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %a = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %v = alloca float, align 4
  %i18 = alloca i32, align 4
  %i26 = alloca i32, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store %struct.btMatrixX* %A, %struct.btMatrixX** %A.addr, align 4
  store i32 %pivotRowIndex, i32* %pivotRowIndex.addr, align 4
  store i32 %pivotColumnIndex, i32* %pivotColumnIndex.addr, align 4
  store %class.btAlignedObjectArray.3* %basis, %class.btAlignedObjectArray.3** %basis.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  %0 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %1 = load i32, i32* %pivotRowIndex.addr, align 4
  %2 = load i32, i32* %pivotColumnIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %0, i32 %1, i32 %2)
  %3 = load float, float* %call, align 4
  %div = fdiv float -1.000000e+00, %3
  store float %div, float* %a, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %call2 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %5)
  %cmp = icmp slt i32 %4, %call2
  br i1 %cmp, label %for.body, label %for.end17

for.body:                                         ; preds = %for.cond
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %pivotRowIndex.addr, align 4
  %cmp3 = icmp ne i32 %6, %7
  br i1 %cmp3, label %if.then, label %if.end14

if.then:                                          ; preds = %for.body
  store i32 0, i32* %j, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %if.then
  %8 = load i32, i32* %j, align 4
  %9 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %call5 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %9)
  %cmp6 = icmp slt i32 %8, %call5
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond4
  %10 = load i32, i32* %j, align 4
  %11 = load i32, i32* %pivotColumnIndex.addr, align 4
  %cmp8 = icmp ne i32 %10, %11
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %for.body7
  %12 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %12, i32 %13, i32 %14)
  %15 = load float, float* %call10, align 4
  store float %15, float* %v, align 4
  %16 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %17 = load i32, i32* %pivotRowIndex.addr, align 4
  %18 = load i32, i32* %j, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %16, i32 %17, i32 %18)
  %19 = load float, float* %call11, align 4
  %20 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %pivotColumnIndex.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %20, i32 %21, i32 %22)
  %23 = load float, float* %call12, align 4
  %mul = fmul float %19, %23
  %24 = load float, float* %a, align 4
  %mul13 = fmul float %mul, %24
  %25 = load float, float* %v, align 4
  %add = fadd float %25, %mul13
  store float %add, float* %v, align 4
  %26 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %27 = load i32, i32* %i, align 4
  %28 = load i32, i32* %j, align 4
  %29 = load float, float* %v, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %26, i32 %27, i32 %28, float %29)
  br label %if.end

if.end:                                           ; preds = %if.then9, %for.body7
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %30 = load i32, i32* %j, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %if.end14

if.end14:                                         ; preds = %for.end, %for.body
  br label %for.inc15

for.inc15:                                        ; preds = %if.end14
  %31 = load i32, i32* %i, align 4
  %inc16 = add nsw i32 %31, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond

for.end17:                                        ; preds = %for.cond
  store i32 0, i32* %i18, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc23, %for.end17
  %32 = load i32, i32* %i18, align 4
  %33 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %call20 = call i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %33)
  %cmp21 = icmp slt i32 %32, %call20
  br i1 %cmp21, label %for.body22, label %for.end25

for.body22:                                       ; preds = %for.cond19
  %34 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %35 = load i32, i32* %pivotRowIndex.addr, align 4
  %36 = load i32, i32* %i18, align 4
  %37 = load float, float* %a, align 4
  %fneg = fneg float %37
  call void @_ZN9btMatrixXIfE7mulElemEiif(%struct.btMatrixX* %34, i32 %35, i32 %36, float %fneg)
  br label %for.inc23

for.inc23:                                        ; preds = %for.body22
  %38 = load i32, i32* %i18, align 4
  %inc24 = add nsw i32 %38, 1
  store i32 %inc24, i32* %i18, align 4
  br label %for.cond19

for.end25:                                        ; preds = %for.cond19
  store i32 0, i32* %i26, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc34, %for.end25
  %39 = load i32, i32* %i26, align 4
  %40 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %call28 = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %40)
  %cmp29 = icmp slt i32 %39, %call28
  br i1 %cmp29, label %for.body30, label %for.end36

for.body30:                                       ; preds = %for.cond27
  %41 = load i32, i32* %i26, align 4
  %42 = load i32, i32* %pivotRowIndex.addr, align 4
  %cmp31 = icmp ne i32 %41, %42
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %for.body30
  %43 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %44 = load i32, i32* %i26, align 4
  %45 = load i32, i32* %pivotColumnIndex.addr, align 4
  call void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %43, i32 %44, i32 %45, float 0.000000e+00)
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %for.body30
  br label %for.inc34

for.inc34:                                        ; preds = %if.end33
  %46 = load i32, i32* %i26, align 4
  %inc35 = add nsw i32 %46, 1
  store i32 %inc35, i32* %i26, align 4
  br label %for.cond27

for.end36:                                        ; preds = %for.cond27
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN16btLemkeAlgorithm24findLexicographicMinimumERK9btMatrixXIfERKi(%class.btLemkeAlgorithm* %this, %struct.btMatrixX* nonnull align 4 dereferenceable(60) %A, i32* nonnull align 4 dereferenceable(4) %pivotColIndex) #2 {
entry:
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %A.addr = alloca %struct.btMatrixX*, align 4
  %pivotColIndex.addr = alloca i32*, align 4
  %RowIndex = alloca i32, align 4
  %dim = alloca i32, align 4
  %Rows = alloca %class.btAlignedObjectArray.8, align 4
  %row = alloca i32, align 4
  %vec = alloca %struct.btVectorX, align 4
  %a = alloca float, align 4
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %j35 = alloca i32, align 4
  %test = alloca %struct.btVectorX, align 4
  %ii = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store %struct.btMatrixX* %A, %struct.btMatrixX** %A.addr, align 4
  store i32* %pivotColIndex, i32** %pivotColIndex.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  store i32 0, i32* %RowIndex, align 4
  %0 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %call = call i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %0)
  store i32 %call, i32* %dim, align 4
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVectorXIfEEC2Ev(%class.btAlignedObjectArray.8* %Rows)
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %entry
  %1 = load i32, i32* %row, align 4
  %2 = load i32, i32* %dim, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end26

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %dim, align 4
  %add = add nsw i32 %3, 1
  %call3 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ei(%struct.btVectorX* %vec, i32 %add)
  call void @_ZN9btVectorXIfE7setZeroEv(%struct.btVectorX* %vec)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE9push_backERKS1_(%class.btAlignedObjectArray.8* %Rows, %struct.btVectorX* nonnull align 4 dereferenceable(20) %vec)
  %4 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %5 = load i32, i32* %row, align 4
  %6 = load i32*, i32** %pivotColIndex.addr, align 4
  %7 = load i32, i32* %6, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %4, i32 %5, i32 %7)
  %8 = load float, float* %call4, align 4
  store float %8, float* %a, align 4
  %9 = load float, float* %a, align 4
  %cmp5 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %11 = load i32, i32* %row, align 4
  %12 = load i32, i32* %dim, align 4
  %mul = mul nsw i32 2, %12
  %add6 = add nsw i32 %mul, 1
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %10, i32 %11, i32 %add6)
  %13 = load float, float* %call7, align 4
  %14 = load float, float* %a, align 4
  %div = fdiv float %13, %14
  %15 = load i32, i32* %row, align 4
  %call8 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %15)
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %call8, i32 0)
  store float %div, float* %call9, align 4
  %16 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %17 = load i32, i32* %row, align 4
  %18 = load i32, i32* %dim, align 4
  %mul10 = mul nsw i32 2, %18
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %16, i32 %17, i32 %mul10)
  %19 = load float, float* %call11, align 4
  %20 = load float, float* %a, align 4
  %div12 = fdiv float %19, %20
  %21 = load i32, i32* %row, align 4
  %call13 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %21)
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %call13, i32 1)
  store float %div12, float* %call14, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %if.then
  %22 = load i32, i32* %j, align 4
  %23 = load i32, i32* %dim, align 4
  %add16 = add nsw i32 %23, 1
  %cmp17 = icmp slt i32 %22, %add16
  br i1 %cmp17, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond15
  %24 = load %struct.btMatrixX*, %struct.btMatrixX** %A.addr, align 4
  %25 = load i32, i32* %row, align 4
  %26 = load i32, i32* %j, align 4
  %sub = sub nsw i32 %26, 1
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btMatrixXIfEclEii(%struct.btMatrixX* %24, i32 %25, i32 %sub)
  %27 = load float, float* %call19, align 4
  %28 = load float, float* %a, align 4
  %div20 = fdiv float %27, %28
  %29 = load i32, i32* %row, align 4
  %call21 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %29)
  %30 = load i32, i32* %j, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %call21, i32 %30)
  store float %div20, float* %call22, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body18
  %31 = load i32, i32* %j, align 4
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond15

for.end:                                          ; preds = %for.cond15
  br label %if.end

if.end:                                           ; preds = %for.end, %for.body
  %call23 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %vec) #6
  br label %for.inc24

for.inc24:                                        ; preds = %if.end
  %32 = load i32, i32* %row, align 4
  %inc25 = add nsw i32 %32, 1
  store i32 %inc25, i32* %row, align 4
  br label %for.cond

for.end26:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc77, %for.end26
  %33 = load i32, i32* %i, align 4
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %Rows)
  %cmp29 = icmp slt i32 %33, %call28
  br i1 %cmp29, label %for.body30, label %for.end79

for.body30:                                       ; preds = %for.cond27
  %34 = load i32, i32* %i, align 4
  %call31 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %34)
  %call32 = call float @_ZNK9btVectorXIfE4nrm2Ev(%struct.btVectorX* %call31)
  %conv = fpext float %call32 to double
  %cmp33 = fcmp ogt double %conv, 0.000000e+00
  br i1 %cmp33, label %if.then34, label %if.end76

if.then34:                                        ; preds = %for.body30
  store i32 0, i32* %j35, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc68, %if.then34
  %35 = load i32, i32* %j35, align 4
  %call37 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %Rows)
  %cmp38 = icmp slt i32 %35, %call37
  br i1 %cmp38, label %for.body39, label %for.end70

for.body39:                                       ; preds = %for.cond36
  %36 = load i32, i32* %i, align 4
  %37 = load i32, i32* %j35, align 4
  %cmp40 = icmp ne i32 %36, %37
  br i1 %cmp40, label %if.then41, label %if.end67

if.then41:                                        ; preds = %for.body39
  %38 = load i32, i32* %j35, align 4
  %call42 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %38)
  %call43 = call float @_ZNK9btVectorXIfE4nrm2Ev(%struct.btVectorX* %call42)
  %conv44 = fpext float %call43 to double
  %cmp45 = fcmp ogt double %conv44, 0.000000e+00
  br i1 %cmp45, label %if.then46, label %if.end66

if.then46:                                        ; preds = %if.then41
  %39 = load i32, i32* %dim, align 4
  %add47 = add nsw i32 %39, 1
  %call48 = call %struct.btVectorX* @_ZN9btVectorXIfEC2Ei(%struct.btVectorX* %test, i32 %add47)
  store i32 0, i32* %ii, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc59, %if.then46
  %40 = load i32, i32* %ii, align 4
  %41 = load i32, i32* %dim, align 4
  %add50 = add nsw i32 %41, 1
  %cmp51 = icmp slt i32 %40, %add50
  br i1 %cmp51, label %for.body52, label %for.end61

for.body52:                                       ; preds = %for.cond49
  %42 = load i32, i32* %j35, align 4
  %call53 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %42)
  %43 = load i32, i32* %ii, align 4
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %call53, i32 %43)
  %44 = load float, float* %call54, align 4
  %45 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %Rows, i32 %45)
  %46 = load i32, i32* %ii, align 4
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %call55, i32 %46)
  %47 = load float, float* %call56, align 4
  %sub57 = fsub float %44, %47
  %48 = load i32, i32* %ii, align 4
  %call58 = call nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %test, i32 %48)
  store float %sub57, float* %call58, align 4
  br label %for.inc59

for.inc59:                                        ; preds = %for.body52
  %49 = load i32, i32* %ii, align 4
  %inc60 = add nsw i32 %49, 1
  store i32 %inc60, i32* %ii, align 4
  br label %for.cond49

for.end61:                                        ; preds = %for.cond49
  %call62 = call zeroext i1 @_ZN16btLemkeAlgorithm21LexicographicPositiveERK9btVectorXIfE(%class.btLemkeAlgorithm* %this1, %struct.btVectorX* nonnull align 4 dereferenceable(20) %test)
  br i1 %call62, label %if.end64, label %if.then63

if.then63:                                        ; preds = %for.end61
  store i32 11, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end64:                                         ; preds = %for.end61
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end64, %if.then63
  %call65 = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %test) #6
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 11, label %for.end70
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end66

if.end66:                                         ; preds = %cleanup.cont, %if.then41
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %for.body39
  br label %for.inc68

for.inc68:                                        ; preds = %if.end67
  %50 = load i32, i32* %j35, align 4
  %inc69 = add nsw i32 %50, 1
  store i32 %inc69, i32* %j35, align 4
  br label %for.cond36

for.end70:                                        ; preds = %cleanup, %for.cond36
  %51 = load i32, i32* %j35, align 4
  %call71 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %Rows)
  %cmp72 = icmp eq i32 %51, %call71
  br i1 %cmp72, label %if.then73, label %if.end75

if.then73:                                        ; preds = %for.end70
  %52 = load i32, i32* %i, align 4
  %53 = load i32, i32* %RowIndex, align 4
  %add74 = add nsw i32 %53, %52
  store i32 %add74, i32* %RowIndex, align 4
  br label %for.end79

if.end75:                                         ; preds = %for.end70
  br label %if.end76

if.end76:                                         ; preds = %if.end75, %for.body30
  br label %for.inc77

for.inc77:                                        ; preds = %if.end76
  %54 = load i32, i32* %i, align 4
  %inc78 = add nsw i32 %54, 1
  store i32 %inc78, i32* %i, align 4
  br label %for.cond27

for.end79:                                        ; preds = %if.then73, %for.cond27
  %55 = load i32, i32* %RowIndex, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call81 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVectorXIfEED2Ev(%class.btAlignedObjectArray.8* %Rows) #6
  ret i32 %55

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN16btLemkeAlgorithm10validBasisERK20btAlignedObjectArrayIiE(%class.btLemkeAlgorithm* %this, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %basis) #2 {
entry:
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %basis.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %isValid = alloca i8, align 1
  %i = alloca i32, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.3* %basis, %class.btAlignedObjectArray.3** %basis.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  store i8 1, i8* %isValid, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %basis.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %basis.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %2, i32 %3)
  %4 = load i32, i32* %call2, align 4
  %5 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %basis.addr, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %5)
  %mul = mul nsw i32 %call3, 2
  %cmp4 = icmp sge i32 %4, %mul
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8 0, i8* %isValid, align 1
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %7 = load i8, i8* %isValid, align 1
  %tobool = trunc i8 %7 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN9btVectorXIfEixEi(%struct.btVectorX* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %index.addr = alloca i32, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 %0)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btMatrixX* @_ZN9btMatrixXIfED2Ev(%struct.btMatrixX* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rowNonZeroElements1 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 6
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.0* %m_rowNonZeroElements1) #6
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray* %m_storage) #6
  ret %struct.btMatrixX* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray* %m_storage) #6
  ret %struct.btVectorX* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4rowsEv(%struct.btMatrixX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_rows, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVectorXIfEEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVectorXIfELj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE9push_backERKS1_(%class.btAlignedObjectArray.8* %this, %struct.btVectorX* nonnull align 4 dereferenceable(20) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %struct.btVectorX*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store %struct.btVectorX* %_Val, %struct.btVectorX** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVectorXIfEE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load %struct.btVectorX*, %struct.btVectorX** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %1, i32 %2
  %3 = bitcast %struct.btVectorX* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btVectorX*
  %5 = load %struct.btVectorX*, %struct.btVectorX** %_Val.addr, align 4
  %call5 = call %struct.btVectorX* @_ZN9btVectorXIfEC2ERKS0_(%struct.btVectorX* %4, %struct.btVectorX* nonnull align 4 dereferenceable(20) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %struct.btVectorX* @_ZN20btAlignedObjectArrayI9btVectorXIfEEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btVectorX*, %struct.btVectorX** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %0, i32 %1
  ret %struct.btVectorX* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVectorXIfE4nrm2Ev(%struct.btVectorX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %norm = alloca float, align 4
  %nn = alloca i32, align 4
  %scale = alloca float, align 4
  %ssq = alloca float, align 4
  %ix = alloca i32, align 4
  %absxi = alloca float, align 4
  %temp = alloca float, align 4
  %temp14 = alloca float, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  store float 0.000000e+00, float* %norm, align 4
  %call = call i32 @_ZNK9btVectorXIfE4rowsEv(%struct.btVectorX* %this1)
  store i32 %call, i32* %nn, align 4
  %0 = load i32, i32* %nn, align 4
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %this1, i32 0)
  %1 = load float, float* %call2, align 4
  %call3 = call float @_Z6btFabsf(float %1)
  store float %call3, float* %norm, align 4
  br label %if.end21

if.else:                                          ; preds = %entry
  store float 0.000000e+00, float* %scale, align 4
  store float 1.000000e+00, float* %ssq, align 4
  store i32 0, i32* %ix, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %2 = load i32, i32* %ix, align 4
  %3 = load i32, i32* %nn, align 4
  %cmp4 = icmp slt i32 %2, %3
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %ix, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %this1, i32 %4)
  %5 = load float, float* %call5, align 4
  %conv = fpext float %5 to double
  %cmp6 = fcmp une double %conv, 0.000000e+00
  br i1 %cmp6, label %if.then7, label %if.end18

if.then7:                                         ; preds = %for.body
  %6 = load i32, i32* %ix, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %this1, i32 %6)
  %7 = load float, float* %call8, align 4
  %call9 = call float @_Z6btFabsf(float %7)
  store float %call9, float* %absxi, align 4
  %8 = load float, float* %scale, align 4
  %9 = load float, float* %absxi, align 4
  %cmp10 = fcmp olt float %8, %9
  br i1 %cmp10, label %if.then11, label %if.else13

if.then11:                                        ; preds = %if.then7
  %10 = load float, float* %scale, align 4
  %11 = load float, float* %absxi, align 4
  %div = fdiv float %10, %11
  store float %div, float* %temp, align 4
  %12 = load float, float* %ssq, align 4
  %13 = load float, float* %temp, align 4
  %14 = load float, float* %temp, align 4
  %mul = fmul float %13, %14
  %mul12 = fmul float %12, %mul
  %add = fadd float %mul12, 1.000000e+00
  store float %add, float* %ssq, align 4
  %15 = load float, float* %absxi, align 4
  store float %15, float* %scale, align 4
  br label %if.end

if.else13:                                        ; preds = %if.then7
  %16 = load float, float* %absxi, align 4
  %17 = load float, float* %scale, align 4
  %div15 = fdiv float %16, %17
  store float %div15, float* %temp14, align 4
  %18 = load float, float* %temp14, align 4
  %19 = load float, float* %temp14, align 4
  %mul16 = fmul float %18, %19
  %20 = load float, float* %ssq, align 4
  %add17 = fadd float %20, %mul16
  store float %add17, float* %ssq, align 4
  br label %if.end

if.end:                                           ; preds = %if.else13, %if.then11
  br label %if.end18

if.end18:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end18
  %21 = load i32, i32* %ix, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %ix, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load float, float* %scale, align 4
  %23 = load float, float* %ssq, align 4
  %call19 = call float @_Z4sqrtf(float %23) #6
  %mul20 = fmul float %22, %call19
  store float %mul20, float* %norm, align 4
  br label %if.end21

if.end21:                                         ; preds = %for.end, %if.then
  %24 = load float, float* %norm, align 4
  ret float %24
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN16btLemkeAlgorithm21LexicographicPositiveERK9btVectorXIfE(%class.btLemkeAlgorithm* %this, %struct.btVectorX* nonnull align 4 dereferenceable(20) %v) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %v.addr = alloca %struct.btVectorX*, align 4
  %i = alloca i32, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store %struct.btVectorX* %v, %struct.btVectorX** %v.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.btVectorX*, %struct.btVectorX** %v.addr, align 4
  %call = call i32 @_ZNK9btVectorXIfE4sizeEv(%struct.btVectorX* %1)
  %sub = sub nsw i32 %call, 1
  %cmp = icmp slt i32 %0, %sub
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load %struct.btVectorX*, %struct.btVectorX** %v.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %2, i32 %3)
  %4 = load float, float* %call2, align 4
  %call3 = call float @_Z4fabsf(float %4) #6
  %call4 = call float @_Z9btMachEpsv()
  %cmp5 = fcmp olt float %call3, %call4
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %5 = phi i1 [ false, %while.cond ], [ %cmp5, %land.rhs ]
  br i1 %5, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %7 = load %struct.btVectorX*, %struct.btVectorX** %v.addr, align 4
  %8 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %7, i32 %8)
  %9 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %while.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVectorXIfEED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %index.addr = alloca i32, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 %0)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btMatrixXIfE4colsEv(%struct.btMatrixX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_cols, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7setElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_setElemOperations, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_setElemOperations, align 4
  %1 = load float, float* %val.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %2 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %2, %3
  %4 = load i32, i32* %col.addr, align 4
  %add = add nsw i32 %mul, %4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 %add)
  store float %1, float* %call, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7mulElemEiif(%struct.btMatrixX* %this, i32 %row, i32 %col, float %val) #1 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %row.addr = alloca i32, align 4
  %col.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store i32 %col, i32* %col.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_setElemOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_setElemOperations, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_setElemOperations, align 4
  %1 = load float, float* %val.addr, align 4
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %2 = load i32, i32* %row.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_cols, align 4
  %mul = mul nsw i32 %2, %3
  %4 = load i32, i32* %col.addr, align 4
  %add = add nsw i32 %mul, %4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 %add)
  %5 = load float, float* %call, align 4
  %mul2 = fmul float %5, %1
  store float %mul2, float* %call, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN16btLemkeAlgorithm11greaterZeroERK9btVectorXIfE(%class.btLemkeAlgorithm* %this, %struct.btVectorX* nonnull align 4 dereferenceable(20) %vector) #2 {
entry:
  %this.addr = alloca %class.btLemkeAlgorithm*, align 4
  %vector.addr = alloca %struct.btVectorX*, align 4
  %isGreater = alloca i8, align 1
  %i = alloca i32, align 4
  store %class.btLemkeAlgorithm* %this, %class.btLemkeAlgorithm** %this.addr, align 4
  store %struct.btVectorX* %vector, %struct.btVectorX** %vector.addr, align 4
  %this1 = load %class.btLemkeAlgorithm*, %class.btLemkeAlgorithm** %this.addr, align 4
  store i8 1, i8* %isGreater, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.btVectorX*, %struct.btVectorX** %vector.addr, align 4
  %call = call i32 @_ZNK9btVectorXIfE4sizeEv(%struct.btVectorX* %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.btVectorX*, %struct.btVectorX** %vector.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVectorXIfEixEi(%struct.btVectorX* %2, i32 %3)
  %4 = load float, float* %call2, align 4
  %cmp3 = fcmp olt float %4, 0.000000e+00
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8 0, i8* %isGreater, align 1
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %6 = load i8, i8* %isGreater, align 1
  %tobool = trunc i8 %6 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIS_IiEED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %3, i32 %4
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %arrayidx) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IiEE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %m_data, align 4
  %tobool = icmp ne %class.btAlignedObjectArray.3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btAlignedObjectArray.3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.3* null, %class.btAlignedObjectArray.3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.3* null, %class.btAlignedObjectArray.3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btAlignedObjectArray.3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btAlignedObjectArray.3* %ptr, %class.btAlignedObjectArray.3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %ptr.addr, align 4
  %1 = bitcast %class.btAlignedObjectArray.3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVectorXIfE4rowsEv(%struct.btVectorX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %m_storage)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z9btSetZeroIfEvPT_i(float* %a, i32 %n) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %acurr = alloca float*, align 4
  %ncurr = alloca i32, align 4
  store float* %a, float** %a.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  store float* %0, float** %acurr, align 4
  %1 = load i32, i32* %n.addr, align 4
  store i32 %1, i32* %ncurr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %2 = load i32, i32* %ncurr, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load float*, float** %acurr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 1
  store float* %incdec.ptr, float** %acurr, align 4
  store float 0.000000e+00, float* %3, align 4
  %4 = load i32, i32* %ncurr, align 4
  %dec = add i32 %4, -1
  store i32 %dec, i32* %ncurr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIS_IiEEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IiEE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE6resizeEii(%struct.btMatrixX* %this, i32 %rows, i32 %cols) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %rows.addr = alloca i32, align 4
  %cols.addr = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %ref.tmp = alloca float, align 4
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  store i32 %rows, i32* %rows.addr, align 4
  store i32 %cols, i32* %cols.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %m_resizeOperations = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_resizeOperations, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_resizeOperations, align 4
  %1 = load i32, i32* %rows.addr, align 4
  %m_rows = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 0
  store i32 %1, i32* %m_rows, align 4
  %2 = load i32, i32* %cols.addr, align 4
  %m_cols = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 1
  store i32 %2, i32* %m_cols, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %3 = load i32, i32* %rows.addr, align 4
  %4 = load i32, i32* %cols.addr, align 4
  %mul = mul nsw i32 %3, %4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %m_storage, i32 %mul, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIiELj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #4

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btMatrixXIfE7setZeroEv(%struct.btMatrixX* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btMatrixX*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %struct.btMatrixX* %this, %struct.btMatrixX** %this.addr, align 4
  %this1 = load %struct.btMatrixX*, %struct.btMatrixX** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.1, i32 0, i32 0))
  %m_storage = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %m_storage, i32 0)
  %m_storage3 = getelementptr inbounds %struct.btMatrixX, %struct.btMatrixX* %this1, i32 0, i32 5
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %m_storage3)
  call void @_Z9btSetZeroIfEvPT_i(float* %call2, i32 %call4)
  %call5 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVectorXIfE4colsEv(%struct.btVectorX* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  ret %class.btAlignedAllocator.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVectorXIfELj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btVectorX* null, %struct.btVectorX** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %struct.btVectorX*, %struct.btVectorX** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %3, i32 %4
  %call = call %struct.btVectorX* @_ZN9btVectorXIfED2Ev(%struct.btVectorX* %arrayidx) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btVectorX*, %struct.btVectorX** %m_data, align 4
  %tobool = icmp ne %struct.btVectorX* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.btVectorX*, %struct.btVectorX** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVectorXIfELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %m_allocator, %struct.btVectorX* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btVectorX* null, %struct.btVectorX** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVectorXIfELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %this, %struct.btVectorX* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %struct.btVectorX*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %struct.btVectorX* %ptr, %struct.btVectorX** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %struct.btVectorX*, %struct.btVectorX** %ptr.addr, align 4
  %1 = bitcast %struct.btVectorX* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVectorXIfEE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btVectorX*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVectorXIfEE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btVectorX*
  store %struct.btVectorX* %2, %struct.btVectorX** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %struct.btVectorX*, %struct.btVectorX** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4copyEiiPS1_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %struct.btVectorX* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVectorXIfEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btVectorX*, %struct.btVectorX** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btVectorX* %4, %struct.btVectorX** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVectorXIfEE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN9btVectorXIfEC2ERKS0_(%struct.btVectorX* returned %this, %struct.btVectorX* nonnull align 4 dereferenceable(20) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btVectorX*, align 4
  %.addr = alloca %struct.btVectorX*, align 4
  store %struct.btVectorX* %this, %struct.btVectorX** %this.addr, align 4
  store %struct.btVectorX* %0, %struct.btVectorX** %.addr, align 4
  %this1 = load %struct.btVectorX*, %struct.btVectorX** %this.addr, align 4
  %m_storage = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %this1, i32 0, i32 0
  %1 = load %struct.btVectorX*, %struct.btVectorX** %.addr, align 4
  %m_storage2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray* %m_storage, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_storage2)
  ret %struct.btVectorX* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVectorXIfEE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btVectorX* @_ZN18btAlignedAllocatorI9btVectorXIfELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %struct.btVectorX** null)
  %2 = bitcast %struct.btVectorX* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVectorXIfEE4copyEiiPS1_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %struct.btVectorX* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btVectorX*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btVectorX* %dest, %struct.btVectorX** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btVectorX*, %struct.btVectorX** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %3, i32 %4
  %5 = bitcast %struct.btVectorX* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btVectorX*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %struct.btVectorX*, %struct.btVectorX** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btVectorX, %struct.btVectorX* %7, i32 %8
  %call = call %struct.btVectorX* @_ZN9btVectorXIfEC2ERKS0_(%struct.btVectorX* %6, %struct.btVectorX* nonnull align 4 dereferenceable(20) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btVectorX* @_ZN18btAlignedAllocatorI9btVectorXIfELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.9* %this, i32 %n, %struct.btVectorX** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btVectorX**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btVectorX** %hint, %struct.btVectorX*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 20, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btVectorX*
  ret %struct.btVectorX* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIfEC2ERKS0_(%class.btAlignedObjectArray* returned %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btAlignedObjectArray* %otherArray, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray* %this1)
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %this1, i32 %1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %2, i32 0, i32 %3, float* %4)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4sqrtf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btLemkeAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
