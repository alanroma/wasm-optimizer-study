; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSequentialImpulseConstraintSolver = type { %class.btConstraintSolver, %class.btAlignedObjectArray, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.18, i32, i32, %class.btAlignedObjectArray.14, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)*, float, i32 }
%class.btConstraintSolver = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSolverBody*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.3, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.6, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.6 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btSolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btSolverConstraint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.12, i32, i32, i32, i32 }
%union.anon.12 = type { i8* }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %"struct.btTypedConstraint::btConstraintInfo1"*, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.22, %union.anon.23, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.22 = type { float }
%union.anon.23 = type { float }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%class.btIDebugDraw = type { i32 (...)** }
%class.CProfileSample = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%class.btDispatcher = type opaque
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN12btSolverBody23internalGetPushVelocityEv = comdat any

$_ZN12btSolverBody23internalGetTurnVelocityEv = comdat any

$_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK12btSolverBody18internalGetInvMassEv = comdat any

$_ZN18btConstraintSolverC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EED2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyED2Ev = comdat any

$_ZN35btSequentialImpulseConstraintSolverdlEPv = comdat any

$_ZN11btRigidBody6upcastEP17btCollisionObject = comdat any

$_ZN12btSolverBody30internalGetDeltaLinearVelocityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN12btSolverBody31internalGetDeltaAngularVelocityEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN12btSolverBody18internalSetInvMassERK9btVector3 = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btRigidBody15getLinearFactorEv = comdat any

$_ZNK11btRigidBody16getAngularFactorEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK11btRigidBody13getTotalForceEv = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody14getTotalTorqueEv = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZNK17btCollisionObject22hasAnisotropicFrictionEi = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK17btCollisionObject22getAnisotropicFrictionEv = comdat any

$_ZN9btVector3mLERKS_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyEixEi = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE21expandNonInitializingEv = comdat any

$_ZNK17btCollisionObject14getCompanionIdEv = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE6expandERKS0_ = comdat any

$_ZN12btSolverBodyC2Ev = comdat any

$_ZN17btCollisionObject14setCompanionIdEi = comdat any

$_ZNK15btManifoldPoint11getDistanceEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK9btVector39fuzzyZeroEv = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN20btPersistentManifold15getContactPointEi = comdat any

$_ZNK20btPersistentManifold29getContactProcessingThresholdEv = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnAEv = comdat any

$_ZNK15btManifoldPoint19getPositionWorldOnBEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK12btSolverBody30getVelocityInLocalPointNoDeltaERK9btVector3RS0_ = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE6resizeEiRKS0_ = comdat any

$_ZNK11btRigidBody8getFlagsEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN17btTypedConstraint25internalSetAppliedImpulseEf = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE18resizeNoInitializeEi = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi = comdat any

$_ZN17btTypedConstraint16getJointFeedbackEv = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZNK17btTypedConstraint30getOverrideNumSolverIterationsEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv = comdat any

$_Z6btFabsf = comdat any

$_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN17btTypedConstraint10setEnabledEb = comdat any

$_ZN12btSolverBody29writebackVelocityAndTransformEff = comdat any

$_ZN12btSolverBody17writebackVelocityEv = comdat any

$_ZN11btRigidBody17setLinearVelocityERK9btVector3 = comdat any

$_ZN11btRigidBody18setAngularVelocityERK9btVector3 = comdat any

$_ZN17btCollisionObject17setWorldTransformERK11btTransform = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE18resizeNoInitializeEi = comdat any

$_ZN18btConstraintSolver12prepareSolveEii = comdat any

$_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw = comdat any

$_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN18btConstraintSolverD2Ev = comdat any

$_ZN18btConstraintSolverD0Ev = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZN18btAlignedAllocatorI12btSolverBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE4initEv = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4initEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btSolverBodyLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btSolverConstraintnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI12btSolverBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE9allocSizeEi = comdat any

$_ZN12btSolverBodynwEmPv = comdat any

$_ZN12btSolverBodyC2ERKS_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI12btSolverBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI12btSolverBodyLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZTS18btConstraintSolver = comdat any

$_ZTI18btConstraintSolver = comdat any

$_ZTV18btConstraintSolver = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gNumSplitImpulseRecoveries = hidden global i32 0, align 4
@_ZTV35btSequentialImpulseConstraintSolver = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI35btSequentialImpulseConstraintSolver to i8*), i8* bitcast (%class.btSequentialImpulseConstraintSolver* (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolverD1Ev to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)* @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolver5resetEv to i8*), i8* bitcast (i32 (%class.btSequentialImpulseConstraintSolver*)* @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)* @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw to i8*)] }, align 4
@.str = private unnamed_addr constant [29 x i8] c"solveGroupCacheFriendlySetup\00", align 1
@.str.1 = private unnamed_addr constant [34 x i8] c"solveGroupCacheFriendlyIterations\00", align 1
@.str.2 = private unnamed_addr constant [11 x i8] c"solveGroup\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS35btSequentialImpulseConstraintSolver = hidden constant [38 x i8] c"35btSequentialImpulseConstraintSolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btConstraintSolver = linkonce_odr hidden constant [21 x i8] c"18btConstraintSolver\00", comdat, align 1
@_ZTI18btConstraintSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btConstraintSolver, i32 0, i32 0) }, comdat, align 4
@_ZTI35btSequentialImpulseConstraintSolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTS35btSequentialImpulseConstraintSolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI18btConstraintSolver to i8*) }, align 4
@_ZTV18btConstraintSolver = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btConstraintSolver*)* @_ZN18btConstraintSolverD2Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*)* @_ZN18btConstraintSolverD0Ev to i8*), i8* bitcast (void (%class.btConstraintSolver*, i32, i32)* @_ZN18btConstraintSolver12prepareSolveEii to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)* @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSequentialImpulseConstraintSolver.cpp, i8* null }]

@_ZN35btSequentialImpulseConstraintSolverC1Ev = hidden unnamed_addr alias %class.btSequentialImpulseConstraintSolver* (%class.btSequentialImpulseConstraintSolver*), %class.btSequentialImpulseConstraintSolver* (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolverC2Ev
@_ZN35btSequentialImpulseConstraintSolverD1Ev = hidden unnamed_addr alias %class.btSequentialImpulseConstraintSolver* (%class.btSequentialImpulseConstraintSolver*), %class.btSequentialImpulseConstraintSolver* (%class.btSequentialImpulseConstraintSolver*)* @_ZN35btSequentialImpulseConstraintSolverD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %call = call float @_ZL51gResolveSingleConstraintRowGeneric_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint(%struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2)
  ret float %call
}

; Function Attrs: noinline optnone
define internal float @_ZL51gResolveSingleConstraintRowGeneric_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint(%struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVel1Dotn = alloca float, align 4
  %deltaVel2Dotn = alloca float, align 4
  %sum = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 0, i32 10
  %1 = load float, float* %m_rhs, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %2, i32 0, i32 7
  %3 = load float, float* %m_appliedImpulse, align 4
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %4, i32 0, i32 11
  %5 = load float, float* %m_cfm, align 4
  %mul = fmul float %3, %5
  %sub = fsub float %1, %mul
  store float %sub, float* %deltaImpulse, align 4
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %6, i32 0, i32 1
  %7 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %7)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %8 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %8, i32 0, i32 0
  %9 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %9)
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %add = fadd float %call1, %call3
  store float %add, float* %deltaVel1Dotn, align 4
  %10 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %10, i32 0, i32 3
  %11 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %11)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %12 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %12, i32 0, i32 2
  %13 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %13)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %add8 = fadd float %call5, %call7
  store float %add8, float* %deltaVel2Dotn, align 4
  %14 = load float, float* %deltaVel1Dotn, align 4
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 9
  %16 = load float, float* %m_jacDiagABInv, align 4
  %mul9 = fmul float %14, %16
  %17 = load float, float* %deltaImpulse, align 4
  %sub10 = fsub float %17, %mul9
  store float %sub10, float* %deltaImpulse, align 4
  %18 = load float, float* %deltaVel2Dotn, align 4
  %19 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv11 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %19, i32 0, i32 9
  %20 = load float, float* %m_jacDiagABInv11, align 4
  %mul12 = fmul float %18, %20
  %21 = load float, float* %deltaImpulse, align 4
  %sub13 = fsub float %21, %mul12
  store float %sub13, float* %deltaImpulse, align 4
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse14 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %22, i32 0, i32 7
  %23 = load float, float* %m_appliedImpulse14, align 4
  %24 = load float, float* %deltaImpulse, align 4
  %add15 = fadd float %23, %24
  store float %add15, float* %sum, align 4
  %25 = load float, float* %sum, align 4
  %26 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %26, i32 0, i32 12
  %27 = load float, float* %m_lowerLimit, align 4
  %cmp = fcmp olt float %25, %27
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %28 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit16 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %28, i32 0, i32 12
  %29 = load float, float* %m_lowerLimit16, align 4
  %30 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse17 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %30, i32 0, i32 7
  %31 = load float, float* %m_appliedImpulse17, align 4
  %sub18 = fsub float %29, %31
  store float %sub18, float* %deltaImpulse, align 4
  %32 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit19 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %32, i32 0, i32 12
  %33 = load float, float* %m_lowerLimit19, align 4
  %34 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse20 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %34, i32 0, i32 7
  store float %33, float* %m_appliedImpulse20, align 4
  br label %if.end30

if.else:                                          ; preds = %entry
  %35 = load float, float* %sum, align 4
  %36 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %36, i32 0, i32 13
  %37 = load float, float* %m_upperLimit, align 4
  %cmp21 = fcmp ogt float %35, %37
  br i1 %cmp21, label %if.then22, label %if.else28

if.then22:                                        ; preds = %if.else
  %38 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_upperLimit23 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %38, i32 0, i32 13
  %39 = load float, float* %m_upperLimit23, align 4
  %40 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse24 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %40, i32 0, i32 7
  %41 = load float, float* %m_appliedImpulse24, align 4
  %sub25 = fsub float %39, %41
  store float %sub25, float* %deltaImpulse, align 4
  %42 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_upperLimit26 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %42, i32 0, i32 13
  %43 = load float, float* %m_upperLimit26, align 4
  %44 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse27 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %44, i32 0, i32 7
  store float %43, float* %m_appliedImpulse27, align 4
  br label %if.end

if.else28:                                        ; preds = %if.else
  %45 = load float, float* %sum, align 4
  %46 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse29 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %46, i32 0, i32 7
  store float %45, float* %m_appliedImpulse29, align 4
  br label %if.end

if.end:                                           ; preds = %if.else28, %if.then22
  br label %if.end30

if.end30:                                         ; preds = %if.end, %if.then
  %47 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %48 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal131 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %48, i32 0, i32 1
  %49 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %49)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal131, %class.btVector3* nonnull align 4 dereferenceable(16) %call32)
  %50 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %50, i32 0, i32 4
  %51 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %51)
  %52 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %53 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal234 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %53, i32 0, i32 3
  %54 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %54)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal234, %class.btVector3* nonnull align 4 dereferenceable(16) %call35)
  %55 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %55, i32 0, i32 5
  %56 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %56)
  %57 = load float, float* %deltaImpulse, align 4
  ret float %57
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %call = call float @_ZL54gResolveSingleConstraintRowLowerLimit_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint(%struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2)
  ret float %call
}

; Function Attrs: noinline optnone
define internal float @_ZL54gResolveSingleConstraintRowLowerLimit_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint(%struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVel1Dotn = alloca float, align 4
  %deltaVel2Dotn = alloca float, align 4
  %sum = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 0, i32 10
  %1 = load float, float* %m_rhs, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %2, i32 0, i32 7
  %3 = load float, float* %m_appliedImpulse, align 4
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %4, i32 0, i32 11
  %5 = load float, float* %m_cfm, align 4
  %mul = fmul float %3, %5
  %sub = fsub float %1, %mul
  store float %sub, float* %deltaImpulse, align 4
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %6, i32 0, i32 1
  %7 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %7)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %8 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %8, i32 0, i32 0
  %9 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %9)
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %add = fadd float %call1, %call3
  store float %add, float* %deltaVel1Dotn, align 4
  %10 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %10, i32 0, i32 3
  %11 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %11)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %12 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %12, i32 0, i32 2
  %13 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %13)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %add8 = fadd float %call5, %call7
  store float %add8, float* %deltaVel2Dotn, align 4
  %14 = load float, float* %deltaVel1Dotn, align 4
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 9
  %16 = load float, float* %m_jacDiagABInv, align 4
  %mul9 = fmul float %14, %16
  %17 = load float, float* %deltaImpulse, align 4
  %sub10 = fsub float %17, %mul9
  store float %sub10, float* %deltaImpulse, align 4
  %18 = load float, float* %deltaVel2Dotn, align 4
  %19 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv11 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %19, i32 0, i32 9
  %20 = load float, float* %m_jacDiagABInv11, align 4
  %mul12 = fmul float %18, %20
  %21 = load float, float* %deltaImpulse, align 4
  %sub13 = fsub float %21, %mul12
  store float %sub13, float* %deltaImpulse, align 4
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse14 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %22, i32 0, i32 7
  %23 = load float, float* %m_appliedImpulse14, align 4
  %24 = load float, float* %deltaImpulse, align 4
  %add15 = fadd float %23, %24
  store float %add15, float* %sum, align 4
  %25 = load float, float* %sum, align 4
  %26 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %26, i32 0, i32 12
  %27 = load float, float* %m_lowerLimit, align 4
  %cmp = fcmp olt float %25, %27
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %28 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit16 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %28, i32 0, i32 12
  %29 = load float, float* %m_lowerLimit16, align 4
  %30 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse17 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %30, i32 0, i32 7
  %31 = load float, float* %m_appliedImpulse17, align 4
  %sub18 = fsub float %29, %31
  store float %sub18, float* %deltaImpulse, align 4
  %32 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit19 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %32, i32 0, i32 12
  %33 = load float, float* %m_lowerLimit19, align 4
  %34 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse20 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %34, i32 0, i32 7
  store float %33, float* %m_appliedImpulse20, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %35 = load float, float* %sum, align 4
  %36 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedImpulse21 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %36, i32 0, i32 7
  store float %35, float* %m_appliedImpulse21, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %37 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %38 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal122 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %38, i32 0, i32 1
  %39 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %39)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal122, %class.btVector3* nonnull align 4 dereferenceable(16) %call23)
  %40 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %40, i32 0, i32 4
  %41 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %41)
  %42 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %43 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal225 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %43, i32 0, i32 3
  %44 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %44)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal225, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %45 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %45, i32 0, i32 5
  %46 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %46)
  %47 = load float, float* %deltaImpulse, align 4
  ret float %47
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  %deltaImpulse = alloca float, align 4
  %deltaVel1Dotn = alloca float, align 4
  %deltaVel2Dotn = alloca float, align 4
  %sum = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store float 0.000000e+00, float* %deltaImpulse, align 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 0, i32 14
  %1 = load float, float* %m_rhsPenetration, align 4
  %tobool = fcmp une float %1, 0.000000e+00
  br i1 %tobool, label %if.then, label %if.end30

if.then:                                          ; preds = %entry
  %2 = load i32, i32* @gNumSplitImpulseRecoveries, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* @gNumSplitImpulseRecoveries, align 4
  %3 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_rhsPenetration2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %3, i32 0, i32 14
  %4 = load float, float* %m_rhsPenetration2, align 4
  %5 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %5, i32 0, i32 6
  %6 = load float, float* %m_appliedPushImpulse, align 4
  %7 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %7, i32 0, i32 11
  %8 = load float, float* %m_cfm, align 4
  %mul = fmul float %6, %8
  %sub = fsub float %4, %mul
  store float %sub, float* %deltaImpulse, align 4
  %9 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %9, i32 0, i32 1
  %10 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %10)
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %11 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %11, i32 0, i32 0
  %12 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %12)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %add = fadd float %call3, %call5
  store float %add, float* %deltaVel1Dotn, align 4
  %13 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %13, i32 0, i32 3
  %14 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %14)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 2
  %16 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %16)
  %call9 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call8)
  %add10 = fadd float %call7, %call9
  store float %add10, float* %deltaVel2Dotn, align 4
  %17 = load float, float* %deltaVel1Dotn, align 4
  %18 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %18, i32 0, i32 9
  %19 = load float, float* %m_jacDiagABInv, align 4
  %mul11 = fmul float %17, %19
  %20 = load float, float* %deltaImpulse, align 4
  %sub12 = fsub float %20, %mul11
  store float %sub12, float* %deltaImpulse, align 4
  %21 = load float, float* %deltaVel2Dotn, align 4
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_jacDiagABInv13 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %22, i32 0, i32 9
  %23 = load float, float* %m_jacDiagABInv13, align 4
  %mul14 = fmul float %21, %23
  %24 = load float, float* %deltaImpulse, align 4
  %sub15 = fsub float %24, %mul14
  store float %sub15, float* %deltaImpulse, align 4
  %25 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedPushImpulse16 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %25, i32 0, i32 6
  %26 = load float, float* %m_appliedPushImpulse16, align 4
  %27 = load float, float* %deltaImpulse, align 4
  %add17 = fadd float %26, %27
  store float %add17, float* %sum, align 4
  %28 = load float, float* %sum, align 4
  %29 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %29, i32 0, i32 12
  %30 = load float, float* %m_lowerLimit, align 4
  %cmp = fcmp olt float %28, %30
  br i1 %cmp, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.then
  %31 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit19 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %31, i32 0, i32 12
  %32 = load float, float* %m_lowerLimit19, align 4
  %33 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedPushImpulse20 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %33, i32 0, i32 6
  %34 = load float, float* %m_appliedPushImpulse20, align 4
  %sub21 = fsub float %32, %34
  store float %sub21, float* %deltaImpulse, align 4
  %35 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_lowerLimit22 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %35, i32 0, i32 12
  %36 = load float, float* %m_lowerLimit22, align 4
  %37 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedPushImpulse23 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %37, i32 0, i32 6
  store float %36, float* %m_appliedPushImpulse23, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %38 = load float, float* %sum, align 4
  %39 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_appliedPushImpulse24 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %39, i32 0, i32 6
  store float %38, float* %m_appliedPushImpulse24, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then18
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %41 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal125 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %41, i32 0, i32 1
  %42 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %42)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal125, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %43 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %43, i32 0, i32 4
  %44 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %44)
  %45 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %46 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_contactNormal228 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %46, i32 0, i32 3
  %47 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %47)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal228, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  %48 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %48, i32 0, i32 5
  %49 = load float, float* %deltaImpulse, align 4
  call void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB, float %49)
  br label %if.end30

if.end30:                                         ; preds = %if.end, %entry
  %50 = load float, float* %deltaImpulse, align 4
  ret float %50
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  ret %class.btVector3* %m_pushVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  ret %class.btVector3* %m_turnVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody24internalApplyPushImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_pushVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_turnVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_invMass
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %body2, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %c) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body1.addr = alloca %struct.btSolverBody*, align 4
  %body2.addr = alloca %struct.btSolverBody*, align 4
  %c.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %body1, %struct.btSolverBody** %body1.addr, align 4
  store %struct.btSolverBody* %body2, %struct.btSolverBody** %body2.addr, align 4
  store %struct.btSolverConstraint* %c, %struct.btSolverConstraint** %c.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %body1.addr, align 4
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %body2.addr, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %c.addr, align 4
  %call = call float @_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverC2Ev(%class.btSequentialImpulseConstraintSolver* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to %class.btConstraintSolver*
  %call = call %class.btConstraintSolver* @_ZN18btConstraintSolverC2Ev(%class.btConstraintSolver* %0) #9
  %1 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTV35btSequentialImpulseConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSolverBodyEC2Ev(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %call5 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 5
  %call6 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool)
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %call7 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool)
  %m_orderNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 7
  %call8 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool)
  %m_orderFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 8
  %call9 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool)
  %m_tmpConstraintSizesPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %call10 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEC2Ev(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool)
  %m_kinematicBodyUniqueIdToSolverBodyTable = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 12
  %call11 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* %m_kinematicBodyUniqueIdToSolverBodyTable)
  %m_resolveSingleConstraintRowGeneric = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 13
  store float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZL51gResolveSingleConstraintRowGeneric_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)** %m_resolveSingleConstraintRowGeneric, align 4
  %m_resolveSingleConstraintRowLowerLimit = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 14
  store float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZL54gResolveSingleConstraintRowLowerLimit_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint, float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)** %m_resolveSingleConstraintRowLowerLimit, align 4
  %m_btSeed2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 16
  store i32 0, i32* %m_btSeed2, align 4
  ret %class.btSequentialImpulseConstraintSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConstraintSolver* @_ZN18btConstraintSolverC2Ev(%class.btConstraintSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  %this1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV18btConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btConstraintSolver* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSolverBodyEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSolverBodyLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.15* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEC2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EEC2Ev(%class.btAlignedAllocator.19* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD2Ev(%class.btSequentialImpulseConstraintSolver* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTV35btSequentialImpulseConstraintSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_kinematicBodyUniqueIdToSolverBodyTable = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 12
  %call = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_kinematicBodyUniqueIdToSolverBodyTable) #9
  %m_tmpConstraintSizesPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %call2 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EED2Ev(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool) #9
  %m_orderFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 8
  %call3 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool) #9
  %m_orderNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 7
  %call4 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool) #9
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %call5 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool) #9
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 5
  %call6 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool) #9
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %call7 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool) #9
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %call8 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool) #9
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call9 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool) #9
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %call10 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSolverBodyED2Ev(%class.btAlignedObjectArray* %m_tmpSolverBodyPool) #9
  %1 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to %class.btConstraintSolver*
  %call11 = call %class.btConstraintSolver* @_ZN18btConstraintSolverD2Ev(%class.btConstraintSolver* %1) #9
  ret %class.btSequentialImpulseConstraintSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EED2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE5clearEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI18btSolverConstraintED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSolverBodyED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolverD0Ev(%class.btSequentialImpulseConstraintSolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %call = call %class.btSequentialImpulseConstraintSolver* @_ZN35btSequentialImpulseConstraintSolverD1Ev(%class.btSequentialImpulseConstraintSolver* %this1) #9
  %0 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to i8*
  call void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN35btSequentialImpulseConstraintSolverdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZN35btSequentialImpulseConstraintSolver35getScalarConstraintRowSolverGenericEv(%class.btSequentialImpulseConstraintSolver* %this) #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  ret float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZL51gResolveSingleConstraintRowGeneric_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint
}

; Function Attrs: noinline nounwind optnone
define hidden float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZN35btSequentialImpulseConstraintSolver38getScalarConstraintRowSolverLowerLimitEv(%class.btSequentialImpulseConstraintSolver* %this) #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  ret float (%struct.btSolverBody*, %struct.btSolverBody*, %struct.btSolverConstraint*)* @_ZL54gResolveSingleConstraintRowLowerLimit_scalar_referenceR12btSolverBodyS0_RK18btSolverConstraint
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN35btSequentialImpulseConstraintSolver7btRand2Ev(%class.btSequentialImpulseConstraintSolver* %this) #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_btSeed2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_btSeed2, align 4
  %mul = mul i32 1664525, %0
  %add = add i32 %mul, 1013904223
  %m_btSeed22 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 16
  store i32 %add, i32* %m_btSeed22, align 4
  %m_btSeed23 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 16
  %1 = load i32, i32* %m_btSeed23, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %this, i32 %n) #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %n.addr = alloca i32, align 4
  %un = alloca i32, align 4
  %r = alloca i32, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  store i32 %0, i32* %un, align 4
  %call = call i32 @_ZN35btSequentialImpulseConstraintSolver7btRand2Ev(%class.btSequentialImpulseConstraintSolver* %this1)
  store i32 %call, i32* %r, align 4
  %1 = load i32, i32* %un, align 4
  %cmp = icmp ule i32 %1, 65536
  br i1 %cmp, label %if.then, label %if.end21

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %r, align 4
  %shr = lshr i32 %2, 16
  %3 = load i32, i32* %r, align 4
  %xor = xor i32 %3, %shr
  store i32 %xor, i32* %r, align 4
  %4 = load i32, i32* %un, align 4
  %cmp2 = icmp ule i32 %4, 256
  br i1 %cmp2, label %if.then3, label %if.end20

if.then3:                                         ; preds = %if.then
  %5 = load i32, i32* %r, align 4
  %shr4 = lshr i32 %5, 8
  %6 = load i32, i32* %r, align 4
  %xor5 = xor i32 %6, %shr4
  store i32 %xor5, i32* %r, align 4
  %7 = load i32, i32* %un, align 4
  %cmp6 = icmp ule i32 %7, 16
  br i1 %cmp6, label %if.then7, label %if.end19

if.then7:                                         ; preds = %if.then3
  %8 = load i32, i32* %r, align 4
  %shr8 = lshr i32 %8, 4
  %9 = load i32, i32* %r, align 4
  %xor9 = xor i32 %9, %shr8
  store i32 %xor9, i32* %r, align 4
  %10 = load i32, i32* %un, align 4
  %cmp10 = icmp ule i32 %10, 4
  br i1 %cmp10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.then7
  %11 = load i32, i32* %r, align 4
  %shr12 = lshr i32 %11, 2
  %12 = load i32, i32* %r, align 4
  %xor13 = xor i32 %12, %shr12
  store i32 %xor13, i32* %r, align 4
  %13 = load i32, i32* %un, align 4
  %cmp14 = icmp ule i32 %13, 2
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then11
  %14 = load i32, i32* %r, align 4
  %shr16 = lshr i32 %14, 1
  %15 = load i32, i32* %r, align 4
  %xor17 = xor i32 %15, %shr16
  store i32 %xor17, i32* %r, align 4
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.then11
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then7
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then3
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %if.then
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %entry
  %16 = load i32, i32* %r, align 4
  %17 = load i32, i32* %un, align 4
  %rem = urem i32 %16, %17
  ret i32 %rem
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverBody* %solverBody, %class.btCollisionObject* %collisionObject, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %solverBody.addr = alloca %struct.btSolverBody*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %timeStep.addr = alloca float, align 4
  %rb = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp46 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverBody* %solverBody, %struct.btSolverBody** %solverBody.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %tobool = icmp ne %class.btCollisionObject* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %1)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btRigidBody* [ %call, %cond.true ], [ null, %cond.false ]
  store %class.btRigidBody* %cond, %class.btRigidBody** %rb, align 4
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %2)
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call2, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %3 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %3)
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %4 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %4)
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %5 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %5)
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %6 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %tobool17 = icmp ne %class.btRigidBody* %6, null
  br i1 %tobool17, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %7 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %8 = bitcast %class.btRigidBody* %7 to %class.btCollisionObject*
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %8)
  %9 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %9, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %call18)
  %10 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %11 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call23 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %11)
  store float %call23, float* %ref.tmp22, align 4
  %12 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call25 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %12)
  store float %call25, float* %ref.tmp24, align 4
  %13 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call27 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %13)
  store float %call27, float* %ref.tmp26, align 4
  %call28 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %14 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %14)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  call void @_ZN12btSolverBody18internalSetInvMassERK9btVector3(%struct.btSolverBody* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %15 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %16 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %16, i32 0, i32 12
  store %class.btRigidBody* %15, %class.btRigidBody** %m_originalBody, align 4
  %17 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %17)
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %18, i32 0, i32 3
  %19 = bitcast %class.btVector3* %m_angularFactor to i8*
  %20 = bitcast %class.btVector3* %call30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %21)
  %22 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %22, i32 0, i32 4
  %23 = bitcast %class.btVector3* %m_linearFactor to i8*
  %24 = bitcast %class.btVector3* %call31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %25 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %25)
  %26 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %26, i32 0, i32 8
  %27 = bitcast %class.btVector3* %m_linearVelocity to i8*
  %28 = bitcast %class.btVector3* %call32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %29)
  %30 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %30, i32 0, i32 9
  %31 = bitcast %class.btVector3* %m_angularVelocity to i8*
  %32 = bitcast %class.btVector3* %call33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  %33 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody13getTotalForceEv(%class.btRigidBody* %33)
  %34 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call38 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %34)
  store float %call38, float* %ref.tmp37, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %call36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %35 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %35, i32 0, i32 10
  %36 = bitcast %class.btVector3* %m_externalForceImpulse to i8*
  %37 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  %38 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody14getTotalTorqueEv(%class.btRigidBody* %38)
  %39 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call42 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %39)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %call41, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call42)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %40, i32 0, i32 11
  %41 = bitcast %class.btVector3* %m_externalTorqueImpulse to i8*
  %42 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %cond.end
  %43 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_worldTransform43 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %43, i32 0, i32 0
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_worldTransform43)
  %44 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  store float 0.000000e+00, float* %ref.tmp45, align 4
  store float 0.000000e+00, float* %ref.tmp46, align 4
  store float 0.000000e+00, float* %ref.tmp47, align 4
  %call48 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp46, float* nonnull align 4 dereferenceable(4) %ref.tmp47)
  call void @_ZN12btSolverBody18internalSetInvMassERK9btVector3(%struct.btSolverBody* %44, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp44)
  %45 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_originalBody49 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %45, i32 0, i32 12
  store %class.btRigidBody* null, %class.btRigidBody** %m_originalBody49, align 4
  %46 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_angularFactor50 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %46, i32 0, i32 3
  store float 1.000000e+00, float* %ref.tmp51, align 4
  store float 1.000000e+00, float* %ref.tmp52, align 4
  store float 1.000000e+00, float* %ref.tmp53, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularFactor50, float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %47 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_linearFactor54 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %47, i32 0, i32 4
  store float 1.000000e+00, float* %ref.tmp55, align 4
  store float 1.000000e+00, float* %ref.tmp56, align 4
  store float 1.000000e+00, float* %ref.tmp57, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearFactor54, float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  %48 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_linearVelocity58 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %48, i32 0, i32 8
  store float 0.000000e+00, float* %ref.tmp59, align 4
  store float 0.000000e+00, float* %ref.tmp60, align 4
  store float 0.000000e+00, float* %ref.tmp61, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearVelocity58, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %49 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_angularVelocity62 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %49, i32 0, i32 9
  store float 0.000000e+00, float* %ref.tmp63, align 4
  store float 0.000000e+00, float* %ref.tmp64, align 4
  store float 0.000000e+00, float* %ref.tmp65, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularVelocity62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  %50 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_externalForceImpulse66 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %50, i32 0, i32 10
  store float 0.000000e+00, float* %ref.tmp67, align 4
  store float 0.000000e+00, float* %ref.tmp68, align 4
  store float 0.000000e+00, float* %ref.tmp69, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_externalForceImpulse66, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %51 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody.addr, align 4
  %m_externalTorqueImpulse70 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %51, i32 0, i32 11
  store float 0.000000e+00, float* %ref.tmp71, align 4
  store float 0.000000e+00, float* %ref.tmp72, align 4
  store float 0.000000e+00, float* %ref.tmp73, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_externalTorqueImpulse70, float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #2 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  ret %class.btVector3* %m_deltaLinearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_deltaAngularVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btSolverBody18internalSetInvMassERK9btVector3(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %invMass) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %invMass.addr = alloca %class.btVector3*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %invMass, %class.btVector3** %invMass.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %invMass.addr, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  %1 = bitcast %class.btVector3* %m_invMass to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  ret %class.btVector3* %m_linearFactor
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  ret %class.btVector3* %m_angularFactor
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody13getTotalForceEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  ret %class.btVector3* %m_totalForce
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody14getTotalTorqueEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  ret %class.btVector3* %m_totalTorque
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver* %this, float %rel_vel, float %restitution) #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %rel_vel.addr = alloca float, align 4
  %restitution.addr = alloca float, align 4
  %rest = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store float %rel_vel, float* %rel_vel.addr, align 4
  store float %restitution, float* %restitution.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load float, float* %restitution.addr, align 4
  %1 = load float, float* %rel_vel.addr, align 4
  %fneg = fneg float %1
  %mul = fmul float %0, %fneg
  store float %mul, float* %rest, align 4
  %2 = load float, float* %rest, align 4
  ret float %2
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %colObj, %class.btVector3* nonnull align 4 dereferenceable(16) %frictionDirection, i32 %frictionMode) #2 {
entry:
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  %frictionDirection.addr = alloca %class.btVector3*, align 4
  %frictionMode.addr = alloca i32, align 4
  %loc_lateral = alloca %class.btVector3, align 4
  %friction_scaling = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  store %class.btVector3* %frictionDirection, %class.btVector3** %frictionDirection.addr, align 4
  store i32 %frictionMode, i32* %frictionMode.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %tobool = icmp ne %class.btCollisionObject* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = load i32, i32* %frictionMode.addr, align 4
  %call = call zeroext i1 @_ZNK17btCollisionObject22hasAnisotropicFrictionEi(%class.btCollisionObject* %1, i32 %2)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %3 = load %class.btVector3*, %class.btVector3** %frictionDirection.addr, align 4
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %4)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %loc_lateral, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call2)
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject22getAnisotropicFrictionEv(%class.btCollisionObject* %5)
  store %class.btVector3* %call3, %class.btVector3** %friction_scaling, align 4
  %6 = load %class.btVector3*, %class.btVector3** %friction_scaling, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %loc_lateral, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %7)
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %call5)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %loc_lateral)
  %8 = load %class.btVector3*, %class.btVector3** %frictionDirection.addr, align 4
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject22hasAnisotropicFrictionEi(%class.btCollisionObject* %this, i32 %frictionMode) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %frictionMode.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %frictionMode, i32* %frictionMode.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_hasAnisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 6
  %0 = load i32, i32* %m_hasAnisotropicFriction, align 4
  %1 = load i32, i32* %frictionMode.addr, align 4
  %and = and i32 %0, %1
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btCollisionObject22getAnisotropicFrictionEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_anisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 5
  ret %class.btVector3* %m_anisotropicFriction
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %4
  store float %mul8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %8, %7
  store float %mul13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver23setupFrictionConstraintER18btSolverConstraintRK9btVector3iiR15btManifoldPointS4_S4_P17btCollisionObjectS8_fff(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %solverConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, i32 %solverBodyIdA, i32 %solverBodyIdB, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btSolverConstraint*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %body0 = alloca %class.btRigidBody*, align 4
  %body1 = alloca %class.btRigidBody*, align 4
  %ftorqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ftorqueAxis119 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %denom = alloca float, align 4
  %rel_vel = alloca float, align 4
  %vel1Dotn = alloca float, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp62 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %vel2Dotn = alloca float, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp82 = alloca %class.btVector3, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %velocityError = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverConstraint* %solverConstraint, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %0 = load i32, i32* %solverBodyIdA.addr, align 4
  %call = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %0)
  store %struct.btSolverBody* %call, %struct.btSolverBody** %solverBodyA, align 4
  %m_tmpSolverBodyPool2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %1 = load i32, i32* %solverBodyIdB.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool2, i32 %1)
  store %struct.btSolverBody* %call3, %struct.btSolverBody** %solverBodyB, align 4
  %m_tmpSolverBodyPool4 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %2 = load i32, i32* %solverBodyIdA.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool4, i32 %2)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call5, i32 0, i32 12
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %3, %class.btRigidBody** %body0, align 4
  %m_tmpSolverBodyPool6 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %4 = load i32, i32* %solverBodyIdB.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool6, i32 %4)
  %m_originalBody8 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call7, i32 0, i32 12
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody8, align 4
  store %class.btRigidBody* %5, %class.btRigidBody** %body1, align 4
  %6 = load i32, i32* %solverBodyIdA.addr, align 4
  %7 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %7, i32 0, i32 18
  store i32 %6, i32* %m_solverBodyIdA, align 4
  %8 = load i32, i32* %solverBodyIdB.addr, align 4
  %9 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %9, i32 0, i32 19
  store i32 %8, i32* %m_solverBodyIdB, align 4
  %10 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %10, i32 0, i32 6
  %11 = load float, float* %m_combinedFriction, align 4
  %12 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %12, i32 0, i32 8
  store float %11, float* %m_friction, align 4
  %13 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %14 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %13, i32 0, i32 15
  %m_originalContactPoint = bitcast %union.anon.12* %14 to i8**
  store i8* null, i8** %m_originalContactPoint, align 4
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %16 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %16, i32 0, i32 6
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  %17 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool = icmp ne %class.btRigidBody* %17, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %18 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %19 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %19, i32 0, i32 1
  %20 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %21 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  %22 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %23 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal19 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %23, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis1, %class.btVector3* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal19)
  %24 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %24, i32 0, i32 0
  %25 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %26 = bitcast %class.btVector3* %ftorqueAxis1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %27 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %27)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call11, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis1)
  %28 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %28)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  %29 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %29, i32 0, i32 4
  %30 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %31 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %32 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal113 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %32, i32 0, i32 1
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal113)
  %33 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal14 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %33, i32 0, i32 0
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos1CrossNormal14)
  %34 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA15 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %34, i32 0, i32 4
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_angularComponentA15)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %35 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool16 = icmp ne %class.btRigidBody* %35, null
  br i1 %tobool16, label %if.then17, label %if.else25

if.then17:                                        ; preds = %if.end
  %36 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %36)
  %37 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %37, i32 0, i32 3
  %38 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %39 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false)
  %40 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %41 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal220 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %41, i32 0, i32 3
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ftorqueAxis119, %class.btVector3* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal220)
  %42 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %42, i32 0, i32 2
  %43 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %44 = bitcast %class.btVector3* %ftorqueAxis119 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 16, i1 false)
  %45 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %45)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp22, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call23, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis119)
  %46 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %46)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  %47 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %47, i32 0, i32 5
  %48 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %49 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false)
  br label %if.end29

if.else25:                                        ; preds = %if.end
  %50 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal226 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %50, i32 0, i32 3
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal226)
  %51 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal27 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %51, i32 0, i32 2
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos2CrossNormal27)
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB28 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 5
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_angularComponentB28)
  br label %if.end29

if.end29:                                         ; preds = %if.else25, %if.then17
  %call30 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  store float 0.000000e+00, float* %denom0, align 4
  store float 0.000000e+00, float* %denom1, align 4
  %53 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool31 = icmp ne %class.btRigidBody* %53, null
  br i1 %tobool31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end29
  %54 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA34 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %54, i32 0, i32 4
  %55 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* %m_angularComponentA34, %class.btVector3* nonnull align 4 dereferenceable(16) %55)
  %56 = bitcast %class.btVector3* %vec to i8*
  %57 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false)
  %58 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call35 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %58)
  %59 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %call36 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %59, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add = fadd float %call35, %call36
  store float %add, float* %denom0, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.then32, %if.end29
  %60 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool38 = icmp ne %class.btRigidBody* %60, null
  br i1 %tobool38, label %if.then39, label %if.end46

if.then39:                                        ; preds = %if.end37
  %61 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB42 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %61, i32 0, i32 5
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB42)
  %62 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  %63 = bitcast %class.btVector3* %vec to i8*
  %64 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 16, i1 false)
  %65 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call43 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %65)
  %66 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %call44 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %66, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add45 = fadd float %call43, %call44
  store float %add45, float* %denom1, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.then39, %if.end37
  %67 = load float, float* %relaxation.addr, align 4
  %68 = load float, float* %denom0, align 4
  %69 = load float, float* %denom1, align 4
  %add47 = fadd float %68, %69
  %div = fdiv float %67, %add47
  store float %div, float* %denom, align 4
  %70 = load float, float* %denom, align 4
  %71 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %71, i32 0, i32 9
  store float %70, float* %m_jacDiagABInv, align 4
  %72 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal148 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %72, i32 0, i32 1
  %73 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool50 = icmp ne %class.btRigidBody* %73, null
  br i1 %tobool50, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end46
  %74 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %74, i32 0, i32 8
  %75 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %75, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse)
  br label %cond.end

cond.false:                                       ; preds = %if.end46
  store float 0.000000e+00, float* %ref.tmp51, align 4
  store float 0.000000e+00, float* %ref.tmp52, align 4
  store float 0.000000e+00, float* %ref.tmp53, align 4
  %call54 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %call55 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal148, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49)
  %76 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal56 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %76, i32 0, i32 0
  %77 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool58 = icmp ne %class.btRigidBody* %77, null
  br i1 %tobool58, label %cond.true59, label %cond.false60

cond.true59:                                      ; preds = %cond.end
  %78 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %78, i32 0, i32 9
  %79 = bitcast %class.btVector3* %ref.tmp57 to i8*
  %80 = bitcast %class.btVector3* %m_angularVelocity to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %79, i8* align 4 %80, i32 16, i1 false)
  br label %cond.end65

cond.false60:                                     ; preds = %cond.end
  store float 0.000000e+00, float* %ref.tmp61, align 4
  store float 0.000000e+00, float* %ref.tmp62, align 4
  store float 0.000000e+00, float* %ref.tmp63, align 4
  %call64 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp57, float* nonnull align 4 dereferenceable(4) %ref.tmp61, float* nonnull align 4 dereferenceable(4) %ref.tmp62, float* nonnull align 4 dereferenceable(4) %ref.tmp63)
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false60, %cond.true59
  %call66 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp57)
  %add67 = fadd float %call55, %call66
  store float %add67, float* %vel1Dotn, align 4
  %81 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal268 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %81, i32 0, i32 3
  %82 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool70 = icmp ne %class.btRigidBody* %82, null
  br i1 %tobool70, label %cond.true71, label %cond.false74

cond.true71:                                      ; preds = %cond.end65
  %83 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_linearVelocity72 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %83, i32 0, i32 8
  %84 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_externalForceImpulse73 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %84, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity72, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse73)
  br label %cond.end79

cond.false74:                                     ; preds = %cond.end65
  store float 0.000000e+00, float* %ref.tmp75, align 4
  store float 0.000000e+00, float* %ref.tmp76, align 4
  store float 0.000000e+00, float* %ref.tmp77, align 4
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp69, float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  br label %cond.end79

cond.end79:                                       ; preds = %cond.false74, %cond.true71
  %call80 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal268, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp69)
  %85 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal81 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %85, i32 0, i32 2
  %86 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool83 = icmp ne %class.btRigidBody* %86, null
  br i1 %tobool83, label %cond.true84, label %cond.false86

cond.true84:                                      ; preds = %cond.end79
  %87 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_angularVelocity85 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %87, i32 0, i32 9
  %88 = bitcast %class.btVector3* %ref.tmp82 to i8*
  %89 = bitcast %class.btVector3* %m_angularVelocity85 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %88, i8* align 4 %89, i32 16, i1 false)
  br label %cond.end91

cond.false86:                                     ; preds = %cond.end79
  store float 0.000000e+00, float* %ref.tmp87, align 4
  store float 0.000000e+00, float* %ref.tmp88, align 4
  store float 0.000000e+00, float* %ref.tmp89, align 4
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp82, float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  br label %cond.end91

cond.end91:                                       ; preds = %cond.false86, %cond.true84
  %call92 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal81, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp82)
  %add93 = fadd float %call80, %call92
  store float %add93, float* %vel2Dotn, align 4
  %90 = load float, float* %vel1Dotn, align 4
  %91 = load float, float* %vel2Dotn, align 4
  %add94 = fadd float %90, %91
  store float %add94, float* %rel_vel, align 4
  %92 = load float, float* %desiredVelocity.addr, align 4
  %93 = load float, float* %rel_vel, align 4
  %sub = fsub float %92, %93
  store float %sub, float* %velocityError, align 4
  %94 = load float, float* %velocityError, align 4
  %95 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv95 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %95, i32 0, i32 9
  %96 = load float, float* %m_jacDiagABInv95, align 4
  %mul = fmul float %94, %96
  store float %mul, float* %velocityImpulse, align 4
  %97 = load float, float* %velocityImpulse, align 4
  %98 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %98, i32 0, i32 10
  store float %97, float* %m_rhs, align 4
  %99 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %99, i32 0, i32 14
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  %100 = load float, float* %cfmSlip.addr, align 4
  %101 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %101, i32 0, i32 11
  store float %100, float* %m_cfm, align 4
  %102 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction96 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %102, i32 0, i32 8
  %103 = load float, float* %m_friction96, align 4
  %fneg = fneg float %103
  %104 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %104, i32 0, i32 12
  store float %fneg, float* %m_lowerLimit, align 4
  %105 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction97 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %105, i32 0, i32 8
  %106 = load float, float* %m_friction97, align 4
  %107 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %107, i32 0, i32 13
  store float %106, float* %m_upperLimit, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %0, i32 %1
  ret %struct.btSolverBody* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, i32 %solverBodyIdA, i32 %solverBodyIdB, i32 %frictionIndex, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %frictionIndex.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %solverConstraint = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store i32 %frictionIndex, i32* %frictionIndex.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %call = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  store %struct.btSolverConstraint* %call, %struct.btSolverConstraint** %solverConstraint, align 4
  %0 = load i32, i32* %frictionIndex.addr, align 4
  %1 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %1, i32 0, i32 17
  store i32 %0, i32* %m_frictionIndex, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %3 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %4 = load i32, i32* %solverBodyIdA.addr, align 4
  %5 = load i32, i32* %solverBodyIdB.addr, align 4
  %6 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0.addr, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1.addr, align 4
  %11 = load float, float* %relaxation.addr, align 4
  %12 = load float, float* %desiredVelocity.addr, align 4
  %13 = load float, float* %cfmSlip.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver23setupFrictionConstraintER18btSolverConstraintRK9btVector3iiR15btManifoldPointS4_S4_P17btCollisionObjectS8_fff(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 %4, i32 %5, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btCollisionObject* %9, %class.btCollisionObject* %10, float %11, float %12, float %13)
  %14 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  ret %struct.btSolverConstraint* %14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %2, i32 %3
  ret %struct.btSolverConstraint* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver32setupTorsionalFrictionConstraintER18btSolverConstraintRK9btVector3iiR15btManifoldPointfS4_S4_P17btCollisionObjectS8_fff(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %solverConstraint, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis1, i32 %solverBodyIdA, i32 %solverBodyIdB, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, float %combinedTorsionalFriction, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btSolverConstraint*, align 4
  %normalAxis1.addr = alloca %class.btVector3*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %combinedTorsionalFriction.addr = alloca float, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %normalAxis = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %body0 = alloca %class.btRigidBody*, align 4
  %body1 = alloca %class.btRigidBody*, align 4
  %ftorqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ftorqueAxis121 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %iMJaA = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %iMJaB = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %sum = alloca float, align 4
  %rel_vel = alloca float, align 4
  %vel1Dotn = alloca float, align 4
  %ref.tmp60 = alloca %class.btVector3, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %vel2Dotn = alloca float, align 4
  %ref.tmp83 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp90 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %velocityError = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverConstraint* %solverConstraint, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  store %class.btVector3* %normalAxis1, %class.btVector3** %normalAxis1.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store float %combinedTorsionalFriction, float* %combinedTorsionalFriction.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %2 = bitcast %class.btVector3* %normalAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis)
  %3 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %3, i32 0, i32 3
  %4 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %6 = load i32, i32* %solverBodyIdA.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %6)
  store %struct.btSolverBody* %call5, %struct.btSolverBody** %solverBodyA, align 4
  %m_tmpSolverBodyPool6 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %7 = load i32, i32* %solverBodyIdB.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool6, i32 %7)
  store %struct.btSolverBody* %call7, %struct.btSolverBody** %solverBodyB, align 4
  %m_tmpSolverBodyPool8 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %8 = load i32, i32* %solverBodyIdA.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool8, i32 %8)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call9, i32 0, i32 12
  %9 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %9, %class.btRigidBody** %body0, align 4
  %m_tmpSolverBodyPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %10 = load i32, i32* %solverBodyIdB.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool10, i32 %10)
  %m_originalBody12 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call11, i32 0, i32 12
  %11 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody12, align 4
  store %class.btRigidBody* %11, %class.btRigidBody** %body1, align 4
  %12 = load i32, i32* %solverBodyIdA.addr, align 4
  %13 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %13, i32 0, i32 18
  store i32 %12, i32* %m_solverBodyIdA, align 4
  %14 = load i32, i32* %solverBodyIdB.addr, align 4
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %15, i32 0, i32 19
  store i32 %14, i32* %m_solverBodyIdB, align 4
  %16 = load float, float* %combinedTorsionalFriction.addr, align 4
  %17 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %17, i32 0, i32 8
  store float %16, float* %m_friction, align 4
  %18 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %19 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %18, i32 0, i32 15
  %m_originalContactPoint = bitcast %union.anon.12* %19 to i8**
  store i8* null, i8** %m_originalContactPoint, align 4
  %20 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %20, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %21 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %21, i32 0, i32 6
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  %22 = load %class.btVector3*, %class.btVector3** %normalAxis1.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ftorqueAxis1, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  %23 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %23, i32 0, i32 0
  %24 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %25 = bitcast %class.btVector3* %ftorqueAxis1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false)
  %26 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool = icmp ne %class.btRigidBody* %26, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %27 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call15 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %27)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp14, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis1)
  %28 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %28)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %call16)
  br label %cond.end

cond.false:                                       ; preds = %entry
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %29 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %29, i32 0, i32 4
  %30 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %31 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  %32 = load %class.btVector3*, %class.btVector3** %normalAxis1.addr, align 4
  %33 = bitcast %class.btVector3* %ftorqueAxis121 to i8*
  %34 = bitcast %class.btVector3* %32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  %35 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %35, i32 0, i32 2
  %36 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %37 = bitcast %class.btVector3* %ftorqueAxis121 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  %38 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool23 = icmp ne %class.btRigidBody* %38, null
  br i1 %tobool23, label %cond.true24, label %cond.false28

cond.true24:                                      ; preds = %cond.end
  %39 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %39)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call26, %class.btVector3* nonnull align 4 dereferenceable(16) %ftorqueAxis121)
  %40 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %40)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  br label %cond.end33

cond.false28:                                     ; preds = %cond.end
  store float 0.000000e+00, float* %ref.tmp29, align 4
  store float 0.000000e+00, float* %ref.tmp30, align 4
  store float 0.000000e+00, float* %ref.tmp31, align 4
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  br label %cond.end33

cond.end33:                                       ; preds = %cond.false28, %cond.true24
  %41 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %41, i32 0, i32 5
  %42 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %43 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false)
  %44 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool34 = icmp ne %class.btRigidBody* %44, null
  br i1 %tobool34, label %cond.true35, label %cond.false38

cond.true35:                                      ; preds = %cond.end33
  %45 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %45)
  %46 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal37 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %46, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %iMJaA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call36, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal37)
  br label %cond.end43

cond.false38:                                     ; preds = %cond.end33
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 0.000000e+00, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %iMJaA, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  br label %cond.end43

cond.end43:                                       ; preds = %cond.false38, %cond.true35
  %47 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool44 = icmp ne %class.btRigidBody* %47, null
  br i1 %tobool44, label %cond.true45, label %cond.false48

cond.true45:                                      ; preds = %cond.end43
  %48 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %48)
  %49 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal47 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %49, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %iMJaB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call46, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal47)
  br label %cond.end53

cond.false48:                                     ; preds = %cond.end43
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  store float 0.000000e+00, float* %ref.tmp51, align 4
  %call52 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %iMJaB, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  br label %cond.end53

cond.end53:                                       ; preds = %cond.false48, %cond.true45
  store float 0.000000e+00, float* %sum, align 4
  %50 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal54 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %50, i32 0, i32 0
  %call55 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJaA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal54)
  %51 = load float, float* %sum, align 4
  %add = fadd float %51, %call55
  store float %add, float* %sum, align 4
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal56 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 2
  %call57 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJaB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal56)
  %53 = load float, float* %sum, align 4
  %add58 = fadd float %53, %call57
  store float %add58, float* %sum, align 4
  %54 = load float, float* %sum, align 4
  %div = fdiv float 1.000000e+00, %54
  %55 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %55, i32 0, i32 9
  store float %div, float* %m_jacDiagABInv, align 4
  %56 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal159 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %56, i32 0, i32 1
  %57 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool61 = icmp ne %class.btRigidBody* %57, null
  br i1 %tobool61, label %cond.true62, label %cond.false63

cond.true62:                                      ; preds = %cond.end53
  %58 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %58, i32 0, i32 8
  %59 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %59, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp60, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse)
  br label %cond.end68

cond.false63:                                     ; preds = %cond.end53
  store float 0.000000e+00, float* %ref.tmp64, align 4
  store float 0.000000e+00, float* %ref.tmp65, align 4
  store float 0.000000e+00, float* %ref.tmp66, align 4
  %call67 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66)
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false63, %cond.true62
  %call69 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal159, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp60)
  %60 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal70 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %60, i32 0, i32 0
  %61 = load %class.btRigidBody*, %class.btRigidBody** %body0, align 4
  %tobool72 = icmp ne %class.btRigidBody* %61, null
  br i1 %tobool72, label %cond.true73, label %cond.false74

cond.true73:                                      ; preds = %cond.end68
  %62 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %62, i32 0, i32 9
  %63 = bitcast %class.btVector3* %ref.tmp71 to i8*
  %64 = bitcast %class.btVector3* %m_angularVelocity to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 16, i1 false)
  br label %cond.end79

cond.false74:                                     ; preds = %cond.end68
  store float 0.000000e+00, float* %ref.tmp75, align 4
  store float 0.000000e+00, float* %ref.tmp76, align 4
  store float 0.000000e+00, float* %ref.tmp77, align 4
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  br label %cond.end79

cond.end79:                                       ; preds = %cond.false74, %cond.true73
  %call80 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal70, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp71)
  %add81 = fadd float %call69, %call80
  store float %add81, float* %vel1Dotn, align 4
  %65 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal282 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %65, i32 0, i32 3
  %66 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool84 = icmp ne %class.btRigidBody* %66, null
  br i1 %tobool84, label %cond.true85, label %cond.false88

cond.true85:                                      ; preds = %cond.end79
  %67 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_linearVelocity86 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %67, i32 0, i32 8
  %68 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_externalForceImpulse87 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %68, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp83, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity86, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse87)
  br label %cond.end93

cond.false88:                                     ; preds = %cond.end79
  store float 0.000000e+00, float* %ref.tmp89, align 4
  store float 0.000000e+00, float* %ref.tmp90, align 4
  store float 0.000000e+00, float* %ref.tmp91, align 4
  %call92 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp89, float* nonnull align 4 dereferenceable(4) %ref.tmp90, float* nonnull align 4 dereferenceable(4) %ref.tmp91)
  br label %cond.end93

cond.end93:                                       ; preds = %cond.false88, %cond.true85
  %call94 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal282, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp83)
  %69 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal95 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %69, i32 0, i32 2
  %70 = load %class.btRigidBody*, %class.btRigidBody** %body1, align 4
  %tobool97 = icmp ne %class.btRigidBody* %70, null
  br i1 %tobool97, label %cond.true98, label %cond.false100

cond.true98:                                      ; preds = %cond.end93
  %71 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_angularVelocity99 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %71, i32 0, i32 9
  %72 = bitcast %class.btVector3* %ref.tmp96 to i8*
  %73 = bitcast %class.btVector3* %m_angularVelocity99 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 16, i1 false)
  br label %cond.end105

cond.false100:                                    ; preds = %cond.end93
  store float 0.000000e+00, float* %ref.tmp101, align 4
  store float 0.000000e+00, float* %ref.tmp102, align 4
  store float 0.000000e+00, float* %ref.tmp103, align 4
  %call104 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp102, float* nonnull align 4 dereferenceable(4) %ref.tmp103)
  br label %cond.end105

cond.end105:                                      ; preds = %cond.false100, %cond.true98
  %call106 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal95, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96)
  %add107 = fadd float %call94, %call106
  store float %add107, float* %vel2Dotn, align 4
  %74 = load float, float* %vel1Dotn, align 4
  %75 = load float, float* %vel2Dotn, align 4
  %add108 = fadd float %74, %75
  store float %add108, float* %rel_vel, align 4
  %76 = load float, float* %desiredVelocity.addr, align 4
  %77 = load float, float* %rel_vel, align 4
  %sub = fsub float %76, %77
  store float %sub, float* %velocityError, align 4
  %78 = load float, float* %velocityError, align 4
  %79 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv109 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %79, i32 0, i32 9
  %80 = load float, float* %m_jacDiagABInv109, align 4
  %mul = fmul float %78, %80
  store float %mul, float* %velocityImpulse, align 4
  %81 = load float, float* %velocityImpulse, align 4
  %82 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %82, i32 0, i32 10
  store float %81, float* %m_rhs, align 4
  %83 = load float, float* %cfmSlip.addr, align 4
  %84 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %84, i32 0, i32 11
  store float %83, float* %m_cfm, align 4
  %85 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction110 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %85, i32 0, i32 8
  %86 = load float, float* %m_friction110, align 4
  %fneg = fneg float %86
  %87 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %87, i32 0, i32 12
  store float %fneg, float* %m_lowerLimit, align 4
  %88 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction111 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %88, i32 0, i32 8
  %89 = load float, float* %m_friction111, align 4
  %90 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %90, i32 0, i32 13
  store float %89, float* %m_upperLimit, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver30addTorsionalFrictionConstraintERK9btVector3iiiR15btManifoldPointfS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis, i32 %solverBodyIdA, i32 %solverBodyIdB, i32 %frictionIndex, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, float %combinedTorsionalFriction, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %colObj0, %class.btCollisionObject* %colObj1, float %relaxation, float %desiredVelocity, float %cfmSlip) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %frictionIndex.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %combinedTorsionalFriction.addr = alloca float, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %colObj0.addr = alloca %class.btCollisionObject*, align 4
  %colObj1.addr = alloca %class.btCollisionObject*, align 4
  %relaxation.addr = alloca float, align 4
  %desiredVelocity.addr = alloca float, align 4
  %cfmSlip.addr = alloca float, align 4
  %solverConstraint = alloca %struct.btSolverConstraint*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store i32 %frictionIndex, i32* %frictionIndex.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store float %combinedTorsionalFriction, float* %combinedTorsionalFriction.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  store %class.btCollisionObject* %colObj0, %class.btCollisionObject** %colObj0.addr, align 4
  store %class.btCollisionObject* %colObj1, %class.btCollisionObject** %colObj1.addr, align 4
  store float %relaxation, float* %relaxation.addr, align 4
  store float %desiredVelocity, float* %desiredVelocity.addr, align 4
  store float %cfmSlip, float* %cfmSlip.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 5
  %call = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool)
  store %struct.btSolverConstraint* %call, %struct.btSolverConstraint** %solverConstraint, align 4
  %0 = load i32, i32* %frictionIndex.addr, align 4
  %1 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %1, i32 0, i32 17
  store i32 %0, i32* %m_frictionIndex, align 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %3 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %4 = load i32, i32* %solverBodyIdA.addr, align 4
  %5 = load i32, i32* %solverBodyIdB.addr, align 4
  %6 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %7 = load float, float* %combinedTorsionalFriction.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0.addr, align 4
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1.addr, align 4
  %12 = load float, float* %relaxation.addr, align 4
  %13 = load float, float* %desiredVelocity.addr, align 4
  %14 = load float, float* %cfmSlip.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver32setupTorsionalFrictionConstraintER18btSolverConstraintRK9btVector3iiR15btManifoldPointfS4_S4_P17btCollisionObjectS8_fff(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 %4, i32 %5, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %6, float %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btCollisionObject* %10, %class.btCollisionObject* %11, float %12, float %13, float %14)
  %15 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  ret %struct.btSolverConstraint* %15
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %body, float %timeStep) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %body.addr = alloca %class.btCollisionObject*, align 4
  %timeStep.addr = alloca float, align 4
  %solverBodyIdA = alloca i32, align 4
  %rb = alloca %class.btRigidBody*, align 4
  %solverBody = alloca %struct.btSolverBody*, align 4
  %ref.tmp = alloca %struct.btSolverBody, align 4
  %fixedBody = alloca %struct.btSolverBody*, align 4
  %ref.tmp19 = alloca %struct.btSolverBody, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject* %body, %class.btCollisionObject** %body.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store i32 -1, i32* %solverBodyIdA, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %0)
  %cmp = icmp sge i32 %call, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body.addr, align 4
  %call2 = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %1)
  store i32 %call2, i32* %solverBodyIdA, align 4
  br label %if.end24

if.else:                                          ; preds = %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body.addr, align 4
  %call3 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %2)
  store %class.btRigidBody* %call3, %class.btRigidBody** %rb, align 4
  %3 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %tobool = icmp ne %class.btRigidBody* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.else12

land.lhs.true:                                    ; preds = %if.else
  %4 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %call4 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %4)
  %tobool5 = fcmp une float %call4, 0.000000e+00
  br i1 %tobool5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %5 = load %class.btRigidBody*, %class.btRigidBody** %rb, align 4
  %6 = bitcast %class.btRigidBody* %5 to %class.btCollisionObject*
  %call6 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %6)
  br i1 %call6, label %if.then7, label %if.else12

if.then7:                                         ; preds = %lor.lhs.false, %land.lhs.true
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  store i32 %call8, i32* %solverBodyIdA, align 4
  %m_tmpSolverBodyPool9 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %7 = bitcast %struct.btSolverBody* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %7, i8 0, i32 244, i1 false)
  %call10 = call %struct.btSolverBody* @_ZN12btSolverBodyC2Ev(%struct.btSolverBody* %ref.tmp)
  %call11 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE6expandERKS0_(%class.btAlignedObjectArray* %m_tmpSolverBodyPool9, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %ref.tmp)
  store %struct.btSolverBody* %call11, %struct.btSolverBody** %solverBody, align 4
  %8 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody, align 4
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %body.addr, align 4
  %10 = load float, float* %timeStep.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* %8, %class.btCollisionObject* %9, float %10)
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %body.addr, align 4
  %12 = load i32, i32* %solverBodyIdA, align 4
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %11, i32 %12)
  br label %if.end23

if.else12:                                        ; preds = %lor.lhs.false, %if.else
  %m_fixedBodyId = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 11
  %13 = load i32, i32* %m_fixedBodyId, align 4
  %cmp13 = icmp slt i32 %13, 0
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.else12
  %m_tmpSolverBodyPool15 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool15)
  %m_fixedBodyId17 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 11
  store i32 %call16, i32* %m_fixedBodyId17, align 4
  %m_tmpSolverBodyPool18 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %14 = bitcast %struct.btSolverBody* %ref.tmp19 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 0, i32 244, i1 false)
  %call20 = call %struct.btSolverBody* @_ZN12btSolverBodyC2Ev(%struct.btSolverBody* %ref.tmp19)
  %call21 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE6expandERKS0_(%class.btAlignedObjectArray* %m_tmpSolverBodyPool18, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %ref.tmp19)
  store %struct.btSolverBody* %call21, %struct.btSolverBody** %fixedBody, align 4
  %15 = load %struct.btSolverBody*, %struct.btSolverBody** %fixedBody, align 4
  %16 = load float, float* %timeStep.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver14initSolverBodyEP12btSolverBodyP17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* %15, %class.btCollisionObject* null, float %16)
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.else12
  %m_fixedBodyId22 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 11
  %17 = load i32, i32* %m_fixedBodyId22, align 4
  store i32 %17, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.then7
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then
  %18 = load i32, i32* %solverBodyIdA, align 4
  store i32 %18, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end24, %if.end
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_companionId, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyE6expandERKS0_(%class.btAlignedObjectArray* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %fillValue) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %fillValue.addr = alloca %struct.btSolverBody*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btSolverBody* %fillValue, %struct.btSolverBody** %fillValue.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI12btSolverBodyE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %2, i32 %3
  %4 = bitcast %struct.btSolverBody* %arrayidx to i8*
  %call5 = call i8* @_ZN12btSolverBodynwEmPv(i32 244, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btSolverBody*
  %6 = load %struct.btSolverBody*, %struct.btSolverBody** %fillValue.addr, align 4
  %call6 = call %struct.btSolverBody* @_ZN12btSolverBodyC2ERKS_(%struct.btSolverBody* %5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %6)
  %m_data7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data7, align 4
  %8 = load i32, i32* %sz, align 4
  %arrayidx8 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %7, i32 %8
  ret %struct.btSolverBody* %arrayidx8
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSolverBody* @_ZN12btSolverBodyC2Ev(%struct.btSolverBody* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_worldTransform)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaAngularVelocity)
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularFactor)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearFactor)
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 5
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invMass)
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_turnVelocity)
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearVelocity)
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularVelocity)
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 10
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_externalForceImpulse)
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 11
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_externalTorqueImpulse)
  ret %struct.btSolverBody* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %this, i32 %id) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %id.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  store i32 %0, i32* %m_companionId, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver22setupContactConstraintER18btSolverConstraintiiR15btManifoldPointRK19btContactSolverInfoRfRK9btVector3SA_(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %solverConstraint, i32 %solverBodyIdA, i32 %solverBodyIdB, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, float* nonnull align 4 dereferenceable(4) %relaxation, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btSolverConstraint*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %relaxation.addr = alloca float*, align 4
  %rel_pos1.addr = alloca %class.btVector3*, align 4
  %rel_pos2.addr = alloca %class.btVector3*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %invTimeStep = alloca float, align 4
  %cfm = alloca float, align 4
  %erp = alloca float, align 4
  %denom = alloca float, align 4
  %torqueAxis0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %torqueAxis1 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  %denom74 = alloca float, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %restitution = alloca float, align 4
  %penetration = alloca float, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %ref.tmp98 = alloca %class.btVector3, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %vel = alloca %class.btVector3, align 4
  %rel_vel = alloca float, align 4
  %ref.tmp129 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca %class.btVector3, align 4
  %ref.tmp140 = alloca %class.btVector3, align 4
  %ref.tmp141 = alloca %class.btVector3, align 4
  %ref.tmp145 = alloca %class.btVector3, align 4
  %externalForceImpulseA = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %externalTorqueImpulseA = alloca %class.btVector3, align 4
  %ref.tmp165 = alloca float, align 4
  %ref.tmp166 = alloca float, align 4
  %ref.tmp167 = alloca float, align 4
  %externalForceImpulseB = alloca %class.btVector3, align 4
  %ref.tmp175 = alloca float, align 4
  %ref.tmp176 = alloca float, align 4
  %ref.tmp177 = alloca float, align 4
  %externalTorqueImpulseB = alloca %class.btVector3, align 4
  %ref.tmp185 = alloca float, align 4
  %ref.tmp186 = alloca float, align 4
  %ref.tmp187 = alloca float, align 4
  %vel1Dotn = alloca float, align 4
  %ref.tmp191 = alloca %class.btVector3, align 4
  %ref.tmp194 = alloca %class.btVector3, align 4
  %vel2Dotn = alloca float, align 4
  %ref.tmp198 = alloca %class.btVector3, align 4
  %ref.tmp202 = alloca %class.btVector3, align 4
  %rel_vel206 = alloca float, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverConstraint* %solverConstraint, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store float* %relaxation, float** %relaxation.addr, align 4
  store %class.btVector3* %rel_pos1, %class.btVector3** %rel_pos1.addr, align 4
  store %class.btVector3* %rel_pos2, %class.btVector3** %rel_pos2.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %0 = load i32, i32* %solverBodyIdA.addr, align 4
  %call = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %0)
  store %struct.btSolverBody* %call, %struct.btSolverBody** %bodyA, align 4
  %m_tmpSolverBodyPool2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %1 = load i32, i32* %solverBodyIdB.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool2, i32 %1)
  store %struct.btSolverBody* %call3, %struct.btSolverBody** %bodyB, align 4
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %2, i32 0, i32 12
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %3, %class.btRigidBody** %rb0, align 4
  %4 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody4 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %4, i32 0, i32 12
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody4, align 4
  store %class.btRigidBody* %5, %class.btRigidBody** %rb1, align 4
  %6 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %7 = bitcast %struct.btContactSolverInfo* %6 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %7, i32 0, i32 7
  %8 = load float, float* %m_sor, align 4
  %9 = load float*, float** %relaxation.addr, align 4
  store float %8, float* %9, align 4
  %10 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %11 = bitcast %struct.btContactSolverInfo* %10 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %11, i32 0, i32 3
  %12 = load float, float* %m_timeStep, align 4
  %div = fdiv float 1.000000e+00, %12
  store float %div, float* %invTimeStep, align 4
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %14 = bitcast %struct.btContactSolverInfo* %13 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 10
  %15 = load float, float* %m_globalCfm, align 4
  store float %15, float* %cfm, align 4
  %16 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %17 = bitcast %struct.btContactSolverInfo* %16 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %17, i32 0, i32 9
  %18 = load float, float* %m_erp2, align 4
  store float %18, float* %erp, align 4
  %19 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %19, i32 0, i32 15
  %20 = load i32, i32* %m_contactPointFlags, align 4
  %and = and i32 %20, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %21 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags5 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %21, i32 0, i32 15
  %22 = load i32, i32* %m_contactPointFlags5, align 4
  %and6 = and i32 %22, 4
  %tobool7 = icmp ne i32 %and6, 0
  br i1 %tobool7, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %23 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags8 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %23, i32 0, i32 15
  %24 = load i32, i32* %m_contactPointFlags8, align 4
  %and9 = and i32 %24, 2
  %tobool10 = icmp ne i32 %and9, 0
  br i1 %tobool10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then
  %25 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %26 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %25, i32 0, i32 21
  %m_contactCFM = bitcast %union.anon.22* %26 to float*
  %27 = load float, float* %m_contactCFM, align 4
  store float %27, float* %cfm, align 4
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then
  %28 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags12 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %28, i32 0, i32 15
  %29 = load i32, i32* %m_contactPointFlags12, align 4
  %and13 = and i32 %29, 4
  %tobool14 = icmp ne i32 %and13, 0
  br i1 %tobool14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  %30 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %31 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %30, i32 0, i32 22
  %m_contactERP = bitcast %union.anon.23* %31 to float*
  %32 = load float, float* %m_contactERP, align 4
  store float %32, float* %erp, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %if.end
  br label %if.end30

if.else:                                          ; preds = %lor.lhs.false
  %33 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_contactPointFlags17 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %33, i32 0, i32 15
  %34 = load i32, i32* %m_contactPointFlags17, align 4
  %and18 = and i32 %34, 8
  %tobool19 = icmp ne i32 %and18, 0
  br i1 %tobool19, label %if.then20, label %if.end29

if.then20:                                        ; preds = %if.else
  %35 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %36 = bitcast %struct.btContactSolverInfo* %35 to %struct.btContactSolverInfoData*
  %m_timeStep21 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %36, i32 0, i32 3
  %37 = load float, float* %m_timeStep21, align 4
  %38 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %39 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %38, i32 0, i32 21
  %m_combinedContactStiffness1 = bitcast %union.anon.22* %39 to float*
  %40 = load float, float* %m_combinedContactStiffness1, align 4
  %mul = fmul float %37, %40
  %41 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %42 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %41, i32 0, i32 22
  %m_combinedContactDamping1 = bitcast %union.anon.23* %42 to float*
  %43 = load float, float* %m_combinedContactDamping1, align 4
  %add = fadd float %mul, %43
  store float %add, float* %denom, align 4
  %44 = load float, float* %denom, align 4
  %cmp = fcmp olt float %44, 0x3E80000000000000
  br i1 %cmp, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then20
  store float 0x3E80000000000000, float* %denom, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.then22, %if.then20
  %45 = load float, float* %denom, align 4
  %div24 = fdiv float 1.000000e+00, %45
  store float %div24, float* %cfm, align 4
  %46 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %47 = bitcast %struct.btContactSolverInfo* %46 to %struct.btContactSolverInfoData*
  %m_timeStep25 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %47, i32 0, i32 3
  %48 = load float, float* %m_timeStep25, align 4
  %49 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %50 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %49, i32 0, i32 21
  %m_combinedContactStiffness126 = bitcast %union.anon.22* %50 to float*
  %51 = load float, float* %m_combinedContactStiffness126, align 4
  %mul27 = fmul float %48, %51
  %52 = load float, float* %denom, align 4
  %div28 = fdiv float %mul27, %52
  store float %div28, float* %erp, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.end23, %if.else
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.end16
  %53 = load float, float* %invTimeStep, align 4
  %54 = load float, float* %cfm, align 4
  %mul31 = fmul float %54, %53
  store float %mul31, float* %cfm, align 4
  %55 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  %56 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %56, i32 0, i32 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis0, %class.btVector3* %55, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB)
  %57 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool32 = icmp ne %class.btRigidBody* %57, null
  br i1 %tobool32, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end30
  %58 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %58)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp33, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis0)
  %59 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %59)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %call35)
  br label %cond.end

cond.false:                                       ; preds = %if.end30
  store float 0.000000e+00, float* %ref.tmp36, align 4
  store float 0.000000e+00, float* %ref.tmp37, align 4
  store float 0.000000e+00, float* %ref.tmp38, align 4
  %call39 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %60 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %60, i32 0, i32 4
  %61 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %62 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 16, i1 false)
  %63 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  %64 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB40 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %64, i32 0, i32 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %torqueAxis1, %class.btVector3* %63, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB40)
  %65 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool42 = icmp ne %class.btRigidBody* %65, null
  br i1 %tobool42, label %cond.true43, label %cond.false48

cond.true43:                                      ; preds = %cond.end
  %66 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call45 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %66)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp44, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call45, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp46)
  %67 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %67)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %call47)
  br label %cond.end53

cond.false48:                                     ; preds = %cond.end
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  store float 0.000000e+00, float* %ref.tmp51, align 4
  %call52 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  br label %cond.end53

cond.end53:                                       ; preds = %cond.false48, %cond.true43
  %68 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %68, i32 0, i32 5
  %69 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %70 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 16, i1 false)
  %call54 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vec)
  store float 0.000000e+00, float* %denom0, align 4
  store float 0.000000e+00, float* %denom1, align 4
  %71 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool55 = icmp ne %class.btRigidBody* %71, null
  br i1 %tobool55, label %if.then56, label %if.end63

if.then56:                                        ; preds = %cond.end53
  %72 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA58 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %72, i32 0, i32 4
  %73 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp57, %class.btVector3* %m_angularComponentA58, %class.btVector3* nonnull align 4 dereferenceable(16) %73)
  %74 = bitcast %class.btVector3* %vec to i8*
  %75 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false)
  %76 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call59 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %76)
  %77 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB60 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %77, i32 0, i32 4
  %call61 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normalWorldOnB60, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add62 = fadd float %call59, %call61
  store float %add62, float* %denom0, align 4
  br label %if.end63

if.end63:                                         ; preds = %if.then56, %cond.end53
  %78 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool64 = icmp ne %class.btRigidBody* %78, null
  br i1 %tobool64, label %if.then65, label %if.end73

if.then65:                                        ; preds = %if.end63
  %79 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB68 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %79, i32 0, i32 5
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB68)
  %80 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %80)
  %81 = bitcast %class.btVector3* %vec to i8*
  %82 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false)
  %83 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call69 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %83)
  %84 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB70 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %84, i32 0, i32 4
  %call71 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normalWorldOnB70, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add72 = fadd float %call69, %call71
  store float %add72, float* %denom1, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.then65, %if.end63
  %85 = load float*, float** %relaxation.addr, align 4
  %86 = load float, float* %85, align 4
  %87 = load float, float* %denom0, align 4
  %88 = load float, float* %denom1, align 4
  %add75 = fadd float %87, %88
  %89 = load float, float* %cfm, align 4
  %add76 = fadd float %add75, %89
  %div77 = fdiv float %86, %add76
  store float %div77, float* %denom74, align 4
  %90 = load float, float* %denom74, align 4
  %91 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %91, i32 0, i32 9
  store float %90, float* %m_jacDiagABInv, align 4
  %92 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool78 = icmp ne %class.btRigidBody* %92, null
  br i1 %tobool78, label %if.then79, label %if.else81

if.then79:                                        ; preds = %if.end73
  %93 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB80 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %93, i32 0, i32 4
  %94 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %94, i32 0, i32 1
  %95 = bitcast %class.btVector3* %m_contactNormal1 to i8*
  %96 = bitcast %class.btVector3* %m_normalWorldOnB80 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 16, i1 false)
  %97 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %97, i32 0, i32 0
  %98 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %99 = bitcast %class.btVector3* %torqueAxis0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %98, i8* align 4 %99, i32 16, i1 false)
  br label %if.end84

if.else81:                                        ; preds = %if.end73
  %100 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal182 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %100, i32 0, i32 1
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal182)
  %101 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal83 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %101, i32 0, i32 0
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos1CrossNormal83)
  br label %if.end84

if.end84:                                         ; preds = %if.else81, %if.then79
  %102 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool85 = icmp ne %class.btRigidBody* %102, null
  br i1 %tobool85, label %if.then86, label %if.else90

if.then86:                                        ; preds = %if.end84
  %103 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB88 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %103, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp87, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB88)
  %104 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %104, i32 0, i32 3
  %105 = bitcast %class.btVector3* %m_contactNormal2 to i8*
  %106 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %106, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* nonnull align 4 dereferenceable(16) %torqueAxis1)
  %107 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %107, i32 0, i32 2
  %108 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %109 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 16, i1 false)
  br label %if.end93

if.else90:                                        ; preds = %if.end84
  %110 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal291 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %110, i32 0, i32 3
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal291)
  %111 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal92 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %111, i32 0, i32 2
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos2CrossNormal92)
  br label %if.end93

if.end93:                                         ; preds = %if.else90, %if.then86
  store float 0.000000e+00, float* %restitution, align 4
  %112 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %call94 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %112)
  %113 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %114 = bitcast %struct.btContactSolverInfo* %113 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %114, i32 0, i32 14
  %115 = load float, float* %m_linearSlop, align 4
  %add95 = fadd float %call94, %115
  store float %add95, float* %penetration, align 4
  %call96 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %call97 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %116 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool99 = icmp ne %class.btRigidBody* %116, null
  br i1 %tobool99, label %cond.true100, label %cond.false101

cond.true100:                                     ; preds = %if.end93
  %117 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %118 = load %class.btVector3*, %class.btVector3** %rel_pos1.addr, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp98, %class.btRigidBody* %117, %class.btVector3* nonnull align 4 dereferenceable(16) %118)
  br label %cond.end106

cond.false101:                                    ; preds = %if.end93
  store float 0.000000e+00, float* %ref.tmp102, align 4
  store float 0.000000e+00, float* %ref.tmp103, align 4
  store float 0.000000e+00, float* %ref.tmp104, align 4
  %call105 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp98, float* nonnull align 4 dereferenceable(4) %ref.tmp102, float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104)
  br label %cond.end106

cond.end106:                                      ; preds = %cond.false101, %cond.true100
  %119 = bitcast %class.btVector3* %vel1 to i8*
  %120 = bitcast %class.btVector3* %ref.tmp98 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %119, i8* align 4 %120, i32 16, i1 false)
  %121 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool108 = icmp ne %class.btRigidBody* %121, null
  br i1 %tobool108, label %cond.true109, label %cond.false110

cond.true109:                                     ; preds = %cond.end106
  %122 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %123 = load %class.btVector3*, %class.btVector3** %rel_pos2.addr, align 4
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp107, %class.btRigidBody* %122, %class.btVector3* nonnull align 4 dereferenceable(16) %123)
  br label %cond.end115

cond.false110:                                    ; preds = %cond.end106
  store float 0.000000e+00, float* %ref.tmp111, align 4
  store float 0.000000e+00, float* %ref.tmp112, align 4
  store float 0.000000e+00, float* %ref.tmp113, align 4
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  br label %cond.end115

cond.end115:                                      ; preds = %cond.false110, %cond.true109
  %124 = bitcast %class.btVector3* %vel2 to i8*
  %125 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %124, i8* align 4 %125, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %126 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB116 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %126, i32 0, i32 4
  %call117 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normalWorldOnB116, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call117, float* %rel_vel, align 4
  %127 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %127, i32 0, i32 6
  %128 = load float, float* %m_combinedFriction, align 4
  %129 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_friction = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %129, i32 0, i32 8
  store float %128, float* %m_friction, align 4
  %130 = load float, float* %rel_vel, align 4
  %131 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_combinedRestitution = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %131, i32 0, i32 9
  %132 = load float, float* %m_combinedRestitution, align 4
  %call118 = call float @_ZN35btSequentialImpulseConstraintSolver16restitutionCurveEff(%class.btSequentialImpulseConstraintSolver* %this1, float %130, float %132)
  store float %call118, float* %restitution, align 4
  %133 = load float, float* %restitution, align 4
  %cmp119 = fcmp ole float %133, 0.000000e+00
  br i1 %cmp119, label %if.then120, label %if.end121

if.then120:                                       ; preds = %cond.end115
  store float 0.000000e+00, float* %restitution, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then120, %cond.end115
  %134 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %135 = bitcast %struct.btContactSolverInfo* %134 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %135, i32 0, i32 16
  %136 = load i32, i32* %m_solverMode, align 4
  %and122 = and i32 %136, 4
  %tobool123 = icmp ne i32 %and122, 0
  br i1 %tobool123, label %if.then124, label %if.else149

if.then124:                                       ; preds = %if.end121
  %137 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %137, i32 0, i32 16
  %138 = load float, float* %m_appliedImpulse, align 4
  %139 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %140 = bitcast %struct.btContactSolverInfo* %139 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %140, i32 0, i32 15
  %141 = load float, float* %m_warmstartingFactor, align 4
  %mul125 = fmul float %138, %141
  %142 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse126 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %142, i32 0, i32 7
  store float %mul125, float* %m_appliedImpulse126, align 4
  %143 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool127 = icmp ne %class.btRigidBody* %143, null
  br i1 %tobool127, label %if.then128, label %if.end136

if.then128:                                       ; preds = %if.then124
  %144 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %145 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1131 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %145, i32 0, i32 1
  %146 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %146)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1131, %class.btVector3* nonnull align 4 dereferenceable(16) %call132)
  %147 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call133 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %147)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp129, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %call133)
  %148 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentA134 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %148, i32 0, i32 4
  %149 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse135 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %149, i32 0, i32 7
  %150 = load float, float* %m_appliedImpulse135, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %144, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp129, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA134, float %150)
  br label %if.end136

if.end136:                                        ; preds = %if.then128, %if.then124
  %151 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool137 = icmp ne %class.btRigidBody* %151, null
  br i1 %tobool137, label %if.then138, label %if.end148

if.then138:                                       ; preds = %if.end136
  %152 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %153 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2142 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %153, i32 0, i32 3
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp141, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2142)
  %154 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %call143 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btSolverBody18internalGetInvMassEv(%struct.btSolverBody* %154)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp140, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp141, %class.btVector3* nonnull align 4 dereferenceable(16) %call143)
  %155 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call144 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %155)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp139, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp140, %class.btVector3* nonnull align 4 dereferenceable(16) %call144)
  %156 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_angularComponentB146 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %156, i32 0, i32 5
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp145, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB146)
  %157 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse147 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %157, i32 0, i32 7
  %158 = load float, float* %m_appliedImpulse147, align 4
  %fneg = fneg float %158
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %152, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp139, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp145, float %fneg)
  br label %if.end148

if.end148:                                        ; preds = %if.then138, %if.end136
  br label %if.end151

if.else149:                                       ; preds = %if.end121
  %159 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedImpulse150 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %159, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse150, align 4
  br label %if.end151

if.end151:                                        ; preds = %if.else149, %if.end148
  %160 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %160, i32 0, i32 6
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  %161 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody152 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %161, i32 0, i32 12
  %162 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody152, align 4
  %tobool153 = icmp ne %class.btRigidBody* %162, null
  br i1 %tobool153, label %cond.true154, label %cond.false155

cond.true154:                                     ; preds = %if.end151
  %163 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %163, i32 0, i32 10
  %164 = bitcast %class.btVector3* %externalForceImpulseA to i8*
  %165 = bitcast %class.btVector3* %m_externalForceImpulse to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %164, i8* align 4 %165, i32 16, i1 false)
  br label %cond.end160

cond.false155:                                    ; preds = %if.end151
  store float 0.000000e+00, float* %ref.tmp156, align 4
  store float 0.000000e+00, float* %ref.tmp157, align 4
  store float 0.000000e+00, float* %ref.tmp158, align 4
  %call159 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalForceImpulseA, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157, float* nonnull align 4 dereferenceable(4) %ref.tmp158)
  br label %cond.end160

cond.end160:                                      ; preds = %cond.false155, %cond.true154
  %166 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody161 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %166, i32 0, i32 12
  %167 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody161, align 4
  %tobool162 = icmp ne %class.btRigidBody* %167, null
  br i1 %tobool162, label %cond.true163, label %cond.false164

cond.true163:                                     ; preds = %cond.end160
  %168 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %168, i32 0, i32 11
  %169 = bitcast %class.btVector3* %externalTorqueImpulseA to i8*
  %170 = bitcast %class.btVector3* %m_externalTorqueImpulse to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %169, i8* align 4 %170, i32 16, i1 false)
  br label %cond.end169

cond.false164:                                    ; preds = %cond.end160
  store float 0.000000e+00, float* %ref.tmp165, align 4
  store float 0.000000e+00, float* %ref.tmp166, align 4
  store float 0.000000e+00, float* %ref.tmp167, align 4
  %call168 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalTorqueImpulseA, float* nonnull align 4 dereferenceable(4) %ref.tmp165, float* nonnull align 4 dereferenceable(4) %ref.tmp166, float* nonnull align 4 dereferenceable(4) %ref.tmp167)
  br label %cond.end169

cond.end169:                                      ; preds = %cond.false164, %cond.true163
  %171 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody170 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %171, i32 0, i32 12
  %172 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody170, align 4
  %tobool171 = icmp ne %class.btRigidBody* %172, null
  br i1 %tobool171, label %cond.true172, label %cond.false174

cond.true172:                                     ; preds = %cond.end169
  %173 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_externalForceImpulse173 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %173, i32 0, i32 10
  %174 = bitcast %class.btVector3* %externalForceImpulseB to i8*
  %175 = bitcast %class.btVector3* %m_externalForceImpulse173 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %174, i8* align 4 %175, i32 16, i1 false)
  br label %cond.end179

cond.false174:                                    ; preds = %cond.end169
  store float 0.000000e+00, float* %ref.tmp175, align 4
  store float 0.000000e+00, float* %ref.tmp176, align 4
  store float 0.000000e+00, float* %ref.tmp177, align 4
  %call178 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalForceImpulseB, float* nonnull align 4 dereferenceable(4) %ref.tmp175, float* nonnull align 4 dereferenceable(4) %ref.tmp176, float* nonnull align 4 dereferenceable(4) %ref.tmp177)
  br label %cond.end179

cond.end179:                                      ; preds = %cond.false174, %cond.true172
  %176 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody180 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %176, i32 0, i32 12
  %177 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody180, align 4
  %tobool181 = icmp ne %class.btRigidBody* %177, null
  br i1 %tobool181, label %cond.true182, label %cond.false184

cond.true182:                                     ; preds = %cond.end179
  %178 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_externalTorqueImpulse183 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %178, i32 0, i32 11
  %179 = bitcast %class.btVector3* %externalTorqueImpulseB to i8*
  %180 = bitcast %class.btVector3* %m_externalTorqueImpulse183 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %179, i8* align 4 %180, i32 16, i1 false)
  br label %cond.end189

cond.false184:                                    ; preds = %cond.end179
  store float 0.000000e+00, float* %ref.tmp185, align 4
  store float 0.000000e+00, float* %ref.tmp186, align 4
  store float 0.000000e+00, float* %ref.tmp187, align 4
  %call188 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalTorqueImpulseB, float* nonnull align 4 dereferenceable(4) %ref.tmp185, float* nonnull align 4 dereferenceable(4) %ref.tmp186, float* nonnull align 4 dereferenceable(4) %ref.tmp187)
  br label %cond.end189

cond.end189:                                      ; preds = %cond.false184, %cond.true182
  %181 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal1190 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %181, i32 0, i32 1
  %182 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %182, i32 0, i32 8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp191, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %externalForceImpulseA)
  %call192 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1190, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp191)
  %183 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos1CrossNormal193 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %183, i32 0, i32 0
  %184 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %184, i32 0, i32 9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp194, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %externalTorqueImpulseA)
  %call195 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal193, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp194)
  %add196 = fadd float %call192, %call195
  store float %add196, float* %vel1Dotn, align 4
  %185 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_contactNormal2197 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %185, i32 0, i32 3
  %186 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_linearVelocity199 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %186, i32 0, i32 8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp198, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity199, %class.btVector3* nonnull align 4 dereferenceable(16) %externalForceImpulseB)
  %call200 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp198)
  %187 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_relpos2CrossNormal201 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %187, i32 0, i32 2
  %188 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_angularVelocity203 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %188, i32 0, i32 9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp202, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity203, %class.btVector3* nonnull align 4 dereferenceable(16) %externalTorqueImpulseB)
  %call204 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal201, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp202)
  %add205 = fadd float %call200, %call204
  store float %add205, float* %vel2Dotn, align 4
  %189 = load float, float* %vel1Dotn, align 4
  %190 = load float, float* %vel2Dotn, align 4
  %add207 = fadd float %189, %190
  store float %add207, float* %rel_vel206, align 4
  store float 0.000000e+00, float* %positionalError, align 4
  %191 = load float, float* %restitution, align 4
  %192 = load float, float* %rel_vel206, align 4
  %sub = fsub float %191, %192
  store float %sub, float* %velocityError, align 4
  %193 = load float, float* %penetration, align 4
  %cmp208 = fcmp ogt float %193, 0.000000e+00
  br i1 %cmp208, label %if.then209, label %if.else212

if.then209:                                       ; preds = %cond.end189
  store float 0.000000e+00, float* %positionalError, align 4
  %194 = load float, float* %penetration, align 4
  %195 = load float, float* %invTimeStep, align 4
  %mul210 = fmul float %194, %195
  %196 = load float, float* %velocityError, align 4
  %sub211 = fsub float %196, %mul210
  store float %sub211, float* %velocityError, align 4
  br label %if.end216

if.else212:                                       ; preds = %cond.end189
  %197 = load float, float* %penetration, align 4
  %fneg213 = fneg float %197
  %198 = load float, float* %erp, align 4
  %mul214 = fmul float %fneg213, %198
  %199 = load float, float* %invTimeStep, align 4
  %mul215 = fmul float %mul214, %199
  store float %mul215, float* %positionalError, align 4
  br label %if.end216

if.end216:                                        ; preds = %if.else212, %if.then209
  %200 = load float, float* %positionalError, align 4
  %201 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv217 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %201, i32 0, i32 9
  %202 = load float, float* %m_jacDiagABInv217, align 4
  %mul218 = fmul float %200, %202
  store float %mul218, float* %penetrationImpulse, align 4
  %203 = load float, float* %velocityError, align 4
  %204 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv219 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %204, i32 0, i32 9
  %205 = load float, float* %m_jacDiagABInv219, align 4
  %mul220 = fmul float %203, %205
  store float %mul220, float* %velocityImpulse, align 4
  %206 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %207 = bitcast %struct.btContactSolverInfo* %206 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %207, i32 0, i32 11
  %208 = load i32, i32* %m_splitImpulse, align 4
  %tobool221 = icmp ne i32 %208, 0
  br i1 %tobool221, label %lor.lhs.false222, label %if.then224

lor.lhs.false222:                                 ; preds = %if.end216
  %209 = load float, float* %penetration, align 4
  %210 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %211 = bitcast %struct.btContactSolverInfo* %210 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %211, i32 0, i32 12
  %212 = load float, float* %m_splitImpulsePenetrationThreshold, align 4
  %cmp223 = fcmp ogt float %209, %212
  br i1 %cmp223, label %if.then224, label %if.else226

if.then224:                                       ; preds = %lor.lhs.false222, %if.end216
  %213 = load float, float* %penetrationImpulse, align 4
  %214 = load float, float* %velocityImpulse, align 4
  %add225 = fadd float %213, %214
  %215 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %215, i32 0, i32 10
  store float %add225, float* %m_rhs, align 4
  %216 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %216, i32 0, i32 14
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  br label %if.end229

if.else226:                                       ; preds = %lor.lhs.false222
  %217 = load float, float* %velocityImpulse, align 4
  %218 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhs227 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %218, i32 0, i32 10
  store float %217, float* %m_rhs227, align 4
  %219 = load float, float* %penetrationImpulse, align 4
  %220 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_rhsPenetration228 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %220, i32 0, i32 14
  store float %219, float* %m_rhsPenetration228, align 4
  br label %if.end229

if.end229:                                        ; preds = %if.else226, %if.then224
  %221 = load float, float* %cfm, align 4
  %222 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_jacDiagABInv230 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %222, i32 0, i32 9
  %223 = load float, float* %m_jacDiagABInv230, align 4
  %mul231 = fmul float %221, %223
  %224 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %224, i32 0, i32 11
  store float %mul231, float* %m_cfm, align 4
  %225 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %225, i32 0, i32 12
  store float 0.000000e+00, float* %m_lowerLimit, align 4
  %226 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %226, i32 0, i32 13
  store float 1.000000e+10, float* %m_upperLimit, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 5
  %0 = load float, float* %m_distance1, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %linearComponent, %class.btVector3* nonnull align 4 dereferenceable(16) %angularComponent, float %impulseMagnitude) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %linearComponent.addr = alloca %class.btVector3*, align 4
  %angularComponent.addr = alloca %class.btVector3*, align 4
  %impulseMagnitude.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %linearComponent, %class.btVector3** %linearComponent.addr, align 4
  store %class.btVector3* %angularComponent, %class.btVector3** %angularComponent.addr, align 4
  store float %impulseMagnitude, float* %impulseMagnitude.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %linearComponent.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr)
  %m_linearFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaLinearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = load %class.btVector3*, %class.btVector3** %angularComponent.addr, align 4
  %m_angularFactor = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 3
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, float* nonnull align 4 dereferenceable(4) %impulseMagnitude.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_deltaAngularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver28setFrictionConstraintImpulseER18btSolverConstraintiiR15btManifoldPointRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %solverConstraint, i32 %solverBodyIdA, i32 %solverBodyIdB, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %solverConstraint.addr = alloca %struct.btSolverConstraint*, align 4
  %solverBodyIdA.addr = alloca i32, align 4
  %solverBodyIdB.addr = alloca i32, align 4
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %rb0 = alloca %class.btRigidBody*, align 4
  %rb1 = alloca %class.btRigidBody*, align 4
  %frictionConstraint1 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %frictionConstraint2 = alloca %struct.btSolverConstraint*, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca float, align 4
  %ref.tmp56 = alloca %class.btVector3, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %struct.btSolverConstraint* %solverConstraint, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  store i32 %solverBodyIdA, i32* %solverBodyIdA.addr, align 4
  store i32 %solverBodyIdB, i32* %solverBodyIdB.addr, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %0 = load i32, i32* %solverBodyIdA.addr, align 4
  %call = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %0)
  store %struct.btSolverBody* %call, %struct.btSolverBody** %bodyA, align 4
  %m_tmpSolverBodyPool2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %1 = load i32, i32* %solverBodyIdB.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool2, i32 %1)
  store %struct.btSolverBody* %call3, %struct.btSolverBody** %bodyB, align 4
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %2, i32 0, i32 12
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %3, %class.btRigidBody** %rb0, align 4
  %4 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %m_originalBody4 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %4, i32 0, i32 12
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody4, align 4
  store %class.btRigidBody* %5, %class.btRigidBody** %rb1, align 4
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %6, i32 0, i32 17
  %7 = load i32, i32* %m_frictionIndex, align 4
  %call5 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool, i32 %7)
  store %struct.btSolverConstraint* %call5, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %8 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %9 = bitcast %struct.btContactSolverInfo* %8 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 16
  %10 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %10, 4
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %11 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %11, i32 0, i32 17
  %12 = load float, float* %m_appliedImpulseLateral1, align 4
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %14 = bitcast %struct.btContactSolverInfo* %13 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 15
  %15 = load float, float* %m_warmstartingFactor, align 4
  %mul = fmul float %12, %15
  %16 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %16, i32 0, i32 7
  store float %mul, float* %m_appliedImpulse, align 4
  %17 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool6 = icmp ne %class.btRigidBody* %17, null
  br i1 %tobool6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %19 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %19, i32 0, i32 1
  %20 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call10 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %20)
  store float %call10, float* %ref.tmp9, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %21 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %21)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %call11)
  %22 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %22, i32 0, i32 4
  %23 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_appliedImpulse12 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %23, i32 0, i32 7
  %24 = load float, float* %m_appliedImpulse12, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA, float %24)
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %25 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool13 = icmp ne %class.btRigidBody* %25, null
  br i1 %tobool13, label %if.then14, label %if.end23

if.then14:                                        ; preds = %if.end
  %26 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %27 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %27, i32 0, i32 3
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2)
  %28 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call19 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %28)
  store float %call19, float* %ref.tmp18, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %29 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %29)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %call20)
  %30 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %30, i32 0, i32 5
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB)
  %31 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_appliedImpulse22 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %31, i32 0, i32 7
  %32 = load float, float* %m_appliedImpulse22, align 4
  %fneg = fneg float %32
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, float %fneg)
  br label %if.end23

if.end23:                                         ; preds = %if.then14, %if.end
  br label %if.end25

if.else:                                          ; preds = %entry
  %33 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint1, align 4
  %m_appliedImpulse24 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %33, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse24, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.end23
  %34 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %35 = bitcast %struct.btContactSolverInfo* %34 to %struct.btContactSolverInfoData*
  %m_solverMode26 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %35, i32 0, i32 16
  %36 = load i32, i32* %m_solverMode26, align 4
  %and27 = and i32 %36, 16
  %tobool28 = icmp ne i32 %and27, 0
  br i1 %tobool28, label %if.then29, label %if.end64

if.then29:                                        ; preds = %if.end25
  %m_tmpSolverContactFrictionConstraintPool30 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %37 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint.addr, align 4
  %m_frictionIndex31 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %37, i32 0, i32 17
  %38 = load i32, i32* %m_frictionIndex31, align 4
  %add = add nsw i32 %38, 1
  %call32 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool30, i32 %add)
  store %struct.btSolverConstraint* %call32, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %39 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %40 = bitcast %struct.btContactSolverInfo* %39 to %struct.btContactSolverInfoData*
  %m_solverMode33 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %40, i32 0, i32 16
  %41 = load i32, i32* %m_solverMode33, align 4
  %and34 = and i32 %41, 4
  %tobool35 = icmp ne i32 %and34, 0
  br i1 %tobool35, label %if.then36, label %if.else61

if.then36:                                        ; preds = %if.then29
  %42 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %42, i32 0, i32 18
  %43 = load float, float* %m_appliedImpulseLateral2, align 4
  %44 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %45 = bitcast %struct.btContactSolverInfo* %44 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor37 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %45, i32 0, i32 15
  %46 = load float, float* %m_warmstartingFactor37, align 4
  %mul38 = fmul float %43, %46
  %47 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_appliedImpulse39 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %47, i32 0, i32 7
  store float %mul38, float* %m_appliedImpulse39, align 4
  %48 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %tobool40 = icmp ne %class.btRigidBody* %48, null
  br i1 %tobool40, label %if.then41, label %if.end48

if.then41:                                        ; preds = %if.then36
  %49 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %50 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_contactNormal143 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %50, i32 0, i32 1
  %51 = load %class.btRigidBody*, %class.btRigidBody** %rb0, align 4
  %call45 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %51)
  store float %call45, float* %ref.tmp44, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal143, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_angularComponentA46 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 4
  %53 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_appliedImpulse47 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %53, i32 0, i32 7
  %54 = load float, float* %m_appliedImpulse47, align 4
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentA46, float %54)
  br label %if.end48

if.end48:                                         ; preds = %if.then41, %if.then36
  %55 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %tobool49 = icmp ne %class.btRigidBody* %55, null
  br i1 %tobool49, label %if.then50, label %if.end60

if.then50:                                        ; preds = %if.end48
  %56 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %57 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_contactNormal253 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %57, i32 0, i32 3
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp52, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal253)
  %58 = load %class.btRigidBody*, %class.btRigidBody** %rb1, align 4
  %call55 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %58)
  store float %call55, float* %ref.tmp54, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp54)
  %59 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_angularComponentB57 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %59, i32 0, i32 5
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp56, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularComponentB57)
  %60 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_appliedImpulse58 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %60, i32 0, i32 7
  %61 = load float, float* %m_appliedImpulse58, align 4
  %fneg59 = fneg float %61
  call void @_ZN12btSolverBody20internalApplyImpulseERK9btVector3S2_f(%struct.btSolverBody* %56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp56, float %fneg59)
  br label %if.end60

if.end60:                                         ; preds = %if.then50, %if.end48
  br label %if.end63

if.else61:                                        ; preds = %if.then29
  %62 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %frictionConstraint2, align 4
  %m_appliedImpulse62 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %62, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse62, align 4
  br label %if.end63

if.end63:                                         ; preds = %if.else61, %if.end60
  br label %if.end64

if.end64:                                         ; preds = %if.end63, %if.end25
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %0, i32 %1
  ret %struct.btSolverConstraint* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this, %class.btPersistentManifold* %manifold, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  %solverBodyA = alloca %struct.btSolverBody*, align 4
  %solverBodyB = alloca %struct.btSolverBody*, align 4
  %rollingFriction = alloca i32, align 4
  %j = alloca i32, align 4
  %cp = alloca %class.btManifoldPoint*, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %relaxation = alloca float, align 4
  %frictionIndex = alloca i32, align 4
  %solverConstraint = alloca %struct.btSolverConstraint*, align 4
  %pos1 = alloca %class.btVector3*, align 4
  %pos2 = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %rel_vel = alloca float, align 4
  %axis0 = alloca %class.btVector3, align 4
  %axis1 = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  %lat_rel_vel = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj0, align 4
  store %class.btCollisionObject* null, %class.btCollisionObject** %colObj1, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %0)
  store %class.btCollisionObject* %call, %class.btCollisionObject** %colObj0, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call2 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %1)
  store %class.btCollisionObject* %call2, %class.btCollisionObject** %colObj1, align 4
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %3 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %4 = bitcast %struct.btContactSolverInfo* %3 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %4, i32 0, i32 3
  %5 = load float, float* %m_timeStep, align 4
  %call3 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %2, float %5)
  store i32 %call3, i32* %solverBodyIdA, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %8 = bitcast %struct.btContactSolverInfo* %7 to %struct.btContactSolverInfoData*
  %m_timeStep4 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %8, i32 0, i32 3
  %9 = load float, float* %m_timeStep4, align 4
  %call5 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %6, float %9)
  store i32 %call5, i32* %solverBodyIdB, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %10 = load i32, i32* %solverBodyIdA, align 4
  %call6 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %10)
  store %struct.btSolverBody* %call6, %struct.btSolverBody** %solverBodyA, align 4
  %m_tmpSolverBodyPool7 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %11 = load i32, i32* %solverBodyIdB, align 4
  %call8 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool7, i32 %11)
  store %struct.btSolverBody* %call8, %struct.btSolverBody** %solverBodyB, align 4
  %12 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %tobool = icmp ne %struct.btSolverBody* %12, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %13 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  %m_invMass = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %13, i32 0, i32 5
  %call9 = call zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %m_invMass)
  br i1 %call9, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %lor.lhs.false
  %14 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %tobool10 = icmp ne %struct.btSolverBody* %14, null
  br i1 %tobool10, label %lor.lhs.false11, label %if.then

lor.lhs.false11:                                  ; preds = %land.lhs.true
  %15 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  %m_invMass12 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %15, i32 0, i32 5
  %call13 = call zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %m_invMass12)
  br i1 %call13, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false11, %land.lhs.true, %entry
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false11, %lor.lhs.false
  store i32 1, i32* %rollingFriction, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %16 = load i32, i32* %j, align 4
  %17 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call14 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %17)
  %cmp = icmp slt i32 %16, %call14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %19 = load i32, i32* %j, align 4
  %call15 = call nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %18, i32 %19)
  store %class.btManifoldPoint* %call15, %class.btManifoldPoint** %cp, align 4
  %20 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %call16 = call float @_ZNK15btManifoldPoint11getDistanceEv(%class.btManifoldPoint* %20)
  %21 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %call17 = call float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %21)
  %cmp18 = fcmp ole float %call16, %call17
  br i1 %cmp18, label %if.then19, label %if.end138

if.then19:                                        ; preds = %for.body
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos1)
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rel_pos2)
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call22 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call22, i32* %frictionIndex, align 4
  %m_tmpSolverContactConstraintPool23 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call24 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool23)
  store %struct.btSolverConstraint* %call24, %struct.btSolverConstraint** %solverConstraint, align 4
  %22 = load i32, i32* %solverBodyIdA, align 4
  %23 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %23, i32 0, i32 18
  store i32 %22, i32* %m_solverBodyIdA, align 4
  %24 = load i32, i32* %solverBodyIdB, align 4
  %25 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %25, i32 0, i32 19
  store i32 %24, i32* %m_solverBodyIdB, align 4
  %26 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %27 = bitcast %class.btManifoldPoint* %26 to i8*
  %28 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %29 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %28, i32 0, i32 15
  %m_originalContactPoint = bitcast %union.anon.12* %29 to i8**
  store i8* %27, i8** %m_originalContactPoint, align 4
  %30 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %30)
  store %class.btVector3* %call25, %class.btVector3** %pos1, align 4
  %31 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %31)
  store %class.btVector3* %call26, %class.btVector3** %pos2, align 4
  %32 = load %class.btVector3*, %class.btVector3** %pos1, align 4
  %33 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call27 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %33)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call27)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %32, %class.btVector3* nonnull align 4 dereferenceable(16) %call28)
  %34 = bitcast %class.btVector3* %rel_pos1 to i8*
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  %36 = load %class.btVector3*, %class.btVector3** %pos2, align 4
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %37)
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call30)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %36, %class.btVector3* nonnull align 4 dereferenceable(16) %call31)
  %38 = bitcast %class.btVector3* %rel_pos2 to i8*
  %39 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false)
  %call32 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel1)
  %call33 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vel2)
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyA, align 4
  call void @_ZNK12btSolverBody30getVelocityInLocalPointNoDeltaERK9btVector3RS0_(%struct.btSolverBody* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1)
  %41 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBodyB, align 4
  call void @_ZNK12btSolverBody30getVelocityInLocalPointNoDeltaERK9btVector3RS0_(%struct.btSolverBody* %41, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %42 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %42, i32 0, i32 4
  %call34 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normalWorldOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call34, float* %rel_vel, align 4
  %43 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %44 = load i32, i32* %solverBodyIdA, align 4
  %45 = load i32, i32* %solverBodyIdB, align 4
  %46 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %47 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver22setupContactConstraintER18btSolverConstraintiiR15btManifoldPointRK19btContactSolverInfoRfRK9btVector3SA_(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %43, i32 %44, i32 %45, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %46, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %47, float* nonnull align 4 dereferenceable(4) %relaxation, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %call35 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  %48 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %48, i32 0, i32 17
  store i32 %call35, i32* %m_frictionIndex, align 4
  %49 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedRollingFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %49, i32 0, i32 7
  %50 = load float, float* %m_combinedRollingFriction, align 4
  %cmp36 = fcmp ogt float %50, 0.000000e+00
  br i1 %cmp36, label %land.lhs.true37, label %if.end60

land.lhs.true37:                                  ; preds = %if.then19
  %51 = load i32, i32* %rollingFriction, align 4
  %cmp38 = icmp sgt i32 %51, 0
  br i1 %cmp38, label %if.then39, label %if.end60

if.then39:                                        ; preds = %land.lhs.true37
  %52 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB40 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %52, i32 0, i32 4
  %53 = load i32, i32* %solverBodyIdA, align 4
  %54 = load i32, i32* %solverBodyIdB, align 4
  %55 = load i32, i32* %frictionIndex, align 4
  %56 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %57 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedSpinningFriction = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %57, i32 0, i32 8
  %58 = load float, float* %m_combinedSpinningFriction, align 4
  %59 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %60 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %61 = load float, float* %relaxation, align 4
  %call41 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver30addTorsionalFrictionConstraintERK9btVector3iiiR15btManifoldPointfS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB40, i32 %53, i32 %54, i32 %55, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %56, float %58, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %59, %class.btCollisionObject* %60, float %61, float 0.000000e+00, float 0.000000e+00)
  %call42 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis0)
  %call43 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis1)
  %62 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB44 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %62, i32 0, i32 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB44, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1)
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %axis0)
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %axis1)
  %63 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %63, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, i32 2)
  %64 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %64, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, i32 2)
  %65 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %65, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, i32 2)
  %66 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %66, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, i32 2)
  %call47 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %axis0)
  %conv = fpext float %call47 to double
  %cmp48 = fcmp ogt double %conv, 1.000000e-03
  br i1 %cmp48, label %if.then49, label %if.end52

if.then49:                                        ; preds = %if.then39
  %67 = load i32, i32* %solverBodyIdA, align 4
  %68 = load i32, i32* %solverBodyIdB, align 4
  %69 = load i32, i32* %frictionIndex, align 4
  %70 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %71 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedRollingFriction50 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %71, i32 0, i32 7
  %72 = load float, float* %m_combinedRollingFriction50, align 4
  %73 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %74 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %75 = load float, float* %relaxation, align 4
  %call51 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver30addTorsionalFrictionConstraintERK9btVector3iiiR15btManifoldPointfS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, i32 %67, i32 %68, i32 %69, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %70, float %72, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %73, %class.btCollisionObject* %74, float %75, float 0.000000e+00, float 0.000000e+00)
  br label %if.end52

if.end52:                                         ; preds = %if.then49, %if.then39
  %call53 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %axis1)
  %conv54 = fpext float %call53 to double
  %cmp55 = fcmp ogt double %conv54, 1.000000e-03
  br i1 %cmp55, label %if.then56, label %if.end59

if.then56:                                        ; preds = %if.end52
  %76 = load i32, i32* %solverBodyIdA, align 4
  %77 = load i32, i32* %solverBodyIdB, align 4
  %78 = load i32, i32* %frictionIndex, align 4
  %79 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %80 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_combinedRollingFriction57 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %80, i32 0, i32 7
  %81 = load float, float* %m_combinedRollingFriction57, align 4
  %82 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %83 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %84 = load float, float* %relaxation, align 4
  %call58 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver30addTorsionalFrictionConstraintERK9btVector3iiiR15btManifoldPointfS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, i32 %76, i32 %77, i32 %78, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %79, float %81, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %82, %class.btCollisionObject* %83, float %84, float 0.000000e+00, float 0.000000e+00)
  br label %if.end59

if.end59:                                         ; preds = %if.then56, %if.end52
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %land.lhs.true37, %if.then19
  %85 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %86 = bitcast %struct.btContactSolverInfo* %85 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %86, i32 0, i32 16
  %87 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %87, 32
  %tobool61 = icmp ne i32 %and, 0
  br i1 %tobool61, label %lor.lhs.false62, label %if.then65

lor.lhs.false62:                                  ; preds = %if.end60
  %88 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %88, i32 0, i32 15
  %89 = load i32, i32* %m_contactPointFlags, align 4
  %and63 = and i32 %89, 1
  %tobool64 = icmp ne i32 %and63, 0
  br i1 %tobool64, label %if.else126, label %if.then65

if.then65:                                        ; preds = %lor.lhs.false62, %if.end60
  %90 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB68 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %90, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB68, float* nonnull align 4 dereferenceable(4) %rel_vel)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp67)
  %91 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %91, i32 0, i32 25
  %92 = bitcast %class.btVector3* %m_lateralFrictionDir1 to i8*
  %93 = bitcast %class.btVector3* %ref.tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false)
  %94 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir169 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %94, i32 0, i32 25
  %call70 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_lateralFrictionDir169)
  store float %call70, float* %lat_rel_vel, align 4
  %95 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %96 = bitcast %struct.btContactSolverInfo* %95 to %struct.btContactSolverInfoData*
  %m_solverMode71 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %96, i32 0, i32 16
  %97 = load i32, i32* %m_solverMode71, align 4
  %and72 = and i32 %97, 64
  %tobool73 = icmp ne i32 %and72, 0
  br i1 %tobool73, label %if.else, label %land.lhs.true74

land.lhs.true74:                                  ; preds = %if.then65
  %98 = load float, float* %lat_rel_vel, align 4
  %cmp75 = fcmp ogt float %98, 0x3E80000000000000
  br i1 %cmp75, label %if.then76, label %if.else

if.then76:                                        ; preds = %land.lhs.true74
  %99 = load float, float* %lat_rel_vel, align 4
  %call78 = call float @_Z6btSqrtf(float %99)
  %div = fdiv float 1.000000e+00, %call78
  store float %div, float* %ref.tmp77, align 4
  %100 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir179 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %100, i32 0, i32 25
  %call80 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_lateralFrictionDir179, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  %101 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %102 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir181 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %102, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %101, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir181, i32 1)
  %103 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %104 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir182 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %104, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %103, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir182, i32 1)
  %105 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir183 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %105, i32 0, i32 25
  %106 = load i32, i32* %solverBodyIdA, align 4
  %107 = load i32, i32* %solverBodyIdB, align 4
  %108 = load i32, i32* %frictionIndex, align 4
  %109 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %110 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %111 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %112 = load float, float* %relaxation, align 4
  %call84 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir183, i32 %106, i32 %107, i32 %108, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %109, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %110, %class.btCollisionObject* %111, float %112, float 0.000000e+00, float 0.000000e+00)
  %113 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %114 = bitcast %struct.btContactSolverInfo* %113 to %struct.btContactSolverInfoData*
  %m_solverMode85 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %114, i32 0, i32 16
  %115 = load i32, i32* %m_solverMode85, align 4
  %and86 = and i32 %115, 16
  %tobool87 = icmp ne i32 %and86, 0
  br i1 %tobool87, label %if.then88, label %if.end98

if.then88:                                        ; preds = %if.then76
  %116 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir190 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %116, i32 0, i32 25
  %117 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB91 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %117, i32 0, i32 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp89, %class.btVector3* %m_lateralFrictionDir190, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB91)
  %118 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %118, i32 0, i32 26
  %119 = bitcast %class.btVector3* %m_lateralFrictionDir2 to i8*
  %120 = bitcast %class.btVector3* %ref.tmp89 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %119, i8* align 4 %120, i32 16, i1 false)
  %121 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir292 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %121, i32 0, i32 26
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_lateralFrictionDir292)
  %122 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %123 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir294 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %123, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %122, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir294, i32 1)
  %124 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %125 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir295 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %125, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %124, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir295, i32 1)
  %126 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir296 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %126, i32 0, i32 26
  %127 = load i32, i32* %solverBodyIdA, align 4
  %128 = load i32, i32* %solverBodyIdB, align 4
  %129 = load i32, i32* %frictionIndex, align 4
  %130 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %131 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %132 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %133 = load float, float* %relaxation, align 4
  %call97 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir296, i32 %127, i32 %128, i32 %129, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %130, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %131, %class.btCollisionObject* %132, float %133, float 0.000000e+00, float 0.000000e+00)
  br label %if.end98

if.end98:                                         ; preds = %if.then88, %if.then76
  br label %if.end125

if.else:                                          ; preds = %land.lhs.true74, %if.then65
  %134 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_normalWorldOnB99 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %134, i32 0, i32 4
  %135 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1100 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %135, i32 0, i32 25
  %136 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2101 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %136, i32 0, i32 26
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB99, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1100, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2101)
  %137 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %138 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1102 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %138, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %137, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1102, i32 1)
  %139 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %140 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1103 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %140, i32 0, i32 25
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %139, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1103, i32 1)
  %141 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1104 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %141, i32 0, i32 25
  %142 = load i32, i32* %solverBodyIdA, align 4
  %143 = load i32, i32* %solverBodyIdB, align 4
  %144 = load i32, i32* %frictionIndex, align 4
  %145 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %146 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %147 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %148 = load float, float* %relaxation, align 4
  %call105 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1104, i32 %142, i32 %143, i32 %144, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %145, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %146, %class.btCollisionObject* %147, float %148, float 0.000000e+00, float 0.000000e+00)
  %149 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %150 = bitcast %struct.btContactSolverInfo* %149 to %struct.btContactSolverInfoData*
  %m_solverMode106 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %150, i32 0, i32 16
  %151 = load i32, i32* %m_solverMode106, align 4
  %and107 = and i32 %151, 16
  %tobool108 = icmp ne i32 %and107, 0
  br i1 %tobool108, label %if.then109, label %if.end114

if.then109:                                       ; preds = %if.else
  %152 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %153 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2110 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %153, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %152, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2110, i32 1)
  %154 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %155 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2111 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %155, i32 0, i32 26
  call void @_ZN35btSequentialImpulseConstraintSolver24applyAnisotropicFrictionEP17btCollisionObjectR9btVector3i(%class.btCollisionObject* %154, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2111, i32 1)
  %156 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2112 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %156, i32 0, i32 26
  %157 = load i32, i32* %solverBodyIdA, align 4
  %158 = load i32, i32* %solverBodyIdB, align 4
  %159 = load i32, i32* %frictionIndex, align 4
  %160 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %161 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %162 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %163 = load float, float* %relaxation, align 4
  %call113 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2112, i32 %157, i32 %158, i32 %159, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %160, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %161, %class.btCollisionObject* %162, float %163, float 0.000000e+00, float 0.000000e+00)
  br label %if.end114

if.end114:                                        ; preds = %if.then109, %if.else
  %164 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %165 = bitcast %struct.btContactSolverInfo* %164 to %struct.btContactSolverInfoData*
  %m_solverMode115 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %165, i32 0, i32 16
  %166 = load i32, i32* %m_solverMode115, align 4
  %and116 = and i32 %166, 16
  %tobool117 = icmp ne i32 %and116, 0
  br i1 %tobool117, label %land.lhs.true118, label %if.end124

land.lhs.true118:                                 ; preds = %if.end114
  %167 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %168 = bitcast %struct.btContactSolverInfo* %167 to %struct.btContactSolverInfoData*
  %m_solverMode119 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %168, i32 0, i32 16
  %169 = load i32, i32* %m_solverMode119, align 4
  %and120 = and i32 %169, 64
  %tobool121 = icmp ne i32 %and120, 0
  br i1 %tobool121, label %if.then122, label %if.end124

if.then122:                                       ; preds = %land.lhs.true118
  %170 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactPointFlags123 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %170, i32 0, i32 15
  %171 = load i32, i32* %m_contactPointFlags123, align 4
  %or = or i32 %171, 1
  store i32 %or, i32* %m_contactPointFlags123, align 4
  br label %if.end124

if.end124:                                        ; preds = %if.then122, %land.lhs.true118, %if.end114
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.end98
  br label %if.end137

if.else126:                                       ; preds = %lor.lhs.false62
  %172 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir1127 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %172, i32 0, i32 25
  %173 = load i32, i32* %solverBodyIdA, align 4
  %174 = load i32, i32* %solverBodyIdB, align 4
  %175 = load i32, i32* %frictionIndex, align 4
  %176 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %177 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %178 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %179 = load float, float* %relaxation, align 4
  %180 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %180, i32 0, i32 19
  %181 = load float, float* %m_contactMotion1, align 4
  %182 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_frictionCFM = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %182, i32 0, i32 23
  %183 = load float, float* %m_frictionCFM, align 4
  %call128 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir1127, i32 %173, i32 %174, i32 %175, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %176, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %177, %class.btCollisionObject* %178, float %179, float %181, float %183)
  %184 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %185 = bitcast %struct.btContactSolverInfo* %184 to %struct.btContactSolverInfoData*
  %m_solverMode129 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %185, i32 0, i32 16
  %186 = load i32, i32* %m_solverMode129, align 4
  %and130 = and i32 %186, 16
  %tobool131 = icmp ne i32 %and130, 0
  br i1 %tobool131, label %if.then132, label %if.end136

if.then132:                                       ; preds = %if.else126
  %187 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_lateralFrictionDir2133 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %187, i32 0, i32 26
  %188 = load i32, i32* %solverBodyIdA, align 4
  %189 = load i32, i32* %solverBodyIdB, align 4
  %190 = load i32, i32* %frictionIndex, align 4
  %191 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %192 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %193 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %194 = load float, float* %relaxation, align 4
  %195 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %195, i32 0, i32 20
  %196 = load float, float* %m_contactMotion2, align 4
  %197 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %m_frictionCFM134 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %197, i32 0, i32 23
  %198 = load float, float* %m_frictionCFM134, align 4
  %call135 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN35btSequentialImpulseConstraintSolver21addFrictionConstraintERK9btVector3iiiR15btManifoldPointS2_S2_P17btCollisionObjectS6_fff(%class.btSequentialImpulseConstraintSolver* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_lateralFrictionDir2133, i32 %188, i32 %189, i32 %190, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %191, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2, %class.btCollisionObject* %192, %class.btCollisionObject* %193, float %194, float %196, float %198)
  br label %if.end136

if.end136:                                        ; preds = %if.then132, %if.else126
  br label %if.end137

if.end137:                                        ; preds = %if.end136, %if.end125
  %199 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %200 = load i32, i32* %solverBodyIdA, align 4
  %201 = load i32, i32* %solverBodyIdB, align 4
  %202 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp, align 4
  %203 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver28setFrictionConstraintImpulseER18btSolverConstraintiiR15btManifoldPointRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %199, i32 %200, i32 %201, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %202, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %203)
  br label %if.end138

if.end138:                                        ; preds = %if.end137, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end138
  %204 = load i32, i32* %j, align 4
  %inc = add nsw i32 %204, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %cmp = fcmp olt float %call, 0x3D10000000000000
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %class.btManifoldPoint* @_ZN20btPersistentManifold15getContactPointEi(%class.btPersistentManifold* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %index.addr = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %0
  ret %class.btManifoldPoint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK20btPersistentManifold29getContactProcessingThresholdEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 6
  %0 = load float, float* %m_contactProcessingThreshold, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnAEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  ret %class.btVector3* %m_positionWorldOnA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btManifoldPoint19getPositionWorldOnBEv(%class.btManifoldPoint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  ret %class.btVector3* %m_positionWorldOnB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btSolverBody30getVelocityInLocalPointNoDeltaERK9btVector3RS0_(%struct.btSolverBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse)
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 11
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalTorqueImpulse)
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %2 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  %3 = bitcast %class.btVector3* %2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %5, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver15convertContactsEPP20btPersistentManifoldiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %manifold, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numManifolds.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %4, %class.btPersistentManifold** %manifold, align 4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %6 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  call void @_ZN35btSequentialImpulseConstraintSolver14convertContactEP20btPersistentManifoldRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this1, %class.btPersistentManifold* %5, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %6)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver28solveGroupCacheFriendlySetupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %i = alloca i32, align 4
  %ref.tmp = alloca %struct.btSolverBody, align 4
  %i4 = alloca i32, align 4
  %bodyId = alloca i32, align 4
  %body = alloca %class.btRigidBody*, align 4
  %solverBody = alloca %struct.btSolverBody*, align 4
  %gyroForce = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %totalNumRows = alloca i32, align 4
  %i58 = alloca i32, align 4
  %info1 = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %fb = alloca %struct.btJointFeedback*, align 4
  %currentRow = alloca i32, align 4
  %info188 = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %currentConstraintRow = alloca %struct.btSolverConstraint*, align 4
  %constraint96 = alloca %class.btTypedConstraint*, align 4
  %rbA = alloca %class.btRigidBody*, align 4
  %rbB = alloca %class.btRigidBody*, align 4
  %solverBodyIdA = alloca i32, align 4
  %solverBodyIdB = alloca i32, align 4
  %bodyAPtr = alloca %struct.btSolverBody*, align 4
  %bodyBPtr = alloca %struct.btSolverBody*, align 4
  %overrideNumSolverIterations = alloca i32, align 4
  %j116 = alloca i32, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp138 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp142 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp146 = alloca float, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %ref.tmp150 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp153 = alloca float, align 4
  %ref.tmp154 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp161 = alloca float, align 4
  %ref.tmp162 = alloca float, align 4
  %ref.tmp163 = alloca float, align 4
  %info2 = alloca %"struct.btTypedConstraint::btConstraintInfo2", align 4
  %solverConstraint = alloca %struct.btSolverConstraint*, align 4
  %ftorqueAxis1 = alloca %class.btVector3*, align 4
  %ref.tmp205 = alloca %class.btVector3, align 4
  %ref.tmp206 = alloca %class.btVector3, align 4
  %ftorqueAxis2 = alloca %class.btVector3*, align 4
  %ref.tmp212 = alloca %class.btVector3, align 4
  %ref.tmp213 = alloca %class.btVector3, align 4
  %iMJlA = alloca %class.btVector3, align 4
  %ref.tmp219 = alloca float, align 4
  %iMJaA = alloca %class.btVector3, align 4
  %iMJlB = alloca %class.btVector3, align 4
  %ref.tmp224 = alloca float, align 4
  %iMJaB = alloca %class.btVector3, align 4
  %sum = alloca float, align 4
  %fsum = alloca float, align 4
  %rel_vel = alloca float, align 4
  %externalForceImpulseA = alloca %class.btVector3, align 4
  %ref.tmp249 = alloca float, align 4
  %ref.tmp250 = alloca float, align 4
  %ref.tmp251 = alloca float, align 4
  %externalTorqueImpulseA = alloca %class.btVector3, align 4
  %ref.tmp259 = alloca float, align 4
  %ref.tmp260 = alloca float, align 4
  %ref.tmp261 = alloca float, align 4
  %externalForceImpulseB = alloca %class.btVector3, align 4
  %ref.tmp269 = alloca float, align 4
  %ref.tmp270 = alloca float, align 4
  %ref.tmp271 = alloca float, align 4
  %externalTorqueImpulseB = alloca %class.btVector3, align 4
  %ref.tmp279 = alloca float, align 4
  %ref.tmp280 = alloca float, align 4
  %ref.tmp281 = alloca float, align 4
  %vel1Dotn = alloca float, align 4
  %ref.tmp285 = alloca %class.btVector3, align 4
  %ref.tmp289 = alloca %class.btVector3, align 4
  %vel2Dotn = alloca float, align 4
  %ref.tmp294 = alloca %class.btVector3, align 4
  %ref.tmp298 = alloca %class.btVector3, align 4
  %restitution = alloca float, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  %numNonContactPool = alloca i32, align 4
  %numConstraintPool = alloca i32, align 4
  %numFrictionPool = alloca i32, align 4
  %i336 = alloca i32, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_fixedBodyId = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 11
  store i32 -1, i32* %m_fixedBodyId, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str, i32 0, i32 0))
  %m_maxOverrideNumSolverIterations = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 10
  store i32 0, i32* %m_maxOverrideNumSolverIterations, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %2, i32 %3
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %4, i32 -1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %6 = load i32, i32* %numBodies.addr, align 4
  %add = add nsw i32 %6, 1
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %add)
  %m_tmpSolverBodyPool2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %7 = bitcast %struct.btSolverBody* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %7, i8 0, i32 244, i1 false)
  %call3 = call %struct.btSolverBody* @_ZN12btSolverBodyC2Ev(%struct.btSolverBody* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_tmpSolverBodyPool2, i32 0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %ref.tmp)
  store i32 0, i32* %i4, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc48, %for.end
  %8 = load i32, i32* %i4, align 4
  %9 = load i32, i32* %numBodies.addr, align 4
  %cmp6 = icmp slt i32 %8, %9
  br i1 %cmp6, label %for.body7, label %for.end50

for.body7:                                        ; preds = %for.cond5
  %10 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %11 = load i32, i32* %i4, align 4
  %arrayidx8 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %10, i32 %11
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx8, align 4
  %13 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %14 = bitcast %struct.btContactSolverInfo* %13 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 3
  %15 = load float, float* %m_timeStep, align 4
  %call9 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %12, float %15)
  store i32 %call9, i32* %bodyId, align 4
  %16 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %17 = load i32, i32* %i4, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %16, i32 %17
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx10, align 4
  %call11 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %18)
  store %class.btRigidBody* %call11, %class.btRigidBody** %body, align 4
  %19 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool = icmp ne %class.btRigidBody* %19, null
  br i1 %tobool, label %land.lhs.true, label %if.end47

land.lhs.true:                                    ; preds = %for.body7
  %20 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call12 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %20)
  %tobool13 = fcmp une float %call12, 0.000000e+00
  br i1 %tobool13, label %if.then, label %if.end47

if.then:                                          ; preds = %land.lhs.true
  %m_tmpSolverBodyPool14 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %21 = load i32, i32* %bodyId, align 4
  %call15 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool14, i32 %21)
  store %struct.btSolverBody* %call15, %struct.btSolverBody** %solverBody, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %gyroForce, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %22 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call20 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %22)
  %and = and i32 %call20, 2
  %tobool21 = icmp ne i32 %and, 0
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then
  %23 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %24 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %25 = bitcast %struct.btContactSolverInfo* %24 to %struct.btContactSolverInfoData*
  %m_maxGyroscopicForce = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %25, i32 0, i32 19
  %26 = load float, float* %m_maxGyroscopicForce, align 4
  call void @_ZNK11btRigidBody30computeGyroscopicForceExplicitEf(%class.btVector3* sret align 4 %ref.tmp23, %class.btRigidBody* %23, float %26)
  %27 = bitcast %class.btVector3* %gyroForce to i8*
  %28 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %29)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %gyroForce, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call26)
  %30 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %31 = bitcast %struct.btContactSolverInfo* %30 to %struct.btContactSolverInfoData*
  %m_timeStep27 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %31, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %m_timeStep27)
  %32 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody, align 4
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %32, i32 0, i32 11
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_externalTorqueImpulse, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24)
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then
  %33 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call29 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %33)
  %and30 = and i32 %call29, 4
  %tobool31 = icmp ne i32 %and30, 0
  br i1 %tobool31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end
  %34 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %35 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %36 = bitcast %struct.btContactSolverInfo* %35 to %struct.btContactSolverInfoData*
  %m_timeStep34 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %36, i32 0, i32 3
  %37 = load float, float* %m_timeStep34, align 4
  call void @_ZNK11btRigidBody38computeGyroscopicImpulseImplicit_WorldEf(%class.btVector3* sret align 4 %ref.tmp33, %class.btRigidBody* %34, float %37)
  %38 = bitcast %class.btVector3* %gyroForce to i8*
  %39 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false)
  %40 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody, align 4
  %m_externalTorqueImpulse35 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %40, i32 0, i32 11
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_externalTorqueImpulse35, %class.btVector3* nonnull align 4 dereferenceable(16) %gyroForce)
  br label %if.end37

if.end37:                                         ; preds = %if.then32, %if.end
  %41 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %call38 = call i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %41)
  %and39 = and i32 %call38, 8
  %tobool40 = icmp ne i32 %and39, 0
  br i1 %tobool40, label %if.then41, label %if.end46

if.then41:                                        ; preds = %if.end37
  %42 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %43 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %44 = bitcast %struct.btContactSolverInfo* %43 to %struct.btContactSolverInfoData*
  %m_timeStep43 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %44, i32 0, i32 3
  %45 = load float, float* %m_timeStep43, align 4
  call void @_ZNK11btRigidBody37computeGyroscopicImpulseImplicit_BodyEf(%class.btVector3* sret align 4 %ref.tmp42, %class.btRigidBody* %42, float %45)
  %46 = bitcast %class.btVector3* %gyroForce to i8*
  %47 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false)
  %48 = load %struct.btSolverBody*, %struct.btSolverBody** %solverBody, align 4
  %m_externalTorqueImpulse44 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %48, i32 0, i32 11
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_externalTorqueImpulse44, %class.btVector3* nonnull align 4 dereferenceable(16) %gyroForce)
  br label %if.end46

if.end46:                                         ; preds = %if.then41, %if.end37
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %land.lhs.true, %for.body7
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47
  %49 = load i32, i32* %i4, align 4
  %inc49 = add nsw i32 %49, 1
  store i32 %inc49, i32* %i4, align 4
  br label %for.cond5

for.end50:                                        ; preds = %for.cond5
  store i32 0, i32* %j, align 4
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc55, %for.end50
  %50 = load i32, i32* %j, align 4
  %51 = load i32, i32* %numConstraints.addr, align 4
  %cmp52 = icmp slt i32 %50, %51
  br i1 %cmp52, label %for.body53, label %for.end57

for.body53:                                       ; preds = %for.cond51
  %52 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %53 = load i32, i32* %j, align 4
  %arrayidx54 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %52, i32 %53
  %54 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx54, align 4
  store %class.btTypedConstraint* %54, %class.btTypedConstraint** %constraint, align 4
  %55 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %56 = bitcast %class.btTypedConstraint* %55 to void (%class.btTypedConstraint*)***
  %vtable = load void (%class.btTypedConstraint*)**, void (%class.btTypedConstraint*)*** %56, align 4
  %vfn = getelementptr inbounds void (%class.btTypedConstraint*)*, void (%class.btTypedConstraint*)** %vtable, i64 2
  %57 = load void (%class.btTypedConstraint*)*, void (%class.btTypedConstraint*)** %vfn, align 4
  call void %57(%class.btTypedConstraint* %55)
  %58 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  call void @_ZN17btTypedConstraint25internalSetAppliedImpulseEf(%class.btTypedConstraint* %58, float 0.000000e+00)
  br label %for.inc55

for.inc55:                                        ; preds = %for.body53
  %59 = load i32, i32* %j, align 4
  %inc56 = add nsw i32 %59, 1
  store i32 %inc56, i32* %j, align 4
  br label %for.cond51

for.end57:                                        ; preds = %for.cond51
  store i32 0, i32* %totalNumRows, align 4
  %m_tmpConstraintSizesPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %60 = load i32, i32* %numConstraints.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE18resizeNoInitializeEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool, i32 %60)
  store i32 0, i32* %i58, align 4
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc82, %for.end57
  %61 = load i32, i32* %i58, align 4
  %62 = load i32, i32* %numConstraints.addr, align 4
  %cmp60 = icmp slt i32 %61, %62
  br i1 %cmp60, label %for.body61, label %for.end84

for.body61:                                       ; preds = %for.cond59
  %m_tmpConstraintSizesPool62 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %63 = load i32, i32* %i58, align 4
  %call63 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool62, i32 %63)
  store %"struct.btTypedConstraint::btConstraintInfo1"* %call63, %"struct.btTypedConstraint::btConstraintInfo1"** %info1, align 4
  %64 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %65 = load i32, i32* %i58, align 4
  %arrayidx64 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %64, i32 %65
  %66 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx64, align 4
  %call65 = call %struct.btJointFeedback* @_ZN17btTypedConstraint16getJointFeedbackEv(%class.btTypedConstraint* %66)
  store %struct.btJointFeedback* %call65, %struct.btJointFeedback** %fb, align 4
  %67 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %tobool66 = icmp ne %struct.btJointFeedback* %67, null
  br i1 %tobool66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %for.body61
  %68 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedForceBodyA = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %68, i32 0, i32 0
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_appliedForceBodyA)
  %69 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedTorqueBodyA = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %69, i32 0, i32 1
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_appliedTorqueBodyA)
  %70 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedForceBodyB = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %70, i32 0, i32 2
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_appliedForceBodyB)
  %71 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedTorqueBodyB = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %71, i32 0, i32 3
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_appliedTorqueBodyB)
  br label %if.end68

if.end68:                                         ; preds = %if.then67, %for.body61
  %72 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %73 = load i32, i32* %i58, align 4
  %arrayidx69 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %72, i32 %73
  %74 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx69, align 4
  %call70 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %74)
  br i1 %call70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %if.end68
  br label %if.end72

if.end72:                                         ; preds = %if.then71, %if.end68
  %75 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %76 = load i32, i32* %i58, align 4
  %arrayidx73 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %75, i32 %76
  %77 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx73, align 4
  %call74 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %77)
  br i1 %call74, label %if.then75, label %if.else

if.then75:                                        ; preds = %if.end72
  %78 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %79 = load i32, i32* %i58, align 4
  %arrayidx76 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %78, i32 %79
  %80 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx76, align 4
  %81 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info1, align 4
  %82 = bitcast %class.btTypedConstraint* %80 to void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)***
  %vtable77 = load void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)**, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)*** %82, align 4
  %vfn78 = getelementptr inbounds void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)*, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)** %vtable77, i64 4
  %83 = load void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)*, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)** %vfn78, align 4
  call void %83(%class.btTypedConstraint* %80, %"struct.btTypedConstraint::btConstraintInfo1"* %81)
  br label %if.end79

if.else:                                          ; preds = %if.end72
  %84 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info1, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %84, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %85 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info1, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %85, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  br label %if.end79

if.end79:                                         ; preds = %if.else, %if.then75
  %86 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info1, align 4
  %m_numConstraintRows80 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %86, i32 0, i32 0
  %87 = load i32, i32* %m_numConstraintRows80, align 4
  %88 = load i32, i32* %totalNumRows, align 4
  %add81 = add nsw i32 %88, %87
  store i32 %add81, i32* %totalNumRows, align 4
  br label %for.inc82

for.inc82:                                        ; preds = %if.end79
  %89 = load i32, i32* %i58, align 4
  %inc83 = add nsw i32 %89, 1
  store i32 %inc83, i32* %i58, align 4
  br label %for.cond59

for.end84:                                        ; preds = %for.cond59
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %90 = load i32, i32* %totalNumRows, align 4
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool, i32 %90)
  store i32 0, i32* %currentRow, align 4
  store i32 0, i32* %i58, align 4
  br label %for.cond85

for.cond85:                                       ; preds = %for.inc320, %for.end84
  %91 = load i32, i32* %i58, align 4
  %92 = load i32, i32* %numConstraints.addr, align 4
  %cmp86 = icmp slt i32 %91, %92
  br i1 %cmp86, label %for.body87, label %for.end322

for.body87:                                       ; preds = %for.cond85
  %m_tmpConstraintSizesPool89 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %93 = load i32, i32* %i58, align 4
  %call90 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool89, i32 %93)
  store %"struct.btTypedConstraint::btConstraintInfo1"* %call90, %"struct.btTypedConstraint::btConstraintInfo1"** %info188, align 4
  %94 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info188, align 4
  %m_numConstraintRows91 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %94, i32 0, i32 0
  %95 = load i32, i32* %m_numConstraintRows91, align 4
  %tobool92 = icmp ne i32 %95, 0
  br i1 %tobool92, label %if.then93, label %if.end315

if.then93:                                        ; preds = %for.body87
  %m_tmpSolverNonContactConstraintPool94 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %96 = load i32, i32* %currentRow, align 4
  %call95 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool94, i32 %96)
  store %struct.btSolverConstraint* %call95, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %97 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %98 = load i32, i32* %i58, align 4
  %arrayidx97 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %97, i32 %98
  %99 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx97, align 4
  store %class.btTypedConstraint* %99, %class.btTypedConstraint** %constraint96, align 4
  %100 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call98 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %100)
  store %class.btRigidBody* %call98, %class.btRigidBody** %rbA, align 4
  %101 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call99 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %101)
  store %class.btRigidBody* %call99, %class.btRigidBody** %rbB, align 4
  %102 = load %class.btRigidBody*, %class.btRigidBody** %rbA, align 4
  %103 = bitcast %class.btRigidBody* %102 to %class.btCollisionObject*
  %104 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %105 = bitcast %struct.btContactSolverInfo* %104 to %struct.btContactSolverInfoData*
  %m_timeStep100 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %105, i32 0, i32 3
  %106 = load float, float* %m_timeStep100, align 4
  %call101 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %103, float %106)
  store i32 %call101, i32* %solverBodyIdA, align 4
  %107 = load %class.btRigidBody*, %class.btRigidBody** %rbB, align 4
  %108 = bitcast %class.btRigidBody* %107 to %class.btCollisionObject*
  %109 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %110 = bitcast %struct.btContactSolverInfo* %109 to %struct.btContactSolverInfoData*
  %m_timeStep102 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %110, i32 0, i32 3
  %111 = load float, float* %m_timeStep102, align 4
  %call103 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %108, float %111)
  store i32 %call103, i32* %solverBodyIdB, align 4
  %m_tmpSolverBodyPool104 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %112 = load i32, i32* %solverBodyIdA, align 4
  %call105 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool104, i32 %112)
  store %struct.btSolverBody* %call105, %struct.btSolverBody** %bodyAPtr, align 4
  %m_tmpSolverBodyPool106 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %113 = load i32, i32* %solverBodyIdB, align 4
  %call107 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool106, i32 %113)
  store %struct.btSolverBody* %call107, %struct.btSolverBody** %bodyBPtr, align 4
  %114 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call108 = call i32 @_ZNK17btTypedConstraint30getOverrideNumSolverIterationsEv(%class.btTypedConstraint* %114)
  %cmp109 = icmp sgt i32 %call108, 0
  br i1 %cmp109, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then93
  %115 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call110 = call i32 @_ZNK17btTypedConstraint30getOverrideNumSolverIterationsEv(%class.btTypedConstraint* %115)
  br label %cond.end

cond.false:                                       ; preds = %if.then93
  %116 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %117 = bitcast %struct.btContactSolverInfo* %116 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %117, i32 0, i32 5
  %118 = load i32, i32* %m_numIterations, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call110, %cond.true ], [ %118, %cond.false ]
  store i32 %cond, i32* %overrideNumSolverIterations, align 4
  %119 = load i32, i32* %overrideNumSolverIterations, align 4
  %m_maxOverrideNumSolverIterations111 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 10
  %120 = load i32, i32* %m_maxOverrideNumSolverIterations111, align 4
  %cmp112 = icmp sgt i32 %119, %120
  br i1 %cmp112, label %if.then113, label %if.end115

if.then113:                                       ; preds = %cond.end
  %121 = load i32, i32* %overrideNumSolverIterations, align 4
  %m_maxOverrideNumSolverIterations114 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 10
  store i32 %121, i32* %m_maxOverrideNumSolverIterations114, align 4
  br label %if.end115

if.end115:                                        ; preds = %if.then113, %cond.end
  store i32 0, i32* %j116, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc129, %if.end115
  %122 = load i32, i32* %j116, align 4
  %123 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info188, align 4
  %m_numConstraintRows118 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %123, i32 0, i32 0
  %124 = load i32, i32* %m_numConstraintRows118, align 4
  %cmp119 = icmp slt i32 %122, %124
  br i1 %cmp119, label %for.body120, label %for.end131

for.body120:                                      ; preds = %for.cond117
  %125 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %126 = load i32, i32* %j116, align 4
  %arrayidx121 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %125, i32 %126
  %127 = bitcast %struct.btSolverConstraint* %arrayidx121 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %127, i8 0, i32 152, i1 false)
  %128 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %129 = load i32, i32* %j116, align 4
  %arrayidx122 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %128, i32 %129
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx122, i32 0, i32 12
  store float 0xC7EFFFFFE0000000, float* %m_lowerLimit, align 4
  %130 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %131 = load i32, i32* %j116, align 4
  %arrayidx123 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %130, i32 %131
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx123, i32 0, i32 13
  store float 0x47EFFFFFE0000000, float* %m_upperLimit, align 4
  %132 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %133 = load i32, i32* %j116, align 4
  %arrayidx124 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %132, i32 %133
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx124, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %134 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %135 = load i32, i32* %j116, align 4
  %arrayidx125 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %134, i32 %135
  %m_appliedPushImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx125, i32 0, i32 6
  store float 0.000000e+00, float* %m_appliedPushImpulse, align 4
  %136 = load i32, i32* %solverBodyIdA, align 4
  %137 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %138 = load i32, i32* %j116, align 4
  %arrayidx126 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %137, i32 %138
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx126, i32 0, i32 18
  store i32 %136, i32* %m_solverBodyIdA, align 4
  %139 = load i32, i32* %solverBodyIdB, align 4
  %140 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %141 = load i32, i32* %j116, align 4
  %arrayidx127 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %140, i32 %141
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx127, i32 0, i32 19
  store i32 %139, i32* %m_solverBodyIdB, align 4
  %142 = load i32, i32* %overrideNumSolverIterations, align 4
  %143 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %144 = load i32, i32* %j116, align 4
  %arrayidx128 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %143, i32 %144
  %m_overrideNumSolverIterations = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %arrayidx128, i32 0, i32 16
  store i32 %142, i32* %m_overrideNumSolverIterations, align 4
  br label %for.inc129

for.inc129:                                       ; preds = %for.body120
  %145 = load i32, i32* %j116, align 4
  %inc130 = add nsw i32 %145, 1
  store i32 %inc130, i32* %j116, align 4
  br label %for.cond117

for.end131:                                       ; preds = %for.cond117
  %146 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %146)
  store float 0.000000e+00, float* %ref.tmp133, align 4
  store float 0.000000e+00, float* %ref.tmp134, align 4
  store float 0.000000e+00, float* %ref.tmp135, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call132, float* nonnull align 4 dereferenceable(4) %ref.tmp133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %ref.tmp135)
  %147 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %call136 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %147)
  store float 0.000000e+00, float* %ref.tmp137, align 4
  store float 0.000000e+00, float* %ref.tmp138, align 4
  store float 0.000000e+00, float* %ref.tmp139, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call136, float* nonnull align 4 dereferenceable(4) %ref.tmp137, float* nonnull align 4 dereferenceable(4) %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp139)
  %148 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %call140 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %148)
  store float 0.000000e+00, float* %ref.tmp141, align 4
  store float 0.000000e+00, float* %ref.tmp142, align 4
  store float 0.000000e+00, float* %ref.tmp143, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call140, float* nonnull align 4 dereferenceable(4) %ref.tmp141, float* nonnull align 4 dereferenceable(4) %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143)
  %149 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %call144 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %149)
  store float 0.000000e+00, float* %ref.tmp145, align 4
  store float 0.000000e+00, float* %ref.tmp146, align 4
  store float 0.000000e+00, float* %ref.tmp147, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call144, float* nonnull align 4 dereferenceable(4) %ref.tmp145, float* nonnull align 4 dereferenceable(4) %ref.tmp146, float* nonnull align 4 dereferenceable(4) %ref.tmp147)
  %150 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %call148 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody30internalGetDeltaLinearVelocityEv(%struct.btSolverBody* %150)
  store float 0.000000e+00, float* %ref.tmp149, align 4
  store float 0.000000e+00, float* %ref.tmp150, align 4
  store float 0.000000e+00, float* %ref.tmp151, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call148, float* nonnull align 4 dereferenceable(4) %ref.tmp149, float* nonnull align 4 dereferenceable(4) %ref.tmp150, float* nonnull align 4 dereferenceable(4) %ref.tmp151)
  %151 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %call152 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody31internalGetDeltaAngularVelocityEv(%struct.btSolverBody* %151)
  store float 0.000000e+00, float* %ref.tmp153, align 4
  store float 0.000000e+00, float* %ref.tmp154, align 4
  store float 0.000000e+00, float* %ref.tmp155, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call152, float* nonnull align 4 dereferenceable(4) %ref.tmp153, float* nonnull align 4 dereferenceable(4) %ref.tmp154, float* nonnull align 4 dereferenceable(4) %ref.tmp155)
  %152 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %call156 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetPushVelocityEv(%struct.btSolverBody* %152)
  store float 0.000000e+00, float* %ref.tmp157, align 4
  store float 0.000000e+00, float* %ref.tmp158, align 4
  store float 0.000000e+00, float* %ref.tmp159, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call156, float* nonnull align 4 dereferenceable(4) %ref.tmp157, float* nonnull align 4 dereferenceable(4) %ref.tmp158, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  %153 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN12btSolverBody23internalGetTurnVelocityEv(%struct.btSolverBody* %153)
  store float 0.000000e+00, float* %ref.tmp161, align 4
  store float 0.000000e+00, float* %ref.tmp162, align 4
  store float 0.000000e+00, float* %ref.tmp163, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call160, float* nonnull align 4 dereferenceable(4) %ref.tmp161, float* nonnull align 4 dereferenceable(4) %ref.tmp162, float* nonnull align 4 dereferenceable(4) %ref.tmp163)
  %154 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %155 = bitcast %struct.btContactSolverInfo* %154 to %struct.btContactSolverInfoData*
  %m_timeStep164 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %155, i32 0, i32 3
  %156 = load float, float* %m_timeStep164, align 4
  %div = fdiv float 1.000000e+00, %156
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 0
  store float %div, float* %fps, align 4
  %157 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %158 = bitcast %struct.btContactSolverInfo* %157 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %158, i32 0, i32 8
  %159 = load float, float* %m_erp, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 1
  store float %159, float* %erp, align 4
  %160 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %160, i32 0, i32 1
  %call165 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal1)
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 2
  store float* %call165, float** %m_J1linearAxis, align 4
  %161 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %161, i32 0, i32 0
  %call166 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos1CrossNormal)
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 3
  store float* %call166, float** %m_J1angularAxis, align 4
  %162 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %162, i32 0, i32 3
  %call167 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_contactNormal2)
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 4
  store float* %call167, float** %m_J2linearAxis, align 4
  %163 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %163, i32 0, i32 2
  %call168 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_relpos2CrossNormal)
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 5
  store float* %call168, float** %m_J2angularAxis, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 6
  store i32 38, i32* %rowskip, align 4
  %164 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_rhs = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %164, i32 0, i32 10
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 7
  store float* %m_rhs, float** %m_constraintError, align 4
  %165 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %166 = bitcast %struct.btContactSolverInfo* %165 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %166, i32 0, i32 10
  %167 = load float, float* %m_globalCfm, align 4
  %168 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_cfm = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %168, i32 0, i32 11
  store float %167, float* %m_cfm, align 4
  %169 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %170 = bitcast %struct.btContactSolverInfo* %169 to %struct.btContactSolverInfoData*
  %m_damping = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %170, i32 0, i32 1
  %171 = load float, float* %m_damping, align 4
  %m_damping169 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 12
  store float %171, float* %m_damping169, align 4
  %172 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_cfm170 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %172, i32 0, i32 11
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 8
  store float* %m_cfm170, float** %cfm, align 4
  %173 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_lowerLimit171 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %173, i32 0, i32 12
  %m_lowerLimit172 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 9
  store float* %m_lowerLimit171, float** %m_lowerLimit172, align 4
  %174 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %m_upperLimit173 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %174, i32 0, i32 13
  %m_upperLimit174 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 10
  store float* %m_upperLimit173, float** %m_upperLimit174, align 4
  %175 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %176 = bitcast %struct.btContactSolverInfo* %175 to %struct.btContactSolverInfoData*
  %m_numIterations175 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %176, i32 0, i32 5
  %177 = load i32, i32* %m_numIterations175, align 4
  %m_numIterations176 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 11
  store i32 %177, i32* %m_numIterations176, align 4
  %178 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %179 = load i32, i32* %i58, align 4
  %arrayidx177 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %178, i32 %179
  %180 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx177, align 4
  %181 = bitcast %class.btTypedConstraint* %180 to void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)***
  %vtable178 = load void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)**, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)*** %181, align 4
  %vfn179 = getelementptr inbounds void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)*, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)** %vtable178, i64 5
  %182 = load void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)*, void (%class.btTypedConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)** %vfn179, align 4
  call void %182(%class.btTypedConstraint* %180, %"struct.btTypedConstraint::btConstraintInfo2"* %info2)
  store i32 0, i32* %j116, align 4
  br label %for.cond180

for.cond180:                                      ; preds = %for.inc312, %for.end131
  %183 = load i32, i32* %j116, align 4
  %184 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info188, align 4
  %m_numConstraintRows181 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %184, i32 0, i32 0
  %185 = load i32, i32* %m_numConstraintRows181, align 4
  %cmp182 = icmp slt i32 %183, %185
  br i1 %cmp182, label %for.body183, label %for.end314

for.body183:                                      ; preds = %for.cond180
  %186 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %currentConstraintRow, align 4
  %187 = load i32, i32* %j116, align 4
  %arrayidx184 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %186, i32 %187
  store %struct.btSolverConstraint* %arrayidx184, %struct.btSolverConstraint** %solverConstraint, align 4
  %188 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_upperLimit185 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %188, i32 0, i32 13
  %189 = load float, float* %m_upperLimit185, align 4
  %190 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %191 = load i32, i32* %i58, align 4
  %arrayidx186 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %190, i32 %191
  %192 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx186, align 4
  %call187 = call float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %192)
  %cmp188 = fcmp oge float %189, %call187
  br i1 %cmp188, label %if.then189, label %if.end193

if.then189:                                       ; preds = %for.body183
  %193 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %194 = load i32, i32* %i58, align 4
  %arrayidx190 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %193, i32 %194
  %195 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx190, align 4
  %call191 = call float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %195)
  %196 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_upperLimit192 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %196, i32 0, i32 13
  store float %call191, float* %m_upperLimit192, align 4
  br label %if.end193

if.end193:                                        ; preds = %if.then189, %for.body183
  %197 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_lowerLimit194 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %197, i32 0, i32 12
  %198 = load float, float* %m_lowerLimit194, align 4
  %199 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %200 = load i32, i32* %i58, align 4
  %arrayidx195 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %199, i32 %200
  %201 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx195, align 4
  %call196 = call float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %201)
  %fneg = fneg float %call196
  %cmp197 = fcmp ole float %198, %fneg
  br i1 %cmp197, label %if.then198, label %if.end203

if.then198:                                       ; preds = %if.end193
  %202 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %203 = load i32, i32* %i58, align 4
  %arrayidx199 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %202, i32 %203
  %204 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx199, align 4
  %call200 = call float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %204)
  %fneg201 = fneg float %call200
  %205 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_lowerLimit202 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %205, i32 0, i32 12
  store float %fneg201, float* %m_lowerLimit202, align 4
  br label %if.end203

if.end203:                                        ; preds = %if.then198, %if.end193
  %206 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %207 = bitcast %class.btTypedConstraint* %206 to i8*
  %208 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %209 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %208, i32 0, i32 15
  %m_originalContactPoint = bitcast %union.anon.12* %209 to i8**
  store i8* %207, i8** %m_originalContactPoint, align 4
  %210 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos1CrossNormal204 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %210, i32 0, i32 0
  store %class.btVector3* %m_relpos1CrossNormal204, %class.btVector3** %ftorqueAxis1, align 4
  %211 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call207 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %211)
  %call208 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %call207)
  %212 = load %class.btVector3*, %class.btVector3** %ftorqueAxis1, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp206, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call208, %class.btVector3* nonnull align 4 dereferenceable(16) %212)
  %213 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call209 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %213)
  %call210 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %call209)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp205, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp206, %class.btVector3* nonnull align 4 dereferenceable(16) %call210)
  %214 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %214, i32 0, i32 4
  %215 = bitcast %class.btVector3* %m_angularComponentA to i8*
  %216 = bitcast %class.btVector3* %ref.tmp205 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %215, i8* align 4 %216, i32 16, i1 false)
  %217 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos2CrossNormal211 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %217, i32 0, i32 2
  store %class.btVector3* %m_relpos2CrossNormal211, %class.btVector3** %ftorqueAxis2, align 4
  %218 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call214 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %218)
  %call215 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %call214)
  %219 = load %class.btVector3*, %class.btVector3** %ftorqueAxis2, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp213, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call215, %class.btVector3* nonnull align 4 dereferenceable(16) %219)
  %220 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint96, align 4
  %call216 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %220)
  %call217 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %call216)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp212, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp213, %class.btVector3* nonnull align 4 dereferenceable(16) %call217)
  %221 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %221, i32 0, i32 5
  %222 = bitcast %class.btVector3* %m_angularComponentB to i8*
  %223 = bitcast %class.btVector3* %ref.tmp212 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %222, i8* align 4 %223, i32 16, i1 false)
  %224 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal1218 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %224, i32 0, i32 1
  %225 = load %class.btRigidBody*, %class.btRigidBody** %rbA, align 4
  %call220 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %225)
  store float %call220, float* %ref.tmp219, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %iMJlA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1218, float* nonnull align 4 dereferenceable(4) %ref.tmp219)
  %226 = load %class.btRigidBody*, %class.btRigidBody** %rbA, align 4
  %call221 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %226)
  %227 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos1CrossNormal222 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %227, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %iMJaA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call221, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal222)
  %228 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal2223 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %228, i32 0, i32 3
  %229 = load %class.btRigidBody*, %class.btRigidBody** %rbB, align 4
  %call225 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %229)
  store float %call225, float* %ref.tmp224, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %iMJlB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2223, float* nonnull align 4 dereferenceable(4) %ref.tmp224)
  %230 = load %class.btRigidBody*, %class.btRigidBody** %rbB, align 4
  %call226 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %230)
  %231 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos2CrossNormal227 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %231, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %iMJaB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call226, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal227)
  %232 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal1228 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %232, i32 0, i32 1
  %call229 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJlA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1228)
  store float %call229, float* %sum, align 4
  %233 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos1CrossNormal230 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %233, i32 0, i32 0
  %call231 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJaA, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal230)
  %234 = load float, float* %sum, align 4
  %add232 = fadd float %234, %call231
  store float %add232, float* %sum, align 4
  %235 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal2233 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %235, i32 0, i32 3
  %call234 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJlB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2233)
  %236 = load float, float* %sum, align 4
  %add235 = fadd float %236, %call234
  store float %add235, float* %sum, align 4
  %237 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos2CrossNormal236 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %237, i32 0, i32 2
  %call237 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %iMJaB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal236)
  %238 = load float, float* %sum, align 4
  %add238 = fadd float %238, %call237
  store float %add238, float* %sum, align 4
  %239 = load float, float* %sum, align 4
  %call239 = call float @_Z6btFabsf(float %239)
  store float %call239, float* %fsum, align 4
  %240 = load float, float* %fsum, align 4
  %cmp240 = fcmp ogt float %240, 0x3E80000000000000
  br i1 %cmp240, label %cond.true241, label %cond.false243

cond.true241:                                     ; preds = %if.end203
  %241 = load float, float* %sum, align 4
  %div242 = fdiv float 1.000000e+00, %241
  br label %cond.end244

cond.false243:                                    ; preds = %if.end203
  br label %cond.end244

cond.end244:                                      ; preds = %cond.false243, %cond.true241
  %cond245 = phi float [ %div242, %cond.true241 ], [ 0.000000e+00, %cond.false243 ]
  %242 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %242, i32 0, i32 9
  store float %cond245, float* %m_jacDiagABInv, align 4
  %243 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %243, i32 0, i32 12
  %244 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool246 = icmp ne %class.btRigidBody* %244, null
  br i1 %tobool246, label %cond.true247, label %cond.false248

cond.true247:                                     ; preds = %cond.end244
  %245 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %245, i32 0, i32 10
  %246 = bitcast %class.btVector3* %externalForceImpulseA to i8*
  %247 = bitcast %class.btVector3* %m_externalForceImpulse to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %246, i8* align 4 %247, i32 16, i1 false)
  br label %cond.end253

cond.false248:                                    ; preds = %cond.end244
  store float 0.000000e+00, float* %ref.tmp249, align 4
  store float 0.000000e+00, float* %ref.tmp250, align 4
  store float 0.000000e+00, float* %ref.tmp251, align 4
  %call252 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalForceImpulseA, float* nonnull align 4 dereferenceable(4) %ref.tmp249, float* nonnull align 4 dereferenceable(4) %ref.tmp250, float* nonnull align 4 dereferenceable(4) %ref.tmp251)
  br label %cond.end253

cond.end253:                                      ; preds = %cond.false248, %cond.true247
  %248 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %m_originalBody254 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %248, i32 0, i32 12
  %249 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody254, align 4
  %tobool255 = icmp ne %class.btRigidBody* %249, null
  br i1 %tobool255, label %cond.true256, label %cond.false258

cond.true256:                                     ; preds = %cond.end253
  %250 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyAPtr, align 4
  %m_externalTorqueImpulse257 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %250, i32 0, i32 11
  %251 = bitcast %class.btVector3* %externalTorqueImpulseA to i8*
  %252 = bitcast %class.btVector3* %m_externalTorqueImpulse257 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %251, i8* align 4 %252, i32 16, i1 false)
  br label %cond.end263

cond.false258:                                    ; preds = %cond.end253
  store float 0.000000e+00, float* %ref.tmp259, align 4
  store float 0.000000e+00, float* %ref.tmp260, align 4
  store float 0.000000e+00, float* %ref.tmp261, align 4
  %call262 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalTorqueImpulseA, float* nonnull align 4 dereferenceable(4) %ref.tmp259, float* nonnull align 4 dereferenceable(4) %ref.tmp260, float* nonnull align 4 dereferenceable(4) %ref.tmp261)
  br label %cond.end263

cond.end263:                                      ; preds = %cond.false258, %cond.true256
  %253 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %m_originalBody264 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %253, i32 0, i32 12
  %254 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody264, align 4
  %tobool265 = icmp ne %class.btRigidBody* %254, null
  br i1 %tobool265, label %cond.true266, label %cond.false268

cond.true266:                                     ; preds = %cond.end263
  %255 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %m_externalForceImpulse267 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %255, i32 0, i32 10
  %256 = bitcast %class.btVector3* %externalForceImpulseB to i8*
  %257 = bitcast %class.btVector3* %m_externalForceImpulse267 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %256, i8* align 4 %257, i32 16, i1 false)
  br label %cond.end273

cond.false268:                                    ; preds = %cond.end263
  store float 0.000000e+00, float* %ref.tmp269, align 4
  store float 0.000000e+00, float* %ref.tmp270, align 4
  store float 0.000000e+00, float* %ref.tmp271, align 4
  %call272 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalForceImpulseB, float* nonnull align 4 dereferenceable(4) %ref.tmp269, float* nonnull align 4 dereferenceable(4) %ref.tmp270, float* nonnull align 4 dereferenceable(4) %ref.tmp271)
  br label %cond.end273

cond.end273:                                      ; preds = %cond.false268, %cond.true266
  %258 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %m_originalBody274 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %258, i32 0, i32 12
  %259 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody274, align 4
  %tobool275 = icmp ne %class.btRigidBody* %259, null
  br i1 %tobool275, label %cond.true276, label %cond.false278

cond.true276:                                     ; preds = %cond.end273
  %260 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyBPtr, align 4
  %m_externalTorqueImpulse277 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %260, i32 0, i32 11
  %261 = bitcast %class.btVector3* %externalTorqueImpulseB to i8*
  %262 = bitcast %class.btVector3* %m_externalTorqueImpulse277 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %261, i8* align 4 %262, i32 16, i1 false)
  br label %cond.end283

cond.false278:                                    ; preds = %cond.end273
  store float 0.000000e+00, float* %ref.tmp279, align 4
  store float 0.000000e+00, float* %ref.tmp280, align 4
  store float 0.000000e+00, float* %ref.tmp281, align 4
  %call282 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %externalTorqueImpulseB, float* nonnull align 4 dereferenceable(4) %ref.tmp279, float* nonnull align 4 dereferenceable(4) %ref.tmp280, float* nonnull align 4 dereferenceable(4) %ref.tmp281)
  br label %cond.end283

cond.end283:                                      ; preds = %cond.false278, %cond.true276
  %263 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal1284 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %263, i32 0, i32 1
  %264 = load %class.btRigidBody*, %class.btRigidBody** %rbA, align 4
  %call286 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %264)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp285, %class.btVector3* nonnull align 4 dereferenceable(16) %call286, %class.btVector3* nonnull align 4 dereferenceable(16) %externalForceImpulseA)
  %call287 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal1284, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp285)
  %265 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos1CrossNormal288 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %265, i32 0, i32 0
  %266 = load %class.btRigidBody*, %class.btRigidBody** %rbA, align 4
  %call290 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %266)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp289, %class.btVector3* nonnull align 4 dereferenceable(16) %call290, %class.btVector3* nonnull align 4 dereferenceable(16) %externalTorqueImpulseA)
  %call291 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos1CrossNormal288, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp289)
  %add292 = fadd float %call287, %call291
  store float %add292, float* %vel1Dotn, align 4
  %267 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_contactNormal2293 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %267, i32 0, i32 3
  %268 = load %class.btRigidBody*, %class.btRigidBody** %rbB, align 4
  %call295 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %268)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp294, %class.btVector3* nonnull align 4 dereferenceable(16) %call295, %class.btVector3* nonnull align 4 dereferenceable(16) %externalForceImpulseB)
  %call296 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormal2293, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp294)
  %269 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_relpos2CrossNormal297 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %269, i32 0, i32 2
  %270 = load %class.btRigidBody*, %class.btRigidBody** %rbB, align 4
  %call299 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %270)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp298, %class.btVector3* nonnull align 4 dereferenceable(16) %call299, %class.btVector3* nonnull align 4 dereferenceable(16) %externalTorqueImpulseB)
  %call300 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_relpos2CrossNormal297, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp298)
  %add301 = fadd float %call296, %call300
  store float %add301, float* %vel2Dotn, align 4
  %271 = load float, float* %vel1Dotn, align 4
  %272 = load float, float* %vel2Dotn, align 4
  %add302 = fadd float %271, %272
  store float %add302, float* %rel_vel, align 4
  store float 0.000000e+00, float* %restitution, align 4
  %273 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_rhs303 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %273, i32 0, i32 10
  %274 = load float, float* %m_rhs303, align 4
  store float %274, float* %positionalError, align 4
  %275 = load float, float* %restitution, align 4
  %276 = load float, float* %rel_vel, align 4
  %m_damping304 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %info2, i32 0, i32 12
  %277 = load float, float* %m_damping304, align 4
  %mul = fmul float %276, %277
  %sub = fsub float %275, %mul
  store float %sub, float* %velocityError, align 4
  %278 = load float, float* %positionalError, align 4
  %279 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_jacDiagABInv305 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %279, i32 0, i32 9
  %280 = load float, float* %m_jacDiagABInv305, align 4
  %mul306 = fmul float %278, %280
  store float %mul306, float* %penetrationImpulse, align 4
  %281 = load float, float* %velocityError, align 4
  %282 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_jacDiagABInv307 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %282, i32 0, i32 9
  %283 = load float, float* %m_jacDiagABInv307, align 4
  %mul308 = fmul float %281, %283
  store float %mul308, float* %velocityImpulse, align 4
  %284 = load float, float* %penetrationImpulse, align 4
  %285 = load float, float* %velocityImpulse, align 4
  %add309 = fadd float %284, %285
  %286 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_rhs310 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %286, i32 0, i32 10
  store float %add309, float* %m_rhs310, align 4
  %287 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstraint, align 4
  %m_appliedImpulse311 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %287, i32 0, i32 7
  store float 0.000000e+00, float* %m_appliedImpulse311, align 4
  br label %for.inc312

for.inc312:                                       ; preds = %cond.end283
  %288 = load i32, i32* %j116, align 4
  %inc313 = add nsw i32 %288, 1
  store i32 %inc313, i32* %j116, align 4
  br label %for.cond180

for.end314:                                       ; preds = %for.cond180
  br label %if.end315

if.end315:                                        ; preds = %for.end314, %for.body87
  %m_tmpConstraintSizesPool316 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 9
  %289 = load i32, i32* %i58, align 4
  %call317 = call nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %m_tmpConstraintSizesPool316, i32 %289)
  %m_numConstraintRows318 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %call317, i32 0, i32 0
  %290 = load i32, i32* %m_numConstraintRows318, align 4
  %291 = load i32, i32* %currentRow, align 4
  %add319 = add nsw i32 %291, %290
  store i32 %add319, i32* %currentRow, align 4
  br label %for.inc320

for.inc320:                                       ; preds = %if.end315
  %292 = load i32, i32* %i58, align 4
  %inc321 = add nsw i32 %292, 1
  store i32 %inc321, i32* %i58, align 4
  br label %for.cond85

for.end322:                                       ; preds = %for.cond85
  %293 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %294 = load i32, i32* %numManifolds.addr, align 4
  %295 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %296 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)***
  %vtable323 = load void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)**, void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)*** %296, align 4
  %vfn324 = getelementptr inbounds void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)*, void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)** %vtable323, i64 7
  %297 = load void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)*, void (%class.btSequentialImpulseConstraintSolver*, %class.btPersistentManifold**, i32, %struct.btContactSolverInfo*)** %vfn324, align 4
  call void %297(%class.btSequentialImpulseConstraintSolver* %this1, %class.btPersistentManifold** %293, i32 %294, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %295)
  %m_tmpSolverNonContactConstraintPool325 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %call326 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool325)
  store i32 %call326, i32* %numNonContactPool, align 4
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call327 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call327, i32* %numConstraintPool, align 4
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %call328 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  store i32 %call328, i32* %numFrictionPool, align 4
  %m_orderNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 7
  %298 = load i32, i32* %numNonContactPool, align 4
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool, i32 %298)
  %299 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %300 = bitcast %struct.btContactSolverInfo* %299 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %300, i32 0, i32 16
  %301 = load i32, i32* %m_solverMode, align 4
  %and329 = and i32 %301, 16
  %tobool330 = icmp ne i32 %and329, 0
  br i1 %tobool330, label %if.then331, label %if.else333

if.then331:                                       ; preds = %for.end322
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %302 = load i32, i32* %numConstraintPool, align 4
  %mul332 = mul nsw i32 %302, 2
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool, i32 %mul332)
  br label %if.end335

if.else333:                                       ; preds = %for.end322
  %m_orderTmpConstraintPool334 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %303 = load i32, i32* %numConstraintPool, align 4
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool334, i32 %303)
  br label %if.end335

if.end335:                                        ; preds = %if.else333, %if.then331
  %m_orderFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 8
  %304 = load i32, i32* %numFrictionPool, align 4
  call void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool, i32 %304)
  store i32 0, i32* %i336, align 4
  br label %for.cond337

for.cond337:                                      ; preds = %for.inc342, %if.end335
  %305 = load i32, i32* %i336, align 4
  %306 = load i32, i32* %numNonContactPool, align 4
  %cmp338 = icmp slt i32 %305, %306
  br i1 %cmp338, label %for.body339, label %for.end344

for.body339:                                      ; preds = %for.cond337
  %307 = load i32, i32* %i336, align 4
  %m_orderNonContactConstraintPool340 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 7
  %308 = load i32, i32* %i336, align 4
  %call341 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool340, i32 %308)
  store i32 %307, i32* %call341, align 4
  br label %for.inc342

for.inc342:                                       ; preds = %for.body339
  %309 = load i32, i32* %i336, align 4
  %inc343 = add nsw i32 %309, 1
  store i32 %inc343, i32* %i336, align 4
  br label %for.cond337

for.end344:                                       ; preds = %for.cond337
  store i32 0, i32* %i336, align 4
  br label %for.cond345

for.cond345:                                      ; preds = %for.inc350, %for.end344
  %310 = load i32, i32* %i336, align 4
  %311 = load i32, i32* %numConstraintPool, align 4
  %cmp346 = icmp slt i32 %310, %311
  br i1 %cmp346, label %for.body347, label %for.end352

for.body347:                                      ; preds = %for.cond345
  %312 = load i32, i32* %i336, align 4
  %m_orderTmpConstraintPool348 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %313 = load i32, i32* %i336, align 4
  %call349 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool348, i32 %313)
  store i32 %312, i32* %call349, align 4
  br label %for.inc350

for.inc350:                                       ; preds = %for.body347
  %314 = load i32, i32* %i336, align 4
  %inc351 = add nsw i32 %314, 1
  store i32 %inc351, i32* %i336, align 4
  br label %for.cond345

for.end352:                                       ; preds = %for.cond345
  store i32 0, i32* %i336, align 4
  br label %for.cond353

for.cond353:                                      ; preds = %for.inc358, %for.end352
  %315 = load i32, i32* %i336, align 4
  %316 = load i32, i32* %numFrictionPool, align 4
  %cmp354 = icmp slt i32 %315, %316
  br i1 %cmp354, label %for.body355, label %for.end360

for.body355:                                      ; preds = %for.cond353
  %317 = load i32, i32* %i336, align 4
  %m_orderFrictionConstraintPool356 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 8
  %318 = load i32, i32* %i336, align 4
  %call357 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool356, i32 %318)
  store i32 %317, i32* %call357, align 4
  br label %for.inc358

for.inc358:                                       ; preds = %for.body355
  %319 = load i32, i32* %i336, align 4
  %inc359 = add nsw i32 %319, 1
  store i32 %inc359, i32* %i336, align 4
  br label %for.cond353

for.end360:                                       ; preds = %for.cond353
  %call361 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #9
  ret float 0.000000e+00
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSolverBody*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btSolverBodyE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btSolverBody*
  store %struct.btSolverBody* %2, %struct.btSolverBody** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btSolverBody*, %struct.btSolverBody** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btSolverBody* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btSolverBody*, %struct.btSolverBody** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSolverBody* %4, %struct.btSolverBody** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btSolverBody*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btSolverBody* %fillData, %struct.btSolverBody** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end15

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %14, i32 %15
  %16 = bitcast %struct.btSolverBody* %arrayidx10 to i8*
  %call11 = call i8* @_ZN12btSolverBodynwEmPv(i32 244, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btSolverBody*
  %18 = load %struct.btSolverBody*, %struct.btSolverBody** %fillData.addr, align 4
  %call12 = call %struct.btSolverBody* @_ZN12btSolverBodyC2ERKS_(%struct.btSolverBody* %17, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6

for.end15:                                        ; preds = %for.cond6
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK11btRigidBody8getFlagsEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_rigidbodyFlags = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 22
  %0 = load i32, i32* %m_rigidbodyFlags, align 4
  ret i32 %0
}

declare void @_ZNK11btRigidBody30computeGyroscopicForceExplicitEf(%class.btVector3* sret align 4, %class.btRigidBody*, float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

declare void @_ZNK11btRigidBody38computeGyroscopicImpulseImplicit_WorldEf(%class.btVector3* sret align 4, %class.btRigidBody*, float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

declare void @_ZNK11btRigidBody37computeGyroscopicImpulseImplicit_BodyEf(%class.btVector3* sret align 4, %class.btRigidBody*, float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint25internalSetAppliedImpulseEf(%class.btTypedConstraint* %this, float %appliedImpulse) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %appliedImpulse.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store float %appliedImpulse, float* %appliedImpulse.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load float, float* %appliedImpulse.addr, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 10
  store float %0, float* %m_appliedImpulse, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE18resizeNoInitializeEi(%class.btAlignedObjectArray.18* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi(%class.btAlignedObjectArray.18* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 %1
  ret %"struct.btTypedConstraint::btConstraintInfo1"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btJointFeedback* @_ZN17btTypedConstraint16getJointFeedbackEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_jointFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 12
  %0 = load %struct.btJointFeedback*, %struct.btJointFeedback** %m_jointFeedback, align 4
  ret %struct.btJointFeedback* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btTypedConstraint30getOverrideNumSolverIterationsEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_overrideNumSolverIterations = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 7
  %0 = load i32, i32* %m_overrideNumSolverIterations, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_breakingImpulseThreshold = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 4
  %0 = load float, float* %m_breakingImpulseThreshold, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE18resizeNoInitializeEi(%class.btAlignedObjectArray.14* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.14* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #6

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver20solveSingleIterationEiPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %this, i32 %iteration, %class.btCollisionObject** %0, i32 %1, %class.btPersistentManifold** %2, i32 %3, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %4) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %iteration.addr = alloca i32, align 4
  %.addr = alloca %class.btCollisionObject**, align 4
  %.addr1 = alloca i32, align 4
  %.addr2 = alloca %class.btPersistentManifold**, align 4
  %.addr3 = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr4 = alloca %class.btIDebugDraw*, align 4
  %leastSquaresResidual = alloca float, align 4
  %numNonContactPool = alloca i32, align 4
  %numConstraintPool = alloca i32, align 4
  %numFrictionPool = alloca i32, align 4
  %j = alloca i32, align 4
  %tmp = alloca i32, align 4
  %swapi = alloca i32, align 4
  %j18 = alloca i32, align 4
  %tmp22 = alloca i32, align 4
  %swapi24 = alloca i32, align 4
  %j36 = alloca i32, align 4
  %tmp40 = alloca i32, align 4
  %swapi42 = alloca i32, align 4
  %j59 = alloca i32, align 4
  %constraint = alloca %struct.btSolverConstraint*, align 4
  %residual = alloca float, align 4
  %j83 = alloca i32, align 4
  %bodyAid = alloca i32, align 4
  %bodyBid = alloca i32, align 4
  %bodyA = alloca %struct.btSolverBody*, align 4
  %bodyB = alloca %struct.btSolverBody*, align 4
  %numPoolConstraints = alloca i32, align 4
  %multiplier = alloca i32, align 4
  %c = alloca i32, align 4
  %totalImpulse = alloca float, align 4
  %solveManifold = alloca %struct.btSolverConstraint*, align 4
  %residual122 = alloca float, align 4
  %applyFriction = alloca i8, align 1
  %solveManifold134 = alloca %struct.btSolverConstraint*, align 4
  %residual145 = alloca float, align 4
  %solveManifold160 = alloca %struct.btSolverConstraint*, align 4
  %residual176 = alloca float, align 4
  %numPoolConstraints192 = alloca i32, align 4
  %j195 = alloca i32, align 4
  %solveManifold199 = alloca %struct.btSolverConstraint*, align 4
  %residual204 = alloca float, align 4
  %numFrictionPoolConstraints = alloca i32, align 4
  %solveManifold222 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse227 = alloca float, align 4
  %residual240 = alloca float, align 4
  %numRollingFrictionPoolConstraints = alloca i32, align 4
  %rollingFrictionConstraint = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse260 = alloca float, align 4
  %rollingFrictionMagnitude = alloca float, align 4
  %residual277 = alloca float, align 4
  %j294 = alloca i32, align 4
  %constraint300 = alloca %struct.btSolverConstraint*, align 4
  %residual308 = alloca float, align 4
  %j325 = alloca i32, align 4
  %bodyAid332 = alloca i32, align 4
  %bodyBid337 = alloca i32, align 4
  %bodyA342 = alloca %struct.btSolverBody*, align 4
  %bodyB345 = alloca %struct.btSolverBody*, align 4
  %numPoolConstraints356 = alloca i32, align 4
  %j359 = alloca i32, align 4
  %solveManifold363 = alloca %struct.btSolverConstraint*, align 4
  %residual368 = alloca float, align 4
  %numFrictionPoolConstraints381 = alloca i32, align 4
  %j384 = alloca i32, align 4
  %solveManifold388 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse393 = alloca float, align 4
  %residual407 = alloca float, align 4
  %numRollingFrictionPoolConstraints421 = alloca i32, align 4
  %j424 = alloca i32, align 4
  %rollingFrictionConstraint428 = alloca %struct.btSolverConstraint*, align 4
  %totalImpulse431 = alloca float, align 4
  %rollingFrictionMagnitude438 = alloca float, align 4
  %residual449 = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store i32 %iteration, i32* %iteration.addr, align 4
  store %class.btCollisionObject** %0, %class.btCollisionObject*** %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %.addr2, align 4
  store i32 %3, i32* %.addr3, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %4, %class.btIDebugDraw** %.addr4, align 4
  %this5 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store float 0.000000e+00, float* %leastSquaresResidual, align 4
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  store i32 %call, i32* %numNonContactPool, align 4
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call6, i32* %numConstraintPool, align 4
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool)
  store i32 %call7, i32* %numFrictionPool, align 4
  %5 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %6 = bitcast %struct.btContactSolverInfo* %5 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %6, i32 0, i32 16
  %7 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %7, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end54

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %j, align 4
  %9 = load i32, i32* %numNonContactPool, align 4
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_orderNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %10 = load i32, i32* %j, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool, i32 %10)
  %11 = load i32, i32* %call8, align 4
  store i32 %11, i32* %tmp, align 4
  %12 = load i32, i32* %j, align 4
  %add = add nsw i32 %12, 1
  %call9 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %this5, i32 %add)
  store i32 %call9, i32* %swapi, align 4
  %m_orderNonContactConstraintPool10 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %13 = load i32, i32* %swapi, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool10, i32 %13)
  %14 = load i32, i32* %call11, align 4
  %m_orderNonContactConstraintPool12 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %15 = load i32, i32* %j, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool12, i32 %15)
  store i32 %14, i32* %call13, align 4
  %16 = load i32, i32* %tmp, align 4
  %m_orderNonContactConstraintPool14 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %17 = load i32, i32* %swapi, align 4
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool14, i32 %17)
  store i32 %16, i32* %call15, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %j, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = load i32, i32* %iteration.addr, align 4
  %20 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %21 = bitcast %struct.btContactSolverInfo* %20 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %21, i32 0, i32 5
  %22 = load i32, i32* %m_numIterations, align 4
  %cmp16 = icmp slt i32 %19, %22
  br i1 %cmp16, label %if.then17, label %if.end

if.then17:                                        ; preds = %for.end
  store i32 0, i32* %j18, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc33, %if.then17
  %23 = load i32, i32* %j18, align 4
  %24 = load i32, i32* %numConstraintPool, align 4
  %cmp20 = icmp slt i32 %23, %24
  br i1 %cmp20, label %for.body21, label %for.end35

for.body21:                                       ; preds = %for.cond19
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %25 = load i32, i32* %j18, align 4
  %call23 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool, i32 %25)
  %26 = load i32, i32* %call23, align 4
  store i32 %26, i32* %tmp22, align 4
  %27 = load i32, i32* %j18, align 4
  %add25 = add nsw i32 %27, 1
  %call26 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %this5, i32 %add25)
  store i32 %call26, i32* %swapi24, align 4
  %m_orderTmpConstraintPool27 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %28 = load i32, i32* %swapi24, align 4
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool27, i32 %28)
  %29 = load i32, i32* %call28, align 4
  %m_orderTmpConstraintPool29 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %30 = load i32, i32* %j18, align 4
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool29, i32 %30)
  store i32 %29, i32* %call30, align 4
  %31 = load i32, i32* %tmp22, align 4
  %m_orderTmpConstraintPool31 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %32 = load i32, i32* %swapi24, align 4
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool31, i32 %32)
  store i32 %31, i32* %call32, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body21
  %33 = load i32, i32* %j18, align 4
  %inc34 = add nsw i32 %33, 1
  store i32 %inc34, i32* %j18, align 4
  br label %for.cond19

for.end35:                                        ; preds = %for.cond19
  store i32 0, i32* %j36, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc51, %for.end35
  %34 = load i32, i32* %j36, align 4
  %35 = load i32, i32* %numFrictionPool, align 4
  %cmp38 = icmp slt i32 %34, %35
  br i1 %cmp38, label %for.body39, label %for.end53

for.body39:                                       ; preds = %for.cond37
  %m_orderFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %36 = load i32, i32* %j36, align 4
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool, i32 %36)
  %37 = load i32, i32* %call41, align 4
  store i32 %37, i32* %tmp40, align 4
  %38 = load i32, i32* %j36, align 4
  %add43 = add nsw i32 %38, 1
  %call44 = call i32 @_ZN35btSequentialImpulseConstraintSolver10btRandInt2Ei(%class.btSequentialImpulseConstraintSolver* %this5, i32 %add43)
  store i32 %call44, i32* %swapi42, align 4
  %m_orderFrictionConstraintPool45 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %39 = load i32, i32* %swapi42, align 4
  %call46 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool45, i32 %39)
  %40 = load i32, i32* %call46, align 4
  %m_orderFrictionConstraintPool47 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %41 = load i32, i32* %j36, align 4
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool47, i32 %41)
  store i32 %40, i32* %call48, align 4
  %42 = load i32, i32* %tmp40, align 4
  %m_orderFrictionConstraintPool49 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %43 = load i32, i32* %swapi42, align 4
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool49, i32 %43)
  store i32 %42, i32* %call50, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %for.body39
  %44 = load i32, i32* %j36, align 4
  %inc52 = add nsw i32 %44, 1
  store i32 %inc52, i32* %j36, align 4
  br label %for.cond37

for.end53:                                        ; preds = %for.cond37
  br label %if.end

if.end:                                           ; preds = %for.end53, %for.end
  br label %if.end54

if.end54:                                         ; preds = %if.end, %entry
  %45 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %46 = bitcast %struct.btContactSolverInfo* %45 to %struct.btContactSolverInfoData*
  %m_solverMode55 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %46, i32 0, i32 16
  %47 = load i32, i32* %m_solverMode55, align 4
  %and56 = and i32 %47, 256
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.else293

if.then58:                                        ; preds = %if.end54
  store i32 0, i32* %j59, align 4
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc77, %if.then58
  %48 = load i32, i32* %j59, align 4
  %m_tmpSolverNonContactConstraintPool61 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 3
  %call62 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool61)
  %cmp63 = icmp slt i32 %48, %call62
  br i1 %cmp63, label %for.body64, label %for.end79

for.body64:                                       ; preds = %for.cond60
  %m_tmpSolverNonContactConstraintPool65 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 3
  %m_orderNonContactConstraintPool66 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %49 = load i32, i32* %j59, align 4
  %call67 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool66, i32 %49)
  %50 = load i32, i32* %call67, align 4
  %call68 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool65, i32 %50)
  store %struct.btSolverConstraint* %call68, %struct.btSolverConstraint** %constraint, align 4
  %51 = load i32, i32* %iteration.addr, align 4
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_overrideNumSolverIterations = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 16
  %53 = load i32, i32* %m_overrideNumSolverIterations, align 4
  %cmp69 = icmp slt i32 %51, %53
  br i1 %cmp69, label %if.then70, label %if.end76

if.then70:                                        ; preds = %for.body64
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %54 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %54, i32 0, i32 18
  %55 = load i32, i32* %m_solverBodyIdA, align 4
  %call71 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %55)
  %m_tmpSolverBodyPool72 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %56 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %56, i32 0, i32 19
  %57 = load i32, i32* %m_solverBodyIdB, align 4
  %call73 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool72, i32 %57)
  %58 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint, align 4
  %call74 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call71, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call73, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %58)
  store float %call74, float* %residual, align 4
  %59 = load float, float* %residual, align 4
  %60 = load float, float* %residual, align 4
  %mul = fmul float %59, %60
  %61 = load float, float* %leastSquaresResidual, align 4
  %add75 = fadd float %61, %mul
  store float %add75, float* %leastSquaresResidual, align 4
  br label %if.end76

if.end76:                                         ; preds = %if.then70, %for.body64
  br label %for.inc77

for.inc77:                                        ; preds = %if.end76
  %62 = load i32, i32* %j59, align 4
  %inc78 = add nsw i32 %62, 1
  store i32 %inc78, i32* %j59, align 4
  br label %for.cond60

for.end79:                                        ; preds = %for.cond60
  %63 = load i32, i32* %iteration.addr, align 4
  %64 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %65 = bitcast %struct.btContactSolverInfo* %64 to %struct.btContactSolverInfoData*
  %m_numIterations80 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %65, i32 0, i32 5
  %66 = load i32, i32* %m_numIterations80, align 4
  %cmp81 = icmp slt i32 %63, %66
  br i1 %cmp81, label %if.then82, label %if.end292

if.then82:                                        ; preds = %for.end79
  store i32 0, i32* %j83, align 4
  br label %for.cond84

for.cond84:                                       ; preds = %for.inc103, %if.then82
  %67 = load i32, i32* %j83, align 4
  %68 = load i32, i32* %numConstraints.addr, align 4
  %cmp85 = icmp slt i32 %67, %68
  br i1 %cmp85, label %for.body86, label %for.end105

for.body86:                                       ; preds = %for.cond84
  %69 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %70 = load i32, i32* %j83, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %69, i32 %70
  %71 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  %call87 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %71)
  br i1 %call87, label %if.then88, label %if.end102

if.then88:                                        ; preds = %for.body86
  %72 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %73 = load i32, i32* %j83, align 4
  %arrayidx89 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %72, i32 %73
  %74 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx89, align 4
  %call90 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %74)
  %75 = bitcast %class.btRigidBody* %call90 to %class.btCollisionObject*
  %76 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %77 = bitcast %struct.btContactSolverInfo* %76 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %77, i32 0, i32 3
  %78 = load float, float* %m_timeStep, align 4
  %call91 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this5, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %75, float %78)
  store i32 %call91, i32* %bodyAid, align 4
  %79 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %80 = load i32, i32* %j83, align 4
  %arrayidx92 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %79, i32 %80
  %81 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx92, align 4
  %call93 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %81)
  %82 = bitcast %class.btRigidBody* %call93 to %class.btCollisionObject*
  %83 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %84 = bitcast %struct.btContactSolverInfo* %83 to %struct.btContactSolverInfoData*
  %m_timeStep94 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %84, i32 0, i32 3
  %85 = load float, float* %m_timeStep94, align 4
  %call95 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this5, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %82, float %85)
  store i32 %call95, i32* %bodyBid, align 4
  %m_tmpSolverBodyPool96 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %86 = load i32, i32* %bodyAid, align 4
  %call97 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool96, i32 %86)
  store %struct.btSolverBody* %call97, %struct.btSolverBody** %bodyA, align 4
  %m_tmpSolverBodyPool98 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %87 = load i32, i32* %bodyBid, align 4
  %call99 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool98, i32 %87)
  store %struct.btSolverBody* %call99, %struct.btSolverBody** %bodyB, align 4
  %88 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %89 = load i32, i32* %j83, align 4
  %arrayidx100 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %88, i32 %89
  %90 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx100, align 4
  %91 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA, align 4
  %92 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB, align 4
  %93 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %94 = bitcast %struct.btContactSolverInfo* %93 to %struct.btContactSolverInfoData*
  %m_timeStep101 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %94, i32 0, i32 3
  %95 = load float, float* %m_timeStep101, align 4
  %96 = bitcast %class.btTypedConstraint* %90 to void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)***
  %vtable = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)**, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*** %96, align 4
  %vfn = getelementptr inbounds void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vtable, i64 6
  %97 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vfn, align 4
  call void %97(%class.btTypedConstraint* %90, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %91, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %92, float %95)
  br label %if.end102

if.end102:                                        ; preds = %if.then88, %for.body86
  br label %for.inc103

for.inc103:                                       ; preds = %if.end102
  %98 = load i32, i32* %j83, align 4
  %inc104 = add nsw i32 %98, 1
  store i32 %inc104, i32* %j83, align 4
  br label %for.cond84

for.end105:                                       ; preds = %for.cond84
  %99 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %100 = bitcast %struct.btContactSolverInfo* %99 to %struct.btContactSolverInfoData*
  %m_solverMode106 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %100, i32 0, i32 16
  %101 = load i32, i32* %m_solverMode106, align 4
  %and107 = and i32 %101, 512
  %tobool108 = icmp ne i32 %and107, 0
  br i1 %tobool108, label %if.then109, label %if.else

if.then109:                                       ; preds = %for.end105
  %m_tmpSolverContactConstraintPool110 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %call111 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool110)
  store i32 %call111, i32* %numPoolConstraints, align 4
  %102 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %103 = bitcast %struct.btContactSolverInfo* %102 to %struct.btContactSolverInfoData*
  %m_solverMode112 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %103, i32 0, i32 16
  %104 = load i32, i32* %m_solverMode112, align 4
  %and113 = and i32 %104, 16
  %tobool114 = icmp ne i32 %and113, 0
  %105 = zext i1 %tobool114 to i64
  %cond = select i1 %tobool114, i32 2, i32 1
  store i32 %cond, i32* %multiplier, align 4
  store i32 0, i32* %c, align 4
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc189, %if.then109
  %106 = load i32, i32* %c, align 4
  %107 = load i32, i32* %numPoolConstraints, align 4
  %cmp116 = icmp slt i32 %106, %107
  br i1 %cmp116, label %for.body117, label %for.end191

for.body117:                                      ; preds = %for.cond115
  store float 0.000000e+00, float* %totalImpulse, align 4
  %m_tmpSolverContactConstraintPool118 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %m_orderTmpConstraintPool119 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %108 = load i32, i32* %c, align 4
  %call120 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool119, i32 %108)
  %109 = load i32, i32* %call120, align 4
  %call121 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool118, i32 %109)
  store %struct.btSolverConstraint* %call121, %struct.btSolverConstraint** %solveManifold, align 4
  %m_tmpSolverBodyPool123 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %110 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdA124 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %110, i32 0, i32 18
  %111 = load i32, i32* %m_solverBodyIdA124, align 4
  %call125 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool123, i32 %111)
  %m_tmpSolverBodyPool126 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %112 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdB127 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %112, i32 0, i32 19
  %113 = load i32, i32* %m_solverBodyIdB127, align 4
  %call128 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool126, i32 %113)
  %114 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %call129 = call float @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call125, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call128, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %114)
  store float %call129, float* %residual122, align 4
  %115 = load float, float* %residual122, align 4
  %116 = load float, float* %residual122, align 4
  %mul130 = fmul float %115, %116
  %117 = load float, float* %leastSquaresResidual, align 4
  %add131 = fadd float %117, %mul130
  store float %add131, float* %leastSquaresResidual, align 4
  %118 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %118, i32 0, i32 7
  %119 = load float, float* %m_appliedImpulse, align 4
  store float %119, float* %totalImpulse, align 4
  store i8 1, i8* %applyFriction, align 1
  %120 = load i8, i8* %applyFriction, align 1
  %tobool132 = trunc i8 %120 to i1
  br i1 %tobool132, label %if.then133, label %if.end188

if.then133:                                       ; preds = %for.body117
  %m_tmpSolverContactFrictionConstraintPool135 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %m_orderFrictionConstraintPool136 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %121 = load i32, i32* %c, align 4
  %122 = load i32, i32* %multiplier, align 4
  %mul137 = mul nsw i32 %121, %122
  %call138 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool136, i32 %mul137)
  %123 = load i32, i32* %call138, align 4
  %call139 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool135, i32 %123)
  store %struct.btSolverConstraint* %call139, %struct.btSolverConstraint** %solveManifold134, align 4
  %124 = load float, float* %totalImpulse, align 4
  %cmp140 = fcmp ogt float %124, 0.000000e+00
  br i1 %cmp140, label %if.then141, label %if.end155

if.then141:                                       ; preds = %if.then133
  %125 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_friction = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %125, i32 0, i32 8
  %126 = load float, float* %m_friction, align 4
  %127 = load float, float* %totalImpulse, align 4
  %mul142 = fmul float %126, %127
  %fneg = fneg float %mul142
  %128 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_lowerLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %128, i32 0, i32 12
  store float %fneg, float* %m_lowerLimit, align 4
  %129 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_friction143 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %129, i32 0, i32 8
  %130 = load float, float* %m_friction143, align 4
  %131 = load float, float* %totalImpulse, align 4
  %mul144 = fmul float %130, %131
  %132 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_upperLimit = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %132, i32 0, i32 13
  store float %mul144, float* %m_upperLimit, align 4
  %m_tmpSolverBodyPool146 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %133 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_solverBodyIdA147 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %133, i32 0, i32 18
  %134 = load i32, i32* %m_solverBodyIdA147, align 4
  %call148 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool146, i32 %134)
  %m_tmpSolverBodyPool149 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %135 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %m_solverBodyIdB150 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %135, i32 0, i32 19
  %136 = load i32, i32* %m_solverBodyIdB150, align 4
  %call151 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool149, i32 %136)
  %137 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold134, align 4
  %call152 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call148, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call151, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %137)
  store float %call152, float* %residual145, align 4
  %138 = load float, float* %residual145, align 4
  %139 = load float, float* %residual145, align 4
  %mul153 = fmul float %138, %139
  %140 = load float, float* %leastSquaresResidual, align 4
  %add154 = fadd float %140, %mul153
  store float %add154, float* %leastSquaresResidual, align 4
  br label %if.end155

if.end155:                                        ; preds = %if.then141, %if.then133
  %141 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %142 = bitcast %struct.btContactSolverInfo* %141 to %struct.btContactSolverInfoData*
  %m_solverMode156 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %142, i32 0, i32 16
  %143 = load i32, i32* %m_solverMode156, align 4
  %and157 = and i32 %143, 16
  %tobool158 = icmp ne i32 %and157, 0
  br i1 %tobool158, label %if.then159, label %if.end187

if.then159:                                       ; preds = %if.end155
  %m_tmpSolverContactFrictionConstraintPool161 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %m_orderFrictionConstraintPool162 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %144 = load i32, i32* %c, align 4
  %145 = load i32, i32* %multiplier, align 4
  %mul163 = mul nsw i32 %144, %145
  %add164 = add nsw i32 %mul163, 1
  %call165 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool162, i32 %add164)
  %146 = load i32, i32* %call165, align 4
  %call166 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool161, i32 %146)
  store %struct.btSolverConstraint* %call166, %struct.btSolverConstraint** %solveManifold160, align 4
  %147 = load float, float* %totalImpulse, align 4
  %cmp167 = fcmp ogt float %147, 0.000000e+00
  br i1 %cmp167, label %if.then168, label %if.end186

if.then168:                                       ; preds = %if.then159
  %148 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_friction169 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %148, i32 0, i32 8
  %149 = load float, float* %m_friction169, align 4
  %150 = load float, float* %totalImpulse, align 4
  %mul170 = fmul float %149, %150
  %fneg171 = fneg float %mul170
  %151 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_lowerLimit172 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %151, i32 0, i32 12
  store float %fneg171, float* %m_lowerLimit172, align 4
  %152 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_friction173 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %152, i32 0, i32 8
  %153 = load float, float* %m_friction173, align 4
  %154 = load float, float* %totalImpulse, align 4
  %mul174 = fmul float %153, %154
  %155 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_upperLimit175 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %155, i32 0, i32 13
  store float %mul174, float* %m_upperLimit175, align 4
  %m_tmpSolverBodyPool177 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %156 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_solverBodyIdA178 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %156, i32 0, i32 18
  %157 = load i32, i32* %m_solverBodyIdA178, align 4
  %call179 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool177, i32 %157)
  %m_tmpSolverBodyPool180 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %158 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %m_solverBodyIdB181 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %158, i32 0, i32 19
  %159 = load i32, i32* %m_solverBodyIdB181, align 4
  %call182 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool180, i32 %159)
  %160 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold160, align 4
  %call183 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call179, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call182, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %160)
  store float %call183, float* %residual176, align 4
  %161 = load float, float* %residual176, align 4
  %162 = load float, float* %residual176, align 4
  %mul184 = fmul float %161, %162
  %163 = load float, float* %leastSquaresResidual, align 4
  %add185 = fadd float %163, %mul184
  store float %add185, float* %leastSquaresResidual, align 4
  br label %if.end186

if.end186:                                        ; preds = %if.then168, %if.then159
  br label %if.end187

if.end187:                                        ; preds = %if.end186, %if.end155
  br label %if.end188

if.end188:                                        ; preds = %if.end187, %for.body117
  br label %for.inc189

for.inc189:                                       ; preds = %if.end188
  %164 = load i32, i32* %c, align 4
  %inc190 = add nsw i32 %164, 1
  store i32 %inc190, i32* %c, align 4
  br label %for.cond115

for.end191:                                       ; preds = %for.cond115
  br label %if.end291

if.else:                                          ; preds = %for.end105
  %m_tmpSolverContactConstraintPool193 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %call194 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool193)
  store i32 %call194, i32* %numPoolConstraints192, align 4
  store i32 0, i32* %j195, align 4
  br label %for.cond196

for.cond196:                                      ; preds = %for.inc214, %if.else
  %165 = load i32, i32* %j195, align 4
  %166 = load i32, i32* %numPoolConstraints192, align 4
  %cmp197 = icmp slt i32 %165, %166
  br i1 %cmp197, label %for.body198, label %for.end216

for.body198:                                      ; preds = %for.cond196
  %m_tmpSolverContactConstraintPool200 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %m_orderTmpConstraintPool201 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %167 = load i32, i32* %j195, align 4
  %call202 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool201, i32 %167)
  %168 = load i32, i32* %call202, align 4
  %call203 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool200, i32 %168)
  store %struct.btSolverConstraint* %call203, %struct.btSolverConstraint** %solveManifold199, align 4
  %m_tmpSolverBodyPool205 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %169 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold199, align 4
  %m_solverBodyIdA206 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %169, i32 0, i32 18
  %170 = load i32, i32* %m_solverBodyIdA206, align 4
  %call207 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool205, i32 %170)
  %m_tmpSolverBodyPool208 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %171 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold199, align 4
  %m_solverBodyIdB209 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %171, i32 0, i32 19
  %172 = load i32, i32* %m_solverBodyIdB209, align 4
  %call210 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool208, i32 %172)
  %173 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold199, align 4
  %call211 = call float @_ZN35btSequentialImpulseConstraintSolver40resolveSingleConstraintRowLowerLimitSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call207, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call210, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %173)
  store float %call211, float* %residual204, align 4
  %174 = load float, float* %residual204, align 4
  %175 = load float, float* %residual204, align 4
  %mul212 = fmul float %174, %175
  %176 = load float, float* %leastSquaresResidual, align 4
  %add213 = fadd float %176, %mul212
  store float %add213, float* %leastSquaresResidual, align 4
  br label %for.inc214

for.inc214:                                       ; preds = %for.body198
  %177 = load i32, i32* %j195, align 4
  %inc215 = add nsw i32 %177, 1
  store i32 %inc215, i32* %j195, align 4
  br label %for.cond196

for.end216:                                       ; preds = %for.cond196
  %m_tmpSolverContactFrictionConstraintPool217 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %call218 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool217)
  store i32 %call218, i32* %numFrictionPoolConstraints, align 4
  store i32 0, i32* %j195, align 4
  br label %for.cond219

for.cond219:                                      ; preds = %for.inc251, %for.end216
  %178 = load i32, i32* %j195, align 4
  %179 = load i32, i32* %numFrictionPoolConstraints, align 4
  %cmp220 = icmp slt i32 %178, %179
  br i1 %cmp220, label %for.body221, label %for.end253

for.body221:                                      ; preds = %for.cond219
  %m_tmpSolverContactFrictionConstraintPool223 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %m_orderFrictionConstraintPool224 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %180 = load i32, i32* %j195, align 4
  %call225 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool224, i32 %180)
  %181 = load i32, i32* %call225, align 4
  %call226 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool223, i32 %181)
  store %struct.btSolverConstraint* %call226, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_tmpSolverContactConstraintPool228 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %182 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %182, i32 0, i32 17
  %183 = load i32, i32* %m_frictionIndex, align 4
  %call229 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool228, i32 %183)
  %m_appliedImpulse230 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call229, i32 0, i32 7
  %184 = load float, float* %m_appliedImpulse230, align 4
  store float %184, float* %totalImpulse227, align 4
  %185 = load float, float* %totalImpulse227, align 4
  %cmp231 = fcmp ogt float %185, 0.000000e+00
  br i1 %cmp231, label %if.then232, label %if.end250

if.then232:                                       ; preds = %for.body221
  %186 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_friction233 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %186, i32 0, i32 8
  %187 = load float, float* %m_friction233, align 4
  %188 = load float, float* %totalImpulse227, align 4
  %mul234 = fmul float %187, %188
  %fneg235 = fneg float %mul234
  %189 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_lowerLimit236 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %189, i32 0, i32 12
  store float %fneg235, float* %m_lowerLimit236, align 4
  %190 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_friction237 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %190, i32 0, i32 8
  %191 = load float, float* %m_friction237, align 4
  %192 = load float, float* %totalImpulse227, align 4
  %mul238 = fmul float %191, %192
  %193 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_upperLimit239 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %193, i32 0, i32 13
  store float %mul238, float* %m_upperLimit239, align 4
  %m_tmpSolverBodyPool241 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %194 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_solverBodyIdA242 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %194, i32 0, i32 18
  %195 = load i32, i32* %m_solverBodyIdA242, align 4
  %call243 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool241, i32 %195)
  %m_tmpSolverBodyPool244 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %196 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %m_solverBodyIdB245 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %196, i32 0, i32 19
  %197 = load i32, i32* %m_solverBodyIdB245, align 4
  %call246 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool244, i32 %197)
  %198 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold222, align 4
  %call247 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call243, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call246, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %198)
  store float %call247, float* %residual240, align 4
  %199 = load float, float* %residual240, align 4
  %200 = load float, float* %residual240, align 4
  %mul248 = fmul float %199, %200
  %201 = load float, float* %leastSquaresResidual, align 4
  %add249 = fadd float %201, %mul248
  store float %add249, float* %leastSquaresResidual, align 4
  br label %if.end250

if.end250:                                        ; preds = %if.then232, %for.body221
  br label %for.inc251

for.inc251:                                       ; preds = %if.end250
  %202 = load i32, i32* %j195, align 4
  %inc252 = add nsw i32 %202, 1
  store i32 %inc252, i32* %j195, align 4
  br label %for.cond219

for.end253:                                       ; preds = %for.cond219
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 5
  %call254 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool)
  store i32 %call254, i32* %numRollingFrictionPoolConstraints, align 4
  store i32 0, i32* %j195, align 4
  br label %for.cond255

for.cond255:                                      ; preds = %for.inc288, %for.end253
  %203 = load i32, i32* %j195, align 4
  %204 = load i32, i32* %numRollingFrictionPoolConstraints, align 4
  %cmp256 = icmp slt i32 %203, %204
  br i1 %cmp256, label %for.body257, label %for.end290

for.body257:                                      ; preds = %for.cond255
  %m_tmpSolverContactRollingFrictionConstraintPool258 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 5
  %205 = load i32, i32* %j195, align 4
  %call259 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool258, i32 %205)
  store %struct.btSolverConstraint* %call259, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_tmpSolverContactConstraintPool261 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %206 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_frictionIndex262 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %206, i32 0, i32 17
  %207 = load i32, i32* %m_frictionIndex262, align 4
  %call263 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool261, i32 %207)
  %m_appliedImpulse264 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call263, i32 0, i32 7
  %208 = load float, float* %m_appliedImpulse264, align 4
  store float %208, float* %totalImpulse260, align 4
  %209 = load float, float* %totalImpulse260, align 4
  %cmp265 = fcmp ogt float %209, 0.000000e+00
  br i1 %cmp265, label %if.then266, label %if.end287

if.then266:                                       ; preds = %for.body257
  %210 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction267 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %210, i32 0, i32 8
  %211 = load float, float* %m_friction267, align 4
  %212 = load float, float* %totalImpulse260, align 4
  %mul268 = fmul float %211, %212
  store float %mul268, float* %rollingFrictionMagnitude, align 4
  %213 = load float, float* %rollingFrictionMagnitude, align 4
  %214 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction269 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %214, i32 0, i32 8
  %215 = load float, float* %m_friction269, align 4
  %cmp270 = fcmp ogt float %213, %215
  br i1 %cmp270, label %if.then271, label %if.end273

if.then271:                                       ; preds = %if.then266
  %216 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_friction272 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %216, i32 0, i32 8
  %217 = load float, float* %m_friction272, align 4
  store float %217, float* %rollingFrictionMagnitude, align 4
  br label %if.end273

if.end273:                                        ; preds = %if.then271, %if.then266
  %218 = load float, float* %rollingFrictionMagnitude, align 4
  %fneg274 = fneg float %218
  %219 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_lowerLimit275 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %219, i32 0, i32 12
  store float %fneg274, float* %m_lowerLimit275, align 4
  %220 = load float, float* %rollingFrictionMagnitude, align 4
  %221 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_upperLimit276 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %221, i32 0, i32 13
  store float %220, float* %m_upperLimit276, align 4
  %m_tmpSolverBodyPool278 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %222 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_solverBodyIdA279 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %222, i32 0, i32 18
  %223 = load i32, i32* %m_solverBodyIdA279, align 4
  %call280 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool278, i32 %223)
  %m_tmpSolverBodyPool281 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %224 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %m_solverBodyIdB282 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %224, i32 0, i32 19
  %225 = load i32, i32* %m_solverBodyIdB282, align 4
  %call283 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool281, i32 %225)
  %226 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint, align 4
  %call284 = call float @_ZN35btSequentialImpulseConstraintSolver37resolveSingleConstraintRowGenericSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call280, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call283, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %226)
  store float %call284, float* %residual277, align 4
  %227 = load float, float* %residual277, align 4
  %228 = load float, float* %residual277, align 4
  %mul285 = fmul float %227, %228
  %229 = load float, float* %leastSquaresResidual, align 4
  %add286 = fadd float %229, %mul285
  store float %add286, float* %leastSquaresResidual, align 4
  br label %if.end287

if.end287:                                        ; preds = %if.end273, %for.body257
  br label %for.inc288

for.inc288:                                       ; preds = %if.end287
  %230 = load i32, i32* %j195, align 4
  %inc289 = add nsw i32 %230, 1
  store i32 %inc289, i32* %j195, align 4
  br label %for.cond255

for.end290:                                       ; preds = %for.cond255
  br label %if.end291

if.end291:                                        ; preds = %for.end290, %for.end191
  br label %if.end292

if.end292:                                        ; preds = %if.end291, %for.end79
  br label %if.end464

if.else293:                                       ; preds = %if.end54
  store i32 0, i32* %j294, align 4
  br label %for.cond295

for.cond295:                                      ; preds = %for.inc319, %if.else293
  %231 = load i32, i32* %j294, align 4
  %m_tmpSolverNonContactConstraintPool296 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 3
  %call297 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool296)
  %cmp298 = icmp slt i32 %231, %call297
  br i1 %cmp298, label %for.body299, label %for.end321

for.body299:                                      ; preds = %for.cond295
  %m_tmpSolverNonContactConstraintPool301 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 3
  %m_orderNonContactConstraintPool302 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 7
  %232 = load i32, i32* %j294, align 4
  %call303 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderNonContactConstraintPool302, i32 %232)
  %233 = load i32, i32* %call303, align 4
  %call304 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool301, i32 %233)
  store %struct.btSolverConstraint* %call304, %struct.btSolverConstraint** %constraint300, align 4
  %234 = load i32, i32* %iteration.addr, align 4
  %235 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint300, align 4
  %m_overrideNumSolverIterations305 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %235, i32 0, i32 16
  %236 = load i32, i32* %m_overrideNumSolverIterations305, align 4
  %cmp306 = icmp slt i32 %234, %236
  br i1 %cmp306, label %if.then307, label %if.end318

if.then307:                                       ; preds = %for.body299
  %m_tmpSolverBodyPool309 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %237 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint300, align 4
  %m_solverBodyIdA310 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %237, i32 0, i32 18
  %238 = load i32, i32* %m_solverBodyIdA310, align 4
  %call311 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool309, i32 %238)
  %m_tmpSolverBodyPool312 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %239 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint300, align 4
  %m_solverBodyIdB313 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %239, i32 0, i32 19
  %240 = load i32, i32* %m_solverBodyIdB313, align 4
  %call314 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool312, i32 %240)
  %241 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %constraint300, align 4
  %call315 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call311, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call314, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %241)
  store float %call315, float* %residual308, align 4
  %242 = load float, float* %residual308, align 4
  %243 = load float, float* %residual308, align 4
  %mul316 = fmul float %242, %243
  %244 = load float, float* %leastSquaresResidual, align 4
  %add317 = fadd float %244, %mul316
  store float %add317, float* %leastSquaresResidual, align 4
  br label %if.end318

if.end318:                                        ; preds = %if.then307, %for.body299
  br label %for.inc319

for.inc319:                                       ; preds = %if.end318
  %245 = load i32, i32* %j294, align 4
  %inc320 = add nsw i32 %245, 1
  store i32 %inc320, i32* %j294, align 4
  br label %for.cond295

for.end321:                                       ; preds = %for.cond295
  %246 = load i32, i32* %iteration.addr, align 4
  %247 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %248 = bitcast %struct.btContactSolverInfo* %247 to %struct.btContactSolverInfoData*
  %m_numIterations322 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %248, i32 0, i32 5
  %249 = load i32, i32* %m_numIterations322, align 4
  %cmp323 = icmp slt i32 %246, %249
  br i1 %cmp323, label %if.then324, label %if.end463

if.then324:                                       ; preds = %for.end321
  store i32 0, i32* %j325, align 4
  br label %for.cond326

for.cond326:                                      ; preds = %for.inc353, %if.then324
  %250 = load i32, i32* %j325, align 4
  %251 = load i32, i32* %numConstraints.addr, align 4
  %cmp327 = icmp slt i32 %250, %251
  br i1 %cmp327, label %for.body328, label %for.end355

for.body328:                                      ; preds = %for.cond326
  %252 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %253 = load i32, i32* %j325, align 4
  %arrayidx329 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %252, i32 %253
  %254 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx329, align 4
  %call330 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %254)
  br i1 %call330, label %if.then331, label %if.end352

if.then331:                                       ; preds = %for.body328
  %255 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %256 = load i32, i32* %j325, align 4
  %arrayidx333 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %255, i32 %256
  %257 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx333, align 4
  %call334 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %257)
  %258 = bitcast %class.btRigidBody* %call334 to %class.btCollisionObject*
  %259 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %260 = bitcast %struct.btContactSolverInfo* %259 to %struct.btContactSolverInfoData*
  %m_timeStep335 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %260, i32 0, i32 3
  %261 = load float, float* %m_timeStep335, align 4
  %call336 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this5, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %258, float %261)
  store i32 %call336, i32* %bodyAid332, align 4
  %262 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %263 = load i32, i32* %j325, align 4
  %arrayidx338 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %262, i32 %263
  %264 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx338, align 4
  %call339 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %264)
  %265 = bitcast %class.btRigidBody* %call339 to %class.btCollisionObject*
  %266 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %267 = bitcast %struct.btContactSolverInfo* %266 to %struct.btContactSolverInfoData*
  %m_timeStep340 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %267, i32 0, i32 3
  %268 = load float, float* %m_timeStep340, align 4
  %call341 = call i32 @_ZN35btSequentialImpulseConstraintSolver19getOrInitSolverBodyER17btCollisionObjectf(%class.btSequentialImpulseConstraintSolver* %this5, %class.btCollisionObject* nonnull align 4 dereferenceable(324) %265, float %268)
  store i32 %call341, i32* %bodyBid337, align 4
  %m_tmpSolverBodyPool343 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %269 = load i32, i32* %bodyAid332, align 4
  %call344 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool343, i32 %269)
  store %struct.btSolverBody* %call344, %struct.btSolverBody** %bodyA342, align 4
  %m_tmpSolverBodyPool346 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %270 = load i32, i32* %bodyBid337, align 4
  %call347 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool346, i32 %270)
  store %struct.btSolverBody* %call347, %struct.btSolverBody** %bodyB345, align 4
  %271 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %272 = load i32, i32* %j325, align 4
  %arrayidx348 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %271, i32 %272
  %273 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx348, align 4
  %274 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyA342, align 4
  %275 = load %struct.btSolverBody*, %struct.btSolverBody** %bodyB345, align 4
  %276 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %277 = bitcast %struct.btContactSolverInfo* %276 to %struct.btContactSolverInfoData*
  %m_timeStep349 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %277, i32 0, i32 3
  %278 = load float, float* %m_timeStep349, align 4
  %279 = bitcast %class.btTypedConstraint* %273 to void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)***
  %vtable350 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)**, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*** %279, align 4
  %vfn351 = getelementptr inbounds void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vtable350, i64 6
  %280 = load void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)*, void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)** %vfn351, align 4
  call void %280(%class.btTypedConstraint* %273, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %274, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %275, float %278)
  br label %if.end352

if.end352:                                        ; preds = %if.then331, %for.body328
  br label %for.inc353

for.inc353:                                       ; preds = %if.end352
  %281 = load i32, i32* %j325, align 4
  %inc354 = add nsw i32 %281, 1
  store i32 %inc354, i32* %j325, align 4
  br label %for.cond326

for.end355:                                       ; preds = %for.cond326
  %m_tmpSolverContactConstraintPool357 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %call358 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool357)
  store i32 %call358, i32* %numPoolConstraints356, align 4
  store i32 0, i32* %j359, align 4
  br label %for.cond360

for.cond360:                                      ; preds = %for.inc378, %for.end355
  %282 = load i32, i32* %j359, align 4
  %283 = load i32, i32* %numPoolConstraints356, align 4
  %cmp361 = icmp slt i32 %282, %283
  br i1 %cmp361, label %for.body362, label %for.end380

for.body362:                                      ; preds = %for.cond360
  %m_tmpSolverContactConstraintPool364 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %m_orderTmpConstraintPool365 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 6
  %284 = load i32, i32* %j359, align 4
  %call366 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool365, i32 %284)
  %285 = load i32, i32* %call366, align 4
  %call367 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool364, i32 %285)
  store %struct.btSolverConstraint* %call367, %struct.btSolverConstraint** %solveManifold363, align 4
  %m_tmpSolverBodyPool369 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %286 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold363, align 4
  %m_solverBodyIdA370 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %286, i32 0, i32 18
  %287 = load i32, i32* %m_solverBodyIdA370, align 4
  %call371 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool369, i32 %287)
  %m_tmpSolverBodyPool372 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %288 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold363, align 4
  %m_solverBodyIdB373 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %288, i32 0, i32 19
  %289 = load i32, i32* %m_solverBodyIdB373, align 4
  %call374 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool372, i32 %289)
  %290 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold363, align 4
  %call375 = call float @_ZN35btSequentialImpulseConstraintSolver36resolveSingleConstraintRowLowerLimitER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call371, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call374, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %290)
  store float %call375, float* %residual368, align 4
  %291 = load float, float* %residual368, align 4
  %292 = load float, float* %residual368, align 4
  %mul376 = fmul float %291, %292
  %293 = load float, float* %leastSquaresResidual, align 4
  %add377 = fadd float %293, %mul376
  store float %add377, float* %leastSquaresResidual, align 4
  br label %for.inc378

for.inc378:                                       ; preds = %for.body362
  %294 = load i32, i32* %j359, align 4
  %inc379 = add nsw i32 %294, 1
  store i32 %inc379, i32* %j359, align 4
  br label %for.cond360

for.end380:                                       ; preds = %for.cond360
  %m_tmpSolverContactFrictionConstraintPool382 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %call383 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool382)
  store i32 %call383, i32* %numFrictionPoolConstraints381, align 4
  store i32 0, i32* %j384, align 4
  br label %for.cond385

for.cond385:                                      ; preds = %for.inc418, %for.end380
  %295 = load i32, i32* %j384, align 4
  %296 = load i32, i32* %numFrictionPoolConstraints381, align 4
  %cmp386 = icmp slt i32 %295, %296
  br i1 %cmp386, label %for.body387, label %for.end420

for.body387:                                      ; preds = %for.cond385
  %m_tmpSolverContactFrictionConstraintPool389 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 4
  %m_orderFrictionConstraintPool390 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 8
  %297 = load i32, i32* %j384, align 4
  %call391 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderFrictionConstraintPool390, i32 %297)
  %298 = load i32, i32* %call391, align 4
  %call392 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool389, i32 %298)
  store %struct.btSolverConstraint* %call392, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_tmpSolverContactConstraintPool394 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %299 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_frictionIndex395 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %299, i32 0, i32 17
  %300 = load i32, i32* %m_frictionIndex395, align 4
  %call396 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool394, i32 %300)
  %m_appliedImpulse397 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call396, i32 0, i32 7
  %301 = load float, float* %m_appliedImpulse397, align 4
  store float %301, float* %totalImpulse393, align 4
  %302 = load float, float* %totalImpulse393, align 4
  %cmp398 = fcmp ogt float %302, 0.000000e+00
  br i1 %cmp398, label %if.then399, label %if.end417

if.then399:                                       ; preds = %for.body387
  %303 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_friction400 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %303, i32 0, i32 8
  %304 = load float, float* %m_friction400, align 4
  %305 = load float, float* %totalImpulse393, align 4
  %mul401 = fmul float %304, %305
  %fneg402 = fneg float %mul401
  %306 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_lowerLimit403 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %306, i32 0, i32 12
  store float %fneg402, float* %m_lowerLimit403, align 4
  %307 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_friction404 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %307, i32 0, i32 8
  %308 = load float, float* %m_friction404, align 4
  %309 = load float, float* %totalImpulse393, align 4
  %mul405 = fmul float %308, %309
  %310 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_upperLimit406 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %310, i32 0, i32 13
  store float %mul405, float* %m_upperLimit406, align 4
  %m_tmpSolverBodyPool408 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %311 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_solverBodyIdA409 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %311, i32 0, i32 18
  %312 = load i32, i32* %m_solverBodyIdA409, align 4
  %call410 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool408, i32 %312)
  %m_tmpSolverBodyPool411 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %313 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %m_solverBodyIdB412 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %313, i32 0, i32 19
  %314 = load i32, i32* %m_solverBodyIdB412, align 4
  %call413 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool411, i32 %314)
  %315 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold388, align 4
  %call414 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call410, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call413, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %315)
  store float %call414, float* %residual407, align 4
  %316 = load float, float* %residual407, align 4
  %317 = load float, float* %residual407, align 4
  %mul415 = fmul float %316, %317
  %318 = load float, float* %leastSquaresResidual, align 4
  %add416 = fadd float %318, %mul415
  store float %add416, float* %leastSquaresResidual, align 4
  br label %if.end417

if.end417:                                        ; preds = %if.then399, %for.body387
  br label %for.inc418

for.inc418:                                       ; preds = %if.end417
  %319 = load i32, i32* %j384, align 4
  %inc419 = add nsw i32 %319, 1
  store i32 %inc419, i32* %j384, align 4
  br label %for.cond385

for.end420:                                       ; preds = %for.cond385
  %m_tmpSolverContactRollingFrictionConstraintPool422 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 5
  %call423 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool422)
  store i32 %call423, i32* %numRollingFrictionPoolConstraints421, align 4
  store i32 0, i32* %j424, align 4
  br label %for.cond425

for.cond425:                                      ; preds = %for.inc460, %for.end420
  %320 = load i32, i32* %j424, align 4
  %321 = load i32, i32* %numRollingFrictionPoolConstraints421, align 4
  %cmp426 = icmp slt i32 %320, %321
  br i1 %cmp426, label %for.body427, label %for.end462

for.body427:                                      ; preds = %for.cond425
  %m_tmpSolverContactRollingFrictionConstraintPool429 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 5
  %322 = load i32, i32* %j424, align 4
  %call430 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool429, i32 %322)
  store %struct.btSolverConstraint* %call430, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_tmpSolverContactConstraintPool432 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 2
  %323 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_frictionIndex433 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %323, i32 0, i32 17
  %324 = load i32, i32* %m_frictionIndex433, align 4
  %call434 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool432, i32 %324)
  %m_appliedImpulse435 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call434, i32 0, i32 7
  %325 = load float, float* %m_appliedImpulse435, align 4
  store float %325, float* %totalImpulse431, align 4
  %326 = load float, float* %totalImpulse431, align 4
  %cmp436 = fcmp ogt float %326, 0.000000e+00
  br i1 %cmp436, label %if.then437, label %if.end459

if.then437:                                       ; preds = %for.body427
  %327 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_friction439 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %327, i32 0, i32 8
  %328 = load float, float* %m_friction439, align 4
  %329 = load float, float* %totalImpulse431, align 4
  %mul440 = fmul float %328, %329
  store float %mul440, float* %rollingFrictionMagnitude438, align 4
  %330 = load float, float* %rollingFrictionMagnitude438, align 4
  %331 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_friction441 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %331, i32 0, i32 8
  %332 = load float, float* %m_friction441, align 4
  %cmp442 = fcmp ogt float %330, %332
  br i1 %cmp442, label %if.then443, label %if.end445

if.then443:                                       ; preds = %if.then437
  %333 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_friction444 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %333, i32 0, i32 8
  %334 = load float, float* %m_friction444, align 4
  store float %334, float* %rollingFrictionMagnitude438, align 4
  br label %if.end445

if.end445:                                        ; preds = %if.then443, %if.then437
  %335 = load float, float* %rollingFrictionMagnitude438, align 4
  %fneg446 = fneg float %335
  %336 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_lowerLimit447 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %336, i32 0, i32 12
  store float %fneg446, float* %m_lowerLimit447, align 4
  %337 = load float, float* %rollingFrictionMagnitude438, align 4
  %338 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_upperLimit448 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %338, i32 0, i32 13
  store float %337, float* %m_upperLimit448, align 4
  %m_tmpSolverBodyPool450 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %339 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_solverBodyIdA451 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %339, i32 0, i32 18
  %340 = load i32, i32* %m_solverBodyIdA451, align 4
  %call452 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool450, i32 %340)
  %m_tmpSolverBodyPool453 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this5, i32 0, i32 1
  %341 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %m_solverBodyIdB454 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %341, i32 0, i32 19
  %342 = load i32, i32* %m_solverBodyIdB454, align 4
  %call455 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool453, i32 %342)
  %343 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %rollingFrictionConstraint428, align 4
  %call456 = call float @_ZN35btSequentialImpulseConstraintSolver33resolveSingleConstraintRowGenericER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this5, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call452, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call455, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %343)
  store float %call456, float* %residual449, align 4
  %344 = load float, float* %residual449, align 4
  %345 = load float, float* %residual449, align 4
  %mul457 = fmul float %344, %345
  %346 = load float, float* %leastSquaresResidual, align 4
  %add458 = fadd float %346, %mul457
  store float %add458, float* %leastSquaresResidual, align 4
  br label %if.end459

if.end459:                                        ; preds = %if.end445, %for.body427
  br label %for.inc460

for.inc460:                                       ; preds = %if.end459
  %347 = load i32, i32* %j424, align 4
  %inc461 = add nsw i32 %347, 1
  store i32 %inc461, i32* %j424, align 4
  br label %for.cond425

for.end462:                                       ; preds = %for.cond425
  br label %if.end463

if.end463:                                        ; preds = %for.end462, %for.end321
  br label %if.end464

if.end464:                                        ; preds = %if.end463, %if.end292
  %348 = load float, float* %leastSquaresResidual, align 4
  ret float %348
}

; Function Attrs: noinline optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver45solveGroupCacheFriendlySplitImpulseIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %iteration = alloca i32, align 4
  %leastSquaresResidual = alloca float, align 4
  %numPoolConstraints = alloca i32, align 4
  %j = alloca i32, align 4
  %solveManifold = alloca %struct.btSolverConstraint*, align 4
  %residual = alloca float, align 4
  %leastSquaresResidual25 = alloca float, align 4
  %numPoolConstraints26 = alloca i32, align 4
  %j29 = alloca i32, align 4
  %solveManifold33 = alloca %struct.btSolverConstraint*, align 4
  %residual38 = alloca float, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %1 = bitcast %struct.btContactSolverInfo* %0 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 11
  %2 = load i32, i32* %m_splitImpulse, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.end63

if.then:                                          ; preds = %entry
  %3 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %4 = bitcast %struct.btContactSolverInfo* %3 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %4, i32 0, i32 16
  %5 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %5, 256
  %tobool2 = icmp ne i32 %and, 0
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %iteration, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc18, %if.then3
  %6 = load i32, i32* %iteration, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %8 = bitcast %struct.btContactSolverInfo* %7 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %8, i32 0, i32 5
  %9 = load i32, i32* %m_numIterations, align 4
  %cmp = icmp slt i32 %6, %9
  br i1 %cmp, label %for.body, label %for.end20

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %leastSquaresResidual, align 4
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call, i32* %numPoolConstraints, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %j, align 4
  %11 = load i32, i32* %numPoolConstraints, align 4
  %cmp5 = icmp slt i32 %10, %11
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %m_tmpSolverContactConstraintPool7 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %m_orderTmpConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %12 = load i32, i32* %j, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool, i32 %12)
  %13 = load i32, i32* %call8, align 4
  %call9 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool7, i32 %13)
  store %struct.btSolverConstraint* %call9, %struct.btSolverConstraint** %solveManifold, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %14 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %14, i32 0, i32 18
  %15 = load i32, i32* %m_solverBodyIdA, align 4
  %call10 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool, i32 %15)
  %m_tmpSolverBodyPool11 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %16 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %16, i32 0, i32 19
  %17 = load i32, i32* %m_solverBodyIdB, align 4
  %call12 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool11, i32 %17)
  %18 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %call13 = call float @_ZN35btSequentialImpulseConstraintSolver27resolveSplitPenetrationSIMDER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call10, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call12, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %18)
  store float %call13, float* %residual, align 4
  %19 = load float, float* %residual, align 4
  %20 = load float, float* %residual, align 4
  %mul = fmul float %19, %20
  %21 = load float, float* %leastSquaresResidual, align 4
  %add = fadd float %21, %mul
  store float %add, float* %leastSquaresResidual, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %22 = load i32, i32* %j, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %23 = load float, float* %leastSquaresResidual, align 4
  %24 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %25 = bitcast %struct.btContactSolverInfo* %24 to %struct.btContactSolverInfoData*
  %m_leastSquaresResidualThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %25, i32 0, i32 21
  %26 = load float, float* %m_leastSquaresResidualThreshold, align 4
  %cmp14 = fcmp ole float %23, %26
  br i1 %cmp14, label %if.then17, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end
  %27 = load i32, i32* %iteration, align 4
  %28 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %29 = bitcast %struct.btContactSolverInfo* %28 to %struct.btContactSolverInfoData*
  %m_numIterations15 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %29, i32 0, i32 5
  %30 = load i32, i32* %m_numIterations15, align 4
  %sub = sub nsw i32 %30, 1
  %cmp16 = icmp sge i32 %27, %sub
  br i1 %cmp16, label %if.then17, label %if.end

if.then17:                                        ; preds = %lor.lhs.false, %for.end
  br label %for.end20

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc18

for.inc18:                                        ; preds = %if.end
  %31 = load i32, i32* %iteration, align 4
  %inc19 = add nsw i32 %31, 1
  store i32 %inc19, i32* %iteration, align 4
  br label %for.cond

for.end20:                                        ; preds = %if.then17, %for.cond
  br label %if.end62

if.else:                                          ; preds = %if.then
  store i32 0, i32* %iteration, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc59, %if.else
  %32 = load i32, i32* %iteration, align 4
  %33 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %34 = bitcast %struct.btContactSolverInfo* %33 to %struct.btContactSolverInfoData*
  %m_numIterations22 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %34, i32 0, i32 5
  %35 = load i32, i32* %m_numIterations22, align 4
  %cmp23 = icmp slt i32 %32, %35
  br i1 %cmp23, label %for.body24, label %for.end61

for.body24:                                       ; preds = %for.cond21
  store float 0.000000e+00, float* %leastSquaresResidual25, align 4
  %m_tmpSolverContactConstraintPool27 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool27)
  store i32 %call28, i32* %numPoolConstraints26, align 4
  store i32 0, i32* %j29, align 4
  br label %for.cond30

for.cond30:                                       ; preds = %for.inc48, %for.body24
  %36 = load i32, i32* %j29, align 4
  %37 = load i32, i32* %numPoolConstraints26, align 4
  %cmp31 = icmp slt i32 %36, %37
  br i1 %cmp31, label %for.body32, label %for.end50

for.body32:                                       ; preds = %for.cond30
  %m_tmpSolverContactConstraintPool34 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %m_orderTmpConstraintPool35 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 6
  %38 = load i32, i32* %j29, align 4
  %call36 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.14* %m_orderTmpConstraintPool35, i32 %38)
  %39 = load i32, i32* %call36, align 4
  %call37 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool34, i32 %39)
  store %struct.btSolverConstraint* %call37, %struct.btSolverConstraint** %solveManifold33, align 4
  %m_tmpSolverBodyPool39 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %40 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold33, align 4
  %m_solverBodyIdA40 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %40, i32 0, i32 18
  %41 = load i32, i32* %m_solverBodyIdA40, align 4
  %call41 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool39, i32 %41)
  %m_tmpSolverBodyPool42 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %42 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold33, align 4
  %m_solverBodyIdB43 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %42, i32 0, i32 19
  %43 = load i32, i32* %m_solverBodyIdB43, align 4
  %call44 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool42, i32 %43)
  %44 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold33, align 4
  %call45 = call float @_ZN35btSequentialImpulseConstraintSolver43resolveSplitPenetrationImpulseCacheFriendlyER12btSolverBodyS1_RK18btSolverConstraint(%class.btSequentialImpulseConstraintSolver* %this1, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call41, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %call44, %struct.btSolverConstraint* nonnull align 4 dereferenceable(152) %44)
  store float %call45, float* %residual38, align 4
  %45 = load float, float* %residual38, align 4
  %46 = load float, float* %residual38, align 4
  %mul46 = fmul float %45, %46
  %47 = load float, float* %leastSquaresResidual25, align 4
  %add47 = fadd float %47, %mul46
  store float %add47, float* %leastSquaresResidual25, align 4
  br label %for.inc48

for.inc48:                                        ; preds = %for.body32
  %48 = load i32, i32* %j29, align 4
  %inc49 = add nsw i32 %48, 1
  store i32 %inc49, i32* %j29, align 4
  br label %for.cond30

for.end50:                                        ; preds = %for.cond30
  %49 = load float, float* %leastSquaresResidual25, align 4
  %50 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %51 = bitcast %struct.btContactSolverInfo* %50 to %struct.btContactSolverInfoData*
  %m_leastSquaresResidualThreshold51 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %51, i32 0, i32 21
  %52 = load float, float* %m_leastSquaresResidualThreshold51, align 4
  %cmp52 = fcmp ole float %49, %52
  br i1 %cmp52, label %if.then57, label %lor.lhs.false53

lor.lhs.false53:                                  ; preds = %for.end50
  %53 = load i32, i32* %iteration, align 4
  %54 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %55 = bitcast %struct.btContactSolverInfo* %54 to %struct.btContactSolverInfoData*
  %m_numIterations54 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %55, i32 0, i32 5
  %56 = load i32, i32* %m_numIterations54, align 4
  %sub55 = sub nsw i32 %56, 1
  %cmp56 = icmp sge i32 %53, %sub55
  br i1 %cmp56, label %if.then57, label %if.end58

if.then57:                                        ; preds = %lor.lhs.false53, %for.end50
  br label %for.end61

if.end58:                                         ; preds = %lor.lhs.false53
  br label %for.inc59

for.inc59:                                        ; preds = %if.end58
  %57 = load i32, i32* %iteration, align 4
  %inc60 = add nsw i32 %57, 1
  store i32 %inc60, i32* %iteration, align 4
  br label %for.cond21

for.end61:                                        ; preds = %if.then57, %for.cond21
  br label %if.end62

if.end62:                                         ; preds = %for.end61, %for.end20
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver33solveGroupCacheFriendlyIterationsEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDraw(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %maxIterations = alloca i32, align 4
  %iteration = alloca i32, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.1, i32 0, i32 0))
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %3 = load i32, i32* %numManifolds.addr, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %5 = load i32, i32* %numConstraints.addr, align 4
  %6 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %7 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %8 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable = load void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable, i64 8
  %9 = load void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn, align 4
  call void %9(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject** %0, i32 %1, %class.btPersistentManifold** %2, i32 %3, %class.btTypedConstraint** %4, i32 %5, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %6, %class.btIDebugDraw* %7)
  %m_maxOverrideNumSolverIterations = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 10
  %10 = load i32, i32* %m_maxOverrideNumSolverIterations, align 4
  %11 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %12 = bitcast %struct.btContactSolverInfo* %11 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %12, i32 0, i32 5
  %13 = load i32, i32* %m_numIterations, align 4
  %cmp = icmp sgt i32 %10, %13
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_maxOverrideNumSolverIterations2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 10
  %14 = load i32, i32* %m_maxOverrideNumSolverIterations2, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %15 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %16 = bitcast %struct.btContactSolverInfo* %15 to %struct.btContactSolverInfoData*
  %m_numIterations3 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 5
  %17 = load i32, i32* %m_numIterations3, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %14, %cond.true ], [ %17, %cond.false ]
  store i32 %cond, i32* %maxIterations, align 4
  store i32 0, i32* %iteration, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %18 = load i32, i32* %iteration, align 4
  %19 = load i32, i32* %maxIterations, align 4
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %iteration, align 4
  %21 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %22 = load i32, i32* %numBodies.addr, align 4
  %23 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %24 = load i32, i32* %numManifolds.addr, align 4
  %25 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %26 = load i32, i32* %numConstraints.addr, align 4
  %27 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %28 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %29 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable5 = load float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %29, align 4
  %vfn6 = getelementptr inbounds float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable5, i64 10
  %30 = load float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, i32, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn6, align 4
  %call7 = call float %30(%class.btSequentialImpulseConstraintSolver* %this1, i32 %20, %class.btCollisionObject** %21, i32 %22, %class.btPersistentManifold** %23, i32 %24, %class.btTypedConstraint** %25, i32 %26, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %27, %class.btIDebugDraw* %28)
  %m_leastSquaresResidual = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 15
  store float %call7, float* %m_leastSquaresResidual, align 4
  %m_leastSquaresResidual8 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 15
  %31 = load float, float* %m_leastSquaresResidual8, align 4
  %32 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %33 = bitcast %struct.btContactSolverInfo* %32 to %struct.btContactSolverInfoData*
  %m_leastSquaresResidualThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %33, i32 0, i32 21
  %34 = load float, float* %m_leastSquaresResidualThreshold, align 4
  %cmp9 = fcmp ole float %31, %34
  br i1 %cmp9, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %35 = load i32, i32* %iteration, align 4
  %36 = load i32, i32* %maxIterations, align 4
  %sub = sub nsw i32 %36, 1
  %cmp10 = icmp sge i32 %35, %sub
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %37 = load i32, i32* %iteration, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %iteration, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %call11 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #9
  ret float 0.000000e+00
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver29solveGroupCacheFriendlyFinishEPP17btCollisionObjectiRK19btContactSolverInfo(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numPoolConstraints = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %solveManifold = alloca %struct.btSolverConstraint*, align 4
  %pt = alloca %class.btManifoldPoint*, align 4
  %solverConstr = alloca %struct.btSolverConstraint*, align 4
  %constr = alloca %class.btTypedConstraint*, align 4
  %fb = alloca %struct.btJointFeedback*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  %body = alloca %class.btRigidBody*, align 4
  %ref.tmp86 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_tmpSolverContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool)
  store i32 %call, i32* %numPoolConstraints, align 4
  %0 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %1 = bitcast %struct.btContactSolverInfo* %0 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 16
  %2 = load i32, i32* %m_solverMode, align 4
  %and = and i32 %2, 4
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end15

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %j, align 4
  %4 = load i32, i32* %numPoolConstraints, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_tmpSolverContactConstraintPool2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  %5 = load i32, i32* %j, align 4
  %call3 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool2, i32 %5)
  store %struct.btSolverConstraint* %call3, %struct.btSolverConstraint** %solveManifold, align 4
  %6 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %7 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %6, i32 0, i32 15
  %m_originalContactPoint = bitcast %union.anon.12* %7 to i8**
  %8 = load i8*, i8** %m_originalContactPoint, align 4
  %9 = bitcast i8* %8 to %class.btManifoldPoint*
  store %class.btManifoldPoint* %9, %class.btManifoldPoint** %pt, align 4
  %10 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_appliedImpulse = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %10, i32 0, i32 7
  %11 = load float, float* %m_appliedImpulse, align 4
  %12 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulse4 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %12, i32 0, i32 16
  store float %11, float* %m_appliedImpulse4, align 4
  %m_tmpSolverContactFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %13 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_frictionIndex = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %13, i32 0, i32 17
  %14 = load i32, i32* %m_frictionIndex, align 4
  %call5 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool, i32 %14)
  %m_appliedImpulse6 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call5, i32 0, i32 7
  %15 = load float, float* %m_appliedImpulse6, align 4
  %16 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %16, i32 0, i32 17
  store float %15, float* %m_appliedImpulseLateral1, align 4
  %17 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %18 = bitcast %struct.btContactSolverInfo* %17 to %struct.btContactSolverInfoData*
  %m_solverMode7 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %18, i32 0, i32 16
  %19 = load i32, i32* %m_solverMode7, align 4
  %and8 = and i32 %19, 16
  %tobool9 = icmp ne i32 %and8, 0
  br i1 %tobool9, label %if.then10, label %if.end

if.then10:                                        ; preds = %for.body
  %m_tmpSolverContactFrictionConstraintPool11 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  %20 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solveManifold, align 4
  %m_frictionIndex12 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %20, i32 0, i32 17
  %21 = load i32, i32* %m_frictionIndex12, align 4
  %add = add nsw i32 %21, 1
  %call13 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool11, i32 %add)
  %m_appliedImpulse14 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %call13, i32 0, i32 7
  %22 = load float, float* %m_appliedImpulse14, align 4
  %23 = load %class.btManifoldPoint*, %class.btManifoldPoint** %pt, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %23, i32 0, i32 18
  store float %22, float* %m_appliedImpulseLateral2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then10, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %24 = load i32, i32* %j, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.end15:                                         ; preds = %for.end, %entry
  %m_tmpSolverNonContactConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool)
  store i32 %call16, i32* %numPoolConstraints, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc64, %if.end15
  %25 = load i32, i32* %j, align 4
  %26 = load i32, i32* %numPoolConstraints, align 4
  %cmp18 = icmp slt i32 %25, %26
  br i1 %cmp18, label %for.body19, label %for.end66

for.body19:                                       ; preds = %for.cond17
  %m_tmpSolverNonContactConstraintPool20 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  %27 = load i32, i32* %j, align 4
  %call21 = call nonnull align 4 dereferenceable(152) %struct.btSolverConstraint* @_ZN20btAlignedObjectArrayI18btSolverConstraintEixEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool20, i32 %27)
  store %struct.btSolverConstraint* %call21, %struct.btSolverConstraint** %solverConstr, align 4
  %28 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %29 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %28, i32 0, i32 15
  %m_originalContactPoint22 = bitcast %union.anon.12* %29 to i8**
  %30 = load i8*, i8** %m_originalContactPoint22, align 4
  %31 = bitcast i8* %30 to %class.btTypedConstraint*
  store %class.btTypedConstraint* %31, %class.btTypedConstraint** %constr, align 4
  %32 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call23 = call %struct.btJointFeedback* @_ZN17btTypedConstraint16getJointFeedbackEv(%class.btTypedConstraint* %32)
  store %struct.btJointFeedback* %call23, %struct.btJointFeedback** %fb, align 4
  %33 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %tobool24 = icmp ne %struct.btJointFeedback* %33, null
  br i1 %tobool24, label %if.then25, label %if.end56

if.then25:                                        ; preds = %for.body19
  %34 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %34, i32 0, i32 1
  %35 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse28 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %35, i32 0, i32 7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %m_appliedImpulse28)
  %36 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call29 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %36)
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %call29)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %call30)
  %37 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %38 = bitcast %struct.btContactSolverInfo* %37 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %38, i32 0, i32 3
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %m_timeStep)
  %39 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedForceBodyA = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %39, i32 0, i32 0
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_appliedForceBodyA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %40 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %40, i32 0, i32 3
  %41 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse35 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %41, i32 0, i32 7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormal2, float* nonnull align 4 dereferenceable(4) %m_appliedImpulse35)
  %42 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call36 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %42)
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody15getLinearFactorEv(%class.btRigidBody* %call36)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %call37)
  %43 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %44 = bitcast %struct.btContactSolverInfo* %43 to %struct.btContactSolverInfoData*
  %m_timeStep38 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %44, i32 0, i32 3
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %m_timeStep38)
  %45 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedForceBodyB = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %45, i32 0, i32 2
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_appliedForceBodyB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  %46 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %46, i32 0, i32 0
  %47 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call43 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %47)
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %call43)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos1CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call44)
  %48 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse45 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %48, i32 0, i32 7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42, float* nonnull align 4 dereferenceable(4) %m_appliedImpulse45)
  %49 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %50 = bitcast %struct.btContactSolverInfo* %49 to %struct.btContactSolverInfoData*
  %m_timeStep46 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %50, i32 0, i32 3
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41, float* nonnull align 4 dereferenceable(4) %m_timeStep46)
  %51 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedTorqueBodyA = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %51, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_appliedTorqueBodyA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp40)
  %52 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %52, i32 0, i32 2
  %53 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call51 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %53)
  %call52 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody16getAngularFactorEv(%class.btRigidBody* %call51)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp50, %class.btVector3* nonnull align 4 dereferenceable(16) %m_relpos2CrossNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %call52)
  %54 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse53 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %54, i32 0, i32 7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %m_appliedImpulse53)
  %55 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %56 = bitcast %struct.btContactSolverInfo* %55 to %struct.btContactSolverInfoData*
  %m_timeStep54 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %56, i32 0, i32 3
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %m_timeStep54)
  %57 = load %struct.btJointFeedback*, %struct.btJointFeedback** %fb, align 4
  %m_appliedTorqueBodyB = getelementptr inbounds %struct.btJointFeedback, %struct.btJointFeedback* %57, i32 0, i32 3
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_appliedTorqueBodyB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp48)
  br label %if.end56

if.end56:                                         ; preds = %if.then25, %for.body19
  %58 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %59 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse57 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %59, i32 0, i32 7
  %60 = load float, float* %m_appliedImpulse57, align 4
  call void @_ZN17btTypedConstraint25internalSetAppliedImpulseEf(%class.btTypedConstraint* %58, float %60)
  %61 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %solverConstr, align 4
  %m_appliedImpulse58 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %61, i32 0, i32 7
  %62 = load float, float* %m_appliedImpulse58, align 4
  %call59 = call float @_Z6btFabsf(float %62)
  %63 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  %call60 = call float @_ZNK17btTypedConstraint27getBreakingImpulseThresholdEv(%class.btTypedConstraint* %63)
  %cmp61 = fcmp oge float %call59, %call60
  br i1 %cmp61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %if.end56
  %64 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constr, align 4
  call void @_ZN17btTypedConstraint10setEnabledEb(%class.btTypedConstraint* %64, i1 zeroext false)
  br label %if.end63

if.end63:                                         ; preds = %if.then62, %if.end56
  br label %for.inc64

for.inc64:                                        ; preds = %if.end63
  %65 = load i32, i32* %j, align 4
  %inc65 = add nsw i32 %65, 1
  store i32 %inc65, i32* %j, align 4
  br label %for.cond17

for.end66:                                        ; preds = %for.cond17
  store i32 0, i32* %i, align 4
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc112, %for.end66
  %66 = load i32, i32* %i, align 4
  %m_tmpSolverBodyPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %call68 = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %m_tmpSolverBodyPool)
  %cmp69 = icmp slt i32 %66, %call68
  br i1 %cmp69, label %for.body70, label %for.end114

for.body70:                                       ; preds = %for.cond67
  %m_tmpSolverBodyPool71 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %67 = load i32, i32* %i, align 4
  %call72 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool71, i32 %67)
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call72, i32 0, i32 12
  %68 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  store %class.btRigidBody* %68, %class.btRigidBody** %body, align 4
  %69 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4
  %tobool73 = icmp ne %class.btRigidBody* %69, null
  br i1 %tobool73, label %if.then74, label %if.end111

if.then74:                                        ; preds = %for.body70
  %70 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %71 = bitcast %struct.btContactSolverInfo* %70 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %71, i32 0, i32 11
  %72 = load i32, i32* %m_splitImpulse, align 4
  %tobool75 = icmp ne i32 %72, 0
  br i1 %tobool75, label %if.then76, label %if.else

if.then76:                                        ; preds = %if.then74
  %m_tmpSolverBodyPool77 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %73 = load i32, i32* %i, align 4
  %call78 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool77, i32 %73)
  %74 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %75 = bitcast %struct.btContactSolverInfo* %74 to %struct.btContactSolverInfoData*
  %m_timeStep79 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %75, i32 0, i32 3
  %76 = load float, float* %m_timeStep79, align 4
  %77 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %78 = bitcast %struct.btContactSolverInfo* %77 to %struct.btContactSolverInfoData*
  %m_splitImpulseTurnErp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %78, i32 0, i32 13
  %79 = load float, float* %m_splitImpulseTurnErp, align 4
  call void @_ZN12btSolverBody29writebackVelocityAndTransformEff(%struct.btSolverBody* %call78, float %76, float %79)
  br label %if.end82

if.else:                                          ; preds = %if.then74
  %m_tmpSolverBodyPool80 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %80 = load i32, i32* %i, align 4
  %call81 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool80, i32 %80)
  call void @_ZN12btSolverBody17writebackVelocityEv(%struct.btSolverBody* %call81)
  br label %if.end82

if.end82:                                         ; preds = %if.else, %if.then76
  %m_tmpSolverBodyPool83 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %81 = load i32, i32* %i, align 4
  %call84 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool83, i32 %81)
  %m_originalBody85 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call84, i32 0, i32 12
  %82 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody85, align 4
  %m_tmpSolverBodyPool87 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %83 = load i32, i32* %i, align 4
  %call88 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool87, i32 %83)
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call88, i32 0, i32 8
  %m_tmpSolverBodyPool89 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %84 = load i32, i32* %i, align 4
  %call90 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool89, i32 %84)
  %m_externalForceImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call90, i32 0, i32 10
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp86, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalForceImpulse)
  call void @_ZN11btRigidBody17setLinearVelocityERK9btVector3(%class.btRigidBody* %82, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp86)
  %m_tmpSolverBodyPool91 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %85 = load i32, i32* %i, align 4
  %call92 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool91, i32 %85)
  %m_originalBody93 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call92, i32 0, i32 12
  %86 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody93, align 4
  %m_tmpSolverBodyPool95 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %87 = load i32, i32* %i, align 4
  %call96 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool95, i32 %87)
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call96, i32 0, i32 9
  %m_tmpSolverBodyPool97 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %88 = load i32, i32* %i, align 4
  %call98 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool97, i32 %88)
  %m_externalTorqueImpulse = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call98, i32 0, i32 11
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_externalTorqueImpulse)
  call void @_ZN11btRigidBody18setAngularVelocityERK9btVector3(%class.btRigidBody* %86, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94)
  %89 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %90 = bitcast %struct.btContactSolverInfo* %89 to %struct.btContactSolverInfoData*
  %m_splitImpulse99 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %90, i32 0, i32 11
  %91 = load i32, i32* %m_splitImpulse99, align 4
  %tobool100 = icmp ne i32 %91, 0
  br i1 %tobool100, label %if.then101, label %if.end107

if.then101:                                       ; preds = %if.end82
  %m_tmpSolverBodyPool102 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %92 = load i32, i32* %i, align 4
  %call103 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool102, i32 %92)
  %m_originalBody104 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call103, i32 0, i32 12
  %93 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody104, align 4
  %94 = bitcast %class.btRigidBody* %93 to %class.btCollisionObject*
  %m_tmpSolverBodyPool105 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %95 = load i32, i32* %i, align 4
  %call106 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool105, i32 %95)
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call106, i32 0, i32 0
  call void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %94, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end107

if.end107:                                        ; preds = %if.then101, %if.end82
  %m_tmpSolverBodyPool108 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  %96 = load i32, i32* %i, align 4
  %call109 = call nonnull align 4 dereferenceable(244) %struct.btSolverBody* @_ZN20btAlignedObjectArrayI12btSolverBodyEixEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool108, i32 %96)
  %m_originalBody110 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %call109, i32 0, i32 12
  %97 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody110, align 4
  %98 = bitcast %class.btRigidBody* %97 to %class.btCollisionObject*
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %98, i32 -1)
  br label %if.end111

if.end111:                                        ; preds = %if.end107, %for.body70
  br label %for.inc112

for.inc112:                                       ; preds = %if.end111
  %99 = load i32, i32* %i, align 4
  %inc113 = add nsw i32 %99, 1
  store i32 %inc113, i32* %i, align 4
  br label %for.cond67

for.end114:                                       ; preds = %for.cond67
  %m_tmpSolverContactConstraintPool115 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactConstraintPool115, i32 0)
  %m_tmpSolverNonContactConstraintPool116 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %m_tmpSolverNonContactConstraintPool116, i32 0)
  %m_tmpSolverContactFrictionConstraintPool117 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 4
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactFrictionConstraintPool117, i32 0)
  %m_tmpSolverContactRollingFrictionConstraintPool = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE18resizeNoInitializeEi(%class.btAlignedObjectArray.9* %m_tmpSolverContactRollingFrictionConstraintPool, i32 0)
  %m_tmpSolverBodyPool118 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE18resizeNoInitializeEi(%class.btAlignedObjectArray* %m_tmpSolverBodyPool118, i32 0)
  ret float 0.000000e+00
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint10setEnabledEb(%class.btTypedConstraint* %this, i1 zeroext %enabled) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %enabled.addr = alloca i8, align 1
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %frombool = zext i1 %enabled to i8
  store i8 %frombool, i8* %enabled.addr, align 1
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load i8, i8* %enabled.addr, align 1
  %tobool = trunc i8 %0 to i1
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_isEnabled, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btSolverBody29writebackVelocityAndTransformEff(%struct.btSolverBody* %this, float %timeStep, float %splitImpulseTurnErp) #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %timeStep.addr = alloca float, align 4
  %splitImpulseTurnErp.addr = alloca float, align 4
  %newTransform = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store float %splitImpulseTurnErp, float* %splitImpulseTurnErp.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end33

if.then:                                          ; preds = %entry
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newTransform)
  %m_pushVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pushVelocity)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %1 = load float, float* %arrayidx, align 4
  %cmp = fcmp une float %1, 0.000000e+00
  br i1 %cmp, label %if.then28, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %m_pushVelocity5 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pushVelocity5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %2 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp une float %2, 0.000000e+00
  br i1 %cmp8, label %if.then28, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %lor.lhs.false
  %m_pushVelocity10 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_pushVelocity10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 2
  %3 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp une float %3, 0.000000e+00
  br i1 %cmp13, label %if.then28, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %lor.lhs.false9
  %m_turnVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_turnVelocity)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 0
  %4 = load float, float* %arrayidx16, align 4
  %cmp17 = fcmp une float %4, 0.000000e+00
  br i1 %cmp17, label %if.then28, label %lor.lhs.false18

lor.lhs.false18:                                  ; preds = %lor.lhs.false14
  %m_turnVelocity19 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_turnVelocity19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %5 = load float, float* %arrayidx21, align 4
  %cmp22 = fcmp une float %5, 0.000000e+00
  br i1 %cmp22, label %if.then28, label %lor.lhs.false23

lor.lhs.false23:                                  ; preds = %lor.lhs.false18
  %m_turnVelocity24 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_turnVelocity24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %6 = load float, float* %arrayidx26, align 4
  %cmp27 = fcmp une float %6, 0.000000e+00
  br i1 %cmp27, label %if.then28, label %if.end

if.then28:                                        ; preds = %lor.lhs.false23, %lor.lhs.false18, %lor.lhs.false14, %lor.lhs.false9, %lor.lhs.false, %if.then
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  %m_pushVelocity29 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 6
  %m_turnVelocity30 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_turnVelocity30, float* nonnull align 4 dereferenceable(4) %splitImpulseTurnErp.addr)
  %7 = load float, float* %timeStep.addr, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pushVelocity29, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float %7, %class.btTransform* nonnull align 4 dereferenceable(64) %newTransform)
  %m_worldTransform31 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  %call32 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform31, %class.btTransform* nonnull align 4 dereferenceable(64) %newTransform)
  br label %if.end

if.end:                                           ; preds = %if.then28, %lor.lhs.false23
  br label %if.end33

if.end33:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btSolverBody17writebackVelocityEv(%struct.btSolverBody* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_originalBody = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 12
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_originalBody, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %m_linearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 9
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_deltaAngularVelocity)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btRigidBody17setLinearVelocityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %lin_vel) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %lin_vel.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %lin_vel, %class.btVector3** %lin_vel.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 33
  %1 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %2 = load %class.btVector3*, %class.btVector3** %lin_vel.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %3 = bitcast %class.btVector3* %m_linearVelocity to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btRigidBody18setAngularVelocityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %ang_vel) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ang_vel.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %ang_vel, %class.btVector3** %ang_vel.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 33
  %1 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %2 = load %class.btVector3*, %class.btVector3** %ang_vel.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %3 = bitcast %class.btVector3* %m_angularVelocity to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN17btCollisionObject17setWorldTransformERK11btTransform(%class.btCollisionObject* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTrans) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %worldTrans.addr = alloca %class.btTransform*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btTransform* %worldTrans, %class.btTransform** %worldTrans.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %1 = load %class.btTransform*, %class.btTransform** %worldTrans.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE18resizeNoInitializeEi(%class.btAlignedObjectArray* %this, i32 %newsize) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZN35btSequentialImpulseConstraintSolver10solveGroupEPP17btCollisionObjectiPP20btPersistentManifoldiPP17btTypedConstraintiRK19btContactSolverInfoP12btIDebugDrawP12btDispatcher(%class.btSequentialImpulseConstraintSolver* %this, %class.btCollisionObject** %bodies, i32 %numBodies, %class.btPersistentManifold** %manifoldPtr, i32 %numManifolds, %class.btTypedConstraint** %constraints, i32 %numConstraints, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal, %class.btIDebugDraw* %debugDrawer, %class.btDispatcher* %0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  %bodies.addr = alloca %class.btCollisionObject**, align 4
  %numBodies.addr = alloca i32, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold**, align 4
  %numManifolds.addr = alloca i32, align 4
  %constraints.addr = alloca %class.btTypedConstraint**, align 4
  %numConstraints.addr = alloca i32, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  store %class.btCollisionObject** %bodies, %class.btCollisionObject*** %bodies.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  store %class.btPersistentManifold** %manifoldPtr, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  store i32 %numManifolds, i32* %numManifolds.addr, align 4
  store %class.btTypedConstraint** %constraints, %class.btTypedConstraint*** %constraints.addr, align 4
  store i32 %numConstraints, i32* %numConstraints.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.2, i32 0, i32 0))
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %2 = load i32, i32* %numBodies.addr, align 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %4 = load i32, i32* %numManifolds.addr, align 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %6 = load i32, i32* %numConstraints.addr, align 4
  %7 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %8 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %9 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %9, align 4
  %vfn = getelementptr inbounds float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable, i64 11
  %10 = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn, align 4
  %call2 = call float %10(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject** %1, i32 %2, %class.btPersistentManifold** %3, i32 %4, %class.btTypedConstraint** %5, i32 %6, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %7, %class.btIDebugDraw* %8)
  %11 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %12 = load i32, i32* %numBodies.addr, align 4
  %13 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr.addr, align 4
  %14 = load i32, i32* %numManifolds.addr, align 4
  %15 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraints.addr, align 4
  %16 = load i32, i32* %numConstraints.addr, align 4
  %17 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %18 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %19 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable3 = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %19, align 4
  %vfn4 = getelementptr inbounds float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable3, i64 12
  %20 = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn4, align 4
  %call5 = call float %20(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject** %11, i32 %12, %class.btPersistentManifold** %13, i32 %14, %class.btTypedConstraint** %15, i32 %16, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %17, %class.btIDebugDraw* %18)
  %21 = load %class.btCollisionObject**, %class.btCollisionObject*** %bodies.addr, align 4
  %22 = load i32, i32* %numBodies.addr, align 4
  %23 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %24 = bitcast %class.btSequentialImpulseConstraintSolver* %this1 to float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)***
  %vtable6 = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)**, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)*** %24, align 4
  %vfn7 = getelementptr inbounds float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)** %vtable6, i64 9
  %25 = load float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)*, float (%class.btSequentialImpulseConstraintSolver*, %class.btCollisionObject**, i32, %struct.btContactSolverInfo*)** %vfn7, align 4
  %call8 = call float %25(%class.btSequentialImpulseConstraintSolver* %this1, %class.btCollisionObject** %21, i32 %22, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %23)
  %call9 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #9
  ret float 0.000000e+00
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN35btSequentialImpulseConstraintSolver5resetEv(%class.btSequentialImpulseConstraintSolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %m_btSeed2 = getelementptr inbounds %class.btSequentialImpulseConstraintSolver, %class.btSequentialImpulseConstraintSolver* %this1, i32 0, i32 16
  store i32 0, i32* %m_btSeed2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver12prepareSolveEii(%class.btConstraintSolver* %this, i32 %0, i32 %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca i32, align 4
  %.addr1 = alloca i32, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store i32 %1, i32* %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolver9allSolvedERK19btContactSolverInfoP12btIDebugDraw(%class.btConstraintSolver* %this, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %0, %class.btIDebugDraw* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  %.addr = alloca %struct.btContactSolverInfo*, align 4
  %.addr1 = alloca %class.btIDebugDraw*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  store %struct.btContactSolverInfo* %0, %struct.btContactSolverInfo** %.addr, align 4
  store %class.btIDebugDraw* %1, %class.btIDebugDraw** %.addr1, align 4
  %this2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK35btSequentialImpulseConstraintSolver13getSolverTypeEv(%class.btSequentialImpulseConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSequentialImpulseConstraintSolver*, align 4
  store %class.btSequentialImpulseConstraintSolver* %this, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  %this1 = load %class.btSequentialImpulseConstraintSolver*, %class.btSequentialImpulseConstraintSolver** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConstraintSolver* @_ZN18btConstraintSolverD2Ev(%class.btConstraintSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  %this1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  ret %class.btConstraintSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConstraintSolverD0Ev(%class.btConstraintSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConstraintSolver*, align 4
  store %class.btConstraintSolver* %this, %class.btConstraintSolver** %this.addr, align 4
  %this1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load i32, i32* %m_internalType, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %3 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call3, float* %fAngle, align 4
  %4 = load float, float* %fAngle, align 4
  %5 = load float, float* %timeStep.addr, align 4
  %mul = fmul float %4, %5
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %timeStep.addr, align 4
  %div = fdiv float 0x3FE921FB60000000, %6
  store float %div, float* %fAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load float, float* %fAngle, align 4
  %cmp4 = fcmp olt float %7, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %8 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %9 = load float, float* %timeStep.addr, align 4
  %mul8 = fmul float 5.000000e-01, %9
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load float, float* %timeStep.addr, align 4
  %mul9 = fmul float %10, %11
  %12 = load float, float* %timeStep.addr, align 4
  %mul10 = fmul float %mul9, %12
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %13 = load float, float* %fAngle, align 4
  %mul12 = fmul float %mul11, %13
  %14 = load float, float* %fAngle, align 4
  %mul13 = fmul float %mul12, %14
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %8, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast %class.btVector3* %axis to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %18 = load float, float* %fAngle, align 4
  %mul16 = fmul float 5.000000e-01, %18
  %19 = load float, float* %timeStep.addr, align 4
  %mul17 = fmul float %mul16, %19
  %call18 = call float @_Z5btSinf(float %mul17)
  %20 = load float, float* %fAngle, align 4
  %div19 = fdiv float %call18, %20
  store float %div19, float* %ref.tmp15, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %21 = bitcast %class.btVector3* %axis to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %23 = load float, float* %fAngle, align 4
  %24 = load float, float* %timeStep.addr, align 4
  %mul25 = fmul float %23, %24
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %25 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %25)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %26 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSolverBodyLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSolverBody* null, %struct.btSolverBody** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* null, %struct.btSolverConstraint** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.15* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  ret %class.btAlignedAllocator.15* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EEC2Ev(%class.btAlignedAllocator.19* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  ret %class.btAlignedAllocator.19* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4initEv(%class.btAlignedObjectArray.18* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* null, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSolverBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSolverBodyE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %tobool = icmp ne %struct.btSolverBody* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI12btSolverBodyLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btSolverBody* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSolverBody* null, %struct.btSolverBody** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btSolverBodyLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btSolverBody* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btSolverBody*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btSolverBody* %ptr, %struct.btSolverBody** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btSolverBody*, %struct.btSolverBody** %ptr.addr, align 4
  %1 = bitcast %struct.btSolverBody* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE5clearEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %3 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %tobool = icmp ne %struct.btSolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %m_allocator, %struct.btSolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* null, %struct.btSolverConstraint** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %this, %struct.btSolverConstraint* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %struct.btSolverConstraint*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store %struct.btSolverConstraint* %ptr, %struct.btSolverConstraint** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %ptr.addr, align 4
  %1 = bitcast %struct.btSolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.15* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.15* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE5clearEv(%class.btAlignedObjectArray.18* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7destroyEii(%class.btAlignedObjectArray.18* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv(%class.btAlignedObjectArray.18* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE10deallocateEv(%class.btAlignedObjectArray.18* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %tobool = icmp ne %"struct.btTypedConstraint::btConstraintInfo1"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE10deallocateEPS1_(%class.btAlignedAllocator.19* %m_allocator, %"struct.btTypedConstraint::btConstraintInfo1"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* null, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE10deallocateEPS1_(%class.btAlignedAllocator.19* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %ptr.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %ptr, %"struct.btTypedConstraint::btConstraintInfo1"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %ptr.addr, align 4
  %1 = bitcast %"struct.btTypedConstraint::btConstraintInfo1"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSolverConstraint*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btSolverConstraint*
  store %struct.btSolverConstraint* %2, %struct.btSolverConstraint** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %3 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %struct.btSolverConstraint* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btSolverConstraintE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btSolverConstraint* %4, %struct.btSolverConstraint** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btSolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btSolverConstraintE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btSolverConstraint* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %struct.btSolverConstraint** null)
  %2 = bitcast %struct.btSolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btSolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %struct.btSolverConstraint* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btSolverConstraint* %dest, %struct.btSolverConstraint** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %3, i32 %4
  %5 = bitcast %struct.btSolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN18btSolverConstraintnwEmPv(i32 152, i8* %5)
  %6 = bitcast i8* %call to %struct.btSolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %7 = load %struct.btSolverConstraint*, %struct.btSolverConstraint** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btSolverConstraint, %struct.btSolverConstraint* %7, i32 %8
  %9 = bitcast %struct.btSolverConstraint* %6 to i8*
  %10 = bitcast %struct.btSolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 152, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSolverConstraint* @_ZN18btAlignedAllocatorI18btSolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %this, i32 %n, %struct.btSolverConstraint** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSolverConstraint**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btSolverConstraint** %hint, %struct.btSolverConstraint*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 152, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSolverConstraint*
  ret %struct.btSolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btSolverConstraintnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSolverBodyE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI12btSolverBodyE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN12btSolverBodynwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSolverBody* @_ZN12btSolverBodyC2ERKS_(%struct.btSolverBody* returned %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSolverBody*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  store %struct.btSolverBody* %this, %struct.btSolverBody** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  %this1 = load %struct.btSolverBody*, %struct.btSolverBody** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 0
  %1 = load %struct.btSolverBody*, %struct.btSolverBody** %.addr, align 4
  %m_worldTransform2 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform2)
  %m_deltaLinearVelocity = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %this1, i32 0, i32 1
  %2 = load %struct.btSolverBody*, %struct.btSolverBody** %.addr, align 4
  %m_deltaLinearVelocity3 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %2, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_deltaLinearVelocity to i8*
  %4 = bitcast %class.btVector3* %m_deltaLinearVelocity3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %3, i8* align 4 %4, i64 180, i1 false)
  ret %struct.btSolverBody* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btSolverBodyE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btSolverBody* @_ZN18btAlignedAllocatorI12btSolverBodyLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btSolverBody** null)
  %2 = bitcast %struct.btSolverBody* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btSolverBodyE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btSolverBody* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSolverBody*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btSolverBody* %dest, %struct.btSolverBody** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btSolverBody*, %struct.btSolverBody** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %3, i32 %4
  %5 = bitcast %struct.btSolverBody* %arrayidx to i8*
  %call = call i8* @_ZN12btSolverBodynwEmPv(i32 244, i8* %5)
  %6 = bitcast i8* %call to %struct.btSolverBody*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btSolverBody*, %struct.btSolverBody** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btSolverBody, %struct.btSolverBody* %7, i32 %8
  %call3 = call %struct.btSolverBody* @_ZN12btSolverBodyC2ERKS_(%struct.btSolverBody* %6, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSolverBody* @_ZN18btAlignedAllocatorI12btSolverBodyLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btSolverBody** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSolverBody**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btSolverBody** %hint, %struct.btSolverBody*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 244, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSolverBody*
  ret %struct.btSolverBody* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7reserveEi(%class.btAlignedObjectArray.18* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8capacityEv(%class.btAlignedObjectArray.18* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8allocateEi(%class.btAlignedObjectArray.18* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btTypedConstraint::btConstraintInfo1"*
  store %"struct.btTypedConstraint::btConstraintInfo1"* %2, %"struct.btTypedConstraint::btConstraintInfo1"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4copyEiiPS1_(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call3, %"struct.btTypedConstraint::btConstraintInfo1"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %4, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8capacityEv(%class.btAlignedObjectArray.18* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE8allocateEi(%class.btAlignedObjectArray.18* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.19* %m_allocator, i32 %1, %"struct.btTypedConstraint::btConstraintInfo1"** null)
  %2 = bitcast %"struct.btTypedConstraint::btConstraintInfo1"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN17btTypedConstraint17btConstraintInfo1EE4copyEiiPS1_(%class.btAlignedObjectArray.18* %this, i32 %start, i32 %end, %"struct.btTypedConstraint::btConstraintInfo1"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %dest, %"struct.btTypedConstraint::btConstraintInfo1"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 %4
  %5 = bitcast %"struct.btTypedConstraint::btConstraintInfo1"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btTypedConstraint::btConstraintInfo1"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %7 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %7, i32 %8
  %9 = bitcast %"struct.btTypedConstraint::btConstraintInfo1"* %6 to i8*
  %10 = bitcast %"struct.btTypedConstraint::btConstraintInfo1"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btTypedConstraint::btConstraintInfo1"* @_ZN18btAlignedAllocatorIN17btTypedConstraint17btConstraintInfo1ELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.19* %this, i32 %n, %"struct.btTypedConstraint::btConstraintInfo1"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"**, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"** %hint, %"struct.btTypedConstraint::btConstraintInfo1"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btTypedConstraint::btConstraintInfo1"*
  ret %"struct.btTypedConstraint::btConstraintInfo1"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.14* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.14* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.14* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.14* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.15* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.14* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.15* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSequentialImpulseConstraintSolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
