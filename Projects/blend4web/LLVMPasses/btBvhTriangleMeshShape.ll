; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btTriangleCallback = type { i32 (...)** }
%struct.MyNodeOverlapCallback = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback* }
%class.btNodeOverlapCallback = type { i32 (...)** }
%struct.MyNodeOverlapCallback.20 = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback* }
%struct.MyNodeOverlapCallback.21 = type { %class.btNodeOverlapCallback, %class.btStridingMeshInterface*, %class.btTriangleCallback*, [3 x %class.btVector3], i32 }
%class.btSerializer = type { i32 (...)** }
%struct.btTriangleMeshShapeData = type { %struct.btCollisionShapeData, %struct.btStridingMeshInterfaceData, %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhDoubleData*, %struct.btTriangleInfoMapData*, float, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoMapData = type { i32*, i32*, %struct.btTriangleInfoData*, i32*, float, float, float, float, float, i32, i32, i32, i32, [4 x i8] }
%struct.btTriangleInfoData = type { i32, float, float, float }
%class.btChunk = type { i32, i32, i8*, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN14btOptimizedBvhnwEmPv = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN22btBvhTriangleMeshShapedlEPv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK22btBvhTriangleMeshShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btConcaveShape9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN21btNodeOverlapCallbackC2Ev = comdat any

$_ZN21btNodeOverlapCallbackD2Ev = comdat any

$_ZN21btNodeOverlapCallbackD0Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZTS21btNodeOverlapCallback = comdat any

$_ZTI21btNodeOverlapCallback = comdat any

$_ZTV21btNodeOverlapCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV22btBvhTriangleMeshShape = hidden unnamed_addr constant { [23 x i8*] } { [23 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btBvhTriangleMeshShape to i8*), i8* bitcast (%class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD1Ev to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleMeshShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btVector3*)* @_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btTriangleMeshShape*)* @_ZNK19btTriangleMeshShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleMeshShape*, float, %class.btVector3*)* @_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btBvhTriangleMeshShape*)* @_ZNK22btBvhTriangleMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConcaveShape*, float)* @_ZN14btConcaveShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btBvhTriangleMeshShape*)* @_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btBvhTriangleMeshShape*, i8*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)* @_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)* @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape18serializeSingleBvhEP12btSerializer to i8*), i8* bitcast (void (%class.btBvhTriangleMeshShape*, %class.btSerializer*)* @_ZNK22btBvhTriangleMeshShape30serializeSingleTriangleInfoMapEP12btSerializer to i8*)] }, align 4
@.str = private unnamed_addr constant [24 x i8] c"btTriangleMeshShapeData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btBvhTriangleMeshShape = hidden constant [25 x i8] c"22btBvhTriangleMeshShape\00", align 1
@_ZTI19btTriangleMeshShape = external constant i8*
@_ZTI22btBvhTriangleMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btBvhTriangleMeshShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI19btTriangleMeshShape to i8*) }, align 4
@_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%struct.MyNodeOverlapCallback* (%struct.MyNodeOverlapCallback*)* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*)* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback*, i32, i32)* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant [104 x i8] c"ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btNodeOverlapCallback = linkonce_odr hidden constant [24 x i8] c"21btNodeOverlapCallback\00", comdat, align 1
@_ZTI21btNodeOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btNodeOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTIZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([104 x i8], [104 x i8]* @_ZTSZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@_ZTV21btNodeOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*), i8* bitcast (%class.btNodeOverlapCallback* (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%class.btNodeOverlapCallback*)* @_ZN21btNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%struct.MyNodeOverlapCallback.20* (%struct.MyNodeOverlapCallback.20*)* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.20*)* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.20*, i32, i32)* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal constant [113 x i8] c"ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback\00", align 1
@_ZTIZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([113 x i8], [113 x i8]* @_ZTSZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback to i8*), i8* bitcast (%struct.MyNodeOverlapCallback.21* (%struct.MyNodeOverlapCallback.21*)* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.21*)* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev to i8*), i8* bitcast (void (%struct.MyNodeOverlapCallback.21*, i32, i32)* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii to i8*)] }, align 4
@_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant [110 x i8] c"ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback\00", align 1
@_ZTIZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([110 x i8], [110 x i8]* @_ZTSZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btNodeOverlapCallback to i8*) }, align 4
@.str.1 = private unnamed_addr constant [16 x i8] c"BVHTRIANGLEMESH\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btBvhTriangleMeshShape.cpp, i8* null }]

@_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebb = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, i1), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, i1)* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb
@_ZN22btBvhTriangleMeshShapeC1EP23btStridingMeshInterfacebRK9btVector3S4_b = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, %class.btVector3*, %class.btVector3*, i1), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*, %class.btStridingMeshInterface*, i1, %class.btVector3*, %class.btVector3*, i1)* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b
@_ZN22btBvhTriangleMeshShapeD1Ev = hidden unnamed_addr alias %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*), %class.btBvhTriangleMeshShape* (%class.btBvhTriangleMeshShape*)* @_ZN22btBvhTriangleMeshShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebb(%class.btBvhTriangleMeshShape* returned %this, %class.btStridingMeshInterface* %meshInterface, i1 zeroext %useQuantizedAabbCompression, i1 zeroext %buildBvh) unnamed_addr #2 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %buildBvh.addr = alloca i8, align 1
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1
  %frombool1 = zext i1 %buildBvh to i8
  store i8 %frombool1, i8* %buildBvh.addr, align 1
  %this2 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this2, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btTriangleMeshShape*
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %call = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* %0, %class.btStridingMeshInterface* %1)
  %2 = bitcast %class.btBvhTriangleMeshShape* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* null, %class.btOptimizedBvh** %m_bvh, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 2
  store %struct.btTriangleInfoMap* null, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %3 = load i8, i8* %useQuantizedAabbCompression.addr, align 1
  %tobool = trunc i8 %3 to i1
  %frombool3 = zext i1 %tobool to i8
  store i8 %frombool3, i8* %m_useQuantizedAabbCompression, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1
  %4 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 21, i32* %m_shapeType, align 4
  %5 = load i8, i8* %buildBvh.addr, align 1
  %tobool4 = trunc i8 %5 to i1
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %6
}

declare %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* returned, %class.btStridingMeshInterface*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %mem = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_ownsBvh, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %2 = bitcast %class.btOptimizedBvh* %1 to %class.btOptimizedBvh* (%class.btOptimizedBvh*)***
  %vtable = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)**, %class.btOptimizedBvh* (%class.btOptimizedBvh*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vtable, i64 0
  %3 = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vfn, align 4
  %call = call %class.btOptimizedBvh* %3(%class.btOptimizedBvh* %1) #8
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %4 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4
  %5 = bitcast %class.btOptimizedBvh* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call3 = call i8* @_Z22btAlignedAllocInternalmi(i32 172, i32 16)
  store i8* %call3, i8** %mem, align 4
  %6 = load i8*, i8** %mem, align 4
  %call4 = call i8* @_ZN14btOptimizedBvhnwEmPv(i32 172, i8* %6)
  %7 = bitcast i8* %call4 to %class.btOptimizedBvh*
  %call5 = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* %7)
  %m_bvh6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  store %class.btOptimizedBvh* %7, %class.btOptimizedBvh** %m_bvh6, align 4
  %m_bvh7 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %8 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh7, align 4
  %9 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %9, i32 0, i32 3
  %10 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 3
  %11 = load i8, i8* %m_useQuantizedAabbCompression, align 4
  %tobool8 = trunc i8 %11 to i1
  %12 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %12, i32 0, i32 1
  %13 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %13, i32 0, i32 2
  call void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %8, %class.btStridingMeshInterface* %10, i1 zeroext %tobool8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax)
  %m_ownsBvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  store i8 1, i8* %m_ownsBvh9, align 1
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeC2EP23btStridingMeshInterfacebRK9btVector3S4_b(%class.btBvhTriangleMeshShape* returned %this, %class.btStridingMeshInterface* %meshInterface, i1 zeroext %useQuantizedAabbCompression, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax, i1 zeroext %buildBvh) unnamed_addr #2 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %buildBvh.addr = alloca i8, align 1
  %mem = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4
  %frombool1 = zext i1 %buildBvh to i8
  store i8 %frombool1, i8* %buildBvh.addr, align 1
  %this2 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this2, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btTriangleMeshShape*
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %call = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeC2EP23btStridingMeshInterface(%class.btTriangleMeshShape* %0, %class.btStridingMeshInterface* %1)
  %2 = bitcast %class.btBvhTriangleMeshShape* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* null, %class.btOptimizedBvh** %m_bvh, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 2
  store %struct.btTriangleInfoMap* null, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %m_useQuantizedAabbCompression = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %3 = load i8, i8* %useQuantizedAabbCompression.addr, align 1
  %tobool = trunc i8 %3 to i1
  %frombool3 = zext i1 %tobool to i8
  store i8 %frombool3, i8* %m_useQuantizedAabbCompression, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1
  %4 = bitcast %class.btBvhTriangleMeshShape* %this2 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %4, i32 0, i32 1
  store i32 21, i32* %m_shapeType, align 4
  %5 = load i8, i8* %buildBvh.addr, align 1
  %tobool4 = trunc i8 %5 to i1
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 172, i32 16)
  store i8* %call5, i8** %mem, align 4
  %6 = load i8*, i8** %mem, align 4
  %call6 = call i8* @_ZN14btOptimizedBvhnwEmPv(i32 172, i8* %6)
  %7 = bitcast i8* %call6 to %class.btOptimizedBvh*
  %call7 = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* %7)
  %m_bvh8 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  store %class.btOptimizedBvh* %7, %class.btOptimizedBvh** %m_bvh8, align 4
  %m_bvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 1
  %8 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh9, align 4
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %m_useQuantizedAabbCompression10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 3
  %10 = load i8, i8* %m_useQuantizedAabbCompression10, align 4
  %tobool11 = trunc i8 %10 to i1
  %11 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4
  %12 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4
  call void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %8, %class.btStridingMeshInterface* %9, i1 zeroext %tobool11, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %m_ownsBvh12 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this2, i32 0, i32 4
  store i8 1, i8* %m_ownsBvh12, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %13
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN14btOptimizedBvhnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btOptimizedBvh* @_ZN14btOptimizedBvhC1Ev(%class.btOptimizedBvh* returned) unnamed_addr #3

declare void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, i1 zeroext, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape16partialRefitTreeERK9btVector3S2_(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %0, %class.btStridingMeshInterface* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMin = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %5, i32 0, i32 1
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_localAabbMax = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %7, i32 0, i32 2
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  ret void
}

declare void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape9refitTreeERK9btVector3S2_(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %0, %class.btStridingMeshInterface* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  call void @_ZN19btTriangleMeshShape15recalcLocalAabbEv(%class.btTriangleMeshShape* %5)
  ret void
}

declare void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh*, %class.btStridingMeshInterface*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare void @_ZN19btTriangleMeshShape15recalcLocalAabbEv(%class.btTriangleMeshShape*) #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeD2Ev(%class.btBvhTriangleMeshShape* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btBvhTriangleMeshShape*, align 4
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btBvhTriangleMeshShape* %this1, %class.btBvhTriangleMeshShape** %retval, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [23 x i8*] }, { [23 x i8*] }* @_ZTV22btBvhTriangleMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  %1 = load i8, i8* %m_ownsBvh, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %2 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %3 = bitcast %class.btOptimizedBvh* %2 to %class.btOptimizedBvh* (%class.btOptimizedBvh*)***
  %vtable = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)**, %class.btOptimizedBvh* (%class.btOptimizedBvh*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vtable, i64 0
  %4 = load %class.btOptimizedBvh* (%class.btOptimizedBvh*)*, %class.btOptimizedBvh* (%class.btOptimizedBvh*)** %vfn, align 4
  %call = call %class.btOptimizedBvh* %4(%class.btOptimizedBvh* %2) #8
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %5 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4
  %6 = bitcast %class.btOptimizedBvh* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %call3 = call %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeD2Ev(%class.btTriangleMeshShape* %7) #8
  %8 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %retval, align 4
  ret %class.btBvhTriangleMeshShape* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: nounwind
declare %class.btTriangleMeshShape* @_ZN19btTriangleMeshShapeD2Ev(%class.btTriangleMeshShape* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btBvhTriangleMeshShapeD0Ev(%class.btBvhTriangleMeshShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %call = call %class.btBvhTriangleMeshShape* @_ZN22btBvhTriangleMeshShapeD1Ev(%class.btBvhTriangleMeshShape* %this1) #8
  %0 = bitcast %class.btBvhTriangleMeshShape* %this1 to i8*
  call void @_ZN22btBvhTriangleMeshShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btBvhTriangleMeshShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %call = call %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback* %myNodeCallback, %class.btTriangleCallback* %0, %class.btStridingMeshInterface* %2)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %3 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %4 = bitcast %class.btOptimizedBvh* %3 to %class.btQuantizedBvh*
  %5 = bitcast %struct.MyNodeOverlapCallback* %myNodeCallback to %class.btNodeOverlapCallback*
  %6 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  call void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %4, %class.btNodeOverlapCallback* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call2 = call %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback* %myNodeCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #8
  %1 = bitcast %struct.MyNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4
  ret %struct.MyNodeOverlapCallback* %this1
}

declare void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define internal %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* %0) #8
  ret %struct.MyNodeOverlapCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback.20, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %call = call %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.20* %myNodeCallback, %class.btTriangleCallback* %0, %class.btStridingMeshInterface* %2)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %3 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %4 = bitcast %class.btOptimizedBvh* %3 to %class.btQuantizedBvh*
  %5 = bitcast %struct.MyNodeOverlapCallback.20* %myNodeCallback to %class.btNodeOverlapCallback*
  %6 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %4, %class.btNodeOverlapCallback* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %call2 = call %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.20* %myNodeCallback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.20* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #8
  %1 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4
  ret %struct.MyNodeOverlapCallback.20* %this1
}

declare void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define internal %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.20* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* %0) #8
  ret %struct.MyNodeOverlapCallback.20* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btBvhTriangleMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %myNodeCallback = alloca %struct.MyNodeOverlapCallback.21, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %1, i32 0, i32 3
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %call = call %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.21* %myNodeCallback, %class.btTriangleCallback* %0, %class.btStridingMeshInterface* %2)
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %3 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %4 = bitcast %class.btOptimizedBvh* %3 to %class.btQuantizedBvh*
  %5 = bitcast %struct.MyNodeOverlapCallback.21* %myNodeCallback to %class.btNodeOverlapCallback*
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %4, %class.btNodeOverlapCallback* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call2 = call %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.21* %myNodeCallback) #8
  ret void
}

; Function Attrs: noinline optnone
define internal %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackC2ES1_P23btStridingMeshInterface(%struct.MyNodeOverlapCallback.21* returned %this, %class.btTriangleCallback* %callback, %class.btStridingMeshInterface* %meshInterface) unnamed_addr #2 {
entry:
  %retval = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  store %struct.MyNodeOverlapCallback.21* %this1, %struct.MyNodeOverlapCallback.21** %retval, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* %0) #8
  %1 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_E21MyNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 2
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btTriangleCallback* %3, %class.btTriangleCallback** %m_callback, align 4
  %m_triangle = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_numOverlap = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 4
  store i32 0, i32* %m_numOverlap, align 4
  %4 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %retval, align 4
  ret %struct.MyNodeOverlapCallback.21* %4
}

declare void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh*, %class.btNodeOverlapCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define internal %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.21* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to %class.btNodeOverlapCallback*
  %call = call %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* %0) #8
  ret %struct.MyNodeOverlapCallback.21* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape15setLocalScalingERK9btVector3(%class.btBvhTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %1 = bitcast %class.btTriangleMeshShape* %0 to %class.btVector3* (%class.btTriangleMeshShape*)***
  %vtable = load %class.btVector3* (%class.btTriangleMeshShape*)**, %class.btVector3* (%class.btTriangleMeshShape*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vtable, i64 7
  %2 = load %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %2(%class.btTriangleMeshShape* %0)
  %3 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %cmp = fcmp ogt float %call2, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %5 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  call void @_ZN22btBvhTriangleMeshShape17buildOptimizedBvhEv(%class.btBvhTriangleMeshShape* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

declare void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN22btBvhTriangleMeshShape15setOptimizedBvhEP14btOptimizedBvhRK9btVector3(%class.btBvhTriangleMeshShape* %this, %class.btOptimizedBvh* %bvh, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %bvh.addr = alloca %class.btOptimizedBvh*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btOptimizedBvh* %bvh, %class.btOptimizedBvh** %bvh.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %bvh.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  store %class.btOptimizedBvh* %0, %class.btOptimizedBvh** %m_bvh, align 4
  %m_ownsBvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 4
  store i8 0, i8* %m_ownsBvh, align 1
  %1 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %2 = bitcast %class.btTriangleMeshShape* %1 to %class.btVector3* (%class.btTriangleMeshShape*)***
  %vtable = load %class.btVector3* (%class.btTriangleMeshShape*)**, %class.btVector3* (%class.btTriangleMeshShape*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vtable, i64 7
  %3 = load %class.btVector3* (%class.btTriangleMeshShape*)*, %class.btVector3* (%class.btTriangleMeshShape*)** %vfn, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* %3(%class.btTriangleMeshShape* %1)
  %4 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %cmp = fcmp ogt float %call2, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %6 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZN19btTriangleMeshShape15setLocalScalingERK9btVector3(%class.btTriangleMeshShape* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK22btBvhTriangleMeshShape9serializeEPvP12btSerializer(%class.btBvhTriangleMeshShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %trimeshData = alloca %struct.btTriangleMeshShapeData*, align 4
  %chunk = alloca i8*, align 4
  %sz = alloca i32, align 4
  %chunk25 = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  %chunk48 = alloca i8*, align 4
  %sz62 = alloca i32, align 4
  %chunk67 = alloca %class.btChunk*, align 4
  %structType71 = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btTriangleMeshShapeData*
  store %struct.btTriangleMeshShapeData* %1, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %2 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %6 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %6, i32 0, i32 3
  %7 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %8 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_meshInterface2 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %8, i32 0, i32 1
  %9 = bitcast %struct.btStridingMeshInterfaceData* %m_meshInterface2 to i8*
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %11 = bitcast %class.btStridingMeshInterface* %7 to i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)***
  %vtable = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)**, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*** %11, align 4
  %vfn = getelementptr inbounds i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vtable, i64 14
  %12 = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vfn, align 4
  %call3 = call i8* %12(%class.btStridingMeshInterface* %7, i8* %9, %class.btSerializer* %10)
  %13 = bitcast %class.btBvhTriangleMeshShape* %this1 to %class.btConcaveShape*
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %13, i32 0, i32 1
  %14 = load float, float* %m_collisionMargin, align 4
  %15 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %15, i32 0, i32 5
  store float %14, float* %m_collisionMargin4, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %16 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %tobool = icmp ne %class.btOptimizedBvh* %16, null
  br i1 %tobool, label %land.lhs.true, label %if.else36

land.lhs.true:                                    ; preds = %entry
  %17 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %18 = bitcast %class.btSerializer* %17 to i32 (%class.btSerializer*)***
  %vtable5 = load i32 (%class.btSerializer*)**, i32 (%class.btSerializer*)*** %18, align 4
  %vfn6 = getelementptr inbounds i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vtable5, i64 13
  %19 = load i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vfn6, align 4
  %call7 = call i32 %19(%class.btSerializer* %17)
  %and = and i32 %call7, 1
  %tobool8 = icmp ne i32 %and, 0
  br i1 %tobool8, label %if.else36, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %20 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_bvh9 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %21 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh9, align 4
  %22 = bitcast %class.btOptimizedBvh* %21 to i8*
  %23 = bitcast %class.btSerializer* %20 to i8* (%class.btSerializer*, i8*)***
  %vtable10 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %23, align 4
  %vfn11 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable10, i64 6
  %24 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn11, align 4
  %call12 = call i8* %24(%class.btSerializer* %20, i8* %22)
  store i8* %call12, i8** %chunk, align 4
  %25 = load i8*, i8** %chunk, align 4
  %tobool13 = icmp ne i8* %25, null
  br i1 %tobool13, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.then
  %26 = load i8*, i8** %chunk, align 4
  %27 = bitcast i8* %26 to %struct.btQuantizedBvhFloatData*
  %28 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedFloatBvh = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %28, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* %27, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh, align 4
  %29 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedDoubleBvh = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %29, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %30 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_bvh15 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %31 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh15, align 4
  %32 = bitcast %class.btOptimizedBvh* %31 to i8*
  %33 = bitcast %class.btSerializer* %30 to i8* (%class.btSerializer*, i8*)***
  %vtable16 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %33, align 4
  %vfn17 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable16, i64 7
  %34 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn17, align 4
  %call18 = call i8* %34(%class.btSerializer* %30, i8* %32)
  %35 = bitcast i8* %call18 to %struct.btQuantizedBvhFloatData*
  %36 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedFloatBvh19 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %36, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* %35, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh19, align 4
  %37 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedDoubleBvh20 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %37, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh20, align 4
  %m_bvh21 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %38 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh21, align 4
  %39 = bitcast %class.btOptimizedBvh* %38 to %class.btQuantizedBvh*
  %40 = bitcast %class.btQuantizedBvh* %39 to i32 (%class.btQuantizedBvh*)***
  %vtable22 = load i32 (%class.btQuantizedBvh*)**, i32 (%class.btQuantizedBvh*)*** %40, align 4
  %vfn23 = getelementptr inbounds i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vtable22, i64 3
  %41 = load i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vfn23, align 4
  %call24 = call i32 %41(%class.btQuantizedBvh* %39)
  store i32 %call24, i32* %sz, align 4
  %42 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %43 = load i32, i32* %sz, align 4
  %44 = bitcast %class.btSerializer* %42 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable26 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %44, align 4
  %vfn27 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable26, i64 4
  %45 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn27, align 4
  %call28 = call %class.btChunk* %45(%class.btSerializer* %42, i32 %43, i32 1)
  store %class.btChunk* %call28, %class.btChunk** %chunk25, align 4
  %m_bvh29 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %46 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh29, align 4
  %47 = bitcast %class.btOptimizedBvh* %46 to %class.btQuantizedBvh*
  %48 = load %class.btChunk*, %class.btChunk** %chunk25, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %48, i32 0, i32 2
  %49 = load i8*, i8** %m_oldPtr, align 4
  %50 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %51 = bitcast %class.btQuantizedBvh* %47 to i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)***
  %vtable30 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)**, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*** %51, align 4
  %vfn31 = getelementptr inbounds i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vtable30, i64 4
  %52 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vfn31, align 4
  %call32 = call i8* %52(%class.btQuantizedBvh* %47, i8* %49, %class.btSerializer* %50)
  store i8* %call32, i8** %structType, align 4
  %53 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %54 = load %class.btChunk*, %class.btChunk** %chunk25, align 4
  %55 = load i8*, i8** %structType, align 4
  %m_bvh33 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %56 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh33, align 4
  %57 = bitcast %class.btOptimizedBvh* %56 to i8*
  %58 = bitcast %class.btSerializer* %53 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable34 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %58, align 4
  %vfn35 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable34, i64 5
  %59 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn35, align 4
  call void %59(%class.btSerializer* %53, %class.btChunk* %54, i8* %55, i32 1213612625, i8* %57)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then14
  br label %if.end39

if.else36:                                        ; preds = %land.lhs.true, %entry
  %60 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedFloatBvh37 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %60, i32 0, i32 2
  store %struct.btQuantizedBvhFloatData* null, %struct.btQuantizedBvhFloatData** %m_quantizedFloatBvh37, align 4
  %61 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_quantizedDoubleBvh38 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %61, i32 0, i32 3
  store %struct.btQuantizedBvhDoubleData* null, %struct.btQuantizedBvhDoubleData** %m_quantizedDoubleBvh38, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.else36, %if.end
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %62 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %tobool40 = icmp ne %struct.btTriangleInfoMap* %62, null
  br i1 %tobool40, label %land.lhs.true41, label %if.else81

land.lhs.true41:                                  ; preds = %if.end39
  %63 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %64 = bitcast %class.btSerializer* %63 to i32 (%class.btSerializer*)***
  %vtable42 = load i32 (%class.btSerializer*)**, i32 (%class.btSerializer*)*** %64, align 4
  %vfn43 = getelementptr inbounds i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vtable42, i64 13
  %65 = load i32 (%class.btSerializer*)*, i32 (%class.btSerializer*)** %vfn43, align 4
  %call44 = call i32 %65(%class.btSerializer* %63)
  %and45 = and i32 %call44, 2
  %tobool46 = icmp ne i32 %and45, 0
  br i1 %tobool46, label %if.else81, label %if.then47

if.then47:                                        ; preds = %land.lhs.true41
  %66 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_triangleInfoMap49 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %67 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap49, align 4
  %68 = bitcast %struct.btTriangleInfoMap* %67 to i8*
  %69 = bitcast %class.btSerializer* %66 to i8* (%class.btSerializer*, i8*)***
  %vtable50 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %69, align 4
  %vfn51 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable50, i64 6
  %70 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn51, align 4
  %call52 = call i8* %70(%class.btSerializer* %66, i8* %68)
  store i8* %call52, i8** %chunk48, align 4
  %71 = load i8*, i8** %chunk48, align 4
  %tobool53 = icmp ne i8* %71, null
  br i1 %tobool53, label %if.then54, label %if.else56

if.then54:                                        ; preds = %if.then47
  %72 = load i8*, i8** %chunk48, align 4
  %73 = bitcast i8* %72 to %struct.btTriangleInfoMapData*
  %74 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_triangleInfoMap55 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %74, i32 0, i32 4
  store %struct.btTriangleInfoMapData* %73, %struct.btTriangleInfoMapData** %m_triangleInfoMap55, align 4
  br label %if.end80

if.else56:                                        ; preds = %if.then47
  %75 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_triangleInfoMap57 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %76 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap57, align 4
  %77 = bitcast %struct.btTriangleInfoMap* %76 to i8*
  %78 = bitcast %class.btSerializer* %75 to i8* (%class.btSerializer*, i8*)***
  %vtable58 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %78, align 4
  %vfn59 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable58, i64 7
  %79 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn59, align 4
  %call60 = call i8* %79(%class.btSerializer* %75, i8* %77)
  %80 = bitcast i8* %call60 to %struct.btTriangleInfoMapData*
  %81 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_triangleInfoMap61 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %81, i32 0, i32 4
  store %struct.btTriangleInfoMapData* %80, %struct.btTriangleInfoMapData** %m_triangleInfoMap61, align 4
  %m_triangleInfoMap63 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %82 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap63, align 4
  %83 = bitcast %struct.btTriangleInfoMap* %82 to i32 (%struct.btTriangleInfoMap*)***
  %vtable64 = load i32 (%struct.btTriangleInfoMap*)**, i32 (%struct.btTriangleInfoMap*)*** %83, align 4
  %vfn65 = getelementptr inbounds i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vtable64, i64 2
  %84 = load i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vfn65, align 4
  %call66 = call i32 %84(%struct.btTriangleInfoMap* %82)
  store i32 %call66, i32* %sz62, align 4
  %85 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %86 = load i32, i32* %sz62, align 4
  %87 = bitcast %class.btSerializer* %85 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable68 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %87, align 4
  %vfn69 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable68, i64 4
  %88 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn69, align 4
  %call70 = call %class.btChunk* %88(%class.btSerializer* %85, i32 %86, i32 1)
  store %class.btChunk* %call70, %class.btChunk** %chunk67, align 4
  %m_triangleInfoMap72 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %89 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap72, align 4
  %90 = load %class.btChunk*, %class.btChunk** %chunk67, align 4
  %m_oldPtr73 = getelementptr inbounds %class.btChunk, %class.btChunk* %90, i32 0, i32 2
  %91 = load i8*, i8** %m_oldPtr73, align 4
  %92 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %93 = bitcast %struct.btTriangleInfoMap* %89 to i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)***
  %vtable74 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)**, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*** %93, align 4
  %vfn75 = getelementptr inbounds i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vtable74, i64 3
  %94 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vfn75, align 4
  %call76 = call i8* %94(%struct.btTriangleInfoMap* %89, i8* %91, %class.btSerializer* %92)
  store i8* %call76, i8** %structType71, align 4
  %95 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %96 = load %class.btChunk*, %class.btChunk** %chunk67, align 4
  %97 = load i8*, i8** %structType71, align 4
  %m_triangleInfoMap77 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %98 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap77, align 4
  %99 = bitcast %struct.btTriangleInfoMap* %98 to i8*
  %100 = bitcast %class.btSerializer* %95 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable78 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %100, align 4
  %vfn79 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable78, i64 5
  %101 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn79, align 4
  call void %101(%class.btSerializer* %95, %class.btChunk* %96, i8* %97, i32 1346456916, i8* %99)
  br label %if.end80

if.end80:                                         ; preds = %if.else56, %if.then54
  br label %if.end83

if.else81:                                        ; preds = %land.lhs.true41, %if.end39
  %102 = load %struct.btTriangleMeshShapeData*, %struct.btTriangleMeshShapeData** %trimeshData, align 4
  %m_triangleInfoMap82 = getelementptr inbounds %struct.btTriangleMeshShapeData, %struct.btTriangleMeshShapeData* %102, i32 0, i32 4
  store %struct.btTriangleInfoMapData* null, %struct.btTriangleInfoMapData** %m_triangleInfoMap82, align 4
  br label %if.end83

if.end83:                                         ; preds = %if.else81, %if.end80
  ret i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZNK22btBvhTriangleMeshShape18serializeSingleBvhEP12btSerializer(%class.btBvhTriangleMeshShape* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_bvh = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %0 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh, align 4
  %tobool = icmp ne %class.btOptimizedBvh* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_bvh2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh2, align 4
  %2 = bitcast %class.btOptimizedBvh* %1 to %class.btQuantizedBvh*
  %3 = bitcast %class.btQuantizedBvh* %2 to i32 (%class.btQuantizedBvh*)***
  %vtable = load i32 (%class.btQuantizedBvh*)**, i32 (%class.btQuantizedBvh*)*** %3, align 4
  %vfn = getelementptr inbounds i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vtable, i64 3
  %4 = load i32 (%class.btQuantizedBvh*)*, i32 (%class.btQuantizedBvh*)** %vfn, align 4
  %call = call i32 %4(%class.btQuantizedBvh* %2)
  store i32 %call, i32* %len, align 4
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %6 = load i32, i32* %len, align 4
  %7 = bitcast %class.btSerializer* %5 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable3 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %7, align 4
  %vfn4 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable3, i64 4
  %8 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn4, align 4
  %call5 = call %class.btChunk* %8(%class.btSerializer* %5, i32 %6, i32 1)
  store %class.btChunk* %call5, %class.btChunk** %chunk, align 4
  %m_bvh6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %9 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh6, align 4
  %10 = bitcast %class.btOptimizedBvh* %9 to %class.btQuantizedBvh*
  %11 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %11, i32 0, i32 2
  %12 = load i8*, i8** %m_oldPtr, align 4
  %13 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %14 = bitcast %class.btQuantizedBvh* %10 to i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)***
  %vtable7 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)**, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*** %14, align 4
  %vfn8 = getelementptr inbounds i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vtable7, i64 4
  %15 = load i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)*, i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)** %vfn8, align 4
  %call9 = call i8* %15(%class.btQuantizedBvh* %10, i8* %12, %class.btSerializer* %13)
  store i8* %call9, i8** %structType, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %18 = load i8*, i8** %structType, align 4
  %m_bvh10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 1
  %19 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %m_bvh10, align 4
  %20 = bitcast %class.btOptimizedBvh* %19 to i8*
  %21 = bitcast %class.btSerializer* %16 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable11 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %21, align 4
  %vfn12 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable11, i64 5
  %22 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn12, align 4
  call void %22(%class.btSerializer* %16, %class.btChunk* %17, i8* %18, i32 1213612625, i8* %20)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btBvhTriangleMeshShape30serializeSingleTriangleInfoMapEP12btSerializer(%class.btBvhTriangleMeshShape* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %tobool = icmp ne %struct.btTriangleInfoMap* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_triangleInfoMap2 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %1 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap2, align 4
  %2 = bitcast %struct.btTriangleInfoMap* %1 to i32 (%struct.btTriangleInfoMap*)***
  %vtable = load i32 (%struct.btTriangleInfoMap*)**, i32 (%struct.btTriangleInfoMap*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vtable, i64 2
  %3 = load i32 (%struct.btTriangleInfoMap*)*, i32 (%struct.btTriangleInfoMap*)** %vfn, align 4
  %call = call i32 %3(%struct.btTriangleInfoMap* %1)
  store i32 %call, i32* %len, align 4
  %4 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %5 = load i32, i32* %len, align 4
  %6 = bitcast %class.btSerializer* %4 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable3 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %6, align 4
  %vfn4 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable3, i64 4
  %7 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn4, align 4
  %call5 = call %class.btChunk* %7(%class.btSerializer* %4, i32 %5, i32 1)
  store %class.btChunk* %call5, %class.btChunk** %chunk, align 4
  %m_triangleInfoMap6 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %8 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap6, align 4
  %9 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %9, i32 0, i32 2
  %10 = load i8*, i8** %m_oldPtr, align 4
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %12 = bitcast %struct.btTriangleInfoMap* %8 to i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)***
  %vtable7 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)**, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*** %12, align 4
  %vfn8 = getelementptr inbounds i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vtable7, i64 3
  %13 = load i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)*, i8* (%struct.btTriangleInfoMap*, i8*, %class.btSerializer*)** %vfn8, align 4
  %call9 = call i8* %13(%struct.btTriangleInfoMap* %8, i8* %10, %class.btSerializer* %11)
  store i8* %call9, i8** %structType, align 4
  %14 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %15 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %16 = load i8*, i8** %structType, align 4
  %m_triangleInfoMap10 = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %17 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap10, align 4
  %18 = bitcast %struct.btTriangleInfoMap* %17 to i8*
  %19 = bitcast %class.btSerializer* %14 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable11 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %19, align 4
  %vfn12 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable11, i64 5
  %20 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn12, align 4
  call void %20(%class.btSerializer* %14, %class.btChunk* %15, i8* %16, i32 1346456916, i8* %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_ZNK19btTriangleMeshShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleMeshShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

declare nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK19btTriangleMeshShape15getLocalScalingEv(%class.btTriangleMeshShape*) unnamed_addr #3

declare void @_ZNK19btTriangleMeshShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleMeshShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK22btBvhTriangleMeshShape7getNameEv(%class.btBvhTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btConcaveShape9setMarginEf(%class.btConcaveShape* %this, float %collisionMargin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4
  store float %collisionMargin, float* %collisionMargin.addr, align 4
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %0 = load float, float* %collisionMargin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btBvhTriangleMeshShape28calculateSerializeBufferSizeEv(%class.btBvhTriangleMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  ret i32 60
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK19btTriangleMeshShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btTriangleMeshShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK19btTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %1 = bitcast %class.btTriangleMeshShape* %this1 to void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btTriangleMeshShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %agg.result, %class.btTriangleMeshShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackC2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  %0 = bitcast %class.btNodeOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV21btNodeOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btNodeOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback* @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback* %this1) #8
  %0 = bitcast %struct.MyNodeOverlapCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %m_triangle = alloca [3 x %class.btVector3], align 16
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %graphicsbase23 = alloca double*, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  store %struct.MyNodeOverlapCallback* %this, %struct.MyNodeOverlapCallback** %this.addr, align 4
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback*, %struct.MyNodeOverlapCallback** %this.addr, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %1 = load i32, i32* %nodeSubPart.addr, align 4
  %2 = bitcast %class.btStridingMeshInterface* %0 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %3 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %3(%class.btStridingMeshInterface* %0, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %1)
  %4 = load i8*, i8** %indexbase, align 4
  %5 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %6 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %7 = bitcast i8* %add.ptr to i32*
  store i32* %7, i32** %gfxbase, align 4
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %8 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %8)
  store %class.btVector3* %call3, %class.btVector3** %meshScaling, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %9 = load i32, i32* %j, align 4
  %cmp = icmp sge i32 %9, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i32, i32* %indicestype, align 4
  %cmp4 = icmp eq i32 %10, 3
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32*, i32** %gfxbase, align 4
  %12 = bitcast i32* %11 to i16*
  %13 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %14 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %15 = load i32*, i32** %gfxbase, align 4
  %16 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx5, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %17, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4
  %18 = load i32, i32* %type, align 4
  %cmp6 = icmp eq i32 %18, 0
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %19 = load i8*, i8** %vertexbase, align 4
  %20 = load i32, i32* %graphicsindex, align 4
  %21 = load i32, i32* %stride, align 4
  %mul7 = mul nsw i32 %20, %21
  %add.ptr8 = getelementptr inbounds i8, i8* %19, i32 %mul7
  %22 = bitcast i8* %add.ptr8 to float*
  store float* %22, float** %graphicsbase, align 4
  %23 = load float*, float** %graphicsbase, align 4
  %arrayidx10 = getelementptr inbounds float, float* %23, i32 0
  %24 = load float, float* %arrayidx10, align 4
  %25 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %25)
  %26 = load float, float* %call11, align 4
  %mul12 = fmul float %24, %26
  store float %mul12, float* %ref.tmp9, align 4
  %27 = load float*, float** %graphicsbase, align 4
  %arrayidx14 = getelementptr inbounds float, float* %27, i32 1
  %28 = load float, float* %arrayidx14, align 4
  %29 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %29)
  %30 = load float, float* %call15, align 4
  %mul16 = fmul float %28, %30
  store float %mul16, float* %ref.tmp13, align 4
  %31 = load float*, float** %graphicsbase, align 4
  %arrayidx18 = getelementptr inbounds float, float* %31, i32 2
  %32 = load float, float* %arrayidx18, align 4
  %33 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %33)
  %34 = load float, float* %call19, align 4
  %mul20 = fmul float %32, %34
  store float %mul20, float* %ref.tmp17, align 4
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %35 = load i32, i32* %j, align 4
  %arrayidx22 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %35
  %36 = bitcast %class.btVector3* %arrayidx22 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %36, i8* align 4 %37, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %cond.end
  %38 = load i8*, i8** %vertexbase, align 4
  %39 = load i32, i32* %graphicsindex, align 4
  %40 = load i32, i32* %stride, align 4
  %mul24 = mul nsw i32 %39, %40
  %add.ptr25 = getelementptr inbounds i8, i8* %38, i32 %mul24
  %41 = bitcast i8* %add.ptr25 to double*
  store double* %41, double** %graphicsbase23, align 4
  %42 = load double*, double** %graphicsbase23, align 4
  %arrayidx28 = getelementptr inbounds double, double* %42, i32 0
  %43 = load double, double* %arrayidx28, align 8
  %conv29 = fptrunc double %43 to float
  %44 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %44)
  %45 = load float, float* %call30, align 4
  %mul31 = fmul float %conv29, %45
  store float %mul31, float* %ref.tmp27, align 4
  %46 = load double*, double** %graphicsbase23, align 4
  %arrayidx33 = getelementptr inbounds double, double* %46, i32 1
  %47 = load double, double* %arrayidx33, align 8
  %conv34 = fptrunc double %47 to float
  %48 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %48)
  %49 = load float, float* %call35, align 4
  %mul36 = fmul float %conv34, %49
  store float %mul36, float* %ref.tmp32, align 4
  %50 = load double*, double** %graphicsbase23, align 4
  %arrayidx38 = getelementptr inbounds double, double* %50, i32 2
  %51 = load double, double* %arrayidx38, align 8
  %conv39 = fptrunc double %51 to float
  %52 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call40, align 4
  %mul41 = fmul float %conv39, %53
  store float %mul41, float* %ref.tmp37, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %54 = load i32, i32* %j, align 4
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %54
  %55 = bitcast %class.btVector3* %arrayidx43 to i8*
  %56 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %55, i8* align 4 %56, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %57 = load i32, i32* %j, align 4
  %dec = add nsw i32 %57, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 2
  %58 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %59 = load i32, i32* %nodeSubPart.addr, align 4
  %60 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %61 = bitcast %class.btTriangleCallback* %58 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable44 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %61, align 4
  %vfn45 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable44, i64 2
  %62 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn45, align 4
  call void %62(%class.btTriangleCallback* %58, %class.btVector3* %arraydecay, i32 %59, i32 %60)
  %m_meshInterface46 = getelementptr inbounds %struct.MyNodeOverlapCallback, %struct.MyNodeOverlapCallback* %this1, i32 0, i32 1
  %63 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface46, align 4
  %64 = load i32, i32* %nodeSubPart.addr, align 4
  %65 = bitcast %class.btStridingMeshInterface* %63 to void (%class.btStridingMeshInterface*, i32)***
  %vtable47 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %65, align 4
  %vfn48 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable47, i64 6
  %66 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn48, align 4
  call void %66(%class.btStridingMeshInterface* %63, i32 %64)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btNodeOverlapCallback* @_ZN21btNodeOverlapCallbackD2Ev(%class.btNodeOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  ret %class.btNodeOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btNodeOverlapCallbackD0Ev(%class.btNodeOverlapCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNodeOverlapCallback*, align 4
  store %class.btNodeOverlapCallback* %this, %class.btNodeOverlapCallback** %this.addr, align 4
  %this1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback.20* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback.20* @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.20* %this1) #8
  %0 = bitcast %struct.MyNodeOverlapCallback.20* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback.20* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.20*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %m_triangle = alloca [3 x %class.btVector3], align 16
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %graphicsbase23 = alloca double*, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  store %struct.MyNodeOverlapCallback.20* %this, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.20*, %struct.MyNodeOverlapCallback.20** %this.addr, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %1 = load i32, i32* %nodeSubPart.addr, align 4
  %2 = bitcast %class.btStridingMeshInterface* %0 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %3 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %3(%class.btStridingMeshInterface* %0, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %1)
  %4 = load i8*, i8** %indexbase, align 4
  %5 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %6 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %7 = bitcast i8* %add.ptr to i32*
  store i32* %7, i32** %gfxbase, align 4
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %8 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %8)
  store %class.btVector3* %call3, %class.btVector3** %meshScaling, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %9 = load i32, i32* %j, align 4
  %cmp = icmp sge i32 %9, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i32, i32* %indicestype, align 4
  %cmp4 = icmp eq i32 %10, 3
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32*, i32** %gfxbase, align 4
  %12 = bitcast i32* %11 to i16*
  %13 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %14 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %15 = load i32*, i32** %gfxbase, align 4
  %16 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx5, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %17, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4
  %18 = load i32, i32* %type, align 4
  %cmp6 = icmp eq i32 %18, 0
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  %19 = load i8*, i8** %vertexbase, align 4
  %20 = load i32, i32* %graphicsindex, align 4
  %21 = load i32, i32* %stride, align 4
  %mul7 = mul nsw i32 %20, %21
  %add.ptr8 = getelementptr inbounds i8, i8* %19, i32 %mul7
  %22 = bitcast i8* %add.ptr8 to float*
  store float* %22, float** %graphicsbase, align 4
  %23 = load float*, float** %graphicsbase, align 4
  %arrayidx10 = getelementptr inbounds float, float* %23, i32 0
  %24 = load float, float* %arrayidx10, align 4
  %25 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %25)
  %26 = load float, float* %call11, align 4
  %mul12 = fmul float %24, %26
  store float %mul12, float* %ref.tmp9, align 4
  %27 = load float*, float** %graphicsbase, align 4
  %arrayidx14 = getelementptr inbounds float, float* %27, i32 1
  %28 = load float, float* %arrayidx14, align 4
  %29 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %29)
  %30 = load float, float* %call15, align 4
  %mul16 = fmul float %28, %30
  store float %mul16, float* %ref.tmp13, align 4
  %31 = load float*, float** %graphicsbase, align 4
  %arrayidx18 = getelementptr inbounds float, float* %31, i32 2
  %32 = load float, float* %arrayidx18, align 4
  %33 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %33)
  %34 = load float, float* %call19, align 4
  %mul20 = fmul float %32, %34
  store float %mul20, float* %ref.tmp17, align 4
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %35 = load i32, i32* %j, align 4
  %arrayidx22 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %35
  %36 = bitcast %class.btVector3* %arrayidx22 to i8*
  %37 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %36, i8* align 4 %37, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %cond.end
  %38 = load i8*, i8** %vertexbase, align 4
  %39 = load i32, i32* %graphicsindex, align 4
  %40 = load i32, i32* %stride, align 4
  %mul24 = mul nsw i32 %39, %40
  %add.ptr25 = getelementptr inbounds i8, i8* %38, i32 %mul24
  %41 = bitcast i8* %add.ptr25 to double*
  store double* %41, double** %graphicsbase23, align 4
  %42 = load double*, double** %graphicsbase23, align 4
  %arrayidx28 = getelementptr inbounds double, double* %42, i32 0
  %43 = load double, double* %arrayidx28, align 8
  %conv29 = fptrunc double %43 to float
  %44 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %44)
  %45 = load float, float* %call30, align 4
  %mul31 = fmul float %conv29, %45
  store float %mul31, float* %ref.tmp27, align 4
  %46 = load double*, double** %graphicsbase23, align 4
  %arrayidx33 = getelementptr inbounds double, double* %46, i32 1
  %47 = load double, double* %arrayidx33, align 8
  %conv34 = fptrunc double %47 to float
  %48 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %48)
  %49 = load float, float* %call35, align 4
  %mul36 = fmul float %conv34, %49
  store float %mul36, float* %ref.tmp32, align 4
  %50 = load double*, double** %graphicsbase23, align 4
  %arrayidx38 = getelementptr inbounds double, double* %50, i32 2
  %51 = load double, double* %arrayidx38, align 8
  %conv39 = fptrunc double %51 to float
  %52 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call40, align 4
  %mul41 = fmul float %conv39, %53
  store float %mul41, float* %ref.tmp37, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %54 = load i32, i32* %j, align 4
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %54
  %55 = bitcast %class.btVector3* %arrayidx43 to i8*
  %56 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %55, i8* align 4 %56, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %57 = load i32, i32* %j, align 4
  %dec = add nsw i32 %57, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 2
  %58 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 0
  %59 = load i32, i32* %nodeSubPart.addr, align 4
  %60 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %61 = bitcast %class.btTriangleCallback* %58 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable44 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %61, align 4
  %vfn45 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable44, i64 2
  %62 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn45, align 4
  call void %62(%class.btTriangleCallback* %58, %class.btVector3* %arraydecay, i32 %59, i32 %60)
  %m_meshInterface46 = getelementptr inbounds %struct.MyNodeOverlapCallback.20, %struct.MyNodeOverlapCallback.20* %this1, i32 0, i32 1
  %63 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface46, align 4
  %64 = load i32, i32* %nodeSubPart.addr, align 4
  %65 = bitcast %class.btStridingMeshInterface* %63 to void (%class.btStridingMeshInterface*, i32)***
  %vtable47 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %65, align 4
  %vfn48 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable47, i64 6
  %66 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn48, align 4
  call void %66(%class.btStridingMeshInterface* %63, i32 %64)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD0Ev(%struct.MyNodeOverlapCallback.21* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %call = call %struct.MyNodeOverlapCallback.21* @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallbackD2Ev(%struct.MyNodeOverlapCallback.21* %this1) #8
  %0 = bitcast %struct.MyNodeOverlapCallback.21* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZNK22btBvhTriangleMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_EN21MyNodeOverlapCallback11processNodeEii(%struct.MyNodeOverlapCallback.21* %this, i32 %nodeSubPart, i32 %nodeTriangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.MyNodeOverlapCallback.21*, align 4
  %nodeSubPart.addr = alloca i32, align 4
  %nodeTriangleIndex.addr = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %graphicsbase29 = alloca double*, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  store %struct.MyNodeOverlapCallback.21* %this, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  store i32 %nodeSubPart, i32* %nodeSubPart.addr, align 4
  store i32 %nodeTriangleIndex, i32* %nodeTriangleIndex.addr, align 4
  %this1 = load %struct.MyNodeOverlapCallback.21*, %struct.MyNodeOverlapCallback.21** %this.addr, align 4
  %m_numOverlap = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_numOverlap, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_numOverlap, align 4
  %m_meshInterface = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %2 = load i32, i32* %nodeSubPart.addr, align 4
  %3 = bitcast %class.btStridingMeshInterface* %1 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %4 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %4(%class.btStridingMeshInterface* %1, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %2)
  %5 = load i8*, i8** %indexbase, align 4
  %6 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %7 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %6, %7
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %mul
  %8 = bitcast i8* %add.ptr to i32*
  store i32* %8, i32** %gfxbase, align 4
  %m_meshInterface2 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %9 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface2, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %9)
  store %class.btVector3* %call, %class.btVector3** %meshScaling, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load i32, i32* %j, align 4
  %cmp = icmp sge i32 %10, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %indicestype, align 4
  %cmp3 = icmp eq i32 %11, 3
  br i1 %cmp3, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %12 = load i32*, i32** %gfxbase, align 4
  %13 = bitcast i32* %12 to i16*
  %14 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i16, i16* %13, i32 %14
  %15 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %15 to i32
  br label %cond.end10

cond.false:                                       ; preds = %for.body
  %16 = load i32, i32* %indicestype, align 4
  %cmp4 = icmp eq i32 %16, 2
  br i1 %cmp4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.false
  %17 = load i32*, i32** %gfxbase, align 4
  %18 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %17, i32 %18
  %19 = load i32, i32* %arrayidx6, align 4
  br label %cond.end

cond.false7:                                      ; preds = %cond.false
  %20 = load i32*, i32** %gfxbase, align 4
  %21 = bitcast i32* %20 to i8*
  %22 = load i32, i32* %j, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %21, i32 %22
  %23 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %23 to i32
  br label %cond.end

cond.end:                                         ; preds = %cond.false7, %cond.true5
  %cond = phi i32 [ %19, %cond.true5 ], [ %conv9, %cond.false7 ]
  br label %cond.end10

cond.end10:                                       ; preds = %cond.end, %cond.true
  %cond11 = phi i32 [ %conv, %cond.true ], [ %cond, %cond.end ]
  store i32 %cond11, i32* %graphicsindex, align 4
  %24 = load i32, i32* %type, align 4
  %cmp12 = icmp eq i32 %24, 0
  br i1 %cmp12, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end10
  %25 = load i8*, i8** %vertexbase, align 4
  %26 = load i32, i32* %graphicsindex, align 4
  %27 = load i32, i32* %stride, align 4
  %mul13 = mul nsw i32 %26, %27
  %add.ptr14 = getelementptr inbounds i8, i8* %25, i32 %mul13
  %28 = bitcast i8* %add.ptr14 to float*
  store float* %28, float** %graphicsbase, align 4
  %29 = load float*, float** %graphicsbase, align 4
  %arrayidx16 = getelementptr inbounds float, float* %29, i32 0
  %30 = load float, float* %arrayidx16, align 4
  %31 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %31)
  %32 = load float, float* %call17, align 4
  %mul18 = fmul float %30, %32
  store float %mul18, float* %ref.tmp15, align 4
  %33 = load float*, float** %graphicsbase, align 4
  %arrayidx20 = getelementptr inbounds float, float* %33, i32 1
  %34 = load float, float* %arrayidx20, align 4
  %35 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %35)
  %36 = load float, float* %call21, align 4
  %mul22 = fmul float %34, %36
  store float %mul22, float* %ref.tmp19, align 4
  %37 = load float*, float** %graphicsbase, align 4
  %arrayidx24 = getelementptr inbounds float, float* %37, i32 2
  %38 = load float, float* %arrayidx24, align 4
  %39 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %39)
  %40 = load float, float* %call25, align 4
  %mul26 = fmul float %38, %40
  store float %mul26, float* %ref.tmp23, align 4
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %m_triangle = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %41 = load i32, i32* %j, align 4
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle, i32 0, i32 %41
  %42 = bitcast %class.btVector3* %arrayidx28 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %cond.end10
  %44 = load i8*, i8** %vertexbase, align 4
  %45 = load i32, i32* %graphicsindex, align 4
  %46 = load i32, i32* %stride, align 4
  %mul30 = mul nsw i32 %45, %46
  %add.ptr31 = getelementptr inbounds i8, i8* %44, i32 %mul30
  %47 = bitcast i8* %add.ptr31 to double*
  store double* %47, double** %graphicsbase29, align 4
  %48 = load double*, double** %graphicsbase29, align 4
  %arrayidx34 = getelementptr inbounds double, double* %48, i32 0
  %49 = load double, double* %arrayidx34, align 8
  %conv35 = fptrunc double %49 to float
  %50 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %50)
  %51 = load float, float* %call36, align 4
  %mul37 = fmul float %conv35, %51
  store float %mul37, float* %ref.tmp33, align 4
  %52 = load double*, double** %graphicsbase29, align 4
  %arrayidx39 = getelementptr inbounds double, double* %52, i32 1
  %53 = load double, double* %arrayidx39, align 8
  %conv40 = fptrunc double %53 to float
  %54 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %54)
  %55 = load float, float* %call41, align 4
  %mul42 = fmul float %conv40, %55
  store float %mul42, float* %ref.tmp38, align 4
  %56 = load double*, double** %graphicsbase29, align 4
  %arrayidx44 = getelementptr inbounds double, double* %56, i32 2
  %57 = load double, double* %arrayidx44, align 8
  %conv45 = fptrunc double %57 to float
  %58 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %58)
  %59 = load float, float* %call46, align 4
  %mul47 = fmul float %conv45, %59
  store float %mul47, float* %ref.tmp43, align 4
  %call48 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %m_triangle49 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %60 = load i32, i32* %j, align 4
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle49, i32 0, i32 %60
  %61 = bitcast %class.btVector3* %arrayidx50 to i8*
  %62 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %63 = load i32, i32* %j, align 4
  %dec = add nsw i32 %63, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_callback = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 2
  %64 = load %class.btTriangleCallback*, %class.btTriangleCallback** %m_callback, align 4
  %m_triangle51 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 3
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_triangle51, i32 0, i32 0
  %65 = load i32, i32* %nodeSubPart.addr, align 4
  %66 = load i32, i32* %nodeTriangleIndex.addr, align 4
  %67 = bitcast %class.btTriangleCallback* %64 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable52 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %67, align 4
  %vfn53 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable52, i64 2
  %68 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn53, align 4
  call void %68(%class.btTriangleCallback* %64, %class.btVector3* %arraydecay, i32 %65, i32 %66)
  %m_meshInterface54 = getelementptr inbounds %struct.MyNodeOverlapCallback.21, %struct.MyNodeOverlapCallback.21* %this1, i32 0, i32 1
  %69 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface54, align 4
  %70 = load i32, i32* %nodeSubPart.addr, align 4
  %71 = bitcast %class.btStridingMeshInterface* %69 to void (%class.btStridingMeshInterface*, i32)***
  %vtable55 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %71, align 4
  %vfn56 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable55, i64 6
  %72 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn56, align 4
  call void %72(%class.btStridingMeshInterface* %69, i32 %70)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btBvhTriangleMeshShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
